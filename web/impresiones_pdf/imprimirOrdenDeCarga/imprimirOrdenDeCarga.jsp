<!--  
	 - Author(s)       :      LREALES
	 - Description     :      Vista que maneja Impresion Orden de Carga en PDF
	 - Date            :      11/05/2005  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
    <title>Impresion Orden de Carga en PDF</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
    <script>
		var controlador ="<%=CONTROLLER%>";
		
		function Buscar ( orden ){
		
			if( orden != "" ){
			
				document.formulario.action = controlador+"?estado=BuscarPdf&accion=OrdenDeCarga&orden="+orden;
				document.formulario.submit();
				
			} else{
			
				alert( "Por Favor Digitar el Numero de la Orden de Carga!" );
				
			}
		}
		
		function Imprimir ( orden ){
		
			if( orden != "" ){
			
				document.formulario.action = controlador+"?estado=Pdf&accion=OrdenDeCarga";
				document.formulario.submit();
				
			} else{
			
				alert( "Por Favor Digitar el Numero de la Orden de Carga!" );
				
			}
		}		
		
		function Enviar ( formulario ){
		 	
			return false;
			
		}
    </script>
</head>
  <%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String distrito = (String) session.getAttribute("Distrito");
    String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
    String sw = (request.getParameter("sw")!=null)?request.getParameter("sw"):"";  
  %>
<body>

 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Impresion Orden de Carga en PDF"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px;"> 
 <form action='<%= BASEURL %>/impresiones_pdf/imprimirOrdenDeCarga/imprimirOrdenDeCarga.jsp?orden=""' onSubmit="return Enviar(this);" method='post' name='formulario'>
 
   <table width="30%" border="2" align="center">
   <tr>
      <td>    
           <table width='100%' align='center' class='tablaInferior'>
              <tr>
                <td colspan="2" class="barratitulo" >
                   <table cellpadding='0' cellspacing='0' width='100%'>
                     <tr class="fila">
                      <td width="50%" align="left" class="subtitulo1">Buscar</td>
                      <td width="50%" align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                    </tr>
                   </table>   
				</td>
              </tr>
              <tr class="letrafila">
                   <td width='50%' class="fila" >
                        <div align="center">
  Orden de Carga N&deg;				     </div></td>
                   <td width='50%' class="fila" align="center">
				     <input name="orden" type="text" class="textbox" id="orden" size="12" maxlength="12" value="<%= request.getParameter("orden")==null? "" : request.getParameter("orden")%>" <%=(sw.equals("Ok")?"readonly":"")%>>
                     <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" name="obligatorio"></td>
              </tr>
        </table>
      </td>
   </tr>
   </table>   
   <br>
   
   <!-- Botones -->   
   <center>
   	 <p>
        <%if ( sw.equals("Ok") ){%>
	     <img src = "<%=BASEURL%>/images/botones/imprimir.gif" style = "cursor:hand" name = "imgimprimir"   onClick = "Imprimir(orden.value); this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
        <%} else{%>
	     <img src = "<%=BASEURL%>/images/botones/buscar.gif" style = "cursor:hand" name = "imgbuscar"     onClick = "Buscar(orden.value);"                       onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">   
        <%}%>
	     <img src = "<%=BASEURL%>/images/botones/cancelar.gif" style = "cursor:hand" name = "imgcancelar"   onClick = "formulario.reset(); formulario.submit();"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	     <img src = "<%=BASEURL%>/images/botones/salir.gif"    style = "cursor:hand" name = "imgsalir"      onClick = "parent.close();"                            onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	<script>formulario.orden.focus();</script></p>
   </center>
   
   <p><% if( !Mensaje.equals("") ){%></p>
   <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="260" align="center" class="mensajes"><%=Mensaje%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
   </table>
    <p>
      <% } %>

      <%
	  Vector info_orden = null;
	  model.imprimirOrdenService.listaOrdenDeCarga( request.getParameter("distrito"), request.getParameter("orden") );
	  info_orden = model.imprimirOrdenService.getVec_lista();
	  if( ( info_orden != null ) && ( info_orden.size() > 0 ) ){ 
	  
	  		HojaOrdenDeCarga hojaOrden = ( HojaOrdenDeCarga ) info_orden.elementAt( 0 );
			
			String p1 = hojaOrden.getPrecinto1();
			String p2 = hojaOrden.getPrecinto2();
			String p3 = hojaOrden.getPrecinto3();
			String p4 = hojaOrden.getPrecinto4();
			String p5 = hojaOrden.getPrecinto5();
			String pc1 = hojaOrden.getPrecintoc1();
			String pc2 = hojaOrden.getPrecintoc2();
			
			String precintos = "";
			
			if ( !p1.equals("") && precintos.equals("") )
				precintos = p1;
			if ( !p2.equals("") && precintos.equals("") )
				precintos = p2;
			if ( !p2.equals("") && !precintos.equals("") )
				precintos = precintos + " - " + p2;
			if ( !p3.equals("") && precintos.equals("") )
				precintos = p3;
			if ( !p3.equals("") && !precintos.equals("") )
				precintos = precintos + " - " + p3;
			if ( !p4.equals("") && precintos.equals("") )
				precintos = p4;
			if ( !p4.equals("") && !precintos.equals("") )
				precintos = precintos + " - " + p4;
			if ( !p5.equals("") && precintos.equals("") )
				precintos = p5;
			if ( !p5.equals("") && !precintos.equals("") )
				precintos = precintos + " - " + p5;
			if ( !pc1.equals("") && precintos.equals("") )
				precintos = pc1;
			if ( !pc1.equals("") && !precintos.equals("") )
				precintos = precintos + " - " + pc1;
			if ( !pc2.equals("") && precintos.equals("") )
				precintos = pc2;
			if ( !pc2.equals("") && !precintos.equals("") )
				precintos = precintos + " - " + pc2;
				
			String cont = (hojaOrden.getContenedores()!=null)?hojaOrden.getContenedores():"";
	  %>
        </p>
    <table width="100%" border="2" align="center" id="tabla1" >
    <tr>
        <td width="" >
           <table width="100%" height="50%" align="center">
              <tr>
                <td class="subtitulo1">Informaci&oacute;n de la Orden de Carga # <%=hojaOrden.getOrden()%> . Valida Unicamente En La Fecha: <%=hojaOrden.getCreation_date()%></td>
                <td width="30%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table>
         <div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:100%; " >
         <table id="tabla3" width="100%">    
			<tr id="titulos">
			  <td width="8%" align="left" nowrap class="fila" abbr="">Se&ntilde;or (es):</td>
			  <td width="32%" align="left" nowrap class="letra" abbr=""><%=hojaOrden.getEmpresa()%></td>
			  <td width="8%" align="left" nowrap class="fila" abbr="">Direcci&oacute;n:</td>
			  <td width="22" colspan="2" align="left" nowrap class="letra" abbr=""><%=hojaOrden.getDireccion()%></td>
			  <td width="15%" align="left" nowrap class="fila" abbr="">Ciudad:</td>
			  <td width="15%" align="left" nowrap class="letra" abbr=""><%=hojaOrden.getCiudad()%></td>
			  </tr>
			<tr id="titulos">
			  <td align="left" nowrap class="fila" abbr="">El conductor:</td>
			  <td align="left" nowrap class="letra" abbr=""><%=hojaOrden.getNomconductor()%></td>
			  <td align="left" nowrap class="fila" abbr="">con C.C.</td>
			  <td colspan="2" align="left" nowrap class="letra" abbr=""><%=hojaOrden.getConductor()%></td>
			  <td align="left" nowrap class="fila" abbr="">de</td>
			  <td align="left" nowrap class="letra" abbr=""><%=hojaOrden.getExpced()%></td>
			  </tr>
			<tr id="titulos">
			  <td align="left" nowrap class="fila" abbr="">Pase N&deg;</td>
			  <td align="left" nowrap class="letra" abbr=""><%=hojaOrden.getPase()%></td>
			  <td align="left" nowrap class="fila" abbr="">Cami&oacute;n Placas:</td>
			  <td colspan="2" align="left" nowrap class="letra" abbr=""><%=hojaOrden.getPlaca()%></td>
			  <td align="left" nowrap class="fila" abbr="">Marca:</td>
			  <td align="left" nowrap class="letra" abbr=""><%=hojaOrden.getMarca()%></td>
			  </tr>
			<tr id="titulos">
			  <td align="left" nowrap class="fila" abbr="">Color:</td>
			  <td align="left" nowrap class="letra" abbr=""><%=hojaOrden.getColor()%></td>
			  <td align="left" nowrap class="fila" abbr="">Modelo:</td>
			  <td colspan="2" align="left" nowrap class="letra" abbr=""><%=hojaOrden.getModelo()%></td>
			  <td align="left" nowrap class="fila" abbr="">Motor:</td>
			  <td align="left" nowrap class="letra" abbr=""><%=hojaOrden.getMotor()%></td>
			  </tr>
			<tr id="titulos">
				<td align="left" nowrap class="fila" abbr="">Chas&iacute;s:</td>
				<td align="left" nowrap class="letra" abbr=""><%=hojaOrden.getChasis()%></td>
			    <td align="left" nowrap class="fila" abbr="">Afiliado a: </td>
			    <td colspan="2" align="left" nowrap class="letra" abbr=""><%=hojaOrden.getEmpresaafil()%></td>
			    <td align="left" nowrap class="fila" abbr="">Tarjeta de Operaci&oacute;n N&deg; </td>
			    <td align="left" nowrap class="letra" abbr=""><%=hojaOrden.getTarjetaoper()%></td>
			</tr>
            <tr id="titulos">
              <td colspan="7"  align="center"  nowrap class="fila" abbr="">ESTA AUTORIZADO POR NUESTRA EMPRESA PARA RECIBIR DE UD.(S) EL SIGUIENTE CARGAMENTO:</td>
              </tr>
            <tr id="titulos">
              <td  align="left"  nowrap class="fila" abbr="">Contenido:</td>
              <td colspan="6"  align="left"  nowrap class="letra" abbr=""><%=hojaOrden.getContenido()%></td>
            </tr>
            <tr id="titulos">
              <td  align="left"  nowrap class="fila" abbr="">Trailer:</td>
              <td colspan="6"  align="left"  nowrap class="letra" abbr=""><%=hojaOrden.getTrailer()%></td>
            </tr>
            <tr id="titulos">
              <td  align="left"  nowrap class="fila" abbr="">Contenedores:</td>
              <td colspan="6"  align="left"  nowrap class="letra" abbr=""><%=cont%></td>
			</tr>
            <tr id="titulos">
              <td  align="left"  nowrap class="fila" abbr="">Destino:</td>
              <td colspan="2"  align="left"  nowrap class="letra" abbr=""><%=hojaOrden.getDestino()%></td>
              <td  align="left"  nowrap class="fila" abbr="">Entregar a:</td>
              <td colspan="3"  align="left"  nowrap class="letra" abbr=""><%=hojaOrden.getEntregar()%></td>
              </tr>
            <tr id="titulos">
              <td  align="left"  nowrap class="fila" abbr="">Observaciones:</td>
              <td colspan="6"  align="left"  nowrap class="letra" abbr=""><%=hojaOrden.getObservacion()%></td>
            </tr>
            <tr id="titulos">
              <td  align="left"  nowrap class="fila" abbr="">Precintos:</td>
              <td colspan="6"  align="left"  nowrap class="letra" abbr=""><%=precintos%></td>
            </tr>
            <tr id="titulos">
              <td  align="left"  nowrap class="fila" abbr="">Elaborado por: </td>
              <td colspan="6"  align="left"  nowrap class="letra" abbr=""><%=hojaOrden.getCreation_user()%></td>
            </tr>
       	<%}%>           
          </table> 
		  </div>
        </td>
    </tr>
 </table>

 </form>
</div>  
<%=datos[1]%>
</body>
</html>