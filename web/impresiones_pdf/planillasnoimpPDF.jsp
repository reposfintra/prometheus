<!--
- Autor : Ing. Jose de la rosa
- Date  : 23 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-
-
- Ultima Modificacion :  mfontalvo
- Date  : 29 de Noviembre de 2005
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite la impresion de las planillas
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
    CIA DatosCia          = model.PlanillaImpresionSvc.getCIA(); 
    List ListaPlanillas   = model.PlanillaImpresionSvc.getPlanillas();
    String CodigoRegional = (DatosCia!=null)?DatosCia.getCodigoRegional():"";
    String CodigoEmpresa  = (DatosCia!=null)?DatosCia.getCodigoEmpresa():"";
    String Resolucion     = (DatosCia!=null)?DatosCia.getResolucion():"";
    String RangoInicial   = (DatosCia!=null)?DatosCia.getRangoInicial():"";
    String RangoFinal     = (DatosCia!=null)?DatosCia.getRangoFinal():"";
    String NombreAseguradora      = (DatosCia!=null)?DatosCia.getNombreAseguradora():"";
    String PolizaAseguradora      = (DatosCia!=null)?DatosCia.getPolizaAseguradora():"";
    String FechaVencimientoPoliza = (DatosCia!=null)?DatosCia.getFechaVencimientoPoliza():"";
    String Separador  = "~";
    String Origen     = (request.getParameter("Origen")!=null && (!request.getParameter("Origen").equals(""))   )?request.getParameter("Origen"):"FListado";
    
    Usuario   usuario = (Usuario) session.getAttribute("Usuario");
%>
<html>
<head>
<title>Impresion de Planillas</title>
<script> var Separador = '<%= Separador %>';</script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script>
var Planillas = ""; 
function go(){	
	Planillas = "";
	for (var i=0;i<forma.elements.length; i++){
	 if (forma.elements[i].type=='checkbox' && forma.elements[i].checked)
	   Planillas += forma.elements[i].value + ',';
	}
	Planillas = Planillas.substr(0, Planillas.length-1);
        
        if( Planillas != ""  ){
            if( confirm ("\nMENSAJE DE IMPRESION "+
           "\n\n� Desea imprimir las remesas asociadas a esta planillas(s)? \n"+
           "\nPLANILLA(S)\t"+ Planillas   +
            "\n\nNOTA: \nAsegurese que la impresora est� lista para llevar a cabo \t\nla impresi�n")){
                            forma.opcion.value = "conremesas";			
                            forma.submit();
                    }
                    else{
                            forma.opcion.value = "sinremesas";
                            forma.submit();
                    }
                    Planillas="";
        }
        else{
          alert('Debe seleccionar alguna planilla');
        }
}
</script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Impresi�n de Planillas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
<form action="<%=CONTROLLER%>?estado=Impresion&accion=PlanillasPDF" method="post" name="forma" id="forma">
<input type='hidden'   name='opcion' value=''>
<%  if (ListaPlanillas!=null && ListaPlanillas.size()>0)  {  %>
<br><br>
<table width='800' border='2'>
	<tr>
		<td>
			<table width="100%" align="center">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Imprimir Planillas</td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
			  <tr class="tblTitulo" align="center">
				<th>&nbsp;</th>
				<th>Planilla</th>
				<th>Fecha</th>
				<th>Conductor</th>
				<th>Placa</th>
				<th>Ruta</th>
				<th>Remesas - Clientes</th>
			 </tr>
		
			<% 	int i=0;
				Iterator it = ListaPlanillas.iterator();
				while(it.hasNext()){
					DatosPlanillaImp datos = (DatosPlanillaImp) it.next();
					String estilo = (i % 2 == 0 )?"filagris":"filaazul";
			%>

			  
			  <span class="letra">
			  
			  </span>
			  <tr <% if( datos.getPlanviaje().equals("S")  ){ %>onclick="jscript: Impresa.checked = !Impresa.checked; this.className=(Impresa.checked?'filaseleccion':'<%= estilo %>'); this.cells[6].firstChild.className=(Impresa.checked?'filaseleccion':'<%= estilo %>'); " <%}else{%> onclick='alert("La planilla no puede ser seleccionada, debe crearse primero su plan de viaje")' <%}%> class="<%= estilo %>" style="cursor:hand" >
				<td valign='center' class="bordereporte" style='color '>				    
                                    <input <% if( datos.getPlanviaje().equals("N")  ){ %> disabled title='Se debe crear primero, el plan de viaje de esta planilla'  <%}%> type="checkbox" name="Impresa" onclick="jscript: Impresa.checked = !Impresa.checked; " value='<%= (datos.getNumeroPlanilla()  !=null)?datos.getNumeroPlanilla()  :"&nbsp"  %>'>                                   
				</td>
				<td class="bordereporte" ><%= (datos.getNumeroPlanilla()  !=null)?datos.getNumeroPlanilla()  :"&nbsp"  %></td>
				<td class="bordereporte" ><%= (datos.getFechaPlanilla()   !=null)?datos.getFechaPlanilla()   :"&nbsp"  %></td>
				<td class="bordereporte" ><%= (datos.getNombreConductor() !=null)?datos.getNombreConductor() :"&nbsp"  %></td>
				<td class="bordereporte" ><%= (datos.getPlaca() !=null&&  !datos.getPlaca().equals(""))?     datos.getPlaca() :"&nbsp"  %></td>
				<td class="bordereporte" ><%= (datos.getRuta()            !=null)?datos.getRuta()            :"&nbsp"  %></td>
				<td class="bordereporte" >
				  <% i++;
					List Remesas = datos.getRemesas();
					int CantidadRemesas = 0;
					if (Remesas!= null && Remesas.size()>0){
						Iterator it2 = Remesas.iterator();
						out.println("\t<table width='100%' border='0' align='center' class="+ estilo +">");
						while (it2.hasNext()){
							DatosRemesaImp datosR = (DatosRemesaImp) it2.next();
							CantidadRemesas++;
							String NumeroRemesa   = (datosR.getNumeroRemesa()    !=null)?datosR.getNumeroRemesa()    :"";
							String DocInterno     = (datosR.getDocumentoInterno()!=null)?datosR.getDocumentoInterno():"";
							String Cliente        = (datosR.getCliente()         !=null)?datosR.getCliente()         :"";
							out.println("\t<tr ><td valign='top' >"+ NumeroRemesa +" - "+ Cliente +"</td></tr>");
							out.println("\t\t<input type='hidden' name='Remesas' value='" + NumeroRemesa + Separador + DocInterno + Separador + Cliente + "'>");
						}//end while
						out.println("\t</table>");
                                                String TextoOC = ((DatosRemesaImp) Remesas.get(0)).getTextoOC();
						out.print("<input type='hidden' name='TextoOC' value='"+  (TextoOC!=null?TextoOC:"") +"'/>");
					}else{
						out.println("\tNo hay remesas");
						out.print("<input type='hidden' name='TextoOC' value=''/>");
					}
					
					
					// informacion de los descuentos
					List Descuentos    = datos.getDescuentos();
					if (Descuentos!=null && Descuentos.size()>0){
						Iterator itDesc = Descuentos.iterator();
						while(itDesc.hasNext()){
							DatosDescuentosImp datosDesc = (DatosDescuentosImp)itDesc.next();
							String Codigo   = (datosDesc.getCodigo()   !=null)?datosDesc.getCodigo()  :"";
							String Concepto = (datosDesc.getConcepto() !=null)?datosDesc.getConcepto():"";
							String Valor    = (datosDesc.getValor()    !=null)?datosDesc.getValor()   :"00.00";
							String Moneda   = (datosDesc.getMoneda()   !=null)?datosDesc.getMoneda()  :"";
							String Tipo     = (datosDesc.getTipo()     !=null)?datosDesc.getTipo()    :"";
							out.println("\t\t<input type='hidden' name='Descuentos' value='"+ Codigo + Separador + Concepto + Separador + Valor + Separador + Moneda + Separador + Tipo + "'>");
						}
					}
			%>
				  </td>
			  </tr>
			  <span class="letra">
			  </span>
			<% } %>
			</table>
		</td>
	</tr>
</table>
    <br>


        <img src="<%=BASEURL%>/images/botones/imprimir.gif" title="Buscar Remesas" style="cursor:hand" name="Imprimir_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="go();">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
</form>
    <% if (Origen.equals("FBuscar")) {  %>
         <input type="image"  src="<%=BASEURL%>/images/botones/regresar.gif" title="Regresar" style="cursor:hand" name="regresar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
    <%  }
    }else { %>
	<table border="2" align="center">
            <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="229" align="center" class="mensajes">No hay planillas por Imprimir...</td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
	</table>
    <% } %>
</center>
</div>
</body>
</html>

