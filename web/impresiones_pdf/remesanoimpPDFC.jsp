<!--
- Autor : Ing. Jose de la rosa
- Date  : 22 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite imprimir una remesa
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Imprimir Remesas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar_2.js"></script>

<% Usuario usuario = (Usuario)session.getAttribute("Usuario"); %>

</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Imprimir Remesas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<div align="center">
 <form action="<%=CONTROLLER%>?estado=Impresion&accion=RemesasPDF" method="post" name="forma" id="forma">
  <input type="hidden" name="opcion" value="multiple">
  <%
List     Lista;
Iterator It;
%>
  <%
if(model.RemesaSvc.getRegistros().size()>0)
{
%>
  <input type='checkbox' Id='Verificacion'>
  <span  style="color: #003399;">Preguntar antes de Imprimir</span>
</div>
<br>
<table width='990' border='2'>
	<tr>
		<td>
			<table width="100%" align="center">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Imprimir Remesas</td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">       
				<tr class="tblTitulo" align="center">
					<td width='1%'  align='center'>&nbsp;</td>
					<td width='6%'  align='center'>Remesa</td>
					<td width='15%' align='center'>Fecha</td>
					<td width='17%' align='center'>Cliente</td>
					<td width='17%' align='center'>Ciudad de Origen</td>
					<td width='17%' align='center'>Ciudad de Destino</td>
			  </tr>
				<%
				Lista = model.RemesaSvc.getRegistros();
				It = Lista.iterator();
				int i=0;
				String Remesa = "?";
				while(It.hasNext()) {
					Remesa datos = (Remesa) It.next();
                    String estilo = (i % 2 == 0 )?"filagris":"filaazul";
                    if (datos.getEstado().equals("P")) estilo = "filaresaltada";
	 				  if(!Remesa.equals(datos.getNumrem())) {  %>
 						

						<tr class="<%= estilo %>" <% if (datos.getEstado().equals("P")) { out.print("onclick='warningRemesa();' style='cursor:hand;'"); }  %>><%
						out.print("<td class='bordereporte' align='center'><input type='checkbox' name='Impresa' value="+datos.getNumrem()+" "+ (datos.getEstado().equals("P")?"disabled":"") +"></td>\n");
						out.print("<td class='bordereporte' align='center'>"+datos.getNumrem()+"</td>\n");
						out.print("<td class='bordereporte'>"+datos.getFecRem()+"</td>\n");
						out.print("<td class='bordereporte'>"+datos.getCliente()+"</td>\n");
						out.print("<td class='bordereporte'>"+datos.getOriRem()+"</td>\n");
						out.print("<td class='bordereporte'>"+datos.getDesRem()+"</td>\n");
						out.print("</tr>\n");
						i++;
						%> 
						<input type='hidden' name='Usuario'      value='<%= usuario.getNombre()%>'  >
						<input type='hidden' name='agenciaUsuario'  value='<%= usuario.getId_agencia()%>'  >
						<input type='hidden' name='Precinto'     value='<%= (datos.getDerechosCedido()==null)?"":datos.getDerechosCedido()%>'  >
						<input type='hidden' name='Observacion'  value='<%= (datos.getObservacion()    !=null?datos.getObservacion():"")    %>'>
						<input type='hidden' name='DocInterno'   value='<%= (datos.getDocInterno()     !=null?datos.getDocInterno():"")     %>'>
						<input type='hidden' name='Remitente'    value='<%= (datos.getRemitente()      !=null?datos.getRemitente():"")      %>'>
						<input type='hidden' name='DireccionDes' value='<%= (datos.getDirDestinatario()!=null?datos.getDirDestinatario():"")%>'>
						<input type='hidden' name='Destinatario' value='<%= (datos.getDestinatario()   !=null?datos.getDestinatario():"")   %>'>
						<input type='hidden' name='CiudadDes'    value='<%= (datos.getCiuDestinatario()!=null?datos.getCiuDestinatario():"")%>'>
						<input type='hidden' name='AgenciaOri'   value='<%= (datos.getAgcRem()         !=null?datos.getAgcRem():"")         %>'>
						<input type='hidden' name='FechaImp'     value='<%= (datos.getPrinter_Date()   !=null?datos.getPrinter_Date():"")   %>'>
						<input type='hidden' name='Numrem'       value='<%= (datos.getNumrem()         !=null?datos.getNumrem():"")         %>'>
						<input type='hidden' name='Fecha'        value='<%= datos.getFecRem() %>'>
						<input type='hidden' name='Cliente'      value='<%= (datos.getCliente()        !=null?datos.getCliente():"")        %>'>
						<input type='hidden' name='Origen'       value='<%= (datos.getOriRem()         !=null?datos.getOriRem():"")         %>'>
						<input type='hidden' name='Destino'      value='<%= (datos.getDesRem()         !=null?datos.getDesRem():"")         %>'>
						<input type='hidden' name='Oc'           value='<%= (datos.getOc()             !=null?datos.getOc():"")             %>'>
						<input type='hidden' name='Placa'        value='<%= (datos.getPlaca()          !=null?datos.getPlaca():"")          %>'>
						<input type='hidden' name='Conductor'    value='<%= (datos.getConductor()      !=null?datos.getConductor():"")      %>'>
						<input type='hidden' name='Contenedor'   value='<%= (datos.getContenedor()     !=null?datos.getContenedor():"")      %>'>
						<input type='hidden' name='TextoExt'     value='<%= (model.RemesaSvc.getTextoExtendido()!=null?model.RemesaSvc.getTextoExtendido().replaceAll("\n|'","") : "")      %>'>
						<input type='hidden' name='Fiduciaria'   value='<%= (datos.getFiduciaria()     !=null?datos.getFiduciaria():"")      %>'>
						<%
					}
					Remesa = datos.getNumrem();
				}
				%>    
			</table>
		</td>
	</tr>
</table>
<br>
<p align="center">
	<input type="image" src="<%=BASEURL%>/images/botones/imprimir.gif"  title="Buscar Remesas" style="cursor:hand" name="Aceptar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
</p>
</form>
<%
}
else{
%>
<br>
<table border="2" align="center">
	<tr>
		<td>
			<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
				<tr>
					<td width="229" align="center" class="mensajes">No hay Remesas para imprimir...</td>
					<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
					<td width="58">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>       
<%
}
%>
</div>
</body>
</html>
