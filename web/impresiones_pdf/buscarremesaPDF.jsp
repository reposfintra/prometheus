<!--
- Autor : Ing. Juan Manuel Escando Perez
- Date  : 29 Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite buscar una remesa para la impresion de esta
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    List Listado    =  model.remesaImpresionSvc.getList();
    String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
    String numrem  = (request.getParameter("numrem")!=null)?request.getParameter("numrem"):"";
%>
<script>
var flag = false;
function Validar(){
    if( forma.numpla != null ){
        if(flag == false){
          alert('Debe escojer una Planilla');
          return false;
        }
    }
    return true;
}
</script>
<html>
<head><title>Impresion Remesa</title></head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Impresión de Remesas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
	<form action="<%=CONTROLLER%>?estado=Impresion&accion=RemesasPDF" method="post" name="forma" onsubmit='return Validar();'>
		<table align="center" width="350" border="2">
			<tr>
				<td>
					<TABLE width="100%" align="center"  class="tablaInferior">
				        <tr class="fila">
							<td width="50%" class="subtitulo1">&nbsp;Impresión de Remesas </td>
							<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
						<tr class="fila">
							<td align="center">Numero de Remesa.</td>
							<td><input name='numrem' type='text' class="textbox" size="10" maxlength="10" value='<%=numrem%>'></td>
						</tr>
					</TABLE>
				</td>
			</tr>
		</table>
		<input type='hidden' name='Origen' value='FBuscar'>
		<input name="opcion" type='hidden' value='Buscar' >
		<p align="center">
			<input type="image" src="<%=BASEURL%>/images/botones/imprimir.gif" title="Buscar Remesas" style="cursor:hand" name="Aceptar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
		</p>
	
	<br>
	<%--Mensaje de informacion--%>
	<%if(!Mensaje.equals("")){%>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%= Mensaje %></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<%}%>

<% if(Listado!=null && Listado.size()>0) { %>
	 <table width="400" border="2" align="center">
      <tr>
        <td><table width="100%"  border="0" class="tablaInferior">
           <tr>
          	<td width="60%" class="subtitulo1">Listado de Planilla Asociadas</td>
            	<td width="40%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        	</tr>    
		  </table>
          <table width="100%"  border="0" class="tablaInferior">
		  <tr class="tblTitulo">
            <th width="10%">N&ordm;</th>
            <th width="10%"></th>
            <th width='40%'>PLANILLA</th>
            <th width='40%'>PLACA</th>
          </tr>
          <%
           int Cont = 1;
           Iterator it2 = Listado.iterator();
            while(it2.hasNext()){
              	DatosRemesaImpPDF dat  = (DatosRemesaImpPDF) it2.next();
		String Estilo = (Cont % 2 == 0 )?"filagris":"filaazul";
           %>
          <tr class='<%=Estilo%>'>
            <td align='center' class="bordereporte"><%=Cont++%></td>
            <td align='center' class="bordereporte"><input name="numpla" id="numpla" type="radio" value="<%=dat.getNumpla()%>" onclick='flag=true;'></td>
            <td class="bordereporte" align="center"><%=(dat.getNumpla()!=null)?dat.getNumpla():""%></td>
            <td align='center' class="bordereporte"><%=(dat.getPlacas()!=null)?dat.getPlacas():""%></td>
          </tr>
          <%  }   %>
        </table></td>
      </tr>
    </table>
     <%  } %>
     </form>
</div>
</body>
</html>
