<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.pdf.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ page import="java.io.File"%>
<html>
<head>
<title>Impresion Planilla</title>
</head>

<body>
<%
  File xslt = (File) session.getAttribute("ArchivoXSLTPlanilla");
  File pdf = (File) session.getAttribute("ArchivoPDFPlanilla");
  List datos = (List)session.getAttribute("datosPlanilla");
  PlanillaPDF planilla_pdf = new PlanillaPDF();

  planilla_pdf.generarPDF(xslt, pdf,datos);
  response.sendRedirect(BASEURL+"/pdf/p2.pdf");
%>
</body>
</html>
