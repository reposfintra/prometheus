<%@page contentType="text/html"%>
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<% UtilFinanzas u = new UtilFinanzas();



%>
<html>
<head>
<title>Relacion Egreso</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
</head>

<script>
function Info(){
    var msg = "Se va a generar el archivo en excel, los registros que se generen\n";
       msg += "en el archivo seran marcados si usted es un usuario habilitado\n";
       msg += "y no se podran ver mas en generaciones posteriores en excel.\n";
       msg += " Podran seguir siendo consultados por la interfaz principal\n";       
       alert(msg)
}
</script>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Relaci�n de Egresos "/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
 	<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Egreso">
      <input name="Guardar" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Info();Opcion.value='Guardar';">      
      <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
	  
      <input name="Opcion" type="hidden" id="Opcion">
    </form>
	<table width="100%" border="1" cellpadding="2" cellspacing="1" bordercolor="#CCCCCC">
	  <% //lista bancos 
		 List	ListaBancos 	= model.ReporteEgresoSvc.getListBancos(); 
		 Iterator It = ListaBancos.iterator();  
		 while(It.hasNext()) {
		 	Banco banco = (Banco) It.next();
	  	%>
	  <tr>
        <td class="subtitulo1"><strong>BANCO : <%=banco.getBanco() + " " + banco.getBank_account_no()%></strong></td>
      </tr>
	  <tr>
	  <td class="fila">
	  <table width="100%"  border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
	  <tr class="tblTitulo">
	  <td align="center">&nbsp;</td>
	  <td align="center">BANCO</td>
	  <td align="center">SUC</td>
	  <td align="center">CHEQUE</td>
	  <td align="center">VALOR CHEQUE EN PESOS</td>
	  <td align="center">VALOR CHEQUE</td>
	  <td align="center">MONEDA CHEQUE</td>
	  <td align="center">BENEFICIARIO</td>
	  <td align="center">FECHA CHEQUE</td>
	  <td align="center">DESPACHADOR</td> 
          <td align="center">PROVEEDOR</td>
	  <td align="center">ESTADO</td>  
	  </tr>
	  <% //Relacion Egreso AND IF CONDICIONAL BANCO
	  List	ListaRE			= model.ReporteEgresoSvc.getListRE();
	  int sw = 0;
	  int i=0;
	  Iterator It2 = ListaRE.iterator();  
	  double ttl_ml = 0;
		 while(It2.hasNext()) {
		 	ReporteEgreso re = (ReporteEgreso) It2.next();
		 	if(re.getBanco().equals(banco.getBanco()) && re.getBancoCuenta().equals(banco.getBank_account_no())){		 	
				ttl_ml += re.getValorChk();
				sw = 1;
				i++;
	  %>
	   <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
	   <td align="center" class="bordereporte"><img src="<%= BASEURL %>/images/botones/iconos/detalles.gif" alt="Ver detalle del cheque"  name="imgsalir" style="cursor:hand" title="Ver detalles"  onClick="window.open('<%= CONTROLLER %>?estado=ConsultarEgresos&accion=Manager&opc=ver&dstrct=<%= re.getDistrito() %>&branch_code=<%= re.getBanco()%>&bank_account_no=<%= re.getBancoCuenta() %>&document_no=<%= re.getNumeroChk()%>','DETALLCHEQ','status=yes,scrollbars=no,width=780,height=650,resizable=yes');"></td>
	   <td align="center" class="bordereporte"><%=re.getBanco()%></td>
	   <td align="center" class="bordereporte"><%=re.getBancoCuenta()%></td>
	   <td align="center" class="bordereporte"><%=(re.getNumeroChk()!=null)?re.getNumeroChk():""%></td>
	   <td align="center" class="bordereporte"><div align="right"><%= u.customFormat2(re.getValorChk())%></div></td>
	   <td align="center" class="bordereporte"><%= u.customFormat2(re.getVlr_for())%></td>
	   <td align="center" class="bordereporte"><%=(re.getMoneda()!=null)?re.getMoneda():""%></td>
	   <td class="bordereporte"><%=(re.getBeneficiario()!=null)?re.getBeneficiario():""%></td>
	   <td align="center" class="bordereporte"><%=(re.getFechaChk()!=null)?re.getFechaChk():""%></td>
	   <td align="center" class="bordereporte"><%=(re.getDespachador()!=null)?re.getDespachador():""%></td>
	   <td align="center" class="bordereporte"><%=(re.getProveedor()!=null)?re.getProveedor():""%></td>
	   <td align="center" class="bordereporte"><%if(re.getReg_status().equals("A")){%>ANULADO<%}else{%>&nbsp;<%}%></td>
	   </tr>
	   <%	}//CERRA IF AND CICLO REPETITIVO DE RELACION DE EGRESO 
		  }	%>
	   <tr class="filaverde">
	   <td colspan="4" align="center" class="bordereporte"><div align="right"><strong>TOTAL EN PESOS</strong> </div></td>
	   <td align="center" class="bordereporte"><div align="right"><strong><%= u.customFormat2(ttl_ml)%></strong></div></td>
	   <td colspan="7" align="center" class="bordereporte">&nbsp;</td>
	   </tr>
		    
	      <% if(sw == 0){%>
		   <tr class="fila">
            <td colspan="23" class="filaverde"><strong>NO SE ENCONTRARON DATOS PARA ESTE BANCO </strong></td>
          </tr>
		  <%}%>
	  </table>
	  </td>
	  </tr>
	  <%//FIN BANCOS
	  }
	  %>
	</table>
	<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Egreso">
            <input name="Guardar" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Info();Opcion.value='Guardar';">            
            <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
      <input name="Opcion" type="hidden" id="Opcion">
    </form>
	</div>
</body>
</html>
