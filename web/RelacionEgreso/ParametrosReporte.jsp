<%@page contentType="text/html"%>
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% 
List	ListaAgencias = (List) request.getAttribute("agencias");//model.ciudadService.ListarAgencias();
TreeMap bancos = model.servicioBanco.getBanco();
TreeMap sucursales = model.servicioBanco.getSucursal();
TreeMap agencias = (TreeMap) request.getAttribute("agencias_tm");

bancos.put( " Seleccione", "");
sucursales.put( " Seleccione", "");
agencias.put( " Seleccione", "");

%>

<html>
<head>
<title>Relacion de Egreso</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src='<%=BASEURL%>/js/validar.js'></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
<script>
	function loadBancos(){
		form1.action = '<%= CONTROLLER%>?estado=ReporteEgreso&accion=Cargar&target=bancos';
		form1.submit();
	}
	
	function loadSucursales(){
		form1.action = '<%= CONTROLLER%>?estado=ReporteEgreso&accion=Cargar&target=sucursales';
		form1.submit();
	}
</script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Relaci�n de Egresos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Egreso" onSubmit="return verificarReporteEgreso();">
  <table width="60%" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1">Relaci�n de Egresos
            <input name="Opcion" type="hidden" id="Opcion2"></td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>        <table width="100%"  class="tablaInferior">
       
        <tr class="fila">
          <td align="left"class="letra_resaltada">&nbsp;Fecha Inicial</td>
          <td align="left">&nbsp;&nbsp;
              <input name="fechai" type="text" class="textbox" id="fechai" value="<%= request.getAttribute("fechai")!=null ? (String) request.getAttribute("fechai") : "" %>">
              <span class="Letras"> <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechai);return false;" HIDEFOCUS>&nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></span></td>
        </tr>
        <tr class="fila">
          <td align="left"class="letra_resaltada">&nbsp;&nbsp;Fecha Final</td>
          <td align="left">&nbsp;&nbsp;
              <input name="fechaf" type="text" class="textbox" id="fechaf" value="<%= request.getAttribute("fechaf")!=null ? (String) request.getAttribute("fechaf") : "" %>">
              <span class="Letras"><a href="javascript:void(0)" onclick="jscript: show_calendar('fechaf');" HIDEFOCUS> </a><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechaf);return false;" HIDEFOCUS>&nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></span></td>
        </tr>
        <tr class="fila">
          <td class="letra_resaltada" align="left">&nbsp;&nbsp;Agencia</td>
          <td align="left">&nbsp;&nbsp;&nbsp;<input:select name="agencia" attributesText="class=textbox onChange='loadBancos();' id='agencia'" options="<%=agencias %>"/>              
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
        </tr>
		<tr class="fila">
		  <td class="letra_resaltada" align="left">&nbsp;&nbsp;Banco</td>
		  <td align="left">&nbsp;&nbsp;&nbsp;<input:select name="banco" attributesText="class=textbox onChange='loadSucursales();'" options="<%=bancos %>"/></td>
		  <tr class="fila">
		  <td class="letra_resaltada" align="left">&nbsp;&nbsp;Sucursal</td>
		  <td align="left">&nbsp;&nbsp;&nbsp;<input:select name="sucursal" attributesText="class=textbox" options="<%=sucursales %>"/></td>
		  <tr class="fila">
			<td class="letra_resaltada" align="left">&nbsp;&nbsp;Usuario</td>
			<td align="left">&nbsp;&nbsp;
			<input name="usuario" type="text" class="textbox" id="usuario" value="<%= request.getAttribute("usuario")!=null ? (String) request.getAttribute("usuario") : "" %>" maxlength="15">
			</td>
		  <tr>
		  <tr class="fila">
                    <td class="letra_resaltada" align="left">&nbsp;&nbsp;Nit Proveedor</td>
                        <td align="left">&nbsp;&nbsp;
			<input name="usuario" type="text" class="textbox" id="proveedor" maxlength="15">
			</td>
		   <tr>
      </table></td>
    </tr>
  </table>
  <br>
  <table align="center">
<tr>
        <td colspan="2" nowrap align="center">		
		<input name="Guardar" src="<%=BASEURL%>/images/botones/aceptar.gif" align="middle" type="image"  id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="Opcion.value='Generar';">&nbsp;
	    <img name="cerrar" src="<%=BASEURL%>/images/botones/salir.gif" align="middle" type="image" id="cerrar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="window.close();" style="cursor:hand "></td>
      </tr>
</table>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
