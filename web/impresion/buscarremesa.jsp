<!--
- Autor : Ing. Jose de la rosa
- Date  : 22 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite buscar una remesa
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Buscar Remesas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script language=JavaScript1.2 src="<%=BASEURL%>/js/coolmenus3.js">
</script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Remesas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
	<FORM name='formulario' id='formulario' method='POST' onsubmit='jscript: if(formulario.Numrem.value==""){ alert("El campo Numero de la remesa no puede ser vacio..."); return false; }' action='<%= CONTROLLER %>?estado=Remesa&accion=Buscar'>
		<table align="center" width="350" border="2">
			<tr>
				<td>
					<table width='100%' align="center"  class="tablaInferior">
				        <tr class="fila">
							<td align="left" class="subtitulo1">&nbsp;Buscar Remesas</td>
							<td align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>      
						<tr class="fila">
							<td width='50%'align='center' class='fila'>Numero de la Remesa</td>
							<td width='50%'  align='center' class=''><div align="left">
							  <input name='Numrem' type='text' class="textbox" size="10" maxlength="10">
							  </div></td>        
						</tr>
					</table>
				</td>
			</tr>
	  </table>
		<p align="center">
			<input type="image" src="<%=BASEURL%>/images/botones/buscar.gif" title="Buscar Remesas" style="cursor:hand" name="modificar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
		</p>
	</FORM>
	<%if((request.getParameter("Mensaje")!=null) && (!request.getParameter("Mensaje").equals(""))){%>
		<%--Mensaje de informacion--%>
		<br>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=request.getParameter("Mensaje")%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<%}%>
</div>
</body>
</html>
