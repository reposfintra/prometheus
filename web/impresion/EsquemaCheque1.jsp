<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Esquema de Cheques</title>
<link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>
	<link href="../css/estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>
</head>
<body>
    <% //--- variables de bloqueos                                 
        String estadoInsert = request.getParameter("estadoInsert");
        String estadoSearch = request.getParameter("estadoSearch");
        String estadoUpdate = request.getParameter("estadoUpdate");
        String estadoDelete = request.getParameter("estadoDelete");
        String botones      = request.getParameter("botones");     
        String comentario   = request.getParameter("comentario");  
        EsquemaCheque esquema = model.EsquemaChequeSvc.getEsquema();
    %>
    
    <FORM ACTION="<%=CONTROLLER%>?estado=EsquemaCheque&accion=Manager" METHOD='post' id='formulario' name='formulario'>
    <TABLE width='400'  class='letra'>
      <TR><TH class='titulo' colspan='2' align='center' height='50'> ESQUEMA IMPRESION DE CHEQUES</TH>
      </TR>
      <TR class="fila">
        <TD class='comentario' width='50%'> &nbsp Distrito :               </TD>
           <TD width='50%'>  
              <select class='comentario' name='distrito'>
                 <option value='FINV'>FINV</option>
             </select>
      </TD></TR>
            
       <TR class="fila">
         <TD class='comentario' width='50%'> &nbsp Banco :                  </TD>
            <TD width='50%'>  
                  <%
                   if(esquema!=null){%>
                     <input type='text' class='comentario' style='width=130' name='banco' value='<%= esquema.getBanco() %>' readonly='readonly'> 
                   <%}else{%>
                   <select class='comentario' name='banco' style='width=130'>
                     <option value=''>Escoja Banco</option>
                   <% List listadoBanco = model.EsquemaChequeSvc.searchBancos();
                     if(listadoBanco!=null){
                        Iterator itbanco = listadoBanco.iterator();
                        while(itbanco.hasNext()){
                          CodeValue banco = (CodeValue) itbanco.next();
                          %><option value='<%= banco.getCodigo() %>'><%= banco.getCodigo() %></option><%
                        }
                     }
                     else{%> <option value=''>No hay bancos</option>   <%}%>
                   </select>
                 <% }%>
                
      </TD></TR>
            
        <TR class="fila">
          <TD class='comentario' width='50%'> &nbsp Densidad Impresion (CPI):</TD>
            <TD width='50%'>  
               <% int est2=(esquema!=null)?esquema.getCPI():0;%>
                <input type='text' value='<%= est2 %>' class='comentario' name='cpi' maxlength='4' size='5'>       
      </TD></TR>
            
        <TR class="fila">
          <TD class='comentario' width='50%'> &nbsp Descripci�n Campo :      </TD>
            <TD width='50%'>
               <%
                 if(esquema!=null){%>
                    <input type='text' class='comentario' style='width=130' name='descripcion' value='<%= esquema.getDescripcion() %>' readonly='readonly'> 
                <% }else{%>
                    <select class='comentario' name='descripcion' style='width=130'>
                      <option value=''>Escoja Campo</option>
                        <option value='A�o'          >A�o</option>
                        <option value='Mes'          >Mes</option>
                        <option value='Dia'          >Dia</option>
                        <option value='Valor'        >Valor</option>
                        <option value='Beneficiario' >Beneficiario</option>
                        <option value='Monto Escrito'>Monto Escrito</option>
                    </select>
                <%}%>
      </TD></TR>
             
        <TR class="fila">
          <TD class='comentario' width='50%'> &nbsp Posici�n de Linea :      </TD>
            <TD width='50%'> 
                <% int est4=(esquema!=null)?esquema.getLinea():0;%> 
                <input type='text' value='<%=est4%>' class='comentario' name='linea' maxlength='4' size='5'>     
      </TD></TR>
            
        <TR class="fila">
          <TD class='comentario' width='50%'> &nbsp Posici�n de Columna :    </TD>
            <TD width='50%'>
                <% int est5=(esquema!=null)?esquema.getColumna():0;%>
                <input type='text' value='<%=est5%>' class='comentario' name='columna' maxlength='4' size='5'>    
      </TD></TR>
    </TABLE>
    <br>
    <TABLE BGCOLOR='cdcdcd' class="pie">
     <TR><TD>
       <input type='button' value='Nuevo'    style='width=75'                  onclick="ValidarConductor(5);">
       <input type='button' name='insertar'  style='width=75' value='Insertar' <%= estadoInsert %>    onclick="ValidarEsquema(1);">
       <input type='button' name='buscar'    style='width=75' value='Listar'   <%= estadoSearch %>    onclick="ValidarEsquema(2);">
       <input type='button' name='modificar' style='width=75' value='Modificar'<%= estadoUpdate %>    onclick="ValidarEsquema(3);">
       <input type='button' name='eliminar'  style='width=75' value='Eliminar' <%= estadoDelete %>    onclick="ValidarEsquema(4);">
       <input type='hidden' name='evento'>
     </TD></TR>
     </TABLE>    
    </FORM>
    
    <!--  Presentamos el listado de esuqema -->    
    <%
    List listado = model.EsquemaChequeSvc.getList();
    if(listado!=null && listado.size()>0){%>
        <TABLE width='550' border='1' cellpadding='2' cellspacing='1' bordercolor="#CCCCCC" class='letra'>
        <TR><TH class='titulo' colspan='7'>LISTADO DE ESQUEMAS</TH>
        </TR>
        <TR class='subtitulos'>
              <TH width='10%'>DISTRITO</TH>
              <TH width='30%'>BANCO   </TH>
              <TH width='20%'>CAMPO   </TH>
              <TH width='8%'>CPI     </TH>
              <TH width='10%'>LINEA   </TH>
              <TH width='12%'>COLUMNA </TH>
              <TH width='10%'>LINK    </TH>
        </TR>
     <%Iterator itLista = listado.iterator();
      while(itLista.hasNext()){
         EsquemaCheque objeto = (EsquemaCheque)itLista.next();%>
         <TR class='fila'>
              <TD align='center'> <%= objeto.getDistrito()   %> </TD>
              <TD align='center'> <%= objeto.getBanco()      %> </TD>
              <TD align='center'> <%= objeto.getDescripcion()%> </TD>
              <TD align='center'> <%= objeto.getCPI()        %> </TD>
              <TD align='center'> <%= objeto.getLinea()      %> </TD>
              <TD align='center'> <%= objeto.getColumna()    %> </TD>
              <TD align='center'> 
                  <a href="<%=CONTROLLER%>?estado=EsquemaCheque&accion=Manager&evento=SUBIR&distrito2=<%= objeto.getDistrito()%>&banco2=<%= objeto.getBanco()%>&descripcion2=<%= objeto.getDescripcion()%>&cpi2=<%= objeto.getCPI()%>&linea2=<%= objeto.getLinea()%>&columna2=<%= objeto.getColumna()%>">subir</a>               
              </TD>
         </TR>
<%}%></TABLE> 
    <%}
    %>
    
    <!--  Presentamos los comentario de las transacciones, si los hay  -->    
    <% if(comentario!=null && !comentario.equals("")){%>
       <script>aviso('<%= comentario %>')</script>
    <%}%>

</body>
</html>
