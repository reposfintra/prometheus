<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Buscar Remesas</title>
    <link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>
	<link href="../css/estilo.css" rel="stylesheet" type="text/css">

     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
     <script language=JavaScript1.2 
         src="<%=BASEURL%>/js/coolmenus3.js">
     </script>
</head>
<body>
<br>
<FORM name='formulario' id='formulario' method='POST' onsubmit='jscript: if(formulario.Numrem.value==""){ alert("El campo Numero de la remesa no puede ser vacio..."); return false; }' action='<%= CONTROLLER %>?estado=Remesa&accion=Buscar'>
<table width='400' border='1' align='center' cellpadding='2' cellspacing='1' bordercolor='#CCCCCC' class="letra">
    <tr class='titulo1'>
            <td height='50' colspan='9' align='center' class="titulo">BUSCAR REMESA</td>
    </tr>        
    <tr class='fila'>
        <td height='40'>
            <table width='95%' border='0' align='center' cellpadding='' cellspacing='0' bordercolor='0' class="fila">
                <tr class="fila">
                    <td width='50%'align='center' class='fila'>NUMERO DE LA REMESA</td>
                    <td width='50%'  align='center' class=''><input type='text' name='Numrem' style='width:100%;'></td>        
                </tr>
          </table>        
        </td>
    </tr>
    <tr class="pie">
        <td height='50' align='center' class="pie"><input type='submit' value='Buscar' style='width:60%;'>
      </center></td>
    </tr>
</table>
</FORM>
</body>
<%
if((request.getParameter("Mensaje")!=null) && (!request.getParameter("Mensaje").equals("")))
{
%>
<script language="javascript"> 
alert(<%=request.getParameter("Mensaje")%>);
</script>
<%
}
%>
</html>
