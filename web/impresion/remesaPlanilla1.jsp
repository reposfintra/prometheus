<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Imprimir Remesas</title>
<link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>
	<link href="../css/estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar_2.js"></script>

<% Usuario usuario = (Usuario)session.getAttribute("Usuario"); %>
</head>
<body class="letra" onLoad="CheckRemesas();Impresion();window.close();"">
<div align="center">
  <input type="hidden" name="Registros" value="">
  <%
List     Lista;
Iterator It;
%>
  <br>
  <%
if(model.RemesaSvc.getRegistros().size()>0)
{
%>
  <input type='checkbox' Id='Verificacion'>
  <br>
</div>
<table width='990' border='1' align='center' cellpadding='2' cellspacing='1' bordercolor='#CCCCCC' class="letra">
    <tr class='titulo1'>
            <td height='50' colspan='9' align='center' class="titulo">IMPRIMIR REMESAS</td>
    </tr>        
    <tr class='subtitulos'>
        <td width='1%'  align='center' class='comentario'>&nbsp;</td>
        <td width='6%'  align='center' class='comentario'>REMESA</td>
        <td width='15%' align='center' class='comentario'>FECHA</td>
        <td width='17%' align='center' class='comentario'>CLIENTE</td>
        <td width='17%' align='center' class='comentario'>CIUDAD DE ORIGEN</td>
        <td width='17%' align='center' class='comentario'>CIUDAD DE DESTINO</td>
        <td width='6%'  align='center' class='comentario'>PLANILLA</td>
        <td width='7%'  align='center' class='comentario'>PLACA</td>
        <td width='14%' align='center' class='comentario'>CONDUCTOR</td>        
  </tr>
    <%
    Lista = model.RemesaSvc.getRegistros();
    It = Lista.iterator();
    int i=0;
    String Remesa = "?";
    while(It.hasNext()) {
        Remesa datos = (Remesa) It.next();
        if(!Remesa.equals(datos.getNumrem())) {
            %>
            <form name='formulario_<%=i%>' id='formulario_<%=i%>' method='POST'>
          <%        
            out.print("<tr class='fila'>\n");
            out.print("<td class='fila' align='center'><input type='checkbox' name='Impresa' value="+datos.getNumrem()+"></td>\n");
            out.print("<td class='fila' align='center'>"+datos.getNumrem()+"</td>\n");
            out.print("<td class='fila'>"+datos.getFecRem()+"</td>\n");
            out.print("<td class='fila'>"+datos.getCliente()+"</td>\n");
            out.print("<td class='fila'>"+datos.getOriRem()+"</td>\n");
            out.print("<td class='fila'>"+datos.getDesRem()+"</td>\n");
            out.print("<td class='fila' align='center'>"+datos.getOc()+"</td>\n");
            out.print("<td class='fila' align='center'>"+datos.getPlaca()+"</td>\n");
            out.print("<td class='fila'>"+datos.getConductor()+"</td>\n");
            out.print("</tr>\n");
            i++;
            %> 
            <input type='hidden' name='Usuario'      value='<%= usuario.getNombre()%>'  >
            <input type='hidden' name='Precinto'     value='<%= datos.getDerechosCedido()%>'  >
            <input type='hidden' name='Observacion'  value='<%= (datos.getObservacion()    !=null?datos.getObservacion():"")    %>'>
            <input type='hidden' name='DocInterno'   value='<%= (datos.getDocInterno()     !=null?datos.getDocInterno():"")     %>'>
            <input type='hidden' name='Remitente'    value='<%= (datos.getRemitente()      !=null?datos.getRemitente():"")      %>'>
            <input type='hidden' name='DireccionDes' value='<%= (datos.getDirDestinatario()!=null?datos.getDirDestinatario():"")%>'>
            <input type='hidden' name='Destinatario' value='<%= (datos.getDestinatario()   !=null?datos.getDestinatario():"")   %>'>
            <input type='hidden' name='CiudadDes'    value='<%= (datos.getCiuDestinatario()!=null?datos.getCiuDestinatario():"")%>'>
            <input type='hidden' name='AgenciaOri'   value='<%= (datos.getAgcRem()         !=null?datos.getAgcRem():"")         %>'>
            <input type='hidden' name='FechaImp'     value='<%= (datos.getPrinter_Date()   !=null?datos.getPrinter_Date():"")   %>'>
            <input type='hidden' name='Numrem'       value='<%= (datos.getNumrem()         !=null?datos.getNumrem():"")         %>'>
            <input type='hidden' name='Fecha'        value='<%= datos.getFecRem() %>'>
            <input type='hidden' name='Cliente'      value='<%= (datos.getCliente()        !=null?datos.getCliente():"")        %>'>
            <input type='hidden' name='Origen'       value='<%= (datos.getOriRem()         !=null?datos.getOriRem():"")         %>'>
            <input type='hidden' name='Destino'      value='<%= (datos.getDesRem()         !=null?datos.getDesRem():"")         %>'>
            <input type='hidden' name='Oc'           value='<%= (datos.getOc()             !=null?datos.getOc():"")             %>'>
            <input type='hidden' name='Placa'        value='<%= (datos.getPlaca()          !=null?datos.getPlaca():"")          %>'>
            <input type='hidden' name='Conductor'    value='<%= (datos.getConductor()      !=null?datos.getConductor():"")      %>'>
            <input type='hidden' name='TextoExt'     value='<%= (model.RemesaSvc.getTextoExtendido()!=null?model.RemesaSvc.getTextoExtendido().replaceAll("\n|'","") : "")      %>'>
            </form>
            <%
        }
        Remesa = datos.getNumrem();
    }
    %>    
</table>
<br>
<center>
</center>
<%
}
else{
%>
<br><br><br><br>
<table width='300' border='1' align='center' cellpadding="2" cellspacing="1" bordercolor="#CCCCCC">
    <tr><td align='center' class='titulo' height='30'>INFORMACIÓN</td>
    </tr>
    <tr class="fila"><td class='fondo' height='70' align='center'><font size='2px' face='tahoma' class="fila">No hay Remesas para imprimir...</font></td>
    </tr>
</table>        
<%
}
%>
</body>
</html>
