<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    CIA DatosCia          = model.PlanillaImpresionSvc.getCIA(); 
    List ListaPlanillas   = model.PlanillaImpresionSvc.getPlanillas();
    String CodigoRegional = (DatosCia!=null)?DatosCia.getCodigoRegional():"";
    String CodigoEmpresa  = (DatosCia!=null)?DatosCia.getCodigoEmpresa():"";
    String Resolucion     = (DatosCia!=null)?DatosCia.getResolucion():"";
    String RangoInicial   = (DatosCia!=null)?DatosCia.getRangoInicial():"";
    String RangoFinal     = (DatosCia!=null)?DatosCia.getRangoFinal():"";
    String NombreAseguradora      = (DatosCia!=null)?DatosCia.getNombreAseguradora():"";
    String PolizaAseguradora      = (DatosCia!=null)?DatosCia.getPolizaAseguradora():"";
    String FechaVencimientoPoliza = (DatosCia!=null)?DatosCia.getFechaVencimientoPoliza():"";
    String Separador  = "~";
    String Origen     = (request.getParameter("Origen")!=null && (!request.getParameter("Origen").equals(""))   )?request.getParameter("Origen"):"FListado";
    
    Usuario   usuario = (Usuario) session.getAttribute("Usuario");
%>
<html>
<head>
<title>Impresion de Planillas</title>
<script> var Separador = '<%= Separador %>';</script>


<link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>
	<link href="../css/estilo.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
</head>
<body onLoad="CheckPlanillas();Impresion();window.close();"">

<center>
    <span class="letra">
<input type='hidden'   name='Registros' value=''>
<%  if (ListaPlanillas!=null && ListaPlanillas.size()>0)  {  %>
<input type='checkbox' Id='Verificacion'>
Preguntar antes de Imprimir
<br>
<br>
    </span>
    <table width="800" border="1" cellpadding="2" cellspacing="1" bordercolor="#CCCCCC" class="fondotabla">
      <tr class="titulo1"><th colspan="7" class="titulo">LISTADO DE PLANILLAS 
        <% if(Origen.equals("FListado")) out.print("NO IMPRESAS"); %>
      </th></tr>
      <tr class='titulo3'>
        <th class="subtitulos">&nbsp;</th>
        <th class="subtitulos">Planilla</th>
        <th class="subtitulos">Fecha</th>
        <th class="subtitulos">Conductor</th>
        <th class="subtitulos">Placa</th>
        <th class="subtitulos">Ruta</th>
        <th class="subtitulos">Remesas - Clientes</th>
     </tr>

    <% 
        Iterator it = ListaPlanillas.iterator();
        while(it.hasNext()){
            DatosPlanillaImp datos = (DatosPlanillaImp) it.next();
    %>
      <form action="" method="post" onsubmit="jscript: return false;">
      
      <span class="letra">
      <input type="hidden" name="Usuario"           value="<%= (datos.getDespachador()!=null)?datos.getDespachador():""  %>">
      <input type="hidden" name="CodigoRegional"    value="<%= CodigoRegional         %>">
      <input type="hidden" name="CodigoEmpresa"     value="<%= CodigoEmpresa          %>">
      <input type="hidden" name="Resolucion"        value="<%= Resolucion             %>">
      <input type="hidden" name="RangoInicial"      value="<%= RangoInicial           %>">
      <input type="hidden" name="RangoFinal"        value="<%= RangoFinal             %>">
      <input type="hidden" name="NombreAseguradora" value="<%= NombreAseguradora      %>">
      <input type="hidden" name="PolizaAseguradora" value="<%= PolizaAseguradora      %>">
      <input type="hidden" name="FecVencimientoPol" value="<%= FechaVencimientoPoliza %>">
      
      <input type="hidden" name="NumeroPlanilla"  value="<%= (datos.getNumeroPlanilla()!=null)?datos.getNumeroPlanilla():""  %>">
      <input type="hidden" name="FechaPlanilla"   value="<%= (datos.getFechaPlanilla() !=null)?datos.getFechaPlanilla() :""  %>">
      <input type="hidden" name="CodOrigen"       value="<%= (datos.getCodOrigen()     !=null)?datos.getCodOrigen()     :""  %>">
      <input type="hidden" name="DesOrigen"       value="<%= (datos.getDesOrigen()     !=null)?datos.getDesOrigen()     :""  %>">
      <input type="hidden" name="CodDestino"      value="<%= (datos.getCodDestino()    !=null)?datos.getCodDestino()    :""  %>">
      <input type="hidden" name="DesDestino"      value="<%= (datos.getDesDestino()    !=null)?datos.getDesDestino()    :""  %>">
      <input type="hidden" name="Ruta"            value="<%= (datos.getRuta()          !=null)?datos.getRuta()          :""  %>">
      
      <input type="hidden" name="Placa"                 value="<%= (datos.getPlaca()              !=null)?datos.getPlaca()  :""             %>">
      <input type="hidden" name="Marca"                 value="<%= (datos.getMarca()              !=null)?datos.getMarca()  :""             %>">
      <input type="hidden" name="Modelo"                value="<%= (datos.getModelo()             !=null)?datos.getModelo() :""             %>">
      <input type="hidden" name="Serial"                value="<%= (datos.getSerial()             !=null)?datos.getSerial() :""             %>">
      <input type="hidden" name="Color"                 value="<%= (datos.getColor()              !=null)?datos.getColor()  :""             %>">
      <input type="hidden" name="DimensionCarroceria"   value="<%= (datos.getDimensionCarroceria()!=null)?datos.getDimensionCarroceria():"" %>">
      <input type="hidden" name="NumeroEjes"            value="<%= (datos.getNumeroEjes()         !=null)?datos.getNumeroEjes() :""         %>">
      <input type="hidden" name="PesoVacio"             value="<%= (datos.getPesoVacio()          !=null)?datos.getPesoVacio()  :""         %>">

      <input type="hidden" name="CedulaPropietario"    value="<%= (datos.getCedulaPropietario()   !=null)?datos.getCedulaPropietario()   :""  %>">
      <input type="hidden" name="NombrePropietario"    value="<%= (datos.getNombrePropietario()   !=null)?datos.getNombrePropietario()   :""  %>">
      <input type="hidden" name="DireccionPropietario" value="<%= (datos.getDireccionPropietario()!=null)?datos.getDireccionPropietario():""  %>">
      <input type="hidden" name="TelefonoPropietario"  value="<%= (datos.getTelefonoPropietario() !=null)?datos.getTelefonoPropietario() :""  %>">
      <input type="hidden" name="CiudadPropietario"    value="<%= (datos.getCiudadPropietario()   !=null)?datos.getCiudadPropietario()   :""  %>">

      <input type="hidden" name="CedulaTenedor"    value="<%= (datos.getCedulaTenedor()   !=null)?datos.getCedulaTenedor()   :""  %>">
      <input type="hidden" name="NombreTenedor"    value="<%= (datos.getNombreTenedor()   !=null)?datos.getNombreTenedor()   :""  %>">
      <input type="hidden" name="DireccionTenedor" value="<%= (datos.getDireccionTenedor()!=null)?datos.getDireccionTenedor():""  %>">
      <input type="hidden" name="TelefonoTenedor"  value="<%= (datos.getTelefonoTenedor() !=null)?datos.getTelefonoTenedor() :""  %>">
      <input type="hidden" name="CiudadTenedor"    value="<%= (datos.getCiudadTenedor()   !=null)?datos.getCiudadTenedor()   :""  %>">
      
      <input type="hidden" name="CedulaConductor"    value="<%= (datos.getCedulaConductor()   !=null)?datos.getCedulaConductor()   :""  %>">
      <input type="hidden" name="NombreConductor"    value="<%= (datos.getNombreConductor()   !=null)?datos.getNombreConductor()   :""  %>">
      <input type="hidden" name="DireccionConductor" value="<%= (datos.getDireccionConductor()!=null)?datos.getDireccionConductor():""  %>">
      <input type="hidden" name="TelefonoConductor"  value="<%= (datos.getTelefonoConductor() !=null)?datos.getTelefonoConductor() :""  %>">
      <input type="hidden" name="CiudadConductor"    value="<%= (datos.getCiudadConductor()   !=null)?datos.getCiudadConductor()   :""  %>">
      
      <input type="hidden" name="MensajeImpresion" value="<%= (datos.getFechaImpresion().equals("0099-01-01 00:00:00"))?"Original Impreso":"Re-Impresion"  %>">
      <input type="hidden" name="ValorAnticipo"    value="<%= datos.getValorAnticipo ()  %>">
      <input type="hidden" name="ValorAnticipo2"   value="<%= datos.getValorAnticipo2()  %>">
      <input type="hidden" name="Moneda"           value="<%= (datos.getMoneda() !=null)?datos.getMoneda()         :""  %>">
      
      </span>
      <tr onclick="jscript: Impresa.checked = !Impresa.checked; " class='fila'>
        <td valign='center' class="letra">
          <input type="checkbox" name="Impresa" onclick="jscript: Impresa.checked = !Impresa.checked; " value='<%= (datos.getNumeroPlanilla()  !=null)?datos.getNumeroPlanilla()  :"&nbsp"  %>'>
        </td>
        <td class="letra" ><%= (datos.getNumeroPlanilla()  !=null)?datos.getNumeroPlanilla()  :"&nbsp"  %></td>
        <td class="letra" ><%= (datos.getFechaPlanilla()   !=null)?datos.getFechaPlanilla()   :"&nbsp"  %></td>
        <td class="letra" ><%= (datos.getNombreConductor() !=null)?datos.getNombreConductor() :"&nbsp"  %></td>
        <td class="letra" ><%= (datos.getPlaca() !=null&&  !datos.getPlaca().equals(""))?     datos.getPlaca() :"&nbsp"  %></td>
        <td class="letra" ><%= (datos.getRuta()            !=null)?datos.getRuta()            :"&nbsp"  %></td>
        <td class="letra" >
          <span class="letra">
          <%
            List Remesas = datos.getRemesas();
            int CantidadRemesas = 0;
            if (Remesas!= null && Remesas.size()>0){
                String TextoOC = ((DatosRemesaImp) Remesas.get(0)).getTextoOC();
                out.print("<input type='hidden' name='TextoOC' value='"+  (TextoOC!=null?TextoOC:"") +"'/>");
                Iterator it2 = Remesas.iterator();
                out.println("\t<table width='100%' class='fondotabla'>");
                while (it2.hasNext()){
                    DatosRemesaImp datosR = (DatosRemesaImp) it2.next();
                    CantidadRemesas++;
                    String NumeroRemesa   = (datosR.getNumeroRemesa()    !=null)?datosR.getNumeroRemesa()    :"";
                    String DocInterno     = (datosR.getDocumentoInterno()!=null)?datosR.getDocumentoInterno():"";
                    String Cliente        = (datosR.getCliente()         !=null)?datosR.getCliente()         :"";
                    out.println("\t<tr class='fila'><td valign='top'>"+ NumeroRemesa +" - "+ Cliente +"</td></tr>");
                    out.println("\t\t<input type='hidden' name='Remesas' value='" + NumeroRemesa + Separador + DocInterno + Separador + Cliente + "'>");
                    List Descuentos    = datosR.getDescuentos();
                    List Remitentes    = datosR.getRemitentes();
                    List Destinatarios = datosR.getDestinatarios();
                    // informacion de los remitentes
                    int CantidadRemitentes = 0;
                    if (Remitentes!=null && Remitentes.size()>0){
                        Iterator itR = Remitentes.iterator();
                        while(itR.hasNext()){
                            CantidadRemitentes++;
                            DatosRemDesImp datosRe = (DatosRemDesImp)itR.next();
                            String Nombre = (datosRe.getNombre()    !=null)?datosRe.getNombre()    :"";
                            String Ciudad = (datosRe.getNomCiudad() !=null)?datosRe.getNomCiudad() :"";
                            out.println("\t\t<input type='hidden' name='Remitentes' value='"+ NumeroRemesa + Separador + Nombre + Separador + Ciudad +"'>");
                        }
                    }
                    out.println("\t\t<input type='hidden' name='CantidadRemitentes' value='" + NumeroRemesa + Separador + CantidadRemitentes + "'>");
                    // informacion de los destinatarios
                    int CantidadDestinatarios = 0;
                    if (Destinatarios!=null && Destinatarios.size()>0){
                        Iterator itD = Destinatarios.iterator();
                        while(itD.hasNext()){
                            CantidadDestinatarios++;
                            DatosRemDesImp datosDe = (DatosRemDesImp)itD.next();
                            String Nombre = (datosDe.getNombre()    !=null)?datosDe.getNombre()    :"";
                            String Ciudad = (datosDe.getNomCiudad() !=null)?datosDe.getNomCiudad() :"";
                            out.println("\t\t<input type='hidden' name='Destinatarios' value='"+ NumeroRemesa + Separador + Nombre + Separador + Ciudad +"'>");
                        }
                    }
                    out.println("\t\t<input type='hidden' name='CantidadDestinatarios' value='" + NumeroRemesa + Separador + CantidadDestinatarios + "'>");
                    // informacion de los descuentos
                    int CantidadDescuentos = 0;
                    if (Descuentos!=null && Descuentos.size()>0){
                        Iterator itDesc = Descuentos.iterator();
                        while(itDesc.hasNext()){
                            CantidadDescuentos++;
                            DatosDescuentosImp datosDesc = (DatosDescuentosImp)itDesc.next();
                            String Codigo   = (datosDesc.getCodigo()   !=null)?datosDesc.getCodigo()  :"";
                            String Concepto = (datosDesc.getConcepto() !=null)?datosDesc.getConcepto():"";
                            String Valor    = (datosDesc.getValor()    !=null)?datosDesc.getValor()   :"00.00";
                            String Moneda   = (datosDesc.getMoneda()   !=null)?datosDesc.getMoneda()  :"";
                            String Tipo     = (datosDesc.getTipo()     !=null)?datosDesc.getTipo()    :"";
                            out.println("\t\t<input type='hidden' name='Descuentos' value='"+ NumeroRemesa + Separador + Codigo + Separador + Concepto + Separador + Valor + Separador + Moneda + Separador + Tipo + "'>");
                        }
                    }
                    out.println("\t\t<input type='hidden' name='CantidadDescuentos' value='" + NumeroRemesa + Separador + CantidadDescuentos + "'>");
                }//end while
                out.println("\t</table>");
            }else{
                out.println("\tNo hay remesas");
                out.print("<input type='hidden' name='TextoOC' value=''/>");
            }
            out.print("\t\t<input type='hidden' name='CantidadRemesas' value='"+CantidadRemesas+"'>");
    %>
          </span></td>
      </tr>
      <span class="letra">
      <input type='hidden' name='Origen' value='<%= Origen %>'>
      </span>
      </form>
    <% } %>
    </table>
    <span class="letra"><br>
    </span>
    <form action="<%=CONTROLLER%>?estado=Menu&accion=BuscarPlanillaImpresion" method='post' name='regreso'>
    <span class="letra">
    <% if (Origen.equals("FBuscar")) {  %>
      <input type="submit" name="Opcion" value="Regresar" style="width:120">
    <%  }
    }else { %>
    </span>
    <table width='250' border="1" cellpadding="2" cellspacing="1" bordercolor="#CCCCCC" class="letra">
    <tr><th class="titulo">ATENCION</th></tr>
    <tr><td height='80' ALIGN="center" class="pie">No hay planillas por Imprimir</td></tr>
    </table>
    <% } %>
  </form>
</center>
</body>
</html>
