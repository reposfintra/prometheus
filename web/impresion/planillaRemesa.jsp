<!--
- Autor : Ing. Jose de la rosa
- Date  : 22 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite la impresion de las planillas
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    CIA DatosCia          = model.PlanillaImpresionSvc.getCIA(); 
    List ListaPlanillas   = model.PlanillaImpresionSvc.getPlanillas();
    String CodigoRegional = (DatosCia!=null)?DatosCia.getCodigoRegional():"";
    String CodigoEmpresa  = (DatosCia!=null)?DatosCia.getCodigoEmpresa():"";
    String Resolucion     = (DatosCia!=null)?DatosCia.getResolucion():"";
    String RangoInicial   = (DatosCia!=null)?DatosCia.getRangoInicial():"";
    String RangoFinal     = (DatosCia!=null)?DatosCia.getRangoFinal():"";
    String NombreAseguradora      = (DatosCia!=null)?DatosCia.getNombreAseguradora():"";
    String PolizaAseguradora      = (DatosCia!=null)?DatosCia.getPolizaAseguradora():"";
    String FechaVencimientoPoliza = (DatosCia!=null)?DatosCia.getFechaVencimientoPoliza():"";
    String Separador  = "~";
    String Origen     = (request.getParameter("Origen")!=null && (!request.getParameter("Origen").equals(""))   )?request.getParameter("Origen"):"FListado";
    
    Usuario   usuario = (Usuario) session.getAttribute("Usuario");
%>
<html>
<head>
<title>Impresion de Planillas</title>
<script> var Separador = '<%= Separador %>';</script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
</head>
<body onLoad="CheckPlanillas();Impresion();window.close();">

<center>
    <span class="letra">
<input type='hidden'   name='Registros' value=''>
<%  if (ListaPlanillas!=null && ListaPlanillas.size()>0)  {  %>
<input type='checkbox' Id='Verificacion'>
Preguntar antes de Imprimir
<br>
<br>
    </span>
    <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
      <tr class="titulo1"><th colspan="7" class="titulo">
	  	  <table width="100%" align="center">
              <tr>
                <td width="50%" class="subtitulo1">&nbsp;Listado de Planillas <% if(Origen.equals("FListado")) out.print("NO IMPRESAS"); %></td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
      </th></tr>
      <tr class="tblTitulo" align="center">
        <th class="subtitulos">&nbsp;</th>
        <th class="subtitulos">Planilla</th>
        <th class="subtitulos">Fecha</th>
        <th class="subtitulos">Conductor</th>
        <th class="subtitulos">Placa</th>
        <th class="subtitulos">Ruta</th>
        <th class="subtitulos">Remesas - Clientes</th>
     </tr>

	<% 	int i=0;
	        Iterator it = ListaPlanillas.iterator();
		while(it.hasNext()){
		    DatosPlanillaImp datos = (DatosPlanillaImp) it.next();
		    String estilo = (i % 2 == 0 )?"filagris":"filaazul";
                    if (datos.getEstado().equals("P")) estilo = "filaresaltada";
        %>
      <form action="" method="post" onsubmit="jscript: return false;">
      
			  <span class="letra">
			  <input type="hidden" name="Usuario"           value="<%= (datos.getDespachador()!=null)?datos.getDespachador():""  %>">
			  <input type="hidden" name="CodigoRegional"    value="<%= CodigoRegional         %>">
			  <input type="hidden" name="CodigoEmpresa"     value="<%= CodigoEmpresa          %>">
			  <input type="hidden" name="Resolucion"        value="<%= Resolucion             %>">
			  <input type="hidden" name="RangoInicial"      value="<%= RangoInicial           %>">
			  <input type="hidden" name="RangoFinal"        value="<%= RangoFinal             %>">
			  <input type="hidden" name="NombreAseguradora" value="<%= NombreAseguradora      %>">
			  <input type="hidden" name="PolizaAseguradora" value="<%= PolizaAseguradora      %>">
			  <input type="hidden" name="FecVencimientoPol" value="<%= FechaVencimientoPoliza %>">
			  
			  <input type="hidden" name="NumeroPlanilla"  value="<%= (datos.getNumeroPlanilla()!=null)?datos.getNumeroPlanilla():""  %>">
			  <input type="hidden" name="FechaPlanilla"   value="<%= (datos.getFechaPlanilla() !=null)?datos.getFechaPlanilla() :""  %>">
			  <input type="hidden" name="CodOrigen"       value="<%= (datos.getCodOrigen()     !=null)?datos.getCodOrigen()     :""  %>">
			  <input type="hidden" name="DesOrigen"       value="<%= (datos.getDesOrigen()     !=null)?datos.getDesOrigen()     :""  %>">
			  <input type="hidden" name="CodDestino"      value="<%= (datos.getCodDestino()    !=null)?datos.getCodDestino()    :""  %>">
			  <input type="hidden" name="DesDestino"      value="<%= (datos.getDesDestino()    !=null)?datos.getDesDestino()    :""  %>">
			  <input type="hidden" name="Ruta"            value="<%= (datos.getRuta()          !=null)?datos.getRuta()          :""  %>">
			  <input type="hidden" name="FechaCargue"     value="<%= (datos.getFechaCargue()   !=null)?datos.getFechaCargue()   :""  %>">
			  <input type="hidden" name="Soportes"        value="<%= (datos.getSoportes()      !=null)?datos.getSoportes().values().toString().replaceAll("\\[|\\]","")   :""  %>">
			  <input type="hidden" name="Placa"                 value="<%= (datos.getPlaca()              !=null)?datos.getPlaca()  :""             %>">
			  <input type="hidden" name="Marca"                 value="<%= (datos.getMarca()              !=null)?datos.getMarca()  :""             %>">
			  <input type="hidden" name="Modelo"                value="<%= (datos.getModelo()             !=null)?datos.getModelo() :""             %>">
			  <input type="hidden" name="Serial"                value="<%= (datos.getSerial()             !=null)?datos.getSerial() :""             %>">
			  <input type="hidden" name="Color"                 value="<%= (datos.getColor()              !=null)?datos.getColor()  :""             %>">
			  <input type="hidden" name="DimensionCarroceria"   value="<%= (datos.getDimensionCarroceria()!=null)?datos.getDimensionCarroceria():"" %>">
			  <input type="hidden" name="NumeroEjes"            value="<%= (datos.getNumeroEjes()         !=null)?datos.getNumeroEjes() :""         %>">
			  <input type="hidden" name="PesoVacio"             value="<%= (datos.getPesoVacio()          !=null)?datos.getPesoVacio()  :""         %>">
			  <input type="hidden" name="PlacaSemiRemolque"     value="<%= (datos.getPlacaTrailer()       !=null)?datos.getPlacaTrailer():""        %>">
			  <input type="hidden" name="Contenedores"          value="<%= (datos.getContenedores()       !=null)?datos.getContenedores():""        %>">
			  <input type="hidden" name="Observaciones"         value="<%= (datos.getObservaciones()      !=null)?datos.getObservaciones():""       %>">
			  
			  <input type="hidden" name="RegistroNacionalCarga" value="<%= (datos.getRegistroNacionalCarga() !=null)?datos.getRegistroNacionalCarga() :""  %>">
			  <input type="hidden" name="NumeroPolizaSOAT"      value="<%= (datos.getNumeroPolizaSoat()      !=null)?datos.getNumeroPolizaSoat()      :""  %>">
			  <input type="hidden" name="CiaSegurosSOAT"        value="<%= (datos.getCompaniaSoat()          !=null)?datos.getCompaniaSoat()          :""  %>">
			  <input type="hidden" name="VencimientoSOAT"       value="<%= (datos.getVecimientoSoat()        !=null)?datos.getVecimientoSoat()        :""  %>">
			  
		
			  <input type="hidden" name="CedulaPropietario"    value="<%= (datos.getCedulaPropietario()   !=null)?datos.getCedulaPropietario()   :""  %>">
			  <input type="hidden" name="NombrePropietario"    value="<%= (datos.getNombrePropietario()   !=null)?datos.getNombrePropietario()   :""  %>">
			  <input type="hidden" name="DireccionPropietario" value="<%= (datos.getDireccionPropietario()!=null)?datos.getDireccionPropietario():""  %>">
			  <input type="hidden" name="TelefonoPropietario"  value="<%= (datos.getTelefonoPropietario() !=null)?datos.getTelefonoPropietario() :""  %>">
			  <input type="hidden" name="CiudadPropietario"    value="<%= (datos.getCiudadPropietario()   !=null)?datos.getCiudadPropietario()   :""  %>">
		
			  <input type="hidden" name="CedulaTenedor"    value="<%= (datos.getCedulaTenedor()   !=null)?datos.getCedulaTenedor()   :""  %>">
			  <input type="hidden" name="NombreTenedor"    value="<%= (datos.getNombreTenedor()   !=null)?datos.getNombreTenedor()   :""  %>">
			  <input type="hidden" name="DireccionTenedor" value="<%= (datos.getDireccionTenedor()!=null)?datos.getDireccionTenedor():""  %>">
			  <input type="hidden" name="TelefonoTenedor"  value="<%= (datos.getTelefonoTenedor() !=null)?datos.getTelefonoTenedor() :""  %>">
			  <input type="hidden" name="CiudadTenedor"    value="<%= (datos.getCiudadTenedor()   !=null)?datos.getCiudadTenedor()   :""  %>">
			  
			  <input type="hidden" name="CedulaConductor"    value="<%= (datos.getCedulaConductor()   !=null)?datos.getCedulaConductor()   :""  %>">
			  <input type="hidden" name="NombreConductor"    value="<%= (datos.getNombreConductor()   !=null)?datos.getNombreConductor()   :""  %>">
			  <input type="hidden" name="DireccionConductor" value="<%= (datos.getDireccionConductor()!=null)?datos.getDireccionConductor():""  %>">
			  <input type="hidden" name="TelefonoConductor"  value="<%= (datos.getTelefonoConductor() !=null)?datos.getTelefonoConductor() :""  %>">
			  <input type="hidden" name="CiudadConductor"    value="<%= (datos.getCiudadConductor()   !=null)?datos.getCiudadConductor()   :""  %>">
			  <input type="hidden" name="CategoriaPase"      value="<%= (datos.getCategoriaPase()     !=null)?datos.getCategoriaPase()     :""  %>">
			  
			  <input type="hidden" name="MensajeImpresion" value="<%= (datos.getFechaImpresion().equals("0099-01-01 00:00:00"))?"Original Impreso":"Re-Impresion"  %>">
			  <input type="hidden" name="ValorAnticipo"    value="<%= datos.getValorAnticipo ()  %>">
			  <input type="hidden" name="ValorAnticipo2"   value="<%= datos.getValorAnticipo2()  %>">
			  <input type="hidden" name="Moneda"           value="<%= (datos.getMoneda() !=null)?datos.getMoneda()         :""  %>">
			  
			  </span>
			  <tr onclick="jscript: if (!Impresa.disabled) { Impresa.checked = !Impresa.checked; this.className=(Impresa.checked?'filaseleccion':'<%= estilo %>'); this.cells[6].firstChild.className=(Impresa.checked?'filaseleccion':'<%= estilo %>'); } else warningPlanilla(); " class="<%= estilo %>" style="cursor:hand" >
				<td valign='center' class="bordereporte" style='color '>
				  <input type="checkbox" name="Impresa" onclick="jscript: Impresa.checked = !Impresa.checked;  <% if (datos.getEstado().equals("P")) { %> warningPlanilla(); <% } %>" value='<%= (datos.getNumeroPlanilla()  !=null)?datos.getNumeroPlanilla()  :"&nbsp"  %>'  <% if (datos.getEstado().equals("P")) { out.print("disabled"); }  %> >
				</td>
				<td class="bordereporte" ><%= (datos.getNumeroPlanilla()  !=null)?datos.getNumeroPlanilla()  :"&nbsp"  %></td>
				<td class="bordereporte" ><%= (datos.getFechaPlanilla()   !=null)?datos.getFechaPlanilla()   :"&nbsp"  %></td>
				<td class="bordereporte" ><%= (datos.getNombreConductor() !=null)?datos.getNombreConductor() :"&nbsp"  %></td>
				<td class="bordereporte" ><%= (datos.getPlaca() !=null&&  !datos.getPlaca().equals(""))?     datos.getPlaca() :"&nbsp"  %></td>
				<td class="bordereporte" ><%= (datos.getRuta()            !=null)?datos.getRuta()            :"&nbsp"  %></td>
				<td class="bordereporte" >
				  <% i++;
					List Remesas = datos.getRemesas();
					int CantidadRemesas = 0;
					if (Remesas!= null && Remesas.size()>0){
						Iterator it2 = Remesas.iterator();
						out.println("\t<table width='100%' border='0' align='center' class="+ estilo +">");
						while (it2.hasNext()){
							DatosRemesaImp datosR = (DatosRemesaImp) it2.next();
							CantidadRemesas++;
							String NumeroRemesa   = (datosR.getNumeroRemesa()    !=null)?datosR.getNumeroRemesa()    :"";
							String DocInterno     = (datosR.getDocumentoInterno()!=null)?datosR.getDocumentoInterno():"";
							String Cliente        = (datosR.getCliente()         !=null)?datosR.getCliente()         :"";
							out.println("\t<tr ><td valign='top' >"+ NumeroRemesa +" - "+ Cliente +"</td></tr>");
							out.println("\t\t<input type='hidden' name='Remesas' value='" + NumeroRemesa + Separador + DocInterno + Separador + Cliente + "'>");
						}//end while
						out.println("\t</table>");
                                                String TextoOC = ((DatosRemesaImp) Remesas.get(0)).getTextoOC();
						out.print("<input type='hidden' name='TextoOC' value='"+  (TextoOC!=null?TextoOC:"") +"'/>");
					}else{
						out.println("\tNo hay remesas");
						out.print("<input type='hidden' name='TextoOC' value=''/>");
					}
					
					
					// informacion de los descuentos
					List Descuentos    = datos.getDescuentos();
					if (Descuentos!=null && Descuentos.size()>0){
						Iterator itDesc = Descuentos.iterator();
						while(itDesc.hasNext()){
							DatosDescuentosImp datosDesc = (DatosDescuentosImp)itDesc.next();
							String Codigo   = (datosDesc.getCodigo()   !=null)?datosDesc.getCodigo()  :"";
							String Concepto = (datosDesc.getConcepto() !=null)?datosDesc.getConcepto():"";
							String Valor    = (datosDesc.getValor()    !=null)?datosDesc.getValor()   :"00.00";
							String Moneda   = (datosDesc.getMoneda()   !=null)?datosDesc.getMoneda()  :"";
							String Tipo     = (datosDesc.getTipo()     !=null)?datosDesc.getTipo()    :"";
							out.println("\t\t<input type='hidden' name='Descuentos' value='"+ Codigo + Separador + Concepto + Separador + Valor + Separador + Moneda + Separador + Tipo + "'>");
						}
					}
			%>
				  </td>
			  </tr>
			  <input type='hidden' name='Origen' value='<%= Origen %>'>
			  </span>
      </form>
    <% } %>
    </table><br>
    <form action="<%=CONTROLLER%>?estado=Menu&accion=BuscarPlanillaImpresion" method='post' name='regreso'>
    <span class="letra">
    <% if (Origen.equals("FBuscar")) {  %>
      <input type="hidden" name="Opcion" value="Regresar" style="width:120">
	  <input type="image" src="<%=BASEURL%>/images/botones/regresar.gif" title="Buscar Remesas" style="cursor:hand" name="Aceptar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	  <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
    <%  }
    }else { %>
    </span>
			<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes">No hay planillas por Imprimir</td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
    <% } %>
  </form>
</center>
</body>
</html>
