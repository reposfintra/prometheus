<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
%>
<html>
<head><title>Impresion de Planillas</title></head>
<link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>
	<link href="../css/estilo.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
<body>
    
<center>
<form action="<%=CONTROLLER%>?estado=PlanillaImpresion&accion=Opciones" method="post"  onSubmit="jscript: return validar_form_busqueda(this);">
<P>
<TABLE width='300' border='1' cellpadding="2" cellspacing="1" bordercolor="#CCCCCC" class="letra">
    <tr>
        <th colspan="2" class="titulo">IMPRESION DE PLANILLAS</th>
    </tr>
    <tr >
    <td class="comentario"><div align="center" class="fila"><br>
          <CENTER>
          Numero de Planilla. &nbsp;&nbsp;&nbsp;
          <input type='text'   style="width:120"   name='NumeroPlanilla'>
          <br>
          <br>
          <br>
        &nbsp;
    </div></td>
    </tr>
    <tr class="pie">
      <td class="pie"><div align="center">
        <input name="Opcion" type='submit' style="width:120" value='Buscar' >
      </div></td>
    </tr>
</TABLE>
</P><BR>
<input type='hidden' name='Origen' value='FBuscar'>
</form>
<br>
<font size='4px'>
<%= Mensaje %>
</font>
</center>
</body>
</html>
