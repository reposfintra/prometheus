<!--
- Autor : Ing. Sandra Escalante
- Modificado: Ing Ivan Dario Gomez
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista para mostrar la informacion de las ayudas
--%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY>
<% String BASEIMG = BASEURL +"/images/ayuda/procesos/placa/"; %> 
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="1" >      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="20"><div align="center">MANUAL PARA LA CONCILIACION DE PLACAS </div></td>
        </tr>
        <tr class="subtitulo1">
          <td>Descripci&oacute;n del funcionamiento del programa para la conciliacion de placas </td>
        </tr>
        <tr>
          <td  height="18"  class="ayudaHtmlTexto">En el programa para la conciliacion de placas, existe un boton examinar que permite buscar el archivo a procesar, como se muestra en la figura 1. despues de buscar el archivo debera dar click en el boton Aceptar para subir el archivo, el archivo a subir debe ser csv para que el programa de lectura lo leea sin ninguna dificultad.. </td>
        </tr>
        <tr>
          <td height="18"  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>figura1.JPG" width="551" height="182"> </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">Luego de subir el archivo el programa le mostrara la sigiente pantalla, pidiendole como parametro la fecha de corte en la cual se encuentran las placas.</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">Para procesar el archivo debera dar click en el boton Aceptar.</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>figura2.JPG" width="753" height="215"> </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">Luego le mostrara una pantalla de informacion, en donde se describen los resultados de la conciliacion, tales, como se muestran el la figura 3.</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="left">Si no se encuentran placas en planilla, se mostraran un listado de las placas no encontradas como se muestra en la figura 4. </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>figura3.JPG" width="678" height="231"></div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="left">Si hay placas que no se encontraron en planilla el program a enviara un correo a los usuarios correspondientes. </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><table cellspacing="0" cellpadding="0">
              <tr>
                <td width="943" class="ayudaHtmlTexto">&nbsp;</td>
              </tr>
          </table>
            <div align="center"><img src="<%= BASEIMG%>figura4.JPG" width="552" height="373"></div></td>
        </tr>
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
