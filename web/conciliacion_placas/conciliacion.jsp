<!--
- Autor : Ing. Sandra Escalante
- Modificado: Ing Ivan Dario Gomez
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista para la subida del archivo a procesar
--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>.: Conciliacion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language='JavaScript' src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src="js/validar.js"></script>
<script>
function validarArchivo(){
	var cadena = forma.arch.value;
	forma.extencion.value = cadena.substring(cadena.length-3, cadena.length);
	if (forma.arch.value == ""){
		alert("No se puede procesar la información. Seleccione un archivo.");
		forma.arch.focus();
		return (false);
	}
	/*else {
		if (forma.extencion.value != "csv"){
			alert ("Debe seleccionar un archivo .csv");
			forma.arch.focus();
			return (false);
		}
		return (true);
	}*/
	return (true);
}

function pasarArchivo (){
	forma.file.value = forma.arch.value;
}
function validarFecha(){
	if (form1.fecha.value == ""){
		alert("No se puede procesar la información. Seleccione una fecha.");
		form1.fecha.focus();
		return (false);
	}
	return (true);
}
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Conciliacion"/>
</div>	
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<%String file="";%>
<table width="530" border="2" align="center">
  <tr>
    <td>	<table width="530" align="center" class="tablaInferior">
  <tr>
    <td colspan="2" nowrap>
    
        <table width="100%"  border="0" class="tablaInferior">
          <tr class="barratitulo">
            <td width="50%" class="subtitulo1">CONCILIACION </td>
            <td width="50%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
        </td>
  </tr>
    <%if (request.getParameter("sw").equals("1")){%>
	<tr class="letra informacion">
      <td colspan="2" nowrap>**El archivo <strong><%= request.getParameter("filename")%></strong> archivo ha sido cargado con &egrave;xito, elija una Fecha de Corte para realizar la sincronizaci&oacute;n</td>
    </tr>
	<%}else{%>
	<form name="forma" id="forma" method="post" enctype="MULTIPART/FORM-DATA" action="<%=CONTROLLER%>?estado=Conciliacion&accion=UploadArchivo" onSubmit="return validarArchivo();">
	<tr class="fila">
      <td width="188" nowrap >Archivo (*.csv):</td>
      <td nowrap valign="middle">
	  	<input name="arch" type="file" class="textbox" id="archivo" onBlur="pasarArchivo();">
	    <input name="subir" type="image" style="cursor:hand" title="Haga clic aqui para procesar el archivo para la conciliacion" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" src="<%=BASEURL%>/images/botones/aceptar.gif" align="middle">
      	<input type="hidden" name="extencion" value="">
      	<input type="hidden" name="file" value="<%=file%>"></td>
    </tr>
	</form>
	<%}%>
	<%if (request.getParameter("sw").equals("1")){
		String  hoy       = Utility.getHoy("-"); %>
	<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Conciliacion&accion=Leer&carpeta=conciliacion_placas&pagina=conciliacion3.jsp&filename=<%=request.getParameter("filename")%>&ruta=<%=request.getParameter("ruta")%>" onSubmit="return validarFecha();">
	 <tr  class="fila">
      <td width="188" nowrap>Fecha de Corte:</td>
      <td width="536" nowrap>
	  	<input  name='fecha' readonly="true" class="textbox" value='<%=hoy%>'> 
        <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fecha);return false;" title="Haga clic aqui para seleccionar una Fecha de Corte" hidefocus>
	        <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a>
			<input name="conciliar" type="image" style="cursor:hand" title="Haga clic aqui para realizar el procesao de conciliacion" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" src="<%=BASEURL%>/images/botones/aceptar.gif" align="middle">
	  </td>
    </tr>
	</form>
	<%}%>
    <tr class="letra informacion">
      <td colspan="2" nowrap >&nbsp;
	  <%if (request.getParameter("sw").equals("0")){%>
	    Seleccione el archivo *.csv y haga clic en ACEPTAR
	  <%}
	  else if (request.getParameter("sw").equals("1")){%>
	    Debe seleccionar una fecha y luego haga clic en ACEPTAR 
	  <%}%>
	  </td>
    </tr>
</table>	

	</td>
  </tr>
</table>
<br>
<center>
<img src="<%=BASEURL%>/images/botones/regresar.gif" name="c"  height="21" title="Regresar..." onClick="alert ('Si desea regresar a la pagina anterior...RECUERDE cargar de nuevo el Archivo');window.location='<%=BASEURL%>/conciliacion_placas/conciliacion.jsp?sw=0'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
<img src="<%=BASEURL%>/images/botones/salir.gif" name="s"  height="21" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
</center>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
</body>
</html>
