<%@page contentType="text/html"%>
<%@ page language="java" import="javax.servlet.http.HttpServletRequest,javax.servlet.ServletInputStream,java.util.*,java.util.Hashtable,java.io.*"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%String archivo="";
String ruta = "";
try {
	String newLine = null;
	String savePath = "C:/exportar/masivo/conciliacion_placas/";
	String filepath = null;
	String filename = null;
	String contentType = null;
	String s = null;
	int xi;
  	int pos;
	Dictionary fields = new Hashtable();
  	fields.put("JMAN", "tbemp.jsp"); 

  	ServletInputStream in = request.getInputStream();

	byte[] line = new byte[128];
  	byte[] line2 = new byte[128];
  	int i = in.readLine(line, 0, 128);

  	if (i > 2) {
    	int boundaryLength = i - 2;
     	String boundary = new String(line, 0, boundaryLength); //-2 discards the newline character

     	while (i != -1) {
        	newLine = new String(line, 0, i);

        	if (newLine.startsWith("Content-Disposition: form-data; name=\"")) {
           		if (newLine.indexOf("filename=\"") != -1) {
               		s = new String(line, 0, i-2);
		            if (s!=null) {
                 		pos = s.indexOf("filename=\"");
                 		if (pos != -1) {
                    		filepath = s.substring(pos+10, s.length()-1);  
          		          	// Windows browsers include the full path on the client
                		    // But Linux/Unix and Mac browsers only send the filename
                    		// test if this is from a Windows browser
                    		pos = filepath.lastIndexOf("\\");
                    		if (pos != -1) {
                       			filename = filepath.substring(pos + 1);
                    		} else {
                       			filename = filepath;
                    		}
                 		}  
              		}                 
         			// AQUI NO EXISTE RETURN, SI EL ARCHIVO ESTA EN BLANCO DEBE GENERAR ERROR
              		// Y/O REGRESAR
           			System.out.println("filename:" + filename);
              		if (filename!=null) {
                 		//this is the file content
                 		i = in.readLine(line, 0, 128);
        
                 		//setContentType
                 		s = new String(line, 0, i-2);
                 		if (s!=null) {
                   	 		pos = s.indexOf(": ");
                    		if (pos != -1) {
                       			contentType = s.substring(pos+2, s.length());
                    		}
                 		}
                 		//FIN: setContentType(new String(line, 0, i-2)); 
                 		i = in.readLine(line, 0, 128);
                 		// blank line
                 		i = in.readLine(line, 0, 128);
                 		newLine = new String(line, 0, i);
                 		//PrintWriter pw = new PrintWriter(new BufferedWriter(new
                 		//FileWriter((savePath==null? "" : savePath) + filename))); 
                 		PrintStream pw = new PrintStream(new BufferedOutputStream(new
                 		FileOutputStream((savePath==null? "" : savePath) + "up_" +filename.replace(' ','_')))); 
                 
                 		fields.put("filename", (savePath==null? "" : savePath) + "up_" +filename.replace(' ','_')); 
                 		out.println("<b>Subiendo archivo:" + filename.replace(' ','_'));
				 		archivo="up_" +filename.replace(' ','_');
                 		out.println("<br><b>Subido archivo:" + archivo);
				 		//out.flush();
                 		while (i != -1 && !newLine.startsWith(boundary)) {
                    		// the problem is the last line of the file content
	                     	// contains the new line character.
    	                 	// So, we need to check if the current line is
        	             	// the last line.
            	         	line2 = (byte[])line.clone();
                	     	xi = i;
                    	 	i = in.readLine(line, 0, 128);
                     
                     		//if (i != -1) {
	                      		//if ((i==boundaryLength+2 || i==boundaryLength+4) // + 4 is eof
    	                  		//    && (new String(line, 0, i).startsWith(boundary))) {
        	                if (new String(line, 0, i).startsWith(boundary)) {
            	            	pw.write(line2,0,xi-2);
                	        } else {
                    	        pw.write(line2,0,xi);
                        	}
	                        newLine = new String(line, 0, i);
    	                 //}
        	         	}
            	     	pw.close();
              		}  
           		} else {
                	//this is a field
	                // get the field name
    	            pos = newLine.indexOf("name=\"");
        	        String fieldName = newLine.substring(pos+6, newLine.length()-3);
          
            	    // System.out.println("fieldName:" + fieldName);
	                i = in.readLine(line, 0, 128);
    	            // blank line                
        	        i = in.readLine(line, 0, 128);
            	    newLine = new String(line, 0, i);
                	StringBuffer fieldValue = new StringBuffer(128);
	                while (i != -1 && !newLine.startsWith(boundary)) {
    	                // The last line of the field
        	            // contains the new line character.
            	        // So, we need to check if the current line is
                	    // the last line.
                    	i = in.readLine(line, 0, 128);
	                    //System.out.println("i: " + i + "boundaryLength: " + boundaryLength);
    	                if ((i==boundaryLength+2 || i==boundaryLength+4) // + 4 is eof
        	             && (new String(line, 0, i).startsWith(boundary))) {
            	        	fieldValue.append(newLine.substring(0, newLine.length()-2));
                	    } else {
                      		fieldValue.append(newLine.substring(0, newLine.length()-2));
                    	}
             	       newLine = new String(line, 0, i);                    
                	} 
               	 	System.out.println("fieldValue:" + fieldValue.toString());
	                fields.put(fieldName, fieldValue.toString()); 
					ruta = " " + fieldValue.toString();
					System.out.println(" RUTA = " + ruta);
    	       	}
        	}
      		out.println(newLine+"<br>");
       		i = in.readLine(line, 0, 128);
		} // end while   
	}
	out.println("<br>... finalizado.</b><p>\n");
	//for (Enumeration e = fields.keys() ; e.hasMoreElements() ;) {
	//       System.out.println(e.nextElement());
	//}

	/*String cEnlace = fields.get("UPLOACODE").toString() + "?filename=" +
    	               fields.get("filename").toString() + "&task=" + cTask;
	out.println( cEnlace );
	out.flush();*/
%>
<%
} catch (Exception e) {
	//e.printStackTrace();
  	out.println(e.getMessage());
  	//out.println(e.getMessage());
}

response.sendRedirect("conciliacion.jsp?sw=1&filename=" + archivo + "&ruta=" + ruta );
%>      