<!--
- Autor : Ing. Sandra Escalante
- Modificado: Ing Ivan Dario Gomez
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista para mostrar la informacion del archivo procesado 
--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>.: Conciliacion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language='JavaScript' src='<%=BASEURL%>/js/date-picker.js'></script>
<script src="js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#EBEBEB">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Conciliacion"/>
</div>	
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" id="forma" method="post" enctype="MULTIPART/FORM-DATA" action="">
<%
int placasEncontradas = Integer.parseInt(request.getParameter("npe"));
int placasNoEncontradas = Integer.parseInt(request.getParameter("npne"));
int placasLeidas = placasEncontradas + placasNoEncontradas;
String listaPlacasEncontradas = request.getParameter("lpne");
%>
<table width="530" border="2" align="center">
  <tr>
    <td>
	
	<table width="530" align="center" class="tablaInferior">
    <tr class="barratitulo">
      <td colspan="2" nowrap>
        <table width="100%" >
          <tr class="barratitulo">
            <td width="50%" class="subtitulo1">CONCILIACION</td>
            <td width="50%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"><%=datos[0]%></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr class="letra informacion">
      <td colspan="2" nowrap>La sincronizacion ha finalizado con exito!</td>
    </tr>
    <tr>
      <td width="291" nowrap class="fila"  >Nro. Placas Leidas </td>
      <td nowrap  class="letra" ><%=placasLeidas%></td>
    </tr>
    <tr>
      <td width="291" nowrap class="fila"  ><strong>No. Placas encontradas en planilla: </strong></td>
      <td width="215" nowrap class="letra" ><%=placasEncontradas%>
      </td>
    </tr>
	<tr  >
      <td width="291" nowrap class="fila" ><strong>No. Placas NO encontradas en  planilla: </strong></td>
      <td width="215" nowrap class="letra"><%=placasNoEncontradas%>
      </td>
    </tr>
    <%if ( placasNoEncontradas != 0 ){%>
	<tr class="fila" >
      <td width="291" nowrap ><strong>Lista de Placas: </strong></td>
      <td width="215" nowrap ><textarea name="lple" cols="15" rows='10' readonly="readonly" class="textbox"><%=listaPlacasEncontradas%></textarea></td>
    </tr>
    <tr class="letra informacion" >
      <td colspan="2" nowrap  class="Estilo1">***Ha sido enviado un correo a los usuarios correspondientes***</td>
    </tr>
	<%}%>
  </table>
	
	</td>
  </tr>
</table>

  
  <center>
  <br>
  <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c"  height="21" title="Regresar..." onClick="window.location='<%=BASEURL%>/conciliacion_placas/conciliacion.jsp?sw=0'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
    <!--<input name="regresar" type="button" value="Regresar" title="Presione aqui para regresar a la pagina principal de Conciliacion" >-->
  </center>
</form>
</div>
<%=datos[1]%>
</body>
</html>

