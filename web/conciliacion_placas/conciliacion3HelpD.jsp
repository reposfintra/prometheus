<!--
- Autor : Ing. Sandra Escalante
- Modificado: Ing Ivan Dario Gomez
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista para mostrar la informacion de las ayudas
--%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos para la conciliacion</title>


<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">PROCESOS</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Conciliacion </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> Upload Archivo </td>
        </tr>
        <tr>
          <td width="131" class="fila"> Boton Examinar</td>
          <td width="543"  class="ayudaHtmlTexto">Boton que permite buscar el archivo a cargar.</td>
        </tr>
        <tr>
          <td  class="fila"> Boton Aceptar </td>
          <td  class="ayudaHtmlTexto">Boton para cargar el archivo. </td>
        </tr>
        <tr>
          <td  class="fila">Boton regresar</td>
          <td  class="ayudaHtmlTexto"> Boton que permite regrear ala pagina anterior. </td>
        </tr>
        <tr>
          <td  class="fila">Boton Salir</td>
          <td  class="ayudaHtmlTexto">Boton que cierra la ventana.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
