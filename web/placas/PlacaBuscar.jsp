<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Buscar Placa Y Conductores</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
</head>

<body>
<% String tipid="";%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Placa_conductor&accion=Serch&listar=True">
  <table width="642" border="2" align="center">
    <tr>
      <td><table width="100%" align="center"  class="tablaInferior">
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Consulta Historial Viajes </td>
          <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
        <tr class="fila">
          <td align="left" >Placa</td>
          <td valign="middle"><input name="c_placa" type="text" class="textbox" id="c_placa" size="12" maxlength="12">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td valign="middle">&nbsp;</td>
          <td valign="middle">&nbsp;</td>
        </tr>
        <tr class="fila" id="cantidad">
          <td width="124" align="left" > Fecha Inicio </td>
          <td width="182" valign="middle">
            <input name="c_fecha_inicio" type="text" class="textbox" id="c_fecha_inicio" size="10" readonly>
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.c_fecha_inicio);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a></td>
          <td width="125" valign="middle">Fecha Fin </td>
          <td width="183" valign="middle">
            <input name="c_fecha_fin" type="text" class="textbox" id="c_fecha_fin" size="10" readonly>
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.c_fecha_fin);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a>
          </td>
        </tr>
        <tr class="fila">
          <td width="124" align="left" > Agencia Origen </td>
          <td width="182" valign="middle"><input name="c_agencia_origen" type="text" class="textbox" id="c_agencia_origen" size="20"></td>
          <td width="125" valign="middle">Agencia Destino </td>
          <td width="183" valign="middle"><input name="c_agencia_destino" type="text" class="textbox" id="c_agencia_destino" value="" size="20">
          </td>
        </tr>
        <tr class="fila">
		  <td width="124" align="left" > Agencia Despacho </td>
          <td width="182" valign="middle"><select name="c_agencia_despacho" class="textbox" id="c_agencia_despacho">
		   	  <option value=""></option>
			  <%model.agenciaService.cargarAgencias();
				Vector vec = model.agenciaService.getAgencias();
				for(int i = 0; i<vec.size(); i++){	
					Agencia a = (Agencia) vec.elementAt(i);	%>
					<option value="<%=a.getId_agencia()%>"><%=a.getNombre()%></option>
				<%}%>
		  </select></td>
          <td width="125" valign="middle">Cedula Conductor</td>
          <td width="183" valign="middle"><input name="c_cedula" type="text" class="textbox" id="c_cedula" value="" size="20">          </td>
        </tr>
        <tr class="fila">
          <td colspan="4" align="left" >&nbsp;</td>
        </tr>
      </table></td>
    </tr>
  </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar placa" name="buscar"  onClick="validarbusqueda();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p>
<%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
if(!msg.equals("")){%>
	<p>
   <table width="407" border="2" align="center">
     <tr>
    <td><table width="100%"   align="center"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58" bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
</form>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
