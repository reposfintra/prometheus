<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<html>
<head>
<title>Modificar Placa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</head>
<body onLoad="<%if(request.getParameter("mensajeAut")!=null){%>alert('<%=request.getParameter("mensajeAut")%>');<%}%>">
<%//nuevo   
        String tipdoc="012"; 
		String act="005";
		//*****
%>
<form name="frmbuscarplaca" action="<%=CONTROLLER%>?estado=Placa&accion=Search&cmd=show" method="post">
<table border="2" align="center" width="845">
  <tr>
    <td>
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Buscar Placa </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
<tr class="fila">
    <td width="164" height="27">Digite la placa a Modificar </td>
    <td width="624" ><input name="idplaca" type="text" class="textbox" title="Numero de la placa" maxlength="10"> 
      <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgsalir"  height="21" align="absmiddle" style="cursor:hand" onClick="frmbuscarplaca.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
</tr>
</table>
</td>
</tr>
</table>
</form>
<input:form name="frmplaca" action="controller?estado=Placa&accion=Update&cmd=show" method="post" bean="placa">
<table border="2" align="center" width="845">
  <tr>
    <td>
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Caracteristicas del vehiculo </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
<tr class="fila">
    <td height="21" colspan="10" align="right" style="cursor:hand "  class="Simulacion_Hiper" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>','','<%=act%>','031')">Agregar imagen del vehiculo</span></td>
    </tr>
<tr class="fila">
    <td height="22">Estado</td>
    <td height="22" colspan="9"><% TreeMap estados = new TreeMap();
                estados.put("INACTIVO", ""); 
                estados.put("ACTIVO", "A");
             %> 
            <input:select name="reg_status" options="<%=estados%>" attributesText="style='width:15%;' class='textbox'"/></td>
    </tr>
  <tr class="fila">
    <td width="50" height="30">Placa</td>
    <td width="52" ><input:text name="placa" attributesText="maxlength='10' size='8' readonly class='textbox'"/> </td>
    <td width="29" >Tipo</td>
    <td width="73"><% TreeMap tipos = model.tblgensvc.getTipos(); %>
        <input:select name="tipo" options="<%=tipos%>" attributesText="style='width:84%;' class='listmenu'"/></td>
    <td width="107">Chasis Numero </td>
    <td width="71"><input:text name="nochasis" attributesText="maxlength='15' size='16' class='textbox'"/></td>
    <td width="37">Clase</td>
    <td width="92"><% TreeMap clases = model.tblgensvc.getClases(); %>      <input:select name="clase" options="<%=clases%>" attributesText="style='width:84%;' class='listmenu'"/></td>
    <td width="75">Numero Rin </td>
    <td width="146"><input:text name="numero_rin" attributesText="maxlength='5' size='6' class='textbox'"/></td>
  </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="50" height="22">Marca</td>
    <td width="182"><% TreeMap marcas = model.marcaService.listar(); 
         %>
      <input:select name="marca" options="<%=marcas%>" attributesText="style='width:100%;' class='textbox'"/></td>
    <td width="92">Modelo</td>
    <td width="70"><input:text name="modelo" attributesText="class='listmenu' size='10' onkeypress='return acceptNum(event)' maxlength='4''"/></td>
    <td width="37">Color</td>
    <td width="325"><% TreeMap colores = model.colorService.listar();
         %>
      <input:select name="color" options="<%=colores%>" attributesText="style='width:50%;' class='listmenu'"/> </td>
  </tr>
  <tr class="fila">
    <td width="50" height="30">Carroceria</td>
    <td><% TreeMap carrs = model.carrService.listar();%>      <input:select name="carroceria" attributesText="style='width:100%;' class='listmenu'" options="<%=carrs%>" default="SS"/></td>
    <td width="92">Placa Trailer</td>
	<td><input:text name="placa_trailer" attributesText="maxlength='15' size='8' class='textbox'"/></td>
	<td>Serial Motor </td>
	<td><input:text name="nomotor" attributesText="maxlength='15' class='textbox'"/></td>
  </tr>
</table>
</td>
</tr>
</table>
<table border="2" align="center" width="845">
  <tr>
    <td>
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Informaci&oacute;n del Propietario </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="56" height="22">Agencia</td>
    <td width="136" ><% TreeMap agencia = model.agenciaService.listar(); 
         agencia.put("", "");%>
      <input:select name="agencia" attributesText="style='width:90%;' class='listmenu'" options="<%=agencia%>" default="SM"/></td>
    <td width="66" >Homologado</td>
    <td width="107"><input:text name="homologado" attributesText="maxlength='1' size='13' class='textbox'"/></td>
    <td width="74">Condici&oacute;n</td>
    <td width="317"><input:text name="condicion" attributesText="maxlength='1' size='13' class='textbox'"/></td>
  </tr>
</table>
<table width="99%"  align="center"   >
  <tr class="fila">
    <td width="88" height="30">Tarjeta de Operaci&oacute;n </td>
    <td width="128" ><input:text name="tarjetaoper" attributesText="maxlength='15' size='19' class='textbox'"/></td>
    <td width="70" >Fecha de Vencimiento </td>
    <td width="99" nowrap><input:text name="venctarjetaoper" attributesText="maxlength='10' size='11' class='textbox' readonly"/><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venctarjetaoper);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
    <td width="120">Tarjeta Empresarial </td>
    <td width="130"><input:text name="tarempresa" attributesText="maxlength='15'  size='16' class='textbox'"/><img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',9,'<%=act%>','018')" style="cursor:hand "></td>
    <td width="108">Fecha de vencimiento </td>
    <td width="108" nowrap><input:text name="fecvenempresa" attributesText="maxlength='15'  size='11' class='textbox' readonly"/><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.fecvenempresa);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a>   </td>
  </tr>
  <tr class="fila">
    <td width="88" height="30">Tarjeta de Habilitaci&oacute;n </td>
    <td width="128" ><input:text name="tarhabil" attributesText="maxlength='15'  size='16' class='textbox'"/><img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',11,'<%=act%>','015')" style="cursor:hand "></td>
    <td width="70" >Fecha de Vencimiento </td>
    <td width="99" nowrap><input:text name="fecvenhabil" attributesText="maxlength='15'  size='11' class='textbox' readonly"/><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venhabilitacion);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
    <td width="120">Registro nal de carga </td>
    <td width="130"><input:text name="reg_nal_carga" attributesText="maxlength='15'  size='16' class='textbox'"/><img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',7,'<%=act%>','019')" style="cursor:hand "></td>
    <td>Fecha de vencimiento    
      </td>
	  
	  <td nowrap><input:text name="fecvenreg" attributesText="maxlength='15'  size='11' class='textbox' readonly"/><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.fecvenreg);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
  </tr>
  <tr class="fila">
    <td width="88" height="30">Tarjeta de Propiedad </td>
    <td width="128" ><input:text name="tarprop" attributesText="maxlength='15'  size='16' class='textbox'"/><img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',1,'<%=act%>','014')" style="cursor:hand "></td>
    <td width="70" >Fecha de Vencimiento </td>
    <td width="99" nowrap><input:text name="fecvenprop" attributesText="maxlength='15'  size='11' class='textbox' readonly"/><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.fecvenprop);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
    <td width="120">Poliza Andina </td>
    <td width="130"><input:text name="poliza_andina" attributesText="maxlength='15'  size='16' class='textbox'"/><img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',11,'<%=act%>','016')" style="cursor:hand "></td>
    <td width="108">Fecha de vencimiento </td>
    <td width="108" nowrap><input:text name="fecvenandina" attributesText="maxlength='15'  size='11' class='textbox' readonly"/><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.fecvenandina);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
  </tr>
  <tr class="fila">
    <td width="88" height="30">Poliza SOAT </td>
    <td width="128" ><input:text name="polizasoat" attributesText="maxlength='15'  size='16' class='textbox'"/><img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',6,'<%=act%>','019')" style="cursor:hand "></td>
    <td width="70" >Fecha de Vencimiento </td>
    <td width="99" nowrap><input:text name="venseguroobliga" attributesText="maxlength='15'  size='11' class='textbox' readonly"/><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venseguroobliga);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
    <td width="120">Tenedor</td>
    <td width="120"><input:text name="tenedor" attributesText="maxlength='15'  size='17' class='textbox'"/></td>
    <td width="108">A Titulo</td>
    <td width="108"><input:text name="atitulo" attributesText="maxlength='20' size='17' class='textbox'"/></td>
  </tr>
  <tr class="fila">
    <td width="88" height="30">Certificado de Emisi&oacute;n de Gases </td>
    <td width="128"><input:text name="certgases" attributesText="maxlength='15'  size='16' class='textbox'"/><img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',5,'<%=act%>','017')" style="cursor:hand "></td>
    <td width="70" >Fecha de Vencimiento </td>
    <td width="99" nowrap><input:text name="fecvengases" attributesText="maxlength='15'  size='11' class='textbox' readonly"/><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.fecvengases);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
		<td>Grado de riesgo</td>
		<td colspan="3"><input:text name="grado_riesgo" attributesText="maxlength='2'  size='4' class='textbox'"/></td>
  </tr>
  <tr class="fila">
    <td width="88" height="30" rowspan="2">Empresa Afiliada</td>
    <td width="105" rowspan="2" ><input:text name="empresaafil" attributesText="maxlength='15' size='18' class='textbox'"/></td>
    <td width="70" rowspan="2" >Cedula Conductor</td>
    <td width="99" rowspan="2"><input:text name="conductor" attributesText="maxlength='15' size='16' class='textbox'"/></td>
    <td width="120" rowspan="2">Cedula Propietario </td>
    <td colspan="3"><input:text name="propietario" attributesText="maxlength='15' size='16' class='textbox'"/>
	<input:hidden name="vprop" attributesText="maxlength='15' size='16' class='textbox'" /> </td>
  </tr>
  <tr class="fila">
    <td colspan="3"><a class="Simulacion_Hiper" style="cursor:hand "  onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=37&propietario=ok','Prop','height=350,width=650,scrollbars=yes,status=yes,resizable=yes')" >Buscar Propietario</a></td>
  </tr>
</table>
</td>
</tr>
</table>
<table border="2" align="center" width="845">
  <tr>
    <td>
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Dimensiones del Vehiculo </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center"   >
  <tr class="fila">
    <td width="62">Capacidad</td>
    <td width="102" ><input:text name="capacidad" attributesText="maxlength='8' class='textbox' size='7' onkeypress='return acceptNum(event)'"/>Ton. </td>
    <td width="44" >Volumen</td>
    <td width="77"><input:text name="volumen" attributesText="maxlength='15' class='textbox' size='10' onkeypress='return acceptNum(event)'"/></td>
    <td width="59">Ancho</td>
    <td width="77"><input:text name="ancho" attributesText="maxlength='15' class='textbox' size='4' onkeypress='return acceptNum(event)'"/>      mts </td>
    <td width="33">Largo</td>
    <td width="80"><input:text name="largo" attributesText="maxlength='15' class='textbox' size='4' onkeypress='return acceptNum(event)'"/>      mts</td>
    <td width="28">Alto</td>
    <td width="171"><input:text name="alto" attributesText="maxlength='15' class='textbox' size='4'  onkeypress='return acceptNum(event)'"/>      mts </td>
  </tr>
  <tr class="fila">
    <td width="62">Tara</td>
    <td width="102" ><input:text name="tara" attributesText="maxlength='15' size='14' class='textbox' onkeypress='return acceptNum(event)'"/></td>
    <td width="44" >Cargue</td>
    <td width="77"><input:text name="cargue" attributesText="maxlength='15' size='10' class='textbox' onkeypress='return acceptNum(event)'"/></td>
    <td width="59">Llantas</td>
    <td width="77"><input:text name="llantas" attributesText="maxlength='15' size='10' class='textbox'"/></td>
    <td width="33">Piso</td>
    <td colspan="3"><input:text name="piso" attributesText="maxlength='15' size='8' class='textbox'"/></td>
  </tr>
  <tr class="fila">
    <td width="62" height="30">Dimensi&oacute;n Carroceria </td>
    <td width="102"><input:text name="dimcarroceria" attributesText="maxlength='20' size='20' class='textbox'"/></td>
    <td width="44">Numero de Ejes</td>
    <td width="77"><input:text name="noejes" attributesText="maxlength='1' size='8' onkeypress='return acceptNum(event)' class='textbox'"/></td>
    <td width="59">Capacidad Trailer </td>
    <td colspan="5"><input:text name="capacidad_trailer" attributesText="maxlength='1' size='8' onkeypress='return acceptNum(event)' class='textbox'"/></td>
  </tr>
</table>
</td>
</tr>
</table>
<table border="2" align="center" width="845">
  <tr>
    <td>
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1">Datos del Grupo </td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center"   >
  <tr class="fila">
    <td width="47" height="17"><p>Recurso</p>    </td>
    <td width="84" ><input:text name="recurso" attributesText="maxlength='10' size='16' class='textbox'"/></td>
    <td width="60" >Id. Gripo </td>
    <td width="71"><input:text name="grupoid" attributesText="maxlength='10' size='10' class='textbox'"/></td>
    <td width="40">Grupo</td>
    <td width="115"><input:text name="grupo" attributesText="maxlength='10' size='18' class='textbox'"/></td>
    <td width="48">Nombre</td>
    <td width="280"><input:text name="nombre" attributesText="maxlength='10' size='30' class='textbox'"/></td>
  </tr>

  <tr class="fila">
    <td width="47" height="30"><p>Estado Equipo </p></td>
    <td width="84"><input:text name="estadoequipo" attributesText="maxlength='10' size='10' class='textbox'"/></td>
    <td width="60">Localizaci&oacute;n</td>
    <td colspan="5"><input:text name="localizacion" attributesText="maxlength='10' size='20' class='textbox'"/></td>
  </tr>
</table>
</td>
</tr>
</table>
<table width="845" border="2" align="center">
  <tr>
    <td height="63">
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="22"  class="subtitulo1">Referencias</td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center"  >
  <tr class="fila">
    <td height="22" style="cursor:hand " class="Simulacion_Hiper" onClick="modReferencia('<%=BASEURL%>')">Modificar referencias de este vehiculo</td>
    </tr>
</table>
    </td>
  </tr>
</table>
<br>
<table width="797"  align="center"   bordercolor="#F7F5F4">
  <tr>
    <td height="27"><p align="center">   
	   <img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod"  height="21" onclick="validarPlaca('<%=CONTROLLER%>','<%=BASEURL%>','<%=request.getParameter("verificacion")%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="frmplaca.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> 
	   </p>    </td>
    </tr>
</table>
<%if(!msg.equals("")){%>
	<p>
   <table width="451" border="2" align="center">
     <tr>
    <td><table width="100%"   align="center"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>


<br>
<%if(request.getParameter("clave")!=null){%>
<table width="50%" border="2" align="center">
  <tr>
    <td><table width="100%"   align="center" cellpadding="3" cellspacing="2"  class="fila">
        <tr >
          <td width="52%" height="22" class="subtitulo1" ><div align="center" class=>
              <div align="left"><strong> Solicitud de Clave</strong></div>
          </div></td>
          <td width="48%" height="22" class="barratitulo" ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
        <tr class="fila">
          <td>INGRESE SU CLAVE: </td>
          <td><input name="nclave" type="text" class="textbox" id="nclave"></td>
        </tr>
        <tr class="fila">
          <td><div align="center" class="fila" >
              <div align="left">INGRESE SU NUMERO DE SOLICITUD:</div>
          </div></td>
          <td><input name="numsol" type="text" class="textbox" id="numsol"></td>
        </tr>
        <tr>
          <td colspan="2"><div align="center">              <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod" id="mod" height="21" onClick="frmplaca.action='<%=CONTROLLER%>?estado=Validar&accion=ClavePlacaProp';frmplaca.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="visibility:visible;cursor:hand" ></div></td>
        </tr>
    </table></td>
  </tr>
</table>
<%}%>
</input:form>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</iframe>
</body>
</html>
<script>
function modReferencia(BASEURL) {
    var pag="";
	if(frmplaca.placa.value==""){
		alert("Debe digitar una placa");
		frmplaca.placa.focus();
	} else {
	    pag = "/jsp/hvida/referencias/referenciaUpdate.jsp?tipo=EA-_-documento="+frmplaca.placa.value;
		window.open(BASEURL+"/Marcostsp.jsp?encabezado=Actualizar&dir="+pag,'Trafico','width=700,height=520,scrollbars=yes,resizable=yes,top=10,left=65');
	}
}
</script>
