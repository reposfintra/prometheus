<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page errorPage="../../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input"%>
<%@page import="com.tsp.util.*"%>
<%@page import="java.util.*" %>

<%! public String reset(String value){
      return value!=null? value : "";
    }
%>

<%String placa ="", conductor="";
	if(request.getParameter("placa")!=null){
		placa = request.getParameter("placa");
		conductor = request.getParameter("conductor");
	}
	if(request.getParameter("conductor")!=null){
		
	}
        LinkedList list_gps = model.tablaGenService.getDatos2();
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String control = session.getAttribute("USRIDEN")!=null?(String) session.getAttribute("USRIDEN"):"";
%>

<html>
<head>
<title>Ingreso de Placas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/referencia.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 

</head>

<body onResize="redimensionar();" onLoad="document.frmplaca.placa.focus();redimensionar();<%if(request.getParameter("mensajeAut")!=null){%>alert('<%=request.getParameter("mensajeAut")%>');<%}%>">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Placas Cabezote"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%//nuevo   
                String tipdoc="012"; 
                String hoy1  = com.tsp.util.Utility.getDate(8).substring(0,10);
		String act="005";
		String hoy          = Utility.getHoy("-");
		int ano = Integer.parseInt(hoy.substring(0,4));
		//*****
%>

<form name="frmplaca" id="frmplaca" action="<%=CONTROLLER%>?estado=Placa&accion=Insert&cmd=show" method="post">   
  <input name="hoy1" type="hidden" id="hoy1" value="<%=hoy1%>">
  <table border="2" align="center" width="950">
  <tr>
    <td>

<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Caracteristicas del vehiculo </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
<tr class="fila">
    <td height="21" colspan="8" align="right"  style="cursor:hand "  class="filaresaltada" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>','','<%=act%>','031')">Agregar imagen del vehiculo</span></td>
    </tr>
  <tr class="fila">
    <td width="63" height="30">Placa</td>
    <td width="84" ><input name="placa" type="text" class="textbox" id="placa" size="8" maxlength="12" value="<%=request.getParameter("placa")!=null?request.getParameter("placa"):""%>">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="31" >Tipo</td>
    <td width="197" nowrap><% TreeMap tipos = model.placaService.obtenerTreeMapLista( model.tablaGenService.getTipo() );
                              tipos.put("Seleccione un item","");%>
        <input:select name="tipo" options="<%=tipos%>" attributesText="style='width:90%;' class='listmenu'" default="<%=reset(request.getParameter("tipo"))%>"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="76">Chasis Numero </td>
    <td width="150" nowrap><input name="noChasis" type="text" class="textbox" id="noChasis" size="22" maxlength="25" value="<%=reset(request.getParameter("noChasis"))%>">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="46">Clase</td>
    <td width="262" nowrap><% TreeMap clases = model.placaService.obtenerTreeMapLista( model.tablaGenService.getClase() );
                              clases.put("Seleccione un item","");%>
        <input:select name="clase" options="<%=clases%>" attributesText="style='width:90%;' class='listmenu'" default="<%=reset(request.getParameter("clase"))%>"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="64" height="30">Marca</td>
    <td width="180"  nowrap><% TreeMap marcas = model.marcaService.listar();
                              marcas.put("Seleccione un item","");
         %><input:select name="marca" options="<%=marcas%>" attributesText="style='width:90%;' class='listmenu'" default="<%=reset(request.getParameter("marca"))%>"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">        </td>
    <td width="52" >Modelo </td>
    <td width="165" nowrap><select name="modelo" class="textbox" id="modelo">
  			          <%for(int i = 1900; i<=ano; i++){%>
  			                  <option value="<%=i%>"><%=i%></option>
  			        <%}%>
					</select>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
	  
	  <script>frmplaca.modelo.value = "<%=reset(request.getParameter("modelo"))%>"</script>
	  
	  </td>
    <td width="86">Color</td>
    <td colspan="3" nowrap><% TreeMap colores = model.colorService.listar();
                              colores.put("Seleccione un item","");
         %>
            <input:select name="color" options="<%=colores%>" attributesText="style='width:65%;' class='listmenu'" default="<%=reset(request.getParameter("color"))%>" /> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
  </tr>
  <tr class="fila">
    <td width="64" height="30">Carroceria</td>
    <td colspan="3" ><% TreeMap carrs = model.carrService.listar();
                        carrs.put("Seleccione un item",""); %>
        <input:select name="carroceria" attributesText="style='width:100%;' class='listmenu'" options="<%=carrs%>" default="<%=reset(request.getParameter("carroceria"))%>"/>        	</td>
    <td>Serial Motor</td>
    <td width="144" nowrap><input name="nomotor" type="text" id="nomotor" class="textbox" maxlength="15" value="<%=reset(request.getParameter("nomotor"))%>">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="49" nowrap>No. Rin </td>
    <td width="153" nowrap><select name="norin" class="textbox"  id="norin" style="width:86%">
						<option value="">Seleccione Un Item</option>
  			          <%
                      LinkedList tblrin = model.tablaGenService.getRin();
					  System.out.println("--");
					  System.out.println("Pagina JSP Rin " + tblrin.size ());
  			          for(int i = 0; i<tblrin.size(); i++){
  			                  TablaGen tblr = (TablaGen) tblrin.get(i);	%>
  			                  <option value="<%=tblr.getTable_code()%>"><%=tblr.getDescripcion()%></option>
  			        <%}%>
					</select>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
	  	<script> frmplaca.norin.value = "<%=reset(request.getParameter("norin"))%>" </script>
	  	</td>
  </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="69" height="30">Capacidad</td>
    <td width="125" ><input name="capacidad" type="text" class="textbox" id="capacidad" onKeyPress="soloDigitos(event, 'decOK')" size="8" maxlength="8" value="<%=reset(request.getParameter("capacidad"))%>">
Ton.<img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="84" >Tara</td>
    <td width="124" nowrap><input name="tara" type="text" class="textbox" id="tara" onKeyPress="soloDigitos(event, 'decOK')" size="15" maxlength="15" value="<%=reset(request.getParameter("tara"))%>"> Ton.</td>
    <td width="54">Cargue</td>
    <td width="226" nowrap><input name="cargue" type="text" class="textbox" id="cargue" onKeyPress="soloDigitos(event, 'decOK')" size="15" maxlength="15" value="<%=reset(request.getParameter("cargue"))%>"></td>
    <td width="58">Numero de Ejes</td>
    <td width="153" nowrap><input name="noejes" type="text" class="textbox" id="noejes" onKeyPress="return acceptNum(event)" size="8" maxlength="1" value="<%=reset(request.getParameter("noejes"))%>"> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
  </tr>
  <tr class="fila">
    <td height="30">Capacidad<br>
      Galones</td>
    <td ><input name="capac_gal" type="text" class="textbox" id="volumen" size="10"  onblur='ValidarCampo(this,10,3)' maxlength="11" onKeyPress="soloDigitos(event, 'decOK')" value="<%=reset(request.getParameter("capac_gal"))%>"></td>
    <td >Capacidad<br>mts&#179;</td>
    <td><input name="capac_mts" type="text" class="textbox" id="capac_mts" size="10" onblur='ValidarCampo(this,10,3)' maxlength="11" onKeyPress="soloDigitos(event, 'decOK')" value="<%=reset(request.getParameter("capac_mts"))%>"></td>
    <td>Grupo</td>
	<td><% TreeMap grupoeq = model.placaService.obtenerTreeMapLista( model.tablaGenService.getGrupoid() ); 
                           grupoeq.put("Seleccione un item","");%>
    <input:select name="grupoid" attributesText="style='width:90%;' class='listmenu'" options="<%=grupoeq%>" default="<%=reset(request.getParameter("grupoid"))%>"/><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>	
    <td>Recurso</td>
    <td><input name="recurso" type="text" class="textbox" id="cedula22624" size="16" maxlength="10" value="<%=reset(request.getParameter("recurso"))%>"></td>
  </tr>
</table>
    </td>
  </tr>
</table>
<input type='hidden' name='uservetonit' id='uservetonit' value='<%=control%>'>
<%if(control.equals("S")){%>

<table border="2" align="center" width="950">
  <tr>
    <td>
        <table width="99%"  align="center"   >
            <tr>
                <td width="392" height="24"  class="subtitulo1"><p align="left">Informaci&oacute;n del Reporte </p></td>
                <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
        </table>
        <table width="99%" align="center">
        <% TreeMap ca = model.tablaGenService.BuscarDatosTreemap("CAUSAVETO"); %>
        <% TreeMap fu = model.tablaGenService.BuscarDatosTreemap("FUENTEREP"); %>
            <tr class="fila">
                <td width="15%">Veto </td>
                <td colspan="3">
                    <select name="veto" onchange="enableFila2()">
                        <option value="S">Si</option>
						<option value="N">No</option>                        
                    </select>
                </td>
            </tr>    
            <tr id="fila_razon" class="fila">
                 <td width="15%">Causa</td>
                 <td width="30%">
                    <input:select name="causa" options="<%=ca%>" attributesText="style='width:80%' class='textbox' "/>
                    <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                 <td width="15%">Observaci&oacute;n</td>
                 <td width="40%">
                    <input name="observacion" value="" size='65%'>           
                    <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>    
            </tr>
            <tr id="fila_razon1" class="fila">
                 <td>Fuente </td>
                 <td colspan='3'><input:select name="fuente" options="<%=fu%>" attributesText="style='width:35%' class='textbox' "/></td>
            </tr>
         </table>
     </td>
  </tr>
</table>

<%}%>


  <!-- ************* GPS **************-->
  <table border="2" align="center" width="950">
    <tr>
        <td>
        
        <table width="99%"   align="center"> 
          <tr>
            <td width="50%" height="24"  class="subtitulo1"><p align="left">Información GPS </p></td>
            <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
        <table width="99%"  align="center"   >
          <tr class="fila">
            <td width="47" height="28">&nbsp;GPS</td>
            <td width="77" >
              <select name ="gps" id="gps" onchange="activarDatosGpsInsert()">
                <option value='SI'>SI
                <option selected value='NO'>NO
              </select>
            </td>
            <td width="96" >&nbsp;Operador GPS</td>
            <td width="138">
              <select name="operador_gps" class="textbox"  id="operador_gps" disabled>
                        <option value="">--Seleccione--</option>
              <%
                            for(int i = 0; i<list_gps.size(); i++){
                                    TablaGen oper = (TablaGen) list_gps.get(i);	%>
              <option value="<%=oper.getTable_code()%>"><%=oper.getDescripcion()%></option>
              <%}%>
            </select>
              <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            <td width="157">&nbsp;Clave Seguimiento</td>
            <td width="152"><input disabled name="clave_seg" type="password" class="textbox" id="clave_seg" value="<%//=request.getParameter("propietario")!=null?request.getParameter("propietario"):""%>" size="16" maxlength="12">
              <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            <td width="111">&nbsp;Confirme Clave</td>
            <td width="118"><input  disabled name="conf_clave" type="password" class="textbox" id="conf_clave" value="<%//=request.getParameter("propietario")!=null?request.getParameter("propietario"):""%>" size="14" maxlength="12">
              <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            </tr>
        </table>
        
        </td>
    </tr>  
  </table>
  
  <!-- ************* Documemtos del Vehiculo **************-->
<table width="950" border="2" align="center">
  <tr>
    <td>
<table width="99%"   align="center"> 
  <tr>
    <td width="50%" height="24"  class="subtitulo1"><p align="left">Documentos del Vehiculo </p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="86" height="22">Agencia</td>
    <td width="269" ><% TreeMap agencia = model.agenciaService.listar(); 
         agencia.put("", "");%>
            <input:select name="agencia" attributesText="style='width:90%;' class='listmenu'" options="<%=agencia%>" default="<%=reset(request.getParameter("agencia"))%>"/><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="91" >Homologado</td>
    <td width="114"><input name="homologado" type="text" class="textbox" id="clase23" size="4" maxlength="1" value="<%=reset(request.getParameter("homologado"))%>"></td>
    <td width="85">Condici&oacute;n</td>
    <td width="256"><input name="condicion" type="text" class="textbox" id="tarhabilitacion4243" size="4" maxlength="1" value="<%=reset(request.getParameter("condicion"))%>"></td>
  </tr>
</table>
<table width="99%"  align="center"   >
  <tr class="fila">
    <td width="85" height="30">Tarjeta de Habilitaci&oacute;n</td>
    <td width="141"><input name="tarhabil" type="text" class="textbox" id="tarhabil" size="16" maxlength="15" value="<%=reset(request.getParameter("tarhabil"))%>">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',11,'<%=act%>','015')" style="cursor:hand "> </td>
    <td width="87" >Fecha de Vencimiento</td>
    <td width="129"><input name="venhabilitacion" type="text" class="textbox" id="venhabilitacion" size="11" readonly value="<%=reset(request.getParameter("venhabilitacion"))%>">
      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venhabilitacion);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span>  </td>
    <td width="102">Tarjeta Empresarial </td>
    <td width="153"><input name="tarEmpresa" type="text" class="textbox" id="cedula2224" size="16" maxlength="15" value="<%=reset(request.getParameter("tarEmpresa"))%>">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',9,'<%=act%>','018')" style="cursor:hand "></td>
    <td width="80">Fecha de vencimiento </td>
    <td width="116"><input name="venempresa" type="text" class="textbox" id="habilitacion2332" size="11" readonly value="<%=reset(request.getParameter("venempresa"))%>"> 
    <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venempresa);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a>   
     </td>
  </tr>
  <tr class="fila">
    <td width="85" height="30">Tarjeta de Propiedad </td>
    <td width="141"><input name="tarpropiedad" type="text" class="textbox" id="tarpropiedad" size="16" maxlength="15" value="<%=reset(request.getParameter("tarpropiedad"))%>">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',1,'<%=act%>','014')" style="cursor:hand "> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="87" >Ciudad Tarjeta</td>
    <td width="129"><%model.ciudadService.searchTreMapCiudades();//va en el action
                        TreeMap ciuadad = model.ciudadService.getTreMapCiudades(); %>
      <input:select name="c_ciudad" options="<%=ciuadad%>" attributesText="style='width:90% ' class='textbox'" default="<%=reset(request.getParameter("c_ciudad"))%>"/><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="102">Registro nal de carga. </td>
    <td width="153"><input name="registro" type="text" class="textbox" id="registro" size="16" maxlength="15" value="<%=reset(request.getParameter("registro"))%>">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',7,'<%=act%>','020')" style="cursor:hand "> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="80">Fecha de vencimiento </td>
    <td width="116"><input name="venregistro" type="text" class="textbox" id="venregistro" size="11" readonly value="<%=reset(request.getParameter("venregistro"))%>">
      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venregistro);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span> </td>
  </tr>
  <tr class="fila">
    <td width="85" height="30">Poliza SOAT</td>
    <td width="141" ><input name="soat" type="text" class="textbox" id="soat" size="16" maxlength="15" value="<%=reset(request.getParameter("soat"))%>">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',6,'<%=act%>','019')" style="cursor:hand "> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="87" >Fecha de Vencimiento</td>
    <td width="129"><input name="venseguroobliga" type="text" class="textbox" id="venseguroobliga" size="11" readonly value="<%=reset(request.getParameter("venseguroobliga"))%>">
      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venseguroobliga);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></span></td>
    <td width="102">Poliza Andina </td>
    <td width="153"><input name="polizaandina" type="text" class="textbox" id="polizaandina2" size="16" maxlength="15" value="<%=reset(request.getParameter("polizaandina"))%>">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',11,'<%=act%>','016')" style="cursor:hand "></td>
    <td width="80">Fecha de vencimiento </td>
    <td width="116"><input name="venpolizaandina" type="text" class="textbox" id="venpolizaandina" size="11" readonly="" value="<%=reset(request.getParameter("venpolizaandina"))%>">
      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venpolizaandina);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span></td>
  </tr>
  <tr class="fila">
    <td width="85" height="30">Certificado de medio ambiente </td>
    <td width="141" ><input name="certgases" type="text" class="textbox" id="certgases" size="16" maxlength="15" value="<%=reset(request.getParameter("certgases"))%>">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',5,'<%=act%>','017')" style="cursor:hand "> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="87" >Fecha de Vencimiento</td>
    <td width="129"><input name="fecvengases" type="text" class="textbox" id="fecvengases" size="11" readonly value="<%=reset(request.getParameter("fecvengases"))%>">
      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.fecvengases);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></span></td>
    <td width="102">Poliza de Resp. Civil </td>
    <td width="153"><input name="rcivil" type="text" class="textbox" id="rcivil" size="16" maxlength="15" value="<%=reset(request.getParameter("rcivil"))%>">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',11,'<%=act%>','016')" style="cursor:hand "></td>
    <td width="80">Fecha de vencimiento</td>
    <td width="116"><input name="venrcivil" type="text" class="textbox" id="venrcivil" size="11" readonly="" value="<%=reset(request.getParameter("venrcivil"))%>">
      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venrcivil);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span> </td>
  </tr>
  <tr class="fila">
    <td width="85" height="30">Empresa Afiliadora</td>
    <td width="141" ><input name="empafiliada" type="text" class="textbox" id="empafiliada" size="16" maxlength="15" value="<%=reset(request.getParameter("empafiliada"))%>"> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td colspan="6" >&nbsp;</td>
    </tr>
</table>
</td>
</tr>
</table>
<table  border="2" align="center" width="950">
  <tr>
    <td>
	
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Informaci&oacute;n del Propietario </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center"   >
  <tr class="fila">
    <td width="71">Tenedor</td>
    <td width="124" ><input name="tenedor" type="text" class="textbox" id="tenedor"  value="<%=request.getParameter("tenedor")!=null?request.getParameter("tenedor"):""%>" size="15" maxlength="15">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="78" >Cedula Conductor</td>
    <td width="123"><input name="conductor" type="text" class="textbox" id="conductor" value="<%=request.getParameter("conductor")!=null?request.getParameter("conductor"):""%>" size="13" maxlength="15">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="90">Cedula Propietario </td>
    <td width="135"><input name="propietario" type="text" class="textbox" id="propietario" value="<%=request.getParameter("propietario")!=null?request.getParameter("propietario"):""%>" size="16" maxlength="15">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="78">A Titulo</td>
    <td width="95"><select name="atitulo" class="textbox"  id="atitulo">
		<option value="">Seleccione Un Item</option>
      <%
                    LinkedList tbltitu = model.tablaGenService.getTitulo();
  		    for(int i = 0; i<tbltitu.size(); i++){
  		            TablaGen tblt = (TablaGen) tbltitu.get(i);	%>
      <option value="<%=tblt.getTable_code()%>"><%=tblt.getDescripcion()%></option>
      <%}%>
    </select>
	
	<script>frmplaca.atitulo.value="<%=reset(request.getParameter("atitulo"))%>"</script>
	
	</td>
    </tr>
</table>
</td>
</tr>
</table>
<table width="950" border="2" align="center">
  <tr>
    <td  height="63">
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="22"  class="subtitulo1">Referencias</td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center"  >
  <tr class="fila">
    <td height="21" style="cursor:hand " class="fila" onClick="ReferenciaPlaca('<%=CONTROLLER%>','1')" >Agregar referencias a este vehiculo</td>
    </tr>
</table>
    </td>
  </tr>
</table>
<br>
<table width="797" border="0" align="center">
  <tr>
    <td align="center">         
      <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod" id="mod" style="visibility:visible;cursor:hand" height="21" onClick="validarPlacaCabezoteInsert('<%=CONTROLLER%>','<%=BASEURL%>')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> &nbsp; <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="resetear();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/placas&pagina=placaSeleccion.jsp&marco=no&tipo=I'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">      </td>
    </tr>
</table>
<%if(!msg.equals("")){%>
	<p>
   <table width="407" border="2" align="center">
     <tr>
    <td><table width="100%"   align="center"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58" bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
   <br>
   <input name="hoy" value="<%=hoy%>" type="hidden">
   <input name="grupo_equipo" value="C" type="hidden">
</form><br>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</iframe>
</html>

<%if( msg.equals("Placa agregada exitosamente") ){%>
     <script>resetear();</script> 
<%}%>

<script>
    function noCambiar(){
        document.getElementById( 'causa' ).value = document.getElementById( 'causa_ant' ).value;
    }

</script>
