<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page errorPage="../../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input"%>
<%String placa ="", conductor="";
	if(request.getParameter("placa")!=null){
		placa = request.getParameter("placa");
		conductor = request.getParameter("conductor");
	}
	if(request.getParameter("conductor")!=null){
		
	}
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 


</head>

<body onLoad="<%if(request.getParameter("mensajeAut")!=null){%>alert('<%=request.getParameter("mensajeAut")%>');<%}%>">
<%//nuevo   
        String tipdoc="012"; 
		String act="005";
		//*****
%>

<form name="frmplaca" action="<%=CONTROLLER%>?estado=Placa&accion=Insert&cmd=show" method="post">   
   
  <table border="2" align="center" width="850">
  <tr>
    <td>

<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Caracteristicas del vehiculo </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
<tr class="fila">
    <td height="21" colspan="10" align="right" style="cursor:hand "  class="filaresaltada" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>','','<%=act%>','031')">Agregar imagen del vehiculo</span></td>
    </tr>
  <tr class="fila">
    <td width="62" height="30">Placa</td>
    <td width="76" ><input name="placa" type="text" class="textbox" id="placa" size="6" maxlength="8" value="<%=request.getParameter("placa")!=null?request.getParameter("placa"):""%>">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="41" >Tipo</td>
    <td width="91" nowrap><% TreeMap tipos = model.tblgensvc.getTipos(); %>
        <input:select name="tipo" options="<%=tipos%>" attributesText="style='width:84%;' class='listmenu'"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="51">Chasis Numero </td>
    <td width="119" nowrap><input name="noChasis" type="text" class="textbox" id="clase26" size="16" maxlength="15">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="44">Clase</td>
    <td width="106" nowrap><% TreeMap clases = model.tblgensvc.getClases(); %>
        <input:select name="clase" options="<%=clases%>" attributesText="style='width:86%;' class='listmenu'"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="85">Numero Rin </td>
    <td width="106"><input name="norin" type="text" class="textbox" id="noChasis" size="6"></td>
  </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="61" height="30">Marca</td>
    <td width="215"  nowrap><% TreeMap marcas = model.marcaService.listar(); 
         %>
        <input:select name="marca" options="<%=marcas%>" attributesText="style='width:100%;' class='listmenu'"/></td>
    <td width="49" >Modelo </td>
    <td width="117" nowrap><input name="modelo" type="text" class="textbox" id="modelo" size="10">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="100">Color</td>
    <td width="255" nowrap><% TreeMap colores = model.colorService.listar();
         %>
      <input:select name="color" options="<%=colores%>" attributesText="style='width:50%;' class='listmenu'"/> </td>
  </tr>
  <tr class="fila">
    <td width="61" height="30">Carroceria</td>
    <td width="215" ><% TreeMap carrs = model.carrService.listar();%>
        <input:select name="carroceria" attributesText="style='width:100%;' class='listmenu'" options="<%=carrs%>" default="SS"/></td>
    <td width="49" >Placa Trailer</td>
    <td><input name="placatrailer" type="text" class="textbox" id="placatrailer" size="8">        	</td>
    <td>Serial Motor</td>
    <td nowrap><input name="nomotor" type="text" class="textbox" maxlength="15">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
  </tr>
</table>
</td>
</tr>
</table>
<table width="850" border="2" align="center">
  <tr>
    <td>
<table width="99%"   align="center" bgcolor="#FFFFFF" bordercolor="#F7F5F4"> 
  <tr>
    <td width="393" height="24"  class="subtitulo1"><p align="left">Informaci&oacute;n del Propietario </p></td>
    <td width="414"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="56" height="22">Agencia</td>
    <td width="147" ><% TreeMap agencia = model.agenciaService.listar(); 
         agencia.put("", "");%>
            <input:select name="agencia" attributesText="style='width:90%;' class='listmenu'" options="<%=agencia%>" default="SM"/><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="67" >Homologado</td>
    <td width="30"><input name="homologado" type="text" class="textbox" id="clase23" size="4" maxlength="1"></td>
    <td width="56">Condici&oacute;n</td>
    <td width="401"><input name="condicion" type="text" class="textbox" id="tarhabilitacion4243" size="4" maxlength="1"></td>
  </tr>
</table>
<table width="99%"  align="center"   >
  <tr class="fila">
    <td width="77" height="30">Tarjeta de Operaci&oacute;n </td>
    <td width="125"><input name="tarjetaoper" type="text" class="textbox" id="cedula23" size="17">      </td>
    <td width="79" >Fecha de Vencimiento </td>
    <td width="104"><input name="venctarjetaoper" type="text" class="textbox" id="habilitacion24" size="11" readonly>      
    <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venctarjetaoper);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" border="0" alt=""></a> </span> </td>
    <td width="90">Tarjeta Empresarial </td>
    <td width="135"><input name="tarEmpresa" type="text" class="textbox" id="cedula2224" size="16">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',9,'<%=act%>','018')" style="cursor:hand "></td>
    <td width="78">Fecha de vencimiento </td>
    <td width="101"><input name="venempresa" type="text" class="textbox" id="habilitacion2332" size="11" readonly> 
    <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venempresa);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a>   
     </td>
  </tr>
  <tr class="fila">
    <td width="77" height="30">Tarjeta de Habilitaci&oacute;n </td>
    <td width="130"><input name="tarhabil" type="text" class="textbox" id="cedula226" size="16">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',11,'<%=act%>','015')" style="cursor:hand "></td>
    <td width="79" >Fecha de Vencimiento </td>
    <td width="124"><input name="venhabilitacion" type="text" class="textbox" id="habilitacion224" size="11" readonly>      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venhabilitacion);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span></span>  </td>
    <td width="90">Registro nal de carga. </td>
    <td width="135"><input name="registro" type="text" class="textbox" id="registro" size="16">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',7,'<%=act%>','020')" style="cursor:hand "></td>
    <td width="78">Fecha de vencimiento </td>
    <td width="101"><input name="venregistro" type="text" class="textbox" id="venregistro" size="11" readonly>
      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venregistro);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span> </td>
  </tr>
  <tr class="fila">
    <td width="77" height="30">Tarjeta de Propiedad </td>
    <td width="130" ><input name="tarpropiedad" type="text" class="textbox" id="cedula227" size="16">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',1,'<%=act%>','014')" style="cursor:hand "></td>
    <td width="79" >Fecha de Vencimiento </td>
    <td width="104"><input name="venpropiedad" type="text" class="textbox" id="habilitacion225" size="11" readonly>        <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venpropiedad);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span></span></td>
    <td width="90">Poliza Andina </td>
    <td width="135"><input name="polizaandina" type="text" class="textbox" id="polizaandina" size="16">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',11,'<%=act%>','016')" style="cursor:hand "></td>
    <td width="78">Fecha de vencimiento </td>
    <td width="101"><input name="venpolizaandina" type="text" class="textbox" id="venpolizaandina" size="11" readonly="">
      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venpolizaandina);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span></td>
  </tr>
  <tr class="fila">
    <td width="77" height="30">Poliza SOAT </td>
    <td width="130" ><input name="soat" type="text" class="textbox" id="cedula228" size="16">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',6,'<%=act%>','019')" style="cursor:hand "></td>
    <td width="79" >Fecha de Vencimiento </td>
    <td width="104"><input name="venseguroobliga" type="text" class="textbox" id="habilitacion226" size="11" readonly>        </span><span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venseguroobliga);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span></td>
    <td width="90">Tenedor</td>
    <td width="130"><input name="tenedor" type="text" class="textbox" id="tenedor" size="15"  value="<%=request.getParameter("tenedor")!=null?request.getParameter("tenedor"):""%>">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="78">A Titulo</td>
    <td width="101"><input name="atitulo" type="text" class="textbox" id="atitulo" size="16"></td>
  </tr>
  <tr class="fila">
    <td width="77" height="30">Certificado de Emisi&oacute;n de Gases </td>
    <td width="130" ><input name="certgases" type="text" class="textbox" id="cedula2243" size="16">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',5,'<%=act%>','017')" style="cursor:hand "></td>
    <td width="79" >Fecha de Vencimiento </td>
    <td><input name="fecvengases" type="text" class="textbox" id="habilitacion2243" size="11" readonly>      </span><span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.fecvengases);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span></td>
		<td>Grado de riesgo</td>
		<td colspan="3"><input name="griesgo" type="text" class="textbox" id="griesgo" onkeypress="return acceptNum(event)" size="4" maxlength="2"></td>
	</tr>
  <tr class="fila">
    <td width="77" height="30">Empresa Afiliada</td>
    <td width="112" ><input name="empafiliada" type="text" class="textbox" id="cedula2243" size="18"></td>
    <td width="79" >Cedula Conductor</td>
    <td width="104"><input name="conductor" type="text" class="textbox" id="cedula222334" size="13" value="<%=request.getParameter("conductor")!=null?request.getParameter("conductor"):""%>">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="90">Cedula Propietario </td>
    <td colspan="3"><input name="propietario" type="text" class="textbox" id="propietario" size="16" value="<%=request.getParameter("propietario")!=null?request.getParameter("propietario"):""%>">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
	</tr>
</table>
</td>
</tr>
</table>
<table  border="2" align="center" width="850">
  <tr>
    <td>
	
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Dimensiones del Vehiculo </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center"   >
  <tr class="fila">
    <td width="62">Capacidad</td>
    <td width="102" ><input name="capacidad" type="text" class="textbox" id="capacidad2" size="7" onkeypress="return acceptNum(event)">
      Ton. </td>
    <td width="44" >Volumen</td>
    <td width="77"><input name="volumen" type="text" class="textbox" id="volumen2" size="10" onkeypress="return acceptNum(event)"></td>
    <td width="59">Ancho</td>
    <td width="77"><input name="ancho" type="text" class="textbox" id="ancho" size="4" onkeypress="return acceptNum(event)">
      mts </td>
    <td width="33">Largo</td>
    <td width="80"><input name="largo" type="text" class="textbox" id="largo" size="4" onkeypress="return acceptNum(event)">
      mts </td>
    <td width="28">Alto</td>
    <td width="171"><input name="alto" type="text" class="textbox" id="alto2" size="4" onkeypress="return acceptNum(event)">
      mts </td>
  </tr>
  <tr class="fila">
    <td width="62">Tara</td>
    <td width="102" ><input name="tara" type="text" class="textbox" id="capacidad2" size="14" onkeypress="return acceptNum(event)"></td>
    <td width="44" >Cargue</td>
    <td width="77"><input name="cargue" type="text" class="textbox" id="cargue2" size="10" onkeypress="return acceptNum(event)"></td>
    <td width="59">Llantas</td>
    <td width="77"><input name="llantas" type="text" class="textbox" id="llanatas" size="10"></td>
    <td width="33">Piso</td>
    <td colspan="3"><input name="piso" type="text" class="textbox" id="largo" size="8">            </td>
    </tr>
  <tr class="fila">
    <td width="62" height="30">Dimensi&oacute;n Carroceria </td>
    <td width="102"><input name="dimcarroceria" type="text" class="textbox" id="dimcarroceria" size="20"></td>
    <td width="44">Numero de Ejes</td>
    <td width="77"><input name="noejes" type="text" class="textbox" id="noejes" onkeypress="return acceptNum(event)" size="8" maxlength="1">
    </td>
    <td width="59">Capacidad Trailer </td>
    <td colspan="5"><input name="captrailer" type="text" class="textbox"  id="captrailer" size="8">                </td>	
	    </tr>
</table>
</td>
</tr>
</table>
<table width="850" border="2" align="center" class="tabla">
  <tr>
    <td>
<table width="99%"  align="center">
  <tr>
    <td width="392" height="24"  class="subtitulo1">Datos del Grupo </td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%" align="center" >
  <tr class="fila">
    <td width="47" height="22"><p>Recurso</p>    </td>
    <td width="84" ><input name="recurso" type="text" class="textbox" id="cedula22624" size="16"></td>
    <td width="56" >Id. Gripo </td>
    <td width="68"><input name="grupoid" type="text" class="textbox" id="grupoid" size="10"></td>
    <td width="47">Grupo</td>
    <td width="115"><input name="grupo" type="text" class="textbox" id="grupo" size="18">    </td>
    <td width="48">Nombre</td>
    <td width="280"><input name="nombre" type="text" class="textbox" id="nombre" size="30"></td>
  </tr>

  <tr class="fila">
    <td width="47" height="30"><p>Estado Equipo </p></td>
    <td width="84"><input name="estadoequipo" type="text" class="textbox" id="estadoequipo" size="10"></td>
    <td width="56">Localizaci&oacute;n</td>
    <td colspan="5"><input name="localizacion" type="text" class="textbox" id="localizacion" size="20">
    </td>
	</tr>
</table>
    </td>
  </tr>
</table>
<table width="850" border="2" align="center">
  <tr>
    <td  height="63">
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="22"  class="subtitulo1">Referencias</td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center"  >
  <tr class="fila">
    <td height="21" style="cursor:hand " class="fila" onClick="agregarReferencia('<%=CONTROLLER%>')">Agregar referencias a este vehiculo</td>
    </tr>
</table>
    </td>
  </tr>
</table>
<br>
<table width="797" border="0" align="center">
  <tr>
    <td align="center">         
      <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="validarPlacaInsert('<%=CONTROLLER%>','<%=BASEURL%>','<%=request.getParameter("verificacion")%>')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="frmplaca.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">      </td>
    </tr>
</table>
<%if(!msg.equals("")){%>
	<p>
   <table width="407" border="2" align="center">
     <tr>
    <td><table width="100%"   align="center"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58" bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
   <br>
   <%if(request.getParameter("clave")!=null){%>
   <table width="50%" border="2" align="center">
     <tr>
       <td><table width="100%"   align="center" cellpadding="3" cellspacing="2"  class="fila">
           <tr >
             <td width="52%" height="22" class="subtitulo1" ><div align="center" class=>
                 <div align="left"><strong> Solicitud de Clave</strong></div>
             </div></td>
             <td width="48%" height="22" class="barratitulo" ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
           </tr>
           <tr class="fila">
             <td>INGRESE SU CLAVE: </td>
             <td><input name="nclave" type="text" class="textbox" id="nclave"></td>
           </tr>
           <tr class="fila">
             <td><div align="center" class="fila" >
                 <div align="left">INGRESE SU NUMERO DE SOLICITUD:</div>
             </div></td>
             <td><input name="numsol" type="text" class="textbox" id="numsol"></td>
           </tr>
           <tr>
             <td colspan="2"><div align="center"> <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="frmplaca.action='<%=CONTROLLER%>?estado=Validar&accion=ClavePlacaProp';frmplaca.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div></td>
           </tr>
       </table></td>
     </tr>
   </table>
   <%}%>
</form><br>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</iframe>
</html>
<script>
function agregarReferencia(CONTROLLER) {
	if(frmplaca.placa.value==""){
		alert("Debe digitar una placa");
		frmplaca.placa.focus();
	} else {
		window.open(CONTROLLER+"?estado=Referencia&accion=Insert&tiporeferencia=EA&opcion=LANZAR&documento="+frmplaca.placa.value,'Trafico','width=700,height=350,scrollbars=yes,resizable=yes,top=10,left=65');
	}
}
</script>
