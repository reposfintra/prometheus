<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado De Consulta Historial Viajes</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Placa_conductor&accion=Serch&exportar=true">
  <%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	Planilla pla = model.planillaService.getPla();
	Vector vec = model.planillaService.ReporteVector();
        String total = (request.getParameter("total")!=null)?request.getParameter("total").toString():"";
        Planilla p;
	if ( vec.size() >0 ){  
%>
<table width="1400" border="2" align="center">
     <tr>
     <td>No Viajes: &nbsp;<%=total%></td>
     </tr>
    <tr>
      <td>
	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Datos Consulta Viajes </td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
		  <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
          <tr class="tblTitulo" align="center">
          <td width="5%" height="43" align="center">Estado Despacho</td>
          <td width="4%"  align="center">Planilla</td>
          <td width="5%" align="center">Estado Remesa </td>
          <td width="5%" align="center">Remesa</td>
          <td width="6%" align="center">Fecha</td>
          <td width="13%" align="center">Nombre Conductor </td>
          <td width="9%" align="center">Agencia Despacho </td>
          <td width="9%" align="center">Origen Planilla</td>
          <td width="9%" align="center">Destino Planilla </td>
          <td width="17%" align="center">Cliente</td>
          <td width="9%" align="center">Origen Remesa </td>
          <td width="9%" align="center">Destino Remesa </td>
          </tr>
        <pg:pager
         items="<%=vec.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
    
    <pg:param name="total" value="<%=total%>"/>
        <%-- keep track of preference --%>
        <%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++)
	  {
          p = (Planilla) vec.elementAt(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" onClick="window.open('<%=CONTROLLER%>?estado=ConsultaOCNormal&accion=Buscar&tipo=1&distrito=TSP&numeroOC=<%=p.getNumpla()%>&numeroOT=<%=p.getNumrem()%>','P','status=yes,scrollbars=no,width=800,height=600,resizable=yes')">
          <td width="5%" align="center" class="bordereporte">&nbsp;<%=p.getReg_status()%></td>
          <td width="4%" align="center" class="bordereporte"><%=p.getNumpla()%></td>  
          <td width="5%" align="center" class="bordereporte">&nbsp;<%=p.getStatus_220()%></td>
          <td width="5%" align="center" class="bordereporte"><%=p.getNumrem()%></td>
          <td width="6%" align="center" class="bordereporte"><%=p.getFecdsp()%></td>
          <td width="13%" align="center" class="bordereporte"><%=p.getNomCond()%></td>
          <td width="9%" align="center" class="bordereporte"><%=p.getAgcpla()%></td>
          <td width="9%" align="center" class="bordereporte"><%=p.getOripla()%></td>
          <td width="9%" align="center" class="bordereporte"><%=p.getDespla()%></td>
          <td width="17%" align="center" class="bordereporte"><%=p.getNitpro()%></td>
          <td width="9%" align="center" class="bordereporte"><%=p.getNomori()%></td>
          <td width="9%" align="center" class="bordereporte"><%=p.getNomdest()%></td>
        </tr>
        </pg:item>
        <%}%>
        <tr class="pie" align="center">
          <td td height="20" colspan="12" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
<br>
      <%}
 else { %>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
<%}%>
   <table border="0" align="center">
    <tr>
      <td>
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/placas&pagina=PlacaBuscar.jsp&titulo=Consulta Viajes'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	  </td>
	  <td>
          <img src="<%=BASEURL%>/images/botones/iconos/excel.jpg"  name="imgaceptar" onClick="forma.submit();"  style="cursor:hand" height="30">
	  </td>
    </tr>
</table>
</form>
</body>
</html>
