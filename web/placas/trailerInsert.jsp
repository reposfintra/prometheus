<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page errorPage="../../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input"%>

<%! public String reset(String value){
      return value!=null? value : "";
    }
%>


<%String placa ="", conductor="";
	if(request.getParameter("placa")!=null){
		placa = request.getParameter("placa");
		conductor = request.getParameter("conductor");
	}
	if(request.getParameter("conductor")!=null){
		
	}
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String control = session.getAttribute("USRIDEN")!=null?(String) session.getAttribute("USRIDEN"):"";
%>

<html>
<head>
<title>Insertar Trailer</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script>  


</head>

<body onResize="redimensionar();" onLoad="document.frmplaca.placa.focus();redimensionar();<%if(request.getParameter("mensajeAut")!=null){%>alert('<%=request.getParameter("mensajeAut")%>');<%}%>">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Placa Trailer"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%//nuevo   
        String tipdoc="012"; 
		String act="005";
		String hoy          = Utility.getHoy("-");
		int ano = Integer.parseInt(hoy.substring(0,4));
		//*****
%>

<form name="frmplaca" action="<%=CONTROLLER%>?estado=Placa&accion=Insert&cmd=show" method="post">   
   
  <table border="2" align="center" width="950">
  <tr>
    <td>

<table width="99%"  align="center"   >
  <tr>
    <td width="50%" height="24"  class="subtitulo1"><p align="left">Caracteristicas del vehiculo </p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
<tr class="fila">
    <td height="21" colspan="8" align="right" style="cursor:hand "  class="filaresaltada" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>','','<%=act%>','031')">Agregar imagen del vehiculo</span></td>
    </tr>
  <tr class="fila">
    <td width="62" height="30">Placa</td>
    <td width="83" ><input name="placa" type="text" class="textbox" id="placa" size="8" maxlength="12" value="<%=request.getParameter("placa")!=null?request.getParameter("placa"):""%>">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="30" >Tipo</td>
    <td width="269" nowrap><% TreeMap tipos = model.placaService.obtenerTreeMapLista( model.tablaGenService.getTipo() ); %>
        <input:select name="tipo" options="<%=tipos%>" attributesText="style='width:90%;' class='listmenu'" default="<%=reset(request.getParameter("tipo"))%>"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="57">Modelo</td>
    <td width="153" nowrap><select name="modelo" class="textbox" id="modelo">
  			          <%for(int i = 1900; i<=ano; i++){%>
  			                  <option value="<%=i%>"><%=i%></option>
  			        <%}%>
					</select>
					
					<script>frmplaca.modelo.value='<%=reset(request.getParameter("modelo"))%>'</script>
					
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="46">Clase</td>
    <td width="193" nowrap><% TreeMap clases = model.placaService.obtenerTreeMapLista( model.tablaGenService.getClase() ); %>
        <input:select name="clase" options="<%=clases%>" attributesText="style='width:90%;' class='listmenu'" default="<%=reset(request.getParameter("clase"))%>"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="61" height="30">Marca</td>
    <td width="391"  nowrap><% TreeMap marcas = model.marcaService.listar(); 
         %>
        <input:select name="marca" options="<%=marcas%>" attributesText="style='width:90%;' class='listmenu'" default="<%=reset(request.getParameter("marca"))%>"/>
		 <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="61">Color</td>
    <td width="396" nowrap><% TreeMap colores = model.colorService.listar();
         %>
      <input:select name="color" options="<%=colores%>" attributesText="style='width:65%;' class='listmenu'" default="<%=reset(request.getParameter("color"))%>"/> 
	  <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
  </tr>
  <tr class="fila">
    <td width="61" height="30">Carroceria</td>
    <td ><% TreeMap carrs = model.carrService.listar();%>
        <input:select name="carroceria" attributesText="style='width:90%;' class='listmenu'" options="<%=carrs%>" default="<%=reset(request.getParameter("carroceria"))%>"/>
		 <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">        	</td>
    <td >No. Rin</td>
    <td ><select name="norin" class="textbox"  id="norin" style='width:90%;'>
      <%
                      LinkedList tblrin = model.tablaGenService.getRin();
  			          for(int i = 0; i<tblrin.size(); i++){
  			                  TablaGen tblr = (TablaGen) tblrin.get(i);	%>
      <option value="<%=tblr.getTable_code()%>"><%=tblr.getDescripcion()%></option>
      <%}%>
    </select>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
	  
	  <script>frmplaca.norin.value = "<%=reset(request.getParameter("norin"))%>"</script>
	  
    </tr>
	<tr class="fila">
		<td>Grupo</td>
		<td><% TreeMap grupoeq = model.placaService.obtenerTreeMapLista( model.tablaGenService.getGrupoid() ); %>
        <input:select name="grupoid" attributesText="style='width:80%;' class='listmenu'" options="<%=grupoeq%>" default="<%=reset(request.getParameter("grupoid"))%>"/><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
	    <td>Recurso</td>
	    <td><input name="recurso" type="text" class="textbox" id="cedula22624" size="16" maxlength="10" value="<%=reset(request.getParameter("recurso"))%>"></td>
	</tr>	
</table>
</td>
</tr>
</table>
<input type='hidden' name='uservetonit' id='uservetonit' value='<%=control%>'>
<%if(control.equals("S")){%>

<table border="2" align="center" width="950">
  <tr>
    <td>
        <table width="99%"  align="center"   >
            <tr>
                <td width="392" height="24"  class="subtitulo1"><p align="left">Informaci&oacute;n del Reporte </p></td>
                <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
        </table>
        <table width="99%" align="center">
        <% TreeMap ca = model.tablaGenService.BuscarDatosTreemap("CAUSAVETO"); %>
        <% TreeMap fu = model.tablaGenService.BuscarDatosTreemap("FUENTEREP"); %>
            <tr class="fila">
                <td width="15%">Veto </td>
                <td colspan="3">
                    <select name="veto" onchange="enableFila2()">
                        <option value="S">Si</option>
						<option value="N">No</option>                        
                    </select>
                </td>
            </tr>    
            <tr id="fila_razon" class="fila">
                 <td width="15%">Causa</td>
                 <td width="30%">
                    <input:select name="causa" options="<%=ca%>" attributesText="style='width:80%' class='textbox' "/>
                    <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                 <td width="15%">Observaci&oacute;n</td>
                 <td width="40%">
                    <input name="observacion" value="" size='65%'>           
                    <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>    
            </tr>
            <tr id="fila_razon1" class="fila">
                 <td>Fuente </td>
                 <td colspan='3'><input:select name="fuente" options="<%=fu%>" attributesText="style='width:35%' class='textbox' "/></td>
            </tr>
         </table>
     </td>
  </tr>
</table>

<%}%>


<table width="950" border="2" align="center">
  <tr>
    <td>
<table width="99%"   align="center" bgcolor="#FFFFFF" bordercolor="#F7F5F4"> 
  <tr>
    <td width="50%" height="24"  class="subtitulo1"><p align="left">Informaci&oacute;n del Propietario </p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="83" height="22">Agencia</td>
    <td width="153" ><% TreeMap agencia = model.agenciaService.listar(); 
         agencia.put("", "");%>
            <input:select name="agencia" attributesText="style='width:90%;' class='listmenu'" options="<%=agencia%>" default="<%=reset(request.getParameter("agencia"))%>"/><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="91" >Homologado</td>
    <td width="116"><input name="homologado" type="text" class="textbox" id="clase23" size="4" maxlength="1" value="<%=reset(request.getParameter("homologado"))%>"></td>
    <td width="72">Condici&oacute;n</td>
    <td width="386"><input name="condicion" type="text" class="textbox" id="tarhabilitacion4243" size="4" maxlength="1" value="<%=reset(request.getParameter("condicion"))%>"></td>
  </tr>
</table>
<table width="99%"  align="center"   >
  <tr class="fila">
    <td width="81" height="30">Tarjeta de Propiedad </td>
    <td width="149" ><input name="tarpropiedad" type="text" class="textbox" id="tarpropiedad" size="16" maxlength="15" value="<%=reset(request.getParameter("tarpropiedad"))%>">
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',1,'<%=act%>','014')" style="cursor:hand "> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="88" >Cedula Propietario</td>
    <td width="125"><input name="propietario" type="text" class="textbox" id="propietario" value="<%=reset(request.getParameter("propietario"))%>" size="16" maxlength="15">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="86">Tenedor</td>
    <td width="126"><input name="tenedor" type="text" class="textbox" id="tenedor"  value="<%=reset(request.getParameter("tenedor"))%>" size="15" maxlength="15"></td>
    <td width="78">A Titulo</td>
    <td width="160"><select name="atitulo" class="textbox"  id="atitulo">
		<option value="">Seleccione Un Item</option>
      <%
                    LinkedList tbltitu = model.tablaGenService.getTitulo();
  		    for(int i = 0; i<tbltitu.size(); i++){
  		            TablaGen tblt = (TablaGen) tbltitu.get(i);	%>
      <option value="<%=tblt.getTable_code()%>"><%=tblt.getDescripcion()%></option>
      <%}%>
    </select>
		<script>frmplaca.atitulo.value = '<%=reset(request.getParameter("atitulo"))%>'</script>
	</td>
  </tr>
</table>
</td>
</tr>
</table>
<table  border="2" align="center" width="950">
  <tr>
    <td>
	
<table width="99%"  align="center"   >
  <tr>
    <td width="50%" height="24"  class="subtitulo1"><p align="left">Dimensiones del Vehiculo </p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center"   >
  <tr class="fila">
    <td width="87">Capacidad</td>
    <td width="153" ><input name="capacidad" type="text" class="textbox" id="capacidad2" onkeypress="soloDigitos(event, 'decOK')" size="8" maxlength="8" value="<%=reset(request.getParameter("capacidad"))%>">
      Ton. <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="80" >Volumen</td>
    <td width="102"><input name="volumen" type="text" class="textbox" id="volumen" onkeypress="soloDigitos(event, 'decOK')" size="6" maxlength="15" value="<%=reset(request.getParameter("volumen"))%>"> mts&#179;<img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
    <td width="64">Ancho</td>
    <td width="100"><input name="ancho" type="text" class="textbox" id="ancho" onkeypress="soloDigitos(event, 'decOK')" size="4" maxlength="15" value="<%=reset(request.getParameter("ancho"))%>">
      mts <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="25">Largo</td>
    <td width="100"><input name="largo" type="text" class="textbox" id="largo" onkeypress="soloDigitos(event, 'decOK')" size="4" maxlength="15" value="<%=reset(request.getParameter("largo"))%>">
      mts <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="30">Alto</td>
    <td width="100"><input name="alto" type="text" class="textbox" id="alto" onkeypress="soloDigitos(event, 'decOK')" size="4" maxlength="15" value="<%=reset(request.getParameter("alto"))%>">
      mts <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
  </tr>
  <tr class="fila">
    <td width="87">Tara</td>
    <td width="153" ><input name="tara" type="text" class="textbox" id="tara" onkeypress="soloDigitos(event, 'decOK')" size="15" maxlength="15" value="<%=reset(request.getParameter("tara"))%>"> Ton. <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
    <td width="108" >Cargue</td>
    <td width="102"><input name="cargue" type="text" class="textbox" id="cargue2" onkeypress="soloDigitos(event, 'decOK')" size="15" maxlength="15" value="<%=reset(request.getParameter("cargue"))%>"> </td>
    <td width="64">Llantas</td>
    <td width="100"><input name="llantas" type="text" class="textbox" id="llanatas" size="10" maxlength="15" value="<%=reset(request.getParameter("llantas"))%>"></td>
    <td width="55">Piso</td>
    <td colspan="3"><select name="piso" class="textbox" id="piso">
  			          <%
                      LinkedList tblpiso = model.tablaGenService.getPiso();
  			          for(int i = 0; i<tblpiso.size(); i++){
  			                  TablaGen tblp = (TablaGen) tblpiso.get(i);	%>
  			                  <option value="<%=tblp.getTable_code()%>"><%=tblp.getDescripcion()%></option>
  			        <%}%>
					</select> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> 
					<script>frmplaca.piso.value = "<%=reset(request.getParameter("piso"))%>"</script>
					</td>
					
    </tr>
	
  <tr class="fila">
    <td width="87" height="30">Dimensi&oacute;n Carroceria </td>
    <td width="153"><input name="dimcarroceria" type="text" class="textbox" id="dimcarroceria" size="20" maxlength="20" value="<%=reset(request.getParameter("dimcarroceria"))%>"> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
    <td width="108">Numero de Ejes</td>
    <td width="102"><input name="noejes" type="text" class="textbox" id="noejes" onkeypress="return acceptNum(event)" size="8" maxlength="1" value="<%=reset(request.getParameter("noejes"))%>"> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
    </td>
    <td>Capacidad<br>
      Galones</td>
    <td><input name="capac_gal" type="text" class="textbox" id="capac_gal" onblur='ValidarCampo(this,10,3)' onKeyPress="soloDigitos(event, 'decOK')" size="10" maxlength="10" value="<%=reset(request.getParameter("capac_gal"))%>"></td>
    <td>Capacidad<br>
      mts&#179;      </td>
    <td colspan="3"><input name="capac_mts" type="text" class="textbox" id="capac_mts"  onblur='ValidarCampo(this,10,3)' onKeyPress="soloDigitos(event, 'decOK')" size="10" maxlength="10" value="<%=reset(request.getParameter("capac_mts"))%>"></td>
    </tr>
</table>
</td>
</tr>
</table>
<table width="950" border="2" align="center" class="tabla">
  <tr>
    <td>
      <table width="99%"  align="center">
        <tr>
          <td width="392" height="24"  class="subtitulo1">Descuentos Equipos </td>
          <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="99%" align="center" >
        <tr class="fila">
          <td width="127" height="22"><p>Descuento Equipo </p></td>
          <td width="332" ><select name="c_descuento" class="textbox" id="c_descuento">
            <option value="N">No</option>
            <option value="S">Si</option>
          </select>
            <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> 
			<script>frmplaca.c_descuento.value='<%=reset(request.getParameter("c_descuento"))%>'</script>
			
			</td>
          <td width="138" >Clasificaci&oacute;n Equipo</td>
          <td width="312"><select name="c_clasificacion" class="textbox" id="c_clasificacion">
  			          <%
                      LinkedList tblcla = model.tablaGenService.getClasificacion();
  			          for(int i = 0; i<tblcla.size(); i++){
  			                  TablaGen tblc = (TablaGen) tblcla.get(i);	%>
  			                  <option value="<%=tblc.getTable_code()%>"><%=tblc.getDescripcion()%></option>
  			        <%}%>
					</select> 
            <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			<script>frmplaca.c_clasificacion.value = "<%=reset(request.getParameter("c_clasificacion"))%>"</script>
			</td>
          </tr>
    </table></td>
  </tr>
</table>
<br>
<table width="797" border="0" align="center">
  <tr>
    <td align="center">         
      <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod" id="mod" height="21" onClick="validarPlacaTrailerInsert('<%=CONTROLLER%>','<%=BASEURL%>')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="visibility:visible;cursor:hand"> &nbsp; <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="resetear();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/placas&pagina=placaSeleccion.jsp&marco=no&tipo=I'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">     </td>
    </tr>
</table>
<%if(!msg.equals("")){%>
	<p>
   <table width="407" border="2" align="center">
     <tr>
    <td><table width="100%"   align="center"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58" bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
   <br>
   <input name="grupo_equipo" value="T" type="hidden">
</form><br>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</iframe>
</html>

<%if( msg.equals("Placa agregada exitosamente") ){%>
     <script>resetear();</script> 
<%}%>

<script>
    function noCambiar(){
        document.getElementById( 'causa' ).value = document.getElementById( 'causa_ant' ).value;
    }

</script>
