<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Autorizaciones.</title>
<link href="<%= BASEURL %>/css/estilotsp.css" rel='stylesheet'>
<link href="../../../css/estilotsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body>
<FORM name='forma' method='POST' action="<%=CONTROLLER%>?estado=Autorizar&accion=CambioPlacaProp" >
  <br>
  <table width="650" border="2"align="center">
    <tr>
      <td>
	  <table width="99%" border="1" align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF" >
        <tr >
          <td align="left" class="subtitulo1">SOLICITUD DE AUTORIZACI&Oacute;N</td>
          <td colspan="3" align="left" class="subtitulo1"><img src="<%=BASEURL%>/images/cuadrosverde.JPG" width="32" height="20" class="barratitulo"></td>
          </tr>
        <tr class="fila">
          <td width="178" align="left" > PLACA </td>
          <td colspan="3" valign="middle"><%=request.getParameter("placa")%>
            <input name="placa" type="hidden" id="placa" value="<%=request.getParameter("placa")%>">            </td>
          </tr>
        <tr class="fila" id="nombre">
          <td align="left" >Nuevo Propietario</td>
          <td colspan="3" valign="middle"><%=request.getParameter("nombreNProp")%> <input name="nombreNProp" type="hidden" id="nombreNProp" value="<%=request.getParameter("nombreNProp")%>"> 
          - <%=request.getParameter("NProp")%>
          <input name="NProp" type="hidden" id="NProp" value="<%=request.getParameter("NProp")%>"></td>
        </tr>
        <tr class="fila" id="nombre">
          <td align="left" >Propietario Actual </td>
          <td colspan="3" valign="middle"><%=request.getParameter("nombreVProp")%>
            <input name="nombreVProp" type="hidden" id="nombreVProp" value="<%=request.getParameter("nombreVProp")%>">
- <%=request.getParameter("VProp")%>
<input name="VProp" type="hidden" id="VProp" value="<%=request.getParameter("VProp")%>"></td>
        </tr>
        <tr class="fila">
          <td align="left" class="subtitulo1" >Solicitante</td>
          <td colspan="3" align="left" class="subtitulo1" ><img src="<%=BASEURL%>/images/cuadrosverde.JPG" width="32" height="20" class="barratitulo"></td>
          </tr>
        <tr class="fila">
          <td width="178" align="left" valign="top" >Despachador</td>
          <td width="168" valign="top"><%=request.getParameter("despachador")%>
            <input name="despachador" type="hidden" id="despachador" value="<%=request.getParameter("despachador")%>"> 
            <input name="email" type="hidden" id="email" value="<%=request.getParameter("email")%>"></td>
          <td width="113" valign="top">email</td>
          <td width="145" valign="top"><%=request.getParameter("email")%></td>
        </tr>
        <tr class="fila">
          <td width="178" align="left" valign="top" >Solicitud No. </td>
          <td colspan="3" valign="top"><%=request.getParameter("numsol")%>
            <input name="numsol" type="hidden" id="numsol" value="<%=request.getParameter("numsol")%>"></td>
        </tr>
		   <%if(request.getParameter("clave")!=null){%>
        <tr  class="mensajes">
          <td colspan="4" align="left" valign="top" >RECUERDE INFORMAR AL DESPACHADOR SOBRE ESTA CLAVE DE AUTORIZACION: <%=request.getParameter("clave")%></td>
          </tr>
		  <%}%>
 
        <tr class="pie">
          <td colspan="4"><div align="center">
              <input type="submit" class="boton" id="ingresar2" name="ingresar"  value="AUTORIZAR">
          </div></td>
        </tr>
      </table>	  </td>
    </tr>
  </table>
  <p>
</form>
</body>
</html>
