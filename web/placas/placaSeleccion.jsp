<!--
- Autor : Ing. Jose de la rosa
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que envia una planilla para buscar las discrepancias asociadas a esta
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Programa de Placas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<%-- Inicio Body --%>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Selecci�n programas de placas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%-- Inicio Formulario --%>
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Menu&accion=Cargar&opcion=8&carpeta=&marco=no&tipo=<%=request.getParameter("tipo")%>" >
	<%-- Tabla de los numeros de planilla --%>
	<table width="420" border="2" align="center">
		<tr>
			<td>
				<table width="100%" class="tablaInferior">
					<tr class="fila">
					<td colspan="2" >
						<table width="100%"  border="0" cellspacing="1" cellpadding="0">
							<tr>
								<td width="50%" class="subtitulo1">&nbsp;Selecci&oacute;n de placas </td>
								<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
							</tr>
						</table>
					</td>
					</tr>
					<tr class="fila">
						<td width="37%" align="center">Seleccione el Tipo:</td>
						<td width="63%" align="center">
							Cabezote:<input name="grupo_equipo" type="radio" id="tipo" value="C" checked>
							Trailer:<input name="grupo_equipo" type="radio" id="tipo" value="T">
							Otros:<input name="grupo_equipo" type="radio" id="tipo" value="O">
						</td>
					</tr>					
				</table>
			</td>
		</tr>
	</table>
	<p>
		<div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Buscar una planilla" name="buscar"  onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>  
</form>
</div>
</body>
</html>