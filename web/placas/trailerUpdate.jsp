<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String control = session.getAttribute("USRIDEN")!=null?(String) session.getAttribute("USRIDEN"):"";
  String op = "";
  Veto vetoB = null;
  vetoB = model.vetoSvc.obtenerVeto();
  String causa = (vetoB.getCausa()!=null)?vetoB.getCausa():"";
  String ob = (vetoB.getObservacion()!=null)?vetoB.getObservacion():"";
  String ref = (vetoB.getReferencia()!=null)?vetoB.getReferencia():"";
  String desc = (vetoB.getDescripcion()!=null)?vetoB.getDescripcion():"";
  String fuent = (vetoB.getFuente()!=null)?vetoB.getFuente():"";
  System.out.println("CAUSA: "+causa);
  System.out.println("OB: "+ob);
  System.out.println("FUENTE: "+fuent);
  String mod = "";%>
<html>
<head>
<title>Modificar Placa Trailer</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script>  
</head>

<script>
 var controlador = '<%=CONTROLLER%>';
</script>


<body onResize="redimensionar();" onLoad="redimensionar();<%if(request.getParameter("mensajeAut")!=null){%>alert('<%=request.getParameter("mensajeAut")%>');<%}%>">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Placas Trailer"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%//nuevo   
        String tipdoc="012"; 
		String act="005";
		String clase = "";
		String dato = "";   
		String hoy          = Utility.getHoy("-");
		int ano = Integer.parseInt(hoy.substring(0,4));
		//*****
%>
<!--

<form name="frmbuscarplaca" action="<%=CONTROLLER%>?estado=Placa&accion=Buscar" method="post">
<table border="2" align="center" width="950">
  <tr>
    <td>
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Buscar Placa </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
<tr class="fila">
    <td width="164" height="27">Digite la placa a Modificar </td>
    <td width="624" ><input name="idplaca" id="idplaca" type="text" class="textbox" title="Numero de la placa" maxlength="10"> 
	<input name="grupo_equipo" value="T" type="hidden">
      <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgsalir"  height="21" align="absmiddle" style="cursor:hand" onClick="frmbuscarplaca.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
</tr>
</table>
</td>
</tr>
</table>
</form>

-->

<input:form name="frmplaca" action="controller?estado=Placa&accion=Update&cmd=show" method="post" bean="placa">
<table border="2" align="center" width="950">
  <tr>
    <td>
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Caracteristicas del vehiculo </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
<tr class="fila">
    <td height="21" colspan="9" align="right" style="cursor:hand "  class="Simulacion_Hiper" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>','','<%=act%>','031')">Agregar imagen del vehiculo</span></td>
    </tr>
<tr class="fila">
    
			<td height="22">Tipo</td>
			<td height="22"><% TreeMap grupos = model.placaService.obtenerTreeMapLista( model.tablaGenService.getGrupo() ); %>
        		<input:select name="grupo_equipo" options="<%=grupos%>" attributesText="style='width:93%;' class='listmenu' default='T' onchange='cargar_pagina( this, controlador )' "/></td>
			<td height="31">Grupo</td>
		<td width="198"><% TreeMap grupoeq = model.placaService.obtenerTreeMapLista( model.tablaGenService.getGrupoid() ); %>
                <input:select name="grupoid" attributesText="style='width:90%;' class='listmenu'" options="<%=grupoeq%>"/><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
			
                                        <%Placa placa = model.placaService.getPlaca();
			   		if ( (placa.getVeto().equals("S")) && (!ref.equals("S")) ) {
			        	clase = "class='filaroja'";
			   			dato = "Reportado";
					}%>
			   
			       <td height="22">Recurso</td>
                   <td height="22"><input:text name="recurso" attributesText="maxlength='10' size='16' class='textbox'"/></td>
                   <td height="22"></td>
              <td width="98" height="22" align="center" <%=clase%>><%=dato%></td>			  
			  
			  
			  		<%clase = "";
					dato = "";
			   		if ( placa.getAprobado().equals("N") ) {
			        	clase = "class='filaroja'";
			   			dato = "No Aprobado";
					}%>
			   
			  
			  <td width="115" height="22" align="center" <%=clase%>><%=dato%></td>

    
</tr>
  <tr class="fila">
    <td width="56" height="30">Placa</td>
    <td width="80" ><input:text name="placa" attributesText="maxlength='10' size='8' readonly class='textbox'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="37" >Tipo</td>
    <td width="198"><% TreeMap tipos = model.placaService.obtenerTreeMapLista( model.tablaGenService.getTipo() ); %> 
        <input:select name="tipo" options="<%=tipos%>" attributesText="style='width:93%;' class='listmenu'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="45">Modelo</td>
    <td width="131"><% TreeMap modelos = new TreeMap();
                for(int i = 1900; i<=ano; i++){
					modelos.put(""+i, ""+i); 
					modelos.put(""+i, ""+i);
				}
             %>        
     <input:select name="modelo" options="<%=modelos%>" attributesText="style='width:85%;' class='listmenu'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="55">Clase</td>
    <td colspan="2"><% TreeMap clases = model.placaService.obtenerTreeMapLista( model.tablaGenService.getClase() ); %>     
     <input:select name="clase" options="<%=clases%>" attributesText="style='width:85%;' class='listmenu'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="61" height="22">Marca</td>
    <td><% TreeMap marcas = model.marcaService.listar(); 
         %>
      <input:select name="marca" options="<%=marcas%>" attributesText="style='width:90%;' class='textbox'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="63">Color</td>
    <td width="395"><% TreeMap colores = model.colorService.listar();
         %>
      <input:select name="color" options="<%=colores%>" attributesText="style='width:82%;' class='listmenu'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
  </tr>
  <tr class="fila">
    <td width="61" height="30">Carroceria</td>
    <td><% TreeMap carrs = model.carrService.listar();%><input:select name="carroceria" attributesText="style='width:90%;' class='listmenu'" options="<%=carrs%>" default="SS"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td>No. Rin </td>
	<td><% TreeMap rin = model.placaService.obtenerTreeMapLista( model.tablaGenService.getRin() ); %>      
     <input:select name="numero_rin" options="<%=rin%>" attributesText="style='width:85%;' class='listmenu'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
	</tr>
</table>
</td>
</tr>
</table>
<input type='hidden' name='uservetonit' id='uservetonit' value='<%=control%>'>

<% TreeMap ca = model.tablaGenService.BuscarDatosTreemap("CAUSAVETO"); %>
<% TreeMap fu = model.tablaGenService.BuscarDatosTreemap("FUENTEREP"); %>
<%if(control.equals("S")){%>
<table border="2" align="center" width="950">
  <tr>
    <td>
        <table width="99%"  align="center"   >
            <tr>
                <td width="392" height="24"  class="subtitulo1"><p align="left">Informaci&oacute;n del Reporte </p></td>
                <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
        </table>
        <table width="99%" align="center">
            <tr class="fila">
                <td width="15%">Veto </td>
                <td colspan='3'>
                    <select name="veto" onclick="enableFila2()">
                        <option value="N" <%=!placa.getVeto().equals("S")?"selected":""%>  >No</option>
                        <option value="S" <%=placa.getVeto().equals("S")?"selected":""%>>Si</option>
                    </select>
                </td>
            </tr>    

            <%if(placa.getVeto().equals("S")){%>
			   <tr id="fila_razon" class="fila" style="display:block">

                 <td width="15%">Causa </td>
                 <td width="30%">
                    <input type='hidden' name='causa_cambio' id='causa_cambio' value='<%=causa%>'>
                    <input:select name="causa" options="<%=ca%>" attributesText="style='width:80%' class='textbox' disable='false'" default="<%=causa%>"/>
                    <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                 <td width="15%">Observaci&oacute;n </td>
                 <td width="40%">
                    <input type='hidden' name='observacion_cambio' id='observacion_cambio' value='<%=ob%>'>
                    <input name="observacion" value="<%=ob%>" size='65%'>           
                    <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>    
            	</tr>
				<tr id="fila_razon1" class="fila" style="display:block">
					 <td>Fuente </td>
						<input type='hidden' name='fuente_cambio' id='fuente_cambio' value='<%=causa%>'>
					 <td colspan='3'><input:select name="fuente" options="<%=fu%>" attributesText="style='width:35%' class='textbox' disable='false'" default="<%=fuent%>"/></td>
				</tr>
				
			<%}else{%>
				<tr id="fila_razon" class="fila" style="display:none">

                 <td width="15%">Causa </td>
                 <td width="30%">
                    <input type='hidden' name='causa_cambio' id='causa_cambio' value='<%=causa%>'>
                    <input:select name="causa" options="<%=ca%>" attributesText="style='width:80%' class='textbox' disable='true'" default="<%=causa%>"/>
                    <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                 <td width="15%">Observaci&oacute;n </td>
                 <td width="40%">
                    <input type='hidden' name='observacion_cambio' id='observacion_cambio' value='<%=ob%>'>
                    <input name="observacion" value="<%=ob%>" size='65%' disabled>           
                    <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>    
            	</tr>
				<tr id="fila_razon1" class="fila" style="display:none">
					 <td>Fuente </td>
						<input type='hidden' name='fuente_cambio' id='fuente_cambio' value='<%=causa%>'>
					 <td colspan='3'><input:select name="fuente" options="<%=fu%>" attributesText="style='width:35%' class='textbox' disable='true'" default="<%=fuent%>"/></td>
				</tr>			
			<%}%>
       </table>
     </td>
  </tr>
</table>            
<%}else{%>
            <%if (ref.equals("S")){%>
            <table border="2" align="center" width="950">
              <tr>
                <td>
                    <table width="99%"  align="center"   >
                        <tr>
                            <td width="392" height="24"  class="subtitulo1"><p align="left">Informaci&oacute;n del Reporte </p></td>
                            <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                    </table>
                    <table width="99%" align="center">
                        <tr class="fila">
                            <input type='hidden' name='veto' id='causa_cambio' value='<%=placa.getVeto()%>'>
                            <td width="15%">Causa </td>
                            <td colspan="3"><%=desc%></td>
                        </tr>
                                <input type='hidden' id='causa_ant' value='<%=causa%>'>
                                <input type='hidden' id='causa' value='<%=causa%>'>
                                <input type='hidden' name='observacion_cambio' id='observacion_cambio' value='<%=ob%>'>
                                <input name="observacion" value="<%=ob%>" type='hidden'>
                    </table>
              </td>
            </tr>
           </table>
         <%}else{%>
         <input type='hidden' name='veto' id='causa_cambio' value='<%=placa.getVeto()%>'>
         <input type='hidden' id='causa_ant' value='<%=causa%>'>
         <input type='hidden' name='observacion_cambio' id='observacion_cambio' value='<%=ob%>'>
         <input type='hidden' name='observacion' id='observacion_cambio' value='<%=ob%>'>
         <input type='hidden' id='causa' value='<%=causa%>'>
         <%}%>
<%}%>  


<table border="2" align="center" width="950">
  <tr>
    <td>
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Informaci&oacute;n del Propietario </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="80" height="22">Agencia</td>
    <td width="225" ><% TreeMap agencia = model.agenciaService.listar(); 
         agencia.put("", "");%>
      <input:select name="agencia" attributesText="style='width:90%;' class='listmenu'" options="<%=agencia%>" default="SM"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="88" >Homologado</td>
    <td width="152"><input:text name="homologado" attributesText="maxlength='1' size='13' class='textbox'"/></td>
    <td width="72">Condici&oacute;n</td>
    <td width="284"><input:text name="condicion" attributesText="maxlength='1' size='13' class='textbox'"/></td>
  </tr>
</table>
<table width="99%"  align="center"   >
  <tr class="fila">
    <td width="79" height="30" rowspan="2">Tarjeta de Propiedad </td>
    <td width="141" rowspan="2" ><input:text name="tarprop" attributesText="maxlength='15'  size='16' class='textbox'"/>&nbsp;<img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',1,'<%=act%>','014')" style="cursor:hand "> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="81" rowspan="2" >Cedula Propietario</td>
	<%
		
		Usuario usuario = (Usuario) session.getAttribute("Usuario");
		String agenciaP   = usuario.getId_agencia();						
	%>
	<%if(agenciaP.equals("OP")){%>
		<td width="145" nowrap><input:text name="propietario" attributesText="maxlength='15' size='16' class='textbox'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
		<input:hidden name="vprop" attributesText="maxlength='15' size='16' class='textbox'" /> </td>
	<%}else{%>
		<td width="145" nowrap><input:text name="propietario" attributesText="maxlength='15' readonly size='16' class='textbox'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
		<input:hidden name="vprop" attributesText="maxlength='15' size='16' class='textbox'" /> </td>		
	<%}%>
    
	
    <td width="77" rowspan="2">Tenedor</td>
    <td width="146" rowspan="2"><input:text name="tenedor" attributesText="maxlength='15'  size='17' class='textbox'"/></td>
    <td width="64" rowspan="2">A Titulo</td>
    <td width="160" rowspan="2" nowrap><% TreeMap atitulo = model.placaService.obtenerTreeMapLista( model.tablaGenService.getTitulo() ); %>      
     <input:select name="atitulo" options="<%=atitulo%>" attributesText="style='width:95%;' class='listmenu'"/></td>
  </tr>
  <tr class="fila">
    <%if(agenciaP.equals("OP")){%>
    	<td nowrap><a class="Simulacion_Hiper" style="cursor:hand "  onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=37&propietario=ok','Prop','height=350,width=650,scrollbars=yes,status=yes,resizable=yes')" >Buscar Propietario</a></td>
	<%}else{%>
		<td nowrap></td>
    <%}%>
  </tr>
</table>
</td>
</tr>
</table>
<table border="2" align="center" width="950">
  <tr>
    <td>
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Dimensiones del Vehiculo </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center"   >
  <tr class="fila">
    <td width="79">Capacidad</td>
    <td width="140" ><input:text name="capacidad" attributesText="maxlength='8' class='textbox' size='7' onkeypress=\"soloDigitos(event, 'decOK')\""/>Ton. <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="103" >Volumen</td>
    <td width="124"><input:text name="volumen" attributesText="maxlength='15' class='textbox' size='6' onkeypress=\"soloDigitos(event, 'decOK')\""/> mts&#179; <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="54">Ancho</td>
    <td width="103"><input:text name="ancho" attributesText="maxlength='15' class='textbox' size='4' onkeypress=\"soloDigitos(event, 'decOK')\""/>      mts <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="25">Largo</td>
    <td width="105"><input:text name="largo" attributesText="maxlength='15' class='textbox' size='4' onkeypress=\"soloDigitos(event, 'decOK')\""/>      mts <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="30">Alto</td>
    <td width="100"><input:text name="alto" attributesText="maxlength='15' class='textbox' size='4'  onkeypress=\"soloDigitos(event, 'decOK')\""/>      mts <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
  </tr>
  <tr class="fila">
    <td width="79">Tara</td>
    <td width="150" ><input:text name="tara" attributesText="maxlength='15' size='10' class='textbox' onkeypress=\"soloDigitos(event, 'decOK')\""/> Ton. <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="103" >Cargue</td>
    <td width="124"><input:text name="cargue" attributesText="maxlength='15' size='10' class='textbox' onkeypress=\"soloDigitos(event, 'decOK')\""/></td>
    <td width="54">Llantas</td>
    <td width="103"><input:text name="llantas" attributesText="maxlength='15' size='10' class='textbox'"/></td>
    <td width="49">Piso</td>
    <td colspan="3"><% TreeMap piso = model.placaService.obtenerTreeMapLista( model.tablaGenService.getPiso() ); %>      
     <input:select name="piso" options="<%=piso%>" attributesText="style='width:50%;' class='listmenu'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
  </tr>
  <tr class="fila">
    <td width="79" height="30">Dimensi&oacute;n Carroceria </td>
    <td width="150"><input:text name="dimcarroceria" attributesText="maxlength='20' size='16' class='textbox'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="103">Numero de Ejes</td>
    <td><input:text name="noejes" attributesText="maxlength='1' size='8' onkeypress='return acceptNum(event)' class='textbox'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td>Capacidad<br>
Galones</td>
    <td><input:text name="capac_gal" attributesText="class='textbox' onblur='ValidarCampo(this,10,3)' onKeyPress=\"soloDigitos(event, 'decOK')\" maxlength=\"10\""/></td>
    <td>Capacidad</td>
    <td colspan="3"><input:text name="capac_mts" attributesText="class='textbox' onblur='ValidarCampo(this,10,3)' onKeyPress=\"soloDigitos(event, 'decOK')\" maxlength=\"10\""/> mts&#179;</td>
    </tr>
</table>
</td>
</tr>
</table>
<table width="950" border="2" align="center" class="tabla">
  <tr>
    <td>
      <table width="99%"  align="center">
        <tr>
          <td width="392" height="24"  class="subtitulo1">Descuentos Equipos </td>
          <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="99%" align="center" >
        <tr class="fila">
          <td width="127" height="22"><p>Descuento Equipo </p></td>
          <td width="332" ><% TreeMap c_desc = new TreeMap();
		                      c_desc.put("NO","N");
					c_desc.put("SI","S");%>      
     			<input:select name="descuento_equipo" options="<%=c_desc%>" attributesText="style='width:25%;' class='listmenu'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td width="138" >Clasificaci&oacute;n Equipo</td>
          <td width="312"><% TreeMap clasificacion = model.placaService.obtenerTreeMapLista( model.tablaGenService.getClasificacion() ); %>      
     			<input:select name="clasificacion_equipo" options="<%=clasificacion%>" attributesText="style='width:45%;' class='listmenu'"/> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
    </table></td>
  </tr>
</table>
<br>
<table width="797"  align="center"   bordercolor="#F7F5F4">
  <tr>
    <td height="27"><p align="center">   
	   <img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod" id="mod" height="21" onclick="validarPlacaTrailerUpdate('<%=CONTROLLER%>','<%=BASEURL%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="visibility:visible;cursor:hand"> &nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=BASEURL%>/placas/placaBuscarSinTipo.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	   </p>    </td>
  </tr>
</table>
<%if(!msg.equals("")){%>
	<p>
   <table width="451" border="2" align="center">
     <tr>
    <td><table width="100%"   align="center"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
   <input:text name="grupos" attributesText="maxlength='10' style='visibility:hidden' size='10' class='textbox' VALUE='T'"/>
   <input:text name="reg_status" attributesText="maxlength='10' style='visibility:hidden' size='10' class='textbox'"/>
</input:form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</iframe>
</body>
</html>
<script>
    function noCambiar(){
        document.getElementById( 'causa' ).value = document.getElementById( 'causa_ant' ).value;
    }

</script>

