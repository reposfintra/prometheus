
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page errorPage="../../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@page import="com.tsp.util.*"%>
<%@page import="java.util.*" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input"%>
<%! public String reset(String value){
      return value!=null? value : "";
    }
%>

<%String placa ="", conductor="";
	if(request.getParameter("placa")!=null){
		placa = request.getParameter("placa");
		conductor = request.getParameter("conductor");
	}
	if(request.getParameter("conductor")!=null){
            
	}
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String control = session.getAttribute("USRIDEN")!=null?(String) session.getAttribute("USRIDEN"):"";
%>

<html>
<head>
<title>Ingresar Placa Otros</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/referencia.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 

</head>

<body onResize="redimensionar();" onLoad="document.frmplaca.placa.focus();redimensionar();<%if(request.getParameter("mensajeAut")!=null){%>alert('<%=request.getParameter("mensajeAut")%>');<%}%>">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Placa Otros"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%//nuevo   
                String tipdoc="012"; 
                String hoy1  = com.tsp.util.Utility.getDate(8).substring(0,10);
		String act="005";
		String hoy          = Utility.getHoy("-");
		int ano = Integer.parseInt(hoy.substring(0,4));
		//*****
%>

<form name="frmplaca" action="<%=CONTROLLER%>?estado=Placa&accion=Insert&cmd=show" method="post">   
  <input name="hoy" type="hidden" id="hoy" value="<%=hoy1%>"> 
  <table border="2" align="center" width="950">
  <tr>
    <td>

<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Caracteristicas del vehiculo </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
<tr class="fila">
    <td height="21" colspan="10" align="right" style="cursor:hand "  class="filaresaltada" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>','','<%=act%>','031')">Agregar imagen del vehiculo</span></td>
    </tr>
  <tr class="fila">
    <td width="45" height="30">Placa</td>
    <td width="87" ><input name="placa" type="text" class="textbox" id="placa" size="6" maxlength="8" value="<%=request.getParameter("placa")!=null?request.getParameter("placa"):""%>">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="45" >Tipo</td>
    <td width="155" nowrap><% TreeMap tipos = model.placaService.obtenerTreeMapLista( model.tablaGenService.getTipo() ); %>
        <input:select name="tipo" options="<%=tipos%>" attributesText="style='width:84%; class='listmenu'" default="<%=reset(request.getParameter("tipo"))%>"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="51">Chasis Numero </td>
    <td width="150" nowrap><input name="noChasis" type="text" class="textbox" id="clase26" size="22" maxlength="18" value='<%=reset(request.getParameter("noChasis"))%>'>      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="58">Clase</td>
    <td width="107" nowrap><% TreeMap clases = model.placaService.obtenerTreeMapLista( model.tablaGenService.getClase() ); %>
        <input:select name="clase" options="<%=clases%>" attributesText="style='width:86%;' class='listmenu' " default="<%=reset(request.getParameter("clase"))%>"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="94">Numero Rin </td>
    <td width="111"><select name="norin" class="textbox"  id="norin" style="width:86%">
  			          <%
                      LinkedList tblrin = model.tablaGenService.getRin();
					  System.out.println("--");
					  System.out.println("Pagina JSP Rin " + tblrin.size ());
  			          for(int i = 0; i<tblrin.size(); i++){
  			                  TablaGen tblr = (TablaGen) tblrin.get(i);	%>
  			                  <option value="<%=tblr.getTable_code()%>"><%=tblr.getDescripcion()%></option>
  			        <%}%>
					</select>
              <script> frmplaca.norin.value='<%=reset(request.getParameter("norin"))%>'; </script>
					
    </td>
  </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="61" height="30">Marca</td>
    <td width="212"  nowrap><% TreeMap marcas = model.marcaService.listar(); 
         %>
        <input:select name="marca" options="<%=marcas%>" attributesText="style='width:100%;' class='listmenu'" default="<%=reset(request.getParameter("marca"))%>"/></td>
    <td width="55" >Modelo </td>
    <td width="155" nowrap><select name="modelo" class="textbox" id="modelo">
						<option value="">Seleccione Un Item</option>
  			          <%for(int i = 1900; i<=ano; i++){%>
  			                  <option value="<%=i%>"><%=i%></option>
  			        <%}%>
					</select>
					
                                        <script> frmplaca.modelo.value="<%=reset(request.getParameter("modelo"))%>"; </script>
					
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="88">Color</td>
    <td width="231" nowrap><% TreeMap colores = model.colorService.listar();
         %>
      <input:select name="color" options="<%=colores%>" attributesText="style='width:50%;' class='listmenu'" default="<%=reset(request.getParameter("color"))%>"/> </td>
  </tr>
  <tr class="fila">
    <td width="61" height="30">Carroceria</td>
    <td width="212" ><% TreeMap carrs = model.carrService.listar();%>
        <input:select name="carroceria" attributesText="style='width:100%;' class='listmenu'" options="<%=carrs%>" default="<%=reset(request.getParameter("carroceria"))%>"/></td>
    <td width="55" >Placa Trailer</td>
    <td><input name="placatrailer" type="text" class="textbox" id="placatrailer" size="8" maxlength="12" value='<%=reset(request.getParameter("placatrailer"))%>'>        	</td>
    <td>Serial Motor</td>
    <td nowrap><input name="nomotor" type="text" class="textbox" maxlength="15" value='<%=reset(request.getParameter("nomotor"))%>'>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
  </tr>
 <tr class="fila">
  <td>Grupo</td>
	<td><% TreeMap grupoeq = model.placaService.obtenerTreeMapLista( model.tablaGenService.getGrupoid() ); %>
    <input:select name="grupoid" attributesText="style='width:90%;' class='listmenu'" options="<%=grupoeq%>" default="<%=reset(request.getParameter("grupoid"))%>"/><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>	
    <td>Recurso</td>
    <td><input name="recurso" type="text" class="textbox" id="cedula22624" size="16" maxlength="10" value="<%=reset(request.getParameter("recurso"))%>"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <script>
      frmplaca.grupoid.value='<%=reset(request.getParameter("grupoid"))%>';
  </script>  
</table>
</td>
</tr>
</table>
<input type='hidden' name='uservetonit' id='uservetonit' value='<%=control%>'>
<%if(control.equals("S")){%>

<table border="2" align="center" width="950">
  <tr>
    <td>
        <table width="99%"  align="center"   >
            <tr>
                <td width="392" height="24"  class="subtitulo1"><p align="left">Informaci&oacute;n del Reporte </p></td>
                <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
        </table>
        <table width="99%" align="center">
        <% TreeMap ca = model.tablaGenService.BuscarDatosTreemap("CAUSAVETO"); %>
        <% TreeMap fu = model.tablaGenService.BuscarDatosTreemap("FUENTEREP"); %>
            <tr class="fila">
                <td width="15%">Veto </td>
                <td colspan="3">
                    <select name="veto" onchange="enableFila2()">
                        <option value="S">Si</option>
						<option value="N">No</option>                        
                    </select>
                </td>
            </tr>    
            <tr id="fila_razon" class="fila">
                 <td width="15%">Causa</td>
                 <td width="30%">
                    <input:select name="causa" options="<%=ca%>" attributesText="style='width:80%' class='textbox' "/>
                    <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                 <td width="15%">Observaci&oacute;n</td>
                 <td width="40%">
                    <input name="observacion" value="" size='65%'>           
                    <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>    
            </tr>
            <tr id="fila_razon1" class="fila">
                 <td>Fuente </td>
                 <td colspan='3'><input:select name="fuente" options="<%=fu%>" attributesText="style='width:35%' class='textbox' "/></td>
            </tr>
         </table>
     </td>
  </tr>
</table>

<%}%>

<table width="950" border="2" align="center">
  <tr>
    <td>
<table width="99%"   align="center" bgcolor="#FFFFFF" bordercolor="#F7F5F4"> 
  <tr>
    <td width="393" height="24"  class="subtitulo1"><p align="left">Informaci&oacute;n del Propietario </p></td>
    <td width="414"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="56" height="22">Agencia</td>
    <td width="147" ><% TreeMap agencia = model.agenciaService.listar(); 
         agencia.put("", "");%>
            <input:select name="agencia" attributesText="style='width:90%;' class='listmenu'" options="<%=agencia%>" default="<%=reset(request.getParameter("agencia"))%>"/><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="67" >Homologado</td>
    <td width="30"><input name="homologado" type="text" class="textbox" id="clase23" size="4" maxlength="1" value='<%=reset(request.getParameter("homologado"))%>'></td>
    <td width="56">Condici&oacute;n</td>
    <td width="401"><input name="condicion" type="text" class="textbox" id="tarhabilitacion4243" size="4" maxlength="1" value='<%=reset(request.getParameter("condicion"))%>'></td>
  </tr>
</table>
<table width="99%"  align="center"   >
  <tr class="fila">
    <td width="77" height="30">Tarjeta de Operaci&oacute;n </td>
    <td width="125"><input name="tarjetaoper" type="text" class="textbox" id="cedula23" size="17" maxlength="15" value='<%=reset(request.getParameter("tarjetaoper"))%>'>      </td>
    <td width="79" >Fecha de Vencimiento </td>
    <td width="104"><input name="venctarjetaoper" type="text" class="textbox" id="habilitacion24" size="11" readonly value='<%=reset(request.getParameter("venctarjetaoper"))%>'>      
    <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venctarjetaoper);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" border="0" alt=""></a> </span> </td>
    <td width="90">Tarjeta Empresarial </td>
    <td width="135"><input name="tarEmpresa" type="text" class="textbox" id="cedula2224" size="16" maxlength="15" value='<%=reset(request.getParameter("tarEmpresa"))%>'>
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',9,'<%=act%>','018')" style="cursor:hand "></td>
    <td width="78">Fecha de vencimiento </td>
    <td width="101"><input name="venempresa" type="text" class="textbox" id="habilitacion2332" size="11" value='<%=reset(request.getParameter("venempresa"))%>'> 
    <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venempresa);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a>   
     </td>
  </tr>
  <tr class="fila">
    <td width="77" height="30">Tarjeta de Habilitaci&oacute;n </td>
    <td width="130"><input name="tarhabil" type="text" class="textbox" id="cedula226" size="16" maxlength="15" value='<%=reset(request.getParameter("tarhabil"))%>'>
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',11,'<%=act%>','015')" style="cursor:hand "></td>
    <td width="79" >Fecha de Vencimiento </td>
    <td width="124"><input name="venhabilitacion" type="text" class="textbox" id="habilitacion224" size="11" readonly value='<%=reset(request.getParameter("venhabilitacion"))%>'>      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venhabilitacion);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span></span>  </td>
    <td width="90">Registro nal de carga. </td>
    <td width="135"><input name="registro" type="text" class="textbox" id="registro" size="16" maxlength="15" value='<%=reset(request.getParameter("registro"))%>'>
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',7,'<%=act%>','020')" style="cursor:hand "></td>
    <td width="78">Fecha de vencimiento </td>
    <td width="101"><input name="venregistro" type="text" class="textbox" id="venregistro" size="11" readonly value='<%=reset(request.getParameter("venregistro"))%>'>
      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venregistro);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span> </td>
  </tr>           
  <tr class="fila">
    <td width="77" height="30">Tarjeta de Propiedad </td>
    <td width="130" ><input name="tarpropiedad" type="text" class="textbox" id="cedula227" size="16" maxlength="15" value='<%=reset(request.getParameter("tarpropiedad"))%>'>
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',1,'<%=act%>','014')" style="cursor:hand "></td>
    <td width="79" >Fecha de Vencimiento </td>
    <td width="104"><input name="venpropiedad" type="text" class="textbox" id="habilitacion225" size="11" readonly value='<%=reset(request.getParameter("venpropiedad"))%>'>        <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venpropiedad);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span></span></td>
    <td width="90">Poliza Andina </td>
    <td width="135"><input name="polizaandina" type="text" class="textbox" id="polizaandina" size="16" maxlength="15" value='<%=reset(request.getParameter("polizaandina"))%>'>
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',11,'<%=act%>','016')" style="cursor:hand "></td>
    <td width="78">Fecha de vencimiento </td>
    <td width="101"><input name="venpolizaandina" type="text" class="textbox" id="venpolizaandina" size="11" readonly="" value='<%=reset(request.getParameter("venpolizaandina"))%>'>
      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venpolizaandina);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span></td>
  </tr>
  <tr class="fila">
    <td width="77" height="30">Poliza SOAT </td>
    <td width="130" ><input name="soat" type="text" class="textbox" id="cedula228" size="16" maxlength="15" value='<%=reset(request.getParameter("soat"))%>'>
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',6,'<%=act%>','019')" style="cursor:hand "></td>
    <td width="79" >Fecha de Vencimiento </td>
    <td width="104"><input name="venseguroobliga" type="text" class="textbox" id="habilitacion226" size="11" readonly value='<%=reset(request.getParameter("venseguroobliga"))%>'>        </span><span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venseguroobliga);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span></td>
    <td width="90">Tenedor</td>
    <td width="130"><input name="tenedor" type="text" class="textbox" id="tenedor" size="15"  value="<%=reset(request.getParameter("tenedor"))%>">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="78">A Titulo</td>
    <td width="101"><select name="atitulo" class="textbox"  id="atitulo">
		<option value="">Seleccione Un Item</option>
      <%
                    LinkedList tbltitu = model.tablaGenService.getTitulo();
  		    for(int i = 0; i<tbltitu.size(); i++){
  		            TablaGen tblt = (TablaGen) tbltitu.get(i);	%>
      <option value="<%=tblt.getTable_code()%>"><%=tblt.getDescripcion()%></option>
      <%}%>
    </select>
    </td>
    
    <script>frmplaca.atitulo.value = '<%=reset(request.getParameter("atitulo"))%>'</script>
    
  </tr>
  <tr class="fila">
    <td width="77" height="30">Certificado de Emisi&oacute;n de Gases </td>
    <td width="130" ><input name="certgases" type="text" class="textbox" id="cedula2243" size="16" maxlength="15" value='<%=reset(request.getParameter("certgases"))%>'>
      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" onClick="AdjuntarDocPlaca('<%=CONTROLLER%>',5,'<%=act%>','017')" style="cursor:hand "></td>
    <td width="79" >Fecha de Vencimiento </td>
    <td><input name="fecvengases" type="text" class="textbox" id="habilitacion2243" size="11" readonly value='<%=reset(request.getParameter("fecvengases"))%>'>      </span><span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.fecvengases);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" border="0" alt=""></a></span></td>
		<td>Grado de riesgo</td>
		<td colspan="3"><input name="griesgo" type="text" class="textbox" id="griesgo" onkeypress="return acceptNum(event)" size="4" maxlength="2" value='<%=reset(request.getParameter("griesgo"))%>'></td>
	</tr>
  <tr class="fila">
    <td width="77" height="30">Empresa Afiliada</td>
    <td width="112" ><input name="empafiliada" type="text" class="textbox" id="cedula2243" size="18" maxlength="15" value='<%=reset(request.getParameter("empafiliada"))%>'></td>
    <td width="79" >Cedula Conductor</td>
    <td width="104"><input name="conductor" type="text" class="textbox" id="cedula222334" value="<%=reset(request.getParameter("conductor"))%>" size="13" maxlength="15">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="90">Cedula Propietario </td>
    <td colspan="3"><input name="propietario" type="text" class="textbox" id="propietario" value="<%=reset(request.getParameter("propietario"))%>" size="16" maxlength="15">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
	</tr>
</table>
</td>
</tr>
</table>
<table  border="2" align="center" width="950">
  <tr>
    <td>
	
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Dimensiones del Vehiculo </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center"   >
  <tr class="fila">
    <td width="62">Capacidad</td>
    <td width="102" ><input name="capacidad" type="text" class="textbox" id="capacidad2" onkeypress="soloDigitos(event, 'decOK')" size="7" maxlength="7" value="<%=reset(request.getParameter("capacidad"))%>">
      Ton. </td>
    <td width="44" >Volumen</td>
    <td width="100"><input name="volumen" type="text" class="textbox" id="volumen2" onkeypress="soloDigitos(event, 'decOK')" size="9" maxlength="15" value="<%=reset(request.getParameter("volumen"))%>">mts&#179;</td>
    <td width="59">Ancho</td>
    <td width="77"><input name="ancho" type="text" class="textbox" id="ancho" onkeypress="soloDigitos(event, 'decOK')" size="4" maxlength="15" value="<%=reset(request.getParameter("ancho"))%>">
      mts </td>
    <td width="33">Largo</td>
    <td width="80"><input name="largo" type="text" class="textbox" id="largo" onkeypress="soloDigitos(event, 'decOK')" size="4" maxlength="15" value="<%=reset(request.getParameter("largo"))%>">
      mts </td>
    <td width="28">Alto</td>
    <td width="171"><input name="alto" type="text" class="textbox" id="alto2" onkeypress="soloDigitos(event, 'decOK')" size="4" maxlength="15" value="<%=reset(request.getParameter("alto"))%>">
      mts </td>
  </tr>
  <tr class="fila">
    <td width="62">Tara</td>
    <td width="102" ><input name="tara" type="text" class="textbox" id="capacidad2" onkeypress="soloDigitos(event, 'decOK')" size="14" maxlength="15" value="<%=reset(request.getParameter("tara"))%>">Ton.</td>
    <td width="44" >Cargue</td>
    <td width="77"><input name="cargue" type="text" class="textbox" id="cargue2" onkeypress="soloDigitos(event, 'decOK')" size="10" maxlength="15" value="<%=reset(request.getParameter("cargue"))%>"></td>
    <td width="59">Llantas</td>
    <td width="77"><input name="llantas" type="text" class="textbox" id="llanatas" size="10" maxlength="15" value="<%=reset(request.getParameter("llantas"))%>"></td>
    <td width="33">Piso</td>
    <td colspan="3"><select name="piso" class="textbox" id="piso">
						<option value="">Seleccione Un Item</option>
  			          <%
                      LinkedList tblpiso = model.tablaGenService.getPiso();
  			          for(int i = 0; i<tblpiso.size(); i++){
  			                  TablaGen tblp = (TablaGen) tblpiso.get(i);	%>
  			                  <option value="<%=tblp.getTable_code()%>"><%=tblp.getDescripcion()%></option>
  			        <%}%>
					</select>
					<script>frmplaca.piso.value = '<%=reset(request.getParameter("piso"))%>';</script>
					
					</td>
    </tr>
  <tr class="fila">
    <td width="62" height="30">Dimensi&oacute;n Carroceria </td>
    <td width="102"><input name="dimcarroceria" type="text" class="textbox" id="dimcarroceria" size="20" maxlength="20" value="<%=reset(request.getParameter("dimcarroceria"))%>"></td>
    <td width="44">Numero de Ejes</td>
    <td width="77"><input name="noejes" type="text" class="textbox" id="noejes" onkeypress="return acceptNum(event)" size="8" maxlength="1" value="<%=reset(request.getParameter("noejes"))%>">
    </td>
    <td width="59">Capacidad Trailer </td>
    <td colspan="5"><input name="captrailer" type="text" onkeypress="soloDigitos(event, 'decOK')" class="textbox"  id="captrailer" size="7" maxlength="7" value="<%=reset(request.getParameter("captrailer"))%>">
      <input name="capac_gal" type="hidden" id="capac_gal" value="0">
      <input name="capac_mts" type="hidden" id="capac_mts" value="0"></td>	
	    </tr>
</table>
</td>
</tr>
</table>

<table width="950" border="2" align="center">
  <tr>
    <td  height="63">
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="22"  class="subtitulo1">Referencias</td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center"  >
  <tr class="fila">
    <td height="21" style="cursor:hand " class="fila" onClick="ReferenciaPlaca('<%=CONTROLLER%>','1')">Agregar referencias a este vehiculo</td>
    </tr>
</table>
    </td>
  </tr>
</table>
<br>
<table width="797" border="0" align="center">
  <tr>
    <td align="center">         
      <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod" id="mod" height="21" onClick="validarPlacaInsert('<%=CONTROLLER%>','<%=BASEURL%>')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="visibility:visible;cursor:hand"> &nbsp; <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="resetear();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/placas&pagina=placaSeleccion.jsp&marco=no&tipo=I'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">      </td>
    </tr>
</table>
<%if(!msg.equals("")){%>
	<p>
   <table width="407" border="2" align="center">
     <tr>
    <td><table width="100%"   align="center"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58" bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
   <br>
   <input name="grupo_equipo" value="O" type="hidden">
</form><br>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</iframe>
</html>

<%if( msg.equals("Placa agregada exitosamente") ){%>
     <script>resetear();</script> 
<%}%>

<script>
    function noCambiar(){
        document.getElementById( 'causa' ).value = document.getElementById( 'causa_ant' ).value;
    }

</script>

<script>
    function validaFechas(){
		var fecha1 = document.frmplaca.venempresa.value;
		var fecha2 = Date();
                alert(fecha2);
		var fech1 = parseFloat(fecha1);
		var fech2 = parseFloat(fecha2);
		if(fech2>fech1) {     
			alert('Ojo, La fecha ingresada esta vencida');
		}
		
	}
</script>
