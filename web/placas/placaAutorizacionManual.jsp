<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Autorizaciones.</title>
<link href="<%= BASEURL %>/css/estilotsp.css" rel='stylesheet'>
<link href="../../../css/estilotsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body>
<FORM name='forma' method='POST' action="<%=CONTROLLER%>?estado=Autorizar&accion=CambioPlacaProp" >
  <br>
  <table width="650" border="2"align="center">
    <tr>
      <td>
	  <table width="99%" border="1" align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF" >
        <tr >
          <td align="left" class="subtitulo1">SOLICITUD DE AUTORIZACI&Oacute;N</td>
          <td width="426" colspan="3" align="left" class="subtitulo1"><img src="<%=BASEURL%>/images/cuadrosverde.JPG" width="32" height="20" class="barratitulo">
            <input name="manual" type="hidden" id="manual" value="yes"></td>
          </tr>
        <tr class="fila">
          <td width="178" align="left" > PLACA </td>
          <td colspan="3" valign="middle">
            <input name="placa" type="text" id="placa" value="">            </td>
          </tr>
        <tr class="fila" id="nombre">
          <td align="left" >Nuevo Propietario</td>
          <td colspan="3" valign="middle">            <input name="NProp" type="text" id="NProp" value=""></td>
        </tr>
        <tr class="fila" id="nombre">
          <td align="left" >Propietario Actual </td>
          <td colspan="3" valign="middle">
            <input name="VProp" type="text" id="VProp" value=""></td>
        </tr>
        <tr class="fila">
          <td align="left" class="subtitulo1" >Solicitante</td>
          <td colspan="3" align="left" class="subtitulo1" ><img src="<%=BASEURL%>/images/cuadrosverde.JPG" width="32" height="20" class="barratitulo"></td>
          </tr>
        <tr class="fila">
          <td width="178" align="left" valign="top" >Solicitud No. </td>
          <td colspan="3" valign="top">
            <input name="numsol" type="text" id="numsol" value=""></td>
        </tr>
		   <%if(request.getParameter("clave")!=null){%>
        <tr  class="mensajes">
          <td colspan="4" align="left" valign="top" >RECUERDE INFORMAR AL DESPACHADOR SOBRE ESTA CLAVE DE AUTORIZACION: <%=request.getParameter("clave")%></td>
          </tr>
		  <%}%>
 
        <tr class="pie">
          <td colspan="4"><div align="center">
              <input type="submit" class="boton" id="ingresar2" name="ingresar"  value="AUTORIZAR">
          </div></td>
        </tr>
      </table>	  </td>
    </tr>
  </table>
  <p>
</form>
</body>
</html>
