<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
    <title>Informe Diario de Despacho</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <style type="text/css">
<!--
.Estilo1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 10px;
}
.Estilo2 {font-family: Verdana, Arial, Helvetica, sans-serif}
.Estilo3 {font-size: 12px}
.Estilo5 {font-size: 12}
.Estilo8 {font-size: 10px}
.Estilo9 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; }
-->
    </style>
</head>
<%response.setContentType("application/vnd.ms-excel");%>
<body bgcolor="#FFFFFF">
<div align="center">
  <form name="form1" method="post" action="">
    <div align="center">
      <%
	Informe inf = model.planillaService.getInforme();
	String fechai = inf.getFechai();
    String fechaf = inf.getFechaf();
	java.util.Date fecha_actual = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
    String fecha_hoy=s.format(fecha_actual);
	s = new SimpleDateFormat("HH:mm:ss");
	String hora_actual = s.format(fecha_actual);
	Usuario usuario = (Usuario) session.getAttribute("Usuario");
%>
    </div>
    <table width="95%"  border="1" align="center" cellpadding="0" cellspacing="0" class="letras2">
      <tr>
        <td colspan="6"><div align="center" class="Estilo1 Estilo8">FINTRAVALORES S.A </div></td>
        <td width="19%"><span class="Estilo9">Fecha <%=fecha_hoy%></span></td>
      </tr>
      <tr>
        <td colspan="6"><div align="center" class="Estilo1">INFORME DIARIO DE DESPACHO</div></td>
        <td class="Estilo9">Hora <%=hora_actual%></td>
      </tr>
      <tr>
        <td colspan="5"><span class="Estilo9">Informe de fecha : <%=fechai%></span></td>
        <td colspan="2"><span class="Estilo9">Hasta : <%=fechaf%></span></td>
      </tr>
	  <%//CLIENTE
 		model.planillaService.llenarClientes2(fechai, fechaf,usuario.getBase());
	  	Vector info = (Vector)model.planillaService.getClientes();
		int viajec = 0;
		int equipoc = 0;
		double tonc = 0;
		double antc =0;
		for(int i = 0; i<info.size();i++){
			String cliente = (String) info.elementAt(i); 
			String cv[] = cliente.split(";");
			String nombrec = cv[1];
			cliente = cv[0];
			%>
      <tr>
        <td colspan="7"><span class="Estilo9">CLIENTE: <%=nombrec%></span></td>
      </tr>
	   <%  model.planillaService.llenarRutas2(fechai,fechaf, cliente,usuario.getBase());	
				Vector rutas =model.planillaService.getRutas();
				
				for(int j = 0; j<rutas.size();j++){
				String ruta = (String) rutas.elementAt(j); 
				String rutavec[] = ruta.split(",");
				ruta = rutavec[1];
				String desc  = rutavec[0];
				double totalton = 0;
				double totalant = 0;
			%>
      <tr>
        <td colspan="7"><span class="Estilo9">RUTA: <%=desc%> </span></td>
      </tr>
      <tr class="Estilo1">
        <td height="15"><strong># REMISION</strong></td>
        <td><strong>PLACA</strong></td>
        <td width="17%"><strong>CONDUCTOR</strong></td>
        <td width="15%">PROPIETARIO</td>
        <td><strong>TONELAJE</strong></td>
        <td><strong>VLR ANTICIPO</strong></td>
        <td><strong>FECHA Y HORA</strong></td>
      </tr>
	  
	  <%
	  //AQUI VA UN FOR
	  	model.planillaService.llenarInforme2(fechai, fechaf, cliente,ruta,usuario.getBase());
		Vector datos = model.planillaService.getInformes();
		viajec = viajec + datos.size();
	  for(int k =0;k<datos.size();  k++){
			Informe dato = (Informe)datos.elementAt(k);
			totalton = totalton+dato.getTonelaje();
			totalant = totalant + dato.getAnticipo();
							
	 				
	%>
      <tr class="Estilo1">
        <td><%=dato.getRemision()%></td>
        <td><%=dato.getPlaca()%></td>
        <td><%if(dato.getConductor()!=null)%>
        <%=dato.getConductor()%></td>
        <td><%=dato.getNomprop()!=null?dato.getNomprop():""%></td>
        <td><%=Util.customFormat(dato.getTonelaje())%> T</td>
        <td><%=Util.customFormat(Util.redondear(dato.getAnticipo(),0))%></td>
        <td><%=dato.getFechaCump().substring(0,16)%></td>
      </tr>
	  <%//AQUI FINALIZA UN FOR
	  }
		tonc = tonc +totalton;
		antc = antc+totalant;
		 
	  %>
	  
      <tr class="Estilo1">
        <td colspan="7">&nbsp;</td>
      </tr>
      <tr class="Estilo1">
        <td>RUTA:</td>
        <td>Viajes -&gt;&gt;&gt; <%=datos.size()%></td>
        <td colspan="2">Equipos -&gt;&gt;&gt; <%=model.planillaService.buscarTotalEquipo2(fechai, fechaf, cliente, ruta,usuario.getBase())%></td>
        <td><%=totalton%>T</td>
        <td><%=Util.customFormat(totalant)%></td>
        <td>&nbsp;</td>
		<%equipoc = equipoc + model.planillaService.buscarTotalEquipo2(fechai, fechaf, cliente, ruta,usuario.getBase()); %>
      </tr>
	  <%}%>
      <tr class="Estilo1">
        <td colspan="7">&nbsp;</td>
      </tr>
      <tr class="Estilo1">
        <td width="14%"><span class="letras2 Estilo8 Estilo2">CLIENTE:</span></td>
        <td width="15%"><span class="letras2 Estilo8 Estilo2">Viajes -&gt;&gt;&gt; <%=viajec%> </span></td>
        <td colspan="2"><span class="letras2 Estilo8 Estilo2">Equipos -&gt;&gt;&gt; <%=model.planillaService.buscarTotalEquipo2(fechai, fechaf, usuario.getBase())%></span></td>
        <td width="9%"><span class="Estilo9"><%=Util.redondear(tonc,2)%>T</span></td>
        <td width="11%"><span class="Estilo9"><%=Util.customFormat(antc)%></span></td>
        <td>&nbsp;</td>
      </tr>
	  
      <%
  }//FIN POR 
  %>
    </table>
    <p align="center">&nbsp;</p>
  </form>
</div>
</body>
</html>
