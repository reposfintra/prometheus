<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Fechas Informes</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="<%=BASEURL%>/js/validar.js"></script>
    <link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
	<script src='<%=BASEURL%>/js/date-picker.js'></script>


</head>

<body>
<%
String accion = CONTROLLER+"?estado=Crear&accion=ArchivoNovedades";
%>
    <form name="form2" method="post" action="<%=accion%>" onSubmit="return validarFechasArch(this);">
<table width="650" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
        <tr bgcolor="#FFA928">
        <td colspan="2" nowrap><div align="center" class="letras"><strong>GENERAR ARCHIVO DE NOVEDADES </strong></div></td>
    </tr>
        <tr>
        <td width="264" nowrap><strong>FECHA CORTE </strong></td>
        <td width="370" nowrap><span class="comentario">
          <input name='fechai' type='text' id="fechai" style='width:120' value='' readonly>
          <a href="javascript:void(0)" onclick="jscript: show_calendar('fechai');" HIDEFOCUS><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a></span></td>
        </tr>
        <tr>
          <td nowrap><strong>PROYECTO</strong></td>
          <td nowrap><select name="base" id="base">
            <option value="spo">Sociedad Portuaria SM</option>
            <option value="cdc">Sociedad Portuaria BQ</option>
            <option value="pco">Puerto Prodeco</option>
          </select></td>
        </tr>
    </table>
    <div align="center"><br>
      <input type="submit" name="Submit" value="Generar">
      <br>
      <br>
	  <%if(request.getParameter("mensaje")!=null){%>
      <table width="65%"  border="0" cellspacing="0" cellpadding="0" class="letras">
        <tr>
          <td><strong>Nota: Archivo generado con exito... </strong></td>
        </tr>
      </table>
	  <%}%>
    </div>
    </form>

    
</body>
</html>
