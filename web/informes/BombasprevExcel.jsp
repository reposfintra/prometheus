<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
    <title>Informe Diario de Despacho</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
	    <script src="<%=BASEURL%>/js/validar.js"></script>


</head>

<body bgcolor="#FFFFFF">
<div align="center">
 <%
	Informe inf = model.planillaService.getInforme();
	String fechai = inf.getFechai();
    String fechaf = inf.getFechaf();
	java.util.Date fecha_actual = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
    String fecha_hoy=s.format(fecha_actual);
	s = new SimpleDateFormat("HH:mm:ss");
	String hora_actual = s.format(fecha_actual);
	 Vector info =model.planillaService.getInformes();
%>
</div>
<table width="95%"  border="0" align="center" cellpadding="0" cellspacing="0" class="letras2">
  <tr>
    <td width="100%" colspan="3"><div align="center">FINTRAVALORES S.A </div></td>
  </tr>
  <tr>
    <td colspan="3"><div align="center"></div></td>
  </tr>
  <tr>
    <td colspan="3"><div align="center">Informe de fecha : <%=fechai%> - <%=fechaf%></div></td>
  </tr>

  <tr>
    <td colspan="3"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%">
		  <%if(info.size()>0){%>
		  <table width="100%"  border="1" cellpadding="2" cellspacing="2" bordercolor="#999999" class="letras2">
            <tr>
              <td width="27%"><div align="center"><strong>PROVEEDOR</strong></div></td>
              <td colspan="2"><div align="center"><strong>PEAJES</strong></div></td>
              <td colspan="2"><div align="center"><strong>ACPM</strong></div></td>
              <td width="17%"><div align="center"><strong>EFECTIVO</strong></div></td>
              <td width="13%"><div align="center"><strong>TOTAL</strong></div></td>
            </tr>
            <tr>
              <td bordercolor="#FFFFFF"></td>
              <td width="7%"><div align="center">CANT </div></td>
              <td width="14%"><div align="center">VALOR</div></td>
              <td width="8%"><div align="center">CANT </div></td>
              <td width="14%"><div align="center">VALOR</div></td>
              <td></td>
              <td></td>
            </tr>
            <%Vector proveedores = model.planillaService.getProveedores();
				  for(int j = 0; j<proveedores.size();j++){
						Proveedores dato = (Proveedores)  proveedores.elementAt(j); 
						
			%>
			<tr>
              <td><%=dato.getNombre()%></td>
              <td><%=dato.getCantPeaje()%></td>
              <td><div align="right"><%=Util.customFormat(dato.getValorPeaje())%></div></td>
              <td><%=dato.getCantacpm()%></td>
              <td><div align="right"><%=Util.customFormat(dato.getValoracpm())%></div></td>
              <td><div align="right"><%=Util.customFormat(dato.getTotale())%></div></td>
              <td><div align="right"><%=Util.customFormat((dato.getValorPeaje()+dato.getValoracpm()+dato.getTotale()))%></div></td>
            </tr>
			<%}%>
          </table>
		  <%}%>
		  </td>
        </tr>
      </table>
    </td>
  </tr>

</table>
    
<p align="center">
  <input type="submit" name="Submit" value="Abrir en excel" onClick="abrirPagina('informes/BombasExcel.jsp','Excel');">
</p>
</body>
</html>
