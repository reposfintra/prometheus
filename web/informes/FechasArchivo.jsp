<!--
- Autor : Ing. Jose de la rosa
- Date  : 05 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite generar un archivo de novedades
--%>

<%-- Declaracion de librerias--%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Fechas Informes</title>
    <script src="<%=BASEURL%>/js/validar.js"></script>
	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script src='<%=BASEURL%>/js/date-picker.js'></script>


</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Fecha Informes"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
String accion = CONTROLLER+"?estado=Crear&accion=ArchivoNovedades";
%>
    <form name="form2" method="post" action="<%=accion%>" onSubmit="return validarFechasArch(this);">
		<table align="center" width="500" border="2">
			<tr>
				<td>	
					<TABLE width="100%" align="center"  class="tablaInferior">
						<tr class="fila">
							<td class="subtitulo1">&nbsp;Generar Archivo De Novedades</td>
							<td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
						<tr class="fila">
							<td width="231" nowrap>Fecha Corte </td>
							<td width="245" nowrap><span class="comentario">
								<input name='fechai' type='text' id="fechai" style='width:120' value='' readonly>
								<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechai);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a></td>
						</tr>
						<tr class="fila">
							<td nowrap>Proyecto</td>
							<td nowrap>
							<select name="base" id="base">
								<option value="spo">Sociedad Portuaria SM</option>
								<option value="cdc">Sociedad Portuaria BQ</option>
								<option value="pco">Puerto Prodeco</option>
							</select></td>
						</tr>
					</table>
				</td>
			</tr>
	  </table>			
		<p align="center">
			<input type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" title="Generar" style="cursor:hand" name="Aceptar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
		</p>
		<%if(request.getParameter("mensaje")!=null){%>
			<table border="2" align="center">
				<tr>
					<td>
						<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
							<tr>
								<td width="229" align="center" class="mensajes">Nota: Archivo generado con exito...</td>
								<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
								<td width="58">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>	  
		<%}%>
	</form>
</div>   
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe> 
</body>
</html>
