<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Generar Reporte</title>
 <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
 <link href="../css/estilostsp.css" rel='stylesheet'>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="<%=BASEURL%>/js/validar.js"></script>
    <script src="<%=BASEURL%>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
	<script src='<%=BASEURL%>/js/date-picker.js'></script>


</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Relacion de Egresos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
String accion = CONTROLLER+"?estado=Informe&accion=Generar";
if(request.getParameter("egreso")!=null){
	accion = CONTROLLER+"?estado=InformeEgreso&accion=Generar";
}%>
<br>
    <form name="form2" method="post" action="<%=accion%>" onSubmit="return validarFechas(this);">
<table border="2" align="center" width="377">
  <tr>
    <td  >
<table width="99%" align="center">
  <tr>
    <td width="48%" height="22"  class="subtitulo1">GENERAR INFORME</td>
    <td width="52%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>

<table width="99%" align="center" class="Letras">
        <tr class="fila">
        <td width="135" nowrap><strong>Fecha Inicial </strong></td>
        <td width="214" nowrap><span class="comentario">
          <input name='fechai' type='text' class="textbox" id="fechai" style='width:120' value='' readonly>
          <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechai);return false;" HIDEFOCUS></span></td>
        </tr>
    </table>
</td>
</tr>
</table>
    <div align="center"><br>
      <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21"  onclick="form2.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;  
<img src="<%=BASEURL%>/images/botones/salir.gif" name="mod"  height="21" onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
    </form>
</div>    
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
