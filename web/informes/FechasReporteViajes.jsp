<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Reportes Placa Viaje</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="<%=BASEURL%>/js/validar.js"></script>
    <link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
	<script src='<%=BASEURL%>/js/date-picker.js'></script>
	<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>


</head>
<%String accion = CONTROLLER+"?estado=ReporteViajes&accion=Generar";
if(request.getParameter("peso")!=null){
	accion = CONTROLLER+"?estado=ReporteViajesPesos&accion=Generar";
}
%>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reportes Placa Viaje"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
    <form name="form2" id="form2" method="post" action="<%=accion%>" onSubmit="return validarFechas(this);">
      <table width="500" border="2" align="center">
        <tr>
          <td><table width="100%"  border="0">
            <tr>
              <td width="48%" class="subtitulo1">Generear Reporte</td>
              <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
            <table width="100%" align="center">
            <tr class="fila">
              <td width="264" nowrap>Fecha Inicio </td>
              <td width="370" nowrap><span class="comentario">
                <input name='fechai' type='text' class="textbox" id="fechai3" style='width:120' value='' readonly>
                <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechai);return false;" HIDEFOCUS></span></td>
            </tr>
            <tr class="fila">
              <td nowrap>Fecha Fin</td>
              <td nowrap><span class="comentario">
                <input name='fechaf' type='text' class="textbox" id="fechaf4" style='width:120' value='' readonly>
                <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechaf);return false;" HIDEFOCUS></span></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <div align="center"><br>
      <input type="image" name="Submit" src="<%=BASEURL%>/images/botones/aceptar.gif"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
    </div>
    </form>

</div>   
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>     
</body>
</html>
