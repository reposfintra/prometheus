<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Reportes Placa Viaje</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="js/validar.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>

</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reportes Placa Viaje"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
    <form name="form2" method="post" action="<%=CONTROLLER%>?estado=Acpm&accion=Search&num=1">
 	
      <table width="95%" border="2" align="center">
        <tr>
          <td><table width="100%"  border="0">
            <tr>
              <td width="48%" class="subtitulo1"> Reporte de Viajes </td>
              <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
            <table width="100%"  border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
            <tr class="tblTitulo">
              <td width="139" nowrap align="center">CEDULA PROPIETARIO</td>
              <td nowrap align="center">NOMBRE</td>
              <td width="90" align="center">PLACA</td>
              <td width="96" nowrap align="center">FECHA VIAJE</td>
            </tr>
            <%Vector vec = model.planillaService.getReporte();
    	for(int i = 0; i<vec.size(); i++){
		Planilla pla = (Planilla) vec.elementAt(i);
		%>
            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
              <td width="139" nowrap align="center" class="bordereporte"><%=pla.getNitpro()%></td>
              <td width="383" nowrap align="center" class="bordereporte"><%=pla.getNomCond()%></td>
              <td nowrap align="center" class="bordereporte"><%=pla.getPlaveh()%></td>
              <td nowrap align="center" class="bordereporte"><%=pla.getFecdsp()%></td>
            </tr>
            <% }%>
          </table></td>
        </tr>
      </table>
    </form>

  </div>
</body>
</html>
