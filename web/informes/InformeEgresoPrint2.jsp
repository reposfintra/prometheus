<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
    <title>Informe Diario de Despacho</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="<%=BASEURL%>/js/validar.js"></script>
    <link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">

</head>

<body bgcolor="#FFFFFF" onLoad="window.print();window.location='<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=550'">
<%
	Informe inf = model.planillaService.getInforme();
	String fechai = inf.getFechai();
    String fechaf = inf.getFechaf();
	java.util.Date fecha_actual = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
    String fecha_hoy=s.format(fecha_actual);
	s = new SimpleDateFormat("HH:mm:ss");
	String hora_actual = s.format(fecha_actual);
%>
<table width="95%"  border="0" align="center" cellpadding="0" cellspacing="0" class="letras2">
  <tr>
    <td width="100%" colspan="3"><div align="center">FINTRAVALORES S.A </div></td>
  </tr>
  <tr>
    <td colspan="3"><div align="center">RELACION DIARIA DE EGRESOS </div></td>
  </tr>
  <tr>
    <td colspan="3"><div align="center">Informe de fecha : <%=fechai%> - <%=fechaf%></div></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="4"><table width="100%"  border="0" cellpadding="0" cellspacing="0" bordercolor="#999999" class="letras2">
              <tr>
                <td width="83" class="letras2"><div align="left">EGRESO</div></td>
                <td width="16" class="letras2">&nbsp;</td>
                <td width="140" class="letras2">REMISION</td>
                <td width="138" class="letras2">CEDULA</td>
                <td width="278" class="letras2"><div align="left">NOMBRE</div></td>
                <td width="178" class="letras2">CONCEPTO</td>
                <td width="16" bordercolor="#EBEBEB" class="letras2">&nbsp;</td>
                <td class="letras2"><div align="right">Valor Anticipo</div></td>
                <td class="letras2">&nbsp;</td>
              </tr>
              <%  Vector info =model.planillaService.getInformes();
				double total = 0;
				for(int j = 0; j<info.size();j++){
						Informe dato = (Informe) info.elementAt(j); 
						total = total +dato.getAnticipo();
				%>
              <tr>
                <td class="letras2"><div align="left"><%=dato.getEgreso()%></div></td>
                <td class="letras2">&nbsp;</td>
                <td class="letras2"><%=dato.getRemision()%></td>
                <td class="letras2"><%=dato.getCedula()%></td>
                <td class="letras2"><%if(dato.getConductor()!=null)%>
                    <%=dato.getConductor()%> </td>
                <td class="letras2"><div align="left">Anticipo Planilla </div></td>
                <td bordercolor="#EBEBEB" class="letras2">&nbsp;</td>
                <td width="129" class="letras2"><div align="right"><%=Util.customFormat(Util.redondear(dato.getAnticipo(),0))%></div></td>
                <td width="83" class="letras2">&nbsp;</td>
              </tr>
              <%}%>
          </table></td>
        </tr>
        <tr>
          <td width="61%">&nbsp;</td>
          <td width="19%">&nbsp;</td>
          <td width="12%"><div align="right">--------</div></td>
          <td width="8%">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td width="19%" class="letras2">Total General .. </td>
          <td class="letras2"><div align="right"><%=Util.customFormat(Util.redondear(total,0))%></div></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4">
            <%if(info.size()>0){%>
            <table width="100%"  border="1" cellpadding="2" cellspacing="2" bordercolor="#999999" class="letras2">
              <tr>
                <td width="27%"><div align="center"><strong>PROVEEDOR</strong></div></td>
                <td colspan="2"><div align="center"><strong>PEAJES</strong></div></td>
                <td colspan="2"><div align="center"><strong>ACPM</strong></div></td>
                <td width="17%"><div align="center"><strong>EFECTIVO</strong></div></td>
                <td width="13%"><div align="center"><strong>TOTAL</strong></div></td>
              </tr>
              <tr>
                <td bordercolor="#FFFFFF"></td>
                <td width="7%"><div align="center">CANT </div></td>
                <td width="14%"><div align="center">VALOR</div></td>
                <td width="8%"><div align="center">CANT </div></td>
                <td width="14%"><div align="center">VALOR</div></td>
                <td></td>
                <td></td>
              </tr>
              <%Vector proveedores = model.planillaService.getProveedores();
				  for(int j = 0; j<proveedores.size();j++){
						Proveedores dato = (Proveedores)  proveedores.elementAt(j); 
						
			%>
              <tr>
                <td><%=dato.getNombre()%></td>
                <td><%=dato.getCantPeaje()%></td>
                <td><div align="right"><%=Util.customFormat(dato.getValorPeaje())%></div></td>
                <td><%=dato.getCantacpm()%></td>
                <td><div align="right"><%=Util.customFormat(dato.getValoracpm())%></div></td>
                <td><div align="right"><%=Util.customFormat(dato.getTotale())%></div></td>
                <td><div align="right"><%=Util.customFormat((dato.getValorPeaje()+dato.getValoracpm()+dato.getTotale()))%></div></td>
              </tr>
              <%}%>
</table>
            <%}%>
          </td>
        </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
