<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Informe para Facturaci&oacute;n</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/letras.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#FFFFFF" onLoad="window.print();">
<div align="center">
  <form name="form1" method="post" action="">
    <div align="center">
      <%

        String fechai = (String) session.getAttribute("fechaiIFact");
        String fechaf = (String) session.getAttribute("fechafIFact");
        java.util.Date fecha_actual = new java.util.Date();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        String fecha_hoy = s.format(fecha_actual);
        s = new SimpleDateFormat("HH:mm:ss");
        String hora_actual = s.format(fecha_actual);
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        Vector informe = (Vector) session.getAttribute("resumenFact");
%>
</div>
    <table width="95%"  border="0" align="center" cellpadding="0" cellspacing="0" class="letras2">
      <tr>
        <td colspan="2"><div align="center">FINTRAVALORES S.A.</div></td>
        <td width="28%"><table width="100%"  border="0" class="letras2">
            <tr>
              <td width="22%">Fecha</td>
              <td width="4%">:</td>
              <td width="74%"><%=fecha_hoy%></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2"><div align="center">INFORME PARA FACTURACION </div></td>
        <td><table width="100%"  border="0" class="letras2">
            <tr>
              <td width="22%">Hora</td>
              <td width="4%">:</td>
              <td width="74%"><%=hora_actual%></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2"><div align="center"></div></td>
        <td width="28%">&nbsp;</td>
      </tr>
      <tr>
        <td width="45%">Informe de fecha : <%=fechai%></td>
        <td colspan="2">Hasta : <%=fechaf%></td>
      </tr>
<%//CLIENTE
        model.clienteService.searchCliente("000127");
        Cliente cli = model.clienteService.getCliente();      
%>
      <tr>
        <td colspan="3"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><table width="100%"  border="0" cellspacing="1" cellpadding="1">
                  <tr>
                    <td><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="letras2">
                        <tr>
                          <td width="10%">CLIENTE</td>
                          <td width="2%">:</td>
                          <td width="88%"><%= cli.getNomcli()%></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="100%">&nbsp;</td>
                        </tr>
                        <tr>
                          <td>
                           
                            <table border="0" cellpadding="1" cellspacing="1" class="letras2">
<%
        double tonelaje = 0;
        double tonelaje_ttl = 0;
        long viajes_ttl = 0;
        long viajes_d = 0;
        long viajes = 0;
		int  cols = 0;
        for( int i= 0; i< informe.size(); i++){
                Vector linea = (Vector) informe.elementAt(i);
                if(i==0) cols = linea.size();
                tonelaje = 0;
                viajes = 0;
%>
 <tr>                             

<%
                for( int j=0; j<linea.size(); j++){
                        String obj = (String) linea.elementAt(j);
                 		if( i!=0 && j!=0 )tonelaje += Double.parseDouble(obj);
%><td <%= (j==0? "nowrap width=80" : "width=100")%>> <%= (j!=0? "<div align=right>" + obj + "</div>" : obj)%></td>                              
<%                        
                 }
				 tonelaje_ttl += tonelaje;
%>
<td width="20">&nbsp;</td>
<td width=100><div align=right><%= i!=0? com.tsp.util.Util.redondear( tonelaje, 2) : "TOTAL (T)"%></div></td>
</tr>
<%
        }
		if( cols!=-1 ){
			double[] ar = new double[cols];
%>                      
		<tr>
			<td>TOTAL (T)</td>
<%
			for( int i= 0; i< ar.length; i++){
				ar[i] = 0;
			}
			for( int i= 1; i< informe.size(); i++){
                Vector linea = (Vector) informe.elementAt(i);
				for( int j=1; j<linea.size(); j++){
					String obj = (String) linea.elementAt(j);
					ar[j] += Double.parseDouble(obj);
				}
			}		
			for( int i= 1; i< ar.length; i++){
%>			
				<td width=100><div align="right"><%= com.tsp.util.Util.redondear( ar[i], 2)%></div></td>
<%
			}
%>
			<td width="20">&nbsp;</td>
			<td width=100><div align="right"><%= com.tsp.util.Util.redondear( tonelaje_ttl, 2) %></div></td>
		</tr>                 
<%
	}
%>
                          </table></td>
                        </tr>
                        
                        <tr>
                          <td> </td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="letras2"></td>
                        </tr>
                    </table></td>
                  </tr>
              </table></td>
            </tr>
        </table></td>
      </tr>

      <tr class="letras2">
        <td colspan="3">&nbsp;        </td>
      </tr>
    </table>
    </form>
</div>
</body>
</html>
