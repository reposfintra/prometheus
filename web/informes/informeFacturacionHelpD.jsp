<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion Campos Reemplazo Masiva de Tramos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Informe para Facturaci&oacute;n </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Informe para Facturaci&oacute;n </td>
        </tr>
        <tr>
          <td width="149" class="fila">Fecha Inicial </td>
          <td width="525"  class="ayudaHtmlTexto"> Campo para seleccionar la fecha inicial  del per&iacute;ododel informe para facturaci&oacute;n. </td>
        </tr>
        <tr>
          <td class="fila">Fecha Final </td>
          <td  class="ayudaHtmlTexto"> Campo para seleccionar la fecha inicial  del per&iacute;ododel informe para facturaci&oacute;n. </td>
        </tr>
        <tr>
          <td class="fila">Ruta</td>
          <td  class="ayudaHtmlTexto">Campo para seleccionar la ruta para generar el informe para facturaci&oacute;n. Se puede seleccionar la opci&oacute;n 'Todas las rutas' para generear el informe para todas las rutas,. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que valida el la informaci&oacute;n digitada en los campos para la generaci&oacute;n del informe. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n que cancela todas los operaciones realizadas, y resetea la pagina llev&aacute;ndola a su estado inicial. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cierra  la ventana. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
