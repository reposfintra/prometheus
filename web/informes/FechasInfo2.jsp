<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Modificar proveedor tiquetes</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="<%=BASEURL%>/js/validar.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
	<script src='<%=BASEURL%>/js/date-picker.js'></script>


</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Diario Despachos"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
String accion = CONTROLLER+"?estado=Informe2&accion=Generar";
if(request.getParameter("egreso")!=null){
	accion = CONTROLLER+"?estado=InformeEgreso2&accion=Generar";
}
%>
    <form name="form2" method="post" action="<%=accion%>" onSubmit="return validarFechas(this);">
<table width="650"  border="2" align="center">
  <tr>
    <td><table width="650" border="0" align="center" bordercolor="#Ebebeb" class="Letras">
      <tr>
        <td colspan="2" nowrap><div align="center" class="letras">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="22" colspan=2 class="subtitulo1"><strong>GENERAR INFORME</strong></td>
                <td width="376" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
              </tr>
            </table>
        </div></td>
      </tr>
      <tr class="fila">
        <td width="264" nowrap><strong>Fecha Inicial </strong></td>
        <td width="370" nowrap>
          <input name='fechai' type='text' id="fechai" style='width:120' value='' readonly>
          <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechai);return false;"  HIDEFOCUS ><img class="link" src="<%=BASEURL%>/js/Calendario/cal.gif" alt="Click para ver Calendario" width="16" height="16"></a> </td>
      </tr>
      <tr class="fila" >
        <td><strong>Fecha Final </strong></td>
        <td nowrap>
          <input name='fechaf' type='text' id="fechaf" style='width:120' value='' readonly>
          <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechaf);return false;"  HIDEFOCUS ><img class="link" src="<%=BASEURL%>/js/Calendario/cal.gif" alt="Click para ver Calendario" width="16" height="16"></a> </td>
      </tr>
    </table></td>
  </tr>
</table>
<div align="center"><br>
      <input type="submit" name="Submit" value="Generar">
    </div>
    </form>
	</div> 
   <iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_24.js" id="gToday:datetime:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
   
</body>
</html>
