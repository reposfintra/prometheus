<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Informe para Facturaci&oacute;n</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/letras.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#FFFFFF">
<div align="center">
  <form name="form1" method="post" action="">
    <div align="center">
      <%

        String fechai = (String) session.getAttribute("fechaiIFact");
        String fechaf = (String) session.getAttribute("fechafIFact");
        java.util.Date fecha_actual = new java.util.Date();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        String fecha_hoy = s.format(fecha_actual);
        s = new SimpleDateFormat("HH:mm:ss");
        String hora_actual = s.format(fecha_actual);
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        Vector informe = (Vector) session.getAttribute("InformeFact");
%>
	<div align="right"><%=datos[0]%></div><br>
    <img src="<%= BASEURL%>/images/botones/imprimir.gif"  name="imgimprimir"  onMouseOver="botonOver(this);" onClick="window.location='<%= BASEURL %>/informes/informeFacturacionPrint.jsp';" onMouseOut="botonOut(this);" style="cursor:hand "> <img src="<%= BASEURL%>/images/botones/exportarExcel.gif"  name="imgimprimir"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER + "?estado=InformeFacturacion&accion=GenerarXLS&cmd=show"%>';" onMouseOut="botonOut(this);" style="cursor:hand "> <img src="<%= BASEURL%>/images/botones/regresar.gif"  name="imgimprimir"  onMouseOver="botonOver(this);" onClick="window.location.href = '<%= BASEURL%>/informes/informeFacturacionResumen.jsp';" onMouseOut="botonOut(this);" style="cursor:hand "></div>
    <table width="95%"  border="0" align="center" cellpadding="0" cellspacing="0" class="letras2">
      <tr>
        <td colspan="2"><div align="center">FINTRAVALORES S.A.</div></td>
        <td width="28%"><table width="100%"  border="0" class="letras2">
            <tr>
              <td width="22%">Fecha</td>
              <td width="4%">:</td>
              <td width="74%"><%=fecha_hoy%></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2"><div align="center">INFORME PARA FACTURACION </div></td>
        <td><table width="100%"  border="0" class="letras2">
            <tr>
              <td width="22%">Hora</td>
              <td width="4%">:</td>
              <td width="74%"><%=hora_actual%></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2"><div align="center"></div></td>
        <td width="28%">&nbsp;</td>
      </tr>
      <tr>
        <td width="45%">Informe de fecha : <%=fechai%></td>
        <td colspan="2">Hasta : <%=fechaf%></td>
      </tr>
<%//CLIENTE
        model.clienteService.searchCliente("000127");
        Cliente cli = model.clienteService.getCliente();      
%>
      <tr>
        <td colspan="3"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><table width="100%"  border="0" cellspacing="1" cellpadding="1">
                  <tr>
                    <td><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="letras2">
                        <tr>
                          <td width="10%">CLIENTE</td>
                          <td width="2%">:</td>
                          <td width="88%"><%= cli.getNomcli()%></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="100%">&nbsp;</td>
                        </tr>
                        <tr>
                          <td>
                           
                            <table width="100%"  border="0" cellpadding="1" cellspacing="1" class="letras2">
                              <tr>
                                <td colspan="2"># REMISION</td>
                                <td width="10%">PLACA</td>
                                <td width="28%">CONDUCTOR</td>
                                <td width="15%"><div align="center">TONELAJE</div></td>
                                <td width="2%">&nbsp;</td>
                                <td width="24%"><div align="left">FECHA Y HORA</div></td>
                              </tr>
<%
        double tonelaje = 0;
        double tonelaje_ttl = 0;
        long viajes_ttl = 0;
        long viajes_d = 0;
        long viajes = 0;
        for( int i= 0; i< informe.size(); i++){
                InformeFact info = (InformeFact) informe.elementAt(i);
                Vector linea = info.getRemisiones();
                
                tonelaje = 0;
                viajes = 0;
%>
                              <tr>
                                <td colspan="8">&nbsp;</td>                                
                              </tr>
                              <tr>
<%
                String std_job = info.getStd_job();
                model.stdjobdetselService.buscaStandard(std_job);
                Stdjobdetsel stdj = model.stdjobdetselService.getStandardDetSel();
                String desc = stdj.getSj_desc();
                
                
                
%>
                                <td colspan="8">RUTA: <%= desc + " - Std. Job No. " + std_job %></td>                                
                              </tr>

<%
                for( int j=0; j<linea.size(); j++){
                        Vector obj = (Vector) linea.elementAt(j);
                        Vector rems = (Vector) obj.elementAt(0);
                        String tonelaje_d = (String) obj.elementAt(1);
                        
                        viajes += rems.size();
                        viajes_d = rems.size();
                        
                        for( int k=0; k<rems.size(); k++){
                                RemisionIFact remision = new RemisionIFact();
                                remision = (RemisionIFact) rems.elementAt(k);
                                
                                tonelaje += remision.getTonelaje().doubleValue();

                                String fecha_rem = remision.getFecha();
                        
%>

                              <tr>
                                <td colspan="2"><%= remision.getRemision() %></td>
                                <td width="10%"><%= remision.getPlaca() %></td>
                                <td width="28%"><%= remision.getConductor() %></td>
                                <td width="15%"><div align="center">
                                  <table width="100%"  border="0" cellspacing="0" cellpadding="0" class="letras2">
                                    <tr>
                                      <td width="59%"><div align="right"><%= remision.getTonelaje() %></div></td>
                                      <td width="41%"><div align="center">T </div></td>
                                    </tr>
                                  </table>
                                </div></td>
                                <td width="2%">&nbsp;</td>
                                <td width="24%"><div align="left"><%= remision.getFecha() %></div></td>
                              </tr>

<%
                        }
%>
                              <tr>
                                <td colspan="7">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2"><div align="right">Viajes D&iacute;a &gt;&gt; </div></td>
                                <td width="10%"><div align="right"><%= viajes_d %></div></td>
                                <td width="28%" align="right">Tonelaje Diario &gt;&gt; </td>
                                <td width="15%"><div align="center">
                                  <table width="100%"  border="0" cellspacing="0" cellpadding="0" class="letras2">
                                    <tr>
                                      <td width="59%"><div align="right"><%= com.tsp.util.Util.redondear(Double.valueOf(tonelaje_d).doubleValue(),2) %></div></td>
                                      <td width="41%"><div align="center">T </div></td>
                                    </tr>
                                  </table>
                                </div></td>
                                <td width="2%">&nbsp;</td>
                                <td width="24%"><div align="left"></div></td>
                              </tr>
                              <tr colspan>
                                <td colspan="8">&nbsp;</td>                                
                              </tr>
<%                        
                 }
                 tonelaje_ttl += tonelaje;
                 viajes_ttl += viajes;
%>
                              <tr>
                                <td width="9%">RUTA: </td>
                                <td width="12%"><div align="right">Viajes &gt;&gt; </div></td>
                                <td width="10%"><div align="right"><%= viajes %></div></td>
                                <td width="28%" align="right"><div align="right">Tonelaje &gt;&gt;&nbsp;</div></td>
                                <td width="15%"><div align="center">
                                  <table width="100%"  border="0" cellspacing="0" cellpadding="0" class="letras2">
                                    <tr>
                                      <td width="59%"><div align="right"><%= com.tsp.util.Util.redondear(tonelaje, 2) %></div></td>
                                      <td width="41%"><div align="center">T </div></td>
                                    </tr>
                                  </table>
                                </div></td>
                                <td width="2%">&nbsp;</td>
                                <td width="24%"><div align="left"></div></td>
                              </tr>
                              
                              <tr colspan>
                                <td colspan="8">&nbsp;</td>                                
                              </tr>
<%
                if( i!=(informe.size()-1)){
%>                              
                              <tr colspan>
                                <td colspan="8"><h1 class='SaltoDePagina'>&nbsp;</h1></td>                                
                              </tr>                              

<%
                }
        }
%>         
                              <tr>
                                <td>CLIENTE:</td>
                                <td>Total Viajes &gt;&gt;</td>
                                <td width="10%"><div align="right"><%= viajes_ttl %></div></td>
                                <td width="28%" align="right">Total Tonelaje &gt;&gt; &nbsp;</td>
                                <td width="15%"><div align="center">
                                  <table width="100%"  border="0" cellspacing="0" cellpadding="0" class="letras2">
                                    <tr>
                                      <td width="59%"><div align="right"><%= com.tsp.util.Util.redondear(tonelaje_ttl, 2) %></div></td>
                                      <td width="41%"><div align="center">T </div></td>
                                    </tr>
                                  </table>
                                </div></td>
                                <td width="2%">&nbsp;</td>
                                <td width="24%"><div align="left"></div></td>
                              </tr>
                              
                              <tr colspan>
                                <td colspan="8">&nbsp;</td>                                
                              </tr>

                          </table></td>
                        </tr>
                        
                        <tr>
                          <td> </td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="letras2"></td>
                        </tr>
                    </table></td>
                  </tr>
              </table></td>
            </tr>
        </table></td>
      </tr>

      <tr class="letras2">
        <td colspan="3">&nbsp;        </td>
      </tr>
	  
    </table>
    <p align="center">      <img src="<%= BASEURL%>/images/botones/imprimir.gif"  name="imgimprimir"  onMouseOver="botonOver(this);" onClick="window.location='<%= BASEURL %>/informes/informeFacturacionPrint.jsp';" onMouseOut="botonOut(this);"  style="cursor:hand ">   <img src="<%= BASEURL%>/images/botones/exportarExcel.gif"  name="imgimprimir"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER + "?estado=InformeFacturacion&accion=GenerarXLS&cmd=show"%>';" onMouseOut="botonOut(this);" style="cursor:hand "> <img src="<%= BASEURL%>/images/botones/regresar.gif"  name="imgimprimir"  onMouseOver="botonOver(this);" onClick="window.location.href = '<%= BASEURL%>/informes/informeFacturacionResumen.jsp';" onMouseOut="botonOut(this);" style="cursor:hand "></p>
  </form>
</div>
<%=datos[1]%>
</body>
</html>
