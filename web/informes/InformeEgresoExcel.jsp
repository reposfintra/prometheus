<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<style type="text/css">
<!--
.Estilo1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 10px;
}
.Estilo2 {font-family: Verdana, Arial, Helvetica, sans-serif}
.Estilo3 {font-size: 12px}
.Estilo5 {font-size: 12}
.Estilo8 {font-size: 10px}
.Estilo9 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; }
-->
    </style>
    <title>Informe Diario de Despacho</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<%response.setContentType("application/vnd.ms-excel");%>
<body bgcolor="#FFFFFF">
<%
	Informe inf = model.planillaService.getInforme();
	String fechai = inf.getFechai();
    String fechaf = inf.getFechaf();
	java.util.Date fecha_actual = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
    String fecha_hoy=s.format(fecha_actual);
	s = new SimpleDateFormat("HH:mm:ss");
	String hora_actual = s.format(fecha_actual);
%>
<table width="95%"  border="1" align="center" cellpadding="0" cellspacing="0" class="Estilo9">
  <tr>
    <td width="100%" colspan="6" class="Estilo1"><div align="center">FINTRAVALORES S.A </div></td>
  </tr>
  <tr>
    <td colspan="6" class="Estilo1"><div align="center">RELACION DIARIA DE EGRESOS </div></td>
  </tr>
  <tr>
    <td colspan="6" class="Estilo1"><div align="center">Informe de fecha : <%=fechai%> - <%=fechaf%></div></td>
  </tr>
  <tr>
    <td colspan="6" class="Estilo2">&nbsp;</td>
  </tr>
  <tr class="Estilo1">
    <td class="Estilo2">EGRESO</td>
    <td class="Estilo2"><span class="letras2">REMISION</span></td>
    <td class="Estilo2"><span class="letras2">CEDULA</span></td>
    <td class="Estilo2">NOMBRE</td>
    <td class="Estilo2"><span class="letras2">CONCEPTO</span></td>
    <td class="Estilo2">Valor Anticipo</td>
  </tr>
   <%  Vector info =model.planillaService.getInformes();
				double total = 0;
				for(int j = 0; j<info.size();j++){
						Informe dato = (Informe) info.elementAt(j); 
						total = total +dato.getAnticipo();
				%>
  <tr>
    <td class="Estilo2"><%=dato.getEgreso()%></td>
    <td class="Estilo2"><span class="letras2"><%=dato.getRemision()%></span></td>
    <td class="Estilo2"><span class="letras2"><%=dato.getCedula()%></span></td>
    <td class="Estilo2"><span class="letras2">
      <%if(dato.getConductor()!=null)%>
    <%=dato.getConductor()%></span></td>
    <td class="Estilo2">Anticipo Planilla </td>
    <td class="Estilo2"><%=Util.customFormat(Util.redondear(dato.getAnticipo(),0))%></td>
  </tr>
  <tr>
    <td colspan="6" class="Estilo2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="Estilo2">&nbsp;</td>
    <td class="Estilo2">&nbsp;</td>
    <td class="Estilo2"><span class="letras2">Total General .. </span></td>
    <td class="Estilo2"><%=Util.customFormat(Util.redondear(total,0))%></td>
  </tr>
  <%}%>
  <tr>
    <td colspan="6" class="Estilo2"><%if(info.size()>0){%>
      <table width="100%"  border="1" cellpadding="2" cellspacing="2" bordercolor="#999999" class="Estilo9">
        <tr>
          <td width="27%"><div align="center"><strong>PROVEEDOR</strong></div></td>
          <td colspan="2"><div align="center"><strong>PEAJES</strong></div></td>
          <td colspan="2"><div align="center"><strong>ACPM</strong></div></td>
          <td width="17%"><div align="center"><strong>EFECTIVO</strong></div></td>
          <td width="13%"><div align="center"><strong>TOTAL</strong></div></td>
        </tr>
        <tr>
          <td bordercolor="#FFFFFF"></td>
          <td width="7%"><div align="center">CANT </div></td>
          <td width="14%"><div align="center">VALOR</div></td>
          <td width="8%"><div align="center">CANT </div></td>
          <td width="14%"><div align="center">VALOR</div></td>
          <td></td>
          <td></td>
        </tr>
        <%Vector proveedores = model.planillaService.getProveedores();
				  for(int j = 0; j<proveedores.size();j++){
						Proveedores dato = (Proveedores)  proveedores.elementAt(j); 
						
			%>
        <tr>
          <td><%=dato.getNombre()%></td>
          <td><%=dato.getCantPeaje()%></td>
          <td><div align="right"><%=Util.customFormat(dato.getValorPeaje())%></div></td>
          <td><%=dato.getCantacpm()%></td>
          <td><div align="right"><%=Util.customFormat(dato.getValoracpm())%></div></td>
          <td><div align="right"><%=Util.customFormat(dato.getTotale())%></div></td>
          <td><div align="right"><%=Util.customFormat((dato.getValorPeaje()+dato.getValoracpm()+dato.getTotale()))%></div></td>
        </tr>
        <%}%>
</table>    
      <%}%></td>
  </tr>
</table>
<p align="center">&nbsp;
</p>
</body>
</html>
