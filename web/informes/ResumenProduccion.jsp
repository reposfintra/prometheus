<!--
- Autor : Ing. Jose M Barrios
- Date  : 09 de Septiembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite generar un Resumen de la Produccion de Carbon
--%>

<%-- Declaracion de librerias--%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Resumen Produccion Carbon </title>
    <script src="<%=BASEURL%>/js/validar.js"></script>
	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script src='<%=BASEURL%>/js/date-picker.js'></script>


</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Fecha Informes"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
String accion = CONTROLLER+"?estado=Generar&accion=ResumenPro";
%>
    <form name="form2" method="post" action="<%=accion%>" onSubmit="return validarFecha(this);">
		<table align="center" width="500" border="2">
			<tr>
				<td>	
					<TABLE width="100%" align="center"  class="tablaInferior">
						<tr class="fila">
							<td class="subtitulo1">&nbsp;Resumen de Produccion</td>
							<td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
						<tr class="fila">
							<td width="231" nowrap>Fecha Inicio</td>
							<td width="245" nowrap><span class="comentario">
								<input name='fechai' type='text' id="fechai" style='width:120' value='' readonly>
								<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechai);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a></td>
						</tr>
						<tr class="fila">
							<td width="231" nowrap>Fecha Final </td>
							<td width="245" nowrap><span class="comentario">
								<input name='fechaf' type='text' id="fechaf" style='width:120' value='' readonly>
								<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechaf);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a></td>
						 </tr>
						<tr class="fila">
							<td nowrap>Proyecto</td>
							<td nowrap>
							<select name="base" id="base">
							  <option value="pco">Puerto Prodeco</option>
								<option value="spo">Puerto Carbosan</option>
								<option value="PRC">Puerto Rio Cordoba</option>
							</select></td>
						</tr>
					</table>
				</td>
			</tr>
	  </table>			
		<p align="center">
			<input type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" title="Generar" style="cursor:hand" name="Aceptar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
		</p>
	</form>
</div>   
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe> 
</body>
</html>
