<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
    <title>Informe Diario de Despacho</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="<%=BASEURL%>/js/validar.js"></script>
    <link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">

</head>

<body bgcolor="#FFFFFF" onLoad="window.print();window.location='<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=54'">
<div align="center">
  <form name="form1" method="post" action="">
    <div align="center">      <%
	Informe inf = model.planillaService.getInforme();
	String fechai = inf.getFechai();
    String fechaf = inf.getFechaf();
	java.util.Date fecha_actual = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
    String fecha_hoy=s.format(fecha_actual);
	s = new SimpleDateFormat("HH:mm:ss");
	String hora_actual = s.format(fecha_actual);
	Usuario usuario = (Usuario) session.getAttribute("Usuario");
%>
</div>
    <table width="95%"  border="0" align="center" cellpadding="0" cellspacing="0" class="letras2">
      <tr>
        <td colspan="2"><div align="center">FINTRAVALORES S.A </div></td>
        <td width="28%"><table width="100%"  border="0" class="letras2">
            <tr>
              <td width="22%">Fecha</td>
              <td width="4%">:</td>
              <td width="74%"><%=fecha_hoy%></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2"><div align="center">INFORME DIARIO DE DESPACHO</div></td>
        <td><table width="100%"  border="0" class="letras2">
            <tr>
              <td width="22%">Hora</td>
              <td width="4%">:</td>
              <td width="74%"><%=hora_actual%></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2"><div align="center"></div></td>
        <td width="28%">&nbsp;</td>
      </tr>
      <tr>
        <td width="45%">Informe de fecha : <%=fechai%></td>
        <td width="27%">Hasta : <%=fechaf%></td>
        <td>&nbsp;</td>
      </tr>
      <%//CLIENTE
 		model.planillaService.llenarClientes(fechai, fechaf,usuario.getBase());
	  	Vector info = (Vector)model.planillaService.getClientes();
		int viajec = 0;
		int equipoc = 0;
		float tonc = 0;
		double antc =0;
		for(int i = 0; i<info.size();i++){
			String cliente = (String) info.elementAt(i); 
			String cv[] = cliente.split(";");
			String nombrec = cv[1];
			cliente = cv[0];
			%>
      <tr>
        <td colspan="3"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><table width="100%"  border="0" cellspacing="1" cellpadding="1">
                  <tr>
                    <td><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="letras2">
                        <tr>
                          <td width="10%">CLIENTE</td>
                          <td width="2%">:</td>
                          <td width="88%"><%=nombrec%></td>
                        </tr>
                    </table></td>
                  </tr>
                  <%  model.planillaService.llenarRutas(fechai,fechaf, cliente,usuario.getBase());	
				Vector rutas =model.planillaService.getRutas();
				
				for(int j = 0; j<rutas.size();j++){
				String ruta = (String) rutas.elementAt(j); 
				String rutavec[] = ruta.split(",");
				ruta = rutavec[1];
				String desc  = rutavec[0];
				float totalton = 0;
				double totalant = 0;
			%>
                  <tr>
                    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td colspan="8"><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="letras2">
                              <tr>
                                <td width="10%">RUTA</td>
                                <td width="2%">:</td>
                                <td width="88%"><%=desc%></td>
                              </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td colspan="8">
                           
                            <table width="100%"  border="0" cellpadding="1" cellspacing="1" class="letras2">
                              <tr>
                                <td width="12%" colspan="2"># REMISION</td>
                                <td width="9%">PLACA</td>
                                <td width="28%">CONDUCTOR</td>
                                <td width="14%"><div align="center">TONELAJE</div></td>
                                <td width="12%">VLR ANTICIPO</td>
                                <td width="2%">&nbsp;</td>
                                <td width="23%">FECHA Y HORA</td>
                              </tr>
                              <%
						model.planillaService.llenarInforme(fechai, fechaf, cliente,ruta,usuario.getBase());
					  	Vector datos = model.planillaService.getInformes();
						viajec = viajec + datos.size();
						%>
                              <tr>
                                <td colspan="2">
                                  <%   for(int k =0;k<datos.size();  k++){
							Informe dato = (Informe)datos.elementAt(k);
							totalton = totalton+dato.getTonelaje();
							totalant = totalant + dato.getAnticipo();
							
	 				 %>
                              <tr>
                                      <td colspan="2"><%=dato.getRemision()%></td>
                                      <td><%=dato.getPlaca()%></td>
                                      <td><%if(dato.getConductor()!=null)%>
                                          <%=dato.getConductor()%></td>
                                      <td>
                                        <table width="100%"  border="0" cellspacing="0" cellpadding="0" class="letras2">
                                          <tr>
                                            <td width="59%"><div align="right"><%=dato.getTonelaje()%></div></td>
                                            <td width="41%"><div align="center">T </div></td>
                                          </tr>
                                      </table></td>
                                      <td><div align="right"><%=Util.customFormat(Util.redondear(dato.getAnticipo(),0))%></div></td>
                                      <td>&nbsp;</td>
                                      <td><%=dato.getFechaCump().substring(0,16)%></td>
                              </tr>
                                    <%}
					tonc = tonc +totalton;
							antc = antc+totalant;
	  				%>
                          </table></td>
                        </tr>
                        <tr>
                          <td colspan="8"> </td>
                        </tr>
                        <tr>
                          <td colspan="8">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="10%" class="letras2">RUTA</td>
                          <td width="3%" class="letras2">:</td>
                          <td width="14%" class="letras2">Viajes -&gt;&gt;&gt; <%=datos.size()%> </td>
                          <td width="22%" class="letras2">Equipos -&gt;&gt;&gt; <%=model.planillaService.buscarTotalEquipo(fechai, fechaf, cliente, ruta,usuario.getBase())%></td>
                          <td width="8%" class="letras2"><div align="right"><%=totalton%></div></td>
                          <td width="6%" class="letras2"><div align="center">T</div></td>
                          <td width="12%" class="letras2"><div align="right"><%=Util.customFormat(totalant)%></div></td>
                          <td width="25%">&nbsp;</td>
                          <%equipoc = equipoc + model.planillaService.buscarTotalEquipo(fechai, fechaf, cliente, ruta,usuario.getBase()); %>
                        </tr>
                        <tr>
                          <td colspan="8" class="letras2"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <%}%>
              </table></td>
            </tr>
        </table></td>
      </tr>
      <%
  }//FIN POR 
  %>
      <tr class="letras2">
        <td colspan="3">
          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr class="letras2">
              <td width="10%" class="letras2">CLIENTE</td>
              <td width="3%" class="letras2">:</td>
              <td width="14%" class="letras2">Viajes -&gt;&gt;&gt; <%=viajec%> </td>
              <td width="22%" class="letras2">Equipos -&gt;&gt;&gt; <%=equipoc%></td>
              <td width="8%" class="letras2"><div align="right"><%=tonc%></div></td>
              <td width="6%" class="letras2"><div align="center">T</div></td>
              <td width="12%" class="letras2"><div align="right"><%=Util.customFormat(antc)%></div></td>
              <td width="25%" class="letras2">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
    <p align="center">&nbsp;    </p>
  </form>
</div>
</body>
</html>
