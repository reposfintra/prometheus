<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
  <title>Resumen Produccion Carbon</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <script src="<%=BASEURL%>/js/validar.js"></script>
  <script src="<%=BASEURL%>/js/boton.js"></script>
  <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
  <link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">	
  <link href="../css/letras.css" rel="stylesheet" type="text/css">
  <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
  <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Resumen Produccion Carbon"/>
  </div>

  <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  <div align="center">
    <form name="form1" method="post" action="">
    <div align="center">
<%  Informe inf = model.planillaService.getInforme();
  	String fechai = inf.getFechai() + " 07:00";
    String fechaf = inf.getFechaf() + " 06:59";
	  java.util.Date fecha_actual = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
    String fecha_hoy=s.format(fecha_actual);
	  s = new SimpleDateFormat("HH:mm:ss");
	  String hora_actual = s.format(fecha_actual);
	  Usuario usuario = (Usuario) session.getAttribute("Usuario");
%>
      <table width="80%"  border="0" align="center" cellpadding="0" cellspacing="2" bgcolor="#F7F5F4">
      	<tr class="filaresaltada">
      		<td colspan="2" align="center">FINTRAVALORES S.A</td>
      		<td width="28%">Fecha: <%=fecha_hoy%></td>
        </tr>
        <tr class="filaresaltada">
      		<td colspan="2"><div align="center">RESUMEN PRODUCCION CARBON</div></td>
      		<td> Hora: <%=hora_actual%></td>          	
        </tr>      
      	<tr class="fila"><td colspan="3">&nbsp;</td></tr>
      	<tr class="filaresaltada">
        	<td width="45%" colspan="3">Informe de fecha : <%=fechai%> - Hasta : <%=fechaf%></td>
      	</tr>
      	
		<!-- Inicio...-->
      	<tr class="fila">
      		<td colspan="5" align="center"><br><br>
                           
      			<table width="100%"  border="2" bordercolor="#999999" bgcolor="#F7F5F4">
        			<tr class="tblTitulo">
					  <td width="12%" colspan="2" align="center"><b>STANDART</b></td>
					  <td width="30%"><div align="center"><b>RUTA</b></div></td>
					  <td width="26%" align="center"><b>FECHA CUMPLIDO</b></td>
					  <td width="12%" align="center"><b># VIAJES</b></td>
					  <td width="20%"><div align="center"><b>TONELAJE</b></div></td>
					</tr>
        
         			<%Vector datos = model.planillaService.getResumenPro();
					int totalviajes = 0;
          			double totalton = 0; %>
        			<tr>
           				<td colspan="5">
       						<%for(int k =0;k<datos.size();  k++){
								RemisionIFact dato = (RemisionIFact)datos.elementAt(k);
								totalton = Util.redondear(totalton,2)+dato.getTonelaje();
								totalviajes = totalviajes + dato.getViajes();
			 				%>
						</td>
					</tr>		
          			<tr class="<%=(k % 2 == 0 )? "filagris" : "filaazul" %>">
                    	<td colspan="2"><%=dato.getStd_job_no() %></td>
                     	<td><%=dato.getNomStandar().substring(8) %></td>
                     	<td><%=dato.getFecha() %></td>
                     	<td aling="right"><%=dato.getViajes()     %></td>
                     	<td aling="right"><%=dato.getTonelaje()   %></td>
       						<%} %>
        			</tr>  
         			<tr>
						 <td width="12%" class="letras2" colspan="2">&nbsp;</td>
						 <td width="30%" class="letraresaltada" aling="center">Totales</td>
						 <td width="26%" class="letras2" >&nbsp;</td>
						 <td width="12%" class="letraresaltada" ><b><%= totalviajes %></b></td>
         				 <td width="20%" class="letraresaltada" ><b><%= Util.redondear(totalton,2) %></b></td>
         			</tr>
          		</table>
         	</td>
		</tr>
       <!-- Final...-->
      </table>
      <p align="center">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" name="salir" title="Regresar" onMouseOver="botonOver(this);" onClick="window.history.back();" onMouseOut="botonOut(this);" >&nbsp;
	  <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
      </p>
    </form>
    </div>
  </div>
</body>
</html>
