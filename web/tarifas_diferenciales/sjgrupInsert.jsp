<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Ingresar proveedor ACPM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<link href="css/letras.css" rel="stylesheet" type="text/css">

</head>

<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Stdjobplkcosto&accion=Insert&cmd=show" onSubmit="">
  <table width="530" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#EFE3DE" class="Letras">
    <tr bgcolor="#FFA928">
      <td colspan="2" nowrap><div align="center" class="Estilo2"><strong><strong>INGRESAR GRUPOS PARA TARIFAS DIFERENCIALES</strong></strong></div></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td width="162" nowrap bgcolor="#99CCFF"><strong>Distrito</strong></td>
      <td width="352" nowrap bgcolor="ECE0D8"><select name="distrito" id="select">
        <option value="FINV" selected>FINV</option>
      </select></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Codigo del Grupo</strong></td>
      <td width="352" nowrap bgcolor="ECE0D8"><input name="grupo" type="text" id="gru2" maxlength="15"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td width="162" nowrap bgcolor="#99CCFF"><strong>Fecha de Activaci&oacute;n</strong></td>
      <td nowrap bgcolor="ECE0D8"><input name="activacion" type="text" id="activacion" size="18" readonly>
        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.activacion);return false;" HIDEFOCUS> <img src="js/Calendario/cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha" ></a></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Indicador de viaje </strong></td>
      <td nowrap bgcolor="ECE0D8"><select name="ind" id="ind">
        <option value="Y" selected>Si</option>
        <option value="N">No</option>
      </select></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Costo por viaje </strong></td>
      <td nowrap bgcolor="ECE0D8"><input name="costo" type="text" id="costo">
	</tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Costo por unidad</strong></td>
      <td nowrap bgcolor="ECE0D8"><input name="unit_cost" type="text" id="unit_cost"></td>
    </tr>
	<tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Unidad de transporte</strong></td>
      <td nowrap bgcolor="ECE0D8"><input name="unit_transp" type="text" id="unit_transp"></td>
    </tr>
	<tr bgcolor="#99CCFF">
	  <td nowrap bgcolor="#99CCFF"><strong>Moneda</strong></td>
	  <td nowrap bgcolor="ECE0D8"><select name="moneda" id="moneda">
        <option value="PES" selected>Pesos</option>
        <option value="DOL">Dolar</option>
        <option value="BOL">Bolivar</option>
      </select></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <input type="submit" name="Submit" value="Registrar">
    <input type="button" name="Submit2" value="Regresar">
  </div>
  
</form>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
