<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Ingresar proveedor ACPM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<link href="css/letras.css" rel="stylesheet" type="text/css">

</head>

<body>
<% 	   Vector grupos= model.stdjobplkcostoService.getGruposVec();
 	%>
<table width="76%" border="1" align="center" cellpadding="3" cellspacing="2" bgcolor="ECE0D8" class="letras" bordercolor="#CCCCCC">
  <tr bgcolor="#FFA928">
    <td colspan="5"><div align="center"><strong>ESCOJA EL GRUPO ANULAR </strong></div></td>
  </tr>
  <tr bgcolor="#99CCFF">
    <td width="16%"><div align="center"><strong>DISTRITO </strong></div></td>
    <td width="15%"><strong>GRUPO</strong></td>
    <td width="20%"><strong>COSTO VIAJE</strong></td>
    <td width="27%"><strong>COSTO UNIDAD </strong></td>
    <td width="22%"><strong>UNIDAD DE TRANSPORTE</strong></td>
  </tr>
  <%for(int i =0; i<grupos.size(); i++){
		Stdjobplkcosto s = (Stdjobplkcosto) grupos.elementAt(i);
	%>
  <tr style="cursor:hand" title="Anular Grupos..." onClick="window.location='<%=CONTROLLER%>?estado=Stdjobplkcosto&accion=Delete&buscar=ok&grupo=<%=s.getGroup_code()%>'" onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor=''">
    <td><%=s.getDstrct()%></td>
    <td><%=s.getGroup_code()%></td>
    <td><%=s.getCosto_unitario()%></td>
    <td><%=s.getUnit_cost()%></td>
    <td><%=s.getUnit_transp()%></td>
  </tr>
  <%}%>
</table>
<br>
<% if(request.getAttribute("std")!=null){
	Stdjobplkcosto s = (Stdjobplkcosto) request.getAttribute("std");
%>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Stdjobplkcosto&accion=Delete&cmd=show" onSubmit="">
  <table width="530" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#EFE3DE" class="Letras">
    <tr bgcolor="#FFA928">
      <td colspan="2" nowrap><div align="center" class="Estilo2"><strong><strong>MODIFICAR GRUPOS PARA TARIFAS DIFERENCIALES</strong></strong></div></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td width="162" nowrap bgcolor="#99CCFF"><strong>Distrito</strong></td>
      <td width="352" nowrap bgcolor="ECE0D8"><%=s.getDstrct()%>
      <input name="distrito" type="hidden" id="distrito" value="<%=s.getDstrct()%>"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Codigo del Grupo</strong></td>
      <td width="352" nowrap bgcolor="ECE0D8"><input name="grupo" type="hidden" id="grupo" value="<%=s.getGroup_code()%>">
      <%=s.getGroup_code()%></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td width="162" nowrap bgcolor="#99CCFF"><strong>Fecha de Activaci&oacute;n</strong></td>
      <td nowrap bgcolor="ECE0D8"><%=s.getActivation_date()%>
        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.activacion);return false;" HIDEFOCUS> </a>
        <input name="activacion" type="hidden" id="grupo3" value="<%=s.getActivation_date()%>"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Indicador de viaje </strong></td>
      <td nowrap bgcolor="ECE0D8"><%=s.getInd_trip()%></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Costo por viaje </strong></td>
      <td nowrap bgcolor="ECE0D8"><%=s.getCosto_unitario()%>
	</tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Costo por unidad</strong></td>
      <td nowrap bgcolor="ECE0D8"><%=s.getUnit_cost()%></td>
    </tr>
	<tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Unidad de transporte</strong></td>
      <td nowrap bgcolor="ECE0D8"><%=s.getUnit_transp()%></td>
    </tr>
	<tr bgcolor="#99CCFF">
	  <td nowrap bgcolor="#99CCFF"><strong>Moneda</strong></td>
	  <td nowrap bgcolor="ECE0D8"><%=s.getCurrency()%></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <input type="submit" name="Submit" value="Anular">
    <input type="button" name="Submit2" value="Regresar">
  </div>
  
</form>
<%}%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
