<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reporte diario de despachos</title>
		<link href="../css/estilo.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilo.css" rel="stylesheet" type="text/css">
 
    </head>
    <body>

    <div align="center">
      <input type="button" name="Submit" value="Guardar en Archivo" onClick="window.open('agencia/ReporteDiarioDespachosExcel.jsp','','status=no,menubar=yes,scrollbars=yes,width=800,height=600,resizable=yes')">
    </div>
    <p>
        <ag:mostrarReporteDiario/>
    </p>
    <div align="center">
      <input name="Submit" type="button" class="letra" onClick="window.open('agencia/ReporteDiarioDespachosExcel.jsp','','status=no,menubar=yes,scrollbars=yes,width=800,height=600,resizable=yes')" value="Guardar en Archivo">
      
  </div>
    </body>
</html>
