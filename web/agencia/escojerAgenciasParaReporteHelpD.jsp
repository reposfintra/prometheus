<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRIPTIVA - Reporte Diario de Despacho
	 - Date            :      09/05/2005  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Descripcion de los campos a ingresar en el Reporte Diario de Despacho</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
    <style type="text/css">
<!--
.Estilo3 {font-size: 12px}
-->
    </style>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
<body> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ayuda Descriptiva - Reporte Diario de Despacho"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:90%; z-index:0; left: 0px; top: 100px;"> 
   <p>&nbsp;</p>
   <table width="65%" border="2" align="center" id="tabla1" >
        <td>
         <table width="100%" height="50%" align="center">
              <tr>
                <td class="subtitulo1"><strong>Despacho</strong></td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
         </table>
         <table width="100%" borderColor="#999999" bgcolor="#F7F5F4">
           <tr>
             <td colspan="2" class="tblTitulo"><strong>Reporte Diario de Despacho - Pantalla Preliminar</strong></td>
           </tr>
           <tr>
             <td colspan="2" class="subtitulo"><strong>INFORMACION</strong></td>
           </tr>
           <tr>
             <td class="fila">* Por Agencias Del Distrito</td>
             <td><span class="ayudaHtmlTexto">Campo para escoger si desea ver el Reporte de la Agencia 'Fintravalores S.A.' (TSP) &oacute; 'Venezuela' (VEN).</span></td>
           </tr>
           <tr>
             <td class="fila">Link 'Seleccionar Agencias' </td>
             <td><span class="ayudaHtmlTexto">Enlace para realizar la b&uacute;squeda de las Agencias por Distrito.</span></td>
           </tr>
           <tr>
             <td class="fila">* Realizados Por El Despachador</td>
             <td><span class="ayudaHtmlTexto">Campo para ingresar el Login del Despachador que desea observar.</span></td>
           </tr>
           <tr>
             <td width="40%" class="fila">N&uacute;mero de la Placa</td>
             <td width="60%"><span class="ayudaHtmlTexto">Campo para digitar el n&uacute;mero de la placa del veh&iacute;culo que desea buscar.</span></td>
           </tr>
           <tr>
             <td class="fila">Cedula del Conductor</td>
             <td><span class="ayudaHtmlTexto">Campo para digitar la cedula del conductor que desea obtener.</span></td>
           </tr>
           <tr>
             <td class="fila">Fecha Inicial</td>
             <td><span class="ayudaHtmlTexto">Campo para escoger la fecha inicial de la busqueda de los reportes diarios que se desean ver.</span></td>
           </tr>
           <tr>
             <td class="fila">Fecha Final</td>
             <td><span class="ayudaHtmlTexto">Campo para escoger la fecha final de la busqueda de los reportes diarios que se desean ver.</span></td>
           </tr>
		   <tr>
             <td align="left" class="fila">* Cliente</td>
             <td align="left"><span class="ayudaHtmlTexto">Campo para seleccionar el nombre del cliente que desea buscar. </span></td>
		   </tr>
           <tr>
             <td class="fila">Bot&oacute;n Aceptar</td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para realizar el procedimiento de b&uacute;squeda del Reporte Diario de Despacho. </span></td>
           </tr>
           <tr>
             <td class="fila">Bot&oacute;n Salir</td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Reporte Diario de Despacho' y volver a la vista del men&uacute;.</span></td>
           </tr>
           <tr>
             <td colspan="2" align="center" class="letra"><div align="left">* Filtro pincipal de busqueda. Todos los filtros adicionales se encuentran condicionados a la escongencia de por lo menos una de estas tres opciones. </div></td>
           </tr>
         </table></td>
  </table>
		<p></p>
	<center>
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</center>
</div>  
</body>
</html>