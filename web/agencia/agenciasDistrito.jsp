
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<% 
String distrito = request.getParameter("distrito");
//String distrito = "FINV";
Vector ListAgencias  = model.agenciaService.obtenerAgencias(distrito);

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Agencias</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">   
   <script src='<%=BASEURL%>/js/validar.js'></script>
   <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
</head>

<body onLoad='jscript: iniciarCheckAgencias();'>
<form method='post' name='FormularioListado'>
  <div align="center"><br>
  </div>
<% 
	if( !distrito.equals("VEN") ){ %>
  <table width="50%"  border="2" align="center">
    <tr>
      <td><table width="100%" border="0" align="center">
        <tr>
		 <td width="35%" class="subtitulo1" colspan="2">AGENCIAS <%= distrito %></td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>          
        </tr>        
		<tr class="tblTitulo" align="center">
          <th >
            <input type='checkbox' name='All' id="All" onclick='jscript: SelAll();'>
          </th>
         <th width='62' align="center">N&ordm;</th>
          <th width='273' >AGENCIA</th>
        </tr>
        <%
           int Cont2 = 1;
		   int Cont = 1;
            Iterator it2 = ListAgencias.iterator();
            while(it2.hasNext()){
              	Agencia ag  = (Agencia) it2.next();
				String estilo =  (Cont2++ % 2 == 0 ) ? "filaazul" : "filagris";
                %>
        <tr class="<%=estilo%>">
          <td align='center'><input type='checkbox' id="check<%=ag.getId_agencia()%>" name="check<%=ag.getId_agencia()%>" value='<%= ag.getId_agencia()%>' onClick="onCheckAgencias(this.name);" ></td>
          <td align="center"><%=Cont++%></td>
          <td align='center'><div align="left">[ <%= ag.getId_agencia() %> ] <%= ag.getNombre() %></div></td>
        </tr>
        <%  }   %>
      </table></td>
    </tr>
  </table>
   <br>
	<%}
	 else { %>                    
	  <%out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>No Existen Agencias!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	  <br>
	<%}%>
	
  <div align="center"><br>
    <input type='hidden' name='Opcion'/>
    <input name="Guardar" src="<%=BASEURL%>/images/botones/aceptar.gif" align="middle" type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="window.close();">
  </div>
</form>
</body>
</html>
