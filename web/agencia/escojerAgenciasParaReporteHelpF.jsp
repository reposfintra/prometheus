<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Reporte Diario de Despacho
	 - Date            :      09/05/2005  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>Funcionalidad del Reporte Diario de Despacho</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/reporte/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE REPORTE WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa Reporte Diario de Despacho</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">En la siguiente pantalla  se digita la informaci&oacute;n que coresponda a dicha opci&oacute;n.</p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=340 src="<%=BASEIMG%>image_001.JPG" width=924 border=0 v:shapes="_x0000_i1143"></div></td>
			
          </tr>
		  <tr><td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>Filtro.JPG" border=0 v:shapes="_x0000_i1143"></div></td></tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Se puede realizar una b&uacute;squeda por agencias, presionando el bot&oacute;n 'Buscar' y nos saldr&iacute;a la siguiente pantalla. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=438 src="<%=BASEIMG%>image_002.JPG" width=350 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que existan agencias; si no existen agencias, nos saldr&aacute; el siguiente mensaje. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p align="center"><IMG height=110 src="<%=BASEIMG%>image_003.JPG" width=383 border=0 v:shapes="_x0000_i1052"></p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que se escoja un parametro de b&uacute;squeda en especifico. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error001.JPG" width=356 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que se escoja por lo menos una agencia donde existan varias. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error002.JPG" width=251 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Si al escoger la opci&oacute;n 'REALIZADOS POR EL DESPACHADOR', no ingreso el Login del Despachador, en la pantalla le saldr&aacute; lo siguiente. </p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error003.JPG" width=225 border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que se escoja la fecha de inicio. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=125 src="<%=BASEIMG%>image_error004.JPG" width=369 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">El sistema verifica que se escoja la fecha de finalizaci&oacute;n. </p></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error005.JPG" width=359 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Finalmente si todos los datos estan ingresados correctamente, el proceso se realizar&aacute; satisfactoriamente, y le saldr&aacute; la siguinete pantalla. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=518 src="<%=BASEIMG%>image_004.JPG" width=673 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
