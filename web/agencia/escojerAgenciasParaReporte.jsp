<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%boolean bandera = false; %>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  
  	TreeMap cli = (TreeMap) request.getAttribute("TClientes");         
	cli.put(" TODOS","%");
%>
<html>
    <head>

        <title>Reporte Diario de Despacho</title>        
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
        <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
		<script>
			var valorRadio = "";
			var flag = false;
			
			function setRadio( value ){
				valorRadio = value;	
			}
			
			function validarAgenciasReporte( form ){
																
								
                                if( valorRadio == "1" && form.despachador.value == "" ){
					alert( 'Debe escribir el despachador...' );
					return false;
				}
				
				if ( form.fechai.value == '' ){
					alert( 'Defina la fecha de inicio de busqueda para poder continuar...' );
					return false;
				}
				if ( form.fechaf.value == '' ){
					alert( 'Defina la fecha de fin de busqueda para poder continuar...' );
					return false;
				}
				
				if ( (valorRadio == "0") && (form.distrito.value == 'FINV') && (form.agencias.value == '' || form.agencias.value == 'a') ){
					alert( 'Escoja, por lo menos, una agencia...' );
					return false;
				}
				
				if( form.cliente.value == "" ){
					alert( 'Debe seleccionar el cliente...' );
					return false;
				}
				
				if( valorRadio == "" ){                                   
                                    form.opc.value     = "3";
                                }
						
				
				var msg = '  Se puede verificar, en el log de procesos el estado  \n'+
                                          '  del reporte, para saber si hay algunos registros    \n'+
                                          '  con error en los datos.           ';
   				
                                          alert( msg );
				return true;
			}				
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte Diario de Despacho"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

        <form name='formulario' method='POST' id="formulario" action="<%=CONTROLLER%>?estado=Generar&accion=ReporteDiario" onSubmit="return validarAgenciasReporte(this);">
            <table width="100%"  border="2">
              <tr>
                <td><table width="100%"  border="0" class="tablaInferior">
                  <tr>
                   <td class="subtitulo1" colspan="2">&nbsp;REPORTE DIARIO DE DESPACHO</td>
          			<td class="barratitulo" colspan="2"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                  </tr>
                  <tr class="fila">
                    <td>
                        <input name="tipo" id="tipo" type="radio" value="0" onClick="flag=true;setRadio(this.value);">
                        POR AGENCIAS DEL DISTRITO
					</td>
                    <th scope="col" align="left">
                      <select name="distrito" id="distrito" class="textbox">
                        <option value="FINV">FINV</option>
                        <option value="VEN">VEN</option>
                      </select>
					    <a href="JavaScript:void(0);" class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/agencia/agenciasDistrito.jsp?distrito=' + formulario.distrito.value,'','status=no,scrollbars=yes,resizable=yes')">Seleccionar Agencias</a>
						<input name='agencias' type='hidden' id="agencias" value="a"/>
                                                <input name="todos" type="hidden" id="todos" value="false">
                                                <input name="opc"   type="hidden" id="opc" value="">
                    </th>
                    <td class="fila" nowrap>
                          <input name="tipo" id="tipo" type="radio" value="1" onClick="setRadio(this.value);">
                    REALIZADOS POR EL DESPACHADOR</td>
                    <th scope="col" align="left"><input name="despachador" type="text" id="despachador3" size="15" class="textbox"></th>
                  </tr>
                  <tr class="titulo">
                   <td class="subtitulo1" colspan="2">&nbsp;OTROS DATOS</td>
          			<td class="barratitulo" colspan="2"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                  </tr>
                  <tr class="fila">
                    <td width="24%">&nbsp;&nbsp;N&uacute;mero de la Placa</td>
                    <td width="26%" align="left"><input name="placa" type="text" id="placa" size="20" class="textbox"></td>
                    <td width="26%">&nbsp;&nbsp;Cedula del Conductor</td>
                    <td width="24%" align="left"><input name="conductor" type="text" id="conductor" size="20" class="textbox"></td>
                  </tr>
                  <tr class="fila">
                   <td align="left">&nbsp;&nbsp;Fecha Inicial</td>
          		   <td>
				  		<input name="fechai" type="text" class="textbox" id="fechai" readonly>
              			<span class="Letras"> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" name="obligatorio"> <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechai);return false;" HIDEFOCUS></span></td>
                  <td align="left"class="letra_resaltada">&nbsp;&nbsp;Fecha Final</td>
          		  <td align="left">
              			<input name="fechaf" type="text" class="textbox" id="fechaf" readonly>
              			<span class="Letras"><a href="javascript:void(0)" onclick="jscript: show_calendar('fechaf');" HIDEFOCUS> </a><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" name="obligatorio"> <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechaf);return false;" HIDEFOCUS></span></td>
                  </tr>  
				  <tr class="fila">
                                    <td>&nbsp;&nbsp;Cliente</td>
                                    <td><input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.formulario.cliente,this)" size="15" ></td>
                                    <td></td>
				    <td></td>
                                   </tr>
              	  <tr class="fila">
                	<td></td>
					<td><input:select name="cliente" attributesText="class=textbox" options="<%=cli %>"/>&nbsp;</td>				  				  				 					
					<td></td>
					<td></td>
				  </tr>              
                </table></td>
              </tr>
            </table>
			<br>
            <table align="center">
              <tr>
                <td colspan="2" nowrap align="center"><input name="Guardar" src="<%=BASEURL%>/images/botones/aceptar.gif" align="middle" type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">                  
&nbsp;
                  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
              </tr>
            </table>
        </form > 
	</div>  
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
    <%=datos[1]%>
	</body>
</html>
