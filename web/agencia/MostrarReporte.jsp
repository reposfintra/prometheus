<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reporte diario de despachos</title>
		<link href="../css/estilo.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilo.css" rel="stylesheet" type="text/css">
 
    </head>
    <body>

    <div align="left">    </div>
    <table width="210%" height="36%"  border="1" cellpadding="2" cellspacing="1" bordercolor="#CCCCCC">
      <tr>
        <td height="14%" class="titulo"><div align="center"> FINTRAVALORES S.A <br>
        REPORTE DIARIO DE DESPACHOS </div></td>
      </tr>
	  <%//FOR AGENCIAS
	  	Vector agencias =model.agenciaService.getAgencias();
		for(int i = 0; i<agencias.size();i++){
			Agencia ag = (Agencia) agencias.elementAt(i);
	  %>
      <tr>
        <td height="25%" class="subtitulos"><strong>AGENCIA : <%=ag.getNombre()%></strong></td>
      </tr>
      <tr>
        <td height="47%" class="fila">
		<table width="100%"  border="1" cellpadding="2" cellspacing="1" bordercolor="#CCCCCC">
          <tr class="titulo">
            <td width="4%" height="22">PLANILLA</td>
            <td width="6%">FECHA IMPRESION</td>
            <td width="6%">ORIGEN</td>
            <td width="6%">DESTINO</td>
            <td width="4%">PLACA</td>
            <td width="11%">CONDUCTOR</td>
            <td width="13%">CLIENTE</td>
            <td width="4%">CANTIDAD</td>
            <td width="4%">REMESA</td>
            <td colspan="2">STANDARD</td>
            <td width="9%">VALOR REMESA </td>
            <td width="7%">VALOR PLANILLA </td>
            <td width="6%">VALOR ANTICIPO </td>
          </tr>
		  <%//OBTENGO DATOS DEL INFORME
		  Vector informes = model.planillaService.getInformes();
		  int sw =0;
		  for(int j =0; j<informes.size(); j++ ){
		  	Informe in = (Informe) informes.elementAt(j);
			if(in.getAgencia().equals(ag.getId_agencia())){
			 sw=1;
		  %>
          <tr class="fila">
            <td><%=in.getNumpla()%></td>
            <td><%=in.getFecha()%></td>
            <td><%=in.getOrigen()%></td>
            <td><%=in.getDestino()%></td>
            <td><%=in.getPlaca()%></td>
            <td><%=in.getConductor()%></td>
            <td><%=in.getCliente()%></td>
            <td><%=in.getTonelaje()%></td>
            <td><%=in.getRemision()%></td>
            <td colspan="2"><%=in.getStdjob()%></td>
            <td><%=in.getVlrrem()%></td>
            <td><%=in.getVlrpla()%></td>
            <td><%=in.getAnticipo()%></td>
          </tr>
		  <%}
		  }
		  if(sw == 0){%>
		   <tr class="fila">
            <td colspan="14" class="fondoverde"><strong>NO SE ENCONTRARON DATOS PARA ESTA AGENCIA </strong></td>
          </tr>
		  <%}%>
        </table></td>
      </tr>
	  <%//FIN FOR AGENCIA
	  }
	  %>
    </table>
    </body>
</html>
