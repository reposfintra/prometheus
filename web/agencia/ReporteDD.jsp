<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<% Util u = new Util(); 
   UtilFinanzas ut = new UtilFinanzas();%>
<html>
    <head>        
        <title>Reporte diario de despachos</title>
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
	 <jsp:include page="/toptsp.jsp?encabezado=Reporte Diario de Despacho"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=DiarioExcel">
      <input name="Opcion" type="hidden" id="Opcion">
      <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar(); Opcion.value='Guardar';">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onClick="window.history.back();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>
    <table width="3500"  border="2">      
          <%//FOR AGENCIAS
	  	Vector agencias = model.agenciaService.getAgencias();
		
		if( agencias != null && agencias.size() > 0 ){
			for(int i = 0; i<agencias.size();i++){
				Agencia ag = (Agencia) agencias.elementAt(i);
	  %>
          <tr>
        	<td class="subtitulo1"><strong>AGENCIA : <%=ag.getNombre()%></strong></td>
          </tr>
          <tr>
            <td>
              <table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                <tr class="tblTitulo" align="center">
                  <td>PLANILLA</td>
                  <td>FECHA IMPRESION</td>
                  <td width="150" align="center">ORIGEN</td>
                  <td width="150" align="center">DESTINO</td>
                  <td width="50"  align="center">PLACA</td>
                  <td width="55"  align="center">TRAILER</td>
                  <td>CONTENEDORES</td>
                  <td width="200">CONDUCTOR</td>
                  <td width="200">CLIENTE</td>
                  <td>CANTIDAD DESPACHADA</td>
                  <td>UNIDADES DESPACHADAS </td>
                  <td>VACIO</td>
                  <td>TIPO CARGA</td>
                  <td width="100">VALOR PLANILLA </td>
                  <td >MONEDA</td>
                  <td width="100">VALOR ANTICIPO </td>
                  <td >MONEDA</td>
                  <td >ANTICIPO MAXIMO </td>
                  <td >REMESA</td>
                  <td colspan="2" width="300">STANDARD</td>
                  <td width="100">VALOR REMESA </td>
                  <td >MONEDA</td>
                  <td >CANTIDAD FACTURADA </td>
                  <td>UNIDAD</td>
                  <td width="100">DOCUMENTO</td>
                  <td>VLR REMESA BASE</td>
                  <td title='Valor Remesa prorrateado'>VLR REMESA PRO.</td>
                  <td title='% Remesa prorrateado'>% REMESA PRO.</td>
                  <td>VLR PLANILLA BASE</td>
                  <td title='Valor Planilla prorrateado'>VLR PLANILLA PRO.</td>
                  <td title='% Planilla prorrateado'>% PLANILLA PRO.</td>
                  <td>UTILIDAD</td>
                  <td>% UTILIDAD BRUTA</td>
                  <td>DESPACHADOR</td>
                  <td>VLR COSTO EXTRAFLETE </td>
                  <td>DESCRIPCION COSTO EXTRAFLETE </td>
                  <td>VLR INGRESO EXTRAFLETE </td>
                  <td>DESCRIPCION INGRESO EXTRAFLETE </td>                  
                </tr>
                <%//OBTENGO DATOS DEL INFORME
		  Vector informes = model.planillaService.getInformes();
		  int sw =0;
		  int k=0;
		  for(int j =0; j<informes.size(); j++ ){
		  	Informe in = (Informe) informes.elementAt(j);
			if(in.getAgencia().equals(ag.getId_agencia())){
			 sw=1;
			 k++;
		  %>
                  <tr class="<%=(k % 2 == 0 )?"filagris":"filaazul"%>">
                  <td align="center" class="bordereporte"><%=(in.getNumpla()!=null)?in.getNumpla():""%></td>
                  <td class="bordereporte"><%=(in.getFecha()!=null)?in.getFecha():""%></td>
                  <td align="center" class="bordereporte"><%=(in.getOrigen()!=null)?in.getOrigen():""%></td>
                  <td align="center" class="bordereporte"><%=(in.getDestino()!=null)?in.getDestino():""%></td>
                  <td align="center" class="bordereporte"><%=(in.getPlaca()!=null)?in.getPlaca():""%></td>
                  <td align="center" class="bordereporte"><%=(in.getPlatlr()!=null)?in.getPlatlr():""%></td>
				  <td class="bordereporte"><%=(in.getContenedores()!=null)?in.getContenedores():""%></td>
                  <td class="bordereporte"><%=(in.getConductor()!=null)?in.getConductor():""%></td>
                  <td class="bordereporte"><%=(in.getCliente()!=null)?in.getCliente():""%></td>
                  <td align="center" class="bordereporte"><%=in.getTonelaje()%></td>
                  <td align="center" class="bordereporte"><%=(in.getUnidadesdesp()!=null)?in.getUnidadesdesp():""%></td>
                  <td align="center" class="bordereporte"><%=(in.getVacio()!=null)?in.getVacio():""%></td>
                  <td align="center" class="bordereporte"><%=(in.getTipocarga()!=null)?in.getTipocarga():""%></td>
                  <td align="right" class="bordereporte"><%=u.customFormat(in.getVlrpla())%></td>
                  <td align="center" class="bordereporte"><%=(in.getMonedapla()!=null)?in.getMonedapla():""%></td>
                  <td align="right" class="bordereporte"><%=u.customFormat(in.getAnticipo())%></td>
                  <td align="center" class="bordereporte"><%=(in.getMonedaant()!=null)?in.getMonedaant():""%></td>
                  <td align="center" class="bordereporte">% <%=in.getPorcentajemaximoant()%></td>
                  <td align="center" class="bordereporte"><%=(in.getRemision()!=null)?in.getRemision():""%></td>
                  <td colspan="2" class="bordereporte"><%=(in.getStdjob()!=null)?in.getStdjob():""%></td>
                  <td align="right" class="bordereporte"><%=u.customFormat(in.getVlrrem())%></td>
                  <td align="center" class="bordereporte"><%=(in.getMonedarem()!=null)?in.getMonedarem():""%></td>
                  <td align="right" class="bordereporte"><%=u.customFormat(in.getCantidadfacturada())%></td>
                  <td align="center" class="bordereporte"><%=(in.getUnidad()!=null)?in.getUnidad():""%></td>
                  <td class="bordereporte"><%if(!in.getDocumentos().equals("")){out.print(in.getDocumentos());}else%>&nbsp;</td>
                  
                  <td class="bordereporte" align="right"><%=u.customFormat(in.getVlr_rem_base())%></td>
                  <td class="bordereporte" align="right"><%=u.customFormat(in.getVlr_rem_pro ())%></td>
                  <td class="bordereporte" align="right"><%=u.customFormat(in.getPor_rem_por())%></td>
                  
                  <td class="bordereporte" align="right"><%=u.customFormat(in.getVlr_pla_base())%></td>
                  <td class="bordereporte" align="right"><%=u.customFormat(in.getVlr_pla_pro ())%></td>
                  <td class="bordereporte" align="right"><%=u.customFormat(in.getPor_pla_por())%></td>
                  
                  <td class="bordereporte" align="right">% <%=u.customFormat(in.getUtilidadbruta())%></td>
                  <td class="bordereporte" align="right">% <%=u.customFormat(in.getPor_utilidad_bruta())%></td>
                  
                  
                  <td class="bordereporte"><%=(in.getDespachador()!=null)?in.getDespachador():""%></td>
                  <td align="right" class="bordereporte"><%=u.customFormat(in.getVlrcostoflete())%></td>
                  <td class="bordereporte"><%if(!in.getCostoflete().equals("")){out.print(in.getCostoflete().replaceAll("\\*","<br>"));}else%>&nbsp;</td>
                  <td  align="right" class="bordereporte"><%=u.customFormat(in.getVlringresoflete())%></td>
                  <td class="bordereporte"><%if(!in.getIngresoflete().equals("")){out.print(in.getIngresoflete().replaceAll("\\*","<br>"));}else%>&nbsp;</td>                
                </tr>
                <%}
		  }
		  if(sw == 0){%>
                <tr class="fila">
                  <td colspan="39" class="fondoverde">&nbsp;</td>
                </tr>
                <%}%>
            </table>
          <%//FIN FOR AGENCIA
	  	}
	  }
	  %>
      </table></td>
      </tr>
    </table>
    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=DiarioExcel">
      <input name="Opcion" type="hidden" id="Opcion">
      <input name="Guardar3" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar(); Opcion.value='Guardar';">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onClick="window.history.back();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>
	</div>
    </body>
</html>
