<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:param name="periodo"/>   
<xsl:param name="page-count"/>  
<xsl:param name="fechaactual"/>     
<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  		<fo:layout-master-set>
			<fo:simple-page-master master-name="simple"
				 page-height="29.7cm"
				 page-width="21cm"
				 margin-top="0.25cm"
				 margin-bottom="0.5cm"
			         margin-left="1cm"
				 margin-right="1cm" initial-page-number="1">						                            
                          <fo:region-body margin-top="2.5cm" margin-bottom="2cm"/>
                          <fo:region-before extent="2.5cm"/>                          
			</fo:simple-page-master>
  		</fo:layout-master-set>
  
		<fo:page-sequence master-reference="simple">
                    <fo:static-content flow-name="xsl-region-before">
             <fo:table table-layout="fixed" height="2.5cm">
		<fo:table-column column-width="18cm"/>		
		<fo:table-body>
			<fo:table-row>				
                                <fo:table-cell									
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">
					<fo:table table-layout="fixed">
                                        <fo:table-column column-width="6cm"/>
                                        <fo:table-column column-width="6cm"/>
                                        <fo:table-column column-width="6cm"/>
						<fo:table-body >
                                                
                                                <fo:table-row>
                                                 <fo:table-cell									                                                                       
                                                    text-align="left" vertical-align="middle"
                                                    number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                                    <fo:block font-size="12pt" color="black" padding-before="0.5cm" font-weight='bold'>TRASLADO NACIONAL</fo:block>
						</fo:table-cell>                                                
                                                </fo:table-row>
                                                
                                                <!-- Fila Inicial -->
                                                
                                                
						<fo:table-row>
                                                    <fo:table-cell padding-before="0.2cm">
                                                        <fo:external-graphic src="url(images/logo.bmp)"/>
                                                    </fo:table-cell>
                                                
                                                    <fo:table-cell	                                                    								                                                                       
                                                        text-align="center" vertical-align="middle"
                                                        number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                                        <fo:block font-size="24pt" color="black" padding-before="0.5cm" font-weight='bold' text-decoration="underline">REMISION DE MERCANCIA</fo:block>
                                                    </fo:table-cell>
						
                                            </fo:table-row>
                                            
                                            <!-- Fin Fila inicial -->
                                                                                                                                                                                                                                                
					</fo:table-body>
					</fo:table>
			    </fo:table-cell>		
			</fo:table-row>
                        
                        
                        
		</fo:table-body>
		</fo:table>
</fo:static-content>
                
                                <fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="raiz"/>
				</fo:flow>                                                                
		</fo:page-sequence>
	</fo:root>
</xsl:template>

        <xsl:template match="raiz">	                
                <xsl:apply-templates select="tabla1"/>
                <xsl:apply-templates select="tabla2"/>
                <xsl:apply-templates select="tabla3"/>
                <xsl:apply-templates select="tabla4"/><!-- Filas mixtas -->
                <xsl:apply-templates select="tabla5"/><!-- Filas mixtas -->
                <xsl:apply-templates select="tabla6"/>
                <xsl:apply-templates select="tabla7"/>
                <xsl:apply-templates select="tabla8"/>
                <xsl:apply-templates select="tabla9"/>   
                <xsl:apply-templates select="tabla10"/>                                                                              		
	</xsl:template>
	
        <!-- Inicio Tabla 1-->
        
        <xsl:template match="tabla1">
                     
           
           <fo:table table-layout="fixed" padding-before="0.3cm">
                    <fo:table-column column-width="10cm"/>
                    <fo:table-column column-width="10cm"/>
                    <fo:table-body>
                    
                    <fo:table-row>
                        <fo:table-cell> 
                            <fo:table table-layout="fixed" padding-before="0.3cm">
                                <fo:table-column column-width="4cm"/>
                                <fo:table-column column-width="6cm"/>
                                <fo:table-body>
                                    <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold'>TRANSPORTISTA :</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black" font-weight='bold' text-decoration="underline"><xsl:value-of select="transportista"/></fo:block>
                                         </fo:table-cell>
                                         

                                     </fo:table-row>
                                     <!-- Fin Transportista -->
                                     
                                     <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">SUCURSAL :</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black" font-weight='bold' text-decoration="underline" padding-before="0.175cm"><xsl:value-of select="sucursal"/></fo:block>
                                         </fo:table-cell>
                                         

                                     </fo:table-row>
                                     <!-- Fin Sucursal -->
                                     
                                     <fo:table-row >
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">DIRECCION :</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black" font-weight='bold' text-decoration="underline" padding-before="0.175cm"><xsl:value-of select="direccion"/></fo:block>
                                         </fo:table-cell>
                                         

                                     </fo:table-row>
                                     <!-- Fin direccion -->
                                     
                                     <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">TELEFONO :</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black" font-weight='bold' text-decoration="underline" padding-before="0.175cm"><xsl:value-of select="telefono"/></fo:block>
                                         </fo:table-cell>
                                         

                                     </fo:table-row>
                                     <!-- Fin Telefono -->
                                     
                                 </fo:table-body>
                            </fo:table>  
                            <!-- Fin de Tabla -->
                        </fo:table-cell>	
                        
                        
                        <!-- Fin de Celda 1 -->      
                        <!-- Celda 2-->                  
                        <fo:table-cell> 
                        <fo:table table-layout="fixed">
                                <fo:table-column column-width="4cm"/>
                                <fo:table-column column-width="1cm"/>
                                <fo:table-column column-width="4cm"/>
                                <fo:table-body>
                                    <fo:table-row>
                                         <fo:table-cell                                                         
                                            text-align="center" display-align="center" background-color="white"
                                            border-width="0.5pt"
                                            border-style="solid"				 
                                            border-color="black">
                                            <fo:block font-size="7pt" color="black">OT - # REMESA TERRESTRE</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white"
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"				 
                                            border-left-color="black">
                                            <fo:block font-size="7pt" color="black"></fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" 
                                            border-width="0.5pt"
                                            border-style="solid"				 
                                            border-color="black">
                                            <fo:block font-size="7pt" color="black">NUMERO REMISION</fo:block>
                                         </fo:table-cell>                                                                                                          
                                    </fo:table-row>      
                                    
                                    <fo:table-row height="1cm">
                                         <fo:table-cell                                                         
                                            text-align="center" background-color="white" display-align="center"
                                            border-width="0.5pt"
                                            border-style="solid"				 
                                            border-color="black">
                                            <fo:block font-size="12pt" color="black" font-weight='bold'><xsl:value-of select="remesa"/></fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white"
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"				 
                                            border-left-color="black">
                                            <fo:block font-size="7pt" color="black"></fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" background-color="white" display-align="center"
                                            border-width="0.5pt"
                                            border-style="solid"				 
                                            border-color="black">
                                            <fo:block font-size="12pt" color="black" font-weight='bold'><xsl:value-of select="remision"/></fo:block>
                                            
                                         </fo:table-cell>                                                                                                          
                                    </fo:table-row> 
                                    
                                    <!-- Fin Fila OTS -->
                                    <!-- Filas fechas -->
                                    <fo:table-row >
                                        <fo:table-cell>
                                            <fo:table table-layout="fixed" padding-before="0.3cm">
                                                <fo:table-column column-width="3cm"/>
                                                <fo:table-column column-width="0.5cm"/>
                                                <fo:table-column column-width="3cm"/>
                                                <fo:table-column column-width="0.5cm"/>
                                                <fo:table-column column-width="2.5cm"/>
                                                <fo:table-body>
                                                    <fo:table-row>
                                                        <fo:table-cell                                                         
                                                            text-align="center" background-color="white" display-align="center"
                                                            border-width="0.5pt"
                                                            border-style="solid"				 
                                                            border-color="black">
                                                            <fo:block font-size="8pt" color="black" font-weight='bold'><xsl:value-of select="fechaa"/></fo:block>
                                                         </fo:table-cell>  
                                                    
                                                         <fo:table-cell                                                         
                                                                    text-align="center" vertical-align="middle" background-color="white"
                                                                    border-width="0.5pt"
                                                                    border-style="solid"
                                                                    border-color="white"				 
                                                                    border-left-color="black">
                                                                    <fo:block font-size="7pt" color="black"></fo:block>
                                                         </fo:table-cell>
                                                         
                                                         
                                                         <fo:table-cell                                                         
                                                            text-align="center" background-color="white" display-align="center"
                                                            border-width="0.5pt"
                                                            border-style="solid"				 
                                                            border-color="black">
                                                            <fo:block font-size="8pt" color="black" font-weight='bold'><xsl:value-of select="fechadsp"/></fo:block>
                                                         </fo:table-cell> 
                                                         
                                                         <fo:table-cell                                                         
                                                                    text-align="center" vertical-align="middle" background-color="white"
                                                                    border-width="0.5pt"
                                                                    border-style="solid"
                                                                    border-color="white"				 
                                                                    border-left-color="black">
                                                                    <fo:block font-size="7pt" color="black"></fo:block>
                                                         </fo:table-cell>
                                                    
                                                         <fo:table-cell                                                         
                                                            text-align="center" background-color="white" display-align="center"
                                                            border-width="0.5pt"
                                                            border-style="solid"				 
                                                            border-color="black">
                                                            <fo:block font-size="8pt" color="black" font-weight='bold'><xsl:value-of select="horadsp"/></fo:block>
                                                         </fo:table-cell> 

                                                    </fo:table-row>  
                                                    
                                                    <!-- inicio fila titulos fecha -->
                                                    <fo:table-row > 
                                                    <fo:table-cell                                                         
                                                            text-align="center" background-color="white" display-align="center"
                                                            border-width="0.5pt"
                                                            border-style="solid"				 
                                                            border-color="white"
                                                            border-top-color="black" padding-before="0.1cm">
                                                            <fo:block font-size="6pt" color="black">FECHA ACTUAL</fo:block>
                                                         </fo:table-cell>  
                                                    
                                                         <fo:table-cell                                                         
                                                                    text-align="center" vertical-align="middle" background-color="white"
                                                                    border-width="0.5pt"
                                                                    border-style="solid"
                                                                    border-color="white">
                                                                    <fo:block font-size="6pt" color="black"></fo:block>
                                                         </fo:table-cell>
                                                         
                                                         
                                                         <fo:table-cell                                                         
                                                            text-align="center" background-color="white" display-align="center"
                                                            border-width="0.5pt"
                                                            border-style="solid"				 
                                                            border-color="white"
                                                            border-top-color="black" padding-before="0.1cm">
                                                            <fo:block font-size="6pt" color="black">FECHA DE DESPACHO</fo:block>
                                                         </fo:table-cell> 
                                                         
                                                         <fo:table-cell                                                         
                                                                    text-align="center" vertical-align="middle" background-color="white"
                                                                    border-width="0.5pt"
                                                                    border-style="solid"
                                                                    border-color="white">
                                                                    <fo:block font-size="7pt" color="black"></fo:block>
                                                         </fo:table-cell>
                                                    
                                                         <fo:table-cell                                                         
                                                            text-align="center" background-color="white" display-align="center"
                                                            border-width="0.5pt"
                                                            border-style="solid"				 
                                                            border-color="white" border-top-color="black" padding-before="0.1cm">
                                                            <fo:block font-size="6pt" color="black">HORA DESPACHO</fo:block>
                                                         </fo:table-cell>                                                                                                                                                            
                                                    </fo:table-row>      
                                                    <!-- Fin fila titulos fecha -->                                                                                             
                                                </fo:table-body>
                                            </fo:table>                                        
                                        </fo:table-cell>
                                        <!-- Fila Celda -->  
                                    </fo:table-row> 
                                    <!-- Fin de Fila Fechas--> 
                                                                                                        
                                </fo:table-body>
                         </fo:table>  
                                    
                        </fo:table-cell>	
                        <!-- Fin de Celda 2-->
                    </fo:table-row>
 
		</fo:table-body>
          </fo:table>
       <!-- 1  FILA --> 
                                     

        </xsl:template>
        <!-- Fin tabla 1 -->
        
        
     
        <!-- Inicio Tabla 2 -->
       <xsl:template match="tabla2">
           <fo:table table-layout="fixed" padding-before="0.3cm">
                        <fo:table-column column-width="8cm"/>
                        <fo:table-column column-width="0.5cm"/>
                        <fo:table-column column-width="6cm"/>
                        <fo:table-column column-width="0.5cm"/>
                        <fo:table-column column-width="2cm"/>
                        <fo:table-column column-width="0.5cm"/>
                        <fo:table-column column-width="2cm"/>
                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell                                                         
                                text-align="center" background-color="white" display-align="center"
                                border-width="0.5pt"
                                border-style="solid"				 
                                border-color="black"
                                number-columns-spanned="1">
                                    <fo:block font-size="8pt" color="black" font-weight='bold' text-decoration="underline">PLACA DE VEHICULO</fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            
                             <fo:table-row>                                
                                <fo:table-cell text-align="center">
                                <fo:table table-layout="fixed">
                                    <fo:table-column column-width="4cm"/>
                                    <fo:table-column column-width="4cm"/>
                                    <fo:table-body>
                                        <fo:table-row height="1cm">
                                            <fo:table-cell                                                         
                                                text-align="center" background-color="white" display-align="center"
                                                border-width="0.5pt"
                                                border-style="solid"				 
                                                border-color="black"
                                                number-columns-spanned="1" padding-left="0.7cm">
                                                    <fo:block font-size="20pt" color="black" font-weight='bold' letter-spacing="1em"><xsl:value-of select="letra1"/><xsl:value-of select="letra2"/><xsl:value-of select="letra3"/></fo:block>
                                              </fo:table-cell>

                                              <fo:table-cell                                                         
                                                text-align="center" background-color="white" display-align="center"
                                                border-width="0.5pt"
                                                border-style="solid"				 
                                                border-color="black"
                                                number-columns-spanned="1" padding-left="0.7cm">
                                                    <fo:block font-size="20pt" color="black" font-weight='bold' letter-spacing="1em"><xsl:value-of select="numero1"/><xsl:value-of select="numero2"/><xsl:value-of select="numero3"/></fo:block>
                                              </fo:table-cell>
                                              
                                        </fo:table-row>                      
                                        <!-- fin fila Interna 1-->
                                        
                                        <fo:table-row>
                                            <fo:table-cell                                                         
                                                text-align="center" background-color="white" display-align="center"
                                                border-width="0.5pt"
                                                border-style="solid"				 
                                                border-color="black"
                                                number-columns-spanned="1">
                                                    <fo:block font-size="8pt" color="black"  letter-spacing="0.5em">LETRAS</fo:block>
                                              </fo:table-cell>

                                              <fo:table-cell                                                         
                                                text-align="center" background-color="white" display-align="center"
                                                border-width="0.5pt"
                                                border-style="solid"				 
                                                border-color="black"
                                                number-columns-spanned="1">
                                                    <fo:block font-size="8pt" color="black"  letter-spacing="0.5em">NUMEROS</fo:block>
                                              </fo:table-cell>
                                              
                                        </fo:table-row>
                                        
                                        <!-- fin fila Interna 2-->
                                    </fo:table-body>
                                    </fo:table>
                                </fo:table-cell>
                                                                
                                 <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white"
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"				 
                                            border-left-color="black">
                                            <fo:block font-size="7pt" color="black"></fo:block>
                                 </fo:table-cell>
                                
                                <fo:table-cell text-align="center">
                                <fo:table table-layout="fixed">
                                    <fo:table-column column-width="6cm"/>                                    
                                    <fo:table-body>
                                        <fo:table-row height="1cm">
                                            <fo:table-cell                                                         
                                                text-align="center" background-color="white" display-align="center"
                                                border-width="0.5pt"
                                                border-style="solid"				 
                                                border-color="black"
                                                number-columns-spanned="1">
                                                    <fo:block font-size="12pt" color="black" font-weight='bold'><xsl:value-of select="conductor"/></fo:block>
                                              </fo:table-cell>
                                              
                                              
                                        </fo:table-row>                      
                                        <!-- fin fila Interna 1-->
                                        
                                        <fo:table-row>
                                            <fo:table-cell                                                         
                                                text-align="center" background-color="white" display-align="center"
                                                border-width="0.5pt"
                                                border-style="solid"				 
                                                border-color="black"
                                                number-columns-spanned="1">
                                                    <fo:block font-size="7pt" color="black"  letter-spacing="0.5em">NOMBRE CONDUCTOR</fo:block>
                                              </fo:table-cell>
                                              
                                              
                                        </fo:table-row>
                                        
                                        <!-- fin fila Interna 2-->
                                    </fo:table-body>
                                    </fo:table>
                                </fo:table-cell>
                                
                                
                                <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white"
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"				 
                                            border-left-color="black">
                                            <fo:block font-size="7pt" color="black"></fo:block>
                                 </fo:table-cell>
                                
                                <!--CEDULA -->
                                <fo:table-cell text-align="center">
                                <fo:table table-layout="fixed">
                                    <fo:table-column column-width="2cm"/>                                    
                                    <fo:table-body>
                                        <fo:table-row height="1cm">
                                            <fo:table-cell                                                         
                                                text-align="center" background-color="white" display-align="center"
                                                border-width="0.5pt"
                                                border-style="solid"				 
                                                border-color="black"
                                                number-columns-spanned="1">
                                                    <fo:block font-size="8pt" color="black" font-weight='bold'><xsl:value-of select="cedula"/></fo:block>
                                              </fo:table-cell>
                                              
                                              
                                        </fo:table-row>                      
                                        <!-- fin fila Interna 1-->
                                        
                                        <fo:table-row>
                                            <fo:table-cell                                                         
                                                text-align="center" background-color="white" display-align="center"
                                                border-width="0.5pt"
                                                border-style="solid"				 
                                                border-color="black"
                                                number-columns-spanned="1">
                                                    <fo:block font-size="7pt" color="black"  letter-spacing="0.5em">CEDULA</fo:block>
                                              </fo:table-cell>
                                              
                                              
                                        </fo:table-row>
                                        
                                        <!-- fin fila Interna 2-->
                                    </fo:table-body>
                                    </fo:table>
                                </fo:table-cell>
                                <!-- CELULAR -->
                                
                                <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white"
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"				 
                                            border-left-color="black">
                                            <fo:block font-size="7pt" color="black"></fo:block>
                                 </fo:table-cell>
                                
                                <fo:table-cell text-align="center">
                                <fo:table table-layout="fixed">
                                    <fo:table-column column-width="2cm"/>                                    
                                    <fo:table-body>
                                        <fo:table-row height="1cm">
                                            <fo:table-cell                                                         
                                                text-align="center" background-color="white" display-align="center"
                                                border-width="0.5pt"
                                                border-style="solid"				 
                                                border-color="black"
                                                number-columns-spanned="1">
                                                    <fo:block font-size="8pt" color="black" font-weight='bold'><xsl:value-of select="celular"/></fo:block>
                                              </fo:table-cell>
                                              
                                              
                                        </fo:table-row>                      
                                        <!-- fin fila Interna 1-->
                                        
                                        <fo:table-row>
                                            <fo:table-cell                                                         
                                                text-align="center" background-color="white" display-align="center"
                                                border-width="0.5pt"
                                                border-style="solid"				 
                                                border-color="black"
                                                number-columns-spanned="1">
                                                    <fo:block font-size="7pt" color="black"  letter-spacing="0.4em">CELULAR</fo:block>
                                              </fo:table-cell>
                                              
                                              
                                        </fo:table-row>
                                        
                                        <!-- fin fila Interna 2-->
                                    </fo:table-body>
                                    </fo:table>
                                </fo:table-cell>
                                
                             </fo:table-row>                                 
                             <!--Fila de Placas -->            
                        </fo:table-body>
            </fo:table>  
        <!-- Fin del tabla -->                            
        </xsl:template>  
        <!-- Fin tabla 2 -->
        
        <!-- Tabla 3 -->
        <xsl:template match="tabla3">
        <fo:table table-layout="fixed" padding-before="0.3cm">
            <fo:table-column column-width="1.5cm"/>
            <fo:table-column column-width="0.2cm"/>
            <fo:table-column column-width="2cm"/>
            <fo:table-column column-width="0.2cm"/>
            <fo:table-column column-width="3cm"/>
            <fo:table-column column-width="0.2cm"/>
            <fo:table-column column-width="4cm"/>
            <fo:table-column column-width="0.2cm"/>
            <fo:table-column column-width="6cm"/>
            <fo:table-column column-width="0.2cm"/>
            <fo:table-column column-width="2cm"/>
            <fo:table-body>
                <fo:table-row height="0.75cm">
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"
                    border-width="0.5pt"
                    border-style="solid"				 
                    border-color="black"
                    number-columns-spanned="1">
                    <fo:block font-size="8pt" color="black" font-weight='bold'><xsl:value-of select="cant_piezas"/></fo:block>
                 </fo:table-cell> 
                 
                 <!-- Cantidad Piezas -->
                 <fo:table-cell                                                         
                    text-align="center" vertical-align="middle" background-color="white"
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="white"				 
                    border-left-color="black">
                    <fo:block font-size="7pt" color="black"></fo:block>
                </fo:table-cell>
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"
                    border-width="0.5pt"
                    border-style="solid"				 
                    border-color="black"
                    number-columns-spanned="1">
                    <fo:block font-size="8pt" color="black" font-weight='bold'><xsl:value-of select="peso"/></fo:block>
                 </fo:table-cell>
                 <!-- Peso -->          
                     
                 
                 <fo:table-cell                                                         
                    text-align="center" vertical-align="middle" background-color="white"
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="white"				 
                    border-left-color="black">
                    <fo:block font-size="7pt" color="black"></fo:block>
                </fo:table-cell>
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"
                    border-width="0.5pt"
                    border-style="solid"				 
                    border-color="black"
                    number-columns-spanned="1">
                    <fo:block font-size="7pt" color="black" font-weight='bold'><xsl:value-of select="DesRemision"/></fo:block>
                 </fo:table-cell>
                 <!-- Tipo de vehiculo -->
                
                 <fo:table-cell                                                         
                    text-align="center" vertical-align="middle" background-color="white"
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="white"				 
                    border-left-color="black">
                    <fo:block font-size="7pt" color="black"></fo:block>
                </fo:table-cell>
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"
                    border-width="0.5pt"
                    border-style="solid"				 
                    border-color="black"
                    number-columns-spanned="1">
                    <fo:block keep-together='always'>
                        <fo:block font-size="7pt" color="black" font-weight='bold'><xsl:value-of select="origen"/></fo:block>
                        <fo:block font-size="7pt" color="black" font-weight='bold'><xsl:value-of select="destino"/></fo:block>
                   </fo:block>
                                       
                 </fo:table-cell>
                 <!-- Ruta -->
                 
                 <fo:table-cell                                                         
                    text-align="center" vertical-align="middle" background-color="white"
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="white"				 
                    border-left-color="black">
                    <fo:block font-size="7pt" color="black"></fo:block>
                </fo:table-cell>
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"
                    border-width="0.5pt"
                    border-style="solid"				 
                    border-color="black"
                    number-columns-spanned="1">
                    <fo:block font-size="7pt" color="black" font-weight='bold'><xsl:value-of select="centro"/></fo:block>
                 </fo:table-cell>
                 <!-- Centro de costo -->
                 
                 <fo:table-cell                                                         
                    text-align="center" vertical-align="middle" background-color="white"
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="white"				 
                    border-left-color="black">
                    <fo:block font-size="7pt" color="black"></fo:block>
                </fo:table-cell>
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"
                    border-width="0.5pt"
                    border-style="solid"				 
                    border-color="black"
                    number-columns-spanned="1">
                    <fo:block font-size="8pt" color="black" font-weight='bold'><xsl:value-of select="compensac"/></fo:block>
                 </fo:table-cell>
                 <!-- compensac -->
                 
                </fo:table-row>  
                
                <!-- Fin Fila -->
                
                <fo:table-row>
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"
                    border-width="0.5pt"
                    border-style="solid"				 
                    border-color="black"
                    number-columns-spanned="1">
                    <fo:block font-size="6pt" color="black">CANT. PIEZAS</fo:block>
                 </fo:table-cell> 
                 
                 <!-- Cantidad Piezas -->
                 <fo:table-cell                                                         
                    text-align="center" vertical-align="middle" background-color="white"
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="white"				 
                    border-left-color="black">
                    <fo:block font-size="7pt" color="black"></fo:block>
                </fo:table-cell>
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"
                    border-width="0.5pt"
                    border-style="solid"				 
                    border-color="black"
                    number-columns-spanned="1">
                    <fo:block font-size="6pt" color="black">PESO Kgs</fo:block>
                 </fo:table-cell>
                 <!-- Peso -->          
                     
                 
                 <fo:table-cell                                                         
                    text-align="center" vertical-align="middle" background-color="white"
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="white"				 
                    border-left-color="black">
                    <fo:block font-size="7pt" color="black"></fo:block>
                </fo:table-cell>
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"
                    border-width="0.5pt"
                    border-style="solid"				 
                    border-color="black"
                    number-columns-spanned="1">
                    <fo:block font-size="6pt" color="black">TIPO DE VEHICULO</fo:block>
                 </fo:table-cell>
                 <!-- Tipo de vehiculo -->
                
                 <fo:table-cell                                                         
                    text-align="center" vertical-align="middle" background-color="white"
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="white"				 
                    border-left-color="black">
                    <fo:block font-size="7pt" color="black"></fo:block>
                </fo:table-cell>
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"
                    border-width="0.5pt"
                    border-style="solid"				 
                    border-color="black"
                    number-columns-spanned="1">
                    <fo:block font-size="6pt" color="black">RUTA</fo:block>
                 </fo:table-cell>
                 <!-- Ruta -->
                 
                 <fo:table-cell                                                         
                    text-align="center" vertical-align="middle" background-color="white"
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="white"				 
                    border-left-color="black">
                    <fo:block font-size="7pt" color="black"></fo:block>
                </fo:table-cell>
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"
                    border-width="0.5pt"
                    border-style="solid"				 
                    border-color="black"
                    number-columns-spanned="1">
                    <fo:block font-size="6pt" color="black">CENTRO DE COSTOS</fo:block>
                 </fo:table-cell>
                 <!-- Centro de costo -->
                 
                 <fo:table-cell                                                         
                    text-align="center" vertical-align="middle" background-color="white"
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="white"				 
                    border-left-color="black">
                    <fo:block font-size="7pt" color="black"></fo:block>
                </fo:table-cell>
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"
                    border-width="0.5pt"
                    border-style="solid"				 
                    border-color="black"
                    number-columns-spanned="1">
                    <fo:block font-size="6pt" color="black">COMPENSAC.</fo:block>
                 </fo:table-cell>
                 <!-- compensac -->
                 
                </fo:table-row>  
            </fo:table-body>
         </fo:table>
         <!-- fin de tabla -->
        </xsl:template>
        <!-- Fin del Tabla 3 -->
        
        <!-- Tabla 4 -->
        <xsl:template match="tabla4">
        <fo:table table-layout="fixed" padding-before="0.2cm">
            <fo:table-column column-width="5cm"/>            
            <fo:table-column column-width="5cm"/>            
            <fo:table-column column-width="5cm"/>            
            <fo:table-column column-width="4.5cm"/>            
            <fo:table-body>
            <!-- Titulos -->
            <fo:table-row height="0.75cm">
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-right-width="0pt"
                    border-bottom-width="0pt">
                    <fo:block font-size="8pt" color="black" font-weight='bold' text-decoration="underline">ORIGEN DE LA CARGA</fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"
                    border-right-color="white"
                    border-left-color="white"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-bottom-width="0pt">
                    <fo:block font-size="8pt" color="black" font-weight='bold' text-decoration="underline">DIRECCION</fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"
                    border-right-color="white"
                    border-left-color="white"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-bottom-width="0pt">
                    <fo:block font-size="8pt" color="black" font-weight='bold' text-decoration="underline">TELEFONO-EXT</fo:block>
                 </fo:table-cell>
                                  
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"                   
                    border-left-color="white"
                    border-left-width="0pt"
                    border-bottom-width="0pt">
                    <fo:block font-size="8pt" color="black" font-weight='bold' text-decoration="underline">CONTACTO</fo:block>
                 </fo:table-cell>
                 
            </fo:table-row> 
            <!-- fin fila titulos -->
            
            <!-- Fila de Datos 1-->
            <fo:table-row height="0.5cm">
                <fo:table-cell                                                         
                    text-align="left" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"                    
                    border-color="black"				 
                    border-right-color="white" padding-left="0.2cm"
                    border-right-width="0pt"
                    border-top-width="0pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="origen1"/></fo:block>
                 </fo:table-cell>
                                  
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="direccion1"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="tel1"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				                    
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-top-width="0pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="contacto1"/></fo:block>
                 </fo:table-cell>
                 
            </fo:table-row>                        
            <!-- Fin fila Datos 1-->
            
            <!-- Fila de Datos 2-->
            <fo:table-row height="0.5cm">
                <fo:table-cell                                                         
                    text-align="left" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"                    
                    border-color="black"				 
                    border-right-color="white" padding-left="0.2cm"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="origen2"/></fo:block>
                 </fo:table-cell>
                                  
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="direccion2"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="tel2"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				                    
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="contacto2"/></fo:block>
                 </fo:table-cell>
                 
            </fo:table-row>                        
            <!-- Fin fila Datos 2-->
            
            <!-- Fila de Datos 3-->
            <fo:table-row height="0.5cm">
                <fo:table-cell                                                         
                    text-align="left" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"                    
                    border-color="black"				 
                    border-right-color="white" padding-left="0.2cm"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="origen3"/></fo:block>
                 </fo:table-cell>
                                  
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="direccion3"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="tel3"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				                    
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="contacto3"/></fo:block>
                 </fo:table-cell>
                 
            </fo:table-row>                        
            <!-- Fin fila Datos 3-->
            
            <!-- Fila de Datos 4-->
            <fo:table-row height="0.5cm">
                <fo:table-cell                                                         
                    text-align="left" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"                    
                    border-color="black"				 
                    border-right-color="white" padding-left="0.2cm"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="origen4"/></fo:block>
                 </fo:table-cell>
                                  
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="direccion4"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="tel4"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				                    
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="contacto4"/></fo:block>
                 </fo:table-cell>
                 
            </fo:table-row>                        
            <!-- Fin fila Datos 4-->
            
            <!-- Fila de Datos 5-->
            <fo:table-row height="0.5cm">
                <fo:table-cell                                                         
                    text-align="left" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"                    
                    border-color="black"				 
                    border-right-color="white" padding-left="0.2cm"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="origen5"/></fo:block>
                 </fo:table-cell>
                                  
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="direccion5"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="tel5"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				                    
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="contacto5"/></fo:block>
                 </fo:table-cell>
                 
            </fo:table-row>                        
            <!-- Fin fila Datos 5-->
            </fo:table-body>
         </fo:table>
        </xsl:template>
        <!-- Fin de Tabla 4 -->
        
        
        <!-- Tabla 5 -->
        <xsl:template match="tabla5">
        <fo:table table-layout="fixed" padding-before="0.2cm">
            <fo:table-column column-width="5cm"/>            
            <fo:table-column column-width="5cm"/>            
            <fo:table-column column-width="5cm"/>            
            <fo:table-column column-width="4.5cm"/>            
            <fo:table-body>
            <!-- Titulos -->
            <fo:table-row height="0.75cm">
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-right-width="0pt"
                    border-bottom-width="0pt">
                    <fo:block font-size="8pt" color="black" font-weight='bold' text-decoration="underline">DESTINO DE LA CARGA</fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"
                    border-right-color="white"
                    border-left-color="white"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-bottom-width="0pt">
                    <fo:block font-size="8pt" color="black" font-weight='bold' text-decoration="underline">DIRECCION</fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"
                    border-right-color="white"
                    border-left-color="white"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-bottom-width="0pt">
                    <fo:block font-size="8pt" color="black" font-weight='bold' text-decoration="underline">TELEFONO-EXT</fo:block>
                 </fo:table-cell>
                                  
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"                   
                    border-left-color="white"
                    border-left-width="0pt"
                    border-bottom-width="0pt">
                    <fo:block font-size="8pt" color="black" font-weight='bold' text-decoration="underline">CONTACTO</fo:block>
                 </fo:table-cell>
                 
            </fo:table-row> 
            <!-- fin fila titulos -->
            
            <!-- Fila de Datos 1-->
            <fo:table-row height="0.5cm">
                <fo:table-cell                                                         
                    text-align="left" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"                    
                    border-color="black"				 
                    border-right-color="white" padding-left="0.2cm"
                    border-right-width="0pt"
                    border-top-width="0pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="destino1"/></fo:block>
                 </fo:table-cell>
                                  
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="direcciond1"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="teld1"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				                    
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-top-width="0pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="contactod1"/></fo:block>
                 </fo:table-cell>
                 
            </fo:table-row>                        
            <!-- Fin fila Datos 1-->
            
            <!-- Fila de Datos 2-->
            <fo:table-row height="0.5cm">
                <fo:table-cell                                                         
                    text-align="left" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"                    
                    border-color="black"				 
                    border-right-color="white" padding-left="0.2cm"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="destino2"/></fo:block>
                 </fo:table-cell>
                                  
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="direcciond2"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="teld2"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				                    
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="contactod2"/></fo:block>
                 </fo:table-cell>
                 
            </fo:table-row>                        
            <!-- Fin fila Datos 2-->
            
            <!-- Fila de Datos 3-->
            <fo:table-row height="0.5cm">
                <fo:table-cell                                                         
                    text-align="left" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"                    
                    border-color="black"				 
                    border-right-color="white" padding-left="0.2cm"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="destino3"/></fo:block>
                 </fo:table-cell>
                                  
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="direcciond3"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="teld3"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				                    
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="contactod3"/></fo:block>
                 </fo:table-cell>
                 
            </fo:table-row>                        
            <!-- Fin fila Datos 3-->
            
            <!-- Fila de Datos 4-->
            <fo:table-row height="0.5cm">
                <fo:table-cell                                                         
                    text-align="left" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"                    
                    border-color="black"				 
                    border-right-color="white" padding-left="0.2cm"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="destino4"/></fo:block>
                 </fo:table-cell>
                                  
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="direcciond4"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="teld4"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				                    
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="contactod4"/></fo:block>
                 </fo:table-cell>
                 
            </fo:table-row>                        
            <!-- Fin fila Datos 4-->
            
            <!-- Fila de Datos 5-->
            <fo:table-row height="0.5cm">
                <fo:table-cell                                                         
                    text-align="left" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"                    
                    border-color="black"				 
                    border-right-color="white" padding-left="0.2cm"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="destino5"/></fo:block>
                 </fo:table-cell>
                                  
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="direcciond5"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				 
                    border-right-color="white"
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-right-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="teld5"/></fo:block>
                 </fo:table-cell>
                                 
                
                <fo:table-cell                                                         
                    text-align="center" background-color="white" display-align="center"                    
                    number-columns-spanned="1"
                    border-style="solid"
                    border-color="black"				                    
                    border-left-color="white" padding-left="0.2cm"
                    border-left-width="0pt"
                    border-top-width="0.2pt">
                    <fo:block font-size="8pt" color="black"><xsl:value-of select="contactod5"/></fo:block>
                 </fo:table-cell>
                 
            </fo:table-row>                        
            <!-- Fin fila Datos 5-->
            </fo:table-body>
         </fo:table>
        </xsl:template>
        <!-- Fin de Tabla 5 -->
        
        <!-- Tabla 6 -->
        <xsl:template match="tabla6">
        <fo:table table-layout="fixed" padding-before="0.2cm">
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="18.5cm"/>
            <fo:table-body>
                <fo:table-row height="0.5cm"> 
                    <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="1"
                        border-style="solid"                    
                        border-color="black"				 
                        border-right-color="white" padding-left="0.2cm"
                        border-right-width="0pt"
                        border-bottom-width="0.2pt">
                        <fo:block font-size="8pt" color="black" text-decoration="underline">DO :</fo:block>
                     </fo:table-cell>
                     
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="1"
                        border-style="solid"                    
                        border-color="black"				 
                        padding-left="0.2cm"
                        border-left-width="0pt"
                        border-bottom-width="0.2pt">
                        <fo:block font-size="8pt" color="black"><xsl:value-of select="do"/></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
                 <!-- fila 1 -->
                 
                 <fo:table-row height="0.5cm">                                         
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="2"
                        border-style="solid"
                        border-top-width="0.2pt"
                        border-bottom-width="0pt"                   
                        border-color="black"				 
                        padding-left="0.2cm">
                        <fo:block font-size="8pt" color="black"></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
                 
                 <fo:table-row height="0.5cm">                                         
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="2"
                        border-style="solid"
                        border-top-width="0.2pt"                    
                        border-color="black"				 
                        padding-left="0.2cm">
                        <fo:block font-size="8pt" color="black"></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
            </fo:table-body>
        </fo:table>
        </xsl:template>
        <!-- fin de tabla 6 -->
        
        <!-- Tabla 7 -->
        <xsl:template match="tabla7">
        <fo:table table-layout="fixed" padding-before="0.2cm">
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="18.5cm"/>
            <fo:table-body>
                <fo:table-row height="0.5cm"> 
                    <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="1"
                        border-style="solid"                    
                        border-color="black"				 
                        border-right-color="white" padding-left="0.2cm"
                        border-right-width="0pt"
                        border-bottom-width="0.2pt">
                        <fo:block font-size="8pt" color="black" text-decoration="underline">PO :</fo:block>
                     </fo:table-cell>
                     
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="1"
                        border-style="solid"                    
                        border-color="black"				 
                        padding-left="0.2cm"
                        border-left-width="0pt"
                        border-bottom-width="0.2pt">
                        <fo:block font-size="8pt" color="black"><xsl:value-of select="po"/></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
                 <!-- fila 1 -->
                 
                 <fo:table-row height="0.5cm">                                         
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="2"
                        border-style="solid"
                        border-top-width="0.2pt"
                        border-bottom-width="0pt"                   
                        border-color="black"				 
                        padding-left="0.2cm">
                        <fo:block font-size="8pt" color="black"></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
                 
                 <fo:table-row height="0.5cm">                                         
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="2"
                        border-style="solid"
                        border-top-width="0.2pt"
                        border-bottom-width="0pt"                   
                        border-color="black"				 
                        padding-left="0.2cm">
                        <fo:block font-size="8pt" color="black"></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
                 
                 <fo:table-row height="0.5cm">                                         
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="2"
                        border-style="solid"
                        border-top-width="0.2pt"
                        border-bottom-width="0pt"                   
                        border-color="black"				 
                        padding-left="0.2cm">
                        <fo:block font-size="8pt" color="black"></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
                 
                 <fo:table-row height="0.5cm">                                         
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="2"
                        border-style="solid"
                        border-top-width="0.2pt"                    
                        border-color="black"				 
                        padding-left="0.2cm">
                        <fo:block font-size="8pt" color="black"></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
            </fo:table-body>
        </fo:table>
        </xsl:template>
        <!-- fin de tabla 7 -->
        
        <!-- Tabla 8 -->
        <xsl:template match="tabla8">
        <fo:table table-layout="fixed" padding-before="0.2cm">
            <fo:table-column column-width="2.5cm"/>
            <fo:table-column column-width="17cm"/>
            <fo:table-body>
                <fo:table-row height="0.5cm"> 
                    <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="1"
                        border-style="solid"                    
                        border-color="black"				 
                        border-right-color="white" padding-left="0.2cm"
                        border-right-width="0pt"
                        border-bottom-width="0.2pt">
                        <fo:block font-size="8pt" color="black" text-decoration="underline">CONTENIDO :</fo:block>
                     </fo:table-cell>
                     
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="1"
                        border-style="solid"                    
                        border-color="black"				 
                        padding-left="0.2cm"
                        border-left-width="0pt"
                        border-bottom-width="0.2pt">
                        <fo:block font-size="8pt" color="black"><xsl:value-of select="contenido"/></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
                 <!-- fila 1 -->
                 
                 <fo:table-row height="0.5cm">                                         
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="2"
                        border-style="solid"
                        border-top-width="0.2pt"
                        border-bottom-width="0pt"                   
                        border-color="black"				 
                        padding-left="0.2cm">
                        <fo:block font-size="8pt" color="black"></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
                                  
                 
                 <fo:table-row height="0.5cm">                                         
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="2"
                        border-style="solid"
                        border-top-width="0.2pt"
                        border-bottom-width="0pt"                   
                        border-color="black"				 
                        padding-left="0.2cm">
                        <fo:block font-size="8pt" color="black"></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
                 
                 <fo:table-row height="0.5cm">                                         
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="2"
                        border-style="solid"
                        border-top-width="0.2pt"                    
                        border-color="black"				 
                        padding-left="0.2cm">
                        <fo:block font-size="8pt" color="black"></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
            </fo:table-body>
        </fo:table>
        </xsl:template>
        <!-- fin de tabla 8 -->
        
        <!-- Tabla 9 -->
        <xsl:template match="tabla9">
        <fo:table table-layout="fixed" padding-before="0.2cm">
            <fo:table-column column-width="3cm"/>
            <fo:table-column column-width="16.5cm"/>
            <fo:table-body>
                <fo:table-row height="0.5cm"> 
                    <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="1"
                        border-style="solid"                    
                        border-color="black"				 
                        border-right-color="white" padding-left="0.2cm"
                        border-right-width="0pt"
                        border-bottom-width="0.2pt">
                        <fo:block font-size="8pt" color="black" text-decoration="underline">OBSERVACIONES :</fo:block>
                     </fo:table-cell>
                     
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="1"
                        border-style="solid"                    
                        border-color="black"				 
                        padding-left="0.2cm"
                        border-left-width="0pt"
                        border-bottom-width="0.2pt">
                        <fo:block font-size="8pt" color="black"><xsl:value-of select="observaciones"/></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
                 <!-- fila 1 -->
                 
                 <fo:table-row height="0.5cm">                                         
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="2"
                        border-style="solid"
                        border-top-width="0.2pt"
                        border-bottom-width="0pt"                   
                        border-color="black"				 
                        padding-left="0.2cm">
                        <fo:block font-size="8pt" color="black"></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
                                  
                 
                 <fo:table-row height="0.5cm">                                         
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="2"
                        border-style="solid"
                        border-top-width="0.2pt"
                        border-bottom-width="0pt"                   
                        border-color="black"				 
                        padding-left="0.2cm">
                        <fo:block font-size="8pt" color="black"></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
                 
                 <fo:table-row height="0.5cm">                                         
                     <fo:table-cell                                                         
                        text-align="left" background-color="white" display-align="before"                    
                        number-columns-spanned="2"
                        border-style="solid"
                        border-top-width="0.2pt"                    
                        border-color="black"				 
                        padding-left="0.2cm">
                        <fo:block font-size="8pt" color="black"></fo:block>
                    </fo:table-cell>
                 </fo:table-row> 
            </fo:table-body>
        </fo:table>
        </xsl:template>
        <!-- fin de tabla 9 -->
        <!-- Tabla 10 -->
        <xsl:template match="tabla10">
            <fo:table table-layout="fixed" padding-before="0.2cm">
                <fo:table-column column-width="5cm"/>
                <fo:table-column column-width="4.75cm"/>
                <fo:table-column column-width="5cm"/>
                <fo:table-column column-width="4.75cm"/>
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell                                                         
                            text-align="center" background-color="white" display-align="center"                    
                            number-columns-spanned="2"
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black">
                            <fo:block font-size="12pt" color="black" text-decoration="underline">ENVIO</fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell                                                         
                            text-align="center" background-color="white" display-align="center"                    
                            number-columns-spanned="2"
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black">
                            <fo:block font-size="12pt" color="black" text-decoration="underline">RECIBO</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <!-- Despachador -->
                    
                    <fo:table-row height="2cm">
                        <fo:table-cell                                                         
                            text-align="left" background-color="white" display-align="before"                    
                            number-columns-spanned="1" 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black"
                            border-top-color="white"
                            border-right-color="white"
                            padding-left="0.2cm">
                            <fo:block font-size="8pt" color="black" text-decoration="underline">NOMBRE : <xsl:value-of select="despachador"/></fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell                                                         
                            text-align="center" background-color="white" display-align="before"                                                
                            number-columns-spanned="1" border-width="0.5pt"
                            border-style="solid"
                            border-color="black"
                            border-top-color="white"
                            border-left-color="white">
                            <fo:block font-size="8pt" color="black" text-decoration="underline">    FECHA : <xsl:value-of select="fechaactual"/></fo:block>
                        </fo:table-cell>                    
                                        
                        <fo:table-cell                                                         
                            text-align="left" background-color="white" display-align="before"                    
                            number-columns-spanned="1"
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black"
                            border-top-color="white"
                            border-right-color="white"
                            padding-left="0.2cm">
                            <fo:block font-size="8pt" color="black" text-decoration="underline">    NOMBRE : </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell                                                         
                            text-align="center" background-color="white" display-align="before"                    
                            number-columns-spanned="1"
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black"
                            border-top-color="white"
                            border-left-color="white">
                            <fo:block font-size="8pt" color="black" text-decoration="underline">    FECHA : </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                </fo:table-body>
            </fo:table>
        </xsl:template>
        <!-- fin tabla 10 -->
        
        
        
</xsl:stylesheet>


