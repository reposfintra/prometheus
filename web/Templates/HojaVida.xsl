<?xml version="1.0" encoding="iso-8859-1"?>
<!--
    Document   : HojaVida.xsl
    Created on : 21 abril del 2006
    Author     : Ing. Andrés Maturana De La Cruz
    Description:
        Definición de la plantilla del formato PDF de la impresión del Formato de Verificación de Hoja de Vida.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:template match="/">
      <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
         <fo:layout-master-set>
            <fo:simple-page-master master-name="simple" margin-right="0.3cm" margin-left="2cm" 
                margin-bottom="0.3cm" margin-top="0.5cm" page-width="27.94cm" page-height="21.59cm">
               <fo:region-body margin-top="0.3cm" margin-bottom="0.3cm" />

               <fo:region-before extent="0.3cm" />

               <fo:region-after extent="0.3cm" />
            </fo:simple-page-master>
         </fo:layout-master-set>

         <fo:page-sequence master-reference="simple">
            <fo:flow flow-name="xsl-region-body">
               <xsl:apply-templates select="raiz" />
            </fo:flow>
         </fo:page-sequence>
      </fo:root>
   </xsl:template>

   <xsl:template match="raiz">
      <!--  Información del Veto -->
      <fo:block font-size="10pt" font-weigth="bold"><xsl:value-of select="veto"/></fo:block>
      <!-- Encabezado -->
      <fo:block>
        <fo:table table-layout="fixed" >
            <fo:table-column column-width="4cm"/>
            <fo:table-column column-width="17.3cm"/>
            <fo:table-column column-width="4cm"/>
            
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white" number-rows-spanned="2" height="0.5cm">
                        <fo:block text-align="center">
                        <fo:external-graphic src="url(images/logo.bmp)" width="34px" height="20px" /></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" text-align="center"
                            start-indent="1px" padding-before="2px">TRANSPORTES SANCHEZ POLO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white" number-rows-spanned="2">
                        <fo:block font-size="8pt" font-weigth="bold" font-style="bold" font-family="sans-serif" text-align="center"
                            start-indent="1px" padding-before="3px">F-DR-005-20060308</fo:block>
                    </fo:table-cell>
                </fo:table-row> 
                
                <fo:table-row>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.2cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" text-align="center"
                            start-indent="1px" padding-before="2px">FORMATO DE VERIFICACION DE HOJA DE VIDA</fo:block>
                    </fo:table-cell>
                </fo:table-row> 
            </fo:table-body>
        </fo:table>
      </fo:block>       
       
      <fo:block  padding-before="3px" >
         <xsl:apply-templates select="vehiculo" />
      </fo:block>

      <fo:block padding-before="3px" >
         <xsl:apply-templates select="conductor" />
      </fo:block>

      <fo:block padding-before="3px" >
         <xsl:apply-templates select="propietario" />
      </fo:block>

      <fo:block padding-before="3px" >
         <xsl:apply-templates select="ctrl_reg" />
      </fo:block>
   </xsl:template>

   <!-- Template de la información del vehículo -->
   <xsl:template match="vehiculo">
      <xsl:variable name="img_vehiculo" select="vehiculo_img"/>
      <fo:block>
        <fo:table table-layout="fixed" height="3cm">
            <fo:table-column column-width="6.5cm"/>
            <fo:table-column column-width="2.7cm"/>
            <fo:table-column column-width="4cm"/>
            <fo:table-column column-width="3cm"/>
            <fo:table-column column-width="3.8cm"/>
            <fo:table-column column-width="2.3cm"/>
            <fo:table-column column-width="3cm"/>
            
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.2cm"
                        number-columns-spanned="7" >
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" text-align="center"
                            start-indent="1px" padding-before="2px">HOJA DE VIDA</fo:block>
                    </fo:table-cell>
                </fo:table-row>  
                <fo:table-row>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="#E4E4E4"  height="0.2cm"
                        number-columns-spanned="7" >
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" text-align="center"
                            start-indent="1px" padding-before="2px">INFORMACION DEL VEHICULO</fo:block>
                    </fo:table-cell>
                </fo:table-row>    
                <!-- 1ra fila -->  
                <fo:table-row>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white" number-rows-spanned="10" height="5cm">
                        <xsl:if test="$img_vehiculo='url(documentos/imagenes/prede.jpg)'">
                           <fo:block font-size="12pt" font-weigth="bold" font-style="reverse-normal" font-family="sans-serif" start-indent="1px" padding-before="60px" text-align="center">
                            NINGUNA IMAGEN</fo:block>
                           <fo:block font-size="12pt" font-weigth="bold" font-style="reverse-normal" font-family="sans-serif" start-indent="1px" padding-before="0px" text-align="center">
                            ENCONTRADA</fo:block>
                        </xsl:if>
                        <xsl:if test="$img_vehiculo!='url(documentos/imagenes/prede.jpg)'">
                            <fo:block text-align="center"><fo:external-graphic src="{$img_vehiculo}" width="241px" height="190px"/>                            
                            </fo:block>
                        </xsl:if>  
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white" height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">PLACA CABEZOTE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="placab"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px"># EJES</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="ejes"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">TIPO CARROCERIA</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="carroceria"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 2da fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">TIPO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="tipo"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">REG. NAL CARGA</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="reg_nal"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">VENCE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="venreg"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 3ra fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">CHASIS</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="chasis"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">PLACA TRAILER</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="platlr"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">NUMERO RIN</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="nrin"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 4ta fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">MOTOR</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="motor"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">CAPACIDAD TRAILER</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="captlr"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">GRADO RIESGO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="riesgo"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 5ta fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">COLOR</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="color"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">REG. NAL. GASES</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="gases"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">VENCE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="vengases"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 6ta fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">MARCA</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="marca"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">POLIZA ANDINA</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="polandina"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">VENCE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="venpolandina"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 7ta fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">MODELO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="modelo"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">S.O.A.T. #</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="soat"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">VENCE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="vensoat"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 8va fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">CAPACIDAD</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="capacidad"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">TARJETA EMPRESARIAL</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="empresa"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">VENCE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="venempresa"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 9na fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">EMP. AFILIADORA</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="afiliacion"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">TARJETA HABILITACION</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="tarhabil"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">VENCE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="ventarhabil"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 10ma fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">CIUDAD</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="ciudad"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">TARJETA PROPIEDAD</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="tarprop"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">VENCE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="ventarprop"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
      </fo:block>
   </xsl:template>  
   
    <!-- Template de la información del conductor -->
   <xsl:template match="conductor">
      <xsl:variable name="img_conductor" select="conductor_img"/>
      <fo:block>
        <fo:table table-layout="fixed" height="3cm">
            <fo:table-column column-width="6.5cm"/>
            <fo:table-column column-width="3cm"/>
            <fo:table-column column-width="3.4cm"/>
            <fo:table-column column-width="3cm"/>
            <fo:table-column column-width="3.8cm"/>
            <fo:table-column column-width="2.8cm"/>
            <fo:table-column column-width="2.8cm"/>
            
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="#E4E4E4"  height="0.2cm"
                        number-columns-spanned="7" >
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" text-align="center"
                            start-indent="1px" padding-before="2px">INFORMACION DEL CONDUCTOR</fo:block>
                    </fo:table-cell>
                </fo:table-row>    
                <!-- 1ra fila -->  
                <fo:table-row>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white" number-rows-spanned="10" height="5cm">
                        <xsl:if test="$img_conductor='url(documentos/imagenes/prede.jpg)'">
                           <fo:block font-size="12pt" font-weigth="bold" font-style="reverse-normal" font-family="sans-serif" start-indent="1px" padding-before="60px" text-align="center">
                            NINGUNA IMAGEN</fo:block>
                           <fo:block font-size="12pt" font-weigth="bold" font-style="reverse-normal" font-family="sans-serif" start-indent="1px" padding-before="0px" text-align="center">
                            ENCONTRADA</fo:block>
                        </xsl:if>
                        <xsl:if test="$img_conductor!='url(documentos/imagenes/prede.jpg)'">
                            <fo:block text-align="center"><fo:external-graphic src="{$img_conductor}" width="241px" height="190px"/>                            
                            </fo:block>
                        </xsl:if>                        
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white" height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        NOMBRES</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="nombres"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        CELULAR</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="celular"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        GRUPO SANGUINEO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="rh"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 2da fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        APELLIDOS</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="apellidos"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        CIUDAD / PAIS</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="ciudad"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        GRADO RIESGO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="riesgo"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 3ra fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        CEDULA</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="cedula"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        LIBRETA MILITAR</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="militar"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        LIBRETA TRIPULANTE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="tripulante"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 4ta fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        LUGAR EXPEDICION</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="expced"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        PASE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="pase"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        EPS</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="eps"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 5ta fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        LUGAR NACIMIENTO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="lugarnac"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        RESTRICCION PASE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                       <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="restrpase"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        No. AFILIACION</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="neps"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 6ta fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        FECHA NACIMIENTO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="fechanac"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        CATEGORIA PASE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="catpase"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        AFILIACION EPS</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="feceps"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 7ta fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        ESTADO CIVIL</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="estcivil"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        VENCIMIENTO PASE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="venpase"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        ARP</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="narp"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 8va fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        DIRECCION</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="direccion"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        CERTIFICADO JUDICIAL</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="njudicial"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        AFILIACION ARP</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="fecarp"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 9na fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        BARRIO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="barrio"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        PASAPORTE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="pasaporte"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        VENCE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="venpasaporte"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 10ma fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        TELEFONO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="telefono"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        VISA</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="nvisa"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        VENCE</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="venvisa"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
      </fo:block>
   </xsl:template>
   
   <!-- Información del Propietario y Tenedor -->
   <xsl:template match="propietario">
      <fo:block>
        <fo:table table-layout="fixed">
            <fo:table-column column-width="2.7cm"/>
            <fo:table-column column-width="3.4cm"/>
            <fo:table-column column-width="3cm"/>
            <fo:table-column column-width="3.5cm"/>
            <fo:table-column column-width="3cm"/>
            <fo:table-column column-width="3.4cm"/>
            <fo:table-column column-width="2.8cm"/>
            <fo:table-column column-width="3.5cm"/>
            
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="#E4E4E4"  height="0.2cm"
                        number-columns-spanned="8" >
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" text-align="center"
                            start-indent="1px" padding-before="2px">INFORMACION DEL PROPIETARIO Y TENEDOR</fo:block>
                    </fo:table-cell>
                </fo:table-row>
                 
                <fo:table-row>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.2cm"
                        number-columns-spanned="4" >
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" text-align="center"
                            start-indent="1px" padding-before="2px">PROPIETARIO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.2cm"
                        number-columns-spanned="4" >
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" text-align="center"
                            start-indent="1px" padding-before="2px">TENEDOR</fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 1ra Fila -->
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        NOMBRES</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="prop_nombre"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        CIUDAD / PAIS</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="prop_ciudad"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        NOMBRES</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="ten_nombre"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        CIUDAD / PAIS</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="ten_ciudad"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 2da Fila -->
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        APELLIDOS</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="prop_apellido"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        TELEFONO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="prop_tel"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        APELLIDOS</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="ten_apellido"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        TELEFONO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="ten_tel"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 3ra Fila -->
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        CEDULA</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="prop_nit"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        CELULAR</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="prop_movil"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        CEDULA</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="ten_cedula"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        CELULAR</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="ten_movil"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 4ta Fila -->
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        LUGAR EXPEDICION</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="prop_expced"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        SEDE PAGO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="prop_sede"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        LUGAR EXPEDICION</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="ten_expced"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 5ta Fila -->
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        DIRECCION</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="prop_direccion"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        GRADO RIESGO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        DIRECCION</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="ten_direccion"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
      </fo:block>
   </xsl:template>
   
   <!-- Control de Registro -->
   <xsl:template match="ctrl_reg">
      <xsl:variable name="img_huella1" select="huella1_img"/>
      <xsl:variable name="img_huella2" select="huella2_img"/>
      <xsl:variable name="img_firma" select="firma_img"/>
      <fo:block>
        <fo:table table-layout="fixed">
            <fo:table-column column-width="3.1cm"/>
            <fo:table-column column-width="2.1cm"/>
            <fo:table-column column-width="1.3cm"/>
            <fo:table-column column-width="2.3cm"/>
            <fo:table-column column-width="1.2cm"/>
            <fo:table-column column-width="2.3cm"/>
            <fo:table-column column-width="4.4cm"/>
            <fo:table-column column-width="4.4cm"/>
            <fo:table-column column-width="4.2cm"/>
            
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="#E4E4E4"  height="0.2cm"
                        number-columns-spanned="9" >
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" text-align="center"
                            start-indent="1px" padding-before="2px">CONTROL DE REGISTRO</fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 1ra fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px" text-align="center">
                        FECHA DE CREACION</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="creation_date"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px" text-align="center">
                        USUARIO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="creation_user"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px" text-align="center">
                        AGENCIA</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="creation_agency"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px" text-align="center">
                        HUELLA 1 <xsl:value-of select="leyenda_1"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px" text-align="center">
                        HUELLA 2 <xsl:value-of select="leyenda_2"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px" text-align="center">
                        FIRMA CONDUCTOR</fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <!-- 1ra fila -->  
                <fo:table-row>
                   <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px" text-align="center">
                        FECHA DE ACTUALIZACION</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="last_update"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px" text-align="center">
                        USUARIO</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="user_update"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-weigth="bold" font-style="oblique" font-family="sans-serif" start-indent="1px" padding-before="4px" text-align="center">
                        AGENCIA</fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <fo:block font-size="6.5pt" font-family="sans-serif" start-indent="1px" padding-before="4px">
                        <xsl:value-of select="agency_update"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <xsl:if test="$img_huella1='url(documentos/imagenes/none.jpg)'">
                           <fo:block font-size="12pt" font-weigth="bold" font-style="reverse-normal" font-family="sans-serif" start-indent="1px" padding-before="10px" text-align="center">
                            NINGUNA</fo:block>
                        </xsl:if>
                        <xsl:if test="$img_huella1!='url(documentos/imagenes/none.jpg)'">
                            <fo:block text-align="center"><fo:external-graphic src="{$img_huella1}" width="45px" height="50px"/>                            
                            </fo:block>
                        </xsl:if>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <xsl:if test="$img_huella2='url(documentos/imagenes/none.jpg)'">
                           <fo:block font-size="12pt" font-weigth="bold" font-style="reverse-normal" font-family="sans-serif" start-indent="1px" padding-before="10px" text-align="center">
                            NINGUNA</fo:block>
                        </xsl:if>
                        <xsl:if test="$img_huella2!='url(documentos/imagenes/none.jpg)'">
                            <fo:block text-align="center"><fo:external-graphic src="{$img_huella2}" width="45px" height="50px"/>                            
                            </fo:block>
                        </xsl:if>
                    </fo:table-cell>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="left" vertical-align="middle"
                        background-color="white"  height="0.5cm">
                        <xsl:if test="$img_firma='url(documentos/imagenes/none.jpg)'">
                           <fo:block font-size="12pt" font-weigth="bold" font-style="reverse-normal" font-family="sans-serif" start-indent="1px" padding-before="10px" text-align="center">
                            NINGUNA</fo:block>
                        </xsl:if>
                        <xsl:if test="$img_firma!='url(documentos/imagenes/none.jpg)'">
                            <fo:block text-align="center"><fo:external-graphic src="{$img_firma}" width="45px" height="50px"/>                            
                            </fo:block>
                        </xsl:if>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
      </fo:block>
   </xsl:template>    
</xsl:stylesheet>

