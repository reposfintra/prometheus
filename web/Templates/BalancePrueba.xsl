<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:param name="periodo"/>   
<xsl:param name="page-count"/>  
<xsl:param name="fechaactual"/>     
<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  		<fo:layout-master-set>
			<fo:simple-page-master master-name="simple"
				 page-height="29.7cm"
				 page-width="21cm"
				 margin-top="0.25cm"
				 margin-bottom="0.5cm"
			         margin-left="1cm"
				 margin-right="1cm" initial-page-number="1">						                            
                          <fo:region-body margin-top="2.5cm" margin-bottom="2cm"/>
                          <fo:region-before extent="2.5cm"/>                          
			</fo:simple-page-master>
  		</fo:layout-master-set>
  
		<fo:page-sequence master-reference="simple">
                    <fo:static-content flow-name="xsl-region-before">
                    <fo:table table-layout="fixed" height="2.5cm">
		<fo:table-column column-width="18cm"/>		
		<fo:table-body>
			<fo:table-row>				
                                <fo:table-cell
					border-width="1pt"
					border-style="solid"
					border-bottom-color="black"
                                        border-top-color="white"
                                        border-left-color="white"
                                        border-right-color="white"
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">
					<fo:table table-layout="fixed">
                                        <fo:table-column column-width="6cm"/>
                                        <fo:table-column column-width="6cm"/>
                                        <fo:table-column column-width="6cm"/>
						<fo:table-body >

						<fo:table-row>
					                    <fo:table-cell									                                                                       
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="12pt" color="black" padding-before="0.5cm">TRANSPORTE SANCHEZ POLO</fo:block>
								</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
					                    <fo:table-cell									
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="12pt" color="black">BALANCE DE PRUEBA</fo:block>
								</fo:table-cell>
						</fo:table-row>
                                                
                                                <!-- Perido-->
                                                <fo:table-row>
					                    <fo:table-cell									
									text-align="left" vertical-align="middle"
									number-columns-spanned="1" number-rows-spanned="1" background-color="white">
									<fo:block font-size="8pt" color="black"><xsl:value-of select="$fechaactual"/></fo:block>
							    </fo:table-cell>
                                                
                                                            <fo:table-cell									
									text-align="center" vertical-align="middle"
									number-columns-spanned="1" number-rows-spanned="1" background-color="white">
									<fo:block font-size="8pt" color="black">Periodo : <xsl:value-of select="$periodo"/></fo:block>
							    </fo:table-cell>
                                                            <fo:table-cell									
									text-align="right" vertical-align="middle"
									number-columns-spanned="1" number-rows-spanned="1" background-color="white">
									<fo:block font-size="8pt" color="black">Pagina <fo:page-number/></fo:block>
							    </fo:table-cell>                                                            
						</fo:table-row>
                                                 <!-- Fin Fila Perido-->
					</fo:table-body>
					</fo:table>
			    </fo:table-cell>		
			</fo:table-row>
                        
                        <!-- Cabeceras de las Filas -->
                        <fo:table-row>				
                                <fo:table-cell
					border-width="1.5pt"
					border-style="solid"
					border-bottom-color="black"
                                        border-left-color="white"
                                        border-top-color="white"
                                        border-right-color="white"
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">
					<fo:table table-layout="fixed">
                                        <fo:table-column column-width="2cm"/>
                                        <fo:table-column column-width="4cm"/>
                                        <fo:table-column column-width="3cm"/>
                                        <fo:table-column column-width="3cm"/>
                                        <fo:table-column column-width="3cm"/>
                                        <fo:table-column column-width="3cm"/>
						<fo:table-body>
						                                                
                                                <!-- Cabecera-->
                                                <fo:table-row>
					                    <fo:table-cell									
									text-align="left" vertical-align="middle"
									number-columns-spanned="1" number-rows-spanned="1" background-color="white">
									<fo:block font-size="8pt" color="black">Codigo</fo:block>
							    </fo:table-cell>
                                                
                                                            <fo:table-cell									
									text-align="left" vertical-align="middle"
									number-columns-spanned="1" number-rows-spanned="1" background-color="white">
									<fo:block font-size="8pt" color="black">Descripcion</fo:block>
							    </fo:table-cell>
                                                            
                                                            <fo:table-cell									
									text-align="right" vertical-align="middle"
									number-columns-spanned="1" number-rows-spanned="1" background-color="white">
									<fo:block font-size="8pt" color="black">Saldo Anterior</fo:block>
							    </fo:table-cell>
                                                
                                                            <fo:table-cell									
									text-align="right" vertical-align="middle"
									number-columns-spanned="1" number-rows-spanned="1" background-color="white">
									<fo:block font-size="8pt" color="black">Movto Deb</fo:block>
							    </fo:table-cell>
                                                            
                                                            <fo:table-cell									
									text-align="right" vertical-align="middle"
									number-columns-spanned="1" number-rows-spanned="1" background-color="white">
									<fo:block font-size="8pt" color="black">Movto Cre</fo:block>
							    </fo:table-cell>
                                                
                                                            <fo:table-cell									
									text-align="right" vertical-align="middle"
									number-columns-spanned="1" number-rows-spanned="1" background-color="white">
									<fo:block font-size="8pt" color="black">Saldo Actual</fo:block>
							    </fo:table-cell>
						</fo:table-row>
                                                 <!-- Fin Fila Cabecera-->
					</fo:table-body>
					</fo:table>
			    </fo:table-cell>		
			</fo:table-row>
                        
		</fo:table-body>
		</fo:table>
                                </fo:static-content>
                
                                <fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="raiz"/>
				</fo:flow>                                                                
		</fo:page-sequence>
	</fo:root>
</xsl:template>

        <xsl:template match="raiz">	                
                <xsl:apply-templates select="CuentasBalance"/>
                <xsl:apply-templates select="Ingresos"/>
                
                <!-- TOTAL GENERAL -->
                
                <fo:table table-layout="fixed" height="1cm" padding-before="0.5cm">
                    <fo:table-column column-width="2cm"/>
                    <fo:table-column column-width="4cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>		
                    <fo:table-body>
                    <fo:table-row>				
                                    <fo:table-cell
                                            border-width="1pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            border-top-color="black"
                                            border-bottom-color="black"                                                                                        
                                            number-columns-spanned="2"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">TOTAL GENERAL</fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="1pt"
                                            border-style="solid"
                                            border-color="white" 
                                            border-top-color="black" 
                                            border-bottom-color="black"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoAnteriorT"/></fo:block>
                                     </fo:table-cell>	
                                     
                                     <fo:table-cell
                                            border-width="1pt"
                                            border-style="solid"
                                            border-color="white" 
                                            border-top-color="black"
                                            border-bottom-color="black"                                             
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movDebT"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="1pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-top-color="black"
                                            border-bottom-color="black"                                              
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movCreT"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="1pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-top-color="black"
                                            border-bottom-color="black"                                              
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoActualT"/></fo:block>
                                     </fo:table-cell>				
                            </fo:table-row>
                    
                    </fo:table-body> 
            </fo:table>          
                
                
                
                <!-- FIN GENERAL -->
                
                
                
                		
	</xsl:template>

	<xsl:template match="CuentasBalance">
           <fo:block break-after="page">
                            <xsl:apply-templates select="listaN"/>
           </fo:block>
           
        </xsl:template>
        
        <xsl:template match="listaN">
          <fo:table table-layout="fixed" height="0.2cm">
                    <fo:table-column column-width="18cm"/>		
                    <fo:table-body>
                            <fo:table-row height="0.1cm">				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                           
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" font-weight="bold">Tipo CUENTAS DE BALANCE</fo:block>
                                            </fo:table-cell>					
                            </fo:table-row> 
                     </fo:table-body> 
                     </fo:table>
           <fo:block>
                            <xsl:apply-templates select="elementosN"/>
           </fo:block>
           <!-- Totales Cuentas de Balance -->
           
           <fo:table table-layout="fixed" height="0.2cm">
                    <fo:table-column column-width="2cm"/>
                    <fo:table-column column-width="4cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>		
                    <fo:table-body>
                    <fo:table-row>				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            border-top-color="black"                                            
                                            number-columns-spanned="2"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">SubTotal Tipo</fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white" 
                                            border-top-color="black"                                             
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoAnteriorB"/></fo:block>
                                     </fo:table-cell>	
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white" 
                                            border-top-color="black"                                             
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movDebB"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-top-color="black"                                              
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movCreB"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-top-color="black"                                              
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoActualB"/></fo:block>
                                     </fo:table-cell>				
                            </fo:table-row>
                    
                    </fo:table-body> 
            </fo:table>          
        </xsl:template>
                  
          
        <xsl:template match="Ingresos"> 
           
	   <fo:block>
			   <xsl:apply-templates select="listaA"/>
           </fo:block>	       
	</xsl:template>
        
        <xsl:template match="listaA">
        <fo:table table-layout="fixed" height="0.5cm" padding-before="0.3cm">
                    <fo:table-column column-width="18cm"/>		
                    <fo:table-body>
                            <fo:table-row height="0.1cm">				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                           
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" font-weight="bold">Tipo INGRESOS</fo:block>
                                            </fo:table-cell>					
                            </fo:table-row> 
                     </fo:table-body> 
                     </fo:table>
        
           <fo:block>
                            <xsl:apply-templates select="elementosA"/>
           </fo:block> 
           
           <fo:table table-layout="fixed" height="0.1cm">
                    <fo:table-column column-width="2cm"/>
                    <fo:table-column column-width="4cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>		
                    <fo:table-body>
                    <fo:table-row>				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            border-top-color="black"                                            
                                            number-columns-spanned="2"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">SubTotal Tipo</fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white" 
                                            border-top-color="black"                                             
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoAnteriorTA"/></fo:block>
                                     </fo:table-cell>	
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white" 
                                            border-top-color="black"                                             
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movDebTA"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-top-color="black"                                              
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movCreTA"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-top-color="black"                                              
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoActualTA"/></fo:block>
                                     </fo:table-cell>				
                            </fo:table-row>
                    
                    </fo:table-body> 
            </fo:table>                    
        </xsl:template>
    
	
		
                
                <!-- Elementos del Gasto Balance -->
                
                
                <xsl:template match="elementosN"> 
                    <fo:table table-layout="fixed" height="0.2cm">
                    <fo:table-column column-width="18cm"/>		                    
                    <fo:table-body>                                                                                                                              
                            
                            <!--Elemento de Gasto-->
                            <fo:table-row>				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="2"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">Elemento de Gasto   <xsl:value-of select="codigoE"/>     <xsl:value-of select="descripcionE"/></fo:block>
                                            </fo:table-cell>					
                            </fo:table-row>
                            
                            
                     </fo:table-body>
                    </fo:table>
                    <fo:block>
                        <xsl:apply-templates select="unidadesN"/>
                    </fo:block>
                    <fo:table table-layout="fixed" height="0.2cm">
                        <fo:table-column column-width="2cm"/>
                        <fo:table-column column-width="4cm"/>
                        <fo:table-column column-width="3cm"/>
                        <fo:table-column column-width="3cm"/>
                        <fo:table-column column-width="3cm"/>
                        <fo:table-column column-width="3cm"/>	                    		                    
                        <fo:table-body>
                            <fo:table-row>				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="2"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">SubTotal Elemento</fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoAnteriorE"/></fo:block>
                                     </fo:table-cell>	
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movDebE"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movCreE"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoActualE"/></fo:block>
                                     </fo:table-cell>				
                            </fo:table-row>
                    
                        </fo:table-body>
                    </fo:table>                                        
                </xsl:template> 
                                                
                <!-- Fin del Elemento del Gasto Balance -->     
                
                <!-- Unidad de Negocio -->  
                <xsl:template match="unidadesN"> 
                <fo:table table-layout="fixed" height="0.2cm">
                    <fo:table-column column-width="2cm"/>
                    <fo:table-column column-width="4cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>		
                    <fo:table-body>
                            <fo:table-row>				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="5"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">Unidad de Negocio   <xsl:value-of select="codigoU"/>     <xsl:value-of select="descripcionU"/></fo:block>
                                            </fo:table-cell>					
                            </fo:table-row>
                                                        
                            <xsl:apply-templates select="cuentas"/>
                            <fo:table-row>				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="2"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold">SubTotal Unidad de Negocio</fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoAnteriorU"/></fo:block>
                                     </fo:table-cell>

                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movDebU"/></fo:block>
                                     </fo:table-cell>

                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movCreU"/></fo:block>
                                     </fo:table-cell>

                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoActualU"/></fo:block>
                                     </fo:table-cell>									
                            </fo:table-row>
                            
                       </fo:table-body>
                      </fo:table> 
                                          
                                               
                </xsl:template>
            
                
                
                <!-- Fin Unidad de Negocio -->
                                                    
                
                
                <xsl:template match="cuentas">                   
                            <fo:table-row height="0.1cm">				                                    				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6pt" color="black" padding-before="0.1cm">  <xsl:value-of select="codigo"/>     </fo:block>
                                            </fo:table-cell>
                                            
                                            <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6pt" color="black" padding-before="0.1cm">  <xsl:value-of select="descripcion"/>     </fo:block>
                                            </fo:table-cell>
                                            
                                            <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6pt" color="black" padding-before="0.1cm">  <xsl:value-of select="saldoanterior"/> </fo:block>
                                            </fo:table-cell>
                                            
                                            <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6pt" color="black" padding-before="0.1cm">  <xsl:value-of select="movdeb"/>     </fo:block>
                                            </fo:table-cell>
                                            
                                            <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6pt" color="black" padding-before="0.1cm">  <xsl:value-of select="movcre"/>     </fo:block>
                                            </fo:table-cell>
                                            
                                            <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6pt" color="black" padding-before="0.1cm">  <xsl:value-of select="saldoactual"/>     </fo:block>
                                            </fo:table-cell>					
                            </fo:table-row>                                               
                </xsl:template> 
                
                
                
                 <!-- Elementos del Gasto Ingresos -->
                
                
                <xsl:template match="elementosA"> 
                    <fo:table table-layout="fixed" height="0.2cm">
                    <fo:table-column column-width="9cm"/>
                    <fo:table-column column-width="9cm"/>		                    
                    <fo:table-body>                                                                                                                              
                            
                            <!--Elemento de Gasto-->
                            <fo:table-row>				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">Elemento de Gasto   <xsl:value-of select="codigoEA"/></fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="descripcionEA"/></fo:block>
                                     </fo:table-cell>						
                            </fo:table-row>
                            
                            
                     </fo:table-body>
                    </fo:table>
                    <fo:block>
                        <xsl:apply-templates select="unidadesA"/>
                    </fo:block>
                    <fo:table table-layout="fixed" height="0.2cm">
                        <fo:table-column column-width="2cm"/>
                        <fo:table-column column-width="4cm"/>
                        <fo:table-column column-width="3cm"/>
                        <fo:table-column column-width="3cm"/>
                        <fo:table-column column-width="3cm"/>
                        <fo:table-column column-width="3cm"/>	                    		                    
                        <fo:table-body>
                            <fo:table-row>				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="2"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">SubTotal Elemento</fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoAnteriorEA"/></fo:block>
                                     </fo:table-cell>	
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movDebEA"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movCreEA"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoActualEA"/></fo:block>
                                     </fo:table-cell>				
                            </fo:table-row>
                    
                        </fo:table-body>
                    </fo:table>
                </xsl:template> 
                                                
                <!-- Fin del Elemento del Gasto Balance -->     
                
                <!-- Unidad de Negocio -->  
                <xsl:template match="unidadesA"> 
                <fo:table table-layout="fixed" height="0.2cm">
                    <fo:table-column column-width="2cm"/>
                    <fo:table-column column-width="4cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>		
                    <fo:table-body>
                            <fo:table-row>				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="3"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">Unidad de Negocio   <xsl:value-of select="codigoUA"/></fo:block>
                                     </fo:table-cell>
                                     
                                      <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="2"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="descripcionUA"/></fo:block>
                                     </fo:table-cell>					
                            </fo:table-row>
                                                        
                            <xsl:apply-templates select="cuentasA"/>
                            
                            <fo:table-row>				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="2"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold">SubTotal Unidad de Negocio</fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoAnteriorUA"/></fo:block>
                                     </fo:table-cell>	
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movDebUA"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="movCreUA"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="saldoActualUA"/></fo:block>
                                     </fo:table-cell>									
                            </fo:table-row>
                            
                       </fo:table-body>
                      </fo:table>                    
                </xsl:template>
            
                <!-- Fin Unidad de Negocio -->                                       
		
                <xsl:template match="cuentasA">                   
                            <fo:table-row height="0.1cm">				                                    				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6pt" color="black" padding-before="0.1cm">  <xsl:value-of select="codigoA"/>     </fo:block>
                                            </fo:table-cell>
                                            
                                            <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6pt" color="black" padding-before="0.1cm">  <xsl:value-of select="descripcionA"/>     </fo:block>
                                            </fo:table-cell>
                                            
                                            <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6pt" color="black" padding-before="0.1cm">  <xsl:value-of select="saldoanteriorA"/> </fo:block>
                                            </fo:table-cell>
                                            
                                            <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6pt" color="black" padding-before="0.1cm">  <xsl:value-of select="movdebA"/>     </fo:block>
                                            </fo:table-cell>
                                            
                                            <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6pt" color="black" padding-before="0.1cm">  <xsl:value-of select="movcreA"/>     </fo:block>
                                            </fo:table-cell>
                                            
                                            <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                            
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6pt" color="black" padding-before="0.1cm">  <xsl:value-of select="saldoactualA"/>     </fo:block>
                                            </fo:table-cell>					
                            </fo:table-row>                                               
                </xsl:template> 
                 
	 
	
        
        
</xsl:stylesheet>


