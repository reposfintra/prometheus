<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  		<fo:layout-master-set>
			<fo:simple-page-master master-name="simple"
				 page-height="29.7cm"
							  page-width="21cm"
							  margin-top="0.25cm"
							  margin-bottom="0.5cm"
							  margin-left="1.5cm"
							  margin-right="1.5cm">
			
			  <fo:region-body margin-top="0.5cm" margin-bottom="0.5cm"/>
			  <fo:region-before extent="1cm"/>
			  <fo:region-after extent="1.5cm"/>
			</fo:simple-page-master>
  		</fo:layout-master-set>
  
		<fo:page-sequence master-reference="simple">
				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="raiz"/>
				</fo:flow>
		</fo:page-sequence>
	</fo:root>
        </xsl:template>

        <xsl:template match="raiz">	
		<xsl:apply-templates select="data"/>
	</xsl:template>

	<xsl:template match="data">
           <fo:block>
                    <xsl:apply-templates select="cabecera"/>
           </fo:block>
           <fo:block>
                    <xsl:apply-templates select="titulo"/>
           </fo:block>
           <fo:block>
                    <xsl:apply-templates select="datos_viajes"/>
           </fo:block>	
	</xsl:template>
	
		<xsl:template match="cabecera"> 
                    <fo:table table-layout="fixed" height="3cm">
                    <fo:table-column column-width="3.19cm"/>
                    <fo:table-column column-width="14.25cm"/>		
                    <fo:table-body>
			<fo:table-row>
				<fo:table-cell
					border-width="1pt"
					border-style="solid"
					border-color="black"
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">
					<fo:external-graphic src="url(images/logo.bmp)"/>
				</fo:table-cell>
                                <fo:table-cell
					border-width="1pt"
					border-style="solid"
					border-color="black"
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">
					<fo:table table-layout="fixed">
                                        <fo:table-column column-width="14.25cm"/>
						<fo:table-body border-color="black" border-style="solid" border-bottom-color="white">

						<fo:table-row>
					                    <fo:table-cell
									border-width="0.5pt"
									border-style="none"
									border-color="black"                                                                        
									text-align="center" vertical-align="middle"
									number-columns-spanned="1" number-rows-spanned="1" background-color="white">
									<fo:block font-size="12pt" color="black" padding-before="0.5cm">TRANSPORTE SANCHEZ POLO</fo:block>
								</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
					                    <fo:table-cell
									border-width="0.5pt"
									border-style="none"
									border-color="black"
									text-align="center" vertical-align="middle"
									number-columns-spanned="1" number-rows-spanned="1" background-color="white">
									<fo:block font-size="12pt" color="black">REPORTE FACTURAS DESTINATARIOS</fo:block>
								</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
					</fo:table>
			    </fo:table-cell>
			</fo:table-row>
		</fo:table-body>
		</fo:table>
	</xsl:template >
	 
        <xsl:template match="titulo"> 
                    <fo:table table-layout="fixed" height="0.5cm">
                    <fo:table-column column-width="17.44cm"/>		
                    <fo:table-body>
			<fo:table-row>
						
                                <fo:table-cell
                                            border-width="0.0pt"                                                                      
                                            text-align="center" vertical-align="middle"
                                            background-color="white">
                                            <fo:block font-size="12pt" color="black" padding-before="0.5cm"><xsl:value-of select="tipo"/> No. <xsl:value-of select="numero"/></fo:block>
                                    </fo:table-cell>
						
					  
			</fo:table-row>
		</fo:table-body>
		</fo:table>
	</xsl:template >
        
	<xsl:template match="datos_viajes"> 
        
            <fo:table table-layout="fixed"  height="0.8cm" border-color="black" border-style="solid">
		<fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>


   		<fo:table-body>
	  		<fo:table-row background-color="lightgray" height="0.5cm" border-style="solid" border-color="black" >
	    		<fo:table-cell										
					text-align="left" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" >
					<fo:block font-size="7pt" padding-before="0.1cm" margin-left='0.125cm'>REPORTE FACTURAS DESTINATARIOS</fo:block>
	    		</fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                        <fo:table-cell									
					text-align="left" vertical-align="middle"
					number-columns-spanned="8" number-rows-spanned="1" background-color="white">
					<fo:block>
                                        
                                        <fo:table table-layout="fixed"  height="0.8cm" border-width=".1mm"  >
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-body>
                                        
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell 
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold"  color="#64646F" margin-left='0.125cm' padding-before="0.075cm">DESTINATARIO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="8" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"><xsl:value-of select="destinatario"/></fo:block>
                                        </fo:table-cell>                                        
                                        
                                        </fo:table-row>
                                        
                                        
                                        <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold"  color="#64646F" margin-left='0.125cm' padding-before="0.075cm">TIPO DE DOCUMENTO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="8" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="tipo_documento"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="5" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold"  color="#64646F" padding-before="0.075cm"> DOCUMENTO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="documento"/></fo:block>
                                        </fo:table-cell>
                                       
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                        <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="8" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold"  color="#64646F" margin-left='0.125cm' padding-before="0.075cm">TIPO DOCUMENTO RELACIONADO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm" > <xsl:value-of select="tipo_doc_rel"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="5" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F">DOCUMENTO RELACIONADO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="doc_rel"/></fo:block>
                                        </fo:table-cell>
                                       
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                          <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" margin-left='0.125cm' color="#64646F">FECHA CUMPLIDO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm" > <xsl:value-of select="fecha_cum"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm"  color="#64646F">CANTIDAD CUMPLIDA</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="cantidad_cum"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm"  color="#64646F">DISCREPANCIA</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="discrepancia"/></fo:block>
                                        </fo:table-cell>
                                       
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                        <!--*******************FILA OBSERVACIONES******************-->
                                        <fo:table-row height="0.5cm">                                   
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold"  color="#64646F" margin-left='0.125cm' padding-before="0.075cm">OBSERVACIONES</fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"><xsl:value-of select="observacion"/></fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA OBSERVACIONES*****************-->
                                        
                                </fo:table-body>
                            </fo:table>                                                                                                                       
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template >
</xsl:stylesheet>