<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  		<fo:layout-master-set>
			<fo:simple-page-master master-name="simple"
				margin-right="0.1cm"
				margin-left="0.1cm"
				margin-bottom="0.5cm"
				margin-top="0.5cm"
				page-width="28cm"
				page-height="24cm"
			>
			  <fo:region-body margin-top="0.5cm" margin-bottom="0.5cm"/>
			  <fo:region-before extent="1cm"/>
			  <fo:region-after extent="1.5cm"/>
			</fo:simple-page-master>
  		</fo:layout-master-set>
  
		<fo:page-sequence master-reference="simple">
				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="raiz"/>
				</fo:flow>
		</fo:page-sequence>
	</fo:root>
</xsl:template>

<xsl:template match="raiz">	
		<xsl:apply-templates select="data"/>
	</xsl:template>

	<xsl:template match="data">
       <fo:block>
			<xsl:apply-templates select="tabla1"/>
       </fo:block>
	   <fo:block>
			<xsl:apply-templates select="tabla2"/>
       </fo:block>
	   <fo:block>
			<xsl:apply-templates select="tabla3"/>
       </fo:block>

         <fo:block>
         <fo:table table-layout="fixed"  border-style="none"  border-color="white" border-width=".1mm" height="0.5cm" >
         <fo:table-column column-width="12cm"/>
			<fo:table-column column-width="12cm"/>
			<fo:table-body font-family="sans-serif" font-weight="normal" font-size="6pt" >
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="none"
						border-color="black"
						text-align="left" vertical-align="middle"
						height="0.5cm"
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="10pt" ></fo:block>
                           </fo:table-cell>
	  			</fo:table-row>
			</fo:table-body>
		</fo:table>
         </fo:block>

         <fo:block-container height="22cm" width="26cm">
			<xsl:apply-templates select="valores"/>
	   </fo:block-container>

         <fo:block>
         <fo:table table-layout="fixed"  border-style="solid" border-bottom-color="white" border-left-color="white" border-right-color="white" height="2cm">
         <fo:table-column column-width="12cm"/>
			<fo:table-column column-width="12cm"/>
			<fo:table-body font-family="sans-serif" font-weight="normal" font-size="6pt">
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="none"
						border-color="white"
						text-align="left" vertical-align="middle"
						height="0.5cm"
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="10pt" ></fo:block>
                           </fo:table-cell>
	  			</fo:table-row>
			</fo:table-body>
		</fo:table>
         </fo:block>

		<fo:table table-layout="fixed"  border-style="none"  border-color="white" height="1cm" >
			<fo:table-column column-width="12cm"/>
			<fo:table-column column-width="12cm"/>
			<fo:table-body font-family="sans-serif" font-weight="normal" font-size="6pt" >
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="none"
						border-color="black"
						text-align="left" vertical-align="middle"
						height="1.6cm"
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="10pt" > OBSERVACIONES :  </fo:block>
					</fo:table-cell>
	  			</fo:table-row>
			</fo:table-body>
		</fo:table>			

	</xsl:template>
	
		<xsl:template match="tabla1"> 
		<fo:table table-layout="fixed" height="3cm">
		<fo:table-column column-width="3.19cm"/>
		<fo:table-column column-width="17cm"/>
		<fo:table-column column-width="6cm"/>
		<fo:table-body>
			<fo:table-row>
				<fo:table-cell
					border-width="1pt"
					border-style="solid"
					border-color="black"
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">
					<fo:external-graphic src="url(images/logo.bmp)"/>
				</fo:table-cell>
                        <fo:table-cell
					border-width="1pt"
					border-style="solid"
					border-color="black"
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">
					<fo:table table-layout="fixed">
                                    <fo:table-column column-width="17cm"/>
						<fo:table-body>

						<fo:table-row>
					                    <fo:table-cell
									border-width="0.5pt"
									border-style="none"
									border-color="black"
									text-align="center" vertical-align="middle"
									number-columns-spanned="11" number-rows-spanned="1" background-color="white">
									<fo:block font-size="12pt" color="black" padding-before="0.5cm"> TRANSPORTE SANCHEZ POLO</fo:block>
								</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
					                    <fo:table-cell
									border-width="0.5pt"
									border-style="none"
									border-color="black"
									text-align="center" vertical-align="middle"
									number-columns-spanned="11" number-rows-spanned="1" background-color="white">
									<fo:block font-size="12pt" color="black"> HOJA DE CONTROL DE VIAJE ( HVC )</fo:block>
								</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
					</fo:table>
			    </fo:table-cell>


				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black"
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">					
					<fo:table table-layout="fixed">

						
						<fo:table-body>
						
							<fo:table-row>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="11" number-rows-spanned="1" background-color="gray">
									<fo:block font-size="12pt" color="white"> MANIFIESTO DE CARGA </fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" > <xsl:value-of select="codigor"/> </fo:block>
								</fo:table-cell>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" >  <xsl:value-of select="codigoe"/> </fo:block>
								</fo:table-cell>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="5" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" >  <xsl:value-of select="codigoc"/>  </fo:block>
								</fo:table-cell>
							</fo:table-row>
						
							<fo:table-row>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" > CODIGO REGIONAL </fo:block>
								</fo:table-cell>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" > CODIGO DE EMPRESA </fo:block>
								</fo:table-cell>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="5" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" > CODIGO NUMERICO CONSECUTIVO </fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
			    </fo:table-cell>
			</fo:table-row>
		</fo:table-body>
		</fo:table>
		</xsl:template >
	 
	<xsl:template match="tabla2"> 
	<fo:table table-layout="fixed"  height="0.8cm" border-color="white" border-width=".1mm"  >
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>


   		<fo:table-body>
	  		<fo:table-row>
	    		<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > CLIENTE REMITENTE : </fo:block>
	    		</fo:table-cell>
				<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					border-top-color="white"
					text-align="left" vertical-align="middle"
					number-columns-spanned="8" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="cliente"/></fo:block>
	    		</fo:table-cell>

			</fo:table-row>
		</fo:table-body>
    </fo:table>
	</xsl:template >

	<xsl:template match="tabla3">
	<fo:table table-layout="fixed" height="1cm" border-color="black" border-style="solid">
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>

   		<fo:table-body>

	  		<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="center" vertical-align="middle"
					number-columns-spanned="18" number-rows-spanned="1" background-color="white" height="0.5cm">
	      			<fo:block font-size="7pt" >ORDEN DE TRABAJO (OT)</fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="10" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="ot"/> </fo:block>
			</fo:table-cell>
			<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="10" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" >ORIGEN CARGA</fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="10" number-rows-spanned="1" background-color="white" height="0.5cm">
					<fo:block font-size="7pt" ><xsl:value-of select="origen"/> </fo:block>
			</fo:table-cell>
                  </fo:table-row>



                  <fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="center" vertical-align="middle"
					number-columns-spanned="18" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" >ORDEN DE COMPRA (OC) </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="10" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" ><xsl:value-of select="oc"/> </fo:block>
			</fo:table-cell>
			<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="10" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" >DESTINO FINAL DE LA CARGA  </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="10" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" ><xsl:value-of select="destino"/> </fo:block>
			</fo:table-cell>
                      </fo:table-row>
	  	</fo:table-body>
    </fo:table>
    </xsl:template >

	<xsl:template match="valores">
	  <fo:table table-layout="fixed"  padding-before="0.cm" font-size="10pt" border-style="none"  height="6.5cm">
                  <fo:table-column column-width="4cm"/>
                  <fo:table-column column-width="0.7cm"/>
                  <fo:table-column column-width="0.7cm"/>
                  <fo:table-column column-width="0.7cm"/>
                  <fo:table-column column-width="1.5cm"/>
                  <fo:table-column column-width="1.5cm"/>
                  <fo:table-column column-width="4.0cm"/>
                  <fo:table-column column-width="1.5cm"/>
			<fo:table-column column-width="0.7cm"/>
			<fo:table-column column-width="0.7cm"/>
			<fo:table-column column-width="0.9cm"/>
			<fo:table-column column-width="1.5cm"/>
			<fo:table-column column-width="1.5cm"/>
			<fo:table-column column-width="3.5cm"/>
			<fo:table-column column-width="3cm"/>

		<fo:table-body font-family="sans-serif" font-weight="normal" font-size="7pt" >
				<fo:table-row >
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="none" background-color="white">
						<fo:block text-align="center" color="black" ></fo:block>
					</fo:table-cell>
                              <fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white" number-columns-spanned="3" number-rows-spanned="1">
						<fo:block text-align="center" color="black" >FECHA</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white" number-columns-spanned="4" number-rows-spanned="1">
						<fo:block text-align="center" color="black" >PRECINTOS</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white" number-columns-spanned="3" number-rows-spanned="1">
						<fo:block text-align="center" color="black" >MUESTRA</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white" number-rows-spanned="2">
						<fo:block text-align="center" color="black" >PLACA CABEZOTE</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white" number-rows-spanned="2">
						<fo:block text-align="center" color="black" >PLACA TRAILER</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white" number-rows-spanned="2">
						<fo:block text-align="center" color="black" >TIPO DE TRAILER</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white" number-rows-spanned="2">
						<fo:block text-align="center" color="black" >FIRMAS Y SELLOS</fo:block>
					</fo:table-cell>
				</fo:table-row>

                        <fo:table-row >
                              <fo:table-cell  border-width="0.5pt" border-color="black" border-style="none" background-color="white">
						<fo:block text-align="center" color="black" ></fo:block>
					</fo:table-cell>
                              <fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white">
						<fo:block text-align="center" color="black" >DD</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white">
						<fo:block text-align="center" color="black" >MM</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white">
						<fo:block text-align="center" color="black" >AA</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white">
						<fo:block text-align="center" color="black" >LLEGADA</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white">
						<fo:block text-align="center" color="black" >ESTADO</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white">
						<fo:block text-align="center" color="black" >SALIDA</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white">
						<fo:block text-align="center" color="black" >ESTADO</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white">
						<fo:block text-align="center" color="black" >SI</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white">
						<fo:block text-align="center" color="black" >NO</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="white">
						<fo:block text-align="center" color="black" >CANT</fo:block>
					</fo:table-cell>
                        </fo:table-row>

                        <fo:table-row height="1cm">
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm" font-weight="bold">AGENCIA DESPACHO</fo:block>
                  		</fo:table-cell>
                              <fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="dd"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="mm"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="aa"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt" background-color="gray">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="llegada"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt" background-color="gray">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="estadol"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="salida"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="estados"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="si"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="no"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="cant"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="cabezote"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="trailer"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="tipotrailer"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="firma"/></fo:block>
                  		</fo:table-cell>
            	  </fo:table-row>

            	  <fo:table-row height="1cm">
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm">AGENCIA EXPORTACION</fo:block>
                  		</fo:table-cell>
                              <fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="dde"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="mme"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="aae"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="llegadae"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="estadole"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="salidae"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="estadose"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="sie"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="noe"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="cante"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="cabezotee"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="trailere"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="firmae"/></fo:block>
                  		</fo:table-cell>
            	  </fo:table-row>

            	  <fo:table-row height="1cm">
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm">AGENCIA IMPORTACION</fo:block>
                  		</fo:table-cell>
                              <fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="ddi"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="mmi"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="aai"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="llegadai"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="estadoli"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="salidai"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="estadosi"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="sii"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="noi"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="canti"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="cabezotei"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="traileri"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="firmai"/></fo:block>
                  		</fo:table-cell>
            	  </fo:table-row>

            	  <fo:table-row height="1cm">
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm">CADIVI</fo:block>
                  		</fo:table-cell>
                              <fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="ddc"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="mmc"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="aac"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="llegadac"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="estadolc"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="salidac"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="estadosc"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="sic"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="noc"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="cantc"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="cabezotec"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="trailerc"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="firmac"/></fo:block>
                  		</fo:table-cell>
            	  </fo:table-row>

            	  <fo:table-row height="1cm">
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm">CLIENTE FINAL</fo:block>
                  		</fo:table-cell>
                              <fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="ddcl"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="mmcl"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="aacl"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="llegadacl"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="estadolcl"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="salidacl"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="estadoscl"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="sicl"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="nocl"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="cantcl"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="cabezotecl"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="trailercl"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="firmacl"/></fo:block>
                  		</fo:table-cell>
            	  </fo:table-row>

            	  <fo:table-row height="1cm">
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm">TRANSBORDO</fo:block>
                  		</fo:table-cell>
                              <fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="ddt"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="mmt"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="aat"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="llegadat"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt" >
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="estadolt"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt" background-color="gray">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="salidat"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt" background-color="gray">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="estadost"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="sit"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="not"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="cantt"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="cabezotet"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="trailert"/></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"></fo:block>
                  		</fo:table-cell>
                  		<fo:table-cell padding="2px" font-size="7pt" border-style="solid" border-width="0.5pt">
                  		  <fo:block text-align="center" padding-before="0.3cm"><xsl:value-of select="firmat"/></fo:block>
                  		</fo:table-cell>
            	  </fo:table-row>
	         </fo:table-body>
	      </fo:table>
	</xsl:template>

</xsl:stylesheet>


