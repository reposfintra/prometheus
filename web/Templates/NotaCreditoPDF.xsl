<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:param name="periodo"/>   
<xsl:param name="page-count"/>  
<xsl:param name="fecha"/>
<xsl:param name="hora"/>      
<xsl:param name="usuario"/>   
<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  		<fo:layout-master-set>
			<fo:simple-page-master master-name="simple"
				 page-height="29.7cm"
				 page-width="21cm"
				 margin-top="0.5cm"
				 margin-bottom="0.5cm"
			         margin-left="0.5cm"
				 margin-right="0.5cm" initial-page-number="1">						                            
                          <fo:region-body margin-top="2.5cm" margin-bottom="2cm"/>
                          <fo:region-after  extent="2.5cm"/>
                          <fo:region-before extent="4.5cm"/>                          
			</fo:simple-page-master>
  		</fo:layout-master-set>
  
		<fo:page-sequence master-reference="simple" initial-page-number="1">
                 <fo:static-content flow-name="xsl-region-after">
                     <fo:block font-size="9pt" text-align="left">
                         <fo:inline font-weight="bold">Elaborado por :</fo:inline>                
                         <fo:inline font-weight="bold"><xsl:value-of select="$usuario"/></fo:inline>
                     </fo:block>
                     <!--Elaborado Por --> 
                    <fo:block font-size="9pt" text-align="center">
                         <fo:inline font-weight="bold">P�gina </fo:inline>                
                         <fo:inline font-weight="bold"><fo:page-number/> / <xsl:value-of select="$page-count"/></fo:inline>
                     </fo:block>
                     <!--Contador -->
                 </fo:static-content>
                
                
                    <fo:static-content flow-name="xsl-region-before">
                        <fo:table table-layout="fixed">
                        <fo:table-column column-width="21cm"/>		
                        <fo:table-body>
                            <fo:table-row>				
                                <fo:table-cell									
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">
					<fo:table table-layout="fixed">
                                        <fo:table-column column-width="3cm"/>
                                        <fo:table-column column-width="14cm"/>
                                        <fo:table-column column-width="3cm"/>
						<fo:table-body >                                                                                                                                                
                                                <!--Fila Inicial-->                                                                                                
						<fo:table-row>
                                                    <fo:table-cell text-align="left" vertical-align="middle"
                                                        number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                        <fo:block font-size="8pt" color="black" padding-before="0.5cm" font-weight='bold'>Fecha : <xsl:value-of select="$fecha"/></fo:block>    
                                                    
                                                    </fo:table-cell>
                                                
                                                    <fo:table-cell	                                                    								                                                                       
                                                        text-align="center" vertical-align="middle"
                                                        number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                        <fo:block font-size="14pt" color="black" padding-before="0.5cm" font-weight='bold'>FINTRA VALORES SA</fo:block>
                                                    </fo:table-cell>
                                                    
                                                    <fo:table-cell	                                                    								                                                                       
                                                        text-align="right" vertical-align="middle"
                                                        number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                        <fo:block font-size="8pt" color="black" padding-before="0.5cm" font-weight='bold'>Hora : <xsl:value-of select="$hora"/></fo:block>
                                                    </fo:table-cell>						
                                            </fo:table-row>                                                                                                                                    
                                            <!--Fin Fila inicial-->
                                            
                                            <fo:table-row>                                                    
                                                    <fo:table-cell	                                                    								                                                                       
                                                        text-align="center" vertical-align="middle"
                                                        number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                                        <fo:block font-size="10pt" color="black" font-weight='bold'>NIT. 802.022.016 - 1</fo:block>
                                                    </fo:table-cell>                                                                                                     
                                            </fo:table-row> 
                                            
                                            <fo:table-row>                                                    
                                                    <fo:table-cell	                                                    								                                                                       
                                                        text-align="center" vertical-align="middle"
                                                        number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                                        <fo:block font-size="10pt" color="black" font-weight='bold'>CRA 53 No. 79 - 01 </fo:block>
                                                    </fo:table-cell>                                                                                                     
                                            </fo:table-row> 
                                            
                                            <fo:table-row>                                                    
                                                    <fo:table-cell	                                                    								                                                                       
                                                        text-align="center" vertical-align="middle"
                                                        number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                                        <fo:block font-size="10pt" color="black" font-weight='bold'>Barranquilla - Colombia</fo:block>
                                                    </fo:table-cell>                                                                                                     
                                            </fo:table-row> 
                                            
                                           <fo:table-row>                                                    
                                                    <fo:table-cell	                                                    								                                                                       
                                                        text-align="center" vertical-align="middle"
                                                        number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                                        <fo:block font-size="10pt" color="black" font-weight='bold'></fo:block>
                                                    </fo:table-cell>                                                                                                     
                                            </fo:table-row> 
                                                                                                                                                                                                                                                                                            
					</fo:table-body>
					</fo:table>
			    </fo:table-cell>		
			</fo:table-row>                                                
		</fo:table-body>
		</fo:table>
</fo:static-content>
                
                                <fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="raiz"/>
				</fo:flow>   
                                                                                             
		</fo:page-sequence>
	</fo:root>
</xsl:template>

        <xsl:template match="raiz">	                                     
                  <xsl:apply-templates select="data"/>
	</xsl:template>

         <xsl:template match="data">	                
            <fo:block> 
                <xsl:apply-templates select="cabecera"/><!--Cabecera --> 
             </fo:block>                 
                
             <fo:block>
                <xsl:apply-templates select="detalle"/><!--Detalle-->  
             </fo:block>
                                                                        
             <fo:block> 
                    <xsl:choose>
                        <xsl:when test="position() != last()">
                            <xsl:attribute name="break-after">page</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="break-after">auto</xsl:attribute>
                        </xsl:otherwise> 
                    </xsl:choose> 
             </fo:block>     
                                                                                                                                           		                
	</xsl:template>
        
                	
        <!-- Inicio Tabla 1-->
        
        <xsl:template match="cabecera">
                                
           <fo:table table-layout="fixed">
                    <fo:table-column column-width="21cm"/>                    
                    <fo:table-body>
                    
                    <fo:table-row>
                        <fo:table-cell> 
                            <fo:table table-layout="fixed">
                                <fo:table-column column-width="3cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="5cm"/>
                                
                                <fo:table-column column-width="3cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="7cm"/> 
                                
                                <fo:table-column column-width="3cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="5cm"/>
                                <fo:table-body>                                                                   
                                
                                    <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                        <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.5cm">Nota Credito</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.5cm" font-weight='bold'>:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.5cm"><xsl:value-of select="encabezado"/></fo:block>
                                         </fo:table-cell>
                                         
                                          <!-- Fin Numero Ingreso -->
                                                                                                                                                                  
                                          
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.5cm">Cliente</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.5cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.5cm"><xsl:value-of select="cliente"/></fo:block>
                                         </fo:table-cell>
                                                                                                                                                                    
                                          
                                          <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.5cm"></fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.5cm"></fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.5cm"></fo:block>
                                         </fo:table-cell>
                                          
                                  </fo:table-row>
                                    
                                     <!--Fin 1� FILA -->
                                     
                                     <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                        <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Valor Nota</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm" font-weight='bold'>:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"><xsl:value-of select="vlrconsignacion"/></fo:block>
                                         </fo:table-cell>
                                         
                                          <!-- Fin Numero Ingreso -->
                                                                                                                                                                  
                                          
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Nit Cliente</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.175cm"><xsl:value-of select="nomcli"/></fo:block>
                                         </fo:table-cell>
                                                                                                                                                                    
                                          
                                          <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm"></fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm"></fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.175cm"></fo:block>
                                         </fo:table-cell>
                                          
                                  </fo:table-row>
                                     
                                     
                                  <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                        <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Fecha Nota</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm" font-weight='bold'>:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"><xsl:value-of select="fechaconsg"/></fo:block>
                                         </fo:table-cell>
                                         
                                          <!-- Fin Numero Ingreso -->
                                                                                                                                                                       
                                          
                                          <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Direccion</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.175cm"><xsl:value-of select="dircli"/></fo:block>
                                         </fo:table-cell>                                        
                                          
                                          <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm"></fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" padding-before="0.175cm" font-weight='bold'></fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                        <fo:block font-size="8pt" color="black"   padding-before="0.175cm"></fo:block>
                                         </fo:table-cell>
                                          
                                  </fo:table-row>     
                                  
                                  <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                        <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm"></fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm" font-weight='bold'></fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"></fo:block>
                                         </fo:table-cell>
                                         
                                          <!-- Fin Numero Ingreso -->
                                                                                                                                                                       
                                          
                                          <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Telefono</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.175cm"><xsl:value-of select="telcli"/></fo:block>
                                         </fo:table-cell>                                        
                                                                                    
                                          
                                  </fo:table-row>        
                                  
                                  <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                        <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Descripcion</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm" font-weight='bold'>:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"><xsl:value-of select="descripcion"/></fo:block>
                                         </fo:table-cell>
                                         
                                          <!-- Fin Numero Ingreso -->
                                                                                                                                                                       
                                          
                                          <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Ciudad</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.175cm"><xsl:value-of select="ciucli"/></fo:block>
                                         </fo:table-cell>                                        
                                                                                    
                                          
                                  </fo:table-row>                                                                
                                     
                                 </fo:table-body>
                            </fo:table>  
                            <!-- Fin de Tabla -->
                        </fo:table-cell>	
                                                                      
                    </fo:table-row>
 
		</fo:table-body>
          </fo:table>
       <!-- 1  FILA -->          
        <xsl:apply-templates select="ingreso"/><!--Ingreso --> 
        <xsl:apply-templates select="nota"/><!--Nota-->                                                
        
        </xsl:template>
        <!-- Fin tabla 1 -->
        
        
        <xsl:template match="ingreso">
                                
           <fo:table table-layout="fixed">
                    <fo:table-column column-width="21cm"/>                    
                    <fo:table-body>
                    
                    <fo:table-row>
                        <fo:table-cell> 
                            <fo:table table-layout="fixed">
                                <fo:table-column column-width="3cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="5cm"/>
                                
                                <fo:table-column column-width="3cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="7cm"/> 
                                
                                <fo:table-column column-width="3cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="5cm"/>
                                <fo:table-body>                                                                   
                                
                                    <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                        <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Banco</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm" font-weight='bold'>:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="7pt" color="black"  padding-before="0.175cm"><xsl:value-of select="banco"/></fo:block>
                                         </fo:table-cell>
                                         
                                          <!-- Fin Numero Ingreso -->
                                                                                                                                                                  
                                          
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Sucursal</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="7pt" color="black"   padding-before="0.175cm"><xsl:value-of select="sucursal"/></fo:block>
                                         </fo:table-cell>
                                                                                                                                                                    
                                          
                                          <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Cuenta</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.175cm"><xsl:value-of select="cuenta_b"/></fo:block>
                                         </fo:table-cell>
                                          
                                  </fo:table-row>
                                    
                                     <!--Fin 1� FILA -->                                                                                                                                                                                                                                                
                                 </fo:table-body>
                            </fo:table>  
                            <!-- Fin de Tabla -->
                        </fo:table-cell>	
                                                                      
                    </fo:table-row>
 
		</fo:table-body>
          </fo:table>
       <!-- 1  FILA -->                                     
        </xsl:template>
        <!-- Fin Ingreso -->
        
        
        <xsl:template match="nota">
                                
           <fo:table table-layout="fixed">
                    <fo:table-column column-width="21cm"/>                    
                    <fo:table-body>
                    
                    <fo:table-row>
                        <fo:table-cell> 
                            <fo:table table-layout="fixed">
                                <fo:table-column column-width="3cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="5cm"/>
                                
                                <fo:table-column column-width="3cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="7cm"/> 
                                
                                <fo:table-column column-width="3cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="5cm"/>
                                <fo:table-body>                                                                   
                                
                                    <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                        <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Cuenta</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"><xsl:value-of select="cuenta"/></fo:block>
                                         </fo:table-cell>
                                         
                                          <!-- Fin Numero Ingreso -->
                                                                                                                                                                  
                                          
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Tipo</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.175cm"><xsl:value-of select="tipo"/></fo:block>
                                         </fo:table-cell>
                                                                                                                                                                    
                                          
                                          <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Auxiliar</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black"   padding-before="0.175cm"><xsl:value-of select="auxiliar"/></fo:block>
                                         </fo:table-cell>
                                          
                                  </fo:table-row>
                                    
                                     <!--Fin 1� FILA -->                                                                                                                                                                                                                                                
                                 </fo:table-body>
                            </fo:table>  
                            <!-- Fin de Tabla -->
                        </fo:table-cell>	
                                                                      
                    </fo:table-row>
 
		</fo:table-body>
          </fo:table>
       <!-- 1  FILA -->                                     
        </xsl:template>
        <!-- Fin Ingreso -->
                        
        
       
        <!-- Inicio Tabla 2 -->
       <xsl:template match="items">                    
        
        <fo:table table-layout="fixed" padding-before="0.1cm">                                       
                    <fo:table-column column-width="0.75cm"/><!-- Item-->
                    <fo:table-column column-width="2cm"/><!-- Factura -->
                    <fo:table-column column-width="5cm"/><!-- Descripcion -->                                                                                                    
                    
                    <fo:table-column column-width="1.5cm"/><!-- Fecha Factura -->
                    <fo:table-column column-width="1.75cm"/><!-- Cuenta -->
                    
                    <fo:table-column column-width="3.25cm"/><!-- Vlr Factura MF. -->
                   
                    <fo:table-column column-width="3.25cm"/><!-- Vlr Nota -->                                                              
                    
                    <fo:table-body>
                    <fo:table-row>				
                                    
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                                                                    
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm"><xsl:value-of select="consc"/></fo:block>
                                     </fo:table-cell>
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                                                                    
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm"><xsl:value-of select="factura"/></fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                          
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm"><xsl:value-of select="descripcion"/></fo:block>
                                     </fo:table-cell>	
                                                                         
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm"><xsl:value-of select="fechafac"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm"><xsl:value-of select="cuentai"/></fo:block>
                                     </fo:table-cell>
                                                                        
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6.8pt" color="black" padding-before="0.1cm"><xsl:value-of select="vlrfacturamf"/></fo:block>
                                     </fo:table-cell>
                                                                                                                                                                                                                          
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6.8pt" color="black" padding-before="0.1cm"><xsl:value-of select="vlrnota"/></fo:block>
                                     </fo:table-cell>                                                                          
                                     
                                     		
                            </fo:table-row>
                    
                    </fo:table-body> 
            </fo:table>                        
                                       
           <fo:block space-before='0.25cm' height='0.25cm'></fo:block>                   	
                     
               
        </xsl:template>  
        <!-- Fin tabla 2 -->
        
        <!-- Tabla 3 -->
        <xsl:template match="detalle">
        <fo:table table-layout="fixed" padding-before="0.5cm">                                       
                    <fo:table-column column-width="0.75cm"/><!-- Item-->
                    <fo:table-column column-width="2cm"/><!-- Factura -->
                    <fo:table-column column-width="5cm"/><!-- Descripcion -->                                                                                                    
                    
                    <fo:table-column column-width="1.5cm"/><!-- Fecha Factura -->
                    <fo:table-column column-width="1.75cm"/><!-- Cuenta -->
                    
                    <fo:table-column column-width="3.25cm"/><!-- Vlr Factura MF. -->
                   
                    <fo:table-column column-width="3.25cm"/><!-- Vlr Nota -->                                           
                    
                    
                    <fo:table-body>
                    <fo:table-row>				                                    
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                                                                    
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold">Items</fo:block>
                                     </fo:table-cell>
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                                                                    
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold">Factura</fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                          
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold">Descripcion</fo:block>
                                     </fo:table-cell>	
                                                                         
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold">Fec. Factura</fo:block>
                                     </fo:table-cell>
                                                                          
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold">Cuenta</fo:block>
                                     </fo:table-cell>
                                     
                                                                          
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold">Vlr Factura MF.</fo:block>
                                     </fo:table-cell>
                              
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold">Vlr Nota</fo:block>
                                     </fo:table-cell>                                                                           		
                                     
                                     		
                            </fo:table-row>
                    
                    </fo:table-body> 
            </fo:table>  
                                             
            
             <fo:block>
                    <xsl:apply-templates select="items"/>
             </fo:block>      
             
             <fo:block>
                    <xsl:apply-templates select="total"/>
             </fo:block>                   
          
        </xsl:template>
        <!-- Fin de Tabla 4 --> 
                
        
        <xsl:template match="total">                    
        
        <fo:table table-layout="fixed" padding-before="0.1cm">                                       
                    <fo:table-column column-width="0.75cm"/><!-- Item-->
                    <fo:table-column column-width="2cm"/><!-- Factura -->
                    <fo:table-column column-width="5cm"/><!-- Descripcion -->                                                                                                    
                    
                    <fo:table-column column-width="1.5cm"/><!-- Fecha Factura -->
                    <fo:table-column column-width="1.75cm"/><!-- Cuenta -->
                    
                    <fo:table-column column-width="3.25cm"/><!-- Vlr Factura MF. -->
                   
                    <fo:table-column column-width="3.25cm"/><!-- Vlr Nota -->  
                    
                    <fo:table-body>
                    <fo:table-row>				
                                    
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                                                                    
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm"></fo:block>
                                     </fo:table-cell>
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                                                                    
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold"></fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                          
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm"></fo:block>
                                     </fo:table-cell>	
                                                                         
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm"></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm"></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm" font-weight="bold">TOTALES</fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="6.8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="t_vlrnota"/></fo:block>
                                     </fo:table-cell>
                                                                                                                                                     	                                     
                                     		
                            </fo:table-row>
                    
                    </fo:table-body> 
            </fo:table>                        
                                                  	                     
               
        </xsl:template>  
        
                                                                                    
       
</xsl:stylesheet>


