<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  		<fo:layout-master-set>
			<fo:simple-page-master master-name="simple"
				 page-height="28cm"
							  page-width="21.5cm"
							  margin-top="1.0cm"
							  margin-bottom="0.5cm"
							  margin-left="1.0cm"
							  margin-right="5.0cm">
			
			  <fo:region-body margin-top="0.5cm" margin-bottom="0.5cm"/>
			  <fo:region-before />
			  <fo:region-after />
			</fo:simple-page-master>
  		</fo:layout-master-set>
  
		<fo:page-sequence master-reference="simple">
				<!-- <fo:static-content flow-name="xsl-region-before">
                                    <xsl:apply-templates select="barracolumna"/>
                                 </fo:static-content> -->

                                <fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="raiz"/>
				</fo:flow>
                                   
		</fo:page-sequence>
	</fo:root>
        
</xsl:template>

        <xsl:template match="raiz">	             
                  <xsl:apply-templates select="data"/>
	</xsl:template>

        <xsl:template match="data">
          <fo:block>
           <xsl:choose>
              <xsl:when test="position() != last()">
                  <xsl:attribute name="break-after">page</xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                  <xsl:attribute name="break-after">auto</xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
             
          <fo:block>
  		  <xsl:apply-templates select="datos_clientes"/>
          </fo:block>
        
	
           <fo:block>
  		  <xsl:apply-templates select="barracolumna"/>
          </fo:block>
          
          <fo:block>
  		  <xsl:apply-templates select="datos_items"/>
          </fo:block>
          
           </fo:block>  
         </xsl:template>

        <xsl:template match="datos_clientes">
	<fo:table table-layout="fixed"  height="0.8cm" border-color="white" border-style="solid">
		
                                        <fo:table-column column-width="2.0cm"/>
                                        <fo:table-column column-width="5.0cm"/>
                                        <fo:table-column column-width="4.0cm"/>
                                        <fo:table-column column-width="5.0cm"/>
                                        <fo:table-column column-width="4.0cm"/>

   		                        <fo:table-body>

                                        <!--*************1� FILA*********-->
                                        <fo:table-row >

                                        <fo:table-cell
                                          
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" font-style="bold"  color="black" margin-left='0.125cm' padding-before="0.075cm">CLIENTE: </fo:block>
                                        </fo:table-cell>

                                          <fo:table-cell
                                          
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.075cm"> <xsl:value-of select="cliente"/></fo:block>
                                        </fo:table-cell>

                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" font-style="black"  color="black" margin-left='0.125cm' padding-before="0.075cm">LUGAR: </fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.075cm"><xsl:value-of select="lugar"/></fo:block>
                                        </fo:table-cell>

                                        </fo:table-row>
                                        <!--*************2� FILA*********-->
                                        <fo:table-row >

                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" font-style="bold"  color="black" margin-left='0.125cm' padding-before="0.075cm">NIT: </fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.075cm"><xsl:value-of select="nit"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" font-style="bold" margin-left='0.125cm' padding-before="0.075cm" color="black">FECHA: </fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.075cm"> <xsl:value-of select="fecha"/></fo:block>
                                        </fo:table-cell>

                                        </fo:table-row>
                                        <!--*************FIN 2� FILA*********-->
                                        
                                        <!--*************3� FILA*********-->
                                        <fo:table-row >
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" font-style="bold" padding-before="0.075cm" margin-left='0.125cm' color="black">DIRECCION: </fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.075cm" > <xsl:value-of select="direccion"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" font-style="bold" color="black" padding-before="0.075cm" margin-left='0.125cm'></fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" padding-before="0.075cm" > </fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--*************FIN 3� FILA*********-->

                                        <!--*************4� FILA*********-->
                                        <fo:table-row >

                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" font-style="bold" margin-left='0.125cm' padding-before="0.075cm"  color="black">TELEFONO: </fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.075cm"> <xsl:value-of select="telefono"/></fo:block>
                                        </fo:table-cell>
                                        

                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-top-color="black"
                                            border-left-color="black"
                                            border-right-color="black"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" font-style="bold" margin-left='0.125cm' padding-before="0.075cm"  color="black">ANEXOS</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-top-color="black"
                                            border-right-color="black"
                                            height="0.5cm"
                                           
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" padding-before="0.075cm"> </fo:block>
                                        </fo:table-cell>

                                        </fo:table-row>
                                        <!--*************FIN 4� FILA*********-->

                                        <!--*************5� FILA*********-->
                                        <fo:table-row >

                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" font-style="bold" margin-left='0.125cm' padding-before="0.075cm"  color="black">PAGINA: </fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.075cm"> <xsl:value-of select="ciudad"/></fo:block>
                                        </fo:table-cell>
                                        

                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-bottom="black"
                                            border-left-color="black"
                                            border-right-color="black"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" font-style="bold" margin-left='0.125cm' padding-before="0.075cm"  color="black">DE FACTURA No.</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-bottom-color="black"
                                            border-right-color="black" 
                                            height="0.5cm"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.075cm"> <xsl:value-of select="factura"/></fo:block>
                                        </fo:table-cell>

                                        </fo:table-row>
                                        
                                        
                                         <fo:table-row >

                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="5" number-rows-spanned="1" background-color="white">
                                           
                                        </fo:table-cell>
                                         </fo:table-row >
                                        
                                     </fo:table-body>  
                                 </fo:table>

</xsl:template >

<xsl:template match="datos_items">
	<fo:table table-layout="fixed" border-color="white" border-style="solid">
		
                                        <fo:table-column column-width="4.5cm"/>
                                        <fo:table-column column-width="3.0cm"/>
                                        <fo:table-column column-width="3.0cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                        <fo:table-column column-width="1.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                        
                          
   		<fo:table-body>

                        <fo:table-row>                
                                       <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" margin-left='0.1cm' color="black" padding-before="0.075cm"><xsl:value-of select="remesa"/></fo:block>
                                        </fo:table-cell>
                                        
                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" color="black" padding-before="0.075cm"> <xsl:value-of select="fecha"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" color="black" padding-before="0.075cm"> <xsl:value-of select="placa"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" color="black" padding-before="0.075cm"> <xsl:value-of select="origen"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" color="black" padding-before="0.075cm"> <xsl:value-of select="destino"/></fo:block>
                                        </fo:table-cell>
                                        
                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="right" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" color="black" margin-right='0.125cm' padding-before="0.075cm"> <xsl:value-of select="cantidad"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" color="black" padding-before="0.075cm"> <xsl:value-of select="unidad"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                           height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="right" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt"  color="black" margin-right='0.125cm' padding-before="0.075cm"> <xsl:value-of select="valor"/></fo:block>
                                        </fo:table-cell>
              
			</fo:table-row>
                        <fo:table-row> 
                                       <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="7" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" color="black" margin-left='0.1cm' padding-before="0.075cm"> <xsl:value-of select="descripcion"/></fo:block>
                                        </fo:table-cell>
                                        
                                     <!--    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                           height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" color="black" padding-before="0.075cm"> <xsl:value-of select="tasa"/></fo:block>
                                        </fo:table-cell>
                                        
                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                           height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="right" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" color="black" margin-right='0.125cm' padding-before="0.075cm"> <xsl:value-of select="valorunitario"/> <xsl:value-of select="moneda"/></fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                           height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="right" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" color="black" margin-right='0.125cm' padding-before="0.075cm"> <xsl:value-of select="monedas"/> </fo:block>
                                        </fo:table-cell> -->
                                        
                        </fo:table-row>
		</fo:table-body>          
    </fo:table>

</xsl:template >
<xsl:template match="barracolumna">
	<fo:table table-layout="fixed" border-color="white" border-style="solid">
	
                                      
                                        <fo:table-column column-width="4.0cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                        <fo:table-column column-width="2.0cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                        <fo:table-column column-width="1.5cm"/>
                                        <fo:table-column column-width="1.5cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                      
                                        
   		<fo:table-body>

                        <fo:table-row>                
                                       <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-bottom-color="black"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" >
                                            <fo:block font-size="8pt" font-style="bold"  color="black" padding-before="0.075cm">DOCUMENTO DE ORIGINADOR</fo:block>
                                        </fo:table-cell>
                                        
                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-bottom-color="black"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" >
                                            <fo:block font-size="8pt" font-style="bold"  color="black" padding-before="0.075cm">ORIGEN</fo:block>
                                        </fo:table-cell>
                                        
                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-bottom-color="black"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" >
                                            <fo:block font-size="8pt" font-style="bold"  color="black" padding-before="0.075cm">DESTINO</fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-bottom-color="black"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" >
                                            <fo:block font-size="8pt" font-style="bold"  color="black" padding-before="0.075cm">CANTID. UNID.</fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-bottom-color="black"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" >
                                            <fo:block font-size="8pt" font-style="bold"  color="black" padding-before="0.075cm">VAL.FLETE</fo:block>
                                        </fo:table-cell>
                                        
                                        
                                        
			</fo:table-row>
                         <fo:table-row> 
                           <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-top="black"
                                            height="0.2cm"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" >
                                            <fo:block font-size="8pt" font-style="bold"  color="black" padding-before="0.075cm"></fo:block>
                                        </fo:table-cell>
                                        
                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-top-color="black"
                                            height="0.2cm"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" >
                                            <fo:block font-size="8pt" font-style="bold"  color="black" padding-before="0.075cm"></fo:block>
                                        </fo:table-cell>
                                        
                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-top-color="black"
                                            height="0.2cm"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" >
                                            <fo:block font-size="8pt" font-style="bold"  color="black" padding-before="0.075cm"></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-top-color="black"
                                            height="0.2cm"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" >
                                            <fo:block font-size="8pt" font-style="bold"  color="black" padding-before="0.075cm"></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-top-color="black"
                                            height="0.2cm"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" >
                                            <fo:block font-size="8pt" font-style="bold"  color="black" padding-before="0.075cm"></fo:block>
                                        </fo:table-cell>
                          </fo:table-row> 

		</fo:table-body>          
    </fo:table>

</xsl:template >


</xsl:stylesheet>


