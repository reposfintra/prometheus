<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:param name="page-count"/>
    <xsl:template match="/">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="simple"
                          page-height="29.7cm"
                          page-width="21.5cm"
                          margin-top="1cm"
                          margin-bottom="1cm"
                          margin-left="0.7cm"
                          margin-right="0.7cm">
                    <fo:region-body margin-top="1" margin-bottom="2cm"/>
                    <fo:region-before extent="1cm"/>
                    <fo:region-after extent="1.5cm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="simple"  initial-page-number="1">
                 <fo:static-content flow-name="xsl-region-after">
                 <fo:block font-size="9pt" text-align="right">
                 <fo:inline font-weight="bold">P�gina </fo:inline>                
                 <fo:inline font-weight="bold"><fo:page-number/> / <xsl:value-of select="$page-count"/></fo:inline>
                 </fo:block>
                 </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <xsl:apply-templates select="raiz"/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <xsl:template match="raiz">
        <xsl:apply-templates select="data"/>
    </xsl:template>

    <xsl:template match="data">
        <fo:block>
            <xsl:apply-templates select="cabecera"/>
        </fo:block>        
        <fo:block>
            <xsl:apply-templates select="tabla"/>
        </fo:block>
        <fo:block>
            <xsl:apply-templates select="totales"/>
        </fo:block>

    </xsl:template>


	
   <xsl:template match="cabecera">
        <fo:table table-layout="fixed" height="3cm">
            <fo:table-column column-width="3.19cm"/>
            <fo:table-column column-width="16.5cm"/>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell
                            border-width="1pt"
                            border-style="solid"
                            border-color="black"
                            number-columns-spanned="1"
                            text-align="center" vertical-align="middle"
                            background-color="white">
			    <fo:external-graphic  vertical-align="middle" src="url(images/logo_fintra.png)"/>
                    </fo:table-cell>
                    <fo:table-cell
                            border-width="1pt"
                            border-style="solid"
                            border-color="black"
                            number-columns-spanned="1"
                            text-align="center" vertical-align="middle"
                            background-color="white">
                            <fo:table table-layout="fixed">
                            <fo:table-column column-width="16.5cm"/>
                                    <fo:table-body border-color="black" border-style="solid" border-bottom-color="white">

                                    <fo:table-row>
                                                <fo:table-cell
                                                            border-width="0.5pt"
                                                            border-style="none"
                                                            border-color="black"                                                                        
                                                            text-align="center" vertical-align="middle"
                                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                            <fo:block font-size="12pt" color="black" padding-before="0.5cm"><xsl:value-of select="empresa"/></fo:block>
                                                    </fo:table-cell>
                                    </fo:table-row>
                                    <fo:table-row>
                                                <fo:table-cell
                                                            border-width="0.5pt"
                                                            border-style="none"
                                                            border-color="black"                                                                        
                                                            text-align="center" vertical-align="middle"
                                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                            <fo:block font-size="8pt" color="black" padding-before="0.1cm">NIT: <xsl:value-of select="nit"/></fo:block>
                                                    </fo:table-cell>
                                    </fo:table-row>
                                    <fo:table-row>
                                                <fo:table-cell
                                                            border-width="0.5pt"
                                                            border-style="none"
                                                            border-color="black"
                                                            text-align="center" vertical-align="middle"
                                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                            <fo:block font-size="12pt" color="black"><xsl:value-of select="res"/>LIBRO MAYOR</fo:block>
                                                    </fo:table-cell>
                                    </fo:table-row>
                                    <fo:table-row>
                                                <fo:table-cell
                                                            border-width="0.5pt"
                                                            border-style="none"
                                                            border-color="black"
                                                            text-align="center" vertical-align="middle"
                                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                            <fo:block font-size="12pt" color="black">PERIODO: <xsl:value-of select="periodo"/></fo:block>
                                                    </fo:table-cell>
                                    </fo:table-row>
                            </fo:table-body>
                            </fo:table>
                </fo:table-cell>
            </fo:table-row>
    </fo:table-body>
    </fo:table>
    </xsl:template>

    <!--Tabla de cuentas-->
    <xsl:template match="cuenta">
        <fo:table table-layout="fixed"  height="0.4cm" border-color="white" border-style="solid">
            <fo:table-column column-width="2.5cm"/>
            <fo:table-column column-width="6.5cm"/>
            <fo:table-column column-width="2.8cm"/>
            <fo:table-column column-width="2.7cm"/>
            <fo:table-column column-width="2.7cm"/>
            <fo:table-column column-width="2.8cm"/>

            <fo:table-body>

                   <fo:table-row keep-with-next ="always">
                    <fo:table-cell text-align="left"  vertical-align="middle" number-columns-spanned="" number-rows-spanned="1" background-color="white">
                         <fo:block font-size="7pt" padding-before="0.05cm"><xsl:value-of select="ccuenta"/></fo:block>
                     </fo:table-cell>

                     <fo:table-cell text-align="left" vertical-align="middle" number-columns-spanned="" number-rows-spanned="1" background-color="white">
                         <fo:block font-size="7pt" padding-before="0.05cm"><xsl:value-of select="cdescripcion"/></fo:block>
                     </fo:table-cell>

                     <fo:table-cell text-align="right"  end-indent="3pt"  vertical-align="middle" number-columns-spanned="" number-rows-spanned="1" background-color="white">
                         <fo:block font-size="7pt" padding-before="0.05cm"><xsl:value-of select="anterior"/></fo:block>
                     </fo:table-cell>

                     <fo:table-cell text-align="right" end-indent="3pt"   vertical-align="middle" number-columns-spanned="" number-rows-spanned="1" background-color="white">
                         <fo:block font-size="7pt" padding-before="0.05cm"><xsl:value-of select="cdebito"/></fo:block>
                     </fo:table-cell>

                     <fo:table-cell text-align="right"  end-indent="3pt"  vertical-align="middle" number-columns-spanned="" number-rows-spanned="1" background-color="white">
                         <fo:block font-size="7pt" padding-before="0.05cm"><xsl:value-of select="ccredito"/></fo:block>
                     </fo:table-cell>

                     <fo:table-cell text-align="right"  end-indent="3pt"  vertical-align="middle" number-columns-spanned="" number-rows-spanned="1" background-color="white">
                         <fo:block font-size="7pt" padding-before="0.05cm"><xsl:value-of select="actual"/></fo:block>
                     </fo:table-cell>

                </fo:table-row>


              </fo:table-body>
         </fo:table>
    </xsl:template>
    <!--Tabla con titulos-->
    <xsl:template match="tabla">
         <fo:table table-layout="fixed" border-color="white" border-style="solid">
             <fo:table-column column-width="21cm"/>
            <fo:table-body>

                   <fo:table-row >

                        <fo:table-cell>

                            <fo:table table-layout="fixed"  height="0.8cm" border-color="white" border-style="solid">
                                <fo:table-column column-width="2.5cm"/>
                                <fo:table-column column-width="6.5cm"/>
                                <fo:table-column column-width="2.8cm"/>
                                <fo:table-column column-width="2.7cm"/>
                                <fo:table-column column-width="2.7cm"/>
                                <fo:table-column column-width="2.8cm"/>
                                <fo:table-body>
                                    <fo:table-row  keep-with-next ="always">
                    
                                        <fo:table-cell end-indent="3pt" text-align="left" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.075cm">CUENTA</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="left" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.075cm">DESCRIPCION</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.075cm">SALDO ANTERIOR</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.075cm">DEBITO</fo:block>
                                        </fo:table-cell>

                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.075cm">CREDITO</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.075cm">SALDO ACTUAL</fo:block>
                                        </fo:table-cell>

                                  </fo:table-row>

                               </fo:table-body>
                          </fo:table>
      
               </fo:table-cell>
             </fo:table-row>


        <fo:table-row  keep-with-next ="always">
            <fo:table-cell>
                <!--Cuentas-->
                <fo:block >
                    <xsl:apply-templates select="cuenta"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>

        <fo:table-row  keep-with-next ="always">
            <fo:table-cell>
          <!--Totales debito y credito-->
                   <fo:table table-layout="fixed"  height="0.4cm" border-color="white" border-style="solid">
                      <fo:table-column column-width="2.5cm"/>
                      <fo:table-column column-width="6.5cm"/>
                      <fo:table-column column-width="2.8cm"/>
                      <fo:table-column column-width="2.7cm"/>
                      <fo:table-column column-width="2.7cm"/>
                      <fo:table-column column-width="2.8cm"/>
                      <fo:table-body>
                         <fo:table-row keep-together="always" keep-together.within-page="always">

                              <fo:table-cell text-align="left" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                              </fo:table-cell>
                              <fo:table-cell text-align="left" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                              </fo:table-cell>

                              <fo:table-cell border-top-width="0.1pt" border-top-style="solid" border-top-color="black" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                     <fo:block font-weight="bold" font-size="7pt" padding-before="0.075cm"><xsl:value-of select="totalanterior"/></fo:block>
                              </fo:table-cell>

                              <fo:table-cell border-top-width="0.1pt" border-top-style="solid" border-top-color="black" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                     <fo:block font-weight="bold" font-size="7pt" padding-before="0.075cm"><xsl:value-of select="totaldebito"/></fo:block>
                              </fo:table-cell>

                              <fo:table-cell border-top-width="0.1pt" border-top-style="solid" border-top-color="black" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                    <fo:block font-weight="bold" font-size="7pt" padding-before="0.075cm"><xsl:value-of select="totalcredito"/></fo:block>
                             </fo:table-cell>

                             <fo:table-cell border-top-width="0.1pt" border-top-style="solid" border-top-color="black" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                     <fo:block font-weight="bold" font-size="7pt" padding-before="0.075cm"><xsl:value-of select="totalactual"/></fo:block>
                              </fo:table-cell>
             </fo:table-row>

                 <fo:table-row height="1cm">
          </fo:table-row>
           </fo:table-body>
       </fo:table>
          </fo:table-cell>
         </fo:table-row>

           </fo:table-body>
         </fo:table>
    </xsl:template >

</xsl:stylesheet>
