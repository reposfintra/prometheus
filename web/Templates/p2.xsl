<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  		<fo:layout-master-set>
			<fo:simple-page-master master-name="simple"
				margin-right="1cm"
				margin-left="1.5cm"
				margin-bottom="1cm"
				margin-top="1cm"
				page-width="27cm"
				page-height="23cm"
			>
			  <fo:region-body margin-top="0cm" margin-bottom="0cm"/>			
			</fo:simple-page-master>
  		</fo:layout-master-set>
  
		<fo:page-sequence master-reference="simple">
				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="raiz"/>
				</fo:flow>
		</fo:page-sequence>
	</fo:root>
</xsl:template>

<xsl:template match="raiz">	
		<xsl:apply-templates select="data"/>
	</xsl:template>

	<xsl:template match="data">
       <fo:block>
			<xsl:apply-templates select="tabla1"/>
       </fo:block>
	   <fo:block>
			<xsl:apply-templates select="tabla2"/>
       </fo:block>
	   <fo:block>
			<xsl:apply-templates select="tabla3"/>
       </fo:block>
	   
	   <fo:block-container height="5cm" width="24cm" >
			<xsl:apply-templates select="valores"/>
		</fo:block-container> 
		
	   <fo:table table-layout="fixed"  border-style="solid"  border-color="white" background-color="white" border-width=".1mm" height="5.5cm">
			<fo:table-column column-width="16cm"/>
			<fo:table-column column-width="8cm"/>
			<fo:table-body  font-weight="normal" font-size="6pt" >
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
					   <fo:block>
							<xsl:apply-templates select="tabla5"/>
       					</fo:block>
					 </fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-left-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block>
							<xsl:apply-templates select="tabla6"/>
       					</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<fo:table table-layout="fixed"  border-style="solid"  border-color="white" border-width=".1mm" >
			<fo:table-column column-width="12cm"/>
			<fo:table-column column-width="12cm"/>
			<fo:table-body  font-weight="normal" font-size="6pt" >
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle"
                                                height="1.5cm" 						
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block> FIRMA Y SELLO AUTORIZADOS POR LA EMPRESA</fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
                                                height="1.5cm"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block> FIRMA Y SELLO PROPIETARIO O CONDUCTOR</fo:block>
					</fo:table-cell>
	  			</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<fo:block font-size="12pt"              
                    space-after.optimum="100pt"
                    text-align="center">
                    <xsl:value-of select="trafico"/>
		</fo:block>
		
	</xsl:template>
	
		<xsl:template match="tabla1"> 
		<fo:table table-layout="fixed" height="3cm">
		<fo:table-column column-width="17cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="6cm"/>
		<fo:table-body>
			<fo:table-row>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="white" 
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="black">
					<fo:external-graphic src="url(images/logo3.bmp)"/>
				</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="white" 
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">
					<fo:block font-size="12pt" ></fo:block>
			    </fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="white" 
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">					
					<fo:table table-layout="fixed">
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						
						<fo:table-body>
						
							<fo:table-row>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="11" number-rows-spanned="1" background-color="gray">
									<fo:block font-size="12pt" color="white"> MANIFIESTO DE CARGA </fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" color="black"> <xsl:value-of select="codigor"/> </fo:block>
								</fo:table-cell>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" color="black">  <xsl:value-of select="codigoe"/> </fo:block>
								</fo:table-cell>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="5" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" color="black">  <xsl:value-of select="codigoc"/>  </fo:block>
								</fo:table-cell>
							</fo:table-row>
						
							<fo:table-row>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" font-style="bold"  color="#64646F"> CODIGO REGIONAL </fo:block>
								</fo:table-cell>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" font-style="bold"  color="#64646F"> CODIGO DE EMPRESA </fo:block>
								</fo:table-cell>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="5" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" font-style="bold"  color="#64646F"> CODIGO NUMERICO CONSECUTIVO </fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
			    </fo:table-cell>
			</fo:table-row>
		</fo:table-body>
		</fo:table>
		</xsl:template >
	 
	<xsl:template match="tabla2"> 
	<fo:table table-layout="fixed"  height="1.2cm" border-color="white" border-width=".1mm"  >
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		
   		<fo:table-body>
			<fo:table-row>
                  <fo:table-cell
					border-width="0.5pt"
					border-style="solid"
					border-color="black"
					border-top-color="white"
					border-left-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" background-color="white">
					<fo:block font-size="8pt"></fo:block>
	    		</fo:table-cell>
				<fo:table-cell
					border-width="0.5pt"
					border-style="solid"
					border-color="black"
					border-top-color="white"
					border-left-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="18" number-rows-spanned="1" background-color="white">
					<fo:block font-size="8pt" color="black"> Rangos Autorizados : <xsl:value-of select="rango"/> </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					border-right-color="white"
					border-left-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="8pt" text-align="right" color="black"> Res.No </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					border-right-color="white"
					border-left-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="10" number-rows-spanned="1" background-color="white">
					<fo:block font-size="8pt" text-align="left" color="black"> <xsl:value-of select="res"/> </fo:block>
	    		</fo:table-cell>
			</fo:table-row>
			
	  		<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F"> FECHA DE EXPEDICION(DD/MM/AA) </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black"
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="2" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="14" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F"> ORIGEN DEL VIAJE </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="2" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F" > DESTINO FINAL DEL VIAJE</fo:block>
	    		</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="fechae"/> </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black"
					border-top-color="white"
					border-bottom-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="2" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="14" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="origen"/> </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					border-bottom-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="2" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="destino"/></fo:block>
	    		</fo:table-cell>
			</fo:table-row>
			
		</fo:table-body>
    </fo:table>
	</xsl:template >
	
	<xsl:template match="tabla3"> 
	<fo:table table-layout="fixed" height="4.6cm">
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		
   		<fo:table-body>
	  		<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="48" number-rows-spanned="1" background-color="gray">
					<fo:block font-size="12pt" color="white"> DATOS DEL VEHICULO </fo:block>
	    		</fo:table-cell>
			</fo:table-row>
	  
	  		<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" font-style="bold"  color="#64646F">   PLACA  </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  MARCA </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  LINEA </fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  MODELO </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">MODELO REPONTECIADO A    </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F"> SERIE No.    </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="4" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F"> COLOR     </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F"> TIPO DE CARROCERIA    </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" color="black">    <xsl:value-of select="placa"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black">   <xsl:value-of select="marca"/> </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black">   <xsl:value-of select="linea"/></fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black">  <xsl:value-of select="modelo"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"><xsl:value-of select="modelor"/> </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="serie"/>    </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="4" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="color"/>    </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="tipoc"/>   </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" font-style="bold"  color="#64646F">   REGISTRO NACIONAL DE CARGA No.  </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  CONFIGURACION </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  PESO VACIO </fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  NUMERO POLIZA SOAT </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">COMPA�IA DE SEGUROS SOAT    </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F"> VENCIMIENTO SOAT (DD/MM/AA)   </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F"> PLACA SEMIREMOLQUE     </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
				
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" color="black">   <xsl:value-of select="registro"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black">  <xsl:value-of select="configuracion "/></fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black">  <xsl:value-of select="peso"/></fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="numerop"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="compas"/>  </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="vencimiento"/>   </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="placas"/>  </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="17" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" font-style="bold"  color="#64646F">   PROPIETARIO  </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  DOCUMENTO DE IDENTIFICAION No. </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="11" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  DIRECCION </fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  TELEFONO </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F"> CIUDAD    </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="left" vertical-align="middle"
					number-columns-spanned="17" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" margin='0.5cm' color="black">  <xsl:value-of select="propietario"/>  </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="documentop"/></fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="11" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"><xsl:value-of select="direccionp"/></fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"><xsl:value-of select="telefonop"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="ciudadp"/>   </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="17" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" font-style="bold"  color="#64646F">   TENEDOR  </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  DOCUMENTO DE IDENTIFICAION No. </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="11" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  DIRECCION </fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  TELEFONO </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F"> CIUDAD    </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="left" vertical-align="middle"
					number-columns-spanned="17" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" margin='0.5cm' color="black">  <xsl:value-of select="tenedor"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="documentot"/> </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="11" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"><xsl:value-of select="direcciont"/></fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"><xsl:value-of select="telefonot"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="ciudadt"/>   </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="17" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" font-style="bold"  color="#64646F">   CONDUCTOR  </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  DOCUMENTO DE IDENTIFICAION No. </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="11" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  DIRECCION </fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F">  CAT.LIC. CONDUCCION </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" font-style="bold"  color="#64646F"> CIUDAD    </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="left" vertical-align="middle"
					number-columns-spanned="17" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" margin='0.5cm' color="black">  <xsl:value-of select="conductor"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="documentoc"/> </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="11" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"><xsl:value-of select="direccionc"/></fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"><xsl:value-of select="catc"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" color="black"> <xsl:value-of select="ciudadc"/>  </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>	
	  	</fo:table-body>
    </fo:table>
    </xsl:template >
	
	
	<xsl:template match="valores">
	  <fo:table table-layout="fixed"  padding-before="0.cm" font-size="10pt" border-style="solid"  height="5cm">
			<fo:table-column column-width="2cm"/>
			<fo:table-column column-width="1.5cm"/>
			<fo:table-column column-width="1.5cm"/>
			<fo:table-column column-width="1cm"/>
			<fo:table-column column-width="2cm"/>
			<fo:table-column column-width="1.5cm"/>
			<fo:table-column column-width="1.5cm"/>
			<fo:table-column column-width="3.5cm"/>
			<fo:table-column column-width="3.5cm"/>
			<fo:table-column column-width="3.0cm"/>
			<fo:table-column column-width="3.0cm"/>
		<fo:table-body  font-weight="normal" font-size="7pt" >
		  <fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="11" number-rows-spanned="1" background-color="gray">
						<fo:block font-size="12pt"  color="white" > DATOS DE LA MERCANCIA TRANSPORTADA   </fo:block>
					</fo:table-cell>
	  			</fo:table-row>
				<fo:table-row >
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >NUMERO DE REMESA</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >UNIDAD DE MEDIDA</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >CANTIDAD</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >PESO</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >CODIGO DE NATURALEZA</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >CODIGO DE EMPAQUE</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >CODIGO DE PRODUCTO</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >PRODUCTO TRANSPORTADO</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >REMITENTE</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >DESTINATARIO</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >DESTINO</fo:block>
					</fo:table-cell>
				</fo:table-row>
		  <xsl:apply-templates select="item"/>
          
          
          <fo:table-row>
            <fo:table-cell padding="2px"  number-columns-spanned="11">
			  <fo:block font-size="7pt">
			  <xsl:value-of select="precinto"/>
			  </fo:block>
			</fo:table-cell>
		  </fo:table-row>



          <fo:table-row>
            <fo:table-cell padding="2px"  number-columns-spanned="11"  linefeed-treatment="preserve"
                                            white-space-collapse="false"
                                            white-space-treatment="preserve"
                                            wrap-option="no-wrap">
			  <fo:block font-size="7pt">
			  <xsl:value-of select="leyenda2"/>
			  </fo:block>
			</fo:table-cell>
		  </fo:table-row>
	    </fo:table-body>
	  </fo:table>
	</xsl:template>
	
	<xsl:template match="item">
	  <fo:table-row >
		<fo:table-cell  font-size="7pt">
		  <fo:block><xsl:value-of select="numero"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="unidad"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="cantidad"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="peso"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="codigon"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="codigoe"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="codigop"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="producto"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt"  
                           linefeed-treatment="preserve"
                           white-space-collapse="false"
                           white-space-treatment="preserve"
                           wrap-option="no-wrap">
		  <fo:block><xsl:value-of select="remitente"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px"  font-size="7pt">
		  <fo:block><xsl:value-of select="destinatario"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px"  font-size="7pt">
		  <fo:block><xsl:value-of select="destino"/></fo:block>
		</fo:table-cell>
	  </fo:table-row>
	</xsl:template>
		
	<xsl:template match="tabla5"> 
		<fo:table table-layout="fixed"  border-style="solid"  border-color="black" border-width=".1mm" height="5cm" width="16cm">
			<fo:table-column column-width="3cm"/>
			<fo:table-column column-width="3cm"/>
			<fo:table-column column-width="2.6cm"/>
			<fo:table-column column-width="3.4cm"/>
			<fo:table-column column-width="4cm"/>
			<fo:table-body  font-weight="normal" font-size="6pt" >
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="4" number-rows-spanned="1" background-color="gray">
						<fo:block font-size="10pt"  color="white" > DATOS DE LOS FLETES   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-left-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="gray">
						<fo:block font-size="10pt"  color="white" > SEGURO DE LA MERCANCIA  </fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" font-style="bold"  color="#64646F"> VALOR TOTAL DEL FLETE   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  color="black">   <xsl:value-of select="valor"/></fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.2pt"
						border-style="solid"
						border-color="white" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="2" number-rows-spanned="1" background-color="gray">
						<fo:block font-size="10pt"  color="white" > PAGO DE SALDO </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" font-style="bold"  color="#64646F"> COMPA�IA DE SEGUROS </fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" font-style="bold"  color="#64646F"> RETENCION A LA FUENTE   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  color="black"> $ <xsl:value-of select="retencion"/>  </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  font-style="bold"  color="#64646F"> LUGAR </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="6pt" font-style="bold"  color="#64646F"> FECHA(DD/MM/AA)CONFORME A LA LEY </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" color="black"> <xsl:value-of select="seguro"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" font-style="bold"  color="#64646F"> DESCUENTOS DE LEY   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  color="black"> $  <xsl:value-of select="descuento"/></fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" color="black"> <xsl:value-of select="lugar"/> </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" color="black"> <xsl:value-of select="fecha"/> </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" font-style="bold"  color="#64646F"> POLIZA No.</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" font-style="bold"  color="#64646F"> FLETE NETO   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  color="black"> $ <xsl:value-of select="flete"/> </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" font-style="bold"  color="#64646F"> CARGUE PAGADO POR(CONFORME A LA LEY)</fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" color="black"> <xsl:value-of select="poliza"/> </fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" font-style="bold"  color="#64646F"> VALOR ANTICIPO   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  color="black"> $  <xsl:value-of select="valora"/></fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" color="black"> <xsl:value-of select="cargue"/> </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" font-style="bold"  color="#64646F"> VIGENCIA DE POLIZA HASTA </fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" font-style="bold"  color="#64646F"> NETO A PAGAR   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  color="black"> $  <xsl:value-of select="neto"/></fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" font-style="bold"  color="#64646F"> DESCARGUE PAGADO POR(CONFORME A LA LEY) </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" color="black"> <xsl:value-of select="vigencia"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black"
						border-bottom-color="white" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > VALOR TOTAL DEL FLETE EN LETRAS   </fo:block>
						<fo:block font-size="7pt" > (CONFORME A LA LEY)  </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-bottom-color="black"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  color="black"> <xsl:value-of select="descargue"/></fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-bottom-color="black"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > </fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.01pt"
						border-style="solid"
						border-color = "white"
						border-rigth-color="black"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" color="black">  <xsl:value-of select="valort"/> </fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body> 
		</fo:table>
	</xsl:template>
					
	<xsl:template match="tabla6"> 
		<fo:table table-layout="fixed"  border-style="solid"  border-color="black" border-width=".1mm" height="5cm" width="8cm">
			<fo:table-column column-width="8cm"/>
			<fo:table-body  font-weight="normal" font-size="6pt" >
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black"
						text-align="center" vertical-align="middle"
						number-columns-spanned="1" number-rows-spanned="1" background-color="gray">
						<fo:block font-size="10pt"  color="white" > OBSERVACIONES   </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black"
						border-bottom-color="white" 
						text-align="left" vertical-align="middle"
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" color="black" margin-left='0.125cm'> <xsl:value-of select="soporte"/>    </fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black"
						border-bottom-color="white"
						border-top-color="white"
						text-align="left" vertical-align="middle"
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" color="black" margin-left='0.125cm'> <xsl:value-of select="observacion"/>    </fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black"
						border-bottom-color="white"
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" color="black"> <xsl:value-of select="fecha_cargue"/>    </fo:block>
					</fo:table-cell>
				</fo:table-row>

			</fo:table-body>
		</fo:table>
		
	</xsl:template>
</xsl:stylesheet>


