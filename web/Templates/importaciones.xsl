<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:param name="page-count"/>
    <xsl:template match="/">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="simple"
                          page-height="29.7cm"
                          page-width="21cm"
                          margin-top="1cm"
                          margin-bottom="1cm"
                          margin-left="1.5cm"
                          margin-right="1.5cm">
                    <fo:region-body margin-top="1cm" margin-bottom="2cm"/>
                    <fo:region-before extent="1cm"/>
                    <fo:region-after extent="1.5cm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="simple"  initial-page-number="1">
                 <fo:static-content flow-name="xsl-region-after">
                 <fo:block font-size="9pt" text-align="right">
                 <fo:inline font-weight="bold">Página </fo:inline>                
                 <fo:inline font-weight="bold"><fo:page-number/> / <xsl:value-of select="$page-count"/></fo:inline>
                 </fo:block>
                 </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <xsl:apply-templates select="raiz"/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <xsl:template match="raiz">
            <xsl:apply-templates select="data"/>
    </xsl:template>
 <!-- break-before="page" -->

    <xsl:template match="data">
        <fo:block>
            <xsl:apply-templates select="cabecera"/>
        </fo:block>

<!--
        <fo:block>
            <xsl:apply-templates select="reporte"/>
        </fo:block>
-->
    </xsl:template>



    <xsl:template match="cabecera">
        <fo:block>
        <fo:table table-layout="fixed" height="3cm">
            <fo:table-column column-width="3.19cm"/>
            <fo:table-column column-width="14.25cm"/>
            <fo:table-body>
                <fo:table-row keep-with-next ="always">
                    <fo:table-cell
                            border-width="1pt"
                            border-style="solid"
                            border-color="black"
                            number-columns-spanned="1"
                            text-align="center" vertical-align="middle"
                            background-color="white">
                        <fo:external-graphic  vertical-align="middle" src="url(images/logo.bmp)"/>
                    </fo:table-cell>
                    <fo:table-cell
                            border-width="1pt"
                            border-style="solid"
                            border-color="black"
                            number-columns-spanned="1"
                            text-align="center" vertical-align="middle"
                            background-color="white">
                            <fo:table table-layout="fixed">
                            <fo:table-column column-width="14.25cm"/>
                                    <fo:table-body border-color="black" border-style="solid" border-bottom-color="white">

                                    <fo:table-row keep-with-next ="always">
                                                <fo:table-cell
                                                            border-width="0.5pt"
                                                            border-style="none"
                                                            border-color="black"                                                                        
                                                            text-align="center" vertical-align="middle"
                                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                            <fo:block font-size="12pt" color="black" padding-before="0.5cm">TRANSPORTES SANCHEZ POLO S.A.</fo:block>
                                                    </fo:table-cell>
                                    </fo:table-row>
                                    <fo:table-row keep-with-next ="always">
                                                <fo:table-cell
                                                            border-width="0.5pt"
                                                            border-style="none"
                                                            border-color="black"
                                                            text-align="center" vertical-align="middle"
                                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                            <fo:block font-size="12pt" color="black"><xsl:value-of select="res"/>REPORTE DE IMPORTACIÓN</fo:block>
                                                    </fo:table-cell>
                                    </fo:table-row>
                                    <fo:table-row keep-with-next ="always">
                                                <fo:table-cell
                                                            border-width="0.5pt"
                                                            border-style="none"
                                                            border-color="black"
                                                            text-align="center" vertical-align="middle"
                                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                            <fo:block font-size="12pt" color="black">IMPORTACIÓN No.  <xsl:value-of select="importacion"/></fo:block>
                                                    </fo:table-cell>
                                    </fo:table-row>
                            </fo:table-body>
                            </fo:table>
                </fo:table-cell>
            </fo:table-row>

            <fo:table-row  keep-with-next ="always" height="3cm">
            </fo:table-row>

            <fo:table-row keep-with-next ="always">
                <fo:table-cell>
                  <fo:block>
                      <xsl:apply-templates select="reporte"/>
                  </fo:block>
                </fo:table-cell>
            </fo:table-row>

            <fo:table-row keep-with-next ="always" height="10cm">
            </fo:table-row>

    </fo:table-body>
    </fo:table>
     </fo:block>


    </xsl:template>


    <!--reporte  break-after="page" -->
    <xsl:template match="reporte">
        <fo:block >
        <fo:table table-layout="fixed" border-color="white" border-style="solid">
            <fo:table-column column-width="15cm"/>
            <fo:table-body>
 			    <fo:table-row >
                    <fo:table-cell>

                        <fo:table table-layout="fixed"  height="0.7cm" border-color="black" border-style="solid">
                            <fo:table-column column-width="5.5cm"/>
                            <fo:table-column column-width="7.2cm"/>

                            <fo:table-body>
                                <!-- contenedor -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell text-align="left" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">CONTENEDOR</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="contenedor"/></fo:block>
                                    </fo:table-cell>

                                </fo:table-row>

                                <!-- conductor -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">CONDUCTOR</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="conductor"/></fo:block>
                                    </fo:table-cell>

                                </fo:table-row>

                                <!-- placa -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">PLACA CAMIÓN</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="placa"/></fo:block>
                                    </fo:table-cell>

                                </fo:table-row>

                                <!-- hora salida -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">HORA SALIDA</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="hora_salida"/></fo:block>
                                    </fo:table-cell>

                                </fo:table-row>

                                <!-- fecha de cargue en puerto -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">FECHA DE CARGUE EN PUERTO</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="cargue"/></fo:block>
                                    </fo:table-cell>

                                </fo:table-row>
                                <!-- fecha llegada cdr -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">FECHA LLEGADA CDR</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="fechallegadacdr"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>

                                <!-- fecha salida cdr -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">FECHA SALIDA CDR</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="fecha_salida_cdr"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>

                                <!-- contenedor en buen estado -->
                                <!--
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">CONTENEDOR EN BUEN ESTADO</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="contenedor_en_buen_estado"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                -->

                                <!-- contenedor limpio -->
                                <!--
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">CONTENEDOR LIMPIO</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="contenedor_limpio"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                -->

                                <!-- patio contenedores -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">PATIO DE CONTENEDORES</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="patio_de_contenedores"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>

                                <!-- ot transporte -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">OT. TRANSPORTE</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="ot_transporte"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>

                                <!-- factura -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">No. FACTURA</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="factura"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>

                                <!-- fecha emision factura -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">FECHA EMISIÓN FACTURA TSP</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="fecha_emision_factura"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>


                                <!-- valor por devolucion -->
                                <!--
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">VALOR POR DEVOLUCIÓN</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="valor_por_devolucion"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                -->
                                <!-- valor por transporte -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">VALOR POR TRANSPORTE</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="valor_por_transporte"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>

                                <!-- valor total factura -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">VALOR TOTAL FACTURA</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="valor_total_factura"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>

                            </fo:table-body>
                        </fo:table>
                    </fo:table-cell>
                </fo:table-row>
                
                <fo:table-row height="1cm">
                </fo:table-row>

                <fo:table-row>
                    <fo:table-cell>
                        <fo:table table-layout="fixed"  height="0.7cm" border-color="black" border-style="solid">
                            <fo:table-column column-width="5.5cm"/>
                            <fo:table-column column-width="7.2cm"/>

                            <fo:table-body>
                                <!-- observaciones -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell text-align="left" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">OBSERVACIONES</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="observaciones"/></fo:block>
                                    </fo:table-cell>

                                </fo:table-row>

                                <!-- fecha est arribo -->
                                <fo:table-row keep-with-next ="always" height="0.5cm">

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm">FECHA ESTIMADA ARRIBO</fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell border-top-width="0.3pt" border-top-style="solid" border-top-color="black"
                                                   border-left-width="0.3pt" border-left-style="solid" border-left-color="black"
                                                   text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                   number-rows-spanned="1" background-color="white">
                                        <fo:block font-weight="bold" font-size="7pt" padding-before="0.18cm" start-indent="0.18cm"><xsl:value-of select="arribo"/></fo:block>
                                    </fo:table-cell>

                                </fo:table-row>
                            </fo:table-body>
                        </fo:table>
                    </fo:table-cell>
                </fo:table-row>


           </fo:table-body>
         </fo:table>
        </fo:block>
    </xsl:template >



</xsl:stylesheet>
