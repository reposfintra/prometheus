<?xml version="1.0" encoding="iso-8859-1"?>

 <xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
        
	<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  		<fo:layout-master-set>
			<fo:simple-page-master master-name="simple"
				 page-height="28cm"
							  page-width="21.5cm"
							  margin-top="0cm"
							  margin-bottom="0.5cm"
							  margin-left="0.5cm"
							  margin-right="5.0cm">
			
			  <fo:region-body  margin-top="0.2cm" margin-bottom="0.5cm"/>
			  <fo:region-before />
			  <fo:region-after />
			</fo:simple-page-master>
  		</fo:layout-master-set>
  
		<fo:page-sequence master-reference="simple">
			                                                                                                      
                                <fo:flow flow-name="xsl-region-body">                                                                                                                         
					<xsl:apply-templates select="raiz"/>                                                                               
				</fo:flow>
                                


		</fo:page-sequence>
	</fo:root>
        
</xsl:template>

        <xsl:template match="raiz">	                                     
                  <xsl:apply-templates select="data"/>
	</xsl:template>

        <xsl:template match="data">
          
       
                   
                                
            <fo:block> 
                <xsl:apply-templates select="resoluciones"/>                                                                  
            </fo:block>  
                             
            
            <fo:block>
    		  <xsl:apply-templates select="datos_clientes"/>
            </fo:block>

  	    <fo:block>
    		  <xsl:apply-templates select="descripcionfac"/>
            </fo:block>
            
             <fo:block>
    		  <xsl:apply-templates select="barracolumna"/>
            </fo:block>
            
            <fo:block-container height="7.5cm" top="20cm" left="1cm" width="12cm" absolute-position='absolute'  z-index="2"> 
            <fo:block>
    		  <xsl:apply-templates select="datos_items"/>
            </fo:block>
            </fo:block-container>                 
               
            <fo:block-container height="6cm" top="20cm" left="1cm" width="12cm" absolute-position='absolute' z-index="1">        
             <fo:block>
    		  <xsl:apply-templates select="total"/>
            </fo:block>
            </fo:block-container>
                        
            
            <!-- Fin de Pagina -->
           <!-- <fo:block font-size="12pt" space-after.optimum="100pt"></fo:block> -->
                       
            
            <fo:block> 
                <xsl:choose>
                  <xsl:when test="position() != last()">
                      <xsl:attribute name="break-after">page</xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                      <xsl:attribute name="break-after">auto</xsl:attribute>
                  </xsl:otherwise> 
                </xsl:choose> 
            </fo:block>   
                
        </xsl:template>
        
        <!--Inicio Resoluciones -->
        <xsl:template match="resoluciones">
                             
                
        
        <fo:table table-layout="fixed"  border-color="white" border-style="solid" >
                <fo:table-column column-width="12cm"/>
                <fo:table-column column-width="8cm"/>
                <fo:table-body> 
                          
                   <fo:table-row>                                   
                      <fo:table-cell                                          
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="white"
                        height="0.8cm"
                        text-align="center" vertical-align="middle"
                        number-columns-spanned="2" number-rows-spanned="1" background-color="white">                 
                       <fo:block font-size="10pt" color="black" padding-after="0.2cm">  
                            <xsl:value-of select="resolucion_dian"/>
                        </fo:block>  
                      </fo:table-cell>                                                       
                  </fo:table-row>               
                  <!-- Fin Fila Resolucion DIAN -->
                  
                  
                  
                  <fo:table-row>                                   
                      <fo:table-cell                                          
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="white"
                        height="0.3cm"
                        text-align="center" vertical-align="middle"
                        number-columns-spanned="2" number-rows-spanned="1" background-color="white">                 
                       <fo:block font-size="10pt" color="black" padding-after="3.3cm">  
                            <xsl:value-of select="resolucion_gran"/>
                        </fo:block>  
                      </fo:table-cell>                                                       
                  </fo:table-row>               
                  <!-- Fin Fila Resolucion Gran Contribuyente -->
                  
                </fo:table-body>  
            </fo:table> 
        </xsl:template >        
        <!--Fin Resoluciones -->
        
        <xsl:template match="datos_clientes">
	<fo:table table-layout="fixed"  border-color="white" border-style="solid" >
		
                                        <fo:table-column column-width="2.0cm"/>
                                        <fo:table-column column-width="4.0cm"/>
                                        <fo:table-column column-width="6.0cm"/>
                                        <fo:table-column column-width="3.0cm"/>
                                        <fo:table-column column-width="4.5cm"/>

   		<fo:table-body>


                                         <!--*************1� FILA*********-->
                                        <fo:table-row >

                                        <fo:table-cell
                                          
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                         
                                        </fo:table-cell>

                                          <fo:table-cell
                                          
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="10pt" color="black" padding-before="0.075cm"><xsl:value-of select="cliente"/></fo:block>
                                        </fo:table-cell>

                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                           
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"                                            
                                            text-align="right" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="10pt" color="black" padding-before="0.075cm" padding-right="1cm"><xsl:value-of select="lugar"/></fo:block>
                                        </fo:table-cell>

                                        </fo:table-row>
                                        <!--*************2� FILA*********-->
                                        <fo:table-row >

                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                           
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="10pt" color="black" padding-before="0.075cm"><xsl:value-of select="nit"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                           
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="10pt" color="black" padding-before="0.075cm"> <xsl:value-of select="fecha"/></fo:block>
                                        </fo:table-cell>

                                        </fo:table-row>
                                        <!--*************FIN 2� FILA*********-->
                                        
                                        <!--*************3� FILA*********-->
                                        <fo:table-row >
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                         
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="10pt" color="black" padding-before="0.075cm"> <xsl:value-of select="direccion"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                           
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                           
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--*************FIN 3� FILA*********-->

                                        <!--*************4� FILA*********-->
                                        <fo:table-row >

                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">                                           
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="10pt" color="black" padding-before="0.075cm"> <xsl:value-of select="telefono"/></fo:block>
                                        </fo:table-cell>
                                        

                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                           
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white" text-align="right">
                                            <fo:block font-size="10pt" color="black"  padding-before="0.025cm"><xsl:value-of select="factura"/></fo:block>
                                        </fo:table-cell>

                                        </fo:table-row>
                                        <!--*************FIN 4� FILA*********-->

                                        <!--*************5� FILA*********-->
                                        <fo:table-row >

                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="10pt" color="black" padding-before="0.075cm"> <xsl:value-of select="ciudad"/></fo:block>
                                        </fo:table-cell>
                                        

                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                          
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="0.5cm"
                                            vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white" text-align="right">
                                            <!--<fo:block font-size="10pt" color="black"  padding-before="0.025cm"><xsl:value-of select="factura"/></fo:block>-->
                                        </fo:table-cell>

                                        </fo:table-row>
                                        
                                        
                                         <fo:table-row >

                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            height="4.0cm"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="5" number-rows-spanned="1" background-color="white">
                                          
                                        </fo:table-cell>
                                         </fo:table-row >
                                        
                              </fo:table-body>  
                      </fo:table> 

</xsl:template >


<xsl:template match="descripcionfac">
	<fo:table table-layout="fixed" border-color="white" border-style="solid">		
                                        <fo:table-column column-width="3.5cm"/>
                                        <fo:table-column column-width="3.5cm"/>
                                        <fo:table-column column-width="3.5cm"/>
                                        <fo:table-column column-width="3.5cm"/>
                                        <fo:table-column column-width="5.5cm"/>

   		<fo:table-body>
                
                        
                        
                        <fo:table-row>
                        
                            <fo:table-cell									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.6cm"
                                            border-color="white"                                                                                                                        
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.075cm"> <xsl:value-of select="descripcion_fac1"/></fo:block>
                             </fo:table-cell>
                             
                             <fo:table-cell									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.6cm"
                                            border-color="white"                                                                                                                        
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.075cm"> <xsl:value-of select="descripcion_fac2"/></fo:block>
                             </fo:table-cell>
                             
			</fo:table-row>
                        
                         <fo:table-row>
                            <fo:table-cell									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.6cm"
                                            border-color="white"                                                                                                                        
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.075cm"> <xsl:value-of select="descripcion_fac3"/></fo:block>
                             </fo:table-cell>
                             
                             <fo:table-cell									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.6cm"
                                            border-color="white"                                                                                                                        
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                        <fo:block font-size="9pt" color="black" padding-before="0.075cm"> <xsl:value-of select="descripcion_fac4"/></fo:block>
                             </fo:table-cell>
			</fo:table-row>
                        
                        <fo:table-row>
                            <fo:table-cell									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.5cm"
                                            border-color="white"                                                                                                                         
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="5" number-rows-spanned="1" background-color="white">
                                            
                             </fo:table-cell>
			</fo:table-row>

		</fo:table-body>          
    </fo:table>

</xsl:template >

<xsl:template match="datos_items">
	<fo:table table-layout="fixed" border-color="white" border-style="solid" >
		
                                        <fo:table-column column-width="5cm"/>
                                        <fo:table-column column-width="3cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                        <fo:table-column column-width="1.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                        
                          
   		<fo:table-body>

                        <fo:table-row>                
                                       <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" margin-left='0.1cm' color="black" padding-before="0.040cm"><xsl:value-of select="remesa"/></fo:block>
                                        </fo:table-cell>
                                        
                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.040cm"> <xsl:value-of select="fecha"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.040cm"> <xsl:value-of select="placa"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.040cm"> <xsl:value-of select="origen"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.050cm"> <xsl:value-of select="destino"/></fo:block>
                                        </fo:table-cell>
                                        
                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="right" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" margin-right='0.125cm' padding-before="0.040cm"> <xsl:value-of select="cantidad"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.040cm"> <xsl:value-of select="unidad"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="right" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" margin-right='0.125cm' padding-before="0.040cm"> <xsl:value-of select="valor"/></fo:block>
                                        </fo:table-cell>
              
			</fo:table-row>
                                        
		</fo:table-body>          
    </fo:table>
    
     <fo:block> <xsl:apply-templates select="descripcion"/></fo:block>        
     
</xsl:template >




<xsl:template match="descripcion">
	<fo:table table-layout="fixed" border-color="white" border-style="solid">
                <fo:table-column column-width="5cm"/>
                <fo:table-column column-width="3cm"/>
                <fo:table-column column-width="2.5cm"/>
                <fo:table-column column-width="2.5cm"/>
                <fo:table-column column-width="2.5cm"/>
                <fo:table-column column-width="1.5cm"/>
                <fo:table-column column-width="0.5cm"/>
                <fo:table-column column-width="2.5cm"/>
   		<fo:table-body>
                    <fo:table-row>
                            <fo:table-cell
                                border-width="0.5pt"
                                border-style="solid"
                                border-color="white"                                                                                      
                                text-align="left" vertical-align="middle"
                                number-columns-spanned="7" background-color="white">                                                       
                                <fo:block font-size="9pt"  color="black" margin-left='0.1cm' padding-before="0.040cm"> <xsl:value-of select="s_descripcion"/></fo:block>                                            
                            </fo:table-cell>  
                            
                            <fo:table-cell
                                border-width="0.5pt"
                                border-style="solid"
                                border-color="white"                                                                                      
                                text-align="right" vertical-align="middle"
                                number-columns-spanned="1" background-color="white">                                                       
                                <fo:block font-size="9pt"  color="black" margin-left='0.1cm' padding-before="0.040cm"><xsl:value-of select="anexo"/></fo:block>                                            
                            </fo:table-cell>
                    </fo:table-row>                                          
		</fo:table-body>          
    </fo:table>
</xsl:template>


<xsl:template match="barracolumna">
	<fo:table table-layout="fixed" border-color="white" border-style="solid">
	
                                      
                                       <fo:table-column column-width="4.5cm"/>
                                        <fo:table-column column-width="3.0cm"/>
                                        <fo:table-column column-width="3.0cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                        <fo:table-column column-width="1.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="2.5cm"/>
                                      
                                        
   		<fo:table-body>

                        <fo:table-row>                
                                       <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-bottom-color="black"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" >
                                            <fo:block font-size="7pt"  color="black" padding-before="0.075cm">DOCUMENTO DE ORIGINADOR</fo:block>
                                        </fo:table-cell>
                                        
                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-bottom-color="black"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" >
                                            <fo:block font-size="7pt" font-style="bold"  color="black" padding-before="0.075cm">ORIGEN</fo:block>
                                        </fo:table-cell>
                                        
                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-bottom-color="black"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" >
                                            <fo:block font-size="7pt" font-style="bold"  color="black" padding-before="0.075cm">DESTINO</fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-bottom-color="black"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" >
                                            <fo:block font-size="7pt" font-style="bold"  color="black" padding-before="0.075cm">CANTID. UNID.</fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            border-bottom-color="black"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" >
                                            <fo:block font-size="7pt" font-style="bold"  color="black" padding-before="0.075cm">VAL.FLETE</fo:block>
                                        </fo:table-cell>
                                        
                                        
                                        
			</fo:table-row>                                               

		</fo:table-body>          
    </fo:table>

</xsl:template >



<xsl:template match="total">
	<fo:table table-layout="fixed" border-color="white" border-style="solid">
	
                                        <fo:table-column column-width="13.0cm"/>
                                        <fo:table-column column-width="1.5cm"/>
                                        <fo:table-column column-width="5.5cm"/>
                                        
   		<fo:table-body>

                        <fo:table-row>                
                                       <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="10pt" font-style="bold"  color="white" padding-before="0.045cm">TOTAL A CARGO DEL ACEPTANTE</fo:block>
                                        </fo:table-cell>
                                        
                                         <fo:table-cell									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                          
                                         </fo:table-cell>
                                                                                                                       
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                      
                                            text-align="right" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt"  color="black" margin-right='0.125cm' padding-before="0.35cm"> <xsl:value-of select="valortotal"/></fo:block>
                                        </fo:table-cell>
                                        
			</fo:table-row>
                        <fo:table-row>
                            <fo:table-cell									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"                                                                                                                        
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" margin-left='1.50cm' padding-before="0.45cm"> <xsl:value-of select="valorletras"/></fo:block>
                             </fo:table-cell>
			</fo:table-row>
                    
                        
                        <fo:table-row>
                        
                        

                         
                          <fo:table-cell>
                            <fo:table table-layout="fixed" border-color="white" border-style="solid">
		                        
                                        <fo:table-column column-width="8.75cm"/>
                                        <fo:table-column column-width="8.75cm"/>
   		                   <fo:table-body>
                                              <fo:table-row>
                                               <fo:table-cell
                            									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"
                                           
                                                                                                                          
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" margin-left='0.125cm'  color="white" >FORMA DE PAGO</fo:block>
                                                </fo:table-cell>
                                                 <fo:table-cell
                            									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"
                                           
                                                                                                                          
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" margin-left='0.125cm' color="white" padding-before="0.075cm">INTERESES MORATORIOS</fo:block>
                                                </fo:table-cell>
                                              </fo:table-row>
                                              <fo:table-row>
                                              <fo:table-cell									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.5cm"
                                            border-color="white"
                                                                                                                             
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="10pt" margin-left='0.125cm'  color="black" padding-before="0.05cm"> <xsl:value-of select="formapago"/></fo:block>
                                             </fo:table-cell>
                                            <fo:table-cell									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.5cm"
                                            border-color="white"
                                                                                                                         
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="5pt" margin-left='0.125cm'  color="white" padding-before="0.075cm"> </fo:block>
                                             </fo:table-cell>
                                              </fo:table-row>
                                  </fo:table-body>
                            </fo:table> 
                        </fo:table-cell>
                   </fo:table-row>         
                        
                 <fo:table-row>
                            <fo:table-cell									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"
                                                                                                              
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" color="white" padding-before="0.075cm"> OBSERVACIONES:</fo:block>
                             </fo:table-cell>
			</fo:table-row>
                        <fo:table-row>
                            <fo:table-cell									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.8cm"
                                            border-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" color="black" padding-before="0.075cm"> <xsl:value-of select="observaciones"/></fo:block>
                             </fo:table-cell>
			</fo:table-row>
                        
                        <fo:table-row>
                         
                          <fo:table-cell>
                            <fo:table table-layout="fixed" border-color="white" border-style="solid">
		                        
                                        <fo:table-column column-width="8.75cm"/>
                                        <fo:table-column column-width="8.75cm"/>
   		                   <fo:table-body>
                                              <fo:table-row>
                                               <fo:table-cell
                            									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"
                                           
                                                                                                                          
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" color="white" margin-left='0.125cm'  padding-before="0.075cm">FIRMA DEL LIBRADOR(TRANSPORTADOR)</fo:block>
                                                </fo:table-cell>
                                                 <fo:table-cell
                            									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"
                                           
                                                                                                                              
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" color="white" margin-left='0.125cm'  padding-before="0.075cm">ACEPTADA</fo:block>
                                                </fo:table-cell>
                                              </fo:table-row>
                                              <fo:table-row>
                                             <fo:table-cell									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.7cm"
                                            border-color="white"
                                                                                                                       
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="5pt" margin-left='0.125cm' color="white" padding-before="0.075cm"> </fo:block>
                                             </fo:table-cell>
                                            <fo:table-cell									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.7cm"
                                            border-color="white"
                                                                                                                       
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="5pt" margin-left='0.125cm'  color="white" padding-before="0.075cm"> </fo:block>
                                             </fo:table-cell>
                                              </fo:table-row>
                                              <fo:table-row>
                                               <fo:table-cell
                            									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.3cm"
                                            border-color="white"
                                           
                                                                                                                              
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" margin-left='0.125cm'  color="white" padding-before="0.075cm">NIT. 890.103.161-1</fo:block>
                                                </fo:table-cell>
                                                 <fo:table-cell
                            									
					    border-width="0.5pt"
                                            border-style="solid"
                                            height="0.5cm"
                                            border-color="white"
                                           
                                                                                                                       
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                           
                                                </fo:table-cell>
                                              </fo:table-row>
                                              
                                  </fo:table-body>
                            </fo:table> 
                        </fo:table-cell>
                   </fo:table-row> 
                          
		</fo:table-body>          
    </fo:table>

</xsl:template >

</xsl:stylesheet>


