<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				
				<fo:simple-page-master master-name="simple"
					margin-right="1cm"
					margin-left="1cm"
					margin-bottom="1cm"
					margin-top="0.5cm"
					page-width="27cm"
					page-height="24cm"
					>
					<fo:region-body margin-top="1cm" margin-bottom="1.5cm"/>
					<fo:region-before extent="1cm"/>
					<fo:region-after extent="1.5cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			
			<fo:page-sequence master-reference="simple">
				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="raiz"/>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	
	<xsl:template match="raiz">
		<xsl:apply-templates select="data"/>	
		<xsl:apply-templates select="data1"/>	
	</xsl:template>
	
	<xsl:template match="data">
       <fo:block>
			<xsl:apply-templates select="tabla1"/>
       </fo:block>
		<fo:block height="5cm" width="18cm" >
	  		<xsl:apply-templates select="valores"/>
		</fo:block>
	   <fo:block>
	  		<xsl:apply-templates select="tabla3"/>
		</fo:block>
		<fo:block font-size="12pt" 
            font-family="sans-serif" 
            space-after.optimum="15pt"
            text-align="center"
            break-after="page">
	        <xsl:value-of select="trafico"/>
		</fo:block>
	</xsl:template>
		
	<xsl:template match="data1">
		<fo:block>
			<xsl:apply-templates select="tabla11"/>
       </fo:block>
	   <fo:block>
			<xsl:apply-templates select="tabla21"/>
       </fo:block>
	   <fo:block>
			<xsl:apply-templates select="tabla31"/>
       </fo:block>
	   
	   <fo:block-container height="5.5cm" width="24cm" >
			<xsl:apply-templates select="valores1"/>
		</fo:block-container>
		
	   <fo:table table-layout="fixed"  border-style="solid"  border-color="white" background-color="white" border-width=".1mm" height="5.5cm">
			<fo:table-column column-width="16cm"/>
			<fo:table-column column-width="8cm"/>
			<fo:table-body font-family="sans-serif" font-weight="normal" font-size="6pt" >
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
					   <fo:block>
							<xsl:apply-templates select="tabla51"/>
       					</fo:block>
					 </fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-left-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block>
							<xsl:apply-templates select="tabla61"/>
       					</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<fo:table table-layout="fixed"  border-style="solid"  border-color="white" border-width=".1mm" height="1.7cm">
			<fo:table-column column-width="12cm"/>
			<fo:table-column column-width="12cm"/>
			<fo:table-body font-family="sans-serif" font-weight="normal" font-size="6pt" >
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						height="1.6cm"
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="10pt" > FIRMA Y SELLO AUTORIZADOS POR LA EMPRESA</fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						height="1.6cm"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="10pt" > FIRMA Y SELLO PROPIETARIO OCONDUCTOR</fo:block>
					</fo:table-cell>
	  			</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<fo:block font-size="12pt" 
            font-family="sans-serif" 
            space-after.optimum="15pt"
            text-align="center"
            break-after="page">
	        <xsl:value-of select="trafico1"/>
		</fo:block>
	</xsl:template>
	
	
	<xsl:template match="tabla1"> 
      <fo:table table-layout="fixed">
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>

   	<fo:table-body>
		<fo:table-row>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white"
				border-left-color="white"
				border-right-color="white"	
		  		text-align="center" vertical-align="middle"
		 		 number-columns-spanned="18" number-rows-spanned="1" background-color="white">
	      		<fo:block font-size="10pt">  
					<xsl:value-of select="leyenda"/>
				</fo:block>
	    	</fo:table-cell>	
        </fo:table-row>
		
		<fo:table-row>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white"
				border-left-color="white"
				border-right-color="white"	
		  		text-align="center" vertical-align="middle"
		 		 number-columns-spanned="7" number-rows-spanned="1" background-color="white">
	      		<fo:block font-size="10pt">  
					<fo:external-graphic src="url(images/logo.bmp)"/>
				</fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white"
				border-left-color="white"
				border-right-color="black"	
				text-align="center" vertical-align="middle"
		  		number-columns-spanned="1" number-rows-spanned="1" background-color="white">
	      		<fo:block font-size="10pt">  </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
		 		border-top-color="black"
				border-left-color="black"
				border-right-color="white"	
		  		text-align="left" vertical-align="middle"
		  		number-columns-spanned="2" number-rows-spanned="1" background-color="white">
				<fo:block font-size="5pt">        </fo:block>
	       		<fo:block font-size="5pt"> AGENCIA       </fo:block>
		 		<fo:block font-size="5pt"> BARRANQUILLA </fo:block>
		 		<fo:block font-size="5pt" > BOGOTA       </fo:block>
		 		<fo:block font-size="5pt"> BUENAVENTURA </fo:block>
		 		<fo:block font-size="5pt"> CALI         </fo:block>
		 		<fo:block font-size="5pt"> CARTAGENA    </fo:block>
	    	</fo:table-cell>
			<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="black"
				border-left-color="white"
				border-right-color="white"	
				text-align="left" 
				vertical-align="middle"
		  		number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="5pt">        </fo:block>
				<fo:block font-size="5pt"> TELEFONO</fo:block>
				<fo:block font-size="5pt"> 3511533</fo:block>
				<fo:block font-size="5pt"> 4123555 </fo:block>
				<fo:block font-size="5pt">  2433442</fo:block>
				<fo:block font-size="5pt">  6666288 </fo:block>
				<fo:block font-size="5pt"> 6625172</fo:block>
	    	</fo:table-cell>
			<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="black"
				border-left-color="white"
				border-right-color="white"	
				text-align="left" vertical-align="middle"
				number-columns-spanned="2" number-rows-spanned="1" background-color="white">
				<fo:block font-size="5pt">        </fo:block>
				<fo:block font-size="5pt"> AGENCIA       </fo:block>
				<fo:block font-size="5pt"> CUCUTA</fo:block>
				<fo:block font-size="5pt"> IPIALES</fo:block>
				<fo:block font-size="5pt"> MARACAIBO</fo:block>
				<fo:block font-size="5pt"> MEDELLIN</fo:block>
				<fo:block font-size="5pt"> QUITO</fo:block>
				<fo:block font-size="5pt"> GUAYAQUIL</fo:block>
	    	</fo:table-cell>
			<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="black"
				border-left-color="white"
				border-right-color="white"	
				text-align="left" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="5pt">        </fo:block>
				<fo:block font-size="5pt">TELEFONO </fo:block>
				<fo:block font-size="5pt"> 5782357</fo:block>
				<fo:block font-size="5pt"> 734807 </fo:block>
				<fo:block font-size="5pt"> 9248480</fo:block>
				<fo:block font-size="5pt"> 2625853 </fo:block>
				<fo:block font-size="5pt"> 503232</fo:block>
				<fo:block font-size="5pt"> 295333</fo:block>
	    	</fo:table-cell>
		 	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="black"
				border-left-color="white"
				border-right-color="white"	
				text-align="left" vertical-align="middle"
				number-columns-spanned="2" number-rows-spanned="1" background-color="white">
				<fo:block font-size="5pt">        </fo:block>
				<fo:block font-size="5pt"> AGENCIA       </fo:block>
				<fo:block font-size="5pt"> RIOACHA </fo:block>
				<fo:block font-size="5pt"> SANTAMARTA        </fo:block>
				<fo:block font-size="5pt"> VALENCIA </fo:block>
				<fo:block font-size="5pt"> TULCAN         </fo:block>
				<fo:block font-size="5pt"> MONTELIBANO    </fo:block>
	    	</fo:table-cell>
			<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="black"
				border-left-color="white"
				border-right-color="black"	
				text-align="left" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="5pt">        </fo:block>
				<fo:block font-size="5pt">TELEFONO</fo:block>
				<fo:block font-size="5pt"> 4273315</fo:block>
				<fo:block font-size="5pt"> 421422 </fo:block>
				<fo:block font-size="5pt"> 8718466</fo:block>
				<fo:block font-size="5pt"> 985854 </fo:block>
				<fo:block font-size="5pt"> 7723224</fo:block>
			</fo:table-cell>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-top-color="white"
				border-right-color="white" 
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="10pt">  </fo:block>
	    	</fo:table-cell>
        </fo:table-row>
			
		<fo:table-row>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> OFICINA DE ORIGEN </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> DIA </fo:block>
		    </fo:table-cell>
		    <fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-left-color="white"  
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> MES </fo:block>
		    </fo:table-cell>
		    <fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-left-color="white" 
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> A�O </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-right-color="white" 
				border-top-color="white" 
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  </fo:block>
			</fo:table-cell>
	  		<fo:table-cell  
				border-width="0.5pt"
				border-left-color="white" 
				border-top-color="black" 
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="9" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-left-color="white" 
				border-right-color="white" 
				border-top-color="white" 
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
	      		<fo:block font-size="8pt">  </fo:block>
	    	</fo:table-cell>
	  	</fo:table-row>
	  
	  	<fo:table-row>
	  		<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-top-color="white" 
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  <xsl:value-of select="oorigen"/>   </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-top-color="white" 
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  <xsl:value-of select="dia"/>  </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-top-color="white" 
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  <xsl:value-of select="mes"/>  </fo:block>
			</fo:table-cell>
			<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-top-color="white" 
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  <xsl:value-of select="ano"/>  </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-right-color="white" 
				border-top-color="white" 
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="11" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">REMESA TERRESTRE DE CARGA No. <xsl:value-of select="cargan"/>     </fo:block>
	    	</fo:table-cell>
		</fo:table-row>
	
		<fo:table-row>
			<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> ORIGEN DE CARGA </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="9" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> REMITENTE </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="5" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> CONSIGNATARIO </fo:block>
	    	</fo:table-cell>
		</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="corigen"/>  </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="9" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="remitente"/>  </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="5" number-rows-spanned="1" background-color="white">

				<fo:block font-size="8pt"> <xsl:value-of select="consignatario"/> </fo:block>
	    	</fo:table-cell>
		</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="9" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> DESTINATARIO </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
			
				number-columns-spanned="6" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> DIRECCION </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> CIUDAD </fo:block>
	    	</fo:table-cell>
		</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="9" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="destinatario"/>  </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="6" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="direccion"/> </fo:block>
		    </fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="ciudad"/>  </fo:block>
		    </fo:table-cell>
		</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> PLANILLA No. </fo:block>
		    </fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> PLACAS </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="10" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> CONDUCTOR </fo:block>
		    </fo:table-cell>
		</fo:table-row>
		
		<fo:table-row>
			<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="planilla"/>  </fo:block>
	 	   	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="placas"/>  </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="10" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="conductor"/>  </fo:block>
		    </fo:table-cell>
		</fo:table-row>
		
		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="18" number-rows-spanned="1" background-color="black">			
         	</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
</fo:table>
</xsl:template>  

<xsl:template match="valores">
	  <fo:table table-layout="fixed"  padding-before="0.cm" font-size="10pt" border-style="solid"  height="5cm">
	    <fo:table-column column-width="3cm"/>
		<fo:table-column column-width="3cm"/>
		<fo:table-column column-width="3cm"/>
		<fo:table-column column-width="9cm"/>
		<fo:table-body>
		  <fo:table-row >
			<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="1" number-rows-spanned="1" background-color="lightgray">
			  <fo:block font-size="10pt">MARCAS</fo:block>
			</fo:table-cell>
			<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="1" number-rows-spanned="1" background-color="lightgray">
			  <fo:block font-size="10pt">BULTOS</fo:block>
			</fo:table-cell>
				<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="1" number-rows-spanned="1" background-color="lightgray">
			  <fo:block font-size="10pt">KILOS</fo:block>
			</fo:table-cell>
				<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="1" number-rows-spanned="1" background-color="lightgray">
			  <fo:block font-size="10pt">IDENTIFICACION DE MERCANCIA</fo:block>
			</fo:table-cell>
		  </fo:table-row>  
		  <xsl:apply-templates select="item"/>
          <fo:table-row>
            <fo:table-cell padding="2px"  number-columns-spanned="4">
			  <fo:block font-size="10pt">
			  <xsl:value-of select="leyenda2"/>
			  </fo:block>
			</fo:table-cell>
		  </fo:table-row>
	    </fo:table-body>
	  </fo:table>
	</xsl:template>
	
	<xsl:template match="item">
	  <fo:table-row padding-before="1cm" keep-together="always">
		<fo:table-cell padding="2px" font-size="9pt">
		  <fo:block><xsl:value-of select="marcas"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="9pt">
		  <fo:block><xsl:value-of select="bultos"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="9pt">
		  <fo:block><xsl:value-of select="kilos"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="9pt">
		  <fo:block><xsl:value-of select="mercancia"/></fo:block>
		</fo:table-cell>
	  </fo:table-row>
	</xsl:template>

<xsl:template match="tabla3"> 
	<fo:table table-layout="fixed">
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
   		<fo:table-body>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> VALOR FLETE </fo:block>
		    </fo:table-cell>
	 	    <fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="9" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> PAGADERO POR </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> NIT. </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> CIUDAD </fo:block>
	    	</fo:table-cell>
	  	</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="flete"/>  </fo:block>
		    </fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="9" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="pagadero"/> </fo:block>
		    </fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="nit"/>  </fo:block>
		    </fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="ciudad"/>  </fo:block>
		    </fo:table-cell>
		</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> MANIFIESTO MEMORIAL </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> GUIA DUANA </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> </fo:block>
		    </fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="7" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> RECIBI CONFORME DESTINATARIO </fo:block>
		    </fo:table-cell>
	  	</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="manifiesto"/>  </fo:block>
		    </fo:table-cell>
	 	    <fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="aduana"/>  </fo:block>
		    </fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
	      		<fo:block font-size="8pt">   </fo:block>
	   		</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="7" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> 25 </fo:block>
		    </fo:table-cell>
		</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="8" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> JEFE DE DESPACHOS </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  </fo:block>
	    	</fo:table-cell>
	   		<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="7" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  </fo:block>
		    </fo:table-cell>
		</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="8" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> <xsl:value-of select="jefe"/> </fo:block>
		    </fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  </fo:block>
	    	</fo:table-cell>
	   		<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="7" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  </fo:block>
		    </fo:table-cell>
	  	</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="white"
				border-top-color="black"	
				text-align="center" vertical-align="middle" 
				number-columns-spanned="18" number-rows-spanned="1" background-color="white">
				<fo:block font-size="7pt"> FIRMADO EL CUMPLIDO NO ACEPTAMOS RECLAMOS,FAVOR ENTREGAR CON FIRMA Y SELLO DEL CONDUCTOR </fo:block>
		    </fo:table-cell>
		</fo:table-row>
		</fo:table-body>
	</fo:table>
  </xsl:template>
  
  
  <xsl:template match="tabla11"> 
		<fo:table table-layout="fixed" height="3cm">
		<fo:table-column column-width="17cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="6cm"/>
		<fo:table-body>
			<fo:table-row>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="white" 
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="black">
					<fo:external-graphic src="url(logo3.bmp)"/>
				</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="white" 
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">
					<fo:block font-size="12pt" ></fo:block>
			    </fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="white" 
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">					
					<fo:table table-layout="fixed">
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						<fo:table-column column-width="0.5cm"/>
						
						<fo:table-body>
						
							<fo:table-row>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="11" number-rows-spanned="1" background-color="gray">
									<fo:block font-size="12pt" color="white"> MANIFIESTO DE CARGA </fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" > <xsl:value-of select="codigor"/> </fo:block>
								</fo:table-cell>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" >  <xsl:value-of select="codigoe"/> </fo:block>
								</fo:table-cell>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="5" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" >  <xsl:value-of select="codigoc"/>  </fo:block>
								</fo:table-cell>
							</fo:table-row>
						
							<fo:table-row>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" > CODIGO REGIONAL </fo:block>
								</fo:table-cell>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="3" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" > CODIGO DE EMPRESA </fo:block>
								</fo:table-cell>
								<fo:table-cell 
									border-width="0.5pt"
									border-style="solid"
									border-color="black" 
									text-align="center" vertical-align="middle"
									number-columns-spanned="5" number-rows-spanned="1" background-color="white">
									<fo:block font-size="7pt" > CODIGO NUMERICO CONSECUTIVO </fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
			    </fo:table-cell>
			</fo:table-row>
		</fo:table-body>
		</fo:table>
		</xsl:template >
	 
	<xsl:template match="tabla21"> 
	<fo:table table-layout="fixed"  height="1.2cm" border-color="white" border-width=".1mm"  >
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		
   		<fo:table-body>
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					border-right-color="white"
					border-left-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" text-align="right" > Rangos Autorizados </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black"
					border-top-color="white"
					border-left-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="18" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" text-align="left" > <xsl:value-of select="rango"/> </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					border-right-color="white"
					border-left-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" text-align="right"> Res.No </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					border-right-color="white"
					border-left-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="10" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" text-align="left"> <xsl:value-of select="res"/> </fo:block>
	    		</fo:table-cell>
			</fo:table-row>
			
	  		<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > FECHA DE EXPEDICION(DD/MM/AA) </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black"
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="2" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="14" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > ORIGEN DEL VIAJE </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="2" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > DESTINO FINAL DEL VIAJE</fo:block>
	    		</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="fechae"/> </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black"
					border-top-color="white"
					border-bottom-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="2" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="14" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="origen"/> </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					border-bottom-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="2" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > </fo:block>
	    		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="destino"/></fo:block>
	    		</fo:table-cell>
			</fo:table-row>
			
		</fo:table-body>
    </fo:table>
	</xsl:template >
	
	<xsl:template match="tabla31"> 
	<fo:table table-layout="fixed" height="4.6cm">
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		
   		<fo:table-body>
	  		<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="48" number-rows-spanned="1" background-color="gray">
					<fo:block font-size="12pt" color="white"> DATOS DEL VEHICULO </fo:block>
	    		</fo:table-cell>
			</fo:table-row>
	  
	  		<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" >   PLACA  </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  MARCA </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  LINEA </fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  MODELO </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >MODELO REPONTECIADO A    </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > SERIE No.    </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="4" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > COLOR     </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > TIPO DE CARROCERIA    </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" >    <xsl:value-of select="placa"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >   <xsl:value-of select="marca"/> </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >   <xsl:value-of select="linea"/></fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  <xsl:value-of select="modelo"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" ><xsl:value-of select="modelor"/> </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="serie"/>    </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="4" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="color"/>    </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="tipoc"/>   </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" >   REGISTRO NACIONAL DE CARGA No.  </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  CONFIGURACION </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  PESO VACIO </fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  NUMERO POLIZA SOAT </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >COMPA�IA DE SEGUROS SOAT    </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > VENCIMIENTO SOAT (DD/MM/AA)   </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > PLACA SEMIREMOLQUE     </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
				
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" >   <xsl:value-of select="registro"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  <xsl:value-of select="configuracion "/></fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  <xsl:value-of select="peso"/></fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="numerop"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="compas"/>  </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="vencimiento"/>   </fo:block>
	    		</fo:table-cell>
				<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="7" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="placas"/>  </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="17" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" >   PROPIETARIO  </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  DOCUMENTO DE IDENTIFICAION No. </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="11" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  DIRECCION </fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  TELEFONO </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > CIUDAD    </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="17" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" >  <xsl:value-of select="propietario"/>  </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="documentop"/></fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="11" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" ><xsl:value-of select="direccionp"/></fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" ><xsl:value-of select="telefonop"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="ciudadp"/>   </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="17" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" >   TENEDOR  </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  DOCUMENTO DE IDENTIFICAION No. </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="11" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  DIRECCION </fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  TELEFONO </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > CIUDAD    </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="17" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" >  <xsl:value-of select="tenedor"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="documentot"/> </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="11" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" ><xsl:value-of select="direcciont"/></fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" ><xsl:value-of select="telefonot"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="ciudadt"/>   </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="17" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" >   CONDUCTOR  </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  DOCUMENTO DE IDENTIFICAION No. </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="11" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  DIRECCION </fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" >  CAT.LIC. CONDUCCION </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > CIUDAD    </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>
			
			<fo:table-row>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="17" number-rows-spanned="1" background-color="white">
	      			<fo:block font-size="7pt" >  <xsl:value-of select="conductor"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="9" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="documentoc"/> </fo:block>
				</fo:table-cell>
	    		<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="11" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" ><xsl:value-of select="direccionc"/></fo:block>
		 		</fo:table-cell>
				<fo:table-cell 
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle"
					number-columns-spanned="5" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" ><xsl:value-of select="catc"/> </fo:block>
	    		</fo:table-cell>
	    		<fo:table-cell  
					border-width="0.5pt"
					border-style="solid"
					border-color="black" 
					border-top-color="white"
					text-align="center" vertical-align="middle" 
					number-columns-spanned="6" number-rows-spanned="1" background-color="white">
					<fo:block font-size="7pt" > <xsl:value-of select="ciudadc"/>  </fo:block>
	    		</fo:table-cell>
	  		</fo:table-row>	
	  	</fo:table-body>
    </fo:table>
    </xsl:template >
	
	
	<xsl:template match="valores1">
	  <fo:table table-layout="fixed"  padding-before="0.cm" font-size="10pt" border-style="solid"  height="5cm">
			<fo:table-column column-width="2cm"/>
			<fo:table-column column-width="1.5cm"/>
			<fo:table-column column-width="1.5cm"/>
			<fo:table-column column-width="1.5cm"/>
			<fo:table-column column-width="1.5cm"/>
			<fo:table-column column-width="1.5cm"/>
			<fo:table-column column-width="1.5cm"/>
			<fo:table-column column-width="3.5cm"/>
			<fo:table-column column-width="3.5cm"/>
			<fo:table-column column-width="3.5cm"/>
			<fo:table-column column-width="2.5cm"/>
		<fo:table-body font-family="sans-serif" font-weight="normal" font-size="7pt" >
		  <fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="11" number-rows-spanned="1" background-color="gray">
						<fo:block font-size="12pt"  color="white" > DATOS DE LA MERCANCIA TRANSPORTADA   </fo:block>
					</fo:table-cell>
	  			</fo:table-row>
				<fo:table-row >
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >NUMERO DE REMESA</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >UNIDAD DE MEDIDA</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >CANTIDAD</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >PESO</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >CODIGO DE NATURALEZA</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >CODIGO DE EMPAQUE</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >CODIGO DE PRODUCTO</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >PRODUCTO TRANSPORTADO</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >REMITENTE</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >DESTINATARIO</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-width="0.5pt" border-color="black" border-style="solid" background-color="lightgrey">
						<fo:block text-align="center" color="black" >DESTINO</fo:block>
					</fo:table-cell>
				</fo:table-row>
		  <xsl:apply-templates select="item1"/>
          <fo:table-row>
            <fo:table-cell padding="2px"  number-columns-spanned="11">
			  <fo:block font-size="10pt">
			  <xsl:value-of select="leyenda21"/>
			  </fo:block>
			</fo:table-cell>
		  </fo:table-row>
	    </fo:table-body>
	  </fo:table>
	</xsl:template>
	
	<xsl:template match="item1">
	  <fo:table-row >
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="numero"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="unidad"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="cantidad"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="peso"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="codigon"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="codigoe"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="codigop"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="producto"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="remitente"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="destinatario"/></fo:block>
		</fo:table-cell>
		<fo:table-cell padding="2px" font-size="7pt">
		  <fo:block><xsl:value-of select="destino"/></fo:block>
		</fo:table-cell>
	  </fo:table-row>
	</xsl:template>
	
	<xsl:template match="tabla51"> 
		<fo:table table-layout="fixed"  border-style="solid"  border-color="black" border-width=".1mm" height="5cm" width="16cm">
			<fo:table-column column-width="3cm"/>
			<fo:table-column column-width="3cm"/>
			<fo:table-column column-width="2.6cm"/>
			<fo:table-column column-width="3.4cm"/>
			<fo:table-column column-width="4cm"/>
			<fo:table-body font-family="sans-serif" font-weight="normal" font-size="6pt" >
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="4" number-rows-spanned="1" background-color="gray">
						<fo:block font-size="10pt"  color="white" > DATOS DE LOS FLETES   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-left-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="gray">
						<fo:block font-size="10pt"  color="white" > SEGURO DE LA MERCANCIA  </fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"

						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > VALOR TOTAL DEL FLETE   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  > $  <xsl:value-of select="valor"/></fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.2pt"
						border-style="solid"
						border-color="white" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="2" number-rows-spanned="1" background-color="gray">
						<fo:block font-size="10pt"  color="white" > PAGO DE SALDO </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > COMPA�IA DE SEGUROS </fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > RETENCION A LA FUENTE   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  > $ <xsl:value-of select="retencion"/>  </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  > LUGAR </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="6pt" > FECHA(DD/MM/AA)CONFORME A LA LEY </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > <xsl:value-of select="seguro"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > DESCUENTOS DE LEY   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  > $  <xsl:value-of select="descuento"/></fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > <xsl:value-of select="lugar"/> </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > <xsl:value-of select="fecha"/> </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > POLIZA No.</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > FLETE NETO   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  > $ <xsl:value-of select="flete"/> </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > CARGUE PAGADO POR(CONFORME A LA LEY)</fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > <xsl:value-of select="poliza"/> </fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > VALOR ANTICIPO   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  > $  <xsl:value-of select="valora"/></fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > <xsl:value-of select="cargue"/> </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > VIGENCIA DE POLIZA HASTA </fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > NETO A PAGAR   </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  > $  <xsl:value-of select="neto"/></fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > DESCARGUE PAGADO POR(CONFORME A LA LEY) </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > <xsl:value-of select="vigencia"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-bottom-color="white" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="10pt" > VALOR TOTAL DEL FLETE EN LETRAS   </fo:block>
						<fo:block font-size="7pt" > (CONFORME A LA LEY)  </fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-top-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="2" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt"  > <xsl:value-of select="descargue"/></fo:block>
					</fo:table-cell>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-bottom-color="black"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="7pt" > </fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						border-bottom-color="white"
						text-align="center" vertical-align="middle" 
						number-columns-spanned="5" number-rows-spanned="1" background-color="white">
						<fo:block font-size="10pt" >  <xsl:value-of select="valort"/> </fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body> 
		</fo:table>
	</xsl:template>
					
	<xsl:template match="tabla61"> 
		<fo:table table-layout="fixed"  border-style="solid"  border-color="black" border-width=".1mm" height="5cm" width="8cm">
			<fo:table-column column-width="8cm"/>
			<fo:table-body font-family="sans-serif" font-weight="normal" font-size="6pt" >
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="gray">
						<fo:block font-size="10pt"  color="white" > OBSERVACIONES   </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell
						border-width="0.5pt"
						border-style="solid"
						border-color="black"
						border-bottom-color="white" 
						text-align="center" vertical-align="middle" 
						number-columns-spanned="1" number-rows-spanned="1" background-color="white">
						<fo:block font-size="10pt" > <xsl:value-of select="observacion"/>    </fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
	</xsl:template>
		
</xsl:stylesheet>









