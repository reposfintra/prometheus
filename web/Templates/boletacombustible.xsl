<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  		<fo:layout-master-set>
			<fo:simple-page-master master-name="simple"
				 page-height="29.7cm"
							  page-width="21cm"
							  margin-top="1cm"
							  margin-bottom="1cm"
							  margin-left="3cm"
							  margin-right="3cm">
			
			  <fo:region-body margin-top="0.5cm" margin-bottom="0.5cm"/>
			  <fo:region-before extent="0cm"/>
			  <fo:region-after extent="0cm"/>
			</fo:simple-page-master>
  		</fo:layout-master-set>
  
		<fo:page-sequence master-reference="simple">
				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="raiz"/>
				</fo:flow>
		</fo:page-sequence>
	</fo:root>
</xsl:template>

        <xsl:template match="raiz">	
		<xsl:apply-templates select="data"/>
	</xsl:template>

	<xsl:template match="data">
        
                                        <xsl:apply-templates select="cabecera"/>
                       
                                        <xsl:apply-templates select="tabla1"/>
               
                                        <xsl:apply-templates select="tabla2"/>
                 
                              
       
	</xsl:template>
	<!-- COMIENZO  CABECERA-->
		<xsl:template match="cabecera"> 
		<fo:table table-layout="fixed" border-color='black' border-style="solid" border-left-color='white' border-right-color='white'>
                        
                        <fo:table-column column-width="2.5cm"/>
                        <fo:table-column column-width="10cm"/>
                        <fo:table-column column-width="2.5cm"/>		
		
                        <fo:table-body >
                      
			<fo:table-row>
                               <fo:table-cell
                                        border-width="0.7pt"
                                        border-style="solid"	
                                        border-color="white"	
                                        border-left-color="black"
                                       		
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white" >
					<fo:block font-size="10pt" padding-before="0.75cm"> BOLETA # : <xsl:value-of select="boleta"/></fo:block>
				</fo:table-cell>
				
                                <fo:table-cell
                                        border-color="white"
                                        border-width="0.5pt"
                                        border-style="solid"		
                                        border-left-color="white"
                                        border-right-color="white"
                                      
                                     
                                        background-color="white">
                                        
					<fo:table table-layout="fixed">
                                        <fo:table-column column-width="10cm"/>
						<fo:table-body>

						<fo:table-row>
					                    <fo:table-cell			
                                                                       				                                                                       
									text-align="center" 
                                                                        vertical-align="middle"
									number-columns-spanned="1" 
                                                                        number-rows-spanned="1" 
                                                                        background-color="white">
									<fo:block font-size="12pt" color="black" padding-before="0.5cm">TRANSPORTE SANCHEZ POLO</fo:block>
								</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
					                    <fo:table-cell	
                                                                      
                                                                     	text-align="center" 
                                                                        vertical-align="middle"
									number-columns-spanned="1" 
                                                                        number-rows-spanned="1" 
                                                                        background-color="white">
									<fo:block font-size="12pt" color="black">BOLETA COMBUSTIBLE</fo:block>
								</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
					</fo:table>
			    </fo:table-cell>
                            
                            <fo:table-cell
                                        border-color="white"
                            	        border-width="0.5pt"
                                        border-style="solid"	
                                    
                                        border-right-color="black"
                                        border-bottom-color="white"				
					number-columns-spanned="1"
					text-align="center" 
                                        vertical-align="middle"
					background-color="white">
					<fo:external-graphic src="url(images/logo.bmp)"/>
			  </fo:table-cell>


				
			</fo:table-row>
		</fo:table-body>
		</fo:table>
</xsl:template >
<!-- FIN CABECERA-->

<!-- COMIENZO DE TABLA1-->
	<xsl:template match="tabla1" > <xsl:variable name="image" select="imagen"/>
	<fo:table table-layout="fixed" border-style="solid" border-color='white'>
		                        
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>                                                                               

   		<fo:table-body >                        
	  		<fo:table-row >
	    		<fo:table-cell	
                                        border-color="white"                
                                        border-width="0.5pt"
                                        border-style="solid"	
                                      
                                        background-color="white"
                                        border-left-color="black"								
					text-align="rigth" vertical-align="middle"
					number-columns-spanned="4" number-rows-spanned="1" >
					<fo:block></fo:block>
	    		</fo:table-cell>
                        
                        <fo:table-cell	
                                        border-color="white"   
                                        border-width="0.5pt"
                                        border-style="solid"	
                                        border-right-color="black"									
					text-align="rigth" 
                                        vertical-align="middle"
					height="0.01cm"
                                        number-columns-spanned="11" number-rows-spanned="1" padding-before="0.2cm" background-color="black">
				       
	    		</fo:table-cell>
                        </fo:table-row>
                        
                        <!-- FIN FILA LINE <xsl:value-of select="boleta"/><fo:external-graphic src="url(images/lineanegra.jpg)"/>-->
                        
                        <fo:table-row>
                        
                        
                         <fo:table-cell  
                                        border-color="white"   
                                        border-width="0.5pt"
                                        border-style="solid"	
                                        background-color="white"
                                        border-left-color="black"
                                        text-align="left" number-columns-spanned="1" number-rows-spanned="1" linefeed-treatment="preserve" white-space-collapse="false" white-space-treatment="preserve" wrap-option="no-wrap">
				
			   </fo:table-cell>         
                           
                           <fo:table-cell
                           border-color="white" 
                                 border-width="0.5pt"
                                        border-style="solid"	
                                height="3cm"                                             
                                       									
                                text-align="center" vertical-align="middle"
				number-columns-spanned="4" number-rows-spanned="1" >
				<fo:external-graphic src="{$image}" margin-left="0.5cm" margin-right="0.5cm"/>
			   </fo:table-cell>  
                           
                          <fo:table-cell 
                                       border-color="white"  
                                        border-width="0.5pt"
                                        border-style="solid"	
                                        background-color="white"
                                        border-right-color="white"       
                                        text-align="left" number-columns-spanned="9" >
				<fo:block font-size="10pt" color="black" padding-before="0.2cm"><xsl:value-of select="leyenda"/></fo:block>
			   </fo:table-cell>     
                            <fo:table-cell 
                                       border-color="white"  
                                        border-width="0.5pt"
                                        border-style="solid"	
                                        background-color="white"
                                        border-right-color="black"       
                                        text-align="left" number-columns-spanned="1" >
				
			   </fo:table-cell>                         
			</fo:table-row>
                        
                        <fo:table-row>
                        <fo:table-cell  
                                        border-color="white" 
                                        border-width="0.5pt"
                                        border-style="solid"	
                                       
                                        background-color="white"
                                        border-left-color="black"
                                        text-align="left" number-columns-spanned="1" number-rows-spanned="1" linefeed-treatment="preserve" white-space-collapse="false" white-space-treatment="preserve" wrap-option="no-wrap">
				
			   </fo:table-cell>         
                           
                           <fo:table-cell
                                        border-color="white" 
                                        border-width="0.5pt"
                                        border-style="solid"	
                                        
                                        background-color="white"
                                       
                                       									
                                text-align="center" vertical-align="middle"
				number-columns-spanned="4" number-rows-spanned="1" >
			   </fo:table-cell>  
                           
                         <fo:table-cell  
                                        border-color="white" 
                                        border-width="0.5pt"
                                        border-style="solid"	
                                       
                                        background-color="white"
                                             
                                        text-align="left" number-columns-spanned="9" >
				<fo:block font-size="10pt" color="black" padding-before="0.2cm"><xsl:value-of select="leyenda2"/></fo:block>
			   </fo:table-cell>
                            <fo:table-cell 
                                       border-color="white"  
                                        border-width="0.5pt"
                                        border-style="solid"	
                                        background-color="white"
                                        border-right-color="black"       
                                        text-align="left" number-columns-spanned="1" >
				
			   </fo:table-cell>      
                        </fo:table-row>
                        
                        
		</fo:table-body>
    </fo:table>
	</xsl:template >
<!-- FIN DE TABLA1-->

<!-- COMIENZO DE TABLA2-->
	<xsl:template match="tabla2"> 
	<fo:table table-layout="fixed"  height="1cm" border-style="solid" border-color='white' >
		<fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>                                                                               

   		<fo:table-body  >                        	  		                        
                        <fo:table-row height="2cm">
                           <fo:table-cell
                                border-color="white"  
                                        border-width="0.5pt"
                                        border-style="solid"	
                            									
                                text-align="center" 
                                vertical-align="middle" 
                                border-left-color="black" 
				number-columns-spanned="7" background-color="white">
				<fo:block font-size="8pt" color="black" padding-before="1.5cm">Firma del funcionario </fo:block>
			   </fo:table-cell>  
                           
                      <fo:table-cell  
                             border-color="white"  
                                        border-width="0.5pt"
                                        border-style="solid"	
                                    
                                      text-align="center" 
                                      vertical-align="middle" 
                                      border-right-color="white" 
                                      number-columns-spanned="7" 
                                      background-color="white">
				<fo:block font-size="8pt" color="black" padding-before="1.5cm">Sello</fo:block>
			   </fo:table-cell>      
                           <fo:table-cell  
                             border-color="white"  
                                        border-width="0.5pt"
                                        border-style="solid"	
                                    
                                      text-align="center" 
                                      vertical-align="middle" 
                                      border-right-color="black" 
                                      number-columns-spanned="1" 
                                      background-color="white">
				
			   </fo:table-cell>                        
			</fo:table-row>
                        
                          <fo:table-row>
                           <fo:table-cell
                               border-color="white"  
                                        border-width="0.5pt"
                                        border-style="solid"										
                                text-align="center" 
                                vertical-align="middle" 
                                border-left-color="black" 
                                border-right-color="black" 
				number-columns-spanned="15" 
                                number-rows-spanned="1" 
                                background-color="white"
                                >
				<fo:block font-size="8pt" color="black" padding-after="0.5cm">Presente la boleta al momento de leer el chip � boton</fo:block>
			   </fo:table-cell>  
                                                                    
                    </fo:table-row>
                    
                    <fo:table-row>
                           <fo:table-cell
                               border-color="white"  
                                        border-width="0.5pt"
                                        border-style="solid"										
                              
                                vertical-align="middle" 
                                border-top-color="black"
                               
                                text-align="center" height="5cm"
				number-columns-spanned="15" number-rows-spanned="1" background-color="white">
				
			   </fo:table-cell>  
                                                                  
                    </fo:table-row>
                    
                        
		</fo:table-body>
    </fo:table>
	</xsl:template >
<!-- FIN DE TABLA2-->
     
</xsl:stylesheet>


