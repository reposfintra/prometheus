<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:param name="periodo"/>   
<xsl:param name="page-count"/>  
<xsl:param name="fecha"/>
<xsl:param name="hora"/>      
<xsl:param name="usuario"/>   
<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  		<fo:layout-master-set>
			<fo:simple-page-master master-name="simple"
				 page-height="29.7cm"
				 page-width="21cm"
				 margin-top="0.5cm"
				 margin-bottom="0.5cm"
			         margin-left="1cm"
				 margin-right="1cm" initial-page-number="1">						                            
                          <fo:region-body margin-top="2.5cm" margin-bottom="2cm"/>
                          <fo:region-after  extent="1.5cm"/>
                          <fo:region-before extent="2.5cm"/>                          
			</fo:simple-page-master>
  		</fo:layout-master-set>
  
		<fo:page-sequence master-reference="simple" initial-page-number="1">
                 <fo:static-content flow-name="xsl-region-after">
                     <fo:block font-size="9pt" text-align="left">
                         <fo:inline font-weight="bold">Elaborado por :</fo:inline>                
                         <fo:inline font-weight="bold"><xsl:value-of select="$usuario"/></fo:inline>
                     </fo:block>
                     <!--Elaborado Por --> 
                    <fo:block font-size="9pt" text-align="center">
                         <fo:inline font-weight="bold">P�gina </fo:inline>                
                         <fo:inline font-weight="bold"><fo:page-number/> / <xsl:value-of select="$page-count"/></fo:inline>
                     </fo:block>
                     <!--Contador -->
                 </fo:static-content>
                
                
                    <fo:static-content flow-name="xsl-region-before">
                        <fo:table table-layout="fixed" height="0.5cm">
                        <fo:table-column column-width="18cm"/>		
                        <fo:table-body>
                            <fo:table-row>				
                                <fo:table-cell									
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">
					<fo:table table-layout="fixed">
                                        <fo:table-column column-width="3cm"/>
                                        <fo:table-column column-width="12cm"/>
                                        <fo:table-column column-width="3cm"/>
						<fo:table-body >                                                                                                                                                
                                                <!--Fila Inicial-->                                                                                                
						<fo:table-row>
                                                    <fo:table-cell text-align="left" vertical-align="middle"
                                                        number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                        <fo:block font-size="8pt" color="black" padding-before="0.5cm" font-weight='bold'>Fecha : <xsl:value-of select="$fecha"/></fo:block>    
                                                    
                                                    </fo:table-cell>
                                                
                                                    <fo:table-cell	                                                    								                                                                       
                                                        text-align="center" vertical-align="middle"
                                                        number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                        <fo:block font-size="14pt" color="black" padding-before="0.5cm" font-weight='bold'>TRANSPORTE SANCHEZ POLO</fo:block>
                                                    </fo:table-cell>
                                                    
                                                    <fo:table-cell	                                                    								                                                                       
                                                        text-align="right" vertical-align="middle"
                                                        number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                        <fo:block font-size="8pt" color="black" padding-before="0.5cm" font-weight='bold'>Hora : <xsl:value-of select="$hora"/></fo:block>
                                                    </fo:table-cell>						
                                            </fo:table-row>                                                                                                                                    
                                            <!--Fin Fila inicial-->
                                            
                                           <fo:table-row>                                                    
                                                    <fo:table-cell	                                                    								                                                                       
                                                        text-align="center" vertical-align="middle"
                                                        number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                                        <fo:block font-size="12pt" color="black" font-weight='bold'>I N G R E S O</fo:block>
                                                    </fo:table-cell>                                                                                                     
                                            </fo:table-row> 
                                                                                                                                                                                                                                                                                            
					</fo:table-body>
					</fo:table>
			    </fo:table-cell>		
			</fo:table-row>                                                
		</fo:table-body>
		</fo:table>
</fo:static-content>
                
                                <fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="raiz"/>
				</fo:flow>                                                                                
		</fo:page-sequence>
	</fo:root>
</xsl:template>


        <xsl:template match="raiz">	                                     
                  <xsl:apply-templates select="data"/>
	</xsl:template>

        <xsl:template match="data">	                
            <fo:block> 
                <xsl:apply-templates select="cabecera"/><!--Cabecera --> 
             </fo:block>                 
                
             <fo:block>
                <xsl:apply-templates select="detalle"/><!--Detalle-->  
             </fo:block>
                                                                        
             <fo:block> 
                    <xsl:choose>
                        <xsl:when test="position() != last()">
                            <xsl:attribute name="break-after">page</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="break-after">auto</xsl:attribute>
                        </xsl:otherwise> 
                    </xsl:choose> 
             </fo:block>     
                                                                                                                                           		                
	</xsl:template>
	
        <!-- Inicio Tabla 1-->
        
        <xsl:template match="cabecera">
                                
           <fo:table table-layout="fixed">
                    <fo:table-column column-width="19cm"/>                    
                    <fo:table-body>
                    
                    <fo:table-row>
                        <fo:table-cell> 
                            <fo:table table-layout="fixed">
                                <fo:table-column column-width="3.5cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="4cm"/>
                                
                                <fo:table-column column-width="3cm"/> <!--Separacion-->
                                
                                <fo:table-column column-width="3.5cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="4cm"/>
                                <fo:table-body>                                                                   
                                
                                    <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                        <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Numero ingreso</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"><xsl:value-of select="encabezado"/> MISCELANEO</fo:block>
                                         </fo:table-cell>
                                         
                                          <!-- Fin Numero Ingreso -->
                                          
                                          
                                          <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"> </fo:block>
                                         </fo:table-cell>                                          
                                          
                                          <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Fecha de Ingreso</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.175cm"><xsl:value-of select="fechareg"/></fo:block>
                                         </fo:table-cell>
                                          
                                  </fo:table-row>
                                    
                                     <!--Fin 1� FILA -->
                                     
                                  <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                        <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Fecha consignacion</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"><xsl:value-of select="fechaconsg"/></fo:block>
                                         </fo:table-cell>
                                         
                                          <!-- Fin Numero Ingreso -->
                                          
                                          
                                          <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"> </fo:block>
                                         </fo:table-cell>                                          
                                          
                                          <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Periodo</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.175cm"><xsl:value-of select="periodo"/></fo:block>
                                         </fo:table-cell>
                                          
                                  </fo:table-row>   
                                  
                                  <!-- Fin 2� FILA -->
                                  
                                  <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                        <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Valor ingreso</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"><xsl:value-of select="vlrconsignacion"/> </fo:block>
                                         </fo:table-cell>
                                         
                                          <!-- Fin Numero Ingreso -->
                                          
                                          
                                          <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"> </fo:block>
                                         </fo:table-cell>                                          
                                          
                                          <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Banco</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.175cm"><xsl:value-of select="banco"/></fo:block>
                                         </fo:table-cell>
                                          
                                  </fo:table-row>   
                                                                                                                                                                                                                          
                                   <!-- Fin 3� FILA -->
                                   
                                   
                                   <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                        <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Nro Consignacion</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"><xsl:value-of select="referencia"/></fo:block>
                                         </fo:table-cell>
                                         
                                          <!-- Fin Numero Ingreso -->
                                          
                                          
                                          <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"> </fo:block>
                                         </fo:table-cell>                                          
                                          
                                          <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Agencia</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.175cm"><xsl:value-of select="sucursal"/></fo:block>
                                         </fo:table-cell>
                                          
                                  </fo:table-row>   
                                                                                                                                                                                                                          
                                   <!-- Fin 4� FILA -->
                                   
                                    <fo:table-row>
                                        
                                        <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                        <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm"><xsl:value-of select="estado_c1"/></fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"><xsl:value-of select="estado_c2"/></fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"><xsl:value-of select="estado_c3"/></fo:block>
                                         </fo:table-cell>
                                         
                                          <!-- Fin Numero Ingreso -->
                                          
                                          
                                          <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"  padding-before="0.175cm"> </fo:block>
                                         </fo:table-cell>                                          
                                          
                                          <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" font-weight='bold' padding-before="0.175cm">Cuenta</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="center" vertical-align="middle" background-color="white">
                                            <fo:block font-size="8pt" color="black" padding-before="0.175cm">:</fo:block>
                                         </fo:table-cell>
                                         
                                         <fo:table-cell                                                         
                                            text-align="left" vertical-align="middle" background-color="white" >
                                            <fo:block font-size="8pt" color="black"   padding-before="0.175cm"><xsl:value-of select="cuenta"/></fo:block>
                                         </fo:table-cell>
                                          
                                  </fo:table-row>   
                                                                                                                                                                                                                          
                                   <!-- Fin 5� FILA -->
                                     
                                 </fo:table-body>
                            </fo:table>  
                            <!-- Fin de Tabla -->
                        </fo:table-cell>	
                                                                      
                    </fo:table-row>
 
		</fo:table-body>
          </fo:table>
       <!-- 1  FILA --> 
                                     

        </xsl:template>
        <!-- Fin tabla 1 -->
        
        
     
        <!-- Inicio Tabla 2 -->
       <xsl:template match="detalle">                    
        
        <fo:table table-layout="fixed" height="0.1cm" padding-before="0.5cm">                                       
                    <fo:table-column column-width="1.5cm"/> <!--Item -->
                    <fo:table-column column-width="2cm"/> <!--Cuenta -->
                    <fo:table-column column-width="1.5cm"/> <!--Tipo -->
                    <fo:table-column column-width="2cm"/> <!--Auxiliar -->
                    <fo:table-column column-width="4cm"/> <!--Descripcion -->
                    <fo:table-column column-width="2cm"/> <!--Tipo doc -->
                    <fo:table-column column-width="2cm"/> <!--Documento -->
                    <fo:table-column column-width="3cm"/> <!--Vlr -->                   		
                    <fo:table-body>
                    <fo:table-row>				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                                                                    
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">Item</fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                          
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">Cuenta</fo:block>
                                     </fo:table-cell>	
                                                                         
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">Tipo</fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">Auxiliar</fo:block>
                                     </fo:table-cell>
                                                                          
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">Descripcion</fo:block>
                                     </fo:table-cell>	
                                     
                                      <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">Tipo Doc.</fo:block>
                                     </fo:table-cell>	
                                     
                                      <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">Documento</fo:block>
                                     </fo:table-cell>	
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">Valor</fo:block>
                                     </fo:table-cell>				
                            </fo:table-row>
                    
                    </fo:table-body> 
            </fo:table>                        
        
        
             <fo:block>
                    <xsl:apply-templates select="items"/>
             </fo:block>  
             
             <fo:block>
                    <xsl:apply-templates select="totales"/>
             </fo:block>                        	
                         
               
        </xsl:template>  
        <!-- Fin tabla 2 -->
        
        <!-- Tabla 3 -->
        <xsl:template match="items">
        <fo:table table-layout="fixed" height="0.1cm" padding-before="0.1cm">                                       
                    <fo:table-column column-width="1.5cm"/> <!--Item -->
                    <fo:table-column column-width="2cm"/> <!--Cuenta -->
                    <fo:table-column column-width="1.5cm"/> <!--Tipo -->
                    <fo:table-column column-width="2cm"/> <!--Auxiliar -->
                    <fo:table-column column-width="4cm"/> <!--Descripcion -->
                    <fo:table-column column-width="2cm"/> <!--Tipo doc -->
                    <fo:table-column column-width="2cm"/> <!--Documento -->
                    <fo:table-column column-width="3cm"/> <!--Vlr -->                      		
                    <fo:table-body>
                    <fo:table-row>				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                                                                    
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm"><xsl:value-of select="cont"/></fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                          
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm"><xsl:value-of select="cuentai"/></fo:block>
                                     </fo:table-cell>	
                                                                          
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="center" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm"><xsl:value-of select="tipo"/></fo:block>
                                     </fo:table-cell>
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm"><xsl:value-of select="auxiliar"/></fo:block>
                                     </fo:table-cell>
                                                                          
                                     
                                      <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="7pt" color="black" padding-before="0.1cm"><xsl:value-of select="descripcion"/></fo:block>
                                     </fo:table-cell>	
                                     
                                      <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                        background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm"><xsl:value-of select="tipodoc"/></fo:block>
                                     </fo:table-cell>	
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm"><xsl:value-of select="documento"/></fo:block>
                                     </fo:table-cell>	
                                     
                                      <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                        background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm"><xsl:value-of select="valor"/></fo:block>
                                     </fo:table-cell>			
                            </fo:table-row>
                    
                    </fo:table-body> 
            </fo:table>                                     
            
           <fo:block space-before='0.25cm' height='0.25cm'></fo:block> 
        </xsl:template>
        <!-- Fin de Tabla 4 --> 
        
        
        <xsl:template match="totales">
        <fo:table table-layout="fixed" height="0.1cm" padding-before="0.1cm">                                       
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>                    
                    <fo:table-column column-width="4cm"/>
                    <fo:table-column column-width="2cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="3cm"/>                    		
                    <fo:table-body>
                    <fo:table-row>				
                                    <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                                                                    
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"></fo:block>
                                     </fo:table-cell>
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                          
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"></fo:block>
                                     </fo:table-cell>	
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                          
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"></fo:block>
                                     </fo:table-cell>	
                                                                          
                                     
                                     <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold">TOTALES</fo:block>
                                     </fo:table-cell>
                                                                          
                                     
                                      <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="left" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"></fo:block>
                                     </fo:table-cell>	
                                     
                                      <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white"                                                                                         
                                            number-columns-spanned="1"
                                            text-align="right" vertical-align="middle"
                                            background-color="white"><fo:block font-size="8pt" color="black" padding-before="0.1cm" font-weight="bold"><xsl:value-of select="total"/></fo:block>
                                     </fo:table-cell>				
                            </fo:table-row>
                    
                    </fo:table-body> 
            </fo:table>                                     
            
        </xsl:template>
       
</xsl:stylesheet>


