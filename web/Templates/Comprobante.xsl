<?xml version="1.0" encoding="UTF-8" ?>

<!--
    Document   : Comprobantes.xsl
    Created on : 3 de julio de 2006, 11:21 PM
    Author     : Mario
    Description:
        Formato de Impresion PDF para comprobantes de DIARIO
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

    <xsl:param name="page-count"/>
    <xsl:template match="/">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="simple"
                          margin-top="1cm"
                          margin-bottom="1cm"
                          margin-left="0.7cm"
                          margin-right="0.7cm">
                    <fo:region-body margin-top="3cm" margin-bottom="1cm"/>
                    <fo:region-before extent="4cm"/>
                    <fo:region-after extent="0.5cm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="simple"  initial-page-number="1">
                 <fo:static-content flow-name="xsl-region-before">
                     <fo:block text-align="right">
                          <fo:external-graphic  vertical-align="middle" src="url(images/logoFintra.jpg)" content-width="10cm" content-height="3cm"/>
                     </fo:block>
                 </fo:static-content>             
                 <fo:static-content flow-name="xsl-region-after">
                     <fo:block font-size="7pt" text-align="right">
                        <fo:inline font-weight="bold">Página <fo:page-number/> / <xsl:value-of select="$page-count"/></fo:inline>
                     </fo:block>
                 </fo:static-content>              
                 <fo:flow flow-name="xsl-region-body">
                        <xsl:apply-templates select="raiz"/>
                 </fo:flow>            
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <xsl:template match="raiz">
        <xsl:apply-templates select="data"/>
    </xsl:template>

    <xsl:template match="data">
        <fo:block>
            <xsl:apply-templates select="comprobante"/>
        </fo:block>
        <fo:block>
            <xsl:apply-templates select="newpage"/>
        </fo:block>                       
    </xsl:template>
    <xsl:template match="newpage">
        <fo:block break-before="page"/>
    </xsl:template>
    
    
    <xsl:template match="comprobante">
        <fo:table table-layout="fixed" padding-before="0.cm" font-size="8pt" border-style="solid" border-width="0.4pt">
            <fo:table-column column-width="18.8cm"/>                    
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell text-align="center" vertical-align="middle" padding="3px">
                        <fo:block font-size="10pt" >DATOS DEL COMPROBANTE</fo:block>
                    </fo:table-cell>
                </fo:table-row>    
                <fo:table-row>
                    <fo:table-cell text-align="center" vertical-align="middle"  >
                    <!-- contenido -->
                        <fo:table table-layout="fixed"  padding-before="0.cm" >
                            <fo:table-column column-width="4.7cm"/>
                            <fo:table-column column-width="4.7cm"/>
                            <fo:table-column column-width="4.7cm"/>
                            <fo:table-column column-width="4.7cm"/>
                            <fo:table-body>
                                <fo:table-row>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black" >
                                        <fo:block font-size="7pt">Distrito</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Tipo Documento</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Numero Documento</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Grupo Transacción</fo:block>
                                    </fo:table-cell>                                    
                                </fo:table-row>
                                <fo:table-row>                         
                                    <fo:table-cell vertical-align="middle" padding="2px" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="8pt" color="black" font-family="sans-serif"><xsl:value-of select="distrito"/></fo:block>
                                    </fo:table-cell>                                                    
                                    <fo:table-cell vertical-align="middle" padding="2px" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="8pt" color="black" font-family="sans-serif"><xsl:value-of select="tipodoc"/></fo:block>
                                    </fo:table-cell>                                    
                                    <fo:table-cell vertical-align="middle" padding="2px" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="8pt" color="black" font-family="sans-serif"><xsl:value-of select="numdoc"/></fo:block>
                                    </fo:table-cell>      
                                    <fo:table-cell vertical-align="middle" padding="2px" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="8pt" color="black" font-family="sans-serif"><xsl:value-of select="numtransaccion"/></fo:block>
                                    </fo:table-cell>                                                                                   
                                </fo:table-row>
                                
                                
                                
                                <fo:table-row>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" number-columns-spanned="4"  border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Detalle</fo:block>
                                    </fo:table-cell>                                   
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell vertical-align="middle" number-columns-spanned="4" padding="2px" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="8pt" color="black" font-family="sans-serif"><xsl:value-of select="detalle"/></fo:block>
                                    </fo:table-cell>                                      
                                                    
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Periodo</fo:block>
                                    </fo:table-cell>                                
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Fecha</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Tercero</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Fecha Aplicación</fo:block>
                                    </fo:table-cell>
                               </fo:table-row>
                               <fo:table-row>
                                    <fo:table-cell vertical-align="middle" padding="2px" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="8pt" color="black" font-family="sans-serif" ><xsl:value-of select="periodo"/></fo:block>
                                    </fo:table-cell>                                    
                                    <fo:table-cell vertical-align="middle" padding="2px" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="8pt" color="black" font-family="sans-serif"><xsl:value-of select="fecha"/></fo:block>
                                    </fo:table-cell>                                       
                                    <fo:table-cell vertical-align="middle" padding="2px" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="8pt" color="black" font-family="sans-serif"><xsl:value-of select="tercero"/></fo:block>
                                    </fo:table-cell>                                                    
                                    <fo:table-cell vertical-align="middle" padding="2px" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="8pt" color="black" font-family="sans-serif"><xsl:value-of select="application_date"/></fo:block>
                                    </fo:table-cell>                                                    
                                </fo:table-row>
                                
                                <fo:table-row>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Usuario Creación</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Fecha Creación</fo:block>
                                    </fo:table-cell>      
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" number-columns-spanned="2" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt"></fo:block>
                                    </fo:table-cell>                                          
                                </fo:table-row>
                                <fo:table-row>                                                             
                                    <fo:table-cell vertical-align="middle" padding="2px" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="8pt" color="black" font-family="sans-serif"><xsl:value-of select="creation_user"/></fo:block>
                                    </fo:table-cell>                                    
                                    <fo:table-cell vertical-align="middle" padding="2px" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="8pt" color="black" font-family="sans-serif"><xsl:value-of select="creation_date"/></fo:block>
                                    </fo:table-cell>       
                                    <fo:table-cell vertical-align="middle" number-columns-spanned="2" padding="2px" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block ></fo:block>
                                    </fo:table-cell>                                                                                    
                                </fo:table-row>                                                 
                            </fo:table-body>
                        </fo:table>
                        
                        <fo:table  table-layout="fixed"  padding-before="0.cm" height="17cm" >
                            <fo:table-column column-width="2.15cm"/>
                            <fo:table-column column-width="2.15cm"/>
                            <fo:table-column column-width="4.65cm"/>
                            <fo:table-column column-width="1.95cm"/>
                            <fo:table-column column-width="1.70cm"/>                                
                            <fo:table-column column-width="1.90cm"/>
                            <fo:table-column column-width="2.15cm"/>
                            <fo:table-column column-width="2.15cm"/>                            
                            <fo:table-body>
                                <fo:table-row>
                                    <fo:table-cell number-columns-spanned="8" text-align="center" vertical-align="middle" padding="3px">
                                        <fo:block font-size="10pt" >DETALLE DEL COMPROBANTE</fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Cuenta</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Auxiliar</fo:block>
                                    </fo:table-cell>                                    
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Detalle</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Tercero</fo:block>
                                    </fo:table-cell>                                                                                                                                                
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Tipo Doc.</fo:block>
                                    </fo:table-cell>                                                                                                                                                
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Documento</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Debito</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt">Credito</fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                
                                <xsl:apply-templates select="item"/>
                                
                                
                            </fo:table-body>
                        </fo:table>
                    
                    
                    <!-- contenido -->
                    </fo:table-cell>                
                </fo:table-row>
                
                
                <fo:table-row>
                    <fo:table-cell>
                    <!-- totales -->
                        <fo:table  table-layout="fixed"  padding-before="0.cm" >
                            <fo:table-column column-width="14.5cm"/>
                            <fo:table-column column-width="2.15cm"/>
                            <fo:table-column column-width="2.15cm"/>                          
                            <fo:table-body>
                                <fo:table-row>
                                    <fo:table-cell  text-align="right" padding="1px" vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt" font-weight="bold"> TOTALES </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="right" padding="1px" vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt" font-weight="bold"> <xsl:value-of select="tdebito"/></fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="right" padding="1px" vertical-align="middle" background-color="lightgray" border-width="0.2pt" border-style="solid" border-color="black">
                                        <fo:block font-size="7pt" font-weight="bold"> <xsl:value-of select="tcredito"/></fo:block>
                                    </fo:table-cell>                                                            
                                </fo:table-row>
                            </fo:table-body>
                        </fo:table>
                    <!-- fin totales -->    
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>                        
        </fo:table>
    </xsl:template>
    

    <xsl:template match="item">
        <fo:table-row>
            <fo:table-cell vertical-align="middle" text-align="left" padding="1px">
                <fo:block font-size="7pt"><xsl:value-of select="cuenta"/></fo:block>
            </fo:table-cell>
            <fo:table-cell vertical-align="middle" text-align="left" padding="1px">
                <fo:block font-size="7pt"><xsl:value-of select="auxiliar"/></fo:block>
            </fo:table-cell>
            <fo:table-cell vertical-align="middle" text-align="left" padding="1px">
                <fo:block font-size="7pt"><xsl:value-of select="detalle"/></fo:block>
            </fo:table-cell>
            <fo:table-cell vertical-align="middle" text-align="center" padding="1px">
                <fo:block font-size="7pt"><xsl:value-of select="tercero"/></fo:block>
            </fo:table-cell>
            <fo:table-cell vertical-align="middle" text-align="center" padding="1px">
                <fo:block font-size="7pt"><xsl:value-of select="tdocrel"/></fo:block>
            </fo:table-cell>
            <fo:table-cell vertical-align="middle" text-align="center" padding="1px">
                <fo:block font-size="7pt"><xsl:value-of select="docrel"/></fo:block>
            </fo:table-cell>
            <fo:table-cell vertical-align="middle" text-align="right" padding="1px">
                <fo:block font-size="7pt"><xsl:value-of select="debito"/></fo:block>
            </fo:table-cell>
            <fo:table-cell vertical-align="middle" text-align="right" padding="1px">
                <fo:block font-size="7pt"><xsl:value-of select="credito"/></fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
</xsl:stylesheet>
