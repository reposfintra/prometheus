<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple"
							  page-height="29.7cm"
							  page-width="21cm"
							  margin-top="0.25cm"
							  margin-bottom="0.5cm"
							  margin-left="1.5cm"
							  margin-right="1.5cm">
				  <fo:region-body margin-top="1cm" margin-bottom="1.5cm"/>
			  <fo:region-before extent="1.5cm"/>
			  <fo:region-after extent="1.5cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple">
				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="raiz"/>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	
	<xsl:template match="raiz">	
		<xsl:apply-templates select="data"/>
	</xsl:template>
	
	<xsl:template match="data">
               <fo:block>
                                <xsl:apply-templates select="tabla1"/>
               </fo:block>
		<fo:block height="5cm" width="18cm">
	  		<xsl:apply-templates select="valores"/>
		</fo:block>
                <fo:block>
	  		<xsl:apply-templates select="tabla3"/>
		</fo:block>
		<fo:block font-size="12pt"
                    font-family="sans-serif"
                    text-align="center">
                        <xsl:value-of select="trafico"/>
		</fo:block>

		<fo:block font-size="50pt"
                    font-family="sans-serif"
                    text-align="center" height="1cm" color="white">X</fo:block>
		<fo:block>
                
			<xsl:apply-templates select="tabla1"/>
            </fo:block>
		<fo:block height="5cm" width="18cm">
	  		<xsl:apply-templates select="valores"/>
		</fo:block>
	       <fo:block>
	  		<xsl:apply-templates select="tabla3"/>
		</fo:block>
		<fo:block font-size="12pt"
                    space-after.optimum="100pt"
                    font-family="sans-serif"
                    text-align="center">
                    <xsl:value-of select="trafico2"/>
		</fo:block>
	</xsl:template>

	<xsl:template match="tabla1">
      <fo:table table-layout="fixed">
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>
		<fo:table-column column-width="1cm"/>

   	<fo:table-body>
		<fo:table-row height="1cm">
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="white"
				border-left-color="white"
				border-right-color="white"	
		  		text-align="center" vertical-align="middle"
		 		number-columns-spanned="7" number-rows-spanned="1" background-color="white">
	      		<fo:block>
					<fo:external-graphic src="url(images/logo.bmp)" content-height="50%"/>
				</fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white"
				border-left-color="white"
				border-right-color="black"	
				text-align="center" vertical-align="middle"
		  		number-columns-spanned="1" number-rows-spanned="1" background-color="white">
	      		<fo:block font-size="10pt">  </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
		 		border-top-color="black"
				border-left-color="black"
				border-right-color="white"	
		  		text-align="left" vertical-align="middle"
		  		number-columns-spanned="2" number-rows-spanned="1" background-color="white">
				<fo:block font-size="5pt">        </fo:block>
	       		<fo:block font-size="5pt"> AGENCIA       </fo:block>
		 		<fo:block font-size="5pt"> BARRANQUILLA </fo:block>
		 		<fo:block font-size="5pt" > BOGOTA       </fo:block>
		 		<fo:block font-size="5pt"> BUENAVENTURA </fo:block>
		 		<fo:block font-size="5pt"> CALI         </fo:block>
		 		<fo:block font-size="5pt"> CARTAGENA    </fo:block>
	    	</fo:table-cell>
			<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="black"
				border-left-color="white"
				border-right-color="white"	
				text-align="left" 
				vertical-align="middle"
		  		number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="5pt">        </fo:block>
				<fo:block font-size="5pt"> TELEFONO</fo:block>
				<fo:block font-size="5pt"> 3511533</fo:block>
				<fo:block font-size="5pt"> 4123555 </fo:block>
				<fo:block font-size="5pt">  2433442</fo:block>
				<fo:block font-size="5pt">  6666288 </fo:block>
				<fo:block font-size="5pt"> 6625172</fo:block>
	    	</fo:table-cell>
			<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="black"
				border-left-color="white"
				border-right-color="white"	
				text-align="left" vertical-align="middle"
				number-columns-spanned="2" number-rows-spanned="1" background-color="white">
				<fo:block font-size="5pt">        </fo:block>
				<fo:block font-size="5pt"> AGENCIA       </fo:block>
				<fo:block font-size="5pt"> CUCUTA</fo:block>
				<fo:block font-size="5pt"> IPIALES</fo:block>
				<fo:block font-size="5pt"> MARACAIBO</fo:block>
				<fo:block font-size="5pt"> MEDELLIN</fo:block>
				<fo:block font-size="5pt"> QUITO</fo:block>
				<fo:block font-size="5pt"> GUAYAQUIL</fo:block>
	    	</fo:table-cell>
			<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="black"
				border-left-color="white"
				border-right-color="white"	
				text-align="left" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="5pt">        </fo:block>
				<fo:block font-size="5pt">TELEFONO </fo:block>
				<fo:block font-size="5pt"> 5782357</fo:block>
				<fo:block font-size="5pt"> 734807 </fo:block>
				<fo:block font-size="5pt"> 9248480</fo:block>
				<fo:block font-size="5pt"> 2625853 </fo:block>
				<fo:block font-size="5pt"> 503232</fo:block>
				<fo:block font-size="5pt"> 295333</fo:block>
	    	</fo:table-cell>
		 	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="black"
				border-left-color="white"
				border-right-color="white"	
				text-align="left" vertical-align="middle"
				number-columns-spanned="2" number-rows-spanned="1" background-color="white">
				<fo:block font-size="5pt">        </fo:block>
				<fo:block font-size="5pt"> AGENCIA       </fo:block>
				<fo:block font-size="5pt"> RIOACHA </fo:block>
				<fo:block font-size="5pt"> SANTAMARTA        </fo:block>
				<fo:block font-size="5pt"> VALENCIA </fo:block>
				<fo:block font-size="5pt"> TULCAN         </fo:block>
				<fo:block font-size="5pt"> MONTELIBANO    </fo:block>
	    	</fo:table-cell>
			<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="black"
				border-left-color="white"
				border-right-color="black"	
				text-align="left" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="5pt">        </fo:block>
				<fo:block font-size="5pt">TELEFONO</fo:block>
				<fo:block font-size="5pt"> 4273315</fo:block>
				<fo:block font-size="5pt"> 421422 </fo:block>
				<fo:block font-size="5pt"> 8718466</fo:block>
				<fo:block font-size="5pt"> 985854 </fo:block>
				<fo:block font-size="5pt"> 7723224</fo:block>
			</fo:table-cell>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-top-color="white"
				border-right-color="white" 
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="10pt">  </fo:block>
	    	</fo:table-cell>
        </fo:table-row>
			
		<fo:table-row>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold" color="#64646F"> OFICINA DE ORIGEN </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> DIA </fo:block>
		    </fo:table-cell>
		    <fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-left-color="white"  
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> MES </fo:block>
		    </fo:table-cell>
		    <fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-left-color="white" 
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> A�O </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-right-color="white" 
				border-top-color="white" 
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  </fo:block>
			</fo:table-cell>
	  		<fo:table-cell  
				border-width="0.5pt"
				border-left-color="white" 
				border-top-color="black" 
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="9" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-left-color="white" 
				border-right-color="white" 
				border-top-color="white" 
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
	      		<fo:block font-size="8pt">  </fo:block>
	    	</fo:table-cell>
	  	</fo:table-row>
	  
	  	<fo:table-row>
	  		<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-top-color="white" 
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" color="black">  <xsl:value-of select="oorigen"/>   </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-top-color="white" 
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  <xsl:value-of select="dia"/>  </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-top-color="white" 
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"  color="black">  <xsl:value-of select="mes"/>  </fo:block>
			</fo:table-cell>
			<fo:table-cell 
				border-width="0.5pt"
				border-style="solid"
				border-top-color="white" 
				border-color="black" 
				text-align="center" vertical-align="middle"
				number-columns-spanned="1" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  <xsl:value-of select="ano"/>  </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-right-color="white" 
				border-top-color="white" 
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="11" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" color="black">REMESA TERRESTRE DE CARGA No. <xsl:value-of select="cargan"/>     </fo:block>
	    	</fo:table-cell>
		</fo:table-row>
	
		<fo:table-row>
			<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> ORIGEN DE CARGA </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="9" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> REMITENTE </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="5" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> CONSIGNATARIO </fo:block>
	    	</fo:table-cell>
		</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"  color="black"> <xsl:value-of select="corigen"/>  </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="9" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"  color="black"> <xsl:value-of select="remitente"/>  </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="5" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"  color="black"> <xsl:value-of select="consignatario"/> </fo:block>
	    	</fo:table-cell>
		</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="8" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> DESTINATARIO </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				text-align="center" vertical-align="middle"
				number-columns-spanned="6" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> DIRECCION </fo:block>
	    	</fo:table-cell>
			<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				text-align="center" vertical-align="middle"
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> CIUDAD </fo:block>
	    	</fo:table-cell>
		</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="8" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"  color="black"><xsl:value-of select="destinatario"/>  </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="white"
				text-align="center" vertical-align="middle"
				number-columns-spanned="6" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"  color="black"><xsl:value-of select="direccion"/> </fo:block>
		    </fo:table-cell>
			<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="white"
				text-align="center" vertical-align="middle"
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"  color="black"><xsl:value-of select="ciudad"/>  </fo:block>
		    </fo:table-cell>
		</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> PLANILLA No. </fo:block>
		    </fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> PLACAS </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="10" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> CONDUCTOR </fo:block>
		    </fo:table-cell>
		</fo:table-row>
		
		<fo:table-row>
			<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"  color="black"> <xsl:value-of select="planilla"/>  </fo:block>
	 	   	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"  color="black"> <xsl:value-of select="placas"/>  </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="10" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"  color="black"> <xsl:value-of select="conductor"/>  </fo:block>
		    </fo:table-cell>
		</fo:table-row>
		
		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="18" number-rows-spanned="1" background-color="black">			
         	</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
</fo:table>
</xsl:template>  

    <xsl:template match="valores">
    <fo:table table-layout="fixed"  padding-before="0.cm" font-size="10pt" border-style="solid"  height="5cm">
        <fo:table-column column-width="3cm"/>
        <fo:table-column column-width="3cm"/>
        <fo:table-column column-width="3cm"/>
        <fo:table-column column-width="9cm"/>
        <fo:table-body>
            <fo:table-row >
                <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black" 
                        text-align="center" vertical-align="middle"
                        number-columns-spanned="1" number-rows-spanned="1" background-color="lightgray">
                    <fo:block font-size="8pt">MARCAS</fo:block>
                </fo:table-cell>
                <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="center" vertical-align="middle"
                        number-columns-spanned="1" number-rows-spanned="1" background-color="lightgray">
                    <fo:block font-size="8pt">BULTOS</fo:block>
                </fo:table-cell>
                        <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="center" vertical-align="middle"
                        number-columns-spanned="1" number-rows-spanned="1" background-color="lightgray">
                    <fo:block font-size="8pt" >KILOS</fo:block>
                </fo:table-cell>
                        <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black"
                        text-align="center" vertical-align="middle"
                        number-columns-spanned="1" number-rows-spanned="1" background-color="lightgray">
                    <fo:block font-size="8pt">IDENTIFICACION DE MERCANCIA</fo:block>
                </fo:table-cell>
            </fo:table-row>
		  
            <xsl:apply-templates select="item"/>
            <fo:table-row height="2cm">
                <fo:table-cell padding="2px"  number-columns-spanned="4">
                    <fo:block font-size="8pt">
                        <xsl:value-of select="leyenda2"/>
                    </fo:block>
                </fo:table-cell>
            </fo:table-row>
            
            <fo:table-row>
                <fo:table-cell padding="2px"  number-columns-spanned="4">
                    <fo:block font-size="8pt">
                    </fo:block>
                </fo:table-cell>
            </fo:table-row>
            
            <fo:table-row>
                <fo:table-cell padding="2px"  number-columns-spanned="4">
                    <fo:block font-size="8pt">
                        <xsl:value-of select="contenedores"/>
                    </fo:block>
                </fo:table-cell>
            </fo:table-row>
            
            <fo:table-row>
                <fo:table-cell padding="2px"  number-columns-spanned="4">
                    <fo:block font-size="8pt">
                        <xsl:value-of select="sellos"/>
                    </fo:block>
                </fo:table-cell>
            </fo:table-row>
            
            <fo:table-row>
                <fo:table-cell padding="2px"  number-columns-spanned="4">
                    <fo:block font-size="8pt">
                        <xsl:value-of select="textooc"/>
                    </fo:block>
                </fo:table-cell>
            </fo:table-row>
            
        </fo:table-body>
    </fo:table>
    </xsl:template>
	
    <xsl:template match="item">
        <fo:table-row padding-before="1cm" keep-together="always">
            <fo:table-cell padding="2px" font-size="9pt" number-columns-spanned="3">
                <fo:block><xsl:value-of select="marcas"/></fo:block>
            </fo:table-cell>
            <fo:table-cell padding="2px" font-size="9pt">
                <fo:block><xsl:value-of select="mercancia"/></fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="tabla3"> 
	<fo:table table-layout="fixed">
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="1cm"/>
            <fo:table-body>
		<fo:table-row>
                    <fo:table-cell
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black" 
                        text-align="center" vertical-align="middle" 
                        number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                        <fo:block font-size="8pt" font-style="bold"  color="#64646F"> VALOR FLETE </fo:block>
		    </fo:table-cell>
	 	    <fo:table-cell  
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black" 
                        text-align="center" vertical-align="middle" 
                        number-columns-spanned="9" number-rows-spanned="1" background-color="white">
                        <fo:block font-size="8pt" font-style="bold"  color="#64646F"> PAGADERO POR </fo:block>
                    </fo:table-cell>
                    <fo:table-cell  
                        border-width="0.5pt"
                        border-style="solid"
                        border-color="black" 
                        text-align="center" vertical-align="middle" 
                        number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                        <fo:block font-size="8pt" font-style="bold"  color="#64646F"> NIT. </fo:block>
	    	</fo:table-cell>
                <fo:table-cell  
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="black" 
                    text-align="center" vertical-align="middle" 
                    number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                    <fo:block font-size="8pt" font-style="bold"  color="#64646F"> CIUDAD </fo:block>
	    	</fo:table-cell>
            </fo:table-row>

            <fo:table-row>
	    	<fo:table-cell
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="black" 
                    border-top-color="white" 
                    text-align="center" vertical-align="middle" 
                    number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                    <fo:block font-size="6pt"  color="white"> <xsl:value-of select="flete"/>  </fo:block>
                </fo:table-cell>
	    	<fo:table-cell  
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="black" 
                    border-top-color="white" 
                    text-align="center" vertical-align="middle" 
                    number-columns-spanned="9" number-rows-spanned="1" background-color="white">
                    <fo:block font-size="6pt"  color="white"> <xsl:value-of select="pagadero"/> </fo:block>
                </fo:table-cell>
                <fo:table-cell  
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="black" 
                    border-top-color="white" 
                    text-align="center" vertical-align="middle" 
                    number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                    <fo:block font-size="6pt"  color="white"> <xsl:value-of select="nit"/>  </fo:block>
                </fo:table-cell>
                <fo:table-cell  
                    border-width="0.5pt"
                    border-style="solid"
                    border-color="black" 
                    border-top-color="white" 
                    text-align="center" vertical-align="middle" 
                    number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                    <fo:block font-size="6pt"  color="white"> <xsl:value-of select="ciudad"/>  </fo:block>
                </fo:table-cell>
            </fo:table-row>

            <fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> MANIFIESTO MEMORIAL </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> GUIA ADUANA </fo:block>
	    	</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"> </fo:block>
		    </fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="7" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> RECIBI CONFORME DESTINATARIO </fo:block>
		    </fo:table-cell>
	  	</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="6pt" color="white"> <xsl:value-of select="manifiesto"/>  </fo:block>
		    </fo:table-cell>
	 	    <fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="4" number-rows-spanned="1" background-color="white">
				<fo:block font-size="6pt"  color="white"> <xsl:value-of select="aduana"/>  </fo:block>
		    </fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
	      		<fo:block font-size="8pt">   </fo:block>
	   		</fo:table-cell>
			<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="7" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  </fo:block>
		    </fo:table-cell>
		</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="8" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt" font-style="bold"  color="#64646F"> JEFE DE DESPACHOS </fo:block>
	    	</fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  </fo:block>
	    	</fo:table-cell>
	   		<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="7" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  </fo:block>
		    </fo:table-cell>
		</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black"
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="8" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt"  color="black"> <xsl:value-of select="jefe"/> </fo:block>
		    </fo:table-cell>
	    	<fo:table-cell  
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="3" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  </fo:block>
	    	</fo:table-cell>
	   		<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="black" 
				border-top-color="white" 
				text-align="center" vertical-align="middle" 
				number-columns-spanned="7" number-rows-spanned="1" background-color="white">
				<fo:block font-size="8pt">  </fo:block>
		    </fo:table-cell>
	  	</fo:table-row>

		<fo:table-row>
	    	<fo:table-cell
				border-width="0.5pt"
				border-style="solid"
				border-color="white"
				border-top-color="black"	
				text-align="center" vertical-align="middle" 
				number-columns-spanned="18" number-rows-spanned="1" background-color="white">
				<fo:block font-size="7pt" font-family="times" color="black"> FIRMADO EL CUMPLIDO NO ACEPTAMOS RECLAMOS,FAVOR ENTREGAR CON FIRMA Y SELLO DEL CONDUCTOR </fo:block>
		    </fo:table-cell>
		</fo:table-row>
		</fo:table-body>
	</fo:table>
  </xsl:template>
  
  
  

 </xsl:stylesheet>






