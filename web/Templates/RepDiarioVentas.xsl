<?xml version="1.0" encoding="iso-8859-1"?>

<!--
    Document   : RepDiarioVentas.xls
    Created on : 21 de junio de 2006
    Author     : Ing. Andres Maturana De La Cruz
    Description:
        Purpose of the document follows.
-->



<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="/">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="simple"
                    page-height="27.9cm"
                    page-width="21.6cm"
                    margin-top="1cm"
                    margin-bottom="1cm"
                    margin-left="3cm"
                    margin-right="0.5cm">

                    <fo:region-body margin-top="3cm" margin-bottom="2.5cm"/>
                    <fo:region-before extent="3cm"/>
                    <fo:region-after extent="1.5cm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
  
            <fo:page-sequence master-reference="simple" initial-page-number="1">
                <fo:static-content flow-name="xsl-region-before">
                    <fo:block>
                        <fo:table table-layout="fixed" >
                            <fo:table-column column-width="3cm"/>
                            <fo:table-column column-width="3cm"/>
                            <fo:table-column column-width="3cm"/>
                            <fo:table-column column-width="2cm"/>
                            <fo:table-column column-width="4cm"/>
                            <fo:table-column column-width="2cm"/>

                            <fo:table-body>
                                <fo:table-row>
                                    <fo:table-cell
                                        border-width="0.5pt"
                                        border-style="solid"
                                        border-color="white"
                                        text-align="left" vertical-align="middle"
                                        background-color="white" number-rows-spanned="1" height="0.5cm"
                                        number-columns-spanned="6">
                                        <fo:block text-align="center" font-size="11pt" font-family="sans-serif"
                                            space-before="3pt" font-weight="bold" >
                                        TRANSPORTES SANCHEZ POLO S.A.</fo:block>
                                        <fo:block text-align="center" font-size="11pt" 
                                            font-family="sans-serif"  font-weight="bold">
                                        Nit : 890.103.161-1</fo:block>
                                        <fo:block text-align="center" font-size="11pt" 
                                            font-family="sans-serif" padding-after="15pt"
                                            font-weight="bold">
                                        INFORME SEMANAL DE VENTAS </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                
                                
                                <fo:table-row>
                                    <fo:table-cell
                                        border-width="0.5pt"
                                        border-style="solid"
                                        border-color="white"
                                        text-align="left" vertical-align="middle"
                                        background-color="white" number-rows-spanned="1" height="0.5cm">
                                        <fo:block text-align="center" font-size="11pt" font-family="sans-serif"
                                            space-before="3pt" font-weight="bold">
                                        FACTURA</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        border-width="0.5pt"
                                        border-style="solid"
                                        border-color="white"
                                        text-align="left" vertical-align="middle"
                                        background-color="white" number-rows-spanned="1" height="0.5cm">
                                        <fo:block text-align="center" font-size="11pt" font-family="sans-serif"
                                            space-before="3pt" font-weight="bold">
                                        FECHA DE</fo:block>
                                        <fo:block text-align="center" font-size="11pt" 
                                            font-family="sans-serif" font-weight="bold">
                                        FACTURA</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        border-width="0.5pt"
                                        border-style="solid"
                                        border-color="white"
                                        text-align="left" vertical-align="middle"
                                        background-color="white" number-rows-spanned="1" height="0.5cm">
                                        <fo:block text-align="center" font-size="11pt" font-family="sans-serif"
                                            space-before="3pt" font-weight="bold">
                                        CODIGO DE</fo:block>
                                        <fo:block text-align="center" font-size="11pt" 
                                            font-family="sans-serif" font-weight="bold">
                                        CLIENTE</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        border-width="0.5pt"
                                        border-style="solid"
                                        border-color="white"
                                        text-align="left" vertical-align="middle"
                                        background-color="white" number-rows-spanned="1" height="0.5cm">
                                        <fo:block text-align="center" font-size="11pt" font-family="sans-serif"
                                            space-before="3pt" font-weight="bold">
                                        NIT</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        border-width="0.5pt"
                                        border-style="solid"
                                        border-color="white"
                                        text-align="left" vertical-align="middle"
                                        background-color="white" number-rows-spanned="1" height="0.5cm">
                                        <fo:block text-align="center" font-size="11pt" font-family="sans-serif"
                                            space-before="3pt" font-weight="bold">
                                        VALOR</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        border-width="0.5pt"
                                        border-style="solid"
                                        border-color="white"
                                        text-align="left" vertical-align="middle"
                                        background-color="white" number-rows-spanned="1" height="0.5cm">
                                        <fo:block text-align="center" font-size="11pt" font-family="sans-serif"
                                            space-before="3pt" font-weight="bold">
                                        IVA</fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </fo:table-body>
                        </fo:table>
                    </fo:block>
                </fo:static-content>           
            
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block font-size="11pt" text-align="center" font-weight="bold">
                        <fo:page-number/>
                    </fo:block>
                </fo:static-content>
            
                <fo:flow flow-name="xsl-region-body">
                    <fo:block space-before="5pt">
                        <xsl:apply-templates select="raiz"/>
                    </fo:block>    
                </fo:flow>                
            </fo:page-sequence>
	</fo:root>
    </xsl:template>

    <xsl:template match="raiz">	
            <xsl:apply-templates select="fecha"/>
    </xsl:template>
    
    <xsl:template match="fecha">
        <fo:block>	
            <xsl:choose>
                <xsl:when test="position() != last()">
                    <xsl:attribute name="break-after">page</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="break-after">auto</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>


            <fo:table table-layout="fixed" >
                <fo:table-column column-width="3cm"/>
                <fo:table-column column-width="3cm"/>
                <fo:table-column column-width="3cm"/>
                <fo:table-column column-width="2cm"/>
                <fo:table-column column-width="4cm"/>
                <fo:table-column column-width="2cm"/>

                <fo:table-body>
                    <xsl:apply-templates select="factura"/>

                    <!-- TOTAL DE LA FECHA -->
                    <fo:table-row>
                        <fo:table-cell
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="white"
                            text-align="left" vertical-align="middle"
                            background-color="white" number-rows-spanned="1" height="0.5cm">
                            <fo:block text-align="center" font-size="10pt" font-family="sans-serif"
                            space-before="3pt">
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="white"
                            text-align="left" vertical-align="middle"
                            background-color="white" number-rows-spanned="1" height="0.5cm">
                            <fo:block text-align="center" font-size="10pt" font-family="sans-serif"
                                space-before="3pt">
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="white"
                            text-align="left" vertical-align="middle"
                            background-color="white" number-rows-spanned="1" height="0.5cm">
                            <fo:block text-align="center" font-size="10pt" font-family="sans-serif"
                                space-before="3pt">
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="white"
                            text-align="left" vertical-align="middle"
                            background-color="white" number-rows-spanned="1" height="0.5cm">
                            <fo:block text-align="center" font-size="10pt" font-family="sans-serif"
                                space-before="3pt">
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="white"
                            text-align="left" vertical-align="middle"
                            background-color="white" number-rows-spanned="1" height="0.5cm">
                            <fo:block text-align="right" font-size="10pt" font-family="sans-serif"
                                space-before="3pt" end-indent="3pt">
                            <xsl:value-of select="total"/></fo:block>
                        </fo:table-cell>
                        <fo:table-cell
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="white"
                            text-align="left" vertical-align="middle"
                            background-color="white" number-rows-spanned="1" height="0.5cm">
                            <fo:block text-align="right" font-size="10pt" font-family="sans-serif"
                                space-before="3pt">
                            .00</fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row>
                        <fo:table-cell
                            border-top-width="0.5pt"
                            border-top-style="solid"
                            border-top-color="black"
                            text-align="left" vertical-align="middle"
                            background-color="white" number-rows-spanned="1" height="0.5cm">
                            <fo:block text-align="center" font-size="10pt" font-family="sans-serif"
                                space-before="3pt">
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell
                            border-top-width="0.5pt"
                            border-top-style="solid"
                            border-top-color="black"
                            text-align="left" vertical-align="middle"
                            background-color="white" number-rows-spanned="1" height="0.5cm">
                            <fo:block text-align="center" font-size="10pt" font-family="sans-serif"
                                space-before="3pt">
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell
                            border-top-width="0.5pt"
                            border-top-style="solid"
                            border-top-color="black"
                            text-align="left" vertical-align="middle"
                            background-color="white" number-rows-spanned="1" height="0.5cm">
                            <fo:block text-align="center" font-size="10pt" font-family="sans-serif"
                                space-before="3pt">
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell
                            border-top-width="0.5pt"
                            border-top-style="solid"
                            border-top-color="black"
                            text-align="left" vertical-align="middle"
                            background-color="white" number-rows-spanned="1" height="0.5cm">
                            <fo:block text-align="center" font-size="10pt" font-family="sans-serif"
                                space-before="3pt">
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell
                            border-top-width="0.5pt"
                            border-top-style="solid"
                            border-top-color="black"
                            text-align="left" vertical-align="middle"
                            background-color="white" number-rows-spanned="1" height="0.5cm">
                            <fo:block text-align="right" font-size="10pt" font-family="sans-serif"
                                space-before="3pt" end-indent="3pt">
                            <xsl:value-of select="total"/></fo:block>
                        </fo:table-cell>
                        <fo:table-cell
                            border-top-width="0.5pt"
                            border-top-style="solid"
                            border-top-color="black"
                            text-align="left" vertical-align="middle"
                            background-color="white" number-rows-spanned="1" height="0.5cm">
                            <fo:block text-align="right" font-size="10pt" font-family="sans-serif"
                                space-before="3pt">
                            .00</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="factura">
        <fo:table-row>
            <fo:table-cell
                border-width="0.5pt"
                border-style="solid"
                border-color="white"
                text-align="left" vertical-align="middle"
                background-color="white" number-rows-spanned="1" height="0.5cm">
                <fo:block text-align="left" font-size="10pt" font-family="sans-serif"
                    space-before="3pt">
                <xsl:value-of select="documento"/></fo:block>
            </fo:table-cell>
            <fo:table-cell
                border-width="0.5pt"
                border-style="solid"
                border-color="white"
                text-align="left" vertical-align="middle"
                background-color="white" number-rows-spanned="1" height="0.5cm">
                <fo:block text-align="center" font-size="10pt" font-family="sans-serif"
                    space-before="3pt">
                <xsl:value-of select="fecha"/></fo:block>
            </fo:table-cell>
            <fo:table-cell
                border-width="0.5pt"
                border-style="solid"
                border-color="white"
                text-align="left" vertical-align="middle"
                background-color="white" number-rows-spanned="1" height="0.5cm">
                <fo:block text-align="center" font-size="10pt" font-family="sans-serif"
                    space-before="3pt">
                <xsl:value-of select="codcli"/></fo:block>
            </fo:table-cell>
            <fo:table-cell
                border-width="0.5pt"
                border-style="solid"
                border-color="white"
                text-align="left" vertical-align="middle"
                background-color="white" number-rows-spanned="1" height="0.5cm">
                <fo:block text-align="center" font-size="10pt" font-family="sans-serif"
                    space-before="3pt">
                <xsl:value-of select="nit"/></fo:block>
            </fo:table-cell>
            <fo:table-cell
                border-width="0.5pt"
                border-style="solid"
                border-color="white"
                text-align="left" vertical-align="middle"
                background-color="white" number-rows-spanned="1" height="0.5cm">
                <fo:block text-align="right" font-size="10pt" font-family="sans-serif"
                    space-before="3pt" end-indent="3pt">
                <xsl:value-of select="valor"/></fo:block>
            </fo:table-cell>
            <fo:table-cell
                border-width="0.5pt"
                border-style="solid"
                border-color="white"
                text-align="left" vertical-align="middle"
                background-color="white" number-rows-spanned="1" height="0.5cm">
                <fo:block text-align="center" font-size="10pt" font-family="sans-serif"
                    space-before="3pt">
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
        
</xsl:stylesheet>


