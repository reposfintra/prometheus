<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:param name="page-count"/>
    <xsl:template match="/">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="simple"
                          page-height="29.7cm"
                          page-width="21.5cm"
                          margin-top="0cm"
                          margin-bottom="0cm"
                          margin-left="0.7cm"
                          margin-right="0.7cm">
                    <fo:region-body margin-top="0cm" margin-bottom="0cm"/>

                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="simple"  initial-page-number="1">
                 <fo:static-content flow-name="xsl-region-after">
                 </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <xsl:apply-templates select="raiz"/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <xsl:template match="raiz">
        <xsl:apply-templates select="data"/>
    </xsl:template>

    <xsl:template match="data">

        <fo:block>
            <xsl:apply-templates select="tabla"/>
        </fo:block>

    </xsl:template>
    
    
    <xsl:template match="tabla">
         <fo:table table-layout="fixed" border-color="white" border-style="solid">
             <fo:table-column column-width="20cm"/>
            <fo:table-body>

                   <fo:table-row >

                        <fo:table-cell>

                            <fo:table table-layout="fixed"  height="0.35cm" border-color="white" border-style="solid">
                                <fo:table-column column-width="2.6cm"/>
                                <fo:table-column column-width="2.6cm"/>
                                <fo:table-column column-width="3.1cm"/>
                                <fo:table-column column-width="3.1cm"/>
                                <fo:table-column column-width="3.5cm"/>
                                <fo:table-column column-width="5cm"/>
                                <fo:table-body>
                                <fo:table-row height="1.8cm" keep-with-next ="always">
                                                   </fo:table-row >
                                    <!-- titulos -->
                                    <fo:table-row  keep-with-next ="always">

                                        <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                       border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                       end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1"
                                                       number-rows-spanned="1" background-color="lightgray">
                                            <fo:block font-weight="bold" font-size="9pt" padding-before="0.075cm">REMISION</fo:block>
                                        </fo:table-cell>

                                        <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                       border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                       end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1"
                                                       number-rows-spanned="1" background-color="lightgray">
                                            <fo:block font-weight="bold" font-size="9pt" padding-before="0.075cm">EGRESO</fo:block>
                                        </fo:table-cell>

                                        <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                       border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                       end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1"
                                                       number-rows-spanned="1" background-color="lightgray">
                                            <fo:block font-weight="bold" font-size="9pt" padding-before="0.075cm">PLANILLA</fo:block>
                                        </fo:table-cell>

                                        <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                       border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                       end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1"
                                                       number-rows-spanned="1" background-color="lightgray">

                                            <fo:block font-weight="bold" font-size="9pt" padding-before="0.075cm">REMESA</fo:block>
                                        </fo:table-cell>

                                        <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                       border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                       end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1"
                                                       number-rows-spanned="1" background-color="lightgray">
                                            <fo:block font-weight="bold" font-size="9pt" padding-before="0.075cm">FECHA</fo:block>
                                        </fo:table-cell>

                                        <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                       border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                       border-right-width="0.2pt" border-right-style="solid" border-right-color="black"
                                                       end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1"
                                                       number-rows-spanned="1" background-color="lightgray">
                                            <fo:block font-weight="bold" font-size="9pt" padding-before="0.075cm">PLACA</fo:block>
                                        </fo:table-cell>

                                  </fo:table-row>
                                  <!-- datos titulo -->
                                  <fo:table-row  keep-with-next ="always">

                                        <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                       border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                       border-bottom-width="0.2pt" border-bottom-style="solid" border-bottom-color="black"
                                                       end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1"
                                                       number-rows-spanned="1" background-color="white">
                                            <fo:block  font-size="10pt" padding-before="0.075cm"><xsl:value-of select="remision"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                       border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                       border-bottom-width="0.2pt" border-bottom-style="solid" border-bottom-color="black"
                                                       end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1"
                                                       number-rows-spanned="1" background-color="white">
                                            <fo:block  font-size="10pt" padding-before="0.075cm"><xsl:value-of select="egreso"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                       border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                       border-bottom-width="0.2pt" border-bottom-style="solid" border-bottom-color="black"
                                                       end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1"
                                                       number-rows-spanned="1" background-color="white">
                                            <fo:block  font-size="10pt" padding-before="0.075cm"><xsl:value-of select="planilla"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                       border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                       border-bottom-width="0.2pt" border-bottom-style="solid" border-bottom-color="black"
                                                       end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1"
                                                       number-rows-spanned="1" background-color="white">
                                            <fo:block  font-size="10pt" padding-before="0.075cm"><xsl:value-of select="remesa"/></fo:block>
                                        </fo:table-cell>

                                        <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                       border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                       border-bottom-width="0.2pt" border-bottom-style="solid" border-bottom-color="black"
                                                       end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1"
                                                       number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="10pt" padding-before="0.075cm"><xsl:value-of select="fecha"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                       border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                       border-right-width="0.2pt" border-right-style="solid" border-right-color="black"
                                                       border-bottom-width="0.2pt" border-bottom-style="solid" border-bottom-color="black"
                                                       end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1"
                                                       number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="10pt" padding-before="0.075cm"><xsl:value-of select="placa"/></fo:block>
                                        </fo:table-cell>

                                  </fo:table-row>

                                  <fo:table-row height="0.05cm" keep-with-next ="always">
                                                 <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                                end-indent="3pt" text-align="right" vertical-align="middle"
                                                                number-columns-spanned="6" number-rows-spanned="1" background-color="white">

                                                 </fo:table-cell>
                                  </fo:table-row>

                                   <!-- Contrato y Propietario -->
                                  <fo:table-row>

                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.025cm">CONTRATO:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="left" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" padding-before="0.025cm"><xsl:value-of select="contrato"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.025cm">PROPIETARIO:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="left" vertical-align="middle" number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" padding-before="0.025cm"><xsl:value-of select="propietario"/></fo:block>
                                        </fo:table-cell>

                                  </fo:table-row>

                                  <!-- ton cumplido y efectivo -->
                                  <fo:table-row  keep-with-next ="always">

                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.025cm">TON. CUMPLIDO:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="left" vertical-align="middle" number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" padding-before="0.025cm"><xsl:value-of select="cumplido"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.025cm">EFECTIVO:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" padding-before="0.025cm"><xsl:value-of select="efectivo"/></fo:block>
                                        </fo:table-cell>

                                  </fo:table-row>

                                  <!-- acpm -->
                                  
                                  <fo:table-row  keep-with-next ="always">

                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.025cm">ACPM:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="left" vertical-align="middle" number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" padding-before="0.025cm"><xsl:value-of select="acpm"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" padding-before="0.025cm"><xsl:value-of select="vlracpm"/></fo:block>
                                        </fo:table-cell>

                                  </fo:table-row>
                                  
                                  <!-- peajes -->
                                  
                                  <fo:table-row  keep-with-next ="always">

                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.025cm">PEAJES:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="left" vertical-align="middle" number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" padding-before="0.025cm"><xsl:value-of select="peajes"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" padding-before="0.025cm"><xsl:value-of select="vlrpeajes"/></fo:block>
                                        </fo:table-cell>

                                  </fo:table-row>

                                  <!-- carga y total -->
                                   <fo:table-row  keep-with-next ="always">

                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.025cm">CARGA:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="left" vertical-align="middle" number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" padding-before="0.025cm"><xsl:value-of select="carga"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:table table-layout="fixed"  height="0.4cm" border-color="white" border-style="solid">
                                                <fo:table-column column-width="3.5cm"/>
                                                <fo:table-column column-width="1.5cm"/>
                                                <fo:table-body>
                                                    <fo:table-row>
                                                        <fo:table-cell end-indent="3pt" text-align="left" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                            <fo:block font-weight="bold" font-size="10pt" padding-before="0.025cm">TOTAL:</fo:block>
                                                        </fo:table-cell>
                                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                            <fo:block font-weight="bold" font-size="10pt" padding-before="0.025cm"><xsl:value-of select="total"/></fo:block>
                                                        </fo:table-cell>
                                                    </fo:table-row>
                                                </fo:table-body>
                                           </fo:table>
                                        </fo:table-cell>

                                  </fo:table-row>

                                  <!-- ruta -->
                                   <fo:table-row  keep-with-next ="always">

                                        <fo:table-cell end-indent="3pt" text-align="right" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-weight="bold" font-size="7pt" padding-before="0.025cm">RUTA:</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell end-indent="3pt" text-align="left" vertical-align="middle" number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="9pt" padding-before="0.025cm"><xsl:value-of select="ruta"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell border-top-width="0.2pt" border-top-style="solid" border-top-color="black"
                                                       border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                       border-right-width="0.2pt" border-right-style="solid" border-right-color="black"
                                                       end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1"
                                                       number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="6pt" padding-before="0.025cm">FIRMA Y SELLO DEL BENEFICIARIO</fo:block>

                                        </fo:table-cell>

                                  </fo:table-row>

                                  <fo:table-row  height="1.2cm" >
                                       <fo:table-cell number-columns-spanned="5">
                                            <fo:table table-layout="fixed"  height="0.4cm" border-color="white" border-style="solid">
                                                <fo:table-column column-width="4.2cm"/>
                                                <fo:table-column column-width="4.2cm"/>
                                                <fo:table-column column-width="3.2cm"/>
                                                <fo:table-column column-width="3.2cm"/>
                                                  <fo:table-body>
                                                    <fo:table-row>
                                                        <fo:table-cell end-indent="3pt" text-align="left" vertical-align="middle" number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                                            <fo:block font-size="6pt" padding-before="0.025cm"><xsl:value-of select="leyenda"/></fo:block>
                                                        </fo:table-cell>
                                                     </fo:table-row>
                                                                 <fo:table-row>

                                                                      <fo:table-cell end-indent="3pt" text-align="left" vertical-align="middle" number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                                                           <fo:block  font-size="7pt" padding-before="0.025cm">PREPARADO:<xsl:value-of select="preparado"/></fo:block>
                                                                      </fo:table-cell>

                                                                      <fo:table-cell end-indent="3pt" text-align="center" vertical-align="middle" number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                                                           <fo:block  font-size="7pt" padding-before="0.025cm"><xsl:value-of select="copia"/></fo:block>
                                                                      </fo:table-cell>

                                                                   </fo:table-row>

                                                </fo:table-body>
                                            </fo:table>
                                         </fo:table-cell>
                                        <fo:table-cell border-left-width="0.2pt" border-left-style="solid" border-left-color="black"
                                                                       border-right-width="0.2pt" border-right-style="solid" border-right-color="black"
                                                                       border-bottom-width="0.2pt" border-bottom-style="solid" border-bottom-color="black"
                                                                       end-indent="3pt" text-align="left" vertical-align="middle" number-columns-spanned="1"
                                                                       number-rows-spanned="1" background-color="white">
                                                                       <fo:block font-size="7pt" padding-before="0.025cm"><xsl:value-of select="firma"/></fo:block>
                                                                       <fo:block font-size="7pt" padding-before="0.025cm"><xsl:value-of select="telefono"/></fo:block>
                                      </fo:table-cell>
                                   </fo:table-row>
                                   



                               </fo:table-body>
                          </fo:table>

               </fo:table-cell>
             </fo:table-row>



             <fo:table-row height="0.9cm">
             </fo:table-row>



           </fo:table-body>
         </fo:table>
    </xsl:template >

</xsl:stylesheet>
