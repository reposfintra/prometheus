<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  		<fo:layout-master-set>
			<fo:simple-page-master master-name="simple"
				 page-height="29.7cm"
							  page-width="21cm"
							  margin-top="0.25cm"
							  margin-bottom="0.5cm"
							  margin-left="1.5cm"
							  margin-right="1.5cm">
			
			  <fo:region-body margin-top="0.5cm" margin-bottom="0.5cm"/>
			  <fo:region-before extent="1cm"/>
			  <fo:region-after extent="1.5cm"/>
			</fo:simple-page-master>
  		</fo:layout-master-set>
  
		<fo:page-sequence master-reference="simple">
				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="raiz"/>
				</fo:flow>
		</fo:page-sequence>
	</fo:root>
</xsl:template>

        <xsl:template match="raiz">	
		<xsl:apply-templates select="data"/>
	</xsl:template>

	<xsl:template match="data">
       <fo:block>
			<xsl:apply-templates select="cabecera"/>
       </fo:block>
	   <fo:block>
			<xsl:apply-templates select="datos_viajes"/>
       </fo:block>
	<fo:block>
			<xsl:apply-templates select="datos_conductor"/>
       </fo:block>
       <fo:block>
			<xsl:apply-templates select="equipos_comunicacion"/>
       </fo:block>
        <fo:block>
			<xsl:apply-templates select="datos_familiar"/>
       </fo:block>
       <fo:block>
			<xsl:apply-templates select="propietario"/>
       </fo:block>
       <fo:block>
			<xsl:apply-templates select="alimentacion"/>
       </fo:block>
       <fo:block>
			<xsl:apply-templates select="pernotacion"/>
       </fo:block>
       <fo:block>
			<xsl:apply-templates select="parqueadero"/>
       </fo:block>
       <fo:block>
			<xsl:apply-templates select="tanqueo"/>
       </fo:block>
       <fo:block>
			<xsl:apply-templates select="puestoControl"/>
       </fo:block>
       <fo:block>
			<xsl:apply-templates select="comentario"/>
       </fo:block>
       
	</xsl:template>
	
		<xsl:template match="cabecera"> 
		<fo:table table-layout="fixed" height="3cm">
		<fo:table-column column-width="3.19cm"/>
		<fo:table-column column-width="14.25cm"/>		
		<fo:table-body>
			<fo:table-row>
				<fo:table-cell
					border-width="1pt"
					border-style="solid"
					border-color="black"
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">
					<fo:external-graphic src="url(images/logo.bmp)"/>
				</fo:table-cell>
                                <fo:table-cell
					border-width="1pt"
					border-style="solid"
					border-color="black"
					number-columns-spanned="1"
					text-align="center" vertical-align="middle"
					background-color="white">
					<fo:table table-layout="fixed">
                                        <fo:table-column column-width="14.25cm"/>
						<fo:table-body border-color="black" border-style="solid" border-bottom-color="white">

						<fo:table-row>
					                    <fo:table-cell
									border-width="0.5pt"
									border-style="none"
									border-color="black"                                                                        
									text-align="center" vertical-align="middle"
									number-columns-spanned="1" number-rows-spanned="1" background-color="white">
									<fo:block font-size="12pt" color="black" padding-before="0.5cm">TRANSPORTE SANCHEZ POLO</fo:block>
								</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
					                    <fo:table-cell
									border-width="0.5pt"
									border-style="none"
									border-color="black"
									text-align="center" vertical-align="middle"
									number-columns-spanned="1" number-rows-spanned="1" background-color="white">
									<fo:block font-size="12pt" color="black">INFORMACION PLAN DE VIAJE</fo:block>
								</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
					</fo:table>
			    </fo:table-cell>


				
			</fo:table-row>
		</fo:table-body>
		</fo:table>
		</xsl:template >
	 
	<xsl:template match="datos_viajes"> 
	<fo:table table-layout="fixed"  height="0.8cm" border-color="black" border-style="solid">
		<fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>


   		<fo:table-body>
	  		<fo:table-row background-color="lightgray" height="0.5cm" border-style="solid" border-color="black" >
	    		<fo:table-cell										
					text-align="left" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" >
					<fo:block font-size="7pt" padding-before="0.1cm" margin-left='0.125cm'>DATOS DEL VIAJES</fo:block>
	    		</fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                        <fo:table-cell									
					text-align="left" vertical-align="middle"
					number-columns-spanned="8" number-rows-spanned="1" background-color="white">
					<fo:block>
                                        
                                        <fo:table table-layout="fixed"  height="0.8cm" border-width=".1mm"  >
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-body>
                                        
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell 
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold"  color="#64646F" margin-left='0.125cm' padding-before="0.075cm">PLANILLA</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="planilla"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold"  color="#64646F" padding-before="0.075cm"> DISTRITO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="distrito"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold"  color="#64646F" padding-before="0.075cm"> PLACA</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="1" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="placa"/></fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        
                                        
                                        <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold"  color="#64646F" margin-left='0.125cm' padding-before="0.075cm"> FECHA DE DESPACHO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="fecha_despacho"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold"  color="#64646F" padding-before="0.075cm"> ORIGEN</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="origen"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold"  color="#64646F" padding-before="0.075cm"> DESTINO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="destino"/></fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                        <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold"  color="#64646F" margin-left='0.125cm' padding-before="0.075cm">DESTINATARIO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="9" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm" > <xsl:value-of select="destinatario"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F">TIPO DE CARGA</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="tipo_carga"/></fo:block>
                                        </fo:table-cell>
                                       
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                          <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" margin-left='0.125cm' color="#64646F">PRODUCTOS</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="9" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm" > <xsl:value-of select="productos"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm"  color="#64646F">PLACA UN. CARGA</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="placa_carga"/></fo:block>
                                        </fo:table-cell>
                                       
                                        
                                        </fo:table-row>
                                        
                                        
                                        <!--****************FIN FILA *****************-->
                                        
                                          <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" margin-left='0.125cm' color="#64646F">CONTENEDORES</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="9" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm" > <xsl:value-of select="contenedores"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm"  color="#64646F">TRAILER</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm" > <xsl:value-of select="trailer"/></fo:block>
                                        </fo:table-cell>
                                       
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                         <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" margin-left='0.125cm' color="#64646F">RUTA</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="9" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm" > <xsl:value-of select="ruta"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F">FECHA PLAN VIAJE</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="fecha_planviaje"/></fo:block>
                                        </fo:table-cell>
                                       
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                         <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm"  margin-left='0.125cm' color="#64646F">PROX.P.C.</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="9" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="proxPC"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F">ZONA P.C.</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="zonaPC"/></fo:block>
                                        </fo:table-cell>
                                       
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                        
                                           <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" margin-left='0.125cm' color="#64646F">SALIDA</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="9" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="salida"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F">ESCOLTA</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="escolta"/></fo:block>
                                        </fo:table-cell>
                                       
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                        
                                        <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm"  margin-left='0.125cm' color="#64646F"> USUARIO CREACION</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="usuario_creacion"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F"> FECHA CREACION</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="fecha_creacion"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F"> COMPROMISO RETORNO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="compromiso_retono"/></fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        </fo:table-body>
                                        
                                        </fo:table>                                                                                                                       
                                        </fo:block>
                            </fo:table-cell>    
                      
     
			</fo:table-row>
                        
                        
                        
		</fo:table-body>
    </fo:table>
	</xsl:template >
        
        <!--****************DATOS CONDUCTOR*****************-->
        
        <xsl:template match="datos_conductor">
	<fo:table table-layout="fixed"  height="0.8cm" border-color="black" border-style="solid">
		<fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>


   		<fo:table-body>
	  		<fo:table-row background-color="lightgray" height="0.5cm">
	    		<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" >
					<fo:block font-size="7pt" padding-before="0.1cm" margin-left='0.125cm'>DATOS DEL CONDUCTOR</fo:block>
	    		</fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                        <fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					border-top-color="white"
					text-align="left" vertical-align="middle"
					number-columns-spanned="8" number-rows-spanned="1" background-color="white">
					<fo:block>
                                        
                                        <fo:table table-layout="fixed"  height="0.8cm" border-color="white" border-width=".1mm"  >
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-body>
                                                                                
                                                                                                                        
                                        
                                        <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" margin-left='0.125cm' color="#64646F">CEDULA</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="6" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="ced_conductor"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F">NOMBRE</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="6" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="nom_conductor"/></fo:block>
                                        </fo:table-cell>
                                       
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                          <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm"  margin-left='0.125cm' color="#64646F">CIUDAD</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="6" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="ciudad_conductor"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F">DIRECCION</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="8" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="dir_conductor"/></fo:block>
                                        </fo:table-cell>
                                       
                                        
                                        </fo:table-row>
                                        
                                        
                                        <!--****************FIN FILA *****************-->
                                        
                                          <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" margin-left='0.125cm' color="#64646F">TELEFONO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="9" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="tel_conductor"/></fo:block>
                                        </fo:table-cell>
                                                                               
                                       
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                                                                                                         
                                        </fo:table-body>
                                        
                                        </fo:table>                                                                                                                       
                                        </fo:block>
                            </fo:table-cell>    
                      
     
			</fo:table-row>
                        
                        
                        
		</fo:table-body>
    </fo:table>
	</xsl:template >
        
        <!--****************EQUIPOS DE COMUNICACION*****************-->
        
        <xsl:template match="equipos_comunicacion">
	<fo:table table-layout="fixed"  height="0.8cm" border-color="black" border-style="solid">
		<fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>


   		<fo:table-body>
	  		<fo:table-row background-color="lightgray" height="0.5cm">
	    		<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" >
					<fo:block font-size="7pt" padding-before="0.1cm" margin-left='0.125cm'>EQUIPOS DE COMUNICACION</fo:block>
	    		</fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                        <fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					border-top-color="white"
					text-align="left" vertical-align="middle"
					number-columns-spanned="8" number-rows-spanned="1" background-color="white">
					<fo:block>
                                        
                                        <fo:table table-layout="fixed"  height="0.8cm" border-color="white" border-width=".1mm"  >
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-body>
                                                                                
                                                                                                                        
                                        
                                        <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm"  margin-left='0.125cm' color="#64646F"> RADIO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="5" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="radio"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F"> AVANTEL</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="avantel"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F"> CAZADOR</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="cazador"/></fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                        
                                        <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm"  margin-left='0.125cm' color="#64646F"> CELULAR</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="5" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="celular_comunicacion"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F"> TELEFONO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="telefono_comunicacion"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F"> MOVIL</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="movil"/></fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                           <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm"  margin-left='0.125cm' color="#64646F">OTRO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="9" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="otro_comunicacion"/></fo:block>
                                        </fo:table-cell>
                                                                               
                                       
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                                                                                                         
                                        </fo:table-body>
                                        
                                        </fo:table>                                                                                                                       
                                        </fo:block>
                            </fo:table-cell>    
                      
     
			</fo:table-row>
                        
                        
                        
		</fo:table-body>
    </fo:table>
	</xsl:template >
        
        
          <!--****************DATOS DE UN FAMILIAR*****************-->
        
        <xsl:template match="datos_familiar">
	<fo:table table-layout="fixed"  height="0.8cm" border-color="black" border-style="solid">
		<fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>


   		<fo:table-body>
	  		<fo:table-row background-color="lightgray" height="0.5cm">
	    		<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" >
					<fo:block font-size="7pt" padding-before="0.1cm" margin-left='0.125cm'>DATOS DE UN FAMILIAR</fo:block>
	    		</fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                        <fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					border-top-color="white"
					text-align="left" vertical-align="middle"
					number-columns-spanned="8" number-rows-spanned="1" background-color="white">
					<fo:block>
                                        
                                        <fo:table table-layout="fixed"  height="0.8cm" border-color="white" border-width=".1mm"  >
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-body>
                                                                                
                                                                                                                        
                                        
                                        <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" margin-left='0.125cm' color="#64646F"> NOMBRE</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="5" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="nombre_familiar"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="3" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F"> TELEFONO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm" > <xsl:value-of select="tel_familiar"/></fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                        </fo:table-body>
                                        
                                        </fo:table>                                                                                                                       
                                        </fo:block>
                            </fo:table-cell>    
                      
     
			</fo:table-row>
                        
                        
                        
		</fo:table-body>
    </fo:table>
	</xsl:template >
        
        
           <!--****************DATOS PROPIETARIO*****************-->
        
        <xsl:template match="propietario">
	<fo:table table-layout="fixed"  height="0.8cm" border-color="black" border-style="solid">
		<fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>


   		<fo:table-body>
	  		<fo:table-row background-color="lightgray" height="0.5cm">
	    		<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" >
					<fo:block font-size="7pt" padding-before="0.1cm" margin-left='0.125cm'>DATOS DEL PROPIETARIO</fo:block>
	    		</fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                        <fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					border-top-color="white"
					text-align="left" vertical-align="middle"
					number-columns-spanned="8" number-rows-spanned="1" background-color="white">
					<fo:block>
                                        
                                        <fo:table table-layout="fixed"  height="0.8cm" border-color="white" border-width=".1mm"  >
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-body>
                                                                                
                                                                                                                        
                                        
                                        <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm"  margin-left='0.125cm' color="#64646F">CEDULA</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="6" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="ced_propietario"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F">NOMBRE</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="6" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm" > <xsl:value-of select="nom_propietario"/></fo:block>
                                        </fo:table-cell>
                                       
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                          <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" margin-left='0.125cm' color="#64646F">CIUDAD</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="6" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="ciudad_propietario"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F">DIRECCION</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="8" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="dir_propietario"/></fo:block>
                                        </fo:table-cell>
                                       
                                        
                                        </fo:table-row>
                                        
                                        
                                        <!--****************FIN FILA *****************-->
                                        
                                          <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm"  margin-left='0.125cm' color="#64646F">TELEFONO</fo:block>
                                        </fo:table-cell>
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="9" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="tel_propietario"/></fo:block>
                                        </fo:table-cell>
                                                                               
                                       
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                                                                                                         
                                        </fo:table-body>
                                        
                                        </fo:table>                                                                                                                       
                                        </fo:block>
                            </fo:table-cell>    
                      
     
			</fo:table-row>
                        
                        
                        
		</fo:table-body>
    </fo:table>
	</xsl:template >
        
        
         <!--****************ALIMENTACION*****************-->
        
        <xsl:template match="alimentacion">
	<fo:table table-layout="fixed"  height="0.8cm" border-color="black" border-style="solid">
		<fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>


   		<fo:table-body>
	  		<fo:table-row background-color="lightgray" height="0.5cm">
	    		<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="10" number-rows-spanned="1" >
					<fo:block font-size="7pt" padding-before="0.1cm" margin-left='0.125cm'>ALIMENTACION</fo:block>
	    		</fo:table-cell>
                        
                        <fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="7" number-rows-spanned="1" >
					<fo:block font-size="7pt" padding-before="0.1cm">PERNOTACION</fo:block>
	    		</fo:table-cell>
                        
                        </fo:table-row>
                        <fo:table-row>
                        <fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					border-top-color="white"
					text-align="left" vertical-align="middle"
					number-columns-spanned="8" number-rows-spanned="1" background-color="white">
					<fo:block>
                                        
                                        <fo:table table-layout="fixed"  height="0.8cm" border-color="white" border-width=".1mm"  >
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-body>
                                                                                
                                                                                                                        
                                        
                                        <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="11" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" margin-left='0.125cm' color="#64646F"> RESTAURANTES DEL VIAJE</fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="7" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F"> PERNOTADEROS DEL VIAJE</fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                         <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="11" number-rows-spanned="1" background-color="white"
                                            linefeed-treatment="preserve"
                                            white-space-collapse="false"
                                            white-space-treatment="preserve"
                                            wrap-option="no-wrap"
                                            >
                                            <fo:block font-size="7pt" padding-before="0.075cm" margin-left='0.125cm'> <xsl:value-of select="restaurantes_viaje"/></fo:block>
                                        </fo:table-cell>
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="8" number-rows-spanned="1" background-color="white"
                                            linefeed-treatment="preserve"
                                            white-space-collapse="false"
                                            white-space-treatment="preserve"
                                            wrap-option="no-wrap"
                                            >
                                            <fo:block font-size="7pt" padding-before="0.075cm"> <xsl:value-of select="pernotaderos_viaje"/></fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                        </fo:table-body>
                                        
                                        </fo:table>                                                                                                                       
                                        </fo:block>
                            </fo:table-cell>    
                      
     
			</fo:table-row>
                        
                        
                        
		</fo:table-body>
    </fo:table>
	</xsl:template >
                        
         <!--****************PARQUEADEROS*****************-->
        
        <xsl:template match="parqueadero">
	<fo:table table-layout="fixed"  height="0.8cm" border-color="black" border-style="solid">
		<fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>


   		<fo:table-body>
	  		<fo:table-row background-color="lightgray" height="0.5cm">
	    		<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="10" number-rows-spanned="1" >
					<fo:block font-size="7pt" padding-before="0.1cm" margin-left='0.125cm'>PARQUEADEROS</fo:block>
	    		</fo:table-cell>
                        <fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="7" number-rows-spanned="1" >
					<fo:block font-size="7pt" padding-before="0.1cm">TANQUEOS</fo:block>
	    		</fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                        <fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					border-top-color="white"
					text-align="left" vertical-align="middle"
					number-columns-spanned="8" number-rows-spanned="1" background-color="white">
					<fo:block>
                                        
                                        <fo:table table-layout="fixed"  height="0.8cm" border-color="white" border-width=".1mm"  >
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-body>
                                                                                
                                                                                                                        
                                        
                                        <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="11" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" margin-left='0.125cm' color="#64646F"> PARQUEADEROS DEL VIAJE</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="7" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="8pt" font-style="bold" padding-before="0.075cm" color="#64646F"> TANQUEOS DEL VIAJE</fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                         <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="11" number-rows-spanned="1" background-color="white"
                                            linefeed-treatment="preserve"
                                            white-space-collapse="false"
                                            white-space-treatment="preserve"
                                            wrap-option="no-wrap">
                                            <fo:block font-size="7pt" padding-before="0.075cm" margin-left='0.125cm' > <xsl:value-of select="parqueaderos_viaje"/></fo:block>
                                        </fo:table-cell>
                                         <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="7" number-rows-spanned="1" background-color="white"
                                            linefeed-treatment="preserve"
                                            white-space-collapse="false"
                                            white-space-treatment="preserve"
                                            wrap-option="no-wrap"
                                            >
                                            <fo:block font-size="7pt" padding-before="0.075cm" margin-left='0.125cm'> <xsl:value-of select="tanqueos_viaje"/></fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                        </fo:table-body>
                                        
                                        </fo:table>                                                                                                                       
                                        </fo:block>
                            </fo:table-cell>    
                      
     
			</fo:table-row>
                        
                        
                        
		</fo:table-body>
    </fo:table>
	</xsl:template >
        
         <!--****************PUESTOS DE CONTROL*****************-->
        
        <xsl:template match="puestoControl">
	<fo:table table-layout="fixed"  height="0.8cm" border-color="black" border-style="solid">
		<fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>


   		<fo:table-body>
	  		<fo:table-row background-color="lightgray" height="0.5cm">
	    		<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" >
					<fo:block font-size="7pt" padding-before="0.1cm" margin-left='0.125cm'>PUESTOS DE CONTROL</fo:block>
	    		</fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                        <fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					border-top-color="white"
					text-align="left" vertical-align="middle"
					number-columns-spanned="8" number-rows-spanned="1" background-color="white">
					<fo:block>
                                        
                                        <fo:table table-layout="fixed"  height="0.8cm" border-color="white" border-width=".1mm"  >
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-body>
                                                                                
                                                                                                                        
                                        
                                       
                                         <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="11" number-rows-spanned="1" background-color="white"
                                            linefeed-treatment="preserve"
                                            white-space-collapse="false"
                                            white-space-treatment="preserve"
                                            wrap-option="no-wrap">
                                            <fo:block font-size="7pt" padding-before="0.075cm" margin-left='0.125cm'> <xsl:value-of select="pcontrol"/></fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                        </fo:table-body>
                                        
                                        </fo:table>                                                                                                                       
                                        </fo:block>
                            </fo:table-cell>    
                      
     
			</fo:table-row>
                        
                        
                        
		</fo:table-body>
    </fo:table>
	</xsl:template >
        
        
        <!--****************COMENTARIOS*****************-->
        
        <xsl:template match="comentario">
	<fo:table table-layout="fixed"  height="0.8cm" border-color="black" border-style="solid">
		<fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>


   		<fo:table-body>
	  		<fo:table-row background-color="lightgray" height="0.5cm">
	    		<fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					text-align="left" vertical-align="middle"
					number-columns-spanned="15" number-rows-spanned="1" >
					<fo:block font-size="7pt" padding-before="0.1cm" margin-left='0.125cm'>COMENTARIOS</fo:block>
	    		</fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                        <fo:table-cell
					border-width="0.5pt"
					border-style="none"
					border-color="black"
					border-top-color="white"
					text-align="left" vertical-align="middle"
					number-columns-spanned="8" number-rows-spanned="1" background-color="white">
					<fo:block>
                                        
                                        <fo:table table-layout="fixed"  height="0.8cm" border-color="white" border-width=".1mm"  >
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="0.5cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-column column-width="1.0cm"/>
                                        <fo:table-body>
                                                                                
                                                                                                                        
                                        
                                       
                                         <!--*************Esta es la otra fila*********-->
                                        <fo:table-row height="0.5cm">                                      
                                        
                                          <fo:table-cell
                                            border-width="0.5pt"
                                            border-style="none"
                                            border-color="black"
                                            border-top-color="white"
                                            text-align="left" vertical-align="middle"
                                            number-columns-spanned="15" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" padding-before="0.075cm" margin-left='0.125cm'> <xsl:value-of select="comentario"/></fo:block>
                                        </fo:table-cell>
                                        
                                        </fo:table-row>
                                        <!--****************FIN FILA *****************-->
                                        
                                        </fo:table-body>
                                        
                                        </fo:table>                                                                                                                       
                                        </fo:block>
                            </fo:table-cell>    
                      
     
			</fo:table-row>
                        
                        
                        
		</fo:table-body>
    </fo:table>
	</xsl:template >

</xsl:stylesheet>


