<?xml version="1.0" encoding="iso-8859-1"?>
<!--
    Document   : ordenDeCarga.xsl
    Created on : 12 mayo del 2006
    Author     : LREALES
    Description: Definici�n de la plantilla del formato PDF de la impresi�n de la Orden De Carga.
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  		<fo:layout-master-set>
			<fo:simple-page-master master-name="simple"
				margin-right="1cm"
				margin-left="1.9cm"
				margin-bottom="1cm"
				margin-top="1cm"
				page-width="27.9cm"
				page-height="21.6cm"
			>
			  <fo:region-body margin-top="1cm" margin-bottom="1cm"/>
			  <fo:region-before extent="0cm"/>
			  <fo:region-after extent="0cm"/>
			</fo:simple-page-master>
  		</fo:layout-master-set>
  
		<fo:page-sequence master-reference="simple">
				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="raiz"/>
				</fo:flow>
		</fo:page-sequence>
	</fo:root>
</xsl:template>

<xsl:template match="raiz">
	
        <xsl:apply-templates select="data"/>
	</xsl:template>
        
	<xsl:template match="data">
            <fo:table table-layout="fixed" height="4.6cm">
		<fo:table-column column-width="12.5cm"/>
		<fo:table-column column-width="12.5cm"/>
                
                <fo:table-body>
                
                    <fo:table-row>                    
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="white" text-align="center" vertical-align="middle"><fo:block>
                            <xsl:apply-templates select="datos_orden"/></fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="white" 
                            text-align="center" vertical-align="middle"><fo:block>
                            <xsl:apply-templates select="datos_orden"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row>                    
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-color="white" 
                            text-align="center" vertical-align="middle">
                            <fo:block font-size="8pt" font-style="bold"  color="#64646F">ORIGINAL</fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-color="white" 
                            text-align="center" vertical-align="middle">
                            <fo:block font-size="8pt" font-style="bold"  color="#64646F">COPIA</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
               </fo:table-body>
            </fo:table>            
                
	</xsl:template>
		 
        
	<xsl:template match="datos_orden"> 
        
            <fo:table table-layout="fixed" height="4.6cm">
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		<fo:table-column column-width="0.5cm"/>
		                            
   		  <fo:table-body>
                
                    <fo:table-row>
                    
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            number-columns-spanned="12"
                            text-align="center" vertical-align="middle">
                            <fo:external-graphic src="url(images/logo3.bmp)"/>
                        </fo:table-cell>
                        
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="white" 
                            number-columns-spanned="1"
                            text-align="center" vertical-align="middle"
                            background-color="white">					
                            <fo:table table-layout="fixed">
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="0.5cm"/>
                                <fo:table-column column-width="0.5cm"/>

                                <fo:table-body>
						
                                    <fo:table-row>
                                        <fo:table-cell 
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="black" 
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="24" number-rows-spanned="1">
                                            <fo:block font-size="10pt" font-style="bold"  color="black"> ORDEN DE CARGA N� <xsl:value-of select="orden"/> </fo:block>                                            
                                        </fo:table-cell>
                                    </fo:table-row>
							
                                    <fo:table-row>
                                        <fo:table-cell 
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="black" 
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="6" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" font-style="bold"  color="#64646F">VALIDA UNICAMENTE</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell 
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="black" 
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" font-style="bold"  color="#64646F">DIA</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell 
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="black" 
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" font-style="bold"  color="#64646F">MES</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell 
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="black" 
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" font-style="bold"  color="#64646F">A�O</fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                                        
                                    <fo:table-row>
                                        <fo:table-cell 
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="white" 
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="6" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" font-style="bold" color="#64646F">EN LA FECHA</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell 
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="black" 
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" color="black"><xsl:value-of select="dia"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell 
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="black" 
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" color="black"><xsl:value-of select="mes"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell 
                                            border-width="0.5pt"
                                            border-style="solid"
                                            border-color="black" 
                                            text-align="center" vertical-align="middle"
                                            number-columns-spanned="2" number-rows-spanned="1" background-color="white">
                                            <fo:block font-size="7pt" color="black"><xsl:value-of select="ano"/></fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                                        
                                </fo:table-body>
                            </fo:table>
                        </fo:table-cell>
                        
                    </fo:table-row>
                    
                    <fo:table-row height="0.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black"                             
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Se�or (es) : <xsl:value-of select="empresa"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="0.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="16" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Direcci�n : <xsl:value-of select="direccion"/></fo:block>
                        </fo:table-cell>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="8" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Ciudad : <xsl:value-of select="ciudad"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="0.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> El Se�or Conductor : <xsl:value-of select="nomconductor"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="0.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="8" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> con C.C. N� <xsl:value-of select="cedula"/></fo:block>
                        </fo:table-cell>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="8" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> de <xsl:value-of select="expced"/></fo:block>
                        </fo:table-cell>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="8" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Pase N� <xsl:value-of select="pase"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="0.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="8" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Placa del cami�n : <xsl:value-of select="placa"/></fo:block>
                        </fo:table-cell>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="8" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Marca : <xsl:value-of select="marca"/></fo:block>
                        </fo:table-cell>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="8" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Color : <xsl:value-of select="color"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="0.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="8" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Modelo : <xsl:value-of select="modelo"/></fo:block>
                        </fo:table-cell>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="8" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Motor : <xsl:value-of select="motor"/></fo:block>
                        </fo:table-cell>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="8" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Chas�s : <xsl:value-of select="chasis"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="0.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="12" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Afiliado a : <xsl:value-of select="empresaafil"/></fo:block>
                        </fo:table-cell>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="12" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Tarjeta de Operaci�n N� <xsl:value-of select="empresaafil"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="center" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="6.5pt" color="white" background-color="gray">ESTA AUTORIZADO POR NUESTRA EMPRESA PARA RECIBIR DE UD.(S) EL SIGUIENTE CARGAMENTO:</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="center" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black">Informaci�n del Contenido</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="1.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black"                             
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> <xsl:value-of select="contenido"/> </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="0.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Trailer : <xsl:value-of select="trailer"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="0.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Contenedores : <xsl:value-of select="contenedores"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="6pt" color="white" background-color="gray">
                                NOTA: La empresa se responsabiliza de la entrega del cargamento en el lugar de destino de la misma forma que lo recibi� salvo en
                                los casos de vicio propio de la cosa transportada, de empaque deficiente, de inexactitud, falta o deficiencia en los datos del Remitente, o
                                de las siguientes formas de causa extra�a: Choque o volcamiento sin culpa del Conductor; atraco, actos de pirateria terrestre,
                                decomiso por actos de autoridad y todas las formas delictivas y todos los casos de causa extra�a no imputables al Conductor o al
                                Transportador. Si no se declara el valor de las mercancias tal como lo prescribe el nuevo Art�culo 1010 del C�digo de Comercio, la
                                responsabilidad de la Empresa se limitar� a lo se�alado en el Decreto 01 de 1990.
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="0.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Destino : <xsl:value-of select="destino"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="0.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Entregar a : <xsl:value-of select="entregar"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="1.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black"                             
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Observaciones : <xsl:value-of select="observacion"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="1cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Precintos : <xsl:value-of select="todosprecintos"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="0.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Elaborado por : <xsl:value-of select="creation_user"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="1.5cm">
                        <fo:table-cell
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="center" vertical-align="middle"                             
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black">FIRMA Y SELLO AUTORIZADOS POR LA EMPRESA</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="0.5cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="8" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> Recibi : </fo:block>
                        </fo:table-cell>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="8" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> con C.C. N� </fo:block>
                        </fo:table-cell>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="8" number-rows-spanned="1">
                            <fo:block font-size="8pt" color="black"> de </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row height="1cm">
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black"
                            border-top-color="black"
                            border-left-color="black"
                            border-right-color="white"	
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                            <fo:block font-size="5pt">        </fo:block>
                            <fo:block font-size="5pt"> AGENCIA       </fo:block>
                            <fo:block font-size="5pt"> BARRANQUILLA </fo:block>
                            <fo:block font-size="5pt" > BOGOTA       </fo:block>
                            <fo:block font-size="5pt"> BUENAVENTURA </fo:block>
                            <fo:block font-size="5pt"> CALI         </fo:block>
                            <fo:block font-size="5pt"> CARTAGENA    </fo:block>
                        </fo:table-cell>
			<fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black"
                            border-top-color="black"
                            border-left-color="white"
                            border-right-color="white"	
                            text-align="left" 
                            vertical-align="middle"
                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                            <fo:block font-size="5pt">        </fo:block>
                            <fo:block font-size="5pt"> TELEFONO</fo:block>
                            <fo:block font-size="5pt"> 3511533</fo:block>
                            <fo:block font-size="5pt"> 4123555 </fo:block>
                            <fo:block font-size="5pt">  2433442</fo:block>
                            <fo:block font-size="5pt">  6666288 </fo:block>
                            <fo:block font-size="5pt"> 6625172</fo:block>
                        </fo:table-cell>
			<fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black"
                            border-top-color="black"
                            border-left-color="white"
                            border-right-color="white"	
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                            <fo:block font-size="5pt">        </fo:block>
                            <fo:block font-size="5pt"> AGENCIA       </fo:block>
                            <fo:block font-size="5pt"> CUCUTA</fo:block>
                            <fo:block font-size="5pt"> IPIALES</fo:block>
                            <fo:block font-size="5pt"> MARACAIBO</fo:block>
                            <fo:block font-size="5pt"> MEDELLIN</fo:block>
                            <fo:block font-size="5pt"> QUITO</fo:block>
                        </fo:table-cell>
			<fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black"
                            border-top-color="black"
                            border-left-color="white"
                            border-right-color="white"	
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                            <fo:block font-size="5pt">        </fo:block>
                            <fo:block font-size="5pt">TELEFONO </fo:block>
                            <fo:block font-size="5pt"> 5782357</fo:block>
                            <fo:block font-size="5pt"> 734807 </fo:block>
                            <fo:block font-size="5pt"> 9248480</fo:block>
                            <fo:block font-size="5pt"> 2625853 </fo:block>
                            <fo:block font-size="5pt"> 503232</fo:block>
                        </fo:table-cell>
		 	<fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black"
                            border-top-color="black"
                            border-left-color="white"
                            border-right-color="white"	
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                            <fo:block font-size="5pt">        </fo:block>
                            <fo:block font-size="5pt"> AGENCIA       </fo:block>
                            <fo:block font-size="5pt"> GUAYAQUIL </fo:block>
                            <fo:block font-size="5pt"> SANTAMARTA        </fo:block>
                            <fo:block font-size="5pt"> VALENCIA </fo:block>
                            <fo:block font-size="5pt"> TULCAN         </fo:block>
                            <fo:block font-size="5pt"> MONTELIBANO    </fo:block>
                        </fo:table-cell>                        
			<fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black"
                            border-top-color="black"
                            border-left-color="white"
                            border-right-color="black"	
                            text-align="left" vertical-align="middle"
                            number-columns-spanned="4" number-rows-spanned="1" background-color="white">
                            <fo:block font-size="5pt">        </fo:block>
                            <fo:block font-size="5pt">TELEFONO</fo:block>
                            <fo:block font-size="5pt"> 295333</fo:block>
                            <fo:block font-size="5pt"> 421422 </fo:block>
                            <fo:block font-size="5pt"> 8718466</fo:block>
                            <fo:block font-size="5pt"> 985854 </fo:block>
                            <fo:block font-size="5pt"> 7723224</fo:block>
			</fo:table-cell>
                    </fo:table-row>
        
                    <fo:table-row>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="center" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="6.5pt" color="white" background-color="gray">FAVOR CONFIRMAR DATOS DEL CONDUCTOR Y VEHICULO CON NUESTRAS OFICINAS</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    	  
                    <fo:table-row>
                        <fo:table-cell 
                            border-width="0.5pt"
                            border-style="solid"
                            border-color="black" 
                            text-align="center" vertical-align="middle"
                            number-columns-spanned="24" number-rows-spanned="1">
                            <fo:block font-size="10pt" font-style="bold"  color="#64646F"><xsl:value-of select="esduplicado"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                     
	  	</fo:table-body>
              
        </fo:table>
        
    </xsl:template >
	
</xsl:stylesheet>


