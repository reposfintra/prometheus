<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String opcion = (request.getParameter("opcion")!=null) ? request.getParameter("opcion") : "CL000000ST000000RT000-000EF0000000000";
  String ruta = (request.getParameter("ruta")!=null) ? request.getParameter("ruta") : "";
  String codstd="",ori="",dest="";  
  Vector std=null;
  String men = (request.getParameter("men")!=null) ? request.getParameter("men") : "";
  if (men.equals("OK")){%>
      <script>alert("La informacion ha sido guardada exitosamente")</script>
  <%} else if (men.equals("ERROR")){%>
      <script>alert("Ya existe un registro para este cliente con este standar job y codigo de extraflete")</script>
  <%} else if(men.equals("AUTO")){%>
  	<script>alert("No se pudo agregar el registro.\nPorque no existen usuarios de aprobacion para este cliente")</script>
  <%}%>
<html>
<head>
    <title>Ingresar codigo extraflete</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
	<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>

    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
	<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar SJ Extraflete"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <form name="form1" id="form1"method="post" action="<%=CONTROLLER%>?estado=SJExtraflete&accion=Insert" >
	<table width="603" border="2" align="center">
    <tr>
      <td width="591">
	  <table width="99%" align="center">
              <tr>
                <td width="50%" height="22" class="subtitulo1">Relacion SJ Extraflete </td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="33" height="20"></td>
              </tr>
  </table>
	<table width="99%" align="center" >
      <tr class="fila">
        <th width="170" rowspan="2" scope="row"><div align="left" class="letraresaltada">STANDARD/CLIENTE</div></th>
        <td colspan="2">
          <p> <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')">Consultar clientes...</span></p></td>
      </tr>
      <tr>
        <td colspan="2" class="fila"><p class="fila">
            <input name="cliente" type="text" class="textbox" id="cliente" onKeyPress="return verificarTecla(event);" maxlength="6">
            <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" align="absmiddle" onClick="buscarClient();" style="cursor:hand">
            <input name="clienteR" type="hidden" id="clienteR" value="<%=request.getParameter("cliente")%>">
            <input name="standard_nom" type="hidden" id="standard_nom" value="<%=(String)request.getAttribute("std")%>">
            <input name="remitentes" type="hidden" id="remitentes" value=" ">
            <input name="docinterno" type="hidden" id="docinterno" value=" ">
            <input name="fechadesp" type="hidden" id="fechadesp" value=" ">
            <input name="placa" type="hidden" id="placa" value=" ">
            <input name="destinatarios" type="hidden" id="destinatarios" value=" ">
            <input name="trailer" type="hidden" id="trailer" value=" ">
            <input name="conductor" type="hidden" id="conductor" value=" ">
            <input name="ruta" type="hidden" id="ruta" value=" ">
            <input name="toneladas" type="hidden" id="toneladas" value=" ">
            <input name="valorpla" type="hidden" id="valorpla3" value=" ">
            <input name="anticipo" type="hidden" id="anticipo" value=" ">
            <input type="hidden" name="hiddenField7">
            <input name="precintos" type="hidden" id="precintos" value=" ">
            <input name="observacion" type="hidden" id="observacion" value=" ">
            <input name="click_buscar" type="hidden" id="click_buscar">
            <br>
            <%if(request.getAttribute("cliente")!=null){%>
            <input name="valorpla" type="hidden" id="valorpla">
          </p>
            <table width="100%" border="0" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="letraresaltada" >
              <tr>
                <td><strong><%=(String) request.getAttribute("cliente")%></strong></td>
              </tr>
              <tr>
                <td height="24"><strong>Agencia Due&ntilde;a del Cliente : <%=(String) request.getAttribute("agency")%></strong></td>
              </tr>
            </table>
            <%}%>
        </td>
      </tr>
      <%if(request.getAttribute("std")==null){%>
      <tr class="fila">
        <td rowspan="2" ><strong>RUTA</strong>
        <td width="131" bordercolor="#CCCCCC"><strong>ORIGEN</strong></td>
        <td width="368" bordercolor="#CCCCCC">
          <%TreeMap ciudades = model.stdjobdetselService.getCiudadesOri(); 
	  String corigen="";
	  if(request.getParameter("origen")!=null){
	  	corigen = request.getParameter("origen");
	  }
	  String cdest="";
	  if(request.getParameter("destino")!=null){
	  	cdest = request.getParameter("destino");
	  }
	  %>
          <input:select name="ciudadOri" options="<%=ciudades%>" attributesText="style='width:100%;' onChange='buscarDestinos()' class='listmenu'" default='<%=corigen%>'/> </td>
      </tr>
      <tr class="fila">
        <td ><strong>DESTINO</strong></td>
        <td>
          <%TreeMap ciudadesDest = model.stdjobdetselService.getCiudadesDest(); %>
		  <input:select name="ciudadDest" options="<%=ciudadesDest%>" attributesText="style='width:65%;' class='listmenu'"  default='<%=cdest%>'/>
          <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" align="absmiddle" style="cursor:hand " onClick="buscarStandard()">          </td>
      </tr>
      <%if(ciudadesDest.size()>0){%>
      <tr class="fila">
        <td colspan="3" >
          <div align="center"> </div>
      </tr>
      <% }%>
      <%if(request.getAttribute("ok")!=null){%>
      <tr class="fila">
        <td ><strong> ESTANDARD JOB </strong>
        <td colspan="2"><%TreeMap stdjob = model.stdjobdetselService.getStdjobTree(); %>
            <input:select name="standard" options="<%=stdjob%>" attributesText="style='width:100%;' id='standard' class='listmenu'"/> </td>
      </tr>
      <%}
	}else{%>
      <tr class="fila">
        <td ><strong>ESTANDARD JOB </strong></td>
        <td colspan="2" ><%=(String) request.getAttribute("std")%>
            <input name="standard" type="hidden" id="standard" value="<%=(String)request.getAttribute("sj")%>">
        </td>
      </tr>
	  </table>
      <%}%>
	  <table width="99%" align="center" cols="2">
              <tr>
                <td width="50%" height="22" class="subtitulo1">Informaci&oacute;n SJ Extraflete </td>
                <td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
  <table width="99%" cols="3">
      <tr class="fila">
        <td width="26%" nowrap><strong>Extraflete </strong></td>
        <td colspan="3" nowrap><select name="extraflete" class="listmenu">
            <option value="EF0000000000" selected></option>
            <%int pos = opcion.indexOf("EF");
		  String codextra = opcion.substring(pos+2);
		  model.codextrafleteService.listarCodextraflete();
		  List ext = model.codextrafleteService.getListarCodextraflete();
		  for (int i=0; i<ext.size(); i++){		
		      Codextraflete ef = (Codextraflete) ext.get(i);			    
		%>
            <option value="EF<%=ef.getCodextraflete()%>" <%if(codextra.equals(ef.getCodextraflete())){%>selected<%}%>><%=ef.getDescripcion()%></option>
            <%}%>
        </select></td>
      </tr>
      <tr class="fila">
        <td nowrap><strong>Valor costo </strong></td>
        <td width="24%" nowrap><input name="vlr_costo" type="text" class="textbox" id="vlr_costo" onKeyPress="soloDigitos(event,'decOK')" size="20"></td>
        <td width="18%" nowrap><strong>Moneda costo </strong></td>
        <td width="32%" nowrap><select name="moneda_costo" class="listmenu">
          <option value="0" selected></option>
          <%Vector mon = model.monedaService.listarMonedas();
		  for (int i=0; i<mon.size(); i++){		
		      Moneda m = (Moneda) mon.elementAt(i);			    
		%>
          <option value="<%=m.getCodMoneda()%>"><%=m.getNomMoneda()%></option>
          <%}%>
        </select></td>
      </tr>
      <tr class="fila">
        <td nowrap><strong>Valor fijo costo </strong></td>
        <td nowrap colspan="3"><select name="vf_costo" class="listmenu" id="vf_costo">
            <option value="0" selected></option>
            <option value="V">Variable</option>
            <option value="F">Fijo</option>
        </select></td>
      </tr>
      <tr class="fila">
        <td nowrap><strong>Reembolsable</strong></td>
        <td nowrap colspan="3"><select name="reembolsable" class="listmenu" id="select3">
            <option value="0" selected></option>
            <option value="S">SI</option>
            <option value="N">NO</option>
        </select></td>
      </tr>
      <tr class="fila">
        <td nowrap><strong>Valor ingreso </strong></td>
        <td nowrap><input name="vlr_ingreso" type="text" class="textbox" id="vlr_ingreso" onKeyPress="soloDigitos(event,'decOK')" size="20"></td>
        <td nowrap><strong>Moneda ingreso </strong></td>
        <td nowrap><select name="moneda_ingreso" class="listmenu">
          <option value="0" selected></option>
          <%for (int i=0; i<mon.size(); i++){		
		      Moneda m = (Moneda) mon.elementAt(i);			    
		%>
          <option value="<%=m.getCodMoneda()%>"><%=m.getNomMoneda()%></option>
          <%}%>
        </select></td>
      </tr>
      <tr class="fila">
        <td nowrap><strong>Valor fijo ingreso </strong></td>
        <td nowrap colspan="3"><select name="vf_ingreso" class="listmenu" id="vf_ingreso">
            <option value="" selected></option>
            <option value="V">Variable</option>
            <option value="F">Fijo</option>
        </select></td>
      </tr>
      <tr class="fila">
        <td nowrap><strong>Clase valor </strong></td>
        <td nowrap colspan="3"><select name="clase_valor" class="listmenu" id="clase_valor">
            <option value="" selected></option>
            <option value="V">Valor</option>
            <option value="P">Porcentaje</option>
        </select></td>
      </tr>
      <tr class="fila">
        <td nowrap><strong>Porcentaje</strong></td>
        <td nowrap colspan="3"><input name="porcentaje" type="text" class="textbox" id="porcentaje" onKeyPress="soloDigitos(event,'decOK')" size="5" maxlength="3">
        </td>
      </tr>
    </table>
	</td>
	</tr>
	</table>
	<br>
    <div align="center">        <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod" height="21" onClick="ValidarFormExtraflete(form1)" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
		<img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
  
    </form>
	</div>
</body>
</html>
<script>
    function cargarStdJob(BASEURL) {
    cli = form1.cliente.value;
    std = form1.stdjob.value;  
    rut = form1.ruta.value;  
    ext = form1.extraflete.value;  
    location.href(BASEURL+"/extraflete/extraflete.jsp?opcion="+cli+std+rut+ext+"&ruta=&men=");
    }
    function cargarStdJobsRuta(BASEURL){
    cli = form1.cliente.value;
    std = "ST000000";
    rut = form1.ruta.value;  
    ext = form1.extraflete.value;  
    location.href(BASEURL+"/extraflete/extraflete.jsp?opcion="+cli+std+rut+ext+"&ruta="+rut+"&men=");
    }
    function listarClientes(BASEURL) {
    cli = "CL"+form1.cli.value;
    std = "ST000000";
    rut = "RT000";
    ext = "EX0";
    location.href(BASEURL+"/extraflete/extraflete.jsp?opcion="+cli+std+rut+ext+"&ruta=&men=");
    }
    function listarStdJobs(BASEURL) {
    cli = form1.cliente.value;
    std = "ST"+form1.std.value;
    rut = "RT000";
    ext = "EX0";
    location.href(BASEURL+"/extraflete/extraflete.jsp?opcion="+cli+std+rut+ext+"&ruta=&men=");
    }
    function listarRuta(BASEURL) {
    cli = form1.cliente.value;
    std = "ST000000";
    rut = "RT"+form1.ori.value+"-"+form1.dest.value;
    ext = "EX0";
    location.href(BASEURL+"/extraflete/extraflete.jsp?opcion="+cli+std+rut+ext+"&ruta="+rut+"&men=");
    }
	//NUEVAS FUNCIONES
	function buscarDestinos(){
	//alert('holas');
	window.location='<%=CONTROLLER%>?estado=Buscar&accion=StandardColpapel&op=2&pag=sjextra&cliente='+form1.clienteR.value+'&origen='+form1.ciudadOri.value; 
}
function buscarStandard(){
	//alert('holas');
	window.location='<%=CONTROLLER%>?estado=Buscar&accion=StandardColpapel&op=3&pag=sjextra&cliente='+form1.clienteR.value+'&origen='+form1.ciudadOri.value+'&destino='+form1.ciudadDest.value; 
}
function irADespacho(){
	//alert('holas');
	window.location='extraflete/extraflete.jsp?'; 
}
function buscarClient(){
	//alert('holas');
//	form1.click_buscar.value="ok";
	if(form1.cliente.value.length<6){
		var tamano = 6-form1.cliente.value.length;
		var ceros='';
		i =1;
		while(i<=tamano){
			ceros = ceros + '0';
			i++;
		}
		form1.cliente.value = ceros +form1.cliente.value;
		window.location='<%=CONTROLLER%>?estado=Buscar&accion=StandardColpapel&op=1&pag=sjextra&cliente='+form1.cliente.value;
	}
	else if(form1.cliente.value!=''){
		window.location='<%=CONTROLLER%>?estado=Buscar&accion=StandardColpapel&op=1&pag=sjextra&&cliente='+form1.cliente.value;
	}else{
		alert('Escriba el codigo del cliente o el Standard correcto');
	}
}

</script>
