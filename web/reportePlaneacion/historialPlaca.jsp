<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Modificar Despacho</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=DESPACHO"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">  <br>
  <table width="95%"  border="2" align="center">
    <tr>
      <td>
  <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
    <tr>
      <td width="50%" class="subtitulo1" colspan='3'>PROYECCION PLACA <%=request.getParameter("placa")%></td>
      <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
    </tr>
  </table>
      <table width="100%" border="1" align="center" cellpadding="2" cellspacing="1" bordercolor="#999999" >
        <tr class="tblTitulo">
          <td width="17%"  nowrap>Fecha Disponibilidad</td>
          <td width="8%"><div align="center">Conductor</div></td>
          <td width="8%">Origen</td>
          <td width="17%"><div align="center"><strong>Destino</strong></div></td>
          <td width="36%"><div align="center">Standard</div></td>
          <td width="14%"><div align="center">Fecha de Asignacion</div></td>
        </tr>
        <%model.ipredoSvc.buscarHistorial(request.getParameter("placa"));
		Vector historial = model.ipredoSvc.getHistorial();
	  for (int i=0; i<historial.size();i++){
	  	ReportePlaneacion rep= (ReportePlaneacion) historial.elementAt(i);
		%>
        <tr class="<%=i%2==0?"filagris":"filaazul"%>"  >
          <td nowrap class="bordereporte"> <%=rep.getFecdisrec()%></td>
          <td class="bordereporte"><%=rep.getNombrecond()%></td>
          <td class="bordereporte"><%=rep.getNomorirec()%></td>
          <td class="bordereporte"> <%=rep.getNomdesrec()%> </td>
          <td class="bordereporte"><%=rep.getStd_job_desc_rec()%></td>
          <td class="bordereporte"><%=rep.getFecasigrec()%></td>
        </tr>
        <%}
  %>
      </table></td>
    </tr>
  </table>
</div>

</body>
</html>
