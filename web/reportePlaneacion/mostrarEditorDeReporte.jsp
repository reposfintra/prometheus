<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
<title>Editor de reporte planeación diaria de la operación</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Modificacion de la planeacion"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<form id="form1" name="form1" action="<%=CONTROLLER%>?estado=Asignacion&accion=Manual&cmd=Buscar" method="post" >
    <table width="66%"  border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center" class="tablaInferior">
          <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="22" colspan=2 class="subtitulo1"><div align="center" class="subtitulo1">
                     <strong>Generar Reporte </strong>
                     
                  </div></td>
                  <td width="422" class="barratitulo">
                    <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    <input type="hidden" name="fecha" value="SI"></td>
                </tr>
              </table>
                <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3">
                  <tr class="fila">
                    <td width="22%"><input name="radio" type="radio" value="1" checked onClick="radioPlaneacion(this.value);">
            Por fechas </td>
                    <td colspan="3"><input name="radio" type="radio" value="2" onClick="radioPlaneacion(this.value);">
            Todo lo asignado y no asignado </td>
                  </tr>
                  <tr class="fila">
                    <td width="22%">Fecha Inicial: </td>
                    <td width="27%"><input name="fechai" type="text" id="fechai" style='width:120'  readonly value="<%=request.getParameter("fechai")!=null?request.getParameter("fechai").equals("null")?"":request.getParameter("fechai"):""%>">
                         <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(form1.fechai);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a> </td>
                    <td width="24%"><strong>Fecha Final: </strong></td>
                    <td width="27%"><input name="fechaf" type='text' id="fechaf" style='width:120' readonly  value="<%=request.getParameter("fechaf")!=null?request.getParameter("fechaf").equals("null")?"":request.getParameter("fechaf"):""%>">
                      <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(form1.fechaf);return false;" hidefocus><img  src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a> </td>
                  </tr>
                  <tr class="fila">
                    <td>Cliente</td>
                    <td><input name="cliente" type="text" class="textbox" id="cliente"  maxlength="6" value="<%=request.getParameter("cliente")!=null ? request.getParameter("cliente") : ""%>">
                    <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')">Consultar clientes</span></td>
                    <td><strong>Ciudad Disponibilidad </strong></td>
                    <td><%
            		model.agenciaService.searchAgenciaDistrito("FINV");
			        %><input:select name="agencia" options="<%=model.agenciaService.getCbxAgencia()%>" default="<%=request.getParameter("agencia")!=null?request.getParameter("agencia"):""%>"/></td>
                  </tr>
              </table></td>
          </tr>
        </table></td>
      </tr>
    </table>
    <br>
    <div align="center">
      <img name="imageField" type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" width="90" height="21" border="0" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="if(validarFechaPlaneacion()){form1.submit();this.disabled=true;}">
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </div>
	</form>
    <br/>
	<%  if(request.getParameter("mostrar")!=null){
		%>
		<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Asignacion&accion=Manual&cmd=show">
          <input type="hidden" name="fechai2" value="<%=request.getParameter("fechai")%>">
          <input type="hidden" name="fechaf2" value="<%=request.getParameter("fechaf")%>">
          <input type="hidden" name="agencia" value="<%=request.getParameter("agencia")%>">
          <input name="fecha" type="hidden" id="fecha" value="<%=request.getParameter("fecha")%>">
          <%Vector datos = model.ipredoSvc.getDatos();
            if ( datos == null || datos.isEmpty() ) {%>
          <table border="2" align="center">
            <tr>
              <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="262" align="center" class="mensajes">No fue encontrado ningun recurso ni requerimiento entre las fechas dadas.</td>
                    <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="44">&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
          </table>
          <p><strong></strong></p>
          <%}else {%>
          <table width="1500">
            <tr class="subtitulo1">
              <td width="499" ><strong>Planeaci&oacute;n y programaci&oacute;n diaria de la operaci&oacute;n </strong></td>
              <td width="1943" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
            </tr>
            <tr class="bordereporte">
              <td colspan="2">
                <table width="2446" border="1" bordercolor="#999999" class="letra" >
                  <tr class="tblTitulo">
                    <td height="20" colspan="10" ><div align="center"><strong>REQUERIMIENTOS</strong></div></td>
                    <td colspan="13"><div align="center"><strong>RECURSOS</strong></div></td>
                  </tr>
                  <tr class="tblTitulo">
                    <td width="75" rowspan="2"><div align="center"><strong>FECHA</strong></div></td>
                    <td width="30" rowspan="2" ><div align="center" ><strong>ITEM</strong></div></td>
                    <td width="108" rowspan="2" ><div align="center"><strong>FECHA DISPONIBILIDAD </strong></div></td>
                    <td width="144" rowspan="2" ><div align="center"><strong>TIPO</strong></div></td>
                    <td width="67" rowspan="2" ><div align="center" ><strong>ESTANDAR</strong></div></td>
                    <td width="235" rowspan="2" ><div align="center"><strong>DESCRIPCION</strong></div></td>
                    <td width="94" rowspan="2" ><div align="center"><strong>ORIGEN</strong></div></td>
                    <td width="103" rowspan="2"><div align="center"><strong>DESTINO</strong></div></td>
                    <td colspan="2" >
                      <div align="center" ><strong>ASIGNACION MANUAL </strong></div></td>
                    <td width="190" rowspan="2" ><div align="center" >FECHA DISPONIBILIDAD</div></td>
                    <td width="171" rowspan="2"><div align="center" >DESCRIPCION</div></td>
                    <td width="80" rowspan="2" ><div align="center" >ORIGEN</div></td>
                    <td width="98" rowspan="2" ><div align="center" >DESTINO</div></td>
                    <td width="209" rowspan="2"><div align="center" >CONDUCTOR</div></td>
                    <td width="51" rowspan="2" ><div align="center"  >PLACA</div></td>
                    <td width="91" rowspan="2" ><div align="center" >TIPO RECURSO</div></td>
                    <td width="83" rowspan="2" ><div align="center" >PLACA TRAILER</div></td>
                    <td width="85" rowspan="2" ><div align="center" >TIPO TRAILER</div></td>
                    <td width="84" rowspan="2" ><div align="center" >WORK GROUP</div></td>
                    <td colspan="3" >RETORNAR VACIO</td>
                  </tr>
                  <tr class="tblTitulo">
                    <td width="54"><div align="center"><strong>ITEM</strong></div></td>
                    <td width="61"><div align="center"><strong>GUARDAR</strong></div></td>
                    <td>Seleccionar</td>
                    <td >Destino</td>
                    <td >Retornar</td>
                  </tr>
                  <%
                boolean tieneRequerimiento = false;
                for (int i = 0,c=0,recursos=1; i < datos.size(); i++) {
                    tieneRequerimiento = false;
                    ReportePlaneacion rep = ( ReportePlaneacion ) datos.elementAt( i );
                    String colorReq = "";
                    String colorRec = "#A6C4E1";
					String fechaRecurso=""+rep.getFechadispC();
					%>
                  <%String colorItem="";
					
					if ( rep.getStd_job_no().length() > 0 ){
                        c++;
						colorItem="#6DAD27";
						colorReq = "#FFE084";
					}
					if ( rep.getStd_job_no().length() > 0 && rep.getPlaca().length() > 0 ){%>
                  <%colorReq = colorRec = "#B0E17B";
						tieneRequerimiento = true;
                    }%>
                  <tr>
                    <td bgcolor="#FFFFFF"><div align="center"><%=rep.getFecha_dispxag()%></div></td>
                    <td bgcolor=<%=colorItem%>><div align="center"><%=rep.getStd_job_no().length()>0?"C"+c:""%></div>
                        <input name="C<%=c%>dstrct_code" type=hidden value="<%=rep.getDstrct_code()%>">
                        <input name="C<%=c%>num_sec" type=hidden value="<%=rep.getNum_sec()%>">
                        <input name="C<%=c%>std_job_no" type=hidden value="<%=rep.getStd_job_no()%>">
                        <input name="C<%=c%>fecha_dispo" type=hidden value="<%=rep.getFechadispC()%>">
                        <input name="C<%=c%>numpla" type=hidden value="<%=rep.getNumpla()%>">
                        <input name="filaC<%=c%>" type=hidden value=<%=c%>>
                        <input type="hidden" name="C<%=c%>" id="C<%=c%>" value="">
                    </td>
                    <td <%=rep.isAplazado()?"class='filaroja'":"bgcolor='"+colorReq+"'"%>><%=rep.getStd_job_no().length()>0?fechaRecurso.substring(0,16):""%></td>
                    <td bgcolor=<%=colorReq%>><%=rep.getRecurso_req()!=null?rep.getRecurso_req():""%></td>
                    <td bgcolor=<%=colorReq%>><%=rep.getStd_job_no()!=null?rep.getStd_job_no():""%></td>
                    <td bgcolor=<%=colorReq%>><%=rep.getStd_job_desc_req()%></td>
                    <td bgcolor=<%=colorReq%>><%=rep.getStd_job_no().length()>0?rep.getOrigen_req():""%></td>
                    <td bgcolor=<%=colorReq%>><%=rep.getStd_job_no().length()>0?rep.getDestino_req():""%></td>
                    <%
                   // }
                    if ( rep.getPlaca().length() > 0 ){
                        // campos ocultos%>
                    <td><div align="center" class="Estilo16">
                        <input name="R<%=recursos%>dstrct" type=hidden value="<%=rep.getDstrct()%>">
                        <input name="R<%=recursos%>placa" type=hidden value="<%=rep.getPlaca()%>">
                        <input name="R<%=recursos%>fecha_disp" type=hidden value="<%=rep.getFechadispR()%>">
                        <input name="filaR<%=c%>" type=hidden value="<%=tieneRequerimiento?c:0%>">
                        <input name="numrec<%=recursos%>" type="hidden" id="numrec<%=recursos%>" value="<%=recursos%>">
                        <input name="asignado<%=recursos%>" id="asignado<%=recursos%>" value="<%=tieneRequerimiento?"C"+c:""%>" type="text" size="5" alt="PARA ASIGNAR ESTE RECURSO A UN REQUERIMIENTO DIGITE EL NUMERO DEL ITEM" onKeyUp="copiarRecurso(this.value,'<%=recursos%>');">
                    </div></td>
                    <td bgcolor="<%=colorRec%>"><div align="center">
                        <input name="Submit<%=c%>" type="image" src="<%=BASEURL%>/images/botones/aplicar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="this.disabled=true;form2.submit();" >

                    </div></td>
                    <td bgcolor=<%=colorRec%>><%=rep.getFechadispR()%></td>
                    <td bgcolor=<%=colorRec%>><%=rep.getStd_job_desc_rec()%></td>
                    <td bgcolor=<%=colorRec%>><%=rep.getOrigenR()%></td>
                    <td width="98" bgcolor=<%=colorRec%>><%=rep.getDestinoR()%></td>
                    <td width="209" bgcolor=<%=colorRec%>><%=rep.getNombrecond()%></td>
                    <td width="51" bgcolor=<%=colorRec%> style="cursor:hand " onClick="window.open('<%=BASEURL%>/reportePlaneacion/historialPlaca.jsp?placa=<%=rep.getPlaca()%>')"><%=rep.getPlaca()%></td>
                    <td width="91" bgcolor=<%=colorRec%>><%=rep.getClase()%></td>
                    <td width="83" bgcolor=<%=colorRec%>><%=rep.getEqasorec()%></td>
                    <td width="85" bgcolor=<%=colorRec%>><%=rep.getTipo_asoc()%></td>
                    <td width="84" bgcolor=<%=colorRec%>><%=rep.getWgroup()%></td>
                    <td width="68" bgcolor=<%=colorRec%>><div align="center">
                        <%if(!tieneRequerimiento){%>
                        <input type="checkbox" name="check<%=recursos%>" value="<%=recursos%>">
                        <%}%>
                    </div></td>
                    <td width="57" bgcolor=<%=colorRec%>><%if(!tieneRequerimiento){
						String nombre = "agencia_"+recursos;%>
                        <input:select name="<%=nombre%>" options="<%=model.agenciaService.getCbxAgencia()%>" default="<%=request.getParameter("agencia_"+recursos)%>"/>
                        <%}%></td>
                    <td width="66" bgcolor=<%=colorRec%>><%if(!tieneRequerimiento){%>
                        <img name="Submit2<%=c%>" type="image" src="<%=BASEURL%>/images/botones/aplicar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="this.disabled=true;retornoVacio('<%=CONTROLLER%>?estado=Retornar&accion=Vacio&cmd=show');" >
                        <%}%></td>
                    <%                        recursos++;
                    }
                    %>
                  </tr>
                  <%}%>
              </table></td>
            </tr>
          </table>
          <br>
          <div align="center">
            
            <input name="imageField" type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="this.disabled=true;form2.submit();" >
          </div>
  </form>
		<% }
	} %>
</div>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
