<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Lista de Remesas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/letras.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
</head>

<body>
  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>

<table width="530" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
  <tr bgcolor="#FFA928">
    <td colspan="4" nowrap><div align="center" class="Estilo2"><strong>LISTA REMESAS RELACIONADAS CON LA PLANILLA </strong></div></td>
  </tr>
  <tr bgcolor="#99CCFF">
    <td width="79"><div align="center"><strong>REMESA</strong></div></td>
    <td width="74" bgcolor="#99CCFF"><div align="center"><strong>CLIENTE</strong></div></td>
    <td width="111" class="Estilo4"><div align="center" class="Estilo10 Estilo2 Estilo3">
    <strong>ESTANDAR</strong></div></td>
    <td width="150" class="Estilo4"><div align="center"><span class="Estilo1">VALOR</span></div></td>
  </tr>
  <%  List remesas= model.planillaService.buscarRemesas(request.getParameter("nopla"));
	  Iterator rem=remesas.iterator();
	  int i=0;
	  while (rem.hasNext()){
	  Remesa remesa = (Remesa) rem.next();
		%>
  <tr>
    <td>
    <div align="center" class="Estilo6"><%=remesa.getNumrem()%> </div></td>
    <td>
      <div align="center" class="Estilo6"><%=remesa.getCliente()%>    </div></td>
    <td><div align="center" class="Estilo4"><%=remesa.getDescripcion()%></div></td>
    <td><span class="Estilo6"><%=remesa.getVlrRem()%></span></td>
  </tr>
  <%}
  %>
</table>
</body>
</html>
