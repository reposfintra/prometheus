<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<style type="text/css">
<!--
.Estilo6 {font-family: Arial, Helvetica, sans-serif; font-size: 10pt}
.Estilo7 {font-size: 9pt}
body {
	background-color: #ebebeb;
}
-->
</style>
<link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo10 {
	font-family: "Times New Roman", Times, serif;
	font-style: italic;
	color: #000000;
}
.Estilo11 {color: #FF0000}
.Estilo12 {font-size: 12pt}
.Estilo1 {font-weight: bold}
.style1 {color: #000000; font-family: "Times New Roman", Times, serif;}
-->
</style>
</head>

<body>
<%//Inicializo variables
String estandard="", estandar="", fechadesp="", rutaP="", codruta="", trailer="", conductor="", nomcond="",cliente="", fecha="",standar="", observacion="",docuinterno="", remitentes="", destinatarios="", remesa="", planilla="", cedula="", nombre="", placa="", precinto=""; 
float peso=0, valor=0, ant=0;
String Rem ="", Pla ="";
if(request.getParameter("remesa")!=null){
	Rem = request.getParameter("remesa");
	Pla = request.getParameter("planilla");
}
List planillas=new LinkedList();
String color="#ECE0D8";
if(request.getAttribute("color")!=null){
	color = (String)request.getAttribute("color");
}
java.util.Date date = new java.util.Date();
SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mma");
	String timestamp = s.format(date);
	timestamp = timestamp.toLowerCase();
%>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=RemesaPlanilla&accion=Search&cumplido=ok">
  <table width="530" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
    <tr bgcolor="#FFA928">
      <td colspan="2" nowrap><div align="center" class="Estilo6"><strong><strong>BUSQUE LA REMESA Y LA PLANILLA QUE VA A CUMPLIR </strong></strong></div></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td width="194" nowrap bgcolor="#99CCFF"><strong class="Estilo6">Numero de la Planilla:</strong></td>
      <td width="320" nowrap bgcolor="<%=color%>"><input name="planilla" type="text" id="planilla" value="<%=Pla%>" maxlength="10"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong class="Estilo6">Numero de la Remesa:</strong></td>
      <td nowrap bgcolor="<%=color%>">        <input name="remesa" type="text" id="remesa" value="<%=Rem%>" maxlength="10"></td>
    </tr>
    <tr>
      <td colspan="2" nowrap><div align="center">
        <input type="submit" name="Submit" value="MOSTRAR DATOS...">
      </div></td>
    </tr>
  </table>
</form>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Cumplido&accion=Validar" onSubmit="return ValidarCumplido();">
  <div align="center">
    <p>
      <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
      <%
      if(request.getAttribute("remesa")!=null){
	    Remesa rem = (Remesa)request.getAttribute("remesa");
	    request.getSession().setAttribute("remesa",rem);
        estandar = rem.getDescripcion();
        standar= rem.getStdJobNo();
        docuinterno = rem.getDocInterno();
        remitentes = rem.getRemitente();
        destinatarios = rem.getDestinatario();
        remesa = rem.getNumrem();
		observacion = rem.getObservacion();
		cliente = rem.getCliente();
      }
	   if(request.getAttribute("planilla")!=null){
        Planilla pla= (Planilla) request.getAttribute("planilla");
        placa=pla.getPlaveh();
		trailer=pla.getPlatlr();
		conductor= pla.getCedcon();
		nomcond=pla.getNomCond();
		estandard=pla.getSj_desc();
		standar= pla.getSj();
		fechadesp = pla.getFecdsp();
		peso=pla.getPesoreal();
		rutaP = pla.getRuta_pla();
		codruta = pla.getCodruta();
		precinto = pla.getPrecinto();
		planilla= pla.getNumpla();
    }
	  if(request.getAttribute("movpla")!=null){
       Movpla movpla = (Movpla) request.getAttribute("movpla");
       valor = movpla.getVlr();
      }
	 
     
  %>
    </p>
  </div>
  <table width="655" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#ECE0D8" class="Letras">
    <%if(request.getAttribute("remesa")!=null){%>
	<tr bgcolor="#FFA928">
      <th colspan="2" class="Estilo6" scope="col">REMESA No. <%=remesa%>
      <input name="remesa" type="hidden" id="remesa" value="<%=remesa%>"></th>
    </tr>
    <tr bgcolor="#99CCFF" class="Estilo6">
      <th colspan="2" class="Estilo6" scope="row">INFORMACION DE LA REMESA </th>
    </tr>
    <tr class="Estilo6">
      <th width="164" scope="row"><div align="left">ESTANDARD JOB </div></th>
      <td width="475"><%=estandar%>
          <input name="standard" type="hidden" id="standard" value="<%=standar%>">
      </td>
    </tr>
    <tr class="Estilo6">
      <th scope="row"><div align="left">CLIENTE</div></th>
      <td><div align="left"><%=cliente%> </div></td>
    </tr>
    <tr class="Estilo6">
      <th scope="row"><div align="left"><span class="Estilo7">REMITENTE</span></div></th>
      <td><div align="left"><%=remitentes%> 
        <input name="remitentes" type="hidden" id="remitentes" value="<%=remitentes%>">
      </div></td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">DESTINATARIO</div></th>
      <td><%=destinatarios%>
      <input name="destinatarios" type="hidden" id="destinatarios" value="<%=destinatarios%>"></td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">DOCUMENTO INTERNO </div></th>
      <td><%=docuinterno%></td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">OBSERVACION</div></th>
      <td><%=observacion%></td>
    </tr>
	<%}%>
	<%if(request.getAttribute("remesas")!=null){%>
    <tr bgcolor="#FFA928" class="Estilo6">
      <th colspan="2" scope="row">
	  <table width="100%" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Estilo6">
        <tr bgcolor="#FFA928">
          <td colspan="4" nowrap><div align="center" class="Estilo2"><strong>LISTA REMESAS RELACIONADAS CON LA PLANILLA </strong></div></td>
        </tr>
        <tr bgcolor="#99CCFF">
          <td width="79"><div align="center"><strong>REMESA</strong></div></td>
          <td width="74" bgcolor="#99CCFF"><div align="center"><strong>CLIENTE</strong></div></td>
          <td width="111" class="Estilo4"><div align="center" class="Estilo6"> <strong>ESTANDAR</strong></div></td>
          <td width="150" class="Estilo4"><div align="center"><span class="Estilo1">VALOR</span></div></td>
        </tr>
        <%  
	  List remesas=(List) request.getAttribute("remesas");
	  Iterator rem=remesas.iterator();
	  while (rem.hasNext()){
	  Remesa rms = (Remesa) rem.next();
		%>
        <tr style="cursor:hand" title="Elejir esta remesa..." onClick="window.location='<%=CONTROLLER%>?estado=RemesaPlanilla&accion=Search&cumplido=ok&remesa=<%=rms.getNumrem()%>&planilla=<%=planilla%>'" onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor=''">
          <td><div align="center" class="Estilo6"><%=rms.getNumrem()%> </div></td>
          <td><div align="center" class="Estilo6"><%=rms.getCliente()%> </div></td>
          <td><div align="center" class="Estilo4"><%=rms.getDescripcion()%></div></td>
          <td><%=rms.getVlrRem()%></td>
        </tr>
        <%}
  %>
      </table></th>
    </tr>
	<%}%>
    <tr bgcolor="#FFA928" class="Estilo6">
      <th colspan="2" scope="row">PLANILLA No. <%=planilla%>
        <input name="planilla" type="hidden" id="planilla" value="<%=planilla%>"></th>
    </tr>
    <tr bgcolor="#99CCFF" class="Estilo6">
      <th colspan="2" scope="row">INFORMACION DE LA PLANILLA</th>
    </tr>
    <tr class="Estilo6">
      <th height="11" scope="row"><div align="left">FECHA DESPACHO </div></th>
      <td>
        <div align="left"><%=fechadesp%>        </div></td>
    </tr>
    <tr class="Estilo6">
      <th height="13" scope="row"><div align="left"><span class="Estilo7">&nbsp;PLACA</span></div></th>
      <td><%=placa%></td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">&nbsp;TRAILER</div></th>
      <td>
        <div align="left"><%=trailer%> </div></td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">&nbsp;CEDULA CONDUCTOR </div></th>
      <td>         <div align="left"><%=conductor%> <%=nomcond%>
      </div></td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">&nbsp;RUTA PLANILLA </div></th>
      <td><%=rutaP%>
        <input name="rutaP" type="hidden" id="rutaP" value="<%=codruta%>"> </td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">&nbsp;CANTIDAD</div></th>
      <td><%=peso%> TON </td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">&nbsp;VALOR ANTICIPO </div></th>
      <td><%=ant%> </td>
    </tr>
    <tr class="Estilo6">
      <th scope="row"><div align="left" class="Estilo7">&nbsp;PRECINTOS</div></th>
      <td><%=precinto%></td>
    </tr>
	
  </table>
  <br>
<%if(request.getAttribute("remesa")!=null && request.getAttribute("planilla")!=null ){%>
  <table width="655" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Estilo6">
    <tr bgcolor="#FFA928">
      <td colspan="2"><div align="center" class="Estilo6"><strong>DATOS DEL CUMPLIDO</strong></div></td>
    </tr>
    <tr>
      <td width="166"><strong>FECHA CUMPLIDO</strong></td>
      <td width="473"><input name="feccum" type="text" id="feccum" size="18" readonly value="<%=timestamp%>">
        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.feccum);return false;" HIDEFOCUS> <img src="js/Calendario/cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha"></a></td>
    </tr>
    <tr>
      <td><strong>CANTIDAD CUMPLIDA </strong></td>
      <td><input name="cantidad" type="text" id="cantidad"></td>
    </tr>
    <tr>
      <td><strong>OBSERVACION</strong></td>
      <td><textarea name="observacion" cols="50" id="observacion"></textarea></td>
    </tr>
  </table>  
  <div align="center">
    <input type="submit" name="Submit8" value="Validar">
  </div>
  <%}%>
</form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
