var oldQuery    = "";
var countParams = 0;

function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPagina(url, nombrePagina)
{
  var wdth = screen.width - screen.width * 0.20;
  var hght = screen.height - screen.height * 0.40;
  var lf = screen.width * 0.1;
  var tp = screen.height * 0.2;
  var options = "menubar=yes,scrollbars=yes,resizable=yes,status=yes,titlebar=no," +
                "toolbar=no," + setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( document.window != null && !hWnd.opener )
    hWnd.opener = document.window;
}

function trim(str)
{
  var startIdx = 0, endIdx = str.length - 1;
  while( endIdx > 0 && str.charAt(endIdx) == ' ' )
    endIdx -= 1;
  while( startIdx <= endIdx && str.charAt(startIdx) == ' ' )
    startIdx += 1;
  var trimmedString = "";
  if( startIdx < endIdx + 1 )
    trimmedString = str.substring(startIdx, endIdx + 1);
  return trimmedString;
}

function countSelectedOptions(selectObj)
{
  var contSelected = 0;
  for( var idx = 0; idx < selectObj.options.length; idx++ )
  {
    if( selectObj.options[idx].selected )
      contSelected += 1;
  }
  return contSelected;
}

function parametrosSonCorrectos( actionName )
{
  var creador       = trim(document.filtroCrearQueryFrm.creador.value);
  var usuario       = document.filtroCrearQueryFrm.usuario.value;
  var descripcion   = trim(document.filtroCrearQueryFrm.descripcion.value);
  var query         = trim(document.filtroCrearQueryFrm.query.value);
  var paramNamesListSize  = document.filtroCrearQueryFrm.paramNamesList.options.length;
  var paramValuesListSize = document.filtroCrearQueryFrm.paramValuesList.options.length;
  
  // VERIFICAR QUE TODOS LOS PARAMETROS FUERON DIGITADOS.
  /**
  if( usuario == "" && actionName == 'save' ) {
    alert( "　 Debe digitar el ID del usuario asociado al query !!" );
    return false;
  }
  */
  if( descripcion == "" && actionName == 'save' ) {
    alert( "　 Debe digitar la descripcion del query !!" );
    return false;
  }
  if( query == "" ) {
    alert( "　 Debe digitar el query !!" );
    return false;
  }else if( query.substring(0, 6).toLowerCase() != "select" ){
    alert( "　 Solo se pueden insertar queries para consultar (tipo SELECT) !!" );
    return false;
  }
  if( countParams != paramNamesListSize && (actionName == 'save' || actionName == 'update')) {
    alert( "　 Este query tiene " + countParams + " par�metros !!" );
    return false;
  }
  if( countParams != paramValuesListSize && actionName == 'execute') {
    alert( "　 Este query tiene " + countParams + " par�metros !!" );
    return false;
  }
  return true;
}

function queryBlur()
{
  var query = trim(document.filtroCrearQueryFrm.query.value);
  if( oldQuery != query && query != "" )
  {
    oldQuery = query;
    countParams = new String(query).split("<param>").length - 1;
    var selectObj = document.filtroCrearQueryFrm.paramNamesList;
    if( selectObj.options.length > 0 )
    {
      selectObj.options.length = 0;  
      document.filtroCrearQueryFrm.nombreparam.value = "";
      document.filtroCrearQueryFrm.nombresParams.value = "";
    }
    selectObj = document.filtroCrearQueryFrm.paramValuesList;
    if( selectObj.options.length > 0 )
    {
      selectObj.options.length = 0;
      document.filtroCrearQueryFrm.valorparam.value = "";
      document.filtroCrearQueryFrm.valoresParams.value = "";
    }
    if( countParams > 0 )
    {
      document.filtroCrearQueryFrm.nuevoNombreParamBtn.disabled = false;
      document.filtroCrearQueryFrm.quitarNombreParamBtn.disabled = false;
      document.filtroCrearQueryFrm.nuevoValorParamBtn.disabled = false;
      document.filtroCrearQueryFrm.quitarValorParamBtn.disabled = false;
      document.filtroCrearQueryFrm.nombreparam.disabled = false;
      document.filtroCrearQueryFrm.valorparam.disabled = false;
    }else{
      document.filtroCrearQueryFrm.nuevoNombreParamBtn.disabled = true;
      document.filtroCrearQueryFrm.quitarNombreParamBtn.disabled = true;
      document.filtroCrearQueryFrm.nuevoValorParamBtn.disabled = true;
      document.filtroCrearQueryFrm.quitarValorParamBtn.disabled = true;
      document.filtroCrearQueryFrm.nombreparam.disabled = true;
      document.filtroCrearQueryFrm.valorparam.disabled = true;
      document.filtroCrearQueryFrm.nombreparam.value = "";
      document.filtroCrearQueryFrm.nombresParams.value = "";
      document.filtroCrearQueryFrm.valorparam.value = "";
      document.filtroCrearQueryFrm.valoresParams.value = "";
    }
  }else if( countParams <= 0 ){
    document.filtroCrearQueryFrm.nuevoNombreParamBtn.disabled = true;
    document.filtroCrearQueryFrm.quitarNombreParamBtn.disabled = true;
    document.filtroCrearQueryFrm.nuevoValorParamBtn.disabled = true;
    document.filtroCrearQueryFrm.quitarValorParamBtn.disabled = true;
    document.filtroCrearQueryFrm.nombreparam.disabled = true;
    document.filtroCrearQueryFrm.valorparam.disabled = true;
    document.filtroCrearQueryFrm.nombreparam.value = "";
    document.filtroCrearQueryFrm.nombresParams.value = "";
    document.filtroCrearQueryFrm.valorparam.value = "";
    document.filtroCrearQueryFrm.valoresParams.value = "";
  }
}

function nuevoNombreParamBtnClick()
{
  var paramName = trim(document.filtroCrearQueryFrm.nombreparam.value);
  var selectObj = document.filtroCrearQueryFrm.paramNamesList;
  if( paramName != "" )
  {
    if( selectObj.options.length == countParams )
    {
      alert("　 Ya se insert� el m�x. n�mero de nombres de par�metros permitidos por el query !!");
      return;
    }
    var option = document.createElement("OPTION");
    selectObj.options.add(option);
    option.innerText = paramName;
    option.value = paramName;
    var paramCsvList = document.filtroCrearQueryFrm.nombresParams;
    paramCsvList.value += (paramCsvList.value == "" ? "" : ",") + paramName;
  }else
    alert("　 Debe digitar el nombre del par�metro !!");
}

function nuevoValorParamBtnClick()
{
  var paramValue = trim(document.filtroCrearQueryFrm.valorparam.value);
  var selectObj = document.filtroCrearQueryFrm.paramValuesList;
  if( paramValue != "" )
  {
    if( selectObj.options.length == countParams )
    {
      alert("　 Ya se insert� el m�x. n�mero de par�metros permitidos por el query !!");
      return;
    }
    var option = document.createElement("OPTION");
    selectObj.options.add(option);
    option.innerText = paramValue;
    option.value = paramValue;
    var paramCsvList = document.filtroCrearQueryFrm.valoresParams;
    paramCsvList.value += (paramCsvList.value == "" ? "" : ",") + paramValue;
  }else
    alert("　 Debe digitar el valor del par�metro !!");
}

function quitarNombreParamBtnClick()
{
  var selectObj = document.filtroCrearQueryFrm.paramNamesList;
  if( selectObj.selectedIndex >= 0 )
  {
    var contSelected = countSelectedOptions(selectObj), idx, jdx, optionRemoved;
    for( idx = 1; idx <= contSelected; idx++ )
    {
      jdx = 0; optionRemoved = false;
      while( !optionRemoved && jdx < selectObj.options.length )
      {
        if( selectObj.options[jdx].selected )
        {
          selectObj.options.remove(jdx);
          optionRemoved = true;
        }else
          jdx += 1;
      }
    }
    var paramCsvList = document.filtroCrearQueryFrm.nombresParams;
    paramCsvList.value = "";
    for( idx = 0; idx < selectObj.options.length; idx++ )
      paramCsvList.value += (paramCsvList.value == "" ? "" : ",") + 
                            selectObj.options[idx].value;
  }else
    alert("　 Debe seleccionar al menos el nombre de un par�metro para eliminar !!");
}

function quitarValorParamBtnClick()
{
  var selectObj = document.filtroCrearQueryFrm.paramValuesList;
  if( selectObj.selectedIndex >= 0 )
  {
    var contSelected = countSelectedOptions(selectObj), idx, jdx, optionRemoved;
    for( idx = 1; idx <= contSelected; idx++ )
    {
      jdx = 0; optionRemoved = false;
      while( !optionRemoved && jdx < selectObj.options.length )
      {
        if( selectObj.options[jdx].selected )
        {
          selectObj.options.remove(jdx);
          optionRemoved = true;
        }else
          jdx += 1;
      }
    }
    var paramCsvList = document.filtroCrearQueryFrm.valoresParams;
    paramCsvList.value = "";
    for( idx = 0; idx < selectObj.options.length; idx++ )
      paramCsvList.value += (paramCsvList.value == "" ? "" : ",") + 
                            selectObj.options[idx].value;
  }else
    alert("　 Debe seleccionar al menos el valor de un par�metro para eliminar !!");
}

function queryActionBtnsClick( actionName, baseUrl )
{
  var list = document.forms[0].userAdd;
  var list1 = document.forms[0].paramNamesList;
  var param = "";
  if( parametrosSonCorrectos(actionName) )
  {
    var queryObj = document.filtroCrearQueryFrm.query;
    queryObj.value = trim(queryObj.value).replace(/<param>/gi, "?");
    if( actionName == 'execute' ) {
      document.filtroCrearQueryFrm.cmd.value = "sql_execute";
      document.filtroCrearQueryFrm.submit();
    }else if( actionName == 'save' ){
      if (list.length > 0){
          selectItems(list);
          document.filtroCrearQueryFrm.cmd.value = "sql_create";
          document.filtroCrearQueryFrm.submit();
      }
      else{
          alert("DEBE AGREGAR AL MENOS UN USUARIO ASOCIADO AL QUERY PARA PODER GUARDAR LA CONSULTA");
      }
    }else if (actionName == 'update'){
      if (list.length > 0){
          selectItems(list);
          document.filtroCrearQueryFrm.nombresParams.value = "";
          document.filtroCrearQueryFrm.cmd.value = "sql_update";
          for (i = 0; i < list1.length; i++ ){
              param = document.filtroCrearQueryFrm.nombresParams.value;
              param += (param == "" ? "" : ",") + list1.options[i].text;
              document.filtroCrearQueryFrm.nombresParams.value = param;
          }
          document.filtroCrearQueryFrm.submit();
      }
      else{
          alert("DEBE AGREGAR AL MENOS UN USUARIO ASOCIADO AL QUERY PARA PODER GUARDAR LA CONSULTA");
      }
    }
  }
}

function executeQuery( baseUrl, recordId )
{
  var mayExecute = true;
  var params = "?estado=Consultas&accion=Manage&cmd=sql_listexec&recordId=" + recordId +
               "&descripcion=" + document.getElementById("desc" + recordId).value +
               "&contentType=" + document.getElementById("contentType" + recordId).value +
               "&baseDeDatos=" + document.getElementById("baseDeDatos" + recordId).value;
  var queryParams = document.getElementsByName("param" + recordId);
  if(queryParams != null)
  {
    for( var idx = 0; idx < queryParams.length && mayExecute; idx++ )
    {
      var parameterValue = trim(queryParams[idx].value);
      if( parameterValue == "" )
        mayExecute = false;
      else
        params += "&param" + recordId + "=" + parameterValue;
    }
  }
  if( mayExecute )
    abrirPagina(baseUrl + params, "wexecutesavedquery");
  else
    alert("　Debe digitar los par�metros necesarios para ejecutar el query!!");
}

function addOption(list,valor,texto){
    var opt = document.createElement("OPTION");
    opt.value=valor;
    opt.text=texto;
    list.add(opt);
}
    
function addElement(elem, list){
   if (elem.value != ''){ 
      for(i=0; i < list.length; i++)
         if (list.options[i].value == elem.value){
            alert("Este usuario ya fue agregado, por favor seleccione otro");
            return;
         }
      addOption(list, elem.value, elem.options[elem.selectedIndex].text);
   }
   else{
       alert("Seleccione un usuario para agreagar");
   }
}

function removeElem(list){
   for(i=(list.length-1);i>=0;i--)
    if (list.options[i].selected) list.remove(i);
}

function selectItems(list){
   for (i = 0; i < list.length; i++){
       list.options[i].selected = true;
   }
}