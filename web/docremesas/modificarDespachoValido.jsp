<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Modificacion de Despachos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=MODIFICAR DESPACHO"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;"> 
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Colpapel&accion=Modificar&cmd=show" onSubmit="return  ValidarColpapelModif(this);">

  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <%java.util.Date date = new java.util.Date();
               %>
  <%
  	String placa="", trailer="", conductor="", nomcond="", estandard="",  standar="", remitente="", destinatario="",docinterno="", fechadesp="", observacion="", rutaP="", precinto="" ;
	float ant=0, peso=0, cantfac=0, cantreal=0,vlrpla=0;;
	String faccial="", banco ="", c1="", c2="",tipocont="",unidad="", p1="",p2="",p3="",p4="",p5="",unitVlr="",docudest="";
		String nitpro = "";
	
	placa=request.getParameter("placa");
	trailer=request.getParameter("trailer");
	conductor= request.getParameter("conductor");
	nomcond=request.getParameter("nomcond");
	estandard=request.getParameter("estandard");
	standar= request.getParameter("standard");
	fechadesp = request.getParameter("fechadesp");
	String pc1 = request.getParameter("c1precinto")!=null?request.getParameter("c1precinto"):"";
	String pc2 = request.getParameter("c2precinto")!=null?request.getParameter("c2precinto"):"";
	p1 = request.getParameter("precintos")!=null?request.getParameter("precintos"):"";
	p2 = request.getParameter("precintos2")!=null?request.getParameter("precintos2"):"";
	p3 = request.getParameter("precintos3")!=null?request.getParameter("precintos3"):"";
	p4 = request.getParameter("precintos4")!=null?request.getParameter("precintos4"):"";
	p5 = request.getParameter("precintos5")!=null?request.getParameter("precintos5"):"";
	
	c1 = request.getParameter("c1");
	c2 = request.getParameter("c2");
	tipocont =request.getParameter("tipo_cont");
	banco = request.getParameter("banco");
	remitente = request.getParameter("remitentes");
	destinatario= request.getParameter("destinatarios");
	docinterno = request.getParameter("docinterno");
	observacion = request.getParameter("observacion");
	faccial = request.getParameter("fcial");
	cantfac = Float.parseFloat(request.getParameter("cfacturar"));
	cantreal= Float.parseFloat(request.getParameter("cantreal"));
	unidad = request.getParameter("unidad");
	docudest = request.getParameter("docudest");
        String sticker = "";    
 	model.planillaService.bucarColpapel(request.getParameter("planilla"));
    if(model.planillaService.getPlanilla()!=null){
        Planilla pla= model.planillaService.getPlanilla();
		if(pla.getPrinter_date2()!=null){
			sticker = pla.getPrinter_date2().equals("0099-01-01 00:00:00")?"":"readonly";
		}
        unitVlr = pla.getUnit_transp();
		peso=pla.getPesoreal();
		rutaP = pla.getRuta_pla();
		vlrpla = pla.getVlrpla();
		nitpro = pla.getNitpro();
    }
    
    %>
  <% 
	  Stdjobdetsel sj=new Stdjobdetsel();
	  String pa ="";
	  String uw="";
	  String vlr ="0";
	  model.stdjobdetselService.searchStdJob(standar);
          if(model.stdjobdetselService.getStandardDetSel()!=null){
			sj = model.stdjobdetselService.getStandardDetSel();
            uw=sj.getUnit_of_work();
            pa=sj.getPorcentaje_ant();
			vlr = ""+sj.getVlr_freight(); 
			estandard=sj.getSj_desc();
			standar= sj.getSj();            
            }
			String dis= !p1.equals("")?"disabled":"";
		float maxant =0;
		if(!pa.equals(""))
		 maxant = (vlrpla * Float.parseFloat(pa.replaceAll("%","")))/100;
	  %>
 <table width="100%"  border="2" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td height="22" colspan=2 class="subtitulo1">Informacion del Despacho</td>
          <td width="368" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="100%" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Datos Remesa  <%=request.getParameter("remesa")%></td>
            </tr>
        </table>
          <table width="100%" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC"  >
            <tr class="fila">
              <th colspan="2" scope="row"><div align="left">Estandar Job</div>
                  <span class="Estilo5 Estilo8"> </span><strong></strong></th>
              <td colspan="2"><span class="Estilo5 Estilo8">                <strong> </strong></span><strong><span class="Estilo6"><span class="Estilo5">
                <%=estandard%>
                <input name="planilla" type="hidden" id="planilla2" value="<%=request.getParameter("planilla")%>">
                <input name="standard" type="hidden" id="standard2" value="<%=standar%>">
                <input name="remesa" type="hidden" id="remesa2" value="<%=request.getParameter("remesa")%>">
                <input name="estandard" type="hidden" id="estandard2" value="<%=estandard%>">
                <input name="ruta" type="hidden" value="<%=rutaP%>">
                <input name="vlrpla" type="hidden" id="vlrpla" value="<%=request.getParameter("vlrpla")%>">
                <strong>
                <input name="toneladas" type="hidden" id="toneladas2" value="<%=peso%>">
                </strong>
                <input name="docudest" type="hidden" id="docudest2" value="<%=request.getParameter("docudest")%>">
                <input name="remitentes" type="hidden" id="remitentes2" value="<%=request.getParameter("remitentes")%>">
                <input name="destinatarios" type="hidden" id="destinatarios2" value="<%=request.getParameter("destinatarios")%>" readonly>
                <strong>
                <input name="uw" type="hidden" id="uw2" value="<%=uw%>">
                <input name="vrem2" type="hidden" id="vrem22">
              </strong> </span></span> </strong></td>
              <td width="16%">FECHA DESPACHO </td>
              <td width="19%" colspan="2"><input name="fechadesp" type="text" id="fechadesp" size="18" value="<%=fechadesp.substring(0,16)%>" readonly></td>
            </tr>
            <tr class="fila">
              <th colspan="2" scope="row"><div align="left">Informaci&oacute;n al Cliente </div></th>
              <td colspan="5"><span class="Estilo6"><a onClick="abrirPagina('<%=BASEURL%>/colpapel/remitentes.jsp?sj='+form1.standard.value+'&origen='+form1.origstd.value,'');" class="Simulacion_Hiper" style="cursor:hand ">Modificar Remitentes</a>
                  <input name="origstd" type="hidden" id="origstd2" value="<%=request.getParameter("origstd")%>">
                  <img src=<%=request.getParameter("imagenre")%> width="17" height="17" id="imre">
                  <input name="imagenre" type="hidden" id="imagenre2" value="<%=request.getParameter("imagenre")%>">
                  <input name="imagendest" type="hidden" id="imagendest2" value="<%=request.getParameter("imagendest")%>"> 
              <a style="cursor:hand" onClick="abrirPagina('<%=CONTROLLER%>?estado=Buscar&accion=Destinatarios&sj='+form1.standard.value,'');" class="Simulacion_Hiper">Modificar Destinatarios</a> <img src="<%=request.getParameter("imagendest")%>" width="17" height="17" id="imdest"> <a class="Simulacion_Hiper" onClick="abrirPagina('<%=CONTROLLER%>?estado=Aplicar&accion=Documentos&generar=ok&numrem=<%=request.getParameter("remesa")%>&standard=<%=request.getParameter("standard")%>','')" style="cursor:hand ">Modificar Documentos Relacionados</a><strong> </strong> </span> </td>
            </tr>
            <tr class="fila">
              <th colspan="2" scope="row"><div align="left">Cantidad a Facturar </div></th>
              <td width="30%"><span class="Estilo6"><span class="Estilo8"><span class="Estilo16">
                <input name="cfacturar" type="text" id="cfacturar2" onChange="buscarValorOt();"  onKeyPress="soloDigitos(event,'decNo')" value="<%=cantfac%>" size="12">
                <input name="vrem" type="hidden" id="vrem" readonly value="<%=vlr%>" >
                <input name="vremesa" type="hidden" id="vremesa2" value = "$<%=com.tsp.util.Util.customFormat(Float.parseFloat(vlr)*cantfac)%>" readonly>
              </span></span>              </span></td>
              <td width="17%">Unidad de Facturacion </td>
              <td colspan="2"><strong><%=uw%> </strong> </td><%String fac = "";
	  if(request.getParameter("facturable")!=null){
	  	fac = "checked";
	  }%>
              <td rowspan="2">              
                <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="filaresaltada">
                  <tr>
                    <td><input name="facturable" type="checkbox" id="facturable" value="N" <%=fac%> >
      No Facturable</td>
                  </tr>
                  <tr>
                    <td><input name="cdock" type="checkbox" id="cdock" value="N" <%=request.getParameter("cdock")!=null?"checked":""%>>
      Cross Docking </td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="cadena" value="N" <%=request.getParameter("cadena")!=null?"checked":""%>>
      Cadena</td>
                  </tr>
                </table></td>
            </tr>
            <tr class="fila">
              <th colspan="2" class="Estilo7" scope="row"><div align="left"><span class="Estilo8">Cantidad de Empaque</span></div></th>
              <td><span class="Estilo8"><strong><span class="Estilo16">
                <input name="cantreal" type="text" id="cantreal2" size="12" value="<%=cantreal%>">
              </span></strong> </span> </td>
              <td><strong><span class="Estilo8">Unidad de Empaque</span></strong></td>
              <td colspan="2"><%TreeMap unidades = model.unidadService.getUnidades(); 
	 
	  %><input:select name="unidad" options="<%=unidades%>" attributesText="style='width:100%;'" default='<%=unidad%>' /></td>
            </tr>
          </table>
          <table width="100%" class="tablaInferior">
            <tr>
              <td height="22" class="subtitulo1">Datos Planilla <%=request.getParameter("planilla")%></td>
            </tr>
          </table>
          <table width="100%" border="1" bordercolor="#CCCCCC">
            <tr class="filaresaltada">
              <th height="26" colspan="2" scope="row"><div align="left"><span class="Estilo7">Placa</span></div></th>
              <td colspan="2"><input name="placa" type="text" id="placa" size="12" value="<%=placa%>"  onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');"></td>
              <td>Conductor</td>
              <td colspan="3"><input name="conductor" type="text" id="conductor3" size="12" value="<%=conductor%>" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');">
                <%=request.getAttribute("nombre")%> <a href="<%=CONTROLLER%>?estado=Menu&accion=Conductor" target="_blank">Consultar/Agregar Conductor</a> </td>
            </tr>
            <tr class="filaresaltada">
              <th width="85" rowspan="2" class="Estilo7" scope="row"><div align="left">Trailer</div></th>
              <th width="73" rowspan="2" class="Estilo7" scope="row"><div align="left">
                <input name="trailer" type="text" id="trailer" size="12" value="<%=trailer%>">
</div></th>
              <td><strong>&iquest;Trailer de TSP? </strong></td>
              <td width="86" rowspan="2"><strong>Contenedores</strong></td>
              <td width="93"><input name="c1" type="text" id="c13" value="<%=c1%>" size="15"  onKeyPress="return soloNumText(event);"></td>
              <td width="82" rowspan="2"><strong>
                <input name="tipo_cont" type="radio" value="FINV" <%if(tipocont.equals("FINV")){%>checked<%}%>>
                <span class="Estilo16">FINV</span><br>
                <input name="tipo_cont" type="radio" value="NAV" <%if(tipocont.equals("NAV")){%>checked<%}%>>
                <strong>Naviera</strong>
              </strong></td>
              <td width="116" rowspan="2"><span class="Estilo8"><strong>Precintos de Contedores </strong></span></td>
              <td width="328"><input name="c1precinto" type="text" id="c1precinto3" size="30" onChange="controlPrecintoC('<%=CONTROLLER%>'); cambiarFormulario('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');" value="<%=pc1%>" <%=dis%>></td>
            </tr>
            <tr class="filaresaltada">
              <td height="31"><strong>
                <input name="tipo_tra" type="radio" value="FINV" <%if(request.getParameter("tipo_tra").equals("FINV")){%>checked<%}%>>
Si </strong><br>
<input name="tipo_tra" type="radio" value="NAV" <%if(request.getParameter("tipo_tra").equals("NAV")){%>checked<%}%>>
<strong>No</strong><strong>
                </strong></td>
              <td width="93"><input name="c2" type="text" id="c23" value="<%=c2%>" size="15"  onKeyPress="return soloNumText(event);"></td>
              <td width="328"><input name="c2precinto" type="text" id="c2precinto3" size="30" onChange="controlPrecintoC('<%=CONTROLLER%>');cambiarFormulario('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');" value="<%=pc2%>" <%=dis%>></td>
            </tr>
          </table>
          <table width="100%" class="tablaInferior">
            <tr class="filaresaltada">
              <th width="10%" height="26" scope="row"><div align="left">Ruta Planilla </div></th>
              <th colspan="2" scope="row"><div align="left"><%=rutaP%> </div></th>
              <th width="8%" height="40" scope="row">Cantidad Despachada </th>
              <th width="10%" height="40" scope="row"><%=peso%> <%=unitVlr%></th>
              <th width="13%" height="40" scope="row">Procentaje maximo de anticipo: <%=pa%>
                  <input name="pmax" type="hidden" id="pmax" value="<%=pa.replaceAll("%","")%>"></th>
              <th colspan="3" scope="row"><strong><span class="Estilo8"><strong>Anticipo maximo permitido </strong></span></strong>
                  <div align="left"> <strong> </strong></div></th>
              <th width="9%" scope="row"><strong>
                <input name="antmax" type="text" id="antmax" size="15" readonly value="<%=maxant%>">
              </strong><strong> </strong></th>
            </tr>
            <tr class="filaresaltada">
              <th colspan="2" scope="row"><div align="left"></div>
                  <div align="left"><strong> </strong></div>
                  <span class="Estilo8"><strong><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/colpapel/extrafletes.jsp?sj=<%=request.getParameter("standard")%>&voc='+form1.vocH.value+'&extraflete='+form1.extraflete.value,'Extrafletes','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');" >INGRESAR/ MODIFICAR EXTRAFLETES...</a><strong>
                  <input name="otro" type="hidden" id="otro" value="NO">
                  <strong><strong> </strong></strong> </strong></strong></span></th>
              <th width="20%" scope="row"><span class="Estilo8"><strong><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/colpapel/costosRembolsables.jsp?sj=<%=request.getParameter("standard")%>&voc='+form1.vocH.value+'&cr='+form1.cr.value,'Costos','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');" >INGRESAR/ MODIFICAR COSTOS REEMBOLSABLES</a></strong></span></th>
              <th colspan="2" scope="row"><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('extrafletes.jsp?sj=<%=request.getParameter("standard")%>&voc='+form1.vocH.value+'&extraflete='+form1.extraflete.value,'Extrafletes','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');" ></a><span class="Estilo8"><strong>Valor total en extrafletes</strong></span> </th>
              <th scope="row"><div align="left">
                  <input name="extraflete" type="text" id="extraflete" value="<%=request.getParameter("extraflete")%>" readonly>
                  <strong>
                  <input name="vocH" type="hidden" id="vocH4" value="<%=vlrpla%>">
                  <input name="cr" type="hidden" id="cr" value="<%=request.getParameter("cr")%>">
              </strong></div></th>
              <th colspan="4" scope="row"><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/colpapel/anticipos.jsp','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes')"; ><span class="subtitulo1"><strong>
                </strong></span></a><strong>
                <input name="antprov" type="hidden" id="antprov" value="<%=request.getParameter("antprov")%>">
                </strong><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/colpapel/anticipos.jsp','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes')"; >Aplicar anticipo a proveedor</a></th>
            </tr>
<%			Vector anticipos=model.movplaService.getLista();
if(anticipos!=null){
			if(anticipos.size()>0){%>
            <tr class="filaresaltada">
              <th colspan="10" scope="row"><div align="left"></div>
                  <div align="left">
                    <table width="100%" class="tablaInferior">
                      <tr>
                        <td height="22" class="subtitulo1">Anticipos y/o reanticipos generados<strong>
                          <input name="antprov" type="hidden" id="antprov">
                        </strong></td>
                      </tr>
                    </table>
                </div></th>
            </tr>
            <%
				for(int i=0; i<anticipos.size(); i++){
					Movpla movpla = (Movpla) anticipos.elementAt(i);
					String isImpresa="";
					String textoCheque="";
					String creacion="";
					String cheque="";					
					ant= movpla.getVlr();
					banco = movpla.getBanco();
					if(movpla.isImpresa()){
						isImpresa = " disabled='true'";
						textoCheque="Cheque Impreso";
					}
					
					creacion  = movpla.getCreation_date();
			   		cheque = movpla.getDocument().equals("")?"No impreso":movpla.getDocument();
					String nombre = "beneficiario"+creacion;
					String beneficiario = movpla.getBeneficiario();
					String seleccion = "T";
					if(beneficiario!=null){
						if(beneficiario.equals(nitpro)){
							seleccion = "P";
						}
						else if(beneficiario.equals(conductor)){
							seleccion = "C";
						}
					}
					
			%>
            <tr class="filaresaltada">
              <th colspan="2" scope="row">Valor del Anticipo</th>
              <th scope="row">
                <span class="<%=request.getAttribute("anticipo")%>">
                <input name="anticipo<%=creacion%>" type="text" id="anticipo<%=creacion%>" value="<%=request.getParameter("anticipo"+creacion)!=null?request.getParameter("anticipo"+creacion):String.valueOf(ant)%> " size="12" <%=isImpresa%>  onKeyPress="soloDigitos(event,'decNo')" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');">
                </span>                <input type="hidden" name="moneda<%=i%>" id="moneda<%=i%>" value="">
              <input name="creacion<%=creacion%>" type="hidden" id="creacion<%=creacion%>" value="<%=creacion%>">              </th>
              <th scope="row">Banco</th>
              <th colspan="2" scope="row">
                <%if(isImpresa.equals("")){
                  	TreeMap bancos= model.buService.getListaBancos(usuario.getLogin()); 
					String name ="banco"+creacion;

				 %>
                <input:select name="<%=name%>" options="<%=bancos%>" attributesText="style='width:100%;'"  default='<%=banco%>' />
                <%}else{%>
                <%=banco%>
                <%}%>
              </th>
              <th width="9%" scope="row">Cheque</th>
              <th width="7%" scope="row"><%=cheque%></th>
              <th width="6%" scope="row">Beneficiario </th>
              <th scope="row">
			  <select name="beneficiario<%=creacion%>" id="beneficiario<%=creacion%>" <%=isImpresa.equals("")?"":"disabled"%> >
                <option value="C" <%="C".equals(request.getParameter(nombre)!=null?request.getParameter(nombre):seleccion)?"selected":""%>>Conductor</option>
                <option value="P" <%="P".equals(request.getParameter(nombre)!=null?request.getParameter(nombre):seleccion)?"selected":""%>>Propietario</option>
                <option value="T" <%="T".equals(request.getParameter(nombre)!=null?request.getParameter(nombre):seleccion)?"selected":""%>>Tercero</option>
              </select></th>
            </tr>
            <%}
			}
			}else{%>
			<tr class="filaresaltada">
			  <th colspan="3" scope="row">Valor total del Anticipo entregados por proveedor			  </th>
			  <th colspan="7" scope="row"><div align="left">
                  <input name="anticipo" type="text" id="anticipo" value="<%=request.getParameter("anticipo")%>" size="12"  onKeyPress="soloDigitos(event,'decNo')" readonly >
                  <input type="hidden" name="moneda" id="moneda" value="">
		      </div></th>
		    </tr>
			<%}%>
          </table>
          <table width="100%" class="tablaInferior">
            <tr>
              <td height="22" class="subtitulo1">Otros Precintos </td>
            </tr>
          </table>
          <table width="100%" height="44" border="1" bordercolor="#CCCCCC">
            <tr class="filaresaltada">
              <td width="20%">
			  <%
	  	dis= !pc1.equals("")?"disabled":"";
		
	  %>
                <div align="center">
                  <input name="precintos" type="text" id="precintos" size="20" value="<%=p1%>" <%=dis%> onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');cambiarFormulario('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');">
</div></td>
              <td ><div align="center">
                <input name="precintos2" type="text" id="precintos2" size="20" value="<%=p2%>" <%=dis%> onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');cambiarFormulario('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');">
</div></td>
              <td width="20%">
                <div align="center">
                  <input name="precintos3" type="text" id="precintos3" size="20" value="<%=p3%>" <%=dis%> onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');cambiarFormulario('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');">
</div></td>
              <td width="20%"><div align="center">
                <input name="precintos4" type="text" id="precintos44" size="20" value="<%=p4%>" <%=dis%> onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');cambiarFormulario('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');">
</div></td>
              <td width="20%"><div align="center">
                <input name="precintos5" type="text" id="precintos5" size="20" value="<%=p5%>" <%=dis%> onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');cambiarFormulario('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');">
</div></td>
            </tr>
          </table>
          <%	List listTabla2 = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
			Iterator itTbla2=listTabla2.iterator();
			if(itTbla2.hasNext()){
				%>
          <table width="100%" class="tablaInferior">
            <tr>
              <td height="22" class="subtitulo1">Fechas YYYY-MM-DD HH:MM 24 Hrs </td>
            </tr>
          </table>
          <table width="100%" height="44" border="1" bordercolor="#CCCCCC">
            <tr class="filaresaltada">
              <%		while(itTbla2.hasNext()){
					Tbltiempo tbl = (Tbltiempo) itTbla2.next();
					String id_tabla=tbl.getTimeCode();%>
              <td ><div align="left"><%=tbl.getTimeDescription()%></div></td>
              <td ><input name="<%=id_tabla%>" type="text" id="<%=id_tabla%>2" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this);" size="13" maxlength="16" onChange="formatoFecha(this);" value="<%=request.getParameter(id_tabla)%>"></td>
              <%}%>
            </tr>
          </table>
          <%}%>
          
          
          <table width="100%" class="tablaInferior">
            <tr>
              <td height="22" class="subtitulo1">Observaciones</td>
            </tr>
          </table>
          <table width="100%" height="60" border="1" bordercolor="#CCCCCC">
            <tr class="filaresaltada">
              <th width="50%" height="26" rowspan="2" scope="row"><div align="left">
                <textarea name="observacion" cols="60" id="textarea3"  onKeyPress="return soloNumText(event);"><%=observacion%></textarea>
</div></th>
              <th width="25%" scope="row">Numero del Sticker</th>
              <th width="25%" scope="row"><span class="Estilo8"><strong><strong>
              <strong><strong>
              <input name="sticker" type="text" id="sticker" value="<%=request.getParameter("sticker")%>"  <%=sticker%> onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show');">
              </strong></strong></strong></strong></span></th>
            </tr>
            <tr class="filaresaltada">
              <th colspan="2" scope="row"><span class="Estilo6"><strong><strong><strong><a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=BASEURL%>/colpapel/inventarios.jsp?numpla=<%=request.getParameter("planilla")%>&modif=ok&sj='+form1.standard.value,'');">Despacho mercancia en inventario.</a></strong></strong></strong></span><span class="Estilo6">
              <input name="inventarios" type="hidden" id="inventarios" value="<%=request.getParameter("inventarios")%>" size="80">
              </span></th>
            </tr>
        </table></td>
    </tr>
  </table>
<p align="center">
  <input name="imageField" type="image" src="<%=BASEURL%>/images/botones/modificar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="form1.action=form1.action+'&modif=ok';form1.submit();this.disabled='true'">
  <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </p>
 
</form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>
