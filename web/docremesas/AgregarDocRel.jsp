<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% String destinatario = request.getParameter("destinatario");
String nomdest = request.getParameter("nombre");
model.RemDocSvc.LISTTLBDOC();
List LTipoDoc = model.RemDocSvc.getList();
String lista = model.RemDocSvc.LISTCOMBO();
%>
<html>
<head>
<title>Agregar documentos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/validarDOM.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</script>
</head>
<%
	Vector vec = model.RemDocSvc.getDocumentos();
	int i = 0;
%>
<body <%if(request.getParameter("cerrar")!=null){%>onLoad="parent.close();redimensionar();"<%}%>;onResize="redimensionar();">
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar documentos"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">  
<form method="post" name="formulario" id="formulario" action="<%=CONTROLLER%>?estado=RemesaDocto&accion=Aplicar">
<br>

<table width="95%"  border="2" align="center">
  <tr>
    <td>
      <table width="100%" border="0" class="tablaInferior" align="center" id="tablefather">
        <tr>
		  <td width="60%" height="24"  class="subtitulo1" colspan="2"><p align="left">&nbsp;DOCUMENTOS DE: <%=nomdest%><input type="hidden" name="funcion" onKeyPress="return soloNumText(event)"></p></td>
          <td width="40%"  class="barratitulo" colspan="2"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                <input type="hidden" name="documentosRem" id="documentosRem">
                <input type="hidden" name="Opcion">
                <input name="destinatario" type="hidden" id="destinatario" value="<%=request.getParameter("destinatario")%>">        
        </tr>
        <tr align="center" class="fila">
          <td class="letra_resaltada" width="25%">TIPO</td>
          <td class="letra_resaltada" width="25%">DOCUMENTO</td>
          <td class="letra_resaltada" width="25%">TIPO RELACIONADO </td>
          <td class="letra_resaltada" width="25%">DOCUMENTO RELACIONADO</td>
        </tr>
      </table>
      <%String documento="";
		String tipo_doc ="";
	

%>
      <table width="100%" id="tabla1" align="center" border="0" class="tablaInferior">
       <% for(i =0; i<vec.size();i++){
	 	Vector hijos = new Vector();
		remesa_docto rem = (remesa_docto) vec.elementAt(i);
		//out.print("DESTINATARIO " + destinatario + " ELE " + i + " " + rem.getDestinatario() ); 
		if(rem.getDestinatario().equals(destinatario)){
			documento = rem.getDocumento();
			tipo_doc = rem.getTipo_doc();
			hijos =  rem.getHijos();
		}
		else{
			documento = "";
		}%>
		<tr class="fila" align="center">
          <td width="25%">
		  <select name="tp<%=i%>" id="tp<%=i%>" class="textbox">
              <% Iterator it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   	boolean importacion = false;
	boolean exportacion=false;
	if(rd.getImportacion()!=null)
		importacion=rd.getImportacion().equals("S")?true:false;
		
	if(rd.getExportacion()!=null)
		exportacion=rd.getExportacion().equals("S")?true:false;
		
	if(!importacion && !exportacion){
   %>
              <option value="<%=rd.getDocument_type()%>" <%=rd.getDocument_type().equals(tipo_doc)?"selected":""%>><%=rd.getDocument_name()%></option>
              <% }
		 } %>
            </select>
          </td>
          <td width="25%"><input type="text" name="dp<%=i%>" id="dp<%=i%>" class="textbox" value="<%=documento%>" onKeyPress="return soloNumText(event)">
&nbsp;
        <input type="button" name="enviar3" value="+" class="boton" onClick="agregarFila('<%=lista%>','<%=i%>')"></td>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0" id="tablah<%=i%>">
              <%for(int j = 0; j<hijos.size(); j++){
  		remesa_docto remH = (remesa_docto) hijos.elementAt(j);
  %>
              <tr>
                <td width="50%" height="21"><div align="center">
                    <select name="tp<%=i%>_TipodocRel<%=j%>" class="textbox" id="select">
                      <% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
                      <option value="<%=rd.getDocument_type()%>" <%=rd.getDocument_type().equals(remH.getTipo_doc())?"selected":""%>> <%=rd.getDocument_name()%></option>
                      <% } %>
                    </select>
                </div></td>
                <td width="50%"><div align="center">
                    <input type="text" name="tp<%=i%>_DocumentoRel<%=j%>" id="tp<%=i%>_DocumentoRel<%=j%>" class="textbox" value="<%=remH.getDocumento()%>" onKeyPress="return soloNumText(event)"> 
                </div></td>
              </tr>
              <%}%>
          </table></td>
        </tr>
		<%}%>
      </table>
      
      <table width="100%" id="ultima" align="center" border="0" class="tablaInferior" >
        <tr align="center">
          <td colspan="4" class="fila">
			  <img src="<%=BASEURL%>/images/botones/agregar.gif"  name="imgagregar" onClick="AgregarFilaDOCREL('<%=lista%>','<%=i%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
			  <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="formulario.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
			  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>
</div>
</body>
</html>
