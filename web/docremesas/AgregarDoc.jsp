<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% 
model.RemDocSvc.LISTTLBDOC();
List LTipoDoc = model.RemDocSvc.getList();
String lista = model.RemDocSvc.LISTCOMBO();
%>
<html>
<head>
<title>Agregar documentos</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1;NO-CACHE">

<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/validarDOM.js'></script>

<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body <%if(request.getParameter("cerrar")!=null){%>onLoad="parent.close();redimensionar();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Despacho"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top:100px; overflow: scroll;">  
<form method="post" name="formulario" id="formulario" action="<%=CONTROLLER%>?estado=Aplicar&accion=Documentos">
<br>
<table width="90%"  border="2" align="center">
  <tr>
  
    <td>
	<table width="100%" border="0" class="tablaInferior">
      <tr>
		<td height="24"  class="subtitulo1"><p align="left">&nbsp;Documentos</p>		  </td>
        <td height="24"   class="barratitulo">
		<img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        <input type="hidden" name="documentosRem" id="documentosRem">
        <input type="hidden" name="Opcion">
        <input name="Reload" type="hidden" id="Reload">
      </tr>   
      <tr align="center" class="fila">
        <td width="217" class="letra_resaltada">TIPO DOCUMENTO</td>
        <td class="letra_resaltada">DOCUMENTO</td>
      </tr>
	 <%  Vector vec=model.RemDocSvc.getDocs();
	     int lon = vec.size();
	  for(int i =0; i<vec.size();i++){
	 	remesa_docto rem = (remesa_docto) vec.elementAt(i);
		boolean importacion = rem.getImportacion()!=null?rem.getImportacion().equals("S")?true:false:false;
		%>
		
      <tr class="fila" align="center">
        <td><select name="tipodoc<%=i%>" id="tipodoc<%=i%>" class="textbox">
            <% Iterator it = LTipoDoc.iterator();
			   while (it.hasNext()){ 
			   remesa_docto rd = (remesa_docto) it.next();
   %>
            <option value="<%=rd.getDocument_type()%>/<%=rd.getImportacion()%>/<%=rd.getExportacion()%>" <%=rem.getTipo_doc().equals(rd.getDocument_type())?"selected":""%>><%=rd.getDocument_name()%></option>
            <% } %>
          </select>
        </td>
        <td>
		<table width="100%"  border="0" >
          <tr>
            <td><input name="documento<%=i%>" type="text" class="textbox" id="documento<%=i%>" size="40" value="<%=rem.getDocumento()%>" style="width:100% " onKeyPress="return soloNumText(event)"></td>
          </tr>
		  <%if(importacion){%>
          <tr>
            <td><table width="100%"  border="0">
              <tr class="letraresaltada">
                <td>FECHA SIA</td>
                <td>FECHA ETA </td>
                <td>Descripcion Carga </td>
              </tr>
              <tr>
                <td valign="top"><input name="sia<%=i%>" id="sia<%=i%>" type="text" size="10" value="<%=rem.getFecha_sia()!=null?rem.getFecha_sia():""%>">
                    <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.sia<%=i%>);return false;" hidefocus> <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a></td>
                <td valign="top"><input name="eta<%=i%>" id="eta<%=i%>" type="text" size="10" value="<%=rem.getFecha_eta()!=null?rem.getFecha_eta():""%>">
                    <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.eta<%=i%>);return false;" hidefocus> <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a></td>
                <td><textarea name="descripcion<%=i%>" rows="2" class="textbox" id="textarea4" style="width:100% " onKeyPress="return soloNumText(event)"><%=rem.getDescripcion_cga()!=null?rem.getDescripcion_cga():""%></textarea></td>
              </tr>
            </table></td>
          </tr>
		    <%}%>
        </table></td>
        </tr>
		
	

	  <%}%>
    </table>
	</td>
  </tr>
  </table>
	  <%if(request.getParameter("mensaje")!=null){%>
	  <br>
	  
	  <table border="2" align="center">
        <tr>
          <td>
		  <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
          </table></td>
        </tr>das
      </table>
	  <br>
	  <%}%>
		<table align="center">
          <tr align="center">
            <td colspan="2"> 
			<img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if(validarDocRelacionados(<%=lon%>)){formulario.submit()}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
			</td>
          </tr>
        </table>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
