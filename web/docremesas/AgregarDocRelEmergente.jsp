<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% String destinatario = request.getParameter("destinatario");
String nomdest = request.getParameter("nombre");
String numrem = request.getParameter("numrem");
model.RemDocSvc.LISTTLBDOC();
List LTipoDoc = model.RemDocSvc.getList();
String lista = model.RemDocSvc.LISTCOMBO();
%>
<html>
<head>
<title>Agregar destinatario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilo.css" rel="stylesheet" type="text/css">
<link href="../css/estilo.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/validarDOM.js'></script>
</script>
</head>

<body>
<form method="post" name="formulario" id="formulario" action="<%=CONTROLLER%>?estado=remesa_docto&accion=Manager">
<table width="80%" border="1" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fila" align="center">
<tr class="titulo">
<td align="center" colspan="4"><strong>DOCUMENTOS DE: <%=nomdest%>
    <input type="hidden" name="documentosRem" id="documentosRem">
  <input type="hidden" name="Opcion">
</strong>
</td>
</tr>

<tr align="center" class="fila">
<td class="letra_resaltada" width="50%">TIPO DE DOCUMENTO</td>
<td class="letra_resaltada" width="50%">DOCUMENTO</td>
<td class="letra_resaltada" width="50%">TIPO DE DOCUMENTO RELACIONADO </td>
<td class="letra_resaltada" width="50%">DOCUMENTO RELACIONADO</td>
</tr>
</table>

<table width="80%" id="tabla1" align="center" border="1" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fila">
<tr class="fila" align="center">
<td width="20%"><select name="tipodoc1" id="padre" class="textbox">
<% Iterator it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
	<option value="<%=rd.getDocument_type()%>">[<%=rd.getDocument_type()%>] <%=rd.getDocument_name()%></option>
	<% } %>
	</select>
	
</td>
<td width="20%"><input type="text" name="documento1" id="padre" class="textbox">&nbsp;</td>
<td width="20%"><select name="tipodoc1" class="textbox">
<% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
	<option value="<%=rd.getDocument_type()%>">[<%=rd.getDocument_type()%>] <%=rd.getDocument_name()%></option>
	<% } %>
	</select>
	
</td>
<td width="20%"><input type="text" name="documento1" class="textbox">&nbsp;<input type="button" name="enviar3" value="+" class="boton" onClick="agregarFila('<%=lista%>','1')"></td>


</tr>
</table>

<table width="80%" id="tabla2" align="center" border="1" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fila">
<tr class="fila" align="center">
<td width="20%"><select name="tipodoc2" id="padre" class="textbox">
<% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
	<option value="<%=rd.getDocument_type()%>">[<%=rd.getDocument_type()%>] <%=rd.getDocument_name()%></option>
	<% } %>
	</select>
	
</td>
<td width="20%"><input type="text" name="documento2" id="padre" class="textbox">&nbsp;</td>
<td width="20%"><select name="tipodoc2" id="tipodoc2" class="textbox">
<% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
	<option value="<%=rd.getDocument_type()%>">[<%=rd.getDocument_type()%>] <%=rd.getDocument_name()%></option>
	<% } %>
	</select>
	
</td>
<td width="20%"><input type="text" name="documento2" id="documento2" class="textbox">&nbsp;<input type="button" name="enviar3" value="+" class="boton" onClick="agregarFila('<%=lista%>','2')"></td>

</tr>
</table>

<table width="80%" id="tabla3" align="center" border="1" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fila">
<tr class="fila" align="center">
<td width="20%"><select name="tipodoc3" id="padre" class="textbox">
<% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
	<option value="<%=rd.getDocument_type()%>">[<%=rd.getDocument_type()%>] <%=rd.getDocument_name()%></option>
	<% } %>
	</select>
	
</td>
<td width="20%"><input type="text" name="documento3" id="padre" class="textbox">&nbsp;</td>
<td width="20%"><select name="tipodoc3" id="tipodoc3" class="textbox">
<% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
	<option value="<%=rd.getDocument_type()%>">[<%=rd.getDocument_type()%>] <%=rd.getDocument_name()%></option>
	<% } %>
	</select>
	
</td>
<td width="20%"><input type="text" name="documento3" id="documento3" class="textbox">&nbsp;<input type="button" name="enviar3" value="+" class="boton" onClick="agregarFila('<%=lista%>','3')"></td>

</tr>
</table>

<table width="80%" id="tabla4" align="center" border="1" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fila">
<tr class="fila" align="center">
<td width="20%"><select name="tipodoc4" id="padre" class="textbox">
<% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
	<option value="<%=rd.getDocument_type()%>">[<%=rd.getDocument_type()%>] <%=rd.getDocument_name()%></option>
	<% } %>
	</select>
	
</td>
<td width="20%"><input type="text" name="documento4" id="padre" class="textbox">&nbsp;</td>
<td width="20%"><select name="tipodoc4" id="tipodoc4" class="textbox">
<% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
	<option value="<%=rd.getDocument_type()%>">[<%=rd.getDocument_type()%>] <%=rd.getDocument_name()%></option>
	<% } %>
	</select>
	
</td>
<td width="20%"><input type="text" name="documento4" id="documento4" class="textbox">&nbsp;<input type="button" name="enviar3" value="+" class="boton" onClick="agregarFila('<%=lista%>','4')"></td>

</tr>
</table>

<table width="80%" id="tabla5" align="center" border="1" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fila">
<tr class="fila" align="center">
<td width="20%"><select name="tipodoc5" id="padre" class="textbox">
<% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
	<option value="<%=rd.getDocument_type()%>">[<%=rd.getDocument_type()%>] <%=rd.getDocument_name()%></option>
	<% } %>
	</select>
	
</td>
<td width="20%"><input type="text" name="documento5" id="padre" class="textbox">&nbsp;</td>
<td width="20%"><select name="tipodoc5" id="tipodoc5" class="textbox">
<% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
	<option value="<%=rd.getDocument_type()%>">[<%=rd.getDocument_type()%>] <%=rd.getDocument_name()%></option>
	<% } %>
	</select>
	
</td>
<td width="20%"><input type="text" name="documento5" id="documento5" class="textbox">&nbsp;<input type="button" name="enviar3" value="+" class="boton" onClick="agregarFila('<%=lista%>','5')"></td>
</tr>
</table>
<table width="80%" id="tabla6" align="center" border="1" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fila">
<tr align="center">
  <td colspan="4" class="fila"><input type="button" name="enviar" value="Agregar Mas" class="boton" onClick="AlmacenarInfoRel('<%=destinatario%>')">    
    <input type="button" name="enviar2" value="Guardar Cambios" class="boton" onClick="AlmacenarInfoRel('<%=destinatario%>');Rel('<%=destinatario%>','<%=numrem%>')"></td></tr>
</table>
</form>
</body>
</html>
