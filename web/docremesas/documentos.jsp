<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% 
String numrem = request.getParameter("numrem");
model.RemDocSvc.LISTTLBDOC();
List LTipoDoc = model.RemDocSvc.getList();
%>
<html>
<head>
<title>Agregar documentos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/validarDOM.js'></script>
<script src='<%=BASEURL%>/js/validar.js'></script>
</script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body <%if(request.getParameter("reload")!=null){%>onLoad="window.opener.location.reload();window.close();"<%}%>>
<form method="post" name="formulario" id="formulario" action="<%=CONTROLLER%>?estado=remesa_docto&accion=Manager">
<br>
<table width="80%"  border="2" align="center">
  <tr>
    <td><table width="100%" border="0" class="tablaInferior">
      <tr>
		<td width="190" height="24"  class="subtitulo1"><p align="left">&nbsp;Remesa <%=numrem%></p></td>
        <td width="404"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
		<input type="hidden" name="documentosRem" id="documentosRem">
        <input type="hidden" name="Opcion">
        <input name="Reload" type="hidden" id="Reload">
      </tr>   
      <tr align="center" class="fila">
        <td width="32%" class="letra_resaltada">TIPO DOCUMENTO</td>
        <td width="58%" class="letra_resaltada">DOCUMENTO</td>
      </tr>
      <tr class="fila" align="center">
        <td><select name="tipodoc1" id="tipodoc1" class="textbox">
            <% Iterator it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
            <option value="<%=rd.getDocument_type()%>"><%=rd.getDocument_name()%></option>
            <% } %>
          </select>
        </td>
        <td><input name="documento1" type="text" class="textbox" id="documento1" size="40" onKeyPress="soloAlfa(event);"></td>
      </tr>
      <tr class="fila" align="center">
        <td><select name="tipodoc2" id="tipodoc2" class="textbox">
            <% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
            <option value="<%=rd.getDocument_type()%>"><%=rd.getDocument_name()%></option>
            <% } %>
          </select>
        </td>
        <td><input name="documento2" type="text" class="textbox" id="documento2" size="40" onKeyPress="soloAlfa(event);"></td>
      </tr>
      <tr class="fila" align="center">
        <td><select name="tipodoc3" id="tipodoc3" class="textbox">
            <% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
            <option value="<%=rd.getDocument_type()%>"><%=rd.getDocument_name()%></option>
            <% } %>
          </select>
        </td>
        <td><input name="documento3" type="text" class="textbox" id="documento3" size="40" onKeyPress="soloAlfa(event);"></td>
      </tr>
      <tr class="fila" align="center">
        <td><select name="tipodoc4" id="tipodoc4" class="textbox">
            <% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
            <option value="<%=rd.getDocument_type()%>"><%=rd.getDocument_name()%></option>
            <% } %>
          </select>
        </td>
        <td><input name="documento4" type="text" class="textbox" id="documento4" size="40" onKeyPress="soloAlfa(event);"></td>
      </tr>
      <tr class="fila" align="center">
        <td><select name="tipodoc5" id="tipodoc5" class="textbox">
            <% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
            <option value="<%=rd.getDocument_type()%>"><%=rd.getDocument_name()%></option>
            <% } %>
          </select>
        </td>
        <td><input name="documento5" type="text" class="textbox" id="documento5" size="40" onKeyPress="soloAlfa(event);"></td>
      </tr>
    </table></td>
  </tr>
	</table>
	  <br>
		<table align="center">
          <tr align="center">
            <td colspan="2"> <img src="<%=BASEURL%>/images/botones/agregar.gif"  name="imgagregar" onClick="AlmacenarInfo()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
			<img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="AlmacenarInfo();Mas2('<%=numrem%>')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
          </tr>
        </table>
</form>
</body>
</html>
