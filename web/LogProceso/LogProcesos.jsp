<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*,java.io.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.util.*"%>

<html>
<head>
 <title>Log de Procesos</title>
 
 <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
 <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>
 <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
 <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/reporte.js"></script>
 <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/date-picker.js"></script>
 <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>


<body onLoad="redimensionar();" onResize="redimensionar();">


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Log Procesos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%  String rango1 = (request.getParameter("FechaInicial")==null)?request.getParameter("FechaInicial2"):request.getParameter("FechaInicial"); 
    String rango2 = (request.getParameter("FechaFinal")==null)?request.getParameter("FechaFinal2"):request.getParameter("FechaFinal"); 
    String on     = request.getParameter("Estado");  %>
    
<FORM METHOD='post' name="form1" action='<%= CONTROLLER %>?estado=Log&accion=Proceso' >
    <table border="2" align="center" width="70%" cellpadding='0' cellspacing='0'>
      <tr>
        <td>
        
            <table width="100%" align="center" >
              <tr>
                <td width="48%" height="22"  class="subtitulo1"><p align="left"><span class="titulo"><b>CRITERIO DE BUSQUEDA</b></span></p></td>
                <td width="52%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
            </table>
            
            <table  width='100%' align="center"   cellpadding='0' cellspacing='0'>
                    <tr class="fila">
                      <td width="9%" height="27" nowrap align='center' style="font size:12; font weight: bold">Estado: </td>
                      <td width="15%" nowrap>
                        <select name="Estado" class="listmenu" id="Estado" style="width:90%">
                              <option value="TODO"      >Todos</option>
                              <option value="ACTIVO"    >Activos</option>
                              <option value="FINALIZADO">Finalizados</option>
                              <option value="ANULADO"   >Anulados</option>
                        </select>
                      </td>
                      
                      <td width="13%" nowrap align='center' style="font size:12; font weight: bold">Fecha Inicial :</td>
                      <td width="18%" nowrap align='center'>
                         <input type="text"  size='12'readonly='true' class='textbox' name="FechaInicial" value="<%=rango1%>">
                      <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.FechaInicial);return false;" HIDEFOCUS></td> <!--onClick="javascript:show_calendar('FechaInicial')"-->         
                    
                      <td width="13%" nowrap align='center' style="font size:12; font weight: bold">Fecha Final :</td>
                      <td width="19%" nowrap align='center'>
                         <input type="text" size='12' readonly='true' name="FechaFinal" class='textbox' value="<%=rango2%>">
                         <img src="<%=BASEURL%>/images/cal.gif" width="16"  height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.FechaFinal);return false;" HIDEFOCUS> </td> <!-- onClick="javascript:show_calendar('FechaFinal')-->
                   
                     <td width="13%" ><img src="<%=BASEURL%>/images/botones/buscar.gif" title='Realizar busqueda' name="mod"  height="21" align="absmiddle" style="cursor:hand"  onClick="validarBusqueda(form1);"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                     <input type="hidden" name="Estado2"   value="<%=on%>"></td></tr>
                </table>
         </td>
       </tr>
    </table>
</FORM>
           
    
<% List listaProc= model.LogProcesosSvc.getList();
   if(listaProc!=null && listaProc.size()>0 ){%>   
       <FORM name="form2"  action='<%= CONTROLLER %>?estado=Log&accion=Proceso' method='post'>
          <table border="2" align="center" width="100%">
             <tr>
                  <td>
                        <table width="100%" align="center">
                          <tr>
                            <td width="48%" height="22"  class="subtitulo1">LISTADO DE PROCESOS : &nbsp 
                                            <%=on%>S 
                                            <%  if( rango1!=null && rango2!=null && !rango1.equals("") && !rango2.equals("")  && !rango1.equals("null") && !rango2.equals("null")  ){
                                                   out.print("  "+ rango1+ "  "+ rango2); 
                                              }%>
                            </td>
                            <td width="52%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                          </tr>
                        </table>        
                        
                       <TABLE   border='1' width='100%' align="center" bordercolor="#999999" bgcolor="#F7F5F4" >
                                    
                                       <TR class="tblTitulo">
                                            <TH width='1%'><input type='checkbox' id='All' onclick='javascript: Sell_all(this.form,this)'></TH>
                                            <TH width='5%'   style="font size:9; font weight: bold"> ESTADO             </TH>
                                            <TH width='5%'   style="font size:9; font weight: bold"> ID                 </TH>
                                            <TH width='13%'  style="font size:9; font weight: bold"> PROCESO            </TH>
                                            <TH width='25%'  style="font size:9; font weight: bold"> DESCRIPCION        </TH>
                                            <TH width='8%'   style="font size:9; font weight: bold"> USUARIO            </TH>                            
                                            <TH width='10%'  style="font size:9; font weight: bold"> FECHA INICIAL      </TH>
                                            <TH width='10%'  style="font size:9; font weight: bold"> FECHA FINAL        </TH>
                                            <TH width='10%'  style="font size:9; font weight: bold"> DURACION           </TH>
                                            <TH width='*'    style="font size:9; font weight: bold"> COMENTARIO         </TH>
                                      </TR>
                                      
                                    <% Iterator it= listaProc.iterator();
                                       while(it.hasNext()){
                                         try{
                                                 LogProceso proceso          = (LogProceso)it.next();
                                                 String     tipoEstado       = proceso.getEstado();
                                                 String     estadoFinalizado = (proceso.getEstadoFinalizado()==null)?"":proceso.getEstadoFinalizado();%>
                                             
                                                 <TR valign='top'  class="<%= (!tipoEstado.equals("ACTIVO"))?"letra":"letraTitulo"%>"   bgcolor='<%= proceso.getColor() %>'  style="cursor:hand;" onMouseOver='cambiarColorMouse(this)'  >
                                                         <TD width='1%'>
                                                             <%if(tipoEstado.toUpperCase().equals("ACTIVO")){ %>                    
                                                                <input type='checkbox'    name='proceso'  id='<%= proceso.getId() %>'  value='<%= proceso.getId() %>'     onclick='javascript: Sell_all(this.form,this)'>                       
                                                             <%}%>                        
                                                         </TD>
                                                         <TD width='5%'  class='bordereporte' align="center" ><font size='1'>   <%= proceso.getEstado()       %></TD>
                                                         <TD width='5%'  class='bordereporte' align="center" ><font size='1'>   <%= proceso.getId()           %></TD>
                                                         <TD width='13%' class='bordereporte'                ><font size='1'>   <%= proceso.getProceso()      %></TD>
                                                         <TD width='25%' class='bordereporte'                ><font size='1'>   <%= proceso.getDescripcion()  %></TD>
                                                         <TD width='8%'  class='bordereporte' align="center" ><font size='1'>   <%= proceso.getUsuario()      %></TD>
                                                         <TD width='10%' class='bordereporte' align="center" ><font size='1'>   <%= proceso.getFechaInicial() %></TD>
                                                         <TD width='10%' class='bordereporte' align="center" ><font size='1'>   <%= proceso.getFechaFinal()   %></TD>
                                                         <TD width='10%' class='bordereporte' align="center" ><font size='1'>   <%= proceso.getDuracion()     %></TD>
                                                         <TD width='*'   class='bordereporte'                ><font size='1'>   <%= estadoFinalizado          %></TD>
                                               </TR>
                                       <%}
                                        catch(Exception e){}
                                      } %>                  
                      </TABLE>
        </td>
    </tr>
  </table>
  
  
  <BR><BR>                  
      
       <input type="hidden" name="evento"        >
       <input type="hidden" name="Estado"        value="<%=  on     %>">
       <input type="hidden" name="estado"        value="<%=  request.getParameter("Estado")     %>">
       <input type="hidden" name="FechaInicial2" value="<%=  rango1 %>">
       <input type="hidden" name="FechaFinal2"   value="<%=  rango2 %>">

       <table align='center' width='409'>
          <tr>                         
             <td height="27" align='center'>                         
                 <img src="<%=BASEURL%>/images/botones/restablecer.gif" name="mod" title='Actualiza vista'  height="21"  onclick="validarSinCheckbox(form2,'<%= on %>')"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
                 <img src="<%=BASEURL%>/images/botones/anular.gif"      name="mod" title='Anular procesos'  height="21"  onclick="validarCantCheckbox(form2,'<%= on %>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
                 <img src="<%=BASEURL%>/images/botones/salir.gif"       name="mod" title='Salir'            height="21"  onclick="window.close();"                         onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
             </td>
          </tr>
       </table>

</FORM>  


<%}else{%>
        <table width="70%"  border="2" align="center">
             <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="550"  align="left" class="mensajes"><div align="center"><span class="comentario">No se encontrar&oacute;n registros de procesos &nbsp <%= on %>S
                        <%  if( rango1!=null && rango2!=null && !rango1.equals("") && !rango2.equals("") && !rango1.equals("null") && !rango2.equals("null")  )
                                   out.print("  entre ["+ rango1+ " y "+ rango2 +" ]"); %>
            , si lo desea puede realizar consultas sobre otro tipo de procesos y/o otro rango de fecha </span></div></td>
                <td  width="29"background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="85">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
<%}%>


</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
<script>
   function validarBusqueda(formulario){
		
        if(  (formulario.FechaInicial.value!="") || (formulario.FechaFinal.value!="")  ){
              if(  (formulario.FechaInicial.value=="") || (formulario.FechaFinal.value=="")  ){  
                   alert('Si desea seleccionar por rango de fecha, deber� escoger las dos fechas...');
                   formulario.FechaInicial.focus();
                   return false;
              }           
	      var ano_ini=parseInt(formulario.FechaInicial.value.substr(0,4));
              var mes_ini=parseInt(formulario.FechaInicial.value.substr(5,2));
              var dia_ini=parseInt(formulario.FechaInicial.value.substr(8,3));
	      var ano_fin=parseInt(formulario.FechaFinal.value.substr(0,4));          
	      var mes_fin=parseInt(formulario.FechaFinal.value.substr(5,2));
	      var dia_fin=parseInt(formulario.FechaFinal.value.substr(8,2));
              if(ano_fin<ano_ini){
                alert('El A�o Final deber� ser Mayor o Igual  que el A�o Inicial');
                formulario.FechaFinal.focus();
                return false;
              }
              if(ano_fin==ano_ini && mes_fin<mes_ini){
                alert('El Mes Final deber� ser Mayor  o Igual que el Mes Inicial');
                formulario.FechaFinal.focus();
                return false;
              }
              if(ano_fin==ano_ini &&  mes_fin==mes_ini && dia_fin<dia_ini){
                alert('El Dia de la Fecha Final deber� ser Mayor o Igual que el dia de la Fecha  Inicial');
                formulario.FechaFinal.focus();
                return false;
              }	   
        }
        formulario.submit();
     }
</script>