<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*,com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>


<html>
<head>
   <title>Impresión de Cheques</title>
   <link href="<%=BASEURL%>/css/Letras.css" rel='stylesheet'>
      <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>  
      <style type="text/css">
<!--
.Estilo1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
      </style>
</head>
<body>
 <form name="form" method="post" action="<%=CONTROLLER%>?estado=Cheque&accion=Anular&anular=ok">
 <%
 	Movpla ch = model.movplaService.getMovPla();
 %>
<TABLE width='72%' border='1' align="center" cellpadding='2' cellspacing='1' bordercolor="#CCCCCC" bgcolor="ECE0D8"  class='Estilo1'>
                <TR bgcolor="#FFA928">
                  <TH height='50' colspan='4' align='center' class='letras Estilo1'>ANULAR CHEQUE IMPRESO</TH>
    </TR>
                <TR class='Letras'>
                     <TH width='16%' align='center' > <div align="left"><strong>NUMERO</strong></div></TH>
                     <TH width='33%' align='center'><%=ch.getDocument()%>
                       <input name="cheque" type="hidden" id="cheque" value="<%=ch.getDocument()%>"> </TH>
                     <TH width='17%' align='center'> VALOR         </TH>
                     <TH width='34%' align='center' >  <%=Util.customFormat(ch.getVlr())%><%=ch.getCurrency()%> </TH>
                </TR>
                <TR class='Letras'> 
                     <TD align='center' ><div align="left"><strong>BENEFICIARIO </strong></div></TD>
                  <TD align='left'   ><%=ch.getPla_owner()%></TD>
                       <TD align='right'  ><div align="center"><strong> BANCO 
                  </strong></div></TD>
                       <TD align='center' ><%=ch.getBanco()%> </TD>                 
    </TR>
                <TR class='Letras'>
                  <TD align='center' ><div align="left"><strong>AGENCIA</strong></div></TD>
                  <TD align='left'   ><%=ch.getAgency_id()%></TD>
                  <TD align='right'  ><div align="center"><strong>USUARIO</strong></div></TD>
                  <TD align='center' ><%=ch.getCreation_user()%></TD>
                </TR>
                <TR class='Letras'>
                  <TD colspan="4" align='center' ><strong>INFORMACION DE LA ANULACION </strong></TD>
                </TR>
                <TR class='Letras'>
                  <TD align='center' ><div align="left"><strong>CAUSA</strong></div></TD>
                  <TD align='left'   ><%TreeMap causas= model.causas_anulacionService.getTcausasa(); %>
          <input:select name="causas" options="<%=causas%>" attributesText="style='width:100%;'" /></TD>
                  <TD align='right'  ><div align="left"><strong>TIPO DE RECUPERACION </strong></div></TD>
                  <TD align='center' ><%TreeMap trecuperecion= model.trecuperacionaService.getTreeTRA(); %>
          <input:select name="trecuperacion" options="<%=trecuperecion%>" attributesText="style='width:100%;'" /></TD>
                </TR>
                <TR class='Letras'>
                  <TD height="20" align='center' ><div align="left"><strong>OBSERVACION</strong></div></TD>
                  <TD colspan="3" align='left'   ><textarea name="observacion" id="observacion" style="width:100% "></textarea></TD>
                </TR>
</TABLE>
<div align="center"><br>
  <input name="Submit" type="submit" value="Anular">
 </div>
 </form>
</body>
</html>
