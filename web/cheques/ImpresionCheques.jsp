<!--
- Autor : FERNEL VILLACOB DIAZ
- Date  : 23 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite la impresion de cheques
--%>

<%-- Declaracion de librerias--%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*"%>


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<title>Impresi�n de Cheques de Anticipo</title>

<html>
<head>

	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>  
        
</head>
<body onLoad="redimensionar();" onResize="redimensionar();" onKeyDown="onKeyDown();" >

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Imprimir Cheques de Anticipos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


<%    int             swWindow      = Integer.parseInt(request.getParameter("ventana"));
      int             total         = Integer.parseInt(request.getParameter("total"));
      ImpresionCheque chequePrint   = (ImpresionCheque)request.getAttribute("chequePrint"); 
      ImpresionCheque chequeToPrint = (ImpresionCheque)request.getAttribute("chequeToImprimir"); 
      int             id            = (chequePrint!=null)?chequePrint.getId():0;
      List            listaCheque   = model.ImprimirChequeSvc.getCheques();
      String          msj           = request.getParameter("comentario");


      if(listaCheque!=null && listaCheque.size()>0){
            Iterator it = listaCheque.iterator(); %>     
            <table width="1000" border="2" align="center">
                    <tr>
                      <td>
                      
                               <table width="100%" align="center">
                                          <tr>
                                                <td width="50%" class="subtitulo1">&nbsp;LISTA DE CHEQUES</td>
                                                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                          </tr>
                                </table>
                                
                                <TABLE width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                  <TR class="tblTitulo" align="center">
                                          <TH align='center' width='3%' > No </TH>
                                          <TH align='center' width='3%' > <img src='<%=BASEURL%>\images\imprimir.gif'>  </TH>
                                          <TH align='center' width='17%'> PLANILLA / VALOR     </TH>
                                          <TH align='center' width='20%'> BENEFICIARIO         </TH>
                                          <TH align='center' width='12%'> VLR CHEQUE           </TH>
                                          <TH align='center' width='7%' > MONEDA               </TH>
                                          <TH align='center' width='21%'> BANCO                </TH>
                                          <TH align='center' width='5%' TITLE='Definir si el cheque se imprime por L�ser' > L�SER           </TH>
                                          <TH align='center' width='9%' > USUARIO              </TH>
                                          <TH align='center' width='7%' > AGENCIA              </TH>                                          
                                          <TH align='center' width='*' >&nbsp;                 </TH>
                                          
                                  </TR>
                         <% int cont=0;
                            int sw=0,i=0;
                            while(it.hasNext()){
                                    ImpresionCheque cheque = (ImpresionCheque)it.next();  
                                    cont++;
                                    String op  = "op" +String.valueOf(cont);
                                    String op1 = "opa"+String.valueOf(cont);
                                    String op2 = "opb"+String.valueOf(cont);
                                    String op3 = "opc"+String.valueOf(cont);
                                    String op4 = "opd"+String.valueOf(cont);%>
                                     <form name='formulario_<%=cont%>' id='formulario_<%=cont%>' method='POST'>
                                     
                                         <TR class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">

                                                <TD nowrap align='center' class="bordereporte"> <%= cont %><input type='hidden' name='idCheque' value='<%= cheque.getId()%>'></TD>
                                                <TD nowrap align='center' class="bordereporte"> <input type='checkbox'   name='Impresa' <%= cheque.isActivado() == false ?"disabled":"" %> > </TD>
                                                <TD nowrap align='left'   class="bordereporte">
                                                   <table width='100%' cellpadding='0' cellspacing='0'>
                                                     <% String[] vec     = cheque.getPlanilla().split("<BR>");
                                                        String[] vecVlr  = cheque.getValores().split("<BR>");
                                                        for(int j=0;j<vec.length;j++){
                                                            String oc    = vec[j];
                                                            double valor = Double.parseDouble( vecVlr[j]) ;%>
                                                            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" style="font size:11px">
                                                                 <td width='50%' nowrap > <%= oc %> </td><td nowrap align='right'  width='50%'> <%= Util.customFormat(valor) %>  </td>
                                                            </tr>
                                                       <%}%>
                                                  </table>
                                                </TD>
                                                <TD nowrap align='left'   class="bordereporte" title='PROPIETARIO: <%= cheque.getPropietario() + " - " + cheque.getNombre_propietario() %>'><%= cheque.getBeneficiario() %>            </TD>
                                                <TD nowrap align='right'  class="bordereporte"><%= Util.customFormat(cheque.getValor()) %></TD>
                                                <TD nowrap align='center' class="bordereporte"><%= cheque.getMoneda() %>                  </TD>                 
                                                <TD nowrap align='center' class="bordereporte"><%= cheque.getBanco()   %>/<%= cheque.getAgenciaBanco()%><input name="banco" type="hidden" id="banco" value="<%=cheque.getBanco()%>/<%= cheque.getAgenciaBanco()%>"></TD>
                                                <input type='hidden' name='flag_impresion' value='<%= cheque.isEstado_impresion_oc()%>'>
                                                <input type='hidden' name='planilla' value='<%= cheque.getPlanilla()%>'>
                                                <input type='hidden' name='aux' value='<%= cheque.getAuxiliar()%>'>
                                                <TD nowrap align='center' class="bordereporte">

                                                     <select name='laser'>
                                                         <option value='N'> NO </option>
                                                         <option value='S'> SI </option>
                                                     </select>

                                                </TD>

                                                <TD  align='center' class="bordereporte"><%= cheque.getNombreUsurio()%>             </TD>
                                                <TD  align='center' class="bordereporte"><%= cheque.getNombreAgencia()%>            </TD> 
                                                <TD  align='center' class="bordereporte"><a class="Simulacion_Hiper" href="<%=BASEURL%>/cheques/cambiarBanco.jsp?idCheque=<%=cheque.getId()%>&planilla=<%=cheque.getPlanilla()%>">Cambiar Banco</a></TD>
                                        </TR>
                                    </form>
                       <%i++;}%>
                       
                    </TABLE>
                   </tr>
                </td>
            </table>
                    
            <p align="center">
               <img src="<%=BASEURL%>/images/botones/imprimir.gif" style="cursor:hand" name="Imprimir"  title="Imprimir"                  onclick='javascript:recorrer()' onMouseOver="botonOver(this);"   onMouseOut="botonOut(this);">&nbsp;	 	
               <img src="<%=BASEURL%>/images/botones/salir.gif"    style="cursor:hand" name="salir"     title="Salir al Menu Principal"   onClick="parent.close();"       onMouseOver="botonOver(this);"   onMouseOut="botonOut(this);" >
            </p>

       <%}%>
   
    
    
    
	<!--  Mensaje de Informaci�n-->
        <% if( msj!=null && !msj.equals("") ){ %>		      
             <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="450" align="center" class="mensajes"><%=msj%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table>
            
            
           <% if(listaCheque==null || listaCheque.size()==0){%>
                <p align="center">
                   <img src="<%=BASEURL%>/images/botones/salir.gif"    style="cursor:hand" name="salir"     title="Salir al Menu Principal" onMouseOver="botonOver(this);"  onClick="parent.close();"      onMouseOut="botonOut(this);" >
                </p>
           <%}%>


        <%}%>

        
        
      <!-- IMPRIMIMOS EL CHEQUE EN JS -->
       <%if(chequeToPrint!=null){
             String usuario = request.getParameter("usuario");%>
             <script>
                 //var win = window.open('<%=BASEURL%>/cheques/ChequePrint.jsp?usuario=<%=usuario%>','impresion',' top=1,left=1, width=1, height=1, resizable=no');
                 var win = window.open('<%=BASEURL%>/cheques/ChequePrint.jsp?usuario=<%=usuario%>','impresion',' top=2000,left=1, width=1, height=1, resizable=yes');
             </script>
       	<%}%>

     	
	<!--  MOSTRAMOS LA VENTANA DE CONFIRMACION EN JS -->     
       <%if(swWindow==1 && chequePrint!=null){
              String cadena= chequePrint.getCheque() + "|"+ chequePrint.getBeneficiario()+"|"+ Util.customFormat(chequePrint.getValor()) +"|"+ chequePrint.getMoneda()  +"|" + chequePrint.getBanco()+"|"+chequePrint.getCuenta()+"|"+chequePrint.getAgenciaBanco() +"|"+ chequePrint.getLaser() +"|" + chequePrint.getCopias()  ;%> 
              <script> windowPrint(window,'<%= chequePrint.getId()%>','<%= cadena %>','<%= total %>');</script> 
        <%}%> 
     	
   
</div>
<%=datos[1]%>  
</body>
</html>
