<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*"%>


<html>
<head>
   <title>Impresión de Cheques</title>
   <link href="<%= BASEURL %>/css/Style.css" rel='stylesheet'>
   <link href="../css/Style.css" rel='stylesheet'>
   <link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>
	<link href="../css/estilo.css" rel="stylesheet" type="text/css">

   <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>  
</head>
<body>
<div align="center">
<BR>
<% 
	int swWindow = Integer.parseInt(request.getParameter("ventana"));
	int total    = Integer.parseInt(request.getParameter("total"));
	ImpresionCheque chequePrint   = (ImpresionCheque)request.getAttribute("chequePrint"); 
	ImpresionCheque chequeToPrint = (ImpresionCheque)request.getAttribute("chequeToImprimir"); 
	int  id = (chequePrint!=null)?chequePrint.getId():0;
	if(request.getParameter("comentario").equals("")){
		List listaCheque = model.ImprimirChequeSvc.getCheques();
                
		if(listaCheque!=null && listaCheque.size()>0){        
    		Iterator it = listaCheque.iterator(); %>     
			        
                <TABLE width='100%' border='1' cellpadding='2' cellspacing='1' bordercolor="#CCCCCC"  class='letra'>
        	      <TR> <TH class='titulo' colspan='10' align='center' height='50'>LISTADO DE CHEQUES</TH></TR>
            	      <TR class='subtitulos'>
            		  <TH align='center' width='3%' > No  </TH>
                	  <TH align='center' width='4%' > <img src='<%=BASEURL%>\images\imprimir.gif'>  </TH>
                	  <TH align='center' width='5%' >PLANILLA       </TH>
                	  <TH align='center' width='22%'>BENEFICIARIO   </TH>
                	  <TH align='center' width='12%'> VALOR         </TH>
	                  <TH align='center' width='8%' > MONEDA        </TH>
    	                  <TH align='center' width='22%'> INFORMACION DE BANCO         </TH>
        	          <TH align='center' width='10%'>USUARIO        </TH>
            	          <TH align='center' width='8%'>AGENCIA         </TH>
                	  <TH align='center' width='6%'>&nbsp;          </TH>
	              </TR>
        	<%
        	int cont=0;
	        int sw=0;
    	    while(it.hasNext()){
	            ImpresionCheque cheque = (ImpresionCheque)it.next();
	            cont++;
    	            String op  = "op" +String.valueOf(cont);
        	    String op1 = "opa"+String.valueOf(cont);
	            String op2 = "opb"+String.valueOf(cont);
    	            String op3 = "opc"+String.valueOf(cont);
        	    String op4 = "opd"+String.valueOf(cont);%>
                 <form name='formulario_<%=cont%>' id='formulario_<%=cont%>' method='POST'>
    	         <TR class='fila'> 
			<TD align='center' > <%= cont %><input type='hidden' name='idCheque' value='<%= cheque.getId()%>'></TD>
	                <TD align='center' > <input type='checkbox'   name='Impresa'> </TD>
        	        <TD align='left'   ><%=cheque.getPlanilla()      %></TD>
	                <TD align='left'   ><%= cheque.getBeneficiario() %></TD>
	                <TD align='right'  ><%=Util.customFormat(cheque.getValor()) %></TD>
	                <TD align='center' ><%= cheque.getMoneda() %></TD>                 
	                <TD align='center' ><%=cheque.getBanco()   %><input name="banco" type="hidden" id="banco" value="<%=cheque.getBanco()%>"></TD>
	                <TD align='center' ><%= cheque.getUsuario()%></TD>
    	                <TD align='center' ><%= cheque.getAgencia()%></TD>
                	<TD align='center' ><a href="<%=BASEURL%>/cheques/cambiarBanco.jsp?planilla=<%=cheque.getPlanilla()%>">Cambiar Banco</a></TD>
		   </TR>
		   </form>
	       <%}%>
               </TABLE>
	        <input type='button'  onclick='javascript:recorrer()' value='Imprimir' style='width=100'>	 	
		<br>
             
	<%
		}
    }
    else{%>
	<!--  Mensaje de Información-->
        <TABLE class='fondotabla'  width='500'>
    	<TR><TH CLASS='titulo'>INFORMACIÓN</TH>
	</TR>
            <TR>
               <TD align='center' class='comentario2'>
                  <span class="fila"><br>
                  <br>
                  <%= request.getParameter("comentario")  %> <br>
                  <br>
                  </span>                  <br><br>              </TD>
           </TR>
        </TABLE>
        </div>
     <%}%>

     
     
     
      <!-- IMPRIMIMOS EL CHEQUE EN JS -->
      
      
       <%if(chequeToPrint!=null){  
             String usuario = request.getParameter("usuario");
	         chequeToPrint.formatEsquema();
    	     String cuerpoEsquema = chequeToPrint.getCampos();
             String posEsquema    = chequeToPrint.getPosiciones();
             String cuerpoPrint   = chequeToPrint.getAno()+"-"+ chequeToPrint.getMes()+"-"+ chequeToPrint.getDia()+"-"+ chequeToPrint.getBanco()+"-"+ chequeToPrint.getCheque()+"-"+ chequeToPrint.getBeneficiario()+"-" +chequeToPrint.getPlanilla()+"-"+ Util.customFormat(chequeToPrint.getValor())+"-"+ chequeToPrint.getCedula(); %>
	     <script>PrintCheque('<%=cuerpoPrint%>','<%=cuerpoEsquema %>','<%=posEsquema%>','<%= usuario %>',<%=Util.getFechaActual_String(1)%>,<%=Util.getFechaActual_String(2)%>,<%=Util.getFechaActual_String(3)%>);</script>
     	<%}%>
     	
	<!--  MOSTRAMOS LA VENTANA DE CONFIRMACION EN JS -->
     
           <%if(swWindow==1 && chequePrint!=null){
      		  String cadena= chequePrint.getCheque() + "-"+ chequePrint.getBeneficiario()+"-"+ chequePrint.getValor() +"-"+ chequePrint.getMoneda()  +"-" + chequePrint.getBanco()+"-"+chequePrint.getCuenta()+"-"+chequePrint.getAgenciaBanco();%> 
	     	  <script> windowPrint(window,'<%= chequePrint.getId()%>','<%= cadena %>','<%= total %>');</script> 
     	    <%}%> 
     	
     

</body>
</html>
