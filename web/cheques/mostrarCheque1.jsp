<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*,com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>


<html>
<head>
   <title>Impresión de Cheques</title>
   <link href="<%=BASEURL%>/css/Letras.css" rel='stylesheet'>
      <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>  
      <style type="text/css">
<!--
.Estilo1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
      </style>
</head>
<body>
 <form name="form" method="post" action="<%=CONTROLLER%>?estado=Cheque&accion=Buscar&anular=ok">
 <%
 	Movpla ch = model.movplaService.getMovPla();
 %>
<TABLE width='100%' border='1' cellpadding='2' cellspacing='1' bordercolor="#CCCCCC" bgcolor="ECE0D8"  class='Estilo1'>
                <TR bgcolor="#FFA928">
                  <TH height='50' colspan='7' align='center' class='letras Estilo1'>ANULAR CHEQUE</TH>
    </TR>
                <TR bgcolor="#99CCFF" class='Letras'>
                     <TH width='4%' align='center' > No</TH>
                     <TH width='17%' align='center'> BENEFICIARIO  </TH>
                     <TH width='14%' align='center'> VALOR         </TH>
                     <TH width='13%' align='center' > MONEDA        </TH>
                     <TH width='32%' align='center'> INFORMACION DE BANCO         </TH>
                     <TH width='12%' align='center'>USUARIO</TH>
                     <TH width='8%' align='center'>AGENCIA</TH>
                </TR>
                <TR class='Letras'> 
                     <TD align='center' ><%=ch.getDocument()%>
                     <input name="cheque" type="hidden" id="cheque" value="<%=ch.getDocument()%>"></TD>
                       <TD align='left'   ><%=ch.getPla_owner()%>
                  </TD>
                       <TD align='right'  >$ <%=Util.customFormat(ch.getVlr())%>
                  </TD>
                       <TD align='center' >     <%=ch.getCurrency()%>
                  </TD>                 
                       <TD align='center' ><%=ch.getBanco()%>
                          
                       </TD>
                       <TD align='center' ><%=ch.getCreation_user()%></TD>
                       <TD align='center' ><%=ch.getAgency_id()%></TD>
    </TR>
</TABLE>
<div align="center"><br>
  <input name="Submit" type="submit" value="Anular">
 </div>
 </form>
</body>
</html>
