<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Impresion de Planillas</title>
<link href="../css/letras.css" rel="stylesheet" type="text/css">
</head>
<link  href='<%=BASEURL%>/css/Style.css' rel='stylesheet'>
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
<body>
<% 
  String estado =request.getParameter("estado"); 
  String men = "";
  if ( estado.equals("ok") ){
     men = "Cheque  anulado satisfactoriamente";
  }
  if ( estado.equals("anu") ){
     men = "Cheque anulado anteriormente ";
  }
  if ( estado.equals("no") ){
     men = "Cheque no existe ";
  }
%>
<center>
<form action="<%=CONTROLLER%>?estado=Cheque&accion=Anular" method="post" class="msg">
<P>
<TABLE width='300' height="32%" border='1' cellpadding="2" cellspacing="1" bordercolor="#999999" class="fondotabla">
    <tr>
        <th height="14%" colspan="2" class="titulo1">ANULAR CHEQUE </th>
    </tr>
    <tr >
    <td height="55%" class="comentario"><div align="center"><br>
            <strong> NUMERO </strong>&nbsp;&nbsp;
            <input   name='cheque' type='text' id="cheque"   style="width:120">
        </div></td>
    </tr>
    <tr >
      <td height="17%" class="comentario"><div align="center">
        <input type='submit' value='Buscar'  class="boton" style="width:120" name="Opcion" >
      </div></td>
    </tr>
	
</TABLE>
</P>
<p class="msg"><%=men%><BR>
    <input type='hidden' name='Origen' value='FBuscar'>
</p>
</form>
<br>
</center>
</body>
</html>
