<!--
- Autor : Ing. FERNEL VILLACOB DIAZ
- Date  : 10/02/2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite la impresion de cheques por lotes
--%>


<%-- Declaracion de librerias--%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*"%>

<html>
<head>
   <title>Listado de Cheques a Imprimir</title>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
   <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>  
   <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
   <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>  
</head>
<body>
<center>


 <%  String   msj    =  model.ImprimirChequeSvc.getMSJ(); %>

   <table width="95%" border="2" align="center">
        <tr>
            <td>
               <table width="100%" align="center">
                          <tr>
                                <td width="50%" class="subtitulo1">&nbsp;Listados de Cheques a Imprimir</td>
                                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                          </tr>
                </table>
                
                <TABLE width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                
                
                  <TR class='fila' >
                      <TD colspan='6'> BANCO : <%= model.ImprimirChequeSvc.getBanco() %>    </TD> 
                 </TR>  
                
                  <TR class="tblTitulo" align="center">
                          <TH  width='10%'> Cheque         </TH>
                          <TH  width='35%'> Benficiario    </TH>                          
                          <TH  width='18%'> Banco          </TH>
                          <TH  width='20%'> Sucursal       </TH>
                          <TH  width='*'  > Valor          </TH>
                  </TR> 
                   <%  List  listaImpresion = model.ImprimirChequeSvc.getListPrintView();
                       for(int i=0;i<listaImpresion.size();i++){
                             ImpresionCheque cheque = (ImpresionCheque) listaImpresion.get(i);%>
                              <TR class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"  onMouseOver='cambiarColorMouse(this)'  >
                                      <TD >  <%= cheque.getCheque() %>          </TD>
                                      <TD >  <%= cheque.getBeneficiario() %>    </TD>                          
                                      <TD >  <%= cheque.getBanco() %>           </TD>
                                      <TD >  <%= cheque.getAgenciaBanco() %>    </TD>
                                      <TD align='right' >  <%= Util.customFormat(cheque.getValor()) %> </TD>
                              </TR>  
                     <% } %>
              </TABLE>
            </td>
        </tr>
    </table>
    
   
     <p align="center">
        <img src="<%=BASEURL%>/images/botones/imprimir.gif"   style="cursor:hand" name="Imprimir"  title="Imprimir cheques"         onclick="NuevaVentana('<%=BASEURL%>/cheques/ChequePrint.jsp','imprimir',600,500,4000,400);     parent.opener.location.href='<%=CONTROLLER%>?estado=Menu&accion=ImpresionCheques&evento=update';  parent.close();"     onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);">&nbsp;	 	
        <img src="<%=BASEURL%>/images/botones/cancelar.gif"   style="cursor:hand" name="cancelar"  title="Cancelar Impresión"       onClick="                                                                  parent.close();     parent.opener.location.href='<%=CONTROLLER%>?estado=Menu&accion=ImpresionCheques&evento=cancel'"      onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" >
     </p>

    </center>
    
    <p class='letra'> <%= msj %> </p>
    

</body>
</html>
