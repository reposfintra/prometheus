<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
  <title>Impresion de Cheques - Ayuda</title>
  <META http-equiv=Content-Type content="text/html; charset=windows-1252">
  <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>
<body>

<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/cheques/"; %>

<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL DE IMPRESI�N DE CHEQUES </div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>

                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         El programa permitir� imprimir cheques por concepto de anticipos para planillas. Solamente los anticipos
                         asignados al usuario. Se presentar� una vista como lo indica la Figura 1.
                      </td>
                 </tr>
          
                 <tr><td  align="center" ><img  src="<%= BASEIMG%>ImpresionCheques.JPG" >                  <br>
                     <strong>Figura 1</strong></td>
                 </tr>
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='90'>
                         Los anticipos de las planilla se agrupan de acuerdo al beneficiario con el fin de generar un cheque por cada 
                         beneficiario.
                         Se presenta informaci�n de planilla, valor de anticipo, beneficiario, valor del cheque, banco, usuario y agencia.
                          
                      </td>
                 </tr>

                 <tr>
                      <td  class="ayudaHtmlTexto" height='50'>
                         Si desea que el cheque se pague por otro banco distinto al default, puede cambiarlo en el link llamado "Cambiar Banco" del cheque respectivo.
                         En este caso, hacemos click en el link del segundo registro del listado, nos presenta una vista como lo indica la figura 2.
                         <br><br>
                      </td>
                 </tr>
                 
                 <tr><th  ><img  src="<%= BASEIMG%>cambio de banco cheques.JPG">           <br>Figura 2</th></tr>
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='90'>
                         Si desea imprimir el cheque, primero deber� seleccionarlo, para esto debe hacer click en
                         el checkbox asociado al cheque respectivo, y luego deber� hacer click
                         el el bot�n  imprimir, de lo contrario le aparecer� un mensaje como lo indica la figura 3.<br>
                         Aparecer�  una ventana de confirmaci�n que contiene informaci�n del cheque a Imprimir, como lo indica la Figura  4.
                         Para este caso, seleccionamos el segundo registro del listado.
                      
                      </td>
                 </tr>
                 
                 <tr><th ><img  src="<%= BASEIMG%>MsjError.JPG">                          <br>Figura 3<br><br><br></th></tr>
                 
                 <tr><th  ><img  src="<%= BASEIMG%>Confirmacion de impresion cheques.JPG"> <br>Figura 4</th></tr>
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='90'>
                         Si acepta la impresi�n, deber� hacer click en el bot�n "Aceptar", de lo contrario haga click en el bot�n "Cancelar".
                         Antes de Imprimir deber� tener en cuenta el estado de la impresora y el n�mero de la serie del cheque para el banco indicado.
                         Al aceptar la impresi�n, le aparecer� una ventana como lo indica la figura 5, en la cual se establecer� la impresora en la
                         cual desea imprimir el cheque.
                      </td>
                 </tr>
                 
                 <tr><th  ><img  src="<%= BASEIMG%>BanImpresion.JPG">      <br>Figura 5</th></tr>
                 
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='90'>
                        Para imprimir el cheque deber� hacer click en el bot�n Imprimir.<br>
                        Una vez impreso el cheque, se quitar� de la lista dicho registro  como lo indica la figura 6, y se actualizar� en
                        la base de datos como impreso.
                        Puede realizar nuevamente el proceso.
                      </td>
                 </tr>
                 <tr align='center'><td><img  src="<%= BASEIMG%>Nuevolistacheques.JPG">               <br>
                       <strong>Figura 6 </strong></td></tr>
        </table>
            
      </td>
  </tr>
</table>

</body>
</html>
