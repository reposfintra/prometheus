<!--
- Autor : Ing. Jose De La Rosa
- Date  : 23 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite definir el esquema de Impresion para cheques.
--%>

<%-- Declaracion de librerias--%>

<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>

<!-- Para los botones -->
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>



<html>
<head>
   <title>Esquema de Cheques</title>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
   <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
   <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>
</head>
<body>


   
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Esquema de Impresi�n para Cheques"/>
</div>


<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

    <center>   
   
    <% //--- variables de bloqueos                                 
        String estadoInsert = request.getParameter("estadoInsert");
        String estadoSearch = request.getParameter("estadoSearch");
        String estadoUpdate = request.getParameter("estadoUpdate");
        String estadoDelete = request.getParameter("estadoDelete"); 
        String comentario   = request.getParameter("comentario");  
        EsquemaCheque esquema = model.EsquemaChequeSvc.getEsquema();
    %>
    
    <FORM ACTION="<%=CONTROLLER%>?estado=EsquemaCheque&accion=Manager" METHOD='post' id='formulario' name='formulario'>
    
    
    
    <table width="600"  border="2" align="center">
      <tr>
         <td>
         
             <table  width='100%' class='tablaInferior'>      
                   <tr>
                     <td colspan='2'>
                         <table width='100%'  class="barratitulo">
                            <tr class="fila">
                                    <td align="left" width='40%' class="subtitulo1" nowrap> Definici�n de Esquema </td>
                                    <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif"  align="left"><%=datos[0]%></td>
                            </tr>
                          </table>
                      </td>
                   </tr> 
        
                   <tr class="fila">
                         <TD  width='40%'> &nbsp Distrito :    </TD>
                         <TD width='*'>  
                                  <select class='comentario' name='distrito'>
                                     <option value='FINV'>FINV</option>
                                   </select>
                         </TD>
                    </tr>
                   
                    <tr class="fila">
                            <TD  > &nbsp Banco :                  </TD>
                            <TD >  
                                  <%if(esquema!=null){%>
                                     <input type='text' class='comentario' style='width=130' name='banco' value='<%= esquema.getBanco() %>' readonly='readonly'> 
                                   <%}else{%>
                                   <select class='comentario' name='banco' style='width=50%'>
                                     <option value=''>Escoja Banco</option>
                                       <% List listadoBanco = model.EsquemaChequeSvc.getBancos();
                                           if(listadoBanco!=null){
                                               for(int i=0;i<listadoBanco.size();i++){
                                                    CodeValue banco = (CodeValue) listadoBanco.get(i);%>                                                
                                                   <option value='<%= banco.getCodigo() %>'><%= banco.getCodigo() %></option>
                                              <%}
                                           }
                                           else{%> <option value=''>No hay bancos</option>   <%}%>
                                   </select>
                                 <% }%>

                            </TD>
                    </TR>
                      
                   <tr class="fila">
                       <TD > &nbsp Densidad Impresion (CPI):</TD>
                        <TD>  
                            <% int est2=(esquema!=null)?esquema.getCPI():0;%>
                            <input type='text' value='<%= est2 %>' class='comentario' name='cpi' maxlength='4' size='5'>       
                        </TD>
                    </TR>
                    
                    
                    <tr class="fila">
                        <TD  > &nbsp Descripci�n Campo :      </TD>
                        <TD >
                           <% if(esquema!=null){%>
                                <input type='text' class='comentario' style='width=130' name='descripcion' value='<%= esquema.getDescripcion() %>' readonly='readonly'> 
                            <% }else{%>
                                <select class='comentario' name='descripcion' style='width=130'>
                                  <option value=''>Escoja Campo</option>
                                    <option value='A�o'          >A�o</option>
                                    <option value='Mes'          >Mes</option>
                                    <option value='Dia'          >Dia</option>
                                    <option value='Valor'        >Valor</option>
                                    <option value='Beneficiario' >Beneficiario</option>
                                    <option value='Monto'        >Monto Escrito</option>
                                </select>
                            <%}%>
                         </TD>
                    </TR>
                    
                    
                    <tr class="fila">
                            <TD > &nbsp Posici�n de Linea :      </TD>
                            <TD > 
                                <% int est4=(esquema!=null)?esquema.getLinea():0;%> 
                                <input type='text' value='<%=est4%>' class='comentario' name='linea' maxlength='4' size='5'>     
                            </TD>
                    </TR>
            
                    <tr class="fila">
                          <TD > &nbsp Posici�n de Columna :    </TD>
                          <TD >
                            <% int est5=(esquema!=null)?esquema.getColumna():0;%>
                            <input type='text' value='<%=est5%>' class='comentario' name='columna' maxlength='4' size='5'>    
                          </TD>
                    </TR>
                    
                    

             </table>
         
         </td>
      </tr>
    </table>

    <br>
    <p>            
                   
           
<tsp:boton value="buscar"    disabled="<%= new Boolean(estadoSearch) %>" onclick="ValidarEsquema(2);"/>                   
<tsp:boton value="aceptar"   disabled="<%= new Boolean(estadoInsert)  %>" onclick="ValidarEsquema(1);"/>                   
<tsp:boton value="cancelar"                                 onclick="ValidarConductor(5);"/>
                   
                   
                   <tsp:boton value="modificar" disabled="<%= new Boolean(estadoUpdate) %>" onclick="ValidarEsquema(3);"/>
                   <tsp:boton value="eliminar"  disabled="<%= new Boolean(estadoDelete) %>" onclick="ValidarEsquema(4);"/>
                   <tsp:boton value="salir"                                    onclick="parent.close();"/>
               
                   <input type='hidden' name='evento'>
    </p>
     
    </FORM>
    
    
    
    
    
    
    
    <!--  Presentamos el listado de esuqema -->  
    
 <% List listado = model.EsquemaChequeSvc.getList();
    if(listado!=null && listado.size()>0){%>
    
          <table width="700" border="2" align="center">
            <tr>
                <td>
                   <table width="100%" align="center">
                              <tr>
                                    <td  class="subtitulo1"  width='60%'>&nbsp;Listado de Esquemas</td>
                                    <td  class="barratitulo" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                              </tr>
                              <tr class='fila' >
                                   <td width='100%' colspan='2' >
                                         <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">   
                                               <tr class="tblTitulo">
                                                      <TH width='5%' >DISTRITO    </TH>
                                                      <TH width='30%'>BANCO       </TH>
                                                      <TH width='20%'>CAMPO       </TH>
                                                      <TH width='8%' >CPI         </TH>
                                                      <TH width='10%'>LINEA       </TH>
                                                      <TH width='12%'>COLUMNA     </TH>
                                               </tr> 

                                               <% for (int i=0;i<listado.size();i++){
                                                        EsquemaCheque objeto = ( EsquemaCheque) listado.get(i);%>

                                                     <TR class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand"   onclick="javascript:window.location.href='<%=CONTROLLER%>?estado=EsquemaCheque&accion=Manager&evento=SUBIR&distrito2=<%= objeto.getDistrito()%>&banco2=<%= objeto.getBanco()%>&descripcion2=<%= objeto.getDescripcion()%>&cpi2=<%= objeto.getCPI()%>&linea2=<%= objeto.getLinea()%>&columna2=<%= objeto.getColumna()%>';"  >
                                                          <TD class="bordereporte"              > <%= objeto.getDistrito()    %> </TD>
                                                          <TD class="bordereporte"              > <%= objeto.getBanco()       %> </TD>
                                                          <TD class="bordereporte"              > <%= objeto.getDescripcion() %> </TD>
                                                          <TD class="bordereporte" align='center'> <%= objeto.getCPI()         %> </TD>
                                                          <TD class="bordereporte" align='center'> <%= objeto.getLinea()       %> </TD>
                                                          <TD class="bordereporte" align='center'> <%= objeto.getColumna()     %> </TD>
                                                       </TR>
                                                 <%}%>
                                        </table>
                                    </td>
                             </tr>
                  </table>
             </td>
          </tr>
        </table>     
    
   
    <%}%>
    
    
    
    <!--  Presentamos los comentario de las transacciones, si los hay  -->    
    <% if(comentario!=null && !comentario.equals("")){%>
        <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="400" align="center" class="mensajes"><%=  comentario %></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    <%}%>

    
</div>  
 <%=datos[1]%> 
 
    
</body>
</html>
