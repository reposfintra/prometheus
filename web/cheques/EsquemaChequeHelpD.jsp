<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
      <title>Esquema de Impresi�n - Ayuda</title>
      <META http-equiv=Content-Type content="text/html; charset=windows-1252">
      <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
      <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
    
    
              <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                         <td height="24" colspan="2"><div align="center">ESQUEMA DE IMPRESI�N</div></td>
                    </tr>
                    <tr class="subtitulo1">
                        <td colspan="2"> Definici�n de Esquema</td>
                    </tr>
                    
                    
                    <tr>
                        <td width="149"  class="fila"> Distrito </td>
                        <td width="525"  class="ayudaHtmlTexto"> 
                             Campo para seleccionar el distrito al cual se le definir� el esquema de impresi�n,
                             Los valores se cargar�n en una lista.                        
                        </td>
                    </tr>
                    
                     <tr>
                        <td width="149"  class="fila"> Banco </td>
                        <td width="525"  class="ayudaHtmlTexto"> 
                             Campo para seleccionar el banco al cual se le definir� el esquema de impresi�n,
                             Los valores se cargar�n en una lista.
                        </td>
                    </tr>
                    
                    <tr>
                        <td width="149"  class="fila"> Densidad de Impresi�n </td>
                        <td width="525"  class="ayudaHtmlTexto"> 
                           Campo para definir la densidad de impresi�n, su valor deber� ser num�rico.
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td width="149"  class="fila"> Campo o Elemento </td>
                        <td width="525"  class="ayudaHtmlTexto"> 
                           Campo para seleccionar el campo o elemento a ubicar en la impresi�n.
                           Sus valores se cargar�n en una lista
                        </td>
                    </tr>
                    
                    <tr>
                        <td width="149"  class="fila"> L�nea </td>
                        <td width="525"  class="ayudaHtmlTexto">
                           Campo para definir el n�mero de la fila  que ocupar� el elemento o campo, su valor deber� ser num�rico.
                        </td>
                    </tr>
                    
                     <tr>
                        <td width="149"  class="fila"> Columna </td>
                        <td width="525"  class="ayudaHtmlTexto"> 
                            Campo para definir el n�mero de la columna  que ocupar� el elemento o campo, su valor deber� ser num�rico.
                        </td>
                    </tr>
                    
              </table>
        
        
        
      </td>
   </tr>
</table>
<p>
     <img src="<%=BASEURL%>/images/botones/salir.gif"   name="imgsalir"     onClick=" parent.close();"               onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
</p>

                    


</body>
</html>
