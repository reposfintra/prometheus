<%--
/******************************************************************************
 * Nombre clase :                   reemplazochq.jsp                          *
 * Descripcion :                    Pagina para el Reemplazo los Cheques      *
 * Autor :                          Ing. Juan M. Escandon                     *
 * Fecha Creado :                   15-02-07                                  *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*,com.tsp.util.*, javax.servlet.jsp.tagext.TagExtraInfo"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
    List listado       =  model.egresoService.getListaCheques(); 
    String Mensaje     = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
    String fltBanco    = (String) session.getAttribute("fltBanco");
    String fltSucursal = (String) session.getAttribute("fltSucursal");
    String fltRangoini = ( (String) session.getAttribute("fltRangoini") != null )?(String) session.getAttribute("fltRangoini"):"";
    String fltRangofin = ( (String) session.getAttribute("fltRangofin") != null )?(String) session.getAttribute("fltRangofin"):"";
    String fltTipo     = (String) session.getAttribute("fltTipo");
    fltBanco     = (fltBanco!=null?fltBanco:"");
    fltSucursal  = (fltSucursal!=null?fltSucursal:"");    
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
%>

<html>
<head>
<title>Reemplazo de Cheques</title>
<link  href="<%= BASEURL %>../../../css/estilostsp.css" rel="stylesheet">

<script src="<%= BASEURL %>/js/validarReemplazoCheque.js"></script>
<script src="<%= BASEURL %>/js/boton.js"></script>
<script src="<%= BASEURL %>/js/reporte.js"></script>
<script>
<%
   Vector bancos = model.servicioBanco.obtenerBancosPorAgencia( usuario.getId_agencia() );
   out.print("var datos = [");
   for (int i = 0; bancos!=null && i<bancos.size(); i++){
       Banco bc = (Banco) bancos.get(i);
       out.print("\n'" + bc.getBanco()+"~"+bc.getBank_account_no() + "'" + (i+1 != bancos.size()?",":"") );
   }
   out.print("];");
   

%>
    var CONTROLLER = '<%= CONTROLLER %>';
    var BASEURL    = '<%= BASEURL    %>';
</script>

</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reemplazo de Cheques"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:95%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>
<br>
<div class="fila" style="position:absolute; visibility:hidden; width:350px; height:100px; z-index:1; border: 2 outset #003399; " id="msg">Mario
</div>

<form action="<%= CONTROLLER %>?estado=Reemplazo&accion=Cheques" method="post" name="flist" onSubmit="javascript: return _onsubmit(this);">
<table width="120%" border="2">
  <tr>
    <td>
	
		<!-- cabecera de la seccion -->
		<table width="100%" align="center" class="tablaInferior" >
			<tr>
			<td class="subtitulo1"  width="50%">Reemplazo de Cheques</td>
			<td class="barratitulo" width="50%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align="left"></td>
			</tr>
		</table>		
		<!-- fin cabecera -->	
		
		
		<!-- seccion del filtro -->
		<table width="100%" >
			<tr class="fila">
			<td width="7%">Banco </td>
			<td width="18%"> <select name="fltBanco" class="listmenu" style="width:80% " onchange="LoadSucursal(fltBanco, fltSucursal);"></select> </td>
			<td width="7%">Sucursal </td>
			<td width="18%"> <select name="fltSucursal" class="listmenu" style="width:80% "></select> </td>
			<td width="10%">Rango Inicial</td>
			<td width="15%"> 
					<input type="text" id="fltRangoini" name="fltRangoini" style="width:80%;text-align:center "  	 class="textbox" size="15" maxlength="15" value='<%= fltRangoini %>' onkeypress='soloAlfa(event)'> </td>
           	        <td width="10%">Rango Final</td>
			<td width="15%"> 
					<input type="text" id="fltRangofin" name="fltRangofin" style="width:80%;text-align:center "      class="textbox" size="15" maxlength="15" value='<%= fltRangofin %>' onkeypress='soloAlfa(event)'></td>				
			<td width="7%" align="center"> 
			<img name="Submit" value="Filtrar" src="<%= BASEURL %>/images/botones/buscar.gif" align="absmiddle" style = "cursor:hand" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick=" goFiltro(fltBanco.value, fltSucursal.value, fltRangoini.value, fltRangofin.value);">
			</td>
			</tr>
		</table>		
		<!-- fin filtro -->
		
		<% 
		   if (listado!=null && listado.size()>0) {
				   String style      = "simple";
				   String position   = "bottom";
				   String index      = "center";
				   int maxPageItems  = 10;
				   int maxIndexPages = 10;
		%>			
		
		<!-- seccion del datos -->
		<table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4">
	
			<tr class="tblTitulo">
			<th width="10%">Banco       </th>
			<th width="10%">Sucursal    </th>
			<th width="8%">Cheque       </th>
			<th width="10%">Valor       </th>
			<th width="5%">Moneda       </th>
			<th width="17%">Beneficiario</th>
			<th width="10%" title='Fecha de Impresion'>Fecha Imp.  </th>
			<th width="9%" title='Usuario de Impresion'>Usuario Imp.</th>
			<th width="10%" title='Banco de Reemplazo'>Banco</th>
			<th width="15%" title='Sucursal de Reemplazo'>Sucursal</th>
			<td width='6%' align="center"><input type='checkbox' name='All' onclick='jscript: SelAll();'></td>
			</tr>

			<pg:pager
				 items         ="<%= listado.size()%>"
				 index         ="<%= index %>"
				 maxPageItems  ="<%= maxPageItems %>"
				 maxIndexPages ="<%= maxIndexPages %>"
				 isOffset      ="<%= true %>"
				 export        ="offset,currentPageNumber=pageNumber"
				 scope         ="request">
			<%  for (int i = offset.intValue(), l = Math.min(i + maxPageItems, listado.size()); i < l; i++){
				  Egreso dt = (Egreso) listado.get(i);  				 
			%>
				  <pg:item>
						<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'  nowrap>
							<td class="bordereporte" nowrap ><%= dt.getBranch_code()     %></td>
							<td class="bordereporte" nowrap ><%= dt.getBank_account_no() %></td>
							<td class="bordereporte" nowrap align="right" ><%= dt.getDocument_no()     %></td>
							<td class="bordereporte" nowrap align="right" ><%= Util.customFormat(dt.getVlr_for()) %></td>
							<td class="bordereporte" nowrap align="center"><%= dt.getCurrency() %></td>
							<td class="bordereporte" nowrap ><%= dt.getPayment_name()    %></td>
							<td class="bordereporte" nowrap align="center"><%= dt.getPrinter_date()    %></td>
							<td class="bordereporte" nowrap align="center"><%= dt.getUsuario_impresion()    %></td>
							<td class="bordereporte" nowrap><select name='fltBanco<%= i %>' class="listmenu" style="width:100% " onchange="LoadSucursal(fltBanco<%= i %>, fltSucursal<%= i %>);  flist.LOV<%= i %>.value='<%= dt.getBranch_code()+","+dt.getBank_account_no()+","+dt.getDocument_no()+","+dt.getConcept_code() %>,'+flist.fltBanco<%=i%>.value+','+flist.fltSucursal<%= i %>.value; "></select> </td>
							<td class="bordereporte" nowrap><select name='fltSucursal<%= i %>' class="listmenu" style="width:100% " onchange=" flist.LOV<%= i %>.value='<%= dt.getBranch_code()+","+dt.getBank_account_no()+","+dt.getDocument_no()+","+dt.getConcept_code() %>,'+flist.fltBanco<%=i%>.value+','+flist.fltSucursal<%= i %>.value; "></select> </td>
							<td align="center" class="bordereporte"><input type='checkbox' name='LOV' id="LOV<%= i %>" value="<%= dt.getBranch_code()+","+dt.getBank_account_no()+","+dt.getDocument_no()+","+dt.getConcept_code() %>" onclick='jscript: ActAll();'></td>
							<script>
                                                            LoadBancos(flist.fltBanco<%=i%>, flist.fltSucursal<%=i%>);
                                                            flist.fltBanco<%=i%>.value = '<%= dt.getBranch_code() %>';
                                                            LoadSucursal(flist.fltBanco<%= i %>, flist.fltSucursal<%= i %>);
                                                            flist.fltSucursal<%= i %>.value  = '<%= dt.getBank_account_no()%>';
							</script>
						</tr>	
				  </pg:item>	
			<% } // end for %>
			<tr>
				<td colspan="11" align="center" >
					<pg:index>
					<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
					</pg:index> 
				</td>
			</tr>
			</pg:pager>
		</table>	
		<% } // end if %>
		<!-- fin datos -->				
	</td>
  </tr>
</table>

<br>
<% if ( listado!=null && listado.size()>0 ) { %>
<input type="hidden" name="Opcion" value="">
<img name="imgActualizar" src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" flist.Opcion.value = 'Actualizar'; if (validarlistado()) flist.submit(); ">
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
<% } %>
</form>

 <%if(!Mensaje.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>

<script>
	LoadBancos(flist.fltBanco, flist.fltSucursal);
	flist.fltBanco.value    = '<%= fltBanco    %>';
	LoadSucursal(flist.fltBanco, flist.fltSucursal);
	flist.fltSucursal.value = '<%= fltSucursal %>';
	flist.fltRangoini.value = '<%= fltRangoini %>';
	flist.fltRangofin.value = '<%= fltRangofin %>';

</script>
</center>
</div>
</body>
</html>
