<!--
- Autor : Ing. Jose De La Rosa
- Date  : 23 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite cambiar los bancos
--%>

<%-- Declaracion de librerias--%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*,com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>


<html>
<head>
   <title>Cambiar Bancos</title>
	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>  
</head>


<body onLoad="redimensionar();" onResize="redimensionar();">


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cambiar Bancos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 



 <%     String planilla        = request.getParameter("planilla");
        String idCheque        = request.getParameter("idCheque");
 	ImpresionCheque cheque = model.ImprimirChequeSvc.getChequeListado(idCheque);
	Usuario usuario = (Usuario) session.getAttribute("Usuario"); %>
 

 <form name="form" method="post" action="<%=CONTROLLER%>?estado=Banco&accion=Cambiar">

   <table width="950" border="2" align="center">
    <tr>
      <td>
	  	  <table width="100%" align="center">
              <tr>
                <td width="50%" class="subtitulo1">&nbsp;Cambiar Banco Para Imprimir Cheque
                <input type="hidden" name="idCheque" value="<%=idCheque%>">
                <input type="hidden" name="planilla" value="<%=planilla%>"> </td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
		 <TABLE width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
		   <TR class="tblTitulo" align="center">
			 <TH width='15%'>Planilla / Valor  </TH>
			 <TH width='21%'>Beneficiario      </TH>
			 <TH width='13%'>Valor Cheque      </TH>
			 <TH width='9%' >Moneda            </TH>
			 <TH width='25%'>Información de Banco</TH>
			 <TH width='12%'>Usuario           </TH>
			 <TH width='12%'>Agencia           </TH>
			</TR>
		 
		   <TR class="filagris">
			 <TD align='left' class="bordereporte"> 

			       <table width='100%' cellpadding='0' cellspacing='0' class="bordereporte">							    
                                     <% String[] vec     = cheque.getPlanilla().split("<BR>");
                                        String[] vecVlr  = cheque.getValores().split("<BR>");
                                        for(int j=0;j<vec.length;j++){
                                            String oc    = vec[j];
                                            double valor = Double.parseDouble( vecVlr[j]) ;%>
                                            <tr  class="filagris" style="font size:11px" >
                                                 <td > <%= oc %> </td><td align='right'> <%= Util.customFormat(valor) %>  </td>
                                            </tr>
                                        <%}%>
                </table>
			 
			 </TD>
			 <TD align='left'   class="bordereporte">&nbsp;<%= cheque.getBeneficiario() %>             </TD>
			 <TD align='right'  class="bordereporte">      <%= Util.customFormat(cheque.getValor()) %> </TD>
			 <TD align='center' class="bordereporte">      <%= cheque.getMoneda()       %> <input name="moneda" type="hidden" id="moneda" value="<%= cheque.getMoneda()       %>"></TD>
			 <TD align='center' class="bordereporte">  
				  <%TreeMap bancos= model.buService.getListaBancos(usuario.getLogin());%>
				  <input:select name="banco" options="<%=bancos%>" attributesText="style='width:100%;'" default ='<%=cheque.getBanco()+"/"+ cheque.getAgenciaBanco()%>' /> 
			 </TD>
			 <TD align='center' class="bordereporte"><%= cheque.getNombreUsurio()  %></TD>
			 <TD align='center' class="bordereporte"><%= cheque.getNombreAgencia() %></TD>
			 
			 
			 
			 <input type='hidden' name='idCheque' value='<%= cheque.getId()%>'>
			 <input type="hidden" name="planilla" value="<%= planilla %>"          id="planilla">
			 <input type="hidden" name="bancoAnt" value="<%= cheque.getBanco()+"/"+ cheque.getAgenciaBanco()%>" id="bancoAnt">
			 
			 
			 
		   </TR>
		 </TABLE>
 		</td>
 	</tr>
 </table>

	<p align="center">
		<input type="image" src="<%=BASEURL%>/images/botones/aceptar.gif"   title="Cambiar Banco"              style="cursor:hand"  name="Buscar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img                src="<%=BASEURL%>/images/botones/salir.gif"     title="Salir al Menu Principal"    style="cursor:hand"  name="salir"   onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
		<img                src="<%=BASEURL%>/images/botones/regresar.gif"  title="Volver"                     style="cursor:hand"  name="buscar"  onClick="window.location = '<%=CONTROLLER%>?estado=Menu&accion=ImpresionCheques&opcion=no&total=0'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</p>
	
	 <%if(request.getParameter("mensaje")!=null){%>
 <table border="2" align="center">
   <tr>
     <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
         <tr>
           <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
           <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
           <td width="58">&nbsp;</td>
         </tr>
     </table></td>
   </tr>
 </table>
 <%}%>
 </form>
 <br>

</div>
</body>
</html>
