<!--
- Autor : Ing. Jose De La Rosa
- Date  : 23 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite mostrar cheques no impresos
--%>

<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*,com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>


<html>
<head>
   <title>Impresión de Cheques</title>
	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
      <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>  
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Mostrar Cheques"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <form name="form" method="post" action="<%=CONTROLLER%>?estado=Chequenoimp&accion=Anular&anular=ok">
 <%
 	Movpla ch = model.movplaService.getMovPla();
 %>
 	<table width="900" border="2" align="center">
		<tr>
		  <td>
		  	<table width="100%" align="center">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Anular Anticipo </td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			<TABLE width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                <TR class="tblTitulo" align="center">
                     <TH width='4%' align='center' >Planilla</TH>
                     <TH width='17%' align='center'>Beneficiario</TH>
                     <TH width='14%' align='center'> Valor</TH>
                     <TH width='13%' align='center' > Moneda</TH>
                     <TH width='32%' align='center'> Información de Banco</TH>
                     <TH width='12%' align='center'>Usuario</TH>
                     <TH width='8%' align='center'>Agencia</TH>
                </TR>
                <TR class='filagris'> 
                  <TD align='center'  class="bordereporte"><%=ch.getPlanilla()%><input name="planilla" type="hidden" id="planilla" value="<%=ch.getPlanilla()%>"></TD>
                  <TD align='left'    class="bordereporte"><%=ch.getPla_owner()%></TD>
                  <TD align='right'   class="bordereporte"><%=Util.customFormat(ch.getVlr_for())%></TD>
                  <TD align='center'  class="bordereporte"><%=ch.getCurrency()%></TD>                 
                  <TD align='center'  class="bordereporte"><%=ch.getBanco()%></TD>
                  <TD align='center'  class="bordereporte"><%=ch.getCreation_user()%></TD>
                  <TD align='center'  class="bordereporte"><%=ch.getAgency_id()%></TD>
				</TR>
			</TABLE>
			</tr>
		</td>
	</table>
		<p align="center">
			<img src="<%=BASEURL%>/images/botones/anular.gif" title="Anular" style="cursor:hand" name="Anular" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="form.submit();this.disabled=true;">&nbsp;	 	
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
		</p>
 </form>
  </div>
</body>
</html>
