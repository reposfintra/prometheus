
<!--
- Autor : Ing. Jose De La Rosa
- Date  : 23 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite mostrar cheques impresos
--%>

<%-- Declaracion de librerias--%><%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*,com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>


<html>
<head>
   <title>Impresión de Cheques</title>
	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>      <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>  
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Mostrar Cheques"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <form name="form" method="post" action="<%=CONTROLLER%>?estado=Cheque&accion=Anular&anular=ok&Opcion=ok">
 <%
 	Movpla ch = model.movplaService.getMovPla();
	String bancoC[]=ch.getBanco().split("/");
	String banco="";
	String sucursal = "";
	if(bancoC.length>0){
		banco = bancoC[0];
	}
	if(bancoC.length>1){
		sucursal = bancoC[1];
	}
 %>
   <table width="650" border="2" align="center">
    <tr>
      <td>
		<TABLE width='100%' class="tablaInferior">
				<tr class="fila">
					<td width="50%" colspan="2" class="subtitulo1">&nbsp;Anular Cheques Impreso</td>
					<td width="50%" colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
                <TR class='fila'>
                     <TH width='16%' align='center' > <div align="left"><strong>NUMERO</strong></div></TH>
                  <TH width='33%' align='center'><%=ch.getDocument()%>
				  
                       <input name="cheque" type="hidden" id="cheque" value="<%=ch.getDocument()%>"> <input name="banck" type="HIDDEN" id="banck" value="<%=banco%>">
                       <input type="HIDDEN" name="sucursalbanco" value="<%=sucursal%>"></TH>
                     <TH width='17%' align='center'> Valor         </TH>
                     <TH width='34%' align='center' >  <%=Util.customFormat(ch.getVlr_for())%><%=ch.getCurrency()%> </TH>
                </TR>
                <TR class='fila'> 
                     <TD>Beneficiario</TD>
                  <TD align='left'   ><%=ch.getPla_owner()%></TD>
                       <TD align="center">Banco</TD>
                       <TD align='center' ><%=ch.getBanco()%> </TD>                 
    			</TR>
                <TR class='fila'>
                  <TD >Agencia</TD>
                  <TD align='left'><%=ch.getAgency_id()%></TD>
                  <TD align='center' >Usuario</TD>
                  <TD align='center' ><%=ch.getCreation_user()%></TD>
                </TR>
                <TR class='fila'>
                  <TD colspan="4" align='center' ><strong>Información de la Anulación</TD>
                </TR>
                <TR class='fila'>
                  <TD >Causa</TD>
                  <TD align='left'   ><%TreeMap causas= model.causas_anulacionService.getTcausasa(); %>
          			<input:select name="causas" options="<%=causas%>" attributesText="style='width:100%;'" /></TD>
                  <TD >Tipo de Recuperación</TD>
                  <TD align='center' ><%TreeMap trecuperecion= model.trecuperacionaService.getTreeTRA(); %>
          			<input:select name="trecuperacion" options="<%=trecuperecion%>" attributesText="style='width:100%;'" /></TD>
                </TR>
                <TR class='fila'>
                  <TD height="20" >Observación</TD>
                  <TD colspan="3" align='left'   ><textarea name="observacion" id="observacion" class="textbox" style="width:100% "></textarea></TD>
                </TR>
			</TABLE>
	  </td>
    </tr>
  </table>
		<p align="center">
			<input type="image" src="<%=BASEURL%>/images/botones/anular.gif" title="Anular" style="cursor:hand" name="Anular" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;	 	
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
		</p>
 </form>
 </div>
</body>
</html>
