<!--
- Autor : Ing. Jose De La Rosa
- Date  : 23 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que busca los cheques no impresos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Impresion de Planillas</title>
<link href="../css/letras.css" rel="stylesheet" type="text/css">
</head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Cheque No Impreso"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <% 
  String estado =request.getParameter("estado");
  String men = "";
  if ( estado.equals("ok") ){
     men = "Anticipo anulado satisfactoriamente";
  }
if ( estado.equals("anu") ){
     men = "Anticipo anulado anteriormente ";
  }
  if ( estado.equals("no") ){
     men = "No se encontro anticipo para esta planilla.";
  }
%>
<form action="<%=CONTROLLER%>?estado=Chequenoimp&accion=Anular" method="post" class="msg">
<TABLE width='354' border='2' align="center">
	<tr>
		<td width="342">
    		<TABLE width='100%' class="tablaInferior">
				<tr class="fila">
					<td width="50%" class="subtitulo1">&nbsp;Anular Anticipos</td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
				<tr class="fila">
					<td align="center">Numero Planilla</td>
					<td><input   name='planilla' type='text' id="planilla" class="textbox"></td>
				</tr>
			</TABLE>
		</td>
    </tr>
</TABLE>
    <input type='hidden' name='Origen' value='FBuscar'>
	<input type='hidden' value='Buscar' name="Opcion" >
	<p align="center">
		<input type="image" src="<%=BASEURL%>/images/botones/buscar.gif" title="Buscar Cheques No Impresos" style="cursor:hand" name="Buscar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
	</p>
</form>
	<%--Mensaje de informacion--%>
	<%if(!men.equals("")){%>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="229" align="center" class="mensajes"><%=men%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<%}%>
</div>
</body>
</html>
