<!--
- Autor : Ing. Jose De La Rosa
- Date  : 23 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que busca los cheques
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<%
String banco 	 = (request.getParameter("banco")!=null)?request.getParameter("banco"):"BANCOLOMBIA";
TreeMap b 		 = model.servicioBanco.obtenerNombresBancos();
TreeMap sbancos  = model.servicioBanco.obtenerSucursalesBanco(banco);  
%>
<head><title>Anular con reemplazo</title>
</head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
<script>
 	function bancos(){       
        formulario.Opcion.value = 'BuscarS';
		window.location='<%=CONTROLLER%>?estado=Cheque&accion=Buscar&Opcion=BuscarS&banco='+formulario.banck.value;
    }
</script>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anular Cheque y Reemplazar"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form id = "formulario" name= "formulario" action="<%=CONTROLLER%>?estado=Cheque&accion=Buscar" method="post">
  <table width="422" border="2" align="center">
    <tr>
      <td width="575">
		<TABLE width='100%' class="tablaInferior">
			<tr class="fila">
				<td class="subtitulo1">&nbsp;Anular Cheques</td>
			  <td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			</tr>			
			<tr class="fila">
				<td  align="center">Banco</td>
				<td ><input:select name="banck"  attributesText=" id='banco' style='width:90%;' class='listmenu'  onChange='bancos();'"  default="<%=banco%>" options="<%=b%>"  /></td>
			</tr>
			<tr class="fila">
				<td align="center">Sucursal</td>
				<td ><input:select name="sucursalbanco"   attributesText="style='width:90%;' class='listmenu'"  options="<%=sbancos%>" /></td>
			</tr>
			<tr class="fila">
				<td  align="center">Numero</td>
				<td ><input   name='cheque' type='text' id="cheque" class="textbox"></td>
			</tr>
		</TABLE>
	  </td>
    </tr>
  </table>
	<p align="center">
		<input type="image" src="<%=BASEURL%>/images/botones/buscar.gif" title="Buscar Cheques" style="cursor:hand" name="Buscar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
        <br>
        <%if(request.getParameter("mensaje")!=null){%>
</p>
    <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
    <%}%>
    <p align="center">&nbsp;	</p>
    <input type='hidden' value='Buscar'   name="Opcion" >
<input type='hidden' name='Origen' value='FBuscar'>
<input type='hidden' name='Opcion'/> 
</form>
</div>
<%=datos[1]%>
</body>
</html>
