<!--
- Autor : Ing. Jose De La Rosa
- Date  : 23 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite la impresion de cheques
--%>

<%-- Declaracion de librerias--%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*"%>

<html>
<head>
   <title>Imprimir Cheque</title>
   <style>.comentario   {color:000000;font-family:  Verdana,Arial, Helvetica, sans-serif;font-size: 11px; }
          .comentario2  {color:000000;font-family:  Verdana,Arial, Helvetica, sans-serif;font-size:  9px; }
   </style>
   <script type='text/javascript' src="<%= BASEURL %>/js/mouse.js"></script>
   

   <STYLE>
         H1.SaltoDePagina
         {
             PAGE-BREAK-AFTER: always
         }
   </STYLE>

   
</head>

<body onLoad='window.print();window.close();'> 

 <% ImpresionCheque chequeToPrint = model.ImprimirChequeSvc.getChequeToPrint();  
    String usuario = request.getParameter("usuario");
    int copias     = chequeToPrint.getCopias();
    int linea      = 13; 
    double margen  = 0.4;%>
      
    
    
<% for(int kk = 1; kk<= copias; kk++ ){ %>
    
    <DIV  STYLE="position:relative">
        
            <%double top_comprobante = 0;%>
    
            <!-- Esquema de Cheques -->    
            <% chequeToPrint.formatEsquemaCMS();
               String separador = "#NEWCOLUMN#";
               String cuerpoEsquema = chequeToPrint.getCampos();     
               String posEsquema    = chequeToPrint.getPosiciones(); 
               String[] vecCampos   = cuerpoEsquema.split(separador);
               String[] vecPos      = posEsquema.split(separador);   
               List  esquemasList   = chequeToPrint.getPositionColumn();
               
               
               for(int j=0;j<esquemasList.size();j++){
                     EsquemaCheque  esquema = (EsquemaCheque) esquemasList.get(j);
                     if( esquema.getCampo()!=null && esquema.getCampo().equals("top_comprobante")  ){
                           top_comprobante = esquema.getTop();
                           break;
                       } %>                    
                <%}
               
               for(int i=0;i<vecPos.length;i++){
                  String[] vecXY = vecPos[i].split(",");
                  String   x     = vecXY[0];  
                  String   y     = String.valueOf( (Double.parseDouble( vecXY[1] ) - margen) );%>
                  <DIV class=comentario   STYLE=" overflow:hidden;  position:absolute; left:'<%=x%>cm'; top:'<%=y%>cm'; "><%= vecCampos[i]%></DIV>            
                                   
             <%}%>
                          

             <% if( top_comprobante > 0 ){ %>
                
                <DIV class=comentario STYLE="position:absolute; left:0px; top:<%=top_comprobante%>cm; ">
              
              <%}%>
             
              <% int topeBajo = 340; %>
              
              <DIV class=comentario STYLE=" position:absolute; left:2px;   top:<%=topeBajo%>px;  "> <%= chequeToPrint.getBanco()        %> </DIV> 
              <DIV class=comentario STYLE=" position:absolute; left:270px; top:<%=topeBajo%>px;  "> <%= chequeToPrint.getCheque()       %> </DIV>
              <DIV class=comentario STYLE=" position:absolute; left:431px; top:<%=topeBajo%>px;  "> <%= chequeToPrint.getCedula()       %> </DIV> 
              <DIV class=comentario STYLE=" position:absolute; left:570px; top:<%=topeBajo%>px;  "> <%= Util.getFechaActual_String(1)%>-<%=Util.getFechaActual_String(3)%>-<%=Util.getFechaActual_String(5)%></DIV>     
              
              
              <% topeBajo += ( linea * 2);    %>              
              <DIV class=comentario STYLE=" position:absolute; left:415px;  top:<%=topeBajo%>px;  "> <%= chequeToPrint.getBeneficiario() %> </DIV>  
              
              
              <% topeBajo +=  linea;    %>
              
            <!-- Datos Estaticos -->
              <DIV class=comentario STYLE="position:absolute; left:58px;  top:<%=topeBajo%>px; "        > FACTURA              </DIV>  
              <DIV class=comentario STYLE="position:absolute; left:186px; top:<%=topeBajo%>px; "        > VALOR FACT           </DIV>            
              <DIV class=comentario STYLE="position:absolute; left:301px; top:<%=topeBajo%>px; "        > DESCUENTO            </DIV>  
              <DIV class=comentario STYLE="position:absolute; left:387px; top:<%=topeBajo%>px; "        > RETENCION            </DIV>  
              <DIV class=comentario STYLE="position:absolute; left:530px; top:<%=topeBajo%>px; width=80"> VR&nbspNETO          </DIV> 
              
             
              <!-- Listado de Planilla del Cheque  -->
              <% String[] vec     = chequeToPrint.getPlanilla().split("<BR>");
                 String[] vecVlr  = chequeToPrint.getValores().split("<BR>") ;
                 int topPlanilla  = topeBajo +  linea ;
                 for(int j=0;j<vec.length;j++){
                    String oc    = vec[j];
                    double valor = Double.parseDouble( vecVlr[j]) ;%>        
                    <DIV class=comentario STYLE=" position:absolute; left:58px;  top:<%= topPlanilla %>px;          ">  <%= oc %> </DIV>  
                    <DIV class=comentario STYLE=" position:absolute; left:200px; top:<%= topPlanilla %>px; width:70 ">     
                          <table width='100%'>
                              <tr><td align='right' class=comentario><%= Util.customFormat(valor)  %></td></tr>
                         </table>
                    </DIV>  
                    <DIV class=comentario STYLE=" position:absolute; left:520px; top:<%= topPlanilla %>px; width:70;">      
                         <table width='100%'>
                             <tr><td align='right' class=comentario><%= Util.customFormat(valor)  %></td></tr>
                         </table>
                    </DIV> 
                    <%
                    topPlanilla += linea;
                 }%>


             <!-- Configuracion de Posiciones de Acuerdo a la cantidad de planillas del cheque-->     

              <%
                int restar     = (linea * 4);
                int posParrafo = 570 - restar;
                int posLinea   = 595 - restar;
                int posValor   = 613 - restar;
                int posUser    = 630 - restar;
                if(topPlanilla > ( posParrafo - linea )){
                     posParrafo =  topPlanilla + (linea*2);
                     posLinea   =  posParrafo  + (linea*2);
                     posValor   =  posLinea    + linea;
                     posUser    =  posValor    + linea;
                }
             %>

             <DIV class=comentario2 STYLE="position:absolute; left:58px;  top:'<%=posParrafo%>px';"> 
                      Propietarios:<br>
                      Factura = Valor(es) de la(s) planilla(s) - Descuentos <br>  
                      Descuento = Reteiva + Reteica 
             </DIV>  
             <DIV class=comentario STYLE=" position:absolute; left:455px; top:'<%=posLinea%>px';"  >______________________</DIV> 
             <DIV class=comentario STYLE=" position:absolute; left:430px; top:'<%=posValor%>px'; width:200;">             
                     <table width='100%'>
                           <tr><td align='right' class=comentario>  $&nbsp <%= Util.customFormat(chequeToPrint.getValor()) %>  </td></tr>
                     </table>
             </DIV> 

             <DIV class=comentario STYLE=" position:absolute; left:58px;  top:'<%= posUser%>px';  "> <%= usuario %> </DIV>  
             
             <% if( top_comprobante > 0 ){ %>
                
                    </DIV>
              
             <%}%>
             
     </DIV>
     
     <!-- Para el salto de pagina -->
     <% if(kk<copias){%>
          <H1 class=SaltoDePagina> </H1>
     <%}
   
  }%>

</body>
</html>
