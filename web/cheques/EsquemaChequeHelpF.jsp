<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
      <title>Esquema de Impresi�n - Ayuda</title>
      <META http-equiv=Content-Type content="text/html; charset=windows-1252">
      <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
      <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>

<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/esquema/"; %>


<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL DE ESQUEMA DE IMPRESI�N </div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>

                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         Este programa le permite definir esquema de impresi�n de cheques para los bancos, para lo cual tendra que definir
                         la posici�n que ocupar� dentro del cheque cada elemento.
                         Los elementos que deber� posicionar son:
                         <ul>
                             <li>A�o</li>
                             <li>Mes</li>
                             <li>Dia</li>
                             <li>Valor</li>
                             <li>Beneficiario</li>
                             <li>Monto Escrito</li>
                         </ul>
                         A cada uno de ellos, deber� definir la columna y la l�nea en la cual desea que salga la informaci�n dentro del cheque.
                         Esto se establece a trav�s del formulario descrito en la figura 1.
                         <br><br>
                      </td>
                 </tr>
                 <tr><td  align="center" ><img  src="<%= BASEIMG%>Formulario.JPG" >                  <br>
                     <strong>Figura 1</strong></td>
                 </tr>
                 
                 
                 
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         <br><br><br><br>
                         Usted deber� estipular para un distrito y banco la posici�n  que ocupar� el elemento dentro del cheque, definiendo
                         la L�nea y Columna, y posteriormente realizando click en el bot�n Aceptar.
                         <br>
                         Si desea listar los elementos guardados, deber� hacer click en el boton buscar del formulario, le aparecer� un listado como lo indica la
                         figura 2.  
                         <br><br>
                      </td>
                 </tr>
                 <tr><td  align="center" ><img  src="<%= BASEIMG%>Listado.JPG" >                  <br>
                     <strong>Figura 2</strong></td>
                 </tr>
                 
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         <br><br><br><br>
                         Si desea modificar y/o eliminar un registro de ellos, deber� realizar click sobre registro deseado, inmediatamente el registro se cargar� en el
                         formulario tal como lo indica la figura 3.<br>
                         Se habilita el bot�n de modificar y eliminar.
                         <br><br>
                      </td>
                 </tr>
                 <tr><td  align="center" ><img  src="<%= BASEIMG%>Seleccion.JPG" >                  <br>
                     <strong>Figura 3</strong></td>
                 </tr>
                 
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         <br><br><br><br>
                         Para modificar el esquema, deber� colocar los nuevos valores y dar click en el bot�n modificar.
                        
                         Para eliminar el esquema, deber�  dar click en el bot�n eliminar. El sistema le mostrar� un mensaje de confirmaci�n
                         como lo indica la figura 4. Si realmente desea eliminarlo, deber� dar click en el bot�n Aceptar del mensaje, de lo contrario
                         en el bot�n Cancelar.
                         <br><br><br><br>
                      </td>
                 </tr>
                 <tr><td  align="center" ><img  src="<%= BASEIMG%>Confirm.JPG" >                  
                         <br>
                         <strong>Figura 4</strong>
                         <br><br>
                     </td>
                 </tr>
                 
                 
                 
                 
           </table>
            
      </td>
  </tr>
</table>
<br><br>
<img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          




</body>
</html>
