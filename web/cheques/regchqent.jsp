<%--
/******************************************************************************
 * Nombre clase :                   regchqent.jsp                             *
 * Descripcion :                    Pagina para Registrar los Cheques         *
 *                                  Entregados.                               *
 * Autor :                          Mario Fontalvo                            *
 * Fecha Creado :                   20 de octubre de 2005                     *
 * Modificado por:                  LREALES                                   *
 * Fecha Modificado:                19 de mayo de 2006, 02:11 PM              *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*,com.tsp.util.*, javax.servlet.jsp.tagext.TagExtraInfo"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%
    String fltBanco    = (String) session.getAttribute("fltBanco");
    String fltSucursal = (String) session.getAttribute("fltSucursal");
    String fltTipo     = (String) session.getAttribute("fltTipo");
    fltBanco     = (fltBanco!=null?fltBanco:"");
    fltSucursal  = (fltSucursal!=null?fltSucursal:"");
    fltTipo      = (fltTipo!=null?fltTipo:"0");
    String title = (fltTipo.equals("0") ? "Entrega" :
                    fltTipo.equals("1") ? "Envio"   :
                    fltTipo.equals("2") ? "Recibido": "");
%>

<html>
<head>
<title>Actualizaci&oacute;n Cheques</title>
<link  href="<%= BASEURL %>../../../css/estilostsp.css" rel="stylesheet">

<script src="<%= BASEURL %>/js/validarRegistroCheque.js"></script>
<script src="<%= BASEURL %>/js/boton.js"></script>
<script src="<%= BASEURL %>/js/reporte.js"></script>
<script>
<%
   Vector bancos = model.servicioBanco.obtenerBancos();
   out.print("var datos = [");
   for (int i = 0; bancos!=null && i<bancos.size(); i++){
       Banco bc = (Banco) bancos.get(i);
       out.print("\n'" + bc.getBanco()+"~"+bc.getBank_account_no() + "'" + (i+1 != bancos.size()?",":"") );
   }
   out.print("];");
   

%>
    var CONTROLLER = '<%= CONTROLLER %>';
    var BASEURL    = '<%= BASEURL    %>';
</script>

</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Registro de Cheques Entregados"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:95%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>
<br>
<div class="fila" style="position:absolute; visibility:hidden; width:350px; height:100px; z-index:1; border: 2 outset #003399; " id="msg">Mario
</div>

<form action="<%= CONTROLLER %>?estado=Fechas&accion=Cheques" method="post" name="flist" onSubmit="javascript: return _onsubmit(this);">
<table width="95%" border="2">
  <tr>
    <td>
	
		<!-- cabecera de la seccion -->
		<table width="100%" align="center" class="tablaInferior" >
			<tr>
			<td class="subtitulo1"  width="50%">Registro Cheques Entregados</td>
			<td class="barratitulo" width="50%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			</tr>
		</table>		
		<!-- fin cabecera -->	
		
		
		<!-- seccion del filtro -->
		<table width="100%" >
			<tr class="fila">
			<td width="7%">Banco </td>
			<td width="24%"> <select name="fltBanco" class="listmenu" style="width:80% " onchange="LoadSucursal(fltBanco, fltSucursal);"></select> </td>
			<td width="7%">Sucursal </td>
			<td width="24%"> <select name="fltSucursal" class="listmenu" style="width:80% "></select> </td>
			<td width="7%">Estado</td>
			<td width="24%"> 
					<select name="fltTipo" class="listmenu" style="width:60% ">
					  <option value="0">Entregados</option>
					  <option value="1">Enviados</option>
					  <option value="2">Recibidos</option>
					</select> </td>
			<td width="7%" align="center"> 
			<img name="Submit" value="Filtrar" src="<%= BASEURL %>/images/botones/buscar.gif" align="absmiddle" style = "cursor:hand" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick=" goFiltro(fltBanco.value, fltSucursal.value, fltTipo.value);">
			</td>
			</tr>
		</table>		
		<!-- fin filtro -->
		
		<% List listado = model.egresoService.getListaCheques(); 
		   if (listado!=null && listado.size()>0) {
				   String style      = "simple";
				   String position   = "bottom";
				   String index      = "center";
				   int maxPageItems  = 10;
				   int maxIndexPages = 10;
		%>			
		
		<!-- seccion del datos -->
		<table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4">
	
			<tr class="tblTitulo">
			<th width="1"><FONT size="1">Mas</FONT></th>
			<th width="12%">Banco       </th>
			<th width="18%">Sucursal    </th>
			<th width="8%">Cheque      </th>
			<th width="10%">Valor       </th>
			<th width="9%">Fecha       </th>
			<th width="28%">Beneficiario</th>
			<th width="16%">Fecha <%= title %>  </th>
			</tr>

			<pg:pager
				 items         ="<%= listado.size()%>"
				 index         ="<%= index %>"
				 maxPageItems  ="<%= maxPageItems %>"
				 maxIndexPages ="<%= maxIndexPages %>"
				 isOffset      ="<%= true %>"
				 export        ="offset,currentPageNumber=pageNumber"
				 scope         ="request">
			<%  for (int i = offset.intValue(), l = Math.min(i + maxPageItems, listado.size()); i < l; i++){
				  Egreso dt = (Egreso) listado.get(i);  

				  String fecha_chq = (dt.getPmt_date()     .equals("0099-01-01")          ?"":dt.getPmt_date());
				  String fecha_ent = (dt.getFechaEntrega() .equals("0099-01-01 00:00:00") ?"":dt.getFechaEntrega() );
				  String fecha_env = (dt.getFechaEnvio()   .equals("0099-01-01 00:00:00") ?"":dt.getFechaEnvio()   );
				  String fecha_rec = (dt.getFechaRecibido().equals("0099-01-01 00:00:00") ?"":dt.getFechaRecibido());

				  String fecha = (fltTipo.equals("0") ? fecha_ent:
				                  fltTipo.equals("1") ? fecha_env:
						          fltTipo.equals("2") ? fecha_rec: "");				  
				  				  
				  String codigo = dt.getDstrct()          + "~" + 
				                  dt.getBranch_code()     + "~" + 
								  dt.getBank_account_no() + "~" + 
								  dt.getDocument_no() ;
								  
				  String paramsShow = "'" + dt.getBranch_code()     + "'," + 
				                      "'" + dt.getBank_account_no() + "'," + 
									  "'" + dt.getDocument_no()     + "'," + 
									  "'" + fecha_chq               + "'," +
									  "'" + Util.customFormat(dt.getVlr())  + "'," + 
									  "'" + dt.getCurrency()        + "'," + 
									  "'" + dt.getPayment_name()    + "'," + 
									  "'" + fecha_ent               + "'," + 
									  "'" + fecha_env               + "'," +
									  "'" + fecha_rec               + "'" ;
			%>
				  <pg:item>
						<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'  nowrap>
							<td class="bordereporte"><img src="<%= BASEURL %>/images/iconplus.gif" onMouseMove=" showInfoChq (<%= paramsShow %>); " style = "cursor:hand" onMouseOut=" hiddenMsg(); " width="18"></td>
							<td class="bordereporte" nowrap ><%= dt.getBranch_code()     %></td>
							<td class="bordereporte" nowrap ><%= dt.getBank_account_no() %></td>
							<td class="bordereporte" nowrap align="right" ><%= dt.getDocument_no()     %></td>
							<td class="bordereporte" nowrap align="right" ><%= Util.customFormat(dt.getVlr()) %></td>
							<td class="bordereporte" nowrap align="center"><%= fecha_chq %></td>
							<td class="bordereporte" nowrap ><%= dt.getPayment_name()    %></td>
							
							
							<td class="bordereporte" nowrap >
								<input type="hidden" name="codigos" value="<%= codigo %>">
								<input type="text" id="fecha<%= i %>" name="fechas" style="width:80%;text-align:center " value="<%= fecha %>" class="textbox" readonly onKeyUp=" _keys(this) ;" >
								<span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fecha<%= i %>);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a> </span>
							</td>
						</tr>	
				  </pg:item>	
			<% } // end for %>
			<tr>
				<td colspan="8" align="center" >
					<pg:index>
					<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
					</pg:index> 
				</td>
			</tr>
			</pg:pager>
		</table>	
		<% } // end if %>
		<!-- fin datos -->				
	</td>
  </tr>
</table>

<br>
<% if ( listado!=null && listado.size()>0 ) { %>
<img name="imgActualizar" src="<%=BASEURL%>/images/botones/modificar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" if (_onsubmit(flist)) flist.submit(); ">
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
<input type="hidden" name="Opcion" value="Actualizar">
<% } %>
</form>

<script>
	LoadBancos(flist.fltBanco, flist.fltSucursal);
	flist.fltBanco.value    = '<%= fltBanco    %>';
	LoadSucursal(flist.fltBanco, flist.fltSucursal);
	flist.fltSucursal.value = '<%= fltSucursal %>';
	flist.fltTipo.value     = '<%= fltTipo     %>';
</script>
</center>
</div>
<iframe width=188 height=166 name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%=datos[1]%>
</body>
</html>
