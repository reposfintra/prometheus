<!--
- Autor : Ing. Jose De La Rosa
- Date  : 23 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que busca los cheques no existentes
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head><title>Buscar Cheque No Existente</title>
</head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Cheque No Existente"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
<form action="<%=CONTROLLER%>?estado=Cheque&accion=Buscar&noexiste=ok" method="post">
  <table width="480" border="2" align="center">
    <tr>
      <td>
	  	<table width="100%" align="center">
			<tr class="fila">
				<td width="50%" class="subtitulo1">&nbsp;Anular Cheques no Asignados</td>
				<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
			</tr>
		</table>
		<TABLE width='100%' class="tablaInferior">
			<tr class="fila">
			<td colspan="2" align="center">
				Del 
				<input   name='cheque1' type='text' id="cheque1" class="textbox" size="12">&nbsp;
				Al		
				<input name="cheque2" type="text" id="cheque2" class="textbox" size="12">
				</td>
			</tr>
			<tr class="fila">
			  <td align="center" width="19%">Banco</td>
			  <td width="81%"><%TreeMap bancos= model.buService.getListaBancos(usuario.getLogin());%>
			  <input:select name="banco" options="<%=bancos%>" attributesText="style='width:80%;'" />  </td>
			</tr>
		</TABLE>
	  </td>
    </tr>
  </table>
  <input type='hidden' value='Anular' name="Opcion" >
	<p align="center">
		<input type="image" src="<%=BASEURL%>/images/botones/buscar.gif" title="Buscar Cheques" style="cursor:hand" name="Buscar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
	</p>
<BR>
<%if(request.getAttribute("Mensaje")!=null){%>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="229" align="center" class="mensajes"><%=request.getAttribute("Mensaje")%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%}%>
<input type='hidden' name='Origen' value='FBuscar'>
</form>
</div>
</body>
</html>
