<!--  
     - Author(s)       :      Karen Reales
     - Date            :      06/05/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Ayuda
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
    <head>
        <title>Anular Cheque</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <BR>
        <table width="696"  border="2" align="center">
            <tr>
            <td width="811" valign="top" >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">
                        ANULAR CHEQUE </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> 
                        Anular y Reemplazar un cheque </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                        <td>                            Para anular y reemplazar un cheque debe primero buscar el registro a anular. <br>
                            <div align="center">
                              <img src="../images/ayuda/anularcheque/imagen1.JPG" width="597" height="255"> </div>
                            Aparecen los datos del cheque a anular para que usted este seguro que es el registro correcto. <br>	
		    
                            <div align="center">
                                <p>&nbsp;                              </p>
                          </div>
                            <p align="center"><img src="../images/ayuda/anularcheque/imagen4.JPG" width="622" height="293"> </p>
                      <br>                      </td>
                    </tr>
                </table>
            </td>
            </tr>
        </table>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
	<%=datos[1]%>
    </body>

</html>