<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRIPTIVA - Registro de Cheques Entregados
	 - Date            :      19/05/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Registro de Cheques Entregados</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Cheques</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Registro de Cheques Entregados   - Pantalla Preliminar</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL REGISTRO </td>
        </tr>
        <tr>
          <td  class="fila">'Banco'</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n 'Banco', para escoger el banco por el que se desea realizar la b&uacute;squeda.</td>
        </tr>
        <tr>
          <td  class="fila">'Sucursal'</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n 'Sucursal', que nos permite escoger por las sucursales de dicho banco escogido anteriormente.</td>
        </tr>
        <tr>
          <td  class="fila">'Estado'</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n 'Estado', aqui se puede seleccionar si los cheques a buscar han sido entregados, enviados o recibidos.</td>
        </tr>
        <tr>
          <td  class="fila">'Fecha'</td>
          <td  class="ayudaHtmlTexto">Campo para se&ntilde;alar una fecha con la que desea actualizar el cheque que ha sido, entregado, enviado o recibido.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Buscar</td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que realiza la b&uacute;squeda del registro de cheques entregados, con el filtro que se haya aplicado.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Modificar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para realizar la actualizaci&oacute;n de las fechas de los cheques.</td>
        </tr>
		<tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Registro Cheques Entregados' y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
