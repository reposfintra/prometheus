<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head><title>Impresion de Planillas</title>
<style type="text/css">
<!--
.Estilo1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<link  href='<%=BASEURL%>/css/Style.css' rel='stylesheet'>
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
<body>
    
<center>
 <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
<form action="<%=CONTROLLER%>?estado=Cheque&accion=Buscar&noexiste=ok" method="post">
<P>
<TABLE width='480' height="32%" border='1' cellpadding="2" cellspacing="1" bordercolor="#999999" class="fondotabla">
    <tr>
        <th height="14%" colspan="3" class="titulo1">ANULAR CHEQUES NO ASIGNADOS </th>
    </tr>
    <tr >
    <td height="27%" colspan="2" class="comentario"><div align="center"><br>
            <strong> DEL </strong>&nbsp;&nbsp;
            <input   name='cheque1' type='text' id="cheque1"    size="20">
            <strong> AL 
            <input name="cheque2" type="text" id="cheque2" size="20">
            <br>
</strong></div></td>
    </tr>
    <tr >
      <td width="120" height="14%" class="comentario"><strong>BANCO</strong></td>
      <td width="343" class="comentario"><%TreeMap bancos= model.buService.getListaBancos(usuario.getLogin()); 
	 
	  %>
      <input:select name="banco" options="<%=bancos%>" attributesText="style='width:100%;'" />  </td>
    </tr>
    <tr >
      <td height="17%" colspan="2" class="comentario"><div align="center">
        <input type='submit' value='Anular'  class="boton" style="width:120" name="Opcion" >
      </div></td>
    </tr>
</TABLE>
</P>
<BR>
<%if(request.getAttribute("Mensaje")!=null){%>
	<span class="Estilo1"><%=request.getAttribute("Mensaje")%></span>
<%}%>
<input type='hidden' name='Origen' value='FBuscar'>
</form>
<br>
</center>
</body>
</html>
