<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*,com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>


<html>
<head>
   <title>Impresión de Cheques</title>
    <link href="<%= BASEURL %>/css/Style.css" rel='stylesheet'>
   <link href="../css/Style.css" rel='stylesheet'>
   <link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>
	<link href="../css/estilo.css" rel="stylesheet" type="text/css">

      <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>  
      <style type="text/css">
<!--
.Estilo1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
      </style>
</head>
<body>
 <form name="form" method="post" action="<%=CONTROLLER%>?estado=Banco&accion=Cambiar">
 <%
 	String planilla = request.getParameter("planilla");
 	ImpresionCheque cheque = model.ImprimirChequeSvc.buscarCheque(planilla);
	Usuario usuario = (Usuario) session.getAttribute("Usuario");
 %>
 <TABLE width='100%' border='1' cellpadding='2' cellspacing='1' bordercolor="#CCCCCC"  class='letra'>
   <TR>
     <TH class='titulo' colspan='7' align='center' height='50'>CAMBIAR BANCO PARA CHEQUE DE LA PLANULLA NRO. <%=request.getParameter("planilla")%>
      <input name="planilla" type="hidden" id="planilla" value="<%=request.getParameter("planilla")%>"></TH>
   </TR>
   <TR class='subtitulos'>
     <TH align='center' width='5%'>PLANILLA </TH>
     <TH align='center' width='22%'>BENEFICIARIO</TH>
     <TH align='center' width='12%'> VALOR </TH>
     <TH align='center' width='8%' > MONEDA </TH>
     <TH align='center' width='22%'> INFORMACION DE BANCO </TH>
     <TH align='center' width='10%'>USUARIO</TH>
     <TH align='center' width='8%'>AGENCIA</TH>
    </TR>
 
   <TR class='fila'>
     <TD align='left'   >&nbsp; <%=cheque.getPlanilla()      %> </TD>
     <TD align='left'   >&nbsp<%= cheque.getBeneficiario() %> </TD>
     <TD align='right'  ><%=Util.customFormat(cheque.getValor()) %> </TD>
     <TD align='center' > <%= cheque.getMoneda()       %></TD>
     <TD align='center' >  
           <%TreeMap bancos= model.buService.getListaBancos(usuario.getLogin()); 
	 
	  %>
          <input:select name="banco" options="<%=bancos%>" attributesText="style='width:100%;'" default ='<%=cheque.getBanco()%>' /> 
     </font> </TD>
     <TD align='center' ><%= cheque.getUsuario()%></TD>
     <TD align='center' ><%= cheque.getAgencia()%></TD>
     <input type='hidden' name='idCheque' value='<%= cheque.getId()%>'>
   </TR>
  
  
 </TABLE>
 <div align="center"><br>
  <input name="Submit" type="submit" value="CAMBIAR">
  <input type="button" name="Submit2" value="VOLVER" onClick="window.location = '<%=CONTROLLER%>?estado=Menu&accion=ImpresionCheques&opcion=no&total=0'">
 </div>
 </form>
</body>
</html>
