<%@page session="true"%> 
<%@page import="java.util.*" %>

<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>


<html>
<head><title>Conductores</title>
  <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
  <link href="../css/estilostsp.css" rel='stylesheet'>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
  <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>
  <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/date-picker.js"></script>
  
  <script language="javascript"> 
     
    <%=  model.ConductorSvc.getVector() %>
     function llenar(){  
       var sele = formulario.departamento.value;       
       var cont=0;
       var cantidad=0;
       for(i=0;i<=datos.length-1;i++){
         var item = datos[i][0].split('-');
         if(item[2]==sele)
            cantidad++;
       }
       formulario.ciudad.length=cantidad; 
       for(i=0;i<=datos.length-1;i++){
         var item = datos[i][0].split('-');
         if(item[2]==sele){
           formulario.ciudad.options[cont].value = item[0];
           formulario.ciudad.options[cont].text  = item[1];
           cont++;
         }
       }
     }
</script>

</head>
<body >
    <center>
    <br>
     <%  
   //--- variables de bloqueos
        String estadoInsert=request.getParameter("estadoInsert");
        String estadoSearch=request.getParameter("estadoSearch");
        String estadoUpdate=request.getParameter("estadoUpdate");
        String estadoDelete=request.getParameter("estadoDelete");
        String botones = request.getParameter("botones");
        String comentario = request.getParameter("comentario");
        String bloqueoIden="";
        String bloqueoNit="";
        if(!botones.equals("") ){
          bloqueoIden="readonly='readonly'";
          bloqueoNit="readonly='readonly'";
        }
   //--- objetos
      Conductor conductor = model.ConductorSvc.getConductor();
      Nit nit = model.ConductorSvc.getNit();
    %>
    
    <FORM ACTION="<%=CONTROLLER%>?estado=Conductor&accion=Manager" METHOD='post' id='formulario' name='formulario'>
    <table width="650" border="2">
      <tr>
        <td><TABLE  width='100%'>
          <TR class="fila">
            <TD colspan="2" class="subtitulo1">DATOS BASICOS</TD>
            <TD colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></TD>
          </TR>
          <TR class="fila">
            <TD width='40%'>Estado del registro en Conductores: </TD>
            <TD width='10%'>
              <% String estadoConductor=(conductor!=null && conductor.getEstado()!=null)?conductor.getEstado():""; 
                                        String estadoA_C=(estadoConductor.equals("A"))?"selected='selected'":"";
                                        String estadoV_C=(estadoConductor.equals(""))?"selected='selected'":"" ;
                                     %>
              <select name='estadoConductor' class='textbox'>
                <option value=''  <%= estadoV_C %> ></option>
                <option value='A' <%= estadoA_C %> >A</option>
              </select>
            </TD>
            <TD width='40%' align='center'> Estado del registro en Nit: </TD>
            <TD width='*'>
              <% String estadoNit=(nit!=null && nit.getEstado()!=null)?nit.getEstado():""; 
                                        String estadoA_N=(estadoNit.equals("A"))?"selected='selected'":"";
                                        String estadoV_N=(estadoNit.equals(""))?"selected='selected'":"" ;
                                     %>
              <select name='estadoNit' class='textbox'>
                <option value=''  <%= estadoV_N%> ></option>
                <option value='A' <%= estadoA_N%> >A</option>
              </select>
            </TD>
          </TR>
        </TABLE>
          <TABLE width='100%'>
            <TR class="fila">
              <TD  width='16%'>C&eacute;dula :</TD>
              <TD width='15%'>
                <% String est1=(conductor!=null && conductor.getCedula()!=null)?conductor.getCedula():"";
									if(request.getParameter("conductor")!=null){
										est1 = request.getParameter("conductor");
									} 
                                       if(est1.equals("")) bloqueoIden="";
                                     %>
                <input  class='textbox' type='text' <%= bloqueoIden %>  value='<%=est1%>' name='identificacion' size='15' maxlength='15'>
              </TD>
              <TD class='comentario' width='15%'>&nbsp Nombre : </TD>
              <TD>
                <% String esta3=(nit!=null && nit.getName()!=null)?nit.getName():""; %>
                <input width='*' class='textbox' value='<%=esta3%>' type='text' name='nombre'  size='50' maxlength='60'>
              </TD>
            </TR>
          </TABLE>
          <TABLE width='100%' >
            <TR class='fila'>
              <TD width='16%' class="fila"> Direcci&oacute;n : </TD>
              <TD width='40%'>
                <% String esta4=(nit!=null && nit.getAddress()!=null)?nit.getAddress():""; %>
                <input class='textbox' type='text' value='<%=esta4%>'  name='direccion'    size='44' maxlength='60'>
              </TD>
              <TD width='15%' align='center' >Pa&iacute;s : </TD>
              <TD width='*'>
                <% String paisNit=(nit!=null && nit.getCountryCode()!=null)?nit.getCountryCode():""; %>
                <select name='pais' class='textbox'>
                  <option value=''>Paises...</option>
                  <%
                                                   List listPaises = model.ConductorSvc.searchPaises();
                                                   if(listPaises!=null){
                                                     Iterator itPais = listPaises.iterator();
                                                     while(itPais.hasNext()){
                                                       CodeValue pais =(CodeValue)itPais.next();
                                                       String sele3 =(paisNit.equals(pais.getCodigo()))?"selected='selected'":"";
                                                       %>
                  <option value='<%= pais.getCodigo()%>' <%= sele3%>><%= pais.getDescripcion().toUpperCase()%></option>
                  <%
                                                     }
                                                   }
                                                   %>
                </select>
              </TD>
            </TR>
          </TABLE>
          <TABLE width='100%'>
            <TR class='fila'>
              <TD width='15%' align='center' >Departamento</TD>
              <TD width='20%'>
                <% String dptoNit=(nit!=null && nit.getDptCode()!=null)?nit.getDptCode():""; %>
                <select name='departamento' class='textbox' onchange='llenar()'>
                  <option value=''>Departamentos...</option>
                  <%
                                                   List listDpto = model.ConductorSvc.searchDepartamentos();
                                                   if(listDpto!=null){
                                                     Iterator itDpt = listDpto.iterator();
                                                     while(itDpt.hasNext()){
                                                       CodeValue dpto =(CodeValue)itDpt.next();
                                                       String sele2 =(dptoNit.equals(dpto.getCodigo()))?"selected='selected'":"";
                                                       %>
                  <option value='<%= dpto.getCodigo()%>' <%= sele2%> ><%= dpto.getDescripcion().toUpperCase()%></option>
                  <%
                                                     }
                                                   }
                                                   %>
                </select>              </TD>
              <TD width='15%' align='center'> Ciudad : </TD>
              <TD width='*'>
                <% String estaCiu=(nit!=null && nit.getAgCode()!=null)?nit.getAgCode():""; %>
                <select name='ciudad' class='textbox'>
                  <%
                                                 if(!estaCiu.equals("")){
                                                     List  ciudades = model.ConductorSvc.searchCiudades();
                                                     if(ciudades!=null){
                                                       Iterator itCiu = ciudades.iterator();
                                                       while(itCiu.hasNext()){
                                                          CodeValue ciu =(CodeValue)itCiu.next();
                                                          String selecCiu =(ciu.getCodigo().equals(estaCiu))?"selected='selected'":"";
                                                          %>
                  <option value='<%= ciu.getCodigo()%>' <%=selecCiu%> ><%= ciu.getDescripcion().toUpperCase()%></option>
                  <%
                                                       }
                                                     }
                                                   }
                                                  else{%>
                  <option value=''>Ciudades...</option>
                  <%}%>
                </select>              </TD>
            </TR>
          </TABLE>
          <TABLE width='100%' cellpadding="0" cellspacing="0">
          <TR>
            <TD width='100%'><TABLE width='100%'>
                <TR class='fila'>
                  <TD width='16%'                >Telefono : </TD>
                  <TD width='15%'>
                    <% String esta5=(nit!=null && nit.getPhone()!=null)?nit.getPhone():""; %>
                    <input class='textbox' type='text' value='<%=esta5%>'  name='telefono'    size='20' maxlength='20'>                  </TD>
                  <TD width='15%' align='center' >Celular : </TD>
                  <TD width='20%'>
                    <% String esta6=(nit!=null && nit.getCellular()!=null)?nit.getCellular():""; %>
                    <input class='textbox' type='text' value='<%=esta6%>' name='celular'      size='15' maxlength='15'>                  </TD>
                  <TD width='10%' align='center' >E-Mail : </TD>
                  <TD width='*'>
                    <% String esta7=(nit!=null && nit.getEMail()!=null)?nit.getEMail():""; %>
                    <input class='textbox' type='text' value='<%=esta7%>' name='email'        size='20' maxlength='20'>                  </TD>
                </TR>
            </TABLE></TD>
          </TR>
          <TR>
            <TD><TABLE width='100%'>
                <TR class='fila'>
                  <TD width='15%' >Sexo : </TD>
                  <TD width='24%'>
                    <% String esta8=(nit!=null && nit.getSex()!=null)?nit.getSex():""; 
                                           String seleM_sexo =(esta8.toUpperCase().equals("M"))?"selected='selected'":"";
                                           String seleF_sexo =(esta8.toUpperCase().equals("F"))?"selected='selected'":"";
                                        %>
                    <select name='sexo' class='textbox'>
                      <option value='M' <%= seleM_sexo %>>Masculino</option>
                      <option value='F' <%= seleF_sexo %>>Femenino</option>
                    </select>                  </TD>
                  <TD width='20%' >Grupo Sangu&iacute;neo : </TD>
                  <TD width='10%'>
                    <% String est10 =(conductor!=null && !conductor.getRh().equals(""))?conductor.getRh().substring(0,2).trim():""; 
                                                    String tipoA =(est10.toUpperCase().equals("A"))?"selected='selected'":"";
                                                    String tipoB =(est10.toUpperCase().equals("B"))?"selected='selected'":"";
                                                    String tipoO =(est10.toUpperCase().equals("O"))?"selected='selected'":"";
                                                    String tipoAB=(est10.toUpperCase().equals("AB"))?"selected='selected'":"";
                                                 %>
                    <select name='grupoSanguineo' class='textbox'>
                      <option value='A ' <%= tipoA  %> >A</option>
                      <option value='B ' <%= tipoB  %> >B</option>
                      <option value='O ' <%= tipoO  %> >O</option>
                      <option value='AB' <%= tipoAB %> >AB</option>
                    </select>                  </TD>
                  <TD width='9%' >RH : </TD>
                  <TD width='*'>
                    <% String est11=(conductor!=null && !conductor.getRh().equals(""))?conductor.getRh().substring(2,3).trim():""; 
                                                   String rhP  = (est11.equals("+"))?"selected='selected'":"";
                                                   String rhN  = (est11.equals("-"))?"selected='selected'":"";
                                                %>
                    <select name='rh' class='textbox'>
                      <option value='+' <%= rhP %> >+</option>
                      <option value='-' <%= rhN %> >-</option>
                    </select>                  </TD>
                </TR>
            </TABLE></TD>
          </TR>
          <TR>
            <TD width='100%'><TABLE width='100%'>
                <TR class='fila'>
                  <TD width='15%'> Lugar de Nacimiento : </TD>
                  <TD width='25%'>
                    <% String est3=(conductor!=null &&  conductor.getBirthPlace()!=null)?conductor.getBirthPlace():""; %>
                    <input class='textbox' value='<%= est3%>' type='text' name='lugarNacimiento'   size='20' maxlength='50'>                  </TD>
                  <TD width='20%'> Fecha de Nacimiento : </TD>
                  <TD width='*'>
                    <% String est4=(conductor!=null && conductor.getDateBirth()!=null )?conductor.getDateBirth():""; %>
                    <input  name='fechaNacimiento' class="textbox" value='<%= est4%>' size="11" readonly="true">
                    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(fechaNacimiento);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a> </TD>
                </TR>
            </TABLE></TD>
          </TR>
          <TR>
            <TD width='100%'><TABLE width='100%'>
                <TR class='fila'>
                  <TD width='15%'> Se&ntilde;a particular :</TD>
                  <TD width='*' colspan='3'>
                    <% String est12=(conductor!=null && conductor.getParticularSign()!=null)?conductor.getParticularSign():""; %>
                    <input  class='textbox' value='<%= est12%>' type='text'  name='se&ntilde;aParticular' size='50' maxlength='50'>                  </TD>
                </TR>
            </TABLE></TD>
          </TR>
          <TR>
            <TD><TABLE width='100%'>
                <TR class='fila'>
                  <TD width='15%'> Total viajes : </TD>
                  <TD width='25%'>
                    <% String est13=(conductor!=null && conductor.getTripTotal()!=null)?conductor.getTripTotal():"0"; %>
                    <input class='textbox' value='<%= est13%>' type='text' name='totalViajes'   size='5' maxlength='4'>                  </TD>
                  <TD width='20%'> Fecha de Ingreso: </TD>
                  <TD width='*'>
                    <% String est14=(conductor!=null && conductor.getInitDate()!=null)?conductor.getInitDate():""; %>
                    <input class='textbox' value='<%= est14%>' type='text' name='fechaInicio'   readonly size='10' maxlength='10'>
                  <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(fechaInicio);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a> </TD>
                </TR>
            </TABLE></TD>
          </TR>
          <TR>
            <TD><TABLE width='100%'>
                <TR class='fila'>
                  <TD width='15%'> Documento :</TD>
                  <TD width='*'  >
                    <% String est21=(conductor!=null && conductor.getDocument()!=null)?conductor.getDocument():""; %>
                    <input class='textbox' value='<%= est21%>' style='width=100%' type='text' name='documento'   size='73' maxlength='100'>                  </TD>
                </TR>
            </TABLE></TD>
          </TR>
          <TR>
            <TD ><TABLE width='100%'>
                <TR class='fila'>
                  <TD class='subtitulos' colspan='4'>PASAPORTE</TD>
                </TR>
                <TR class='fila'>
                  <TD width='17%'> Pasaporte : </TD>
                  <TD width='15%'>
                    <% String est8=(conductor!=null && !conductor.getPassport().equals(""))?conductor.getPassport():""; %>
                    <input class='textbox' type='text' value='<%= est8%>' name='pasaporte' size='20' maxlength='20'>                  </TD>
                  <TD width='25%' > &nbsp&nbsp&nbsp Fecha Vencimiento : </TD>
                  <TD width='*'>
                    <% String est9=(conductor!=null && !conductor.getPassportExpiryDate().equals(""))?conductor.getPassportExpiryDate():""; %>
                    <input class='textbox' type='text' value='<%= est9%>'  name='fechaVencimientoPasaporte'   readonly='readonly' size='10' maxlength='10'>
                     <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(fechaVencimientoPasaporte);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a> </TD>
                </TR>
            </TABLE></TD>
          </TR>
          <TR>
            <TD width='100%'><TABLE width='100%'>
                <TR class='fila'>
                  <TD class='subtitulos' colspan='6'>PASE DE CONDUCCION</TD>
                </TR>
                <TR class='fila'>
                  <TD width='17%'                > N&uacute;mero Pase: </TD>
                  <TD width='15%'>
                    <% String est5=(conductor!=null && !conductor.getPassNo().equals(""))?conductor.getPassNo():""; %>
                    <input class='textbox' value='<%= est5%>' type='text' name='numeroPase' size='20' maxlength='20'>                  </TD>
                  <TD width='17%'  align='center'> Categoria : </TD>
                  <TD width='5%'>
                    <% int est6=(conductor!=null && !conductor.getPassCat().equals(""))?Integer.parseInt(conductor.getPassCat()):0; %>
                    <select name='categoriaPase' class='comentario'>
                      <%
                                                    for(int i=1;i<=5;i++){
                                                     String select=(i==est6)?"selected='selected'" :"";%>
                      <option  value='<%=i%>' <%= select %>><%= i%></option>
                      <%}%>
                    </select>                  </TD>
                  <TD width='25%' align='center' > Fecha Vencimiento : </TD>
                  <TD width='*'>
                    <% String est7=(conductor!=null && !conductor.getPassExpiryDate().equals(""))?conductor.getPassExpiryDate():""; %>
                    <input class='comentario' type='text' value='<%= est7%>' name='fechaVencimientoPase'  readonly='readonly'  size='10' maxlength='10'>
                    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(fechaVencimientoPase);return false;" HIDEFOCUS> </a><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(fechaVencimientoPase);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(fechaVencimientoPase);return false;" HIDEFOCUS> </a>
                    
                </TR>
            </TABLE></TD>
          </TR>
          <TR>
            <TD ><TABLE width='100%'>
                <TR class='fila'>
                  <TD class='subtitulos' colspan='4'>REFERENCIAS</TD>
                </TR>
                <TR class='fila'>
                  <TD width='17%'> Nombre : </TD>
                  <TD width='30%'>
                    <% String est15=(conductor!=null && conductor.getRefName()!=null)?conductor.getRefName():""; %>
                    <input class='textbox' value='<%= est15%>' type='text' name='nombreReferencia'   size='30' maxlength='50'>                  </TD>
                  <TD width='13%'> Direcci&oacute;n : </TD>
                  <TD width='*'>
                    <% String est16=(conductor!=null && conductor.getAddressRef()!=null)?conductor.getAddressRef():""; %>
                    <input class='textbox' value='<%= est16%>' type='text' name='direccionReferencia'  size='40' maxlength='40'>                  </TD>
                </TR>
                <TR class='fila'>
                  <TD width='17%'> Telefono : </TD>
                  <TD width='35%'>
                    <% String est17=(conductor!=null && conductor.getPhoneRef()!=null)?conductor. getPhoneRef():""; %>
                    <input class='textbox' value='<%= est17%>' type='text' name='telefonoReferencia'    size='15' maxlength='15'>                  </TD>
                  <TD width='13%'> Ciudad : </TD>
                  <TD width='35%'>
                    <% String estaCiuRR=(conductor!=null && conductor.getCityRef()!=null)?conductor.getCityRef():""; %>
                    <select name='ciudadReferencia' class='textbox'>
                      <%
                                                   List  ciudadesR = model.ConductorSvc.getCiudades();
                                                   if(ciudadesR!=null){
                                                     Iterator itCiuR = ciudadesR.iterator();
                                                     while(itCiuR.hasNext()){
                                                        CodeValue ciuR =(CodeValue)itCiuR.next();
                                                        String selecCiuRR =(ciuR.getCodigo().equals(estaCiuRR))?"selected='selected'":"";
                                                        %>
                      <option value='<%= ciuR.getCodigo()%>' <%=selecCiuRR%> ><%= ciuR.getDescripcion().toUpperCase()%></option>
                      <%
                                                     }
                                                   }
                                                   %>
                    </select>                  </TD>
                </TR>
            </TABLE></TD>
          </TR>
          <TR>
            <TD><TABLE width='100%'>
                <TR class='fila'>
                  <TD class='subtitulos' colspan='4'>JEFE INMEDIATO</TD>
                </TR>
                <TR class='fila'>
                  <TD width='17%'> Nombre : </TD>
                  <TD width='30%'>
                    <% String est18=(conductor!=null && conductor.getNameLastBoss()!=null)?conductor.getNameLastBoss():""; %>
                    <input class='textbox' value='<%= est18%>' type='text' name='nombreJefe'   size='30' maxlength='50'>                  </TD>
                  <TD width='13%'> Direcci&oacute;n : </TD>
                  <TD width='*'>
                    <% String est19=(conductor!=null && conductor.getAddressBoss()!=null)?conductor.getAddressBoss():""; %>
                    <input class='textbox' value='<%= est19%>' type='text' name='direccionJefe'  size='40' maxlength='40'>                  </TD>
                </TR>
                <TR class='fila'>
                  <TD width='17%'> Telefono : </TD>
                  <TD width='35%'>
                    <% String est20=(conductor!=null && conductor.getPhoneBoss()!=null)?conductor.getPhoneBoss():""; %>
                    <input class='textbox' value='<%= est20%>' type='text' name='telefonoJefe'    size='15' maxlength='15'>                  </TD>
                  <TD width='13%'> Ciudad : </TD>
                  <TD width='35%'>
                    <% String estaCiuJefe=(conductor!=null && conductor.getCityBoss()!=null)?conductor.getCityBoss():""; %>
                    <select name='ciudadJefe' class='textbox'>
                      <%
                                                   List  ciudadesJ = model.ConductorSvc.getCiudades();
                                                   if(ciudadesJ!=null){
                                                     Iterator itCiuJ = ciudadesJ.iterator();
                                                     while(itCiuJ.hasNext()){                    
                                                        CodeValue ciuJ =(CodeValue)itCiuJ.next();
                                                        String selecCiuJ =(ciuJ.getCodigo().equals(estaCiuJefe))?"selected='selected'":"";
                                                        %>
                      <option value='<%= ciuJ.getCodigo()%>' <%=selecCiuJ%>><%= ciuJ.getDescripcion().toUpperCase()%></option>
                      <%
                                                     }
                                                   }
                                                   %>
                    </select>                  </TD>
                </TR>
              </TABLE>
            </TD>
          </TR>
        </TABLE></td>
      </tr>
    </table>  
    <br>
   
    
    </FORM>
   <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    <!--  Presentamos los comentario de las transacciones, si los hay  -->
    
    <% if(comentario!=null && !comentario.equals("")){%>
       <script>aviso('<%= comentario %>')</script>
    <%}%>
    
</body>
</html>
