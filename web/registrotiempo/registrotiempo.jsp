<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%
    DatosPlanilla  Datos          = model.RegistroTiempoSvc.getDatosPlanilla();
    List           ListaDelay     = model.RegistroTiempoSvc.getListaDelay();
    List           ListaPlanillaT = model.RegistroTiempoSvc.getListaPlanillaTiempos();
    String         CodigoPlanilla = (Datos!=null)?Datos.getNumeroPlanilla():"";
    String         BloquearCaja   = (Datos!=null)?" readonly ":"";
    String         BloquearBoton  = (Datos!=null)?" disabled ":"";
    String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
    String Usuario = "123";    
%>

<html>
<head>
<script language='JavaScript'>
</script>
<title>Registro de Tiempos</title>
<link  href='<%=BASEURL%>/css/Style.css' rel='stylesheet'>
<script src='<%=BASEURL%>/js/validacionesRT.js'></script>
</head>
<body>
<center>


<form action="<%=CONTROLLER%>?estado=RegistroTiempo&accion=Opciones&cmd=show" method="post">
<P>
<TABLE width='617' border='1' cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fondotabla" onSubmit="jscript: return validar_form_busqueda(this);">
    <tr>
        <th colspan="4" class="titulo1">BUSQUEDA</th>
    </tr>
    <tr >
    <td width='20%' class="titulo2">REMISION No.</td>
    <td width='30%'><input type='text'   value='<%=CodigoPlanilla%>'   <%=BloquearCaja%>  style="width:100%"   name='CodigoPlanilla'></td>
    <td width='25%'><input type='submit' value='Buscar'   <%=BloquearBoton%> style="width:100%" onclick='jscript: cambiar(Opcion,this);'></td>
    <td width='25%'><input type='submit' value='Nuevo'   style="width:100%" onclick='jscript: cambiar(Opcion,this);'></td>
    </tr>
</TABLE>
</P><BR>
<input type='hidden' name='Opcion'>
</form>
<%    if (Datos!=null){  %>

     <P>
     <TABLE WIDTH="800" BORDER="1" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" CLASS="fondotabla">
    	<TR CLASS="titulo1">
    		<TH COLSPAN="4">DATOS GENERALES DE LA PLANILLA</TH>
    	</TR>
    	<TR>
    		<TD WIDTH="20%" CLASS="titulo2">FECHA</TD><TD COLSPAN="3"><%=Datos.getFechaPlanilla()%></TD>
    	</TR>
    	<TR>
    		<TD WIDTH="20%" CLASS="titulo2">ORIGEN</TD>
    		<TD WIDTH="30%"><%=Datos.getDescripcionOrigenPlanilla() %></TD>
    		<TD WIDTH="20%" CLASS="titulo2">DESTINO</TD>
    		<TD WIDTH="30%"><%=Datos.getDescripcionDestinoPlanilla() %></TD>
    	</TR>
    	<TR>
		<TD CLASS="titulo2">PLACA</TD>
		<TD ><%=Datos.getPlacaVehiculo()%></TD>
		<TD CLASS="titulo2">CONDUCTOR</TD>
		<TD ><%=Datos.getCedulaConductor()%></TD>
    	</TR>
  </TABLE>
     </P>
<% } %>



<%    
    if (ListaPlanillaT!=null && ListaPlanillaT.size()>0){ %>
<P>
<TABLE WIDTH="1000" BORDER="1" cellpadding='3' cellspacing='2' bordercolor="#CCCCCC" class="fondotabla">
       <TR class="titulo1">
         <TH COLSPAN="8">REGISTROS DE TIEMPO</TH>
       </TR>
       <TR class="titulo2">
         <TH WIDTH="15">TIMECODE</TH>
         <TH WIDTH="200">TIME DESCRIPTION</TH>
         <TH WIDTH="10">SEQ</TH>
         <TH WIDTH="200">DATE-TIME-TRAFFIC</TH>
         <TH WIDTH="150">DELAY</TH>
         <TH WIDTH="40">DEMORA</TH>
         <TH WIDTH="200">OBSERVACION</TH>
         <TH WIDTH="100">&nbsp;</TH>
       </TR>

       
       <%
            Iterator it2 = ListaPlanillaT.iterator();
            long form=0;
            while (it2.hasNext()){
                form++;
                PlanillaTiempo pt  = (PlanillaTiempo) it2.next();
                String NombreBoton = (pt.getAsignado()==true)?"Modificar":"Grabar"; 
       %>      
               <FORM ACTION='<%=CONTROLLER%>?estado=RegistroTiempo&accion=Opciones&cmd=show' METHOD='POST' NAME ="form<%=form%>" onsubmit="jscript: return validar_form_RT(this);">
               <INPUT TYPE="HIDDEN" NAME="Distrito" value="<%=pt.getDstrct()  %>">
               <INPUT TYPE="HIDDEN" NAME="Planilla" value="<%=pt.getPlanilla()%>">
               <INPUT TYPE="HIDDEN" NAME="Sj"       value="<%=pt.getSJ()      %>">
               <INPUT TYPE="HIDDEN" NAME="CF_Code"  value="<%=pt.getCFCode()  %>">
               <INPUT TYPE="HIDDEN" NAME="Usuario"  value="<%=Usuario         %>">
               <INPUT TYPE="HIDDEN" NAME='Opcion'   value="<%=NombreBoton     %>">
               <INPUT TYPE="HIDDEN" NAME="T_Code"   value="<%=pt.getTimeCode()%>">
               <INPUT TYPE="HIDDEN" NAME="Secuence" value="<%=pt.getSequence()%>">
               <TR >
                 <TD ALIGN="CENTER" ><%=pt.getTimeCode()       %></TD>
                 <TD                ><%=pt.getTimeDescription()%></TD>
                 <TD ALIGN="CENTER" ><%=pt.getSequence()       %></TD>
                 <TD id='dtt'>
                 <INPUT  TYPE="TEXT"   NAME="DTT"   STYLE="width:75%; " VALUE="<%=pt.getDateTimeTraffic() %>" READONLY>
                     <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form<%=form%>.DTT);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a>
                </TD>
                 <TD ><SELECT NAME="D_Code" STYLE="width:100%">
                 <% if (ListaDelay!=null && ListaDelay.size()>0){
                        Iterator it = ListaDelay.iterator();
                        while(it.hasNext()){
                           DatosDelay dd = (DatosDelay) it.next();
                           out.print("<OPTION VALUE='"+ dd.getCodigoDelay() +"'>"+dd.getDescripcionDelay() +"</OPTION>");
                        }
                    }
                    %>                 
                 </SELECT></TD>
                 <TD ><INPUT  TYPE="TEXT"   NAME ="Demora"      STYLE="width:100%; text-align:center" VALUE="<%=pt.getTimeDelay()  %>"     ></TD>
                 <TD ><INPUT  TYPE="TEXT"   NAME ="Observacion" STYLE="width:100%"                    VALUE="<%=pt.getObservation()%>"     ></TD>
                 <TD ><INPUT  TYPE="SUBMIT" VALUE="<%=NombreBoton%>"  CLASS="boton" style="width:100%"></TD>                 
               </TR>
               </FORM>
               <SCRIPT>                    
                    <% out.print("form"+form);  %>.D_Code.value='<%=pt.getDelayCode()%>';
               </SCRIPT>

        <%  } //end while %>
</TABLE>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</P>
<%  out.print("<script>validarTiempos();</script>");
    } 
%>
</center>
<br>
<center><b><font color='navy'><%=Mensaje%></font></b></center>
</body>
</html>
