<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%
String Mensaje = (request.getParameter("Mensaje")!=null)?  request.getParameter("Mensaje") :"";
String Archivo = (request.getParameter("PathFile")!=null)? request.getParameter("PathFile"):"";
%>

<html>
<head>
<script language='JavaScript' src='<%=BASEURL%>/js/date-picker.js'></script>
<script language='JavaScript' src='<%=BASEURL%>/js/validacionesRT.js'></script>
<title>Registro de Tiempos</title>
<link  href='<%=BASEURL%>/css/Style.css' rel='stylesheet'>
<script src='<%=BASEURL%>/js/validacionesRT.js'></script>
</head>
<body>
<center>
<p>
    <form action='<%=CONTROLLER%>?estado=Reporte&accion=RegistroTiempo&cmd=show' name='form1' method='post' onsubmit='jscript: return validar_form_reporteRT(this);'>
    <TABLE WIDTH='300' BORDER='0' cellpadding='0' cellspacing='0' class='fondotabla'>
    <TR><TH>
    <TABLE width='100%' border='1' cellpadding='3' cellspacing='2' bordercolor="#CCCCCC" class='fondotabla'>
    <TR>   
        <TR><TH COLSPAN='2' CLASS='titulo1' height='40'>REPORTE REGISTRO TIEMPO</TH></TR>
        <TR><TH COLSPAN='2' CLASS='titulo2'>BUSQUEDA DE PLANILLA</TH></TR>
        <TR>
            <TD WIDTH='30%'>Distrito</TD>
            <TD width='70%'><SELECT NAME='Distrito' CLASS='select' style='width:100%'><OPTION VALUE='FINV'>FINV</OPTION></SELECT></TD></TR>
        <TR>
            <TD>Fecha Inicial</TD>
            <TD>
                <INPUT TYPE='TEXT' NAME='FInicial' READONLY CLASS='INPUT' STYLE='width:70%' onclick="jscript: show_calendar('FInicial');">
                <a href="javascript:void(0)" onclick="jscript: show_calendar('FInicial');" HIDEFOCUS><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a>
            </TD></TR>
        <TR>
            <TD>Fecha Final</TD>
            <TD>
                <INPUT TYPE='TEXT' NAME='FFinal' READONLY CLASS='INPUT' STYLE='width:70%'>
                <a href="javascript:void(0)" onclick="jscript: show_calendar('FFinal');" HIDEFOCUS><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a>
            </TD></TR>            
        <TR>
            <TH COLSPAN='2'> 
            <INPUT TYPE='HIDDEN' VALUE='Exportar' name='Opcion'> 
            <INPUT TYPE='SUBMIT' VALUE='Excel'  style='width:120'>
            </TH></TR>
    </TABLE>
    </TH></TR></TABLE>
    </form>
</p>


<br>
<b><font color='navy'><%=Mensaje%></font></b>
<br><br>
<% if (!Archivo.equals("")) {  %>
<a href='<%= Archivo %>'>Descarguelo <%= Archivo %></a>
<% } %>
</center>
</body>
</html>