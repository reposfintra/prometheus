<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<% String var = request.getParameter("var");
   model.clienteService.searchCliente(var);
   Cliente cli = model.clienteService.getCliente();
   if(var.equals("ok")) {%>
    <script>alert("Datos Actualizados");</script>
<%}%>
<html>
<head>
<title>Actualizar Cliente</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%= BASEURL %>/js/validar.js"></script>
<script src="<%= BASEURL %>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
<form name="formCli" id="frmCliente" method="post" action="<%=CONTROLLER%>?estado=Cliente&accion=Update">
<table width="955" border="2" align="center">
    <tr>
      <td>
	  <table width="99%" align="center">
              <tr>
                <td width="50%" height="22" class="subtitulo1">Actualizar Datos Cliente</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
  <table width="99%" align="center" >
    <tr valign="bottom" class="fila">
      <td width="123"  ><strong>Estado</strong></td>
      <td width="93" ><strong>Codigo</strong></td>
      <td width="294"  ><strong>Nombre</strong></td>
      <td width="272"  ><strong>Estado del Registro </strong></td>
    </tr>
    <tr valign="top" class="fila">
      <td width="123"  ><strong>
        <input name="est" type="text" class="textbox" id="est" value="<%=cli.getEstado()%>" size="5" maxlength="1" readonly="true">
      </strong></td>
      <td  ><input name="codigo" type="text" class="textbox" id="codigo" value="<%=cli.getCodcli()%>" size="12" maxlength="6" readonly="true"></td>
      <td  ><input name="nombre" type="text" class="textbox" id="nombre" value="<%=cli.getNomcli()%>" size="45" maxlength="200" readonly="true"></td>
      <td  ><input name="reg_status" type="text" class="textbox" id="codigo5" value="<%=cli.getReg_status()%>" size="5" maxlength="1" readonly="true"></td>
    </tr>
    <tr class="fila">
      <td  ><strong>Agencia</strong></td>
      <td  ><strong>Base</strong></td>
      <td  ><strong>Fecha de Creacion </strong></td>
      <td  ><strong>Ultima Actualizaci&oacute;n </strong></td>
    </tr>
	<tr class="fila">
      <td  ><strong>
        <input name="agencia" type="text" class="textbox" id="agencia2" value="<%=cli.getAgduenia()%>" size="5" maxlength="3" readonly="true">
      </strong></td>
      <td  ><input name="base" type="text" class="textbox" id="codigo3" value="<%=cli.getBase()%>" size="12" maxlength="3" readonly="true"></td>
      <td  ><input name="fecha_cre" type="text" class="textbox" id="nombre4" value="<%=cli.getCreation_date()%>" size="30" maxlength="200" readonly="true"></td>
      <td  ><input name="fecha_act" type="text" class="textbox" id="nombre5" value="<%=cli.getLast_update()%>" size="30" maxlength="200" readonly="true"></td>
    </tr>   
    <tr class="fila">
      <td><strong>Notas</strong></td>
      <td><strong>Rentabilidad</strong></td>
      <td><strong>OC</strong></td>
      <td>&nbsp;</td>
    </tr>
	<tr valign="top" class="fila" >
      <td height="47"  ><textarea name="notas" cols="16" rows="4" readonly="true" class="textbox" id="textarea"><%=cli.getNotas()%></textarea></td>
      <td><input name="rentabilidad" type="text" class="textbox" id="rentabilidad" value="<%=cli.getRentabilidad()%>" size="13" maxlength="5"></td>
      <td><textarea name="texto_oc" cols="41" rows="4" class="textbox" id="texto_oc"><%=cli.getTexto_oc()%></textarea></td>
      <td>&nbsp;</td>
	</tr>
  </table>
  </td>  
  </tr>
  </table>
  <br>
  <div align="center">    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="formCli.submit();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
   
</form>
</body>
</html>
