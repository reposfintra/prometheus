<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
        <!--<script src="<%//=BASEURL%>/js/window_effects.js" type="text/javascript"></script>        -->
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/im.css" rel="stylesheet" type="text/css"/>
        <!--<link href="<%//=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">-->
        <!--<link href="<%//=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>-->

        <title>Documento sin t&iacute;tulo</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <style type="text/css">
            <!--
            .style3 {font-size: 36px; color:#003300}
            .style4 {font-style: italic; font-weight: bold; font-family: Times New Roman, Times, serif;}
            .Estilo1 {
                color: #FFCC33;
                font-style: italic;
                font-weight: bold;
            }
            .Estilo2 {color: #00CC00}
            -->
        </style>
    </head>
    <script>
        function AbrirPublicaciones(){
            //<%//=CONTROLLER%>?estado=Publicaciones&accion=Search
            window.location.href = "<%=CONTROLLER%>?estado=Publicaciones&accion=Search";

        }

        function showWin (msjx, ancho_mensaje, altura_mensaje, css, showProgressx){
            //openInfoDialog('<b>procesando...</b>');
            //Dialog.info(mensaje, {
            Dialog.closeInfo();//20100727
            Dialog.alert("<br><center>"+msjx+"</center>", {
                width:ancho_mensaje,
                height:altura_mensaje,
                showProgress: showProgressx,
                windowParameters: {className: css}
            });
        }

    </script>
    <body>
<%String login_into = (String)session.getAttribute("login_into");%>
        <%String mensaje = "";%>
        <form  name="forma1">
            <div align="center">
                <!--table width="718" border="0">
                    <tr>
                        <td width="610"><div align="center"></div></td>
                        < %if (model.publicacionService.getPublica().equals("S")) {%>
                        <td width="98"><div align="center"></div>
                            <div align="center" class="Estilo1" style="cursor:hand" onClick="AbrirPublicaciones()">Mire nuevas Publicaciones
                                <img src="< %=BASEURL%>/images/alert.jpg"  width="22" height="19" style="cursor:hand" >
                            </div>
                        </td>
                        < %} else {%>
                        <td width="98"><div align="center"></div>
                            <div align="center" class="Estilo1" style="cursor:hand" onClick="AbrirPublicaciones()"><span class="Estilo2">Ver  Publicaciones</span>
                                <img src="< %=BASEURL%>/images/botones/iconos/lupa.gif"  width="22" height="19" style="cursor:hand" >
                            </div>
                        </td>
                        < %}%>
                    </tr>
                </table-->
                <div align="center"> </div>
                <table width="100%" border="0">
                    <tr>
                        <td><div align="center">
                            <%
                            Usuario usuario = (Usuario) session.getAttribute("Usuario");
                            String cia_mostrable = usuario.getEmpresa(); // model.mensajeSistemaService.obtainCiaMostrable(usuario.getLogin());
                              String imagenx="";
                            if(!login_into.equals("Fenalco")&& !login_into.equals("Fenalco_bol")){
                                imagenx="<img  src = 'images/login/FINTRAVALORES.jpg' width = '600' >";
                            }

                            if (cia_mostrable.equals("CONS")) {//20100726
                            //if (  model.publicacionService.esUsuarioConsorcio(usuario.getLogin()).equals("S")){
                            //if (usuario.getLogin().equals("NAVI") || usuario.getLogin().equals("SROBINSON") || usuario.getLogin().equals("JGOMEZ") || usuario.getLogin().equals("SENTEL") || usuario.getLogin().equals("TESTER")){
                                imagenx="<img src='images/login/consorcio.png' width='500'  >";
                            }
                            else if (cia_mostrable.equals("PROV")) {//20100726
                                imagenx="<img alt='logo' src='images/login/logo_Provintegral.gif' width='800' height='258'  >";
                            }
                            else if (cia_mostrable.equals("PMND")) {//20100726
                                imagenx="<img alt='logo' src='images/login/logo_Promundo.png' width='700' height='140'  >";
                            }
                            else if (cia_mostrable.equals("STRK")) {//20100726
                                imagenx="<img alt='logo' src='images/login/logo_Selectrik_inicio.png' width='800' height='100'  >";
                            }
                            else if (cia_mostrable.equals("CEST")) {//20100726
                                imagenx="<img alt='logo' src='images/login/logo_Cobranza.png' width='800' height='113'  >";
                            }
                            else if (cia_mostrable.equals("INYM")) {//20100726
                                imagenx="<img alt='logo' src='images/login/logo_Inymec.png' width='800' height='100'  >";
                            }
                            else if (cia_mostrable.equals("FVC")) {//20100726
                                imagenx="<img src='images/login/FINTRAVALORES.jpg' width='800'  >";
                            }
                            %>
                            <%=imagenx%>
                        </div></td>
                    </tr>

                </table>
            </div>
        </form>
        
        <footer style="position: fixed; bottom: 0px ; margin: -5px -8px; width: 100%;">
          
            <img src="images/fondo-footer.png" style=" width: 100%">
        </footer> 
    </body>
</html>