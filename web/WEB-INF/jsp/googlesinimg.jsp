<%@ page session="true" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<jsp:useBean id="currentPageNumber" type="java.lang.Integer" scope="request"/>
<br>
<table width="80%" border=0 cellpadding=0 cellspacing=0>
<tr>
<td width="40%" align="right" valign=bottom nowrap><font face=Tahoma,Arial   size=-1 color="#A90A08" style="font-weight:bold ">Paginas encontradas:</font></td>
<pg:first>
<td align="right" valign=bottom nowrap>
<a href="<%= pageUrl %>"><img src="<%=BASEURL%>/images/inicio.jpg" width="8" height="8" border="0" title="INICIO"></a>&nbsp;
</td>
</pg:first>
<pg:prev export="pageUrl" ifnull="<%= true %>">
	<% if (pageUrl != null) { %>
    <td align="right" valign="bottom" nowrap>
	<A HREF="<%= pageUrl %>"><img src="<%=BASEURL%>/images/ant.gif" width="8" height="8" border="0" title="ANTERIOR"></A>&nbsp;
	</td>
	<% } %>
</pg:prev>

<pg:pages>
  <% if (pageNumber == currentPageNumber) { %>
    <td valign="bottom" align="center"><br>
    	<font face=Tahoma,Arial   size=-1 color="#A90A08" style="font-weight:bold "><%= pageNumber %></font>
	</td>
  <% } else { %>
    <td valign="bottom" align="center">
	<A HREF="<%= pageUrl %>">
		<font face=Tahoma,Arial size=-1 color="#003399" style="font-weight:bold "><%= pageNumber %></font>
	</A>
	</td>
  <% } %>
</pg:pages>

<pg:next export="pageUrl" ifnull="<%= true %>">
    <% if (pageUrl != null) { %>
	<td valign="bottom" nowrap align="left">
	&nbsp;<A HREF="<%= pageUrl %>"><br><img src="<%=BASEURL%>/images/sigte.gif" width="8" height="8" border="0" title="SIGUIENTE"></A>
  </td>
  <%}%>
</pg:next>

<pg:last>
<td align="left" valign=bottom nowrap>
&nbsp;<a href="<%= pageUrl %>"><img src="<%=BASEURL%>/images/fin.jpg" width="8" height="8" border="0" title="FIN"></a>
</td>
</pg:last>
</tr>
</table>
