<%@ page session="false" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<jsp:useBean id="currentPageNumber" type="java.lang.Integer" scope="request"/>
&nbsp;<br>
<table border=0 cellpadding=0 width=38% cellspacing=0>
<tr align=center valign=top>
<td valign=bottom><font face=arial,sans-serif size=-1>Paginas encontradas:</font></td>

<pg:prev export="pageUrl" ifnull="<%= true %>">
  <% if (pageUrl != null) { %>
    <td align=right><a href="<%= pageUrl %>"><img src=../images/atras.jpg alt="" width="26" height="33" border=0></a><A HREF="<%= pageUrl %>"><IMG SRC=../images/t.jpg alt="" width="26" height="33" border=0></A><A HREF="<%= pageUrl %>"><br>
    <b>Anterior</b></A></td>
  <% } else { %>
    <td><IMG SRC=../images/t.jpg alt="" width="26" height="33" border=0></td>
  <% } %>
</pg:prev>
<pg:pages>
  <% if (pageNumber == currentPageNumber) { %>
    <td><IMG SRC=../images/s.jpg alt="" width="26" height="33"><br>
    <font color=#A90A08><%= pageNumber %></font></td>
  <% } else { %>
    <td><A HREF="<%= pageUrl %>"><IMG
      SRC=../images/s.jpg alt="" width="26" height="33" border=0><br>
    <%= pageNumber %></A></td>
  <% } %>
</pg:pages>
<pg:next export="pageUrl" ifnull="<%= true %>">
  <% if (pageUrl != null) { %>
    <td><a href="<%= pageUrl %>"><img
      src=../images/p.jpg alt="" width="26" height="33" border=0></a><A HREF="<%= pageUrl %>"><img src=../images/delante.jpg alt="" width="26" height="33" border=0></A><A HREF="<%= pageUrl %>"><br>
      <b>Siguiente</b></A></td>
  <% } else { %>
    <td><img
      src=../images/p.jpg alt="" width="26" height="33" border=0></td>
  <% } %>
</pg:next>
</tr>
</table>
