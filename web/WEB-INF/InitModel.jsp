<%--
 Extrae las variables de aplicacion del web.xml  por medio del
 metodo init() del modelo y establece una conexion con la base de datos.
 El metodo init() es invocado una sola vez cuando se vincula el modelo
 a la sesion
--%>
<%
    com.tsp.operation.model.beans.Usuario usuario_sesion = (com.tsp.operation.model.beans.Usuario) request.getSession().getAttribute("Usuario");
    if(usuario_sesion == null) {
        throw new Exception("Su sesi�n ha expirado");
    }
    if(request.getSession().getAttribute("model") == null) {
        session.setAttribute("model", new com.tsp.operation.model.Model((usuario_sesion != null) ? usuario_sesion.getBd() : ""));
    } 
    if (request.getSession().getAttribute("modelcontab") == null) {
        session.setAttribute("modelcontab", new com.tsp.finanzas.contab.model.Model((usuario_sesion != null) ? usuario_sesion.getBd() : ""));
    }
    if(request.getSession().getAttribute("modelpto") == null) {
        session.setAttribute("modelpto", new com.tsp.finanzas.presupuesto.model.Model((usuario_sesion != null) ? usuario_sesion.getBd() : ""));
    }
    if(request.getSession().getAttribute("modelopav") == null) {
        session.setAttribute("modelopav", new com.tsp.opav.model.ModelOpav((usuario_sesion != null) ? usuario_sesion.getBd() : ""));
    }
%>
<%-- Define e inicializa el modelo --%>
<jsp:useBean
  id = "model"
  scope = "session"
  class = "com.tsp.operation.model.Model"> 
</jsp:useBean>
<%-- Define e inicializa el modelo de contabilidad --%>
<jsp:useBean
  id = "modelcontab"
  scope = "session"
  class = "com.tsp.finanzas.contab.model.Model"> 
</jsp:useBean>
<%-- Define e inicializa el modelo de presupuesto --%>
<jsp:useBean
  id = "modelpto"
  scope = "session"
  class = "com.tsp.finanzas.presupuesto.model.Model"> 
</jsp:useBean>


<%-- Define e inicializa el modelo de opav --%>
	<jsp:useBean
	id = "modelopav"
	scope = "session"
	class = "com.tsp.opav.model.ModelOpav">
</jsp:useBean>



<%response.addHeader("Cache-Control","no-cache");
    response.addHeader("Pragma","No-cache");
    response.addDateHeader ("Expires", 0);%>
<%-- Provee un alias para el servlet controlador --%>
<%
  String BASEURL = request.getContextPath();
  String CONTROLLER = BASEURL + "/controller";
  String CONTROLLERCONTAB = BASEURL + "/controllercontab";
  String CONTROLLERPTO = BASEURL + "/controllerpto";
  String CONTROLLEROPAV = BASEURL + "/controlleropav";
%>

