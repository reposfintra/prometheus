<%--
 Para tener una apariencia comun en todas las paginas de la vista JSP,
 se utiliza una cabecera estandar
--%>
<TABLE BORDER=0 CELLSPACING=3 CELLPADDING=3 WIDTH=500>
<TR>
   <TD><IMG SRC="<%= BASEURL %>/images/logo.jpg"></TD>
</TR>
<TR>
<TD CLASS="menucell" ALIGN="RIGHT">
   <A CLASS="menuitem"
      HREF="<%= BASEURL %>/reports/FiltroReporteInfoCliente.jsp">Información al Cliente</A>

   <SPAN CLASS="menuitem">|</SPAN>

   <A CLASS="menuitem"
      HREF="<%= BASEURL %>/reports/Ubicacion.jsp">Ubicación Vehicular</A>

   <SPAN CLASS="menuitem">|</SPAN>

   <A CLASS="menuitem"
      HREF="<%= BASEURL %>/reports/TiempoViaje.jsp">Tiempos de Viaje</A>
</TR>
</TABLE>
