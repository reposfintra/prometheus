<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%
  Usuario usuario = (Usuario)session.getAttribute("Usuario");
  String acceso = usuario.getAccesoplanviaje();
  TreeMap atributos = new TreeMap();
  if (acceso.equals("readonly")){
     atributos.put("readonly", acceso);
  }
%>
<html>
    <head> 
        <title>.: MODIFICAR HOJA DE REPORTE PARA CONDUCTORES :.</title>
        <script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="css/Styles.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="js/HojaRuta.js"></script>
    </head>
    <body onresize="redimensionar()" onload='redimensionar()'>
        <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=MODIFICAR HOJA DE REPORTE PARA CONDUCTORES"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
            <br>
            <br>
            <input:form name="frmsearch" method="post" action="controller?estado=HReporte&accion=Update&cmd=show" bean="hReporte" attributesText="target='_blank'">
                <table align='center' border = '1'>
                    <tr>
                    <td>
                        <table>
                            <tr align='center'>
                                <td colspan="3" align="center" class="subtitulo1">MODIFICAR HOJA DE REPORTE</td>
                                <td colspan="1" width="15%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                            <tr class='fila'>
                            <td>FECHA DE CREACION</td>
                            <td>
                                <input:text name="creation_date" attributesText="readonly"/>
                            </td>
                            <td>CREADO POR:</td>
                            <td>
                                <input:text name="creation_user" attributesText="readonly"/>
                            </td>
                            </tr>
                            <tr class='fila'>
                            <td>FECHA DE IMPRESION</td>
                            <td>
                                <input:text name="print_date" attributesText="readonly"/>
                            </td>
                            <td>IMPRESO POR:</td>
                            <td>
                                <input:text name="print_user" attributesText="readonly"/>
                            </td>
                            </tr>
                            <tr class='fila'>
                            <td>
                                PLANILLA:
                            </td>
                            <td>
                                <input:text name="planilla" attributesText="readonly"/>
                                <input:hidden name="conductor"/>
                                <input:hidden name="usuario"/> 
                            </td>
                            <td>DISTRITO :</td>
                            <td><input:text name="cia" attributesText="readonly"/></td>
                            </tr>             
                            <tr class='fila'>
                            <td>PLACA VEHICULO:
                            </td>
                            <td>
                                <input:text name="placa_vehiculo" attributes="<%=atributos%>" attributesText="maxlength='10' readonly"/>
                            </td>
                            <td>
                                PLACAUNIDAD DE CARGA:
                            </td>
                            <td>
                                <input:text name="placa_unidad_carga" attributes="<%=atributos%>" attributesText="maxlength='10'"/>
                            </td>
                            </tr>
                            <tr class='fila'>
                            <td>
                                CONTENEDOR(ES):
                            </td>
                            <td>
                                <input:text name="contenedores" attributes="<%=atributos%>" attributesText="maxlength='50'"/>
                            </td>
                            <td>
                                PRECINTO:
                            </td>
                            <td>
                                <input:text name="precinto" attributes="<%=atributos%>" attributesText="maxlength='27'"/>
                            </td>
                            </tr>
                            <tr class='fila'>
                            <td>
                                VIA:
                            </td>
                            <td>
               <%
                 TreeMap vias = model.viaService.getCbxVias(); 
               %>
                                <input:select name="ruta" options="<%=vias%>"/>
                            </td>
                            <td>
                                VIAJE VACIO:
                            </td>
                            <td>
                                <input:checkbox name="vacio" value="true" attributes="<%=atributos%>"/>
                            </td>
                            </tr>
                            <tr align='center' class='fila'>
                                <td colspan='4'>
                                    COMENTARIOS:
                                </td>
                            </tr>
                            <tr align='center' class='fila'>
                            <td colspan='4'>
                                <input:textarea name="comentarios" cols="90%" attributes="<%=atributos%>"/>
                            </td>
                            </tr>
                        </table>
                    </td>
                    </tr>
                </table>
                <%if(!acceso.equals("readonly")){%>
                <p>
                    <center>
                        <tsp:boton value='modificar' onclick='enviar()'/>
                    </center>
                </p>
                            <%}%>
            </input:form>
        </div>
    </body>
</html>
