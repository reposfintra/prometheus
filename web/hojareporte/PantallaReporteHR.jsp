<%@page contentType="text/html" session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@include file="/WEB-INF/InitModel.jsp" %>

<%! private String imprimirCasilla(String titulo, String texto){
        StringBuffer str = new StringBuffer();
        str.append("\n<table id='tablaMadre' height='100%' width='100%' border='0' bordercolor='gray'>\n");
        str.append("   <tr height='20'>\n");
        str.append("       <td class='filaSmall'>"+titulo+"<hr></td>\n");
        str.append("   </tr>\n");
        str.append("   <tr>\n");
        str.append("       <td valign='top' class='filaSmall'>"+texto+"</td>\n");
        str.append("   </tr>\n");
        str.append("</table>\n");
        return str.toString();
    }
%>
<%ReporteHojaConductor rHConductor = model.hReporteService.getRHConductor();
  String vacio = request.getParameter("vacio");
  String moneda = "";
  
  String[] pais = rHConductor.getPais().split("-_-");
  
  if( pais.length == 1 ){
    if (pais[0].equals("COLOMBIA"))
      moneda = "PESOS";
    else
      moneda = "BOLIVARES";
  }else{
      moneda = "PESOS/BOLIVARES";
  }
  
%>
<html>
    <head><title>HOJA DE REPORTE</title>
        <link href="<%=BASEURL%>/css/hReporte.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/EstilosFiltros.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script language='JavaScript' src='js/HojaRuta.js'></script>
        <script language='Javascript'> 
        if (window.Event)  
        document.captureEvents(Event.MOUSEUP);  
 
        function nocontextmenu() { 
        event.cancelBubble = true 
        event.returnValue = false; 
        return false; 
        } 

        function norightclick(e){ 
        if (window.Event){ 
        if (e.which == 2 || e.which == 3) 
        return false; 
        } 
        else 
        if (event.button == 2 || event.button == 3){ 
        event.cancelBubble = true 
        event.returnValue = false; 
        return false; 
        }	 
        } 

        document.oncontextmenu = nocontextmenu;		 
        document.onmousedown = norightclick; 
        </script> 

    </head>

    <body>
        <a href='<%=CONTROLLER%>?estado=HReporte&accion=Print&cmd=show&planilla=<%=rHConductor.getPlanilla()%>&cia=<%=rHConductor.getCia()%>'>
        <tsp:boton value = 'imprimir' name = 'imprimir' onclick = "document.imprimir.className='NoVisible';window.print();" otrosAtributos="align='right'"/>
        </a>
        <table>
        <tr height="160">
            <td>
                <table width='100%' cellpadding='0' cellspacing='0'>
                    <tr>
                        <td><img src='images/tituloHRC.bmp' name='titulo' width='500'></td>
                        <td><img name='foto' src='<%=BASEURL%>/documentos/imagenes/<%=rHConductor.getFoto()%>' align='right' border='1' height='130' width='130'><br></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table border='0' width='100%' cellspacing='0' cellpadding='0'>
                                <tr class='TablePrintDecoration'>
                                    <th width="13%" align='left'>DISTRITO:</th>
                                    <td width="20%" nowrap><%=rHConductor.getCia()%></td>
                                    <td width="10" nowrap>&nbsp;</td>
                                    <th width="13%" align='left'>PLANILLA:</th>
                                    <td width="20%" nowrap><%=rHConductor.getPlanilla()%></td>
                                    <th width="13%" align='left' nowrap>CONTENEDOR(ES):</th>
                                    <td width="20%" nowrap><%=rHConductor.getContenedores()%></td>
                                </tr>
                                <tr class='TablePrintDecoration'>
                                    <th align='left'>RUTA:</th>
                                    <td nowrap><%=rHConductor.getRuta()%></td>
                                    <td nowrap>&nbsp;</td>
                                    <th align='left' nowrap>PLACA VEHICULO:</th>
                                    <td nowrap><%=rHConductor.getPlaca_vehiculo()%></td>
                                    <th align='left' nowrap>PRECINTO:</th>
                                    <td nowrap><%=rHConductor.getPrecinto()%></td>
                                </tr>
                                <tr class='TablePrintDecoration'>
                                    <th align='left'>CONDUCTOR:</th>
                                    <td nowrap><%=rHConductor.getConductor()%></td>
                                    <td nowrap>&nbsp;</td>
                                    <th align='left' nowrap>PLACA UNIDAD DE CARGA:</th>
                                    <td nowrap><%=rHConductor.getPlaca_unidad_carga()%></td>
                                    <th align='left' nowrap>GENERADO POR:</th>
                                    <td nowrap><%=rHConductor.getUsuario()%></td>
                                </tr>
                                <tr class='TablePrintDecoration'>
                                    <th align='left'>CEDULA:</th>
                                    <td nowrap><%=rHConductor.getCedula()%></td>
                                    <td nowrap>&nbsp;</td>
                                    <th align='left' nowrap>&nbsp;</th>
                                    <td nowrap>&nbsp;</td> 
                                    <td nowrap>&nbsp;</td>
                                    <td nowrap>&nbsp;</td>
                                </tr>
                            </table>
                            <hr>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
            <%
            Vector ciudades = (Vector)rHConductor.getDetalleruta();
            int altoTablaRuta = 525;
            %>
        <tr height="<%=altoTablaRuta%>">
        <td>
            <% if ( ciudades.isEmpty() ) { %>
        <h3>La ruta establecidas no tiene ciudades o puestos de control</h3>
            <% } else { %>
        <table height="100%" width='100%' border='1' cellpadding='0' cellspacing='0'>
            <tr align='center' height='20' class='tbltitulo'><td colspan='5'><b>RUTA ESTABLECIDA</b></td></tr>
            <tr class='filaSmall'>
      <%  int i=0;
        int c = 1;
        
        String flag = null;
        if (vacio == null){
           flag = "";
        }
        else{
            flag = "(X)";
        }
        out.println("<td width='20%'>"+imprimirCasilla("SALIDA: "+ciudades.get(0),"Fecha y Hora<br><br><br>Sello PC "+flag)+"</td>"); 
        for (i=1; i<ciudades.size()-1; i++){
            if (c % 5 == 0){
                out.print("</tr><tr class='filaSmall'>");
            }
            if (i == (int)(ciudades.size()/2))
               out.println("<td width='20%'>"+imprimirCasilla(""+ciudades.get(i),"Fecha y Hora<br><br><br>Sello PC "+flag)+"</td>");
            else
               out.println("<td width='20%'>"+imprimirCasilla(""+ciudades.get(i),"Fecha y Hora<br><br><br>Sello PC")+"</td>");
            c++;
        }
        out.println("<td width='20%'>"+imprimirCasilla("LLEGADA: "+ciudades.get(i),"Fecha y Hora<br><br><br>Sello PC "+flag)+"</td>");  
        %>
            </tr>
        </table>
            <% } %>
        </td>
        </tr>
        <tr height="160">
            <td>
                <table cellpadding="0" cellspacing="0" border='0' width='100%'>
                    <tr height="9" class='TableNoteDecoration'><td height="9">EN CASO DE VARADO, RETRASO O CUALQUIER NOVEDAD EN RUTA FAVOR LLAMAR A LOS SIGUIENTES NUMEROS :</td></tr>
                    <%  
                    String[] grat = rHConductor.getLineaG().split("-_-");
                    String[] fijo = rHConductor.getTelFijo().split("-_-");
                    String[] cels = rHConductor.getTelCel().split("-_-");
                    String[] avan = rHConductor.getAvantel().split("-_-");
                    %>
                    
                    <%if( pais[0].equals("COLOMBIA") ){%>
                        <tr height="9" class='TableNoteDecoration'><td height="9">LINEAS GRATUITAS &nbsp;<%=pais[0]%>:<b><%=grat[0]%></b></td></tr>
                    <%}%>
                    <tr height="9" class='TableNoteDecoration'><td height="9">TELEFONOS DIRECTOS DEL DEPARTAMENTO DE TRAFICO &nbsp;<%=pais[0]%>:<b><%=fijo[0]%></b></td></tr>
                    <tr height="9" class='TableNoteDecoration'><td height="9">CELULARES DEL DEPARTAMENTO DE TRAFICO &nbsp;<%=pais[0]%>:<b><%=cels[0]%></b></td></tr>
                    <%if( pais[0].equals("COLOMBIA") ){%>
                        <tr height="9" class='TableNoteDecoration'><td height="9">AVANTELES DEL DEPARTAMENTO DE TRAFICO &nbsp;<%=pais[0]%>:<b><%=avan[0]%></b></td></tr>
                    <%}%>
                    <%if( pais.length > 1 ){%>
                        <!--<tr height="9" class='TableNoteDecoration'><td height="9">LINEAS GRATUITAS &nbsp;<%=pais[1]%>:<b><%=grat[1]%></b></td></tr>-->
                        <tr height="9" class='TableNoteDecoration'><td></td></tr>
                        <%if( pais[1].equals("COLOMBIA") ){%>
                            <tr height="9" class='TableNoteDecoration'><td height="9">LINEAS GRATUITAS &nbsp;<%=pais[1]%>:<b><%=grat[1]%></b></td></tr>
                        <%}%>
                        <tr height="9" class='TableNoteDecoration'><td height="9">TELEFONOS DIRECTOS DEL DEPARTAMENTO DE TRAFICO &nbsp;<%=pais[1]%>:<b><%=fijo[1]%></b></td></tr>
                        <tr height="9" class='TableNoteDecoration'><td height="9">CELULARES DEL DEPARTAMENTO DE TRAFICO &nbsp;<%=pais[1]%>:<b><%=cels[1]%></b></td></tr>
                        <%if( pais[1].equals("COLOMBIA") ){%>
                            <tr height="9" class='TableNoteDecoration'><td height="9">AVANTELES DEL DEPARTAMENTO DE TRAFICO &nbsp;<%=pais[1]%>:<b><%=avan[1]%></b></td></tr>
                        <%}%>
                    <%}%>                    
                    <tr height="9" class='TableNoteDecoration'><td></td></tr>
                    <tr height="9" class='TableNoteDecoration'><td height="9">SE�OR CONDUCTOR: EL NO REPORTE EN LOS PUESTOS DE CONTROL TENDRA SANCION DE 30.000 <%=moneda%> CADA UNO. <br> OTRAS FALTAS SERAN SANCIONADAS DE ACUERDO A LAS TARIFAS INTERNAS DE LA COMPA��A.</td></tr>
                    <tr height="9" class='TableNoteDecoration'><td></td></tr>
                    <tr>
                    <td class='filaSmall' align='center'>
                        RECUERDE QUE LA  SEGURIDAD DE LA CARGA EN RUTA ES SU RESPONSABILIDAD, POR TAL RAZ�N, AL PARAR
                        Y REINICIAR SU VIAJE,<br>ES SU DEBER HACER REVISION DEL ESTADO DE CARPAS, SELLOS DE SEGURIDAD, 
                        PRECINTOS Y EN GENERAL DEL ESTADO DE LA CARGA.<br>CUALQUIER NOVEDAD OBSERVADA, REP�RTELA DE
                        INMEDIATO A NUESTRA CENTRAL DE TRAFICO.
                    </td></tr>
                    <tr height="9" class='TableNoteDecoration'><td></td></tr>
                    <tr height="9" class='TableNoteDecoration'><td height="9">SI ESTA REALIZANDO UN VIAJE DE VACIO REPORTESE EN LOS PUESTOS DE CONTROL MARCADOS CON (X)</td></tr>
                    
                    <tr height="9" class='TableNoteDecoration'><td height="9">PARQUEADEROS AUTORIZADOS:
                    BARRANQUILLA BQ-Parq Via 40 y Parq Circunvalar, 
                    CALI CA-Parq Los Paisas - CLL 70#1-49 (TERMINALITO) y Parq CENCAR, 
                    CARTAGENA CG-Parq Colombia-Trasv 55 # 25-52 - Bajo San Isidro, 
                    MANIZALES  MN-Parq Los C�mbulos - VIA PANA SECTOR 28 VARIANTE MANIZALES FRESNO, 
                    SANTAFE DE BOGOTA  BG-Parq ILSAC -Av Ciudad de Cali (Kra 86) No 12 - 51</td></tr>
                    
                    
                    <tr class='TableNoteDecoration'><th align='left'>COMENTARIOS:<%=rHConductor.getComentario()%></th></tr>
                </table>
            </td>
        </tr>
        </table>
    </body>
</html>
