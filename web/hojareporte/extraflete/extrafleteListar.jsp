<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<% String var = request.getParameter("var");
   Vector extra = model.sjextrafleteService.listarSJExtraflete(var.toUpperCase());       
%>
<html>
<head>
<title>Mostrar SJExtraflete</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
</head>

<body>
<table width="100%" border="2" align="center">
  <tr>
    <td>  
	<table width="100%" border="1" align="center" cellpadding="0" bordercolor="#F7F5F4">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left">Informacion Relacion SJ Extraflete </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="33" height="20"></td>
  </tr>
</table>
	<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" >
    <tr valign="bottom" class="tblTitulo">
      <td width="69"  >Codigo extraflete </td>
      <td width="61" >Codigo clente</td>
      <td width="67" >Standar job No </td>
      <td width="89"  >Valor costo </td>
      <td width="72"  >Moneda costo </td>
      <td width="58"  >Fijo &oacute; variable </td>
      <td width="97"  >Reembolsable</td>
      <td width="104"  >Valor ingreso </td>
      <td width="60"  >Moneda ingreso </td>
      <td width="59"  >Fijo &oacute; variable</td>
      <td width="54"  >Clase valor </td>
      <td width="71"  >Porcentaje</td>
    </tr>
    <%for (int i=0; i<extra.size(); i++){
	      SJExtraflete sj = (SJExtraflete) extra.elementAt(i);
	%>
    <tr valign="top" class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'  style="cursor:hand" onClick="window.open('extrafleteUpdate.jsp?opcion=CL<%=sj.getCodcli()%>ST<%=sj.getStd_job_no()%>RT000-000EF<%=sj.getCod_extraflete()%>&ruta=&men=','','status=no,scrollbars=no,width=660,height=550,resizable=yes');">
      <td width="69" class="bordereporte" > <%=sj.getCod_extraflete()%> </td>
      <td width="61" class="bordereporte" > <%=sj.getCodcli()%> </td>
      <td  class="bordereporte"> <%=sj.getStd_job_no()%> </td>
      <td  class="bordereporte"> <%=sj.getValor_costo()%> </td>
      <td  class="bordereporte"> <%=sj.getMoneda_costo()%> </td>
      <td  class="bordereporte"> <%=sj.getVf_costo()%> </td>
      <td  class="bordereporte"> <%=sj.getReembolsable()%> </td>
      <td  class="bordereporte"> <%=sj.getValor_ingreso()%> </td>
      <td  class="bordereporte"> <%=sj.getMoneda_ingreso()%> </td>
      <td  class="bordereporte"> <%=sj.getVf_ingreso()%> </td>
      <td class="bordereporte" > <%=sj.getClase_valor()%> </td>
      <td  class="bordereporte"> <%=sj.getPorcentaje()%> </td>
    </tr>
    <%}%>
</table>
</td>
</tr>
</table>
  <br>
  <div align="center"></div>
</body>
</html>
