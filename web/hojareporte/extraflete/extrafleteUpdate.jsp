<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String opcion = request.getParameter("opcion");
  String ruta = request.getParameter("ruta");
  String codstd="",ori="",dest="";  
  String codcliente = opcion.substring(2,8);
  String codStd_job_no = opcion.substring(10,16);
  int posi = opcion.indexOf("EF");
  String codflete = opcion.substring(posi+2);
  SJExtraflete sj = new SJExtraflete();
  double vlr_costo = 0, vlr_ingreso = 0;
  String moneda_costo = "", vf_costo = "", moneda_ingreso = "", vf_ingreso = "", reembolso = "", clase_valor = "";
  float porc = 0;
  if (!codcliente.equals("000000")) {
      sj = model.sjextrafleteService.obtenerSJExtraflete(codcliente,codStd_job_no,codflete);  
	  vlr_costo = sj.getValor_costo();
	  moneda_costo = sj.getMoneda_costo();
	  vf_costo = sj.getVf_costo();
	  reembolso = sj.getReembolsable();
	  vlr_ingreso = sj.getValor_ingreso();
	  moneda_ingreso = sj.getMoneda_ingreso();
	  vf_ingreso =  sj.getVf_ingreso();
	  clase_valor = sj.getClase_valor();
	  porc = sj.getPorcentaje();
  }
  Vector std=null;  
  String clie = "", stdj = "", codE ="";
  String men = request.getParameter("men");
  if(men.equals("OK")){%>
     <script>alert("La informacion ha sido actualizada exitosamente")</script>
<%}else if(men.equals("ERROR")){%>
     <script>alert("No existe un registro para este cliente con este standar job y codigo de extraflete")</script>
<%}else if(men.equals("OKDEL")){%>
     <script>alert("El registro ha sido eliminado exitosamente")</script>
<%}%>
<html>
<head>
<title>Ingresar codigo extraflete</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>

<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=SJExtraflete&accion=Update" onSubmit="return ValidarFormExtraflete(this);">
	<table width="530" border="2" align="center">
    <tr>
      <td>  
	  <table width="99%" align="center">
              <tr>
                <td width="50%" height="22" class="subtitulo1">Modificar Relaci&oacute;n SJ Extraflete </td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="33" height="20"></td>
              </tr>
  </table>
	  <table width="530" align="center" class="Letras">
    <tr class="fila" >
      <td width="162" nowrap ><strong>Cliente:</strong></td>
      <td colspan="3" nowrap ><select name="cliente" disabled class="listmenu" onchange='cargarStdJob()'>
        <option value="CL000000" selected>Lista de clientes</option>
		<%int pos = opcion.indexOf("CL");
		  String codcli = opcion.substring(pos+2,8);
		  Vector cli = model.clienteService.getClientes();
		  for (int i=0; i<cli.size(); i++){		
		      Cliente c = (Cliente) cli.elementAt(i);			    
                      clie = c.getCodcli();
		%>
        <option value="CL<%=clie%>" <%if(codcli.equals(c.getCodcli())){%>selected<%}%>><%=c.getNomcli()%></option>
		<%}%>
      </select>
        <input type="hidden" name="cliente" value="<%=codcliente%>">        
      </td>
    </tr>
	<%if (ruta.equals("")) {%>
    <tr class="fila" >
      <td width="162" nowrap ><strong>Standar Job :</strong></td>
      <td colspan="3" nowrap ><select name="stdjob" disabled class="listmenu" onchange='cargarStdJob()'>
        <option value="ST000000" selected>Lista de standar jobs</option>
		<%pos = opcion.indexOf("ST");
		  codstd = opcion.substring(pos+2,16);
		  std = model.stdjobService.obtenerStdJobClientes(codcli);
		  for (int i=0; i<std.size(); i++){		
		      StdJob s = (StdJob) std.elementAt(i);			    
		%>
        <option value="ST<%=s.getstd_job_no()%>" <%if(codstd.equals(s.getstd_job_no())){%>selected<%}%>><%=s.getstd_job_desc()%></option>
		<%}%>
      </select>
        <input name="stdjob" type="hidden" id="stdjob" value="<%=codStd_job_no%>">
      </td>
    </tr>
	<%}%>
	<%if (!ruta.equals("")) {%>
	<tr class="fila" >
      <td width="162" nowrap ><strong>Standar Job :</strong></td>
      <td colspan="3" nowrap ><select name="stdjob" disabled class="listmenu" onchange='cargarStdJob()'>
        <option value="ST000000" selected>Lista de standar jobs</option>
		<%pos = opcion.indexOf("ST");
		  codstd = opcion.substring(pos+2,16);		  
		  std = model.stdjobService.obtenerStdJobRuta(ruta);
		  String ru = ruta.substring(2);
		  for (int i=0; i<std.size(); i++){		
		      StdJob s = (StdJob) std.elementAt(i);			    
		%>
        <option value="ST<%=s.getstd_job_no()%>" <%//if(ru.equals(s.getorigin_code()+"-"+s.getdestination_code())){%><%//}%>><%=s.getstd_job_desc()%></option>
		<%}%>
      </select>
        <input name="stdjob" type="hidden" id="stdjob" value="<%=codStd_job_no%>">
</td>
	</tr>
	<%}%>
    <tr class="fila" >
      <td nowrap ><strong>Ruta:</strong></td>
      <td colspan="3" nowrap ><select name="ruta" disabled class="listmenu" onChange="cargarStdJobsRuta()">
        <option value="RT000-000" selected>Lista de rutas</option>
		<%pos = opcion.indexOf("RT");
          String codRuta;
		  if (ruta.equals(""))
		      codRuta = model.stdjobService.obtenerRutaStdJob(codstd);  
		  else {
   		      codRuta = ruta.substring(2);
		  }
		  for (int i=0; i<std.size(); i++){		
		      StdJob s = (StdJob) std.elementAt(i);			    			  
		%>
        <option value="RT<%=s.getorigin_code()+"-"+s.getdestination_code()%>" <%if(codRuta.equals(s.getorigin_code()+"-"+s.getdestination_code())){%>selected<%}%>><%=s.getorigin_name()+"-"+s.getdestination_name()%></option>
		<%}%>
      </select>
</td>
    </tr>
    <tr class="fila" >
      <td nowrap ><strong>Extraflete :</strong></td>
      <td colspan="3" nowrap ><select name="extraflete" disabled class="listmenu">
        <option value="EF0000000000" selected>Lista de extrafletes</option>
        <%pos = opcion.indexOf("EF");
		  String codextra = opcion.substring(pos+2);
		  model.codextrafleteService.listarCodextraflete();
		  List ext = model.codextrafleteService.getListarCodextraflete();
		  for (int i=0; i<ext.size(); i++){		
		      Codextraflete ef = (Codextraflete) ext.get(i);			    
		%>
        <option value="EF<%=ef.getCodextraflete()%>" <%if(codextra.equals(ef.getCodextraflete())){%>selected<%}%>><%=ef.getDescripcion()%></option>
        <%}%>
      </select>
      <input name="extraflete" type="hidden" id="stdjob3" value="<%=codflete%>"></td>
    </tr>
    <tr class="fila" >
      <td nowrap ><strong>Valor costo </strong></td>
      <td width="134" nowrap ><input name="vlr_costo" type="text" class="textbox" id="vlr_costo" onKeyPress="soloDigitos(event,'decOK')"  value="<%=vlr_costo%>" size="20"></td>
      <td width="95" nowrap ><strong>Moneda costo </strong></td>
      <td width="119" nowrap ><select name="moneda_costo" class="listmenu">
        <option value="0" selected>Moneda</option>
        <%Vector mon = model.monedaService.listarMonedas();
		  for (int i=0; i<mon.size(); i++){		
		      Moneda m = (Moneda) mon.elementAt(i);			    
		%>
        <option value="<%=m.getCodMoneda()%>" <%if(moneda_costo.equals(m.getCodMoneda())){%>selected<%}%>><%=m.getNomMoneda()%></option>
        <%}%>
      </select></td>
    </tr>
	<tr class="fila" >
      <td nowrap ><strong>Valor fijo  costo </strong></td>
      <td colspan="3" nowrap ><select name="vf_costo" class="listmenu" id="vf_costo">
        <option value="0" selected>Variable � Fijo</option>
		<option value="V" <%if(vf_costo.equals("V")){%>selected<%}%>>Variable</option>
        <option value="F" <%if(vf_costo.equals("F")){%>selected<%}%>>Fijo</option>
      </select></td>
	</tr>
	<tr class="fila" >
      <td nowrap ><strong>Reembolsable:</strong></td>
      <td colspan="3" nowrap ><select name="reembolsable" id="select3">
        <option value="0" selected>Reembolsable</option>
        <option value="S" <%if(reembolso.equals("S")){%>selected<%}%>>SI</option>
        <option value="N" <%if(reembolso.equals("N")){%>selected<%}%>>NO</option>
            </select></td>
    </tr>
	<tr class="fila" >
      <td nowrap ><strong>Valor ingreso </strong></td>
      <td nowrap ><input name="vlr_ingreso" type="text" class="textbox" id="vlr_ingreso" onKeyPress="soloDigitos(event,'decOK')" value="<%=vlr_ingreso%>" size="20"></td>
      <td nowrap ><strong>Moneda ingreso </strong></td>
      <td nowrap ><select name="moneda_ingreso" class="listmenu">
        <option value="0" selected>Moneda</option>
        <%for (int i=0; i<mon.size(); i++){		
		      Moneda m = (Moneda) mon.elementAt(i);			    
		%>
        <option value="<%=m.getCodMoneda()%>" <%if(moneda_ingreso.equals(m.getCodMoneda())){%>selected<%}%>><%=m.getNomMoneda()%></option>
        <%}%>
      </select></td>
	</tr>
	<tr class="fila" >
      <td nowrap ><strong>Valor fijo ingreso  </strong></td>
      <td colspan="3" nowrap ><select name="vf_ingreso" class="listmenu" id="vf_ingreso">
        <option value="" selected>Variable � Fijo</option>
		<option value="V" <%if(vf_ingreso.equals("V")){%>selected<%}%>>Variable</option>
        <option value="F" <%if(vf_ingreso.equals("F")){%>selected<%}%>>Fijo</option>
      </select></td>
	</tr>	
	<tr class="fila" >
	  <td nowrap ><strong>Clase valor </strong></td>
	  <td colspan="3" nowrap ><select name="clase_valor" class="listmenu" id="clase_valor">
        <option value="" selected>Clase de Valor</option>
		<option value="V" <%if(clase_valor.equals("V")){%>selected<%}%>>Valor</option>
        <option value="P" <%if(clase_valor.equals("P")){%>selected<%}%>>Porcentaje</option>        
            </select></td>
	</tr>
	<tr class="fila" >
      <td nowrap ><strong>Porcentaje:</strong></td>
      <td colspan="3" nowrap ><input name="porcentaje" type="text" class="textbox" id="porcentaje" onKeyPress="soloDigitos(event,'decOK')"  value="<%=porc%>" size="5" maxlength="3">
      </td>
    </tr>
  </table>
  </td>
  </tr>
  </table>
  <br>
  <div align="center">  
  <img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod" height="21" onClick="form1.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
  <img src="<%=BASEURL%>/images/botones/eliminar.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=SJExtraflete&accion=Delete&cliente=<%=codcliente%>&stdjob=<%=codStd_job_no%>&extraflete=<%=codflete%>'" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
  <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </div>  
</form>

</body>
</html>
<script>
function cargarStdJob() {
    cli = form1.cliente.value;
    std = form1.stdjob.value;  
    rut = form1.ruta.value;  
	ext = form1.extraflete.value;  
    location.href("extraflete.jsp?opcion="+cli+std+rut+ext+"&ruta=&men=");
}
function cargarStdJobsRuta(){
    cli = form1.cliente.value;
    std = "ST000000"
    rut = form1.ruta.value;  
	ext = form1.extraflete.value;  
    location.href("extraflete.jsp?opcion="+cli+std+rut+"&ruta="+rut+&men=);
}
</script>
