<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<html>
    <head> 
        <title>.: HOJA DE REPORTE DE CONDUCTORES :.</title>
        <script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="js/HojaRuta.js"></script>
    </head>
    <body onresize="redimensionar()" onload='redimensionar()'>
        <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=HOJA DE REPORTE DE CONDUCTORES"/>
        </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

            <%-- <jsp:useBean id="beanInstanceName" scope="session" class="beanPackage.BeanClassName" /> --%>
            <%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
            <input:form name="frmpla" method="post" action="controller?estado=HReporte&accion=Search&cmd=show">
                <table border='1' align='center'>
                    <tr>
                    <td>
                        <table width='400'>
                            <tr align='center'>
                                <td colspan="2" align="center" class="subtitulo1">
                                    HOJA REPORTE
                                </td>
                                <td colspan="3" width="15%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                            <tr class='fila'>
                            <th align = 'left'>Distrito :</th>
                            <td>
                               <%
                                 Usuario usuario = (Usuario)session.getAttribute("Usuario");
                                 TreeMap cia = usuario.getListCia();
                                %>
                                <input:select name="cia" options="<%=cia%>" default="FINV"/>
                            </td>
                            <td>
                                Planilla:
                            </td>
                            <td>
                                <input:text name="planilla" attributesText="maxlength='10' size='10'"/>
                            </td>
                            <td>
                                <tsp:boton value="lupa" type="mini" onclick="validarfrmpla()"/>
                            </td>
                            </tr>
                        </table>
                    </td>
                    </tr>
                </table>
<br>
            <% String mensaje =  (String)request.getAttribute("mensaje");
       mensaje = mensaje == null? (String)request.getParameter("mensaje"): mensaje;
       if ( (mensaje != null) && (!mensaje.equals("")) ){ %>
           <table width="532" border="2" align="center">
    <tr>
      <td><table width="99%" height="27" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="410" align="center" class="mensajes"><%=mensaje%></td>
            <td width="8" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="75">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
            
                      
          
        
    <%}%>
            </input:form>
        </div>
    </body>
</html>
