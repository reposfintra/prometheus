/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
   cargarComboGenerico('unidad_negocio', 5);
   cargarComboGenerico('empresa_fianza', 30);
  // buscarTipoCartera();
   cargarComboGenerico('mora', 31);
   //cargarCuotas();
   maximizarventana();
  
    document.getElementById('filtro_avanzada').style.display = 'none';
    document.getElementById("tabla").style.marginTop = '100px';
   
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    }); 
   
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    
    $('.solo-numeric').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    
    $('.solo-numeric').live('blur',function (event) {  
           this.value = numberConComas(this.value);            
    });  
   
//    $('.boton').click(function () {        
//        if($("#mas").html()==='Mas...'){
//            $("#mas").html('Menos...');
//            $("#busqueda_avanzada").css("display","inline-block");
//        }else{
//             $("#mas").html('Mas...');
//             $("#busqueda_avanzada").css("display","none");
//        }       
//        
//        $("#page_facturas").find("table.navtable").hide();
//    });

   
    $('#buscar').click(function () {
        cargando_toggle();
        buscarFacturas();
        $("#page_facturas").find("table.navtable").show();
    });
    
//      $('#radio2').click(function () {
//         
//         $("#buscarAvanzada").hide();
//         $("#busqueda_avanzada").hide();
//    });
//    
//    $('#radio1').click(function () {
//         
//         $("#buscarAvanzada").show();
//         $("#busqueda_avanzada").hide();
//    });
    
    $('#ver_facturas').click(function () {
         exportarFacturasIndemnizadas();
    }); 
    

    $("#foto").blur(function () {
        var date = new Date();
        var mes = date.getMonth() + 1;
        var anio = date.getFullYear();
        if($("#foto").val()>(anio+""+mes)){
           mensajesDelSistema("El periodo no puede ser mayor al mes actual", '250', '150', true);
        }

    });
    
   /* $("#fecha_inicio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });*/
    $('#ui-datepicker-div').css('clip', 'auto');
    
});

function busqueda_avanzada(imagen) {
    switch (imagen) {
        case 'mas':
            var imagen1 = new Image;
            imagen1.src = '/fintra/images/Minus.png';
            document.images['mas'].src = imagen1.src;
            document.getElementById('mas').id = 'menos';
            document.getElementById("busqueda").style.height = '174px';
            document.getElementById("divInterno").style.height = '149px';
            document.getElementById('filtro_avanzada').style.display = 'block';
            document.getElementById("tabla").style.marginTop = '179px';
            break;
        case 'menos':
            var imagen1 = new Image;
            imagen1.src = '/fintra/images/More.png';
            document.images['menos'].src = imagen1.src;
            document.getElementById('menos').id = 'mas';
            document.getElementById("busqueda").style.height = '89px';
            document.getElementById("divInterno").style.height = '65px';
            document.getElementById('filtro_avanzada').style.display = 'none';
            document.getElementById("tabla").style.marginTop = '100px';
            break;
    }
}


function buscarFacturas() {

    if ($("#unidad_negocio").val() !== '' && $("#empresa_fianza").val() !== '' && $("#foto").val() !=='') {
        
        //setTimeout(function(){
            var caption = "Facturas";
            var grid_facturas = $("#tbl_facturas");
            if ($("#gview_tbl_facturas").length) {
                reloadGridFacturas(grid_facturas);
            } else {

                grid_facturas.jqGrid({
                    caption: caption,
                    url: "./controller?estado=Garantias&accion=Comunitarias",
                    mtype: "POST",
                    datatype: "json",
                    height: '500',
                    width: 'auto',
                    colNames: ['Linea Negocio', '# Pagar�', 'Cod cliente', 'Identificacion', 'Nombre Cliente', 'Foto', '# Factura', 'Negocio', 'Cuota', 'Fecha vencimiento',
                                'Altura Mora', 'Dias mora', 'Vlr factura', 'Saldo Capital' , 'Saldo Seguro','Int. Corriente','Cat','Cuota Admin','Int. de Mora','GAC','Saldo Total','Vlr a desistir',
                                'Fecha Indemnizacion', 'Convenio', 'Cuenta', 'Id linea negocio','Cartera en'],
                    colModel: [
                        {name: 'nombre_linea_negocio', index: 'nombre_linea_negocio', width: 90, align: 'center'},                  
                        {name: 'num_pagare', index: 'num_pagare', width: 70, align: 'center'},
                        {name: 'codcli', index: 'codcli', width: 80, sortable: true, align: 'center',hidden:true},
                        {name: 'nit_cliente', index: 'nit_cliente', width: 80, sortable: true, align: 'left'},
                        {name: 'nombre_cliente', index: 'nombre_cliente', sortable: true, width: 210, align: 'left'},
                        {name: 'periodo_foto', index: 'periodo_foto', width: 70, align: 'center', hidden:true},                   
                        {name: 'documento', index: 'documento', width: 70, align: 'center', key: ($('#chk_acelerar_pagare').is(":checked"))? false:true},
                        {name: 'negocio', index: 'negocio', width: 70, align: 'center', key: ($('#chk_acelerar_pagare').is(":checked"))? true:false},
                        {name: 'cuota', index: 'cuota', width: 50, align: 'center'},
                        {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 100, align: 'center'},
                        {name: 'altura_mora', index: 'altura_mora', width: 100, align: 'left'},
                        {name: 'dias_mora', index: 'dias_mora', width: 70, align: 'center'},
                        {name: 'valor_factura', index: 'valor_factura', width: 100, align: 'right',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                        {name: 'valor_saldo_capital', index: 'valor_saldo_capital', width: 100, align: 'right',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                        {name: 'valor_saldo_seguro', index: 'valor_saldo_seguro', width: 100, align: 'right',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                        {name: 'valor_saldo_mi', index: 'valor_saldo_mi', width: 90, align: 'right',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                        {name: 'valor_saldo_ca', index: 'valor_saldo_ca', width: 90, align: 'right',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                        {name: 'valor_saldo_cm', index: 'valor_saldo_cm', width: 90, align: 'right',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                        {name: 'ixm', index: 'ixm', width: 90, align: 'right',formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                        {name: 'gac', index: 'gac', width: 90, align: 'right',formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},                    
                        {name: 'total_saldo', index: 'total_saldo', width: 100, align: 'right',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                        {name: 'valor_desistir', index: 'valor_desistir', width: 100, align: 'right', editable: true, hidden:true,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                        {name: 'fecha_indemnizacion', index: 'fecha_indemnizacion', width: 100, align: 'center', hidden:true},
                        {name: 'convenio', index: 'convenio', width: 60, align: 'center', hidden:true},
                        {name: 'cuenta', index: 'cuenta', width: 70, align: 'center', hidden:true},
                        {name: 'ref_4', index: 'ref_4', width: 80, align: 'center',hidden:true},                   
                        {name: 'cartera_en', index: 'cartera_en', width: 80, align: 'center',hidden:true}
                    ],
                    rowNum: 1000000,
                    rowTotal: 1000000,
                    loadonce: true,
                    rownumWidth: 40,
                    gridview: true,
                    pager: '#page_facturas',
                    viewrecords: true,
                    hidegrid: false,
                    shrinkToFit: false,
                    footerrow: false,
                    multiselect: true,
                    cellsubmit: "clientArray",
                    editurl: 'clientArray'
                    ,ajaxGridOptions: {                
                        dataType: "json",
                        type: "POST",  
                        async:false,
                        data: {
                            opcion: 11,
                            mora: $("#mora").val(),
                            linea_negocio: $("#unidad_negocio").val(),
                            foto: $("#foto").val(),
                            cartera_en:$("#cartera_en").val(),
                            empresa_fianza: $("#empresa_fianza").val(),
                            cedula_cliente:$('#cedula_cliente').val(),
                            negocio:$('#negocio').val(),
                            monto_inicial: numberSinComas($("#monto_inicial").val()),
                            monto_final: numberSinComas($("#monto_final").val()),
                            acelerar_pagare:$('#chk_acelerar_pagare').is(":checked") ? "true":"false",
                            aplica_gac:$('#chk_gac').is(":checked") ? "true":"false",
                            aplica_ixm:$('#chk_ixm').is(":checked") ? "true":"false"

                        }
                    },loadComplete: function () {
                       //cargando_toggle();
                       if(grid_facturas.jqGrid('getGridParam', 'records') > 0) {
                            deshabilitarPanelBusqueda(true);
                        }else{
                             mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                        }
                    },
                    ondblClickRow: function (rowid, iRow, iCol, e) {
                        if ($('#chk_acelerar_pagare').is(":checked")) {
                            cargarInfoDetalladaNegocio(rowid);
                            AbrirDivInfoNegocio();
                        } 
                    },
                    loadError: function (xhr, status, error) {                  
                        alert(error, 250, 150);
                    },
                    gridComplete: function (index) {
                        cargando_toggle();
                    },
                    onSortCol: function (index, columnIndex, sortOrder) {
                        setTimeout(function(){
                            cargando_toggle();
                         //return 'stop';
                        },500);
                    }
                }).navGrid("#page_facturas", {add: false, edit: false, del: false, search: false, refresh: false}, {});

                //boton que sirve apra desbloquear el panel de buqueda
                $("#tbl_facturas").navButtonAdd('#page_facturas', {
                    caption: "Desbloquear",
                    title: "desbloquear",
                    onClickButton: function (e) {
                        deshabilitarPanelBusqueda(false);
                        $("#tbl_facturas").jqGrid('GridUnload');      
                        //grid_facturas.jqGrid("clearGridData", true);
                    }
                });

                $("#tbl_facturas").navButtonAdd('#page_facturas', {
                    caption: "Exportar Excel",
                    title: "Exportar Excel",
                    onClickButton: function (e) {
                        exportarExcel(grid_facturas);
                    }
                });

               /*$("#tbl_facturas").navButtonAdd('#page_facturas', {
                    caption: "Generar Notificaci�n",
                    title: "Generar Notificaci�n a Cliente",
                    onClickButton: function (e) {
                        generarNotificacion('32670979');
                    }
                });*/      

                $("#tbl_facturas").navButtonAdd('#page_facturas', {
                    caption: "Indemnizar",
                    title: "indemnizar",
                    onClickButton: function (e) {
                        indemnizaNegocios(grid_facturas);
                    }
                });
                $("#tbl_facturas").hideCol("valor_desistir");
                $("#tbl_facturas").jqGrid('setGridWidth', '1338');

                if($('#chk_acelerar_pagare').is(":checked")){
                    $("#tbl_facturas").hideCol("documento");
                    $("#tbl_facturas").hideCol("cuota");
                    $("#tbl_facturas").hideCol("fecha_vencimiento");
                    //$("#tbl_facturas").hideCol("altura_mora");
                    //$("#tbl_facturas").hideCol("dias_mora");
                }else{
                    $("#tbl_facturas").showCol("documento");
                    $("#tbl_facturas").showCol("cuota");
                    $("#tbl_facturas").showCol("fecha_vencimiento");
                    //$("#tbl_facturas").showCol("altura_mora");
                    //$("#tbl_facturas").showCol("dias_mora"); 
                }


        }
  //  },300);
    } else {
        mensajesDelSistema("Debe seleccionar la linea de negocio, la empresa y el periodo de corte", "300", "150");
    }

}

function reloadGridFacturas(grid_facturas) {

    grid_facturas.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Garantias&accion=Comunitarias",
        autowidth: true,
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: 11,
                mora: $("#mora").val(),
                linea_negocio: $("#unidad_negocio").val(),
                foto: $("#foto").val(),
                cartera_en:$("#cartera_en").val(),
                empresa_fianza: $("#empresa_fianza").val(),
                cedula_cliente:$('#cedula_cliente').val(),
                negocio:$('#negocio').val(),
                monto_inicial: numberSinComas($("#monto_inicial").val()),
                monto_final: numberSinComas($("#monto_final").val()),
                acelerar_pagare:$('#chk_acelerar_pagare').is(":checked") ? "true":"false",
                aplica_gac:$('#chk_gac').is(":checked") ? "true":"false",
                aplica_ixm:$('#chk_ixm').is(":checked") ? "true":"false"
            }
        }
    });
    grid_facturas.trigger("reloadGrid");

   
        var $td = $(grid_facturas[0].p.pager + '_left ' + 'td[title="indemnizar"]');
        $td.remove();
        $td = $(grid_facturas[0].p.pager + '_left ' + 'td[title="desistir"]');
        $td.remove();
         
        $("#tbl_facturas").navButtonAdd('#page_facturas', {
            caption: "Indemnizar",
            title: "indemnizar",
            onClickButton: function (e) {
                indemnizaNegocios(grid_facturas);
            }
        });
        $("#tbl_facturas").hideCol("valor_desistir");
        $("#tbl_facturas").jqGrid('setGridWidth','1338');
        
        if ($('#chk_acelerar_pagare').is(":checked")) {
            $("#tbl_facturas").hideCol("documento");
            $("#tbl_facturas").hideCol("cuota");
            $("#tbl_facturas").hideCol("fecha_vencimiento");
            $("#tbl_facturas").hideCol("altura_mora");
            $("#tbl_facturas").hideCol("dias_mora");
        } else {
            $("#tbl_facturas").showCol("documento");
            $("#tbl_facturas").showCol("cuota");
            $("#tbl_facturas").showCol("fecha_vencimiento");
            $("#tbl_facturas").showCol("altura_mora");
            $("#tbl_facturas").showCol("dias_mora");
        }
   
  /*  if ($('input:radio[name=radios]:checked').val() === '12') {
        var $td = $(grid_facturas[0].p.pager + '_left ' + 'td[title="desistir"]');
        $td.remove();
        $td = $(grid_facturas[0].p.pager + '_left ' + 'td[title="indemnizar"]');
        $td.remove();
       

        $("#tbl_facturas").navButtonAdd('#page_facturas', {
            caption: "Desistir",
            title: "desistir",
            onClickButton: function (e) {
                desistirFacturas(grid_facturas);
            }
        });
         $("#tbl_facturas").showCol("valor_desistir");
         $("#tbl_facturas").jqGrid('setGridWidth','1435');
    }*/

}

//function indemnizaFacturas(grid_facturas) {
//    
//    var i, selRowIds = $("#tbl_facturas").jqGrid("getGridParam", "selarrrow"), rowData;
//    if (selRowIds.length > 0) {
//        
//        loading("Espere un momento procesando facturas.", "270", "140");
//        var arrayJson = new Array();
//        for (i = 0; i < selRowIds.length; i++) {
//            rowData = $("#tbl_facturas").jqGrid("getLocalRow", selRowIds[i]);
//            arrayJson.push(rowData);
//        }       
//
//        setTimeout(function () {
//            $.ajax({
//                async: false,
//                url: "./controller?estado=Garantias&accion=Comunitarias",
//                type: 'POST',
//                dataType: 'json',
//                data: {
//                    opcion: 13,
//                    listJson: JSON.stringify(arrayJson),
//                    mora: $("#mora").val(),
//                    perido_corte: $("#foto").val(),
//                    empresa_fianza:$("#empresa_fianza").val(),
//                    acelerar_pagare:$('#chk_acelerar_pagare').is(":checked") ? "true":"false",
//                    aplica_gac:$('#chk_gac').is(":checked") ? "true":"false"
//                },
//                success: function (json) {
//                    
//                    if (json.error) {
//                        mensajesDelSistema(json.error, '250', '180');
//                        $("#dialogLoading").dialog('close');
//                        return;
//                    }
//
//                    if (json.respuesta === 'OK') {                   
//                        deshabilitarPanelBusqueda(false);
//                        grid_facturas.jqGrid("clearGridData", true);
//                        $("#dialogLoading").dialog('close');
//                        mensajesDelSistema('Proceso de indemnizaci�n ha iniciado. Favor revisar el log de proceso', '300', '150', true);
//                    } else {
//                        mensajesDelSistema(json.respuesta, '300', '150', true);
//                        $("#dialogLoading").dialog('close');
//                    }
//
//                }, error: function (xhr, ajaxOptions, thrownError) {
//                    $("#dialogLoading").dialog('close');
//                    alert("Error: " + xhr.status + "\n" +
//                            "Message: " + xhr.statusText + "\n" +
//                            "Response: " + xhr.responseText + "\n" + thrownError);
//                }
//            });
//
//        }, 500);
//
//    }else{
//        
//        mensajesDelSistema("No hay datos seleccionados para indemnizar", '300', '150', true);
//    }
//}

function desistirFacturas(grid_facturas) {

    var i, selRowIds = $("#tbl_facturas").jqGrid("getGridParam", "selarrrow"), rowData;
    
    if (selRowIds.length > 0) {
        
      if(validateGridDesistimiento()){
       loading("Espere un momento procesando facturas.", "270", "140");
        
       var arrayJson = new Array();
        for (i = 0; i < selRowIds.length; i++) {
            rowData = $("#tbl_facturas").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }    

        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Garantias&accion=Comunitarias",
                type: 'POST',
                dataType: 'json',
                data: {
                    opcion: 14,
                    listJson: JSON.stringify(arrayJson)
                },
                success: function (json) {

                    if (json.respuesta === 'TRUE') {
                        reloadGridFacturas(grid_facturas);
                        $("#dialogLoading").dialog('close');
                    } else {
                        mensajesDelSistema(json.respuesta, '300', '150', true);
                        $("#dialogLoading").dialog('close');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);
      }
    } else {
        mensajesDelSistema("No hay datos seleccionados para desistir", '300', '150', true);
    }

}

function exportarExcel(grid_facturas) {
    
    var i, selRowIds = $("#tbl_facturas").jqGrid("getGridParam", "selarrrow"), rowData;
    if (selRowIds.length > 0) {
        
      //  loading("Espere un momento procesando excel.", "270", "140");
        var arrayJson = new Array();
        for (i = 0; i < selRowIds.length; i++) {
            rowData = $("#tbl_facturas").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }
        
        var opt = {
            autoOpen: false,
            modal: true,
            width: 220,
            height: 150,
            title: 'Descarga Reporte'
        };
        //  $("#divSalidaEx").dialog("open");
        $("#divSalidaEx").dialog(opt);
        $("#divSalidaEx").dialog("open");
        $("#imgloadEx").show();
        $("#msjEx").show();
        $("#respEx").hide();
        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Garantias&accion=Comunitarias",
                type: 'POST',
                dataType: "html",
                data: {
                    opcion: 16,
                    listado: JSON.stringify(arrayJson),
                    tipo:$('input:radio[name=radios]:checked').val()
                },
                success: function (resp) {
                    $("#imgloadEx").hide();
                    $("#msjEx").hide();
                    $("#respEx").show();
                    var boton = "<div style='text-align:center'>\n\ </div>";                   
                    document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;


                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);

    }else{
        
        mensajesDelSistema("No hay datos seleccionados para exportar", '300', '150', true);
    }
}

function exportarFacturasIndemnizadas() {    
  
        
        var opt = {
            autoOpen: false,
            modal: true,
            width: 250,
            height: 150,
            title: 'Descarga Reporte'
        };
        //  $("#divSalidaEx").dialog("open");
        $("#divSalidaEx").dialog(opt);
        $("#divSalidaEx").dialog("open");
        $("#imgloadEx").show();
        $("#msjEx").show();
        $("#respEx").hide();
        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Garantias&accion=Comunitarias",
                type: 'POST',
                dataType: "html",
                data: {
                    opcion: 15                   
                },
                success: function (resp) {
                    $("#imgloadEx").hide();
                    $("#msjEx").hide();
                    $("#respEx").show();
                    var boton = "<div style='text-align:center'>\n\ </div>";                   
                    document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;


                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);

  
}

function cargarComboGenerico(idCombo, option) {
    $('#'+idCombo).html('');
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Garantias&accion=Comunitarias",
        dataType: 'json',
        async:false,
        data: {
            opcion: option
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#'+idCombo).append("<option value=''>Seleccione</option>");
                    for (var key in json) {              
                       $('#'+idCombo).append('<option value=' + key + '>' + json[key] + '</option>');                
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#'+idCombo).append("<option value=''>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function buscarTipoCartera(){
    $.ajax({
        type: 'POST',
        url:"./controller?estado=Contabilidad&accion=General",
        dataType: 'json',
        data: {
            opcion: 4
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                try {
                    $('#cartera_en').empty();

                    for (var key in json) {
                        $('#cartera_en').append('<option value=' + json[key].codigo + '>' + json[key].nombre + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                  mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });    
    
}

function cargarVencimientos(){
    $('#mora').html('');
    $('#mora').append("<option value=''>Seleccione</option>");
    var i = 0;
    while(i < 360){
        i = i + 30;
        $('#mora').append('<option value=' + i + '>' + i  + ' d�as</option>');
    }
}


function validaRangoMontos(){
    var estado = true;
    if(($('#monto_inicial').val()==='' && $('#monto_final').val()!=='')||($('#monto_inicial').val()!=='' && $('#monto_final').val()==='')){
        mensajesDelSistema("Rango ingresado inv�lido (Campo Monto).Por favor, verifique!!", '250', '150');
        estado=false;
    }else if(parseFloat($('#monto_inicial').val())>parseFloat($('#monto_final').val())){
        mensajesDelSistema("EL monto inicial no puede ser superior al final.Por favor, verifique!!", '250', '150');
        estado=false;
    }
    return estado;
}

function validateGridDesistimiento() {
    var estado = true;
    var i, selRowIds = $("#tbl_facturas").jqGrid("getGridParam", "selarrrow"), rowData;

    for (i = 0; i < selRowIds.length; i++) {
        rowData = $("#tbl_facturas").jqGrid("getLocalRow", selRowIds[i]);
        var valor_desistir = rowData.valor_desistir;
        if (parseFloat(valor_desistir) === 0) {
            estado = false;            
            mensajesDelSistema('Por favor, ingrese valor a desistir en fila ' + (i + 1), '250', '150');
        }
    }
    return estado;
}

function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function deshabilitarPanelBusqueda(status) {

    var nodes = document.getElementById("busqueda").getElementsByTagName('*');
    for (var i = 0; i < nodes.length; i++)
    {
        nodes[i].disabled = status;
    }

}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}



function generarCxCGarantias(){
    
    var i, selRowIds = $("#tbl_facturas").jqGrid("getGridParam", "selarrrow"), rowData;
    if (selRowIds.length > 0) {
        
        loading("Espere un momento generando pdf.", "270", "140");
        var arrayJson = new Array();
        for (i = 0; i < selRowIds.length; i++) {
            rowData = $("#tbl_facturas").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }

        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Garantias&accion=Comunitarias",
                type: 'POST',
                dataType: 'json',
                data: {
                    opcion: 17,
                    listJson: JSON.stringify(arrayJson),
                    mora: $("#mora").val(),
                    fecha_corte: $("#fecha_inicio").val()
                },
                success: function (json) {

                    if (json.respuesta === 'OK') {
                        window.open('.' + json.Ruta);
                        $("#dialogLoading").dialog('close');
                    } else {
                        mensajesDelSistema("Lo sentimos ocurri� un error al generar el cuenta de cobro!!", '250', '150');
                        $("#dialogLoading").dialog('close');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);

    }else{
        
        mensajesDelSistema("No hay datos seleccionados para generar la cuenta de cobro", '300', '150', true);
    }
 }
 
 function generarNotificacion(nit_cliente) {
 
        var url = './controller?estado=Garantias&accion=Comunitarias';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 18,
                nit_cliente: nit_cliente
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        window.open('.' + json.Ruta);
                    }
                } else {
                    mensajesDelSistema("Lo sentimos ocurri� un error al generar notificaci�n!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
 }
 
function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function AbrirDivInfoNegocio() {
     $("#div_info_negocio").dialog({
        width: 1330,
        height: 470,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'DETALLE NEGOCIO',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $(".ui-dialog-titlebar-close").hide();
}


function cargarInfoDetalladaNegocio(negocio) {     
      
    var grid_tbl_detalle_negocio = jQuery("#tabla_info_negocio");
     if ($("#gview_tabla_info_negocio").length) {        
        refrescarGridInfoDetalladaNegocio(negocio);
     }else { 
        grid_tbl_detalle_negocio.jqGrid({
            caption: "",
            url: "./controller?estado=Garantias&accion=Comunitarias",           	 
            datatype: "json",  
            height: '300',
            width: '1230', 
            colNames: ['Identificacion', 'Nombre Cliente',  'Negocio', 'Documento', 'Cuota','Fecha Ven','Dias Mora','Valor Fac', 'Valor Saldo K','Saldo Seguro',
                        'Saldo Interes','Saldo CAT','Saldo CM','IxM','GaC','Total Saldo','Estado','Altura Mora'],
            colModel: [
                {name: 'nit_cliente', index: 'nit_cliente', width: 90, align: 'left'},                 
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 90, align: 'left'},                 
                {name: 'negocio', index: 'negocio', width: 70, align: 'center'},
                {name: 'documento', index: 'documento', width: 70, align: 'left', key: true},
                {name: 'cuota', index: 'cuota', width: 50, align: 'center'},
                {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 50, align: 'center'},
                {name: 'dias_mora', index: 'dias_mora', width: 50, align: 'center'},                
                {name: 'valor_factura', index: 'valor_factura', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},              
                {name: 'valor_saldo_capital', index: 'valor_saldo_capital', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}, classes: "cvteste"},  
                {name: 'valor_saldo_seguro', index: 'valor_saldo_seguro', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}, classes: "cvteste"},                  
                {name: 'valor_saldo_mi', index: 'valor_saldo_mi', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}, classes: "cvteste" },              
                {name: 'valor_saldo_ca', index: 'valor_saldo_ca', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}, classes: "cvteste"},              
                {name: 'valor_saldo_cm', index: 'valor_saldo_cm', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}, classes: "cvteste"}, 
                {name: 'IxM', index: 'IxM', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}, classes: "cvteste"},              
                {name: 'GaC', index: 'GaC', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}, classes: "cvteste"},              
                {name: 'total_saldo', index: 'total_saldo', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}, classes: "cvteste"},             
                {name: 'estado', index: 'estado', width: 70, align: 'center'} ,          
                {name: 'altura_mora', index: 'altura_mora', width: 70, align: 'center'} 
            ],
            rowNum: 1000,
            rowTotal: 1000,           
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rowTotals: true,
            rownumbers: false,
            pgtext: null,
            pgbuttons: false, 
            cellsubmit: 'clientArray',
            editurl: 'clientArray',           
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                    opcion: 32,   
                    mora: $("#mora").val(),
                    linea_negocio: $("#unidad_negocio").val(),
                    foto: $("#foto").val(),
                    cartera_en:$("#cartera_en").val(),
                    empresa_fianza: $("#empresa_fianza").val(),
                    cedula_cliente:$('#cedula_cliente').val(),
                    negocio:$('#negocio').val(),
                    monto_inicial: numberSinComas($("#monto_inicial").val()),
                    monto_final: numberSinComas($("#monto_final").val()),
                    acelerar_pagare:$('#chk_acelerar_pagare').is(":checked") ? "true":"false",
                    aplica_gac:$('#chk_gac').is(":checked") ? "true":"false",
                    aplica_ixm:$('#chk_ixm').is(":checked") ? "true":"false",
                    cod_neg: negocio
                }
            },   
            gridComplete: function() {             
                cacularTotalesFooter(jQuery("#tabla_info_negocio"));
            },     
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });  
    }
  }
  
    function refrescarGridInfoDetalladaNegocio(negocio) {
        jQuery("#tabla_info_negocio").setGridParam({
            url: "./controller?estado=Garantias&accion=Comunitarias",
            datatype: 'json',
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 32, 
                    mora: $("#mora").val(),
                    linea_negocio: $("#unidad_negocio").val(),
                    foto: $("#foto").val(),
                    cartera_en:$("#cartera_en").val(),
                    empresa_fianza: $("#empresa_fianza").val(),
                    cedula_cliente:$('#cedula_cliente').val(),
                    negocio:$('#negocio').val(),
                    monto_inicial: numberSinComas($("#monto_inicial").val()),
                    monto_final: numberSinComas($("#monto_final").val()),
                    acelerar_pagare:$('#chk_acelerar_pagare').is(":checked") ? "true":"false",
                    aplica_gac:$('#chk_gac').is(":checked") ? "true":"false",
                    aplica_ixm:$('#chk_ixm').is(":checked") ? "true":"false",
                    cod_neg: negocio
                }
            }
        });

        jQuery('#tabla_info_negocio').trigger("reloadGrid");
    }
    
function cacularTotalesFooter(grid_listar_negocios) {

    grid_listar_negocios.jqGrid('footerData', 'set', {
        dias_mora: 'Total:',
        valor_factura:grid_listar_negocios.jqGrid('getCol', 'valor_factura', false, 'sum'),
        valor_saldo_capital: grid_listar_negocios.jqGrid('getCol', 'valor_saldo_capital', false, 'sum'),
        valor_saldo_seguro: grid_listar_negocios.jqGrid('getCol', 'valor_saldo_seguro', false, 'sum'),
        valor_saldo_mi: grid_listar_negocios.jqGrid('getCol', 'valor_saldo_mi', false, 'sum'),
        valor_saldo_ca: grid_listar_negocios.jqGrid('getCol', 'valor_saldo_ca', false, 'sum'),
        valor_saldo_cm: grid_listar_negocios.jqGrid('getCol', 'valor_saldo_cm', false, 'sum'),
        IxM: grid_listar_negocios.jqGrid('getCol', 'IxM', false, 'sum'),
        GaC: grid_listar_negocios.jqGrid('getCol', 'GaC', false, 'sum'),
        total_saldo: grid_listar_negocios.jqGrid('getCol', 'total_saldo', false, 'sum')

    });

}

function indemnizaNegocios(grid_facturas) {

    var i, selRowIds = $("#tbl_facturas").jqGrid("getGridParam", "selarrrow"), rowData;
    if (selRowIds.length > 0) {

        loading("Espere un momento procesando negocios.", "270", "140");
        var arrayJson = new Array();
        for (i = 0; i < selRowIds.length; i++) {
            rowData = $("#tbl_facturas").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }
        var mora = $("#mora").val() === "" ? '90' : $("#mora").val();
        var perido_corte = $("#foto").val();
        var empresa_fianza = $("#empresa_fianza").val();
        var acelerar_pagare = $('#chk_acelerar_pagare').is(":checked") ? true : false;
        var aplica_gac = $('#chk_gac').is(":checked") ? true : false;
        var aplica_ixm = $('#chk_ixm').is(":checked") ? true : false;
        var unidad_negocio = $("#unidad_negocio").val();
        var cedula_cliente = $('#cedula_cliente').val();
        var negocio = $('#negocio').val();        
        var monto_inicial = numberSinComas($("#monto_inicial").val());
        var monto_final = numberSinComas($("#monto_final").val());    

        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Garantias&accion=Comunitarias",
                type: 'POST',
                dataType: 'json',
                data: {
                    opcion: 36,
                    listJson: JSON.stringify({
                        info: arrayJson,
                        empresa_fianza: empresa_fianza,
                        unidad_negocio: unidad_negocio,
                        aplica_gac: aplica_gac,
                        aplica_ixm: aplica_ixm,
                        acelerar_pagare: acelerar_pagare,
                        perido_corte: perido_corte,
                        mora: mora,
                        cedula_cliente: cedula_cliente,
                        negocio: negocio,
                        monto_inicial: monto_inicial,
                        monto_final: monto_final
                    })
                },
                success: function (json) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        $("#dialogLoading").dialog('close');
                        return;
                    }
                    console.log(json.respuesta);
                    if (json.respuesta === 'OK') {  
                        exportarExcelIndemnizado();
                        deshabilitarPanelBusqueda(false);
                        grid_facturas.jqGrid("clearGridData", true);
                        $("#dialogLoading").dialog('close');
                       // mensajesDelSistema('Proceso de indemnizaci�n se ha realizado', '300', '150', true);
                        
                    } else {
                        mensajesDelSistema(json.respuesta, '300', '150', true);
                        $("#dialogLoading").dialog('close');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);

    } else {

        mensajesDelSistema("No hay datos seleccionados para indemnizar", '300', '150', true);
    }
}


function exportarExcelIndemnizado() {
    var opt = {
        autoOpen: false,
        modal: true,
        width: 220,
        height: 150,
        title: 'Descarga Reporte'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        async: false,
        url: "./controller?estado=Garantias&accion=Comunitarias",
        type: 'POST',
        dataType: "html",
        data: {
            opcion: 37
        },
        success: function (resp) {
            cargando_toggle();
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;


        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}