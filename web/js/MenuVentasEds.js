/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function Menu(nombreMenu, opciones){
    this.nombreMenu = nombreMenu;
    this.opciones = opciones;
    this.generarMenu = function(){
        var menu = this.cabecera(this.nombreMenu);
        for(i = 0; i<this.opciones.length;i++){
            menu += this.itemOpcion(opciones[i]);
        }
        menu += this.cerrarSesion();
        return menu;
    };
    this.cabecera = function(nombre){
        return  '<li class = "sidebar-brand">'+
                    '<a>'+ nombre +'</a>'+
                '</li>';
    };
    this.itemOpcion = function(opcion){
        return '<li>'+
                    '<a class="menu-item" ruta-pagina="'+opcion.ruta+'">'+opcion.descripcion+'</a>'+
               '</li>';
    };
    this.cerrarSesion = function(){
        return '<li>'+
                    '<a href="#" onclick=" cerrarSesionEDS(); ">Cerrar Sesion</a>'+
               '</li>';
    };
}

 function cerrarSesionEDS() {
    var token = localStorage.getItem("token");
    $.ajax({
        type: "POST",
        crossDomain: true,
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        headers: {'token': token},
        async: false,
        url: 'http://prometheus.fintra.co:8094/ApiServerEds/webresources/login/salir', 
        success: function(jsonData) {
                if(jsonData.success===true ){
                    mensajesDelSistema(jsonData.info.data, '250', '165', true);     
                    setTimeout(function() {
                        location.reload(true);
                    }, 1000);                      
                } else {
                    mensajesDelSistema(jsonData.message.error, '250', '165');
                    setTimeout(function() {
                        location.reload(true);
                    }, 1000); 
                }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });   
}

