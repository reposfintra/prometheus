/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {  
    
    $('.solo-numero').keyup(function() {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    
    if ($('#div_transxdia').is(':visible')) {
        
        $("#startDatePicker").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
            maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
            defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
        });
        $("#endDatePicker").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
            maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
            defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
        });

        var myDate = new Date();
        $("#startDatePicker").datepicker("setDate", myDate);
        $("#endDatePicker").datepicker("setDate", myDate);
        $('#ui-datepicker-div').css('clip', 'auto');
        
        $('#tbl_trans_xdia tbody tr').live('dblclick', function(event) {    
            var num_venta = $(this).find('td:eq(2)').text();    
            if(num_venta!==''){
                ventanaVentasDetalle();
                verDetalleTransaccionxdia(num_venta);
            }           
        });

    }
    
    cargar_Transportadoras();
    
    $('#btn_save_info_venta').click(function() {
      guardarInfoVenta();     
    });
    
    $('#btn_search_planillas').click(function(){
         buscarPlanilla();
    });
    
    $('#btn_show_ventas').click(function() {
        mostrarTransaccionesxdia();
    });
 });



function cargar_Transportadoras(){
    //Con Ajax
    var token =  localStorage.getItem("token");
    $('#transportadora').html('');
    $.ajax({
        type: "POST",
        crossDomain: true,
        dataType: "json",            
        contentType: "application/x-www-form-urlencoded",
        headers: {'token': token },   
        async: false,
        url: 'http://prometheus.fintra.co:8094/ApiServerEds/webresources/anticipo/transportadoras', 
        success: function(jsonData) {
                if(jsonData.success===true ){
                    var json = jsonData.info.data;
                    try {
                           
                        $('#transportadora').append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#transportadora').append('<option value=' + json[key].id + '>' + json[key].razon_social + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }                   
                } else {                
                    mensajesDelSistema(jsonData.message.error, '320', '165');                    
                }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });         
    
}

function buscarPlanilla(){
    var idtransportadora = $("#transportadora").val();
    var planilla = $("#numplanilla").val().trim();
    var idconductor = $("#idconductor").val();
    var numplaca = $("#numplaca").val().trim();
    if (idtransportadora == '' ){
        mensajesDelSistema("DEBE SELECCIONAR LA TRANSPORTADORA", '250', '165');
    }else if(planilla == ''){
        mensajesDelSistema("DEBE INGRESAR LA PLANILLA", '250', '165');
    }else if(idconductor == ''){
         mensajesDelSistema("DEBE INGRESAR ID DEL CONDUCTOR", '250', '165');
    }else if(numplaca == ''){
         mensajesDelSistema("DEBE INGRESAR PLACA DEL VEHICULO", '250', '165');
    }else{        
         //Ajax para cargar jqgrid informacion de venta
        var token =  localStorage.getItem("token");
        $.ajax({
            type: "POST",
            crossDomain: true,
            dataType: "json",
            contentType: "application/x-www-form-urlencoded",
            headers: {'token': token},
            data: {'data': buscarPlanillaToJSON()},
            async: false,
            url: 'http://prometheus.fintra.co:8094/ApiServerEds/webresources/anticipo/venta',
            success: function(jsonData) {
                if (jsonData.success === true) {
                    $('#ideds').val('');
                    $('#nombre_eds').val('');
                    $('#idmanifiesto').val('');
                    $('#planilla').val('');                   
                    $('#anticipo').val('');
                    $('#disponible').val('');
                    $('#kilometraje').val('');                   
                    $('#disponible').removeClass('letraVerde');
                    var json = jsonData.info.data.productos;                 
                    $('#div_enc_planilla').fadeIn();
                    $('#div_enc_planilla').attr('title', 'PROCESO DE VENTA - Planilla No:' + jsonData.info.data.planilla);
                    $('#ideds').val(jsonData.info.data.id_estacion);
                    $('#nombre_eds').val(jsonData.info.data.nombre_eds);
                    $('#idmanifiesto').val(jsonData.info.data.id_manifiesto);
                    $('#planilla').val(jsonData.info.data.planilla);                    
//                  $('#anticipo').val('$ '+numberConComas(getPartNumber(parseFloat(jsonData.info.data.disponible).toFixed(2),'int',2)))+'.'+getPartNumber(parseFloat(jsonData.info.data.disponible).toFixed(2),'frac',2);
//                  $('#disponible').val('$ '+numberConComas(getPartNumber(parseFloat(jsonData.info.data.disponible).toFixed(2),'int',2)))+'.'+getPartNumber(parseFloat(jsonData.info.data.disponible).toFixed(2),'frac',2);                   
                    $('#anticipo').val('$ '+numberConComas(parseFloat(jsonData.info.data.disponible).toFixed(2)));
                    $('#disponible').val('$ '+numberConComas(parseFloat(jsonData.info.data.disponible).toFixed(2))); 
                    buscarInformacionVenta(json);
                } else {
                    mensajesDelSistema(jsonData.message.error, '320', '180');
                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        }); 

    }
    
}

function buscarInformacionVenta(data) {   
    var grid_tbl_info_venta = jQuery("#tabla_info_venta");
     if ($("#gview_tabla_info_venta").length) {
        refrescarGridInfoVenta();
     }else {
        grid_tbl_info_venta.jqGrid({
            caption: "DETALLE DE VENTAS",
            cellsubmit: "clientArray",
            data: data,
            datatype: "local",
            height: '290',
            width: '700',
            cellEdit: true,
            colNames: ['Id','Producto','Und Medida', 'Precio($) x Und', 'Cantidad', 'Total', 'Edita_Precio'],
            colModel: [
                {name: 'id_producto', index: 'id_producto', width: 80, align: 'left', key: true, hidden: true},
                {name: 'nombre_producto', index: 'nombre_producto', width: 212, align: 'left'},
                {name: 'unidad_medida', index: 'unidad_medida', width: 120, align: 'center'},              
                {name: 'precio_xunidad', index: 'precio_xunidad', sortable: true, editable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 5, prefix: "$ "}},
                {name: 'cantidad', index: 'cantidad', width: 80, editable: true, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 5}},
                {name: 'total', index: 'total', sortable: true, width: 120, align: 'right', search: false, sorttype: 'number',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 5, prefix: "$ "}},   
                {name: 'editar_precio', index: 'editar_precio', width: 80, align: 'center', hidden:true}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_info_venta'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },afterSaveCell: function(rowid, celname, value, iRow, iCol) {                
                if (celname === 'precio_xunidad' || celname === 'cantidad') {
                    var precio = $("#tabla_info_venta").getRowData(rowid).precio_xunidad;
                    var cantidad = $("#tabla_info_venta").getRowData(rowid).cantidad;
                  
                    (isNaN(value)) ? $("#tabla_info_venta").jqGrid("restoreCell", iRow, iCol) : grid_tbl_info_venta.jqGrid('setCell', rowid, 'total', precio*cantidad); 
                    
                    var colSum = grid_tbl_info_venta.jqGrid('getCol', 'total', false, 'sum');
                    var valorAnticipo = $('#anticipo').val().substring(2, $('#anticipo').val().length);
                    var anticipo = numberSinComas(valorAnticipo); 
                    if (colSum > anticipo) { 
                        setTimeout(function() {
                            mensajesDelSistema("EL VALOR INGRESADO EXCEDE EL DISPONIBLE", '320', '165');
                        }, 500);   
                        $("#tabla_info_venta").jqGrid("restoreCell", iRow, iCol);
                        var precio = $("#tabla_info_venta").getRowData(rowid).precio_xunidad;
                        var cantidad = $("#tabla_info_venta").getRowData(rowid).cantidad;
                        $("#tabla_info_venta").jqGrid('setCell', rowid, 'total', parseFloat(precio * cantidad).toFixed(5));
                        return;      
                      
                    }else{                                          
                        $('#disponible').addClass('letraVerde');                        
                       // $('#disponible').val('$ '+numberConComas(getPartNumber(parseFloat(anticipo - colSum).toFixed(2),'int',2))+'.'+getPartNumber(parseFloat(anticipo - colSum).toFixed(2),'frac',2));
                       $('#disponible').val('$ '+numberConComas(parseFloat(anticipo - colSum).toFixed(2)));
                 }
                    grid_tbl_info_venta.jqGrid('footerData', 'set', {total: colSum});   
                }
            },
            loadComplete: function() {
                var ids = grid_tbl_info_venta.jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var id = ids[i];
                    grid_tbl_info_venta.jqGrid('setCell', id, 'cantidad', 0);
                    grid_tbl_info_venta.jqGrid('setCell', id, 'total', 0);
                    if (grid_tbl_info_venta.jqGrid('getCell', id, 'editar_precio') === 'N') {
                        grid_tbl_info_venta.jqGrid('setCell', id, 'precio_xunidad', '', 'not-editable-cell');                        
                    }
                }
            },
            gridComplete: function() {             
                var colSum = grid_tbl_info_venta.jqGrid('getCol', 'total', false, 'sum');               
                grid_tbl_info_venta.jqGrid('footerData', 'set', {total: colSum});                
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_info_venta", {add: false, edit: false, del: false, search: false, refresh: false}, {});
    }  
       
}

function refrescarGridInfoVenta(data){   
    jQuery("#tabla_info_venta").setGridParam({
        data: data,
        datatype: "local",
    });
    
    jQuery('#tabla_info_venta').trigger("reloadGrid");
}

 function guardarInfoVenta() {
    var ideds = $("#ideds").val();
    var nomeds = $("#nombre_eds").val();
    var planilla = $("#planilla").val();
    var idmanifiesto = $("#idmanifiesto").val();
    var kilometraje = $("#kilometraje").val();
    var disponible = $("#disponible").val();
    var anticipo = $("#anticipo").val();
    var filasId = jQuery('#tabla_info_venta').jqGrid('getGridParam', 'data');
    var colSum = jQuery('#tabla_info_venta').jqGrid('getCol', 'total', false, 'sum');
    
    if (kilometraje == '') {
          mensajesDelSistema("DEBE INGRESAR EL KILOMETRAJE", '250', '165');
    }else if (kilometraje==0){
          mensajesDelSistema("EL KILOMETRAJE DEBE SER MAYOR A CERO", '250', '165');
    }else{
        if (filasId.length > 0 && anticipo !== disponible && colSum>0) {    
            var token = localStorage.getItem("token");
            $.ajax({
                type: "PUT",
                crossDomain: true,
                dataType: "json",
                contentType: "application/x-www-form-urlencoded",
                headers: {'token': token},
                data: {'data': guardarVentaToJSON()},
                async: false,
                url: 'http://prometheus.fintra.co:8094/ApiServerEds/webresources/anticipo/venta/guardar',
                success: function(jsonData) {
                    if (jsonData.success === true) {
                        mensajesDelSistema(jsonData.info.data, '320', '180', true);   
                        cargarPagina('jsp/logistica_transporte/eds/buscarPlanillas.jsp');
                    } else {
                        mensajesDelSistema(jsonData.message.error, '320', '165');
                    }

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        } else {
            mensajesDelSistema("NO HAY INFORMACION DE VENTAS POR ACTUALIZAR", '250', '180');
        }

    }
}

function mostrarTransaccionesxdia() {  
    var idtransportadora = $("#transportadora").val();
    var fechaini = $("#startDatePicker").val();
    var fechafin = $("#endDatePicker").val();
    if (idtransportadora == '') {
        mensajesDelSistema("DEBE SELECCIONAR LA TRANSPORTADORA", '250', '165');
    }else if(fechaini == ''){
        mensajesDelSistema("DEBE SELECCIONAR LA FECHA INICIAL", '250', '165');
    }else if (fechafin == ''){
         mensajesDelSistema("DEBE SELECCIONAR LA FECHA FINAL", '250', '165');
    }else{
        if (fechafin >= fechaini){
            var token = localStorage.getItem("token");
            $.ajax({
                type: "POST",
                crossDomain: true,
                dataType: "json",
                contentType: "application/x-www-form-urlencoded",
                headers: {'token': token},
                data: {'data': transxdiaToJSON()},
                async: false,
                url: 'http://prometheus.fintra.co:8094/ApiServerEds/webresources/reporte/ventas',
                success: function(jsonData) {
                    if (jsonData.success === true) {
                        var json = jsonData.info.data;
                        var i = 0, total = 0;
                        $('#tbl_trans_xdia').html('');
                        $('#tbl_trans_xdia').append('<tr><th>#</th> <th>Fecha</th> <th>No Venta</th> <th>Transportadora</th> <th>Planilla</th> <th>No Placa</th> <th>Subtotal</th> </tr>');
                        for (var key in json) {
                            i++;
                            $('#tbl_trans_xdia').append('<tr>');
                            $('#tbl_trans_xdia tr:last').append('<td align="center">' + i + '</td>');
                            $('#tbl_trans_xdia tr:last').append('<td align="center">' + json[key].fecha_venta + '</td>');
                            $('#tbl_trans_xdia tr:last').append('<td align="center">' + json[key].num_venta + '</td>');
                            $('#tbl_trans_xdia tr:last').append('<td align="center">' + json[key].razon_social + '</td>');
                            $('#tbl_trans_xdia tr:last').append('<td align="center">' + json[key].planilla + '</td>');
                            $('#tbl_trans_xdia tr:last').append('<td align="center">' + json[key].placa + '</td>');
                            $('#tbl_trans_xdia tr:last').append('<td align="center">' + '$' + numberConComas(json[key].sub_total) + '</td>');
                            $('#tbl_trans_xdia').append('</tr>');
                            total += json[key].sub_total;

                        }
                        $('#tbl_tot_trans_xdia').html('');
                        $('#tbl_tot_trans_xdia').append('<tr><th colspan="2">Totales</th></tr>');
                        $('#tbl_tot_trans_xdia').append('<tr>');
                        $('#tbl_tot_trans_xdia tr:last').append('<td width="85%" align="right"><b>CANTIDAD:</b></td>');
                        $('#tbl_tot_trans_xdia tr:last').append('<td align="right">' + numberConComas(i) + '</td>');
                        $('#tbl_tot_trans_xdia').append('</tr>');
                        $('#tbl_tot_trans_xdia').append('<tr>');
                        $('#tbl_tot_trans_xdia tr:last').append('<td width="85%" align="right"><b>TOTAL:</b></td>');
                        $('#tbl_tot_trans_xdia tr:last').append('<td align="right">' + '$' + numberConComas(total) + '</td>');
                        $('#tbl_tot_trans_xdia').append('</tr>');
                    } else {
                        mensajesDelSistema(jsonData.message.error, '320', '165');
                    }

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            }); 
         }else{
              mensajesDelSistema("LA FECHA FINAL NO PUEDE SER MENOR A LA INICIAL", '250', '165');
         }       

    } 
}

function verDetalleTransaccionxdia(num_venta) {  
        var token =  localStorage.getItem("token");
        $('#tbl_det_trans_xdia').html('');
        $.ajax({
            type: "POST",
            crossDomain: true,
            dataType: "json",            
            contentType: "application/x-www-form-urlencoded",
            headers: {'token': token },
            data: {'data': JSON.stringify({
                    'num_venta': num_venta
            })},
            async: false,
            url: 'http://prometheus.fintra.co:8094/ApiServerEds/webresources/reporte/detalle', 
            success: function(jsonData) {
                    if(jsonData.success===true ){
                        var json = jsonData.info.data;
                        var i = 1, total = 0;                       
                        $('#tbl_det_trans_xdia').append('<tr><th>#</th> <th>Producto</th> <th>Q</th> <th>Und de Medida</th> <th>Precio(S) x Und</th> <th>Subtotal</th> </tr>');
                        for (var key in json) {
                            $('#tbl_det_trans_xdia').append('<tr>');
                            $('#tbl_det_trans_xdia tr:last').append('<td align="center">' + i + '</td>');
                            $('#tbl_det_trans_xdia tr:last').append('<td align="left">' + json[key].nombre_producto + '</td>');
                            $('#tbl_det_trans_xdia tr:last').append('<td align="center">' + numberConComas(json[key].cantidad_suministrada) + '</td>');
                            $('#tbl_det_trans_xdia tr:last').append('<td align="center">' + json[key].nombre_unidad + '</td>');
                            $('#tbl_det_trans_xdia tr:last').append('<td align="right">' + '$' + numberConComas(json[key].precio_xunidad) + '</td>');
                            $('#tbl_det_trans_xdia tr:last').append('<td align="right">' + '$' + numberConComas(json[key].sub_total) + '</td>');
                            $('#tbl_det_trans_xdia').append('</tr>');
                            total += parseFloat(json[key].sub_total);
                            i++;
                        }
                        $('#tbl_det_trans_xdia').append('<tr>');
                        $('#tbl_det_trans_xdia tr:last').append('<td colspan="5" align="right"><b>TOTAL:</b></td>');
                        $('#tbl_det_trans_xdia tr:last').append('<td align="right">' + '$' + numberConComas(total) + '</td>');
                        $('#tbl_det_trans_xdia').append('</tr>');                       
                    } else {
                        mensajesDelSistema(jsonData.message.error, '320', '165');
                    }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        }); 
        
}


function ventanaVentasDetalle() {
    
    $("#dialogTransxdiaDet").dialog({
        width: 650,
        height: 390,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: true, 
        buttons: {          
            "Salir": function () {          
		$(this).dialog("close");               
            }
        }
    });
}


function buscarPlanillaToJSON(){
    return JSON.stringify({
        "id_transportadora": $("#transportadora").val(),
        "planilla": $("#numplanilla").val(),
        "cedula_conductor": $('#idconductor').val(),
        "placa": $('#numplaca').val()
    });
}

function guardarVentaToJSON(){
       
       var jsonObj = {"id_estacion": parseInt($('#ideds').val()),
                      "nombre_eds": $('#nombre_eds').val(),
                      "id_manifiesto": parseInt($('#idmanifiesto').val()),
                      "planilla": $('#planilla').val(),
                      "kilometraje": parseInt($('#kilometraje').val()),
                      "productos": []};
                  
        var filasId = jQuery('#tabla_info_venta').jqGrid('getDataIDs');       
        if (filasId != '') {
            for (var i = 0; i < filasId.length; i++) {   
                 if ($("#tabla_info_venta").getRowData(filasId[i]).total > 0){
                    var idproducto = filasId[i];
                    var nomproducto = $("#tabla_info_venta").getRowData(filasId[i]).nombre_producto;
                    var precio = $("#tabla_info_venta").getRowData(filasId[i]).precio_xunidad;
                    var cantidad = $("#tabla_info_venta").getRowData(filasId[i]).cantidad;
                    var total = $("#tabla_info_venta").getRowData(filasId[i]).total;
                    var item = {};
                    item ["id_producto"] = parseInt(idproducto);
                    item ["nombre_producto"] = nomproducto;
                    item ["precio_xunidad"] = parseFloat(precio).toFixed(5);
                    item ["cantidad"] = parseFloat(cantidad).toFixed(5);
                    item ["total"] = parseFloat(total).toFixed(5);
                    jsonObj.productos.push(item);
                 }               
            }  
        }
        return JSON.stringify(jsonObj); 
     
}

function transxdiaToJSON(){   
    return JSON.stringify({
        "id_transportadora": $("#transportadora").val(),
        "fecha_inicio": $("#startDatePicker").val(),
        "fecha_fin": $('#endDatePicker').val()
    });
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function getPartNumber(number,part,decimals) {
  if ((decimals <= 0) || (decimals == null)) decimals =1;
  decimals = Math.pow(10,decimals);

  var intPart = Math.floor(number);
  var fracPart = (number % 1)*decimals;
  fracPart = fracPart.toFixed(0);
  if (part == 'int')
    return intPart;
  else
    return fracPart;
}

function cargarPagina(pagina) {
    $.ajax({
        type: 'POST',
        url: "" + pagina,
        dataType: 'html',
        success: function(html) {
            $("#container").html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: "Mensaje",
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("close");             
            }
        }
    });
}
