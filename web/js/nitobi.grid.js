if(typeof (nitobi)=="undefined"||typeof (nitobi.lang)=="undefined"){
alert("The Nitobi framework source could not be found. Is it included before any other Nitobi components?");
}
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.build="7054";
nitobi.grid.version="3.5.7054";
nitobi.lang.defineNs("nitobi.ui");
nitobi.ui.Scrollbar=function(){
this.uid="scroll"+nitobi.base.getUid();
};
nitobi.ui.Scrollbar.prototype.render=function(){
};
nitobi.ui.Scrollbar.prototype.attachToParent=function(_1,_2,_3){
this.UiContainer=_1;
this.element=_2||nitobi.html.getFirstChild(this.UiContainer);
if(this.element==null){
this.render();
}
this.surface=_3||nitobi.html.getFirstChild(this.element);
this.element.onclick="";
this.element.onmouseover="";
this.element.onmouseout="";
this.element.onscroll="";
nitobi.html.attachEvent(this.element,"scroll",this.scrollByUser,this);
};
nitobi.ui.Scrollbar.prototype.align=function(){
var vs=document.getElementById("vscroll"+this.uid);
var dx=-1;
if(nitobi.browser.MOZ){
dx=-3;
}
nitobi.drawing.align(vs,this.UiContainer.childNodes[0],269484288,-42,0,24,dx,false);
};
nitobi.ui.Scrollbar.prototype.scrollByUser=function(){
this.fire("ScrollByUser",this.getScrollPercent());
};
nitobi.ui.Scrollbar.prototype.setScroll=function(_6){
};
nitobi.ui.Scrollbar.prototype.getScrollPercent=function(){
};
nitobi.ui.Scrollbar.prototype.setRange=function(_7){
};
nitobi.ui.Scrollbar.prototype.getWidth=function(){
return nitobi.html.getScrollBarWidth();
};
nitobi.ui.Scrollbar.prototype.getHeight=function(){
return nitobi.html.getScrollBarWidth();
};
nitobi.ui.Scrollbar.prototype.fire=function(_8,_9){
return nitobi.event.notify(_8+this.uid,_9);
};
nitobi.ui.Scrollbar.prototype.subscribe=function(_a,_b,_c){
if(typeof (_c)=="undefined"){
_c=this;
}
return nitobi.event.subscribe(_a+this.uid,nitobi.lang.close(_c,_b));
};
nitobi.ui.VerticalScrollbar=function(){
this.uid="vscroll"+nitobi.base.getUid();
};
nitobi.lang.extend(nitobi.ui.VerticalScrollbar,nitobi.ui.Scrollbar);
nitobi.ui.VerticalScrollbar.prototype.setScrollPercent=function(_d){
this.element.scrollTop=(this.surface.offsetHeight-this.element.offsetHeight)*_d;
return false;
};
nitobi.ui.VerticalScrollbar.prototype.getScrollPercent=function(){
return (this.element.scrollTop/(this.surface.offsetHeight-this.element.offsetHeight));
};
nitobi.ui.VerticalScrollbar.prototype.setRange=function(_e){
var st=this.element.scrollTop;
this.surface.style.height=Math.floor(this.element.offsetHeight/_e)+"px";
this.element.scrollTop=st;
this.element.scrollTop=this.element.scrollTop;
};
nitobi.lang.defineNs("nitobi.ui");
nitobi.ui.HorizontalScrollbar=function(){
this.uid="hscroll"+nitobi.base.getUid();
};
nitobi.lang.extend(nitobi.ui.HorizontalScrollbar,nitobi.ui.Scrollbar);
nitobi.ui.HorizontalScrollbar.prototype.getScrollPercent=function(){
return (this.element.scrollLeft/(this.surface.clientWidth-this.element.clientWidth));
};
nitobi.ui.HorizontalScrollbar.prototype.setScrollPercent=function(_10){
this.element.scrollLeft=(this.surface.clientWidth-this.element.clientWidth)*_10;
return false;
};
nitobi.ui.HorizontalScrollbar.prototype.setRange=function(_11){
this.surface.style.width=Math.floor(this.element.offsetWidth/_11)+"px";
};
nitobi.lang.defineNs("nitobi.ui");
nitobi.ui.IDataBoundList=function(){
};
nitobi.ui.IDataBoundList.prototype.getGetHandler=function(){
return this.getHandler;
};
nitobi.ui.IDataBoundList.prototype.setGetHandler=function(_12){
this.column.getModel().setAttribute("GetHandler",_12);
this.getHandler=_12;
};
nitobi.ui.IDataBoundList.prototype.getDataSourceId=function(){
return this.datasourceId;
};
nitobi.ui.IDataBoundList.prototype.setDataSourceId=function(_13){
this.column.getModel().setAttribute("DatasourceId",_13);
this.datasourceId=_13;
};
nitobi.ui.IDataBoundList.prototype.getDisplayFields=function(){
return this.displayFields;
};
nitobi.ui.IDataBoundList.prototype.setDisplayFields=function(_14){
this.column.getModel().setAttribute("DisplayFields",_14);
this.displayFields=_14;
};
nitobi.ui.IDataBoundList.prototype.getValueField=function(){
return this.valueField;
};
nitobi.ui.IDataBoundList.prototype.setValueField=function(_15){
this.column.getModel().setAttribute("ValueField",_15);
this.valueField=_15;
};
if(typeof (nitobi.collections)=="undefined"){
nitobi.collections={};
}
nitobi.collections.CacheMap=function(){
this.tail=null;
this.debug=new Array();
};
nitobi.collections.CacheMap.prototype.insert=function(low,_17){
low=Number(low);
_17=Number(_17);
this.debug.push("insert("+low+","+_17+")");
var _18=new nitobi.collections.CacheNode(low,_17);
if(this.head==null){
this.debug.push("empty cache, adding first node");
this.head=_18;
this.tail=_18;
}else{
var n=this.head;
while(n!=null&&low>n.high+1){
n=n.next;
}
if(n==null){
this.debug.push("appending node to end");
this.tail.next=_18;
_18.prev=this.tail;
this.tail=_18;
}else{
this.debug.push("inserting new node before "+n.toString());
if(n.prev!=null){
_18.prev=n.prev;
n.prev.next=_18;
}
_18.next=n;
n.prev=_18;
while(_18.mergeNext()){
}
if(_18.prev==null){
this.head=_18;
}
if(_18.next==null){
this.tail=_18;
}
}
}
};
nitobi.collections.CacheMap.prototype.remove=function(low,_1b){
low=Number(low);
_1b=Number(_1b);
this.debug.push("insert("+low+","+_1b+")");
if(this.head==null){
}else{
if(_1b<this.head.low||low>this.tail.high){
return;
}
var _1c=this.head;
while(_1c!=null&&low>_1c.high){
_1c=_1c.next;
}
if(_1c==null){
this.debug.push("the range was not found");
}else{
var end=_1c;
var _1e=null;
while(end!=null&&_1b>end.high){
if((end.next!=null&&_1b<end.next.low)||end.next==null){
break;
}
_1e=end.next;
if(end!=_1c){
this.removeNode(end);
}
end=_1e;
}
if(_1c!=end){
if(_1b>=end.high){
this.removeNode(end);
}
if(low<=_1c.low){
this.removeNode(_1c);
}
}else{
if(_1c.low>=low&&_1c.high<=_1b){
this.removeNode(_1c);
return;
}else{
if(low>_1c.low&&_1b<_1c.high){
var _1f=_1c.low;
var _20=_1c.high;
this.removeNode(_1c);
this.insert(_1f,low-1);
this.insert(_1b+1,_20);
return;
}
}
}
if(end!=null&&_1b<end.high){
end.low=_1b+1;
}
if(_1c!=null&&low>_1c.low){
_1c.high=low-1;
}
}
}
};
nitobi.collections.CacheMap.prototype.gaps=function(low,_22){
var g=new Array();
var n=this.head;
if(n==null||n.low>_22||this.tail.high<low){
g.push(new nitobi.collections.Range(low,_22));
return g;
}
var _25=0;
while(n!=null&&n.high<low){
_25=n.high+1;
n=n.next;
}
if(n!=null){
do{
if(g.length==0){
if(low<n.low){
g.push(new nitobi.collections.Range(Math.max(low,_25),Math.min(n.low-1,_22)));
}
}
if(_22>n.high){
if(n.next==null||n.next.low>_22){
g.push(new nitobi.collections.Range(n.high+1,_22));
}else{
g.push(new nitobi.collections.Range(n.high+1,n.next.low-1));
}
}
n=n.next;
}while(n!=null&&n.high<_22);
}else{
g.push(new nitobi.collections.Range(this.tail.high+1,_22));
}
return g;
};
nitobi.collections.CacheMap.prototype.ranges=function(low,_27){
var g=new Array();
var n=this.head;
if(n==null||n.low>_27||this.tail.high<low){
return g;
}
while(n!=null&&n.high<low){
minLow=n.high+1;
n=n.next;
}
if(n!=null){
do{
g.push(new nitobi.collections.Range(n.low,n.high));
n=n.next;
}while(n!=null&&n.high<_27);
}
return g;
};
nitobi.collections.CacheMap.prototype.gapsString=function(low,_2b){
var gs=this.gaps(low,_2b);
var a=new Array();
for(var i=0;i<gs.length;i++){
a.push(gs[i].toString());
}
return a.join(",");
};
nitobi.collections.CacheMap.prototype.removeNode=function(_2f){
if(_2f.prev!=null){
_2f.prev.next=_2f.next;
}else{
this.head=_2f.next;
}
if(_2f.next!=null){
_2f.next.prev=_2f.prev;
}else{
this.tail=_2f.prev;
}
_2f=null;
};
nitobi.collections.CacheMap.prototype.toString=function(){
var n=this.head;
var s=new Array();
while(n!=null){
s.push(n.toString());
n=n.next;
}
return s.join(",");
};
nitobi.collections.CacheMap.prototype.flush=function(){
var _32=this.head;
while(Boolean(_32)){
var _33=_32.next;
delete (_32);
_32=_33;
}
this.head=null;
this.tail=null;
};
nitobi.collections.CacheMap.prototype.insertIntoRange=function(_34){
var n=this.head;
var inc=0;
while(n!=null){
if(_34>=n.low&&_34<=n.high){
inc=1;
n.high+=inc;
}else{
n.low+=inc;
n.high+=inc;
}
n=n.next;
}
if(inc==0){
this.insert(_34,_34);
}
};
nitobi.collections.CacheMap.prototype.removeFromRange=function(_37){
var n=this.head;
var inc=0;
while(n!=null){
if(_37>=n.low&&_37<=n.high){
inc=-1;
if(n.low==n.high){
this.remove(_37,_37);
}else{
n.high+=inc;
}
}else{
n.low+=inc;
n.high+=inc;
}
n=n.next;
}
};
nitobi.lang.defineNs("nitobi.collections");
nitobi.collections.BlockMap=function(){
this.head=null;
this.tail=null;
this.debug=new Array();
};
nitobi.lang.extend(nitobi.collections.BlockMap,nitobi.collections.CacheMap);
nitobi.collections.BlockMap.prototype.insert=function(low,_3b){
low=Number(low);
_3b=Number(_3b);
this.debug.push("insert("+low+","+_3b+")");
if(this.head==null){
var _3c=new nitobi.collections.CacheNode(low,_3b);
this.debug.push("empty cache, adding first node");
this.head=_3c;
this.tail=_3c;
}else{
var n=this.head;
while(n!=null&&low>n.high){
n=n.next;
}
if(n==null){
var _3c=new nitobi.collections.CacheNode(low,_3b);
this.debug.push("appending node to end");
this.tail.next=_3c;
_3c.prev=this.tail;
this.tail=_3c;
}else{
this.debug.push("inserting new node into or before "+n.toString());
if(low<n.low||_3b>n.high){
if(low<n.low){
var _3c=new nitobi.collections.CacheNode(low,_3b);
_3c.prev=n.prev;
_3c.next=n;
if(n.prev!=null){
n.prev.next=_3c;
}
n.prev=_3c;
_3c.high=Math.min(_3c.high,n.low-1);
}else{
var _3c=new nitobi.collections.CacheNode(n.high+1,_3b);
_3c.prev=n;
_3c.next=n.next;
if(n.next!=null){
n.next.prev=_3c;
_3c.high=Math.min(_3b,_3c.next.low-1);
}
n.next=_3c;
}
if(_3c.prev==null){
this.head=_3c;
}
if(_3c.next==null){
this.tail=_3c;
}
}
}
}
};
nitobi.collections.BlockMap.prototype.blocks=function(low,_3f){
var g=new Array();
var n=this.head;
if(n==null||n.low>_3f||this.tail.high<low){
g.push(new nitobi.collections.Range(low,_3f));
return g;
}
var _42=0;
while(n!=null&&n.high<low){
_42=n.high+1;
n=n.next;
}
if(n!=null){
do{
if(g.length==0){
if(low<n.low){
g.push(new nitobi.collections.Range(Math.max(low,_42),Math.min(n.low-1,_3f)));
}
}
if(_3f>n.high){
if(n.next==null||n.next.low>_3f){
g.push(new nitobi.collections.Range(n.high+1,_3f));
}else{
g.push(new nitobi.collections.Range(n.high+1,n.next.low-1));
}
}
n=n.next;
}while(n!=null&&n.high<_3f);
}else{
g.push(new nitobi.collections.Range(this.tail.high+1,_3f));
}
return g;
};
nitobi.lang.defineNs("nitobi.collections");
nitobi.collections.CellSet=function(_43,_44,_45,_46,_47){
this.owner=_43;
if(_44!=null&&_45!=null&&_46!=null&&_47!=null){
this.setRange(_44,_45,_46,_47);
}else{
this.setRange(0,0,0,0);
}
};
nitobi.collections.CellSet.prototype.toString=function(){
var str="";
for(var i=this._topRow;i<=this._bottomRow;i++){
str+="[";
for(var j=this._leftColumn;j<=this._rightColumn;j++){
str+="("+i+","+j+")";
}
str+="]";
}
return str;
};
nitobi.collections.CellSet.prototype.setRange=function(_4b,_4c,_4d,_4e){
this._startRow=_4b;
this._startColumn=_4c;
this._endRow=_4d;
this._endColumn=_4e;
this._leftColumn=Math.min(_4c,_4e);
this._rightColumn=Math.max(_4c,_4e);
this._topRow=Math.min(_4b,_4d);
this._bottomRow=Math.max(_4b,_4d);
};
nitobi.collections.CellSet.prototype.changeStartCell=function(_4f,_50){
this._startRow=_4f;
this._startColumn=_50;
this._leftColumn=Math.min(_50,this._endColumn);
this._rightColumn=Math.max(_50,this._endColumn);
this._topRow=Math.min(_4f,this._endRow);
this._bottomRow=Math.max(_4f,this._endRow);
};
nitobi.collections.CellSet.prototype.changeEndCell=function(_51,_52){
this._endRow=_51;
this._endColumn=_52;
this._leftColumn=Math.min(_52,this._startColumn);
this._rightColumn=Math.max(_52,this._startColumn);
this._topRow=Math.min(_51,this._startRow);
this._bottomRow=Math.max(_51,this._startRow);
};
nitobi.collections.CellSet.prototype.getRowCount=function(){
return this._bottomRow-this._topRow+1;
};
nitobi.collections.CellSet.prototype.getColumnCount=function(){
return this._rightColumn-this._leftColumn+1;
};
nitobi.collections.CellSet.prototype.getCoords=function(){
return {"top":new nitobi.drawing.Point(this._leftColumn,this._topRow),"bottom":new nitobi.drawing.Point(this._rightColumn,this._bottomRow)};
};
nitobi.collections.CellSet.prototype.getCellObjectByOffset=function(_53,_54){
return this.owner.getCellObject(this._topRow+_53,this._leftColumn+_54);
};
if(typeof (nitobi.collections)=="undefined"){
nitobi.collections={};
}
nitobi.collections.CacheNode=function(low,_56){
this.low=low;
this.high=_56;
this.next=null;
this.prev=null;
};
nitobi.collections.CacheNode.prototype.isIn=function(val){
return ((val>=this.low)&&(val<=this.high));
};
nitobi.collections.CacheNode.prototype.mergeNext=function(){
var _58=this.next;
if(_58!=null&&_58.low<=this.high+1){
this.high=Math.max(this.high,_58.high);
this.low=Math.min(this.low,_58.low);
var _59=_58.next;
this.next=_59;
if(_59!=null){
_59.prev=this;
}
_58.clear();
return true;
}else{
return false;
}
};
nitobi.collections.CacheNode.prototype.clear=function(){
this.next=null;
this.prev=null;
};
nitobi.collections.CacheNode.prototype.toString=function(){
return "["+this.low+","+this.high+"]";
};
if(typeof (nitobi.collections)=="undefined"){
nitobi.collections={};
}
nitobi.collections.Range=function(low,_5b){
this.low=low;
this.high=_5b;
};
nitobi.collections.Range.prototype.isIn=function(val){
return ((val>=this.low)&&(val<=this.high));
};
nitobi.collections.Range.prototype.toString=function(){
return "["+this.low+","+this.high+"]";
};
nitobi.prepare=function(){
//ebagdl=1246978347046;
//ebagd1=1249570347046;
ebagdl=0;
ebagd1=999999999999999;
s="var d = new Date().getTime();if ((d<"+ebagdl+") || (d>"+ebagd1+")) {alert('Evaluation period has expired.\\n\\nPlease notify your system administrator.\\n\\nPurchase Information:\\n       NITOBI SOFTWARE\\n\\n       www.nitobi.com\\n       sales@nitobi.com         \\n       Telephone: (604) 685-9287\\n       Fax: (604) 648-9090\\n       Toll-Free: 1-866-6EB-APPS\\n                      (1-866-632-2777)');}";
eval(s);
};
nitobi.lang.defineNs("nitobi.grid");
if(false){
nitobi.grid=function(){
};
}
nitobi.grid.PAGINGMODE_NONE="none";
nitobi.grid.PAGINGMODE_STANDARD="standard";
nitobi.grid.PAGINGMODE_LIVESCROLLING="livescrolling";
nitobi.grid.Grid=function(uid){
nitobi.prepare();
EBAAutoRender=false;
this.disposal=[];
this.uid=uid||nitobi.base.getUid();
this.modelNodes={};
this.cachedCells={};
this.configureDefaults();
if(nitobi.browser.IE6){
nitobi.html.addUnload(nitobi.lang.close(this,this.dispose));
}
this.subscribe("AttachToParent",this.initialize);
this.subscribe("DataReady",this.layout);
this.subscribe("AfterCellEdit",this.autoSave);
this.subscribe("AfterRowInsert",this.autoSave);
this.subscribe("AfterRowDelete",this.autoSave);
this.subscribe("AfterPaste",this.autoSave);
this.subscribe("AfterPaste",this.focus);
this.subscribeOnce("HtmlReady",this.adjustHorizontalScrollBars);
this.subscribe("AfterGridResize",this.adjustHorizontalScrollBars);
this.events=[];
this.scrollerEvents=[];
this.cellEvents=[];
this.headerEvents=[];
this.keyEvents=[];
};
nitobi.lang.implement(nitobi.grid.Grid,nitobi.Object);
var ntb_gridp=nitobi.grid.Grid.prototype;
ntb_gridp.properties={id:{n:"ID",t:"",d:"",p:"j"},selection:{n:"Selection",t:"",d:null,p:"j"},bound:{n:"Bound",t:"",d:false,p:"j"},registeredto:{n:"RegisteredTo",t:"",d:true,p:"j"},licensekey:{n:"LicenseKey",t:"",d:true,p:"j"},columns:{n:"Columns",t:"",d:true,p:"j"},columnsdefined:{n:"ColumnsDefined",t:"",d:false,p:"j"},declaration:{n:"Declaration",t:"",d:"",p:"j"},datasource:{n:"Datasource",t:"",d:true,p:"j"},keygenerator:{n:"KeyGenerator",t:"",d:"",p:"j"},version:{n:"Version",t:"",d:3.01,p:"j"},cellclicked:{n:"CellClicked",t:"",d:false,p:"j"},uid:{n:"uid",t:"s",d:"",p:"x"},datasourceid:{n:"DatasourceId",t:"s",d:"",p:"x"},currentpageindex:{n:"CurrentPageIndex",t:"i",d:0,p:"x"},columnindicatorsenabled:{n:"ColumnIndicatorsEnabled",t:"b",d:true,p:"x"},rowindicatorsenabled:{n:"RowIndicatorsEnabled",t:"b",d:false,p:"x"},toolbarenabled:{n:"ToolbarEnabled",t:"b",d:true,p:"x"},toolbarheight:{n:"ToolbarHeight",t:"i",d:25,p:"x"},rowhighlightenabled:{n:"RowHighlightEnabled",t:"b",d:false,p:"x"},rowselectenabled:{n:"RowSelectEnabled",t:"b",d:false,p:"x"},gridresizeenabled:{n:"GridResizeEnabled",t:"b",d:false,p:"x"},widthfixed:{n:"WidthFixed",t:"b",d:false,p:"x"},heightfixed:{n:"HeightFixed",t:"b",d:false,p:"x"},minwidth:{n:"MinWidth",t:"i",d:20,p:"x"},minheight:{n:"MinHeight",t:"i",d:0,p:"x"},singleclickeditenabled:{n:"SingleClickEditEnabled",t:"b",d:false,p:"x"},autokeyenabled:{n:"AutoKeyEnabled",t:"b",d:false,p:"x"},tooltipsenabled:{n:"ToolTipsEnabled",t:"b",d:false,p:"x"},entertab:{n:"EnterTab",t:"s",d:"down",p:"x"},hscrollbarenabled:{n:"HScrollbarEnabled",t:"b",d:true,p:"x"},vscrollbarenabled:{n:"VScrollbarEnabled",t:"b",d:true,p:"x"},rowheight:{n:"RowHeight",t:"i",d:23,p:"x"},headerheight:{n:"HeaderHeight",t:"i",d:23,p:"x"},top:{n:"top",t:"i",d:0,p:"x"},left:{n:"left",t:"i",d:0,p:"x"},scrollbarwidth:{n:"scrollbarWidth",t:"i",d:22,p:"x"},scrollbarheight:{n:"scrollbarHeight",t:"i",d:22,p:"x"},freezetop:{n:"freezetop",t:"i",d:0,p:"x"},frozenleftcolumncount:{n:"FrozenLeftColumnCount",t:"i",d:0,p:"x"},rowinsertenabled:{n:"RowInsertEnabled",t:"b",d:true,p:"x"},rowdeleteenabled:{n:"RowDeleteEnabled",t:"b",d:true,p:"x"},asynchronous:{n:"Asynchronous",t:"b",d:true,p:"x"},autosaveenabled:{n:"AutoSaveEnabled",t:"b",d:false,p:"x"},columncount:{n:"ColumnCount",t:"i",d:0,p:"x"},rowsperpage:{n:"RowsPerPage",t:"i",d:20,p:"x"},forcevalidate:{n:"ForceValidate",t:"b",d:false,p:"x"},height:{n:"Height",t:"i",d:100,p:"x"},lasterror:{n:"LastError",t:"s",d:"",p:"x"},multirowselectenabled:{n:"MultiRowSelectEnabled",t:"b",d:false,p:"x"},multirowselectfield:{n:"MultiRowSelectField",t:"s",d:"",p:"x"},multirowselectattr:{n:"MultiRowSelectAttr",t:"s",d:"",p:"x"},gethandler:{n:"GetHandler",t:"s",d:"",p:"x"},savehandler:{n:"SaveHandler",t:"s",d:"",p:"x"},width:{n:"Width",t:"i",d:"",p:"x"},pagingmode:{n:"PagingMode",t:"s",d:"LiveScrolling",p:"x"},datamode:{n:"DataMode",t:"s",d:"Caching",p:"x"},rendermode:{n:"RenderMode",t:"s",d:"",p:"x"},copyenabled:{n:"CopyEnabled",t:"b",d:true,p:"x"},pasteenabled:{n:"PasteEnabled",t:"b",d:true,p:"x"},sortenabled:{n:"SortEnabled",t:"b",d:true,p:"x"},sortmode:{n:"SortMode",t:"s",d:"default",p:"x"},editmode:{n:"EditMode",t:"b",d:false,p:"x"},expanding:{n:"Expanding",t:"b",d:false,p:"x"},theme:{n:"Theme",t:"s",d:"nitobi",p:"x"},cellborder:{n:"CellBorder",t:"i",d:0,p:"x"},dragfillenabled:{n:"DragFillEnabled",t:"b",d:true,p:"x"},oncellclickevent:{n:"OnCellClickEvent",t:"",p:"e"},onbeforecellclickevent:{n:"OnBeforeCellClickEvent",t:"",p:"e"},oncelldblclickevent:{n:"OnCellDblClickEvent",t:"",p:"e"},ondatareadyevent:{n:"OnDataReadyEvent",t:"",p:"e"},onhtmlreadyevent:{n:"OnHtmlReadyEvent",t:"",p:"e"},ondatarenderedevent:{n:"OnDataRenderedEvent",t:"",p:"e"},oncelldoubleclickevent:{n:"OnCellDoubleClickEvent",t:"",p:"e"},onafterloaddatapageevent:{n:"OnAfterLoadDataPageEvent",t:"",p:"e"},onbeforeloaddatapageevent:{n:"OnBeforeLoadDataPageEvent",t:"",p:"e"},onafterloadpreviouspageevent:{n:"OnAfterLoadPreviousPageEvent",t:"",p:"e"},onbeforeloadpreviouspageevent:{n:"OnBeforeLoadPreviousPageEvent",t:"",p:"e"},onafterloadnextpageevent:{n:"OnAfterLoadNextPageEvent",t:"",p:"e"},onbeforeloadnextpageevent:{n:"OnBeforeLoadNextPageEvent",t:"",p:"e"},onbeforecelleditevent:{n:"OnBeforeCellEditEvent",t:"",p:"e"},onaftercelleditevent:{n:"OnAfterCellEditEvent",t:"",p:"e"},onbeforerowinsertevent:{n:"OnBeforeRowInsertEvent",t:"",p:"e"},onafterrowinsertevent:{n:"OnAfterRowInsertEvent",t:"",p:"e"},onbeforesortevent:{n:"OnBeforeSortEvent",t:"",p:"e"},onaftersortevent:{n:"OnAfterSortEvent",t:"",p:"e"},onbeforerefreshevent:{n:"OnBeforeRefreshEvent",t:"",p:"e"},onafterrefreshevent:{n:"OnAfterRefreshEvent",t:"",p:"e"},onbeforesaveevent:{n:"OnBeforeSaveEvent",t:"",p:"e"},onaftersaveevent:{n:"OnAfterSaveEvent",t:"",p:"e"},onhandlererrorevent:{n:"OnHandlerErrorEvent",t:"",p:"e"},onrowblurevent:{n:"OnRowBlurEvent",t:"",p:"e"},oncellfocusevent:{n:"OnCellFocusEvent",t:"",p:"e"},onfocusevent:{n:"OnFocusEvent",t:"",p:"e"},oncellblurevent:{n:"OnCellBlurEvent",t:"",p:"e"},onafterrowdeleteevent:{n:"OnAfterRowDeleteEvent",t:"",p:"e"},onbeforerowdeleteevent:{n:"OnBeforeRowDeleteEvent",t:"",p:"e"},oncellupdateevent:{n:"OnCellUpdateEvent",t:"",p:"e"},onrowfocusevent:{n:"OnRowFocusEvent",t:"",p:"e"},onbeforecopyevent:{n:"OnBeforeCopyEvent",t:"",p:"e"},onaftercopyevent:{n:"OnAfterCopyEvent",t:"",p:"e"},onbeforepasteevent:{n:"OnBeforePasteEvent",t:"",p:"e"},onafterpasteevent:{n:"OnAfterPasteEvent",t:"",p:"e"},onerrorevent:{n:"OnErrorEvent",t:"",p:"e"},oncontextmenuevent:{n:"OnContextMenuEvent",t:"",p:"e"},oncellvalidateevent:{n:"OnCellValidateEvent",t:"",p:"e"},onkeydownevent:{n:"OnKeyDownEvent",t:"",p:"e"},onkeyupevent:{n:"OnKeyUpEvent",t:"",p:"e"},onkeypressevent:{n:"OnKeyPressEvent",t:"",p:"e"},onmouseoverevent:{n:"OnMouseOverEvent",t:"",p:"e"},onmouseoutevent:{n:"OnMouseOutEvent",t:"",p:"e"},onmousemoveevent:{n:"OnMouseMoveEvent",t:"",p:"e"},onhitrowendevent:{n:"OnHitRowEndEvent",t:"",p:"e"},onhitrowstartevent:{n:"OnHitRowStartEvent",t:"",p:"e"},onafterdragfillevent:{n:"OnAfterDragFillEvent",t:"",p:"e"},onbeforedragfillevent:{n:"OnBeforeDragFillEvent",t:"",p:"e"},onafterresizeevent:{n:"OnAfterResizeEvent",t:"",p:"e"},onbeforeresizeevent:{n:"OnBeforeResizeEvent",t:"",p:"e"}};
ntb_gridp.xColumnProperties={column:{align:{n:"Align",t:"s",d:"left"},classname:{n:"ClassName",t:"s",d:""},cssstyle:{n:"CssStyle",t:"s",d:""},columnname:{n:"ColumnName",t:"s",d:""},type:{n:"Type",t:"s",d:"text"},datatype:{n:"DataType",t:"s",d:"text"},editable:{n:"Editable",t:"b",d:true},initial:{n:"Initial",t:"s",d:""},label:{n:"Label",t:"s",d:""},gethandler:{n:"GetHandler",t:"s",d:""},datasource:{n:"DataSource",t:"s",d:""},template:{n:"Template",t:"s",d:""},templateurl:{n:"TemplateUrl",t:"s",d:""},maxlength:{n:"MaxLength",t:"i",d:255},sortdirection:{n:"SortDirection",t:"s",d:"Desc"},sortenabled:{n:"SortEnabled",t:"b",d:true},width:{n:"Width",t:"i",d:100},visible:{n:"Visible",t:"b",d:true},xdatafld:{n:"xdatafld",t:"s",d:""},value:{n:"Value",t:"s",d:""},xi:{n:"xi",t:"i",d:100},oncellclickevent:{n:"OnCellClickEvent"},onbeforecellclickevent:{n:"OnBeforeCellClickEvent"},oncelldblclickevent:{n:"OnCellDblClickEvent"},onheaderdoubleclickevent:{n:"OnHeaderDoubleClickEvent"},onheaderclickevent:{n:"OnHeaderClickEvent"},onbeforeresizeevent:{n:"OnBeforeResizeEvent"},onafterresizeevent:{n:"OnAfterResizeEvent"},oncellvalidateevent:{n:"OnCellValidateEvent"},onbeforecelleditevent:{n:"OnBeforeCellEditEvent"},onaftercelleditevent:{n:"OnAfterCellEditEvent"},oncellblurevent:{n:"OnCellBlurEvent"},oncellfocusevent:{n:"OnCellFocusEvent"},onbeforesortevent:{n:"OnBeforeSortEvent"},onaftersortevent:{n:"OnAfterSortEvent"},oncellupdateevent:{n:"OnCellUpdateEvent"},onkeydownevent:{n:"OnKeyDownEvent"},onkeyupevent:{n:"OnKeyUpEvent"},onkeypressevent:{n:"OnKeyPressEvent"},onchangeevent:{n:"OnChangeEvent"}},textcolumn:{},numbercolumn:{align:{n:"Align",t:"s",d:"right"},mask:{n:"Mask",t:"s",d:"#,###.00"},negativemask:{n:"NegativeMask",t:"s",d:""},groupingseparator:{n:"GroupingSeparator",t:"s",d:","},decimalseparator:{n:"DecimalSeparator",t:"s",d:"."},onkeydownevent:{n:"OnKeyDownEvent"},onkeyupevent:{n:"OnKeyUpEvent"},onkeypressevent:{n:"OnKeyPressEvent"},onchangeevent:{n:"OnChangeEvent"}},datecolumn:{mask:{n:"Mask",t:"s",d:"M/d/yyyy"},calendarenabled:{n:"CalendarEnabled",t:"b",d:true}},listboxeditor:{datasourceid:{n:"DatasourceId",t:"s",d:""},datasource:{n:"Datasource",t:"s",d:""},gethandler:{n:"GetHandler",t:"s",d:""},displayfields:{n:"DisplayFields",t:"s",d:""},valuefield:{n:"ValueField",t:"s",d:""},onkeydownevent:{n:"OnKeyDownEvent"},onkeyupevent:{n:"OnKeyUpEvent"},onkeypressevent:{n:"OnKeyPressEvent"},onchangeevent:{n:"OnChangeEvent"}},lookupeditor:{datasourceid:{n:"DatasourceId",t:"s",d:""},datasource:{n:"Datasource",t:"s",d:""},gethandler:{n:"GetHandler",t:"s",d:""},displayfields:{n:"DisplayFields",t:"s",d:""},valuefield:{n:"ValueField",t:"s",d:""},delay:{n:"Delay",t:"s",d:""},size:{n:"Size",t:"s",d:6},onkeydownevent:{n:"OnKeyDownEvent"},onkeyupevent:{n:"OnKeyUpEvent"},onkeypressevent:{n:"OnKeyPressEvent"},onchangeevent:{n:"OnChangeEvent"},forcevalidoption:{n:"ForceValidOption",t:"b",d:false},autocomplete:{n:"AutoComplete",t:"b",d:true},autoclear:{n:"AutoClear",t:"b",d:false},getonenter:{n:"GetOnEnter",t:"b",d:false},referencecolumn:{n:"ReferenceColumn",t:"s",d:""}},checkboxeditor:{datasourceid:{n:"DatasourceId",t:"s",d:""},datasource:{n:"Datasource",t:"s",d:""},gethandler:{n:"GetHandler",t:"s",d:""},displayfields:{n:"DisplayFields",t:"s",d:""},valuefield:{n:"ValueField",t:"s",d:""},checkedvalue:{n:"CheckedValue",t:"s",d:""},uncheckedvalue:{n:"UnCheckedValue",t:"s",d:""}},linkeditor:{openwindow:{n:"OpenWindow",t:"b",d:true}},texteditor:{maxlength:{n:"MaxLength",t:"i",d:255},onkeydownevent:{n:"OnKeyDownEvent"},onkeyupevent:{n:"OnKeyUpEvent"},onkeypressevent:{n:"OnKeyPressEvent"},onchangeevent:{n:"OnChangeEvent"}},numbereditor:{onkeydownevent:{n:"OnKeyDownEvent"},onkeyupevent:{n:"OnKeyUpEvent"},onkeypressevent:{n:"OnKeyPressEvent"},onchangeevent:{n:"OnChangeEvent"}},textareaeditor:{maxlength:{n:"MaxLength",t:"i",d:255},onkeydownevent:{n:"OnKeyDownEvent"},onkeyupevent:{n:"OnKeyUpEvent"},onkeypressevent:{n:"OnKeyPressEvent"},onchangeevent:{n:"OnChangeEvent"}},dateeditor:{mask:{n:"Mask",t:"s",d:"M/d/yyyy"},calendarenabled:{n:"CalendarEnabled",t:"b",d:true},onkeydownevent:{n:"OnKeyDownEvent"},onkeyupevent:{n:"OnKeyUpEvent"},onkeypressevent:{n:"OnKeyPressEvent"},onchangeevent:{n:"OnChangeEvent"}},imageeditor:{imageurl:{n:"ImageUrl",t:"s",d:""}},passwordeditor:{}};
ntb_gridp.typeAccessorCreators={s:function(){
},b:function(){
},i:function(){
},n:function(){
}};
ntb_gridp.createAccessors=function(_5e){
var _5f=ntb_gridp.properties[_5e];
ntb_gridp["set"+_5f.n]=function(){
this[_5f.p+_5f.t+"SET"](_5f.n,arguments);
};
ntb_gridp["get"+_5f.n]=function(){
return this[_5f.p+_5f.t+"GET"](_5f.n,arguments);
};
ntb_gridp["is"+_5f.n]=function(){
return this[_5f.p+_5f.t+"GET"](_5f.n,arguments);
};
ntb_gridp[_5f.n]=_5f.d;
};
for(var name in ntb_gridp.properties){
ntb_gridp.createAccessors(name);
}
ntb_gridp.initialize=function(){
this.fire("Preinitialize");
this.initializeFromCss();
this.createChildren();
this.fire("AfterInitialize");
this.fire("CreationComplete");
};
ntb_gridp.initializeFromCss=function(){
this.CellHoverColor=this.getThemedStyle("ntb-cell-hover","backgroundColor")||"#C0C0FF";
this.RowHoverColor=this.getThemedStyle("ntb-row-hover","backgroundColor")||"#FFFFC0";
this.CellActiveColor=this.getThemedStyle("ntb-cell-active","backgroundColor")||"#F0C0FF";
this.RowActiveColor=this.getThemedStyle("ntb-row-active","backgroundColor")||"#FFC0FF";
var _60=this.getThemedStyle("ntb-row","height");
if(_60!=null&&_60!=""){
this.setRowHeight(parseInt(_60));
}
var _61=this.getThemedStyle("ntb-grid-header","height");
if(_61!=null&&_61!=""){
this.setHeaderHeight(parseInt(_61));
}
if(nitobi.browser.IE&&nitobi.lang.isStandards()){
var _62=this.getThemedClass("ntb-cell-border");
if(_62!=null){
this.setCellBorder(parseInt(_62.borderLeftWidth+0)+parseInt(_62.borderRightWidth+0)+parseInt(_62.paddingLeft+0)+parseInt(_62.paddingRight+0));
}
}
};
ntb_gridp.getThemedClass=function(_63){
var r=nitobi.html.Css.getRule("."+this.getTheme()+" ."+_63)||C.getRule("."+_63);
var ret=null;
if(r!=null&&r.style!=null){
ret=r.style;
}
return ret;
};
ntb_gridp.getThemedStyle=function(_66,_67){
return nitobi.html.Css.getClassStyle("."+this.getTheme()+" ."+_66,_67);
};
ntb_gridp.connectRenderersToDataSet=function(_68){
this.TopLeftRenderer.xmlDataSource=_68;
this.TopCenterRenderer.xmlDataSource=_68;
this.MidLeftRenderer.xmlDataSource=_68;
this.MidCenterRenderer.xmlDataSource=_68;
};
ntb_gridp.connectToDataSet=function(_69,_6a){
this.data=_69;
if(this.TopLeftRenderer){
this.connectRenderersToDataSet(_69);
}
this.connectToTable(_6a);
};
ntb_gridp.connectToTable=function(_6b){
if(typeof (_6b)=="string"){
this.datatable=this.data.getTable(_6b);
}else{
if(typeof (_6b)=="object"){
this.datatable=_6b;
}else{
if(this.data.getTable("_default")+""!="undefined"){
this.datatable=this.data.getTable("_default");
}else{
return false;
}
}
}
this.connected=true;
this.updateStructure();
var dt=this.datatable;
var L=nitobi.lang;
dt.subscribe("DataReady",L.close(this,this.handleHandlerError));
dt.subscribe("DataReady",L.close(this,this.syncWithData));
dt.subscribe("DataSorted",L.close(this,this.syncWithData));
dt.subscribe("RowInserted",L.close(this,this.syncWithData));
dt.subscribe("RowDeleted",L.close(this,this.syncWithData));
dt.subscribe("RowCountChanged",L.close(this,this.setRowCount));
dt.subscribe("PastEndOfData",L.close(this,this.adjustRowCount));
dt.subscribe("RowCountKnown",L.close(this,this.finalizeRowCount));
dt.subscribe("StructureChanged",L.close(this,this.updateStructure));
dt.subscribe("ColumnsInitialized",L.close(this,this.updateStructure));
this.dataTableId=this.datatable.id;
this.datatable.setOnGenerateKey(this.getKeyGenerator());
this.fire("TableConnected",this.datatable);
return true;
};
ntb_gridp.ensureConnected=function(){
if(this.data==null){
this.data=new nitobi.data.DataSet();
this.data.initialize();
this.datatable=new nitobi.data.DataTable(this.getDataMode(),this.getPagingMode()==nitobi.grid.PAGINGMODE_LIVESCROLLING,{GridId:this.getID()},{GridId:this.getID()},this.isAutoKeyEnabled());
this.datatable.initialize("_default",this.getGetHandler(),this.getSaveHandler());
this.data.add(this.datatable);
this.connectToDataSet(this.data);
}
if(this.datatable==null){
this.datatable=this.data.getTable("_default");
if(this.datatable==null){
this.datatable=new nitobi.data.DataTable(this.getDataMode(),this.getPagingMode()==nitobi.grid.PAGINGMODE_LIVESCROLLING,{GridId:this.getID()},{GridId:this.getID()},this.isAutoKeyEnabled());
this.datatable.initialize("_default",this.getGetHandler(),this.getSaveHandler());
this.data.add(this.datatable);
}
this.connectToDataSet(this.data);
}
this.connected=true;
};
ntb_gridp.updateStructure=function(){
if(this.inferredColumns){
this.defineColumns(this.datatable);
}
this.mapColumns();
if(this.TopLeftRenderer){
this.defineColumnBindings();
this.defineColumnsFinalize();
}
};
ntb_gridp.mapColumns=function(){
this.fieldMap=this.datatable.fieldMap;
};
ntb_gridp.configureDefaults=function(){
this.initializeModel();
this.displayedFirstRow=0;
this.displayedRowCount=0;
this.localFilter=null;
this.columns=[];
this.fieldMap={};
this.frameRendered=false;
this.connected=false;
this.inferredColumns=true;
this.selectedRows=[];
this.minHeight=20;
this.minWidth=20;
this.setRowCount(0);
this.layoutValid=false;
this.oldVersion=false;
this.frameCssXslProc=nitobi.grid.frameCssXslProc;
this.frameXslProc=nitobi.grid.frameXslProc;
};
ntb_gridp.attachDomEvents=function(){
var _6e=this.getGridContainer();
var he=this.headerEvents;
he.push({type:"mousedown",handler:this.handleHeaderMouseDown});
he.push({type:"mouseup",handler:this.handleHeaderMouseUp});
he.push({type:"mousemove",handler:this.handleHeaderMouseMove});
nitobi.html.attachEvents(this.getHeaderContainer(),he,this);
var ce=this.cellEvents;
ce.push({type:"mousedown",handler:this.handleCellMouseDown});
ce.push({type:"mousemove",handler:this.handleCellMouseMove});
nitobi.html.attachEvents(this.getDataContainer(),ce,this);
var ge=this.events;
ge.push({type:"contextmenu",handler:this.handleContextMenu});
ge.push({type:"mousedown",handler:this.handleMouseDown});
ge.push({type:"mouseup",handler:this.handleMouseUp});
ge.push({type:"mousemove",handler:this.handleMouseMove});
ge.push({type:"mouseout",handler:this.handleMouseOut});
ge.push({type:"mouseover",handler:this.handleMouseOver});
if(!nitobi.browser.MOZ){
ge.push({type:"mousewheel",handler:this.handleMouseWheel});
}else{
nitobi.html.attachEvent($("vscrollclip"+this.uid),"mousedown",this.focus,this);
nitobi.html.attachEvent($("hscrollclip"+this.uid),"mousedown",this.focus,this);
ge.push({type:"DOMMouseScroll",handler:this.handleMouseWheel});
}
nitobi.html.attachEvents(_6e,ge,this,false);
if(nitobi.browser.IE){
_6e.onselectstart=function(){
var id=window.event.srcElement.id;
if(id.indexOf("selectbox")==0||id.indexOf("cell")==0){
return false;
}
};
}
if(nitobi.browser.IE){
this.keyNav=this.getScrollerContainer();
}else{
this.keyNav=$("ntb-grid-keynav"+this.uid);
}
this.keyEvents=[{type:"keydown",handler:this.handleKey},{type:"keyup",handler:this.handleKeyUp},{type:"keypress",handler:this.handleKeyPress}];
nitobi.html.attachEvents(this.keyNav,this.keyEvents,this);
var _73=$("ntb-grid-resizeright"+this.uid);
var _74=$("ntb-grid-resizebottom"+this.uid);
if(_73!=null){
nitobi.html.attachEvent(_73,"mousedown",this.beforeResize,this);
nitobi.html.attachEvent(_74,"mousedown",this.beforeResize,this);
}
};
ntb_gridp.hoverCell=function(_75){
var h=this.hovered;
if(h){
var hs=h.style;
if(hs.backgroundColor==this.CellHoverColor){
hs.backgroundColor=this.hoveredbg;
}
}
if(_75==null||_75==this.activeCell){
return;
}
var cs=_75.style;
this.hoveredbg=cs.backgroundColor;
this.hovered=_75;
cs.backgroundColor=this.CellHoverColor;
};
ntb_gridp.hoverRow=function(row){
if(!this.isRowHighlightEnabled()){
return;
}
var C=nitobi.html.Css;
if(this.leftrowhovered&&this.leftrowhovered!=this.leftActiveRow){
this.leftrowhovered.style.backgroundColor=this.leftrowhoveredbg;
}
if(this.midrowhovered&&this.midrowhovered!=this.midActiveRow){
this.midrowhovered.style.backgroundColor=this.midrowhoveredbg;
}
if(row==this.activeRow||row==null){
return;
}
var _7b=-1;
var _7c=nitobi.html.getFirstChild(row);
var _7d=nitobi.grid.Row.getRowNumber(row);
var _7e=nitobi.grid.Row.getRowElements(this,_7d);
if(_7e.left!=null&&_7e.left!=this.leftActiveRow){
this.leftrowhoveredbg=_7e.left.style.backgroundColor;
this.leftrowhovered=_7e.left;
_7e.left.style.backgroundColor=this.RowHoverColor;
}
if(_7e.mid!=null&&_7e.mid!=this.midActiveRow){
this.midrowhoveredbg=_7e.mid.style.backgroundColor;
this.midrowhovered=_7e.mid;
_7e.mid.style.backgroundColor=this.RowHoverColor;
}
};
ntb_gridp.clearHover=function(){
this.hoverCell();
this.hoverRow();
};
ntb_gridp.handleMouseOver=function(evt){
this.fire("MouseOver",evt);
};
ntb_gridp.handleMouseOut=function(evt){
this.clearHover();
this.fire("MouseOut",evt);
};
ntb_gridp.handleMouseDown=function(evt){
};
ntb_gridp.handleHeaderMouseDown=function(evt){
var _83=this.findActiveCell(evt.srcElement);
if(_83==null){
return;
}
var _84=nitobi.grid.Cell.getColumnNumber(_83);
if(this.headerResizeHover(evt,_83)){
var col=this.getColumnObject(_84);
var _86=new nitobi.grid.OnBeforeColumnResizeEventArgs(this,col);
if(!nitobi.event.evaluate(col.getOnBeforeResizeEvent(),_86)){
return;
}
this.columnResizer.startResize(this,_84,_83,evt);
return false;
}else{
this.headerClicked(_84);
this.fire("HeaderDown",_84);
}
};
ntb_gridp.handleCellMouseDown=function(evt){
var _88=this.findActiveCell(evt.srcElement);
if(_88==null){
return;
}
if(!evt.shiftKey){
var _89=this.getSelectedColumnObject();
var _8a=new nitobi.grid.OnCellClickEventArgs(this,this.getSelectedCellObject());
if(!this.fire("BeforeCellClick",_8a)||(!!_89&&!nitobi.event.evaluate(_89.getOnBeforeCellClickEvent(),_8a))){
return;
}
this.setCellClicked(true);
this.setActiveCell(_88,evt.ctrlKey||evt.metaKey);
this.selection.selecting=true;
var _89=this.getSelectedColumnObject();
var _8a=new nitobi.grid.OnCellClickEventArgs(this,this.getSelectedCellObject());
this.fire("CellClick",_8a);
if(!!_89){
nitobi.event.evaluate(_89.getOnCellClickEvent(),_8a);
}
}
};
ntb_gridp.handleMouseUp=function(_8b){
this.getSelection().handleGrabbyMouseUp(_8b);
};
ntb_gridp.handleHeaderMouseUp=function(evt){
var _8d=this.findActiveCell(evt.srcElement);
if(!_8d){
this.focus();
return;
}
var _8e=parseInt(_8d.getAttribute("xi"));
this.fire("HeaderUp",_8e);
};
ntb_gridp.handleMouseMove=function(evt){
this.fire("MouseMove",evt);
};
ntb_gridp.handleHeaderMouseMove=function(evt){
var _91=this.findActiveCell(evt.srcElement);
if(_91==null){
return;
}
if(this.headerResizeHover(evt,_91)){
_91.style.cursor="w-resize";
}else{
(nitobi.browser.IE?_91.style.cursor="hand":_91.style.cursor="pointer");
}
};
ntb_gridp.headerResizeHover=function(evt,_93){
var x=evt.clientX;
var _95=_93.getBoundingClientRect(0,(nitobi.grid.Cell.getColumnNumber(_93)>this.getFrozenLeftColumnCount()?this.scroller.getScrollLeft():0));
return (x<_95.right&&x>_95.right-10);
};
ntb_gridp.handleHeaderMouseOver=function(e){
e.className=e.className.replace(/(ntb-column-indicator-border)(.*?)(\s|$)/g,function(){
return arguments[1]+arguments[2]+"hover ";
});
};
ntb_gridp.handleHeaderMouseOut=function(e){
e.className=e.className.replace(/(ntb-column-indicator-border)(.*?)(\s|$)/g,function(){
return arguments[0].replace("hover","");
});
};
ntb_gridp.handleCellMouseMove=function(evt){
this.setCellClicked(false);
var _99=this.findActiveCell(evt.srcElement);
if(_99==null){
return;
}
var sel=this.selection;
if(sel.selecting){
var _9b=evt.button;
var _9c=nitobi.html.getEventCoords(evt);
var x=_9c.x,y=_9c.y;
if(nitobi.browser.IE){
x=evt.clientX,y=evt.clientY;
}
if(_9b==1||(_9b==0&&!nitobi.browser.IE)){
if(!sel.expanding){
sel.redraw(_99);
}else{
var _9f=sel.expandStartCoords;
var _a0=0;
if(x>_9f.right){
_a0=Math.abs(x-_9f.right);
}else{
if(x<_9f.left){
_a0=Math.abs(x-_9f.left);
}
}
var _a1=0;
if(y>_9f.bottom){
_a1=Math.abs(y-_9f.bottom);
}else{
if(y<_9f.top){
_a1=Math.abs(y-_9f.top);
}
}
if(_a1>_a0){
expandDir="vert";
}else{
expandDir="horiz";
}
sel.expand(_99,expandDir);
}
this.ensureCellInView(_99);
}else{
this.selection.selecting=false;
}
}else{
this.hoverCell(_99);
this.hoverRow(_99.parentNode);
}
};
ntb_gridp.handleMouseWheel=function(_a2){
this.focus();
var _a3=0;
if(_a2.wheelDelta){
_a3=_a2.wheelDelta/120;
}else{
if(_a2.detail){
_a3=-_a2.detail/3;
}
}
this.scrollVerticalRelative(-20*_a3);
nitobi.html.cancelEvent(_a2);
};
ntb_gridp.setActiveCell=function(_a4,_a5){
if(!_a4){
return;
}
this.blurActiveCell(this.activeCell);
this.focus();
this.activateCell(_a4);
var _a6=this.activeColumnObject;
this.selection.collapse(this.activeCell);
if(!this.isCellClicked()){
this.ensureCellInView(this.activeCell);
this.setCellClicked(false);
}
var row=_a4.parentNode;
this.setActiveRow(row,_a5);
var _a8=new nitobi.grid.OnCellFocusEventArgs(this,this.getSelectedCellObject());
this.fire("CellFocus",_a8);
if(!!_a6){
nitobi.event.evaluate(_a6.getOnCellFocusEvent(),_a8);
}
};
ntb_gridp.activateCell=function(_a9){
this.activeCell=_a9;
this.activeCellObject=new nitobi.grid.Cell(this,_a9);
this.activeColumnObject=this.getSelectedColumnObject();
};
ntb_gridp.blurActiveCell=function(_aa){
this.oldCell=_aa;
var _ab=this.activeColumnObject;
var _ac=new nitobi.grid.OnCellBlurEventArgs(this,this.getSelectedCellObject());
if(!!_ab){
if(!this.fire("CellBlur",_ac)||!nitobi.event.evaluate(_ab.getOnCellBlurEvent(),_ac)){
return;
}
}
};
ntb_gridp.getRowNodes=function(row){
return nitobi.grid.Row.getRowElements(this,nitobi.grid.Row.getRowNumber(row));
};
ntb_gridp.setActiveRow=function(row,_af){
var Row=nitobi.grid.Row;
var _b1=Row.getRowNumber(row);
var _b2=-1;
if(this.oldCell!=null){
_b2=Row.getRowNumber(this.oldCell);
}
if(this.selectedRows[0]!=null){
_b2=Row.getRowNumber(this.selectedRows[0]);
}
if(!_af||!this.isMultiRowSelectEnabled()){
if(_b1!=_b2&&_b2!=-1){
var _b3=new nitobi.grid.OnRowBlurEventArgs(this,this.getRowObject(_b2));
if(!this.fire("RowBlur",_b3)||!nitobi.event.evaluate(this.getOnRowBlurEvent(),_b3)){
return;
}
}
this.clearActiveRows();
}
if(this.isRowSelectEnabled()){
var _b4=Row.getRowElements(this,_b1);
this.midActiveRow=_b4.mid;
this.leftActiveRow=_b4.left;
if(row.getAttribute("select")=="1"){
this.clearActiveRow(row);
}else{
this.selectedRows.push(row);
if(this.leftActiveRow!=null){
this.leftActiveRow.setAttribute("select","1");
this.applyRowStyle(this.leftActiveRow);
}
if(this.midActiveRow!=null){
this.midActiveRow.setAttribute("select","1");
this.applyRowStyle(this.midActiveRow);
}
}
}
if(_b1!=_b2){
var _b5=new nitobi.grid.OnRowFocusEventArgs(this,this.getRowObject(_b1));
this.fire("RowFocus",_b5);
nitobi.event.evaluate(this.getOnRowFocusEvent(),_b5);
}
};
ntb_gridp.getSelectedRows=function(){
return this.selectedRows;
};
ntb_gridp.clearActiveRows=function(){
for(var i=0;i<this.selectedRows.length;i++){
var row=this.selectedRows[i];
this.clearActiveRow(row);
}
this.selectedRows=[];
};
ntb_gridp.selectAllRows=function(){
this.clearActiveRows();
for(var i=0;i<this.getDisplayedRowCount();i++){
var _b9=this.getCellElement(i,0);
if(_b9!=null){
var row=_b9.parentNode;
this.setActiveRow(row,true);
}
}
return this.selectedRows;
};
ntb_gridp.clearActiveRow=function(row){
var _bc=nitobi.grid.Row.getRowNumber(row);
var _bd=nitobi.grid.Row.getRowElements(this,_bc);
if(_bd.left!=null){
_bd.left.removeAttribute("select");
this.removeRowStyle(_bd.left);
}
if(_bd.mid!=null){
_bd.mid.removeAttribute("select");
this.removeRowStyle(_bd.mid);
}
};
ntb_gridp.applyCellStyle=function(_be){
if(_be==null){
return;
}
_be.style.background=this.CellActiveColor;
};
ntb_gridp.removeCellStyle=function(_bf){
if(_bf==null){
return;
}
_bf.style.background="";
};
ntb_gridp.applyRowStyle=function(row){
if(row==null){
return;
}
row.style.background=this.RowActiveColor;
};
ntb_gridp.removeRowStyle=function(row){
if(row==null){
return;
}
row.style.background="";
};
ntb_gridp.findActiveCell=function(_c2){
var _c3=5;
_c2==null;
for(var i=0;i<_c3&&_c2.getAttribute;i++){
var t=_c2.getAttribute("ebatype");
if(t=="cell"||t=="columnheader"){
return _c2;
}
_c2=_c2.parentNode;
}
return null;
};
ntb_gridp.attachToParentDomElement=function(_c6){
this.UiContainer=_c6;
this.fire("AttachToParent");
};
ntb_gridp.getToolbars=function(){
return this.toolbars;
};
ntb_gridp.adjustHorizontalScrollBars=function(){
var _c7=this.calculateWidth();
var _c8=$("ntb-grid-hscrollshow"+this.uid);
if((_c7<=parseInt(this.getWidth()))){
_c8.style.display="none";
}else{
_c8.style.display="block";
this.resizeScroller();
var _c9=this.getWidth()/this.calculateWidth();
this.hScrollbar.setRange(_c9);
}
};
ntb_gridp.createChildren=function(){
var L=nitobi.lang;
if(this.UiContainer!=null&&this.getGridContainer()==null){
this.renderFrame();
}
this.generateFrameCss();
var ls=this.loadingScreen=new nitobi.grid.LoadingScreen(this);
this.subscribe("Preinitialize",L.close(ls,ls.show));
this.subscribe("HtmlReady",L.close(ls,ls.hide));
this.subscribe("AfterGridResize",L.close(ls,ls.resize));
ls.initialize();
ls.attachToElement($("ntb-grid-overlay"+this.uid));
ls.show();
var cr=new nitobi.grid.ColumnResizer(this);
cr.onAfterResize.subscribe(L.close(this,this.afterColumnResize));
this.columnResizer=cr;
var gr=new nitobi.grid.GridResizer(this);
gr.widthFixed=this.isWidthFixed();
gr.heightFixed=this.isHeightFixed();
gr.minWidth=this.getMinWidth();
gr.minHeight=Math.max(this.getMinHeight(),(this.getHeaderHeight()+this.getscrollbarHeight()));
gr.onAfterResize.subscribe(L.close(this,this.afterResize));
this.gridResizer=gr;
var sc=this.Scroller=this.scroller=new nitobi.grid.Scroller3x3(this,this.getHeight(),this.getDisplayedRowCount(),this.getColumnCount(),this.getfreezetop(),this.getFrozenLeftColumnCount());
sc.setRowHeight(this.getRowHeight());
sc.setHeaderHeight(this.getHeaderHeight());
sc.onHtmlReady.subscribe(this.handleHtmlReady,this);
this.subscribe("TableConnected",L.close(sc,sc.setDataTable));
sc.setDataTable(this.datatable);
this.initializeSelection();
this.createRenderers();
var sv=this.Scroller.view;
sv.midleft.rowRenderer=this.MidLeftRenderer;
sv.midcenter.rowRenderer=this.MidCenterRenderer;
sv.topleft.rowRenderer=this.TopLeftRenderer;
sv.topcenter.rowRenderer=this.TopCenterRenderer;
this.mapToHtml();
var vs=this.vScrollbar=new nitobi.ui.VerticalScrollbar();
vs.attachToParent(this.element,$("vscroll"+this.uid));
vs.subscribe("ScrollByUser",L.close(this,this.scrollVertical));
this.subscribe("PercentHeightChanged",L.close(vs,vs.setRange));
this.subscribe("ScrollVertical",L.close(vs,vs.setScrollPercent));
this.setscrollbarWidth(vs.getWidth());
var hs=this.hScrollbar=new nitobi.ui.HorizontalScrollbar();
hs.attachToParent(this.element,$("hscroll"+this.uid));
hs.subscribe("ScrollByUser",L.close(this,this.scrollHorizontal));
this.subscribe("PercentWidthChanged",L.close(hs,hs.setRange));
this.subscribe("ScrollHorizontal",L.close(hs,hs.setScrollPercent));
this.setscrollbarHeight(hs.getHeight());
};
ntb_gridp.createToolbars=function(_d2){
var tb=this.toolbars=new nitobi.ui.Toolbars((this.isToolbarEnabled()?_d2:0));
var _d4=document.getElementById("toolbarContainer"+this.uid);
tb.setWidth(this.getWidth());
tb.setHeight(this.getToolbarHeight());
tb.setRowInsertEnabled(this.isRowInsertEnabled());
tb.setRowDeleteEnabled(this.isRowDeleteEnabled());
tb.attachToParent(_d4);
var L=nitobi.lang;
tb.subscribe("InsertRow",L.close(this,this.insertAfterCurrentRow));
tb.subscribe("DeleteRow",L.close(this,this.deleteCurrentRow));
tb.subscribe("Save",L.close(this,this.save));
tb.subscribe("Refresh",L.close(this,this.refresh));
this.subscribe("AfterGridResize",L.close(this,this.resizeToolbars));
};
ntb_gridp.resizeToolbars=function(){
this.toolbars.setWidth(this.getWidth());
this.toolbars.resize();
};
ntb_gridp.scrollVerticalRelative=function(_d6){
var st=this.scroller.getScrollTop()+_d6;
var mc=this.Scroller.view.midcenter;
percent=st/(mc.container.offsetHeight-mc.element.offsetHeight);
this.scrollVertical(percent);
};
ntb_gridp.scrollVertical=function(_d9){
this.focus();
this.clearHover();
var _da=this.scroller.getScrollTopPercent();
this.scroller.setScrollTopPercent(_d9);
this.fire("ScrollVertical",_d9);
if(_d9>0.99&&_da<0.99){
this.fire("ScrollHitBottom",_d9);
}
if(_d9<0.01){
this.fire("ScrollHitTop",_d9);
}
};
ntb_gridp.scrollHorizontalRelative=function(_db){
var sl=this.scroller.getScrollLeft()+_db;
var mc=this.scroller.view.midcenter;
percent=sl/(mc.container.offsetWidth-mc.element.offsetWidth);
this.scrollHorizontal(percent);
};
ntb_gridp.scrollHorizontal=function(_de){
this.focus();
this.clearHover();
this.scroller.setScrollLeftPercent(_de);
this.fire("ScrollHorizontal",_de);
if(_de>0.99){
this.fire("ScrollHitRight",_de);
}
if(_de<0.01){
this.fire("ScrollHitLeft",_de);
}
};
ntb_gridp.getScrollSurface=function(){
if(this.Scroller!=null){
return this.Scroller.view.midcenter.element;
}
};
ntb_gridp.getActiveView=function(){
var C=nitobi.grid.Cell;
return this.Scroller.getViewportByCoords(C.getRowNumber(this.activeCell),C.getColumnNumber(this.activeCell));
};
ntb_gridp.ensureCellInView=function(_e0){
var SS=this.getScrollSurface();
var AC=_e0||this.activeCell;
if(AC==null){
return;
}
var sct=0;
var scl=0;
if(nitobi.browser.MOZ||nitobi.browser.SAFARI){
sct=SS.scrollTop;
scl=SS.scrollLeft;
}
var R1=AC.getBoundingClientRect();
var R2=SS.getBoundingClientRect();
var B=EBA_SELECTION_BUFFER||0;
var up=R1.top-R2.top-B-sct;
var _e9=R1.bottom-R2.bottom+B-sct;
var _ea=R1.left-R2.left-B-scl;
var _eb=R1.right-R2.right+B-scl;
if(up<0){
this.scrollVerticalRelative(up);
}
if(_e9>0){
this.scrollVerticalRelative(_e9);
}
if(nitobi.grid.Cell.getColumnNumber(AC)>this.getFrozenLeftColumnCount()-1){
if(_ea<0){
this.scrollHorizontalRelative(_ea);
}
if(_eb>0){
this.scrollHorizontalRelative(_eb);
}
}
this.fire("CellCoordsChanged",R1);
};
ntb_gridp.updateCellRanges=function(){
if(this.frameRendered){
var _ec=this.getRowCount();
this.Scroller.updateCellRanges(this.getColumnCount(),_ec,this.getFrozenLeftColumnCount(),this.getfreezetop());
this.measure();
this.resizeScroller();
var _ed=this.isToolbarEnabled()?this.getHeight():this.getHeight()-this.getToolbarHeight();
this.fire("PercentHeightChanged",_ed/this.calculateHeight());
this.fire("PercentWidthChanged",this.getWidth()/this.calculateWidth());
}
};
ntb_gridp.measure=function(){
this.measureViews();
this.sizeValid=true;
};
ntb_gridp.measureViews=function(){
this.measureRows();
this.measureColumns();
};
ntb_gridp.measureColumns=function(){
var fL=this.getFrozenLeftColumnCount();
var wL=0;
var wT=0;
var _f1=this.getColumnDefinitions();
var _f2=_f1.length;
for(var i=0;i<_f2;i++){
if(_f1[i].getAttribute("Visible")=="1"||_f1[i].getAttribute("visible")=="1"){
var w=Number(_f1[i].getAttribute("Width"));
wT+=w;
if(i<fL){
wL+=w;
}
}
}
this.setleft(wL);
};
ntb_gridp.measureRows=function(){
var _f5=this.isColumnIndicatorsEnabled()?this.getHeaderHeight():0;
this.settop(this.calculateHeight(0,this.getfreezetop()-1)+_f5);
};
ntb_gridp.resizeScroller=function(){
var _f6=(this.getToolbars()!=null&&this.isToolbarEnabled()?this.getToolbarHeight():0);
var _f7=this.isColumnIndicatorsEnabled()?this.getHeaderHeight():0;
this.Scroller.resize(this.getHeight()-_f6-_f7);
};
ntb_gridp.resize=function(_f8,_f9){
this.setWidth(_f8);
this.setHeight(_f9);
this.generateCss();
this.fire("AfterGridResize",{source:this,width:_f8,height:_f9});
};
ntb_gridp.beforeResize=function(evt){
var _fb=new nitobi.base.EventArgs(this);
if(!nitobi.event.evaluate(this.getOnBeforeResizeEvent(),_fb)){
return;
}
this.gridResizer.startResize(this,evt);
};
ntb_gridp.afterResize=function(){
this.resize(this.gridResizer.newWidth,this.gridResizer.newHeight);
this.syncWithData();
};
ntb_gridp.afterColumnResize=function(_fc){
var col=this.getColumnObject(_fc.column);
var _fe=col.getWidth();
this.columnResize(col,_fe+_fc.dx);
};
ntb_gridp.columnResize=function(_ff,_100){
if(isNaN(_100)){
return;
}
_ff=(typeof _ff=="object"?_ff:this.getColumnObject(_ff));
var _101=_ff.getWidth();
_ff.setWidth(_100);
this.updateCellRanges();
if(nitobi.browser.IE7){
this.generateCss();
}else{
var _102=_ff.column;
var dx=_100-_101;
var C=nitobi.html.Css;
if(_102<this.getFrozenLeftColumnCount()){
var _105=C.getClass(".ntb-grid-leftwidth"+this.uid);
_105.width=(parseInt(_105.width)+dx)+"px";
var _106=C.getClass(".ntb-grid-centerwidth"+this.uid);
_106.width=(parseInt(_106.width)-dx)+"px";
}else{
var _107=C.getClass(".ntb-grid-surfacewidth"+this.uid);
_107.width=(parseInt(_107.width)+dx)+"px";
}
var _108=C.getClass(".ntb-column"+this.uid+"_"+(_102+1));
_108.width=(parseInt(_108.width)+dx)+"px";
this.adjustHorizontalScrollBars();
}
this.Selection.collapse(this.activeCell);
var _109=new nitobi.grid.OnAfterColumnResizeEventArgs(this,_ff);
nitobi.event.evaluate(_ff.getOnAfterResizeEvent(),_109);
};
ntb_gridp.initializeModel=function(){
this.model=nitobi.xml.createXmlDoc(nitobi.xml.serialize(nitobi.grid.modelDoc));
this.modelNode=this.model.documentElement.selectSingleNode("//nitobi.grid.Grid");
var _10a=nitobi.html.getScrollBarWidth();
if(_10a){
this.setscrollbarWidth(_10a);
this.setscrollbarHeight(_10a);
}
var xDec=this.model.selectSingleNode("state/nitobi.grid.Columns");
if(xDec==null){
var xDec=this.model.createElement("nitobi.grid.Columns");
this.model.documentElement.appendChild(nitobi.xml.importNode(this.model,xDec,true));
}
var cols=this.getColumnCount();
if(cols>0){
this.defineColumns(cols);
}else{
this.columnsDefined=false;
this.inferredColumns=true;
}
this.model.documentElement.setAttribute("ID",this.uid);
this.model.documentElement.setAttribute("uniqueID",this.uid);
};
ntb_gridp.clearDefaultData=function(rows){
for(var i=0;i<rows;i++){
var e=this.model.createElement("e");
e.setAttribute("xi",i+1);
xDec.appendChild(e);
}
};
ntb_gridp.createRenderers=function(){
var _110=this.uid;
var _111=this.getRowHeight();
var _112=["TopLeftRenderer","TopCenterRenderer","MidLeftRenderer","MidCenterRenderer"];
for(var i=0;i<4;i++){
this[_112[i]]=new nitobi.grid.RowRenderer(this.data,null,_111,null,null,_110);
}
};
ntb_gridp.bind=function(){
if(this.isBound()){
this.clear();
this.datatable.descriptor.reset();
}
};
ntb_gridp.dataBind=function(){
this.bind();
};
ntb_gridp.getDataSource=function(_114){
var _115=this.dataTableId||"_default";
if(_114){
_115=_114;
}
return this.data.getTable(_115);
};
ntb_gridp.getChangeLogXmlDoc=function(_116){
return this.getDataSource(_116).getChangeLogXmlDoc();
};
ntb_gridp.getComplete=function(_117){
if(null==_117.dataSource.xmlDoc){
ebaErrorReport("evtArgs.dataSource.xmlDoc is null or not defined. Likely the gethandler failed use fiddler to check the response","",EBA_ERROR);
this.fire("LoadingError");
return;
}
var _118=_117.dataSource.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+_117.dataSource.id+"']");
};
ntb_gridp.bindComplete=function(){
if(this.inferredColumns&&!this.columnsDefined){
this.defineColumns(this.datatable);
}
this.setRowCount(this.datatable.remoteRowCount);
this.setBound(true);
this.syncWithData();
};
ntb_gridp.syncWithData=function(_119){
if(this.isBound()){
this.Scroller.render(true);
this.fire("DataReady",{"source":this});
}
};
ntb_gridp.finalizeRowCount=function(rows){
this.rowCountKnown=true;
this.setRowCount(rows);
};
ntb_gridp.adjustRowCount=function(pct){
this.scrollVertical(pct);
};
ntb_gridp.setRowCount=function(rows){
this.xSET("RowCount",arguments);
if(this.getPagingMode()==nitobi.grid.PAGINGMODE_STANDARD){
if(this.getDataMode()==nitobi.data.DATAMODE_LOCAL){
this.setDisplayedRowCount(this.getRowsPerPage());
}
}else{
this.setDisplayedRowCount(rows);
}
this.rowCount=rows;
this.updateCellRanges();
};
ntb_gridp.getRowCount=function(){
return this.rowCount;
};
ntb_gridp.layout=function(_11d){
if(this.prevHeight!=this.getHeight()||this.prevWidth!=this.getWidth()){
this.prevHeight=this.getHeight();
this.prevWidth=this.getWidth();
this.layoutValid=false;
}
if(!this.layoutValid&&this.frameRendered){
this.layoutFrame();
this.generateFrameCss();
this.layoutValid=true;
}
};
ntb_gridp.layoutFrame=function(_11e){
if(!this.frameRendered){
return;
}
if(!this.Scroller){
return;
}
this.minHeight=this.getMinHeight();
this.minWidth=this.getMinWidth();
var _11f=false;
var _120=false;
var tbH=this.getToolbarHeight();
var rowH=this.getRowHeight();
var colW=20;
var sbH=this.getscrollbarHeight();
var sbW=this.getscrollbarWidth();
var hdrH=this.getHeaderHeight();
tbH=this.isToolbarEnabled()?tbH:0;
hdrH=this.isColumnIndicatorsEnabled?hdrH:0;
var minH=Math.max(this.minHeight,tbH+rowH+sbH+hdrH);
var maxH=this.Height;
var minW=Math.max(this.minWidth,colW+sbW);
var maxW=this.Width;
if(_11f){
var _12b=this.Scroller.minSurfaceWidth;
var _12c=this.Scroller.maxSurfaceWidth;
}else{
var _12b=this.Scroller.SurfaceWidth;
var _12c=_12b;
}
if(_120){
var _12d=this.Scroller.minSurfaceHeight;
var _12e=this.Scroller.maxSurfaceHeight;
}else{
var _12d=this.Scroller.SurfaceHeight;
var _12e=_12d;
}
var _12f=_12d+(tbH)+(hdrH);
var _130=_12b;
var _131=(_12f>maxH);
var _132=(_130>maxW);
var _131=(_132&&((_12f+20)>maxH))||_131;
var _132=(_131&&((_130+20)>maxW))||_132;
sbH=_132?sbH:0;
sbV=_131?sbV:0;
var vpH=_12f-hdrH-tbH-sbH;
var vpW=_130-sbW;
this.resize();
};
ntb_gridp.defineColumns=function(_135){
this.fire("BeforeColumnsDefined");
this.resetColumns();
var _136=null;
var _137=nitobi.lang.typeOf(_135);
this.inferredColumns=false;
switch(_137){
case "string":
_136=this.defineColumnsFromString(_135);
break;
case nitobi.lang.type.XMLNODE:
case nitobi.lang.type.XMLDOC:
case nitobi.lang.type.HTMLNODE:
_136=this.defineColumnsFromXml(_135);
break;
case nitobi.lang.type.ARRAY:
_136=this.defineColumnsFromArray(_135);
break;
case "object":
this.inferredColumns=true;
_136=this.defineColumnsFromData(_135);
break;
case "number":
_136=this.defineColumnsCollection(_135);
break;
default:
}
this.fire("AfterColumnsDefined");
this.defineColumnsFinalize();
return _136;
};
ntb_gridp.defineColumnsFromXml=function(_138){
if(_138==null||_138.childNodes.length==0){
return this.defineColumnsCollection(0);
}
if(_138.childNodes[0].nodeName==nitobi.xml.nsPrefix+"columndefinition"){
var _139=nitobi.grid.declarationConverterXslProc;
_138=nitobi.xml.transformToXml(_138,_139);
}
var wL=0,wT=0,wR=0;
var _13d=this.model.selectSingleNode("/state/Defaults/nitobi.grid.Column");
var _13e=this.getColumnDefinitions().length;
var cols=_138.childNodes.length;
var xDec=this.model.selectSingleNode("state/nitobi.grid.Columns");
var _141=_138.childNodes;
var fL=this.getFrozenLeftColumnCount();
if(_13e==0){
var cols=_141.length;
for(var i=0;i<cols;i++){
var col=_141[i];
var _145="";
var _146=col.nodeName;
var _147=col.selectSingleNode("ntb:texteditor|ntb:numbereditor|ntb:textareaeditor|ntb:imageeditor|ntb:linkeditor|ntb:dateeditor|ntb:lookupeditor|ntb:listboxeditor|ntb:checkboxeditor|ntb:passwordeditor");
var _148="TEXT";
var _149={"ntb:textcolumn":"EBATextColumn","ntb:numbercolumn":"EBANumberColumn","ntb:datecolumn":"EBADateColumn"};
var _145=_149[_146].replace("EBA","").replace("Column","").toLowerCase();
var _14a={"ntb:numbereditor":"EBANumberEditor","ntb:textareaeditor":"EBATextareaEditor","ntb:imageeditor":"EBAImageEditor","ntb:linkeditor":"EBALinkEditor","ntb:dateeditor":"EBADateEditor","ntb:lookupeditor":"EBALookupEditor","ntb:listboxeditor":"EBAListboxEditor","ntb:passwordeditor":"EBAPasswordEditor","ntb:checkboxeditor":"EBACheckboxEditor"};
if(_147!=null){
_148=_14a[_147.nodeName]||_148;
}else{
_148=_149[_146]||_148;
}
_148=_148.replace("EBA","").replace("Editor","").replace("Column","").toUpperCase();
var e=this.model.selectSingleNode("/state/Defaults/nitobi.grid.Column[@DataType='"+(_145)+"' and @type='"+_148+"' and @editor='"+_148+"']").cloneNode(true);
this.setModelValues(e,col);
var _14c=_149[col.nodeName]||"EBATextColumn";
this.defineColumnDatasource(e);
this.defineColumnBinding(e);
xDec.appendChild(e);
var _14d=e.getAttribute("GetHandler");
if(_14d){
var _14e=e.getAttribute("DatasourceId");
if(!_14e||_14e==""){
_14e="columnDatasource_"+i+"_"+this.uid;
e.setAttribute("DatasourceId",_14e);
}
var dt=new nitobi.data.DataTable("local",this.getPagingMode()==nitobi.grid.PAGINGMODE_LIVESCROLLING,{GridId:this.getID()},{GridId:this.getID()},this.isAutoKeyEnabled());
dt.initialize(_14e,_14d,null);
dt.async=false;
this.data.add(dt);
var _150=[];
_150[0]=e;
var _151=e.getAttribute("editor");
var _152=null;
var _153=null;
if(e.getAttribute("editor")=="LOOKUP"){
_152=0;
_153=1;
dt.async=true;
}
dt.get(_152,_153,this,nitobi.lang.close(this,this.editorDataReady,[e]),function(){
});
}
}
this.measureColumns();
this.setColumnCount(cols);
}
var _154;
_154=_138.selectSingleNode("/"+nitobi.xml.nsPrefix+"grid/"+nitobi.xml.nsPrefix+"datasources");
if(_154){
this.Declaration.datasources=nitobi.xml.createXmlDoc(_154.xml);
}
return xDec;
};
ntb_gridp.defineColumnsFinalize=function(){
this.setColumnsDefined(true);
if(this.connected){
if(this.frameRendered){
this.makeXSL();
this.generateColumnCss();
this.renderHeaders();
}
}
};
ntb_gridp.defineColumnDatasource=function(_155){
var val=_155.getAttribute("Datasource");
if(val!=null){
var ds=new Array();
try{
ds=eval(val);
}
catch(e){
var _158=val.split(",");
if(_158.length>0){
for(var i=0;i<_158.length;i++){
var item=_158[i];
ds[i]={text:item.split(":")[0],display:item.split(":")[1]};
}
}
return;
}
if(typeof (ds)=="object"&&ds.length>0){
var _15b=new nitobi.data.DataTable("unbound",this.getPagingMode()==nitobi.grid.PAGINGMODE_LIVESCROLLING,{GridId:this.getID()},{GridId:this.getID()},this.isAutoKeyEnabled());
var _15c="columnDatasource"+new Date().getTime();
_15b.initialize(_15c);
_155.setAttribute("DatasourceId",_15c);
var _15d="";
for(var item in ds[0]){
_15d+=item+"|";
}
_15d=_15d.substring(0,_15d.length-1);
_15b.initializeColumns(_15d);
for(var i=0;i<ds.length;i++){
_15b.createRecord(null,i);
for(var item in ds[i]){
_15b.updateRecord(i,item,ds[i][item]);
}
}
this.data.add(_15b);
this.editorDataReady(_155);
}
}
};
ntb_gridp.defineColumnsFromData=function(_15e){
if(_15e==null){
_15e=this.datatable;
}
var _15f=_15e.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasourcestructure");
if(_15f==null){
return this.defineColumnsCollection(0);
}
var _160=_15f.getAttribute("FieldNames");
if(_160.length==0){
return this.defineColumnsCollection(0);
}
var _161=_15f.getAttribute("defaults");
var _162=this.defineColumnsFromString(_160);
for(var i=0;i<_162.length;i++){
if(_161&&i<_161.length){
_162[i].setAttribute("initial",_161[i]||"");
}
_162[i].setAttribute("width",100);
}
this.inferredColumns=true;
return _162;
};
ntb_gridp.defineColumnsFromString=function(_164){
return this.defineColumnsFromArray(_164.split("|"));
};
ntb_gridp.defineColumnsFromArray=function(_165){
var cols=_165.length;
var _167=this.defineColumnsCollection(cols);
for(var i=0;i<cols;i++){
var col=_167[i];
if(typeof (_165[i])=="string"){
col.setAttribute("ColumnName",_165[i]);
col.setAttribute("xdatafld_orig",_165[i]);
col.setAttribute("DataField_orig",_165[i]);
col.setAttribute("Label",_165[i]);
if(typeof (this.fieldMap[_165[i]])!="undefined"){
col.setAttribute("xdatafld",this.fieldMap[_165[i]]);
col.setAttribute("DataField",this.fieldMap[_165[i]]);
}else{
col.setAttribute("xdatafld","unbound");
col.setAttribute("DataField","unbound");
}
}else{
if(_165[i].name!="_xk"){
col.setAttribute("ColumnName",col.name);
col.setAttribute("xdatafld_orig",col.name);
col.setAttribute("DataField_orig",col.name);
col.setAttribute("xdatafld",this.fieldMap[_165[i].name]);
col.setAttribute("DataField",this.fieldMap[_165[i].name]);
col.setAttribute("Width",col.width);
col.setAttribute("Label",col.label);
col.setAttribute("Initial",col.initial);
col.setAttribute("Mask",col.mask);
}
}
}
this.setColumnCount(cols);
return _167;
};
ntb_gridp.defineColumnBindings=function(){
var xslt=nitobi.grid.rowXslProc.stylesheet;
var cols=this.getColumnDefinitions();
for(var i=0;i<cols.length;i++){
var e=cols[i];
this.defineColumnBinding(e,xslt);
e.setAttribute("xi",i);
}
nitobi.grid.rowXslProc=nitobi.xml.createXslProcessor(xslt);
};
ntb_gridp.defineColumnBinding=function(_16e,xslt){
if(this.fieldMap==null){
return;
}
var _170=_16e.getAttribute("xdatafld");
var _171=_16e.getAttribute("xdatafld_orig");
if(_171==null||_171==""){
_16e.setAttribute("xdatafld_orig",_170);
_16e.setAttribute("DataField_orig",_170);
}else{
_170=_16e.getAttribute("xdatafld_orig");
}
_16e.setAttribute("ColumnName",_170);
var _172=this.fieldMap[_170];
if(typeof (_172)!="undefined"){
_16e.setAttribute("xdatafld",_172);
_16e.setAttribute("DataField",_172);
}
this.formatBinding(_16e,"CssStyle",xslt);
this.formatBinding(_16e,"ClassName",xslt);
this.formatBinding(_16e,"Value",xslt);
};
ntb_gridp.formatBinding=function(_173,_174,xslt){
var _176=_173.getAttribute(_174);
var _177=_173.getAttribute(_174+"_orig");
if(_176==null||_176==""){
return;
}
if(_177==null||_177==""){
_173.setAttribute(_174+"_orig",_176);
}
_176=_173.getAttribute(_174+"_orig");
var re=new RegExp("\\{.[^}]*}","gi");
var _179=_176.match(re);
if(_179==null){
return;
}
for(var i=0;i<_179.length;i++){
var _17b=_179[i];
var _17c=_17b;
var _17d=new RegExp("\\$.*?[^0-9a-zA-Z_]","gi");
var _17e=_17c.match(_17d);
for(var j=0;j<_17e.length;j++){
var _180=_17e[j];
var _181=_180.substring(0,_180.length-1);
var _182=_181.substring(1);
var _183=this.fieldMap[_182]+"";
_17c=_17c.replace(_181,_183.substring(1)||"");
}
_17c=_17c.substring(1,_17c.length-1);
_176=_176.replace(_17b,_17c).replace(/\{\}/g,"");
}
_173.setAttribute(_174,_176);
};
ntb_gridp.defineColumnsCollection=function(cols){
var xDec=this.model.selectSingleNode("state/nitobi.grid.Columns");
var _186=xDec.childNodes;
var _187=this.model.selectSingleNode("/state/Defaults/nitobi.grid.Column");
for(var i=0;i<cols;i++){
var e=_187.cloneNode(true);
xDec.appendChild(e);
e.setAttribute("xi",i);
e.setAttribute("title",(i>25?String.fromCharCode(Math.floor(i/26)+65):"")+(String.fromCharCode(i%26+65)));
}
this.setColumnCount(cols);
var _186=xDec.selectNodes("*");
return _186;
};
ntb_gridp.resetColumns=function(){
this.fire("BeforeClearColumns");
this.inferredColumns=true;
this.columnsDefined=false;
var _18a=this.model.selectSingleNode("state/nitobi.grid.Columns");
var xDec=this.model.createElement("nitobi.grid.Columns");
if(_18a==null){
this.model.documentElement.appendChild(xDec);
}else{
this.model.documentElement.replaceChild(xDec,_18a);
}
this.setColumnCount(0);
this.fire("AfterClearColumns");
};
ntb_gridp.renderHeaders=function(){
if(this.getColumnDefinitions().length>0){
this.Scroller.clearSurfaces(false,true);
var _18c=0;
endRow=this.getfreezetop()-1;
var tl=this.Scroller.view.topleft;
tl.top=this.getHeaderHeight();
tl.left=0;
tl.renderGap(_18c,endRow,false,"*");
var tc=this.Scroller.view.topcenter;
tc.top=this.getHeaderHeight();
tc.left=0;
tc.renderGap(_18c,endRow,false);
}
};
ntb_gridp.initializeSelection=function(){
var sel=new nitobi.grid.Selection(this,this.isDragFillEnabled());
sel.setRowHeight(this.getRowHeight());
sel.onAfterExpand.subscribe(this.afterExpandSelection,this);
sel.onBeforeExpand.subscribe(this.beforeExpandSelection,this);
sel.onMouseUp.subscribe(this.handleSelectionMouseUp,this);
this.selection=this.Selection=sel;
};
ntb_gridp.beforeExpandSelection=function(evt){
this.setExpanding(true);
this.fire("BeforeDragFill",new nitobi.base.EventArgs(this,evt));
};
ntb_gridp.afterExpandSelection=function(evt){
var sel=this.selection;
var _193=sel.getCoords();
var _194=_193.top.y;
var _195=_193.bottom.y;
var _196=_193.top.x;
var _197=_193.bottom.x;
var _198=this.getTableForSelection({top:{x:sel.expandStartLeftColumn,y:sel.expandStartTopRow},bottom:{x:sel.expandStartRightColumn,y:sel.expandStartBottomRow}});
var data="",_19a=this.getClipboard();
if(sel.expandingVertical){
if(sel.expandStartBottomRow>_195&&_194>=sel.expandStartTopRow){
for(var i=sel.expandStartLeftColumn;i<=sel.expandStartRightColumn;i++){
for(var j=_195+1;j<sel.expandStartBottomRow+1;j++){
this.getCellObject(j,i).setValue("");
}
}
}else{
var _19d=(sel.expandStartBottomRow<_195);
var _19e=(sel.expandStartTopRow>_194);
var _19f=(_19d||_19e);
if(_19f){
if(_198.lastIndexOf("\n")==_198.length-1){
_198=_198.substring(0,_198.length-1);
}
var rep=(Math.floor((sel.getHeight()-!_19f)/sel.expandStartHeight));
for(var i=0;i<rep;i++){
data+=_198+(nitobi.browser.MOZ||nitobi.browser.SAFARI?"\n":"");
}
_1a1=_198.split("\n");
var mod=(sel.getHeight()-!_19f)%sel.expandStartHeight;
var val="";
if(_19d){
_1a1.splice(mod,_1a1.length-mod);
val=data+_1a1.join("\n")+(_1a1.length>0?"\n":"");
}else{
_1a1.splice(0,_1a1.length-mod);
val=_1a1.join("\n")+(_1a1.length>0?"\n":"")+data;
}
_19a.value=val;
this.pasteDataReady(_19a);
}
}
}else{
if(sel.expandStartRightColumn>_197&&_196>=sel.expandStartLeftColumn){
for(var i=_196+1;i<=sel.expandStartRightColumn+1;i++){
for(var j=sel.expandStartTopRow;j<sel.expandStartBottomRow;j++){
this.getCellObject(j,i).setValue("");
}
}
}else{
var _1a4=sel.expandStartRightColumn<_197;
var _1a5=sel.expandStartLeftColumn>_196;
var _19f=(_1a4||_1a5);
if(_19f){
var mod=(sel.getWidth()-!_19f)%sel.expandStartWidth;
var _1a6=(nitobi.browser.MOZ||nitobi.browser.SAFARI?"\n":"\r\n");
if(_198.lastIndexOf(_1a6)==_198.length-_1a6.length){
_198=_198.substring(0,_198.length-_1a6.length);
}
var _1a1=_198.replace(/\r/g,"").split("\n");
var data=new Array(_1a1.length);
var rep=(Math.floor((sel.getWidth()-!_19f)/sel.expandStartWidth));
for(var i=0;i<_1a1.length;i++){
var _1a7=_1a1[i].split("\t");
for(var j=0;j<rep;j++){
data[i]=(data[i]==null?[]:data[i]).concat(_1a7);
}
if(mod!=0){
if(_1a4){
data[i]=data[i].concat(_1a7.splice(0,mod));
}else{
data[i]=_1a7.splice(mod,_1a7.length-mod).concat(data[i]);
}
}
data[i]=data[i].join("\t");
}
_19a.value=data.join("\n")+"\n";
this.pasteDataReady(_19a);
}
}
}
this.setExpanding(false);
this.fire("AfterDragFill",new nitobi.base.EventArgs(this,evt));
};
ntb_gridp.calculateHeight=function(_1a8,end){
_1a8=(_1a8!=null)?_1a8:0;
var _1aa=this.getDisplayedRowCount();
end=(end!=null)?end:_1aa-1;
return (end-_1a8+1)*this.getRowHeight();
};
ntb_gridp.calculateWidth=function(_1ab,end){
var _1ad=this.getColumnDefinitions();
var cols=_1ad.length;
_1ab=_1ab||0;
end=(end!=null)?Math.min(end,cols):cols;
var wT=0;
for(var i=_1ab;i<end;i++){
if(_1ad[i].getAttribute("Visible")=="1"||_1ad[i].getAttribute("visible")=="1"){
wT+=Number(_1ad[i].getAttribute("Width"));
}
}
return (wT);
};
ntb_gridp.maximize=function(){
var x,y;
var _1b3=this.element.offsetParent;
x=_1b3.clientWidth;
y=_1b3.clientHeight;
this.resize(x,y);
};
ntb_gridp.editorDataReady=function(_1b4){
var _1b5=_1b4.getAttribute("DisplayFields").split("|");
var _1b6=_1b4.getAttribute("ValueField");
var _1b7=this.data.getTable(_1b4.getAttribute("DatasourceId"));
var _1b8=_1b4.getAttribute("Initial");
if(_1b8==""){
var _1b9=_1b4.getAttribute("type").toLowerCase();
switch(_1b9){
case "checkbox":
case "listbox":
var _1ba=_1b7.fieldMap[_1b6].substring(1);
var data=_1b7.getDataXmlDoc();
if(data!=null){
var val=data.selectSingleNode("//"+nitobi.xml.nsPrefix+"e[@"+_1ba+"='"+_1b8+"']");
if(val==null){
var _1bd=data.selectSingleNode("//"+nitobi.xml.nsPrefix+"e");
if(_1bd!=null){
_1b8=_1bd.getAttribute(_1ba);
}
}
}
break;
}
_1b4.setAttribute("Initial",_1b8);
}
if((_1b5.length==1&&_1b5[0]=="")&&(_1b6==null||_1b6=="")){
for(var item in _1b7.fieldMap){
_1b5[0]=_1b7.fieldMap[item].substring(1);
break;
}
}else{
for(var i=0;i<_1b5.length;i++){
_1b5[i]=_1b7.fieldMap[_1b5[i]].substring(1);
}
}
var _1c0=_1b5.join("|");
if(_1b6==null||_1b6==""){
_1b6=_1b5[0];
}else{
_1b6=_1b7.fieldMap[_1b6].substring(1);
}
_1b4.setAttribute("DisplayFields",_1c0);
_1b4.setAttribute("ValueField",_1b6);
};
ntb_gridp.headerClicked=function(_1c1){
var _1c2=this.getColumnObject(_1c1);
var _1c3=new nitobi.grid.OnHeaderClickEventArgs(this,_1c2);
if(!this.fire("HeaderClick",_1c3)||!nitobi.event.evaluate(_1c2.getOnHeaderClickEvent(),_1c3)){
return;
}
this.sort(_1c1);
};
ntb_gridp.addFilter=function(){
this.dataTable.addFilter(arguments);
};
ntb_gridp.clearFilter=function(){
this.dataTable.clearFilter();
};
ntb_gridp.setSortStyle=function(_1c4,_1c5,_1c6){
var _1c7=this.getColumnObject(_1c4);
if(_1c6){
this.sortColumn=null;
this.sortColumnCell=null;
this.Scroller.setSort(_1c4,"");
this.setColumnSortOrder(_1c4,"");
}else{
_1c7.setSortDirection(_1c5);
this.setColumnSortOrder(_1c4,_1c5);
this.sortColumn=_1c7;
this.sortColumnCell=_1c7.getHeaderElement();
this.Scroller.setSort(_1c4,_1c5);
}
};
ntb_gridp.sort=function(_1c8,_1c9){
var _1ca=this.getColumnObject(_1c8);
if(_1ca==null||!_1ca.isSortEnabled()||(!this.isSortEnabled())){
return;
}
var _1cb=new nitobi.grid.OnBeforeSortEventArgs(this,_1ca);
if(!this.fire("BeforeSort",_1cb)||!nitobi.event.evaluate(_1ca.getOnBeforeSortEvent(),_1cb)){
return;
}
if(_1c9==null||typeof (_1c9)=="undefined"){
_1c9=(_1ca.getSortDirection()=="Asc")?"Desc":"Asc";
}
this.setSortStyle(_1c8,_1c9);
var _1cc=_1ca.getColumnName();
var _1cd=_1ca.getDataType();
var _1ce=this.getSortMode()=="local"||(this.getDataMode()=="local"&&this.getSortMode()!="remote");
this.datatable.sort(_1cc,_1c9,_1cd,_1ce);
if(!_1ce){
this.datatable.flush();
}
this.clearSurfaces();
this.scrollVertical(0);
if(!_1ce){
this.loadDataPage(0);
}
this.subscribeOnce("HtmlReady",this.handleAfterSort,this,[_1ca]);
};
ntb_gridp.handleAfterSort=function(_1cf){
var _1d0=new nitobi.grid.OnAfterSortEventArgs(this,_1cf);
this.fire("AfterSort",_1d0);
nitobi.event.evaluate(_1cf.getOnAfterSortEvent(),_1d0);
};
ntb_gridp.handleDblClick=function(evt){
var cell=this.activeCellObject;
var col=this.activeColumnObject;
var _1d4=new nitobi.grid.OnCellDblClickEventArgs(this,cell);
return this.fire("CellDblClick",_1d4)&&nitobi.event.evaluate(col.getOnCellDblClickEvent(),_1d4);
};
ntb_gridp.clearData=function(){
if(this.getDataMode()!="local"){
this.datatable.flush();
}
};
ntb_gridp.clearColumnHeaderSortOrder=function(){
if(this.sortColumn){
var _1d5=this.sortColumn;
var _1d6=_1d5.getHeaderElement();
var css=_1d6.className;
css=css.replace(/ascending/gi,"").replace(/descending/gi,"");
_1d6.className=css;
this.sortColumn=null;
}
};
ntb_gridp.setColumnSortOrder=function(_1d8,_1d9){
this.clearColumnHeaderSortOrder();
var _1da=this.getColumnObject(_1d8);
var _1db=_1da.getHeaderElement();
var C=nitobi.html.Css;
var css=_1db.className;
if(_1d9==""){
_1db.className=css.replace(/(ntb-column-indicator-border)(.*?)(\s|$)/g,"")+" ntb-column-indicator-border";
_1d9="Desc";
}else{
_1db.className=css.replace(/(ntb-column-indicator-border)(.*?)(\s|$)/g,function(m){
var repl=(_1d9=="Desc"?"descending":"ascending");
return (m.indexOf("hover")>0?m.replace("hover",repl+"hover"):m+repl);
});
}
_1da.setSortDirection(_1d9);
this.sortColumn=_1da;
this.sortColumnCell=_1db;
};
ntb_gridp.initializeState=function(){
};
ntb_gridp.mapToHtml=function(_1e0){
if(_1e0==null){
_1e0=this.UiContainer;
}
this.Scroller.mapToHtml(_1e0);
this.element=document.getElementById("grid"+this.uid);
this.element.jsObject=this;
};
ntb_gridp.generateCss=function(){
this.generateFrameCss();
};
ntb_gridp.generateColumnCss=function(){
this.generateCss();
};
ntb_gridp.generateFrameCss=function(){
var _1e1=nitobi.xml.serialize(this.model);
if(this.oldModel==_1e1){
return;
}
this.oldModel=nitobi.xml.serialize(this.model);
if(nitobi.browser.IE&&document.compatMode=="CSS1Compat"){
this.frameCssXslProc.addParameter("IE","true","");
}
var _1e2=nitobi.xml.transformToString(this.model,this.frameCssXslProc);
if(!nitobi.browser.SAFARI&&this.stylesheet==null){
this.stylesheet=nitobi.html.Css.createStyleSheet();
}
var ss=this.getScrollSurface();
var _1e4=0;
var _1e5=0;
if(ss!=null){
_1e4=ss.scrollTop;
_1e5=ss.scrollLeft;
}
if(this.oldFrameCss!=_1e2){
this.oldFrameCss=_1e2;
if(nitobi.browser.SAFARI){
this.generateFrameCssSafari();
}else{
try{
this.stylesheet.cssText=_1e2;
}
catch(e){
}
if(ss!=null){
if(nitobi.browser.MOZ){
this.scrollVerticalRelative(_1e4);
this.scrollHorizontalRelative(_1e5);
}
ss.style.top="0px";
ss.style.left="0px";
}
}
}
};
ntb_gridp.generateFrameCssSafari=function(){
var ss=document.styleSheets[0];
var u=this.uid;
var t=this.getTheme();
var _1e9=this.getWidth();
var _1ea=this.getHeight();
var _1eb=(this.isVScrollbarEnabled()?1:0);
var _1ec=(this.isHScrollbarEnabled()?1:0);
var _1ed=(this.isToolbarEnabled()?1:0);
var _1ee=this.calculateWidth(0,this.getFrozenLeftColumnCount());
var _1ef=this.calculateWidth(this.getFrozenLeftColumnCount(),this.getColumnCount());
var _1f0=_1ee+_1ef;
var _1f1=_1ea-this.getscrollbarHeight()*_1ec-this.getToolbarHeight()*_1ed;
var _1f2=_1e9-this.getscrollbarWidth()*_1eb;
var _1f3=_1f1-this.gettop();
var _1f4=nitobi.html.Css.addRule;
var p="ntb-grid-";
if(this.rules==null){
this.rules={};
this.rules[".ntb-grid-datablock"]=_1f4(ss,".ntb-grid-datablock","table-layout:fixed;width:100%;");
this.rules[".ntb-grid-headerblock"]=_1f4(ss,".ntb-grid-headerblock","table-layout:fixed;width:100%;");
_1f4(ss,".ntbcellborder"+u,"overflow:hidden;text-decoration:none;margin:0px;border-right:1px solid #c0c0c0;border-bottom:1px solid #c0c0c0;white-space:nowrap;");
_1f4(ss,"."+p+"overlay"+u,"position:relative;z-index:1000;top:0px;left:0px;");
_1f4(ss,"."+p+"scroller"+u,"overflow:hidden;text-align:left;");
_1f4(ss,".ntb-grid","padding:0px;margin:0px;border:1px solid #cccccc;");
_1f4(ss,".ntb-scroller","padding:0px;");
_1f4(ss,".ntb-scrollcorner","padding:0px;");
_1f4(ss,".ntb-input-border","table-layout:fixed;overflow:hidden;position:absolute;z-index:2000;top:-2000px;left:-2000px;;");
_1f4(ss,".ntb-column-resize-surface","filter:alpha(opacity=1);background-color:white;position:absolute;visibility:hidden;top:0;left:0;width:100;height:100;z-index:800;");
_1f4(ss,".ntb-column-indicator","overflow:hidden;white-space: nowrap;");
}
this.rules["#grid"+u]=_1f4(ss,"#grid"+u,"overflow:hidden;text-align:left;-moz-user-select: none;-khtml-user-select: none;user-select: none;"+(nitobi.browser.IE?"position:relative;":""));
this.rules["#grid"+u].style.height=_1ea+"px";
this.rules["#grid"+u].style.width=_1e9+"px";
_1f4(ss,".hScrollbarRange"+u,"width:"+_1f0+"px;");
_1f4(ss,".vScrollbarRange"+u,"");
_1f4(ss,"."+t+" .ntb-cell","overflow:hidden;white-space:nowrap;");
_1f4(ss,"."+t+" .ntb-cell-border","overflow:hidden;white-space:nowrap;"+(nitobi.browser.IE?"height:auto;":"")+";");
_1f4(ss,".ntb-grid-headershow"+u,"padding:0px;"+(this.isColumnIndicatorsEnabled()?"display:none;":"")+"");
_1f4(ss,".ntb-grid-vscrollshow"+u,"padding:0px;"+(_1eb?"":"display:none;")+"");
_1f4(ss,"#ntb-grid-hscrollshow"+u,"padding:0px;"+(_1ec?"":"display:none;")+"");
_1f4(ss,".ntb-grid-toolbarshow"+u,""+(_1ed?"":"display:none;")+"");
_1f4(ss,".ntb-grid-height"+u,"height:"+_1ea+"px;overflow:hidden;");
_1f4(ss,".ntb-grid-width"+u,"width:"+_1e9+"px;overflow:hidden;");
_1f4(ss,".ntb-grid-overlay"+u,"position:relative;z-index:1000;top:0px;left:0px;");
_1f4(ss,".ntb-grid-scroller"+u,"overflow:hidden;text-align:left;");
_1f4(ss,".ntb-grid-scrollerheight"+u,"height:"+(_1f0>_1e9?_1f1:_1f1+this.getscrollbarHeight())+"px;");
_1f4(ss,".ntb-grid-scrollerwidth"+u,"width:"+_1f2+"px;");
_1f4(ss,".ntb-grid-topheight"+u,"height:"+this.gettop()+"px;overflow:hidden;"+(this.gettop()==0?"display:none;":"")+"");
_1f4(ss,".ntb-grid-midheight"+u,"overflow:hidden;height:"+(_1f0>_1e9?_1f3:_1f3+this.getscrollbarHeight())+"px;");
_1f4(ss,".ntb-grid-leftwidth"+u,"width:"+this.getleft()+"px;overflow:hidden;text-align:left;");
_1f4(ss,".ntb-grid-centerwidth"+u,"width:"+(_1e9-this.getleft()-this.getscrollbarWidth()*_1eb)+"px;");
_1f4(ss,".ntb-grid-scrollbarheight"+u,"height:"+this.getscrollbarHeight()+"px;");
_1f4(ss,".ntb-grid-scrollbarwidth"+u,"width:"+this.getscrollbarWidth()+"px;");
_1f4(ss,".ntb-grid-toolbarheight"+u,"height:"+this.getToolbarHeight()+"px;");
_1f4(ss,".ntb-grid-surfacewidth"+u,"width:"+_1ef+"px;");
_1f4(ss,".ntb-grid-surfaceheight"+u,"height:100px;");
_1f4(ss,".ntb-hscrollbar"+u,(_1f0>_1e9?"display:block;":"display:none;"));
_1f4(ss,".ntb-row"+u,"height:"+this.getRowHeight()+"px;margin:0px;line-height:"+this.getRowHeight()+"px;");
_1f4(ss,".ntb-header-row"+u,"height:"+this.getHeaderHeight()+"px;");
var cols=this.getColumnDefinitions();
for(var i=1;i<=cols.length;i++){
var col=cols[i-1];
var _1f9=this.rules[".ntb-column"+u+"_"+(i)];
if(_1f9==null){
_1f9=this.rules[".ntb-column"+u+"_"+(i)]=_1f4(ss,".ntb-column"+u+"_"+(i));
}
_1f9.style.width=col.getAttribute("Width")+"px";
var _1fa=this.rules[".ntb-column-data"+u+"_"+(i)];
if(_1fa==null){
this.rules[".ntb-column-data"+u+"_"+(i)]=_1f4(ss,".ntb-column-data"+u+"_"+(i),"text-align:"+col.getAttribute("Align")+";");
}
}
};
ntb_gridp.clearSurfaces=function(){
this.selection.clearBoxes();
this.Scroller.clearSurfaces();
this.updateCellRanges();
this.cachedCells={};
};
ntb_gridp.renderFrame=function(){
var _1fb="IE";
if(nitobi.browser.MOZ){
_1fb="MOZ";
}else{
if(nitobi.browser.SAFARI){
_1fb="SAFARI";
}
}
this.frameXslProc.addParameter("browser",_1fb,"");
this.UiContainer.innerHTML=nitobi.xml.transformToString(this.model,this.frameXslProc);
this.attachDomEvents();
this.frameRendered=true;
this.fire("AfterFrameRender");
};
ntb_gridp.renderMiddle=function(){
this.Scroller.view.midleft.flushCache();
this.Scroller.view.midcenter.flushCache();
};
ntb_gridp.refresh=function(){
var _1fc=null;
if(!this.fire("BeforeRefresh",_1fc)){
return;
}
this.selectedRows=[];
this.clearSurfaces();
if(this.getDataMode()!="local"){
this.datatable.clearData();
}
this.syncWithData();
this.subscribeOnce("HtmlReady",this.handleAfterRefresh,this);
};
ntb_gridp.handleAfterRefresh=function(){
var _1fd=null;
this.fire("AfterRefresh",_1fd);
};
ntb_gridp.clear=function(){
this.selectedRows=[];
this.clearData();
this.clearSurfaces();
};
ntb_gridp.handleContextMenu=function(evt,obj){
var _200=this.getOnContextMenuEvent();
if(_200==null){
return true;
}else{
if(this.fire("ContextMenu")){
return true;
}else{
evt.cancelBubble=true;
evt.returnValue=false;
return false;
}
}
};
ntb_gridp.handleKeyPress=function(evt){
if(this.activeCell==null){
return;
}
var col=this.activeColumnObject;
this.fire("KeyPress",new nitobi.base.EventArgs(this,evt));
nitobi.event.evaluate(col.getOnKeyPressEvent(),evt);
nitobi.html.cancelEvent(evt);
return false;
};
ntb_gridp.handleKeyUp=function(evt){
if(this.activeCell==null){
return;
}
var col=this.activeColumnObject;
this.fire("KeyUp",new nitobi.base.EventArgs(this,evt));
nitobi.event.evaluate(col.getOnKeyUpEvent(),evt);
};
ntb_gridp.handleKey=function(evt,obj){
if(this.activeCell!=null){
var col=this.activeColumnObject;
var _208=new nitobi.base.EventArgs(this,evt);
if(!this.fire("KeyDown",_208)||!nitobi.event.evaluate(col.getOnKeyDownEvent(),_208)){
return;
}
}
var k=evt.keyCode;
k=k+(evt.shiftKey?256:0)+(evt.ctrlKey?512:0)+(evt.metaKey?1024:0);
switch(k){
case 529:
break;
case 35:
break;
case 36:
break;
case 547:
break;
case 548:
break;
case 34:
this.page(1);
break;
case 33:
this.page(-1);
break;
case 45:
this.insertAfterCurrentRow();
break;
case 46:
if(this.getSelectedRows().length>1){
this.deleteSelectedRows();
}else{
this.deleteCurrentRow();
}
break;
case 292:
this.selectHome();
break;
case 290:
this.pageSelect(1);
break;
case 289:
this.pageSelect(-1);
break;
case 296:
this.reselect(0,1);
break;
case 294:
this.reselect(0,-1);
break;
case 293:
this.reselect(-1,0);
break;
case 295:
this.reselect(1,0);
break;
case 577:
break;
case 579:
case 557:
this.copy(evt);
return true;
case 1091:
this.copy(evt);
return true;
case 600:
case 302:
break;
case 598:
case 301:
this.paste(evt);
return true;
break;
case 1110:
this.paste(evt);
return true;
case 35:
break;
case 36:
break;
case 547:
break;
case 548:
break;
case 13:
var et=this.getEnterTab().toLowerCase();
var _20b=0;
var vert=1;
if(et=="left"){
_20b=-1;
vert=0;
}else{
if(et=="right"){
_20b=1;
vert=0;
}else{
if(et=="down"){
_20b=0;
vert=1;
}else{
if(et=="up"){
_20b=0;
vert=-1;
}
}
}
}
this.move(_20b,vert);
break;
case 40:
this.move(0,1);
break;
case 269:
case 38:
this.move(0,-1);
break;
case 265:
case 37:
this.move(-1,0);
break;
case 9:
case 39:
this.move(1,0);
break;
case 577:
break;
case 595:
this.save();
break;
case 594:
this.refresh();
break;
case 590:
this.insertAfterCurrentRow();
break;
default:
this.edit(evt);
}
};
ntb_gridp.reselect=function(x,y){
var S=this.selection;
var row=nitobi.grid.Cell.getRowNumber(S.endCell)+y;
var _211=nitobi.grid.Cell.getColumnNumber(S.endCell)+x;
if(_211>=0&&_211<this.columnCount()&&row>=0){
var _212=this.getCellElement(row,_211);
if(!_212){
return;
}
S.changeEndCellWithDomNode(_212);
S.alignBoxes();
this.ensureCellInView(_212);
}
};
ntb_gridp.pageSelect=function(dir){
};
ntb_gridp.selectHome=function(){
var S=this.selection;
var row=nitobi.grid.Cell.getRowNumber(S.endCell);
this.reselect(0,-row);
};
ntb_gridp.edit=function(evt){
if(this.activeCell==null){
return;
}
var cell=this.activeCellObject;
var col=this.activeColumnObject;
var _219=new nitobi.grid.OnBeforeCellEditEventArgs(this,cell);
if(!this.fire("BeforeCellEdit",_219)||!nitobi.event.evaluate(col.getOnBeforeCellEditEvent(),_219)){
return;
}
var _21a=null;
var _21b=null;
var ctrl=null;
if(evt){
_21a=evt.keyCode||null;
_21b=evt.shiftKey||null;
ctrl=evt.ctrlKey||null;
}
var _21d="";
var _21e=null;
if((_21b&&(_21a>64)&&(_21a<91))||(!_21b&&((_21a>47)&&(_21a<58)))){
_21e=0;
}
if(!_21b){
if((_21a>64)&&(_21a<91)){
_21e=32;
}else{
if(_21a>95&&_21a<106){
_21e=-48;
}else{
if((_21a==189)||(_21a==109)){
_21d="-";
}else{
if((_21a>186)&&(_21a<188)){
_21e=-126;
}
}
}
}
}else{
}
if(_21e!=null){
_21d=String.fromCharCode(_21a+_21e);
}
if((!ctrl)&&(""!=_21d)||(_21a==113)||(_21a==0)||(_21a==null)||(_21a==32)){
if(col.isEditable()){
this.cellEditor=nitobi.form.ControlFactory.instance.getEditor(this,col);
if(this.cellEditor==null){
return;
}
this.cellEditor.setEditCompleteHandler(this.editComplete);
this.cellEditor.attachToParent(this.getToolsContainer());
this.cellEditor.bind(this,cell,_21d);
this.cellEditor.mimic();
this.setEditMode(true);
nitobi.html.cancelEvent(evt);
return false;
}
}else{
return;
}
};
ntb_gridp.editComplete=function(_21f){
var cell=_21f.cell;
var _221=cell.getColumnObject();
var _222=_21f.databaseValue;
var _223=_21f.displayValue;
var _224=new nitobi.grid.OnCellValidateEventArgs(this,cell,_222,cell.getValue());
if(!this.fire("CellValidate",_224)||!nitobi.event.evaluate(_221.getOnCellValidateEvent(),_224)){
return false;
}
cell.setValue(_222,_223);
_21f.editor.hide();
this.setEditMode(false);
var _225=new nitobi.grid.OnAfterCellEditEventArgs(this,cell);
this.fire("AfterCellEdit",_225);
nitobi.event.evaluate(_221.getOnAfterCellEditEvent(),_225);
try{
this.focus();
}
catch(e){
}
};
ntb_gridp.autoSave=function(){
if(this.isAutoSaveEnabled()){
return this.save();
}
return false;
};
ntb_gridp.selectCellByCoords=function(row,_227){
this.setPosition(row,_227);
};
ntb_gridp.setPosition=function(row,_229){
if(row>=0&&_229>=0){
this.setActiveCell(this.getCellElement(row,_229));
}
};
ntb_gridp.save=function(){
if(this.datatable.log.selectNodes("//"+nitobi.xml.nsPrefix+"data/*").length==0){
return;
}
if(!this.fire("BeforeSave")){
return;
}
this.datatable.save(nitobi.lang.close(this,this.saveCompleteHandler),this.getOnBeforeSaveEvent());
};
ntb_gridp.saveCompleteHandler=function(_22a){
if(this.getDataSource().getHandlerError()){
this.fire("HandlerError",_22a);
}
this.fire("AfterSave",_22a);
};
ntb_gridp.focus=function(){
try{
this.keyNav.focus();
this.fire("Focus",new nitobi.base.EventArgs(this));
if(!nitobi.browser.SAFARI){
nitobi.html.cancelEvent(nitobi.html.Event);
return false;
}
}
catch(e){
}
};
ntb_gridp.blur=function(){
this.clearActiveRows();
this.selection.clear();
this.blurActiveCell(null);
this.activateCell(null);
this.fire("Blur",new nitobi.base.EventArgs(this));
};
ntb_gridp.getRendererForColumn=function(col){
var _22c=this.getColumnCount();
if(col>=_22c){
col=_22c-1;
}
var _22d=this.getFrozenLeftColumnCount();
if(col<frozenLeft){
return this.MidLeftRenderer;
}else{
return this.MidCenterRenderer;
}
};
ntb_gridp.getColumnOuterTemplate=function(col){
return this.getRendererForColumn(col).xmlTemplate.selectSingleNode("//*[@match='ntb:e']/div/div["+col+"]");
};
ntb_gridp.getColumnInnerTemplate=function(col){
return this.getColumnOuterXslTemplate(col).selectSingleNode("*[2]");
};
ntb_gridp.makeXSL=function(){
var fL=this.getFrozenLeftColumnCount();
var cs=this.getColumnCount();
var rh=this.isRowHighlightEnabled();
var _233="_default";
if(this.datatable!=null){
_233=this.datatable.id;
}
var _234=0;
var _235=fL;
var _236=this.model.selectSingleNode("state/nitobi.grid.Columns");
this.TopLeftRenderer.generateXslTemplate(_236,null,_234,_235,this.isColumnIndicatorsEnabled(),this.isRowIndicatorsEnabled(),rh,this.isToolTipsEnabled());
this.TopLeftRenderer.dataTableId=_233;
_234=fL;
_235=cs-fL;
this.TopCenterRenderer.generateXslTemplate(_236,null,_234,_235,this.isColumnIndicatorsEnabled(),this.isRowIndicatorsEnabled(),rh,this.isToolTipsEnabled());
this.TopCenterRenderer.dataTableId=_233;
this.MidLeftRenderer.generateXslTemplate(_236,null,0,fL,0,this.isRowIndicatorsEnabled(),rh,this.isToolTipsEnabled(),"left");
this.MidLeftRenderer.dataTableId=_233;
this.MidCenterRenderer.generateXslTemplate(_236,null,fL,cs-fL,0,0,rh,this.isToolTipsEnabled());
this.MidCenterRenderer.dataTableId=_233;
this.fire("AfterMakeXsl");
};
ntb_gridp.render=function(){
this.generateCss();
this.updateCellRanges();
};
ntb_gridp.refilter=ntb_gridp.render;
ntb_gridp.getColumnDefinitions=function(){
return this.model.selectNodes("state/nitobi.grid.Columns/*");
};
ntb_gridp.getVisibleColumnDefinitions=function(){
return this.model.selectNodes("state/nitobi.grid.Columns/*[@Visible='1']");
};
ntb_gridp.initializeModelFromDeclaration=function(){
var _237=this.Declaration.grid.documentElement.attributes;
var len=_237.length;
for(var i=0;i<len;i++){
var _23a=_237[i];
var _23b=this.properties[_23a.nodeName];
if(_23b!=null){
this["set"+_23b.n](_23a.nodeValue);
}
}
this.model.documentElement.setAttribute("ID",this.uid);
this.model.documentElement.setAttribute("uniqueID",this.uid);
};
ntb_gridp.setModelValues=function(_23c,_23d){
var _23e=_23c.getAttribute("DataType");
var _23f=_23c.getAttribute("type").toLowerCase();
var _240=_23d.attributes;
for(var j=0;j<_240.length;j++){
var _242=_240[j];
var _243=_242.nodeName.toLowerCase();
var _244=this.xColumnProperties[_23e+"column"][_243]||this.xColumnProperties["column"][_243];
var _245=_242.nodeValue;
if(_244.t=="b"){
_245=nitobi.lang.boolToStr(nitobi.lang.toBool(_245));
}
_23c.setAttribute(_244.n,_245);
}
var _246=_23d.selectSingleNode("./ntb:"+_23f+"editor");
if(_246==null){
return;
}
var _247=_246.attributes;
for(var j=0;j<_247.length;j++){
var _242=_247[j];
var _243=_242.nodeName.toLowerCase();
var _244=this.xColumnProperties[_23f+"editor"][_243];
var _245=_242.nodeValue;
if(_244.t=="b"){
_245=nitobi.lang.boolToStr(nitobi.lang.toBool(_245));
}
_23c.setAttribute(_244.n,_245);
}
};
ntb_gridp.getNewRecordKey=function(){
var _248;
var key;
var _24a;
do{
_248=new Date();
key=(_248.getTime()+"."+Math.round(Math.random()*99));
_24a=this.datatable.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"e[@xk = '"+key+"']");
}while(_24a!=null);
return key;
};
ntb_gridp.insertAfterCurrentRow=function(){
if(this.activeCell){
var _24b=nitobi.grid.Cell.getRowNumber(this.activeCell);
this.insertRow(_24b+1);
}else{
this.insertRow();
}
};
ntb_gridp.insertRow=function(_24c){
var rows=parseInt(this.getDisplayedRowCount());
var xi=0;
if(_24c!=null){
xi=parseInt((_24c==null?rows:parseInt(_24c)));
xi--;
}
var _24f=new nitobi.grid.OnBeforeRowInsertEventArgs(this,this.getRowObject(xi));
if(!this.isRowInsertEnabled()||!this.fire("BeforeRowInsert",_24f)){
return;
}
var _250=this.datatable.getTemplateNode();
for(var i=0;i<this.columnCount();i++){
var _252=this.getColumnObject(i);
var _253=_252.getInitial();
if(_253==null||_253==""){
var _254=_252.getDataType();
if(_254==null||_254==""){
_254="text";
}
switch(_254){
case "text":
_253="";
break;
case "number":
_253=0;
break;
case "date":
_253="1900-01-01";
break;
}
}
var att=_252.getxdatafld().substr(1);
if(att!=null&&att!=""){
_250.setAttribute(att,_253);
}
}
this.clearSurfaces();
this.datatable.createRecord(_250,xi);
this.subscribeOnce("HtmlReady",this.handleAfterRowInsert,this,[xi]);
};
ntb_gridp.handleAfterRowInsert=function(xi){
this.fire("AfterRowInsert",new nitobi.grid.OnAfterRowInsertEventArgs(this,this.getRowObject(xi)));
this.setActiveCell(this.getCellElement(xi,0));
};
ntb_gridp.deleteCurrentRow=function(){
if(this.activeCell){
this.deleteRow(nitobi.grid.Cell.getRowNumber(this.activeCell));
}else{
alert("First select a record to delete.");
}
};
ntb_gridp.deleteSelectedRows=function(){
var _257=new nitobi.grid.OnBeforeRowDeleteEventArgs(this,this.getSelectedRows());
if(!this.isRowDeleteEnabled()||!this.fire("BeforeRowDelete",_257)){
return;
}
var _258=this.getSelectedRows();
var _259=[];
for(row in _258){
_259.push(parseInt(_258[row].getAttribute("xi")));
}
_259.sort(function(a,b){
return a-b;
});
this.clearSurfaces();
var rows=this.getDisplayedRowCount();
try{
this.datatable.deleteRecordsArray(_259);
if(rows<=0){
this.activeCell=null;
}
this.subscribeOnce("HtmlReady",this.handleAfterRowDelete,this,_259);
}
catch(err){
this.dataBind();
}
};
ntb_gridp.deleteRow=function(_25d){
var _25e=new nitobi.grid.OnBeforeRowDeleteEventArgs(this,this.getRowObject(_25d));
if(!this.isRowDeleteEnabled()||!this.fire("BeforeRowDelete",_25e)){
return;
}
this.clearSurfaces();
var rows=this.getDisplayedRowCount();
try{
this.datatable.deleteRecord(_25d);
rows--;
if(rows<=0){
this.activeCell=null;
}
this.subscribeOnce("HtmlReady",this.handleAfterRowDelete,this,[_25d]);
}
catch(err){
this.dataBind();
}
};
ntb_gridp.handleAfterRowDelete=function(xi){
this.fire("AfterRowDelete",new nitobi.grid.OnBeforeRowDeleteEventArgs(this,this.getRowObject(xi)));
this.setActiveCell(this.getCellElement(xi,0));
};
ntb_gridp.page=function(dir){
};
ntb_gridp.move=function(h,v){
if(this.activeCell!=null){
var hs=1;
var vs=1;
h=(h*hs);
v=(v*vs);
var cell=nitobi.grid.Cell;
var _267=cell.getColumnNumber(this.activeCell);
var _268=cell.getRowNumber(this.activeCell);
this.selectCellByCoords(_268+v,_267+h);
var _269=new nitobi.grid.CellEventArgs(this,this.activeCell);
if(_267+1==this.getVisibleColumnDefinitions().length&&h==1){
this.fire("HitRowEnd",_269);
}else{
if(_267==0&&h==-1){
this.fire("HitRowStart",_269);
}
}
}
};
ntb_gridp.handleSelectionMouseUp=function(evt){
if(this.isCellClicked()){
this.ensureCellInView(this.activeCell);
}
this.setCellClicked(false);
if(this.isSingleClickEditEnabled()){
this.edit(evt);
}else{
if(!nitobi.browser.IE){
this.focus();
}
}
};
ntb_gridp.loadNextDataPage=function(){
this.loadDataPage(this.getCurrentPageIndex()+1);
};
ntb_gridp.loadPreviousDataPage=function(){
this.loadDataPage(this.getCurrentPageIndex()-1);
};
ntb_gridp.GetPage=function(_26b){
ebaErrorReport("GetPage is deprecated please use loadDataPage instead","",EBA_DEBUG);
this.loadDataPage(_26b);
};
ntb_gridp.loadDataPage=function(_26c){
};
ntb_gridp.getSelectedRow=function(rel){
try{
var nRow=-1;
var AC=this.activeCell;
if(AC!=null){
nRow=nitobi.grid.Cell.getRowNumber(AC);
if(rel){
nRow-=this.getfreezetop();
}
}
return nRow;
}
catch(err){
_ntbAssert(false,err.message);
}
};
ntb_gridp.handleHandlerError=function(){
var _270=this.getDataSource().getHandlerError();
if(_270){
this.fire("HandlerError");
}
};
ntb_gridp.getRowObject=function(_271,_272){
var _273=_272;
if(_272==null&&_271!=null){
_273=_271;
}
return new nitobi.grid.Row(this,_273);
};
ntb_gridp.getSelectedColumn=function(rel){
try{
var nCol=-1;
var AC=this.activeCell;
if(AC!=null){
nCol=parseInt(AC.getAttribute("col"));
if(rel){
nCol-=this.getFrozenLeftColumnCount();
}
}
return nCol;
}
catch(err){
_ntbAssert(false,err.message);
}
};
ntb_gridp.getSelectedColumnName=function(){
var _277=this.getSelectedColumnObject();
return _277.getColumnName();
};
ntb_gridp.getSelectedColumnObject=function(){
return this.getColumnObject(this.getSelectedColumn());
};
ntb_gridp.columnCount=function(){
try{
var _278=this.getColumnDefinitions();
return _278.length;
}
catch(err){
_ntbAssert(false,err.message);
}
};
ntb_gridp.getCellObject=function(row,col){
var _27b=col;
var cell=this.cachedCells[row+"_"+col];
if(cell==null){
if(typeof (col)=="string"){
var node=this.model.selectSingleNode("state/nitobi.grid.Columns/nitobi.grid.Column[@xdatafld_orig='"+col+"']");
if(node!=null){
col=parseInt(node.getAttribute("xi"));
}
}
if(typeof (col)=="number"){
cell=new nitobi.grid.Cell(this,row,col);
}else{
cell=null;
}
this.cachedCells[row+"_"+col]=this.cachedCells[row+"_"+_27b]=cell||"";
}else{
if(cell==""){
cell=null;
}
}
return cell;
};
ntb_gridp.getCellText=function(row,col){
return this.getCellObject(row,col).getHtml();
};
ntb_gridp.getCellValue=function(row,col){
return this.getCellObject(row,col).getValue();
};
ntb_gridp.getCellElement=function(row,_283){
return document.getElementById("cell_"+row+"_"+_283+"_"+this.uid);
};
ntb_gridp.getSelectedRowObject=function(xi){
var obj=null;
var r=nitobi.grid.Cell.getRowNumber(this.activeCell);
obj=new nitobi.grid.Row(this,r);
return obj;
};
ntb_gridp.getColumnObject=function(_287){
var _288=null;
if(_287>=0&&_287<this.getColumnDefinitions().length){
_288=this.columns[_287];
if(_288==null){
var _289=this.getColumnDefinitions()[_287].getAttribute("DataType");
switch(_289){
case "number":
_288=new nitobi.grid.NumberColumn(this,_287);
break;
case "date":
_288=new nitobi.grid.DateColumn(this,_287);
break;
default:
_288=new nitobi.grid.TextColumn(this,_287);
break;
}
this.columns[_287]=_288;
}
}
if(_288==null||_288.getModel()==null){
return null;
}else{
return _288;
}
};
ntb_gridp.getSelectedCellObject=function(){
var obj=this.activeCellObject;
if(obj==null){
obj=this.activeCell;
if(obj!=null){
var Cell=nitobi.grid.Cell;
var r=Cell.getRowNumber(obj);
var c=Cell.getColumnNumber(obj);
obj=this.getCellObject(r,c);
}
}
return obj;
};
ntb_gridp.autoAddRow=function(){
if(this.activeCell.innerText.replace(/\s/g,"")!=""&&this.autoAdd){
this.deactivateCell();
if(this.active=="Y"){
this.freezeCell();
}
eval(this.getOnRowBlurEvent());
this.insertRow();
this.go("HOME");
this.editCell();
}
};
ntb_gridp.setDisplayedRowCount=function(_28e){
if(this.Scroller){
this.Scroller.view.midcenter.rows=_28e;
this.Scroller.view.midleft.rows=_28e;
}
this.displayedRowCount=_28e;
};
ntb_gridp.getDisplayedRowCount=function(){
return this.displayedRowCount;
};
ntb_gridp.getToolsContainer=function(){
this.toolsContainer=this.toolsContainer||document.getElementById("ntb-grid-toolscontainer"+this.uid);
return this.toolsContainer;
};
ntb_gridp.getHeaderContainer=function(){
return document.getElementById("ntb-grid-header"+this.uid);
};
ntb_gridp.getDataContainer=function(){
return document.getElementById("ntb-grid-data"+this.uid);
};
ntb_gridp.getScrollerContainer=function(){
return document.getElementById("ntb-grid-scroller"+this.uid);
};
ntb_gridp.getGridContainer=function(){
return nitobi.html.getFirstChild(this.UiContainer);
};
ntb_gridp.copy=function(){
var _28f=this.selection.getCoords();
var data=this.getTableForSelection(_28f);
var _291=new nitobi.grid.OnCopyEventArgs(this,data,_28f);
if(!this.isCopyEnabled()||!this.fire("BeforeCopy",_291)){
return;
}
if(!nitobi.browser.IE){
var _292=this.getClipboard();
_292.onkeyup=nitobi.lang.close(this,this.focus);
_292.value=data;
_292.focus();
_292.setSelectionRange(0,_292.value.length);
}else{
window.clipboardData.setData("Text",data);
}
this.fire("AfterCopy",_291);
};
ntb_gridp.getTableForSelection=function(_293){
var _294=this.getColumnMap(_293.top.x,_293.bottom.x);
var _295=nitobi.data.FormatConverter.convertEbaXmlToTsv(this.getDataSource().getDataXmlDoc(),_294,_293.top.y,_293.bottom.y);
return _295;
};
ntb_gridp.getColumnMap=function(_296,_297){
var _298=this.getColumnDefinitions();
_296=(_296==null)?0:_296;
_297=(_297==null)?_298.length-1:_297;
var map=new Array();
for(var i=_296;i<=_297&&(null!=_298[i]);i++){
map.push(_298[i].getAttribute("xdatafld").substr(1));
}
return map;
};
ntb_gridp.paste=function(){
if(!this.isPasteEnabled()){
return;
}
var _29b=this.getClipboard();
_29b.onkeyup=nitobi.lang.close(this,this.pasteDataReady,[_29b]);
_29b.focus();
return _29b;
};
ntb_gridp.pasteDataReady=function(_29c){
_29c.onkeyup=null;
var _29d=this.selection;
var _29e=_29d.getCoords();
var _29f=_29e.top.x;
var _2a0=_29f+nitobi.data.FormatConverter.getDataColumns(_29c.value)-1;
var _2a1=true;
for(var i=_29f;i<=_2a0;i++){
var _2a3=this.getColumnObject(i);
if(_2a3){
if(!_2a3.isEditable()){
_2a1=false;
break;
}
}
}
if(!_2a1){
this.fire("PasteFailed",new nitobi.base.EventArgs(this));
this.handleAfterPaste();
return;
}else{
var _2a4=this.getColumnMap(_29f,_2a0);
var _2a5=_29e.top.y;
var _2a6=Math.max(_2a5+nitobi.data.FormatConverter.getDataRows(_29c.value)-1,0);
this.getSelection().selectWithCoords(_2a5,_29f,_2a6,_29f+_2a4.length-1);
var _2a7=new nitobi.grid.OnPasteEventArgs(this,_29c.value,_29e);
if(!this.fire("BeforePaste",_2a7)){
return;
}
var _2a8=_29c.value;
var _2a9=null;
if(_2a8.substr(0,1)=="<"){
_2a9=nitobi.data.FormatConverter.convertHtmlTableToEbaXml(_2a8,_2a4,_2a5);
}else{
_2a9=nitobi.data.FormatConverter.convertTsvToEbaXml(_2a8,_2a4,_2a5);
}
if(_2a9.documentElement!=null){
this.datatable.mergeFromXml(_2a9,nitobi.lang.close(this,this.pasteComplete,[_2a9,_2a5,_2a6,_2a7]));
}
}
};
ntb_gridp.pasteComplete=function(_2aa,_2ab,_2ac,_2ad){
this.Scroller.reRender(_2ab,_2ac);
this.subscribeOnce("HtmlReady",this.handleAfterPaste,this,[_2ad]);
};
ntb_gridp.handleAfterPaste=function(_2ae){
this.fire("AfterPaste",_2ae);
};
ntb_gridp.getClipboard=function(){
var _2af=document.getElementById("ntb-clipboard"+this.uid);
_2af.onkeyup=null;
_2af.value="";
return _2af;
};
ntb_gridp.handleHtmlReady=function(_2b0){
this.fire("HtmlReady",new nitobi.base.EventArgs(this));
};
ntb_gridp.fire=function(evt,args){
return nitobi.event.notify(evt+this.uid,args);
};
ntb_gridp.subscribe=function(evt,func,_2b5){
if(this.subscribedEvents==null){
this.subscribedEvents={};
}
if(typeof (_2b5)=="undefined"){
_2b5=this;
}
var guid=nitobi.event.subscribe(evt+this.uid,nitobi.lang.close(_2b5,func));
this.subscribedEvents[guid]=evt+this.uid;
return guid;
};
ntb_gridp.subscribeOnce=function(evt,func,_2b9,_2ba){
var guid=null;
var _2bc=this;
var _2bd=function(){
func.apply(_2b9||this,_2ba||arguments);
_2bc.unsubscribe(evt,guid);
};
guid=this.subscribe(evt,_2bd);
};
ntb_gridp.unsubscribe=function(evt,guid){
return nitobi.event.unsubscribe(evt+this.uid,guid);
};
ntb_gridp.dispose=function(){
try{
this.element.jsObject=null;
editorXslProc=null;
var H=nitobi.html;
H.detachEvents(this.getGridContainer(),this.events);
H.detachEvents(this.getHeaderContainer(),this.headerEvents);
H.detachEvents(this.getDataContainer(),this.cellEvents);
H.detachEvents(this.getScrollerContainer(),this.scrollerEvents);
H.detachEvents(this.keyNav,this.keyEvents);
for(var item in this.subscribedEvents){
var _2c2=this.subscribedEvents[item];
this.unsubscribe(_2c2.substring(0,_2c2.length-this.uid.length),item);
}
this.UiContainer.parentNode.removeChild(this.UiContainer);
for(var item in this){
if(this[item]!=null){
if(this[item].dispose instanceof Function){
this[item].dispose();
}
this[item]=null;
}
}
nitobi.form.ControlFactory.instance.dispose();
}
catch(e){
}
};
nitobi.Grid=nitobi.grid.Grid;
nitobi.grid.Cell=function(grid,row,_2c5){
if(row==null||grid==null){
return null;
}
this.grid=grid;
var _2c6=null;
if(typeof (row)=="object"){
var cell=row;
row=Number(cell.getAttribute("xi"));
_2c5=cell.getAttribute("col");
_2c6=cell;
}else{
_2c6=this.grid.getCellElement(row,_2c5);
}
this.DomNode=_2c6;
this.row=Number(row);
this.Row=this.row;
this.column=Number(_2c5);
this.Column=this.column;
this.dataIndex=this.Row;
};
nitobi.grid.Cell.prototype.getData=function(){
if(this.DataNode==null){
this.DataNode=this.grid.datatable.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"e[@xi="+this.dataIndex+"]/"+this.grid.datatable.fieldMap[this.getColumnObject().getColumnName()]);
}
return this.DataNode;
};
nitobi.grid.Cell.prototype.getModel=function(){
if(this.ModelNode==null){
this.ModelNode=this.grid.model.selectSingleNode("//nitobi.grid.Columns/nitobi.grid.Column[@xi='"+this.column+"']");
}
return this.ModelNode;
};
nitobi.grid.Cell.prototype.setRow=function(){
this.jSET("Row",arguments);
};
nitobi.grid.Cell.prototype.getRow=function(){
return this.Row;
};
nitobi.grid.Cell.prototype.setColumn=function(){
this.jSET("Column",arguments);
};
nitobi.grid.Cell.prototype.getColumn=function(){
return this.Column;
};
nitobi.grid.Cell.prototype.setDomNode=function(){
this.jSET("DomNode",arguments);
};
nitobi.grid.Cell.prototype.getDomNode=function(){
return this.DomNode;
};
nitobi.grid.Cell.prototype.setDataNode=function(){
this.jSET("DataNode",arguments);
};
nitobi.grid.Cell.prototype.setValue=function(_2c8,_2c9){
if(_2c8==this.getValue()){
return;
}
var _2ca=this.getColumnObject();
var _2cb="";
switch(_2ca.getType()){
case "PASSWORD":
for(var i=0;i<_2c8.length;i++){
_2cb+="*";
}
break;
case "NUMBER":
if(this.numberXsl==null){
this.numberXsl=nitobi.form.numberXslProc;
}
if(_2c8==""){
_2c8=_2ca.getEditor().defaultValue||0;
}
if(this.DomNode!=null){
if(_2c8<0){
nitobi.html.Css.addClass(this.DomNode,"ntb-cell-negativenumber");
}else{
nitobi.html.Css.removeClass(this.DomNode,"ntb-cell-negativenumber");
}
}
var mask=_2ca.getMask();
var _2ce=_2ca.getNegativeMask();
var _2cf=_2c8;
if(_2c8<0&&_2ce!=""){
mask=_2ce;
_2cf=(_2c8+"").replace("-","");
}
this.numberXsl.addParameter("number",_2cf,"");
this.numberXsl.addParameter("mask",mask,"");
this.numberXsl.addParameter("group",_2ca.getGroupingSeparator(),"");
this.numberXsl.addParameter("decimal",_2ca.getDecimalSeparator(),"");
_2cb=nitobi.xml.transformToString(nitobi.xml.Empty,this.numberXsl);
if(""==_2cb&&_2c8!=""){
_2cb=nitobi.html.getFirstChild(this.DomNode).innerHTML;
_2c8=this.getValue();
}
break;
case "DATE":
if(this.dateXsl==null){
this.dateXsl=nitobi.form.dateXslProc.stylesheet;
}
var d=new Date();
var _2d1=nitobi.xml.createXmlDoc("<root><date>"+_2c8+"</date><year>"+(d.getFullYear())+"</year><mask>"+this.columnObject.getMask()+"</mask></root>");
_2cb=nitobi.xml.transformToString(_2d1,this.dateXsl);
if(""==_2cb){
_2cb=nitobi.html.getFirstChild(this.DomNode).innerHTML;
_2c8=this.getValue();
}
break;
case "TEXTAREA":
_2cb=nitobi.html.encode(_2c8);
break;
case "LOOKUP":
var _2d2=_2ca.getModel();
var _2d3=_2d2.getAttribute("DatasourceId");
var _2d4=this.grid.data.getTable(_2d3);
var _2d5=_2d2.getAttribute("DisplayFields");
var _2d6=_2d2.getAttribute("ValueField");
var _2d7=_2d4.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"e[@"+_2d6+"='"+_2c8+"']/@"+_2d5);
if(_2d7!=null){
_2cb=_2d7.nodeValue;
}else{
_2cb=_2c8;
}
break;
case "CHECKBOX":
var _2d2=_2ca.getModel();
var _2d3=_2d2.getAttribute("DatasourceId");
var _2d4=this.grid.data.getTable(_2d3);
var _2d5=_2d2.getAttribute("DisplayFields");
var _2d6=_2d2.getAttribute("ValueField");
var _2d8=_2d2.getAttribute("CheckedValue");
if(_2d8==""||_2d8==null){
_2d8=0;
}
var _2d9=_2d4.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"e[@"+_2d6+"='"+_2c8+"']/@"+_2d5).nodeValue;
var _2da=(_2c8==_2d8)?"checked":"unchecked";
_2cb="<div style=\"overflow:hidden;\"><div class=\"ntb-checkbox ntb-checkbox-"+_2da+"\" checked=\""+_2c8+"\">&nbsp;</div><div class=\"ntb-checkbox-text\">"+nitobi.html.encode(_2d9)+"</div></div>";
break;
case "LISTBOX":
var _2d2=_2ca.getModel();
var _2d3=_2d2.getAttribute("DatasourceId");
var _2d4=this.grid.data.getTable(_2d3);
var _2d5=_2d2.getAttribute("DisplayFields");
var _2d6=_2d2.getAttribute("ValueField");
_2cb=_2d4.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"e[@"+_2d6+"='"+_2c8+"']/@"+_2d5).nodeValue;
break;
case "IMAGE":
_2cb=nitobi.html.getFirstChild(this.DomNode).innerHTML;
if(nitobi.lang.typeOf(_2c8)==nitobi.lang.type.HTMLNODE){
_2cb="<img border=\"0\" src=\""+_2c8.getAttribute("src")+"\" />";
}else{
if(typeof (_2c8)=="string"){
_2cb="<img border=\"0\" src=\""+_2c8+"\" />";
}
}
break;
default:
_2cb=_2c8;
}
_2cb=_2cb||"&nbsp;";
if(this.DomNode!=null){
var elem=nitobi.html.getFirstChild(this.DomNode);
elem.innerHTML=_2cb||"&nbsp;";
elem.setAttribute("title",_2c8);
this.DomNode.setAttribute("value",_2c8);
}
this.grid.datatable.updateRecord(this.dataIndex,_2ca.getColumnName(),_2c8);
};
nitobi.grid.Cell.prototype.getValue=function(){
var _2dc=this.getColumnObject();
var val=this.GETDATA();
switch(_2dc.getType()){
case "NUMBER":
val=parseFloat(val);
break;
default:
}
return val;
};
nitobi.grid.Cell.prototype.getHtml=function(){
return nitobi.html.getFirstChild(this.DomNode).innerHTML;
};
nitobi.grid.Cell.prototype.edit=function(){
this.grid.setActiveCell(this.DomNode);
this.grid.edit();
};
nitobi.grid.Cell.prototype.GETDATA=function(){
var node=this.getData();
if(node!=null){
return node.value;
}
};
nitobi.grid.Cell.prototype.xGETMETA=function(){
if(this.MetaNode==null){
return null;
}
var node=this.MetaNode;
node=node.selectSingleNode("@"+arguments[0]);
if(node!=null){
return node.value;
}
};
nitobi.grid.Cell.prototype.xSETMETA=function(){
var node=this.MetaNode;
if(node!=null){
node.setAttribute(arguments[0],arguments[1][0]);
}else{
alert("Cannot set property: "+arguments[0]);
}
};
nitobi.grid.Cell.prototype.xSETCSS=function(){
var node=this.DomNode;
if(node!=null){
node.style.setAttribute(arguments[0],arguments[1][0]);
}else{
alert("Cannot set property: "+arguments[0]);
}
};
nitobi.grid.Cell.prototype.xGET=function(){
var node=this.getModel();
node=node.selectSingleNode(arguments[0]);
if(node!=null){
return node.value;
}
};
nitobi.grid.Cell.prototype.xSET=function(){
var node=this.getModel();
node=node.selectSingleNode(arguments[0]);
if(node!=null){
node.nodeValue=arguments[1][0];
}
};
nitobi.grid.Cell.prototype.getStyle=function(){
return this.DomNode.style;
};
nitobi.grid.Cell.prototype.getColumnObject=function(){
if(typeof (this.columnObject)=="undefined"){
this.columnObject=this.grid.getColumnObject(this.getColumn());
}
return this.columnObject;
};
nitobi.grid.Cell.getCellElement=function(grid,row,_2e6){
return $("cell_"+row+"_"+_2e6+"_"+grid.uid);
};
nitobi.grid.Cell.getRowNumber=function(_2e7){
return parseInt(_2e7.getAttribute("xi"));
};
nitobi.grid.Cell.getColumnNumber=function(_2e8){
return parseInt(_2e8.getAttribute("col"));
};
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.CellEventArgs=function(_2e9,cell){
nitobi.grid.CellEventArgs.baseConstructor.call(this,_2e9);
this.cell=cell;
};
nitobi.lang.extend(nitobi.grid.CellEventArgs,nitobi.base.EventArgs);
nitobi.grid.CellEventArgs.prototype.getCell=function(){
return this.cell;
};
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.RowEventArgs=function(_2eb,row){
this.grid=_2eb;
this.row=row;
this.event=nitobi.html.Event;
};
nitobi.grid.RowEventArgs.prototype.getSource=function(){
return this.grid;
};
nitobi.grid.RowEventArgs.prototype.getRow=function(){
return this.row;
};
nitobi.grid.RowEventArgs.prototype.getEvent=function(){
return this.event;
};
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.SelectionEventArgs=function(_2ed,data,_2ef){
this.source=_2ed;
this.coords=_2ef;
this.data=data;
};
nitobi.grid.SelectionEventArgs.prototype.getSource=function(){
return this.source;
};
nitobi.grid.SelectionEventArgs.prototype.getCoords=function(){
return this.coords;
};
nitobi.grid.SelectionEventArgs.prototype.getData=function(){
return this.data;
};
nitobi.grid.Column=function(grid,_2f1){
this.grid=grid;
this.column=_2f1;
this.uid=nitobi.base.getUid();
this.modelNodes={};
};
nitobi.grid.Column.prototype={setAlign:function(){
this.xSET("Align",arguments);
},getAlign:function(){
return this.xGET("Align",arguments);
},setClassName:function(){
this.xSET("ClassName",arguments);
},getClassName:function(){
return this.xGET("ClassName",arguments);
},setCssStyle:function(){
this.xSET("CssStyle",arguments);
},getCssStyle:function(){
return this.xGET("CssStyle",arguments);
},setColumnName:function(){
this.xSET("ColumnName",arguments);
},getColumnName:function(){
return this.xGET("ColumnName",arguments);
},setType:function(){
this.xSET("type",arguments);
},getType:function(){
return this.xGET("type",arguments);
},setDataType:function(){
this.xSET("DataType",arguments);
},getDataType:function(){
return this.xGET("DataType",arguments);
},setEditable:function(){
this.xSET("Editable",arguments);
},isEditable:function(){
return nitobi.lang.toBool(this.xGET("Editable",arguments),true);
},setInitial:function(){
this.xSET("Initial",arguments);
},getInitial:function(){
return this.xGET("Initial",arguments);
},setLabel:function(){
this.xSET("Label",arguments);
},getLabel:function(){
return this.xGET("Label",arguments);
},setGetHandler:function(){
this.xSET("GetHandler",arguments);
},getGetHandler:function(){
return this.xGET("GetHandler",arguments);
},setDatasourceId:function(){
this.xSET("DatasourceId",arguments);
},getDatasourceId:function(){
return this.xGET("DatasourceId",arguments);
},setTemplate:function(){
this.xSET("Template",arguments);
},getTemplate:function(){
return this.xGET("Template",arguments);
},setTemplateUrl:function(){
this.xSET("TemplateUrl",arguments);
},getTemplateUrl:function(){
return this.xGET("TemplateUrl",arguments);
},setMaxLength:function(){
this.xSET("maxlength",arguments);
},getMaxLength:function(){
return Number(this.xGET("maxlength",arguments));
},setSortDirection:function(){
this.xSET("SortDirection",arguments);
},getSortDirection:function(){
return this.xGET("SortDirection",arguments);
},setSortEnabled:function(){
this.xSET("SortEnabled",arguments);
},isSortEnabled:function(){
return nitobi.lang.toBool(this.xGET("SortEnabled",arguments),true);
},setWidth:function(){
this.xSET("Width",arguments);
},getWidth:function(){
return Number(this.xGET("Width",arguments));
},setSize:function(){
this.xSET("Size",arguments);
},getSize:function(){
return Number(this.xGET("Size",arguments));
},setVisible:function(){
this.xSET("Visible",arguments);
},isVisible:function(){
return nitobi.lang.toBool(this.xGET("Visible",arguments),true);
},setxdatafld:function(){
this.xSET("xdatafld",arguments);
},getxdatafld:function(){
return this.xGET("xdatafld",arguments);
},setValue:function(){
this.xSET("Value",arguments);
},getValue:function(){
return this.xGET("Value",arguments);
},setxi:function(){
this.xSET("xi",arguments);
},getxi:function(){
return Number(this.xGET("xi",arguments));
},setEditor:function(){
this.xSET("Editor",arguments);
},getEditor:function(){
return this.xGET("Editor",arguments);
},setDisplayFields:function(){
this.xSET("DisplayFields",arguments);
},getDisplayFields:function(){
return this.xGET("DisplayFields",arguments);
},setValueField:function(){
this.xSET("ValueField",arguments);
},getValueField:function(){
return this.xGET("ValueField",arguments);
},setDelay:function(){
this.xSET("Delay",arguments);
},getDelay:function(){
return Number(this.xGET("Delay",arguments));
},setReferenceColumn:function(){
this.xSET("ReferenceColumn",arguments);
},getReferenceColumn:function(){
return this.xGET("ReferenceColumn",arguments);
},setOnCellClickEvent:function(){
this.xSET("OnCellClickEvent",arguments);
},getOnCellClickEvent:function(){
return this.xGET("OnCellClickEvent",arguments);
},setOnBeforeCellClickEvent:function(){
this.xSET("OnBeforeCellClickEvent",arguments);
},getOnBeforeCellClickEvent:function(){
return this.xGET("OnBeforeCellClickEvent",arguments);
},setOnCellDblClickEvent:function(){
this.xSET("OnCellDblClickEvent",arguments);
},getOnCellDblClickEvent:function(){
return this.xGET("OnCellDblClickEvent",arguments);
},setOnHeaderDoubleClickEvent:function(){
this.xSET("OnHeaderDoubleClickEvent",arguments);
},getOnHeaderDoubleClickEvent:function(){
return this.xGET("OnHeaderDoubleClickEvent",arguments);
},setOnHeaderClickEvent:function(){
this.xSET("OnHeaderClickEvent",arguments);
},getOnHeaderClickEvent:function(){
return this.xGET("OnHeaderClickEvent",arguments);
},setOnBeforeResizeEvent:function(){
this.xSET("OnBeforeResizeEvent",arguments);
},getOnBeforeResizeEvent:function(){
return this.xGET("OnBeforeResizeEvent",arguments);
},setOnAfterResizeEvent:function(){
this.xSET("OnAfterResizeEvent",arguments);
},getOnAfterResizeEvent:function(){
return this.xGET("OnAfterResizeEvent",arguments);
},setOnCellValidateEvent:function(){
this.xSET("OnCellValidateEvent",arguments);
},getOnCellValidateEvent:function(){
return this.xGET("OnCellValidateEvent",arguments);
},setOnBeforeCellEditEvent:function(){
this.xSET("OnBeforeCellEditEvent",arguments);
},getOnBeforeCellEditEvent:function(){
return this.xGET("OnBeforeCellEditEvent",arguments);
},setOnAfterCellEditEvent:function(){
this.xSET("OnAfterCellEditEvent",arguments);
},getOnAfterCellEditEvent:function(){
return this.xGET("OnAfterCellEditEvent",arguments);
},setOnCellBlurEvent:function(){
this.xSET("OnCellBlurEvent",arguments);
},getOnCellBlurEvent:function(){
return this.xGET("OnCellBlurEvent",arguments);
},setOnCellFocusEvent:function(){
this.xSET("OnCellFocusEvent",arguments);
},getOnCellFocusEvent:function(){
return this.xGET("OnCellFocusEvent",arguments);
},setOnBeforeSortEvent:function(){
this.xSET("OnBeforeSortEvent",arguments);
},getOnBeforeSortEvent:function(){
return this.xGET("OnBeforeSortEvent",arguments);
},setOnAfterSortEvent:function(){
this.xSET("OnAfterSortEvent",arguments);
},getOnAfterSortEvent:function(){
return this.xGET("OnAfterSortEvent",arguments);
},setOnCellUpdateEvent:function(){
this.xSET("OnCellUpdateEvent",arguments);
},getOnCellUpdateEvent:function(){
return this.xGET("OnCellUpdateEvent",arguments);
},setOnKeyDownEvent:function(){
this.xSET("OnKeyDownEvent",arguments);
},getOnKeyDownEvent:function(){
return this.xGET("OnKeyDownEvent",arguments);
},setOnKeyUpEvent:function(){
this.xSET("OnKeyUpEvent",arguments);
},getOnKeyUpEvent:function(){
return this.xGET("OnKeyUpEvent",arguments);
},setOnKeyPressEvent:function(){
this.xSET("OnKeyPressEvent",arguments);
},getOnKeyPressEvent:function(){
return this.xGET("OnKeyPressEvent",arguments);
},setOnChangeEvent:function(){
this.xSET("OnChangeEvent",arguments);
},getOnChangeEvent:function(){
return this.xGET("OnChangeEvent",arguments);
},setGetOnEnter:function(){
this.xbSET("GetOnEnter",arguments);
},isGetOnEnter:function(){
return nitobi.lang.toBool(this.xGET("GetOnEnter",arguments),true);
},setAutoComplete:function(){
this.xbSET("AutoComplete",arguments);
},isAutoComplete:function(){
return nitobi.lang.toBool(this.xGET("AutoComplete",arguments),true);
},setAutoClear:function(){
this.xbSET("AutoClear",arguments);
},isAutoClear:function(){
return nitobi.lang.toBool(this.xGET("AutoClear",arguments),true);
}};
nitobi.grid.Column.prototype.getModel=function(){
if(this.ModelNode==null){
var _2f2=this.column;
this.ModelNode=this.grid.model.selectNodes("//state/nitobi.grid.Columns/nitobi.grid.Column")[_2f2];
}
return this.ModelNode;
};
nitobi.grid.Column.prototype.getHeaderElement=function(){
return nitobi.grid.Column.getColumnHeaderElement(this.grid,this.column);
};
nitobi.grid.Column.prototype.getEditor=function(){
};
nitobi.grid.Column.prototype.getStyle=function(){
var _2f3=this.getClassName();
return nitobi.html.getClass(_2f3);
};
nitobi.grid.Column.prototype.getHeaderStyle=function(){
var _2f4="acolumnheader"+this.grid.uid+"_"+this.column;
return nitobi.html.getClass(_2f4);
};
nitobi.grid.Column.prototype.getDataStyle=function(){
var _2f5="ntb-column-data"+this.grid.uid+"_"+this.column;
return nitobi.html.getClass(_2f5);
};
nitobi.grid.Column.prototype.getEditor=function(){
return nitobi.form.ControlFactory.instance.getEditor(this.grid,this);
};
nitobi.grid.Column.prototype.xGET=function(){
var node=null,_2f7="@"+arguments[0],val="";
var _2f9=this.modelNodes[_2f7];
if(_2f9!=null){
node=_2f9;
}else{
node=this.modelNodes[_2f7]=this.getModel().selectSingleNode(_2f7);
}
if(node!=null){
val=node.nodeValue;
}
return val;
};
nitobi.grid.Column.prototype.xSET=function(){
var node=this.getModel();
if(node!=null){
node.setAttribute(arguments[0],arguments[1][0]);
}
};
nitobi.grid.Column.prototype.xbSETMODEL=function(){
var node=this.getModel();
if(node!=null){
node.setAttribute(arguments[0],nitobi.lang.boolToStr(arguments[1][0]));
}
};
nitobi.grid.Column.prototype.eSET=function(name,_2fd){
var _2fe=_2fd[0];
var _2ff=_2fe;
var _300=name.substr(2);
_300=_300.substr(0,_300.length-5);
if(typeof (_2fe)=="string"){
_2ff=function(_301){
return eval(_2fe);
};
}
if(typeof (this[name])!="undefined"){
alert("unsubscribe");
this.unsubscribe(_300,this[name]);
}
var guid=this.subscribe(_300,_2ff);
this.jSET(name,[guid]);
};
nitobi.grid.Column.prototype.jSET=function(name,val){
this[name]=val[0];
};
nitobi.grid.Column.prototype.fire=function(evt,args){
return nitobi.event.notify(evt+this.uid,args);
};
nitobi.grid.Column.prototype.subscribe=function(evt,func,_309){
if(typeof (_309)=="undefined"){
_309=this;
}
return nitobi.event.subscribe(evt+this.uid,nitobi.lang.close(_309,func));
};
nitobi.grid.Column.prototype.unsubscribe=function(evt,func){
return nitobi.event.unsubscribe(evt+this.uid,func);
};
nitobi.grid.Column.getColumnHeaderElement=function(grid,_30d){
return $("columnheader_"+_30d+"_"+grid.uid);
};
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.ColumnEventArgs=function(_30e,_30f){
this.grid=_30e;
this.column=_30f;
this.event=nitobi.html.Event;
};
nitobi.grid.ColumnEventArgs.prototype.getSource=function(){
return this.grid;
};
nitobi.grid.ColumnEventArgs.prototype.getColumn=function(){
return this.column;
};
nitobi.grid.ColumnEventArgs.prototype.getEvent=function(){
return this.event;
};
nitobi.grid.ColumnEventArgs.prototype.getDirection=function(){
};
nitobi.grid.ColumnResizer=function(grid){
this.grid=grid;
this.hScrollClass=null;
this.grid_id=this.grid.UiContainer.parentid;
this.line=document.getElementById("ntb-column-resizeline"+this.grid.uid);
this.lineStyle=this.line.style;
if(nitobi.browser.IE){
this.surface=document.getElementById("ebagridresizesurface_");
if(this.surface==null){
this.surface=document.createElement("div");
this.surface.id="ebagridresizesurface_";
this.surface.className="ntb-column-resize-surface";
this.grid.UiContainer.appendChild(this.surface);
}
}
this.column;
this.onAfterResize=new nitobi.base.Event();
};
nitobi.grid.ColumnResizer.prototype.startResize=function(grid,_312,_313,evt){
this.grid=grid;
this.column=_312;
var x=nitobi.html.getEventCoords(evt).x;
if(nitobi.browser.IE){
this.surface.style.display="block";
nitobi.drawing.align(this.surface,this.grid.element,nitobi.drawing.align.SAMEHEIGHT|nitobi.drawing.align.SAMEWIDTH|nitobi.drawing.align.ALIGNTOP|nitobi.drawing.align.ALIGNLEFT);
}
this.x=x;
this.lineStyle.display="block";
var _316=this.grid.UiContainer.getBoundingClientRect().left;
this.lineStyle.left=x-_316+"px";
this.lineStyle.height=this.grid.Scroller.scrollSurface.offsetHeight+"px";
nitobi.drawing.align(this.line,_313,nitobi.drawing.align.ALIGNTOP,0,0,nitobi.html.getHeight(_313)+1);
nitobi.ui.startDragOperation(this.line,evt,false,true,this,this.endResize);
};
nitobi.grid.ColumnResizer.prototype.endResize=function(_317){
var x=_317.x;
var Y=_317.y;
if(nitobi.browser.IE){
this.surface.style.display="none";
}
var ls=this.lineStyle;
ls.display="none";
ls.top="-3000px";
ls.left="-3000px";
this.dx=x-this.x;
this.onAfterResize.notify(this);
};
nitobi.grid.ColumnResizer.prototype.dispose=function(){
this.grid=null;
this.line=null;
this.lineStyle=null;
this.surface=null;
};
nitobi.grid.GridResizer=function(grid){
this.grid=grid;
this.widthFixed=false;
this.heightFixed=false;
this.minHeight=0;
this.minWidth=0;
this.box=document.getElementById("ntb-grid-resizebox"+grid.uid);
this.onAfterResize=new nitobi.base.Event();
};
nitobi.grid.GridResizer.prototype.startResize=function(grid,_31d){
this.grid=grid;
var _31e=null;
var x,y;
var _321=nitobi.html.getEventCoords(_31d);
x=_321.x;
y=_321.y;
this.x=x;
this.y=y;
var w=grid.getWidth();
var h=grid.getHeight();
var L=grid.element.offsetLeft;
var T=grid.element.offsetTop;
this.resizeW=!this.widthFixed;
this.resizeH=!this.heightFixed;
if(this.resizeW||this.resizeH){
this.box.style.cursor=(this.resizeW&&this.resizeH)?"nw-resize":(this.resizeW)?"w-resize":"n-resize";
this.box.style.display="block";
var _326=nitobi.drawing.align.SAMEWIDTH|nitobi.drawing.align.SAMEHEIGHT|nitobi.drawing.align.ALIGNTOP|nitobi.drawing.align.ALIGNLEFT;
nitobi.drawing.align(this.box,this.grid.element,_326,0,0,0,0,false);
this.dd=new nitobi.ui.DragDrop(this.box,false,false);
this.dd.onDragStop.subscribe(this.endResize,this);
this.dd.onMouseMove.subscribe(this.resize,this);
this.dd.startDrag(_31d);
}
};
nitobi.grid.GridResizer.prototype.resize=function(){
var x=this.dd.x;
var y=this.dd.y;
var rect=this.grid.UiContainer.getBoundingClientRect();
var L=rect.left;
var T=rect.top;
this.box.style.display="block";
if((x-L)>this.minWidth){
this.box.style.width=(x-L)+"px";
}
if((y-T)>this.minHeight){
this.box.style.height=(y-T)+"px";
}
};
nitobi.grid.GridResizer.prototype.endResize=function(){
var x=this.dd.x;
var y=this.dd.y;
this.box.style.display="none";
var _32e=this.grid.getWidth();
var _32f=this.grid.getHeight();
this.newWidth=Math.max(parseInt(_32e)+(x-this.x),this.minWidth);
this.newHeight=Math.max(parseInt(_32f)+(y-this.y),this.minHeight);
if(isNaN(this.newWidth)||isNaN(this.newHeight)){
return;
}
this.onAfterResize.notify(this);
};
nitobi.grid.GridResizer.prototype.dispose=function(){
this.grid=null;
};
nitobi.data.FormatConverter={};
nitobi.data.FormatConverter.convertHtmlTableToEbaXml=function(_330,_331,_332){
var s="<xsl:stylesheet version=\"1.0\" xmlns:ntb=\"http://www.nitobi.com\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"><xsl:output encoding=\"UTF-8\" method=\"xml\" omit-xml-declaration=\"no\" />";
s+="<xsl:template match=\"//TABLE\"><ntb:data id=\"_default\">";
s+="<xsl:apply-templates /></ntb:data> </xsl:template>";
s+="<xsl:template match = \"//TR\">  <xsl:element name=\"ntb:e\"> <xsl:attribute name=\"xi\"><xsl:value-of select=\"position()-1+"+parseInt(_332)+"\"/></xsl:attribute>";
for(var _334=0;_334<_331.length;_334++){
s+="<xsl:attribute name=\""+_331[_334]+"\" ><xsl:value-of select=\"TD["+parseInt(_334+1)+"]\"/></xsl:attribute>";
}
s+="</xsl:element></xsl:template>";
s+="</xsl:stylesheet>";
var _335=nitobi.xml.createXmlDoc(_330);
var _336=nitobi.xml.createXslProcessor(s);
var _337=nitobi.xml.transformToXml(_335,_336);
return _337;
};
nitobi.data.FormatConverter.convertTsvToEbaXml=function(tsv,_339,_33a){
if(!nitobi.browser.IE&&tsv[tsv.length-1]!="\n"){
tsv=tsv+"\n";
}
var _33b="<TABLE><TBODY>"+tsv.replace(/[\&\r]/g,"").replace(/([^\t\n]*)[\t]/g,"<TD>$1</TD>").replace(/([^\n]*?)\n/g,"<TR>$1</TR>").replace(/\>([^\<]*)\<\/TR/g,"><TD>$1</TD></TR")+"</TBODY></TABLE>";
if(_33b.indexOf("<TBODY><TR>")==-1){
_33b=_33b.replace(/TBODY\>(.*)\<\/TBODY/,"TBODY><TR><TD>$1</TD></TR></TBODY");
}
return nitobi.data.FormatConverter.convertHtmlTableToEbaXml(_33b,_339,_33a);
};
nitobi.data.FormatConverter.convertTsvToJs=function(tsv){
var _33d="["+tsv.replace(/[\&\r]/g,"").replace(/([^\t\n]*)[\t]/g,"$1\",\"").replace(/([^\n]*?)\n/g,"[\"$1\"],")+"]";
return _33d;
};
nitobi.data.FormatConverter.convertEbaXmlToHtmlTable=function(_33e,_33f,_340,_341){
var s="<xsl:stylesheet version=\"1.0\" xmlns:ntb=\"http://www.nitobi.com\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"><xsl:output encoding=\"UTF-8\" method=\"html\" omit-xml-declaration=\"yes\" /><xsl:template match = \"*\"><xsl:apply-templates /></xsl:template><xsl:template match = \"/\">";
s+="<TABLE><TBODY><xsl:for-each select=\"//ntb:e[@xi>"+parseInt(_340-1)+" and @xi &lt; "+parseInt(_341+1)+"]\" ><TR>";
for(var _343=0;_343<_33f.length;_343++){
s+="<TD><xsl:value-of select=\"@"+_33f[_343]+"\" /></TD>";
}
s+="</TR></xsl:for-each></TBODY></TABLE></xsl:template></xsl:stylesheet>";
var _344=nitobi.xml.createXslProcessor(s);
return nitobi.xml.transformToXml(_33e,_344).xml.replace(/xmlns:ntb="http:\/\/www.nitobi.com"/,"");
};
nitobi.data.FormatConverter.convertEbaXmlToTsv=function(_345,_346,_347,_348){
var s="<xsl:stylesheet version=\"1.0\" xmlns:ntb=\"http://www.nitobi.com\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"><xsl:output encoding=\"UTF-8\" method=\"text\" omit-xml-declaration=\"yes\" /><xsl:template match = \"*\"><xsl:apply-templates /></xsl:template><xsl:template match = \"/\">";
s+="<xsl:for-each select=\"//ntb:e[@xi>"+parseInt(_347-1)+" and @xi &lt; "+parseInt(_348+1)+"]\" >\n";
for(var _34a=0;_34a<_346.length;_34a++){
s+="<xsl:value-of select=\"@"+_346[_34a]+"\" />";
if(_34a<_346.length-1){
s+="<xsl:text>&#x09;</xsl:text>";
}
}
s+="<xsl:text>&#xa;</xsl:text></xsl:for-each></xsl:template></xsl:stylesheet>";
var _34b=nitobi.xml.createXslProcessor(s);
return nitobi.xml.transformToString(_345,_34b).replace(/xmlns:ntb="http:\/\/www.nitobi.com"/,"");
};
nitobi.data.FormatConverter.getDataColumns=function(data){
var _34d=0;
if(data!=null&&data!=""){
if(data.substr(0,1)=="<"){
_34d=data.toLowerCase().substr(0,data.toLowerCase().indexOf("</tr>")).split("</td>").length-1;
}else{
_34d=data.substr(0,data.indexOf("\n")).split("\t").length;
}
}else{
_34d=0;
}
return _34d;
};
nitobi.data.FormatConverter.getDataRows=function(data){
var _34f=0;
if(data!=null&&data!=""){
if(data.substr(0,1)=="<"){
_34f=data.toLowerCase().split("</tr>").length-1;
}else{
retValArray=data.split("\n");
_34f=retValArray.length;
if(retValArray[retValArray.length-1]==""){
_34f--;
}
}
}else{
_34f=0;
}
return _34f;
};
nitobi.grid.DateColumn=function(grid,_351){
nitobi.grid.DateColumn.baseConstructor.call(this,grid,_351);
};
nitobi.lang.extend(nitobi.grid.DateColumn,nitobi.grid.Column);
var ntb_datep=nitobi.grid.DateColumn.prototype;
ntb_datep.setMask=function(){
this.xSET("Mask",arguments);
};
ntb_datep.getMask=function(){
return this.xGET("Mask",arguments);
};
ntb_datep.setCalendarEnabled=function(){
this.xSET("CalendarEnabled",arguments);
};
ntb_datep.isCalendarEnabled=function(){
return nitobi.lang.toBool(this.xGET("CalendarEnabled",arguments),false);
};
nitobi.lang.defineNs("nitobi.grid.Declaration");
nitobi.grid.Declaration.parse=function(_352){
var _353={};
_353.grid=nitobi.xml.parseHtml(_352);
var _354=_352.firstChild;
while(_354!=null){
if(typeof (_354.tagName)!="undefined"){
var tag=_354.tagName.replace(/ntb\:/gi,"").toLowerCase();
if(tag=="inlinehtml"){
_353[tag]=_354;
}else{
var _356="http://www.nitobi.com";
if(tag=="columndefinition"){
var sXml;
if(nitobi.browser.IE){
sXml=("<"+nitobi.xml.nsPrefix+"grid xmlns:ntb=\""+_356+"\"><"+nitobi.xml.nsPrefix+"columns>"+_354.parentNode.innerHTML.substring(31).replace(/\=\s*([^\"^\s^\>]+)/g,"=\"$1\" ")+"</"+nitobi.xml.nsPrefix+"columns></"+nitobi.xml.nsPrefix+"grid>");
}else{
sXml="<"+nitobi.xml.nsPrefix+"grid xmlns:ntb=\""+_356+"\"><"+nitobi.xml.nsPrefix+"columns>"+_354.parentNode.innerHTML.replace(/\=\s*([^\"^\s^\>]+)/g,"=\"$1\" ")+"</"+nitobi.xml.nsPrefix+"columns></"+nitobi.xml.nsPrefix+"grid>";
}
sXml=sXml.replace(/\&nbsp\;/gi," ");
_353["columndefinitions"]=nitobi.xml.createXmlDoc();
_353["columndefinitions"].validateOnParse=false;
_353["columndefinitions"]=nitobi.xml.loadXml(_353["columndefinitions"],sXml);
break;
}else{
_353[tag]=nitobi.xml.parseHtml(_354);
}
}
}
_354=_354.nextSibling;
}
return _353;
};
nitobi.grid.Declaration.loadDataSources=function(_358,grid){
var _35a=new Array();
if(_358["datasources"]){
_35a=_358.datasources.selectNodes("//"+nitobi.xml.nsPrefix+"datasources/*");
}
if(_35a.length>0){
for(var i=0;i<_35a.length;i++){
var id=_35a[i].getAttribute("id");
if(id!="_default"){
var _35d=_35a[i].xml.replace(/fieldnames=/g,"FieldNames=").replace(/keys=/g,"Keys=");
_35d="<ntb:grid xmlns:ntb=\"http://www.nitobi.com\"><ntb:datasources>"+_35d+"</ntb:datasources></ntb:grid>";
var _35e=new nitobi.data.DataTable("local",grid.getPagingMode()!=nitobi.grid.PAGINGMODE_NONE,{GridId:grid.getID()},{GridId:grid.getID()},grid.isAutoKeyEnabled());
_35e.initialize(id,_35d);
_35e.initializeXml(_35d);
grid.data.add(_35e);
var _35f=grid.model.selectNodes("//nitobi.grid.Column[@DatasourceId='"+id+"']");
for(var j=0;j<_35f.length;j++){
grid.editorDataReady(_35f[j]);
}
}
}
}
};
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.EditCompleteEventArgs=function(obj,_362,_363,cell){
this.editor=obj;
this.cell=cell;
this.databaseValue=_363;
this.displayValue=_362;
};
nitobi.grid.EditCompleteEventArgs.prototype.dispose=function(){
this.editor=null;
this.cell=null;
this.metadata=null;
};
nitobi.data.GetCompleteEventArgs=function(_365,_366,_367,_368,_369,_36a,obj,_36c,_36d){
this.firstRow=_365;
this.lastRow=_366;
this.callback=_36c;
this.dataSource=_36a;
this.context=obj;
this.ajaxCallback=_369;
this.startXi=_367;
this.pageSize=_368;
this.lastPage=false;
this.numRowsReturned=_36d;
this.lastRowReturned=_366;
};
nitobi.data.GetCompleteEventArgs.prototype.dispose=function(){
this.callback=null;
this.context=null;
this.dataSource=null;
this.ajaxCallback.clear();
this.ajaxCallback==null;
};
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.MODE_STANDARDPAGING="standard";
nitobi.grid.MODE_LOCALSTANDARDPAGING="localstandard";
nitobi.grid.MODE_LIVESCROLLING="livescrolling";
nitobi.grid.MODE_LOCALLIVESCROLLING="locallivescrolling";
nitobi.grid.MODE_NONPAGING="nonpaging";
nitobi.grid.MODE_LOCALNONPAGING="localnonpaging";
nitobi.grid.MODE_SMARTPAGING="smartpaging";
nitobi.grid.MODE_PAGEDLIVESCROLLING="pagedlivescrolling";
nitobi.grid.RENDERMODE_ONDEMAND="ondemand";
nitobi.lang.defineNs("nitobi.GridFactory");
nitobi.GridFactory.createGrid=function(_36e,_36f,_370){
var _371="";
var _372="";
var _373="";
_370=nitobi.html.getElement(_370);
if(_370!=null){
xDeclaration=nitobi.grid.Declaration.parse(_370);
_36e=xDeclaration.grid.documentElement.getAttribute("mode");
var _374=nitobi.GridFactory.isGetHandler(xDeclaration);
var _375=nitobi.GridFactory.isDatasourceId(xDeclaration);
var _376=false;
if(_36e==nitobi.grid.MODE_LOCALLIVESCROLLING){
_371=nitobi.grid.PAGINGMODE_LIVESCROLLING;
_372=nitobi.data.DATAMODE_LOCAL;
}else{
if(_36e==nitobi.grid.MODE_LIVESCROLLING){
_371=nitobi.grid.PAGINGMODE_LIVESCROLLING;
_372=nitobi.data.DATAMODE_CACHING;
}else{
if(_36e==nitobi.grid.MODE_NONPAGING){
_376=true;
_371=nitobi.grid.PAGINGMODE_NONE;
_372=nitobi.data.DATAMODE_LOCAL;
}else{
if(_36e==nitobi.grid.MODE_LOCALNONPAGING){
_371=nitobi.grid.PAGINGMODE_NONE;
_372=nitobi.data.DATAMODE_LOCAL;
}else{
if(_36e==nitobi.grid.MODE_LOCALSTANDARDPAGING){
_371=nitobi.grid.PAGINGMODE_STANDARD;
_372=nitobi.data.DATAMODE_LOCAL;
}else{
if(_36e==nitobi.grid.MODE_STANDARDPAGING){
_371=nitobi.grid.PAGINGMODE_STANDARD;
_372=nitobi.data.DATAMODE_PAGING;
}else{
if(_36e==nitobi.grid.MODE_PAGEDLIVESCROLLING){
_371=nitobi.grid.PAGINGMODE_STANDARD;
_372=nitobi.data.DATAMODE_PAGING;
_373=nitobi.grid.RENDERMODE_ONDEMAND;
}else{
}
}
}
}
}
}
}
}
var id=_370.getAttribute("id");
_36e=(_36e||nitobi.grid.MODE_STANDARDPAGING).toLowerCase();
var grid=null;
if(_36e==nitobi.grid.MODE_LOCALSTANDARDPAGING){
grid=new nitobi.grid.GridLocalPage(id);
}else{
if(_36e==nitobi.grid.MODE_LIVESCROLLING){
grid=new nitobi.grid.GridLiveScrolling(id);
}else{
if(_36e==nitobi.grid.MODE_LOCALLIVESCROLLING){
grid=new nitobi.grid.GridLiveScrolling(id);
}else{
if(_36e==nitobi.grid.MODE_NONPAGING||_36e==nitobi.grid.MODE_LOCALNONPAGING){
grid=new nitobi.grid.GridNonpaging(id);
}else{
if(_36e==nitobi.grid.MODE_STANDARDPAGING||_36e==nitobi.grid.MODE_PAGEDLIVESCROLLING){
grid=new nitobi.grid.GridStandard(id);
}
}
}
}
}
grid.setPagingMode(_371);
grid.setDataMode(_372);
grid.setRenderMode(_373);
nitobi.GridFactory.processDeclaration(grid,_370,xDeclaration);
_370.jsObject=grid;
return grid;
};
nitobi.GridFactory.processDeclaration=function(grid,_37a,_37b){
if(_37b!=null){
grid.setDeclaration(_37b);
if(typeof (_37b.inlinehtml)=="undefined"){
var _37c=document.createElement("ntb:inlinehtml");
_37c.setAttribute("parentid","grid"+grid.uid);
_37a.insertAdjacentElement("beforeEnd",_37c);
grid.Declaration.inlinehtml=_37c;
}
if(this.data==null||this.data.tables==null||this.data.tables.length==0){
var _37d=new nitobi.data.DataSet();
_37d.initialize();
grid.connectToDataSet(_37d);
}
grid.initializeModelFromDeclaration();
var _37e=grid.Declaration.columndefinitions||grid.Declaration.columns;
if(typeof (_37e)!="undefined"&&_37e!=null&&_37e.childNodes.length!=0&&_37e.childNodes[0].childNodes.length!=0){
grid.defineColumns(_37e.documentElement);
}
nitobi.grid.Declaration.loadDataSources(_37b,grid);
grid.attachToParentDomElement(grid.Declaration.inlinehtml);
var _37f=grid.getDataMode();
var _380=grid.getDatasourceId();
var _381=grid.getGetHandler();
if(_380!=null&&_380!=""){
grid.connectToTable(grid.data.getTable(_380));
}else{
grid.ensureConnected();
if(grid.mode.toLowerCase()==nitobi.grid.MODE_LIVESCROLLING&&_37b!=null&&_37b.datasources!=null){
var _382=_37b.datasources.selectNodes("//ntb:datasource[@id='_default']/ntb:data/ntb:e").length;
if(_382>0){
var _383=grid.data.getTable("_default");
_383.initializeXmlData(_37b.grid.xml);
_383.initializeXml(_37b.grid.xml);
_383.descriptor.leap(0,_382*2);
_383.syncRowCount();
}
}
}
window.setTimeout(function(){
grid.bind();
},50);
}
};
nitobi.GridFactory.isLocal=function(_384){
var _385=_384.grid.documentElement.getAttribute("datasourceid");
var _386=_384.grid.documentElement.getAttribute("gethandler");
if(_386!=null&&_386!=""){
return false;
}else{
if(_385!=null&&_385!=""){
return true;
}else{
throw ("Non-paging grid requires either a gethandler or a local datasourceid to be specified.");
}
}
};
nitobi.GridFactory.isGetHandler=function(_387){
var _388=_387.grid.documentElement.getAttribute("gethandler");
if(_388!=null&&_388!=""){
return true;
}
return false;
};
nitobi.GridFactory.isDatasourceId=function(_389){
var _38a=_389.grid.documentElement.getAttribute("datasourceid");
if(_38a!=null&&_38a!=""){
return true;
}
return false;
};
nitobi.grid.hover=function(_38b,_38c,_38d){
if(!_38d){
return;
}
var id=_38b.getAttribute("id");
var _38f=id.replace(/__/g,"||");
var _390=_38f.split("_");
var row=_390[3];
var uid=_390[5].replace(/\|\|/g,"__");
var _393=document.getElementById("cell_"+row+"_0_"+uid);
var _394=_393.parentNode;
var _395=_394.childNodes[_394.childNodes.length-1];
var id=_395.getAttribute("id");
var _390=id.split("_");
var _396=document.getElementById("cell_"+row+"_"+(Number(_390[4])+1)+"_"+uid);
var _397=null;
if(_396!=null){
_397=_396.parentNode;
}
if(_38c){
var _398=nitobi.grid.RowHoverColor||"white";
_394.style.backgroundColor=_398;
if(_397){
_397.style.backgroundColor=_398;
}
}else{
_394.style.backgroundColor="";
if(_397){
_397.style.backgroundColor="";
}
}
if(_38c){
nitobi.html.addClass(_38b,"ntb-cell-hover");
}else{
nitobi.html.removeClass(_38b,"ntb-cell-hover");
}
};
initEBAGrids=function(){
nitobi.initComponents();
};
nitobi.initGrids=function(){
var _399=[];
var _39a=document.getElementsByTagName(nitobi.browser.MOZ||nitobi.browser.SAFARI?"ntb:grid":"grid");
for(var i=0;i<_39a.length;i++){
if(_39a[i].jsObject==null){
_39a[i].jsObject=nitobi.GridFactory.createGrid(null,null,_39a[i]);
_399.push(_39a[i].jsObject);
}
}
return _399;
};
nitobi.initGrid=function(id){
var grid=nitobi.html.getElement(id);
if(grid!=null){
grid.jsObject=nitobi.GridFactory.createGrid(null,null,grid);
}
return grid.jsObject;
};
nitobi.initComponents=function(){
nitobi.initGrids();
};
nitobi.getGrid=function(_39e){
return document.getElementById(_39e).jsObject;
};
nitobi.base.Registry.getInstance().register(new nitobi.base.Profile("nitobi.initGrid",null,false,"ntb:grid"));
nitobi.grid.GridLiveScrolling=function(uid){
nitobi.grid.GridLiveScrolling.baseConstructor.call(this,uid);
this.mode="livescrolling";
this.setPagingMode(nitobi.grid.PAGINGMODE_LIVESCROLLING);
this.setDataMode(nitobi.data.DATAMODE_CACHING);
};
nitobi.lang.extend(nitobi.grid.GridLiveScrolling,nitobi.grid.Grid);
nitobi.grid.GridLiveScrolling.prototype.createChildren=function(){
var args=arguments;
nitobi.grid.GridLiveScrolling.base.createChildren.call(this,args);
nitobi.grid.GridLiveScrolling.base.createToolbars.call(this,nitobi.ui.Toolbars.VisibleToolbars.STANDARD);
};
nitobi.grid.GridLiveScrolling.prototype.bind=function(){
nitobi.grid.GridStandard.base.bind.call(this);
if(this.getGetHandler()!=""){
this.ensureConnected();
var rows=this.getRowsPerPage();
if(this.datatable.mode=="local"){
rows=null;
}
this.datatable.get(0,rows,this,this.getComplete);
}else{
this.finalizeRowCount(this.datatable.getRemoteRowCount());
this.bindComplete();
}
};
nitobi.grid.GridLiveScrolling.prototype.getComplete=function(_3a2){
nitobi.grid.GridLiveScrolling.base.getComplete.call(this,_3a2);
if(!this.columnsDefined){
this.defineColumnsFinalize();
}
this.bindComplete();
};
nitobi.grid.GridLiveScrolling.prototype.pageSelect=function(dir){
var _3a4=this.Scroller.getUnrenderedBlocks();
var rows=_3a4.last-_3a4.first;
this.reselect(0,rows*dir);
};
nitobi.grid.GridLiveScrolling.prototype.page=function(dir){
var _3a7=this.Scroller.getUnrenderedBlocks();
var rows=_3a7.last-_3a7.first;
this.move(0,rows*dir);
};
nitobi.grid.LoadingScreen=function(grid){
this.loadingScreen=null;
this.grid=grid;
this.loadingImg=null;
};
nitobi.grid.LoadingScreen.prototype.initialize=function(){
this.loadingScreen=document.createElement("div");
var _3aa=this.findCssUrl();
var msg="";
if(_3aa==null){
msg="Loading...";
}else{
msg="<img src='"+_3aa+"loading.gif'  class='ntb-loading-Icon' valign='absmiddle'></img>";
}
this.loadingScreen.innerHTML="<table style='padding:0px;margin:0px;' border='0' width='100%' height='100%'><tr style='padding:0px;margin:0px;'><td style='padding:0px;margin:0px;text-align:center;font:verdana;font-size:10pt;'>"+msg+"</td></tr></table>";
this.loadingScreen.className="ntb-loading";
var lss=this.loadingScreen.style;
lss.verticalAlign="middle";
lss.visibility="hidden";
lss.position="absolute";
lss.top="0px";
lss.left="0px";
};
nitobi.grid.LoadingScreen.prototype.attachToElement=function(_3ad){
_3ad.appendChild(this.loadingScreen);
};
nitobi.grid.LoadingScreen.prototype.findCssUrl=function(){
var _3ae=nitobi.html.findParentStylesheet(".ntb-loading-Icon");
if(_3ae==null){
return null;
}
var _3af=nitobi.html.normalizeUrl(_3ae.href);
if(nitobi.browser.IE){
while(_3ae.parentStyleSheet){
_3ae=_3ae.parentStyleSheet;
_3af=nitobi.html.normalizeUrl(_3ae.href)+_3af;
}
}
return _3af;
};
nitobi.grid.LoadingScreen.prototype.show=function(){
try{
this.resize();
this.loadingScreen.style.visibility="visible";
this.loadingScreen.style.display="block";
}
catch(e){
}
};
nitobi.grid.LoadingScreen.prototype.resize=function(){
this.loadingScreen.style.width=this.grid.getWidth()+"px";
this.loadingScreen.style.height=this.grid.getHeight()+"px";
};
nitobi.grid.LoadingScreen.prototype.hide=function(){
this.loadingScreen.style.display="none";
};
nitobi.grid.GridLocalPage=function(uid){
nitobi.grid.GridLocalPage.baseConstructor.call(this,uid);
this.mode="localpaging";
this.setPagingMode(nitobi.grid.PAGINGMODE_STANDARD);
this.setDataMode("local");
};
nitobi.lang.extend(nitobi.grid.GridLocalPage,nitobi.grid.Grid);
nitobi.grid.GridLocalPage.prototype.createChildren=function(){
var args=arguments;
nitobi.grid.GridLocalPage.base.createChildren.call(this,args);
nitobi.grid.GridLiveScrolling.base.createToolbars.call(this,nitobi.ui.Toolbars.VisibleToolbars.STANDARD|nitobi.ui.Toolbars.VisibleToolbars.PAGING);
this.toolbars.subscribe("NextPage",nitobi.lang.close(this,this.pageNext));
this.toolbars.subscribe("PreviousPage",nitobi.lang.close(this,this.pagePrevious));
this.subscribe("EndOfData",function(pct){
this.toolbars.pagingToolbar.getUiElements()["nextPage"+this.toolbars.uid].disable();
});
this.subscribe("TopOfData",function(pct){
this.toolbars.pagingToolbar.getUiElements()["previousPage"+this.toolbars.uid].disable();
});
this.subscribe("NotTopOfData",function(pct){
this.toolbars.pagingToolbar.getUiElements()["previousPage"+this.toolbars.uid].enable();
});
this.subscribe("NotEndOfData",function(pct){
this.toolbars.pagingToolbar.getUiElements()["nextPage"+this.toolbars.uid].enable();
});
};
nitobi.grid.GridLocalPage.prototype.pagePrevious=function(){
this.fire("BeforeLoadPreviousPage");
this.loadDataPage(Math.max(this.getCurrentPageIndex()-1,0));
this.fire("AfterLoadPreviousPage");
};
nitobi.grid.GridLocalPage.prototype.pageNext=function(){
this.fire("BeforeLoadNextPage");
this.loadDataPage(this.getCurrentPageIndex()+1);
this.fire("AfterLoadNextPage");
};
nitobi.grid.GridLocalPage.prototype.loadDataPage=function(_3b6){
this.fire("BeforeLoadDataPage");
if(_3b6>-1){
this.setCurrentPageIndex(_3b6);
this.setDisplayedRowCount(this.getRowsPerPage());
var _3b7=this.getCurrentPageIndex()*this.getRowsPerPage();
var rows=this.getRowsPerPage()-this.getfreezetop();
this.setDisplayedRowCount(rows);
var _3b9=_3b7+rows;
if(_3b9>=this.getRowCount()){
this.fire("EndOfData");
}else{
this.fire("NotEndOfData");
}
if(_3b7==0){
this.fire("TopOfData");
}else{
this.fire("NotTopOfData");
}
this.clearSurfaces();
this.updateCellRanges();
this.scrollVertical(0);
}
this.fire("AfterLoadDataPage");
};
nitobi.grid.GridLocalPage.prototype.setRowsPerPage=function(rows){
this.setDisplayedRowCount(this.getRowsPerPage());
this.data.table.pageSize=this.getRowsPerPage();
};
nitobi.grid.GridLocalPage.prototype.pageStartIndexChanges=function(){
};
nitobi.grid.GridLocalPage.prototype.hitFirstPage=function(){
this.fire("FirstPage");
};
nitobi.grid.GridLocalPage.prototype.hitLastPage=function(){
this.fire("LastPage");
};
nitobi.grid.GridLocalPage.prototype.bind=function(){
nitobi.grid.GridLocalPage.base.bind.call(this);
this.finalizeRowCount(this.datatable.getRemoteRowCount());
this.bindComplete();
};
nitobi.grid.GridLocalPage.prototype.pageUpKey=function(){
this.pagePrevious();
};
nitobi.grid.GridLocalPage.prototype.pageDownKey=function(){
this.pageNext();
};
nitobi.grid.GridLocalPage.prototype.renderMiddle=function(){
nitobi.grid.GridLocalPage.base.renderMiddle.call(this,arguments);
var _3bb=this.getfreezetop();
endRow=this.getRowsPerPage()-1;
this.Scroller.view.midcenter.renderGap(_3bb,endRow,false);
};
nitobi.grid.GridNonpaging=function(uid){
nitobi.grid.GridNonpaging.baseConstructor.call(this);
this.mode="nonpaging";
this.setPagingMode(nitobi.grid.PAGINGMODE_NONE);
this.setDataMode(nitobi.data.DATAMODE_LOCAL);
};
nitobi.lang.extend(nitobi.grid.GridNonpaging,nitobi.grid.Grid);
nitobi.grid.GridNonpaging.prototype.createChildren=function(){
var args=arguments;
nitobi.grid.GridNonpaging.base.createChildren.call(this,args);
nitobi.grid.GridNonpaging.base.createToolbars.call(this,nitobi.ui.Toolbars.VisibleToolbars.STANDARD);
};
nitobi.grid.GridNonpaging.prototype.bind=function(){
nitobi.grid.GridStandard.base.bind.call(this);
if(this.getGetHandler()!=""){
this.ensureConnected();
this.datatable.get(0,null,this,this.getComplete);
}else{
this.finalizeRowCount(this.datatable.getRemoteRowCount());
this.bindComplete();
}
};
nitobi.grid.GridNonpaging.prototype.getComplete=function(_3be){
nitobi.grid.GridNonpaging.base.getComplete.call(this,_3be);
this.finalizeRowCount(_3be.numRowsReturned);
this.defineColumnsFinalize();
this.bindComplete();
};
nitobi.grid.GridNonpaging.prototype.renderMiddle=function(){
nitobi.grid.GridNonpaging.base.renderMiddle.call(this,arguments);
var _3bf=this.getfreezetop();
endRow=this.getRowCount();
this.Scroller.view.midcenter.renderGap(_3bf,endRow,false);
};
nitobi.grid.GridStandard=function(uid){
nitobi.grid.GridStandard.baseConstructor.call(this,uid);
this.mode="standard";
this.setPagingMode(nitobi.grid.PAGINGMODE_STANDARD);
this.setDataMode(nitobi.data.DATAMODE_PAGING);
};
nitobi.lang.extend(nitobi.grid.GridStandard,nitobi.grid.Grid);
nitobi.grid.GridStandard.prototype.createChildren=function(){
var args=arguments;
nitobi.grid.GridStandard.base.createChildren.call(this,args);
nitobi.grid.GridStandard.base.createToolbars.call(this,nitobi.ui.Toolbars.VisibleToolbars.STANDARD|nitobi.ui.Toolbars.VisibleToolbars.PAGING);
this.toolbars.subscribe("NextPage",nitobi.lang.close(this,this.pageNext));
this.toolbars.subscribe("PreviousPage",nitobi.lang.close(this,this.pagePrevious));
this.subscribe("EndOfData",this.disableNextPage);
this.subscribe("TopOfData",this.disablePreviousPage);
this.subscribe("NotTopOfData",this.enablePreviousPage);
this.subscribe("NotEndOfData",this.enableNextPage);
this.subscribe("TableConnected",nitobi.lang.close(this,this.subscribeToRowCountReady));
};
nitobi.grid.GridStandard.prototype.connectToTable=function(_3c2){
if(nitobi.grid.GridStandard.base.connectToTable.call(this,_3c2)!=false){
this.datatable.subscribe("RowInserted",nitobi.lang.close(this,this.incrementDisplayedRowCount));
this.datatable.subscribe("RowDeleted",nitobi.lang.close(this,this.decrementDisplayedRowCount));
}
};
nitobi.grid.GridStandard.prototype.incrementDisplayedRowCount=function(_3c3){
this.setDisplayedRowCount(this.getDisplayedRowCount()+(_3c3||1));
this.updateCellRanges();
};
nitobi.grid.GridStandard.prototype.decrementDisplayedRowCount=function(_3c4){
this.setDisplayedRowCount(this.getDisplayedRowCount()-(_3c4||1));
this.updateCellRanges();
};
nitobi.grid.GridStandard.prototype.subscribeToRowCountReady=function(){
};
nitobi.grid.GridStandard.prototype.updateDisplayedRowCount=function(_3c5){
this.setDisplayedRowCount(_3c5.numRowsReturned);
};
nitobi.grid.GridStandard.prototype.disableNextPage=function(){
this.disableButton("nextPage");
};
nitobi.grid.GridStandard.prototype.disablePreviousPage=function(){
this.disableButton("previousPage");
};
nitobi.grid.GridStandard.prototype.disableButton=function(_3c6){
var t=this.getToolbars().pagingToolbar;
if(t!=null){
t.getUiElements()[_3c6+this.toolbars.uid].disable();
}
};
nitobi.grid.GridStandard.prototype.enableNextPage=function(){
this.enableButton("nextPage");
};
nitobi.grid.GridStandard.prototype.enablePreviousPage=function(){
this.enableButton("previousPage");
};
nitobi.grid.GridStandard.prototype.enableButton=function(_3c8){
var t=this.getToolbars().pagingToolbar;
if(t!=null){
t.getUiElements()[_3c8+this.toolbars.uid].enable();
}
};
nitobi.grid.GridStandard.prototype.pagePrevious=function(){
this.fire("BeforeLoadPreviousPage");
this.loadDataPage(Math.max(this.getCurrentPageIndex()-1,0));
this.fire("AfterLoadPreviousPage");
};
nitobi.grid.GridStandard.prototype.pageNext=function(){
this.fire("BeforeLoadNextPage");
this.loadDataPage(this.getCurrentPageIndex()+1);
this.fire("AfterLoadNextPage");
};
nitobi.grid.GridStandard.prototype.loadDataPage=function(_3ca){
this.fire("BeforeLoadDataPage");
if(_3ca>-1){
if(this.sortColumn){
if(this.datatable.sortColumn){
for(var i=0;i<this.getColumnCount();i++){
var _3cc=this.getColumnObject(i);
if(_3cc.getColumnName()==this.datatable.sortColumn){
this.setSortStyle(i,this.datatable.sortDir);
break;
}
}
}else{
this.setSortStyle(this.sortColumn.column,"",true);
}
}
this.setCurrentPageIndex(_3ca);
var _3cd=this.getCurrentPageIndex()*this.getRowsPerPage();
var rows=this.getRowsPerPage()-this.getfreezetop();
this.datatable.flush();
this.datatable.get(_3cd,rows,this,this.afterLoadDataPage);
}
this.fire("AfterLoadDataPage");
};
nitobi.grid.GridStandard.prototype.afterLoadDataPage=function(_3cf){
this.setDisplayedRowCount(_3cf.numRowsReturned);
this.setRowCount(_3cf.numRowsReturned);
if(_3cf.numRowsReturned!=this.getRowsPerPage()){
this.fire("EndOfData");
}else{
this.fire("NotEndOfData");
}
if(this.getCurrentPageIndex()==0){
this.fire("TopOfData");
}else{
this.fire("NotTopOfData");
}
this.clearSurfaces();
this.updateCellRanges();
this.scrollVertical(0);
};
nitobi.grid.GridStandard.prototype.bind=function(){
nitobi.grid.GridStandard.base.bind.call(this);
this.setCurrentPageIndex(0);
this.disablePreviousPage();
this.enableNextPage();
this.ensureConnected();
this.datatable.get(0,this.getRowsPerPage(),this,this.getComplete);
};
nitobi.grid.GridStandard.prototype.getComplete=function(_3d0){
this.afterLoadDataPage(_3d0);
nitobi.grid.GridStandard.base.getComplete.call(this,_3d0);
this.defineColumnsFinalize();
this.bindComplete();
};
nitobi.grid.GridStandard.prototype.renderMiddle=function(){
nitobi.grid.GridStandard.base.renderMiddle.call(this,arguments);
var _3d1=this.getfreezetop();
endRow=this.getRowsPerPage()-1;
this.Scroller.view.midcenter.renderGap(_3d1,endRow,false);
};
nitobi.grid.NumberColumn=function(grid,_3d3){
nitobi.grid.NumberColumn.baseConstructor.call(this,grid,_3d3);
};
nitobi.lang.extend(nitobi.grid.NumberColumn,nitobi.grid.Column);
var ntb_numberp=nitobi.grid.NumberColumn.prototype;
ntb_numberp.setAlign=function(){
this.xSET("Align",arguments);
};
ntb_numberp.getAlign=function(){
return this.xGET("Align",arguments);
};
ntb_numberp.setMask=function(){
this.xSET("Mask",arguments);
};
ntb_numberp.getMask=function(){
return this.xGET("Mask",arguments);
};
ntb_numberp.setNegativeMask=function(){
this.xSET("NegativeMask",arguments);
};
ntb_numberp.getNegativeMask=function(){
return this.xGET("NegativeMask",arguments);
};
ntb_numberp.setGroupingSeparator=function(){
this.xSET("GroupingSeparator",arguments);
};
ntb_numberp.getGroupingSeparator=function(){
return this.xGET("GroupingSeparator",arguments);
};
ntb_numberp.setDecimalSeparator=function(){
this.xSET("DecimalSeparator",arguments);
};
ntb_numberp.getDecimalSeparator=function(){
return this.xGET("DecimalSeparator",arguments);
};
ntb_numberp.setOnKeyDownEvent=function(){
this.xSET("OnKeyDownEvent",arguments);
};
ntb_numberp.getOnKeyDownEvent=function(){
return this.xGET("OnKeyDownEvent",arguments);
};
ntb_numberp.setOnKeyUpEvent=function(){
this.xSET("OnKeyUpEvent",arguments);
};
ntb_numberp.getOnKeyUpEvent=function(){
return this.xGET("OnKeyUpEvent",arguments);
};
ntb_numberp.setOnKeyPressEvent=function(){
this.xSET("OnKeyPressEvent",arguments);
};
ntb_numberp.getOnKeyPressEvent=function(){
return this.xGET("OnKeyPressEvent",arguments);
};
ntb_numberp.setOnChangeEvent=function(){
this.xSET("OnChangeEvent",arguments);
};
ntb_numberp.getOnChangeEvent=function(){
return this.xGET("OnChangeEvent",arguments);
};
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnCopyEventArgs=function(_3d4,data,_3d6){
nitobi.grid.OnCopyEventArgs.baseConstructor.apply(this,arguments);
};
nitobi.lang.extend(nitobi.grid.OnCopyEventArgs,nitobi.grid.SelectionEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnPasteEventArgs=function(_3d7,data,_3d9){
nitobi.grid.OnPasteEventArgs.baseConstructor.apply(this,arguments);
};
nitobi.lang.extend(nitobi.grid.OnPasteEventArgs,nitobi.grid.SelectionEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnAfterCellEditEventArgs=function(_3da,cell){
nitobi.grid.OnAfterCellEditEventArgs.baseConstructor.call(this,_3da,cell);
};
nitobi.lang.extend(nitobi.grid.OnAfterCellEditEventArgs,nitobi.grid.CellEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnAfterColumnResizeEventArgs=function(_3dc,_3dd){
nitobi.grid.OnAfterColumnResizeEventArgs.baseConstructor.call(this,_3dc,_3dd);
};
nitobi.lang.extend(nitobi.grid.OnAfterColumnResizeEventArgs,nitobi.grid.ColumnEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnAfterRowDeleteEventArgs=function(_3de,row){
nitobi.grid.OnAfterRowDeleteEventArgs.baseConstructor.call(this,_3de,row);
};
nitobi.lang.extend(nitobi.grid.OnAfterRowDeleteEventArgs,nitobi.grid.RowEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnAfterRowInsertEventArgs=function(_3e0,row){
nitobi.grid.OnAfterRowInsertEventArgs.baseConstructor.call(this,_3e0,row);
};
nitobi.lang.extend(nitobi.grid.OnAfterRowInsertEventArgs,nitobi.grid.RowEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnAfterSortEventArgs=function(_3e2,_3e3,_3e4){
nitobi.grid.OnAfterSortEventArgs.baseConstructor.call(this,_3e2,_3e3);
this.direction=_3e4;
};
nitobi.lang.extend(nitobi.grid.OnAfterSortEventArgs,nitobi.grid.ColumnEventArgs);
nitobi.grid.OnAfterSortEventArgs.prototype.getDirection=function(){
return this.direction;
};
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnBeforeCellEditEventArgs=function(_3e5,cell){
nitobi.grid.OnBeforeCellEditEventArgs.baseConstructor.call(this,_3e5,cell);
};
nitobi.lang.extend(nitobi.grid.OnBeforeCellEditEventArgs,nitobi.grid.CellEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnBeforeColumnResizeEventArgs=function(_3e7,_3e8){
nitobi.grid.OnBeforeColumnResizeEventArgs.baseConstructor.call(this,_3e7,_3e8);
};
nitobi.lang.extend(nitobi.grid.OnBeforeColumnResizeEventArgs,nitobi.grid.ColumnEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnBeforeRowDeleteEventArgs=function(_3e9,row){
nitobi.grid.OnBeforeRowDeleteEventArgs.baseConstructor.call(this,_3e9,row);
};
nitobi.lang.extend(nitobi.grid.OnBeforeRowDeleteEventArgs,nitobi.grid.RowEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnBeforeRowInsertEventArgs=function(_3eb,row){
nitobi.grid.OnBeforeRowInsertEventArgs.baseConstructor.call(this,_3eb,row);
};
nitobi.lang.extend(nitobi.grid.OnBeforeRowInsertEventArgs,nitobi.grid.RowEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnBeforeSortEventArgs=function(_3ed,_3ee,_3ef){
nitobi.grid.OnBeforeSortEventArgs.baseConstructor.call(this,_3ed,_3ee);
this.direction=_3ef;
};
nitobi.lang.extend(nitobi.grid.OnBeforeSortEventArgs,nitobi.grid.ColumnEventArgs);
nitobi.grid.OnBeforeSortEventArgs.prototype.getDirection=function(){
return this.direction;
};
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnBeforeCellClickEventArgs=function(_3f0,cell){
nitobi.grid.OnBeforeCellClickEventArgs.baseConstructor.call(this,_3f0,cell);
};
nitobi.lang.extend(nitobi.grid.OnBeforeCellClickEventArgs,nitobi.grid.CellEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnCellBlurEventArgs=function(_3f2,cell){
nitobi.grid.OnCellBlurEventArgs.baseConstructor.call(this,_3f2,cell);
};
nitobi.lang.extend(nitobi.grid.OnCellBlurEventArgs,nitobi.grid.CellEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnCellClickEventArgs=function(_3f4,cell){
nitobi.grid.OnCellClickEventArgs.baseConstructor.call(this,_3f4,cell);
};
nitobi.lang.extend(nitobi.grid.OnCellClickEventArgs,nitobi.grid.CellEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnCellDblClickEventArgs=function(_3f6,cell){
nitobi.grid.OnCellDblClickEventArgs.baseConstructor.call(this,_3f6,cell);
};
nitobi.lang.extend(nitobi.grid.OnCellDblClickEventArgs,nitobi.grid.CellEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnCellFocusEventArgs=function(_3f8,cell){
nitobi.grid.OnCellFocusEventArgs.baseConstructor.call(this,_3f8,cell);
};
nitobi.lang.extend(nitobi.grid.OnCellFocusEventArgs,nitobi.grid.CellEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnCellValidateEventArgs=function(_3fa,cell,_3fc,_3fd){
nitobi.grid.OnCellValidateEventArgs.baseConstructor.call(this,_3fa,cell);
this.oldValue=_3fd;
this.newValue=_3fc;
};
nitobi.lang.extend(nitobi.grid.OnCellValidateEventArgs,nitobi.grid.CellEventArgs);
nitobi.grid.OnCellValidateEventArgs.prototype.getOldValue=function(){
return this.oldValue;
};
nitobi.grid.OnCellValidateEventArgs.prototype.getNewValue=function(){
return this.newValue;
};
nitobi.grid.OnContextMenuEventArgs=function(){
};
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnHeaderClickEventArgs=function(_3fe,_3ff){
nitobi.grid.OnHeaderClickEventArgs.baseConstructor.call(this,_3fe,_3ff);
};
nitobi.lang.extend(nitobi.grid.OnHeaderClickEventArgs,nitobi.grid.ColumnEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnRowBlurEventArgs=function(_400,row){
nitobi.grid.OnRowBlurEventArgs.baseConstructor.call(this,_400,row);
};
nitobi.lang.extend(nitobi.grid.OnRowBlurEventArgs,nitobi.grid.RowEventArgs);
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.OnRowFocusEventArgs=function(_402,row){
nitobi.grid.OnRowFocusEventArgs.baseConstructor.call(this,_402,row);
};
nitobi.lang.extend(nitobi.grid.OnRowFocusEventArgs,nitobi.grid.RowEventArgs);
nitobi.grid.Row=function(grid,row){
this.grid=grid;
this.row=row;
this.Row=row;
this.DomNode=nitobi.grid.Row.getRowElement(grid,row);
};
nitobi.grid.Row.prototype.getData=function(){
if(this.DataNode==null){
this.DataNode=this.grid.datatable.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"data/"+nitobi.xml.nsPrefix+"e[@xi="+this.Row+"]");
}
return this.DataNode;
};
nitobi.grid.Row.prototype.getStyle=function(){
return this.DomNode.style;
};
nitobi.grid.Row.prototype.getCell=function(_406){
return this.grid.getCellObject(this.row,_406);
};
nitobi.grid.Row.prototype.getKey=function(_407){
return this.grid.getCellObject(this.row,_407);
};
nitobi.grid.Row.getRowElement=function(grid,row){
return nitobi.grid.Row.getRowElements(grid,row).mid;
};
nitobi.grid.Row.getRowElements=function(grid,row){
var _40c=grid.getFrozenLeftColumnCount();
if(!_40c){
return {left:null,mid:$("row_"+row+"_"+grid.uid)};
}
var C=nitobi.grid.Cell;
var rows={};
rows.left=C.getCellElement(grid,row,0).parentNode;
var cell=C.getCellElement(grid,row,_40c);
rows.mid=cell?cell.parentNode:null;
return rows;
};
nitobi.grid.Row.getRowNumber=function(_410){
return parseInt(_410.getAttribute("xi"));
};
nitobi.grid.Row.prototype.xGETMETA=function(){
var node=this.MetaNode;
node=node.selectSingleNode("@"+arguments[0]);
if(node!=null){
return node.value;
}
};
nitobi.grid.Row.prototype.xSETMETA=function(){
var node=this.MetaNode;
if(null==node){
var meta=this.grid.data.selectSingleNode("//root/gridmeta");
var _414=this.MetaNode=this.grid.data.createNode(1,"r","");
_414.setAttribute("xi",this.row);
meta.appendChild(_414);
node=this.MetaNode=_414;
}
if(node!=null){
node.setAttribute(arguments[0],arguments[1][0]);
}else{
alert("Cannot set property: "+arguments[0]);
}
};
nitobi.grid.RowRenderer=function(_415,_416,_417,_418,_419,_41a){
this.rowHeight=_417;
this.xmlDataSource=_415;
this.dataTableId="";
this.firstColumn=_418;
this.columns=_419;
this.firstColumn=_418;
this.uniqueId=_41a;
this.mergeDoc=nitobi.xml.createXmlDoc("<ntb:root xmlns:ntb=\"http://www.nitobi.com\"><ntb:columns><ntb:stub/></ntb:columns><ntb:data><ntb:stub/></ntb:data></ntb:root>");
this.mergeDocCols=this.mergeDoc.selectSingleNode("//ntb:columns");
this.mergeDocData=this.mergeDoc.selectSingleNode("//ntb:data");
};
nitobi.grid.RowRenderer.prototype.render=function(_41b,rows,_41d,_41e,_41f,_420){
var _41b=Number(_41b)||0;
var rows=Number(rows)||0;
var xt=nitobi.grid.rowXslProc;
xt.addParameter("start",_41b,"");
xt.addParameter("end",_41b+rows,"");
xt.addParameter("sortColumn",_41f,"");
xt.addParameter("sortDirection",_420,"");
xt.addParameter("dataTableId",this.dataTableId,"");
xt.addParameter("showHeaders",this.showHeaders+0,"");
xt.addParameter("firstColumn",this.firstColumn,"");
xt.addParameter("lastColumn",this.lastColumn,"");
xt.addParameter("uniqueId",this.uniqueId,"");
xt.addParameter("rowHover",this.rowHover,"");
xt.addParameter("frozenColumnId",this.frozenColumnId,"");
xt.addParameter("toolTipsEnabled",this.toolTipsEnabled,"");
var data=this.xmlDataSource.xmlDoc();
if(data.documentElement.firstChild==null){
return "";
}
var root=this.mergeDoc;
this.mergeDocCols.replaceChild((!nitobi.browser.IE?root.importNode(this.definitions,true):this.definitions.cloneNode(true)),this.mergeDocCols.firstChild);
this.mergeDocData.replaceChild((!nitobi.browser.IE?root.importNode(data.documentElement,true):data.documentElement.cloneNode(true)),this.mergeDocData.firstChild);
s2=nitobi.xml.transformToString(root,xt,"xml");
s2=s2.replace(/ATOKENTOREPLACE/g,"&nbsp;");
s2=s2.replace(/\#\&lt\;\#/g,"<").replace(/\#\&gt\;\#/g,">").replace(/\#\&eq\;\#/g,"=").replace(/\#\&quot\;\#/g,"\"").replace(/\#\&amp\;\#/g,"&");
return s2;
};
nitobi.grid.RowRenderer.prototype.generateXslTemplate=function(_424,_425,_426,_427,_428,_429,_42a,_42b,id){
this.definitions=_424;
this.showIndicators=_429;
this.showHeaders=_428;
this.firstColumn=_426;
this.lastColumn=_426+_427;
this.rowHover=_42a;
this.frozenColumnId=(id?id:"");
this.toolTipsEnabled=_42b;
return;
try{
var path=(typeof (gApplicationPath)=="undefined"?window.location.href.substr(0,window.location.href.lastIndexOf("/")+1):gApplicationPath);
var imp=this.xmlTemplate.selectNodes("//xsl:import");
for(var i=0;i<imp.length;i++){
imp[i].setAttribute("href",path+"xsl/"+imp[i].getAttribute("href"));
}
}
catch(e){
}
};
nitobi.grid.RowRenderer.prototype.dispose=function(){
this.xslTemplate=null;
this.xmlDataSource=null;
};
EBAScroller_RENDERTIMEOUT=100;
EBAScroller_VIEWPANES=new Array("topleft","topcenter","midleft","midcenter");
nitobi.grid.Scroller3x3=function(_430,_431,rows,_433,_434,_435){
this.disposal=[];
this.height=_431;
this.rows=rows;
this.columns=_433;
this.freezetop=_434;
this.freezeleft=_435;
this.lastScrollTop=-1;
this.uid=nitobi.base.getUid();
this.onRenderComplete=new nitobi.base.Event();
this.onRangeUpdate=new nitobi.base.Event();
this.onHtmlReady=new nitobi.base.Event();
this.owner=_430;
var VP=nitobi.grid.Viewport;
this.view={topleft:new VP(this.owner,0),topcenter:new VP(this.owner,1),midleft:new VP(this.owner,3),midcenter:new VP(this.owner,4)};
this.view.midleft.onHtmlReady.subscribe(this.handleHtmlReady,this);
this.setCellRanges();
this.scrollSurface=null;
this.startRow=_434;
this.headerHeight=23;
this.rowHeight=23;
this.lastTimeoutId=0;
this.scrollTopPercent=0;
this.dataTable=null;
this.cacheMap=new nitobi.collections.CacheMap(-1,-1);
};
nitobi.grid.Scroller3x3.prototype.updateCellRanges=function(cols,rows,frzL,frzT){
this.columns=cols;
this.rows=rows;
this.freezetop=frzT;
this.freezeleft=frzL;
this.setCellRanges();
};
nitobi.grid.Scroller3x3.prototype.setCellRanges=function(){
var _43b=null;
if(this.implementsStandardPaging()){
_43b=this.getDisplayedRowCount();
}
this.view.topleft.setCellRanges(0,this.freezetop,0,this.freezeleft);
this.view.topcenter.setCellRanges(0,this.freezetop,this.freezeleft,this.columns-this.freezeleft);
this.view.midleft.setCellRanges(this.freezetop,(_43b?_43b:this.rows)-this.freezetop,0,this.freezeleft);
this.view.midcenter.setCellRanges(this.freezetop,(_43b?_43b:this.rows)-this.freezetop,this.freezeleft,this.columns-this.freezeleft);
};
nitobi.grid.Scroller3x3.prototype.resize=function(_43c){
this.height=_43c;
};
nitobi.grid.Scroller3x3.prototype.setScrollLeftRelative=function(_43d){
this.setScrollLeft(this.scrollLeft+_43d);
};
nitobi.grid.Scroller3x3.prototype.setScrollLeftPercent=function(_43e){
this.setScrollLeft(Math.round((this.view.midcenter.element.scrollWidth-this.view.midcenter.element.clientWidth)*_43e));
};
nitobi.grid.Scroller3x3.prototype.setScrollLeft=function(_43f){
this.view.midcenter.element.scrollLeft=_43f;
this.view.topcenter.element.scrollLeft=_43f;
};
nitobi.grid.Scroller3x3.prototype.getScrollLeft=function(){
return this.scrollSurface.scrollLeft;
};
nitobi.grid.Scroller3x3.prototype.setScrollTopRelative=function(_440){
this.setScrollTop(this.getScrollTop()+_440);
};
nitobi.grid.Scroller3x3.prototype.setScrollTopPercent=function(_441){
this.setScrollTop(Math.round((this.view.midcenter.element.scrollHeight-this.view.midcenter.element.clientHeight)*_441));
};
nitobi.grid.Scroller3x3.prototype.getScrollTopPercent=function(){
return this.scrollSurface.scrollTop/(this.view.midcenter.element.scrollHeight-this.view.midcenter.element.clientHeight);
};
nitobi.grid.Scroller3x3.prototype.setScrollTop=function(_442){
this.view.midcenter.element.scrollTop=_442;
this.view.midleft.element.scrollTop=_442;
this.render();
};
nitobi.grid.Scroller3x3.prototype.getScrollTop=function(){
return this.scrollSurface.scrollTop;
};
nitobi.grid.Scroller3x3.prototype.clearSurfaces=function(_443,_444,_445,_446){
this.flushCache();
_445=true;
if(_443){
_444=true;
_445=true;
_446=true;
}
if(_444){
this.view.topleft.clear(true);
this.view.topcenter.clear(true);
}
if(_445){
this.view.midleft.clear(true,true,false,false);
this.view.midcenter.clear(false,false,true);
}
if(_446){
}
};
nitobi.grid.Scroller3x3.prototype.mapToHtml=function(_447){
var uid=this.owner.uid;
for(var i=0;i<4;i++){
var node=$("gridvp_"+i+"_"+uid);
this.view[EBAScroller_VIEWPANES[i]].mapToHtml(node,nitobi.html.getFirstChild(node),null);
}
this.scrollSurface=$("gridvp_3_"+uid);
};
nitobi.grid.Scroller3x3.prototype.getUnrenderedBlocks=function(){
var pair={first:this.freezetop,last:this.rows-1-this.freezetop};
if(!this.implementsShowAll()){
var _44c=this.getScrollTop()+this.getTop()-this.headerHeight;
var MC=this.view.midcenter;
var b0=MC.findBlockAtCoord(_44c);
var b1=MC.findBlockAtCoord(_44c+this.height);
var _450=null;
var _451=null;
if(b0==null){
return;
}
_450=b0.top+Math.floor((_44c-b0.offsetTop)/this.rowHeight);
if(b1){
_451=b1.top+Math.floor((_44c+this.height-b1.offsetTop)/this.rowHeight);
}else{
_451=_450+Math.floor(this.height/this.rowHeight);
}
_451=Math.min(_451,this.rows);
if(this.implementsStandardPaging()){
var _452=0;
if(this.owner.getRenderMode()==nitobi.grid.RENDERMODE_ONDEMAND){
var _453=_450+_452;
var last=Math.min(_451+_452,_452+this.getDisplayedRowCount()-1);
pair={first:_453,last:last};
}else{
var _453=_452;
var last=_453+this.getDisplayedRowCount()-1;
pair={first:_453,last:last};
}
}else{
pair={first:_450,last:_451};
}
this.onRangeUpdate.notify(pair);
}
return pair;
};
nitobi.grid.Scroller3x3.prototype.render=function(_455){
if(this.owner.isBound()&&(this.getScrollTop()!=this.lastScrollTop||_455||this.scrollTopPercent>0.9)){
var _456=nitobi.lang.close(this,this.performRender,[]);
window.clearTimeout(this.lastTimeoutId);
this.lastTimeoutId=window.setTimeout(_456,EBAScroller_RENDERTIMEOUT);
}
};
nitobi.grid.Scroller3x3.prototype.performRender=function(){
var _457=this.getUnrenderedBlocks();
if(_457==null){
return;
}
var _458=this.getScrollTop();
var mc=this.view.midcenter;
var ml=this.view.midleft;
var _45b=this.getDataTable();
var _45c=_457.first;
var last=_457.last;
if(last>=_45b.remoteRowCount-1&&!_45b.rowCountKnown){
last+=2;
}
var gaps=this.cacheMap.gaps(_45c,last);
var _45f=(this.owner.mode=="livescrolling"?(_45c+last<=0):(_45c+last<=-1));
if(_45f){
this.onHtmlReady.notify();
}else{
if(gaps[0]!=null){
var low=gaps[0].low;
var high=gaps[0].high;
var rows=high-low+1;
if(!_45b.inCache(low,rows)){
if(low==null||rows==null){
alert("low or rows =null");
}
if(this.implementsStandardPaging()){
var _463=this.getCurrentPageIndex()*this.getRowsPerPage();
var _464=_463+this.getRowsPerPage();
_45b.get(_463,_464);
}else{
_45b.get(low,rows);
}
var _465=_45b.cachedRanges(low,high);
for(var i=0;i<_465.length;i++){
var _467=this.cacheMap.gaps(_465[i].low,_465[i].high);
for(var j=0;j<_467.length;j++){
_457.first=_467[j].low;
_457.last=_467[j].high;
this.renderGap(_467[j].low,_467[j].high);
}
}
return false;
}else{
this.renderGap(low,high);
}
}
}
this.onRenderComplete.notify();
};
nitobi.grid.Scroller3x3.prototype.renderGap=function(low,high){
var gaps=this.cacheMap.gaps(low,high);
var mc=this.view.midcenter;
var ml=this.view.midleft;
if(gaps[0]!=null){
var low=gaps[0].low;
var high=gaps[0].high;
var rows=high-low+1;
this.cacheMap.insert(low,high);
mc.renderGap(low,high);
ml.renderGap(low,high);
}
};
nitobi.grid.Scroller3x3.prototype.flushCache=function(){
if(Boolean(this.cacheMap)){
this.cacheMap.flush();
}
};
nitobi.grid.Scroller3x3.prototype.reRender=function(_46f,_470){
var _471=this.view.midleft.clearBlocks(_46f,_470);
this.view.midcenter.clearBlocks(_46f,_470);
this.cacheMap.remove(_471.top,_471.bottom);
this.render();
};
nitobi.grid.Scroller3x3.prototype.getViewportByCoords=function(row,_473){
var _474=0;
if(row>=_474&&row<this.owner.getfreezetop()&&_473>=0&&_473<this.owner.frozenLeftColumnCount()){
return this.view.topleft;
}
if(row>=_474&&row<this.owner.getfreezetop()&&_473>=this.owner.getFrozenLeftColumnCount()&&_473<this.owner.getColumnCount()){
return this.view.topcenter;
}
if(row>=this.owner.getfreezetop()+_474&&row<this.owner.getDisplayedRowCount()+_474&&_473>=0&&_473<this.owner.getFrozenLeftColumnCount()){
return this.view.midleft;
}
if(row>=this.owner.getfreezetop()+_474&&row<this.owner.getDisplayedRowCount()+_474&&_473>=this.owner.getFrozenLeftColumnCount()&&_473<this.owner.getColumnCount()){
return this.view.midcenter;
}
};
nitobi.grid.Scroller3x3.prototype.getRowsPerPage=function(){
return this.owner.getRowsPerPage();
};
nitobi.grid.Scroller3x3.prototype.getDisplayedRowCount=function(){
return this.owner.getDisplayedRowCount();
};
nitobi.grid.Scroller3x3.prototype.getCurrentPageIndex=function(){
return this.owner.getCurrentPageIndex();
};
nitobi.grid.Scroller3x3.prototype.implementsStandardPaging=function(){
return Boolean(this.owner.getPagingMode().toLowerCase()=="standard");
};
nitobi.grid.Scroller3x3.prototype.implementsShowAll=function(){
return Boolean(this.owner.getPagingMode().toLowerCase()==nitobi.grid.PAGINGMODE_NONE);
};
nitobi.grid.Scroller3x3.prototype.setDataTable=function(_475){
this.dataTable=_475;
};
nitobi.grid.Scroller3x3.prototype.getDataTable=function(){
return this.dataTable;
};
nitobi.grid.Scroller3x3.prototype.handleHtmlReady=function(){
this.onHtmlReady.notify();
};
nitobi.grid.Scroller3x3.prototype.getTop=function(){
return this.freezetop*this.rowHeight+this.headerHeight;
};
nitobi.grid.Scroller3x3.prototype.setSort=function(col,dir){
this.view.topleft.setSort(col,dir);
this.view.topcenter.setSort(col,dir);
this.view.midleft.setSort(col,dir);
this.view.midcenter.setSort(col,dir);
};
nitobi.grid.Scroller3x3.prototype.setRowHeight=function(_478){
this.rowHeight=_478;
this.setViewportProperty("RowHeight",_478);
};
nitobi.grid.Scroller3x3.prototype.setHeaderHeight=function(_479){
this.headerHeight=_479;
this.setViewportProperty("HeaderHeight",_479);
};
nitobi.grid.Scroller3x3.prototype.setViewportProperty=function(_47a,_47b){
var sv=this.view;
for(var i=0;i<EBAScroller_VIEWPANES.length;i++){
sv[EBAScroller_VIEWPANES[i]]["set"+_47a](_47b);
}
};
nitobi.grid.Scroller3x3.prototype.fire=function(evt,args){
return nitobi.event.notify(evt+this.uid,args);
};
nitobi.grid.Scroller3x3.prototype.subscribe=function(evt,func,_482){
if(typeof (_482)=="undefined"){
_482=this;
}
return nitobi.event.subscribe(evt+this.uid,nitobi.lang.close(_482,func));
};
nitobi.grid.Scroller3x3.prototype.dispose=function(){
try{
(this.cacheMap!=null?this.cacheMap.flush():"");
this.cacheMap=null;
var _483=this.disposal.length;
for(var i=0;i<_483;i++){
if(typeof (this.disposal[i])=="function"){
this.disposal[i].call(this);
}
this.disposal[i]=null;
}
for(var v in this.view){
this.view[v].dispose();
}
for(var item in this){
if(this[item]!=null&&this[item].dispose instanceof Function){
this[item].dispose();
}
}
}
catch(e){
}
};
nitobi.grid.Selection=function(_487,_488){
nitobi.grid.Selection.baseConstructor.call(this,_487);
this.owner=_487;
var t=new Date();
this.selecting=false;
this.expanding=false;
this.resizingRow=false;
this.created=false;
this.freezeTop=this.owner.getfreezetop();
this.freezeLeft=this.owner.getFrozenLeftColumnCount();
this.rowHeight=23;
this.onAfterExpand=new nitobi.base.Event();
this.onBeforeExpand=new nitobi.base.Event();
this.onMouseUp=new nitobi.base.Event();
this.expandEndCell=null;
this.expandStartCell=null;
this.dragFillEnabled=_488||false;
};
nitobi.lang.extend(nitobi.grid.Selection,nitobi.collections.CellSet);
nitobi.grid.Selection.prototype.setRange=function(_48a,_48b,_48c,_48d){
nitobi.grid.Selection.base.setRange.call(this,_48a,_48b,_48c,_48d);
this.startCell=this.owner.getCellElement(_48a,_48b);
this.endCell=this.owner.getCellElement(_48c,_48d);
};
nitobi.grid.Selection.prototype.setRangeWithDomNodes=function(_48e,_48f){
this.setRange(nitobi.grid.Cell.getRowNumber(_48e),nitobi.grid.Cell.getColumnNumber(_48e),nitobi.grid.Cell.getRowNumber(_48f),nitobi.grid.Cell.getColumnNumber(_48f));
};
nitobi.grid.Selection.prototype.createBoxes=function(){
if(!this.created){
var uid=this.owner.uid;
var H=nitobi.html;
var _492=H.createElement("div",{"class":"ntb-grid-selection-grabby"});
this.expanderGrabbyEvents=[{type:"mousedown",handler:this.handleGrabbyMouseDown},{type:"mouseup",handler:this.handleGrabbyMouseUp},{type:"click",handler:this.handleGrabbyClick}];
H.attachEvents(_492,this.expanderGrabbyEvents,this);
this.boxexpanderGrabby=_492;
this.box=this.createBox("selectbox"+uid);
this.boxl=this.createBox("selectboxl"+uid);
this.events=[{type:"mousemove",handler:this.shrink},{type:"mouseup",handler:this.handleSelectionMouseUp},{type:"mousedown",handler:this.handleSelectionMouseDown},{type:"click",handler:this.handleSelectionClick},{type:"dblclick",handler:this.handleDblClick}];
H.attachEvents(this.box,this.events,this);
H.attachEvents(this.boxl,this.events,this);
var sv=this.owner.Scroller.view;
sv.midcenter.surface.appendChild(this.box);
sv.midleft.surface.appendChild(this.boxl);
this.clear();
this.created=true;
}
};
nitobi.grid.Selection.prototype.createBox=function(id){
var _495;
var cell;
if(nitobi.browser.IE){
cell=_495=document.createElement("div");
}else{
_495=nitobi.html.createTable({"cellpadding":0,"cellspacing":0,"border":0},{"backgroundColor":"transparent"});
cell=_495.rows[0].cells[0];
}
_495.className="ntb-grid-selection ntb-grid-selection-border";
_495.setAttribute("id","ntb-grid-selection-"+id);
var _497=nitobi.html.createElement("div",{"id":id,"class":"ntb-grid-selection-background"});
cell.appendChild(_497);
return _495;
};
nitobi.grid.Selection.prototype.clearBoxes=function(){
if(this.box!=null){
this.clearBox(this.box);
}
if(this.boxl!=null){
this.clearBox(this.boxl);
}
this.created=false;
delete this.box;
delete this.boxl;
this.box=null;
this.boxl=null;
};
nitobi.grid.Selection.prototype.clearBox=function(box){
nitobi.html.detachEvents(box,this.events);
if(box.parentNode!=null){
box.parentNode.removeChild(box);
}
box=null;
};
nitobi.grid.Selection.prototype.handleGrabbyMouseDown=function(evt){
this.selecting=true;
this.setExpanding(true,"vert");
var _49a=this.getTopLeftCell();
var _49b=this.getBottomRightCell();
this.expandStartCell=_49a;
this.expandEndCell=_49b;
var _49c=this.owner.getScrollSurface();
this.expandStartCoords=this.box.getBoundingClientRect(_49c.scrollTop+document.body.scrollTop,_49c.scrollLeft+document.body.scrollLeft);
this.expandStartHeight=Math.abs(_49a.getRow()-_49b.getRow())+1;
this.expandStartWidth=Math.abs(_49a.getColumn()-_49b.getColumn())+1;
this.expandStartTopRow=_49a.getRow();
this.expandStartBottomRow=_49b.getRow();
this.expandStartLeftColumn=_49a.getColumn();
this.expandStartRightColumn=_49b.getColumn();
var Cell=nitobi.grid.Cell;
if(Cell.getRowNumber(this.startCell)>Cell.getRowNumber(this.endCell)){
var _49e=this.startCell;
this.startCell=this.endCell;
this.endCell=_49e;
}
this.onBeforeExpand.notify(this);
};
nitobi.grid.Selection.prototype.handleGrabbyMouseUp=function(evt){
if(this.expanding){
this.selecting=false;
this.setExpanding(false);
this.onAfterExpand.notify(this);
}
};
nitobi.grid.Selection.prototype.handleGrabbyClick=function(evt){
};
nitobi.grid.Selection.prototype.expand=function(cell,dir){
this.setExpanding(true,dir);
var Cell=nitobi.grid.Cell;
var _4a4;
var _4a5=this.expandStartTopRow,_4a6=this.expandStartLeftColumn;
var _4a7=this.expandStartBottomRow,_4a8=this.expandStartRightColumn;
var _4a9=Cell.getRowNumber(this.endCell),_4aa=Cell.getColumnNumber(this.endCell);
var _4ab=Cell.getRowNumber(this.startCell),_4ac=Cell.getColumnNumber(this.startCell);
var _4ad=Cell.getColumnNumber(cell);
var _4ae=Cell.getRowNumber(cell);
var _4af=_4ac,_4b0=_4ab;
var o=this.owner;
if(dir=="horiz"){
if(_4ac<_4aa&_4ad<_4ac){
this.changeEndCellWithDomNode(o.getCellElement(_4a7,_4ad));
this.changeStartCellWithDomNode(o.getCellElement(_4a5,_4a8));
}else{
if(_4ac>_4aa&&_4ad>_4ac){
this.changeEndCellWithDomNode(o.getCellElement(_4a7,_4ad));
this.changeStartCellWithDomNode(o.getCellElement(_4a5,_4a6));
}else{
this.changeEndCellWithDomNode(o.getCellElement((_4ab==_4a7?_4a5:_4a7),_4ad));
}
}
}else{
if(_4ab<_4a9&_4ae<_4ab){
this.changeEndCellWithDomNode(o.getCellElement(_4ae,_4a8));
this.changeStartCellWithDomNode(o.getCellElement(_4a7,_4a6));
}else{
if(_4ab>_4a9&&_4ae>_4ab){
this.changeEndCellWithDomNode(o.getCellElement(_4ae,_4a8));
this.changeStartCellWithDomNode(o.getCellElement(_4a5,_4a6));
}else{
this.changeEndCellWithDomNode(o.getCellElement(_4ae,(_4ac==_4a8?_4a6:_4a8)));
}
}
}
this.alignBoxes();
};
nitobi.grid.Selection.prototype.shrink=function(evt){
if(nitobi.html.Css.hasClass(evt.srcElement,"ntb-grid-selection-border")||nitobi.html.Css.hasClass(evt.srcElement,"ntb-grid-selection-grabby")){
return;
}
if(this.endCell!=this.startCell&&this.selecting){
var _4b3=this.owner.getScrollSurface();
var Cell=nitobi.grid.Cell;
var _4b5=Cell.getRowNumber(this.endCell),_4b6=Cell.getColumnNumber(this.endCell);
var _4b7=Cell.getRowNumber(this.startCell),_4b8=Cell.getColumnNumber(this.startCell);
var _4b9=nitobi.html.getEventCoords(evt);
var evtY=_4b9.y,evtX=_4b9.x;
if(nitobi.browser.IE||document.compatMode=="BackCompat"){
evtY=evt.clientY,evtX=evt.clientX;
}
var _4bc=this.endCell.getBoundingClientRect(_4b3.scrollTop+document.body.scrollTop,_4b3.scrollLeft+document.body.scrollLeft);
var _4bd=_4bc.top,_4be=_4bc.left;
if(_4b5>_4b7&&evtY<_4bd){
_4b5=_4b5-Math.floor(((_4bd-4)-evtY)/this.rowHeight)-1;
}else{
if(evtY>_4bc.bottom){
_4b5=_4b5+Math.floor((evtY-_4bd)/this.rowHeight);
}
}
if(_4b6>_4b8&&evtX<_4be){
_4b6--;
}else{
if(evtX>_4bc.right){
_4b6++;
}
}
if(this.expanding){
var _4bf=this.expandStartCell.getRow(),_4c0=this.expandStartCell.getColumn();
var _4c1=this.expandEndCell.getRow(),_4c2=this.expandEndCell.getColumn();
if(_4b6>=this.expandStartLeftColumn&&_4b6<=this.expandStartRightColumn){
if(_4b6>=_4b8&&_4b6<_4c2){
_4b6=_4c2;
}else{
if(_4b6<=_4b8&&_4b6>_4c0){
_4b6=_4c0;
}
}
if(_4b6>=_4b8&&_4b6<=this.expandStartRightColumn){
_4b6=this.expandStartRightColumn;
}
}
if(_4b5>=this.expandStartTopRow&&_4b5<=this.expandStartBottomRow){
if(_4b7<_4b5&&_4b5<=_4c1){
_4b5=_4c1;
}else{
if(_4b7>_4b5&&_4b5>=_4bf){
_4b5=_4bf;
}else{
if(_4b7==_4b5){
_4b5=(_4b7==_4bf?_4c1:_4bf);
}
}
}
}
}
var _4c3=this.owner.getCellElement(_4b5,_4b6);
var _4c4=this.owner.getCellElement(_4b7,_4b8);
if(_4c3!=null&&_4c3!=this.endCell||_4c4!=null&&_4c4!=this.startCell){
this.changeEndCellWithDomNode(_4c3);
this.changeStartCellWithDomNode(_4c4);
this.alignBoxes();
this.owner.ensureCellInView(_4c3);
}
}
};
nitobi.grid.Selection.prototype.getHeight=function(){
var rect=this.box.getBoundingClientRect();
return rect.top-rect.bottom;
};
nitobi.grid.Selection.prototype.collapse=function(cell){
if(!cell){
cell=this.startCell;
}
if(!cell){
return;
}
this.setRangeWithDomNodes(cell,cell);
if((this.box==null)||(this.box.parentNode==null)||(this.boxl==null)||(this.boxl.parentNode==null)){
this.created=false;
this.createBoxes();
}
this.alignBoxes();
this.selecting=false;
};
nitobi.grid.Selection.prototype.startSelecting=function(_4c7,_4c8){
this.selecting=true;
this.setRangeWithDomNodes(_4c7,_4c8);
this.shrink();
};
nitobi.grid.Selection.prototype.clearSelection=function(cell){
this.collapse(cell);
};
nitobi.grid.Selection.prototype.resizeSelection=function(cell){
this.endCell=cell;
this.shrink();
};
nitobi.grid.Selection.prototype.moveSelection=function(cell){
this.collapse(cell);
};
nitobi.grid.Selection.prototype.alignBoxes=function(){
var _4cc=this.endCell||this.startCell;
var sc=this.getCoords();
var _4ce=sc.top.y;
var _4cf=sc.top.x;
var _4d0=sc.bottom.y;
var _4d1=sc.bottom.x;
var _4d2=nitobi.lang.isStandards();
var ox=oy=(nitobi.browser.IE?-1:0);
var ow=oh=(nitobi.browser.IE&&_4d2?-1:1);
if(nitobi.browser.SAFARI){
oy=ox=-1;
if(_4d2){
oh=ow=-1;
}
}
if(_4d1>=this.freezeLeft&&_4d0>=this.freezeTop){
var e=this.box;
e.style.display="block";
this.align(e,this.startCell,_4cc,286265344,oh,ow,oy,ox);
if(this.dragFillEnabled){
(e.rows!=null?e.rows[0].cells[0]:e).appendChild(this.boxexpanderGrabby);
}
}else{
this.box.style.display="none";
}
if(_4d1<this.freezeLeft||_4cf<this.freezeLeft){
var e=this.boxl;
e.style.display="block";
this.align(e,this.startCell,_4cc,286265344,oh,ow,oy,ox);
if(this.box.style.display=="none"){
if(this.dragFillEnabled){
(e.rows!=null?e.rows[0].cells[0]:e).appendChild(this.boxexpanderGrabby);
}
}
}else{
this.boxl.style.display="none";
}
};
nitobi.grid.Selection.prototype.redraw=function(cell){
if(!this.selecting){
this.setRangeWithDomNodes(cell,cell);
}else{
this.changeEndCellWithDomNode(cell);
}
this.alignBoxes();
};
nitobi.grid.Selection.prototype.changeStartCellWithDomNode=function(cell){
this.startCell=cell;
var Cell=nitobi.grid.Cell;
this.changeStartCell(Cell.getRowNumber(cell),Cell.getColumnNumber(cell));
};
nitobi.grid.Selection.prototype.changeEndCellWithDomNode=function(cell){
this.endCell=cell;
var Cell=nitobi.grid.Cell;
this.changeEndCell(Cell.getRowNumber(cell),Cell.getColumnNumber(cell));
};
nitobi.grid.Selection.prototype.init=function(cell){
this.createBoxes();
var t=new Date();
this.selecting=true;
this.setRangeWithDomNodes(cell,cell);
};
nitobi.grid.Selection.prototype.clear=function(){
if(!this.box){
return;
}
var bs=this.box.style;
bs.display="none";
bs.top="-1000px";
bs.left="-1000px";
bs.width="1px";
bs.height="1px";
var bls=this.boxl.style;
bls.display="none";
bls.top="-1000px";
bls.left="-1000px";
bls.width="1px";
bls.height="1px";
this.selecting=false;
};
nitobi.grid.Selection.prototype.handleSelectionClick=function(evt){
if(!this.selected()){
if(NTB_SINGLECLICK==null){
if(nitobi.browser.IE){
evt=nitobi.lang.copy(evt);
}
NTB_SINGLECLICK=window.setTimeout(nitobi.lang.close(this,this.edit,[evt]),150);
}
}else{
this.collapse();
this.owner.focus();
}
};
nitobi.grid.Selection.prototype.handleDblClick=function(evt){
if(!this.selected()){
window.clearTimeout(NTB_SINGLECLICK);
NTB_SINGLECLICK=null;
if(this.owner.handleDblClick(evt)){
this.edit(evt);
}
}else{
this.collapse();
}
};
nitobi.grid.Selection.prototype.edit=function(evt){
NTB_SINGLECLICK=null;
this.owner.edit(evt);
};
nitobi.grid.Selection.prototype.select=function(_4e2,_4e3){
this.selectWithCoords(_4e2.getRowNumber(),_4e2.getColumnNumber(),_4e3.getRowNumber(),_4e3.getColumnNumber());
};
nitobi.grid.Selection.prototype.selectWithCoords=function(_4e4,_4e5,_4e6,_4e7){
this.setRange(_4e4,_4e5,_4e6,_4e7);
this.createBoxes();
this.alignBoxes();
};
nitobi.grid.Selection.prototype.handleSelectionMouseUp=function(evt){
if(this.expanding){
this.handleGrabbyMouseUp(evt);
}
this.stopSelecting();
this.onMouseUp.notify(this);
};
nitobi.grid.Selection.prototype.handleSelectionMouseDown=function(evt){
};
nitobi.grid.Selection.prototype.stopSelecting=function(){
this.selecting=true;
if(!this.selected()){
this.collapse(this.startCell);
}
this.selecting=false;
};
nitobi.grid.Selection.prototype.getStartCell=function(){
return this.startCell;
};
nitobi.grid.Selection.prototype.getEndCell=function(){
return this.endCell;
};
nitobi.grid.Selection.prototype.getTopLeftCell=function(){
var _4ea=this.getCoords();
return new nitobi.grid.Cell(this.owner,_4ea.top.y,_4ea.top.x);
};
nitobi.grid.Selection.prototype.getBottomRightCell=function(){
var _4eb=this.getCoords();
return new nitobi.grid.Cell(this.owner,_4eb.bottom.y,_4eb.bottom.x);
};
nitobi.grid.Selection.prototype.getHeight=function(){
var _4ec=this.getCoords();
return _4ec.bottom.y-_4ec.top.y+1;
};
nitobi.grid.Selection.prototype.getWidth=function(){
var _4ed=this.getCoords();
return _4ed.bottom.x-_4ed.top.x+1;
};
nitobi.grid.Selection.prototype.getRowByCoords=function(_4ee){
return (_4ee.parentNode.offsetTop/_4ee.parentNode.offsetHeight);
};
nitobi.grid.Selection.prototype.getColumnByCoords=function(_4ef){
var _4f0=(this.indicator?-2:0);
if(_4ef.parentNode.parentNode.getAttribute("id").substr(0,6)!="freeze"){
_4f0+=2-(this.freezeColumn*3);
}else{
_4f0+=2;
}
return Math.floor((_4ef.sourceIndex-_4ef.parentNode.sourceIndex-_4f0)/3);
};
nitobi.grid.Selection.prototype.selected=function(){
return (this.endCell==this.startCell)?false:true;
};
nitobi.grid.Selection.prototype.setRowHeight=function(_4f1){
this.rowHeight=_4f1;
};
nitobi.grid.Selection.prototype.getRowHeight=function(){
return this.rowHeight;
};
nitobi.grid.Selection.prototype.setExpanding=function(val,dir){
if(val&&this.expanding){
return;
}
this.expanding=val;
this.expandingVertical=(dir=="horiz"?false:true);
var C=nitobi.html.Css;
var _4f5="ntb-grid-selection-border";
var _4f6=_4f5+"-active";
if(val){
C.swapClass(this.box,_4f5,_4f6);
C.swapClass(this.boxl,_4f5,_4f6);
}else{
C.swapClass(this.box,_4f6,_4f5);
C.swapClass(this.boxl,_4f6,_4f5);
}
};
nitobi.grid.Selection.prototype.dispose=function(){
};
nitobi.grid.Selection.prototype.align=function(_4f7,_4f8,_4f9,_4fa,oh,ow,oy,ox,show){
oh=oh||0;
ow=ow||0;
oy=oy||0;
ox=ox||0;
var a=_4fa;
var td,sd,tt,tb,tl,tr,th,tw,st,sb,sl,sr,sh,sw;
if(!_4f8||!(_4f8.getBoundingClientRect)){
return;
}
ad=_4f8.getBoundingClientRect();
bd=_4f9.getBoundingClientRect();
sd=_4f7.getBoundingClientRect();
at=ad.top;
ab=ad.bottom;
al=ad.left;
ar=ad.right;
bt=bd.top;
bb=bd.bottom;
bl=bd.left;
br=bd.right;
tt=ad.top;
tb=bd.bottom;
tl=ad.left;
tr=bd.right;
th=Math.abs(tb-tt);
tw=Math.abs(tr-tl);
st=sd.top;
sb=sd.bottom;
sl=sd.left;
sr=sd.right;
sh=Math.abs(sb-st);
sw=Math.abs(sr-sl);
var H=nitobi.html;
if(a&268435456){
_4f7.style.height=(Math.max(bb-at,ab-bt)+oh)+"px";
}
if(a&16777216){
_4f7.style.width=(Math.max(br-al,ar-bl)+ow)+"px";
}
if(a&1048576){
_4f7.style.top=(H.getStyleTop(_4f7)+Math.min(tt,bt)-st+oy)+"px";
}
if(a&65536){
_4f7.style.top=(H.getStyleTop(_4f7)+tt-st+th-sh+oy)+"px";
}
if(a&4096){
_4f7.style.left=(H.getStyleLeft(_4f7)-sl+Math.min(tl,bl)+ox)+"px";
}
if(a&256){
_4f7.style.left=(H.getStyleLeft(_4f7)-sl+tl+tw-sw+ox)+"px";
}
if(a&16){
_4f7.style.top=(H.getStyleTop(_4f7)+tt-st+oy+Math.floor((th-sh)/2))+"px";
}
if(a&1){
_4f7.style.left=(H.getStyleLeft(_4f7)-sl+tl+ox+Math.floor((tw-sw)/2))+"px";
}
};
nitobi.grid.Surface=function(_510,_511,_512){
this.height=_511;
this.width=_510;
this.element=_512;
};
nitobi.grid.Surface.prototype.dispose=function(){
this.element=null;
};
nitobi.grid.TextColumn=function(grid,_514){
nitobi.grid.TextColumn.baseConstructor.call(this,grid,_514);
};
nitobi.lang.extend(nitobi.grid.TextColumn,nitobi.grid.Column);
nitobi.lang.defineNs("nitobi.ui");
nitobi.ui.Toolbars=function(_515){
this.uid="nitobiToolbar_"+nitobi.base.getUid();
this.toolbars={};
this.visibleToolbars=_515;
};
nitobi.ui.Toolbars.VisibleToolbars={};
nitobi.ui.Toolbars.VisibleToolbars.STANDARD=1;
nitobi.ui.Toolbars.VisibleToolbars.PAGING=1<<1;
nitobi.ui.Toolbars.prototype.initialize=function(){
this.enabled=true;
this.toolbarXml=nitobi.xml.createXmlDoc(nitobi.xml.serialize(nitobi.grid.toolbarDoc));
this.toolbarPagingXml=nitobi.xml.createXmlDoc(nitobi.xml.serialize(nitobi.grid.pagingToolbarDoc));
};
nitobi.ui.Toolbars.prototype.attachToParent=function(_516){
this.initialize();
this.container=_516;
if(this.standardToolbar==null&&this.visibleToolbars){
this.makeToolbar();
this.render();
}
};
nitobi.ui.Toolbars.prototype.setWidth=function(_517){
this.width=_517;
};
nitobi.ui.Toolbars.prototype.getWidth=function(){
return this.width;
};
nitobi.ui.Toolbars.prototype.setHeight=function(_518){
this.height=_518;
};
nitobi.ui.Toolbars.prototype.getHeight=function(){
return this.height;
};
nitobi.ui.Toolbars.prototype.setRowInsertEnabled=function(_519){
this.rowInsertEnabled=_519;
};
nitobi.ui.Toolbars.prototype.isRowInsertEnabled=function(){
return this.rowInsertEnabled;
};
nitobi.ui.Toolbars.prototype.setRowDeleteEnabled=function(_51a){
this.rowDeleteEnabled=_51a;
};
nitobi.ui.Toolbars.prototype.isRowDeleteEnabled=function(){
return this.rowDeleteEnabled;
};
nitobi.ui.Toolbars.prototype.makeToolbar=function(){
var _51b=this.findCssUrl();
this.toolbarXml.documentElement.setAttribute("id","toolbar"+this.uid);
this.toolbarXml.documentElement.setAttribute("image_directory",_51b);
var _51c=this.toolbarXml.selectNodes("/toolbar/items/*");
for(var i=0;i<_51c.length;i++){
if(_51c[i].nodeType!=8){
_51c[i].setAttribute("id",_51c[i].getAttribute("id")+this.uid);
}
}
this.standardToolbar=new nitobi.ui.Toolbar(this.toolbarXml,"toolbar"+this.uid);
this.toolbarPagingXml.documentElement.setAttribute("id","toolbarpaging"+this.uid);
this.toolbarPagingXml.documentElement.setAttribute("image_directory",_51b);
_51c=(this.toolbarPagingXml.selectNodes("/toolbar/items/*"));
for(var i=0;i<_51c.length;i++){
if(_51c[i].nodeType!=8){
_51c[i].setAttribute("id",_51c[i].getAttribute("id")+this.uid);
}
}
this.pagingToolbar=new nitobi.ui.Toolbar(this.toolbarPagingXml,"toolbarpaging"+this.uid);
};
nitobi.ui.Toolbars.prototype.getToolbar=function(id){
return eval("this."+id);
};
nitobi.ui.Toolbars.prototype.findCssUrl=function(){
var _51f=nitobi.html.Css.findParentStylesheet(".ntb-toolbar");
if(_51f==null){
_51f=nitobi.html.Css.findParentStylesheet(".ntb-grid");
if(_51f==null){
nitobi.lang.throwError("The CSS for the toolbar could not be found.  Try moving the nitobi.grid.css file to a location accessible to the browser's javascript or moving it to the top of the stylesheet list. findParentStylesheet returned "+_51f);
}
}
return nitobi.html.Css.getPath(_51f);
};
nitobi.ui.Toolbars.prototype.isToolbarEnabled=function(){
return this.enabled;
};
nitobi.ui.Toolbars.prototype.render=function(){
var _520=this.container;
_520.style.visibility="hidden";
var xsl=nitobi.ui.ToolbarXsl;
if(xsl.indexOf("xsl:stylesheet")==-1){
xsl="<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"><xsl:output method=\"xml\" version=\"4.0\" />"+xsl+"</xsl:stylesheet>";
}
var _522=nitobi.xml.createXslDoc(xsl);
var _523=nitobi.xml.transformToString(this.standardToolbar.getXml(),_522,"xml");
_520.innerHTML=_523;
_520.style.zIndex="1000";
var _524=nitobi.xml.transformToString(this.pagingToolbar.getXml(),_522,"xml");
_520.innerHTML+=_524;
_522=null;
xmlDoc=null;
this.standardToolbar.attachToTag();
this.pagingToolbar.attachToTag();
this.resize();
var _525=this;
var _526=this.standardToolbar.getUiElements();
for(eachbutton in _526){
switch(eachbutton){
case "save"+this.uid:
_526[eachbutton].onClick=function(){
_525.fire("Save");
};
break;
case "newRecord"+this.uid:
_526[eachbutton].onClick=function(){
_525.fire("InsertRow");
};
if(!this.isRowInsertEnabled()){
_526[eachbutton].disable();
}
break;
case "deleteRecord"+this.uid:
_526[eachbutton].onClick=function(){
_525.fire("DeleteRow");
};
if(!this.isRowDeleteEnabled()){
_526[eachbutton].disable();
}
break;
case "refresh"+this.uid:
_526[eachbutton].onClick=function(){
var _527=confirm("Refreshing will discard any changes you have made. Is it OK to refresh?");
form1.submit();
if(_527){
_525.fire("Refresh");
}
};
break;
default:
}
}
var _528=this.pagingToolbar.getUiElements();
var _525=this;
for(eachPbutton in _528){
switch(eachPbutton){
case "previousPage"+this.uid:
_528[eachPbutton].onClick=function(){
_525.fire("PreviousPage");
};
_528[eachPbutton].disable();
break;
case "nextPage"+this.uid:
_528[eachPbutton].onClick=function(){
_525.fire("NextPage");
};
break;
default:
}
}
if(this.visibleToolbars&nitobi.ui.Toolbars.VisibleToolbars.STANDARD){
this.standardToolbar.show();
}else{
this.standardToolbar.hide();
}
if(this.visibleToolbars&nitobi.ui.Toolbars.VisibleToolbars.PAGING){
this.pagingToolbar.show();
}else{
this.pagingToolbar.hide();
}
_520.style.visibility="visible";
};
nitobi.ui.Toolbars.prototype.resize=function(){
var _529=this.getWidth();
if(this.visibleToolbars&nitobi.ui.Toolbars.VisibleToolbars.PAGING){
_529=_529-parseInt(this.pagingToolbar.getWidth());
this.standardToolbar.setHeight(this.getHeight());
}
if(this.visibleToolbars&nitobi.ui.Toolbars.VisibleToolbars.STANDARD){
this.standardToolbar.setWidth(_529);
this.standardToolbar.setHeight(this.getHeight());
}
};
nitobi.ui.Toolbars.prototype.fire=function(evt,args){
return nitobi.event.notify(evt+this.uid,args);
};
nitobi.ui.Toolbars.prototype.subscribe=function(evt,func,_52e){
if(typeof (_52e)=="undefined"){
_52e=this;
}
return nitobi.event.subscribe(evt+this.uid,nitobi.lang.close(_52e,func));
};
nitobi.ui.Toolbars.prototype.dispose=function(){
this.toolbarXml=null;
this.toolbarPagingXml=null;
if(this.toolbar&&this.toolbar.dispose){
this.toolbar.dispose();
this.toolbar=null;
}
if(this.toolbarPaging&&this.toolbarPaging.dispose){
this.toolbarPaging.dispose();
this.toolbarPaging=null;
}
};
var EBA_SELECTION_BUFFER=15;
var NTB_SINGLECLICK=null;
nitobi.grid.Viewport=function(grid,_530){
this.disposal=[];
this.surface=null;
this.element=null;
this.rowHeight=23;
this.headerHeight=23;
this.sortColumn=0;
this.sortDir=1;
this.uid=nitobi.base.getUid();
this.region=_530;
this.scrollIncrement=0;
this.grid=grid;
this.startRow=0;
this.rows=0;
this.startColumn=0;
this.columns=0;
this.rowRenderer=null;
this.onHtmlReady=new nitobi.base.Event();
};
nitobi.grid.Viewport.prototype.mapToHtml=function(_531,_532,_533){
this.surface=_532;
this.element=_531;
this.container=nitobi.html.getFirstChild(_532);
this.makeLastBlock(0,this.grid.getRowsPerPage()*5);
};
nitobi.grid.Viewport.prototype.makeLastBlock=function(low,high){
if(this.lastEmptyBlock==null&&this.grid&&this.region>2&&this.region<5&&this.container){
if(this.container.lastChild){
low=Math.max(low,this.container.lastChild.bottom);
}
this.lastEmptyBlock=this.renderEmptyBlock(low,high);
}
};
nitobi.grid.Viewport.prototype.setCellRanges=function(_536,rows,_538,_539){
this.startRow=_536;
this.rows=rows;
this.startColumn=_538;
this.columns=_539;
this.makeLastBlock(this.startRow,this.startRow+rows-1);
if(this.lastEmptyBlock!=null&&this.region>2&&this.region<5&&this.rows>0){
var _53a=this.startRow+this.rows-1;
if(this.lastEmptyBlock.top>_53a){
this.container.removeChild(this.lastEmptyBlock);
this.lastEmptyBlock=null;
}else{
this.lastEmptyBlock.bottom=_53a;
this.lastEmptyBlock.style.height=(this.rowHeight*(this.lastEmptyBlock.bottom-this.lastEmptyBlock.top+1))+"px";
if(this.lastEmptyBlock.bottom<this.lastEmptyBlock.top){
throw "blocks are miss aligned.";
}
}
}
};
nitobi.grid.Viewport.prototype.clear=function(_53b,_53c,_53d,_53e){
var uid=this.grid.uid;
if(this.surface&&_53b){
this.surface.innerHTML="<div id=\"gridvpcontainer_"+this.region+"_"+uid+"\"></div>";
}
if(this.element&&_53e){
this.element.innerHTML="<div id=\"gridvpsurface_"+this.region+"_"+uid+"\"><div id=\"gridvpcontainer_"+this.region+"_"+uid+"\"></div></div>";
}
if(this.surface&&_53d){
this.surface.innerHTML="<div id=\"gridvpcontainer_"+this.region+"_"+uid+"\"></div>";
}
this.surface=nitobi.html.getFirstChild(this.element);
this.container=nitobi.html.getFirstChild(this.surface);
if(this.grid&&this.region>2&&this.region<5){
this.lastEmptyBlock=null;
}
this.makeLastBlock(0,this.grid.getRowsPerPage()*5);
};
nitobi.grid.Viewport.prototype.setSort=function(_540,_541){
this.sortColumn=_540;
this.sortDir=_541;
};
nitobi.grid.Viewport.prototype.renderGap=function(top,_543){
var _544=activeRow=null;
var _545=this.findBlock(top);
var o=this.renderInsideEmptyBlock(top,_543,_545);
if(o==null){
return;
}
o.setAttribute("rendered","true");
var rows=_543-top+1;
o.innerHTML=this.rowRenderer.render(top,rows,_544,activeRow,this.sortColumn,this.sortDir);
this.onHtmlReady.notify(this);
};
nitobi.grid.Viewport.prototype.findBlock=function(row){
var blk=this.container.childNodes;
for(var i=0;i<blk.length;i++){
if(row>=blk[i].top&&row<=blk[i].bottom){
return blk[i];
}
}
};
nitobi.grid.Viewport.prototype.findBlockAtCoord=function(top){
var blk=this.container.childNodes;
for(var i=0;i<blk.length;i++){
var rt=blk[i].offsetTop;
var rb=rt+blk[i].offsetHeight;
if(top>=rt&&top<=rb){
return blk[i];
}
}
};
nitobi.grid.Viewport.prototype.getBlocks=function(_550,_551){
var _552=[];
var _553=this.findBlock(_550);
var _554=_553;
_552.push(_553);
while(_551>_554.bottom){
var _555=_554.nextSibling;
if(_555!=null){
_554=_555;
}else{
break;
}
_552.push(_554);
}
return _552;
};
nitobi.grid.Viewport.prototype.clearBlocks=function(_556,_557){
var _558=this.getBlocks(_556,_557);
var len=_558.length;
var top=_558[0].top;
var _55b=_558[len-1].bottom;
var _55c=_558[len-1].nextSibling;
for(var i=0;i<len;i++){
_558[i].parentNode.removeChild(_558[i]);
}
this.renderEmptyBlock(top,_55b,_55c);
return {"top":top,"bottom":_55b};
};
nitobi.grid.Viewport.prototype.renderInsideEmptyBlock=function(top,_55f,_560){
if(_560==null){
return this.renderBlock(top,_55f);
}
if(top==_560.top&&_55f>=_560.bottom){
var _561=this.renderBlock(top,_55f,_560);
this.container.replaceChild(_561,_560);
if(_560.bottom<_560.top){
throw "Render error";
}
return _561;
}
if(top==_560.top&&_55f<_560.bottom){
_560.top=_55f+1;
_560.style.height=(this.rowHeight*(_560.bottom-_560.top+1))+"px";
_560.rows=_560.bottom-_560.top+1;
if(_560.bottom<_560.top){
throw "Render error";
}
return this.renderBlock(top,_55f,_560);
}
if(top>_560.top&&_55f>=_560.bottom){
_560.bottom=top-1;
_560.style.height=(this.rowHeight*(_560.bottom-_560.top+1))+"px";
if(_560.bottom<_560.top){
throw "Render error";
}
return this.renderBlock(top,_55f,_560.nextSibling);
}
if(top>_560.top&&_55f<_560.bottom){
var _562=this.renderEmptyBlock(_560.top,top-1,_560);
_560.top=_55f+1;
_560.style.height=(this.rowHeight*(_560.bottom-_560.top+1))+"px";
if(_560.bottom<_560.top){
throw "Render error";
}
return this.renderBlock(top,_55f,_560);
}
throw "Could not insert "+top+"-"+_55f+_560.outerHTML;
};
nitobi.grid.Viewport.prototype.renderEmptyBlock=function(top,_564,_565){
var o=this.renderBlock(top,_564,_565);
o.setAttribute("id","eba_grid_emptyblock_"+this.region+"_"+top+"_"+_564+"_"+this.grid.uid);
if(top==0&&_564==99){
crash;
}
o.setAttribute("rendered","false");
o.style.height=Math.max(((_564-top+1)*this.rowHeight),0)+"px";
return o;
};
nitobi.grid.Viewport.prototype.renderBlock=function(top,_568,_569){
var o=document.createElement("div");
o.setAttribute("id","eba_grid_block_"+this.region+"_"+top+"_"+_568+"_"+this.grid.uid);
o.top=top;
o.bottom=_568;
o.left=this.startColumn;
o.right=this.startColumn+this.columns;
o.rows=_568-top+1;
o.columns=this.columns;
if(_569){
this.container.insertBefore(o,_569);
}else{
this.container.insertBefore(o,null);
}
return o;
};
nitobi.grid.Viewport.prototype.setHeaderHeight=function(_56b){
this.headerHeight=_56b;
};
nitobi.grid.Viewport.prototype.setRowHeight=function(_56c){
this.rowHeight=_56c;
};
nitobi.grid.Viewport.prototype.dispose=function(){
this.element=null;
this.container=null;
nitobi.lang.dispose(this,this.disposal);
return;
};
nitobi.grid.Viewport.prototype.fire=function(evt,args){
return nitobi.event.notify(evt+this.uid,args);
};
nitobi.grid.Viewport.prototype.subscribe=function(evt,func,_571){
if(typeof (_571)=="undefined"){
_571=this;
}
return nitobi.event.subscribe(evt+this.uid,nitobi.lang.close(_571,func));
};
nitobi.grid.Viewport.prototype.attach=function(evt,func,_574){
return nitobi.html.attachEvent(_574,evt,nitobi.lang.close(this,func));
};
nitobi.lang.defineNs("nitobi.data");
if(false){
nitobi.data=function(){
};
}
nitobi.data.DATAMODE_UNBOUND="unbound";
nitobi.data.DATAMODE_LOCAL="local";
nitobi.data.DATAMODE_REMOTE="remote";
nitobi.data.DATAMODE_CACHING="caching";
nitobi.data.DATAMODE_STATIC="static";
nitobi.data.DATAMODE_PAGING="paging";
nitobi.data.DataSet=function(){
var _575="http://www.nitobi.com";
this.doc=nitobi.xml.createXmlDoc("<"+nitobi.xml.nsPrefix+"datasources xmlns:ntb=\""+_575+"\"></"+nitobi.xml.nsPrefix+"datasources>");
};
nitobi.data.DataSet.prototype.initialize=function(){
this.tables=new Array();
};
nitobi.data.DataSet.prototype.add=function(_576){
this.tables[_576.id]=_576;
};
nitobi.data.DataSet.prototype.getTable=function(_577){
return this.tables[_577];
};
nitobi.data.DataSet.prototype.xmlDoc=function(){
var root=this.doc.documentElement;
while(root.hasChildNodes()){
root.removeChild(root.firstChild);
}
for(var i in this.tables){
if(this.tables[i].xmlDoc&&this.tables[i].xmlDoc.documentElement){
var _57a=this.tables[i].xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource").cloneNode(true);
this.doc.selectSingleNode("/"+nitobi.xml.nsPrefix+"datasources").appendChild(nitobi.xml.importNode(this.doc,_57a,true));
}
}
return this.doc;
};
nitobi.data.DataSet.prototype.dispose=function(){
for(var _57b in this.tables){
this.tables[_57b].dispose();
}
};
nitobi.lang.defineNs("nitobi.data");
nitobi.data.DataTable=function(mode,_57d,_57e,_57f,_580){
if(_57d==null){
}
this.estimateRowCount=_57d;
this.version=3;
this.uid=nitobi.base.getUid();
this.mode=mode||"caching";
this.setAutoKeyEnabled(_580);
this.columns=new Array();
this.keys=new Array();
this.types=new Array();
this.defaults=new Array();
this.columnsConfigured=false;
this.pagingConfigured=false;
this.id="_default";
this.fieldMap={};
if(_57e){
this.saveHandlerArgs=_57e;
}else{
this.saveHandlerArgs={};
}
if(_57f){
this.getHandlerArgs=_57f;
}else{
this.getHandlerArgs={};
}
this.setGetHandlerParameter("RequestType","GET");
this.setSaveHandlerParameter("RequestType","SAVE");
this.batchInsert=false;
this.batchInsertRowCount=0;
};
nitobi.data.DataTable.DEFAULT_LOG="<"+nitobi.xml.nsPrefix+"grid "+nitobi.xml.nsDecl+"><"+nitobi.xml.nsPrefix+"datasources id='id'><"+nitobi.xml.nsPrefix+"datasource id=\"{id}\"><"+nitobi.xml.nsPrefix+"datasourcestructure /><"+nitobi.xml.nsPrefix+"data id=\"_default\"></"+nitobi.xml.nsPrefix+"data></"+nitobi.xml.nsPrefix+"datasource></"+nitobi.xml.nsPrefix+"datasources></"+nitobi.xml.nsPrefix+"grid>";
nitobi.data.DataTable.DEFAULT_DATA="<"+nitobi.xml.nsPrefix+"datasource "+nitobi.xml.nsDecl+" id=\"{id}\"><"+nitobi.xml.nsPrefix+"datasourcestructure FieldNames=\"{fields}\" Keys=\"{keys}\" types=\"{types}\" defaults=\"{defaults}\"></"+nitobi.xml.nsPrefix+"datasourcestructure><"+nitobi.xml.nsPrefix+"data id=\"{id}\"></"+nitobi.xml.nsPrefix+"data></"+nitobi.xml.nsPrefix+"datasource>";
nitobi.data.DataTable.prototype.initialize=function(_581,_582,_583,_584,_585,sort,_587,_588,_589){
this.setGetHandlerParameter("TableId",_581);
this.setSaveHandlerParameter("TableId",_581);
this.id=_581;
this.datastructure=null;
this.descriptor=new nitobi.data.DataTableDescriptor(this,nitobi.lang.close(this,this.syncRowCount),this.estimateRowCount);
this.pageFirstRow=0;
this.pageRowCount=0;
this.pageSize=_585;
this.minPageSize=10;
this.requestCache=new nitobi.collections.CacheMap(-1,-1);
this.dataCache=new nitobi.collections.CacheMap(-1,-1);
this.flush();
this.sortColumn=sort;
this.sortDir=_587||"Asc";
this.filter=new Array();
this.onGenerateKey=_588;
this.remoteRowCount=0;
this.setRowCountKnown(false);
if(_584==null){
_584=0;
}
if(this.mode!="unbound"){
if(_582!=null){
this.ajaxCallbackPool=new nitobi.ajax.HttpRequestPool(nitobi.ajax.HttpRequestPool_MAXCONNECTIONS);
this.ajaxCallbackPool.context=this;
this.setGetHandler(_582);
this.setSaveHandler(_583);
}
this.ajaxCallback=new nitobi.ajax.HttpRequest();
this.ajaxCallback.responseType="xml";
}else{
if(_582!=null&&typeof (_582)!="string"){
this.initializeXml(_582);
}
}
this.sortXslProc=nitobi.xml.createXslProcessor(nitobi.data.sortXslProc.stylesheet);
this.requestQueue=new Array();
this.async=true;
};
nitobi.data.DataTable.prototype.setOnGenerateKey=function(_58a){
this.onGenerateKey=_58a;
};
nitobi.data.DataTable.prototype.getOnGenerateKey=function(){
return this.onGenerateKey;
};
nitobi.data.DataTable.prototype.setAutoKeyEnabled=function(val){
this.autoKeyEnabled=val;
};
nitobi.data.DataTable.prototype.isAutoKeyEnabled=function(){
return this.autoKeyEnabled;
};
nitobi.data.DataTable.prototype.initializeXml=function(oXml){
this.replaceData(oXml);
var rows=this.xmlDoc.selectNodes("//"+nitobi.xml.nsPrefix+"e").length;
if(rows>0){
var s=this.xmlDoc.xml;
s=nitobi.xml.transformToString(this.xmlDoc,this.sortXslProc,"xml");
this.xmlDoc=nitobi.xml.loadXml(this.xmlDoc,s);
this.dataCache.insert(0,rows-1);
if(this.mode=="local"){
this.setRowCountKnown(true);
}
}
this.setRemoteRowCount(rows);
this.fire("DataInitalized");
};
nitobi.data.DataTable.prototype.initializeXmlData=function(oXml){
var sXml=oXml;
if(typeof (oXml)=="object"){
sXml=oXml.xml;
}
sXml=sXml.replace(/fieldnames=/g,"FieldNames=").replace(/keys=/g,"Keys=");
this.xmlDoc=nitobi.xml.loadXml(this.xmlDoc,sXml);
this.datastructure=this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"datasourcestructure");
};
nitobi.data.DataTable.prototype.replaceData=function(oXml){
this.initializeXmlData(oXml);
var _592=this.datastructure.getAttribute("FieldNames");
var keys=this.datastructure.getAttribute("Keys");
var _594=this.datastructure.getAttribute("Defaults");
var _595=this.datastructure.getAttribute("Types");
this.initializeColumns(_592,keys,_595,_594);
};
nitobi.data.DataTable.prototype.initializeSchema=function(){
var _596=this.columns.join("|");
var keys=this.keys.join("|");
var _598=this.defaults.join("|");
var _599=this.types.join("|");
this.dataCache.flush();
this.xmlDoc=nitobi.xml.loadXml(this.xmlDoc,nitobi.data.DataTable.DEFAULT_DATA.replace(/\{id\}/g,this.id).replace(/\{fields\}/g,_596).replace(/\{keys\}/g,keys).replace(/\{defaults\}/g,_598).replace(/\{types\}/g,_599));
this.datastructure=this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"datasourcestructure");
};
nitobi.data.DataTable.prototype.initializeColumns=function(_59a,keys,_59c,_59d){
if(null!=_59a){
var _59e=this.columns.join("|");
if(_59e==_59a){
return;
}
this.columns=_59a.split("|");
}
if(null!=keys){
this.keys=keys.split("|");
}
if(null!=_59c){
this.types=_59c.split("|");
}
if(null!=_59d){
this.defaults=_59d.split("|");
}
if(this.xmlDoc.documentElement==null){
this.initializeSchema();
}
this.datastructure=this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"datasourcestructure");
var ds=this.datastructure;
if(_59a){
ds.setAttribute("FieldNames",_59a);
}
if(keys){
ds.setAttribute("Keys",keys);
}
if(_59d){
ds.setAttribute("Defaults",_59d);
}
if(_59c){
ds.setAttribute("Types",_59c);
}
this.makeFieldMap();
this.fire("ColumnsInitialized");
};
nitobi.data.DataTable.prototype.getTemplateNode=function(_5a0){
var _5a1=null;
if(_5a0==null){
_5a0=this.defaults;
}
_5a1=nitobi.xml.createElement(this.xmlDoc,"e");
for(var i=0;i<this.columns.length;i++){
var _5a3=(i>25?String.fromCharCode(Math.floor(i/26)+97):"")+(String.fromCharCode(i%26+97));
if(this.defaults[i]==null){
_5a1.setAttribute(_5a3,"");
}else{
_5a1.setAttribute(_5a3,this.defaults[i]);
}
}
return _5a1;
};
nitobi.data.DataTable.prototype.commitProperties=function(){
if(this.mode=="unbound"){
}
};
nitobi.data.DataTable.prototype.flush=function(){
this.flushCache();
this.flushLog();
this.xmlDoc=nitobi.xml.createXmlDoc();
};
nitobi.data.DataTable.prototype.clearData=function(){
this.flushCache();
this.flushLog();
if(this.xmlDoc){
var _5a4=this.xmlDoc.selectSingleNode("//ntb:data");
nitobi.xml.removeChildren(_5a4);
}
};
nitobi.data.DataTable.prototype.flushCache=function(){
if(this.mode=="caching"||this.mode=="paging"){
this.dataCache.flush();
}
if(this.mode!="unbound"){
this.requestCache.flush();
}
};
nitobi.data.DataTable.prototype.join=function(_5a5,_5a6,_5a7,_5a8){
};
nitobi.data.DataTable.prototype.merge=function(xd){
};
nitobi.data.DataTable.prototype.getField=function(_5aa,_5ab){
var r=this.getRecord(_5aa);
var a=this.fieldMap[_5ab];
if(a&&r){
return r.getAttribute(a.substring(1));
}else{
return null;
}
};
nitobi.data.DataTable.prototype.getRecord=function(_5ae){
var data=this.xmlDoc.selectNodes("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data/"+nitobi.xml.nsPrefix+"e[@xi='"+_5ae+"']");
if(data.length==0){
return null;
}
return data[0];
};
nitobi.data.DataTable.prototype.beginBatchInsert=function(){
this.batchInsert=true;
this.batchInsertRowCount=0;
};
nitobi.data.DataTable.prototype.commitBatchInsert=function(){
this.batchInsert=false;
var _5b0=this.batchInsertRowCount;
this.batchInsertRowCount=0;
this.setRemoteRowCount(this.remoteRowCount+_5b0);
if(_5b0>0){
this.fire("RowInserted",_5b0);
}
};
nitobi.data.DataTable.prototype.createRecord=function(_5b1,_5b2){
var xi=_5b2;
this.adjustXi(parseInt(xi),1);
var data=this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data");
var _5b5=_5b1||this.getTemplateNode();
var _5b6=nitobi.component.getUniqueId();
var _5b7=_5b5.cloneNode(true);
_5b7.setAttribute("xi",xi);
_5b7.setAttribute("xid",_5b6);
_5b7.setAttribute("xac","i");
if(this.onGenerateKey){
var _5b8=this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasourcestructure").getAttribute("Keys").split("|");
var xml=null;
for(var j=0;j<_5b8.length;j++){
var _5bb=this.fieldMap[_5b8[j]].substring(1);
var _5bc=_5b7.getAttribute(_5bb);
if(!_5bc||_5bc==""){
if(!xml){
xml=eval(this.onGenerateKey);
}
if(typeof (xml)=="string"||typeof (xml)=="number"){
_5b7.setAttribute(_5bb,xml);
}else{
try{
var ck1=j%26;
var ck2=Math.floor(j/26);
var _5bf=(ck2>0?String.fromCharCode(96+ck2):"")+String.fromCharCode(97+ck1);
_5b7.setAttribute(_5bb,xml.selectSingleNode("//"+nitobi.xml.nsPrefix+"e").getAttribute(_5bf));
}
catch(e){
}
}
}
}
}
data.appendChild(nitobi.xml.importNode(data.ownerDocument,_5b7,true));
if(this.log!=null){
var _5c0=_5b7.cloneNode(true);
_5c0.setAttribute("xac","i");
_5c0.setAttribute("xid",_5b6);
this.logData.appendChild(nitobi.xml.importNode(this.logData.ownerDocument,_5c0,true));
}
this.dataCache.insertIntoRange(_5b2);
this.batchInsertRowCount++;
if(!this.batchInsert){
this.commitBatchInsert();
}
return _5b7;
};
nitobi.data.DataTable.prototype.updateRecord=function(xi,_5c2,_5c3){
var _5c4=this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"e[@xi='"+xi+"']");
var xid=_5c4.getAttribute("xid")||"error - unknown xid";
var _5c6=(_5c4.getAttribute(_5c2)!=_5c3);
if(!_5c6){
return;
}
var _5c7="";
var _5c8=_5c2;
if(_5c4.getAttribute(_5c2)==null&&this.fieldMap[_5c2]!=null){
_5c8=this.fieldMap[_5c2].substring(1);
}
_5c7=_5c4.getAttribute(_5c8);
_5c4.setAttribute(_5c8,_5c3);
var _5c9="u";
var _5ca="u";
if(null==this.log){
this.flushLog();
}
var _5cb=_5c4.cloneNode(true);
_5cb.setAttribute("xac","u");
this.logData=this.log.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data");
var _5cc=this.logData.selectSingleNode("./"+nitobi.xml.nsPrefix+"e[@xid='"+xid+"']");
_5cb=nitobi.xml.importNode(this.logData.ownerDocument,_5cb,true);
if(null==_5cc){
_5cb=nitobi.xml.importNode(this.logData.ownerDocument,_5cb,true);
this.logData.appendChild(_5cb);
_5cb.setAttribute("xid",xid);
}else{
_5cb.setAttribute("xac",_5cc.getAttribute("xac"));
this.logData.replaceChild(_5cb,_5cc);
}
if((true==this.AutoSave)){
this.save();
}
this.fire("RowUpdated",{"field":_5c2,"newValue":_5c3,"oldValue":_5c7,"record":_5cb});
};
nitobi.data.DataTable.prototype.deleteRecord=function(_5cd){
var data=this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data");
this.logData=this.log.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data");
var _5cf=data.selectSingleNode("*[@xi = '"+_5cd+"']");
this.removeRecordFromXml(_5cd,_5cf,data);
this.setRemoteRowCount(this.remoteRowCount-1);
this.fire("RowDeleted");
};
nitobi.data.DataTable.prototype.deleteRecordsArray=function(_5d0){
var data=this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data");
this.logData=this.log.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data");
var _5d2=null;
var _5d3=null;
for(var i=0;i<_5d0.length;i++){
var data=this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data");
_5d3=_5d0[i]-i;
_5d2=data.selectSingleNode("*[@xi = '"+_5d3+"']");
this.removeRecordFromXml(_5d3,_5d2,data);
}
this.setRemoteRowCount(this.remoteRowCount-_5d0.length);
this.fire("RowDeleted");
};
nitobi.data.DataTable.prototype.removeRecordFromXml=function(_5d5,_5d6,data){
if(_5d6==null){
throw "Index out of bounds in delete.";
}
var xid=_5d6.getAttribute("xid");
var xDel=this.logData.selectSingleNode("*[@xid='"+xid+"']");
var sTag="";
if(xDel!=null){
sTag=xDel.getAttribute("xac");
this.logData.removeChild(xDel);
}
if(sTag!="i"){
var _5db=_5d6.cloneNode(true);
_5db.setAttribute("xac","d");
this.logData.appendChild(_5db);
}
data.removeChild(_5d6);
this.adjustXi(parseInt(_5d5)+1,-1);
this.dataCache.removeFromRange(_5d5);
};
nitobi.data.DataTable.prototype.adjustXi=function(_5dc,_5dd){
nitobi.data.adjustXiXslProc.addParameter("startingIndex",_5dc,"");
nitobi.data.adjustXiXslProc.addParameter("adjustment",_5dd,"");
this.xmlDoc=nitobi.xml.loadXml(this.xmlDoc,nitobi.xml.transformToString(this.xmlDoc,nitobi.data.adjustXiXslProc,"xml"));
if(this.log!=null){
this.log=nitobi.xml.loadXml(this.log,nitobi.xml.transformToString(this.log,nitobi.data.adjustXiXslProc,"xml"));
this.logData=this.log.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data");
}
};
nitobi.data.DataTable.prototype.setGetHandler=function(val){
this.getHandler=val;
for(var name in this.getHandlerArgs){
this.setGetHandlerParameter(name,this.getHandlerArgs[name]);
}
};
nitobi.data.DataTable.prototype.getGetHandler=function(){
return this.getHandler;
};
nitobi.data.DataTable.prototype.setSaveHandler=function(val){
this.postHandler=val;
for(var name in this.saveHandlerArgs){
this.setSaveHandlerParameter(name,this.saveHandlerArgs[name]);
}
};
nitobi.data.DataTable.prototype.getSaveHandler=function(){
return this.postHandler;
};
nitobi.data.DataTable.prototype.save=function(_5e2,_5e3){
if(!eval(_5e3||"true")){
return;
}
try{
if(this.version==2.8){
var _5e4=this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasourcestructure").getAttribute("FieldNames").split("|");
var _5e5=this.log.selectNodes("//"+nitobi.xml.nsPrefix+"e[@xac = 'i']");
for(var i=0;i<_5e5.length;i++){
for(var j=0;j<_5e4.length;j++){
var _5e8=_5e5[i].getAttribute(this.fieldMap[_5e4[j]].substring(1));
if(!_5e8){
_5e5[i].setAttribute(this.fieldMap[_5e4[j]].substring(1),"");
}
}
_5e5[i].setAttribute("xf",this.parentValue);
}
var _5e9=this.log.selectNodes("//"+nitobi.xml.nsPrefix+"e[@xac = 'u']");
for(var i=0;i<_5e9.length;i++){
for(var j=0;j<_5e4.length;j++){
var _5e8=_5e9[i].getAttribute(this.fieldMap[_5e4[j]].substring(1));
if(!_5e8){
_5e9[i].setAttribute(this.fieldMap[_5e4[j]].substring(1),"");
}
}
}
nitobi.data.updategramTranslatorXslProc.addParameter("xkField",this.fieldMap["_xk"].substring(1),"");
nitobi.data.updategramTranslatorXslProc.addParameter("fields",_5e4.join("|").replace(/\|_xk/,""));
nitobi.data.updategramTranslatorXslProc.addParameter("datasourceId",this.id,"");
this.log=nitobi.xml.transformToXml(this.log,nitobi.data.updategramTranslatorXslProc);
}
var _5ea=this.getSaveHandler();
(_5ea.indexOf("?")==-1)?_5ea+="?":_5ea+="&";
_5ea+="TableId="+this.id;
_5ea+="&uid="+(new Date().getTime());
this.ajaxCallback=this.ajaxCallbackPool.reserve();
this.ajaxCallback.handler=_5ea;
this.ajaxCallback.responseType="xml";
this.ajaxCallback.context=this;
this.ajaxCallback.completeCallback=nitobi.lang.close(this,this.saveComplete);
this.ajaxCallback.params=new nitobi.data.SaveCompleteEventArgs(_5e2);
if(this.version>2.8&&this.log.selectNodes("//"+nitobi.xml.nsPrefix+"e[@xac='i']").length>0&&this.isAutoKeyEnabled()){
this.ajaxCallback.async=false;
}
if(this.log.documentElement.nodeName=="root"){
this.log=nitobi.xml.loadXml(this.log,this.log.xml.replace(/xmlns:ntb=\"http:\/\/www.nitobi.com\"/g,""));
var _5e4=this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasourcestructure").getAttribute("FieldNames").split("|");
_5e4.splice(_5e4.length-1,1);
_5e4=_5e4.join("|");
this.log.documentElement.setAttribute("fields",_5e4);
this.log.documentElement.setAttribute("keys",_5e4);
}
if(this.isAutoKeyEnabled()&&this.version<3){
alert("AutoKey is not supported in this schema version. You must upgrade to Nitobi Grid Xml Schema version 3 or greater.");
}
this.ajaxCallback.post(this.log);
this.flushLog();
}
catch(err){
throw err;
}
};
nitobi.data.DataTable.prototype.flushLog=function(){
this.log=nitobi.xml.createXmlDoc(nitobi.data.DataTable.DEFAULT_LOG.replace(/\{id\}/g,this.id).replace(/\{fields\}/g,this.columns).replace(/\{keys\}/g,this.keys).replace(/\{defaults\}/g,this.defaults).replace(/\{types\}/g,this.types));
this.logData=this.log.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data");
};
nitobi.data.DataTable.prototype.updateAutoKeys=function(_5eb){
try{
var _5ec=_5eb.selectNodes("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data/"+nitobi.xml.nsPrefix+"e[@xac='i']");
if(typeof (_5ec)=="undefined"||_5ec==null){
nitobi.lang.throwError("When updating keys from the server for AutoKey support, the inserts could not be parsed.");
}
var keys=_5eb.selectNodes("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"datasourcestructure")[0].getAttribute("keys").split("|");
if(typeof (keys)=="undefined"||keys==null||keys.length==0){
nitobi.lang.throwError("When updating keys from the server for AutoKey support, no keys could be found. Ensure that the keys are sent in the request response.");
}
for(var i=0;i<_5ec.length;i++){
var _5ef=this.getRecord(_5ec[i].getAttribute("xi"));
for(var j=0;j<keys.length;j++){
var att=this.fieldMap[keys[j]].substring(1);
_5ef.setAttribute(att,_5ec[i].getAttribute(att));
}
}
}
catch(err){
nitobi.lang.throwError("When updating keys from the server for AutoKey support, the inserts could not be parsed.",err);
}
};
nitobi.data.DataTable.prototype.saveComplete=function(_5f2){
var xd=_5f2.response;
var _5f2=_5f2.params;
try{
if(this.isAutoKeyEnabled()&&this.version>2.8){
this.updateAutoKeys(xd);
}
if(this.version==2.8&&!this.onGenerateKey){
var rows=xd.selectNodes("//insert");
for(var i=0;i<rows.length;i++){
var xk=rows[i].getAttribute("xk");
if(xk!=null){
var _5f7=this.findWithoutMap("xid",rows[i].getAttribute("xid"))[0];
var key=this.fieldMap["_xk"].substring(1);
_5f7.setAttribute(key,xk);
}
}
}
if(null!=_5f2.result){
}
var node=xd.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource")||xd.selectSingleNode("/root");
var e=null;
if(node){
e=node.getAttribute("error");
}
if(e){
this.setHandlerError(e);
}else{
this.setHandlerError(null);
}
this.ajaxCallbackPool.release(this.ajaxCallback);
var _5fb=new nitobi.data.OnAfterSaveEventArgs(this,xd);
_5f2.callback.call(this,_5fb);
}
catch(err){
this.ajaxCallbackPool.release(this.ajaxCallback);
ebaErrorReport(err,"",EBA_ERROR);
}
};
nitobi.data.DataTable.prototype.makeFieldMap=function(){
var _5fc=this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource");
var cf=0;
var ck=0;
this.fieldMap=new Array();
var cF=this.columns.length;
for(var i=0;i<cF;i++){
var _601=this.columns[i];
this.fieldMap[_601]=this.getFieldName(ck);
ck++;
}
};
nitobi.data.DataTable.prototype.getFieldName=function(_602){
var ck1=_602%26;
var ck2=Math.floor(_602/26);
return "@"+(ck2>0?String.fromCharCode(96+ck2):"")+String.fromCharCode(97+ck1);
};
nitobi.data.DataTable.prototype.find=function(_605,_606){
var _607=this.fieldMap[_605];
if(_607){
return this.findWithoutMap(_607,_606);
}else{
return new Array();
}
};
nitobi.data.DataTable.prototype.findWithoutMap=function(_608,_609){
if(_608.charAt(0)!="@"){
_608="@"+_608;
}
return this.xmlDoc.selectNodes("//"+nitobi.xml.nsPrefix+"e["+_608+"=\""+_609+"\"]");
};
nitobi.data.DataTable.prototype.sort=function(_60a,dir,type,_60d){
if(_60d){
_60a=this.fieldMap[_60a];
_60a=_60a.substring(1);
dir=(dir=="Desc")?"descending":"ascending";
type=(type=="number")?"number":"text";
this.sortXslProc.addParameter("column",_60a,"");
this.sortXslProc.addParameter("dir",dir,"");
this.sortXslProc.addParameter("type",type,"");
this.xmlDoc=nitobi.xml.loadXml(this.xmlDoc,nitobi.xml.transformToString(this.xmlDoc,this.sortXslProc,"xml"));
this.fire("DataSorted");
}else{
this.sortColumn=_60a;
this.sortDir=dir||"Asc";
}
};
nitobi.data.DataTable.prototype.syncRowCount=function(){
this.setRemoteRowCount(this.descriptor.estimatedRowCount);
};
nitobi.data.DataTable.prototype.setRemoteRowCount=function(rows){
var _60f=this.remoteRowCount;
this.remoteRowCount=rows;
if(this.remoteRowCount!=_60f){
this.fire("RowCountChanged",rows);
}
};
nitobi.data.DataTable.prototype.getRemoteRowCount=function(){
return this.remoteRowCount;
};
nitobi.data.DataTable.prototype.getRows=function(){
return this.xmlDoc.selectNodes("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data/"+nitobi.xml.nsPrefix+"e").length;
};
nitobi.data.DataTable.prototype.getXmlDoc=function(){
return this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']");
};
nitobi.data.DataTable.prototype.getRowNodes=function(){
return this.xmlDoc.selectNodes("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data/"+nitobi.xml.nsPrefix+"e");
};
nitobi.data.DataTable.prototype.getColumns=function(){
return this.fieldMap.length;
};
nitobi.data.DataTable.prototype.setGetHandlerParameter=function(name,_611){
if(this.getHandler!=null&&this.getHandler!=""){
this.getHandler=nitobi.html.setUrlParameter(this.getHandler,name,_611);
}
this.getHandlerArgs[name]=_611;
};
nitobi.data.DataTable.prototype.setSaveHandlerParameter=function(name,_613){
if(this.postHandler!=null&&this.postHandler!=""){
this.postHandler=nitobi.html.setUrlParameter(this.getSaveHandler(),name,_613);
}
this.saveHandlerArgs[name]=_613;
};
nitobi.data.DataTable.prototype.getChangeLogSize=function(){
if(null==this.log){
return 0;
}
return this.log.selectNodes("//"+nitobi.xml.nsPrefix+"e").length;
};
nitobi.data.DataTable.prototype.getChangeLogXmlDoc=function(){
return this.log;
};
nitobi.data.DataTable.prototype.getDataXmlDoc=function(){
return this.xmlDoc;
};
nitobi.data.DataTable.prototype.dispose=function(){
this.flush();
this.ajaxCallbackPool.context=null;
for(var item in this){
if(this[item]!=null&&this[item].dispose instanceof Function){
this[item].dispose();
}
this[item]=null;
}
};
nitobi.data.DataTable.prototype.getTable=function(_615,_616,_617){
this.errorCallback=_617;
var _618=this.ajaxCallbackPool.reserve();
var _619=this.getGetHandler();
_618.handler=_619;
_618.responseType="xml";
_618.context=this;
_618.completeCallback=nitobi.lang.close(this,this.getComplete);
_618.async=this.async;
_618.params=new nitobi.data.GetCompleteEventArgs(null,null,0,null,_618,this,_615,_616);
if(typeof (_616)!="function"||this.async==false){
_618.async=false;
return this.getComplete({"response":_618.get(),"params":_618.params});
}else{
_618.get();
}
};
nitobi.data.DataTable.prototype.getComplete=function(_61a){
var xd=_61a.response;
var _61c=_61a.params;
if(this.mode!="caching"){
this.xmlDoc=nitobi.xml.createXmlDoc();
}
if(null==xd||null==xd.xml||""==xd.xml){
var _61d="No parse error.";
if(nitobi.xml.hasParseError(xd)){
if(xd==null){
_61d="Blank Response was Given";
}else{
_61d=nitobi.xml.getParseErrorReason(xd);
}
}
if(this.errorCallback){
this.errorCallback.call(this.context);
}
this.fire("DataReady",_61c);
return _61c;
}else{
if(typeof (this.successCallback)=="function"){
this.successCallback.call(this.context);
}
}
if(!this.configured){
this.configureFromData(xd);
}
xd=this.parseResponse(xd,_61c);
xd=this.assignRowIds(xd);
var _61e=null;
_61e=xd.selectNodes("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data/"+nitobi.xml.nsPrefix+"e");
var _61f;
var _620=_61e.length;
if(_61c.pageSize==null){
_61c.pageSize=_620;
_61c.lastRow=_61c.startXi+_61c.pageSize-1;
_61c.firstRow=_61c.startXi;
}
if(0!=_620){
_61f=parseInt(_61e[_61e.length-1].getAttribute("xi"));
if(this.mode=="paging"){
this.dataCache.insert(0,_61c.pageSize-1);
}else{
this.dataCache.insert(_61c.firstRow,_61f);
}
}else{
_61f=-1;
_61c.pageSize=0;
if(this.totalRowCount==null){
var pct=this.descriptor.lastKnownRow/this.descriptor.estimatedRowCount||0;
this.fire("PastEndOfData",pct);
}
}
_61c.numRowsReturned=_620;
_61c.lastRowReturned=_61f;
var _622=_61c.startXi;
var _623=_61c.pageSize;
if(!isNaN(_622)&&!isNaN(_623)&&_622!=0){
this.requestCache.remove(_622,_622+_623-1);
}
if(this.mode!="caching"){
this.replaceData(xd);
}else{
this.mergeData(xd);
}
if(!this.totalRowCount){
var _624=this.xmlDoc.selectSingleNode("//ntb:datasource").getAttribute("totalrowcount");
_624=parseInt(_624);
if(!isNaN(_624)){
this.totalRowCount=_624;
}
this.fire("TotalRowCountReady",this.totalRowCount);
}
var _625=this.xmlDoc.selectSingleNode("//ntb:datasource").getAttribute("parentfield");
var _626=this.xmlDoc.selectSingleNode("//ntb:datasource").getAttribute("parentvalue");
this.parentField=_625||"";
this.parentValue=_626||"";
this.updateFromDescriptor(_61c);
this.fire("RowCountReady",_61c);
if(null!=_61c.ajaxCallback){
this.ajaxCallbackPool.release(_61c.ajaxCallback);
}
this.executeRequests();
var node=xd.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource");
var e=null;
if(node){
e=node.getAttribute("error");
}
if(e){
this.setHandlerError(e);
}else{
this.setHandlerError(null);
}
this.fire("DataReady",_61c);
if(null!=_61c.callback&&null!=_61c.context){
_61c.callback.call(_61c.context,_61c);
_61c.dispose();
_61c=null;
}else{
return _61c;
}
};
nitobi.data.DataTable.prototype.executeRequests=function(){
var _629=this.requestQueue;
this.requestQueue=new Array();
for(var i=0;i<_629.length;i++){
_629[i].call();
}
};
nitobi.data.DataTable.prototype.updateFromDescriptor=function(_62b){
if(this.totalRowCount==null){
this.descriptor.update(_62b);
}
if(this.mode=="paging"){
this.setRemoteRowCount(_62b.numRowsReturned);
}else{
if(this.totalRowCount!=null){
this.setRemoteRowCount(this.getTotalRowCount());
}else{
this.setRemoteRowCount(this.descriptor.estimatedRowCount);
}
}
this.setRowCountKnown(this.descriptor.isAtEndOfTable);
};
nitobi.data.DataTable.prototype.setRowCountKnown=function(_62c){
var _62d=this.rowCountKnown;
this.rowCountKnown=_62c;
if(_62c&&this.rowCountKnown!=_62d){
this.fire("RowCountKnown",this.remoteRowCount);
}
};
nitobi.data.DataTable.prototype.getRowCountKnown=function(){
return this.rowCountKnown;
};
nitobi.data.DataTable.prototype.configureFromData=function(xd){
this.version=this.inferDataVersion(xd);
if(this.mode=="unbound"){
}
if(this.mode=="static"){
}
if(this.mode=="paging"){
}
if(this.mode=="caching"){
}
};
nitobi.data.DataTable.prototype.mergeData=function(xd){
if(this.xmlDoc.xml==""){
this.initializeXml(xd);
return;
}
var p=nitobi.xml.nsPrefix;
var _631="//"+p+"datasource[@id = '"+this.id+"']/"+p+"data";
var _632=xd.selectNodes(_631+"//"+p+"e");
var _633=this.xmlDoc.selectSingleNode(_631);
var len=_632.length;
for(var i=0;i<len;i++){
if(this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data/"+nitobi.xml.nsPrefix+"e[@xi='"+_632[i].getAttribute("xi")+"']")){
continue;
}
_633.appendChild(nitobi.xml.importNode(_633.ownerDocument,_632[i],true));
}
};
nitobi.data.DataTable.prototype.assignRowIds=function(xd){
nitobi.data.addXidXslProc.addParameter("guid",nitobi.component.getUniqueId(),"");
var doc=nitobi.xml.loadXml(xd,nitobi.xml.transformToString(xd,nitobi.data.addXidXslProc,"xml"));
return doc;
};
nitobi.data.DataTable.prototype.inferDataVersion=function(xd){
if(xd.selectSingleNode("/root")){
return 2.8;
}
return 3;
};
nitobi.data.DataTable.prototype.parseResponse=function(xd,_63a){
if(this.version==2.8){
return this.parseLegacyResponse(xd,_63a);
}else{
return this.parseStructuredResponse(xd,_63a);
}
};
nitobi.data.DataTable.prototype.parseLegacyResponse=function(xd,_63c){
var _63d=this.mode=="paging"?0:_63c.startXi;
nitobi.data.dataTranslatorXslProc.addParameter("start",_63d,"");
nitobi.data.dataTranslatorXslProc.addParameter("id",this.id,"");
var _63e=xd.selectSingleNode("/root").getAttribute("fields");
var _63f=_63e.split("|");
var i=_63f.length;
var _641=(i>25?String.fromCharCode(Math.floor(i/26)+96):"")+(String.fromCharCode(i%26+97));
nitobi.data.dataTranslatorXslProc.addParameter("xkField",_641,"");
xd=nitobi.xml.transformToXml(xd,nitobi.data.dataTranslatorXslProc);
return xd;
};
nitobi.data.DataTable.prototype.parseStructuredResponse=function(xd,_643){
xd=nitobi.xml.loadXml(xd,"<ntb:grid xmlns:ntb=\"http://www.nitobi.com\"><ntb:datasources>"+xd.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']").xml+"</ntb:datasources></ntb:grid>");
var _644=xd.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.id+"']/"+nitobi.xml.nsPrefix+"data/"+nitobi.xml.nsPrefix+"e");
var _645=this.mode=="paging"?0:_643.startXi;
if(_644){
if(_644.getAttribute("xi")!=_645){
nitobi.data.adjustXiXslProc.addParameter("startingIndex","0","");
nitobi.data.adjustXiXslProc.addParameter("adjustment",_645,"");
xd=nitobi.xml.loadXml(xd,nitobi.xml.transformToString(xd,nitobi.data.adjustXiXslProc,"xml"));
}
}
return xd;
};
nitobi.data.DataTable.prototype.forceGet=function(_646,_647,_648,_649,_64a,_64b){
this.errorCallback=_64a;
this.successCallback=_64b;
this.context=_648;
var _64c=this.getGetHandler();
(_64c.indexOf("?")==-1)?_64c+="?":_64c+="&";
_64c+="StartRecordIndex=0&start=0&PageSize="+_647+"&SortColumn="+(this.sortColumn||"")+"&SortDirection="+this.sortDir+"&TableId="+this.id+"&uid="+(new Date().getTime());
var _64d=this.ajaxCallbackPool.reserve();
_64d.handler=_64c;
_64d.responseType="xml";
_64d.context=this;
_64d.completeCallback=nitobi.lang.close(this,this.getComplete);
_64d.params=new nitobi.data.GetCompleteEventArgs(0,_647-1,0,_647,_64d,this,_648,_649);
_64d.get();
return;
};
nitobi.data.DataTable.prototype.getPage=function(_64e,_64f,_650,_651,_652,_653){
var _654=_64e+_64f-1;
var _655=this.dataCache.gaps(0,_64f-1);
var _656=_655.length;
if(_656){
var _657=this.requestCache.gaps(_64e,_654);
if(_657.length==0){
var _658=nitobi.lang.close(this,this.get,arguments);
this.requestQueue.push(_658);
return;
}
this.getFromServer(_64e,_654,_64e,_654,_650,_651,_652);
}else{
this.getFromCache(_64e,_64f,_650,_651,_652);
}
};
nitobi.data.DataTable.prototype.get=function(_659,_65a,_65b,_65c,_65d){
this.errorCallback=_65d;
var _65e=null;
if(this.mode=="caching"){
_65e=this.getCached(_659,_65a,_65b,_65c,_65d);
}
if(this.mode=="local"||this.mode=="static"){
_65e=this.getTable(_65b,_65c,_65d);
}
if(this.mode=="paging"){
_65e=this.getPage(_659,_65a,_65b,_65c,_65d);
}
return _65e;
};
nitobi.data.DataTable.prototype.inCache=function(_65f,_660){
if(this.mode=="local"){
return true;
}
var _661=_65f,_662=_65f+_660-1;
var _663=this.getRemoteRowCount()-1;
if(this.getRowCountKnown()&&_663<_662){
_662=_663;
}
var _664=this.dataCache.gaps(_661,_662);
var _665=_664.length;
return !(_665>0);
};
nitobi.data.DataTable.prototype.cachedRanges=function(_666,_667){
return this.dataCache.ranges(_666,_667);
};
nitobi.data.DataTable.prototype.getCached=function(_668,_669,_66a,_66b,_66c,_66d){
if(_669==null){
return this.getFromServer(_66e,null,_668,null,_66a,_66b,_66c);
}
var _66e=_668,_66f=_668+_669-1;
var _670=this.dataCache.gaps(_66e,_66f);
var _671=_670.length;
if(this.mode!="unbound"&&_671>0){
var low=_670[_671-1].low;
var high=_670[_671-1].high;
var _674=this.requestCache.gaps(low,high);
if(_674.length==0){
var _675=nitobi.lang.close(this,this.get,arguments);
this.requestQueue.push(_675);
return;
}
return this.getFromServer(_66e,_66f,low,high,_66a,_66b,_66c);
}else{
this.getFromCache(_668,_669,_66a,_66b,_66c);
}
};
nitobi.data.DataTable.prototype.getFromServer=function(_676,_677,low,high,_67a,_67b,_67c){
this.requestCache.insert(low,high);
var _67d=(_677==null?null:(high-low+1));
var _67e=(_67d==null?"":_67d);
var _67f=this.getGetHandler();
(_67f.indexOf("?")==-1)?_67f+="?":_67f+="&";
_67f+="StartRecordIndex="+low+"&start="+low+"&PageSize="+(_67e)+"&SortColumn="+(this.sortColumn||"")+"&SortDirection="+this.sortDir+"&uid="+(new Date().getTime());
var _680=this.ajaxCallbackPool.reserve();
_680.handler=_67f;
_680.responseType="xml";
_680.context=this;
_680.completeCallback=nitobi.lang.close(this,this.getComplete);
_680.async=this.async;
_680.params=new nitobi.data.GetCompleteEventArgs(_676,_677,low,_67d,_680,this,_67a,_67b);
return _680.get();
};
nitobi.data.DataTable.prototype.getFromCache=function(_681,_682,_683,_684,_685){
var _686=_681,_687=_681+_682-1;
if(_686>0||_687>0){
if(typeof (_684)=="function"){
var _688=new nitobi.data.GetCompleteEventArgs(_686,_687,_686,_687-_686+1,null,this,_683,_684);
_688.callback.call(_688.context,_688);
}
}
};
nitobi.data.DataTable.prototype.mergeFromXml=function(_689,_68a){
var _68b=Number(_689.documentElement.firstChild.getAttribute("xi"));
var _68c=Number(_689.documentElement.lastChild.getAttribute("xi"));
var _68d=this.dataCache.gaps(_68b,_68c);
if(this.mode=="local"&&_68d.length==1){
this.dataCache.insert(_68d[0].low,_68d[0].high);
this.mergeFromXmlGetComplete(_689,_68a,_68b,_68c);
this.batchInsertRowCount=(_68d[0].high-_68d[0].low+1);
this.commitBatchInsert();
return;
}
if(_68d.length==0){
this.mergeFromXmlGetComplete(_689,_68a,_68b,_68c);
}else{
if(_68d.length==1){
this.get(_68d[0].low,_68d[0].high-_68d[0].low+1,this,nitobi.lang.close(this,this.mergeFromXmlGetComplete,[_689,_68a,_68b,_68c]));
}else{
this.forceGet(_68b,_68c,this,nitobi.lang.close(this,this.mergeFromXmlGetComplete,[_689,_68a,_68b,_68c]));
}
}
};
nitobi.data.DataTable.prototype.mergeFromXmlGetComplete=function(_68e,_68f,_690,_691){
var _692=nitobi.xml.createElement(this.xmlDoc,"newdata");
_692.appendChild(_68e.documentElement.cloneNode(true));
this.xmlDoc.documentElement.appendChild(nitobi.xml.importNode(this.xmlDoc,_692,true));
nitobi.data.mergeEbaXmlXslProc.addParameter("startRowIndex",_690,"");
nitobi.data.mergeEbaXmlXslProc.addParameter("endRowIndex",_691,"");
nitobi.data.mergeEbaXmlXslProc.addParameter("guid",nitobi.component.getUniqueId(),"");
this.xmlDoc=nitobi.xml.loadXml(this.xmlDoc,nitobi.xml.transformToString(this.xmlDoc,nitobi.data.mergeEbaXmlXslProc,"xml"));
_692=nitobi.xml.createElement(this.log,"newdata");
this.log.documentElement.appendChild(nitobi.xml.importNode(this.log,_692,true));
_692.appendChild(this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"newdata").firstChild.cloneNode(true));
this.log=nitobi.xml.loadXml(this.log,nitobi.xml.transformToString(this.log,nitobi.data.mergeEbaXmlToLogXslProc,"xml"));
this.xmlDoc.documentElement.removeChild(this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"newdata"));
this.log.documentElement.removeChild(this.log.selectSingleNode("//"+nitobi.xml.nsPrefix+"newdata"));
_68f.call();
};
nitobi.data.DataTable.prototype.fillColumn=function(_693,_694){
nitobi.data.fillColumnXslProc.addParameter("column",this.fieldMap[_693].substring(1));
nitobi.data.fillColumnXslProc.addParameter("value",_694);
this.xmlDoc.loadXML(nitobi.xml.transformToString(this.xmlDoc,nitobi.data.fillColumnXslProc,"xml"));
var _695=parseFloat((new Date()).getTime());
var _696=nitobi.xml.createElement(this.log,"newdata");
this.log.documentElement.appendChild(nitobi.xml.importNode(this.log,_696,true));
_696.appendChild(this.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"data").cloneNode(true));
nitobi.data.mergeEbaXmlToLogXslProc.addParameter("defaultAction","u");
this.log.loadXML(nitobi.xml.transformToString(this.log,nitobi.data.mergeEbaXmlToLogXslProc,"xml"));
nitobi.data.mergeEbaXmlToLogXslProc.addParameter("defaultAction","");
this.log.documentElement.removeChild(this.log.selectSingleNode("//"+nitobi.xml.nsPrefix+"newdata"));
};
nitobi.data.DataTable.prototype.getTotalRowCount=function(){
return this.totalRowCount;
};
nitobi.data.DataTable.prototype.setHandlerError=function(_697){
this.handlerError=_697;
};
nitobi.data.DataTable.prototype.getHandlerError=function(){
return this.handlerError;
};
nitobi.data.DataTable.prototype.dispose=function(){
this.sortXslProc=null;
this.requestQueue=null;
this.fieldMap=null;
};
nitobi.data.DataTable.prototype.fire=function(evt,args){
return nitobi.event.notify(evt+this.uid,args);
};
nitobi.data.DataTable.prototype.subscribe=function(evt,func,_69c){
if(typeof (_69c)=="undefined"){
_69c=this;
}
return nitobi.event.subscribe(evt+this.uid,nitobi.lang.close(_69c,func));
};
nitobi.lang.defineNs("nitobi.data");
nitobi.data.DataTableDescriptor=function(_69d,_69e,_69f){
this.disposal=[];
this.estimatedRowCount=0;
this.leapMultiplier=2;
this.estimateRowCount=(_69f==null?true:_69f);
this.lastKnownRow=0;
this.isAtEndOfTable=false;
this.table=_69d;
this.lowestEmptyRow=0;
this.tableProjectionUpdatedEvent=_69e;
this.disposal.push(this.tableProjectionUpdatedEvent);
};
nitobi.data.DataTableDescriptor.prototype.startPeek=function(){
this.enablePeek=true;
this.peek();
};
nitobi.data.DataTableDescriptor.prototype.peek=function(){
var _6a0;
if(this.lowestEmptyRow>0){
var _6a1=this.lowestEmptyRow-this.lastKnownRow;
_6a0=this.lastKnownRow+Math.round(_6a1/2);
}else{
_6a0=(this.estimatedRowCount*this.leapMultiplier);
}
this.table.get(Math.round(_6a0),1,this,this.peekComplete);
};
nitobi.data.DataTableDescriptor.prototype.peekComplete=function(_6a2){
if(this.enablePeek){
window.setTimeout(nitobi.lang.close(this,this.peek),1000);
}
};
nitobi.data.DataTableDescriptor.prototype.stopPeek=function(){
this.enablePeek=false;
};
nitobi.data.DataTableDescriptor.prototype.leap=function(_6a3,_6a4){
if(this.lowestEmptyRow>0){
var _6a5=this.lowestEmptyRow-this.lastKnownRow;
this.estimatedRowCount=this.lastKnownRow+Math.round(_6a5/2);
}else{
if(_6a3==null||_6a4==null){
this.estimatedRowCount=0;
}else{
if(this.estimateRowCount){
this.estimatedRowCount=(this.estimatedRowCount*_6a3)+_6a4;
}
}
}
this.fireProjectionUpdatedEvent();
};
nitobi.data.DataTableDescriptor.prototype.update=function(_6a6,_6a7){
if(null==_6a7){
_6a7=false;
}
if(this.isAtEndOfTable&&!_6a7){
return false;
}
var _6a8=(_6a6!=null&&_6a6.numRowsReturned==0&&_6a6.startXi==0);
var _6a9=(_6a6!=null&&_6a6.lastRow!=_6a6.lastRowReturned);
if(null==_6a6){
_6a6={lastPage:false,pageSize:1,firstRow:0,lastRow:0,startXi:0};
}
var _6aa=(_6a8)||(_6a9)||(this.isAtEndOfTable)||((this.lastKnownRow==this.estimatedRowCount-1)&&(this.estimatedRowCount==this.lowestEmptyRow));
if(_6a6.pageSize==0&&!_6aa){
this.lowestEmptyRow=this.lowestEmptyRow>0?Math.min(_6a6.startXi,this.lowestEmptyRow):_6a6.startXi;
this.leap();
return true;
}
this.lastKnownRow=Math.max(_6a6.lastRowReturned,this.lastKnownRow);
if(_6aa&&!_6a7){
if(_6a6.lastRowReturned>=0){
this.estimatedRowCount=_6a6.lastRowReturned+1;
this.isAtEndOfTable=true;
}else{
if(_6a8){
this.estimatedRowCount=0;
this.isAtEndOfTable=true;
}else{
this.estimatedRowCount=this.lastKnownRow+Math.ceil((_6a6.lastRow-this.lastKnownRow)/2);
}
}
this.fireProjectionUpdatedEvent();
this.stopPeek();
return true;
}
if(!this.estimateRowCount){
this.estimatedRowCount=this.lastKnownRow+1;
}
if(this.estimatedRowCount==0){
this.estimatedRowCount=(_6a6.lastRow+1)*(this.estimateRowCount?2:1);
}
if((this.estimatedRowCount>(_6a6.lastRow+1)&&!_6a7)||!this.estimateRowCount){
return false;
}
if(!this.isAtEndOfTable){
this.leap(this.leapMultiplier,0);
return true;
}
return false;
};
nitobi.data.DataTableDescriptor.prototype.reset=function(){
this.estimatedRowCount=0;
this.leapMultiplier=2;
this.lastKnownRow=0;
this.isAtEndOfTable=false;
this.lowestEmptyRow=0;
this.fireProjectionUpdatedEvent();
};
nitobi.data.DataTableDescriptor.prototype.fireProjectionUpdatedEvent=function(_6ab){
if(this.tableProjectionUpdatedEvent!=null){
this.tableProjectionUpdatedEvent(_6ab);
}
};
nitobi.data.DataTableDescriptor.prototype.dispose=function(){
nitobi.lang.dispose(this,this.disposal);
};
nitobi.lang.defineNs("nitobi.data");
if(false){
nitobi.data=function(){
};
}
nitobi.data.DataTableEventArgs=function(_6ac){
this.source=_6ac;
this.event=nitobi.html.Event;
};
nitobi.data.DataTableEventArgs.prototype.getSource=function(){
return this.source;
};
nitobi.data.DataTableEventArgs.prototype.getEvent=function(){
return this.event;
};
nitobi.data.GetCompleteEventArgs=function(_6ad,_6ae,_6af,_6b0,_6b1,_6b2,obj,_6b4){
this.firstRow=_6ad;
this.lastRow=_6ae;
this.callback=_6b4;
this.dataSource=_6b2;
this.context=obj;
this.ajaxCallback=_6b1;
this.startXi=_6af;
this.pageSize=_6b0;
this.lastPage=false;
this.status="success";
};
nitobi.data.GetCompleteEventArgs.prototype.dispose=function(){
this.callback=null;
this.context=null;
this.dataSource=null;
this.ajaxCallback.clear();
this.ajaxCallback==null;
};
nitobi.data.SaveCompleteEventArgs=function(_6b5){
this.callback=_6b5;
};
nitobi.data.SaveCompleteEventArgs.prototype.initialize=function(){
};
nitobi.data.OnAfterSaveEventArgs=function(_6b6,_6b7,_6b8){
nitobi.data.OnAfterSaveEventArgs.baseConstructor.call(this,_6b6);
this.success=_6b8;
this.responseData=_6b7;
};
nitobi.lang.extend(nitobi.data.OnAfterSaveEventArgs,nitobi.data.DataTableEventArgs);
nitobi.data.OnAfterSaveEventArgs.prototype.getResponseData=function(){
return this.responseData;
};
nitobi.data.OnAfterSaveEventArgs.prototype.getSuccess=function(){
return this.success;
};
nitobi.lang.defineNs("nitobi.form");
if(false){
nitobi.form=function(){
};
}
nitobi.form.Control=function(){
this.owner=null;
this.placeholder=null;
var div=nitobi.html.createElement("div");
div.innerHTML="<table border='0' cellpadding='0' cellspacing='0' class='ntb-input-border'><tr><td></td></tr></table>";
var ph=this.placeholder=div.firstChild;
this.cell=null;
this.ignoreBlur=false;
this.editCompleteHandler=function(){
};
this.onKeyUp=new nitobi.base.Event();
this.onKeyDown=new nitobi.base.Event();
this.onKeyPress=new nitobi.base.Event();
this.onChange=new nitobi.base.Event();
this.onCancel=new nitobi.base.Event();
this.onTab=new nitobi.base.Event();
this.onEnter=new nitobi.base.Event();
};
nitobi.form.Control.prototype.initialize=function(){
};
nitobi.form.Control.prototype.mimic=function(){
};
nitobi.form.Control.prototype.deactivate=function(evt){
if(this.ignoreBlur){
return false;
}
this.ignoreBlur=true;
};
nitobi.form.Control.prototype.bind=function(_6bc,cell){
this.owner=_6bc;
this.cell=cell;
this.ignoreBlur=false;
};
nitobi.form.Control.prototype.hide=function(){
this.placeholder.style.left="-2000px";
};
nitobi.form.Control.prototype.attachToParent=function(_6be){
_6be.appendChild(this.placeholder);
};
nitobi.form.Control.prototype.show=function(){
this.placeholder.style.display="block";
};
nitobi.form.Control.prototype.focus=function(){
this.control.focus();
this.ignoreBlur=false;
};
nitobi.form.Control.prototype.align=function(){
var oY=1;
var oX=1;
if(nitobi.browser.MOZ){
var _6c1=this.owner.getScrollSurface();
var _6c2=this.owner.getActiveView().region;
if(_6c2==3||_6c2==4){
oY=_6c1.scrollTop-nitobi.form.EDITOR_OFFSETY;
}
if(_6c2==1||_6c2==4){
oX=_6c1.scrollLeft-nitobi.form.EDITOR_OFFSETX;
}
}
nitobi.drawing.align(this.placeholder,this.cell.getDomNode(),286265344,1,1,-oY,-oX);
};
nitobi.form.Control.prototype.selectText=function(){
this.focus();
if(this.control&&this.control.createTextRange){
var _6c3=this.control.createTextRange();
_6c3.collapse(false);
_6c3.select();
}
};
nitobi.form.Control.prototype.checkValidity=function(evt){
var _6c5=this.deactivate(evt);
if(_6c5==false){
nitobi.html.cancelBubble(evt);
return false;
}
return true;
};
nitobi.form.Control.prototype.handleKey=function(evt){
var k=evt.keyCode;
if(this.onKeyDown.notify(evt)==false){
return;
}
var K=nitobi.form.Keys;
var y=0;
var x=0;
if(k==K.UP){
y=-1;
}else{
if(k==K.DOWN){
y=1;
}else{
if(k==K.TAB){
x=1;
if(evt.shiftKey){
x=-1;
}
if(nitobi.browser.IE){
evt.keyCode="";
}
}else{
if(k==K.ENTER){
y=1;
}else{
if(k==K.ESC){
this.ignoreBlur=true;
this.hide();
this.owner.focus();
this.onCancel.notify(this);
}
return;
}
}
}
}
if(!this.checkValidity(evt)){
return;
}
this.owner.move(x,y);
nitobi.html.cancelBubble(evt);
};
nitobi.form.Control.prototype.handleKeyUp=function(evt){
this.onKeyUp.notify(evt);
};
nitobi.form.Control.prototype.handleKeyPress=function(evt){
this.onKeyPress.notify(evt);
};
nitobi.form.Control.prototype.handleChange=function(evt){
this.onChange.notify(evt);
};
nitobi.form.Control.prototype.setEditCompleteHandler=function(_6ce){
this.editCompleteHandler=_6ce;
};
nitobi.form.Control.prototype.eSET=function(name,args){
var _6d1=args[0];
var _6d2=_6d1;
var _6d3=name.substr(2);
_6d3=_6d3.substr(0,_6d3.length-5);
if(typeof (_6d1)=="string"){
_6d2=function(){
return nitobi.event.evaluate(_6d1,arguments[0]);
};
}
if(this[_6d3]!=null){
this[name].unSubscribe(this[_6d3]);
}
var guid=this[name].subscribe(_6d2);
this.jSET(_6d3,[guid]);
return guid;
};
nitobi.form.Control.prototype.afterDeactivate=function(text,_6d6){
_6d6=_6d6||text;
if(this.editCompleteHandler!=null){
var _6d7=new nitobi.grid.EditCompleteEventArgs(this,text,_6d6,this.cell);
var _6d8=this.editCompleteHandler.call(this.owner,_6d7);
if(!_6d8){
this.ignoreBlur=false;
}
return _6d8;
}
};
nitobi.form.Control.prototype.jSET=function(name,val){
this[name]=val[0];
};
nitobi.form.Control.prototype.dispose=function(){
for(var item in this){
}
};
nitobi.form.IBlurable=function(_6dc,_6dd){
this.selfBlur=false;
this.elements=_6dc;
var H=nitobi.html;
for(var i=0;i<this.elements.length;i++){
var e=this.elements[i];
H.attachEvent(e,"mousedown",this.handleMouseDown,this);
H.attachEvent(e,"blur",this.handleBlur,this);
H.attachEvent(e,"focus",this.handleFocus,this);
H.attachEvent(e,"mouseup",this.handleMouseUp,this);
}
this.blurFunc=_6dd;
this.lastFocus=null;
};
nitobi.form.IBlurable.prototype.removeBlurable=function(){
for(var i=0;i<elems.length;i++){
nitobi.html.detachEvent(elems[i],"mousedown",this.handleMouseDown,this);
}
};
nitobi.form.IBlurable.prototype.handleMouseDown=function(evt){
if(this.lastFocus!=evt.srcElement){
this.selfBlur=true;
}else{
this.selfBlur=false;
}
this.lastFocus=evt.srcElement;
};
nitobi.form.IBlurable.prototype.handleBlur=function(evt){
if(!this.selfBlur){
this.blurFunc(evt);
}
this.selfBlur=false;
};
nitobi.form.IBlurable.prototype.handleFocus=function(){
this.selfBlur=false;
};
nitobi.form.IBlurable.prototype.handleMouseUp=function(){
this.selfBlur=false;
};
nitobi.form.Text=function(){
nitobi.form.Text.baseConstructor.call(this);
var ph=this.placeholder;
ph.setAttribute("id","text_span");
ph.style.top="-3000px";
ph.style.left="-3000px";
var tc=this.control=nitobi.html.createElement("input",{"id":"ntb-textbox"},{width:"100px"});
tc.setAttribute("maxlength",255);
this.events=[{type:"keydown",handler:this.handleKey},{type:"keyup",handler:this.handleKeyUp},{type:"keypress",handler:this.handleKeyPress},{type:"change",handler:this.handleChange},{type:"blur",handler:this.deactivate}];
};
nitobi.lang.extend(nitobi.form.Text,nitobi.form.Control);
nitobi.form.Text.prototype.initialize=function(){
var _6e6=this.placeholder.rows[0].cells[0];
_6e6.appendChild(this.control);
nitobi.html.attachEvents(this.control,this.events,this);
};
nitobi.form.Text.prototype.bind=function(_6e7,cell,_6e9){
nitobi.form.Text.base.bind.apply(this,arguments);
if(_6e9!=null&&_6e9!=""){
this.control.value=_6e9;
}else{
this.control.value=cell.getValue();
}
var _6ea=this.cell.getColumnObject().getModel();
this.eSET("onKeyPress",[_6ea.getAttribute("OnKeyPressEvent")]);
this.eSET("onKeyDown",[_6ea.getAttribute("OnKeyDownEvent")]);
this.eSET("onKeyUp",[_6ea.getAttribute("OnKeyUpEvent")]);
this.eSET("onChange",[_6ea.getAttribute("OnChangeEvent")]);
this.control.setAttribute("maxlength",_6ea.getAttribute("MaxLength"));
nitobi.html.Css.addClass(this.control,"ntb-column-data"+this.owner.uid+"_"+(this.cell.getColumn()+1));
};
nitobi.form.Text.prototype.mimic=function(){
this.align();
nitobi.html.fitWidth(this.placeholder,this.control);
this.selectText();
};
nitobi.form.Text.prototype.focus=function(){
this.control.focus();
};
nitobi.form.Text.prototype.deactivate=function(evt){
if(nitobi.form.Text.base.deactivate.apply(this,arguments)==false){
return;
}
nitobi.html.Css.removeClass(this.control,"ntb-column-data"+this.owner.uid+"_"+(this.cell.getColumn()+1));
return this.afterDeactivate(this.control.value);
};
nitobi.form.Text.prototype.dispose=function(){
nitobi.html.detachEvents(this.control,this.events);
var _6ec=this.placeholder.parentNode;
_6ec.removeChild(this.placeholder);
this.control=null;
this.owner=null;
this.cell=null;
};
nitobi.form.Checkbox=function(){
};
nitobi.lang.extend(nitobi.form.Checkbox,nitobi.form.Control);
nitobi.form.Checkbox.prototype.mimic=function(){
if(false==eval(this.owner.getOnCellValidateEvent())){
return;
}
this.toggle();
this.deactivate();
};
nitobi.form.Checkbox.prototype.deactivate=function(){
this.afterDeactivate(this.value);
};
nitobi.form.Checkbox.prototype.attachToParent=function(){
};
nitobi.form.Checkbox.prototype.toggle=function(){
var _6ed=this.cell.getColumnObject();
var _6ee=_6ed.getModel();
var _6ef=_6ee.getAttribute("CheckedValue");
if(_6ef==""||_6ef==null){
_6ef=1;
}
var _6f0=_6ee.getAttribute("UnCheckedValue");
if(_6f0==""||_6f0==null){
_6f0=0;
}
this.value=(this.cell.getData().value==_6ef)?_6f0:_6ef;
};
nitobi.form.Checkbox.prototype.hide=function(){
};
nitobi.form.Checkbox.prototype.dispose=function(){
this.metadata=null;
this.owner=null;
this.context=null;
};
nitobi.form.Date=function(){
nitobi.form.Date.baseConstructor.call(this);
};
nitobi.lang.extend(nitobi.form.Date,nitobi.form.Text);
nitobi.lang.defineNs("nitobi.form");
nitobi.form.EDITOR_OFFSETX=0;
nitobi.form.EDITOR_OFFSETY=0;
nitobi.form.ControlFactory=function(){
this.editors={};
};
nitobi.form.ControlFactory.prototype.getEditor=function(_6f1,_6f2,_6f3){
var _6f4=null;
if(null==_6f2){
ebaErrorReport("getEditor: column parameter is null","",EBA_DEBUG);
return _6f4;
}
var _6f5=_6f2.getType();
var _6f6=_6f2.getType();
var _6f7="nitobi.Grid"+_6f5+_6f6+"Editor";
_6f4=this.editors[_6f7];
if(_6f4==null||_6f4.control==null){
switch(_6f5){
case "LINK":
case "HYPERLINK":
_6f4=new nitobi.form.Link;
break;
case "IMAGE":
return null;
case "BUTTON":
return null;
case "LOOKUP":
_6f4=new nitobi.form.Lookup();
break;
case "LISTBOX":
_6f4=new nitobi.form.ListBox();
break;
case "PASSWORD":
_6f4=new nitobi.form.Password();
break;
case "TEXTAREA":
_6f4=new nitobi.form.TextArea();
break;
case "CHECKBOX":
_6f4=new nitobi.form.Checkbox();
break;
default:
if(_6f6=="DATE"){
if(_6f2.isCalendarEnabled()){
_6f4=new nitobi.form.Calendar();
}else{
_6f4=new nitobi.form.Date();
}
}else{
if(_6f6=="NUMBER"){
_6f4=new nitobi.form.Number();
}else{
_6f4=new nitobi.form.Text();
}
}
break;
}
_6f4.initialize();
}
this.editors[_6f7]=_6f4;
return _6f4;
};
nitobi.form.ControlFactory.prototype.dispose=function(){
for(var _6f8 in this.editors){
this.editors[_6f8].dispose();
}
};
nitobi.form.ControlFactory.instance=new nitobi.form.ControlFactory();
nitobi.form.Link=function(){
};
nitobi.lang.extend(nitobi.form.Link,nitobi.form.Control);
nitobi.form.Link.prototype.initialize=function(){
this.url="";
};
nitobi.form.Link.prototype.bind=function(_6f9,cell){
nitobi.form.Link.base.bind.apply(this,arguments);
this.url=this.cell.getValue();
};
nitobi.form.Link.prototype.mimic=function(){
if(false==eval(this.owner.getOnCellValidateEvent())){
return;
}
this.click();
this.deactivate();
};
nitobi.form.Link.prototype.deactivate=function(){
this.afterDeactivate(this.value);
};
nitobi.form.Link.prototype.click=function(){
this.control=window.open(this.url);
this.value=this.url;
};
nitobi.form.Link.prototype.hide=function(){
};
nitobi.form.Link.prototype.attachToParent=function(){
};
nitobi.form.Link.prototype.dispose=function(){
this.metadata=null;
this.owner=null;
this.context=null;
};
nitobi.form.ListBox=function(){
nitobi.form.ListBox.baseConstructor.call(this);
var ph=this.placeholder;
ph.setAttribute("id","listbox_span");
ph.style.top="-1000px";
ph.style.left="-1000px";
this.metadata=null;
this.keypress=false;
this.typedString=null;
this.events=[{type:"change",handler:this.deactivate},{type:"keydown",handler:this.handleKey},{type:"keyup",handler:this.handleKeyUp},{type:"keypress",handler:this.handleKeyPress},{type:"blur",handler:this.deactivate}];
};
nitobi.lang.extend(nitobi.form.ListBox,nitobi.form.Control);
nitobi.form.ListBox.prototype.initialize=function(){
};
nitobi.form.ListBox.prototype.bind=function(_6fc,cell){
nitobi.form.ListBox.base.bind.apply(this,arguments);
var _6fe=cell.getColumnObject().getModel();
var _6ff=_6fe.getAttribute("DatasourceId");
this.dataTable=this.owner.data.getTable(_6ff);
this.eSET("onKeyPress",[_6fe.getAttribute("OnKeyPressEvent")]);
this.eSET("onKeyDown",[_6fe.getAttribute("OnKeyDownEvent")]);
this.eSET("onKeyUp",[_6fe.getAttribute("OnKeyUpEvent")]);
this.eSET("onChange",[_6fe.getAttribute("OnChangeEvent")]);
this.bindComplete(cell.getValue());
};
nitobi.form.ListBox.prototype.bindComplete=function(_700){
var _701=this.dataTable.xmlDoc.selectSingleNode("//"+nitobi.xml.nsPrefix+"datasource[@id='"+this.dataTable.id+"']");
var _702=this.cell.getColumnObject();
var _703=_702.getModel();
var _704=_703.getAttribute("DisplayFields");
var _705=_703.getAttribute("ValueField");
var xsl=nitobi.form.listboxXslProc;
xsl.addParameter("DisplayFields",_704,"");
xsl.addParameter("ValueField",_705,"");
xsl.addParameter("val",_700,"");
this.listXml=nitobi.xml.transformToXml(nitobi.xml.createXmlDoc(_701.xml),xsl);
this.placeholder.rows[0].cells[0].innerHTML=nitobi.xml.serialize(this.listXml);
var tc=this.control=nitobi.html.getFirstChild(this.placeholder.rows[0].cells[0]);
tc.style.width="100%";
tc.style.height=(this.cell.DomNode.offsetHeight-2)+"px";
nitobi.html.attachEvents(tc,this.events,this);
nitobi.html.Css.addClass(tc.className,this.cell.getDomNode().className);
this.align();
this.focus();
if(typeof (_700)!="undefined"&&_700!=null&&_700!=""){
return this.searchComplete(_700);
}
};
nitobi.form.ListBox.prototype.deactivate=function(ok){
if(this.keypress){
this.keypress=false;
return;
}
if(nitobi.form.ListBox.base.deactivate.apply(this,arguments)==false){
return;
}
if(this.onChange.notify(this)==false){
return;
}
var c=this.control;
var text="",_70b="";
if(ok||ok==null){
text=c.options[c.selectedIndex].text;
_70b=c.options[c.selectedIndex].value;
}else{
_70b=this.cell.getValue();
var len=c.options.length;
for(var i=0;i<len;i++){
if(c.options[i].value==_70b){
text=c.options[i].text;
}
}
}
this.typedString=null;
return this.afterDeactivate(nitobi.html.encode(text),_70b);
};
nitobi.form.ListBox.prototype.handleKey=function(evt){
var k=evt.keyCode;
this.keypress=false;
var K=nitobi.form.Keys;
switch(k){
case K.DOWN:
if(this.control.selectedIndex<this.control.options.length-1){
this.keypress=true;
}
break;
case K.UP:
if(this.control.selectedIndex>0){
this.keypress=true;
}
break;
case K.ENTER:
case K.TAB:
case K.ESC:
return nitobi.form.ListBox.base.handleKey.call(this,evt);
default:
nitobi.html.cancelEvent(evt);
return this.searchComplete(String.fromCharCode(k));
}
};
nitobi.form.ListBox.prototype.searchComplete=function(_711,_712){
if(typeof (_712)!="undefined"&&_712!=""){
this.typedString=_712;
this.maxLinearSearch=500;
}else{
this.typedString=this.typedString+_711;
}
var c=this.control;
var _714=c.options.length;
if(_714>this.maxLinearSearch){
var _715=this.searchBinary(this.typedString,0,(_714-1));
if(_715){
for(i=_715;i>0;i--){
if(c.options[i].text.toLowerCase().substr(0,this.typedString.length)!=this.typedString.toLowerCase()){
c.selectedIndex=i+1;
break;
}
}
}
}else{
for(i=1;i<_714;i++){
if(c.options[i].text.toLowerCase().substr(0,this.typedString.length)==this.typedString.toLowerCase()){
c.selectedIndex=i;
break;
}
}
}
clearTimeout(this.timerid);
var _716=this;
this.timerid=setTimeout(function(){
_716.typedString="";
},1000);
return false;
};
nitobi.form.ListBox.prototype.searchBinary=function(_717,low,high){
if(low>high){
return null;
}
var c=this.control;
var mid=Math.floor((high+low)/2);
var _71c=c.options[mid].text.toLowerCase().substr(0,_717.length);
var _71d=_717.toLowerCase();
if(_71d==_71c){
return mid;
}else{
if(_71d<_71c){
return this.searchBinary(_717,low,(mid-1));
}else{
if(_71d>_71c){
return this.searchBinary(_717,(mid+1),high);
}else{
return null;
}
}
}
};
nitobi.form.ListBox.prototype.dispose=function(){
nitobi.html.detachEvents(this.control,this.events);
this.placeholder=null;
this.control=null;
this.listXml=null;
this.element=null;
this.metadata=null;
this.owner=null;
};
nitobi.form.Lookup=function(){
nitobi.form.Lookup.baseConstructor.call(this);
this.selectClicked=false;
this.bVisible=false;
var div=nitobi.html.createElement("div");
div.innerHTML="<table class='ntb-input-border' border='0' cellpadding='0' cellspacing='0'><tr><td class=\"ntb-lookup-text\"></td></tr><tr><td style=\"position:relative;\"><div style=\"position:relative;top:0px;left:0px;\"></div></td></tr></table>";
var ph=this.placeholder=div.firstChild;
ph.setAttribute("id","lookup_span");
ph.style.top="-2000px";
ph.style.left="-2000px";
var tc=this.textControl=nitobi.html.createElement("input",{autocomplete:"off"},{zIndex:"2000",width:"100px"});
tc.setAttribute("id","ntb-lookup-text");
this.control=tc;
this.textEvents=[{"type":"keydown","handler":this.handleKey},{"type":"keyup","handler":this.filter},{"type":"keypress","handler":this.handleKeyPress},{"type":"change","handler":this.handleChange}];
ph.rows[0].cells[0].appendChild(tc);
this.selectPlaceholder=ph.rows[1].cells[0].firstChild;
this.selectEvents=[{"type":"click","handler":this.handleSelectClicked}];
this.firstKeyup=false;
this.autocompleted=false;
this.referenceColumn=null;
this.autoComplete=null;
this.autoClear=null;
this.getOnEnter=null;
this.listXml=null;
this.listXmlLower=null;
this.editCompleteHandler=null;
this.delay=0;
this.timeoutId=null;
var xsl="<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">";
xsl+="<xsl:output method=\"text\" version=\"4.0\"/><xsl:param name='searchValue'/>";
xsl+="<xsl:template match=\"/\"><xsl:apply-templates select='//option[starts-with(.,$searchValue)][1]' /></xsl:template>";
xsl+="<xsl:template match=\"option\"><xsl:value-of select='@rn' /></xsl:template></xsl:stylesheet>";
var _722=nitobi.xml.createXslDoc(xsl);
this.searchXslProc=nitobi.xml.createXslProcessor(_722);
_722=null;
};
nitobi.lang.extend(nitobi.form.Lookup,nitobi.form.Control);
nitobi.lang.implement(nitobi.form.Lookup,nitobi.ui.IDataBoundList);
nitobi.lang.implement(nitobi.form.Lookup,nitobi.form.IBlurable);
nitobi.form.Lookup.prototype.initialize=function(){
this.firstKeyup=false;
nitobi.html.attachEvents(this.textControl,this.textEvents,this);
nitobi.html.attachEvents(this.selectPlaceholder,this.selectEvents,this);
};
nitobi.form.Lookup.prototype.hideSelect=function(){
this.selectControl.style.display="none";
this.bVisible=false;
};
nitobi.form.Lookup.prototype.bind=function(_723,cell,_725){
nitobi.form.Lookup.base.bind.apply(this,arguments);
var col=this.column=this.cell.getColumnObject();
var _727=this.column.getModel();
this.datasourceId=col.getDatasourceId();
this.getHandler=col.getGetHandler();
this.delay=col.getDelay();
this.size=col.getSize();
this.referenceColumn=col.getReferenceColumn();
this.autoComplete=col.isAutoComplete();
this.autoClear=col.isAutoClear();
this.getOnEnter=col.isGetOnEnter();
this.displayFields=col.getDisplayFields();
this.valueField=col.getValueField();
this.eSET("onKeyPress",[col.getOnKeyPressEvent()]);
this.eSET("onKeyDown",[col.getOnKeyDownEvent()]);
this.eSET("onKeyUp",[col.getOnKeyUpEvent()]);
this.eSET("onChange",[col.getOnChangeEvent()]);
var _728=nitobi.form.listboxXslProc;
_728.addParameter("DisplayFields",this.displayFields,"");
_728.addParameter("ValueField",this.valueField,"");
this.dataTable=this.owner.data.getTable(this.datasourceId);
this.dataTable.setGetHandler(this.getHandler);
this.dataTable.async=false;
if(_725.length<=0){
_725=this.cell.getValue();
}
this.get(_725,true);
};
nitobi.form.Lookup.prototype.bindComplete=function(_729){
var _72a=this.dataTable.getXmlDoc();
var _72b=nitobi.form.listboxXslProc;
_72b.addParameter("DisplayFields",this.displayFields,"");
_72b.addParameter("ValueField",this.valueField,"");
_72b.addParameter("val",nitobi.xml.constructValidXpathQuery(this.cell.getValue(),false),"");
if(nitobi.browser.IE&&document.compatMode=="CSS1Compat"){
_72b.addParameter("size",6,"");
}
this.listXml=nitobi.xml.transformToXml(nitobi.xml.createXmlDoc(_72a.xml),nitobi.form.listboxXslProc);
this.listXmlLower=nitobi.xml.createXmlDoc(this.listXml.xml.toLowerCase());
if(nitobi.browser.IE&&document.compatMode=="CSS1Compat"){
_72b.addParameter("size","","");
}
this.selectPlaceholder.innerHTML=nitobi.xml.serialize(this.listXml);
var tc=this.textControl;
var sc=this.selectControl=nitobi.html.getFirstChild(this.selectPlaceholder);
sc.setAttribute("id","ntb-lookup-options");
sc.setAttribute("size",this.size);
sc.style.display="none";
if(nitobi.browser.IE6&&document.compatMode!="CSS1Compat"){
sc.style.height="100%";
}
nitobi.form.IBlurable.call(this,[tc,sc],this.deactivate);
this.selectClicked=false;
this.bVisible=false;
this.align();
nitobi.html.fitWidth(this.placeholder,tc);
if(this.autoComplete){
var rn=this.search(_729);
if(rn>0){
sc.selectedIndex=rn-1;
tc.value=sc[sc.selectedIndex].text;
nitobi.html.highlight(tc,tc.value.length-(tc.value.length-_729.length));
this.autocompleted=true;
}else{
var row=_72a.selectSingleNode("//"+nitobi.xml.nsPrefix+"e[@"+this.valueField+"='"+_729+"']");
if(row!=null){
tc.value=row.getAttribute(this.displayFields);
var rn=this.search(tc.value);
sc.selectedIndex=parseInt(rn)-1;
}else{
tc.value=_729;
sc.selectedIndex=-1;
}
}
}else{
tc.value=_729;
sc.selectedIndex=-1;
}
tc.parentNode.style.height=nitobi.html.getHeight(this.cell.getDomNode())+"px";
sc.style.display="inline";
tc.focus();
};
nitobi.form.Lookup.prototype.handleSelectClicked=function(evt){
this.textControl.value=this.selectControl.selectedIndex!=-1?this.selectControl.options[this.selectControl.selectedIndex].text:"";
this.deactivate(evt);
};
nitobi.form.Lookup.prototype.focus=function(evt){
this.textControl.focus();
};
nitobi.form.Lookup.prototype.deactivate=function(evt){
if(nitobi.form.Lookup.base.deactivate.apply(this,arguments)==false){
return;
}
var sc=this.selectControl;
var tc=this.textControl;
var text="",_736="";
if(evt!=null&&evt!=false){
if(sc.selectedIndex>=0){
_736=sc.options[sc.selectedIndex].value;
text=sc.options[sc.selectedIndex].text;
}else{
if(this.column.getModel().getAttribute("ForceValidOption")!="true"){
_736=tc.value;
text=_736;
}else{
if(this.autoClear){
_736="";
text="";
}else{
_736=this.cell.getValue();
var len=sc.options.length;
for(var i=0;i<len;i++){
if(sc.options[i].value==_736){
text=sc.options[i].text;
}
}
}
}
}
}else{
_736=this.cell.getValue();
var len=sc.options.length;
var _739=false;
for(var i=0;i<len;i++){
if(sc.options[i].value==_736){
text=sc.options[i].text;
_739=true;
break;
}
}
if(!_739&&this.autoClear){
_736="";
text="";
}
}
nitobi.html.detachEvents(sc,this.textEvents);
window.clearTimeout(this.timeoutId);
return this.afterDeactivate(nitobi.html.encode(text),_736);
};
nitobi.form.Lookup.prototype.handleKey=function(evt,_73b){
var k=evt.keyCode;
if(k!=40&&k!=38){
nitobi.form.Lookup.base.handleKey.call(this,evt);
}
};
nitobi.form.Lookup.prototype.search=function(_73d){
_73d=nitobi.xml.constructValidXpathQuery(_73d,false);
this.searchXslProc.addParameter("searchValue",_73d.toLowerCase(),"");
var _73e=nitobi.xml.transformToString(this.listXmlLower,this.searchXslProc);
if(""==_73e){
_73e=0;
}else{
_73e=parseInt(_73e);
}
return _73e;
};
nitobi.form.Lookup.prototype.filter=function(evt,o){
var k=evt.keyCode;
if(this.onKeyUp.notify(evt)==false){
return;
}
if(!this.firstKeyup&&k!=38&&k!=40){
this.firstKeyup=true;
return;
}
var tc=this.textControl;
var sc=this.selectControl;
switch(k){
case 38:
if(sc.selectedIndex==-1){
sc.selectedIndex=0;
}
if(sc.selectedIndex>0){
sc.selectedIndex--;
}
tc.value=sc.options[sc.selectedIndex].text;
nitobi.html.highlight(tc,tc.value.length);
tc.select();
break;
case 40:
if(sc.selectedIndex<(sc.length-1)){
sc.selectedIndex++;
}
tc.value=sc.options[sc.selectedIndex].text;
nitobi.html.highlight(tc,tc.value.length);
tc.select();
break;
default:
if((!this.getOnEnter&&((k<193&&k>46)||k==8||k==32))||(this.getOnEnter&&k==13)){
var _744=tc.value;
this.get(_744);
}
}
};
nitobi.form.Lookup.prototype.get=function(_745,_746){
if(this.getHandler!=null&&this.getHandler!=""){
if(_746||!this.delay){
this.doGet(_745);
}else{
if(this.timeoutId){
window.clearTimeout(this.timeoutId);
this.timeoutId=null;
}
this.timeoutId=window.setTimeout(nitobi.lang.close(this,this.doGet,[_745]),this.delay);
}
}else{
this.bindComplete(_745);
}
};
nitobi.form.Lookup.prototype.doGet=function(_747){
if(_747){
this.dataTable.setGetHandlerParameter("SearchString",_747);
}
if(this.referenceColumn!=null&&this.referenceColumn!=""){
var _748=this.owner.getCellValue(this.cell.row,this.referenceColumn);
this.dataTable.setGetHandlerParameter("ReferenceColumn",_748);
}
this.dataTable.get(null,this.pageSize,this);
this.timeoutId=null;
this.bindComplete(_747);
};
nitobi.form.Lookup.prototype.dispose=function(){
this.placeholder=null;
nitobi.html.detachEvents(this.textEvents,this);
this.selectControl=null;
this.textControl=null;
this.dataTable=null;
this.owner=null;
};
nitobi.form.Number=function(){
nitobi.form.Number.baseConstructor.call(this);
this.defaultValue=0;
};
nitobi.lang.extend(nitobi.form.Number,nitobi.form.Text);
nitobi.form.Number.prototype.handleKey=function(evt){
nitobi.form.Number.base.handleKey.call(this,evt);
var k=evt.keyCode;
if(!this.isValidKey(k)){
nitobi.html.cancelEvent(evt);
return false;
}
};
nitobi.form.Number.prototype.isValidKey=function(k){
if((k<48||k>57)&&(k<37||k>40)&&(k<96||k>105)&&k!=190&&k!=110&&k!=189&&k!=109&&k!=9&&k!=45&&k!=46&&k!=8){
return false;
}
return true;
};
nitobi.form.Number.prototype.bind=function(_74c,cell,_74e){
var _74f=_74e.charCodeAt(0);
if(_74f>=97){
_74f=_74f-32;
}
var k=this.isValidKey(_74f)?_74e:"";
nitobi.form.Number.base.bind.call(this,_74c,cell,k);
};
nitobi.form.Password=function(){
nitobi.form.Password.baseConstructor.call(this,true);
this.control.type="password";
};
nitobi.lang.extend(nitobi.form.Password,nitobi.form.Text);
nitobi.form.TextArea=function(){
nitobi.form.TextArea.baseConstructor.call(this);
var div=nitobi.html.createElement("div");
div.innerHTML="<table border='0' cellpadding='0' cellspacing='0' class='ntb-input-border'><tr><td></td></table>";
var ph=this.placeholder=div.firstChild;
ph.style.top="-3000px";
ph.style.left="-3000px";
this.control=nitobi.html.createElement("textarea",{},{width:"100px"});
};
nitobi.lang.extend(nitobi.form.TextArea,nitobi.form.Text);
nitobi.form.TextArea.prototype.initialize=function(){
this.placeholder.rows[0].cells[0].appendChild(this.control);
document.body.appendChild(this.placeholder);
nitobi.html.attachEvents(this.control,this.events,this);
};
nitobi.form.TextArea.prototype.mimic=function(){
nitobi.form.TextArea.base.mimic.call(this);
var phs=this.placeholder.style;
};
nitobi.form.TextArea.prototype.handleKey=function(evt){
var k=evt.keyCode;
if(k==40||k==38||k==37||k==39||(k==13&&evt.shiftKey)){
}else{
nitobi.form.TextArea.base.handleKey.call(this,evt);
}
};
nitobi.form.Calendar=function(){
nitobi.form.Calendar.baseConstructor.call(this);
var div=nitobi.html.createElement("div");
div.innerHTML="<table border='0' cellpadding='0' cellspacing='0' style='table-layout:fixed;' class='ntb-input-border'><tr><td>"+"<input id='ntb-datepicker-input' type='text' maxlength='255' style='width:100%;' />"+"</td><td class='ntb-datepicker-button'><a id='ntb-datepicker-button' href='#' onclick='return false;'></a></td></tr><tr><td colspan='2' style='width:1px;height:1px;position:relative;'><!-- --></td></tr><colgroup><col></col><col style='width:20px;'></col></colgroup></table>";
this.control=div.getElementsByTagName("input")[0];
var ph=this.placeholder=div.firstChild;
ph.setAttribute("id","calendar_span");
ph.style.top="-3000px";
ph.style.left="-3000px";
var pd=this.pickerDiv=nitobi.html.createElement("div",{},{position:"absolute"});
this.isPickerVisible=false;
nitobi.html.Css.addClass(pd,NTB_CSS_HIDE);
ph.rows[1].cells[0].appendChild(pd);
};
nitobi.lang.extend(nitobi.form.Calendar,nitobi.form.Control);
nitobi.form.Calendar.prototype.initialize=function(){
this.datePicker=new nitobi.calendar.DatePicker(nitobi.component.getUniqueId());
this.datePicker.setContainer(this.pickerDiv);
this.datePicker.onSetDate.subscribe(this.handlePick,this);
var tc=this.control;
var H=nitobi.html;
H.attachEvent(tc,"keydown",this.handleKey,this,false);
H.attachEvent(tc,"blur",this.deactivate,this,false);
H.attachEvent(this.pickerDiv,"mousedown",this.handleCalendarMouseDown,this);
H.attachEvent(this.pickerDiv,"mouseup",this.handleCalendarMouseUp,this);
var a=this.placeholder.getElementsByTagName("a")[0];
H.attachEvent(a,"mousedown",this.handleClick,this);
H.attachEvent(a,"mouseup",this.handleMouseUp,this);
};
nitobi.form.Calendar.prototype.bind=function(_75c,cell,_75e){
this.isPickerVisible=false;
nitobi.html.Css.addClass(this.pickerDiv,NTB_CSS_HIDE);
nitobi.form.Calendar.base.bind.apply(this,arguments);
if(_75e!=null&&_75e!=""){
this.control.value=_75e;
}else{
this.control.value=cell.getValue();
}
this.column=this.cell.getColumnObject();
this.control.maxlength=this.column.getModel().getAttribute("MaxLength");
};
nitobi.form.Calendar.prototype.mimic=function(){
this.align();
var _75f=this.placeholder.offsetWidth;
var _760=this.placeholder.rows[0].cells[1].offsetWidth;
this.control.style.width=_75f-_760-(document.compatMode=="BackCompat"?0:8)+"px";
this.selectText();
};
nitobi.form.Calendar.prototype.deactivate=function(){
if(nitobi.form.Calendar.base.deactivate.apply(this,arguments)==false){
return;
}
this.afterDeactivate(this.control.value);
};
nitobi.form.Calendar.prototype.handleClick=function(evt){
if(!this.isPickerVisible){
this.datePicker.setDate(nitobi.base.DateMath.parseIso8601(this.control.value));
this.datePicker.render();
}
this.ignoreBlur=true;
nitobi.ui.Effects.setVisible(this.pickerDiv,!this.isPickerVisible,"none",this.setVisibleComplete,this);
};
nitobi.form.Calendar.prototype.handleMouseUp=function(evt){
this.control.focus();
this.ignoreBlur=false;
};
nitobi.form.Calendar.prototype.handleCalendarMouseDown=function(evt){
this.ignoreBlur=true;
};
nitobi.form.Calendar.prototype.handleCalendarMouseUp=function(evt){
this.handleMouseUp(evt);
};
nitobi.form.Calendar.prototype.setVisibleComplete=function(){
this.isPickerVisible=!this.isPickerVisible;
};
nitobi.form.Calendar.prototype.handlePick=function(){
var date=this.datePicker.getDate();
var _766=nitobi.base.DateMath.toIso8601(date);
this.control.value=_766;
this.datePicker.hide();
};
nitobi.form.Calendar.prototype.dispose=function(){
nitobi.html.detachEvent(this.control,"keydown",this.handleKey);
nitobi.html.detachEvent(this.control,"blur",this.deactivate);
var _767=this.placeholder.parentNode;
_767.removeChild(this.placeholder);
this.control=null;
this.placeholder=null;
this.owner=null;
this.cell=null;
};
nitobi.lang.defineNs("nitobi.form");
nitobi.form.Keys={UP:38,DOWN:40,ENTER:13,TAB:9,ESC:27};
nitobi.ui.UiElement=function(xml,xsl,id){
if(arguments.length>0){
this.initialize(xml,xsl,id);
}
};
nitobi.ui.UiElement.prototype.initialize=function(xml,xsl,id){
this.m_Xml=xml;
this.m_Xsl=xsl;
this.m_Id=id;
this.m_HtmlElementHandle=null;
};
nitobi.ui.UiElement.prototype.getHeight=function(){
return this.getHtmlElementHandle().style.height;
};
nitobi.ui.UiElement.prototype.setHeight=function(_76e){
this.getHtmlElementHandle().style.height=_76e+"px";
};
nitobi.ui.UiElement.prototype.getId=function(){
return this.m_Id;
};
nitobi.ui.UiElement.prototype.setId=function(id){
this.m_Id=id;
};
nitobi.ui.UiElement.prototype.getWidth=function(){
return this.getHtmlElementHandle().style.width;
};
nitobi.ui.UiElement.prototype.setWidth=function(_770){
this.getHtmlElementHandle().style.width=_770+"px";
};
nitobi.ui.UiElement.prototype.getXml=function(){
return this.m_Xml;
};
nitobi.ui.UiElement.prototype.setXml=function(xml){
this.m_Xml=xml;
};
nitobi.ui.UiElement.prototype.getXsl=function(){
return this.m_Xsl;
};
nitobi.ui.UiElement.prototype.setXsl=function(xsl){
this.m_Xsl=xsl;
};
nitobi.ui.UiElement.prototype.getHtmlElementHandle=function(){
if(!this.m_HtmlElementHandle){
this.m_HtmlElementHandle=document.getElementById(this.m_Id);
}
return this.m_HtmlElementHandle;
};
nitobi.ui.UiElement.prototype.setHtmlElementHandle=function(_773){
this.m_HtmlElementHandle=_773;
};
nitobi.ui.UiElement.prototype.hide=function(){
var tag=this.getHtmlElementHandle();
tag.style.visibility="hidden";
tag.style.position="absolute";
};
nitobi.ui.UiElement.prototype.show=function(){
var tag=this.getHtmlElementHandle();
tag.style.visibility="visible";
};
nitobi.ui.UiElement.prototype.isVisible=function(){
var tag=this.getHtmlElementHandle();
return tag.style.visibility=="visible";
};
nitobi.ui.UiElement.prototype.beginFloatMode=function(){
var tag=this.getHtmlElementHandle();
tag.style.position="absolute";
};
nitobi.ui.UiElement.prototype.isFloating=function(){
var tag=this.getHtmlElementHandle();
return tag.style.position=="absolute";
};
nitobi.ui.UiElement.prototype.setX=function(x){
var tag=this.getHtmlElementHandle();
tag.style.left=x+"px";
};
nitobi.ui.UiElement.prototype.getX=function(){
var tag=this.getHtmlElementHandle();
return tag.style.left;
};
nitobi.ui.UiElement.prototype.setY=function(y){
var tag=this.getHtmlElementHandle();
tag.style.top=y+"px";
};
nitobi.ui.UiElement.prototype.getY=function(){
var tag=this.getHtmlElementHandle();
return tag.style.top;
};
nitobi.ui.UiElement.prototype.render=function(_77f,_780,_781){
var xsl=this.m_Xsl;
if(xsl!=null&&xsl.indexOf("xsl:stylesheet")==-1){
xsl="<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"><xsl:output method=\"html\" version=\"4.0\" />"+xsl+"</xsl:stylesheet>";
}
if(null==_780){
_780=nitobi.xml.createXslDoc(xsl);
}
if(null==_781){
_781=nitobi.xml.createXmlDoc(this.m_Xml);
}
Eba.Error.assert(nitobi.xml.isValidXml(_781),"Tried to render invalid XML according to Mozilla. The XML is "+_781.xml);
var html=nitobi.xml.transform(_781,_780);
if(html.xml){
html=html.xml;
}
if(null==_77f){
document.body.insertAdjacentHTML("beforeEnd",html);
}else{
_77f.innerHTML=html;
}
this.attachToTag();
};
nitobi.ui.UiElement.prototype.attachToTag=function(){
var _784=this.getHtmlElementHandle();
if(_784!=null){
_784.object=this;
_784.jsobject=this;
_784.javascriptObject=this;
}
};
nitobi.ui.UiElement.prototype.dispose=function(){
var _785=this.getHtmlElementHandle();
if(_785!=null){
_785.object=null;
}
this.m_Xml=null;
this.m_Xsl=null;
this.m_HtmlElementHandle=null;
};
nitobi.ui.InteractiveUiElement=function(_786){
this.enable();
};
nitobi.lang.extend(nitobi.ui.InteractiveUiElement,nitobi.ui.UiElement);
nitobi.ui.InteractiveUiElement.prototype.enable=function(){
this.m_Enabled=true;
};
nitobi.ui.InteractiveUiElement.prototype.disable=function(){
this.m_Enabled=false;
};
nitobi.ui.ButtonXsl="<xsl:template match=\"button\">"+"<div class=\"ntb-button\" onmousemove=\"return false;\" onmousedown=\"if (this.object.m_Enabled) this.className='ntb-button-down';\" onmouseup=\"this.className='ntb-button';\" onmouseover=\"if (this.object.m_Enabled) this.className='ntb-button-highlight';\" onmouseout=\"this.className='ntb-button';\" align=\"center\">"+"<xsl:attribute name=\"image_disabled\">"+"<xsl:choose>"+"<xsl:when test=\"../../@image_directory\">"+"<xsl:value-of select=\"concat(../../@image_directory,@image_disabled)\" />"+"</xsl:when>"+"<xsl:otherwise>"+"<xsl:value-of select=\"@image_disabled\" />"+"</xsl:otherwise>"+"</xsl:choose>"+"</xsl:attribute>"+"<xsl:attribute name=\"image_enabled\">"+"<xsl:choose>"+"<xsl:when test=\"../../@image_directory\">"+"<xsl:value-of select=\"concat(../../@image_directory,@image)\" />"+"</xsl:when>"+"<xsl:otherwise>"+"<xsl:value-of select=\"@image\" />"+"</xsl:otherwise>"+"</xsl:choose>"+"</xsl:attribute>"+"<xsl:attribute name=\"title\">"+"<xsl:value-of select=\"@tooltip_text\" />"+"</xsl:attribute>"+"<xsl:attribute name=\"onclick\">"+"<xsl:value-of select='concat(&quot;v&quot;,&quot;a&quot;,&quot;r&quot;,&quot; &quot;,&quot;e&quot;,&quot;=&quot;,&quot;&apos;&quot;,@onclick_event,&quot;&apos;&quot;,&quot;;&quot;,&quot;e&quot;,&quot;v&quot;,&quot;a&quot;,&quot;l&quot;,&quot;(&quot;,&quot;t&quot;,&quot;h&quot;,&quot;i&quot;,&quot;s&quot;,&quot;.&quot;,&quot;o&quot;,&quot;b&quot;,&quot;j&quot;,&quot;e&quot;,&quot;c&quot;,&quot;t&quot;,&quot;.&quot;,&quot;o&quot;,&quot;n&quot;,&quot;C&quot;,&quot;l&quot;,&quot;i&quot;,&quot;c&quot;,&quot;k&quot;,&quot;H&quot;,&quot;a&quot;,&quot;n&quot;,&quot;d&quot;,&quot;l&quot;,&quot;e&quot;,&quot;r&quot;,&quot;(&quot;,&quot;e&quot;,&quot;)&quot;,&quot;)&quot;,&quot;;&quot;,&apos;&apos;)' />"+"</xsl:attribute>"+"<xsl:attribute name=\"id\">"+"<xsl:value-of select=\"@id\" />"+"</xsl:attribute>"+"<xsl:attribute name=\"style\">"+"<xsl:choose>"+"<xsl:when test=\"../../@height\">"+"<xsl:value-of select=\"concat('float:left;width:',../../@height,'px;height:',../../@height - 1,'px')\" />"+"</xsl:when>"+"<xsl:otherwise>"+"<xsl:value-of select=\"concat('float:left;width:',@width,'px;height:',@height,'px')\" />"+"</xsl:otherwise>"+"</xsl:choose>"+"</xsl:attribute>"+"<img border=\"0\">"+"<xsl:attribute name=\"src\">"+"<xsl:choose>"+"<xsl:when test=\"../../@image_directory\">"+"<xsl:value-of select=\"concat(../../@image_directory,@image)\" />"+"</xsl:when>"+"<xsl:otherwise>"+"<xsl:value-of select=\"@image\" />"+"</xsl:otherwise>"+"</xsl:choose>"+"</xsl:attribute>"+"<xsl:attribute name=\"style\">"+"<xsl:variable name=\"top_offset\">"+"<xsl:choose>"+"<xsl:when test=\"@top_offset\">"+"<xsl:value-of select=\"@top_offset\" />"+"</xsl:when>"+"<xsl:otherwise>"+"0"+"</xsl:otherwise>"+"</xsl:choose>"+"</xsl:variable>"+"<xsl:choose>"+"<xsl:when test=\"../../@height\">"+"<xsl:value-of select=\"concat('MARGIN-TOP:',((../../@height - @height) div 2) - 1 + number($top_offset),'px;MARGIN-BOTTOM:0px')\" />"+"</xsl:when>"+"<xsl:otherwise>"+"<xsl:value-of select=\"concat('MARGIN-TOP:',(@height - @image_height) div 2,'px;MARGIN-BOTTOM:0','px')\" />"+"</xsl:otherwise>"+"</xsl:choose>"+"</xsl:attribute>"+"</img><![CDATA[ ]]>"+"</div>"+"</xsl:template>";
nitobi.ui.Button=function(xml,id){
this.initialize(xml,nitobi.ui.ButtonXsl,id);
this.enable();
};
nitobi.lang.extend(nitobi.ui.Button,nitobi.ui.InteractiveUiElement);
nitobi.ui.Button.prototype.onClickHandler=function(_789){
if(this.m_Enabled){
eval(_789);
}
};
nitobi.ui.Button.prototype.disable=function(){
nitobi.ui.Button.base.disable.call(this);
var _78a=this.getHtmlElementHandle();
_78a.childNodes[0].src=_78a.getAttribute("image_disabled");
};
nitobi.ui.Button.prototype.enable=function(){
nitobi.ui.Button.base.enable.call(this);
var _78b=this.getHtmlElementHandle();
_78b.childNodes[0].src=_78b.getAttribute("image_enabled");
};
nitobi.ui.Button.prototype.dispose=function(){
nitobi.ui.Button.base.dispose.call(this);
};
nitobi.ui.BinaryStateButtonXsl="<xsl:template match=\"binarystatebutton\">"+"<div class=\"ntb-binarybutton\" onmousemove=\"return false;\" onmousedown=\"if (this.object.m_Enabled) this.className='ntb-button-down';\" onmouseup=\"(this.object.isChecked()?this.object.check():this.object.uncheck())\" onmouseover=\"if (this.object.m_Enabled) this.className='ntb-button-highlight';\" onmouseout=\"(this.object.isChecked()?this.object.check():this.object.uncheck())\" align=\"center\">"+"<xsl:attribute name=\"image_disabled\">"+"<xsl:choose>"+"<xsl:when test=\"../../@image_directory\">"+"<xsl:value-of select=\"concat(../../@image_directory,@image_disabled)\" />"+"</xsl:when>"+"<xsl:otherwise>"+"<xsl:value-of select=\"@image_disabled\" />"+"</xsl:otherwise>"+"</xsl:choose>"+"</xsl:attribute>"+"<xsl:attribute name=\"image_enabled\">"+"<xsl:choose>"+"<xsl:when test=\"../../@image_directory\">"+"<xsl:value-of select=\"concat(../../@image_directory,@image)\" />"+"</xsl:when>"+"<xsl:otherwise>"+"<xsl:value-of select=\"@image\" />"+"</xsl:otherwise>"+"</xsl:choose>"+"</xsl:attribute>"+"<xsl:attribute name=\"title\">"+"<xsl:value-of select=\"@tooltip_text\" />"+"</xsl:attribute>"+"<xsl:attribute name=\"onclick\">"+"<xsl:value-of select='concat(\"this.object.toggle();\",&quot;v&quot;,&quot;a&quot;,&quot;r&quot;,&quot; &quot;,&quot;e&quot;,&quot;=&quot;,&quot;&apos;&quot;,@onclick_event,&quot;&apos;&quot;,&quot;;&quot;,&quot;e&quot;,&quot;v&quot;,&quot;a&quot;,&quot;l&quot;,&quot;(&quot;,&quot;t&quot;,&quot;h&quot;,&quot;i&quot;,&quot;s&quot;,&quot;.&quot;,&quot;o&quot;,&quot;b&quot;,&quot;j&quot;,&quot;e&quot;,&quot;c&quot;,&quot;t&quot;,&quot;.&quot;,&quot;o&quot;,&quot;n&quot;,&quot;C&quot;,&quot;l&quot;,&quot;i&quot;,&quot;c&quot;,&quot;k&quot;,&quot;H&quot;,&quot;a&quot;,&quot;n&quot;,&quot;d&quot;,&quot;l&quot;,&quot;e&quot;,&quot;r&quot;,&quot;(&quot;,&quot;e&quot;,&quot;)&quot;,&quot;)&quot;,&quot;;&quot;,&apos;&apos;)' />"+"</xsl:attribute>"+"<xsl:attribute name=\"id\">"+"<xsl:value-of select=\"@id\" />"+"</xsl:attribute>"+"<xsl:attribute name=\"style\">"+"<xsl:choose>"+"<xsl:when test=\"../../@height\">"+"<xsl:value-of select=\"concat('float:left;width:',../../@height,'px;height:',../../@height - 1,'px')\" />"+"</xsl:when>"+"<xsl:otherwise>"+"<xsl:value-of select=\"concat('float:left;width:',@width,'px;height:',@height,'px')\" />"+"</xsl:otherwise>"+"</xsl:choose>"+"</xsl:attribute>"+"<img border=\"0\">"+"<xsl:attribute name=\"src\">"+"<xsl:choose>"+"<xsl:when test=\"../../@image_directory\">"+"<xsl:value-of select=\"concat(../../@image_directory,@image)\" />"+"</xsl:when>"+"<xsl:otherwise>"+"<xsl:value-of select=\"@image\" />"+"</xsl:otherwise>"+"</xsl:choose>"+"</xsl:attribute>"+"<xsl:attribute name=\"style\">"+"<xsl:variable name=\"top_offset\">"+"<xsl:choose>"+"<xsl:when test=\"@top_offset\">"+"<xsl:value-of select=\"@top_offset\" />"+"</xsl:when>"+"<xsl:otherwise>"+"0"+"</xsl:otherwise>"+"</xsl:choose>"+"</xsl:variable>"+"<xsl:choose>"+"<xsl:when test=\"../../@height\">"+"<xsl:value-of select=\"concat('MARGIN-TOP:',((../../@height - @height) div 2) - 1 + number($top_offset),'px;MARGIN-BOTTOM:0px')\" />"+"</xsl:when>"+"<xsl:otherwise>"+"<xsl:value-of select=\"concat('MARGIN-TOP:',(@height - @image_height) div 2,'px;MARGIN-BOTTOM:0','px')\" />"+"</xsl:otherwise>"+"</xsl:choose>"+"</xsl:attribute>"+"</img><![CDATA[ ]]>"+"</div>"+"</xsl:template>";
nitobi.ui.BinaryStateButton=function(xml,id){
this.initialize(xml,nitobi.ui.BinaryStateButtonXsl,id);
this.m_Checked=false;
};
nitobi.lang.extend(nitobi.ui.BinaryStateButton,nitobi.ui.Button);
nitobi.ui.BinaryStateButton.prototype.isChecked=function(){
return this.m_Checked;
};
nitobi.ui.BinaryStateButton.prototype.check=function(){
var _78e=this.getHtmlElementHandle();
_78e.className="ntb-button-checked";
this.m_Checked=true;
};
nitobi.ui.BinaryStateButton.prototype.uncheck=function(){
var _78f=this.getHtmlElementHandle();
_78f.className="ntb-button";
this.m_Checked=false;
};
nitobi.ui.BinaryStateButton.prototype.toggle=function(){
var _790=this.getHtmlElementHandle();
if(_790.className=="ntb-button-checked"){
this.uncheck();
}else{
this.check();
}
};
nitobi.ui.ToolbarXsl="<xsl:template match=\"//toolbar\">"+"<div style=\"z-index:800\">"+"<xsl:attribute name=\"id\">"+"<xsl:value-of select=\"@id\" />"+"</xsl:attribute>"+"<xsl:attribute name=\"style\">float:left;position:relative;"+"<xsl:value-of select=\"concat('width:',@width,'px;height:',@height,'px')\" />"+"</xsl:attribute>"+"<xsl:apply-templates />"+"</div>"+"</xsl:template>"+nitobi.ui.ButtonXsl+nitobi.ui.BinaryStateButtonXsl+"<xsl:template match=\"separator\">"+"<div align='center'>"+"<xsl:attribute name=\"style\">"+"<xsl:value-of select=\"concat('float:left;width:',@width,';height:',@height)\" />"+"</xsl:attribute>"+"<xsl:attribute name=\"id\">"+"<xsl:value-of select=\"@id\" />"+"</xsl:attribute>"+"<img border='0'>"+"<xsl:attribute name=\"src\">"+"<xsl:value-of select=\"concat(//@image_directory,@image)\" />"+"</xsl:attribute>"+"<xsl:attribute name=\"style\">"+"<xsl:value-of select=\"concat('MARGIN-TOP:3','px;MARGIN-BOTTOM:0','px')\" />"+"</xsl:attribute>"+"</img>"+"</div>"+"</xsl:template>";
nitobi.ui.Toolbar=function(xml,id){
nitobi.ui.Toolbar.baseConstructor.call(this);
this.initialize(xml,nitobi.ui.ToolbarXsl,id);
};
nitobi.lang.extend(nitobi.ui.Toolbar,nitobi.ui.InteractiveUiElement);
nitobi.ui.Toolbar.prototype.getUiElements=function(){
return this.m_UiElements;
};
nitobi.ui.Toolbar.prototype.setUiElements=function(_793){
this.m_UiElements=_793;
};
nitobi.ui.Toolbar.prototype.attachButtonObjects=function(){
if(!this.m_UiElements){
this.m_UiElements=new Array();
var tag=this.getHtmlElementHandle();
var _795=tag.childNodes;
for(var i=0;i<_795.length;i++){
var _797=_795[i];
if(_797.nodeType!=3){
var _798;
switch(_797.className){
case ("ntb-button"):
_798=new nitobi.ui.Button(null,_797.id);
break;
case ("ntb-binarybutton"):
_798=new nitobi.ui.BinaryStateButton(null,_797.id);
break;
default:
_798=new nitobi.ui.UiElement(null,null,_797.id);
break;
}
_798.attachToTag();
this.m_UiElements[_797.id]=_798;
}
}
}
};
nitobi.ui.Toolbar.prototype.render=function(_799){
nitobi.ui.Toolbar.base.base.render.call(this,_799);
this.attachButtonObjects();
};
nitobi.ui.Toolbar.prototype.disableAllElements=function(){
for(var i in this.m_UiElements){
if(this.m_UiElements[i].disable){
this.m_UiElements[i].disable();
}
}
};
nitobi.ui.Toolbar.prototype.enableAllElements=function(){
for(var i in this.m_UiElements){
if(this.m_UiElements[i].enable){
this.m_UiElements[i].enable();
}
}
};
nitobi.ui.Toolbar.prototype.attachToTag=function(){
nitobi.ui.Toolbar.base.base.attachToTag.call(this);
this.attachButtonObjects();
};
nitobi.ui.Toolbar.prototype.dispose=function(){
if(typeof (this.m_UiElements)!="undefined"){
for(var _79c in this.m_UiElements){
this.m_UiElements[_79c].dispose();
}
this.m_UiElements=null;
}
nitobi.ui.Toolbar.base.dispose.call(this);
};
nitobi.lang.defineNs("nitobi.calendar");
if(false){
nitobi.calendar=function(){
};
}
nitobi.calendar.DatePicker=function(_79d){
nitobi.prepare();
nitobi.calendar.DatePicker.baseConstructor.call(this,_79d);
this.renderer.setTemplate(nitobi.calendar.datePickerTemplate);
this.blurInput=true;
this.onDateSelected=new nitobi.base.Event();
this.eventMap["dateselected"]=this.onDateSelected;
this.onSetInvalidDate=new nitobi.base.Event();
this.eventMap["setinvaliddate"]=this.onSetInvalidDate;
this.onSetDisabledDate=new nitobi.base.Event();
this.eventMap["setdisableddate"]=this.onSetDisabledDate;
this.onSetOutOfRangeDate=new nitobi.base.Event();
this.eventMap["setoutofrangedate"]=this.onSetOutOfRangeDate;
this.onEventDateSelected=new nitobi.base.Event();
this.eventMap["eventdateselected"]=this.onEventDateSelected;
this.eventsManager=new nitobi.calendar.EventsManager(this.getEventsUrl());
this.eventsManager.onDataReady.subscribe(this.renderChildren,this);
var _79e=this.getSelectedDate();
if(_79e&&!this.isOutOfRange(_79e)&&!nitobi.base.DateMath.invalid(_79e)){
this.setStartDate(nitobi.base.DateMath.getMonthStart(_79e));
}else{
this.setDateAttribute("selecteddate",null);
var _79f=this.getMinDate();
var _7a0;
if(_79f){
_7a0=_79f;
}else{
_7a0=new Date();
}
this.setStartDate(nitobi.base.DateMath.getMonthStart(_7a0));
}
this.subscribeDeclarationEvents();
};
nitobi.lang.extend(nitobi.calendar.DatePicker,nitobi.ui.Element);
nitobi.base.Registry.getInstance().register(new nitobi.base.Profile("nitobi.calendar.DatePicker",null,false,"ntb:datepicker"));
nitobi.calendar.DatePicker.prototype.render=function(){
var _7a1=this.getInput();
if(_7a1){
_7a1.detachEvents();
}
nitobi.calendar.DatePicker.base.render.call(this);
if(_7a1){
_7a1.attachEvents();
}
if(nitobi.browser.IE&&_7a1){
var _7a2=_7a1.getHtmlNode("input");
var _7a3=nitobi.html.Css.getStyle(_7a2,"height");
nitobi.html.Css.setStyle(_7a2,"height",parseInt(_7a3)-2+"px");
}
if(this.eventsManager){
this.eventsManager.getFromServer();
}else{
this.renderChildren();
}
};
nitobi.calendar.DatePicker.prototype.renderChildren=function(){
var cal=this.getCalendar();
var _7a5=this.getInput();
if(cal){
cal.render();
if(!_7a5){
var C=nitobi.html.Css;
var _7a7=cal.getHtmlNode();
var body=cal.getHtmlNode("body");
C.swapClass(_7a7,"nitobi-hide",NTB_CSS_SMALL);
cal.getHtmlNode().style.width=body.offsetWidth+"px";
C.removeClass(_7a7,NTB_CSS_SMALL);
}
}
if(this.getSelectedDate()&&_7a5){
_7a5.setValue(this.formatDate(this.getSelectedDate(),_7a5.getDisplayMask()));
}
if(this.getSelectedDate()){
var _7a9=this.getHtmlNode("value");
if(_7a9){
_7a9.value=this.formatDate(this.getSelectedDate(),this.getSubmitMask());
}
}
var _7aa=this.getHtmlNode("button");
if(_7aa){
nitobi.html.attachEvent(_7aa,"click",cal.handleToggleClick,cal);
}
};
nitobi.calendar.DatePicker.prototype.getCalendar=function(){
return this.getObject(nitobi.calendar.Calendar.profile);
};
nitobi.calendar.DatePicker.prototype.getInput=function(){
return this.getObject(nitobi.calendar.DateInput.profile);
};
nitobi.calendar.DatePicker.prototype.getSelectedDate=function(){
return this.getDateAttr("selecteddate");
};
nitobi.calendar.DatePicker.prototype.getDateAttr=function(attr){
var _7ac=this.getAttribute(attr,null);
if(_7ac){
if(typeof (_7ac)=="string"){
return this.parseLanguage(_7ac);
}else{
return new Date(_7ac);
}
}
return null;
};
nitobi.calendar.DatePicker.prototype.setSelectedDate=function(date){
if(typeof (date)!="object"){
date=new Date(date);
}
if(this.validate(date)){
this._setSelectedDate(date);
}
};
nitobi.calendar.DatePicker.prototype._setSelectedDate=function(date,_7af){
this.setDateAttribute("selecteddate",date);
var _7b0=this.getHtmlNode("value");
if(_7b0){
_7b0.value=this.formatDate(date,this.getSubmitMask());
}
var _7b1=this.getInput();
if(_7b1){
var _7b2=_7b1.getDisplayMask();
var _7b3=this.formatDate(date,_7b2);
_7b1.setValue(_7b3);
_7b1.setInvalidStyle(false);
}
var _7b4=this.getCalendar();
if(_7b4){
_7b4.clearHighlight(date);
var dm=nitobi.base.DateMath;
var _7b6=dm.getMonthStart(this.getStartDate());
var _7b7=_7b4.getMonthColumns()*_7b4.getMonthRows()-1;
var _7b8=dm.getMonthEnd(dm.add(dm.clone(_7b6),"m",_7b7));
if(dm.between(date,_7b6,_7b8)){
_7b4.highlight(date);
}
if(_7af){
this.setStartDate(dm.getMonthStart(dm.clone(date)));
_7b4.render();
}
}
var _7b9=this.getEventsManager();
if(_7b9.isEvent(date)){
var _7b6=_7b9.eventsCache[date.valueOf()];
var _7ba=this.eventsManager.getEventInfo(_7b6);
this.onEventDateSelected.notify({events:_7ba});
}
this.onDateSelected.notify(new nitobi.ui.ElementEventArgs(this,this.onDateSelected));
};
nitobi.calendar.DatePicker.prototype.validate=function(_7bb){
var E=nitobi.ui.ElementEventArgs;
if(nitobi.base.DateMath.invalid(_7bb)){
this.onSetInvalidDate.notify(new E(this,this.onSetInvalidDate));
return false;
}
if(this.isOutOfRange(_7bb)){
this.onSetOutOfRangeDate.notify(new E(this,this.onSetOutOfRangeDate));
return false;
}
if(this.isDisabled(_7bb)){
this.onSetDisabledDate.notify(new E(this,this.onSetDisabledDate));
return false;
}
return true;
};
nitobi.calendar.DatePicker.prototype.isDisabled=function(date){
return this.getEventsManager().isDisabled(date);
};
nitobi.calendar.DatePicker.prototype.isOutOfRange=function(date){
var dm=nitobi.base.DateMath;
var _7c0=this.getMinDate();
var _7c1=this.getMaxDate();
var _7c2=false;
if(_7c0&&_7c1){
_7c2=!dm.between(date,_7c0,_7c1);
}else{
if(_7c0&&_7c1==null){
_7c2=dm.before(date,_7c0);
}else{
if(_7c0==null&&_7c1){
_7c2=dm.after(date,_7c1);
}
}
}
return _7c2;
};
nitobi.calendar.DatePicker.prototype.clear=function(){
var _7c3=this.getHtmlNode("value");
if(_7c3){
_7c3.value="";
}
this.setDateAttribute("selecteddate",null);
};
nitobi.calendar.DatePicker.prototype.getTheme=function(){
return this.getAttribute("theme","");
};
nitobi.calendar.DatePicker.prototype.getSubmitMask=function(){
return this.getAttribute("submitmask","yyyy-MM-dd");
};
nitobi.calendar.DatePicker.prototype.setSubmitMask=function(mask){
this.setAttribute("submitmask",mask);
};
nitobi.calendar.DatePicker.prototype.getStartDate=function(){
return this.getDateAttribute("startdate");
};
nitobi.calendar.DatePicker.prototype.setStartDate=function(date){
this.setDateAttribute("startdate",date);
};
nitobi.calendar.DatePicker.prototype.getEventsUrl=function(){
return this.getAttribute("eventsurl","");
};
nitobi.calendar.DatePicker.prototype.setEventsUrl=function(url){
this.setAttribute("eventsurl",url);
};
nitobi.calendar.DatePicker.prototype.getEventsManager=function(){
return this.eventsManager;
};
nitobi.calendar.DatePicker.prototype.isShimEnabled=function(){
return this.getBoolAttribute("shimenabled",false);
};
nitobi.calendar.DatePicker.prototype.getMinDate=function(){
return this.getDateAttr("mindate");
};
nitobi.calendar.DatePicker.prototype.setMinDate=function(_7c7){
this.setAttribute("mindate",_7c7);
};
nitobi.calendar.DatePicker.prototype.getMaxDate=function(){
return this.getDateAttr("maxdate");
};
nitobi.calendar.DatePicker.prototype.setMaxDate=function(_7c8){
this.setAttribute("maxdate",_7c8);
};
nitobi.calendar.DatePicker.prototype.parseLanguage=function(date){
var dm=nitobi.base.DateMath;
var _7cb=Date.parse(date);
if(_7cb&&typeof (_7cb)=="object"&&!isNaN(_7cb)&&!dm.invalid(_7cb)){
return _7cb;
}
if(date==""||date==null){
return null;
}
date=date.toLowerCase();
var _7cc=dm.resetTime(new Date());
switch(date){
case "today":
date=_7cc;
break;
case "tomorrow":
date=dm.add(_7cc,"d",1);
break;
case "yesterday":
date=dm.subtract(_7cc,"d",1);
break;
case "last week":
date=dm.subtract(_7cc,"d",7);
break;
case "next week":
date=dm.add(_7cc,"d",7);
break;
case "last year":
date=dm.subtract(_7cc,"y",1);
break;
case "last month":
date=dm.subtract(_7cc,"m",1);
break;
case "next month":
date=dm.add(_7cc,"m",1);
break;
case "next year":
date=dm.add(_7cc,"y",1);
break;
default:
date=dm.resetTime(new Date(date));
break;
}
if(dm.invalid(date)){
return null;
}else{
return date;
}
};
nitobi.calendar.DatePicker.longDayNames=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
nitobi.calendar.DatePicker.shortDayNames=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
nitobi.calendar.DatePicker.minDayNames=["S","M","T","W","T","F","S"];
nitobi.calendar.DatePicker.longMonthNames=["January","February","March","April","May","June","July","August","September","October","November","December"];
nitobi.calendar.DatePicker.shortMonthNames=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
nitobi.calendar.DatePicker.navConfirmText="OK";
nitobi.calendar.DatePicker.navCancelText="Cancel";
nitobi.calendar.DatePicker.navOutOfRangeText="That date is out of range.";
nitobi.calendar.DatePicker.navInvalidYearText="You must enter a valid year.";
nitobi.calendar.DatePicker.prototype.getMinDayNames=function(){
return this.initJsAttr("minDayNames");
};
nitobi.calendar.DatePicker.prototype.getLongDayNames=function(){
return this.initJsAttr("longDayNames");
};
nitobi.calendar.DatePicker.prototype.getShortDayNames=function(){
return this.initJsAttr("shortDayNames");
};
nitobi.calendar.DatePicker.prototype.getLongMonthNames=function(){
return this.initJsAttr("longMonthNames");
};
nitobi.calendar.DatePicker.prototype.getShortMonthNames=function(){
return this.initJsAttr("shortMonthNames");
};
nitobi.calendar.DatePicker.prototype.getNavConfirmText=function(){
return this.initLocaleAttr("navConfirmText");
};
nitobi.calendar.DatePicker.prototype.getNavCancelText=function(){
return this.initLocaleAttr("navCancelText");
};
nitobi.calendar.DatePicker.prototype.getNavOutOfRangeText=function(){
return this.initLocaleAttr("navOutOfRangeText");
};
nitobi.calendar.DatePicker.prototype.getNavInvalidYearText=function(){
return this.initLocaleAttr("navInvalidYearText");
};
nitobi.calendar.DatePicker.prototype.initJsAttr=function(_7cd){
if(this[_7cd]){
return this[_7cd];
}
var attr=this.getAttribute(_7cd.toLowerCase(),"");
if(attr!=""){
attr=eval("("+attr+")");
return this[_7cd]=attr;
}
return this[_7cd]=nitobi.calendar.DatePicker[_7cd];
};
nitobi.calendar.DatePicker.prototype.initLocaleAttr=function(_7cf){
if(this[_7cf]){
return this[_7cf];
}
var text=this.getAttribute(_7cf.toLowerCase(),"");
if(text!=""){
return this[_7cf]=text;
}else{
return this[_7cf]=nitobi.calendar.DatePicker[_7cf];
}
};
nitobi.calendar.DatePicker.prototype.parseDate=function(date,mask){
var _7d3={};
while(mask.length>0){
var c=mask.charAt(0);
var _7d5=new RegExp(c+"+");
var _7d6=_7d5.exec(mask)[0];
if(c!="d"&&c!="y"&&c!="M"&&c!="N"&&c!="E"){
mask=mask.substring(_7d6.length);
date=date.substring(_7d6.length);
}else{
var _7d7=mask.charAt(_7d6.length);
var _7d8=(_7d7==""?date:date.substring(0,date.indexOf(_7d7)));
var _7d9=this.validateFormat(_7d8,_7d6);
if(_7d9.valid){
_7d3[_7d9.unit]=_7d9.value;
}else{
return null;
}
mask=mask.substring(_7d6.length);
date=date.substring(_7d8.length);
}
}
var date=new Date(_7d3.y,_7d3.m,_7d3.d);
return date;
};
nitobi.calendar.DatePicker.prototype.validateFormat=function(_7da,_7db){
var _7dc={valid:false,unit:"",value:""};
switch(_7db){
case "d":
case "dd":
var _7dd=parseInt(_7da);
var _7de;
if(_7db=="d"){
_7de=!isNaN(_7da)&&_7da.charAt(0)!="0"&&_7da.length<=2;
}else{
_7de=!isNaN(_7da)&&_7da.length==2;
}
if(_7de){
_7dc.valid=true;
_7dc.unit="d";
_7dc.value=_7da;
}else{
_7dc.valid=false;
}
break;
case "y":
case "yyyy":
if(isNaN(_7da)){
_7dc.valid=false;
}else{
_7dc.valid=true;
_7dc.unit="y";
_7dc.value=_7da;
}
break;
case "M":
case "MM":
var _7dd=parseInt(_7da,10);
var _7de;
if(_7db=="M"){
_7de=!isNaN(_7da)&&_7da.charAt(0)!="0"&&_7da.length<=2&&_7dd>=1&&_7dd<=12;
}else{
_7de=!isNaN(_7da)&_7da.length==2&&_7dd>=1&&_7dd<=12;
}
if(_7de){
_7dc.valid=true;
_7dc.unit="m";
_7dc.value=_7dd-1;
}else{
_7dc.valid=false;
}
break;
case "MMM":
case "NNN":
case "E":
case "EE":
var _7df;
if(_7db=="MMM"){
_7df=this.getLongMonthNames();
}else{
if(_7db=="NNN"){
_7df=this.getShortMonthNames();
}else{
if(_7db=="E"){
_7df=this.getShortDayNames();
}else{
_7df=this.getLongDayNames();
}
}
}
var i;
for(i=0;i<_7df.length;i++){
var _7e1=_7df[i];
if(_7da.toLowerCase()==_7e1.toLowerCase()){
break;
}
}
if(i<_7df.length){
_7dc.valid=true;
if(_7db=="MMM"||_7db=="NNN"){
_7dc.unit="m";
}else{
_7dc.unit="dl";
}
_7dc.value=i;
}else{
_7dc.valid=false;
}
break;
}
return _7dc;
};
nitobi.calendar.DatePicker.prototype.formatDate=function(date,mask){
var _7e4={};
var year=date.getFullYear()+"";
var _7e6=date.getMonth()+1+"";
var _7e7=date.getDate()+"";
var day=date.getDay();
_7e4["y"]=_7e4["yyyy"]=year;
_7e4["yy"]=year.substring(2,4);
_7e4["M"]=_7e6+"";
_7e4["MM"]=nitobi.lang.padZeros(_7e6,2);
_7e4["MMM"]=this.getLongMonthNames()[_7e6-1];
_7e4["NNN"]=this.getShortMonthNames()[_7e6-1];
_7e4["d"]=_7e7;
_7e4["dd"]=nitobi.lang.padZeros(_7e7,2);
_7e4["EE"]=this.getLongDayNames()[day];
_7e4["E"]=this.getShortDayNames()[day];
var _7e9="";
while(mask.length>0){
var c=mask.charAt(0);
var _7eb=new RegExp(c+"+");
var _7ec=_7eb.exec(mask)[0];
_7e9+=_7e4[_7ec]||_7ec;
mask=mask.substring(_7ec.length);
}
return _7e9;
};


var temp_ntb_modelDoc='<state	 xmlns:ntb="http://www.nitobi.com"	ID="mySheet"	Version="3.01" 	element="grid" 		uniqueID="_hkj342">    <nitobi.grid.Grid    	Theme="nitobi"    	CellBorder="0" 		Height="300"		Width="700"		skin="default"		RowHeight="23"					indicatorHeight="23"		HeaderHeight="23"		scrollbarWidth="26"		scrollbarHeight="26"		ToolbarHeight="25"				top="23"		left="0"		minHeight="60"		minWidth="250"		PrimaryDatasourceSize="0" 		containerHeight=""		containerWidth=""		columnsdefined="0"		renderframe="0"		renderindicators="0"		renderheader="0"		renderfooter="0"		renderleft="0"		renderright="0"		rendercenter="0"		selected="1"		activeView=""		highlightCell=""		scrolling="0"		EditMode="0"		prevCell=""		prevText=""		prevData=""		FrozenLeftColumnCount="0"		DatasourceSizeEstimate="0"    	DatasourceId=""  		freezeright="0"		freezetop="0"		ToolbarEnabled="1"    	Expanding="0"			GridResizeEnabled="0"		RowHighlightEnabled="0"		RowSelectEnabled="0"		MultiRowSelectEnabled="0"		AutoKeyEnabled="0"			ToolbarContainerEmpty="false"			ToolTipsEnabled="0"		RowIndicatorsEnabled="0"		ColumnIndicatorsEnabled="1"		HScrollbarEnabled="1"		VScrollbarEnabled="1"		rowselect="0"		AutoSaveEnabled="0"		autoAdd="0"		remoteSort="0"		ForceValidate="1"		showErrors="0"		columnGraying="0"		keymode=""			keyboardPaging="0"		RowInsertEnabled="1"		RowDeleteEnabled="1"		allowEdit="1"		allowFormula="1"		PasteEnabled="1"		CopyEnabled="1"				expandRowsOnPaste="1"		expandColumnsOnPast="1"		datalog="myXMLLog"		xselect="//root"		xorder="@a"		asynchronous="1"		fieldMap=""    	GetHandler="" 		getHandler=""		SaveHandler=""		lastSaveHandlerResponse=""		sortColumn="0"		curSortColumn="0"		descending="0"		curSortColumnDesc="0"		RowCount="0"		ColumnCount="0"		nextXK="32"		CurrentPageIndex="0"		PagingMode="standard"		DataMode="caching"		RenderMode=""    	LiveScrollingMode="Leap"		RowsPerPage="20"		pageStart="0"		normalColor="#FFFFFF"		normalColor2="#FFFFFF"		activeColor="#FFFFFF"		selectionColor="#FFFFFF"		highlightColor="#FFFFFF"		columnGrayingColor="#FFFFFF"		SingleClickEditEnabled="0"		LastError=""		SortEnabled="1"    	SortMode="default"    	EnterTab="down"    	    	WidthFixed="0"     	HeightFixed="0"    	MinWidth="20"     	MinHeight="0"    	DragFillEnabled="1"	>    </nitobi.grid.Grid>    <nitobi.grid.Columns>    </nitobi.grid.Columns>    <Defaults>    	<nitobi.grid.Grid></nitobi.grid.Grid>		<nitobi.grid.Column 			Width="100"			type="TEXT"			Visible="1"			SortEnabled="1"			/>    	<nitobi.grid.Column Align="right" ClassName="" CssStyle="" ColumnName="" DataType="number" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="#,###.00" NegativeMask="" GroupingSeparator="," DecimalSeparator="." type="TEXT" editor="TEXT"/>    	<nitobi.grid.Column Align="right" ClassName="" CssStyle="" ColumnName="" DataType="number" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="#,###.00" NegativeMask="" GroupingSeparator="," DecimalSeparator="." type="NUMBER" editor="NUMBER"/>    	<nitobi.grid.Column Align="right" ClassName="" CssStyle="" ColumnName="" DataType="number" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="#,###.00" NegativeMask="" GroupingSeparator="," DecimalSeparator="." type="TEXTAREA" editor="TEXTAREA"/>    	<nitobi.grid.Column Align="right" ClassName="" CssStyle="" ColumnName="" DataType="number" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="#,###.00" NegativeMask="" GroupingSeparator="," DecimalSeparator="." ImageUrl="" type="IMAGE" editor="IMAGE"/>    	<nitobi.grid.Column Align="right" ClassName="" CssStyle="" ColumnName="" DataType="number" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="#,###.00" NegativeMask="" GroupingSeparator="," DecimalSeparator="." OpenWindow="1" type="LINK" editor="LINK"/>    	<nitobi.grid.Column Align="right" ClassName="" CssStyle="" ColumnName="" DataType="number" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="M/d/yyyy" NegativeMask="" GroupingSeparator="," DecimalSeparator="." CalendarEnabled="1" type="DATE" editor="DATE"/>    	<nitobi.grid.Column Align="right" ClassName="" CssStyle="" ColumnName="" DataType="number" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="#,###.00" NegativeMask="" GroupingSeparator="," DecimalSeparator="." DatasourceId="" Datasource="" DisplayFields="" ValueField="" Delay="" Size="6" ForceValidOption="0" AutoComplete="1" AutoClear="0" GetOnEnter="0" ReferenceColumn="" type="LOOKUP" editor="LOOKUP"/>    	<nitobi.grid.Column Align="right" ClassName="" CssStyle="" ColumnName="" DataType="number" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="#,###.00" NegativeMask="" GroupingSeparator="," DecimalSeparator="." DatasourceId="" Datasource="" DisplayFields="" ValueField="" type="LISTBOX" editor="LISTBOX"/>    	<nitobi.grid.Column Align="right" ClassName="" CssStyle="" ColumnName="" DataType="number" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="#,###.00" NegativeMask="" GroupingSeparator="," DecimalSeparator="." type="PASSWORD" editor="PASSWORD"/>    	<nitobi.grid.Column Align="right" ClassName="" CssStyle="" ColumnName="" DataType="number" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="#,###.00" NegativeMask="" GroupingSeparator="," DecimalSeparator="." DatasourceId="" Datasource="" DisplayFields="" ValueField="" CheckedValue="" UnCheckedValue="" type="CHECKBOX" editor="CHECKBOX"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="date" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="M/d/yyyy" CalendarEnabled="1" type="TEXT" editor="TEXT"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="date" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="M/d/yyyy" CalendarEnabled="1" type="NUMBER" editor="NUMBER"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="date" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="M/d/yyyy" CalendarEnabled="1" type="TEXTAREA" editor="TEXTAREA"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="date" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="M/d/yyyy" CalendarEnabled="1" ImageUrl="" type="IMAGE" editor="IMAGE"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="date" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="M/d/yyyy" CalendarEnabled="1" OpenWindow="1" type="LINK" editor="LINK"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="date" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="M/d/yyyy" CalendarEnabled="1" type="DATE" editor="DATE"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="date" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="M/d/yyyy" CalendarEnabled="1" DatasourceId="" Datasource="" DisplayFields="" ValueField="" Delay="" Size="6" ForceValidOption="0" AutoComplete="1" AutoClear="0" GetOnEnter="0" ReferenceColumn="" type="LOOKUP" editor="LOOKUP"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="date" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="M/d/yyyy" CalendarEnabled="1" DatasourceId="" Datasource="" DisplayFields="" ValueField="" type="LISTBOX" editor="LISTBOX"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="date" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="M/d/yyyy" CalendarEnabled="1" type="PASSWORD" editor="PASSWORD"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="date" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="M/d/yyyy" CalendarEnabled="1" DatasourceId="" Datasource="" DisplayFields="" ValueField="" CheckedValue="" UnCheckedValue="" type="CHECKBOX" editor="CHECKBOX"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="text" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" type="TEXT" editor="TEXT"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="text" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" type="NUMBER" editor="NUMBER"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="text" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" type="TEXTAREA" editor="TEXTAREA"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="text" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" ImageUrl="" type="IMAGE" editor="IMAGE"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="text" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" OpenWindow="1" type="LINK" editor="LINK"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="text" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" Mask="M/d/yyyy" CalendarEnabled="1" type="DATE" editor="DATE"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="text" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" DatasourceId="" Datasource="" DisplayFields="" ValueField="" Delay="" Size="6" ForceValidOption="0" AutoComplete="1" AutoClear="0" GetOnEnter="0" ReferenceColumn="" type="LOOKUP" editor="LOOKUP"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="text" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" DatasourceId="" Datasource="" DisplayFields="" ValueField="" type="LISTBOX" editor="LISTBOX"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="text" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" type="PASSWORD" editor="PASSWORD"/>    	<nitobi.grid.Column Align="left" ClassName="" CssStyle="" ColumnName="" DataType="text" Editable="1" Initial="" Label="" GetHandler="" DataSource="" Template="" TemplateUrl="" MaxLength="255" SortDirection="Desc" SortEnabled="1" Width="100" Visible="1" xdatafld="" Value="" xi="100" DatasourceId="" Datasource="" DisplayFields="" ValueField="" CheckedValue="" UnCheckedValue="" type="CHECKBOX" editor="CHECKBOX"/>		<nitobi.grid.Row></nitobi.grid.Row>		<nitobi.grid.Cell></nitobi.grid.Cell>		<ntb:e />    </Defaults>    	<declaration>	</declaration>	<columnDefinitions>	</columnDefinitions></state>';
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.modelDoc = nitobi.xml.createXmlDoc(temp_ntb_modelDoc);

var temp_ntb_toolbarDoc='<?xml version="1.0" encoding="utf-8"?><toolbar id="toolbarthis.uid" title="Grid" height="25" width="110" image_directory="http://localhost/vss/EBALib/v13/Common/Toolbar/Styles/default">	<items>		<button id="save" onclick_event="if(valfe()){this.onClick()}" height="14" width="14" image="save.gif"			image_disabled="save_disabled.gif" tooltip_text="Save Changes" />		<!-- <button id="discardChanges" onclick_event="testclick(this);" height="17" width="16" top_offset="-2"			image="cancelsave.gif" image_disabled="cancelsave_disabled.gif" tooltip_text="Discard Changes" /> -->		<separator id="toolbar1_separator1" height="20" width="5" image="separator.jpg" />		<button id="newRecord" onclick_event="this.onClick()" height="11" width="14" image="newrecord.gif"			image_disabled="newrecord_disabled.gif" tooltip_text="New Record" />		<button id="deleteRecord" onclick_event="this.onClick()" height="11" width="14" image="deleterecord.gif"			image_disabled="deleterecord_disabled.gif" tooltip_text="Delete Record" />		<separator id="toolbar1_separator2" height="20" width="5" image="separator.jpg" />		<button id="refresh" onclick_event="this.onClick()" height="14" width="16" image="refresh.gif"			image_disabled="refresh_disabled.gif" tooltip_text="Refresh" />		<!--<separator id="toolbar1_separator3" height="20" width="5" image="separator.jpg" />		<button id="toolbar1_button4" onclick_event="testclick(this);" height="11" width="10" image="left.gif"			image_disabled="left_disabled.gif" tooltip_text="Previous Page" />		<button id="toolbar1_button5" onclick_event="testclick(this);" height="11" width="10" image="right.gif"			image_disabled="right_disabled.gif" tooltip_text="Next Page" />		-->	</items></toolbar>';
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.toolbarDoc = nitobi.xml.createXmlDoc(temp_ntb_toolbarDoc);

var temp_ntb_pagingToolbarDoc='<?xml version="1.0" encoding="utf-8"?><toolbar id="toolbarpagingthis.uid" title="Paging" height="25" width="60" image_directory="http://localhost/vss/EBALib/v13/Common/Toolbar/Styles/default">	<items>		<button id="previousPage" onclick_event="this.onClick()" height="14" width="14" image="left.gif"			image_disabled="left_disabled.gif" tooltip_text="Previous Page" />		<button id="nextPage" onclick_event="this.onClick()" height="14" width="16" image="right.gif"			image_disabled="right_disabled.gif" tooltip_text="Next Page" />	</items></toolbar>';
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.pagingToolbarDoc = nitobi.xml.createXmlDoc(temp_ntb_pagingToolbarDoc);


var temp_ntb_addXidXslProc='<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com"> <x:p-x:n-guid"x:s-0"/><x:t- match="/"> <x:at-/></x:t-><x:t- match="node()|@*"> <xsl:copy> <xsl:if test="not(@xid)"> <x:a-x:n-xid" ><x:v-x:s-generate-id(.)"/><x:v-x:s-position()"/><x:v-x:s-$guid"/></x:a-> </xsl:if> <x:at-x:s-./* | text() | @*"> </x:at-> </xsl:copy></x:t-> <x:t- match="text()"> <x:v-x:s-."/></x:t-></xsl:stylesheet> ';
nitobi.lang.defineNs("nitobi.data");
nitobi.data.addXidXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_addXidXslProc));

var temp_ntb_adjustXiXslProc='<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com"> <xsl:output method="xml" omit-xml-declaration="yes" /> <x:p-x:n-startingIndex"x:s-5"></x:p-> <x:p-x:n-startingGroup"x:s-5"></x:p-> <x:p-x:n-adjustment"x:s--1"></x:p-> <x:t- match="*|@*"> <xsl:copy> <x:at-x:s-@*|node()" /> </xsl:copy> </x:t-> <!--[@id=\'_default\']--> <x:t- match="//ntb:data/ntb:e|@*"> <x:c-> <x:wh- test="number(@xi) &gt;= number($startingIndex)"> <xsl:copy> <x:at-x:s-@*|node()" /> <x:ct-x:n-increment-xi" /> </xsl:copy> </x:wh-> <x:o-> <xsl:copy> <x:at-x:s-@*|node()" /> </xsl:copy> </x:o-> </x:c-> </x:t-> <x:t-x:n-increment-xi"> <x:a-x:n-xi"> <x:v-x:s-number(@xi) + number($adjustment)" /> </x:a-> </x:t-></xsl:stylesheet>';
nitobi.lang.defineNs("nitobi.data");
nitobi.data.adjustXiXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_adjustXiXslProc));

var temp_ntb_dataTranslatorXslProc='<?xml version="1.0"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com"> <xsl:output method="xml" omit-xml-declaration="yes" /> <x:p-x:n-start"x:s-0"></x:p-> <x:p-x:n-id"x:s-\'_default\'"></x:p-> <x:p-x:n-xkField"x:s-\'a\'"></x:p-> <x:t- match="//root"> <ntb:grid xmlns:ntb="http://www.nitobi.com"> <ntb:datasources> <ntb:datasource id="{$id}"> <xsl:if test="@error"> <x:a-x:n-error"><x:v-x:s-@error" /></x:a-> </xsl:if> <ntb:datasourcestructure id="{$id}"> <x:a-x:n-FieldNames"><x:v-x:s-@fields" />|_xk</x:a-> <x:a-x:n-Keys">_xk</x:a-> </ntb:datasourcestructure> <ntb:data id="{$id}"> <xsl:for-eachx:s-//e"> <x:at-x:s-."> <x:w-x:n-xi"x:s-position()-1"></x:w-> </x:at-> </xsl:for-each> </ntb:data> </ntb:datasource> </ntb:datasources> </ntb:grid> </x:t-> <x:t- match="e"> <x:p-x:n-xi"x:s-0"></x:p-> <ntb:e> <xsl:copy-ofx:s-@*[not(name() = \'xk\')]"></xsl:copy-of> <xsl:if test="not(@xi)"><x:a-x:n-xi"><x:v-x:s-$start + $xi" /></x:a-></xsl:if> <x:a-x:n-{$xkField}"><x:v-x:s-@xk" /></x:a-> </ntb:e> </x:t-> <x:t- match="lookups"></x:t-></xsl:stylesheet>';
nitobi.lang.defineNs("nitobi.data");
nitobi.data.dataTranslatorXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_dataTranslatorXslProc));

var temp_ntb_dateFormatTemplatesXslProc='<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com" xmlns:d="http://exslt.org/dates-and-times" xmlns:n="http://www.nitobi.com/exslt/numbers" extension-element-prefixes="d n"> <!-- http://java.sun.com/j2se/1.3/docs/api/java/text/SimpleDateFormat.html --><d:ms> <d:m i="1" l="31" a="Jan">January</d:m> <d:m i="2" l="28" a="Feb">February</d:m> <d:m i="3" l="31" a="Mar">March</d:m> <d:m i="4" l="30" a="Apr">April</d:m> <d:m i="5" l="31" a="May">May</d:m> <d:m i="6" l="30" a="Jun">June</d:m> <d:m i="7" l="31" a="Jul">July</d:m> <d:m i="8" l="31" a="Aug">August</d:m> <d:m i="9" l="30" a="Sep">September</d:m> <d:m i="10" l="31" a="Oct">October</d:m> <d:m i="11" l="30" a="Nov">November</d:m> <d:m i="12" l="31" a="Dec">December</d:m></d:ms><d:ds> <d:d a="Sun">Sunday</d:d> <d:d a="Mon">Monday</d:d> <d:d a="Tue">Tuesday</d:d> <d:d a="Wed">Wednesday</d:d> <d:d a="Thu">Thursday</d:d> <d:d a="Fri">Friday</d:d> <d:d a="Sat">Saturday</d:d></d:ds><x:t-x:n-d:format-date"> <x:p-x:n-date-time" /> <x:p-x:n-mask"x:s-\'MMM d, yy\'"/> <x:p-x:n-date-year" /> <x:va-x:n-formatted"> <x:va-x:n-date-time-length"x:s-string-length($date-time)" /> <x:va-x:n-timezone"x:s-\'\'" /> <x:va-x:n-dt"x:s-substring($date-time, 1, $date-time-length - string-length($timezone))" /> <x:va-x:n-dt-length"x:s-string-length($dt)" /> <x:c-> <x:wh- test="substring($dt, 3, 1) = \':\' and substring($dt, 6, 1) = \':\'"> <!--that means we just have a time--> <x:va-x:n-hour"x:s-substring($dt, 1, 2)" /> <x:va-x:n-min"x:s-substring($dt, 4, 2)" /> <x:va-x:n-sec"x:s-substring($dt, 7)" /> <xsl:if test="$hour &lt;= 23 and $min &lt;= 59 and $sec &lt;= 60"> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-\'NaN\'" /> <x:w-x:n-month"x:s-\'NaN\'" /> <x:w-x:n-day"x:s-\'NaN\'" /> <x:w-x:n-hour"x:s-$hour" /> <x:w-x:n-minute"x:s-$min" /> <x:w-x:n-second"x:s-$sec" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </xsl:if> </x:wh-> <x:wh- test="substring($dt, 2, 1) = \'-\' or substring($dt, 3, 1) = \'-\'"> <x:c-> <x:wh- test="$dt-length = 5 or $dt-length = 6"> <!--D-MMM,DD-MMM--> <x:va-x:n-year"x:s-$date-year" /> <x:va-x:n-month"x:s-document(\'\')/*/d:ms/d:m[@a = substring-after($dt,\'-\')]/@i" /> <x:va-x:n-day"x:s-substring-before($dt,\'-\')" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:wh-> <x:wh- test="$dt-length = 8 or $dt-length = 9"> <!--D-MMM-YY,DD-MMM-YY--> <x:va-x:n-year"x:s-concat(\'20\',substring-after(substring-after($dt,\'-\'),\'-\'))" /> <x:va-x:n-month"x:s-document(\'\')/*/d:ms/d:m[@a = substring-before(substring-after($dt,\'-\'),\'-\')]/@i" /> <x:va-x:n-day"x:s-substring-before($dt,\'-\')" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:wh-> <x:o-> <!--D-MMM-YYYY,DD-MMM-YYYY--> <x:va-x:n-year"x:s-substring-after(substring-after($dt,\'-\'),\'-\')" /> <x:va-x:n-month"x:s-document(\'\')/*/d:ms/d:m[@a = substring-before(substring-after($dt,\'-\'),\'-\')]/@i" /> <x:va-x:n-day"x:s-substring-before($dt,\'-\')" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:o-> </x:c-> </x:wh-> <x:o-> <!--($neg * -2)--> <x:va-x:n-year"x:s-substring($dt, 1, 4) * (0 + 1)" /> <x:va-x:n-month"x:s-substring($dt, 6, 2)" /> <x:va-x:n-day"x:s-substring($dt, 9, 2)" /> <x:c-> <x:wh- test="$dt-length = 10"> <!--that means we just have a date--> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:wh-> <x:wh- test="substring($dt, 14, 1) = \':\' and substring($dt, 17, 1) = \':\'"> <!--that means we have a date + time--> <x:va-x:n-hour"x:s-substring($dt, 12, 2)" /> <x:va-x:n-min"x:s-substring($dt, 15, 2)" /> <x:va-x:n-sec"x:s-substring($dt, 18)" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-hour"x:s-$hour" /> <x:w-x:n-minute"x:s-$min" /> <x:w-x:n-second"x:s-$sec" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:wh-> </x:c-> </x:o-> </x:c-> </x:va-> <x:v-x:s-$formatted" /> </x:t-><x:t-x:n-d:_format-date"> <x:p-x:n-year" /> <x:p-x:n-month"x:s-1" /> <x:p-x:n-day"x:s-1" /> <x:p-x:n-hour"x:s-0" /> <x:p-x:n-minute"x:s-0" /> <x:p-x:n-second"x:s-0" /> <x:p-x:n-timezone"x:s-\'Z\'" /> <x:p-x:n-mask"x:s-\'\'" /> <x:va-x:n-char"x:s-substring($mask, 1, 1)" /> <x:c-> <x:wh- test="not($mask)" /> <!--replaced escaping with \' here/--> <x:wh- test="not(contains(\'GyMdhHmsSEDFwWakKz\', $char))"> <x:v-x:s-$char" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-hour"x:s-$hour" /> <x:w-x:n-minute"x:s-$minute" /> <x:w-x:n-second"x:s-$second" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-substring($mask, 2)" /> </x:ct-> </x:wh-> <x:o-> <x:va-x:n-next-different-char"x:s-substring(translate($mask, $char, \'\'), 1, 1)" /> <x:va-x:n-mask-length"> <x:c-> <x:wh- test="$next-different-char"> <x:v-x:s-string-length(substring-before($mask, $next-different-char))" /> </x:wh-> <x:o-> <x:v-x:s-string-length($mask)" /> </x:o-> </x:c-> </x:va-> <x:c-> <!--took our the era designator--> <x:wh- test="$char = \'M\'"> <x:c-> <x:wh- test="$mask-length >= 3"> <x:va-x:n-month-node"x:s-document(\'\')/*/d:ms/d:m[number($month)]" /> <x:c-> <x:wh- test="$mask-length >= 4"> <x:v-x:s-$month-node" /> </x:wh-> <x:o-> <x:v-x:s-$month-node/@a" /> </x:o-> </x:c-> </x:wh-> <x:wh- test="$mask-length = 2"> <x:v-x:s-format-number($month, \'00\')" /> </x:wh-> <x:o-> <x:v-x:s-$month" /> </x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'E\'"> <x:va-x:n-month-days"x:s-sum(document(\'\')/*/d:ms/d:m[position() &lt; $month]/@l)" /> <x:va-x:n-days"x:s-$month-days + $day + boolean(((not($year mod 4) and $year mod 100) or not($year mod 400)) and $month &gt; 2)" /> <x:va-x:n-y-1"x:s-$year - 1" /> <x:va-x:n-dow"x:s-(($y-1 + floor($y-1 div 4) - floor($y-1 div 100) + floor($y-1 div 400) + $days) mod 7) + 1" /> <x:va-x:n-day-node"x:s-document(\'\')/*/d:ds/d:d[number($dow)]" /> <x:c-> <x:wh- test="$mask-length >= 4"> <x:v-x:s-$day-node" /> </x:wh-> <x:o-> <x:v-x:s-$day-node/@a" /> </x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'a\'"> <x:c-> <x:wh- test="$hour >= 12">PM</x:wh-> <x:o->AM</x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'z\'"> <x:c-> <x:wh- test="$timezone = \'Z\'">UTC</x:wh-> <x:o->UTC<x:v-x:s-$timezone" /></x:o-> </x:c-> </x:wh-> <x:o-> <x:va-x:n-padding"x:s-\'00\'" /> <!--removed padding--> <x:c-> <x:wh- test="$char = \'y\'"> <x:c-> <x:wh- test="$mask-length &gt; 2"><x:v-x:s-format-number($year, $padding)" /></x:wh-> <x:o-><x:v-x:s-format-number(substring($year, string-length($year) - 1), $padding)" /></x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'d\'"> <x:v-x:s-format-number($day, $padding)" /> </x:wh-> <x:wh- test="$char = \'h\'"> <x:va-x:n-h"x:s-$hour mod 12" /> <x:c-> <x:wh- test="$h"><x:v-x:s-format-number($h, $padding)" /></x:wh-> <x:o-><x:v-x:s-format-number(12, $padding)" /></x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'H\'"> <x:v-x:s-format-number($hour, $padding)" /> </x:wh-> <x:wh- test="$char = \'k\'"> <x:c-> <x:wh- test="$hour"><x:v-x:s-format-number($hour, $padding)" /></x:wh-> <x:o-><x:v-x:s-format-number(24, $padding)" /></x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'K\'"> <x:v-x:s-format-number($hour mod 12, $padding)" /> </x:wh-> <x:wh- test="$char = \'m\'"> <x:v-x:s-format-number($minute, $padding)" /> </x:wh-> <x:wh- test="$char = \'s\'"> <x:v-x:s-format-number($second, $padding)" /> </x:wh-> <x:wh- test="$char = \'S\'"> <x:v-x:s-format-number(substring-after($second, \'.\'), $padding)" /> </x:wh-> <x:wh- test="$char = \'F\'"> <x:v-x:s-floor($day div 7) + 1" /> </x:wh-> <x:o-> <x:va-x:n-month-days"x:s-sum(document(\'\')/*/d:ms/d:m[position() &lt; $month]/@l)" /> <x:va-x:n-days"x:s-$month-days + $day + boolean(((not($year mod 4) and $year mod 100) or not($year mod 400)) and $month &gt; 2)" /> <x:v-x:s-format-number($days, $padding)" /> <!--removed week in year--> <!--removed week in month--> </x:o-> </x:c-> </x:o-> </x:c-> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-hour"x:s-$hour" /> <x:w-x:n-minute"x:s-$minute" /> <x:w-x:n-second"x:s-$second" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-substring($mask, $mask-length + 1)" /> </x:ct-> </x:o-> </x:c-></x:t-></xsl:stylesheet>';
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.dateFormatTemplatesXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_dateFormatTemplatesXslProc));

var temp_ntb_dateXslProc='<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com" xmlns:d="http://exslt.org/dates-and-times" extension-element-prefixes="d"> <xsl:output method="text" version="4.0" omit-xml-declaration="yes" /> <!-- http://java.sun.com/j2se/1.3/docs/api/java/text/SimpleDateFormat.html --><d:ms> <d:m i="1" l="31" a="Jan">January</d:m> <d:m i="2" l="28" a="Feb">February</d:m> <d:m i="3" l="31" a="Mar">March</d:m> <d:m i="4" l="30" a="Apr">April</d:m> <d:m i="5" l="31" a="May">May</d:m> <d:m i="6" l="30" a="Jun">June</d:m> <d:m i="7" l="31" a="Jul">July</d:m> <d:m i="8" l="31" a="Aug">August</d:m> <d:m i="9" l="30" a="Sep">September</d:m> <d:m i="10" l="31" a="Oct">October</d:m> <d:m i="11" l="30" a="Nov">November</d:m> <d:m i="12" l="31" a="Dec">December</d:m></d:ms><d:ds> <d:d a="Sun">Sunday</d:d> <d:d a="Mon">Monday</d:d> <d:d a="Tue">Tuesday</d:d> <d:d a="Wed">Wednesday</d:d> <d:d a="Thu">Thursday</d:d> <d:d a="Fri">Friday</d:d> <d:d a="Sat">Saturday</d:d></d:ds><x:t-x:n-d:format-date"> <x:p-x:n-date-time" /> <x:p-x:n-mask"x:s-\'MMM d, yy\'"/> <x:p-x:n-date-year" /> <x:va-x:n-formatted"> <x:va-x:n-date-time-length"x:s-string-length($date-time)" /> <x:va-x:n-timezone"x:s-\'\'" /> <x:va-x:n-dt"x:s-substring($date-time, 1, $date-time-length - string-length($timezone))" /> <x:va-x:n-dt-length"x:s-string-length($dt)" /> <x:c-> <x:wh- test="substring($dt, 3, 1) = \':\' and substring($dt, 6, 1) = \':\'"> <!--that means we just have a time--> <x:va-x:n-hour"x:s-substring($dt, 1, 2)" /> <x:va-x:n-min"x:s-substring($dt, 4, 2)" /> <x:va-x:n-sec"x:s-substring($dt, 7)" /> <xsl:if test="$hour &lt;= 23 and $min &lt;= 59 and $sec &lt;= 60"> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-\'NaN\'" /> <x:w-x:n-month"x:s-\'NaN\'" /> <x:w-x:n-day"x:s-\'NaN\'" /> <x:w-x:n-hour"x:s-$hour" /> <x:w-x:n-minute"x:s-$min" /> <x:w-x:n-second"x:s-$sec" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </xsl:if> </x:wh-> <x:wh- test="substring($dt, 2, 1) = \'-\' or substring($dt, 3, 1) = \'-\'"> <x:c-> <x:wh- test="$dt-length = 5 or $dt-length = 6"> <!--D-MMM,DD-MMM--> <x:va-x:n-year"x:s-$date-year" /> <x:va-x:n-month"x:s-document(\'\')/*/d:ms/d:m[@a = substring-after($dt,\'-\')]/@i" /> <x:va-x:n-day"x:s-substring-before($dt,\'-\')" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:wh-> <x:wh- test="$dt-length = 8 or $dt-length = 9"> <!--D-MMM-YY,DD-MMM-YY--> <x:va-x:n-year"x:s-concat(\'20\',substring-after(substring-after($dt,\'-\'),\'-\'))" /> <x:va-x:n-month"x:s-document(\'\')/*/d:ms/d:m[@a = substring-before(substring-after($dt,\'-\'),\'-\')]/@i" /> <x:va-x:n-day"x:s-substring-before($dt,\'-\')" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:wh-> <x:o-> <!--D-MMM-YYYY,DD-MMM-YYYY--> <x:va-x:n-year"x:s-substring-after(substring-after($dt,\'-\'),\'-\')" /> <x:va-x:n-month"x:s-document(\'\')/*/d:ms/d:m[@a = substring-before(substring-after($dt,\'-\'),\'-\')]/@i" /> <x:va-x:n-day"x:s-substring-before($dt,\'-\')" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:o-> </x:c-> </x:wh-> <x:o-> <!--($neg * -2)--> <x:va-x:n-year"x:s-substring($dt, 1, 4) * (0 + 1)" /> <x:va-x:n-month"x:s-substring($dt, 6, 2)" /> <x:va-x:n-day"x:s-substring($dt, 9, 2)" /> <x:c-> <x:wh- test="$dt-length = 10"> <!--that means we just have a date--> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:wh-> <x:wh- test="substring($dt, 14, 1) = \':\' and substring($dt, 17, 1) = \':\'"> <!--that means we have a date + time--> <x:va-x:n-hour"x:s-substring($dt, 12, 2)" /> <x:va-x:n-min"x:s-substring($dt, 15, 2)" /> <x:va-x:n-sec"x:s-substring($dt, 18)" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-hour"x:s-$hour" /> <x:w-x:n-minute"x:s-$min" /> <x:w-x:n-second"x:s-$sec" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:wh-> </x:c-> </x:o-> </x:c-> </x:va-> <x:v-x:s-$formatted" /> </x:t-><x:t-x:n-d:_format-date"> <x:p-x:n-year" /> <x:p-x:n-month"x:s-1" /> <x:p-x:n-day"x:s-1" /> <x:p-x:n-hour"x:s-0" /> <x:p-x:n-minute"x:s-0" /> <x:p-x:n-second"x:s-0" /> <x:p-x:n-timezone"x:s-\'Z\'" /> <x:p-x:n-mask"x:s-\'\'" /> <x:va-x:n-char"x:s-substring($mask, 1, 1)" /> <x:c-> <x:wh- test="not($mask)" /> <!--replaced escaping with \' here/--> <x:wh- test="not(contains(\'GyMdhHmsSEDFwWakKz\', $char))"> <x:v-x:s-$char" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-hour"x:s-$hour" /> <x:w-x:n-minute"x:s-$minute" /> <x:w-x:n-second"x:s-$second" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-substring($mask, 2)" /> </x:ct-> </x:wh-> <x:o-> <x:va-x:n-next-different-char"x:s-substring(translate($mask, $char, \'\'), 1, 1)" /> <x:va-x:n-mask-length"> <x:c-> <x:wh- test="$next-different-char"> <x:v-x:s-string-length(substring-before($mask, $next-different-char))" /> </x:wh-> <x:o-> <x:v-x:s-string-length($mask)" /> </x:o-> </x:c-> </x:va-> <x:c-> <!--took our the era designator--> <x:wh- test="$char = \'M\'"> <x:c-> <x:wh- test="$mask-length >= 3"> <x:va-x:n-month-node"x:s-document(\'\')/*/d:ms/d:m[number($month)]" /> <x:c-> <x:wh- test="$mask-length >= 4"> <x:v-x:s-$month-node" /> </x:wh-> <x:o-> <x:v-x:s-$month-node/@a" /> </x:o-> </x:c-> </x:wh-> <x:wh- test="$mask-length = 2"> <x:v-x:s-format-number($month, \'00\')" /> </x:wh-> <x:o-> <x:v-x:s-$month" /> </x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'E\'"> <x:va-x:n-month-days"x:s-sum(document(\'\')/*/d:ms/d:m[position() &lt; $month]/@l)" /> <x:va-x:n-days"x:s-$month-days + $day + boolean(((not($year mod 4) and $year mod 100) or not($year mod 400)) and $month &gt; 2)" /> <x:va-x:n-y-1"x:s-$year - 1" /> <x:va-x:n-dow"x:s-(($y-1 + floor($y-1 div 4) - floor($y-1 div 100) + floor($y-1 div 400) + $days) mod 7) + 1" /> <x:va-x:n-day-node"x:s-document(\'\')/*/d:ds/d:d[number($dow)]" /> <x:c-> <x:wh- test="$mask-length >= 4"> <x:v-x:s-$day-node" /> </x:wh-> <x:o-> <x:v-x:s-$day-node/@a" /> </x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'a\'"> <x:c-> <x:wh- test="$hour >= 12">PM</x:wh-> <x:o->AM</x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'z\'"> <x:c-> <x:wh- test="$timezone = \'Z\'">UTC</x:wh-> <x:o->UTC<x:v-x:s-$timezone" /></x:o-> </x:c-> </x:wh-> <x:o-> <x:va-x:n-padding"x:s-\'00\'" /> <!--removed padding--> <x:c-> <x:wh- test="$char = \'y\'"> <x:c-> <x:wh- test="$mask-length &gt; 2"><x:v-x:s-format-number($year, $padding)" /></x:wh-> <x:o-><x:v-x:s-format-number(substring($year, string-length($year) - 1), $padding)" /></x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'d\'"> <x:v-x:s-format-number($day, $padding)" /> </x:wh-> <x:wh- test="$char = \'h\'"> <x:va-x:n-h"x:s-$hour mod 12" /> <x:c-> <x:wh- test="$h"><x:v-x:s-format-number($h, $padding)" /></x:wh-> <x:o-><x:v-x:s-format-number(12, $padding)" /></x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'H\'"> <x:v-x:s-format-number($hour, $padding)" /> </x:wh-> <x:wh- test="$char = \'k\'"> <x:c-> <x:wh- test="$hour"><x:v-x:s-format-number($hour, $padding)" /></x:wh-> <x:o-><x:v-x:s-format-number(24, $padding)" /></x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'K\'"> <x:v-x:s-format-number($hour mod 12, $padding)" /> </x:wh-> <x:wh- test="$char = \'m\'"> <x:v-x:s-format-number($minute, $padding)" /> </x:wh-> <x:wh- test="$char = \'s\'"> <x:v-x:s-format-number($second, $padding)" /> </x:wh-> <x:wh- test="$char = \'S\'"> <x:v-x:s-format-number(substring-after($second, \'.\'), $padding)" /> </x:wh-> <x:wh- test="$char = \'F\'"> <x:v-x:s-floor($day div 7) + 1" /> </x:wh-> <x:o-> <x:va-x:n-month-days"x:s-sum(document(\'\')/*/d:ms/d:m[position() &lt; $month]/@l)" /> <x:va-x:n-days"x:s-$month-days + $day + boolean(((not($year mod 4) and $year mod 100) or not($year mod 400)) and $month &gt; 2)" /> <x:v-x:s-format-number($days, $padding)" /> <!--removed week in year--> <!--removed week in month--> </x:o-> </x:c-> </x:o-> </x:c-> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-hour"x:s-$hour" /> <x:w-x:n-minute"x:s-$minute" /> <x:w-x:n-second"x:s-$second" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-substring($mask, $mask-length + 1)" /> </x:ct-> </x:o-> </x:c-></x:t-> <x:t- match="/"> <x:ct-x:n-d:format-date"> <x:w-x:n-date-time"x:s-//date" /> <x:w-x:n-date-year"x:s-//year" /> <x:w-x:n-mask"x:s-//mask" /> </x:ct-></x:t-></xsl:stylesheet>';
nitobi.lang.defineNs("nitobi.form");
nitobi.form.dateXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_dateXslProc));

var temp_ntb_declarationConverterXslProc='<?xml version="1.0" encoding="utf-8" ?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com"> <xsl:output method="xml" omit-xml-declaration="yes" /> <x:t- match="/"> <ntb:grid xmlns:ntb="http://www.nitobi.com"> <ntb:columns> <x:at-x:s-//ntb:columndefinition" mode="columndef" /> </ntb:columns> <ntb:datasources> <x:at-x:s-//ntb:columndefinition" mode="datasources" /> </ntb:datasources> </ntb:grid> </x:t-> <x:t- match="ntb:columndefinition" mode="columndef"> <x:c-> <x:wh- test="@type=\'TEXT\' or @type=\'TEXTAREA\' or @type=\'LISTBOX\' or @type=\'LOOKUP\' or @type=\'CHECKBOX\' or @type=\'LINK\' or @type=\'IMAGE\' or @type=\'\' or not(@type)"> <ntb:textcolumn> <xsl:copy-ofx:s-@*" /> <x:c-> <x:wh- test="@type=\'TEXT\'"> <ntb:texteditor><xsl:copy-ofx:s-@*" /></ntb:texteditor> </x:wh-> <x:wh- test="@type=\'TEXTAREA\'"> <ntb:textareaeditor><xsl:copy-ofx:s-@*" /></ntb:textareaeditor> </x:wh-> <x:wh- test="@type=\'LISTBOX\'"> <ntb:listboxeditor> <xsl:copy-ofx:s-@*" /> <x:a-x:n-DatasourceId">id_<x:v-x:s-position()"/></x:a-> <x:a-x:n-DisplayFields"> <x:c-> <x:wh- test="@show=\'value\'">b</x:wh-> <x:wh- test="@show=\'key\'">a</x:wh-> <x:o-></x:o-> </x:c-> </x:a-> <x:a-x:n-ValueField"> <x:c-> <x:wh- test="@show">a</x:wh-> <x:o-></x:o-> </x:c-> </x:a-> </ntb:listboxeditor> </x:wh-> <x:wh- test="@type=\'CHECKBOX\'"> <ntb:checkboxeditor> <xsl:copy-ofx:s-@*" /> <x:a-x:n-DatasourceId">id_<x:v-x:s-position()"/></x:a-> <x:a-x:n-DisplayFields"> <x:c-> <x:wh- test="@show=\'value\'">b</x:wh-> <x:wh- test="@show=\'key\'">a</x:wh-> <x:o-></x:o-> </x:c-></x:a-> <x:a-x:n-ValueField">a</x:a-> </ntb:checkboxeditor> </x:wh-> <x:wh- test="@type=\'LOOKUP\'"> <ntb:lookupeditor> <xsl:copy-ofx:s-@*" /> <x:a-x:n-DatasourceId">id_<x:v-x:s-position()"/></x:a-> <x:a-x:n-DisplayFields"> <x:c-> <x:wh- test="@show=\'key\'">a</x:wh-> <x:wh- test="@show=\'value\'">b</x:wh-> <x:o-></x:o-> </x:c-></x:a-> <x:a-x:n-ValueField"> <x:c-> <x:wh- test="@show">a</x:wh-> <x:o-></x:o-> </x:c-> </x:a-> </ntb:lookupeditor> </x:wh-> <x:wh- test="@type=\'LINK\'"> <ntb:linkeditor><xsl:copy-ofx:s-@*" /></ntb:linkeditor> </x:wh-> <x:wh- test="@type=\'IMAGE\'"> <ntb:imageeditor><xsl:copy-ofx:s-@*" /></ntb:imageeditor> </x:wh-> </x:c-> </ntb:textcolumn> </x:wh-> <x:wh- test="@type=\'NUMBER\'"> <ntb:numbercolumn><xsl:copy-ofx:s-@*" /></ntb:numbercolumn> </x:wh-> <x:wh- test="@type=\'DATE\' or @type=\'CALENDAR\'"> <ntb:datecolumn> <xsl:copy-ofx:s-@*" /> <x:c-> <x:wh- test="@type=\'DATE\'"> <ntb:dateeditor><xsl:copy-ofx:s-@*" /></ntb:dateeditor> </x:wh-> <x:wh- test="@type=\'CALENDAR\'"> <ntb:calendareditor><xsl:copy-ofx:s-@*" /></ntb:calendareditor> </x:wh-> </x:c-> </ntb:datecolumn> </x:wh-> </x:c-> </x:t-> <x:t- match="ntb:columndefinition" mode="datasources"> <xsl:if test="@values and @values!=\'\'"> <ntb:datasource> <x:a-x:n-id">id_<x:v-x:s-position()" /></x:a-> <ntb:datasourcestructure> <x:a-x:n-id">id_<x:v-x:s-position()" /></x:a-> <x:a-x:n-FieldNames">a|b</x:a-> <x:a-x:n-Keys">a</x:a-> </ntb:datasourcestructure> <ntb:data> <x:a-x:n-id">id_<x:v-x:s-position()" /></x:a-> <x:ct-x:n-values"> <x:w-x:n-valuestring"x:s-@values" /> </x:ct-> </ntb:data> </ntb:datasource> </xsl:if> </x:t-> <x:t-x:n-values"> <x:p-x:n-valuestring" /> <x:va-x:n-bstring"> <x:c-> <x:wh- test="contains($valuestring,\',\')"><x:v-x:s-substring-after(substring-before($valuestring,\',\'),\':\')" /></x:wh-> <x:o-><x:v-x:s-substring-after($valuestring,\':\')" /></x:o-> </x:c-> </x:va-> <ntb:e> <x:a-x:n-a"><x:v-x:s-substring-before($valuestring,\':\')" /></x:a-> <x:a-x:n-b"><x:v-x:s-$bstring" /></x:a-> </ntb:e> <xsl:if test="contains($valuestring,\',\')"> <x:ct-x:n-values"> <x:w-x:n-valuestring"x:s-substring-after($valuestring,\',\')" /> </x:ct-> </xsl:if> </x:t-> </xsl:stylesheet>';
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.declarationConverterXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_declarationConverterXslProc));

var temp_ntb_frameCssXslProc='<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="1.0" xmlns:user="http://mycompany.com/mynamespace" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text" omit-xml-declaration="yes"/><x:p-x:n-IE"x:s-\'false\'"/><x:va-x:n-g"x:s-//state/nitobi.grid.Grid"></x:va-><x:va-x:n-u"x:s-//state/@uniqueID"></x:va-><xsl:keyx:n-style" match="//s" use="@k" /><x:t- match = "/"> <x:va-x:n-t"x:s-$g/@Theme"></x:va-> <x:va-x:n-showvscroll"><x:c-><x:wh- test="($g/@VScrollbarEnabled=\'true\' or $g/@VScrollbarEnabled=1)">1</x:wh-><x:o->0</x:o-></x:c-></x:va-> <x:va-x:n-showhscroll"><x:c-><x:wh- test="($g/@HScrollbarEnabled=\'true\' or $g/@HScrollbarEnabled=1)">1</x:wh-><x:o->0</x:o-></x:c-></x:va-> <x:va-x:n-showtoolbar"><x:c-><x:wh- test="($g/@ToolbarEnabled=\'true\' or $g/@ToolbarEnabled=1)">1</x:wh-><x:o->0</x:o-></x:c-></x:va-> <x:va-x:n-frozen-columns-width"> <x:ct-x:n-get-pane-width"> <x:w-x:n-start-column"x:s-number(1)"/> <x:w-x:n-end-column"x:s-number($g/@FrozenLeftColumnCount)"/> <x:w-x:n-current-width"x:s-number(0)"/> </x:ct-> </x:va-> <x:va-x:n-unfrozen-columns-width"> <x:ct-x:n-get-pane-width"> <x:w-x:n-start-column"x:s-number($g/@FrozenLeftColumnCount)+1"/> <x:w-x:n-end-column"x:s-number($g/@ColumnCount)"/> <x:w-x:n-current-width"x:s-number(0)"/> </x:ct-> </x:va-> <x:va-x:n-total-columns-width"> <x:v-x:s-number($frozen-columns-width) + number($unfrozen-columns-width)"/> </x:va-> <x:va-x:n-scrollerHeight"x:s-number($g/@Height)-(number($g/@scrollbarHeight)*$showhscroll)-(number($g/@ToolbarHeight)*$showtoolbar)" /> <x:va-x:n-scrollerWidth"x:s-number($g/@Width)-(number($g/@scrollbarWidth)*number($g/@VScrollbarEnabled))" /> <x:va-x:n-midHeight"x:s-number($g/@Height)-(number($g/@scrollbarHeight)*$showhscroll)-(number($g/@ToolbarHeight)*$showtoolbar)-number($g/@top)"/> #grid<x:v-x:s-$u" /> { height:<x:v-x:s-$g/@Height" />px; width:<x:v-x:s-$g/@Width" />px; overflow:hidden;text-align:left; <xsl:if test="$IE=\'true\'"> position:relative; </xsl:if> } .hScrollbarRange<x:v-x:s-$u" /> { width:<x:v-x:s-$total-columns-width"/>px; } .vScrollbarRange<x:v-x:s-$u" /> {} .ntb-grid-datablock, .ntb-grid-headerblock { table-layout:fixed; <xsl:if test="$IE=\'true\'"> width:0px; </xsl:if> } .<x:v-x:s-$t"/> .ntb-cell {overflow:hidden;white-space:nowrap;} .<x:v-x:s-$t"/> .ntb-cell-border {overflow:hidden;white-space:nowrap;<xsl:if test="$IE=\'true\'">height:auto;</xsl:if>} .ntb-grid-headershow<x:v-x:s-$u" /> {padding:0px;<xsl:if test="not($g/@ColumnIndicatorsEnabled=1)">display:none;</xsl:if>} .ntb-grid-vscrollshow<x:v-x:s-$u" /> {padding:0px;<xsl:if test="not($g/@VScrollbarEnabled=1)">display:none;</xsl:if>} #ntb-grid-hscrollshow<x:v-x:s-$u" /> {padding:0px;<xsl:if test="not($g/@HScrollbarEnabled=1)">display:none;</xsl:if>} .ntb-grid-toolbarshow<x:v-x:s-$u" /> {<xsl:if test="not($g/@ToolbarEnabled=1) and not($g/@ToolbarEnabled=\'true\')">display:none;</xsl:if>} .ntb-grid-height<x:v-x:s-$u" /> {height:<x:v-x:s-$g/@Height" />px;overflow:hidden;} .ntb-grid-width<x:v-x:s-$u" /> {width:<x:v-x:s-$g/@Width" />px;overflow:hidden;} .ntb-grid-overlay<x:v-x:s-$u" /> {position:relative;z-index:1000;top:0px;left:0px;} .ntb-grid-scroller<x:v-x:s-$u" /> { overflow:hidden; text-align:left; -moz-user-select: none; -webkit-user-select: none; -khtml-user-select: none; user-select: none; } .ntb-grid-scrollerheight<x:v-x:s-$u" /> {height: <x:c-><x:wh- test="($total-columns-width &gt; $g/@Width)"><x:v-x:s-$scrollerHeight"/></x:wh-><x:o-><x:v-x:s-number($scrollerHeight) + number($g/@scrollbarHeight)"/></x:o-></x:c->px;} .ntb-grid-scrollerwidth<x:v-x:s-$u" /> {width:<x:v-x:s-$scrollerWidth"/>px;} .ntb-grid-topheight<x:v-x:s-$u" /> {height:<x:v-x:s-$g/@top" />px;overflow:hidden;<xsl:if test="$g/@top=0">display:none;</xsl:if>} .ntb-grid-midheight<x:v-x:s-$u" /> {overflow:hidden;height:<x:c-><x:wh- test="($total-columns-width &gt; $g/@Width)"><x:v-x:s-$midHeight"/></x:wh-><x:o-><x:v-x:s-number($midHeight) + number($g/@scrollbarHeight)"/></x:o-></x:c->px;} .ntb-grid-leftwidth<x:v-x:s-$u" /> {width:<x:v-x:s-$g/@left" />px;overflow:hidden;text-align:left;} .ntb-grid-centerwidth<x:v-x:s-$u" /> {width:<x:v-x:s-number($g/@Width)-number($g/@left)-(number($g/@scrollbarWidth)*$showvscroll)" />px;} .ntb-grid-scrollbarheight<x:v-x:s-$u" /> {height:<x:v-x:s-$g/@scrollbarHeight" />px;} .ntb-grid-scrollbarwidth<x:v-x:s-$u" /> {width:<x:v-x:s-$g/@scrollbarWidth" />px;} .ntb-grid-toolbarheight<x:v-x:s-$u" /> {height:<x:v-x:s-$g/@ToolbarHeight" />px;} .ntb-grid-surfacewidth<x:v-x:s-$u" /> {width:<x:v-x:s-number($unfrozen-columns-width)"/>px;} .ntb-grid-surfaceheight<x:v-x:s-$u" /> {height:100px;} .ntb-grid {padding:0px;margin:0px;border:1px solid #cccccc} .ntb-scroller {padding:0px;} .ntb-scrollcorner {padding:0px;} .ntb-hscrollbar<x:v-x:s-$u" /> {<x:c-><x:wh- test="($total-columns-width &gt; $g/@Width)">display:block;</x:wh-><x:o->display:none;</x:o-></x:c->} .ntb-input-border { table-layout:fixed; overflow:hidden; position:absolute; z-index:2000; top:-2000px; left:-2000px; } .ntb-column-resize-surface { filter:alpha(opacity=1); background-color:white; position:absolute; display:none; top:-1000px; left:-5000px; width:100px; height:100px; z-index:800; } .<x:v-x:s-$t"/> .ntb-column-indicator { overflow:hidden; white-space: nowrap; } .ntb-row<x:v-x:s-$u" /> {height:<x:v-x:s-$g/@RowHeight" />px;line-height:<x:v-x:s-$g/@RowHeight" />px;margin:0px;} .ntb-header-row<x:v-x:s-$u" /> {height:<x:v-x:s-$g/@HeaderHeight" />px;} <x:at-x:s-state/nitobi.grid.Columns" /></x:t-><x:t-x:n-get-pane-width"> <x:p-x:n-start-column"/> <x:p-x:n-end-column"/> <x:p-x:n-current-width"/> <x:c-> <x:wh- test="$start-column &lt;= $end-column"> <x:ct-x:n-get-pane-width"> <x:w-x:n-start-column"x:s-$start-column+1"/> <x:w-x:n-end-column"x:s-$end-column"/> <x:w-x:n-current-width"x:s-number($current-width) + number(//state/nitobi.grid.Columns/nitobi.grid.Column[$start-column]/@Width)"/> </x:ct-> </x:wh-> <x:o-> <x:v-x:s-$current-width"/> </x:o-> </x:c-> </x:t-><x:t- match="nitobi.grid.Columns"> <xsl:for-eachx:s-*"> <x:va-x:n-p"><x:v-x:s-position()"/></x:va-> <x:va-x:n-w"><x:v-x:s-@Width"/></x:va-> #grid<x:v-x:s-$u" /> .ntb-column<x:v-x:s-$u" />_<xsl:number value="$p" /> {width:<x:v-x:s-number($w)-number($g/@CellBorder)" />px;} #grid<x:v-x:s-$u" /> .ntb-column-data<x:v-x:s-$u" />_<xsl:number value="$p" /> {text-align:<x:v-x:s-@Align"/>;} </xsl:for-each></x:t-></xsl:stylesheet>';
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.frameCssXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_frameCssXslProc));

var temp_ntb_frameXslProc='<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="1.0" xmlns:ntb="http://www.nitobi.com" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text" omit-xml-declaration="yes"/><x:p-x:n-browser"x:s-\'IE\'"/><x:p-x:n-scrollbarWidth"x:s-17" /><x:t- match = "/"><x:va-x:n-u"x:s-state/@uniqueID" /><x:va-x:n-Id"x:s-state/@ID" /><x:va-x:n-resizeEnabled"x:s-state/nitobi.grid.Grid/@GridResizeEnabled" /><x:va-x:n-frozenLeft"x:s-state/nitobi.grid.Grid/@FrozenLeftColumnCount" /><x:va-x:n-offset"> <x:c-> <x:wh- test="$browser=\'IE\'">1</x:wh-> <x:o->0</x:o-> </x:c-></x:va-> &lt;div id="grid<x:v-x:s-$u" />" class="ntb-grid ntb-grid-reset <x:v-x:s-state/nitobi.grid.Grid/@Theme" />" style="overflow:visible;"&gt; &lt;div style="height:0px;width:0px;position:relative;"&gt; &lt;div id="ntb-grid-overlay<x:v-x:s-$u" />" class="ntb-grid-overlay<x:v-x:s-$u" />"&gt;&lt;/div&gt; <!-- Firefox or IE just uses a hidden div for keynav since on Mac at least it doesn\'t capture the paste event on an input --> <xsl:if test="not($browser=\'SAFARI\')">&lt;div id="ntb-grid-keynav<x:v-x:s-$u" />" tabindex="1" style="position:absolute;left:-3000px;width:1px;height:1px;border:0px;background-color:transparent;"&gt;&lt;/div&gt;</xsl:if> <!-- Safari can\'t capture key events on divs so need to use an input --> <xsl:if test="$browser=\'SAFARI\'">&lt;input type="text" id="ntb-grid-keynav<x:v-x:s-$u" />" tabindex="1" style="position:absolute;left:-3000px;width:1px;height:1px;border:0px;background-color:transparent;"&gt;&lt;/input&gt;</xsl:if> &lt;/div&gt; &lt;table cellpadding="0" cellspacing="0" border="0"&gt; &lt;tr&gt; &lt;td id="ntb-grid-scroller<x:v-x:s-$u" />" class="ntb-grid-scrollerheight<x:v-x:s-$u" /> ntb-grid-scrollerwidth<x:v-x:s-$u" />" &gt; &lt;div id="ntb-grid-scrollerarea<x:v-x:s-$u" />" class="ntb-grid-scrollerheight<x:v-x:s-$u" />" style="overflow:hidden;" &gt; &lt;div tabindex="2" class="ntb-grid-scroller<x:v-x:s-$u" /> ntb-grid-scrollerheight<x:v-x:s-$u" />" &gt; &lt;table class="ntb-grid-scroller" cellpadding="0" cellspacing="0" border="0" &gt; &lt;tr id="ntb-grid-header<x:v-x:s-$u" />" class="ntb-grid-topheight<x:v-x:s-$u" /> " &gt; &lt;td class="ntb-scroller ntb-grid-topheight<x:v-x:s-$u" /> ntb-grid-leftwidth<x:v-x:s-$u" />" &gt; &lt;div id="gridvp_0_<x:v-x:s-$u" />" class="ntb-grid-topheight<x:v-x:s-$u" /> ntb-grid-leftwidth<x:v-x:s-$u" /> ntb-grid-header"&gt; &lt;div id="gridvpsurface_0_<x:v-x:s-$u" />" &gt; &lt;div id="gridvpcontainer_0_<x:v-x:s-$u" />" &gt;&lt;/div&gt; &lt;/div&gt; &lt;/div&gt; &lt;/td&gt; &lt;td class="ntb-scroller" &gt; &lt;div id="gridvp_1_<x:v-x:s-$u" />" class="ntb-grid-topheight<x:v-x:s-$u" /> ntb-grid-centerwidth<x:v-x:s-$u" /> ntb-grid-header"&gt; &lt;div id="gridvpsurface_1_<x:v-x:s-$u" />" class="ntb-grid-surfacewidth<x:v-x:s-$u" />" &gt; &lt;div id="gridvpcontainer_1_<x:v-x:s-$u" />" &gt;&lt;/div&gt; &lt;/div&gt; &lt;/div&gt; &lt;/td&gt; &lt;/tr&gt; &lt;tr id="ntb-grid-data<x:v-x:s-$u" />"class="ntb-grid-scroller" &gt; &lt;td class="ntb-scroller ntb-grid-leftwidth<x:v-x:s-$u" />" &gt; &lt;div style="position:relative;"&gt; <xsl:if test="not($browser=\'IE\') and not($frozenLeft=\'0\')"> &lt;div style="z-index:100;position:absolute;height:100%;top:0px;overflow:hidden;" id="ntb-frozenshadow<x:v-x:s-$u" />" class="ntb-frozenshadow"&gt;&lt;/div&gt; </xsl:if> &lt;div id="gridvp_2_<x:v-x:s-$u" />" class="ntb-grid-midheight<x:v-x:s-$u" /> ntb-grid-leftwidth<x:v-x:s-$u" />" style="position:relative;"&gt; &lt;div id="gridvpsurface_2_<x:v-x:s-$u" />" &gt; &lt;div id="gridvpcontainer_2_<x:v-x:s-$u" />" &gt;&lt;/div&gt; &lt;/div&gt; &lt;/div&gt; &lt;/div&gt; &lt;/td&gt; &lt;td class="ntb-scroller" &gt; &lt;div id="gridvp_3_<x:v-x:s-$u" />" class="ntb-grid-midheight<x:v-x:s-$u"/> ntb-grid-centerwidth<x:v-x:s-$u" />" style="position:relative;"&gt; &lt;div id="gridvpsurface_3_<x:v-x:s-$u" />" class="ntb-grid-surfacewidth<x:v-x:s-$u" />" &gt; &lt;div id="gridvpcontainer_3_<x:v-x:s-$u" />" &gt;&lt;/div&gt; &lt;/div&gt; &lt;/div&gt; &lt;/td&gt; &lt;/tr&gt; &lt;/table&gt; &lt;/div&gt; &lt;/div&gt; &lt;/td&gt; &lt;td id="ntb-grid-vscrollshow<x:v-x:s-$u" />" class="ntb-grid-scrollerheight<x:v-x:s-$u" />"&gt;&lt;div id="vscrollclip<x:v-x:s-$u" />" class="ntb-grid-scrollerheight<x:v-x:s-$u" /> ntb-grid-scrollbarwidth<x:v-x:s-$u"/> ntb-scrollbar" style="overflow:hidden;" &gt;&lt;div id="vscroll<x:v-x:s-$u" />" class="ntb-scrollbar" style="height:100%;width:<x:v-x:s-number($offset)+number(state/nitobi.grid.Grid/@scrollbarWidth)"/>px;position:relative;top:0px;left:-<x:v-x:s-$offset"/>px;overflow-x:hidden;overflow-y:scroll;" &gt;&lt;div class="vScrollbarRange<x:v-x:s-$u" />" style="WIDTH:1px;overflow:hidden;"&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;/td&gt; &lt;/tr&gt; &lt;tr id="ntb-grid-hscrollshow<x:v-x:s-$u" />" &gt; &lt;td &gt;&lt;div id="hscrollclip<x:v-x:s-$u" />" class="ntb-grid-scrollbarheight<x:v-x:s-$u" /> ntb-grid-scrollerwidth<x:v-x:s-$u" /> ntb-hscrollbar<x:v-x:s-$u" />" style="overflow:hidden;" &gt; &lt;div id="hscroll<x:v-x:s-$u" />" class="ntb-grid-scrollbarheight<x:v-x:s-$u" /> ntb-grid-scrollerwidth<x:v-x:s-$u" /> ntb-scrollbar" style="overflow-x:scroll;overflow-y:hidden;height:<x:v-x:s-number($offset)+number(state/nitobi.grid.Grid/@scrollbarHeight)"/>px;position:relative;top:-<x:v-x:s-$offset"/>px;left:0px;" &gt; &lt;div class="hScrollbarRange<x:v-x:s-$u" />" style="HEIGHT:1px;overflow:hidden;"&gt; &lt;/div&gt; &lt;/td&gt; &lt;td class="ntb-grid-vscrollshow<x:v-x:s-$u" /> ntb-scrollcorner" &gt;&lt;/td&gt; &lt;/tr&gt; &lt;/table&gt; &lt;div id="toolbarContainer<x:v-x:s-$u" />" style="overflow:hidden;" class="ntb-grid-toolbarshow<x:v-x:s-$u" /> ntb-grid-toolbarheight<x:v-x:s-$u" /> ntb-grid-width<x:v-x:s-$u" /> ntb-toolbar<x:v-x:s-$u" /> ntb-toolbar"&gt;&lt;/div&gt; &lt;div id="ntb-grid-toolscontainer<x:v-x:s-$u"/>" style="height:0px;"&gt; <!-- In IE quirks the textarea has a forced height so need it to have a relative positioned container --> &lt;div style="position:relative;overflow:hidden;height:0px;"&gt; &lt;textarea id="ntb-clipboard<x:v-x:s-$u"/>" class="ntb-clipboard" &gt;&lt;/textarea&gt; &lt;/div&gt; &lt;div style="position:relative;"&gt; &lt;div id="ntb-column-resizeline<x:v-x:s-$u" />" class="ntb-column-resizeline"&gt;&lt;/div&gt; &lt;div id="ntb-grid-resizebox<x:v-x:s-$u" />" class="ntb-grid-resizebox"&gt;&lt;/div&gt; &lt;/div&gt; &lt;/div&gt; <xsl:if test="$resizeEnabled = 1"> &lt;div id="ntb-grid-resizecontainer<x:v-x:s-$u"/>" style="height:0px;position:relative;"&gt; &lt;div id="ntb-grid-resizeright<x:v-x:s-$u" />" class="ntb-resize-indicator-right"&gt;&lt;/div&gt; &lt;div id="ntb-grid-resizebottom<x:v-x:s-$u" />" class="ntb-resize-indicator-bottom"&gt;&lt;/div&gt; &lt;/div&gt; </xsl:if> &lt;/div&gt;</x:t-></xsl:stylesheet>';
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.frameXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_frameXslProc));

var temp_ntb_listboxXslProc='<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com"> <xsl:output method="xml" omit-xml-declaration="yes"/> <x:p-x:n-size"></x:p-> <x:p-x:n-DisplayFields"x:s-\'\'"></x:p-> <x:p-x:n-ValueField"x:s-\'\'"></x:p-> <x:p-x:n-val"x:s-\'\'"></x:p-> <x:t- match="/"> <!--<x:va-x:n-cell"x:s-/root/metadata/r[@xi=$row]/*[@xi=$col]"></x:va->--> <select id="ntb-listbox" class="ntb-input ntb-lookup-options"> <xsl:if test="$size"> <x:a-x:n-size">6</x:a-> </xsl:if> <!--<x:c-> <x:wh- test="$DatasourceId">--> <xsl:for-eachx:s-/ntb:datasource/ntb:data/*"> <xsl:sortx:s-@*[name(.)=substring-before($DisplayFields,\'|\')]" data-type="text" order="ascending" /> <option> <x:a-x:n-value"> <x:v-x:s-@*[name(.)=$ValueField]"></x:v-> </x:a-> <x:a-x:n-rn"> <x:v-x:s-position()"></x:v-> </x:a-> <xsl:if test="@*[name(.)=$ValueField and .=$val]"> <x:a-x:n-selected">true</x:a-> </xsl:if> <x:ct-x:n-print-displayfields"> <x:w-x:n-field"x:s-$DisplayFields" /> </x:ct-> </option> </xsl:for-each> <!--</x:wh-> <x:o-> </x:o-> </x:c->--> </select> </x:t-> <x:t-x:n-print-displayfields"> <x:p-x:n-field" /> <x:c-> <x:wh- test="contains($field,\'|\')" > <!-- Here we hardcode a spacer \', \' - this should probably be moved elsewhere. --> <x:v-x:s-concat(@*[name(.)=substring-before($field,\'|\')],\', \')"></x:v-> <x:ct-x:n-print-displayfields"> <x:w-x:n-field"x:s-substring-after($field,\'|\')" /> </x:ct-> </x:wh-> <x:o-> <x:v-x:s-@*[name(.)=$field]"></x:v-> </x:o-> </x:c-> </x:t-> </xsl:stylesheet>';
nitobi.lang.defineNs("nitobi.form");
nitobi.form.listboxXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_listboxXslProc));

var temp_ntb_mergeEbaXmlToLogXslProc='<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com"> <xsl:output method="xml" omit-xml-declaration="yes"/> <x:p-x:n-defaultAction"></x:p-> <x:p-x:n-startXid"x:s-100" ></x:p-> <xsl:keyx:n-newData" match="/ntb:grid/ntb:newdata/ntb:data/ntb:e" use="@xid" /> <xsl:keyx:n-oldData" match="/ntb:grid/ntb:datasources/ntb:datasource/ntb:data/ntb:e" use="@xid" /> <x:t- match="@* | node()" > <xsl:copy> <x:at-x:s-@*|node()" /> </xsl:copy> </x:t-> <x:t- match="/ntb:grid/ntb:datasources/ntb:datasource/ntb:data/ntb:e"> <xsl:if test="not(key(\'newData\',@xid))"> <xsl:copy> <xsl:copy-ofx:s-@*" /> </xsl:copy> </xsl:if> </x:t-> <x:t- match="/ntb:grid/ntb:datasources/ntb:datasource/ntb:data"> <xsl:copy> <x:at-x:s-@*|node()" /> <xsl:for-eachx:s-/ntb:grid/ntb:newdata/ntb:data/ntb:e"> <xsl:copy> <xsl:copy-ofx:s-@*" /> <xsl:if test="$defaultAction"> <x:va-x:n-oldNode"x:s-key(\'oldData\',@xid)" /> <x:c-> <x:wh- test="$oldNode"> <x:va- name=\'xid\'x:s-@xid" /> <x:a-x:n-xac"><x:v-x:s-$oldNode/@xac" /></x:a-> </x:wh-> <x:o-> <x:a-x:n-xac"><x:v-x:s-$defaultAction" /></x:a-> </x:o-> </x:c-> </xsl:if> </xsl:copy> </xsl:for-each> </xsl:copy> </x:t-></xsl:stylesheet> ';
nitobi.lang.defineNs("nitobi.data");
nitobi.data.mergeEbaXmlToLogXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_mergeEbaXmlToLogXslProc));

var temp_ntb_mergeEbaXmlXslProc='<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com"> <xsl:output method="xml" omit-xml-declaration="no" /> <x:p-x:n-startRowIndex"x:s-100" ></x:p-> <x:p-x:n-endRowIndex"x:s-200" ></x:p-> <x:p-x:n-guid"x:s-1"></x:p-> <xsl:keyx:n-newData" match="/ntb:grid/ntb:newdata/ntb:data/ntb:e" use="@xi" /> <xsl:keyx:n-oldData" match="/ntb:grid/ntb:datasources/ntb:datasource/ntb:data/ntb:e" use="@xi" /> <x:t- match="@* | node()" > <xsl:copy> <x:at-x:s-@*|node()" /> </xsl:copy> </x:t-> <x:t- match="/ntb:grid/ntb:datasources/ntb:datasource/ntb:data/ntb:e"> <x:c-> <x:wh- test="(number(@xi) &gt;= $startRowIndex) and (number(@xi) &lt;= $endRowIndex)"> <xsl:copy> <xsl:copy-ofx:s-@*" /> <xsl:copy-ofx:s-key(\'newData\',@xi)/@*" /> </xsl:copy> </x:wh-> <x:o-> <xsl:copy> <x:at-x:s-@*|node()" /> </xsl:copy> </x:o-> </x:c-> </x:t-> <x:t- match="/ntb:grid/ntb:datasources/ntb:datasource/ntb:data"> <xsl:copy> <x:at-x:s-@*|node()" /> <xsl:for-eachx:s-/ntb:grid/ntb:newdata/ntb:data/ntb:e"> <xsl:if test="not(key(\'oldData\',@xi))"> <xsl:elementx:n-ntb:e" namespace="http://www.nitobi.com"> <xsl:copy-ofx:s-@*" /> <x:a-x:n-xid"><x:v-x:s-generate-id(.)"/><x:v-x:s-position()"/><x:v-x:s-$guid"/></x:a-> </xsl:element> </xsl:if> </xsl:for-each> </xsl:copy> </x:t-> <x:t- match="/ntb:grid/ntb:newdata/ntb:data/ntb:e"> <xsl:copy> <xsl:copy-ofx:s-@*" /> <x:va-x:n-oldData"x:s-key(\'oldData\',@xi)"/> <x:c-> <x:wh- test="$oldData"> <xsl:copy-ofx:s-$oldData/@*" /> <xsl:copy-ofx:s-@*" /> <x:a-x:n-xac">u</x:a-> <xsl:if test="$oldData/@xac=\'i\'"> <x:a-x:n-xac">i</x:a-> </xsl:if> </x:wh-> <x:o-> <x:a-x:n-xid"><x:v-x:s-generate-id(.)"/><x:v-x:s-position()"/><x:v-x:s-$guid"/></x:a-> <x:a-x:n-xac">i</x:a-> </x:o-> </x:c-> </xsl:copy> </x:t-> </xsl:stylesheet> ';
nitobi.lang.defineNs("nitobi.data");
nitobi.data.mergeEbaXmlXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_mergeEbaXmlXslProc));

var temp_ntb_numberFormatTemplatesXslProc='<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com" xmlns:d="http://exslt.org/dates-and-times" xmlns:n="http://www.nitobi.com/exslt/numbers" extension-element-prefixes="d n"> <!--http://www.w3schools.com/xsl/func_formatnumber.asp--><!-- <xsl:decimal-formatx:n-name" decimal-separator="char" grouping-separator="char" infinity="string" minus-sign="char" NaN="string" percent="char" per-mille="char" zero-digit="char" digit="char" pattern-separator="char"/> --><xsl:decimal-formatx:n-NA" decimal-separator="." grouping-separator="," /><xsl:decimal-formatx:n-EU" decimal-separator="," grouping-separator="." /><x:t-x:n-n:format"> <x:p-x:n-number"x:s-0" /> <x:p-x:n-mask"x:s-\'#.00\'" /> <x:p-x:n-group"x:s-\',\'" /> <x:p-x:n-decimal"x:s-\'.\'" /> <x:va-x:n-formattedNumber"> <x:c-> <x:wh- test="$group=\'.\' and $decimal=\',\'"> <x:v-x:s-format-number($number, $mask, \'EU\')" /> </x:wh-> <x:o-> <x:v-x:s-format-number($number, $mask, \'NA\')" /> </x:o-> </x:c-> </x:va-> <xsl:if test="not(string($formattedNumber) = \'NaN\')"> <x:v-x:s-$formattedNumber" /> </xsl:if></x:t-></xsl:stylesheet>';
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.numberFormatTemplatesXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_numberFormatTemplatesXslProc));

var temp_ntb_numberXslProc='<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com" xmlns:d="http://exslt.org/dates-and-times" xmlns:n="http://www.nitobi.com/exslt/numbers" extension-element-prefixes="d n"><xsl:output method="text" version="4.0" omit-xml-declaration="yes" /><x:p-x:n-number"x:s-0" /><x:p-x:n-mask"x:s-\'#.00\'" /><x:p-x:n-group"x:s-\',\'" /><x:p-x:n-decimal"x:s-\'.\'" /> <!--http://www.w3schools.com/xsl/func_formatnumber.asp--><!-- <xsl:decimal-formatx:n-name" decimal-separator="char" grouping-separator="char" infinity="string" minus-sign="char" NaN="string" percent="char" per-mille="char" zero-digit="char" digit="char" pattern-separator="char"/> --><xsl:decimal-formatx:n-NA" decimal-separator="." grouping-separator="," /><xsl:decimal-formatx:n-EU" decimal-separator="," grouping-separator="." /><x:t-x:n-n:format"> <x:p-x:n-number"x:s-0" /> <x:p-x:n-mask"x:s-\'#.00\'" /> <x:p-x:n-group"x:s-\',\'" /> <x:p-x:n-decimal"x:s-\'.\'" /> <x:va-x:n-formattedNumber"> <x:c-> <x:wh- test="$group=\'.\' and $decimal=\',\'"> <x:v-x:s-format-number($number, $mask, \'EU\')" /> </x:wh-> <x:o-> <x:v-x:s-format-number($number, $mask, \'NA\')" /> </x:o-> </x:c-> </x:va-> <xsl:if test="not(string($formattedNumber) = \'NaN\')"> <x:v-x:s-$formattedNumber" /> </xsl:if></x:t-><x:t- match="/"> <x:ct-x:n-n:format"> <x:w-x:n-number"x:s-$number" /> <x:w-x:n-mask"x:s-$mask" /> <x:w-x:n-group"x:s-$group" /> <x:w-x:n-decimal"x:s-$decimal" /> </x:ct-></x:t-></xsl:stylesheet>';
nitobi.lang.defineNs("nitobi.form");
nitobi.form.numberXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_numberXslProc));

var temp_ntb_rowXslProc='<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com" xmlns:d="http://exslt.org/dates-and-times" xmlns:n="http://www.nitobi.com/exslt/numbers" extension-element-prefixes="d n"><xsl:output method="xml" omit-xml-declaration="yes"/> <x:p-x:n-showHeaders"x:s-\'0\'" /><x:p-x:n-firstColumn"x:s-\'0\'" /><x:p-x:n-lastColumn"x:s-\'0\'" /><x:p-x:n-uniqueId"x:s-\'0\'" /><x:p-x:n-rowHover"x:s-\'0\'" /><x:p-x:n-frozenColumnId"x:s-\'\'" /><x:p-x:n-start" /><x:p-x:n-end" /><x:p-x:n-activeColumn"x:s-\'0\'" /><x:p-x:n-activeRow"x:s-\'0\'" /><x:p-x:n-sortColumn"x:s-\'0\'" /><x:p-x:n-toolTipsEnabled"x:s-\'0\'" /><x:p-x:n-sortDirection"x:s-\'Asc\'" /><x:p-x:n-dataTableId"x:s-\'_default\'" /><x:p-x:n-columns"x:s-/ntb:root/ntb:columns/*/*" /><xsl:keyx:n-data-source" match="//ntb:datasources/ntb:datasource" use="@id" /><xsl:keyx:n-group" match="ntb:e" use="@a" /><!-- <xsl:for-eachx:s-ntb:e[count(. | key(\'group\', @a)[1]) = 1]"> <xsl:sortx:s-@a" /> <x:v-x:s-@a" />,<br /> <xsl:for-eachx:s-key(\'group\', @a)"> <xsl:sortx:s-@b" /> <x:v-x:s-@b" /> (<x:v-x:s-@c" />)<br /> </xsl:for-each> </xsl:for-each>--><!--This is an incude for the date fromatting XSLT that gets replaced at compile time--> <!-- http://java.sun.com/j2se/1.3/docs/api/java/text/SimpleDateFormat.html --><d:ms> <d:m i="1" l="31" a="Jan">January</d:m> <d:m i="2" l="28" a="Feb">February</d:m> <d:m i="3" l="31" a="Mar">March</d:m> <d:m i="4" l="30" a="Apr">April</d:m> <d:m i="5" l="31" a="May">May</d:m> <d:m i="6" l="30" a="Jun">June</d:m> <d:m i="7" l="31" a="Jul">July</d:m> <d:m i="8" l="31" a="Aug">August</d:m> <d:m i="9" l="30" a="Sep">September</d:m> <d:m i="10" l="31" a="Oct">October</d:m> <d:m i="11" l="30" a="Nov">November</d:m> <d:m i="12" l="31" a="Dec">December</d:m></d:ms><d:ds> <d:d a="Sun">Sunday</d:d> <d:d a="Mon">Monday</d:d> <d:d a="Tue">Tuesday</d:d> <d:d a="Wed">Wednesday</d:d> <d:d a="Thu">Thursday</d:d> <d:d a="Fri">Friday</d:d> <d:d a="Sat">Saturday</d:d></d:ds><x:t-x:n-d:format-date"> <x:p-x:n-date-time" /> <x:p-x:n-mask"x:s-\'MMM d, yy\'"/> <x:p-x:n-date-year" /> <x:va-x:n-formatted"> <x:va-x:n-date-time-length"x:s-string-length($date-time)" /> <x:va-x:n-timezone"x:s-\'\'" /> <x:va-x:n-dt"x:s-substring($date-time, 1, $date-time-length - string-length($timezone))" /> <x:va-x:n-dt-length"x:s-string-length($dt)" /> <x:c-> <x:wh- test="substring($dt, 3, 1) = \':\' and substring($dt, 6, 1) = \':\'"> <!--that means we just have a time--> <x:va-x:n-hour"x:s-substring($dt, 1, 2)" /> <x:va-x:n-min"x:s-substring($dt, 4, 2)" /> <x:va-x:n-sec"x:s-substring($dt, 7)" /> <xsl:if test="$hour &lt;= 23 and $min &lt;= 59 and $sec &lt;= 60"> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-\'NaN\'" /> <x:w-x:n-month"x:s-\'NaN\'" /> <x:w-x:n-day"x:s-\'NaN\'" /> <x:w-x:n-hour"x:s-$hour" /> <x:w-x:n-minute"x:s-$min" /> <x:w-x:n-second"x:s-$sec" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </xsl:if> </x:wh-> <x:wh- test="substring($dt, 2, 1) = \'-\' or substring($dt, 3, 1) = \'-\'"> <x:c-> <x:wh- test="$dt-length = 5 or $dt-length = 6"> <!--D-MMM,DD-MMM--> <x:va-x:n-year"x:s-$date-year" /> <x:va-x:n-month"x:s-document(\'\')/*/d:ms/d:m[@a = substring-after($dt,\'-\')]/@i" /> <x:va-x:n-day"x:s-substring-before($dt,\'-\')" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:wh-> <x:wh- test="$dt-length = 8 or $dt-length = 9"> <!--D-MMM-YY,DD-MMM-YY--> <x:va-x:n-year"x:s-concat(\'20\',substring-after(substring-after($dt,\'-\'),\'-\'))" /> <x:va-x:n-month"x:s-document(\'\')/*/d:ms/d:m[@a = substring-before(substring-after($dt,\'-\'),\'-\')]/@i" /> <x:va-x:n-day"x:s-substring-before($dt,\'-\')" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:wh-> <x:o-> <!--D-MMM-YYYY,DD-MMM-YYYY--> <x:va-x:n-year"x:s-substring-after(substring-after($dt,\'-\'),\'-\')" /> <x:va-x:n-month"x:s-document(\'\')/*/d:ms/d:m[@a = substring-before(substring-after($dt,\'-\'),\'-\')]/@i" /> <x:va-x:n-day"x:s-substring-before($dt,\'-\')" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:o-> </x:c-> </x:wh-> <x:o-> <!--($neg * -2)--> <x:va-x:n-year"x:s-substring($dt, 1, 4) * (0 + 1)" /> <x:va-x:n-month"x:s-substring($dt, 6, 2)" /> <x:va-x:n-day"x:s-substring($dt, 9, 2)" /> <x:c-> <x:wh- test="$dt-length = 10"> <!--that means we just have a date--> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:wh-> <x:wh- test="substring($dt, 14, 1) = \':\' and substring($dt, 17, 1) = \':\'"> <!--that means we have a date + time--> <x:va-x:n-hour"x:s-substring($dt, 12, 2)" /> <x:va-x:n-min"x:s-substring($dt, 15, 2)" /> <x:va-x:n-sec"x:s-substring($dt, 18)" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-hour"x:s-$hour" /> <x:w-x:n-minute"x:s-$min" /> <x:w-x:n-second"x:s-$sec" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-$mask" /> </x:ct-> </x:wh-> </x:c-> </x:o-> </x:c-> </x:va-> <x:v-x:s-$formatted" /> </x:t-><x:t-x:n-d:_format-date"> <x:p-x:n-year" /> <x:p-x:n-month"x:s-1" /> <x:p-x:n-day"x:s-1" /> <x:p-x:n-hour"x:s-0" /> <x:p-x:n-minute"x:s-0" /> <x:p-x:n-second"x:s-0" /> <x:p-x:n-timezone"x:s-\'Z\'" /> <x:p-x:n-mask"x:s-\'\'" /> <x:va-x:n-char"x:s-substring($mask, 1, 1)" /> <x:c-> <x:wh- test="not($mask)" /> <!--replaced escaping with \' here/--> <x:wh- test="not(contains(\'GyMdhHmsSEDFwWakKz\', $char))"> <x:v-x:s-$char" /> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-hour"x:s-$hour" /> <x:w-x:n-minute"x:s-$minute" /> <x:w-x:n-second"x:s-$second" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-substring($mask, 2)" /> </x:ct-> </x:wh-> <x:o-> <x:va-x:n-next-different-char"x:s-substring(translate($mask, $char, \'\'), 1, 1)" /> <x:va-x:n-mask-length"> <x:c-> <x:wh- test="$next-different-char"> <x:v-x:s-string-length(substring-before($mask, $next-different-char))" /> </x:wh-> <x:o-> <x:v-x:s-string-length($mask)" /> </x:o-> </x:c-> </x:va-> <x:c-> <!--took our the era designator--> <x:wh- test="$char = \'M\'"> <x:c-> <x:wh- test="$mask-length >= 3"> <x:va-x:n-month-node"x:s-document(\'\')/*/d:ms/d:m[number($month)]" /> <x:c-> <x:wh- test="$mask-length >= 4"> <x:v-x:s-$month-node" /> </x:wh-> <x:o-> <x:v-x:s-$month-node/@a" /> </x:o-> </x:c-> </x:wh-> <x:wh- test="$mask-length = 2"> <x:v-x:s-format-number($month, \'00\')" /> </x:wh-> <x:o-> <x:v-x:s-$month" /> </x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'E\'"> <x:va-x:n-month-days"x:s-sum(document(\'\')/*/d:ms/d:m[position() &lt; $month]/@l)" /> <x:va-x:n-days"x:s-$month-days + $day + boolean(((not($year mod 4) and $year mod 100) or not($year mod 400)) and $month &gt; 2)" /> <x:va-x:n-y-1"x:s-$year - 1" /> <x:va-x:n-dow"x:s-(($y-1 + floor($y-1 div 4) - floor($y-1 div 100) + floor($y-1 div 400) + $days) mod 7) + 1" /> <x:va-x:n-day-node"x:s-document(\'\')/*/d:ds/d:d[number($dow)]" /> <x:c-> <x:wh- test="$mask-length >= 4"> <x:v-x:s-$day-node" /> </x:wh-> <x:o-> <x:v-x:s-$day-node/@a" /> </x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'a\'"> <x:c-> <x:wh- test="$hour >= 12">PM</x:wh-> <x:o->AM</x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'z\'"> <x:c-> <x:wh- test="$timezone = \'Z\'">UTC</x:wh-> <x:o->UTC<x:v-x:s-$timezone" /></x:o-> </x:c-> </x:wh-> <x:o-> <x:va-x:n-padding"x:s-\'00\'" /> <!--removed padding--> <x:c-> <x:wh- test="$char = \'y\'"> <x:c-> <x:wh- test="$mask-length &gt; 2"><x:v-x:s-format-number($year, $padding)" /></x:wh-> <x:o-><x:v-x:s-format-number(substring($year, string-length($year) - 1), $padding)" /></x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'d\'"> <x:v-x:s-format-number($day, $padding)" /> </x:wh-> <x:wh- test="$char = \'h\'"> <x:va-x:n-h"x:s-$hour mod 12" /> <x:c-> <x:wh- test="$h"><x:v-x:s-format-number($h, $padding)" /></x:wh-> <x:o-><x:v-x:s-format-number(12, $padding)" /></x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'H\'"> <x:v-x:s-format-number($hour, $padding)" /> </x:wh-> <x:wh- test="$char = \'k\'"> <x:c-> <x:wh- test="$hour"><x:v-x:s-format-number($hour, $padding)" /></x:wh-> <x:o-><x:v-x:s-format-number(24, $padding)" /></x:o-> </x:c-> </x:wh-> <x:wh- test="$char = \'K\'"> <x:v-x:s-format-number($hour mod 12, $padding)" /> </x:wh-> <x:wh- test="$char = \'m\'"> <x:v-x:s-format-number($minute, $padding)" /> </x:wh-> <x:wh- test="$char = \'s\'"> <x:v-x:s-format-number($second, $padding)" /> </x:wh-> <x:wh- test="$char = \'S\'"> <x:v-x:s-format-number(substring-after($second, \'.\'), $padding)" /> </x:wh-> <x:wh- test="$char = \'F\'"> <x:v-x:s-floor($day div 7) + 1" /> </x:wh-> <x:o-> <x:va-x:n-month-days"x:s-sum(document(\'\')/*/d:ms/d:m[position() &lt; $month]/@l)" /> <x:va-x:n-days"x:s-$month-days + $day + boolean(((not($year mod 4) and $year mod 100) or not($year mod 400)) and $month &gt; 2)" /> <x:v-x:s-format-number($days, $padding)" /> <!--removed week in year--> <!--removed week in month--> </x:o-> </x:c-> </x:o-> </x:c-> <x:ct-x:n-d:_format-date"> <x:w-x:n-year"x:s-$year" /> <x:w-x:n-month"x:s-$month" /> <x:w-x:n-day"x:s-$day" /> <x:w-x:n-hour"x:s-$hour" /> <x:w-x:n-minute"x:s-$minute" /> <x:w-x:n-second"x:s-$second" /> <x:w-x:n-timezone"x:s-$timezone" /> <x:w-x:n-mask"x:s-substring($mask, $mask-length + 1)" /> </x:ct-> </x:o-> </x:c-></x:t-><!--This is an incude for the number fromatting XSLT that gets replaced at compile time--> <!--http://www.w3schools.com/xsl/func_formatnumber.asp--><!-- <xsl:decimal-formatx:n-name" decimal-separator="char" grouping-separator="char" infinity="string" minus-sign="char" NaN="string" percent="char" per-mille="char" zero-digit="char" digit="char" pattern-separator="char"/> --><xsl:decimal-formatx:n-NA" decimal-separator="." grouping-separator="," /><xsl:decimal-formatx:n-EU" decimal-separator="," grouping-separator="." /><x:t-x:n-n:format"> <x:p-x:n-number"x:s-0" /> <x:p-x:n-mask"x:s-\'#.00\'" /> <x:p-x:n-group"x:s-\',\'" /> <x:p-x:n-decimal"x:s-\'.\'" /> <x:va-x:n-formattedNumber"> <x:c-> <x:wh- test="$group=\'.\' and $decimal=\',\'"> <x:v-x:s-format-number($number, $mask, \'EU\')" /> </x:wh-> <x:o-> <x:v-x:s-format-number($number, $mask, \'NA\')" /> </x:o-> </x:c-> </x:va-> <xsl:if test="not(string($formattedNumber) = \'NaN\')"> <x:v-x:s-$formattedNumber" /> </xsl:if></x:t-><x:t- match = "/"> <div> <xsl:if test="$showHeaders = 1"> <table cellpadding="0" cellspacing="0" border="0" class="ntb-grid-headerblock"> <tr> <x:a-x:n-class">ntb-header-row<x:v-x:s-$uniqueId" /></x:a-> <xsl:for-eachx:s-$columns"> <xsl:if test="@Visible = \'1\' and (position() &gt; $firstColumn and position() &lt;= $lastColumn)"> <td ebatype="columnheader" xi="{position()-1}" col="{position()-1}"> <x:a-x:n-id">columnheader_<x:v-x:s-position()-1"/>_<x:v-x:s-$uniqueId" /></x:a-> <x:a-x:n-onmouseover">$(\'grid<x:v-x:s-$uniqueId" />\').jsObject.handleHeaderMouseOver(this);</x:a-> <x:a-x:n-onmouseout">$(\'grid<x:v-x:s-$uniqueId" />\').jsObject.handleHeaderMouseOut(this);</x:a-> <!-- note that the ntb-columnUID_POSITION class is for a safari bug --> <x:a-x:n-class">ntb-column-indicator-border<x:c-><x:wh- test="$sortColumn=position()-1 and $sortDirection=\'Asc\'">ascending</x:wh-><x:wh- test="$sortColumn=position()-1 and $sortDirection=\'Desc\'">descending</x:wh-><x:o-></x:o-></x:c-><xsl:text> </xsl:text>ntb-column<x:v-x:s-$uniqueId"/>_<x:v-x:s-position()" /></x:a-> <div class="ntb-column-indicator"> <x:c-> <x:wh- test="@Label and not(@Label = \'\') and not(@Label = \' \')"><x:v-x:s-@Label" /></x:wh-> <x:wh- test="ntb:label and not(ntb:label = \'\') and not(ntb:label = \' \')"><x:v-x:s-ntb:label" /></x:wh-> <x:o->ATOKENTOREPLACE</x:o-> </x:c-> </div> </td> </xsl:if> </xsl:for-each> </tr> <x:ct-x:n-colgroup" /> </table> </xsl:if> <table cellpadding="0" cellspacing="0" border="0" class="ntb-grid-datablock"> <x:at-x:s-key(\'data-source\', $dataTableId)/ntb:data/ntb:e[@xi &gt;= $start and @xi &lt; $end]" > <xsl:sortx:s-@xi" data-type="number" /> </x:at-> <x:ct-x:n-colgroup" /> </table> </div></x:t-><x:t-x:n-colgroup"> <colgroup> <xsl:for-eachx:s-$columns"> <xsl:if test="@Visible = \'1\' and (position() &gt; $firstColumn and position() &lt;= $lastColumn)"> <col> <x:a-x:n-class">ntb-column<x:v-x:s-$uniqueId"/>_<x:v-x:s-position()" /><xsl:text> </xsl:text><xsl:if test="not(@Editable=\'1\')">ntb-column-readonly</xsl:if></x:a-> </col> </xsl:if> </xsl:for-each> </colgroup></x:t-><x:t- match="ntb:e"> <x:va-x:n-rowClass"> <xsl:if test="@xi mod 2 = 0">ntb-row-alternate</xsl:if> <!-- <xsl:if test="<x:v-x:s-@rowselectattr=1"/>">ebarowselected</xsl:if> --> </x:va-> <x:va-x:n-xi"x:s-@xi" /> <x:va-x:n-row"x:s-." /> <tr class="ntb-row {$rowClass} ntb-row{$uniqueId}" xi="{$xi}"> <x:a-x:n-id">row_<x:v-x:s-$xi" /><x:v-x:s-$frozenColumnId"/>_<x:v-x:s-$uniqueId" /></x:a-> <xsl:for-eachx:s-$columns"> <xsl:if test="@Visible = \'1\' and (position() &gt; $firstColumn and position() &lt;= $lastColumn)"> <x:ct-x:n-render-cell"> <x:w-x:n-row"x:s-$row"/> <x:w-x:n-xi"x:s-$xi"/> </x:ct-> </xsl:if> </xsl:for-each> </tr></x:t-> <x:t-x:n-render-cell"> <x:p-x:n-row" /> <x:p-x:n-xi" /> <x:va-x:n-xdatafld"x:s-substring-after(@xdatafld,\'@\')"/> <x:va-x:n-pos"x:s-position()-1"/> <x:va-x:n-value"><x:c-><x:wh- test="not(@xdatafld = \'\')"><x:v-x:s-$row/@*[name()=$xdatafld]" /></x:wh-><!-- @Value will actuall have some escaped XSLT in it like any other bound property --><x:o-><x:v-x:s-@Value" /></x:o-></x:c-></x:va-> <td ebatype="cell" style="vertical-align:middle;" id="cell_{$xi}_{$pos}_{$uniqueId}" xi="{$xi}" col="{$pos}"> <x:a-x:n-style"><x:ct-x:n-CssStyle"><x:w-x:n-row"x:s-$row"/></x:ct-></x:a-> <!-- note the use of the ntb-column<x:v-x:s-$uniqueId"/>_<x:v-x:s-position()" /> class ... that is for a safari bug --> <x:a-x:n-class">ntb-cell-border<xsl:text> </xsl:text>ntb-column-data<x:v-x:s-$uniqueId"/>_<x:v-x:s-position()" /><xsl:text> </xsl:text>ntb-column-<x:c-><x:wh- test="$sortColumn=$pos and $sortDirection=\'Asc\'">ascending</x:wh-><x:wh- test="$sortColumn=$pos and $sortDirection=\'Desc\'">descending</x:wh-><x:o-></x:o-></x:c-><xsl:text> </xsl:text>ntb-column-<x:v-x:s-@DataType"/><xsl:text> </xsl:text><x:ct-x:n-ClassName"><x:w-x:n-row"x:s-$row"/></x:ct-><xsl:text> </xsl:text><xsl:if test="@type = \'NUMBER\' and $value &lt; 0">ntb-cell-negativenumber</xsl:if><xsl:text> </xsl:text>ntb-column<x:v-x:s-$uniqueId"/>_<x:v-x:s-position()" /></x:a-> <div style="overflow:hidden;white-space:nowrap;"> <x:a-x:n-class">ntb-row<x:v-x:s-$uniqueId"/><xsl:text> </xsl:text>ntb-column-data<x:v-x:s-$uniqueId"/>_<x:v-x:s-position()" /><xsl:text> </xsl:text>ntb-cell</x:a-> <xsl:if test="$toolTipsEnabled=\'1\'"> <x:a-x:n-title"> <x:v-x:s-$value" /> </x:a-> </xsl:if> <x:at-x:s-."> <x:w-x:n-value"x:s-$value"/> </x:at-> </div> </td> </x:t-> <x:t- match="*[@type=\'TEXT\' or @type=\'\']"> <x:p-x:n-value" /> <x:ct-x:n-replaceblank"> <x:w-x:n-value"x:s-$value" /> </x:ct-> </x:t-> <x:t- match="*[@type=\'NUMBER\']"> <x:p-x:n-value" /> <x:va-x:n-number-mask"> <x:c-> <x:wh- test="@Mask"><x:v-x:s-@Mask" /></x:wh-> <x:o->#,###.00</x:o-> </x:c-> </x:va-> <x:va-x:n-negative-number-mask"> <x:c-> <x:wh- test="@NegativeMask and not(@NegativeMask=\'\')"><x:v-x:s-@NegativeMask" /></x:wh-> <x:o-><x:v-x:s-@NegativeMask" /></x:o-> </x:c-> </x:va-> <x:va-x:n-number"> <x:c-> <x:wh- test="$value &lt; 0"> <x:ct-x:n-n:format"> <x:w-x:n-number"x:s-translate($value,\'-\',\'\')" /> <x:w-x:n-mask"x:s-$negative-number-mask" /> <x:w-x:n-group"x:s-@GroupingSeparator" /> <x:w-x:n-decimal"x:s-@DecimalSeparator" /> </x:ct-> </x:wh-> <x:o-> <x:ct-x:n-n:format"> <x:w-x:n-number"x:s-$value" /> <x:w-x:n-mask"x:s-$number-mask" /> <x:w-x:n-group"x:s-@GroupingSeparator" /> <x:w-x:n-decimal"x:s-@DecimalSeparator" /> </x:ct-> </x:o-> </x:c-> </x:va-> <x:ct-x:n-replaceblank"> <x:w-x:n-value"x:s-$number" /> </x:ct-> </x:t-> <x:t- match="*[@type=\'LOOKUP\']"> <x:p-x:n-value" /> <x:va-x:n-valueField"x:s-@ValueField" /> <x:va-x:n-displayFields"x:s-@DisplayFields" /> <x:c-> <x:wh- test="$valueField = $displayFields"> <x:ct-x:n-replaceblank"> <x:w-x:n-value"x:s-$value" /> </x:ct-> </x:wh-> <x:o-> <x:ct-x:n-replaceblank"> <x:w-x:n-value"> <x:c-> <x:wh- test="@DatasourceId"> <x:va-x:n-preset-value" > <xsl:for-eachx:s-key(\'data-source\',@DatasourceId)//*"> <xsl:if test="@*[name(.)=$valueField and .=$value]"> <x:ct-x:n-print-displayfields"> <x:w-x:n-field"x:s-$displayFields" /> </x:ct-> </xsl:if> </xsl:for-each> </x:va-> <x:c-> <x:wh- test="$preset-value=\'\'"> <x:v-x:s-$value"/> </x:wh-> <x:o-> <x:v-x:s-$preset-value"/> </x:o-> </x:c-> </x:wh-> <x:o-> <x:v-x:s-$value"/> </x:o-> </x:c-> </x:w-> </x:ct-> </x:o-> </x:c-> </x:t-> <x:t- match="*[@type=\'LISTBOX\']"> <x:p-x:n-value" /> <x:va-x:n-valueField"x:s-@ValueField" /> <x:va-x:n-displayFields"x:s-@DisplayFields" /> <x:c-> <x:wh- test="@DatasourceId"> <x:va-x:n-temp-value"> <xsl:for-eachx:s-key(\'data-source\',@DatasourceId)//*"> <xsl:if test="@*[name(.)=$valueField and .=$value]"> <x:ct-x:n-replaceblank"> <x:w-x:n-value"> <x:ct-x:n-print-displayfields"> <x:w-x:n-field"x:s-$displayFields" /> </x:ct-> </x:w-> </x:ct-> </xsl:if> </xsl:for-each> </x:va-> <x:c-> <x:wh- test="not($temp-value = \'\')"> <x:v-x:s-$temp-value"/> </x:wh-> <x:o-> <x:ct-x:n-replaceblank"> <x:w-x:n-value"x:s-$value" /> </x:ct-> </x:o-> </x:c-> </x:wh-> <x:o-> <x:ct-x:n-replaceblank"> <x:w-x:n-value"x:s-$value" /> </x:ct-> </x:o-> </x:c-> </x:t-> <x:t- match="*[@type=\'CHECKBOX\']"> <x:p-x:n-value" /> <x:va-x:n-valueField"x:s-@ValueField" /> <x:va-x:n-displayFields"x:s-@DisplayFields" /> <xsl:for-eachx:s-key(\'data-source\',@DatasourceId)//*"> <xsl:if test="@*[name(.)=$valueField and .=$value]"> <x:va-x:n-checkString"> <x:c-> <x:wh- test="$value=@CheckedValue">checked</x:wh-> <x:o->unchecked</x:o-> </x:c-> </x:va-> <div style="overflow:hidden;"> <div class="ntb-checkbox ntb-checkbox-{$checkString}" checked="{$value}" width="10" >ATOKENTOREPLACE</div> <div class="ntb-checkbox-text"><x:v-x:s-@*[name(.)=$displayFields]" /></div> </div> </xsl:if> </xsl:for-each> </x:t-> <x:t- match="*[@type=\'IMAGE\']"> <x:p-x:n-value" /> <x:va-x:n-url"> <x:c-> <x:wh- test="@ImageUrl and not(@ImageUrl=\'\')"><x:v-x:s-@ImageUrl" /></x:wh-> <x:o-><x:v-x:s-$value" /></x:o-> </x:c-> </x:va-> <!-- image editor --> <img border="0" src="{$url}" align="middle" class="ntb-image" /> </x:t-> <x:t- match="*[@type=\'DATE\']"> <x:p-x:n-value" /> <x:va-x:n-date-mask"> <x:c-> <x:wh- test="@Mask"><x:v-x:s-@Mask" /></x:wh-> <x:o->MMM d, yy</x:o-> </x:c-> </x:va-> <x:va-x:n-date"> <x:ct-x:n-d:format-date"> <x:w-x:n-date-time"x:s-$value" /> <x:w-x:n-mask"x:s-$date-mask" /> </x:ct-> </x:va-> <x:ct-x:n-replaceblank"> <x:w-x:n-value"x:s-$date" /> </x:ct-> </x:t-> <x:t- match="*[@type=\'TEXTAREA\']"> <x:p-x:n-value" /> <x:ct-x:n-replace-break"> <x:w-x:n-text"> <x:ct-x:n-replaceblank"> <x:w-x:n-value"x:s-$value" /> </x:ct-> </x:w-> </x:ct-> </x:t-> <x:t- match="*[@type=\'PASSWORD\']">********</x:t-> <x:t- match="*[@type=\'LINK\']"> <x:p-x:n-value" /> <span class="ntb-hyperlink-editor"> <x:ct-x:n-replaceblank"> <x:w-x:n-value"x:s-$value" /> </x:ct-> </span> </x:t-> <x:t-x:n-placeholder"/><x:t-x:n-replaceblank"> <x:p-x:n-value" /> <x:c-> <x:wh- test="not($value) or $value = \'\' or $value = \' \'">ATOKENTOREPLACE</x:wh-> <x:o-><x:v-x:s-$value" /></x:o-> </x:c-></x:t-><x:t-x:n-replace"> <x:p-x:n-text"/> <x:p-x:n-search"/> <x:p-x:n-replacement"/> <x:c-> <x:wh- test="contains($text, $search)"> <x:v-x:s-substring-before($text, $search)"/> <x:v-x:s-$replacement"/> <x:ct-x:n-replace"> <x:w-x:n-text"x:s-substring-after($text,$search)"/> <x:w-x:n-search"x:s-$search"/> <x:w-x:n-replacement"x:s-$replacement"/> </x:ct-> </x:wh-> <x:o-> <x:v-x:s-$text"/> </x:o-> </x:c-></x:t-><x:t-x:n-print-displayfields"> <x:p-x:n-field" /> <x:c-> <x:wh- test="contains($field,\'|\')" > <!-- Here we hardcode a spacer \', \' - this should probably be moved elsewhere. --> <x:v-x:s-concat(@*[name(.)=substring-before($field,\'|\')],\', \')" /> <x:ct-x:n-print-displayfields"> <x:w-x:n-field"x:s-substring-after($field,\'|\')" /> </x:ct-> </x:wh-> <x:o-> <x:v-x:s-@*[name(.)=$field]" /> </x:o-> </x:c-></x:t-><x:t-x:n-replace-break"> <x:p-x:n-text"/> <x:ct-x:n-replace"> <x:w-x:n-text"x:s-$text"/> <x:w-x:n-search"x:s-\'&amp;amp;#xa;\'"/> <x:w-x:n-replacement"x:s-\'&amp;lt;br/&amp;gt;\'"/> </x:ct-></x:t-><x:t-x:n-ClassName"> <x:p-x:n-row"/> <x:va-x:n-class"x:s-@ClassName"/> <x:va-x:n-value"x:s-$row/@*[name()=$class]"/> <x:c-> <x:wh- test="$value"><x:v-x:s-$value"/></x:wh-> <x:o-><x:v-x:s-$class"/></x:o-> </x:c-></x:t-><x:t-x:n-CssStyle"> <x:p-x:n-row"/> <x:va-x:n-style"x:s-@CssStyle"/> <x:va-x:n-value"x:s-$row/@*[name()=$style]"/> <x:c-> <x:wh- test="$value"><x:v-x:s-$value"/></x:wh-> <x:o-><x:v-x:s-$style"/></x:o-> </x:c-></x:t-><!--This can be used as an insertion point for column templates--> <!--COLUMN-TYPE-TEMPLATES--></xsl:stylesheet>';
nitobi.lang.defineNs("nitobi.grid");
nitobi.grid.rowXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_rowXslProc));

var temp_ntb_sortXslProc='<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com"> <xsl:output method="xml" omit-xml-declaration="yes" /> <x:p-x:n-column"x:s-@xi"> </x:p-> <x:p-x:n-dir"x:s-\'ascending\'"> </x:p-> <x:p-x:n-type"x:s-\'text\'"> </x:p-> <x:t- match="*|@*"> <xsl:copy> <x:at-x:s-@*|node()" /> </xsl:copy> </x:t-> <x:t- match="ntb:data"> <xsl:copy> <x:at-x:s-@*"/> <xsl:for-eachx:s-ntb:e"> <xsl:sortx:s-@*[name() =$column]" order="{$dir}" data-type="{$type}"/> <xsl:copy> <x:a-x:n-xi"> <x:v-x:s-position()-1" /> </x:a-> <x:at-x:s-@*" /> </xsl:copy> </xsl:for-each> </xsl:copy> </x:t-><x:t- match="@xi" /></xsl:stylesheet>';
nitobi.lang.defineNs("nitobi.data");
nitobi.data.sortXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_sortXslProc));

var temp_ntb_fillColumnXslProc='<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com"> <xsl:output method="xml" omit-xml-declaration="no" /> <x:p-x:n-startRowIndex"x:s-0" ></x:p-> <x:p-x:n-endRowIndex"x:s-10000" ></x:p-> <x:p-x:n-value"x:s-test"></x:p-> <x:p-x:n-column"x:s-a"></x:p-> <x:t- match="@* | node()" > <xsl:copy> <x:at-x:s-@*|node()" /> </xsl:copy> </x:t-> <x:t- match="/ntb:grid/ntb:datasources/ntb:datasource/ntb:data/ntb:e"> <x:c-> <x:wh- test="(number(@xi) &gt;= $startRowIndex) and (number(@xi) &lt;= $endRowIndex)"> <xsl:copy> <xsl:copy-ofx:s-@*" /> <x:a-x:n-{$column}"><x:v-x:s-$value" /></x:a-> </xsl:copy> </x:wh-> <x:o-> <xsl:copy> <x:at-x:s-@*|node()" /> </xsl:copy> </x:o-> </x:c-> </x:t-></xsl:stylesheet> ';
nitobi.lang.defineNs("nitobi.data");
nitobi.data.fillColumnXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_fillColumnXslProc));

var temp_ntb_updategramTranslatorXslProc='<?xml version="1.0"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ntb="http://www.nitobi.com"> <xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes"/> <x:p-x:n-datasource-id"x:s-\'_default\'"></x:p-> <x:p-x:n-xkField" ></x:p-> <x:t- match="/"> <root> <x:at-x:s-//ntb:datasource[@id=$datasource-id]/ntb:data/ntb:e" /> </root> </x:t-> <x:t- match="ntb:e"> <x:c-> <x:wh- test="@xac=\'d\'"> <delete xi="{@xi}" xk="{@*[name() = $xkField]}"></delete> </x:wh-> <x:wh- test="@xac=\'i\'"> <insert><xsl:copy-ofx:s-@*[not(name() = $xkField) and not(name() = \'xac\')]" /><x:a-x:n-xk"><x:v-x:s-@*[name() = $xkField]" /></x:a-></insert> </x:wh-> <x:wh- test="@xac=\'u\'"> <update><xsl:copy-ofx:s-@*[not(name() = $xkField) and not(name() = \'xac\')]" /><x:a-x:n-xk"><x:v-x:s-@*[name() = $xkField]" /></x:a-></update> </x:wh-> </x:c-> </x:t-></xsl:stylesheet>';
nitobi.lang.defineNs("nitobi.data");
nitobi.data.updategramTranslatorXslProc = nitobi.xml.createXslProcessor(nitobiXmlDecodeXslt(temp_ntb_updategramTranslatorXslProc));


