/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    
    if ($('#div_nomenclaturas').is(':visible')) {
       cargarNomenclaturas();
    }else{
        cargarDepartamentos("CO", "departamento");
        $('#departamento').val('ATL');
        cargarCiudad('ATL', "ciudad");
        $('#ciudad').val('BQ');
        listarNomenclaturasRelCiudad();
        listarNomenclaturasCiudad();
        $("#departamento").change(function() {
            var op = $(this).find("option:selected").val();
            $("#bt_asignar_default").fadeIn();
            $("#bt_asociar_nomenclatura").fadeOut();
            $("#bt_desasociar_nomenclatura").fadeOut();
            $("#div_nomenclaturas_list").fadeOut();
            $("#div_nomenc_ciudad").fadeOut();
            cargarCiudad(op, "ciudad");     
        });

        $("#ciudad").change(function() {
            var op = $(this).find("option:selected").val();
            if (op === ''){
                $("#bt_asignar_default").fadeIn();              
                $("#bt_asociar_nomenclatura").fadeOut();
                $("#bt_desasociar_nomenclatura").fadeOut();
                $("#div_nomenclaturas_list").fadeOut();
                $("#div_nomenc_ciudad").fadeOut();
            }else{
                $("#bt_asignar_default").fadeOut(); 
                listarNomenclaturasRelCiudad();
                listarNomenclaturasCiudad();
            }
           
        });
        
        $("#bt_asignar_default").click(function() {
            if($('#departamento').val()===''){
                mensajesDelSistema("Debe seleccionar un departamento", '250', '150');
            }else{
                asignarConfiguracionDefault($('#departamento').val());
            }
        });
       
    }
   
});

function cargarDepartamentos(codigo, combo) {
  if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "/fintra/controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 13,
                cod_pais: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
//                      $('#'+combo).append("<option value=''>Seleccione</option>");                 

                        for (var key in json) {
                            $('#'+combo).append('<option value=' + key + '>' + json[key] + '</option>');                      
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
  }
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Todas</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function listarNomenclaturasRelCiudad() {
    var codCiudad = $('#ciudad').val();
    $("#div_nomenc_ciudad").fadeIn();
    $("#bt_desasociar_nomenclatura").fadeIn();   
    if ($("#gview_nomenc_ciudad").length) {
        reloadGridNomenclaturasRelCiudad(codCiudad);
    } else {
        $("#nomenc_ciudad").jqGrid({
            caption: 'Nomenclaturas asociadas',
            url: './controller?estado=GestionSolicitud&accion=Aval&opcion=cargarNomenclaturasRel&ciu='+codCiudad, 
            datatype: 'json',
            height:  384,
            width: 'auto',           
           // editurl: 'clientArray',
            colNames: ['C�digo', 'Nombre', 'Ciudad', 'Estado'],
            colModel: [             
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '80px', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '250px'},
                {name: 'ciudad', index: 'ciudad', sortable: true, hidden: true, align: 'left', width: '100px'},
                {name: 'reg_status', index: 'reg_status', sortable: true, hidden: true, align: 'center', width: '80px'}             
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {                                      
                    $('#bt_asociar_nomenclatura').show();  
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
        
    }
}

function reloadGridNomenclaturasRelCiudad(codCiudad){
    var url = './controller?estado=GestionSolicitud&accion=Aval&opcion=cargarNomenclaturasRel&ciu='+codCiudad;
    jQuery("#nomenc_ciudad").setGridParam({
        datatype: 'json',        
        url: url
    });    
    jQuery('#nomenc_ciudad').trigger("reloadGrid");
}

function listarNomenclaturasCiudad(){
    var codCiudad = $('#ciudad').val();  
    $("#div_nomenclaturas_list").fadeIn();
    $('#bt_asociar_nomenclatura').fadeIn();    
    if ($("#gview_listNomenclaturas").length) {
        reloadGridNomenclaturasCiudad(codCiudad);
    } else {              
            jQuery("#listNomenclaturas").jqGrid({
                caption: 'Nomenclaturas',
                url: './controller?estado=GestionSolicitud&accion=Aval&opcion=cargarNomenclaturas&ciu='+codCiudad,
                datatype: 'json',
                height: 368,
                width: 'auto',
                colNames: ['C�digo', 'Nombre', 'Descripcion', 'Default', 'Estado'],
                colModel: [
                    {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '80px', key: true},
                    {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '250px'},
                    {name: 'descripcion', index: 'descripcion', sortable: true,hidden:true, align: 'left', width: '250px'},
                    {name: 'is_default', index: 'is_default', sortable: true,hidden:true, align: 'left', width: '80px'},
                    {name: 'reg_status', index: 'reg_status', sortable: true, hidden: true, align: 'center', width: '80px'}
                ],
                rowNum: 1000,
                rowTotal: 50000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                ignoreCase: true,
                pgtext: null,
                pgbuttons: false,
                multiselect: true,
                pager:'#page_tabla_nomenclatura',
                jsonReader: {
                    root: 'rows',
                    repeatitems: false,
                    id: '0'
                },              
                gridComplete: function() {                 
                    $('#bt_desasociar_nomenclatura').show();
                },
                loadError: function(xhr, status, error) {
                    mensajesDelSistema(error, 250, 150);
                }
            });             
       
    }
}

function reloadGridNomenclaturasCiudad(codCiudad){
    var url = './controller?estado=GestionSolicitud&accion=Aval&opcion=cargarNomenclaturas&ciu='+codCiudad;
    jQuery("#listNomenclaturas").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#listNomenclaturas').trigger("reloadGrid");
}

function desasociarNomenclatura(){
    
    var codCiudad = $('#ciudad').val();
    var listado = "";
    var filasId =jQuery('#nomenc_ciudad').jqGrid('getGridParam', 'selarrrow');
    if(filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        $.ajax({
            url: './controller?estado=GestionSolicitud&accion=Aval',
            type: "POST",          
            dataType: "json",
            async:false,
            data:{
                opcion: 'desasociarNomenclatura',
                listado: listado,
                codCiudad: codCiudad
            },       
            success: function(json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }

                        if (json.respuesta === "OK") {                              
                           reloadGridNomenclaturasRelCiudad(codCiudad);
                           reloadGridNomenclaturasCiudad(codCiudad);                          
                        }

                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo desasignar la nomenclatura", '250', '150');
                    }

            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }else{
        if (jQuery("#nomenc_ciudad").jqGrid('getGridParam', 'records')>0) {
           mensajesDelSistema("Debe seleccionar las nomenclaturas a desasignar", '250', '150');
        } else {
            mensajesDelSistema("No hay nomenclaturas por desasignar", '250', '150');
        }         
    }
}

function asociarNomenclatura(){
    var codCiudad = $('#ciudad').val();
     var jsonObj = [];
    var listado = "";
    var filasId =jQuery('#listNomenclaturas').jqGrid('getGridParam', 'selarrrow');
    if(filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            var id = filasId[i];          
            var nombre = jQuery("#listNomenclaturas").getRowData(id).nombre;
            var descripcion = jQuery("#listNomenclaturas").getRowData(id).descripcion;
            var item = {};
            item ["idNomenclatura"] = id;
            item ["nombre"] = nombre;
            item ["descripcion"] = descripcion;
            jsonObj.push(item);
        }
       var listNomenc = {};
       listNomenc ["nomenclaturas"] = jsonObj;
        
        var url = './controller?estado=GestionSolicitud&accion=Aval';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async:false,
            data: {
                opcion: 'asociarNomenclatura',
                listado: JSON.stringify(listNomenc),
                cod_ciu: codCiudad
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                  
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {                      
                        reloadGridNomenclaturasRelCiudad(codCiudad);
                        reloadGridNomenclaturasCiudad(codCiudad);   
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asignar la nomenclatura a la ciudad!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            }      
        });
        
    }else{
         if (jQuery("#listNomenclaturas").jqGrid('getGridParam', 'records')>0) {
            mensajesDelSistema("Debe seleccionar las nomenclaturas a asociar", '250', '150');
        } else {
            mensajesDelSistema("No hay nomenclaturas por asociar", '250', '150');
        }         
    }
}


function cargarNomenclaturas(){   
    
    if ($("#gview_tabla_nomenclaturas").length) {
        reloadGridNomenclaturas();
    } else {              
            jQuery("#tabla_nomenclaturas").jqGrid({
                caption: 'Listado de Nomenclaturas',
                url: './controller?estado=GestionSolicitud&accion=Aval&opcion=cargarNomenclaturas',
                datatype: 'json',
                height: 368,
                width: 'auto',
                colNames: ['C�digo', 'Nombre', 'Descripcion', 'Por Defecto', 'Estado', 'Acciones'],
                colModel: [
                    {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '80px', key: true},
                    {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '250px'},
                    {name: 'descripcion', index: 'descripcion', sortable: true,hidden:true, align: 'left', width: '250px'},
                    {name: 'is_default', index: 'is_default', sortable: true, align: 'center', width: '80px'},
                    {name: 'reg_status', index: 'reg_status', sortable: true, hidden: true, align: 'center', width: '80px'},
                    {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '110px'}
                ],
                rowNum: 1000,
                rowTotal: 50000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                ignoreCase: true,
                pgtext: null,
                pgbuttons: false,               
                pager:'#page_tabla_nomenclaturas',
                jsonReader: {
                    root: 'rows',
                    repeatitems: false,
                    id: '0'
                },              
                gridComplete: function() {    
                    var ids = jQuery("#tabla_nomenclaturas").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        var estado = jQuery("#tabla_nomenclaturas").getRowData(cl).reg_status;                      
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarNomenclatura('" + cl + "');\">";
                        if (estado==='A'){
                            $("#tabla_nomenclaturas").jqGrid('setRowData',ids[i],false, {weightfont:'bold',background:'#F6CECE'});          
                            ac = "<img src='/fintra/images/botones/iconos/check-blue.png' align='absbottom'  name='activar' id='activar' width='15' height='15' title ='Activar'  onclick=\"mensajeConfirmAction('Esta seguro de activar la nomenclatura seleccionada?','250','150',activarNomenclatura,'" + cl + "');\">";
                        }else{
                            ac = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"existeNomenclaturaRelCiudad('" + cl + "');\">";
                        }    

                        jQuery("#tabla_nomenclaturas").jqGrid('setRowData', ids[i], {actions: ed+'  '+ac});
                       
                    }
                },
                loadError: function(xhr, status, error) {
                    mensajesDelSistema(error, 250, 150);
                }
            }).navGrid("#page_tabla_nomenclaturas",{edit:false,add:false,del:false});;         
            jQuery("#tabla_nomenclaturas").jqGrid("navButtonAdd", "#page_tabla_nomenclaturas", {
                caption: "Nuevo",
                title: "Agregar nueva nomenclatura",
                onClickButton: function() {
                    crearNomenclatura();
                }
            });                
       
    }
}

function reloadGridNomenclaturas(){
    var url = './controller?estado=GestionSolicitud&accion=Aval&opcion=cargarNomenclaturas';
    jQuery("#tabla_nomenclaturas").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#tabla_nomenclaturas').trigger("reloadGrid");
}


function crearNomenclatura(){   
     $('#nombre').val('');   
     $('#descripcion').val('');   
     $('input[name=is_default]').val(['N']);
     $('#div_nomenclatura').fadeIn("slow");
     AbrirDivCrearNomenclatura();
}

function AbrirDivCrearNomenclatura(){
      $("#div_nomenclatura").dialog({
        width: 675,
        height: 250,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Adicionar": function() {
                guardarNomenclatura();
            },
            "Salir": function() {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarNomenclatura(){    
    var nombre = $('#nombre').val();
    var descripcion = $('#descripcion').val();
    var is_default = $("input[name=is_default]:checked").val(); 
    var url = './controller?estado=GestionSolicitud&accion=Aval';
    if(nombre!=='' && descripcion!==''){
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 'guardarNomenclatura',                    
                    nombre: nombre,
                    descripcion: descripcion,
                    is_default: is_default
                },
                success: function(json) {
                    
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }
                        
                        if (json.respuesta === "OK") {                            
                            mensajesDelSistema("Se cre� la nomenclatura", '250', '150', true);                            
                            $('#nombre').val(''); 
                            $('#descripcion').val(''); 
                            $('input[name=is_default]').val(['N']);
                            reloadGridNomenclaturas();                          
                        }                       
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear la nomenclatura", '250', '150');
                    }                         
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }      
            });
    }else{
         mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');      
    }
}

function editarNomenclatura(cl){
    
    $('#div_editar_nomenclatura').fadeIn("slow");
    var fila = jQuery("#tabla_nomenclaturas").getRowData(cl);  
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    var is_default = fila['is_default'];
    $('#idNomenclatura').val(cl);
    $('#nombreEdit').val(nombre);
    $('#descEdit').val(descripcion);
    $('input[name=is_defaultEdit]').val([is_default]);
    AbrirDivEditarNomenclatura();
}

function AbrirDivEditarNomenclatura(){
      $("#div_editar_nomenclatura").dialog({
        width: 675,
        height: 260,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'EDITAR NOMENCLATURA',
        closeOnEscape: false,
        buttons: {    
            "Guardar": function() {
                actualizarNomenclatura();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarNomenclatura(){   
    var nombre = $('#nombreEdit').val();
    var descripcion = $('#descEdit').val();
    var idNomenclatura = $('#idNomenclatura').val();
    var is_default = $("input[name=is_defaultEdit]:checked").val(); 
    var url = './controller?estado=GestionSolicitud&accion=Aval';
    if(nombre!=='' && descripcion!==''){
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 'guardarNomenclatura',
                nombre: nombre,
                descripcion: descripcion,
                is_default: is_default,
                idNomenclatura: idNomenclatura
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        reloadGridNomenclaturas(); 
                        mensajesDelSistema("Se actualiz� la nomenclatura", '250', '150', true);                      
                        
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar la nomenclatura!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }else{
         mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');      
    }
    
}

function anularNomenclatura(cl){   
    var url = './controller?estado=GestionSolicitud&accion=Aval';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 'cambiarEstadoNomenclatura',    
            estadoNom: 'A',
            idNomenclatura: cl
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    reloadGridNomenclaturas(); 
                    mensajesDelSistema("Se anul� la nomenclatura", '250', '150', true);                             
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular la nomenclatura!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function activarNomenclatura(cl){  
    var url = './controller?estado=GestionSolicitud&accion=Aval';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 'cambiarEstadoNomenclatura',    
            estadoNom: '',
            idNomenclatura: cl
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    reloadGridNomenclaturas(); 
                    mensajesDelSistema("Se activ� la nomenclatura", '250', '150', true);                             
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo activar la nomenclatura!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function existeNomenclaturaRelCiudad(id){
    $.ajax({
        type: "POST",
        url: './controller?estado=GestionSolicitud&accion=Aval',
        dataType: "json",     
        data:{
            opcion: 'existeRelNomenclaturaCiudad',          
            idNomenclatura:id
        },          
        success: function(json) {
            if (!isEmptyJSON(json)) {             
                if (json.respuesta === "SI") {               
                    mensajesDelSistema('No es posible anular la nomenclatura seleccionada. Tiene asociaciones realizadas','350','165', id);                    
                }else{
                    mensajeConfirmAction('Esta seguro de anular la nomenclatura seleccionada?','250','150',anularNomenclatura,id);                  
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo realizar el proceso!!", '250', '150');
            }                 
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }  
    });
}

function asignarConfiguracionDefault(cod_dpto){   
    loading("Espere un momento por favor...", "270", "140");
    var url = './controller?estado=GestionSolicitud&accion=Aval';
    setTimeout(function(){
        $.ajax({
            async: false,
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 'asignarNomenclaturaDefault', 
                dpto: cod_dpto
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {  
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Se estableci� configuracion por defecto al departamento seleccionado", '250', '180', true);                             
                    }

                } else {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Lo sentimos no se pudo asignar la configuraci�n por defecto!!", '250', '150');
                }                
            }, error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    },500);
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function loading(msj, width, height) {
    
    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajeConfirmAction(msj, width, height, okAction, id) { 
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsg").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {  
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsg").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}

