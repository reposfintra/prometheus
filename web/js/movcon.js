function LlenarPeriodo(CmbAnno, CmbMes){
	var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	var FechaAct = new Date();
	//CmbAnno.length = 0;
	//CmbMes.length  = 0;        
	if( CmbAnno!=null ){
		for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
			CmbAnno.value = FechaAct.getYear();
	}
	
	if( CmbMes!=null ){
		for (i=0;i<Meses.length;i++)
			if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
			else          addOption(CmbMes,(i+1),Meses[i]);                
		CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);	
	}
} 
function addOption(Comb,valor,texto){
	var Ele = document.createElement("OPTION");
	Ele.value=valor;
	Ele.text=texto;
	Comb.add(Ele);
}

function ChequearPeriodo(){
	var x = document.getElementById("inicializar");
	var y = document.getElementById("ano");
	var z = document.getElementById("mes");
	var a = document.getElementById("filtro1");
	var b = document.getElementById("filtro2");
	if (x.checked==true) {
		y.disabled = false;
		z.disabled = false;
		a.disabled = false;
		b.disabled = false;
		a.checked = true;
	}else if (x.checked==false) {
		y.disabled = true;
		z.disabled = true;
		a.disabled = true;
		b.disabled = true;
	}
}
function ValidarPrograma(){
	var y = document.getElementById("ano");
	var z = document.getElementById("mes");
	var a = document.getElementById("filtro1");
	var b = document.getElementById("filtro2");
	var mesescogido = z.value;
	var anoescogido = y.value;
	if ( mesescogido.substring(0,1) == 0 ){
		mesescogido = mesescogido.substring(1,2);
	}
	var f = new Date();
	var ano = f.getYear();
	var mes = f.getMonth()+1;
	if ((mesescogido > mes) && (anoescogido == ano)){
		alert('El periodo no puede ser mayor al periodo actual');
	}else if ((anoescogido > ano)){
		alert('El periodo no puede ser mayor al periodo actual');
	}else{
		if (a.checked==true){
			a.value = 'pto';
			
		}else if (b.checked==true){
			b.value = 'movcon';
		}
		forma.submit();
	}
	
}