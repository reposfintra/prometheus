/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 $(document).ready(function() {
    $('.solo-numero').keyup(function() {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $('.solo-numero').live('blur', function(event) {
        this.value = numberConComas(this.value);
    });   
    $("#fecha_pagar").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,      
        minDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    
    $("#fecha_pagar_dir").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,      
        minDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    var myDate = new Date();
    $("#fecha_pagar").datepicker("setDate", myDate);   
    $("#fecha_pagar_dir").datepicker("setDate", myDate);   
    $('#ui-datepicker-div').css('clip', 'auto');
    
    showhidefieldsbytype();
    $('#tipo_gestion').change(function() {  
        resetfieldsvalues();
        showhidefieldsbytype();       
    });
    
   $('#domicilio').change(function() {
        if($(this).is(":checked")) {
           cargarDepartamentos("CO", "departamento");
           $('#departamento').val($('#coddpto').val());
           cargarCiudad($('#coddpto').val(), "ciudad");
           $('#ciudad').val($('#codciu').val());
           $('#div_addCompromiso').fadeOut("fast");
           $('#div_addCompromisoDir').fadeIn();
           return;
        }else{           
           $('#div_addCompromisoDir').fadeOut("fast");
           $('#div_addCompromiso').fadeIn();
           return;
        }
        //'unchecked' event code
    });
   
    cargarDepartamentos("CO", "departamento");
    $('#departamento').val($('#coddpto').val());   
    
    $("#departamento").change(function() {
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciudad");
        $('#ciudad').val($('#codciu').val());
    });
    
    if ($('#div_compromisos').is(':visible')) {    
        $("#fecha_compromiso_ini").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
            defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
        });

        $("#fecha_compromiso_fin").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
            defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
        });
        
        $("#fecha_compromiso_ini").datepicker("setDate", myDate);
        $("#fecha_compromiso_fin").datepicker("setDate", myDate);
        $('#ui-datepicker-div').css('clip', 'auto');
        
        cargarAgentesCampo();

        $('#listarCompromisos').click(function() {
           buscarCompromisosPago();
        });
    }
 });
(function ($) {
    $.fn.styleTable = function (options) {
        var defaults = {
            css: 'styleTable'
        };
        options = $.extend(defaults, options);
        return this.each(function () {
            input = $(this);
            input.addClass(options.css);
            input.find("th").addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header");
            input.find("tr").each(function () {
                $(this).children("td:not(:first)").addClass("first");
                $(this).children("th:not(:first)").addClass("first");
            });
        });
    };
})(jQuery);

function verDetalles1(periodo, unid_negocio, negocio, cuota) {
    var grid_detalle_cartera = jQuery("#tabla_detalles_cartera");

    if ($("#gview_tabla_detalles_cartera").length) {
        reloadGridDetalleCartera(grid_detalle_cartera, periodo, unid_negocio, negocio, cuota);
    } else {

        grid_detalle_cartera.jqGrid({
            //caption: "Detalles",
            url: "./controller?estado=Seguimiento&accion=Cartera&evento=detallefac&periodo=" + periodo + "&unid_negocio=" + unid_negocio + "&negocio=" + negocio + "&cuota=" + cuota,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: 150,
            width: 920,
            colNames: ['Neg', 'Cedula','Factura', 'Cuota', 'Fecha Vencimiento', 'Saldo Foto', 'Saldo Actual','IxM','GAC'],
            colModel: [
            {name: 'negocio', index: 'negocio', width: 80, key: true},
            {name: 'cedula', index: 'cedula', sortable: true, width: 80, align: 'right'},
            {name: 'documento', index: 'documento', sortable: true, width: 90, align: 'right'},
            {name: 'cuota', index: 'cuota', sortable: false, width: 80,align: 'center'},
            {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 145, key: true, align: 'center'},
            {name: 'valor_asignado', index: 'valor_asignado', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
            {name: 'debido_cobrar', index: 'debido_cobrar', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
            {name: 'interes_mora', index: 'interes_mora', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
            {name: 'gasto_cobranza', index: 'gasto_cobranza', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 20,
            rowTotal: 1000,
            // pager: ('#page_detalles_cartera'),
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            onSelectRow: function(rowid, status, e) {
                //alert('Para el futuro');

            },
            loadComplete: function() {

               cacularTotalesDetalle(grid_detalle_cartera);
            }
        });
    }
}


function reloadGridDetalleCartera(grid_detalle_cartera,periodo,unid_negocio,negocio,cuota) {
    grid_detalle_cartera.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Seguimiento&accion=Cartera&evento=detallefac&periodo="+periodo+"&unid_negocio="+unid_negocio+"&negocio="+negocio+"&cuota="+cuota
    });
    grid_detalle_cartera.trigger("reloadGrid");
}

function cacularTotalesDetalle(grid_detalle_cartera) {

    var vlr_asignado = grid_detalle_cartera.jqGrid('getCol', 'valor_asignado', false, 'sum');
    var deb_cobrar = grid_detalle_cartera.jqGrid('getCol', 'debido_cobrar', false, 'sum');
    var vlr_ixM = grid_detalle_cartera.jqGrid('getCol', 'interes_mora', false, 'sum');
    var vlr_GAC=grid_detalle_cartera.jqGrid('getCol', 'gasto_cobranza', false, 'sum');
    grid_detalle_cartera.jqGrid('footerData', 'set', {
        fecha_vencimiento: 'Totales:',
        valor_asignado: vlr_asignado,
        debido_cobrar:deb_cobrar,
        interes_mora:vlr_ixM,
        gasto_cobranza:vlr_GAC
    });

}

function verDetallesPagosGrid(periodo,  negocio, num_ingreso) {
    var grid_detalle_pagos = jQuery("#tabla_detalles_pagos");

    if ($("#gview_tabla_detalles_pagos").length) {
       reloadGridDetallePagos(grid_detalle_pagos, periodo, negocio, num_ingreso);
    } else {

        grid_detalle_pagos.jqGrid({
            //caption: "Detalles",
            url: "./controller?estado=Seguimiento&accion=Cartera&evento=detallepago&periodo=" + periodo +"&negocio=" + negocio + "&num_ingreso=" + num_ingreso,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: 150,
            width: 730,
            colNames: ['Factura', 'Cuota','Fecha Vencimiento', 'Dias Vencidos', 'Valor Factura', 'Saldo Factura','Valor Ingreso'],
            colModel: [
            {name: 'documento', index: 'documento', sortable: true, width: 90, align: 'right'},    
            {name: 'cuota', index: 'cuota', sortable: false, width: 80,align: 'center'},
            {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 145, key: true, align: 'center'},
            {name: 'dias_vencidos', index: 'dias_vencidos', width: 80, key: true, align: 'center'},
            {name: 'valor_factura', index: 'valor_factura', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
            {name: 'valor_saldo', index: 'valor_saldo', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
           {name: 'valor_ingreso', index: 'valor_ingreso', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}

            ],
            rowNum: 20,
            rowTotal: 1000,
            // pager: ('#page_detalles_cartera'),
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            onSelectRow: function(rowid, status, e) {
                //alert('Para el futuro');

            },
            loadComplete: function() {

               cacularTotalesDetallePagos(grid_detalle_pagos);
            }
        });
    }
}

function verDetallesPagosGridTodo(periodo,  negocio, num_ingreso) {
    var grid_detalle_pagosTodos = jQuery("#tabla_detalles_pagos_pagos");

    if ($("#gview_tabla_detalles_pagos_pagos").length) {
       reloadGridDetallePagosTodos(grid_detalle_pagosTodos, periodo, negocio, num_ingreso);
    } else {

        grid_detalle_pagosTodos.jqGrid({
            //caption: "Detalles",
            url: "./controller?estado=Seguimiento&accion=Cartera&evento=detallepagotodo&periodo=" + periodo +"&negocio=" + negocio + "&num_ingreso=" + num_ingreso,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: 150,
            width: 820,
            colNames: ['Factura','Cuota','Cuenta','Fecha Vencimiento', 'Dias Vencidos', 'Valor Factura', 'Saldo Factura','Valor Ingreso'],
            colModel: [
            {name: 'documento', index: 'documento', sortable: true, width: 90, align: 'right'},    
            {name: 'cuota', index: 'cuota', sortable: false, width: 80,align: 'center'},
            {name: 'cuenta', index: 'cuenta', sortable: false, width: 80,align: 'center'},
            {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 145, key: true, align: 'center'},
            {name: 'dias_vencidos', index: 'dias_vencidos', width: 80, key: true, align: 'center'},
            {name: 'valor_factura', index: 'valor_factura', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
            {name: 'valor_saldo', index: 'valor_saldo', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
           {name: 'valor_ingreso', index: 'valor_ingreso', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}

            ],
            rowNum: 20,
            rowTotal: 1000,
            // pager: ('#page_detalles_cartera'),
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            onSelectRow: function(rowid, status, e) {
                //alert('Para el futuro');

            },
            loadComplete: function() {

               cacularTotalesDetallePagos(grid_detalle_pagosTodos);
            }
        });
    }
}
    
    
   function cacularTotalesDetallePagos(grid_detalle_pagos) {

    var vlr_factura = grid_detalle_pagos.jqGrid('getCol', 'valor_factura', false, 'sum');
    var vlr_saldo = grid_detalle_pagos.jqGrid('getCol', 'valor_saldo', false, 'sum');
    var vlr_ingreso = grid_detalle_pagos.jqGrid('getCol', 'valor_ingreso', false, 'sum');
   
    grid_detalle_pagos.jqGrid('footerData', 'set', {
        dias_vencidos: 'Totales:',
        valor_factura: vlr_factura,
        valor_saldo:vlr_saldo,
        valor_ingreso:vlr_ingreso
    });

}

function reloadGridDetallePagos(grid_detalle_pagos,periodo,negocio,num_ingreso) {
    grid_detalle_pagos.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Seguimiento&accion=Cartera&evento=detallepago&periodo=" + periodo +"&negocio=" + negocio + "&num_ingreso=" + num_ingreso
    });
    grid_detalle_pagos.trigger("reloadGrid");
}

function reloadGridDetallePagosTodos(grid_detalle_pagosTodos,periodo,negocio,num_ingreso) {
    grid_detalle_pagosTodos.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Seguimiento&accion=Cartera&evento=detallepagotodo&periodo=" + periodo +"&negocio=" + negocio + "&num_ingreso=" + num_ingreso
    });
    grid_detalle_pagosTodos.trigger("reloadGrid");
}

function exportarTodo() {
    var periodo = $('#periodo_foto').val();
    var undNeg = $('#unidad_negocio').val();
    var url = "./controller?estado=Seguimiento&accion=Cartera&evento=exportarTodo&periodo=" + periodo +"&undNeg=" + undNeg;
    if (periodo != "" && undNeg != "") {
        $("#divSalida").dialog("open");
        $("#divSalida").css('height', 'auto');
        $("#divSalida").dialog("option", "position", "center");
        $("#imgload").show();
        $("#resp").hide();
        $.ajax({
            dataType: 'html',
            url: url,
            method: 'post',
            success: function(resp) {
                var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalida" + "\")' /></div>"
                document.getElementById('resp').innerHTML = resp + "<br/><br/><br/><br/>" + boton
                $("#imgload").hide();
                $("#resp").show();
            },
            error: function(resp) {
                alert("Ocurrio un error enviando los datos al servidor");
            }

        });
    } else {
        alert("Debe seleccionar el periodo y la unidad de negocio");
    }
}

function cerrarDiv(div)
{
    $(div).dialog('close');
}

function exportarTabla() {
    var TipoStatus1 = "";
    var TipoStatus2 = "";
    var TipoStatus3 = "";
    var SuperHaving = "";
    var StatusVecctos = "";
    var DiaMes = "";

    if ($("#TipoStatus1").is(':checked')) {
        TipoStatus1 = $('#TipoStatus1').val();
    } else {
        TipoStatus1 = "";
    }

    if ($("#TipoStatus2").is(':checked')) {
        TipoStatus2 = $('#TipoStatus2').val();
    } else {
        TipoStatus2 = "";
    }

    if ($("#TipoStatus3").is(':checked')) {
        TipoStatus3 = $('#TipoStatus3').val();
    } else {
        TipoStatus3 = "";
    }



    if ($('#tramosg').val() == "" && $('#pagosabonos').val() == "Na" && $('#agentes').val() == "") {
        SuperHaving = "";

    } else if ($('#tramosg').val() != "" && $('#pagosabonos').val() == "Na" && $('#agentes').val() == "") {
        SuperHaving = "having vencimiento_mayor = '" + $('#tramosg').val() + "'";

    } else if ($('#tramosg').val() == "" && $('#pagosabonos').val() != "Na" && $('#agentes').val() == "") {
        SuperHaving = "having " + $('#pagosabonos').val();

    } else if ($('#tramosg').val() == "" && $('#pagosabonos').val() == "Na" && $('#agentes').val() != "") {
        SuperHaving = "having agente = '" + $('#agentes').val() + "'";

    } else if ($('#tramosg').val() != "" && $('#pagosabonos').val() != "Na" && $('#agentes').val() == "") {
        SuperHaving = "having vencimiento_mayor = '" + $('#tramosg').val() + "' and " + $('#pagosabonos').val();

    } else if ($('#tramosg').val() == "" && $('#pagosabonos').val() != "Na" && $('#agentes').val() != "") {
        SuperHaving = "having " + $('#pagosabonos').val() + " and agente = '" + $('#agentes').val() + "'";

    } else if ($('#tramosg').val() != "" && $('#pagosabonos').val() == "Na" && $('#agentes').val() != "") {
        SuperHaving = "having vencimiento_mayor = '" + $('#tramosg').val() + "' and agente = '" + $('#agentes').val() + "'";

    } else if ($('#tramosg').val() != "" && $('#pagosabonos').val() != "Na" && $('#agentes').val() != "") {
        SuperHaving = "having vencimiento_mayor = '" + $('#tramosg').val() + "' and " + $('#pagosabonos').val() + " and agente = '" + $('#agentes').val() + "'";
    }



    if ($('#status_vcto').val() != "") {
        StatusVecctos = $('#status_vcto').val();
    } else {
        StatusVecctos = "";
    }

    if ($('#DiasMes').val() != "") {
        DiaMes = "AND dia_pago = " + $('#DiasMes').val();
    } else {
        DiaMes = "";
    }
    $("#divSalidaTabla").dialog("open");
    $("#divSalidaTabla").css('height', 'auto');
    $("#divSalidaTabla").dialog("option", "position", "center");
    $("#imgload1").show();
    $("#resp1").hide();
    var boton = "";
    $.ajax({
        type: "POST",
        url: "./controller?estado=Seguimiento&accion=Cartera&evento=exportarTabla",
        async: true,
        dataType: "html",
        data: {
            unidad_negocio: $('#unidad_negocio').val(),
            periodo_foto: $('#periodo_foto').val(),
            aldia: TipoStatus1,
            avencer: TipoStatus2,
            vencido: TipoStatus3,
            SuperHaving: SuperHaving,
            StatusVcto: StatusVecctos,
            DayMonth: DiaMes,
            UserLogin: $('#UserLogin').val()
        },
        success: function (resp) {
            boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaTabla" + "\")' /></div>"
            document.getElementById('resp1').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
            $("#imgload1").hide();
            $("#resp1").show();
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function cargarSanciones() {
    var uneg = $('#unidad_negocio').val();
    var periodo = $('#periodo_foto').val();
    
    if(uneg[0] === '1'){
        generarSancionesMicro(uneg,periodo);
    }else{
        generarSancionesFenalco(uneg,periodo);
    }
}

function generarSancionesFenalco(uneg,periodo) {
    $('#tb_fenalco').fadeIn();
    $('#tb_micro').fadeOut();
    var url = './controller?estado=Seguimiento&accion=Cartera&evento=generarSancionesFenalco&periodo=' + periodo + '&uneg=' + uneg;
    if(uneg === '' || periodo === ''){
        alert("Todos los campos son requeridos");
    }else{
        if ($("#gview_ReporteSancionesFenalco").length) {
            refrescarGridReporteSancionesFenalco(periodo, uneg);
        } else {
            jQuery("#ReporteSancionesFenalco").jqGrid({
                caption: 'Reporte de Sanciones',
                url: url,
                datatype: 'json',
                height: 500,
                width: 2200,
                colNames: ['Unidad Negocio','Periodo','Negocio','Cedula','Nombre Cliente','Vencimiento Mayor','Fecha Pago Ingreso','Fecha Vencimiento Mayor','Diferencia Pago','Valor Saldo Foto',
                    'Interes Mora','Gasto Cobranza','Numero Ingreso','Valor Ingreso','Vr. CxC Ingreso','G16252145','G94350302','GI010010014205','I16252147','I94350301','II010010014170','Vr. Interes x Mora Ingreso','Vr. Gasto Cobranza Ingreso'
                ],
                colModel: [
                    {name: 'unidad_negocio', index: 'unidad_negocio', width: '600px', align: 'center'},
                    {name: 'periodo_foto', index: 'periodo_foto', width: '400px', align: 'center'},
                    {name: 'cod_negocio', index: 'cod_negocio', width: '400px', align: 'center'},
                    {name: 'cedula', index: 'cedula', width: '500px', align: 'center', key:true},
                    {name: 'nombre_cliente', index: 'nombre_cliente', width: '1100px', align: 'left'},
                    {name: 'vencimiento_mayor', index: 'vencimiento_mayor', width: '800px', align: 'center'},
                    {name: 'fecha_pago_ingreso', index: 'fecha_pago_ingreso', width: '400px', align: 'center'},
                    {name: 'fecha_vencimiento_mayor', index: 'fecha_vencimiento_mayor', width: '400px', align: 'center'},
                    {name: 'diferencia_pago', index: 'diferencia_pago', width: '500px', align: 'center'},
                    {name: 'valor_saldo_foto', index: 'valor_saldo_foto', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'interes_mora', index: 'interes_mora', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'gasto_cobranza', index: 'gasto_cobranza', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'num_ingreso', index: 'num_ingreso', width: '400px', align: 'center'},
                    {name: 'valor_ingreso', index: 'valor_ingreso', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'valor_cxc_ingreso', index: 'valor_cxc_ingreso', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'cuenta1', index: 'cuenta1', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'cuenta2', index: 'cuenta2', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'cuenta3', index: 'cuenta3', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'cuenta4', index: 'cuenta4', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'cuenta5', index: 'cuenta5', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'cuenta6', index: 'cuenta6', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'valor_ixm_ingreso', index: 'valor_ixm_ingreso', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'valor_gac_ingreso', index: 'valor_gac_ingreso', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
                ],
                rowNum: 10000,
                rowTotal: 500000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                pager: $('#pageFen'),
                viewrecords: true,
                hidegrid: false,
                jsonReader: {
                    root: 'rows',
                    repeatitems: false,
                    id: '0'
                },
                loadError: function(xhr, status, error) {
                    alert(error);
                }
            });
            jQuery("#ReporteSancionesFenalco").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
        }
}
}

function generarSancionesMicro(uneg,periodo) {
    $('#tb_micro').fadeIn();
    $('#tb_fenalco').fadeOut();
    var url = './controller?estado=Seguimiento&accion=Cartera&evento=generarSancionesMicro&periodo=' + periodo + '&uneg=' + uneg;
    if(uneg === '' || periodo === ''){
        alert("Todos los campos son requeridos");
    }else{
        if ($("#gview_ReporteSancionesMicro").length) {
            refrescarGridReporteSancionesMicro(periodo, uneg);
        } else {
            jQuery("#ReporteSancionesMicro").jqGrid({
                caption: 'Reporte de Sanciones',
                url: url,
                datatype: 'json',
                height: 500,
                width: 2200,
                colNames: ['Unidad Negocio','Periodo','Negocio','Cedula','Nombre Cliente','Vencimiento Mayor','Fecha Pago Ingreso','Fecha Vencimiento Mayor','Diferencia Pago','Valor Saldo Foto',
                    'Interes Mora','Gasto Cobranza','Numero Ingreso','Valor Ingreso','Vr. CxC Ingreso','GI010130014205','GI010010014205','I010130024170','I010010014170','Vr. Interes x Mora Ingreso','Vr. Gasto Cobranza Ingreso'
                ],
                colModel: [
                    {name: 'unidad_negocio', index: 'unidad_negocio', width: '600px', align: 'center'},
                    {name: 'periodo_foto', index: 'periodo_foto', width: '400px', align: 'center'},
                    {name: 'cod_negocio', index: 'cod_negocio', width: '400px', align: 'center'},
                    {name: 'cedula', index: 'cedula', width: '500px', align: 'center', key:true},
                    {name: 'nombre_cliente', index: 'nombre_cliente', width: '1100px', align: 'left'},
                    {name: 'vencimiento_mayor', index: 'vencimiento_mayor', width: '800px', align: 'center'},
                    {name: 'fecha_pago_ingreso', index: 'fecha_pago_ingreso', width: '400px', align: 'center'},
                    {name: 'fecha_vencimiento_mayor', index: 'fecha_vencimiento_mayor', width: '400px', align: 'center'},
                    {name: 'diferencia_pago', index: 'diferencia_pago', width: '500px', align: 'center'},
                    {name: 'valor_saldo_foto', index: 'valor_saldo_foto', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'interes_mora', index: 'interes_mora', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'gasto_cobranza', index: 'gasto_cobranza', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'num_ingreso', index: 'num_ingreso', width: '400px', align: 'center'},
                    {name: 'valor_ingreso', index: 'valor_ingreso', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'valor_cxc_ingreso', index: 'valor_cxc_ingreso', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'cuenta1', index: 'cuenta1', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'cuenta2', index: 'cuenta2', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'cuenta3', index: 'cuenta3', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'cuenta4', index: 'cuenta4', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'valor_ixm_ingreso', index: 'valor_ixm_ingreso', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'valor_gac_ingreso', index: 'valor_gac_ingreso', width: '500px', align: 'center', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
                ],
                rowNum: 10000,
                rowTotal: 500000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                pager: $('#pageMc'),
                viewrecords: true,
                hidegrid: false,
                jsonReader: {
                    root: 'rows',
                    repeatitems: false,
                    id: '0'
                },
                loadError: function(xhr, status, error) {
                    alert(error);
                }
            });
            jQuery("#ReporteSancionesMicro").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
        }
}
}

function exportarSanciones(){
    var uneg = $('#unidad_negocio').val();
    var periodo = $('#periodo_foto').val();
    $("#divSalida").dialog("open");
    $("#imgload1").show();
    $("#msj").show();
    $("#resp1").hide();
    $.ajax({
        type: "POST",
        url: "./controller?estado=Seguimiento&accion=Cartera&evento=exportarSanciones&periodo="+ periodo + "&uneg=" + uneg,
        dataType: "html",
        success: function (resp) {
            $("#imgload1").hide();
            $("#msj").hide();
            $("#resp1").show();
            var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalida" + "\")' /></div>"
            document.getElementById('resp1').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function refrescarGridReporteSancionesFenalco(periodo,uneg){
    var url = './controller?estado=Seguimiento&accion=Cartera&evento=generarSancionesFenalco&periodo=' + periodo + '&uneg=' + uneg;
    
    jQuery("#ReporteSancionesFenalco").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#ReporteSancionesFenalco").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    jQuery('#ReporteSancionesFenalco').trigger("reloadGrid");
}

function refrescarGridReporteSancionesMicro(periodo,uneg){
    var url = './controller?estado=Seguimiento&accion=Cartera&evento=generarSancionesMicro&periodo=' + periodo + '&uneg=' + uneg;
    
    jQuery("#ReporteSancionesMicro").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#ReporteSancionesMicro").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    jQuery('#ReporteSancionesMicro').trigger("reloadGrid");
}


function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function showhidefieldsbytype() {
    if ($('#tipo_gestion').val() == 19 && !$('#domicilio').is(":checked")) {      
        $('#div_addGestion').fadeOut("fast");
        $('#lbl_domicilio').fadeIn();
        $('#domicilio').fadeIn();
        $('#div_addCompromisoDir').fadeOut();
        $('#div_addCompromiso').fadeIn();
    }else if ($('#tipo_gestion').val() == 19 && $('#domicilio').is(":checked")) {      
        $('#div_addGestion').fadeOut("fast");
        $('#lbl_domicilio').fadeIn();
        $('#domicilio').fadeIn();
        $('#div_addCompromiso').fadeOut();
         $('#div_addCompromisoDir').fadeIn();
    } else {
        $('#lbl_domicilio').fadeOut("fast");
        $('#domicilio').fadeOut("fast");
        $('#div_addCompromiso').fadeOut("fast");
        $('#div_addCompromisoDir').fadeOut("fast");
        $('#div_addGestion').fadeIn();
    }
}

function resetfieldsvalues(){  
    $("#domicilio").attr("checked", false);
}

function cargarDepartamentos(codigo, combo) {
  if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 13,
                cod_pais: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error);
                        return;
                    }
                    try {
                        $('#'+combo).append("<option value=''>Seleccione</option>");                 

                        for (var key in json) {
                            $('#'+combo).append('<option value=' + key + '>' + json[key] + '</option>');                      
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key]);
                    }

                } else {

                    alert("Lo sentimos no se encontraron resultados!!");

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
  }
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error);
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key]);
                    }

                } else {

                    alert("Lo sentimos no se encontraron resultados!!");

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function cargarAgentesCampo() {

        $('#agente_campo').empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Seguimiento&accion=Cartera",
            dataType: 'json',
            data: {
                evento: "cargarAgentesReporte"
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error);
                        return;
                    }
                    try {
                        $('#agente_campo').append("<option value=''>Todos</option>");                 
                     
                        for (var key in json) {                           
                           $('#agente_campo').append('<option value=' + json[key].idCombostr + '>' + json[key].DescripcionCombo + '</option>');                      
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key]);
                    }

                } else {

                    alert("Lo sentimos no se encontraron resultados!!");

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
}


function buscarCompromisosPago() {      
    var grid_tbl_compromisos_pago = jQuery("#tabla_compromisos_pago");
     if ($("#gview_tabla_compromisos_pago").length) {
        refrescarGridCompromisosPago();
     }else {
        grid_tbl_compromisos_pago.jqGrid({
            caption: "COMPROMISOS DE PAGO",
            url: "./controller?estado=Seguimiento&accion=Cartera",           	 
            datatype: "json",  
            height: '290',
            width: '950',
            cellEdit: true,
            colNames: ['Id','Negocio','Cedula', 'Nombre Cliente', 'Dirección', 'Barrio', 'Ciudad', 'Fecha a pagar', 'Valor a pagar', 'Observación', 'Agente', 'Gestor','Rango Mora', 'Linea Negocio'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'negocio', index: 'negocio', width: 90, align: 'left'},
                {name: 'cedula', index: 'cedula', width: 110, align: 'center'},         
                {name: 'nombre', index: 'nombre', width: 180, align: 'center'},               
                {name: 'direccion', index: 'direccion', width: 120, align: 'center'}, 
                {name: 'barrio', index: 'barrio', width: 120, align: 'center'},  
                {name: 'ciudad', index: 'ciudad', width: 120, align: 'center'},  
                {name: 'fecha_pagar', index: 'fecha_pagar', width: 120, align: 'center'},  
                {name: 'valor_pagar', index: 'valor_pagar', sortable: true, editable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'observacion', index: 'observacion', width: 212, align: 'left'},
                {name: 'agente', index: 'agente', width: 120, align: 'center'}, 
                {name: 'gestor', index: 'gestor', width: 120, align: 'center'},  
                {name: 'rango_mora', index: 'rango_mora', width: 120, align: 'center'},
                {name: 'linea_negocio', index: 'linea_negocio', width: 120, align: 'center'}  
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_compromisos_pago'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       evento:'verCompromisos',
                       fecha_inicio:$('#fecha_compromiso_ini').val(),
                       fecha_fin:$('#fecha_compromiso_fin').val(),
                       agente:$('#agente_campo').val(),
                       domicilio: $('#cbo_domicilio').val()
                     }
            },   
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_compromisos_pago", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_compromisos_pago").jqGrid("navButtonAdd", "#page_tabla_compromisos_pago", {
            caption: "Exportar excel",
            onClickButton: function() {
                var info = jQuery('#tabla_compromisos_pago').getGridParam('records');
                if (info > 0) {
                    exportarExcel();
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }

            }

        });
    }  
       
}

function refrescarGridCompromisosPago(){   
    jQuery("#tabla_compromisos_pago").setGridParam({
        url: "./controller?estado=Seguimiento&accion=Cartera",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: {evento: 'verCompromisos',
                   fecha_inicio: $('#fecha_compromiso_ini').val(),
                   fecha_fin: $('#fecha_compromiso_fin').val(),
                   agente:$('#agente_campo').val(),
                   domicilio: $('#cbo_domicilio').val()
            }
        }       
    });
    
    jQuery('#tabla_compromisos_pago').trigger("reloadGrid");
}

function  exportarExcel() {
    var fullData = jQuery("#tabla_compromisos_pago").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: './controller?estado=Seguimiento&accion=Cartera',
        data: {
            listado: myJsonString,
            evento: "exportarCompromisos"
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

