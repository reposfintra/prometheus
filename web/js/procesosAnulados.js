/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    cargarMetasAnulados();   
});

function cargarMetasAnulados(){
    var url = './controller?estado=Procesos&accion=Meta&opcion=23';
    if ($("#gview_metaAnulados").length) {
         refrescarGridProcesosMeta();
     }else {
        jQuery("#metaAnulados").jqGrid({
            caption: 'Procesos Meta Inactivos',
            url: url,
            datatype: 'json',
            height: 400,
            width: 400,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key:true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '700px'},
                {name: 'descripcion', index: 'descripcion', hidden:true, sortable: true, align: 'center', width: '700px'},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '150px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
//          pager:'#page_tabla_metaAnulados',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            gridComplete: function() {
                    var ids = jQuery("#metaAnulados").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        act = "<img src='/fintra/images/botones/iconos/revert.png' align='absbottom'  name='activar' id='activar' width='15' height='15' title ='Activar meta proceso'  onclick=\"mensajeConfirmAction('Esta seguro de activar el meta proceso seleccionado?','250','150',activarMetaProceso,'" + cl + "');\">";
                        proc = "<img src='/fintra/images/botones/iconos/view-info.png' align='absbottom'  name='ver_proc' id='ver_proc' width='15' height='15' title ='Ver procesos internos'  onclick=\"abrirProcesoMeta('" + cl + "');\">";  
                        jQuery("#metaAnulados").jqGrid('setRowData', ids[i], {actions:act+'  '+proc});
                       
                    }
            },
            loadError: function(xhr, status, error) {
                alert(error);
            }
        });
     }
}

function refrescarGridProcesosMeta(){
    var url = './controller?estado=Procesos&accion=Meta&opcion=23';
    jQuery("#metaAnulados").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#metaAnulados').trigger("reloadGrid");
}

function abrirProcesoMeta(cl){
    
    $('#div_procesos_internos_asoc').fadeIn("slow");
    var fila = jQuery("#metaAnulados").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];    
    $('#nommetaproceso').val(nombre);
    $('#descmetaproceso').val(descripcion);
    AbrirDivProcesosInternos();
    cargarProcesosInternos(cl);
}

function AbrirDivProcesosInternos(){
      $("#div_procesos_internos_asoc").dialog({
        width: 700,
        height: 675,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'INFORMACION DEL META PROCESO',
        closeOnEscape: false,
        buttons: {    
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function cargarProcesosInternos(id){   
    var url = './controller?estado=Procesos&accion=Meta&opcion=25&procesoMeta='+id;
    if ($("#gview_procesos_internos_asoc").length) {
         refrescarGridProcesosInterno(id);
     }else {
        jQuery("#procesos_internos_asoc").jqGrid({
            caption: 'Procesos Internos',
            url: url,
            datatype: 'json',
            height: 330,
            width: 450,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Id Proceso Meta', 'Proceso Meta'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key:true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600px'},
                {name: 'descripcion', index: 'descripcion', hidden:true, sortable: true, align: 'center', width: '600px'},
                {name: 'id_tabla_rel', index: 'id_tabla_rel', hidden:true},
                {name: 'descripcionTablaRel', index: 'descripcionTablaRel', sortable: true, align: 'center', width: '600px'}/*,
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '200px'}*/
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager:'#page_tabla_procesos_internos_asoc',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },   
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
     }
}

function refrescarGridProcesosInterno(id){   
    var url = './controller?estado=Procesos&accion=Meta&opcion=25&procesoMeta='+id;
    jQuery("#procesos_internos_asoc").setGridParam({
        datatype: 'json',
        url: url
    });    
    jQuery('#procesos_internos_asoc').trigger("reloadGrid");
}

function activarMetaProceso(cl){
    var url = './controller?estado=Procesos&accion=Meta';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 24,            
            idProceso: cl
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    mensajesDelSistema("El meta proceso fue activado", '250', '150', true);
                    $('#div_procesosMeta').fadeOut();
                    refrescarGridProcesosMeta();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo activar el meta proceso!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensajeConfirmAction(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgAnulados');
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsgAnulados").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    mostrarContenido('dialogMsgAnulados');
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsgAnulados").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        zIndex: 1200,
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("close");             
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

