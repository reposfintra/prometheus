$(document).ready(function () {    
    
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $('.solo-numeric').live('keypress', function (event) {
        return numbersonly(this, event);
    });
 
    
    cargarAsesores();
    /*cargarEstadosRecibos('cargarEstadosRecibo');
    cargarTiposRecaudo();  */
    
    $("#fecha_entrega").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });


    var myDate = new Date();
    $("#fecha_entrega").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    
  /*  $('#consecutivo').blur(function(){
        cargarInfoRecibo(false);
    });*/  
    
    
    $("#guardar").click(function(){
       guardarRC();
    });
    

    
    $("#salir").click(function(){
       window.close();
    });
    
    if ($('#div_recibos_caja').is(':visible')) {    
        $("#fecha_ini").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
            defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
        });

        $("#fecha_fin").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
            defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
        });
        
        $("#fecha_ini").datepicker("setDate", myDate);
        $("#fecha_fin").datepicker("setDate", myDate);
        $('#ui-datepicker-div').css('clip', 'auto');
        
        cargarEstadosRecibos('estado_recibo','cargarEstadosReciboAll');
        cargarEstadosRecibos('estado_recibo_edit','cargarEstadosRecibo');
        
        $("#fecha_recibido").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
            defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
        });
        $("#fecha_recibido").datepicker("setDate", myDate);
        
        $('#listarRecibos').click(function() {         
           DeshabilitarFiltro(true);           
           buscarRecibosCaja();
           $("#divObservaciones").fadeIn();
        });
        
        $("#clearRecibos").click(function () {
            DeshabilitarFiltro(false);
            resetSearchValues();
            $("#divObservaciones").fadeOut();
            $("#tabla_recibos_caja").jqGrid('GridUnload');
        });

        $("#estado_recibo_edit").change(function () {          
            if ($('#estado_recibo_edit').val() === 'PEND_POR_APLICAR' && ($('#fecha_recibido').val() === '0099-01-01' || $('#fecha_recibido').val() === '')) {
                $('#fecha_recibido').datepicker("setDate", new Date());
            } else {
                resetInfoRecaudo();
            }  
            changeRequiredFields();
        });
        
        $('#id_cliente').change(function () {
            obtenerNombreCliente();
        });
    
        $('#valor_recaudo').live('blur', function (event) {
            this.value = numberConComas(this.value);
        }); 
    }else{
        clearInfoRegistroRC();
    }       
});

function cargarEstadosRecibos(idCmb,evt) {     
        $('#'+idCmb).empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Seguimiento&accion=Cartera",
            dataType: 'json',
            data: {
                evento: evt
            },
            success: function(json) {
              
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error);
                        return;
                    }
                    try {
                        if(evt === 'cargarEstadosReciboAll')  $('#'+idCmb).append("<option value=''>Todos</option>");   
                        for (var key in json) {                              
                           $('#'+idCmb).append('<option value=' + json[key].idCombostr + '>' + json[key].DescripcionCombo + '</option>');                      
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key]);
                    }

                } else {

                    alert("Lo sentimos no se encontraron resultados!!");

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
}

function cargarAsesores() {

        $('#asesor').empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Seguimiento&accion=Cartera",
            dataType: 'json',
            data: {
                evento: "cargarAsesores"
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error);
                        return;
                    }
                    try {
                        ($('#div_recibos_caja').is(':visible')) ? $('#asesor').append("<option value=''>Todos</option>") : $('#asesor').append("<option value=''>Seleccione</option>");                 
                     
                        for (var key in json) {                           
                           $('#asesor').append('<option value=' + json[key].idCombostr + '>' + json[key].DescripcionCombo + '</option>');                      
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key]);
                    }

                } else {

                    alert("Lo sentimos no se encontraron resultados!!");

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
}


function cargarTiposRecaudo() {

        $('#tipo_recaudo').empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Seguimiento&accion=Cartera",
            dataType: 'json',
            data: {
                evento: "cargarTiposRecaudo"
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error);
                        return;
                    }
                    try {
                        $('#tipo_recaudo').append("<option value='0'>Seleccione</option>");                 
                     
                        for (var key in json) {                           
                           $('#tipo_recaudo').append('<option value=' + json[key].idCombostr + '>' + json[key].DescripcionCombo + '</option>');                      
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key]);
                    }

                } else {

                    alert("Lo sentimos no se encontraron resultados!!");

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
}

function getNextConsecutivoRC() { 
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Seguimiento&accion=Cartera",
            dataType: 'json',
            data: {
                evento: "getNextRCNumber"
            },
            success: function(json) {
              
                if (!isEmptyJSON(json)) {
                               
                    if (json.error) {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }
                    
                    if (json.respuesta){   
                        $('#consec_ini').val(json.respuesta);  
                        $('#consec_fin').val(json.respuesta);  
                    }
        
                }else{
                    alert("Lo sentimos no se encontraron resultados!!");
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
}

function clearInfoRegistroRC(){  
    getNextConsecutivoRC();
    cargarAsesores();   
    $('#fecha_entrega').datepicker("setDate", new Date()); 
    $('#asesor').val(''); 
    $('#area').val('');   
  
}

function guardarRC(){
    var jsonParams = {};   
    if (validaCamposFormIngresoRC()){        
            jsonParams = {
                evento: "guardarRC",
                action: "insertar",
                consecutivo_inicial: $('#consec_ini').val(),
                consecutivo_final: $('#consec_fin').val(),
                fecha_entrega: $('#fecha_entrega').val(),
                asesor: $('#asesor').val(),
                area: $('#area').val(),
                estado_recibo: 'PEND_POR_ENTREGAR'
            };
            guardarReciboCaja(jsonParams,"insertar");
        }
       
 }

function validaCamposFormIngresoRC(){
    var sw = false;
    var consecutivo_ini = $('#consec_ini').val();
    var consecutivo_fin = $('#consec_fin').val();
    var fecha_entrega = $('#fecha_entrega').val();
    var asesor = $('#asesor').val();
    var area = $('#area').val();
    if (consecutivo_ini === '' || consecutivo_fin === '' || fecha_entrega === '' || asesor === '' || area === '') {
        mensajesDelSistema("Debe diligenciar todos los campos", '250', '150');
        return;
    }else if(parseInt(consecutivo_fin) < parseInt(consecutivo_ini)){
        mensajesDelSistema('El consecutivo final no puede ser inferior al inicial', '250', '150');
        return;
    }else{
         sw = true;
    }
    return sw;
}

function actualizarRC(){
    var rowid = $('#consecutivo').val();
    var fila = jQuery("#tabla_recibos_caja").getRowData(rowid);
    var fecha_entrega = fila['fecha_entrega'];
    var asesor = fila['asesor'];
    var area = fila['area'];
    var jsonParams = {};   
    if (validaCamposFormRecibo()){        
            jsonParams = {
                evento: "guardarRC",
                action: "actualizar",
                consecutivo: $('#consecutivo').val(),
                fecha_entrega: fecha_entrega,
                asesor: asesor,
                area: area,
                estado_recibo: $('#estado_recibo_edit').val(),
                id_cliente: $('#id_cliente').val(),
                fecha_recibido: $('#fecha_recibido').val(),
                nombre: $('#nombre').val(),
                tipo_recaudo: $('#tipo_recaudo').val(),
                valor_recaudo: numberSinComas($('#valor_recaudo').val()),
                cod_neg: document.getElementById("listaNegocios").value
            };
            guardarReciboCaja(jsonParams,"actualizar");
        }       
 }

function validaCamposFormRecibo (){
    var sw = false;
    var consecutivo = $('#consecutivo').val();
    var fila = jQuery("#tabla_recibos_caja").getRowData(consecutivo);
    var fecha_entrega = fila['fecha_entrega'];
    var estado_recibo = $('#estado_recibo_edit').val();
    var id_cliente = $('#id_cliente').val();
    var fecha_recibido = $('#fecha_recibido').val();
    var nombre = $('#nombre').val();
    var tipo_recaudo = $('#tipo_recaudo').val();
    var valor_recaudo = numberSinComas($('#valor_recaudo').val());
    if (estado_recibo === 'PEND_POR_APLICAR'){
        if (consecutivo==='' || id_cliente ==='' || fecha_recibido==='' || nombre ==='' || tipo_recaudo==='0' || valor_recaudo ===''){
            mensajesDelSistema("Debe diligenciar todos los campos", '250', '150');   
            return;
        }else if (fecha_recibido < fecha_entrega){
            mensajesDelSistema('La fecha de recibido no puede ser inferior a la de entrega', '250', '150');
            return;
        }else if(parseFloat(valor_recaudo)<=0) {
            mensajesDelSistema("El valor del recaudo debe ser mayor a 0", '250', '150');  
            return;
        }else{
             sw = true;
        }
    }else{
        if ((fecha_recibido !== '' && fecha_recibido !== '0099-01-01') && fecha_recibido < fecha_entrega){
            mensajesDelSistema('La fecha de recibido no puede ser inferior a la de entrega', '250', '150');
            return;
        }else{
            sw = true;
        }
    }
    return sw;
}

function guardarReciboCaja(jsonParams, action){  
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function(){
            var url = './controller?estado=Seguimiento&accion=Cartera';
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: jsonParams,
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {    
                            $("#dialogLoading").dialog('close');
                            if (action==='actualizar'){
                                $("#div_info_recaudo").dialog('close');
                                refrescarGridRecibosCaja();
                            }else{
                               clearInfoRegistroRC();
                            }
                            (action==='insertar') ? mensajesDelSistema("Se guardaron los recibos satisfactoriamente", '250', '150', true):mensajesDelSistema("Se guardo el recibo satisfactoriamente", '250', '150', true); 
                        }
                        
                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se pudo guardar el recibo!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            }); 
       },500);
}

function anularReciboCaja(num_recibo){
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function(){
            var url = './controller?estado=Seguimiento&accion=Cartera';
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    evento: "anularRC",
                    consecutivo: num_recibo
                },
                success: function (json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }

                        if (json.respuesta === "OK") {                           
                            $("#dialogLoading").dialog('close'); 
                            refrescarGridRecibosCaja();                   
                            mensajesDelSistema("Se anul&oacute; el recibo de caja", '250', '150', true);
                        }else{
                            $("#dialogLoading").dialog('close');                           
                            mensajesDelSistema(json.respuesta, '280', '160');                           
                        }

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se pudo anular el recibo de caja!!", '250', '150');
                    }
                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        },500);       
}

function buscarRecibosCaja() {      
    var grid_tbl_recibos_caja = jQuery("#tabla_recibos_caja");
    var isAuthorized = validaUserApplyRC();
     if ($("#gview_tabla_recibos_caja").length) {
        refrescarGridRecibosCaja();
     }else {
        grid_tbl_recibos_caja.jqGrid({
            caption: "RECIBOS DE CAJA",
            url: "./controller?estado=Seguimiento&accion=Cartera",
            datatype: "json",
            height: '350',
            width: 'auto',
            colNames: ['No Recibo','Fecha Entrega', 'Asesor', 'Area', 'Id Estado', 'Observacion', 'Fecha Recibido', 'Id Cliente', 'Nombre Cliente', 'Negocio', 'IdTipoRecaudo', 'Tipo Recaudo', 'Valor Recaudo','Comentario','Acciones'],
            colModel: [            
                {name: 'num_recibo', index: 'num_recibo', width: 90, align: 'center', key: true},
                {name: 'fecha_entrega', index: 'fecha_entrega', width: 90, align: 'center'},         
                {name: 'asesor', index: 'asesor', width: 130, align: 'left'},               
                {name: 'area', index: 'area', width: 120, align: 'center'}, 
                {name: 'id_estado_recibo', index: 'id_estado_recibo', width: 180, align: 'left',hidden:true}, 
                {name: 'estado_recibo', index: 'estado_recibo', width: 180, align: 'left'}, 
                {name: 'fecha_recibido', index: 'fecha_recibido', width: 90, align: 'center'},
                {name: 'id_cliente', index: 'id_cliente', width: 90, align: 'center'},
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 180, align: 'left'},
                {name: 'cod_neg', index: 'cod_neg', width: 90, align: 'center'},
                {name: 'id_tipo_recaudo', index: 'id_tipo_recaudo', width: 180, align: 'left',hidden:true}, 
                {name: 'tipo_recaudo', index: 'tipo_recaudo', width: 110, align: 'center'},   
                {name: 'valor_recaudo', index: 'valor_recaudo', sortable: true, editable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'comentario', index: 'comentario', width: 210, align: 'left',hidden:false}, 
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '80px'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_recibos_caja'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            /*pgtext:null,
            pgbuttons:false,*/
            multiselect: ($('#estado_recibo').val()==='PEND_POR_ENTREGAR' || (($('#estado_recibo').val()==='PEND_POR_APLICAR'||$('#estado_recibo').val()==='HURTADO') && isAuthorized)) ? true: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       evento:'cargarRecibosCaja',
                       asesor: $('#asesor').val(),
                       fecha_inicio:$('#fecha_ini').val(),
                       fecha_fin:$('#fecha_fin').val(),
                       estado_recibo:$('#estado_recibo').val(),
                       num_recibo:$('#num_recibo').val()
                    }
            },   
            gridComplete: function () {
                var ids = jQuery("#tabla_recibos_caja").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var estado_recibo = jQuery("#tabla_recibos_caja").getRowData(cl).id_estado_recibo;
                    var bgcolor = '#FFFFFF none repeat scroll 0 0;';
                    if(estado_recibo==='PEND_POR_ENTREGAR' || estado_recibo==='PEND_POR_APLICAR' || estado_recibo==='HURTADO'){
                       
                        if (estado_recibo === 'HURTADO' && !validaExistaInfoRecaudo(cl)) {                         
                            bgcolor = 'rgba(245, 140, 112, 0.53)';                          
                        }
                  
                        changeBackgroundRow('tabla_recibos_caja', cl, bgcolor);
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarInfoRecaudo('" + cl + "');\">";
                        an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el recibo seleccionado?','250','150',anularReciboCaja,'" + cl + "');\">";
                        obs = "<img src='/fintra/images/botones/iconos/notas.gif' align='absbottom'  name='comment' id='comment' width='15' height='15' title ='Agregar comentario'  onclick=\"ventanaAgregarComentario('" + cl + "');\">";
                        jQuery("#tabla_recibos_caja").jqGrid('setRowData', ids[i], {actions: ed + '  ' + obs + '  ' + an});
                    }else{
                        obs = "<img src='/fintra/images/botones/iconos/notas.gif' align='absbottom'  name='comment' id='comment' width='15' height='15' title ='Agregar comentario'  onclick=\"ventanaAgregarComentario('" + cl + "');\">";
                        jQuery("#tabla_recibos_caja").jqGrid('setRowData', ids[i], {actions: obs});
                    }
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_recibos_caja", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        if($('#estado_recibo').val()==='PEND_POR_ENTREGAR'){
            jQuery("#tabla_recibos_caja").jqGrid("navButtonAdd", "#page_tabla_recibos_caja", {
                caption: "Reasignar Asesor",
                onClickButton: function () {
                    var filasId = jQuery('#tabla_recibos_caja').jqGrid('getGridParam', 'selarrrow');
                    if (filasId != '') {
                        AbrirDivReasignarAsesor();
                    } else {
                        if (jQuery('#tabla_recibos_caja').jqGrid('getGridParam', 'records') > 0) {
                            mensajesDelSistema("Debe seleccionar los recibos que desea reasignar!!", '250', '150');
                        } else {
                            mensajesDelSistema("No hay recibos para reasignar asesor", '250', '150');
                        }
                    }    
                   
                }
            });
        }
        if(($('#estado_recibo').val()==='PEND_POR_APLICAR' || $('#estado_recibo').val()==='HURTADO') && isAuthorized){
            jQuery("#tabla_recibos_caja").jqGrid("navButtonAdd", "#page_tabla_recibos_caja", {
                caption: "Aplicar",
                onClickButton: function () {
                    var filasId = jQuery('#tabla_recibos_caja').jqGrid('getGridParam', 'selarrrow');
                    if (filasId != '') {
                        cambiarEstadoReciboCaja();
                    } else {
                        if (jQuery('#tabla_recibos_caja').jqGrid('getGridParam', 'records') > 0) {
                            mensajesDelSistema("Debe seleccionar los recibos que ha aplicado!!", '250', '150');
                        } else {
                            mensajesDelSistema("No hay recibos para aplicar", '250', '150');
                        }
                    }    
                   
                }
            });
        }
        jQuery("#tabla_recibos_caja").jqGrid("navButtonAdd", "#page_tabla_recibos_caja", {
            caption: "Exportar excel",
            onClickButton: function() {
                var info = jQuery('#tabla_recibos_caja').getGridParam('records');
                if (info > 0) {
                    exportarExcel();
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }

            }
        });
        jQuery("#tabla_recibos_caja").jqGrid('filterToolbar',
        {
            autosearch: true,
            searchOnEnter: true,
            defaultSearch: "cn",
            stringResult: true,
            ignoreCase: true,
            multipleSearch: true
        }); 
    }  
       
}

function refrescarGridRecibosCaja(){   
    jQuery("#tabla_recibos_caja").setGridParam({
        url: "./controller?estado=Seguimiento&accion=Cartera",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: {evento: 'cargarRecibosCaja',
                   asesor: $('#asesor').val(),
                   fecha_inicio: $('#fecha_ini').val(),
                   fecha_fin: $('#fecha_fin').val(),
                   estado_recibo:$('#estado_recibo').val(),
                   num_recibo:$('#num_recibo').val()
            }
        }       
    });
    
    jQuery('#tabla_recibos_caja').trigger("reloadGrid");
}

function editarInfoRecaudo(cl){
    $('#div_info_recaudo').fadeIn("slow");
    cargarTiposRecaudo();
    var fila = jQuery("#tabla_recibos_caja").getRowData(cl);
    var num_recibo = cl;
    var estado_recibo = fila['id_estado_recibo'];
    var fecha_recibido = fila['fecha_recibido'];
    var id_cliente = fila['id_cliente'];
    var nombre_cliente = fila['nombre_cliente'];
    var tipo_recaudo = fila['id_tipo_recaudo'];
    var valor_recaudo = numberConComas(fila['valor_recaudo']);
    let negocio = fila['cod_neg'];
    $('#consecutivo').val(num_recibo);
    (estado_recibo === 'PEND_POR_ENTREGAR') ? $('#estado_recibo_edit').val('PEND_POR_APLICAR') : $('#estado_recibo_edit').val(estado_recibo);
    (estado_recibo === 'PEND_POR_ENTREGAR') ? $('#estado_recibo_edit').attr({disabled: false}) : $('#estado_recibo_edit').attr({disabled: true});
    if ($('#estado_recibo_edit').val() === 'PEND_POR_APLICAR' && fecha_recibido === '0099-01-01') { 
        $('#fecha_recibido').datepicker("setDate", new Date());      
    }else if ($('#estado_recibo_edit').val() === 'HURTADO' && fecha_recibido === '0099-01-01') {
        $('#fecha_recibido').val('');
    }else{
        $('#fecha_recibido').val(fecha_recibido);
    }      
    $('#id_cliente').val(id_cliente);
    $('#nombre').val(nombre_cliente);
    $('#tipo_recaudo').val(tipo_recaudo);
    $('#valor_recaudo').val(valor_recaudo);   
    
    cargarNegociosCliente(id_cliente, negocio);
    ventanaInfoRecaudo();
}

function ventanaInfoRecaudo(){    
      $("#div_info_recaudo").dialog({
        width: 750,
        height: 325,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'REGISTRO INFORMACION DE RECAUDO',
        closeOnEscape: false,
        buttons: {  
            "Guardar": function () {
               actualizarRC();
               limpiarSelect("listaNegocios");
            },
            "Salir": function () {
                limpiarSelect("listaNegocios");
                $(this).dialog("destroy");
            }
        }
    });
}


function cambiarEstadoReciboCaja(){
    
    var listado = "";
    var filasId =jQuery('#tabla_recibos_caja').jqGrid('getGridParam', 'selarrrow');
   
        for (var i = 0; i < filasId.length; i++) {  
                var estado_recibo = $("#tabla_recibos_caja").getRowData(filasId[i]).id_estado_recibo;
                if(estado_recibo === 'PEND_POR_APLICAR' || (estado_recibo === 'HURTADO' && validaExistaInfoRecaudo(filasId[i])))    listado += filasId[i] + ","; 
        }    
        if (listado !=''){
            var url = './controller?estado=Seguimiento&accion=Cartera';
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    evento: 'cambiarEstadoRC',
                    estado_recibo: 'APLICADO',
                    listado: listado
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }

                        if (json.respuesta === "OK") {                           
                            mensajesDelSistema("operación exitosa", '250', '150', true);  
                            refrescarGridRecibosCaja();
                        }

                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo realizar la operación!!", '250', '150');
                    }

                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }
}

function validaExistaInfoRecaudo(rowid){     
        var estado = false;
        var fila = jQuery("#tabla_recibos_caja").getRowData(rowid);
        var id_cliente = fila['id_cliente'];
        var fecha_recibido = fila['fecha_recibido'];
        var nombre = fila['nombre'];
        var tipo_recaudo = fila['id_tipo_recaudo'];
        var valor_recaudo = fila['valor_recaudo'];       
        if (id_cliente ==='' || fecha_recibido==='' || nombre ==='' || tipo_recaudo==='0' || valor_recaudo ===''){
           estado = false;
        }else if(parseFloat(valor_recaudo)<=0){
           estado = false;
        }else{
           estado = true;
        }
        
        return estado;
}

function validaUserApplyRC(){
    var estado = false;
    $.ajax({
        url: './controller?estado=Seguimiento&accion=Cartera',
        datatype:'json',
        type:'post',
        async:false,
        data:{
            evento: 'autorizadoAplicarRC'
        },          
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.respuesta === "SI") {               
                   estado = true;                  
                }
                
            }              
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }  
    });
    return estado;
}

function obtenerNombreCliente() {
    let id = $('#id_cliente').val();
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Seguimiento&accion=Cartera",
            dataType: 'json',
            data: {
                evento: "obtenerNombreCliente",
                id_cliente: $('#id_cliente').val()
            },
            success: function(json) {
                cargarNegociosCliente(id);
                
                if (!isEmptyJSON(json)) {
                    if (json.respuesta){   
                        $('#nombre').val(json.respuesta);
                    }        
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
}

function  exportarExcel() {
    var fullData = jQuery("#tabla_recibos_caja").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: './controller?estado=Seguimiento&accion=Cartera',
        data: {
            listado: myJsonString,
            evento: "exportarRecibosCaja"
        },
        success: function (resp) {         
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function resetSearchValues(){  
    cargarAsesores();
    $('#fecha_ini').datepicker("setDate", new Date()); 
    $('#fecha_fin').datepicker("setDate", new Date()); 
    cargarEstadosRecibos('estado_recibo','cargarEstadosReciboAll');  
    $('#num_recibo').val('');
}


function DeshabilitarFiltro(estado){   
        $('#asesor').attr({disabled: estado});
        (estado) ? $('#fecha_ini').datepicker("destroy") : $('#fecha_ini').datepicker({dateFormat: 'yy-mm-dd'});
        (estado) ? $('#fecha_fin').datepicker("destroy") : $('#fecha_fin').datepicker({dateFormat: 'yy-mm-dd'});
        $('#estado_recibo').attr({disabled: estado}); 
         $('#num_recibo').attr({readonly: estado});
} 

function resetInfoRecaudo(){
    $('#fecha_recibido').val(''); 
    $('#id_cliente').val('');
    $('#nombre').val('');    
    $('#tipo_recaudo').val('0');
    $('#valor_recaudo').val(0);
}

function  changeRequiredFields(){  
    if ($('#estado_recibo_edit').val()==='PEND_POR_APLICAR'){
        $('#req_ident').fadeIn();
        $('#req_fec_recib').fadeIn();
        $('#req_nombre').fadeIn();
        $('#req_tipo_recaudo').fadeIn();
        $('#req_vlr_recaudo').fadeIn();
    }else{
        $('#req_ident').fadeOut();
        $('#req_fec_recib').fadeOut();
        $('#req_nombre').fadeOut();
        $('#req_tipo_recaudo').fadeOut();
        $('#req_vlr_recaudo').fadeOut();
    }
}

function changeBackgroundRow(tableid, cl, bgcolor){
    
    jQuery("#" + tableid).jqGrid('setCell', cl, "num_recibo", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "fecha_entrega", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "asesor", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "area", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "estado_recibo", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "fecha_recibido", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "id_cliente", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "nombre_cliente", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "tipo_recaudo", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "valor_recaudo", "", {'background-color': bgcolor, 'background-image': 'none'});  
    
}

function cargarAsesoresReasignacion() {

        $('#asesorEdit').empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Seguimiento&accion=Cartera",
            dataType: 'json',
            data: {
                evento: "cargarAsesores"
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error);
                        return;
                    }
                    try {
                        $('#asesorEdit').append("<option value=''>Seleccione</option>");                 
                     
                        for (var key in json) {                           
                           $('#asesorEdit').append('<option value=' + json[key].idCombostr + '>' + json[key].DescripcionCombo + '</option>');                      
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key]);
                    }

                } else {

                    alert("Lo sentimos no se encontraron resultados!!");

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
}


function AbrirDivReasignarAsesor(){ 
        cargarAsesoresReasignacion();       
        ventanaReasignarAsesor();    
}

function ventanaReasignarAsesor(){     
      $("#dialogReasignarAsesor").dialog({
        width: 450,
        height: 180,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'REASIGNAR ASESOR',
        closeOnEscape: false,
        buttons: {  
            "Reasignar": function () {
                if ($('#asesorEdit').val() === '') {
                    mensajesDelSistema('Seleccione el asesor a reasignar', '250', '150');
                } else {
                    reasignarAsesor();
                }              
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function reasignarAsesor(){
    
    var listado = "";
    var filasId =jQuery('#tabla_recibos_caja').jqGrid('getGridParam', 'selarrrow');
   
        for (var i = 0; i < filasId.length; i++) {  
                var estado_recibo = $("#tabla_recibos_caja").getRowData(filasId[i]).id_estado_recibo;
                if(estado_recibo === 'PEND_POR_ENTREGAR')    listado += filasId[i] + ","; 
        }    
        if (listado !=''){
            var url = './controller?estado=Seguimiento&accion=Cartera';
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    evento: 'reasignarAsesor',
                    asesor: $('#asesorEdit').val(),
                    listado: listado
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }

                        if (json.respuesta === "OK") {                           
                            mensajesDelSistema("operación exitosa", '250', '150', true);  
                            $("#dialogReasignarAsesor").dialog('close');                  
                            refrescarGridRecibosCaja();
                        }

                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo realizar la operación!!", '250', '150');
                    }

                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }
}

function ventanaAgregarComentario(num_recibo){
      var fila = jQuery("#tabla_recibos_caja").getRowData(num_recibo);
      var comment = fila['comentario'];
      $("#observ").val(comment);
      $("#dialogAddComent").dialog({
        width: 580,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR COMENTARIO',
        closeOnEscape: false,
        buttons: {  
            "Actualizar": function () {               
                   updateComentRC(num_recibo);                         
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function updateComentRC(num_recibo){
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function(){
            var url = './controller?estado=Seguimiento&accion=Cartera';
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    evento: "updateCommentRC",
                    consecutivo: num_recibo,
                    comment: $('#observ').val().trim().toUpperCase() 
                },
                success: function (json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }

                        if (json.respuesta === "OK") {                           
                            $("#dialogLoading").dialog('close'); 
                            refrescarGridRecibosCaja();                   
                            $("#dialogAddComent").dialog('close');
                        }else{
                            $("#dialogLoading").dialog('close');                           
                            mensajesDelSistema(json.respuesta, '280', '160');                           
                        }

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se pudo actualizar comentario del recibo de caja!!", '250', '150');
                    }
                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        },500);       
}


function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajeConfirmAction(msj, width, height, okAction, id) {  
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear botón de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargarNegociosCliente(id, negocio) {
    if (id !== "" && id !== undefined) {
        $.ajax({
            type: "POST",
            url: "./controller?estado=Seguimiento&accion=Cartera",
            dataType: "json",
            data: {
                evento: "cargarNegociosCliente",
                idCliente: id
            },
            success: function (negocios) {
                if (!isEmptyJSON(negocios)) {
                    if (negocios.error) {
                        mensajesDelSistema("No se pudieron cargar los negocios", '250', '150');
                        return;
                    }
                    console.log(negocios);
                    
                    let select = document.getElementById("listaNegocios");

                    for (let e of negocios) {
                        opt = document.createElement('option');
                        opt.value = e.codigo;
                        opt.innerHTML = e.codigo;

                        select.add(opt);
                    }
                    if (negocio !== "" && negocio !== undefined) {
                        select.value = negocio;
                    }

                } else {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("No tiene negocios", '250', '150');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                mensajesDelSistema("No se pudieron cargar los negocios", '250', '150');
            }
        });
    }
}

function limpiarSelect(id) {
    document.getElementById(id).innerHTML = "<option>...</option>";
}