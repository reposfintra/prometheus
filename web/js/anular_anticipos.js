/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
   
    $("#container").css({'width': '1600px'});
    verAnticipos($("#transportadora2").val(), $("#banco_tercero").val(),1);
    $("#desaprobar").click(function () {
        anularAnticipos();
    });
    
    $("#transferir").click(function () {      
       transferirAnticipos();
    });


});


function transferirAnticipos() {
    var rowId = $("#tbl_anular_anticipos").jqGrid('getGridParam', 'selarrrow');
    if (rowId.length > 0) {
        loading("Espere un momento se esta generando el archivo transferencia.", "320", "140");
        var arrayJson = [];
        for (var i = 0; i < rowId.length; i++) {
            var rowData = jQuery("#tbl_anular_anticipos").getRowData(rowId[i]);
            arrayJson.push(rowData);
        }

        var myJsonString = JSON.stringify(arrayJson);
        console.log(myJsonString);

        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Logistica",
            dataType: 'json',
            data: {
                opcion: 17,
                datajson: myJsonString,
                banco: $("#banco_tercero").val()
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    $("#dialogo2").dialog('close');
                  
                    if (json.respuesta === 'OK') {
                        //esto debe ir depues de descargar el archivo.
                        reloadGridAnticiposTrans();                       
                    }else{
                        mensajesDelSistema(json.respuesta , '320', '150'); 
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema('Debe seleccionar al menos un anticipo.', '300', '150');
    }
}


function anularAnticipos() {
    var rowId = $("#tbl_anular_anticipos").jqGrid('getGridParam', 'selarrrow');
    if (rowId.length > 0) {
        var arrayJson = [];
        for (var i = 0; i < rowId.length; i++) {
            var rowData = jQuery("#tbl_anular_anticipos").getRowData(rowId[i]);
            arrayJson.push(rowData);
        }

        var myJsonString = JSON.stringify(arrayJson);
        console.log(myJsonString);

        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Logistica",
            dataType: 'json',
            data: {
                opcion: 14,
                datajson: myJsonString
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error === 'error') {
                        mensajesDelSistema('Lo sentimos no se pudieron desaprobar los anticipos', '300', '180');
                    }

                    if (json.respuesta === 'OK') {
                        reloadGridAnticiposTrans();
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema('Debe seleccionar al menos un anticipo.', '300', '150');
    }
}

function verAnticipos(id_transportadora, banco,option){
    if (banco !== '') {
        if (id_transportadora !== '') {
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Administracion&accion=Logistica",
                dataType: 'json',
                async: false,
                data: {
                    opcion: 13,
                    id_transportadora: id_transportadora,
                    banco: banco
                }, success: function (json) {
                    if (!isEmptyJSON(json)) {
                        if (json[0].respuesta === 'error') {
                            mensajesDelSistema('Lo sentimos en estos momentos hay una transferencia en proceso', '330', '150');
                        } else {
                            buscarAnticiposPorTransferir(id_transportadora, banco, json);
                        }
                    } else {

                        if (option === 1) {
                            mensajesDelSistema("Lo sentimos no se encontraron anticipos para transferir!!", '320', '150');
                        } else {
                            jQuery("#tbl_anular_anticipos").jqGrid("clearGridData", true);
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        } else {
             mensajesDelSistema('Debe seleccionar la transportadora.', '300', '150');
        }
    } else {
        mensajesDelSistema('Debe seleccionar el banco de transferencia.', '300', '150');

    }

}

function buscarAnticiposPorTransferir(id_transportadora, banco,data) {
    var grid_tbl_transferir_aticipos = jQuery("#tbl_anular_anticipos");
      
        grid_tbl_transferir_aticipos.jqGrid({
            caption: "Anular anticipos",
            data: data,
            datatype: "local",
            height: '600',
            width: '1570',
            colNames: ['Id','Lote','Transportadora', 'Id manifiesto', 'Agencia', 'Conductor',
                'Propietario', 'Placa', 'Planilla', 'Fecha', 'Usuario', 'Producto',
                'Reanticipo', 'Aprobador', 'Valor', '% dcto','Valor dcto', 'Valor neto', 'comision',
                'Vlr consignacion', 'Banco', 'Sucursal', 'Cuenta', 'Tipo cta', 'Titular cta', 'Nit cta'],
            colModel: [
                {name: 'id', index: 'transportadora', width: 80, align: 'left', key: true},
                {name: 'nro_lote', index: 'nro_lote', width: 80, align: 'left'},
                {name: 'transportadora', index: 'transportadora', width: 120, align: 'left'},
                {name: 'id_manifiesto', index: 'id_manifiesto', width: 100, align: 'left', hidden: true},
                {name: 'nombre_agencia', index: 'nombre_agencia', sortable: true, width: 80, align: 'left'},
                {name: 'conductor', index: 'conductor', sortable: true, width: 195, align: 'left'},
                {name: 'propietario', index: 'propietario', sortable: false, width: 195, align: 'left'},
                {name: 'placa', index: 'placa', width: 80, align: 'center'},
                {name: 'planilla', index: 'planilla', width: 105, align: 'left'},
                {name: 'fecha_anticipo', index: 'fecha_anticipo', width: 120, align: 'left'},
                {name: 'usuario_creacion', index: 'usuario_creacion', width: 100, align: 'left'},
                {name: 'descripcion', index: 'descripcion', width: 120, align: 'left'},
                {name: 'reanticipo', index: 'reanticipo', width: 80, align: 'center'},
                {name: 'usuario_aprobacion', index: 'usuario_aprobacion', width: 80, align: 'center'},
                {name: 'valor_anticipo', index: 'valor_anticipo', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'porcetaje_descuto', index: 'porcetaje_descuto', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 2}},
                {name: 'valor_descuento', index: 'valor_descuento', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'valor_neto_con_descueto', index: 'valor_neto_con_descueto', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'comision', index: 'comision', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'valor_consignacion', index: 'valor_consignacion', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'banco', index: 'banco', width: 80, align: 'center'},
                {name: 'sucursal', index: 'sucursal', width: 80, align: 'center'},
                {name: 'cuenta', index: 'cuenta', width: 80, align: 'center'},
                {name: 'tipo_cuenta', index: 'tipo_cuenta', width: 80, align: 'center'},
                {name: 'nombre_cuenta', index: 'nombre_cuenta', width: 80, align: 'center'},
                {name: 'nit_cuenta', index: 'nit_cuenta', width: 80, align: 'center'}


            ],
            rowNum: 10000,
            rowTotal: 10000000,
            pager: ('#page_anular_anticipo'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },loadComplete: function () {
               // cacularTotalesTrans(grid_tbl_transferir_aticipos);
                if (grid_tbl_transferir_aticipos.jqGrid('getGridParam', 'records') > 0) {
                    cacularTotalesTrans(grid_tbl_transferir_aticipos);
                    // mensajesDelSistema("No se encontraron resultados para los parametros seleccionados.", "270", "170");
                }

            },
            onSelectRow: function(rowId) {
                var selectedRowsIds = $('#tbl_anular_anticipos').jqGrid('getGridParam', 'selarrrow');
                var totalSum = 0;

                $.each(selectedRowsIds, function(index, selectedRowId) {
                    totalSum += parseFloat(($('#tbl_anular_anticipos').jqGrid('getCell', selectedRowId, 'valor_consignacion')));
                });

                $('#tbl_anular_anticipos').jqGrid('footerData', 'set', { 
//                    porcetaje_descuto: 'T. Transferir',
                    valor_neto_con_descueto: totalSum });
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_anular_anticipo", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        
        $("#transDiv").show();   

}

function cacularTotalesTrans(grid_tbl_aprobar_aticipos) {

    var valor_anticipo = grid_tbl_aprobar_aticipos.jqGrid('getCol', 'valor_anticipo', false, 'sum');

    grid_tbl_aprobar_aticipos.jqGrid('footerData', 'set', {
        fecha_anticipo: 'Totales',
        valor_anticipo: valor_anticipo
    });

}

function reloadGridAnticiposTrans() {
    jQuery("#tbl_anular_anticipos").setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Logistica",
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: 13,
                id_transportadora: $("#transportadora2").val(),
                banco: $("#banco_tercero").val()
            }
        }
    });

    jQuery("#tbl_anular_anticipos").trigger("reloadGrid");
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();
}