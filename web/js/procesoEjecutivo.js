/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 $(document).ready(function() {    
    var jsonUnd = cargarUnidadesNegocio();
    cargarTabsEtapas(jsonUnd);  
 });
 
 function initDemanda() {   
    /*$('#unidad_negocio').change(function(){ 
        var und_negocio = $('#unidad_negocio').val();
        if (und_negocio!=='0'){
           verListadoNegocios("tabla_demandas","1");
        }
    });*/

    $('#mas_hechos').click(function() {
        var totItems = document.getElementsByName('descr_hechos').length;   
        var indice = totItems + 1;
        addElement('divHechosDem',"neo_"+indice,"hechos","");                               
    });     

    $('#mas_pretensiones').click(function() {
        var totItems = document.getElementsByName('descr_pretensiones').length; 
        var indice = totItems + 1;
        addElement('divPretensionesDem',"neo_"+indice,"pretensiones","");          
    });     

    $('#mas_medidas').click(function() {
        var totItems = document.getElementsByName('descr_medidas').length; 
        var indice = totItems + 1;
        addElement('divMedidas',"neo_"+indice,"medidas","");          
    });     
   
}


 function cargarTabsEtapas(jsonUnd) {
   
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Proceso&accion=Ejecutivo",
        dataType: 'json',
        async:false,
        data: {
            opcion: 8,
            soloActivas: "S"
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {  
                for (var i=0;i<json.rows.length;i++){
                    addTabEtapa(json.rows[i].id,json.rows[i].nombre,jsonUnd);                 
                }
                $('#tabs_proceso').tabs({
                    show: function(event, ui) {      
                        $('#unidad_negocio_'+json.rows[ui.index].id).val('0');
                        $('#negocio_'+json.rows[ui.index].id).val('');
                        $('#cedula_'+json.rows[ui.index].id).val('');
                        //verListadoNegocios('tabla_'+json.rows[ui.index].id, json.rows[ui.index].id);                 
                    }
                });

            } 
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

 function cargarUnidadesNegocio() {
    var  Result={};
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Proceso&accion=Ejecutivo",
        dataType: 'json',
        async:false,
        data: {
            opcion: 20,
            ref_1: "und_proc_ejec"
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    Result={};
                    return;
                }
                Result=json;
               /* try {
                    $('#'+id).append("<option value='0'>Seleccione</option>");
               
                    for (var key in json) {              
                       $('#'+id).append('<option value=' + json[key].id + '>' + json[key].descripcion + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }*/
            } 

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return Result;
}

function verListadoNegocios(tableid,idetapa){   
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    var params = { 
        opcion:21,
        idetapa:idetapa,
        und_negocio:$('#unidad_negocio_'+idetapa).val(),
        negocio: $('#negocio_'+idetapa).val(),
        cedula: $('#cedula_'+idetapa).val()
    };
    if ($("#gview_"+tableid).length) {
         refrescarGridListadoNegocios(tableid,params);
     }else {
        jQuery("#"+tableid).jqGrid({
            caption: 'Negocios para iniciar proceso juridico',
            url: url,          
            datatype: 'json',
            height: 450,
            width: 1650,
            colNames: ['Und Negocio', 'Negocio', 'Altura mora', 'F. inicio', 'Dias transc.', 'Cedula','Nombre Cliente', 'Ciudad', 'Direcci�n', 'Barrio', 'Tel�fono', 'Celular', 'Email', 'Pagar�', 'IdDemanda', 'Afiliado comercial', 'Id juzgado', 'Radicado', 'Docs generados', 'Valor negocio', 'Valor saldo','Acciones'],
            colModel: [
                {name: 'und_negocio', index: 'und_negocio', sortable: true, align: 'center', width: '120px'},               
                {name: 'negocio', index: 'negocio', sortable: true, align: 'center', width: '110px', key:true},  
                {name: 'mora', index: 'mora', sortable: true, align: 'center', width: '110px'},
                {name: 'fecha_inicio', index: 'fecha_inicio', sortable: true, align: 'center', width: '110px'},
                {name: 'dias_transcurridos', index: 'dias_transcurridos', sortable: true, align: 'center', width: '110px'},
                {name: 'cedula', index: 'cedula', sortable: true, align: 'center', width: '120px'},              
                {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '350px'},
                {name: 'ciudad', index: 'ciudad', sortable: true, align: 'left', width: '150px'},
                {name: 'direccion', index: 'direccion', sortable: true, align: 'left', width: '250px'},
                {name: 'barrio', index: 'barrio', sortable: true, align: 'left', width: '170px'},
                {name: 'telefono', index: 'telefono', sortable: true, align: 'center', width: '110px'},
                {name: 'celular', index: 'celular', sortable: true, align: 'center', width: '120px'},      
                {name: 'email', index: 'email', sortable: true, align: 'left', width: '150px'},                     
                {name: 'num_pagare', index: 'num_pagare', sortable: true, align: 'center', width: '110px'}, 
                {name: 'id_demanda', index: 'id_demanda', sortable: true, hidden:true, align: 'center', width: '90px'},  
                {name: 'niter', index: 'niter', sortable: true, align: 'left', width: '380px'},
                {name: 'id_juzgado', index: 'id_juzgado', sortable: true, hidden:true, align: 'center', width: '90px'},  
                {name: 'radicado', index: 'radicado', sortable: true, hidden:true, align: 'left', width: '380px'},
                {name: 'docs_generados', index: 'docs_generados', sortable: true, hidden:true, align: 'left', width: '380px'},
                {name: 'vr_negocio', index: 'vr_negocio', sortable: true, align: 'right', width: '170px', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},       
                {name: 'valor_saldo', index: 'valor_saldo', sortable: true, align: 'right', width: '130px',sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '210px'}
            ],
            rowNum: 5000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true, 
            subGrid: true,
            subGridOptions: { "plusicon" : "ui-icon-triangle-1-e",
                  "minusicon" :"ui-icon-triangle-1-s",
                  "openicon" : "ui-icon-arrowreturn-1-e",
                  "reloadOnExpand" : true,
                  "selectOnExpand" : true },
            hidegrid: false,
            pager:'#page_'+tableid,
            /*pgtext: null,
            pgbuttons: false,*/
            multiselect:true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: { 
                async:false,              
                type: "POST",                
                data: params
            },
            gridComplete: function() {
                var ids = jQuery("#" + tableid).jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var id_demanda = jQuery("#" + tableid).getRowData(cl).id_demanda;
                    var id_juzgado = jQuery("#" + tableid).getRowData(cl).id_juzgado;
                    var radicado = jQuery("#" + tableid).getRowData(cl).radicado;
                    var docs_generados = jQuery("#" + tableid).getRowData(cl).docs_generados;
                    var bgcolor = '#FFFFFF none repeat scroll 0 0;';
                        if (id_demanda === '0' || (id_demanda !== '0' && docs_generados==='N')) {
                           // $("#" + tableid).jqGrid('setRowData', ids[i], false, {weightfont: 'bold', background: 'rgba(245, 140, 112, 0.53)'});
                           bgcolor = 'rgba(245, 140, 112, 0.53)';                         
                        } else if (id_juzgado === '0' || radicado === '') {
                            //$("#" + tableid).jqGrid('setRowData', ids[i], false, {weightfont: 'bold', background: 'rgba(255, 255, 0, 0.57)'});    
                            bgcolor = 'rgba(255, 255, 0, 0.57)';                         
                        } 
                  
                        changeBackgroundRow(tableid, cl, bgcolor);
                            
                    ed = "<img src='/fintra/images/application_go.png' align='absbottom'  name='addRespuesta' id='addRespuesta' width='15' height='15' title ='Registrar acci�n'  onclick=\"AbrirDivRespuestaProceso('" + idetapa + "','" + cl + "','" + id_demanda + "');\">";
                    genDoc = "<img src='/fintra/images/edit.ico' align='absbottom'  name='genera_docs' id='genera_docs' width='15' height='15' title ='Generar Documentos'  onclick=\"AbrirDivGeneraDemandaDocs('" + idetapa + "','" + cl + "','" + id_demanda + "');\">";
                    verDoc = "<img src='/fintra/images/botones/iconos/pdf.png' align='absbottom'  name='descarga_docs' id='descarga_docs' width='15' height='15' title ='Ver Documento Generado'  onclick=\"bajarDemandaDocs('" + cl + "','" + id_demanda + "');\">";
                    rad = "<img src='/fintra/images/botones/iconos/court.png' align='absbottom'  name='addJuzgado' id='addJuzgado' width='15' height='15' title ='Registrar radicado juzgado'  onclick=\"AbrirDivRadicadoJuzgado('" + idetapa + "','" + cl + "','" + id_demanda + "','" + tableid + "');\">";
                    traz = "<img src='/fintra/images/botones/iconos/trazabilidad.gif' align='absbottom'  name='ver_traz' id='ver_traz' width='15' height='15' title ='Ver trazabilidad'  onclick=\"verTrazabilidadProceso('" + cl + "');\">";
                    (idetapa === 1) ? jQuery("#" + tableid).jqGrid('setRowData', ids[i], {actions: ed + '  ' + genDoc + '  ' + verDoc + '  '  + rad + '  '  + traz }) : jQuery("#" + tableid).jqGrid('setRowData', ids[i], {actions: ed + '  ' + verDoc + '  ' + '  ' + traz});

                }
            },
            subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id = subgrid_id+"_t"; 
                jQuery("#"+subgrid_id).html("<table id='"+subgrid_table_id+"'></table>");
                jQuery("#"+subgrid_table_id).jqGrid({
                    url: './controller?estado=Proceso&accion=Ejecutivo',                    
                    datatype: 'json',
                    colNames: ['Id','Concepto', 'Tipo', 'Valor Porc.', 'Valor','Eliminar'],
                    colModel: [
                        {name: 'id', index: 'id', sortable: true, width: 60, align: 'center', hidden:true, key:true},
                        {name: 'concepto', index: 'concepto', sortable: true, align: 'left',width: '363px'},
                        {name: 'tipo', index: 'tipo', sortable: true, editable:true, hidden:true, align: 'center',width: '90px'},
                        {name: 'valor_porc', index: 'valor_porc', sortable: true, hidden:true, align: 'right', width: '175px', sorttype: 'currency',
                         formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor', index: 'valor', sortable: true, editable: true, align: 'right', width: '175px', sorttype: 'currency',
                         formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '80px'}
                    ],
                    rowNum:150,
                    height:'80%',
                    width:'90%',      
                    cellEdit: true,
                    cellsubmit: "clientArray",
                    jsonReader: {
                        root: 'rows',
                        cell:'',
                        repeatitems: false,
                        id: '0'
                    },   
                    ajaxGridOptions: {
                        async: false,
                        type: "POST",
                        data: {
                                opcion:23,
                                idetapa:idetapa,
                                cod_neg:row_id                                
                              }
                    },
                    afterSaveCell: function(rowid, celname, value, iRow, iCol) {
                        if (celname === 'valor') {
                           var valor_costo = 0;
                           var tipo = jQuery("#"+subgrid_table_id).getRowData(rowid).tipo;
                           var valor_porc = jQuery("#"+subgrid_table_id).getRowData(rowid).valor_porc;
                           valor_costo = (tipo==='P') ? parseFloat((valor_porc*value)/100): value;                     
                           jQuery("#"+subgrid_table_id).jqGrid('setCell', rowid, 'valor', valor_costo);
                           (isNaN(value)) ? $("#"+subgrid_table_id).jqGrid("restoreCell", iRow, iCol): actualizarCostoNegXEtapa(rowid,valor_costo);
                        }
                    },
                    gridComplete: function() {
                        var ids = jQuery("#"+subgrid_table_id).jqGrid('getDataIDs');
                        for (var i = 0; i < ids.length; i++) {
                            var cl = ids[i];
                            del = "<img src='/fintra/images/botones/iconos/eliminar.gif' align='absbottom'  name='eliminar' id='eliminar' width='15' height='15' title ='Eliminar'  onclick=\"eliminarCostoNegXEtapa('" + cl + "','"+tableid+"','"+subgrid_id+"');\">";
                            jQuery("#"+subgrid_table_id).jqGrid('setRowData', ids[i], {actions: del});             
                        }
                    },
                   /*loadComplete: function() {
                        var ids = jQuery("#"+subgrid_table_id).jqGrid('getDataIDs');
                        for (var i = 0; i < ids.length; i++) {
                            var id = ids[i];
                            if (jQuery("#"+subgrid_table_id).jqGrid('getCell', id, 'tipo') !== 'P') {
                                jQuery("#"+subgrid_table_id).jqGrid('setCell', id, 'valor', '', 'not-editable-cell');
                            }
                        }
                    },*/
                    loadError: function(xhr, status, error) {
                         mensajesDelSistema(error, 250, 150);
                    }
                });               
            },
            loadError: function(xhr, status, error) {
                 mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_"+tableid,{search:false,refresh:false,edit:false,add:false,del:false});
        jQuery("#"+tableid).jqGrid("navButtonAdd", "#page_"+tableid, {
            caption: "Asignar costos",
            title: "Asignar costos del proceso para la etapa",
            onClickButton: function() {
                var filasId = jQuery('#'+tableid).jqGrid('getGridParam', 'selarrrow');
                if (filasId != '') {
                    cargarCostosEtapa(idetapa);
                    ventanaCostosEtapa(tableid, idetapa);
                }else {
                    if (jQuery("#"+tableid).jqGrid('getGridParam', 'records') > 0) {
                        mensajesDelSistema("Debe seleccionar el(los) negocio(s) a los que desea asignar costos!!", '250', '150');
                    } else {
                        mensajesDelSistema("No hay negocios para asignar costos", '250', '150');
                    }

                }              
            }
        });  
        if (idetapa === 1) {           
            jQuery("#" + tableid).jqGrid("navButtonAdd", "#page_" + tableid, {
                caption: "Crear Demanda",
                title: "Crear demanda",
                onClickButton: function() {
                    crearDemanda(tableid, idetapa);
                }
            });          
        }   
        jQuery("#"+tableid).jqGrid("navButtonAdd", "#page_"+tableid, {
            caption: "Pasar a sgte etapa",
            title: "Pasar seleccionados a sgte etapa",
            onClickButton: function() {
                var filasId =jQuery('#'+tableid).jqGrid('getGridParam', 'selarrrow');
                if (filasId != ''){
                   cambiarEtapaProcJuridico(tableid, idetapa, parseInt(idetapa)+1);   
                }else{
                    if (jQuery('#'+tableid).jqGrid('getGridParam', 'records')>0) {
                         mensajesDelSistema("Debe seleccionar los procesos que desea pasar a siguiente etapa!!", '250', '150');
                    }else{
                         mensajesDelSistema("No hay procesos para pasar a siguiente etapa", '250', '150');
                    }
                }              
            }
        });
        jQuery("#" + tableid).jqGrid("navButtonAdd", "#page_"+tableid, {
              caption: "Devolver",
              title: "Devolver a etapa previa",
              onClickButton: function() {
                    var filasId =jQuery('#'+tableid).jqGrid('getGridParam', 'selarrrow');
                    if (filasId != ''){
                        $("#dialogAddComent").fadeIn();
                        ventanaAgregarComentario(tableid, idetapa, parseInt(idetapa)-1);
                        //cambiarEtapaProcJuridico(tableid, idetapa, parseInt(idetapa)-1); 
                    }else{
                        if (jQuery('#'+tableid).jqGrid('getGridParam', 'records')>0) {
                            mensajesDelSistema("Debe seleccionar los procesos que desea devolver a etapa previa!!", '250', '150');
                        }else{
                            mensajesDelSistema("No hay procesos para devolver", '250', '150');
                        }
                    }
              
              }
          });  
          jQuery("#" + tableid).jqGrid("navButtonAdd", "#page_" + tableid, {
            caption: "Exportar excel",
            onClickButton: function () {
                var info = jQuery('#' + tableid).getGridParam('records');
                if (info > 0) {
                    exportarExcel(tableid);
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }
            }
        });
  
     }
}


function refrescarGridListadoNegocios(tableid,params){    
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    jQuery("#"+tableid).setGridParam({
        datatype: 'json',
        url: url,
        ajaxGridOptions: {
            async:false,
            type: "POST",
            data: params
        }
    });
    
    jQuery('#'+tableid).trigger("reloadGrid");
}

function cargarCostosEtapa(idetapa){  
    var jsonParam;
    var is_automotor = ($('#unidad_negocio').val()==13) ? "S": "N" ; 
    var jsonParam = {
            opcion:24,
            idEtapa:idetapa,
            is_automotor:is_automotor
    };
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if ($("#gview_tabla_costos_proceso").length) {     
         refrescarGridCostosEtapa(jsonParam);
     }else {
        jQuery("#tabla_costos_proceso").jqGrid({
            caption: 'Costos Proceso',
            url: url,
            datatype: 'json',
            height: 230,
            width: 360,
            colNames: ['Id', 'Concepto', 'Tipo', 'Tipo', 'Valor', 'S. autom', 'Id Etapa'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden:true, align: 'center', width: '100px', key:true},
                {name: 'concepto', index: 'concepto', sortable: true, align: 'left', width: '430px'},
                {name: 'tipo', index: 'tipo', sortable: true, hidden:true, align: 'center', width: '90px'},  
                {name: 'simbolo', index: 'simbolo', sortable: true, align: 'center', width: '90px'},
                {name: 'valor', index: 'valor', sortable: true, editable:true, align: 'right', width: '170px',sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                {name: 'solo_automotor', index: 'solo_automotor', sortable: true, hidden:true, align: 'center', width: '180px'},
                {name: 'id_etapa', index: 'id_etapa', hidden:true}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,           
            pgtext: null,
            pgbuttons: false,
            multiselect:true,
            cellEdit: true,
            cellsubmit: "clientArray",
            jsonReader: {
                root: 'rows',
                cell:'',
                repeatitems: false,
                id: '0'
            },
            afterSaveCell: function(rowid, celname, value, iRow, iCol) {                
                if (celname === 'valor') {            
                   if (isNaN(value)) $("#tabla_costos_proceso").jqGrid("restoreCell", iRow, iCol);
                }
            },
            ajaxGridOptions: {                
               async:false,
               type:"POST",
               data:jsonParam
            },
            loadComplete: function() {
                var ids = jQuery("#tabla_costos_proceso").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var id = ids[i];                   
                    if (jQuery("#tabla_costos_proceso").jqGrid('getCell', id, 'tipo') === 'P') {
                        jQuery("#tabla_costos_proceso").jqGrid('setCell', id, 'valor', '', 'not-editable-cell');
                    }
                }
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });      
     }
}

function refrescarGridCostosEtapa(params){   
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    jQuery("#tabla_costos_proceso").setGridParam({
        url: url,
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: params
        }       
    });    
    jQuery('#tabla_costos_proceso').trigger("reloadGrid");
}


function ventanaCostosEtapa(tableid, idetapa) {
    
    $("#dialogAsignarCostos").dialog({
        width: 420,
        height: 380,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: true, 
        buttons: {      
            "Asignar": function () {          
		asignarCostosProceso(tableid, idetapa);               
            },
            "Salir": function () {          
		$(this).dialog("destroy");               
            }
        }
    });
}

function asignarCostosProceso(tableid, idetapa){

    var jsonNeg = [];
    var jsonObj = [];  
    var filasId =jQuery('#tabla_costos_proceso').jqGrid('getGridParam', 'selarrrow');
    if (filasId != ''){
         loading("Espere un momento por favor...", "270", "140"); 
        //Obtenemos el listado de negocios seleccionados
        var filasNeg =jQuery('#'+tableid).jqGrid('getGridParam', 'selarrrow');
        for (var j = 0; j < filasNeg.length; j++) {
            var cod_neg = filasNeg[j];
            var valor_saldo = jQuery("#"+tableid).getRowData(cod_neg).valor_saldo;         
            var negs = {};
            negs ["cod_neg"] = cod_neg;
            negs ["valor_saldo"] = valor_saldo;            
            jsonNeg.push(negs);
        } 
        var listNegocios = {};
        listNegocios ["negocios"] = jsonNeg;
        //Obtenemos listado de costos a ser asignados a los negocios
        for (var i = 0; i < filasId.length; i++) {
            var id = filasId[i];
            var idEtapa = jQuery("#tabla_costos_proceso").getRowData(id).id_etapa;
            var tipo = jQuery("#tabla_costos_proceso").getRowData(id).tipo;
            var valor = jQuery("#tabla_costos_proceso").getRowData(id).valor;
            var item = {};
            item ["id_costo"] = id;
            item ["id_etapa"] = idEtapa;
            item ["tipo"] = tipo;
            item ["valor"] = valor;
            jsonObj.push(item);
        }
        var listCostos = {};
        listCostos ["costos"] = jsonObj;
      
        var url = './controller?estado=Proceso&accion=Ejecutivo';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 25,
                listadoNegocios: JSON.stringify(listNegocios),
                listadoCostos: JSON.stringify(listCostos)
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {            
                        $("#dialogAsignarCostos").dialog('close');
                        for (var i = 0; i < filasNeg.length; i++) {                           
                            $("#"+tableid).jqGrid ('collapseSubGridRow', filasNeg[i]);                          
                        }                         
                       /* refrescarGridListadoNegocios(tableid,{
                            opcion:21,
                            idetapa:idetapa,
                            und_negocio:$('#unidad_negocio'+idetapa).val(), 
                            negocio: $('#negocio_'+idetapa).val(),
                            cedula: $('#cedula_'+idetapa).val()
                        });*/
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("costos asignados exitosamente", '250', '150', true);
                       
                    }

                } else {
                    $("#dialogLoading").dialog('close');                   
                    mensajesDelSistema("Lo sentimos no se pudo asignar los costos al proceso!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            } 
        });
        
    }else{
        if (jQuery("#tabla_costos_proceso").jqGrid('getGridParam', 'records')>0) {
             mensajesDelSistema("Seleccione los costos que desea asignar!!", '250', '150');
        }else{
             mensajesDelSistema("No hay costos por asignar", '250', '150');
        }
             
    }
}

function actualizarCostoNegXEtapa(id, valor) {
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 26,
            id: id,
            valor: valor
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo actualizar el valor!!", '250', '150');
            }

        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function eliminarCostoNegXEtapa(id, gridId, subgridId) {
  
 var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 27,
            id: id
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }
                
                if (json.respuesta === "OK") {                   
                    jQuery("#"+subgridId+'_t').jqGrid('delRowData',id);
//                  $("#"+gridId).toggleSubGridRow(subgridId+'_t');
//                  $("#"+gridId).expandSubGridRow(subgridId+'_t');
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo desasignar el costo!!", '250', '150');
            }

        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function refrescarGridCostosNegEtapa(tableid,id){    
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    jQuery("#" + tableid).setGridParam({
        datatype: 'json',
        url: url,
        ajaxGridOptions: {
            async: false,
            type: "POST",
            data: {opcion: 23,
                idetapa: 1,
                cod_neg: id}
        }
    });
    
    jQuery('#'+tableid).trigger("reloadGrid");
}

function AbrirDivRespuestaProceso(idetapa, negocio, id_demanda){
    if (id_demanda == "0") {
        mensajesDelSistema('Alerta!!! Para agregar una nueva acci�n primero debe haber creado la demanda', '350', '165');
    } else {
        cargarRespuestasEtapa(idetapa);
        $("#dialogAsignarRespProceso").fadeIn();
        ventanaRespuestasEtapa(idetapa,negocio,id_demanda);
    }  
}

 
  function cargarRespuestasEtapa(idetapa) {
    $('#respuesta_etapa').html('');
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Proceso&accion=Ejecutivo",
        dataType: 'json',
        data: {
            opcion: 28,
            idEtapa: idetapa
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#respuesta_etapa').append("<option value='0'>Seleccione</option>");
               
                    for (var key in json) {              
                       $('#respuesta_etapa').append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#respuesta_etapa').append("<option value='0'>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ventanaRespuestasEtapa(idetapa,negocio,id_demanda){
      $('#comentario').val('');
      $("#dialogAsignarRespProceso").dialog({
        width: 600,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'REGISTRO ACCIONES PROCESO EJECUTIVO',
        closeOnEscape: false,
        buttons: {  
            "Agregar": function () {
               agregarRespuestaProceso(idetapa,negocio,id_demanda);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function agregarRespuestaProceso(idetapa,negocio,id_demanda){
   if($('#respuesta_etapa').val()==='0'){
      mensajesDelSistema('Seleccione una respuesta', '250', '150');
   }else{
        var url = './controller?estado=Proceso&accion=Ejecutivo';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 54,
                id_etapa: idetapa,
                cod_neg: negocio,
                id_demanda: id_demanda,              
                idrespuesta: $('#respuesta_etapa').val(),
                respuesta: $('#respuesta_etapa option:selected').text(),
                comentario: $('#comentario').val()
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        mensajesDelSistema("accion asignada exitosamente", '250', '150', true);
                        $("#dialogAsignarRespProceso").dialog('close');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asignar la accion al proceso!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });      
   }
}

function existeDemandaParaNegocio(negocio, valida_demanda_gen){
    var estado = false;
    $.ajax({
        url: './controller?estado=Proceso&accion=Ejecutivo',
        datatype:'json',
        type:'post',
        async:false,
        data:{
            opcion: 49,
            cod_neg: negocio,
            validar_pdf: valida_demanda_gen
        },          
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.respuesta === "SI") {               
                   estado = true;                  
                }
                
            }              
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }  
    });
    return estado;
}

function crearDemanda(tableid, idetapa){
    
    var jsonNeg = [];
    var filasId =jQuery('#'+tableid).jqGrid('getGridParam', 'selarrrow');
    if (filasId != ''){
        for (var i = 0; i < filasId.length; i++) {           
            var nitDemandado = jQuery("#"+tableid).getRowData(filasId[i]).cedula;   
            var valor_saldo = jQuery("#"+tableid).getRowData(filasId[i]).valor_saldo;         
            var negs = {};
            negs ["cod_neg"] = filasId[i];
            negs ["nit_demandado"] = nitDemandado;
            negs ["valor_saldo"] = valor_saldo;            
            jsonNeg.push(negs);   
        }    
        var listNegocios = {};
        listNegocios ["negocios"] = jsonNeg;
      
        var url = './controller?estado=Proceso&accion=Ejecutivo';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 48,
                listadoNegocios: JSON.stringify(listNegocios)
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {   
                        refrescarGridListadoNegocios(tableid,{
                            opcion:21,
                            idetapa:idetapa,
                            und_negocio:$('#unidad_negocio'+idetapa).val(), 
                            negocio: $('#negocio_'+idetapa).val(),
                            cedula: $('#cedula_'+idetapa).val()     
                        });  
                        mensajesDelSistema("Se generaron los documentos base para los procesos seleccionados!!", '250', '150', true);
                    }else{
                        mensajesDelSistema(json.respuesta, '250', '150');
                        return;
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear la demanda!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            } 
        });
        
    }else{
        if (jQuery('#'+tableid).jqGrid('getGridParam', 'records')>0) {
             mensajesDelSistema("Debe seleccionar los negocios a los que desea crear la demanda!!", '250', '150');
        }else{
             mensajesDelSistema("No hay negocios a los que crear demanda", '250', '150');
        }
             
    }
}


function AbrirDivGeneraDemandaDocs (idetapa, negocio, id_demanda){
    if (id_demanda == "0") {
        mensajesDelSistema('Alerta!!! Para editar los documentos primero debe crear la demanda', '350', '165');
    } else {   
        $('#div_generar_demanda').fadeIn("slow");  
        $("#tabs").tabs();  
        $('.nicEdit-panelContain').parent().width('100%');   
        $('.nicEdit-panelContain').parent().next().width('100%');
        $('.nicEdit-main').width('99%');
        cargarDocumentosDemanda(negocio);  
        ventanaGenerarDemanda(negocio, idetapa);  
    }
}

function ventanaGenerarDemanda(negocio, idetapa){
    $("#div_generar_demanda").dialog({
        width: 980,
        height: 750,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'GENERACION DOCUMENTOS',
        closeOnEscape: false,
        buttons: {  
            "Guardar": function () {
               guardarDemandaDocs(negocio,"N",idetapa);
            },
            "Generar Pdf": function () {
               guardarDemandaDocs(negocio,"S",idetapa);              
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function cargarDocumentosDemanda (negocio) {
 
    $.ajax({
            type: "POST",
            dataType: "json",          
            data: {opcion:50,
                   cod_neg:negocio},
            async: false,
            url: './controller?estado=Proceso&accion=Ejecutivo',
            success: function(jsonData) {                  
                    if (!isEmptyJSON(jsonData)) {  
                        for (var i=0; i < jsonData.docs.length; i++){       
                            $('#id_demanda').val(jsonData.docs[i].id_demanda);
                            if (jsonData.docs[i].tipo_doc == 1){  
                              
                                $('#id_doc_dem').val(jsonData.docs[i].id_enc_doc);
                                nicEditors.findEditor("header_dem").setContent(jsonData.docs[i].header_info);
                                nicEditors.findEditor("intro_dem").setContent(jsonData.docs[i].initial_info);                             
                                nicEditors.findEditor("footer_dem").setContent(jsonData.docs[i].signing_info);                              
                                nicEditors.findEditor("fundamentos").setContent(jsonData.docs[i].aux_1);
                                nicEditors.findEditor("competencia").setContent(jsonData.docs[i].aux_2);
                                nicEditors.findEditor("prueba").setContent(jsonData.docs[i].aux_3);
                                nicEditors.findEditor("anexo").setContent(jsonData.docs[i].aux_4);
                                nicEditors.findEditor("notificacion").setContent(jsonData.docs[i].aux_5);  
                                removeTextEditors('divHechosDem');                              
                                for (var j=0;j < jsonData.docs[i].hechos.length;j++){    
                                    addElement('divHechosDem',jsonData.docs[i].hechos[j].id_det_doc,jsonData.docs[i].hechos[j].titulo,jsonData.docs[i].hechos[j].descripcion);
                                }
                                removeTextEditors('divPretensionesDem');                                                       
                                for (var k=0;k < jsonData.docs[i].pretensiones.length;k++){
                                    addElement('divPretensionesDem',jsonData.docs[i].pretensiones[k].id_det_doc,jsonData.docs[i].pretensiones[k].titulo,jsonData.docs[i].pretensiones[k].descripcion);
                                }
                            }else if (jsonData.docs[i].tipo_doc == 2){
                                $('#id_doc_med').val(jsonData.docs[i].id_enc_doc);
                                nicEditors.findEditor("header_med").setContent(jsonData.docs[i].header_info);
                                nicEditors.findEditor("intro_med").setContent(jsonData.docs[i].initial_info); 
                                nicEditors.findEditor("footer_med").setContent(jsonData.docs[i].signing_info);                                                         
                                
                                removeTextEditors('divMedidas');                            
                                for (var m=0;m < jsonData.docs[i].medidas.length;m++){
                                    addElement('divMedidas',jsonData.docs[i].medidas[m].id_det_doc,jsonData.docs[i].medidas[m].titulo,jsonData.docs[i].medidas[m].descripcion);
                                }                          
                            }else{
                                $('#id_doc_pod').val(jsonData.docs[i].id_enc_doc);
                                nicEditors.findEditor("header_pod").setContent(jsonData.docs[i].header_info);
                                nicEditors.findEditor("intro_pod").setContent(jsonData.docs[i].initial_info);                               
                                nicEditors.findEditor("footer_pod").setContent(jsonData.docs[i].signing_info);                                                           
                            }
                        }       
                        $('.nicEdit-panelContain').parent().width('100%');   
                        $('.nicEdit-panelContain').parent().next().width('100%'); 
                        $('.nicEdit-main').width('99%');                     
                     
                    }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
    });
        
 }
 
 function addElement(divId,indice,titulo,value) {     
    var ni = document.getElementById(divId); 
    var e, b;
    
    b = document.createElement('img');
    b.id = 'del_'+titulo.toLowerCase()+'_' + indice;   
    b.onclick = function() {        
        var id = b.id.replace("del_","");
        $('#'+id+'+ br').remove();
        nicEditors.findEditor(id).remove(); 
        $('#'+id).remove();   
        $('#'+b.id).remove();         
    };
    b.alt = 'menos';
    b.src = '/fintra/images/botones/iconos/close22.png';
    b.style = 'margin-left:0px;height:100%;vertical-align: middle;float:right';
    ni.appendChild(b); 
    
    e = document.createElement('textarea');
    e.id = titulo.toLowerCase()+'_' + indice;   
    e.name = 'descr_'+titulo.toLowerCase();
    e.setAttribute('class', 'editor'); 
    
    ni.appendChild(e); 
    new nicEditor({minHeight : 400, maxHeight : 600, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance(e.id); 
    //new nicEditor({iconsPath : './js/jquery/editor/nicEditorIcons.gif'}).panelInstance(e.id);
    nicEditors.findEditor(e.id).setContent(value);   
    ni.appendChild(document.createElement('br'));
  
}

 function addTabEtapa(idetapa,nometapa,jsonUnd) {     
    var ni = document.getElementById('tabs_proceso'); 
    var ul = document.getElementById('lst_etapas'); 
    var e, li, b;
    li = document.createElement('li');  
    e = document.createElement('a');
    e.setAttribute('href', '#tab_'+idetapa);
    e.innerHTML = nometapa; 
    li.appendChild(e);
    ul.appendChild(li);
    e = document.createElement('div');
    e.id = 'tab_'+ idetapa;
    ni.appendChild(e);
    $('#tab_'+idetapa).append('<fieldset>'+
                  '<legend class="labels">FILTROS</legend>'+
                       '<table width="1100" border="0" align="left" cellpadding="1" cellspacing="1" class="labels">'+
                         '<tr>'+
                           '<td width="190">'+
                               '<fieldset>'+
                                   '<legend>UNIDADES DE NEGOCIO</legend>'+
                                       '<table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">'+
                                            '<tr>'+
                                                '<td>'+
                                                    '<select name="unidad_negocio_' +idetapa+ '" class="combo_180px" id="unidad_negocio_'+idetapa+'">'+

                                                    '</select>'+
                                                '</td>'+
                                           ' </tr>'+
                                        '</table>'+
                                '</fieldset>'+
                        '</td>'+ 
                         '<td width="100">'+
                               '<fieldset>'+
                                   '<legend>NEGOCIO</legend>'+
                                       '<table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">'+
                                            '<tr>'+
                                                '<td>'+
                                                    '<input type="text" name="negocio_' +idetapa+ '" id="negocio_'+idetapa+'"/>'+
                                                '</td>'+
                                           ' </tr>'+
                                        '</table>'+
                                '</fieldset>'+
                        '</td>'+ 
                         '<td width="100">'+
                               '<fieldset>'+
                                   '<legend>ID CLIENTE</legend>'+
                                       '<table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">'+
                                            '<tr>'+
                                                 '<td>'+
                                                    '<input type="text" name="cedula_' +idetapa+ '" id="cedula_'+idetapa+'" class="solo-numero"/>'+
                                                '</td>'+
                                           ' </tr>'+
                                        '</table>'+
                                '</fieldset>'+
                        '</td>'+ 
                        '<td>'+
                            '<span  class="form-submit-button form-submit-button-simple_green_apple" id="btn_search_' +idetapa+ '">Buscar </span>'+
                        '</td>'+
                    '</tr>'+
                '</table>'+
            '</fieldset>'+
            '<center>'+
                '<br>'+
                 '<table id="tabla_' +idetapa+ '"></table>'+
                 '<div id="page_tabla_' +idetapa+ '"></div>'+ 
            '</center>');
    
    if (idetapa == 1)
        $('#tab_' + idetapa).append('<div id="divObservaciones" style="text-align: left">' +
                '<h4> <span> <img id="square_red" src="/fintra/images/cuadroRojo.png" ' +
                'style="margin-left: 5px; height: 16px; width:16px; vertical-align: middle;"> ' +
                '</span> Procesos pendientes por generar demanda. Recuerde que para pasar a la siguiente etapa primero se ha de haber generado la demanda. </h4>' +
                '<h4> <span> <img id="square_yellow" src="/fintra/images/cuadroAmarillo.png" ' +
                'style="margin-left: 5px; height: 16px; width:16px; vertical-align: middle;"> ' +
                '</span> Procesos pendientes por ingresar radicado juzgado. Recuerde que para pasar a la siguiente etapa debe registrar n�mero de radicado. </h4>'+
                '</div>');
    
    //Cargamos combo unidades de negocio
    $('#unidad_negocio_'+idetapa).append("<option value='0'>Todas</option>");
    for (var i=0; i<jsonUnd.length; i++){
          $('#unidad_negocio_' +idetapa).append('<option value=' + jsonUnd[i].id + '>' + jsonUnd[i].descripcion + '</option>');   
    }  
    
    $('.solo-numero').keyup(function() {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    
    //Indicamos evento para cada boton de busqueda
    $('#btn_search_'+idetapa).click(function() {
        verListadoNegocios('tabla_'+idetapa, idetapa);      
    });     
}

 function guardarDemandaDocs (negocio, generar_pdf,idetapa){
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    loading("Espere un momento por favor...", "270", "140"); 
    $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion:51,
                    id_demanda: $('#id_demanda').val(),
                    listadoDocs:guardarDemandaDocsToJSON
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") { 
                            cargarDocumentosDemanda(negocio);
                            if(generar_pdf === "S"){
                                generarDocsPdf(negocio,idetapa);
                                $("#dialogLoading").dialog('close');
                            }else{
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema("Documentos guardados exitosamente", '250', '150', true);                 
                            }                        
                               
                        }
                        
                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se pudo guardar los documentos!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
 }
 
 
function guardarDemandaDocsToJSON(){
        var jsonDocs = { "docs":[                               
                                {
                                 "id_doc": $('#id_doc_dem').val(),                                 
                                 "tipo_doc": 1,
                                 "header_info": new nicEditors.findEditor('header_dem').getContent(),
                                 "initial_info": new nicEditors.findEditor('intro_dem').getContent(),
                                 "footer_info": "",
                                 "signing_info": new nicEditors.findEditor('footer_dem').getContent(),
                                 "footer_page": "",
                                 "aux_1": new nicEditors.findEditor('fundamentos').getContent(),
                                 "aux_2": new nicEditors.findEditor('competencia').getContent(),
                                 "aux_3": new nicEditors.findEditor('prueba').getContent(),
                                 "aux_4": new nicEditors.findEditor('anexo').getContent(),
                                 "aux_5": new nicEditors.findEditor('notificacion').getContent(),
                                 "hechos":[],
                                 "pretensiones":[]
                               },{
                                 "id_doc": $('#id_doc_med').val(),                               
                                 "tipo_doc": 2,
                                 "header_info": new nicEditors.findEditor('header_med').getContent(),
                                 "initial_info": new nicEditors.findEditor('intro_med').getContent(),
                                 "footer_info": "",
                                 "signing_info": new nicEditors.findEditor('footer_med').getContent(),
                                 "footer_page": "",
                                 "aux_1": "",
                                 "aux_2": "",
                                 "aux_3": "",
                                 "aux_4": "",
                                 "aux_5": "",
                                 "medidas":[]
                               },{
                                 "id_doc": $('#id_doc_pod').val(),                               
                                 "tipo_doc": 3,
                                 "header_info": new nicEditors.findEditor('header_pod').getContent(),
                                 "initial_info": new nicEditors.findEditor('intro_pod').getContent(),
                                 "footer_info": "",
                                 "signing_info": new nicEditors.findEditor('footer_pod').getContent(),
                                 "footer_page": "",
                                 "aux_1": "",
                                 "aux_2": "",
                                 "aux_3": "",
                                 "aux_4": "",
                                 "aux_5": ""                                 
                               }
                        ]}; 
                        var h = document.getElementsByName('descr_hechos');                      
                        for (var i = 0; i < h.length; i++) {
                            var id = h[i].id.replace("hechos_","");
                            var item = {};
                            item ["id_det_doc"] = id;
                            item ["tipo"] = "H";
                            item ["titulo"] = "HECHOS";
                            item ["descripcion"] = new nicEditors.findEditor(h[i].id).getContent();            
                            jsonDocs.docs[0].hechos.push(item);
                        }
             
                        var p = document.getElementsByName('descr_pretensiones');                      
                        for (var i = 0; i < p.length; i++) {
                            var id = p[i].id.replace("pretensiones_","");
                            var item = {};                        
                            item ["id_det_doc"] = id;
                            item ["tipo"] = "P";
                            item ["titulo"] = "PRETENSIONES";
                            item ["descripcion"] = new nicEditors.findEditor(p[i].id).getContent();            
                            jsonDocs.docs[0].pretensiones.push(item);
                        }
                        
                        var m = document.getElementsByName('descr_medidas');                      
                        for (var i = 0; i < m.length; i++) {
                            var id = m[i].id.replace("medidas_","");
                            var item = {};                        
                            item ["id_det_doc"] = id;
                            item ["tipo"] = "M";
                            item ["titulo"] = "MEDIDAS";
                            item ["descripcion"] = new nicEditors.findEditor(m[i].id).getContent();            
                            jsonDocs.docs[1].medidas.push(item);
                        }

        return JSON.stringify(jsonDocs); 
     
}

function generarDocsPdf (negocio,idetapa){
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 52,
            id_demanda: $('#id_demanda').val(),
            cod_neg: negocio
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    mensajesDelSistema("Documentos generados exitosamente", '250', '150', true);                 
                    $("#div_generar_demanda").dialog('close');
                    jQuery('#tabla_'+idetapa).jqGrid('setCell', negocio, 'docs_generados', 'S');
                    changeBackgroundRow('tabla_'+idetapa, negocio, 'rgba(255, 255, 0, 0.57)');                 
                }else{
                    mensajesDelSistema(json.respuesta, '250', '150');
                    return;
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo generar los documentos!!", '250', '150');
            }

        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
 }


 
function removeTextEditors(divId) {
    $('#' + divId).find('textarea.editor').each(function() {
        var id = $(this).attr('id');
        nicEditors.findEditor(id).remove();
        $(this).remove();
    });    
    $('#' + divId).find('> img, br').each(function() {       
        $(this).remove();
    });    
}

function ventanaAgregarComentario(tableid, idetapa, idetapaNew){
      $("#comment").val('');
      $("#dialogAddComent").dialog({
        width: 580,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'AGREGAR COMENTARIO',
        closeOnEscape: false,
        buttons: {  
            "Agregar": function () {
                if ($('#comment').val() === '') {
                    mensajesDelSistema('Por favor, ingrese la anotaci�n correspondiente', '250', '150');
                } else {
                    cambiarEtapaProcJuridico(tableid, idetapa, idetapaNew);
                }               
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function cambiarEtapaProcJuridico(tableid, idetapa, idetapaNew){
    
    var listado = "";
    var filasId =jQuery('#'+tableid).jqGrid('getGridParam', 'selarrrow');
   
        for (var i = 0; i < filasId.length; i++) {              
            if (idetapa == 1 && idetapa < idetapaNew){                
                var radicado = $("#" + tableid).getRowData(filasId[i]).radicado;
                var docs_generados = $("#" + tableid).getRowData(filasId[i]).docs_generados;
                if(docs_generados === 'S' && radicado!=='')    listado += filasId[i] + ",";      
            }else{
                listado += filasId[i] + ",";      
            }          
        }   
        mensajesDelSistema(docs_generados, '250', '150');
        if (listado !=''){
            var url = './controller?estado=Proceso&accion=Ejecutivo';
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 22,
                    idetapa: idetapa,
                    idetapaNew: idetapaNew,
                    comentario: $('#comment').val(),
                    actualizar_fecha: 'N',
                    listado: listado
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }

                        if (json.respuesta === "OK") {                           
                            mensajesDelSistema("operaci�n exitosa", '250', '150', true);   
                            if ($('#dialogAddComent').is(':visible')) $("#dialogAddComent").dialog('close');
                            refrescarGridListadoNegocios(tableid, {
                                opcion: 21,
                                idetapa: idetapa,
                                und_negocio: $('#unidad_negocio' + idetapa).val(),
                                negocio: $('#negocio_' + idetapa).val(),
                                cedula: $('#cedula_' + idetapa).val()
                            });
                        }

                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo realizar la operaci�n!!", '250', '150');
                    }

                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }else{
            $("#dialogAddComent").dialog('close');
        }

}

function bajarDemandaDocs(negocio, id_demanda) {   
    if(!existeDemandaParaNegocio(negocio, "SI")) {
           mensajesDelSistema("No se han generado documentos para el negocio seleccionado. Por favor, verifique.", '250', '175');
    }else{
        $.ajax({
            type: "POST",
            url: './controller?estado=Proceso&accion=Ejecutivo',
            dataType: "json",
            data: {
                opcion: 53,
                cod_neg: negocio,
                id_demanda: id_demanda
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "SI") {  
                       // alert(json.Ruta);
                      window.open('.'+json.Ruta);
                    }else{
                         mensajesDelSistema("Lo sentimos no se pudo visualizar el documento!!", '250', '150');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo realizar operacion!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            } 
        });      
    }
}

function verTrazabilidadProceso(cl){
    
    $('#dialogTrazabilidad').fadeIn("slow");  
    cargarProcesoTrazabilidad(cl);
    ventanaVerTrazabilidad();
   
}

function ventanaVerTrazabilidad() { 
    $("#dialogTrazabilidad").dialog({        
        width: 970,
        height: 640,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:"Ver Trazabilidad Proceso",
        closeOnEscape: true,       
        buttons: {          
            "Salir": function () {          
		$(this).dialog("close");               
            }
        }
    });
}

function cargarProcesoTrazabilidad(negocio){  
  
    var jsonParam = {
            opcion:56,
            cod_neg:negocio
    };
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if ($("#gview_tabla_trazabilidad").length) {     
         refrescarGridProcesosTrazabilidad(jsonParam);
     }else {
        jQuery("#tabla_trazabilidad").jqGrid({
            caption: '',
            url: url,
            datatype: 'json',
            height: 450,
            width: 950,
            colNames: ['Id',  'Tipo', 'Etapa', 'Usuario', 'Fecha', 'Negocio', 'Comentarios', 'Concepto', 'Valor'],
            colModel: [
                {name: 'id_etapa', index: 'id_etapa', sortable: true, hidden: true, align: 'center', width: '100px'},
                {name: 'tipo', index: 'tipo', sortable: true, align: 'center', width: '90px'},
                {name: 'etapa', index: 'etapa', sortable: true, align: 'center', width: '230px'},
                {name: 'usuario', index: 'usuario', sortable: true, align: 'center', width: '100px'},
                {name: 'fecha', index: 'fecha', sortable: true, align: 'center', width: '150px'},
                {name: 'negocio', index: 'negocio', sortable: true, align: 'center', width: '100px', key: true},
                {name: 'comentarios', index: 'comentarios', sortable: true, align: 'left', width: '450px'},
                {name: 'concepto', index: 'concepto', sortable: true, align: 'left', width: '300px'},
                {name: 'valor', index: 'valor', sortable: true, width: '110px', align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}    
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,           
            pgtext: null,
            pgbuttons: false,          
            jsonReader: {
                root: 'rows',
                cell:'',
                repeatitems: false,
                id: '0'
            },          
            ajaxGridOptions: {                
               async:false,
               type:"POST",
               data:jsonParam
            },         
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });      
     }
}

function refrescarGridProcesosTrazabilidad(params){   
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    jQuery("#tabla_trazabilidad").setGridParam({
        url: url,
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: params
        }       
    });    
    jQuery('#tabla_trazabilidad').trigger("reloadGrid");
}

function AbrirDivRadicadoJuzgado(idetapa, negocio, id_demanda, tableid){
    if (id_demanda == "0") {
        mensajesDelSistema('Alerta!!! Para agregar juzgado primero debe haber creado la demanda', '350', '165');
    } else {
        cargarJuzgados();       
        ventanaRadicadoJuzgado(idetapa,negocio,id_demanda, tableid);
    }  
}

 
  function cargarJuzgados() {
    $('#juzgados').html('');
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Proceso&accion=Ejecutivo",
        dataType: 'json',
        async:false,
        data: {
            opcion: 63
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#juzgados').append("<option value='0'>Seleccione</option>");
               
                    for (var key in json) {              
                       $('#juzgados').append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#juzgados').append("<option value='0'>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ventanaRadicadoJuzgado(idetapa,negocio,id_demanda, tableid){
      var idJuzgado = $("#"+tableid).getRowData(negocio).id_juzgado;    
      var radicado = $("#"+tableid).getRowData(negocio).radicado;
      $('#juzgados').val(idJuzgado);
      $('#radicado').val(radicado);
      $("#dialogRadicadoJuzgado").dialog({
        width: 450,
        height: 180,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'REGISTRO RADICADO DEL JUZGADO',
        closeOnEscape: false,
        buttons: {  
            "Agregar": function () {
               agregarRadicadoJuzgado(idetapa,negocio,id_demanda, tableid);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function agregarRadicadoJuzgado(idetapa,negocio,id_demanda, tableid){
   if($('#juzgados').val()==='0'){
      mensajesDelSistema('Seleccione el juzgado', '250', '150');
   }else if ($('#radicado').val()===''){
      mensajesDelSistema('Por favor, ingrese el No de radicado otorgado por el juzgado', '250', '150');
   }else{
        var url = './controller?estado=Proceso&accion=Ejecutivo';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 64,
                id_etapa: idetapa,
                cod_neg: negocio,
                id_demanda: id_demanda,              
                idjuzgado: $('#juzgados').val(),
                juzgado: $('#juzgados option:selected').text(),
                radicado: $('#radicado').val()
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {         
                        mensajesDelSistema("radicado asignado exitosamente", '250', '150', true);  
                        $("#dialogRadicadoJuzgado").dialog('close');                     
                      
                            jQuery("#"+tableid).jqGrid('setCell', negocio, 'id_juzgado', $('#juzgados').val());
                            jQuery("#"+tableid).jqGrid('setCell', negocio, 'radicado', $('#radicado').val());   
                            changeBackgroundRow(tableid, negocio, 'rgba(255,255,255, 0.0)');
                           /*refrescarGridListadoNegocios(tableid, {
                                opcion: 21,
                                idetapa: idetapa,
                                und_negocio: $('#unidad_negocio' + idetapa).val(),
                                negocio: $('#negocio_' + idetapa).val(),
                                cedula: $('#cedula_' + idetapa).val()
                            });*/
                      
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asignar el radicado al proceso!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });      
   }
}


function mensajeConfirmAction(msj, width, height, okAction, id) { 
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function loading(msj, width, height) {
    
    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {   
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}


function  exportarExcel(tableid) {
    var fullData = jQuery("#" + tableid).jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: './controller?estado=Proceso&accion=Ejecutivo',
        data: {
            listado: myJsonString,
            opcion: 57
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function changeBackgroundRow(tableid, cl, bgcolor){
    
    jQuery("#" + tableid).jqGrid('setCell', cl, "negocio", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "und_negocio", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "negocio", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "mora", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "fecha_inicio", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "dias_transcurridos", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "cedula", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "nombre", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "ciudad", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "direccion", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "barrio", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "telefono", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "celular", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "email", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "num_pagare", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "niter", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "vr_negocio", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#" + tableid).jqGrid('setCell', cl, "valor_saldo", "", {'background-color': bgcolor, 'background-image': 'none'}); 
    
}

/*function insertarProcesosBajoDemanda(jsonUnd) {   
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Proceso&accion=Ejecutivo",
        dataType: 'json',
        async:false,
        data: {
            opcion: 69
        },
        success: function(json) {
            if (!isEmptyJSON(json)) { 
                if(json.respuesta === 'OK'){
                    cargarTabsEtapas(jsonUnd);
                }                  
            } 
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}*/