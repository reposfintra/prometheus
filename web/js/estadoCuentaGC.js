/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


 $(document).ready(function() {  

     $('#periodo').attr({disabled: false});
    var fechaHoy = new Date(); 
    anioActual = fechaHoy.getFullYear();
    mesActual = (parseInt(fechaHoy.getMonth()+1)<10)?'0'+(fechaHoy.getMonth()+1).toString():fechaHoy.getMonth()+1;   
    $('#periodo').val(anioActual.toString()+mesActual.toString());

    $('#buscar').click(function () {
        cargarEstadoCuenta();
    });
    
 });
 
 
 function cargarEstadoCuenta() {
    var grid_tabla_ = jQuery("#tabla_estadoCuenta");
    if ($("#gview_tabla_estadoCuenta").length) {
        reloadGridMostrar(grid_tabla_, 38);
    } else {
        grid_tabla_.jqGrid({
            caption: "Estado Cuenta",
            url: "./controller?estado=Garantias&accion=Comunitarias",
            mtype: "POST",
            datatype: "json",
            height: '600',
            width: '1200',
            //autowidth: false,
            colNames: ['Periodo','Cod cliente', 'Identificacion', 'Nombre', 'Negocio', 'Cuotas', 'Vlr Indemnizado', 'Fecha Primera Cuota'
                            ,'Linea Negocio', 'Acciones'],
                colModel: [
                    {name: 'periodo_foto', index: 'periodo_foto', width: 70, align: 'center'},
                    {name: 'codcli', index: 'codcli', width: 80, sortable: true, align: 'center',hidden:true},
                    {name: 'nit_cliente', index: 'nit_cliente', width: 80, sortable: true, align: 'left'},
                    {name: 'nombre_cliente', index: 'nombre_cliente', sortable: true, width: 210, align: 'left'},
                    {name: 'negocio', index: 'negocio', width: 70, align: 'center'},
                    {name: 'total_cuotas', index: 'total_cuotas', width: 50, align: 'center'},
                    {name: 'valor_indemnizado', index: 'valor_indemnizado', width: 100, align: 'right',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                    {name: 'fecha_primera_cuota', index: 'fecha_primera_cuota', width: 100, align: 'center'},
                    {name: 'linea_negocio', index: 'linea_negocio', width: 70, align: 'center',hidden:true},
                    {name: 'acciones', index: 'acciones', width: 100, align: 'center', sortable: false}
                    
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            subGrid: false,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 38,
                    periodo: $('#periodo').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {

            }
            ,
            gridComplete: function () {
                var cant = jQuery("#tabla_estadoCuenta").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var negocio = $("#tabla_estadoCuenta").getRowData(cant[i]).negocio;
                        form = "<img src='/fintra/images/botones/iconos/pdf.png' align='center' style='height:30px;width:27px;margin-left: 8px;padding:5px' value='PDF' title='Generar PDF' onclick=\"exportarpdf('" + negocio + "');\" />";
                        jQuery("#tabla_estadoCuenta").jqGrid('setRowData', cant[i], {acciones: form});
                }

            },
//            ondblClickRow: function (rowid, iRow, iCol, e) {
//            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
            caption: "Generar PDF",
            onClickButton: function () {
                generarPDF();
            }
        });
    }


}function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Garantias&accion=Comunitarias",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                periodo: $('#periodo').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function exportarpdf(negocio) {
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Garantias&accion=Comunitarias&negocio="+negocio,
        data: {
            opcion: 39
        },
        success: function (json) {
            alert(json);
            console.log (json.respuesta);
            if (json.respuesta == "GENERADO") {
                mensajesDelSistema("Exito en la generacion de pdf, por favor dirigirse al log de descargas", '335', '150', true);
            }
        }
    });
}


function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}