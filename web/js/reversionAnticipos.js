/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    loadUserSelect();
    $('#salir_reversion').click(function () {
        $('#div_detalle_anticipos').fadeOut('slow');
        return false;
    });

    var f = new Date();
    var anio = f.getFullYear();
    var mes = f.getMonth();
    var dia = f.getDate();
    
    mes = ((mes < 9)?"0":"")+(mes+1);
    
    if(dia < 10){
        dia = "0"+dia;
    }
    
    $('#fecha').val(anio + "-" + mes + "-" + dia);


});

function buscarPlanillasAnticipo() {
    var user = $('#user').val();
    var fecha = $('#fecha').val();
    if (user == "") {
        alert("Debe digitar el nombre del usuario de transferencia de las planillas");
    } else {
        if ($("#gview_grid_planillasAnticipo").length) {
            reloadGridPlanillasAnticipo(user, fecha);
        } else {
            jQuery("#grid_planillasAnticipo").jqGrid({
                caption: "Planillas Anticipo",
                url: "./controller?estado=Corregir&accion=Planilla&opcion=9&user=" + user + "&fecha=" + fecha,
                mtype: "POST",
                datatype: "json",
                height: '400',
                width: 'auto',
                colNames: ['Usuario', 'Transferido', 'Fecha Transf.', 'Banco Transf.', 'Vr. Lote', 'Num. Planillas'],
                colModel: [
                    {name: 'user_transferencia', index: 'user_transferencia', width: 150, align: 'center'},
                    {name: 'transferido', index: 'transferido', width: 100, align: 'center'},
                    {name: 'fecha_transferencia', index: 'fecha_transferencia', width: 200, align: 'center', key: true},
                    {name: 'banco_transferencia', index: 'banco_transferencia', editable: true, width: 200, align: 'center'},
                    {name: 'vlr', index: 'vlr', editable: true, sortable: true, width: 150, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'secuencia', index: 'secuencia', editable: true, width: 100, align: 'center'}

                ],
                rowNum: 20,
                rowTotal: 1000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                rownumbers: false,
                userDataOnFooter: true,
                jsonReader: {
                    root: "rows",
                    repeatitems: false,
                    id: "0"
                },
                ondblClickRow: function (fecha_transferencia) {
                    $('#fechaTransf').val(fecha_transferencia);
                    cargarDetallePlanilla(fecha_transferencia);

                },
                loadError: function (xhr, status, error) {
                    mensajesDelSistema(error, 250, 200);
                }
            });
        }
    }
}

function reloadGridPlanillasAnticipo(user, fecha) {
    var url = "./controller?estado=Corregir&accion=Planilla&opcion=9&user=" + user + "&fecha=" + fecha;

    jQuery("#grid_planillasAnticipo").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#grid_planillasAnticipo').trigger("reloadGrid");
}

function cargarDetallePlanilla(fecha_transferencia) {
    //$('#div_detalle_anticipos').fadeIn();
    divEspera("Espere un momento por favor...", "270", "130");
    var user = $('#user').val();
    if ($("#gview_detalle_anticipos").length) {
        reloadGridDetallePlanilla(user, fecha_transferencia);
    } else {
        jQuery("#detalle_anticipos").jqGrid({
            caption: "Detalle Planilla Anticipo",
            url: "./controller?estado=Corregir&accion=Planilla&opcion=10&user=" + user + "&fecha_transf=" + fecha_transferencia,
            mtype: "POST",
            datatype: "json",
            height: '464',
            width: 'auto',
            colNames: ['Id Planilla', 'Id Propietario', 'Nombre Cuenta', 'Cuenta', 'Vr. Consignacion', 'Banco Transf.', 'Usuario', 'Transferido', 'Fecha Transf.'],
            colModel: [
                {name: 'id', index: 'id', width: 100, align: 'center', key: true},
                {name: 'pla_owner', index: 'pla_owner', width: 100, align: 'center'},
                {name: 'nombre_cuenta', index: 'nombre_cuenta', width: 250, align: 'left'},
                {name: 'cuenta', index: 'cuenta', width: 100, align: 'center'},
                {name: 'vlrConsignar', index: 'vlrConsignar', editable: true, sortable: true, width: 120, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'banco_transferencia', index: 'banco_transferencia', width: 100, align: 'center'},
                {name: 'user_transferencia', index: 'user_transferencia', width: 150, align: 'center'},
                {name: 'transferido', index: 'transferido', width: 100, align: 'center'},
                {name: 'fecha_transferencia', index: 'fecha_transferencia', width: 200, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            rownumbers: true,
            footerrow: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                AbrirDetalleAnticipo();
                cacularTotalesAnticipos(jQuery("#detalle_anticipos"));
                $("#dialogo2").dialog("close");
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });
    }
}


function  cacularTotalesAnticipos(grid_detalle_anticipos) {

    var valor = grid_detalle_anticipos.jqGrid('getCol', 'vlrConsignar', false, 'sum');

    grid_detalle_anticipos.jqGrid('footerData', 'set', {
        cuenta: 'Total:',
        vlrConsignar: valor

    });

}

function reloadGridDetallePlanilla(user, fecha_transferencia) {
    var url = "./controller?estado=Corregir&accion=Planilla&opcion=10&user=" + user + "&fecha_transf=" + fecha_transferencia;

    jQuery("#detalle_anticipos").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#detalle_anticipos').trigger("reloadGrid");
}

function reversarPlanillas() {
    var fecha = $('#fechaTransf').val();
    var user = jQuery("#user").val();
    var fechaHoy = $('#fecha').val();
    $.ajax({
        url: './controller?estado=Corregir&accion=Planilla',
        datatype: 'json',
        type: 'post',
        data: {
            opcion: 11,
            user: user,
            fecha: fecha
        },
        success: function (data) {

            confirmarReversion2("Planillas reversadas con �xito", user, fechaHoy);

        }
    });
}



function confirmarReversion() {
    var msj = "Desea reversas todas las planillas?";
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: 300,
        height: 150,
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Si": function () {
                reversarPlanillas();
            }
            , "No": function () {
                $(this).dialog("close");
            }
        }
    });

}

function mensajes(msj, width, height) {
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'>X</span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });

}

function AbrirDetalleAnticipo() {

    $("#div_detalle_anticipo").dialog({
        width: 1320,
        height: 680,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Reversar": function () {
                confirmarReversion();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });

}

function convertirMayus() {
    var dato = $('#user').val();
    $('#user').val(dato.toUpperCase);
}

function divEspera(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();
}


function confirmarReversion2(msj, user, fechaHoy) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: 300,
        height: 150,
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Salir": function () {
                $("#div_detalle_anticipo").dialog("close");
                reloadGridPlanillasAnticipo(user, fechaHoy);
                $(this).dialog("close");
            }
        }
    });

}

function loadUserSelect() {

    $.ajax({
        type: 'POST',
        url: './controller?estado=Corregir&accion=Planilla',
        dataType: 'json',
        data: {
            opcion: 12
        },
        success: function(json) {
 
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#user').append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#user').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    alert('error : ' + key + '>' + json[key][key]);
                }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}




