/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    var f = new Date();
    var anio = f.getFullYear();
    var mes = f.getMonth();
    var dia = f.getDate();

    mes = ((mes < 9) ? "0" : "") + (mes + 1);

    if (dia < 10) {
        dia = "0" + dia;
    }

    $('#fecha').val(anio + "-" + mes + "-" + dia);

    $("#tabs").tabs({
        show: function (event, ui) {
            $("#container").html("");
        }
    });

    $("#buscar").click(function () {
        buscarLoteReversar($("#transportadora").val());
    });

    $("#reversar").click(function () {
        reversar_lote();
    });

    buscarTransportadoras();
});

function buscarLoteReversar(id_transportadora) {
    var grid_tbl_reversar_lote = jQuery("#tbl_reversar_lote");

    grid_tbl_reversar_lote.jqGrid({
        caption: "Reversar Lote",
        url: "./controller?estado=Administracion&accion=Logistica",
        mtype: "POST",
        datatype: "json",
        height: '300',
        width: '852',
        colNames: ['Usuario Aprobacion', 'Transferido', '# lote', 'Fecha Transferencia', '# Planillas', 'Valor Planillas'],
        colModel: [
            {name: 'usuario_aprobacion', index: 'usuario_aprobacion', width: 130, align: 'center'},
            {name: 'transferido', index: 'transferido', width: 80, align: 'center', },
            {name: 'nro_lote', index: 'nro_lote', sortable: true, width: 80, align: 'center', key: true},
            {name: 'fecha_transferencia', index: 'fecha_transferencia', sortable: true, width: 195, align: 'center'},
            {name: 'nro_planillas', index: 'nro_planillas', sortable: false, width: 120, align: 'center'},           
            {name: 'valor_planillas', index: 'valor_planillas', sortable: true, width: 140, align: 'right', search: false, sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0}},
            
        ],
        rowNum: 10000,
        rowTotal: 10000000,
        pager: ('#page_reversar_lote'),
        loadonce: true,
        rownumWidth: 30,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: false,
        footerrow: true,
        rownumbers: true,
        multiselect: true,
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        }, ajaxGridOptions: {
            async: false,
            data: {
                opcion: 36,//36 BUSCAR LOTE 11 pueba
                id_transportadora: id_transportadora
            }
        },
        loadComplete: function () {
            cacularTotalesAprobacion(grid_tbl_reversar_lote);
            if (grid_tbl_reversar_lote.jqGrid('getGridParam', 'records') <= 0) {
                mensajesDelSistema("No se encontraron resultados para los parametros seleccionados.", "270", "170");

            } else {
                $('#apr').css('display', 'block');
            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 200);
        }
    }).navGrid("#page_reversar_lote", {add: false, edit: false, del: false, search: false, refresh: false}, {});



}

function cacularTotalesAprobacion(grid_tbl_reversar_lote) {

    var valor_planillas = grid_tbl_reversar_lote.jqGrid('getCol', 'valor_planillas', false, 'sum');

    grid_tbl_reversar_lote.jqGrid('footerData', 'set', {
        fecha_anticipo: 'Totales',
        valor_planillas: valor_planillas
    });

}

function buscarTransportadoras() {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        async: false,
        data: {
            opcion: 1
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                try {
                    $('#transportadora').empty();
                    $('#transportadora2').empty();

                    for (var key in json) {
                        $('#transportadora').append('<option value=' + json[key].id + '>' + json[key].razon_social + '</option>');
                        $('#transportadora2').append('<option value=' + json[key].id + '>' + json[key].razon_social + '</option>');

                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

//function buscarTCuetasTercero() {
//
//    $.ajax({
//        type: 'POST',
//        url: "./controller?estado=Administracion&accion=Logistica",
//        dataType: 'json',
//        async: false,
//        data: {
//            opcion: 16
//        },
//        success: function (json) {
//
//            if (!isEmptyJSON(json)) {
//
//                try {
//                    $('#banco_tercero').empty();
//                    $('#banco_tercero').append("<option value=''>Seleccione</option>");
//                    console.log(json);
//                    for (var key in json) {
//                        if (json[key].codigo === 'BANCOLOMBIA PAB CTA CORRIENTE')
//                            $('#banco_tercero').append('<option value=' + json[key].descripcion + '>' + json[key].codigo + '</option>');
//                    }
//
//                } catch (exception) {
//                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
//                }
//
//            } else {
//
//                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');
//
//            }
//        },
//        error: function (xhr, ajaxOptions, thrownError) {
//            alert("Error: " + xhr.status + "\n" +
//                    "Message: " + xhr.statusText + "\n" +
//                    "Response: " + xhr.responseText + "\n" + thrownError);
//        }
//    });
//
//}

//function cargarPagina(pagina) {
//    $.ajax({
//        type: 'POST',
//        url: "" + pagina,
//        dataType: 'html',
//        success: function (html) {
//            $("#container").html(html);
//        },
//        error: function (xhr, ajaxOptions, thrownError) {
//            alert("Error: " + xhr.status + "\n" +
//                    "Message: " + xhr.statusText + "\n" +
//                    "Response: " + xhr.responseText + "\n" + thrownError);
//        }
//    });
//}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");

            }
        }
    });
}

function reversar_lote() {

    var rowId = $("#tbl_reversar_lote").jqGrid('getGridParam', 'selarrrow');
    if (rowId.length === 1) {
        var txt;
        var r = confirm("�En realidad desea reversar el lote?");
        if (r === true) {
            txt = 'Empezo la reversion!';
            loading("Espere un momento estamos reversando el Lote.", "320", "140");
            var arrayJson = [];
            for (var i = 0; i < rowId.length; i++) {
                var rowData = jQuery("#tbl_reversar_lote").getRowData(rowId[i]);
                arrayJson.push(rowData);
            }

            var myJsonString = JSON.stringify(arrayJson);
            console.log(myJsonString);

            $.ajax({
                type: 'POST',
                url: "./controller?estado=Administracion&accion=Logistica",
                dataType: 'json',
                data: {
                    opcion: 35,
                    datajson: myJsonString,
                },
                success: function (json) {

                    if (!isEmptyJSON(json)) {

                        $("#dialogo2").dialog('close');

                        if (json.respuesta === 'OK') {
                            $("#tbl_reversar_lote").clearGridData();
                            mensajesDelSistema('Reversion Exitosa.!', '300', '150');

                        } else {
                            mensajesDelSistema(json.respuesta, '320', '150');
                        }

                    } else {

                        mensajesDelSistema("Lo sentimos no pudimos reversar el Lote!!", '250', '150');
                        $("#dialogo2").dialog('close');

                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        } else {
            mensajesDelSistema('Operacion Cancelada.', '300', '150');
        }
    } else {
        mensajesDelSistema('Debes seleccionar un solo lote para reversar.', '300', '150');
    }



}

//function reloadGridAnticiposTrans() {
//    jQuery("#tbl_reversar_lote").setGridParam({
//        datatype: 'json',
//        url: "./controller?estado=Administracion&accion=Logistica",
//        ajaxGridOptions: {
//            async: false,
//            data: {
//                opcion: 17,
//                id_transportadora: $("#transportadora2").val(),
//                banco: $("#banco_tercero").val()
//            }
//        }
//    });
//
//    jQuery("#tbl_reversar_lote").trigger("reloadGrid");
//}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();
}

