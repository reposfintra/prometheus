/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    cargarCuotaManejo();
});

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function cargarCuotaManejo() {
    var operacion;
    var grid_tabla_ = jQuery("#tabla_cuota_manejo");
    if ($("#gview_tabla_cuota_manejo").length) {
        reloadGridMostrar(grid_tabla_, 87);
    } else {
        grid_tabla_.jqGrid({
            caption: "Cuota Administracion",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '763',
            colNames: ['Id', 'Estado', 'Id convenio', 'Convenio', 'Tipo cal', 'Tipo calculo', 'Descripcion', 'Valor', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'id_convenio', index: 'id_convenio', width: 120, sortable: true, align: 'center', hidden: true},
                {name: 'nombre', index: 'nombre', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'tipo_calculo', index: 'id_convenio', width: 120, sortable: true, align: 'center', hidden: true},
                {name: 'tipo_calculo_', index: 'id_convenio', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 250, sortable: true, align: 'left', hidden: false},
                {name: 'valor', index: 'valor', sortable: true, width: 120, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'cambio', index: 'cambio', width: 120, sortable: true, align: 'left', hidden: false}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 87
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_cuota_manejo").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_cuota_manejo").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoCuotaManejo('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_cuota_manejo").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_cuota_manejo"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var nombre = filas.nombre;
                var tipo_calculo_ = filas.tipo_calculo_;
                var descripcion = filas.descripcion;
                var valor = filas.valor;
                if (estado === 'Activo') {
                    ventanaCuotaManejo(operacion, id, nombre, tipo_calculo_, descripcion, valor);
                } else if (estado === 'Inactivo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function CambiarEstadoCuotaManejo(rowid) {
    var grid_tabla = jQuery("#tabla_cuota_manejo");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 88,
            id: id
        },
        success: function (data) {
            cargarCuotaManejo();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function ventanaCuotaManejo(operacion, id, nombre, tipo_calculo_, descripcion, valor) {
    if (operacion === 'Editar') {
        $("#id").val(id);
        $("#convenio").val(nombre);
        $("#tipocalculo_").val(tipo_calculo_);
        $("#descripcion").val(descripcion);
        $("#valor").val(numberConComas(valor));

        $("#dialogsalarioCuota").dialog({
            width: '1312',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'CUOTA ADMINISTRACION',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarCuotaManejo();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#convenio").val('');
                    $("#tipocalculo_").val('');
                    $("#descripcion").val('');
                    $("#valor").val('');
                }
            }
        });
    }
}

function ActualizarCuotaManejo() {
    var valor_cuota = $("#valor").val();
    if (valor_cuota !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 89,
                id: $("#id").val(),
                valor: numberSinComas( $("#valor").val())
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    $("#id").val('');
                    $("#convenio").val('');
                    $("#tipocalculo_").val('');
                    $("#descripcion").val('');
                    $("#valor").val('');
                    $("#dialogsalarioCuota").dialog("close");
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                }
                cargarCuotaManejo();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}