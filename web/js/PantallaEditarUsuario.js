var claveVieja;

function tipoUsuarioChange()
{
  document.userManagerFrm.submit();
}

function justDigitsKeyPress()
{
  var isDigit = true;
  if( event.keyCode < 48 || event.keyCode > 57 )
    isDigit = false;
  return isDigit;
}
function email(){
	
			var frm = document.userManagerFrm;
			if  (frm.emailUsuario.value != '') {
				 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(frm.emailUsuario.value) ){
					 /*var e = frm.emailUsuario.value;
					 if(frm.tipoUsuario.value=='ADMIN'||frm.tipoUsuario.value=='TSPUSER'){
					    
						 if(e.indexOf('mail.tsp.com')==-1){
							 alert("Favor digitar correo de interno de la empresa '@mail.tsp.com' "); 
							 frm.emailUsuario.value=''; 
							 return (false);
						 }	
					 }*/
					 return(true);
				 }
				 else{
					 alert("La direcci�n de email es incorrecta.");
					 frm.emailUsuario.value=''; 
					 return (false);
				 }
			 }   
			 
		}
function saveUserDataBtnClick()
{
	email();	
  var frm = document.userManagerFrm;
  if( frm.nombreUsuario.value == "" )  {
    alert("El nombre del usuario no puede quedar vac�o.\nFavor rectificar.");
    return false;
  }else if( frm.nitUsuario.value == "" )  {
    alert("El nit del usuario no puede quedar vac�o.\nFavor rectificar.");
    return false;
  }else if( frm.direccionUsuario.value == "" ){
    alert("La direcci�n del usuario no puede quedar vac�a.\nFavor rectificar.");
    return false;
  }else if( frm.ciudadUsuario.value == "" ){
    alert("La ciudad de origen del usuario no puede quedar vac�a.\nFavor rectificar.");
    return false;
  }
  else if( frm.perfil.value == "" ){
    alert("El usuario debe tener por lo menos un perfil.\nFavor rectificar.");
    return false;
  }
  else if( frm.emailUsuario.value == "" ){
    alert("La dir. electr�nica del usuario no puede quedar vac�a.\nFavor rectificar.");
    return false;
  }else if( frm.telefonoUsuario.value == "" ){
    alert("El tel�fono del usuario no puede quedar vac�o.\nFavor rectificar.");
    return false;
  }else if( frm.claveUsuario.value == "" ){
    alert("La clave del usuario no puede quedar vac�a.\nFavor rectificar.");
    return false;
  }
  if( frm.estadoUsuario.value == "R" ){
      var msg = "Esta acci�n eliminar� este usuario DEFINITIVAMENTE.\n" +
                "�Desea continuar?";
      if( confirm(msg) ){
        frm.cmd.value = "save";
        frm.submit();
      }
    }
    else{
      if( claveVieja != frm.claveUsuario.value ){
        frm.claveCambio.value = "S";
      }
      frm.cmd.value = "save";
      frm.submit();
    }
}