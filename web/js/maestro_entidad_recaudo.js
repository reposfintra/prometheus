/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    buscar_entidades_recaudo();
});

function buscar_entidades_recaudo() {
    var grid_tabla = $("#tabla_entidad_recaudo");
    if ($("#gview_tabla_entidad_recaudo").length) {
        reloadGridTabla(grid_tabla, 15);
    } else {
        grid_tabla.jqGrid({
            caption: "Modificar Valor comisi�n",
            url: "./controller?estado=Archivo&accion=Asobancaria",
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '930',
            colNames: ['Codigo Entidad', 'Entidad','Codigo Canal','Canal','Valor Comisi�n','Procentaje Iva','Total Comisi�n','Seleccionar'],
            colModel: [
                {name: 'codigo_entidad', index: 'codigo_entidad', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 200, sortable: true, align: 'center', hidden: false},
                {name: 'codigo_canal', index: 'codigo_canal', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'nombre_canal', index: 'nombre_canal', width: 180, sortable: true, align: 'center', hidden: false},
                {name: 'valor_comision_recaudo', index: 'valor_comision_recaudo', width: 80, sortable: true, align: 'center', hidden: false, search: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'porc_iva', index: 'porc_iva', width: 80, sortable: true, align: 'center', hidden: false, search: false},               
                {name: 'total_comision', index: 'total_comision', width: 80, sortable: true, align: 'center', hidden: false, search: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, }},
                {name: 'select', index: 'select', width: 60, sortable: true, align: 'center', hidden: false, search: false}
                
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#pager',
            pgtext: null,
            pgbuttons: false,
            multiselect:false, 

            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                if (grid_tabla.jqGrid('getGridParam', 'records') <= 0) {
                    mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150');
                }
            },
            ajaxGridOptions: {

                data: {
                    opcion: 15
                }
            },
            gridComplete: function () {
                var rows = $("#tabla_entidad_recaudo").jqGrid('getDataIDs');

                for (let i = 0; i < rows.length; i++) {
                    let codigo_entidad = $("#tabla_entidad_recaudo").getRowData(rows[i]).codigo_entidad;
                    let entidad = $("#tabla_entidad_recaudo").getRowData(rows[i]).descripcion;
                    let valor_comision_recaudo = $("#tabla_entidad_recaudo").getRowData(rows[i]).valor_comision_recaudo;
                    let porc_iva = $("#tabla_entidad_recaudo").getRowData(rows[i]).porc_iva;
                    let total_comision = $("#tabla_entidad_recaudo").getRowData(rows[i]).total_comision;
                    let codigo_canal = $("#tabla_entidad_recaudo").getRowData(rows[i]).codigo_canal;
                    let btn;
                    
                    btn = "<img src='/fintra/images/botones/iconos/editDoc2.png' align='center' style='height:15px;width:15px;margin-left: 4px;padding:3px; cursor: pointer;' value='Editar' title='Editar' onclick=\"editarEntidad('" + codigo_entidad + "','" + entidad + "','" + valor_comision_recaudo + "','" + porc_iva + "','" + total_comision + "','" + codigo_canal + "');\" />";
                    $("#tabla_entidad_recaudo").jqGrid('setRowData', rows[i], {select: btn});
                }

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });

    }

}


function reloadGridTabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Archivo&accion=Asobancaria",

        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op
            }
        }
    });
    grid_tabla.trigger("reloadGrid");

}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {

                $(this).dialog("close");
            }
        }
    });

}


function editarEntidad(codigo_entidad,entidad,valor_comision_recaudo,porc_iva,total_comision,codigo_canal) {
    $("#codigo_entidad").text(codigo_entidad);
    $("#entidad").text(entidad);
    $("#valor_comision").val(valor_comision_recaudo);    
    $("#porcentaje_iva").val(porc_iva);    
    $("#valor_total_comision").val(total_comision);    
    $("#codigo_canal").val(codigo_canal);    
    $("#div_modificarComision").dialog({
        modal: true,
        width: '355',
        height: '220',
        show: "scale",
        hide: "scale",
        resizable: true,
        title: 'Editar Comisi�n',
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Actualizar": function () {
                actualizarComisi�n($("#codigo_entidad").text(),$("#valor_comision").val(),$("#porcentaje_iva").val(),$("#valor_total_comision").val(),$("#codigo_canal").val());
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
  }
  
  function actualizarComisi�n(codigo_entidad,valor_comision,porcentaje_iva,valor_total_comision,codigo_canal) {
     if(valor===""){
        mensajesDelSistema("Por favor ingrese valor de comisi�n", '250', '150');
        return;
     } 
    loading("Espere un momento por favor...", "270", "140");
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Archivo&accion=Asobancaria",
        dataType: 'json',
        data: {
            opcion: 16,
            codigo_entidad:codigo_entidad,
            valor_comision:valor_comision,
            porcentaje_iva:porcentaje_iva,
            valor_total_comision:valor_total_comision,
            codigo_canal:codigo_canal
        },
        success: function (json) {
            $("#dialogLoading").dialog('close');
            if (json.respuesta === "OK") {
                mensajesDelSistema("Se actualizo valor comisi�n con �xito", '250', '150');
                $('#div_modificarComision').dialog('close');
                
                 buscar_entidades_recaudo();
            } else {
                mensajesDelSistema(json.data, '250', '150');
            }

        },
        error: function (xhr) {
            $("#dialogLoading").dialog('close');
            mensajesDelSistema("Error al refinanciar " + "\n" +
                    xhr.responseText, '350', '180', true);
        }
    });

}


function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function calcularValorTotal() {
    let valor_comision =$("#valor_comision").val();
    let porcentaje_iva =$("#porcentaje_iva").val();
    let valor_iva= valor_comision * porcentaje_iva /100; 
    let total_comision=Math.round(Number(valor_comision)+Number(valor_iva));
    $("#valor_total_comision").val(total_comision);
    
}

