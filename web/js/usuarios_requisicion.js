/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    listarUsuariosRelProInterno();
    if ($('#div_procesos').is(':visible')) {
        listarRelProcesosUsuario();
    } else {
        listarRelTiposRequisicionUsuario();
    }
});

function listarUsuariosRelProInterno(){   
    
    if ($("#gview_listUsuarios").length) {
        reloadGridUsuariosRelProInterno();
    } else {             
            jQuery("#listUsuarios").jqGrid({
                caption: 'Listado de usuarios',
                url: './controller?estado=Procesos&accion=Meta&opcion=28',
                datatype: 'json',
                height: 368,
                width: 'auto',
                colNames: ['Cod Usuario', 'Usuario Login'],
                colModel: [
                    {name: 'codUsuario', index: 'codUsuario', hidden:true, align: 'center', width: '100px', key: true},                 
                    {name: 'idusuario', index: 'idusuario', align: 'center', width: '200px'}
                ],
                rowNum: 1000,
                rowTotal: 50000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                ignoreCase: true,              
                jsonReader: {
                    root: 'rows',
                    repeatitems: false,
                    id: '0'
                },                
                ajaxGridOptions: {
                    async: false
                },
                ondblClickRow: function(rowid) {
                    if ($('#div_procesos').is(':visible')) {
                        listarRelProcesosUsuario(rowid);
                    }else{
                        listarRelTiposRequisicionUsuario(rowid);
                    }
                },
                loadError: function(xhr, status, error) {
                    mensajesDelSistema(error, 250, 150);
                }
            });
            
            jQuery("#listUsuarios").jqGrid('filterToolbar',
                    {
                        autosearch: true,
                        searchOnEnter: false,
                        defaultSearch: "cn",
                        stringResult: true,
                        ignoreCase: true,
                        multipleSearch: true
            });              
       
    }
}

function reloadGridUsuariosRelProInterno(){
    var url = './controller?estado=Procesos&accion=Meta';
    jQuery("#listUsuarios").setGridParam({       
        url: url,
        datatype: 'json',
        ajaxGridOptions: {
            async:false,
            type: "POST",
            data: {
                opcion:28                
            }
        }
    });
    
    jQuery("#listUsuarios").jqGrid('filterToolbar',
            {
                autosearch: true,
                searchOnEnter: false,
                defaultSearch: "cn",
                stringResult: true,
                ignoreCase: true,
                multipleSearch: true
    });
                
    jQuery('#listUsuarios').trigger("reloadGrid");
}


function listarRelProcesosUsuario(usuario){   
    var usuario_proceso = (typeof usuario !== 'undefined') ? usuario: "";
    if ($("#gview_listProcesosUsuario").length) {
        reloadGridRelProcesosUsuario(usuario_proceso);
    } else {             
            jQuery("#listProcesosUsuario").jqGrid({
                caption: 'Listado de procesos',
                url: './controller?estado=Procesos&accion=Meta',
                datatype: 'json',
                height: 368,
                width: 'auto',
                colNames: ['Id', 'Proceso', 'Estado'],
                colModel: [
                    {name: 'id', index: 'id', hidden:true, align: 'center', width: '100px', key: true},                 
                    {name: 'descripcion', index: 'descripcion', align: 'left', width: '375px'},
                    {name: 'tipo', index: 'tipo',  hidden:true, align: 'left', width: '80px'}
                ],
                rowNum: 1000,
                rowTotal: 50000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                ignoreCase: true,
                pgtext: null,
                pgbuttons: false,
                multiselect:true,
                pager: '#page_tabla_procesos_usuario',
                jsonReader: {
                    root: 'rows',
                    repeatitems: false,
                    id: '0'
                },
                ajaxGridOptions: {
                    async: false,
                    dataType: "json",
                    type: "POST",
                    data: {
                          opcion:29,
                          usuario_proc: usuario_proceso
                    }
                },    
                gridComplete: function () {
                    var ids = jQuery("#listProcesosUsuario").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        var Estado = jQuery('#listProcesosUsuario').getRowData(cl).tipo;
                        if(Estado === 'A') jQuery("#listProcesosUsuario").jqGrid('setSelection', cl, false);
                    }                    
                },
                loadError: function(xhr, status, error) {
                    mensajesDelSistema(error, 250, 150);
                }
            }).navGrid("#page_tabla_procesos_usuario", {edit: false, add: false, del: false});
            jQuery("#listProcesosUsuario").jqGrid("navButtonAdd", "#page_tabla_procesos_usuario", {
                caption: "Guardar",
                title: "Guarda la configuración de procesos para el usuario seleccionado",
                onClickButton: function() {
                    var usuario_sel = jQuery("#listUsuarios").jqGrid('getGridParam', 'selrow');
                    if (usuario_sel !== null){
                        asignarProcesosUsuario(usuario_sel);
                    }else{
                        mensajesDelSistema('Por favor, seleccione el usuario correspondiente', 250, 150);
                    }            
                }
            });          
       
    }
}

function reloadGridRelProcesosUsuario(usuario_proceso){
    var url = './controller?estado=Procesos&accion=Meta';
    jQuery("#listProcesosUsuario").setGridParam({        
        url: url,
        datatype: 'json',
        ajaxGridOptions: {
            async:false,
            type: "POST",
            data: {
                opcion:29,
                usuario_proc: usuario_proceso               
            }
        }
    });
     
    jQuery('#listProcesosUsuario').trigger("reloadGrid");
}


function listarRelTiposRequisicionUsuario(usuario){   
    var usuario_proceso = (typeof usuario !== 'undefined') ? usuario: "";
    if ($("#gview_listTiposReqUsuario").length) {
        reloadGridRelTiposRequisicionUsuario(usuario_proceso);
    } else {             
            jQuery("#listTiposReqUsuario").jqGrid({
                caption: 'Tipos de prioridad Requisicion',
                url: './controller?estado=Procesos&accion=Meta',  
                datatype: 'json',
                height: 368,
                width: 'auto',
                colNames: ['Id', 'Tipo Requisicion','Estado'],
                colModel: [
                    {name: 'id', index: 'id', hidden:true, align: 'center', width: '100px', key: true},                 
                    {name: 'descripcion', index: 'descripcion', align: 'left', width: '280px'},
                    {name: 'tipo', index: 'tipo',  hidden:true, align: 'left', width: '80px'}
                ],
                rowNum: 1000,
                rowTotal: 50000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                ignoreCase: true, 
                pgtext: null,
                pgbuttons: false,
                multiselect:true,
                pager: '#page_tabla_tipo_req_usuario',
                jsonReader: {
                    root: 'rows',
                    repeatitems: false,
                    id: '0'
                },
                ajaxGridOptions: {
                    async: false,
                    dataType: "json",
                    type: "POST",
                    data: {
                          opcion:30,
                          usuario_proc: usuario_proceso
                    }
                },      
                gridComplete: function () {
                    var ids = jQuery("#listTiposReqUsuario").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        var Estado = jQuery('#listTiposReqUsuario').getRowData(cl).tipo;
                        if(Estado === 'A') jQuery("#listTiposReqUsuario").jqGrid('setSelection', cl, false);
                    }                    
                },
                loadError: function(xhr, status, error) {
                    mensajesDelSistema(error, 250, 150);
                }
            }).navGrid("#page_tabla_tipo_req_usuario", {edit: false, add: false, del: false});
            jQuery("#listTiposReqUsuario").jqGrid("navButtonAdd", "#page_tabla_tipo_req_usuario", {
                caption: "Guardar",
                title: "Guarda la configuración de tipos de requisición para el usuario seleccionado",
                onClickButton: function() {
                    var usuario_sel = jQuery("#listUsuarios").jqGrid('getGridParam', 'selrow');
                    if (usuario_sel !== null){
                        asignarTiposReqUsuario(usuario_sel);
                    }else{
                        mensajesDelSistema('Por favor, seleccione el usuario correspondiente', 250, 150);
                    }                   
                }
            });     
       
    }
}

function reloadGridRelTiposRequisicionUsuario(usuario_proceso){
    var url = './controller?estado=Procesos&accion=Meta';
    jQuery("#listTiposReqUsuario").setGridParam({       
        url: url,
        datatype: 'json',
        ajaxGridOptions: {
            async: false,
            type: "POST",
            data: {
                opcion: 30,
                usuario_proc: usuario_proceso
            }
        }
    });
     
    jQuery('#listTiposReqUsuario').trigger("reloadGrid");
}

function asignarProcesosUsuario(iduser){   
    var listado = "";
    var login = $("#listUsuarios").getRowData(iduser).idusuario;
    var filasId =jQuery('#listProcesosUsuario').jqGrid('getGridParam', 'selarrrow');
    if (jQuery("#listProcesosUsuario").jqGrid('getGridParam', 'records')>0) {
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        
        var url = './controller?estado=Procesos&accion=Meta';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 31,
                listado: listado,
                id_usuario: iduser,
                login: login 
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {                       
                        reloadGridRelProcesosUsuario(iduser);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asignar los procesos al usuario!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            }      
        });
        
    }else{       
            mensajesDelSistema("No hay procesos por asignar", '250', '150'); 
    }
}

function asignarTiposReqUsuario(iduser){   
    var listado = "";
    var login = $("#listUsuarios").getRowData(iduser).idusuario;
    var filasId =jQuery('#listTiposReqUsuario').jqGrid('getGridParam', 'selarrrow');
    if (jQuery("#listTiposReqUsuario").jqGrid('getGridParam', 'records')>0) {
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        
        var url = './controller?estado=Procesos&accion=Meta';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 32,
                listado: listado,
                id_usuario: iduser,
                login: login 
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {                       
                        reloadGridRelTiposRequisicionUsuario(iduser);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asignar los tipos de requisicion al usuario!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            }      
        });
        
    }else{       
         mensajesDelSistema("No hay tipos de requisicion por asignar", '250', '150');
    }
}

function mensajesDelSistema(msj, width, height, swHideDialog) {   
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsg").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear botón cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}
