/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $("#buscar").click(function () {
        buscarTitulosGrid();
        var x = document.getElementById("sub").selectedIndex;
        var y = document.getElementById("sub").options;
        $('#nomsub').val(y[x].text);
    });
    cargarComboTipoInsumo();
    cargarComboCategoria();
    //cargarCombo('sub', $('#categoria option:selected').val());
    
    maximizarventana();


});

function cargarComboTipoInsumo() {
    $('#insumo').html('');

    $.ajax({
        type: 'POST',
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        dataType: 'json',
        async: false,
        data: {
            opcion: 60
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#insumo').append("<option value=''>...</option>");
                    
                    for (var key in json) {
                        $('#insumo').append('<option value=' + key + '>' + json[key] + '</option>');
                    }
                    ordenarCombo('insumo');
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    

}

function cargarComboCategoria() {
    $('#categoria').html('');
    $('#categoria').append("<option value=''>...</option>");
    $('#sub').html('');
    $('#sub').append("<option value=''>...</option>");
    $.ajax({
        type: 'POST',
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        dataType: 'json',
        async: false,
        data: {
            opcion: 61,
            tipo_insumo: $('#insumo option:selected').val()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#categoria').html('');
                    $('#categoria').append("<option value=''>...</option>");
                    for (var key in json) {
                        $('#categoria').append('<option value=' + key + '>' + json[key] + '</option>');
                    }
                    ordenarCombo('categoria');

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}



function cargarCombo(id, filtro) {
    var elemento = $('#' + id)
            , sql = (id === 'categoria') ? 'categorias' : 'subcategoriaxcategoria';
    //$("#lui_tabla_productos,#load_tabla_productos").show();
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'get',
        data: {opcion: 40, informacion: JSON.stringify({query: sql, filtros: filtro}),
        },
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');

                    }
                    ordenarCombo(id);
                }
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });

}



function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function buscarTitulosGrid() {
    var subcategoria = $('#sub').val(), idesp = "0", idum = "0", nombre = "";
    //  $('#tbl_ingreso_catalogo').jqGrid("clearGridData");
    $('#tbl_ingreso_catalogo').jqGrid('GridUnload');
    //   $('#tbl_ingreso_catalogo').remove();
    //$('#gridContainer').empty();
    $.ajax({
        type: 'POST',
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        dataType: 'json',
        data: {
            opcion: 36,
            subcategoria: subcategoria
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {
                var arr = [];
                var ColModel1 = [];

                arr.push('Id');
                ColModel1.push({name: 'id', index: 'id', hidden: true, key: true});
                //console.log(json);
                for (var key in json) {

                    arr.push('tipo-' + json[key].tipo);
                    ColModel1.push({name: 'tipo-' + json[key].tipo, index: 'tipo-' + json[key].tipo, hidden: true});

                    arr.push(json[key].nombre);

                    nombre = json[key].nombre;
                    nombre = nombre.replace(/\s+/g, "");

                    if (json[key].tipo === '2') {

                        ColModel1.push({name: 'esp-' + nombre, index: 'esp-' + nombre, align: 'right', width: 100, search: false, editable: true, formatter: 'number',
                            formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""},
                            editoptions: {size: 15, dataInit: function (elem) {
                                    $(elem).bind("keypress", function (e) {
                                        //alert(soloNumeros(e));
                                        return numeros(e)
                                    })
                                }
                            }
                        });

                        arr.push('espid_' + json[key].idesp);
                        ColModel1.push({name: 'espid-' + json[key].idesp, index: 'espid-' + json[key].idesp, hidden: true});

                    } else if (json[key].tipo === '3') {

                        ColModel1.push({name: 'esp-' + nombre, index: 'esp-' + nombre, align: 'left', width: 100, search: false, editable: true, formatter: 'currency',
                            formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$"}});
                        arr.push('espid_' + json[key].idesp);
                        ColModel1.push({name: 'espid-' + json[key].idesp, index: 'espid-' + json[key].idesp, hidden: true});

                    } else if (json[key].tipo === '4') {

                        ColModel1.push({name: 'esp-' + nombre, index: 'esp-' + nombre, align: 'center', width: 100, search: false, editable: true});
                        arr.push('espid_' + json[key].idesp);
                        ColModel1.push({name: 'espid-' + json[key].idesp, index: 'espid-' + json[key].idesp, hidden: true});

                    } else if (json[key].tipo === '0') {

                        ColModel1.push({name: 'esp-' + nombre, index: 'esp-' + nombre, align: 'center', width: 180, search: false, editable: true, edittype: 'select', editrules: {required: true},
                            editoptions: {
                                value: ajaxSelect('valorespredeterminados', json[key].idesp),
                                dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                            try {

                                                //var rowid = e.target.id.replace("_" + 'esp-' + nombre, "");
                                                var rowid = e.target.id.substring(0, e.target.id.indexOf("_esp"));
                                                //alert(rowid);
                                                idesp = e.target.value.replace("_", "");
                                                /*alert("id:"+e.target.id+" rowid:"+rowid+"des: "+nombre);
                                                 alert(" dddd:"+e.target.id.substring(10));
                                                 
                                                 
                                                 alert(e.target.id.substring(0,e.target.id.indexOf("_esp")));*/



                                                jQuery("#tbl_ingreso_catalogo").jqGrid('setCell', rowid, "espidval-" + e.target.id.substring(e.target.id.indexOf("-") + 1), idesp);





                                            } catch (exc) {
                                            }
                                            return;
                                        }}]
                            }


                        });

                        arr.push('espidval-' + nombre);
                        ColModel1.push({name: 'espidval-' + nombre, index: 'espidval-' + nombre, hidden: true});

                        arr.push('espid-' + json[key].idesp);
                        ColModel1.push({name: 'espid-' + json[key].idesp, index: 'espid-' + json[key].idesp, hidden: true});


                    }
                }

                arr.push('DESCRIPCION');
                ColModel1.push({name: 'descripcion', index: 'descripcion', hidden: false, align: 'center', width: 500, search: false, editable: false});

                arr.push('CODIGO GENERADO');
                ColModel1.push({name: 'codigogenerado', index: 'codigogenerado', hidden: false, align: 'center', width: 160, search: false, editable: false});


                arr.push('ANULAR');
                ColModel1.push({name: 'delete', index: 'delete', resizable: false, align: 'center', width: '100'});

                $("#columnas").val(arr.length);
                crearJqGrid(arr, ColModel1);
            } else {
                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ajaxSelect(sql, id) {
    var subcategoria = $('#sub').val();

    var resultado;
    //$("#lui_tabla_productos,#load_tabla_productos").show();
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'get',
        data: {opcion: 40, informacion: JSON.stringify({query: sql, filtros: [subcategoria, id]})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '300', 'auto', false);
                    resultado = {};
                } else {
                    resultado = json;
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            console.log('error');
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
    return resultado;
}

function crearJqGrid(arrNames, arrColModel) {

    var grid_ingreso_catalogo = jQuery("#tbl_ingreso_catalogo");
//    if ($("#fecha_inicio").val() !== '' && $("#fecha_fin").val() !== '') {

    if ($("#gview_tbl_ingreso_catalogo").length) {
        reloadGridIngresoCatalogo(grid_ingreso_catalogo, arrNames, arrColModel);
    } else {

        grid_ingreso_catalogo.jqGrid({
            caption: "Lista de Especificaciones",
            datatype: "json",
            url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
            height: '500',
            width: '1500',
            colNames: arrNames,
            colModel: arrColModel,
            rowNum: 10000,
            rowTotal: 10000000,
            pager: '#page_ingreso_catalogo',
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            reloadAfterSubmit: true,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            ajaxGridOptions: {
                async: false,
                data: {
                    opcion: 47,
                    subcategoria: $('#sub').val()
                }
            },
            gridComplete: function () {
                var ids = grid_ingreso_catalogo.jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular este Registro?','250','150',anularInsumo,'" + cl + "');\">";
                    grid_ingreso_catalogo.jqGrid('setRowData', ids[i], {delete: an});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                grid_ingreso_catalogo.jqGrid('editRow', rowid, true, function () {
                    $("input, select", e.target).focus();
                });
                return;
            },
            restoreAfterError: true
        });
        grid_ingreso_catalogo.navGrid("#page_ingreso_catalogo", {add: false, edit: false, del: false, search: false, refresh: true});
        grid_ingreso_catalogo.navButtonAdd('#page_ingreso_catalogo', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                guardarEsp(arrColModel, arrNames);
            },
            //position: "first"
        });
        grid_ingreso_catalogo.navButtonAdd('#page_ingreso_catalogo', {
            caption: "Nuevo",
            title: "Agregar nueva fila",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {
                var eds = $("#sub").val();
                if (eds === '') {
                    mensajesDelSistema('Debe escoger una categoria o subcategoria', '300', 'auto', false);
                } else {
                    var grid = $("#tbl_ingreso_catalogo")
                            , rowid = 'neo_' + grid.getRowData().length;
                    //alert(rowid);
                    grid.addRowData(rowid, {});
                    grid.jqGrid('editRow', rowid, true, function () {
                        //  $("input, select", e.target).focus();
                    }, null, null, {}, null, null, function (id) {
                        var g = $('#tbl_ingreso_catalogo')
                                , r = g.jqGrid('getLocalRow', id);
                        if (!r.id) {
                            g.jqGrid('delRowData', id);
                        }
                    });
                }
            },
            position: "first"
        });
        grid_ingreso_catalogo.navButtonAdd('#page_ingreso_catalogo', {
            caption: "Exportar Excel",
            onClickButton: function () {
                var info = grid_ingreso_catalogo.getGridParam('records');
                if (info > 0) {
                    exportarExcel();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }

            }
        });
        //grid_ingreso_catalogo.navGrid("#page_ingreso_catalogo", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        //jQuery("#tbl_ingreso_catalogo").jqGrid('inlineNav', "page_ingreso_catalogo");
    }

}

function anularInsumo(id) {
    $.ajax({
        url: '/fintra/controlleropav?estado=Procesos&accion=Catalogo',
        datatype: 'json',
        type: 'post',
        data: {
            opcion: 54,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.respuesta === "SI") {
                    buscarTitulosGrid();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo realizar el proceso!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function  exportarExcel() {
    var fullData = jQuery("#tbl_ingreso_catalogo").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var subcategoria = $('#nomsub').val();
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        data: {
            listado: myJsonString,
            subcategoria: subcategoria,
            opcion: 53
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function guardarEsp(arrColModel, arrColnames) {
    var sub = $('#sub').val(),
            nomsub = $('#nomsub').val()
            , grid = jQuery("#tbl_ingreso_catalogo")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;



    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);
        /*for(var e=0;e<arrColModel.size(); e++){
         
         alert(arrColModel);
         
         }*/

        /*if (data.comision_aplica && data.estado) {
         if ((data.producto) === 'COMBUSTIBLE ACPM') {
         if (parseInt(data.comision_porcentaje) !== 0
         || parseInt(data.comision_valor) !== 0) {
         error = true;
         mensajesDelSistema('Valores de comision deben ser 0', '300', 'auto', false);
         grid.jqGrid('editRow', filas[i], true, function () {
         $("input, select", e.target).focus();
         });
         break;
         }
         } else {
         if ((data.producto) !== 'COMBUSTIBLE ACPM') {
         if ((data.producto) === 'DINERO EN EFECTIVO') {
         if (parseInt(data.comision_porcentaje) !== 0
         || parseInt(data.comision_valor) !== 0) {
         error = true;
         mensajesDelSistema('Valores de comision deben ser 0', '300', 'auto', false);
         grid.jqGrid('editRow', filas[i], true, function () {
         $("input, select", e.target).focus();
         });
         break;
         }
         } else {
         if (parseInt(data.comision_porcentaje) === 0
         && parseInt(data.comision_valor) === 0) {
         error = true;
         mensajesDelSistema('No registro valores de comision', '300', 'auto', false);
         grid.jqGrid('editRow', filas[i], true, function () {
         $("input, select", e.target).focus();
         });
         break;
         } else if (parseInt(data.comision_porcentaje) !== 0
         && parseInt(data.comision_valor) !== 0) {
         error = true;
         mensajesDelSistema('solo puede registrar un valor comision o porcentaje descuento', '300', 'auto', false);
         grid.jqGrid('editRow', filas[i], true, function () {
         $("input, select", e.target).focus();
         });
         }
         }
         
         }
         }
         }*/


        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }
    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 46, informacion: JSON.stringify({arrColmodel: arrColModel, arrColnames: arrColnames, nomsub: nomsub, sub: sub, rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    if (json.mensaje != "Algunos Registros se Encuentran Duplicados...") {
                        buscarTitulosGrid();
                    }
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}

function reloadGridIngresoCatalogo(grid_ingreso_catalogo, arrNames, arrColModel) {
    var subcategoria = $('#sub').val();
    //grid_ingreso_catalogo.trigger('reloadGrid',[{ colNames:arrNames,colModel:arrColModel}]).jqGrid('GridUnload');
    grid_ingreso_catalogo.setGridParam({
        datatype: 'json',
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: 36,
                subcategoria: subcategoria
            }

        }
    }).trigger('reloadGrid', [{colNames: arrNames, colModel: arrColModel}]);
//   
    //sgrid_ingreso_catalogo.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height) {


    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function buscarProveedor() {
    $('#div_buscar_proveedor').fadeIn("slow");
    AbrirDivBuscarProveedor();
    CargarProveedores();
    $("#dialogLoading").dialog("close");
}
function buscarcategoria() {

    $('#div_buscar_categoria').fadeIn("slow");
    AbrirDivBuscarCategoria();
    CargarCategoria();
    $("#dialogLoading").dialog("close");
}

function buscarsubcategoria() {

    $('#div_buscar_subcategoria').fadeIn("slow");
    AbrirDivBuscarSubCategoria();
    CargarSubCategoria();
    $("#dialogLoading").dialog("close");
}




function AbrirDivBuscarProveedor() {
    $("#div_buscar_proveedor").dialog({
        width: 500,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Buscar Proveedor',
        closeOnEscape: true,
        buttons: {
            "Salir": function () {
                $('#div_buscar_proveedor').dialog("destroy");
            }
        }
    });
}

function AbrirDivBuscarCategoria() {
    $("#div_buscar_categoria").dialog({
        width: 500,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Buscar Categoria',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $('#div_buscar_categoria').dialog("destroy");
            }
        }
    });
}

function AbrirDivBuscarSubCategoria() {
    $("#div_buscar_subcategoria").dialog({
        width: 500,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Buscar SubCategoria',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $('#div_buscar_subcategoria').dialog("destroy");
            }
        }
    });
}


function CargarProveedores() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=37';
    if ($("#gview_procesosMeta").length) {
        refrescarGridProcesosMeta();
    } else {
        jQuery("#proveedores").jqGrid({
            caption: 'Proveedores',
            url: url,
            datatype: 'json',
            height: 340,
            width: 480,
            colNames: ['Nit', 'Nombre', 'Estado'],
            colModel: [
                {name: 'c_nit', index: 'c_nit', sortable: true, align: 'center', width: '100', key: true},
                {name: 'c_payment_name', index: 'c_payment_name', sortable: true, align: 'center', width: '600'},
                {name: 'reg_status', index: 'reg_status', hidden: true, sortable: true, align: 'center', width: '700px'}

            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            /*pager: '#page_tabla_proveedores',*/
            ondblClickRow: function (rowid) {
                getdato(rowid, 'proveedor');
                $("#div_buscar_proveedor").dialog("close");
            },
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });

    }
}

function CargarCategoria() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=1';
    if ($("#gview_procesosMeta").length) {
        refrescarGridProcesosMeta();
    } else {
        jQuery("#categoria").jqGrid({
            caption: 'Categorias',
            url: url,
            datatype: 'json',
            height: 650,
            width: 800,
            colNames: ['Id', 'Nombre'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_categorias',
            ondblClickRow: function (rowid) {
                getdato(rowid, 'categoria');
                $("#div_buscar_categoria").dialog("close");
            },
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });

    }
}

function CargarSubCategoria() {

    var idProcesoMeta = $('#idcategoria').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=4&procesoMeta=' + idProcesoMeta;
    if ($("#gview_subcategoria").length) {
        refrescarGridProcesosInterno(idProcesoMeta);
    } else {
        jQuery("#subcategoria").jqGrid({
            caption: 'Subcategorias',
            url: url,
            datatype: 'json',
            height: 330,
            width: 620,
            colNames: ['Id', 'Id Proceso Meta', 'Categoria', 'Subcategoria', 'Descripcion', 'Acciones', 'Id Subcategoria'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key: true},
                {name: 'id_tabla_rel', index: 'id_tabla_rel', hidden: true},
                {name: 'descripcionTablaRel', index: 'descripcionTablaRel', hidden: true, sortable: true, align: 'center', width: '600px'},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600px'},
                {name: 'descripcion', index: 'descripcion', hidden: true, sortable: true, align: 'center', width: '600px'},
                {name: 'actions', index: 'actions', hidden: true, resizable: false, align: 'center', width: '200px'},
                {name: 'idsubcategoria', index: 'idsubcategoria', hidden: true, sortable: true, align: 'center', width: '600px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_subcategorias',
            ondblClickRow: function (rowid) {
                getdato(rowid, 'subcategoria');
                $("#div_buscar_subcategoria").dialog("close");

            },
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });

    }
}



function getdato(id, campo) {

    if (campo === 'categoria') {
        var idcategoria = $("#categoria").getRowData(id).id;
        var nomcategoria = $("#categoria").getRowData(id).nombre;

        $('#nomcategoria').val(nomcategoria);
        $('#idcategoria').val(idcategoria);




    } else {
        var idsubcategoria = $("#subcategoria").getRowData(id).idsubcategoria;
        var nomsubcategoria = $("#subcategoria").getRowData(id).descripcion;
        var idrelcatsub = $("#subcategoria").getRowData(id).id;

        $('#nomsubcategoria').val(nomsubcategoria);
        $('#idsubcategoria').val(idsubcategoria);
        $('#idrelcatsub').val(idrelcatsub);

    }
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}


function numeros(e)
{
    var tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8)
        return true;
    var patron = /\d/;
    var te = String.fromCharCode(tecla);
    return patron.test(te);
}

function mensajeConfirmAction(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}