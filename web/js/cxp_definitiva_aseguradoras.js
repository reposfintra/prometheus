/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    cargarComboAseguradoras();
    cargarComboPolizas();
    $('#buscar').click(function () {
        if ($("#periodo").val()!="" ){
        CargarCxpsVencidasAseguradoras();
    }else {
        
            mensajesDelSistema("Por favor debe especificar el periodo a consultar");
    }
    });
});

function CargarCxpsVencidasAseguradoras() {
    var grid_tabla = $("#tabla_cxp_vencidas_aseguradoras"); 
    if ($("#gview_tabla_cxp_vencidas_aseguradoras").length) {
        reloadGridTabla(grid_tabla, 10);
    } else {
        
        grid_tabla.jqGrid({
            
            
            caption: "Cxp Consolidada Aseguradoras",
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '1520',
            colNames: ['Nit Aseguradora','Aseguradora','Poliza','No Documento','Fecha Vencimiento','Negocio','Valor Neto Poliza','Iva Poliza','Valor Total Poliza','Saldo Cartera', 'Cuota', 'Capital'],
            colModel: [
                {name: 'nit',        index: 'nit',        width: 90,sortable: true, align: 'center', hidden: false},
                {name: 'aseguradora',index: 'aseguradora',width: 300, sortable: true, align: 'center',   hidden: false},
                {name: 'poliza',index: 'poliza',width: 120, sortable: true, align: 'center',   hidden: false},
                {name: 'documento',       index: 'documento',       width: 80, sortable: true, align: 'center',   hidden: false},
                {name: 'fecha_vencimiento',       index: 'fecha_vencimiento',       width: 110, sortable: true, align: 'center',   hidden: false},
                {name: 'documento_relacionado',       index: 'documento_relacionado',       width: 110, sortable: true, align: 'center',   hidden: false},                
                {name: 'valor', index: 'valor', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'valor_iva', index: 'valor_iva', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'vlr_neto', index: 'vlr_neto', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},                
                {name: 'saldo_cartera', index: 'saldo_cartera', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'cuota', index: 'cuota', width: 90, sortable: true, align: 'center', hidden: false},
                {name: 'capital', index: 'capital', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}}                
            ],
            rowNum: 10000,
            rowTotal:10000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#pager',
            pgtext: null,
            pgbuttons: false,
            multiselect:true,           
            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
                
            },
            loadComplete: function () {
                
                if (grid_tabla.jqGrid('getGridParam', 'records') <= 0) {
                                           
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            ajaxGridOptions: {
                
                data: {
                    opcion: 10,
                    aseguradora: $("#aseguradora").val(),
                    poliza: $("#poliza").val(),                  
                    periodo: $("#periodo").val()                  

                }                
                
            },
            gridComplete: function() { 
                
                var colSumTotal = jQuery("#tabla_cxp_vencidas_aseguradoras").jqGrid('getCol', 'vlr_neto', false, 'sum'); 
                var colSumTotalcapital = jQuery("#tabla_cxp_vencidas_aseguradoras").jqGrid('getCol', 'capital', false, 'sum'); 
                jQuery("#tabla_cxp_vencidas_aseguradoras").jqGrid('footerData', 'set', {vlr_neto: colSumTotal,capital:colSumTotalcapital});
                
            }, 
            
           
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
                
            }
        }).navGrid('#pager', {add: false, edit: false, del: false, search: false, refresh: false});
        $("#tabla_cxp_vencidas_aseguradoras").navButtonAdd('#pager', {
            caption: "Generar Cxp",
            title: "Generar Cxp consolidada",
            onClickButton: function () {
                procesarCxp();
            }
        });
        $("#tabla_cxp_vencidas_aseguradoras").navButtonAdd('#pager', {
            caption: "Exportar excel",
            title: "Exportar excel",
            onClickButton: function() {
                var info = jQuery('#tabla_cxp_vencidas_aseguradoras').getGridParam('records');
                if (info > 0) {
                    ExportarCxpPolizas();
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }

            }

        });
       
           
    }
    
}


function reloadGridTabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",
       
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                aseguradora: $("#aseguradora").val(),
                poliza: $("#poliza").val(),                  
                periodo: $("#periodo").val()   
            }
        }
    });
    grid_tabla.trigger("reloadGrid");    
     
}


function cargarComboAseguradoras() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 8
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
                $('#aseguradora').html('');
                $('#aseguradora').append('<option value="" >...</option>');
                for (var datos in json) {
                    $('#aseguradora').append('<option value=' + datos + ' >' + json[datos] + '</option>');
                }
           

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarComboPolizas() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 9
        },
        success: function (json) {
            if (json.error) {
                return;
            }            
                $('#poliza').html('');
                $('#poliza').append('<option value="" >...</option>');
                for (var datos in json) {
                    $('#poliza').append('<option value=' + datos + ' >' + json[datos] + '</option>');
                }
           
        }, error: function (xhr ,thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function procesarCxp(){
     
    var jsondocumento =[];
    var filasId =jQuery('#tabla_cxp_vencidas_aseguradoras').jqGrid('getGridParam', 'selarrrow');
    if (filasId != ''){
        for (var i = 0; i < filasId.length; i++) {        
            var documento = jQuery("#tabla_cxp_vencidas_aseguradoras").getRowData(filasId[i]).documento;   
           
            var documentos = {};
            documentos ["documento"] = documento;           
            jsondocumento.push(documentos); 
            
        }    
        var listcxp = {};
        listcxp ["documentos"] = jsondocumento;       
      
       loading("Espere un momento por favor...", "270", "140");
        setTimeout(function () {
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Negocios&accion=Fintra",
                dataType: 'json',
                data: {
                    opcion: 11,
                    aseguradora : $("#aseguradora").val(),
                    listadoCxps: JSON.stringify(listcxp),
                    poliza: $('select[name="poliza"] option:selected').text()
                },
                success: function (json) {             
                    if (!isEmptyJSON(json)) {
                       
                        if (json.respuesta === "OK" && json.numCxpClie !== 'cxpc: ') {
                            
                             MsjresultadoCxp("Exito al generar cuenta por pagar aseguradora No:<b>" +json.numCxP+ "</b><br>"+ 
                             "Se generaron las siguientes CXPs de polizas a cliente por realizar pago total <b>" +json.numCxpClie +"</b>"
                            , '350', '150', true);
                            $("#dialogLoading").dialog('close');
                           
                        } else if (json.respuesta === "OK"){
                            MsjresultadoCxp("Exito al generar cuenta por pagar No:<b>" +json.numCxP +"<b>", '250', '150', true);
                            $("#dialogLoading").dialog('close');
                        }
                        
                        else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Error al generar la cuenta por pagar" , '250', '150');
                        }

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos ocurri� un error al generar CXP!!", '250', '150');

                    }
                },
                error: function (xhr) {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Error al generar Cxp consolidada: " + "\n" +
                             xhr.responseText, '650', '250', true);
                }
            });
        }, 500);
        
    }else{
        if (jQuery("#tabla_cxp_vencidas_aseguradoras").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar el(los) negocio(s) a procesar!!", '250', '150');
        } else {
            mensajesDelSistema("No hay negocios para procesar", '250', '150');
        }             
    } 
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                
                $(this).dialog("close");
            }
        }
    });

}

function MsjresultadoCxp(msj, width, height) {
    var grid_tabla = $("#tabla_cxp_vencidas_aseguradoras");
    
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
  
    $("#dialogMsj").dialog({
        dialogClass: 'hide-close',
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
                reloadGridTabla(grid_tabla, 10)
                
            }
        }
    });

}
function  ExportarCxpPolizas() {
    
   
    var data = $("#tabla_cxp_vencidas_aseguradoras").jqGrid('getGridParam','selarrrow'); 
    var jsondocumento =[];
    
     if (data != ''){
    for(var i=0;i<data.length;i++){
        
    var documento = jQuery("#tabla_cxp_vencidas_aseguradoras").getRowData(data[i]);   
     jsondocumento.push(documento);       
     }
    
      var myJsonString = JSON.stringify(jsondocumento);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Negocios&accion=Fintra",
        data: {
            opcion: 15,
            listado: myJsonString,
            poliza: $('select[name="poliza"] option:selected').text()
            
        },
        success: function () {
           
            cerrarDiv("#divSalidaEx");
             mensajesDelSistema ("Documento generado con exito, puede consultarlo en la carpeta descargas", '250', '150');
        },
        error: function () {
            mensajesDelSistema("Ocurrio un error enviando los datos al servidor", '250', '150');
        }
    });
        
    }else{
        if (jQuery("#tabla_cxp_vencidas_aseguradoras").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar el(los) negocio(s) a exportar!!", '250', '150');
        } else {
            mensajesDelSistema("No hay negocios para exportar", '250', '150');
        }             
    }
    
}
    
function cerrarDiv(div)
{
    $(div).dialog('close');
}