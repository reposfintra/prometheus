/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
  $(document).ready(function () {   

/*$("#fecha_negocio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });


    var myDate = new Date();
    $("#fecha_negocio").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
   */
    
    if ($('#div_filtro').is(':visible')) {    
        $("#fecha_ini").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
            defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
        });

        $("#fecha_fin").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
            defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
        });
        
        
        $('#ui-datepicker-div').css('clip', 'auto');
    }
    
    $('#listarAuditoria').click(function() {
           auditoriaLibranzas();
        });
});

function mensajesDelSistema(msj, width, height, swHideDialog) {   
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}

function auditoriaLibranzas() {
    console.log('Entra aqui');
    var grid_tabla_ = jQuery("#tabla_Auditoria");
    if ($("#gview_tabla_Auditoria").length) {
        reloadGridMostrar(grid_tabla_, 66);
    } else {
    //alert('Entra aqui');    
        grid_tabla_.jqGrid({
            
            caption: "Auditoria Libranzas",
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",
            datatype: "json",
            height: '700',
            width: '1500',
            colNames: ['Negocio', 'Cuotas', 'Nombre cliente', 'Cedula', 'Actividad','Estado','Fecha negocio','Fecha aprobacion',
                'Fecha desembolso','Fecha contabilizacion','Documentos','Diferidos','Contabilizado','Periodo','Vlr negocio','Compra cartera', 'Deducciones', 'Egreso'],
            colModel: [
                {name: 'cod_neg', index: 'cod_neg', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'cuotas', index: 'cuotas', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'nombre', index: 'nombre', width: 195, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cod_cli', index: 'cod_cli', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'actividad', index: 'actividad', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'estado_neg', index: 'estado_neg', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_negocio', index: 'fecha_negocio', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_ap', index: 'fecha_ap', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_desembolso', index: 'fecha_desembolso', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_contabilizacion', index: 'fecha_contabilizacion', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'num_facturas', index: 'num_facturas', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'ing_diferidos', index: 'ing_diferidos', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'contabilizado', index: 'contabilizado', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'periodo', index: 'periodo', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'vr_negocio', index: 'vr_negocio', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'compra_cartera', index: 'compra_cartera', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'deducciones', index: 'deducciones', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'egreso', index: 'egreso', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 66,
                    fecha_ini:$('#fecha_ini').val(),
                    fecha_fin:$('#fecha_fin').val(),
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {/*
                var cant = jQuery("#tabla_Auditoria").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_Auditoria").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoPoliza('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_Auditoria").jqGrid('setRowData', cant[i], {cambio: be});
                }*/
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                /*operacion = 'Editar';
                var myGrid = jQuery("#tabla_Auditoria"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var nombre_poliza = filas.nombre_poliza;
                var descripcion = filas.descripcion;
                if (estado === 'Activo') {
                    ventanaPolizas(operacion, id, nombre_poliza, descripcion);
                } else if (estado === 'Inactivo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }*/
            }
        })/*.navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_Auditoria").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaPolizas(operacion);
            }
        });*/
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                fecha_ini:$('#fecha_ini').val(),
                fecha_fin:$('#fecha_fin').val(),
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}
