/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    }); 
});

function initAreas(){
    listarAreas();
    maximizarventana();
}

function initDisciplinas(){
    listarDisciplinas();
    maximizarventana();
}

function initActividades(){
    listarActividades();
    maximizarventana();
}

function initPolizas(){
    listarPolizas();
    maximizarventana();
}

function initAseguradoras(){
    listarAseguradoras();
    maximizarventana();
    
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });  
}

function initBroker(){
    //Caqrgamos la grilla de broker
    listarBroker();    
    //cargamos combo tipos de identificacion
    cargarTiposIdentificacion();
    //cargar paises.
    cargarPaises();
    cargarDepartamentos('CO', 'dep_dir');
    $('#dep_dir').val('ATL');
    cargarCiudad('ATL', "ciu_dir");
    $('#ciu_dir').val('BQ');
    cargarVias('BQ', "via_princip_dir");
    cargarVias('BQ', "via_genera_dir");
       
    $('.solo-numero').keyup(function() {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });    
      
    $("#pais").change(function() {
        var op = $(this).find("option:selected").val();
        cargarDepartamentos(op, "departamento");
    });
    
    $("#departamento").change(function() {
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciudad");
    });
    
    $("#dep_dir").change(function () {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciu_dir");
    });
    
    $("#ciu_dir").change(function () {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarVias(op, "via_princip_dir");
        cargarVias(op, "via_genera_dir");
    });
    
    $("#via_princip_dir").change(function () {
        $("#via_genera_dir").val('');
    });
    maximizarventana();
    
}

function listarAreas() {      
    var grid_tbl_areas = jQuery("#tabla_areas");
     if ($("#gview_tabla_areas").length) {
        refrescarGridAreas();
     }else {
        grid_tbl_areas.jqGrid({
            caption: "AREAS",
            url: "./controlleropav?estado=Maestro&accion=Proyecto",           	 
            datatype: "json",  
            height: '290',
            width: '710',
            cellEdit: true,
            colNames: ['Id','Nombre','Estado','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'nombre', index: 'nombre', width: 530, align: 'left'},              
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden:true}, 
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}         
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_areas'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext:null,
            pgbuttons:false,         
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion: 17
                     }
            },   
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_areas").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_areas").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoArea('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_areas").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_areas"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;
                                 
                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                    editarArea(id);
                }
                    
          }
        }).navGrid("#page_tabla_areas", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_areas").jqGrid("navButtonAdd", "#page_tabla_areas", {
            caption: "Nuevo",
            onClickButton: function() {               
                crearArea();
            }
        });
    }  
       
}

function refrescarGridAreas(){   
    jQuery("#tabla_areas").setGridParam({
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: {
                opcion: 17
            }
        }       
    });
    
    jQuery('#tabla_areas').trigger("reloadGrid");
}

function crearArea(){   
    $('#div_area').fadeIn('slow'); 
    $('#idArea').val('');
    $('#nombre').val('');
    AbrirDivCrearArea();
}

function AbrirDivCrearArea(){
      $("#div_area").dialog({
        width: 'auto',
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR AREA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {         
                guardarArea();             
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_area").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarArea(cl){
    
    $('#div_area').fadeIn("slow");
    var fila = jQuery("#tabla_areas").getRowData(cl);  
    var nombre = fila['nombre'];

    $('#idArea').val(cl);
    $('#nombre').val(nombre); 
    AbrirDivEditarArea();
}

function AbrirDivEditarArea(){
      $("#div_area").dialog({
        width: 'auto',
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR AREA',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              guardarArea();
            },
            "Salir": function () {              
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_area").parent().find(".ui-dialog-titlebar-close").hide();
}

function guardarArea(){   
    var nombre = $('#nombre').val();
    
    if(nombre!==''){
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: "./controlleropav?estado=Maestro&accion=Proyecto",
                    data: {
                        opcion:($('#idArea').val()==='')? 18:19,
                        id:$('#idArea').val(),
                        nombre:$('#nombre').val()
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '270', '165');
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                refrescarGridAreas(); 
                                $("#div_area").dialog('close');
                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo guardar el area!!", '250', '150');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                                "Message: " + xhr.statusText + "\n" +
                                "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                });
             },500);
    }else{
         mensajesDelSistema("DEBE INGRESAR EL NOMBRE!!", '250', '150');      
    }  
}


function CambiarEstadoArea(rowid){
    var grid_tabla = jQuery("#tabla_areas");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        data: {
            opcion: 20,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarGridAreas();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado del area!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function listarDisciplinas() {      
    var grid_tbl_disciplinas = jQuery("#tabla_disciplinas");
     if ($("#gview_tabla_disciplinas").length) {
        refrescarGridDisciplinas();
     }else {
        grid_tbl_disciplinas.jqGrid({
            caption: "DISCIPLINAS",
            url: "./controlleropav?estado=Maestro&accion=Proyecto",           	 
            datatype: "json",  
            height: '290',
            width: '710',
            cellEdit: true,
            colNames: ['Id','Nombre','Estado','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'nombre', index: 'nombre', width: 530, align: 'left'},               
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden:true}, 
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}         
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_disciplinas'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext:null,
            pgbuttons:false,         
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion: 21
                     }
            },   
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_disciplinas").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_disciplinas").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoDisciplina('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_disciplinas").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_disciplinas"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;
                                 
                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                    editarDisciplina(id);
                }
                    
          }
        }).navGrid("#page_tabla_disciplinas", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_disciplinas").jqGrid("navButtonAdd", "#page_tabla_disciplinas", {
            caption: "Nuevo",
            onClickButton: function() {               
                crearDisciplina();
            }
        });
    }  
       
}

function refrescarGridDisciplinas(){   
    jQuery("#tabla_disciplinas").setGridParam({
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: {
                opcion: 21
            }
        }       
    });
    
    jQuery('#tabla_disciplinas').trigger("reloadGrid");
}

function crearDisciplina(){   
    $('#div_disciplina').fadeIn('slow'); 
    $('#idDisciplina').val('');
    $('#nombre').val('');   
    AbrirDivCrearDisciplina();
}

function AbrirDivCrearDisciplina(){
      $("#div_disciplina").dialog({
        width: 'auto',
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR DISCIPLINA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {         
                guardarDisciplina();             
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_disciplina").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarDisciplina(cl){
    
    $('#div_disciplina').fadeIn("slow");
    var fila = jQuery("#tabla_disciplinas").getRowData(cl);  
    var nombre = fila['nombre'];    

    $('#idDisciplina').val(cl);
    $('#nombre').val(nombre);   
    AbrirDivEditarDisciplina();
}

function AbrirDivEditarDisciplina(){
      $("#div_disciplina").dialog({
        width: 'auto',
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR DISCIPLINA',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              guardarDisciplina();
            },
            "Salir": function () {              
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_disciplina").parent().find(".ui-dialog-titlebar-close").hide();
}

function guardarDisciplina(){   
    var nombre = $('#nombre').val();   
    
    if(nombre!==''){
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: "./controlleropav?estado=Maestro&accion=Proyecto",
                    data: {
                        opcion:($('#idDisciplina').val()==='')? 22:23,
                        id:$('#idDisciplina').val(),
                        nombre:$('#nombre').val()
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '270', '165');
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                refrescarGridDisciplinas(); 
                                $("#div_disciplina").dialog('close');
                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo guardar la disciplina!!", '250', '150');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                                "Message: " + xhr.statusText + "\n" +
                                "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                });
             },500);
    }else{
         mensajesDelSistema("DEBE INGRESAR EL NOMBRE!!", '250', '150');      
    }  
}

function CambiarEstadoDisciplina(rowid){
    var grid_tabla = jQuery("#tabla_disciplinas");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        data: {
            opcion: 24,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarGridDisciplinas();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado de la disciplina!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function listarActividades() {      
    var grid_tbl_actividades = jQuery("#tabla_actividades");
     if ($("#gview_tabla_actividades").length) {
        refrescarGridActividades();
     }else {
        grid_tbl_actividades.jqGrid({
            caption: "ACTIVIDADES",
            url: "./controlleropav?estado=Maestro&accion=Proyecto",           	 
            datatype: "json",  
            height: '290',
            width: '850',
            cellEdit: true,
            colNames: ['Id','Descripcion','Estado','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},              
                {name: 'descripcion', index: 'descripcion', width: 650, align: 'left'},  
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden:true}, 
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}         
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_actividades'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext:null,
            pgbuttons:false,         
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion: 13
                     }
            },   
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_actividades").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_actividades").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoActividad('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_actividades").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_actividades"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;
                                 
                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                    editarActividad(id);
                }
                    
          }
        }).navGrid("#page_tabla_actividades", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_actividades").jqGrid("navButtonAdd", "#page_tabla_actividades", {
            caption: "Nuevo",
            onClickButton: function() {               
                crearActividad();
            }
        });
    }  
       
}

function refrescarGridActividades(){   
    jQuery("#tabla_actividades").setGridParam({
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: {
                opcion: 13
            }
        }       
    });
    
    jQuery('#tabla_actividades').trigger("reloadGrid");
}

function crearActividad(){   
    $('#div_actividad').fadeIn('slow'); 
    $('#idActividad').val('');   
    $('#descripcion').val('');
    AbrirDivCrearActividad();
}

function AbrirDivCrearActividad(){
      $("#div_actividad").dialog({
        width: 470,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR ACTIVIDAD',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {         
                guardarActividad();             
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_actividad").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarActividad(cl){
    
    $('#div_actividad').fadeIn("slow");
    var fila = jQuery("#tabla_actividades").getRowData(cl);  
    var descripcion = fila['descripcion'];

    $('#idActividad').val(cl);   
    $('#descripcion').val(descripcion);   
    AbrirDivEditarActividad();
}

function AbrirDivEditarActividad(){
      $("#div_actividad").dialog({
        width: 470,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR ACTIVIDAD',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              guardarActividad();
            },
            "Salir": function () {              
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_actividad").parent().find(".ui-dialog-titlebar-close").hide();
}

function guardarActividad(){       
    var descripcion = $('#descripcion').val();
    
    if(descripcion!==''){
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: "./controlleropav?estado=Maestro&accion=Proyecto",
                    data: {
                        opcion:($('#idActividad').val()==='')? 14:15,
                        id:$('#idActividad').val(),                    
                        descripcion:$('#descripcion').val()
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '270', '165');
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                refrescarGridActividades(); 
                                $("#div_actividad").dialog('close');
                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo guardar la actividad!!", '250', '150');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                                "Message: " + xhr.statusText + "\n" +
                                "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                });
             },500);
    }else{
         mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');      
    }  
}


function CambiarEstadoActividad(rowid){
    var grid_tabla = jQuery("#tabla_actividades");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        data: {
            opcion: 16,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarGridActividades();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado de la actividad!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function listarPolizas(){
    
    var operacion;
    var grid_tabla_ = jQuery("#tabla_polizas");
    if ($("#gview_tabla_polizas").length) {
        reloadGridPolizas();
    } else {    
        grid_tabla_.jqGrid({
            caption: "Polizas",
            url: "./controlleropav?estado=Maestro&accion=Proyecto",           	 
            datatype: "json",  
            height: '290',
            width: '710',          
            colNames: ['Id', 'Nombre Poliza', 'Descripcion', 'Estado', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'nombre_poliza', index: 'nombre_poliza', width: 240, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'descripcion', index: 'descripcion', width: 270, sortable: true, align: 'left', hidden: false, search: true},             
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden:true}, 
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}         
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_polizas'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext:null,
            pgbuttons:false,         
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion: 42
                     }
            },              
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_polizas").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_polizas").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoPoliza('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_polizas").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_polizas"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var nombre_poliza = filas.nombre_poliza;
                var descripcion = filas.descripcion;
                var reg_status = filas.reg_status;
                                 
                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                     ventanaPolizas(operacion, id, nombre_poliza, descripcion);
                }   
            }
        }).navGrid("#page_tabla_polizas", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_polizas").navButtonAdd('#page_tabla_polizas', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaPolizas(operacion);
            }
        });
    }
}

function reloadGridPolizas() {
    jQuery("#tabla_polizas").setGridParam({
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 42
            }
        }
    });
    jQuery("#tabla_polizas").trigger("reloadGrid");
}


function ventanaPolizas(operacion, id, nombre_poliza, descripcion) {
    if (operacion === 'Nuevo') {
        $("#idPoliza").val('');
        $("#nombre").val('');
        $("#descripcion").val('');
        $("#div_poliza").dialog({
            width: '700',
            height: '230',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'NUEVA POLIZA',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarPoliza();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#idPoliza").val('');
                    $("#nombre").val('');
                    $("#descripcion").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#idPoliza").val(id);
        $("#nombre").val(nombre_poliza);
        $("#descripcion").val(descripcion);
        

        $("#div_poliza").dialog({
            width: '700',
            height: '230',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'EDITAR POLIZA',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarPoliza();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#idPoliza").val('');
                    $("#nombre").val('');
                    $("#descripcion").val('');
                }
            }
        });
    }
}

function guardarPoliza() {
    var nombre = $("#nombre").val();
    var desc = $("#descripcion").val();   
    if (nombre !== '' && desc !== '') {
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function () {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "./controlleropav?estado=Maestro&accion=Proyecto",
                data: {
                    opcion: 43,
                    nombre: $("#nombre").val(),
                    desc: $("#descripcion").val(),
                },
                success: function (json, textStatus, jqXHR) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '270', '165');
                            return;
                        }

                        if (json.respuesta === "OK") {
                            $("#dialogLoading").dialog('close');
                            reloadGridPolizas();
                            mensajesDelSistema("Exito al guardar", '230', '150', true);
                            $("#div_poliza").dialog('close');                        
                        }

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se pudo guardar la poliza!!", '250', '150');
                    }
                }, error: function (result) {
                    alert('ERROR ');
                }
            });
          }, 500); 
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function ActualizarPoliza() {
    var nombre = $("#nombre").val();
    var desc = $("#descripcion").val();
    if (nombre !== '' && desc !== '') {
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function () {
               $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "./controlleropav?estado=Maestro&accion=Proyecto",
                data: {
                    opcion: 44,
                    id: $("#idPoliza").val(),
                    nombre: $("#nombre").val(),
                    desc: $("#descripcion").val(),
                },
                success: function (json, textStatus, jqXHR) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '270', '165');
                            return;
                        }

                        if (json.respuesta === "OK") {
                            $("#dialogLoading").dialog('close');
                            reloadGridPolizas();
                            mensajesDelSistema("Exito al actualizar", '230', '150', true);
                            $("#div_poliza").dialog('close');
                        }

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se pudo actualizar la poliza!!", '250', '150');
                    }
                }, error: function (result) {
                    alert('ERROR ');
                }
            });
        }, 500);     
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function CambiarEstadoPoliza(rowid) {
    var grid_tabla = jQuery("#tabla_polizas");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        data: {
            opcion: 45,
            id: id
        },
        success: function (json) {
              if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   reloadGridPolizas();
                }
            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado de la poliza!!", '250', '150');
            }           
        }, error: function (result) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function listarBroker(){
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    if ($("#gview_tabla_broker").length) {
         refrescarGridBroker();
     }else {
        jQuery("#tabla_broker").jqGrid({
            caption: 'Broker',
            url: url,
            datatype: 'json',
            height: 250,
            width: 750,
            colNames: ['Id', 'Tipo Documento', 'Documento', 'Nombre', 'idCiudad', 'idDpto', 'idPais', 'Direcci�n', 'Id Usuario', 'Nombre Contacto', 'Telefono', 'Celular', 'Email', 'Enviar Correo', 'Nombre Asistente', 'Telefono', 'Celular', 'Email', 'Enviar Correo', 'Estado', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', hidden:true, sortable: true, align: 'center', width: '100px', key:true},                 
                {name: 'tipo_documento', index: 'tipo_documento', sortable: true, align: 'center', width: '100px'},
                {name: 'documento', index: 'documento', sortable: true, align: 'center', width: '120px'},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '280px'},
                {name: 'codciu', index: 'codciu', hidden:true, sortable: true, align: 'center', width: '100px'},
                {name: 'coddpto', index: 'coddpto', hidden:true, sortable: true, align: 'center', width: '100px'},
                {name: 'codpais', index: 'codpais', hidden:true, sortable: true, align: 'center', width: '100px'},  
                {name: 'direccion', index: 'direccion', hidden: true, sortable: true, align: 'left', width: '170px'},
                {name: 'idusuario', index: 'idusuario', hidden: true, sortable: true, align: 'left', width: '110px'},
                {name: 'nombre_contacto', index: 'nombre_contacto', hidden:true, sortable: true, align: 'left', width: '280px'},
                {name: 'telcontacto', index: 'telcontacto', hidden:true, sortable: true, align: 'center', width: '100px'},               
                {name: 'celular_contacto', index: 'celular_contacto', hidden:true, sortable: true, align: 'center', width: '100px'}, 
                {name: 'email_contacto', index: 'email_contacto', hidden:true, sortable: true, align: 'center', width: '170px'},   
                {name: 'enviar_correo_contacto', index: 'enviar_correo_contacto', hidden:true, sortable: true, align: 'center', width: '100px'},        
                {name: 'nombre_asistente', index: 'nombre_asistente', hidden:true, sortable: true, align: 'left', width: '280px'},
                {name: 'telasistente', index: 'telasistente', hidden:true, sortable: true, align: 'center', width: '100px'},               
                {name: 'celular_asistente', index: 'celular_asistente', hidden:true, sortable: true, align: 'center', width: '100px'}, 
                {name: 'email_asistente', index: 'email_asistente', hidden:true, sortable: true, align: 'center', width: '170px'},
                {name: 'enviar_correo_asistente', index: 'enviar_correo_asistente', hidden:true, sortable: true, align: 'center', width: '100px'},   
                {name: 'reg_status', index: 'reg_status', hidden:true, sortable: true, align: 'center', width: '90px'},
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            pager:'#page_tabla_broker',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                       opcion: 46
                     }
            },   
            gridComplete: function (index) {
                var cant = jQuery("#tabla_broker").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_broker").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoBroker('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_broker").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_broker"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;
                                 
                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                    editarBroker(id);
                }
                    
            },            
            loadError: function(xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_broker",{search:false,refresh:false,edit:false,add:false,del:false});
        jQuery("#tabla_broker").jqGrid("navButtonAdd", "#page_tabla_broker", {
            caption: "Nuevo", 
            title: "Agregar nuevo Broker",           
            onClickButton: function() {
               crearBroker(); 
            }
        });
     }
}


function refrescarGridBroker(){    
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    jQuery("#tabla_broker").setGridParam({
        url: url,
        datatype: 'json',       
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 46
            }
        }       
    });
    
    jQuery('#tabla_broker').trigger("reloadGrid");
}

function crearBroker(){  
    $('#div_broker').fadeIn('slow');
    AbrirDivCrearBroker();
}

function AbrirDivCrearBroker(){ 
    $('#idBroker').val('');
    $('#tipo_doc').val('');  
    $('#documento').val('');
    $('#nombre').val('');   
    $('#email').val('');
    $('#celular').val('');
    $('#telefono').val('');   
    $('#pais').val('CO');
    cargarDepartamentos('CO', "departamento");
    $('#departamento').val('ATL');
    cargarCiudad('ATL',"ciudad");
    $('#ciudad').val('BQ');    
    $('#direccion').val('');
    $('#idusuario').attr({readonly: false});
    $('#idusuario').val('');    
    $('#nombre_contacto').val('');   
    $('input[name=mailcontacto]').val(['N']);
    $('#nombre_asistente').val('');
    $('#email_asistente').val('');
    $('#celular_asistente').val('');
    $('#telefono_asistente').val('');
    $('input[name=mailasistente]').val(['N']);
    $('#div_broker').fadeIn('slow');
    $("#div_broker").dialog({
        width: 'auto',
        height: 530,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR BROKER',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
              guardarBroker();
            },
            "Salir": function () {          
                setDireccion(0);
                $(this).dialog("destroy");
            }
        }
    });
}

function editarBroker(cl){
    
    $('#div_broker').fadeIn("slow");
    var fila = jQuery("#tabla_broker").getRowData(cl);  
    var tipoDoc = fila['tipo_documento'];
    var documento = fila['documento'];
    var nombre = fila['nombre'];
    var pais = fila['codpais'];
    var dpto = fila['coddpto'];
    var ciudad = fila['codciu']; 
    var email = fila['email_contacto'];
    var celular = fila['celular_contacto'];
    var telefono = fila['telcontacto'];  
    var direccion = fila['direccion'];  
    var idusuario = fila['idusuario'];  
    var nombre_contacto = fila['nombre_contacto'];
    var enviar_correo_contacto = fila['enviar_correo_contacto'];
    var nombre_asistente = fila['nombre_asistente'];
    var email_asistente = fila['email_asistente'];
    var celular_asistente = fila['celular_asistente'];
    var telefono_asistente = fila['telasistente'];  
    var enviar_correo_asistente = fila['enviar_correo_asistente'];
        
    $('#idBroker').val(cl);  
    $('#tipo_doc').val(tipoDoc); 
    $('#documento').val(documento);
    $('#nombre').val(nombre);   
    $('#email_contacto').val(email);
    $('#celular_contacto').val(celular);
    $('#telefono_contacto').val(telefono);    
    $('#pais').val(pais);
    cargarDepartamentos(pais, "departamento");
    $('#departamento').val(dpto);
    cargarCiudad(dpto,"ciudad");
    $('#ciudad').val(ciudad);    
    $('#direccion').val(direccion);
    $('#idusuario').attr({readonly: true});
    $('#idusuario').val(idusuario);    
    $('#nombre_contacto').val(nombre_contacto); 
    $('input[name=mailcontacto]').val([enviar_correo_contacto]); 
    $('#nombre_asistente').val(nombre_asistente);
    $('#email_asistente').val(email_asistente);
    $('#celular_asistente').val(celular_asistente);
    $('#telefono_asistente').val(telefono_asistente);
    $('input[name=mailasistente]').val([enviar_correo_asistente]); 
    AbrirDivEditarBroker();
}

function AbrirDivEditarBroker(){
      $("#div_broker").dialog({
        width: 'auto',
        height: 530,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR BROKER',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              guardarBroker();
            },
            "Salir": function () {    
               setDireccion(0);
               $(this).dialog("destroy");
            }
        }
    });
}

function guardarBroker(){    
    var tipoDoc = $('#tipo_doc').val();
    var documento = $('#documento').val();  
    var nombre = $('#nombre').val();  
    var email = $('#email').val();
    var celular = $('#celular').val();
    var telefono = $('#telefono').val();  
    var pais = $('#pais').val();
    var departamento = $('#departamento').val();
    var ciudad = $('#ciudad').val();   
    var direccion = $('#direccion').val();   
    var idusuario = $('#idusuario').val();   
    //Obtenemos informaci�n de contacto
    var nombre_contacto = $('#nombre_contacto').val();  
    var email = $('#email_contacto').val();
    var celular = $('#celular_contacto').val();
    var telefono = $('#telefono_contacto').val();
    var enviar_correo_contacto = $("input[name=mailcontacto]:checked").val(); 
    //Obtenemos informaci�n del asistente
    var nombre_asistente = $('#nombre_asistente').val();  
    var email_asistente = $('#email_asistente').val();
    var celular_asistente = $('#celular_asistente').val();
    var telefono_asistente = $('#telefono_asistente').val();
    var enviar_correo_asistente = $("input[name=mailasistente]:checked").val(); 
    
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    if(tipoDoc!=='' && documento!=='' && nombre!=='' && ciudad!=='' && (telefono!=='' || celular!=='') && email!=='' && idusuario!=='' && nombre_contacto!==''){
        if(telefono!=='' && !validarTelefono(telefono))
        {
            mensajesDelSistema("Por favor, ingrese un numero de telefono valido", '250', '150'); 
        }else if(!validarEmail(email)){
            mensajesDelSistema("El email ingresado es incorrecto. Por favor, Verifique", '250', '150'); 
        }else if(!verifica_usuario() && $('#idBroker').val()===''){
            mensajesDelSistema("El usuario ingresado ya existe. Por favor, ingrese uno nuevo", '250', '150');      
        }else{
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        opcion: ($('#idBroker').val()==='')? 47:48,
                        id:$('#idBroker').val(), 
                        tipo_doc: tipoDoc,
                        documento: documento,
                        nombre: nombre,                             
                        email: email,
                        celular: celular,
                        telefono: telefono,                  
                        pais: pais,
                        departamento: departamento,
                        ciudad: ciudad,
                        direccion:direccion,
                        idusuario:idusuario,
                        nombre_contacto:nombre_contacto,
                        enviar_correo_contacto: enviar_correo_contacto,
                        nombre_asistente:nombre_asistente,
                        email_asistente: email_asistente,
                        celular_asistente: celular_asistente,
                        telefono_asistente: telefono_asistente,
                        enviar_correo_asistente: enviar_correo_asistente
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '250', '150');                          
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                refrescarGridBroker(); 
                                mensajesDelSistema("Se guardaron los cambios satisfactoriamente", '250', '150', true);     
                                $("#div_broker").dialog('close');

                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo guardar el broker!!", '250', '150');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                              "Message: " + xhr.statusText + "\n" +
                              "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                }); 
            }, 500);
        }
    }else{
       mensajesDelSistema("Hay campos obligatorios que faltan por llenarse", '250', '150');
    }
}

function CambiarEstadoBroker(rowid){
    var grid_tabla = jQuery("#tabla_broker");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        data: {
            opcion: 49,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridBroker();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado del broker!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarTiposIdentificacion() {
  
    $.ajax({
        type: 'POST',
        async:false,
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        dataType: 'json',
        data: {
            opcion: 34
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#tipo_doc').append("<option value=''>Seleccione</option>");                 

                    for (var key in json) {
                        $('#tipo_doc').append('<option value=' + key + '>' + json[key] + '</option>');                        
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarPaises() {
  
    $.ajax({
        type: 'POST',
        async:false,
        url: "./controller?estado=Archivo&accion=Asobancaria",
        dataType: 'json',
        data: {
            opcion: 12
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#pais').append("<option value=''>Seleccione</option>");                 

                    for (var key in json) {
                        $('#pais').append('<option value=' + key + '>' + json[key] + '</option>');                        
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarDepartamentos(codigo, combo) {
  if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 13,
                cod_pais: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#'+combo).append("<option value=''>Seleccione</option>");                 

                        for (var key in json) {
                            $('#'+combo).append('<option value=' + key + '>' + json[key] + '</option>');                      
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
  }
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function cargarVias(codciu, combo) {
   
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=GestionSolicitud&accion=Aval",
            dataType: 'json',
            data: {
                opcion: 'cargarvias',
                ciu: codciu
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''></option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } 
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

}

function genDireccion(elemento,e) {
  
    var contenedor = document.getElementById("direccion_dialogo")
      , res = document.getElementById("dir_resul");
     
    /*$("#direccion_dialogo").draggable({ handle: "#drag_direcciones"});
    Posicionar_div("direccion_dialogo",e); */
    contenedor.style.top = 15 + 'px';  
    contenedor.style.left = 725 + 'px';  
    contenedor.style.display = "block";

   $("#div_broker").css({
        'width':'1110px'
    });
    res.name = elemento;    
    res.value = (elemento.value) ? elemento.value : '';
    
}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3: 
            var res = document.getElementById('dir_resul')
              , des = document.getElementById(res.name);
              des.value = res.value;
        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");
            
            document.getElementById("direccion_dialogo").style.display = "none";
            $("#div_broker").css({
                'width': 'auto'
            });
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
              , g = document.getElementById('via_genera_dir')
              , opcion;
            for (var i = 0; i < g.length; i++) {
                
                opcion = g[i];
                
                if (opcion.value === p) {
                    
                    opcion.style.display = 'none';
                    opcion.disabled = true;
                    
                } else {
                    
                    opcion.disabled = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if(!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                || !jQuery('#placa_dir').val() ) {
                  jQuery('#dir_resul').val("");
                  jQuery('#dir_resul').attr("class","validation-failed");
              } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                    jQuery('#via_princip_dir option:selected').text()
                   + ' '  + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                   + ' '  + jQuery('#via_genera_dir option:selected').text().trim()
                   + ' '  + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                   + ((jQuery('#via_genera_dir option:selected').text().toUpperCase()==='#' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='TRANSVERSAL') ?  '-':  ' ')
                   + jQuery('#placa_dir').val().trim().toUpperCase()
                   + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
              }
            break;
    }
}

function verifica_usuario() {
    var estado = false;
    var id = $('#idusuario').val();
    $.ajax({
        url: '/fintra/controller?estado=GestionUsuarios&accion=AdmonUser',
        datatype: 'json',
        type: 'post',
        data: {opcion: 4, usuario:id},
        async: false,
        success: function (json) {
            if (json.mensaje) {
                if (json.mensaje === 'OK') {
                    estado = true;
                } 
            }
        },
        error: function (error) {
            alert(error);
        }
    });
    
    return estado;
}

function listarAseguradoras(){
    
    var operacion;
    var grid_tabla_ = jQuery("#tabla_aseguradoras");
    if ($("#gview_tabla_aseguradoras").length) {
        reloadGridAseguradoras();
    } else {    
        grid_tabla_.jqGrid({
            caption: "Aseguradoras",
            url: "./controlleropav?estado=Maestro&accion=Proyecto",           	 
            datatype: "json",  
            height: '330',
            width: '675',          
            colNames: ['Id', 'Nit', 'Nombre', 'Estado', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'nit', index: 'nit', width: 120, sortable: true, align: 'left', search: true},
                {name: 'nombre_seguro', index: 'nombre_seguro', width: 350, sortable: true, align: 'left', search: true},             
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden:true}, 
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}         
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_aseguradoras'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext:null,
            pgbuttons:false,         
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion: 68
                     }
            },              
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_aseguradoras").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_aseguradoras").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoAseguradora('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_aseguradoras").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_aseguradoras"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var nit_aseguradora = filas.nit;
                var nombre_aseguradora = filas.nombre_seguro;               
                var reg_status = filas.reg_status;
                                 
                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                     ventanaAseguradoras(operacion, id, nit_aseguradora, nombre_aseguradora);
                }   
            }
        }).navGrid("#page_tabla_aseguradoras", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_aseguradoras").navButtonAdd('#page_tabla_aseguradoras', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaAseguradoras(operacion);
            }
        });
    }
}

function reloadGridAseguradoras() {
    jQuery("#tabla_aseguradoras").setGridParam({
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 68
            }
        }
    });
    jQuery("#tabla_aseguradoras").trigger("reloadGrid");
}


function ventanaAseguradoras(operacion, id, nit_aseguradora, nombre_aseguradora) {
    if (operacion === 'Nuevo') {
        $("#idAseguradora").val('');
        $("#nit_aseguradora").val('');
        $("#nombre_aseguradora").val('');
        $("#div_aseguradora").dialog({
            width: '600',
            height: '165',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'NUEVA ASEGURADORA',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarAseguradora();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#idAseguradora").val('');
                    $("#nit_aseguradora").val('');
                    $("#nombre_aseguradora").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#idAseguradora").val(id);
        $("#nit_aseguradora").val(nit_aseguradora);
        $("#nombre_aseguradora").val(nombre_aseguradora);     

        $("#div_aseguradora").dialog({
            width: '600',
            height: '165',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'EDITAR ASEGURADORA',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarAseguradora();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#idAseguradora").val('');
                    $("#nit_aseguradora").val('');
                    $("#nombre_aseguradora").val('');
                }
            }
        });
    }
}

function guardarAseguradora() {
    var nit = $("#nit_aseguradora").val();
    var nombre = $("#nombre_aseguradora").val();   
    if (nit !== '' && nombre !== '') {
        if(!existeProveedor(nit)){
            mensajesDelSistema("El Nit ingresado no registra en la base de datos de proveedores. Por favor, creelo primero como proveedor", '320', '165');      
        }else{
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function () {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: "./controlleropav?estado=Maestro&accion=Proyecto",
                    data: {
                        opcion: 69,
                        nit_aseguradora: $("#nit_aseguradora").val(),
                        nombre_aseguradora: $("#nombre_aseguradora").val()
                    },
                    success: function (json, textStatus, jqXHR) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '270', '165');
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                reloadGridAseguradoras();
                                mensajesDelSistema("Exito al guardar", '230', '150', true);
                                $("#div_aseguradora").dialog('close');                        
                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo guardar la aseguradora!!", '250', '150');
                        }
                    }, error: function (result) {
                        alert('ERROR ');
                    }
                });
              }, 500); 
        }
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function ActualizarAseguradora() {
    var nit = $("#nit_aseguradora").val();
    var nombre = $("#nombre_aseguradora").val();
    if (nit !== '' && nombre !== '') {
        if(!existeProveedor(nit)){
            mensajesDelSistema("El Nit ingresado no registra en la base de datos de proveedores. Por favor, creelo primero como proveedor", '320', '165');      
        }else{
                loading("Espere un momento por favor...", "270", "140");
                setTimeout(function () {
                       $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: "./controlleropav?estado=Maestro&accion=Proyecto",
                        data: {
                            opcion: 70,
                            id: $("#idAseguradora").val(),
                            nit_aseguradora: $("#nit_aseguradora").val(),
                            nombre_aseguradora: $("#nombre_aseguradora").val()
                        },
                        success: function (json, textStatus, jqXHR) {
                            if (!isEmptyJSON(json)) {

                                if (json.error) {
                                    $("#dialogLoading").dialog('close');
                                    mensajesDelSistema(json.error, '270', '165');
                                    return;
                                }

                                if (json.respuesta === "OK") {
                                    $("#dialogLoading").dialog('close');
                                    reloadGridAseguradoras();
                                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                                    $("#div_aseguradora").dialog('close');
                                }

                            } else {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema("Lo sentimos no se pudo actualizar la aseguradora!!", '250', '150');
                            }
                        }, error: function (result) {
                            alert('ERROR ');
                        }
                    });
                }, 500);     
        }
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function CambiarEstadoAseguradora(rowid) {
    var grid_tabla = jQuery("#tabla_aseguradoras");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        data: {
            opcion: 71,
            id: id
        },
        success: function (json) {
              if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   reloadGridAseguradoras();
                }
            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado de la aseguradora!!", '250', '150');
            }           
        }, error: function (result) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function existeProveedor(nit){
    var estado = false;
    $.ajax({
        url: './controlleropav?estado=Maestro&accion=Proyecto',
        datatype:'json',
        type:'post',
        async:false,
        data:{
            opcion: 72,
            nit: nit
        },          
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.respuesta === "SI") {               
                   estado = true;                  
                }
                
            }              
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }  
    });
    return estado;
}

function validarTelefono(phone) {
    var filter = /^[0-9-()+]+$/;
    return filter.test(phone);
}

function validarEmail(email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( email );
}


function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}


function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}


function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}