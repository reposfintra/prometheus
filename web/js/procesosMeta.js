/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    cargarProcesosMeta();
    cargarComboProcesosMeta();
});

function cargarProcesosMeta(){
    var url = './controller?estado=Procesos&accion=Meta&opcion=1';
    if ($("#gview_procesosMeta").length) {
         refrescarGridProcesosMeta();
     }else {
        jQuery("#procesosMeta").jqGrid({
            caption: 'Procesos Meta',
            url: url,
            datatype: 'json',
            height: 400,
            width: 400,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key:true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '700px'},
                {name: 'descripcion', index: 'descripcion', hidden:true, sortable: true, align: 'center', width: '700px'},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '200px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
            pager:'#page_tabla_procesos_meta',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {
                    var ids = jQuery("#procesosMeta").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarProcesosMeta('" + cl + "');\">";
                        proc_an = "<img src='/fintra/images/botones/iconos/detail1.png' align='absbottom'  name='proc_anul' id='proc_anul' width='15' height='15' title ='Ver procesos internos anulados'  onclick=\"verProcesosAnulados('" + cl + "');\">";
                        an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el meta proceso seleccionado?','250','150',existeUsuarioRelMetaProceso,'" + cl + "');\">";
//                      an = "<img src='/fintra/images/botones/iconos/anular.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAnulacion('Alerta!!! Puede que existan usuarios asociados al Meta proceso, desea continuar?','350','165',anularMetaProceso,'" + cl + "');\">";
                        jQuery("#procesosMeta").jqGrid('setRowData', ids[i], {actions: ed+'   '+proc_an+'   '+an});
                       
                    }
                },
            loadError: function(xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_procesos_meta",{edit:false,add:false,del:false});
        jQuery("#procesosMeta").jqGrid("navButtonAdd", "#page_tabla_procesos_meta", {
            caption: "Nuevo", 
            title: "Agregar nuevo meta proceso",           
            onClickButton: function() {
               crearMetaProceso(); 
            }
        });
     }
}

function crearMetaProceso(){
    $('#nommeta').val('');
    $('#div_procesosMeta').fadeIn('slow');
    AbrirDivCrearMetaProceso();
}

function AbrirDivCrearMetaProceso(){
      $("#div_procesosMeta").dialog({
        width: 700,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR META PROCESO',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
              guardarMetaProceso();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarMetaProceso(){  
    var empresa = $('#nomempresa').val();
    var nomMeta = $('#nommeta').val();
    var descMeta = $('#descmeta').val();
    var url = './controller?estado=Procesos&accion=Meta';
    if(empresa!=='' && nomMeta!=='' && descMeta!==''){
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 2,
                    nombre: nomMeta,
                    descripcion: descMeta
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {
                            refrescarGridProcesosMeta();
                            cargarComboProcesosMeta();
                            mensajesDelSistema("Se cre� el meta proceso", '250', '150', true); 
                            $('#nommeta').val('');
                            $('#descmeta').val('');  
                        }
                        
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear el meta proceso!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
    }else{
       mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }
}

function refrescarGridProcesosMeta(){    
    var url = './controller?estado=Procesos&accion=Meta&opcion=1';
    jQuery("#procesosMeta").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#procesosMeta').trigger("reloadGrid");
}

function editarProcesosMeta(cl){
    
    $('#div_editar_procesosMeta').fadeIn("slow");
    var fila = jQuery("#procesosMeta").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    $('#idMetaEdit').val(cl);
    $('#nommetaEdit').val(nombre);
    $('#descmetaEdit').val(descripcion);
    AbrirDivEditarMetaProceso();
    cargarProcesosInternos();
}

function AbrirDivEditarMetaProceso(){
      $("#div_editar_procesosMeta").dialog({
        width: 700,
        height: 675,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'EDITAR META PROCESO',
        closeOnEscape: false,
        buttons: {    
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarMetaProceso(){
    var nombre = $('#nommetaEdit').val();
    var descripcion = $('#descmetaEdit').val();
    var idProceso = $('#idMetaEdit').val();
    var url = './controller?estado=Procesos&accion=Meta';
    if(nombre!=='' && descripcion!==''){
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 3,
                nombre: nombre,
                descripcion: descripcion,
                idProceso: idProceso
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridProcesosMeta();
                        refrescarGridProcesosInterno(idProceso);   
                        cargarComboProcesosMeta();
                        mensajesDelSistema("Se actualiz� el meta proceso", '250', '150', true);                      
                        
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar el meta proceso!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }else{
         mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');      
    }
    
}

function anularMetaProceso(cl){
    var url = './controller?estado=Procesos&accion=Meta';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 15,            
            idProceso: cl
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridProcesosMeta();
                    cargarComboProcesosMeta();
                    mensajesDelSistema("Se anul� el meta proceso", '250', '150', true);
                    $('#div_procesosMeta').fadeOut();                    
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular el meta proceso!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function existeUsuarioRelMetaProceso(id){
    $.ajax({
        url: './controller?estado=Procesos&accion=Meta',
        datatype:'json',
        type:'post',
        data:{
            opcion: 22,
            tipo:"META",
            idProceso:id
        },          
        success: function(json) {
            if (!isEmptyJSON(json)) {
     
                if (json.respuesta === "SI") {               
                    mensajeConfirmAnulacion('Alerta!!! Existen usuarios asociados al Meta proceso, desea continuar?','350','165', id);                    
                }else{
                    anularMetaProceso(id);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo realizar el proceso!!", '250', '150');
            }                 
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }  
    });
}

function crearProcesoInterno(){
     var idMetaProceso=$('#idMetaEdit').val();
     $('#idProMeta').val(idMetaProceso);     
     $('#nomProceso').val('');   
     $('#div_procesoInterno').fadeIn("slow");
     AbrirDivCrearProcesoInterno();
}

function AbrirDivCrearProcesoInterno(){
      $("#div_procesoInterno").dialog({
        width: 675,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Adicionar": function() {
                guardarProcesoInterno();
            },
            "Salir": function() {
                $(this).dialog("destroy");
            }
        }
    });
}

function cargarProcesosInternos(){
    var idProcesoMeta = $('#idMetaEdit').val();
    var url = './controller?estado=Procesos&accion=Meta&opcion=4&procesoMeta='+idProcesoMeta;
    if ($("#gview_procesosInternos").length) {
         refrescarGridProcesosInterno(idProcesoMeta);
     }else {
        jQuery("#procesosInternos").jqGrid({
            caption: 'Procesos Internos',
            url: url,
            datatype: 'json',
            height: 330,
            width: 620,
            colNames: ['Id', 'Id Proceso Meta', 'Mega Proceso',  'Proceso Interno', 'Descripcion','Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key:true},
                {name: 'id_tabla_rel', index: 'id_tabla_rel', hidden:true},
                {name: 'descripcionTablaRel', index: 'descripcionTablaRel', sortable: true, align: 'center', width: '600px'},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600px'},
                {name: 'descripcion', index: 'descripcion', hidden:true, sortable: true, align: 'center', width: '600px'},               
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '200px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager:'#page_tabla_procesos_internos',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {
                    var ids = jQuery("#procesosInternos").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        ed = "<img src='/fintra/images/link_new.png' align='absbottom'  name='editar' id='editar' width='16' height='16' title ='Asociar unidades'  onclick=\"editarProcesoInterno('" + cl + "');\">";
                        us = "<img src='/fintra/images/botones/iconos/users.png' align='absbottom'  name='asociar' id='asociar' width='16' height='16' title ='Asociar usuarios'  onclick=\"asociarUsuarioProceso('" + cl + "');\">";
//                      an = "<img src='/fintra/images/botones/iconos/anular.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el proceso interno seleccionado?','250','150',existeUsuarioRelProcesoInterno,'" + cl + "');\">";
                        an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='16' height='16' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan usuarios asociados al proceso interno, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
                        jQuery("#procesosInternos").jqGrid('setRowData', ids[i], {actions:ed+'   '+us+'   '+an});

                    }
                },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_procesos_internos",{edit:false,add:false,del:false});
        jQuery("#procesosInternos").jqGrid("navButtonAdd", "#page_tabla_procesos_internos", {
            caption: "Nuevo", 
            title: "Agregar nuevo proceso interno",           
            onClickButton: function() {
               crearProcesoInterno(); 
            }
        });
     }
}

function refrescarGridProcesosInterno(id){   
    var url = './controller?estado=Procesos&accion=Meta&opcion=4&procesoMeta='+id;
    jQuery("#procesosInternos").setGridParam({
        datatype: 'json',
        url: url
    });    
    jQuery('#procesosInternos').trigger("reloadGrid");
}

function guardarProcesoInterno(){
    var idProcesoMetaAct = $('#idMetaEdit').val();
    var idProMeta = $('#idProMeta').val();
    var nomProceso = $('#nomProceso').val();
    var descProceso = $('#descProceso').val();
    var url = './controller?estado=Procesos&accion=Meta';
    if(idProMeta!=='' && nomProceso!=='' && descProceso!==''){
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 5,
                    nombre: nomProceso,
                    descripcion: descProceso,
                    procesoMeta: idProMeta
                },
                success: function(json) {
                    
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }
                        
                        if (json.respuesta === "OK") {                            
                            mensajesDelSistema("Se cre� el proceso interno", '250', '150', true);                            
                            $('#nomProceso').val(''); 
                            $('#descProceso').val(''); 
                            refrescarGridProcesosInterno(idProcesoMetaAct);                            
                        }                       
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear el proceso interno", '250', '150');
                    }                         
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }      
            });
    }else{
         mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');      
    }
}


function anularProcesoInterno(cl){
    var idProcesoMeta = $('#idMetaEdit').val();
    $.ajax({
        url: './controller?estado=Procesos&accion=Meta',
        datatype:'json',
        type:'get',
        data:{
            opcion: 16,
            idProinterno: cl           
        },          
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridProcesosInterno(idProcesoMeta);  
                    mensajesDelSistema("Se anul� el proceso interno", '250', '150', true);                         
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular el proceso interno!!", '250', '150');
            }
            
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }  
    });
}

function editarProcesoInterno(id){
    var nomProinterno = jQuery('#procesosInternos').getCell(id, 'nombre');
    var descProinterno = jQuery('#procesosInternos').getCell(id, 'descripcion');
    var idMetaProceso = jQuery('#procesosInternos').getCell(id, 'id_tabla_rel');
    $('#nomProinterno').val(nomProinterno);
    $('#descProinterno').val(descProinterno);
    $('#idProMetaEdit').val(idMetaProceso);
    $('#idProinterno').val(id);
    $('#div_editar_procesoInterno').fadeIn();
    $('#div_und_prointerno').fadeIn(); 
    AbrirDivEditarProcesoInterno();
    listarUndNegocioPro(id);
    listarUndNegocio();
}

function AbrirDivEditarProcesoInterno(){
      $("#div_editar_procesoInterno").dialog({
        width: 800,
        height: 700,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'EDITAR PROCESO INTERNO',
        closeOnEscape: false,
        buttons: { 
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }/*,
        close: function(event, ui){ 
            $('#div_editar_procesoInterno').fadeOut('slow');
            $('#div_undNegocios').fadeOut();       
            $('#bt_asociar_undPro').hide();
            $('#bt_desasociar_undPro').hide(); 
        }*/
    });
}

function listarUndNegocioPro(id){
    var url = './controller?estado=Procesos&accion=Meta&opcion=8&idProinterno='+id;
     if ($("#gview_UndNegocioPro").length) {
         refrescarGridUndNegocioPro(id);
     }else {
         jQuery("#UndNegocioPro").jqGrid({
            caption: 'Unidades de Negocio del Proceso Interno',
            url: url,           
            datatype: 'json',
            height: 380,
            width: 300,
            colNames: ['Id', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden:true, align: 'center', width: '100px', key:true},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'left', width: '600px'}/*,
                {name: 'eliminar', index: 'eliminar', align: 'center', width: '150px'}*/
           ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect:true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {      
                    $('#bt_asociar_undPro').show();
                },
            loadError: function(xhr, status, error) {
               mensajesDelSistema(error, 250, 150);
            }
        });
      
    }    
}

function refrescarGridUndNegocioPro(id){
    var url = './controller?estado=Procesos&accion=Meta&opcion=8&idProinterno='+id;
    jQuery("#UndNegocioPro").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#UndNegocioPro').trigger("reloadGrid");
}

function listarUndNegocio(){
    var idProInterno = $('#idProinterno').val();
    var url = './controller?estado=Procesos&accion=Meta&opcion=6&idProceso='+idProInterno;
    $('#div_undNegocios').fadeIn();
    $('#bt_listar_undPro').hide();
    $('#bt_asociar_undPro').show();
    if ($("#gview_UndadesNegocios").length) {
         refrescarGridUndNegocio(idProInterno);
    }else {
        jQuery("#UndadesNegocios").jqGrid({
                caption: 'Unidades de Negocio',
                url: url,
                datatype: 'json',
                height: 380,
                width: 300,
                colNames: ['Id', 'Descripcion'],
                colModel: [
                    {name: 'id', index: 'id', sortable: true, hidden:true, align: 'center', width: '100px', key:true},
                    {name: 'descripcion', index: 'descripcion', sortable: true, align: 'left', width: '600px'}
               ],
                rowNum: 1000,
                rowTotal: 50000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                multiselect:true,
                jsonReader: {
                    root: 'rows',
                    repeatitems: false,
                    id: '0'
                },
                gridComplete: function() {                 
                    $('#bt_desasociar_undPro').show();
                },
                loadError: function(xhr, status, error) {
                   mensajesDelSistema(error, 250, 150);
                }
            });           
    }
}

function refrescarGridUndNegocio(id){
    var url = './controller?estado=Procesos&accion=Meta&opcion=6&idProceso='+id;
    jQuery("#UndadesNegocios").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#UndadesNegocios').trigger("reloadGrid");
}

function actualizarProInterno(){
    var idProcesoMetaAct = $('#idMetaEdit').val();
    var idProinterno = $('#idProinterno').val();
    var nomProinterno = $('#nomProinterno').val();
    var descProinterno = $('#descProinterno').val();
    var idProMeta = $('#idProMetaEdit').val();
    if(idProMeta!=='' && nomProinterno!=='' && descProinterno!==''){
        $.ajax({
            url: './controller?estado=Procesos&accion=Meta',
            datatype: 'json',
            type: 'post',
            data: {
                opcion: 9,
                idProinterno: idProinterno,
                nomProinterno: nomProinterno,
                descProinterno: descProinterno,
                idProMeta: idProMeta
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridProcesosInterno(idProcesoMetaAct);
                        mensajesDelSistema("El proceso se actualiz� correctamente!!", '250', '150', true);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar el proceso interno!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }else{
          mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }
   
}

function desasociarUndProinterno(){
    
    var idProinterno = $('#idProinterno').val();
    var listado = "";
    var filasId =jQuery('#UndNegocioPro').jqGrid('getGridParam', 'selarrrow');
    if(filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        $.ajax({
            url: './controller?estado=Procesos&accion=Meta',
            datatype:'json',
            type:'post',
            data:{
                opcion: 10,
                listado: listado,
                idProinterno: idProinterno
            },       
            success: function(json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }

                        if (json.respuesta === "OK") {                         
                           refrescarGridUndNegocioPro(idProinterno);
                           refrescarGridUndNegocio(idProinterno);
                        }

                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo desasignar el convenio del proceso interno", '250', '150');
                    }

            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }else{
        if (jQuery("#UndNegocioPro").jqGrid('getGridParam', 'records')>0) {
           mensajesDelSistema("Debe seleccionar los convenios a desasignar", '250', '150');
        } else {
            mensajesDelSistema("No hay convenios por desasignar", '250', '150');
        }         
    }
}

function asociarUndProinterno(){
    var idProinterno = $('#idProinterno').val();
    var listado = "";
    var filasId =jQuery('#UndadesNegocios').jqGrid('getGridParam', 'selarrrow');
    if(filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        
        var url = './controller?estado=Procesos&accion=Meta';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 7,
                listado: listado,
                idProinterno: idProinterno
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {                       
                        $('#div_procesoInterno').fadeOut();
                        refrescarGridUndNegocioPro(idProinterno);
                        refrescarGridUndNegocio(idProinterno);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar las unidades de negocio al proceso interno!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            }      
        });
        
    }else{
         if (jQuery("#UndadesNegocios").jqGrid('getGridParam', 'records')>0) {
            mensajesDelSistema("Debe seleccionar los convenios a asociar", '250', '150');
        } else {
            mensajesDelSistema("No hay convenios por asociar", '250', '150');
        }         
    }
}

function cargarComboProcesosMeta() {
    $('#idProMeta').html('');
    $('#idProMetaEdit').html('');;
    $.ajax({
        type: 'POST',
        url: './controller?estado=Procesos&accion=Meta',
        dataType: 'json',
        data: {
            opcion: 17
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#idProMeta').append("<option value=''>Seleccione</option>");
                    $('#idProMetaEdit').append("<option value=''>Seleccione</option>");
               
                    for (var key in json) {
                        $('#idProMeta').append('<option value=' + key + '>' + json[key] + '</option>');   
                        $('#idProMetaEdit').append('<option value=' + key + '>' + json[key] + '</option>');   
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                  $('#idProMeta').append("<option value=''>Seleccione</option>");
                  $('#idProMetaEdit').append("<option value=''>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function asociarUsuarioProceso(id){    
       var nomProinterno = jQuery('#procesosInternos').getCell(id, 'nombre');
       var metaProceso = jQuery('#procesosInternos').getCell(id, 'descripcionTablaRel');  
       var descProinterno = jQuery('#procesosInternos').getCell(id,'descripcion');        
       $('#idProinternoAsoc').val(id);
       $('#nomProinternoAsoc').val(nomProinterno);
       $('#nommetaproceso').val(metaProceso);
       $('#descProinternoAsoc').val(descProinterno);
       $('#div_asoc_user_prointerno').fadeIn("slow");
       $('#div_user_prointerno').fadeIn();
       AbrirDivAsociarUsuarioProInterno();
       listarUsuariosProInterno(id);
       listarUsuariosRelProInterno();
}

function AbrirDivAsociarUsuarioProInterno(){
      $("#div_asoc_user_prointerno").dialog({
        width: 850,
        height: 675,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ASOCIAR USUARIOS A PROCESOS INTERNOS',
        closeOnEscape: false,
        buttons: { 
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }/*,
        close: function(event, ui){ 
            $('#div_asoc_user_prointerno').fadeOut('slow');
            $('#div_usuarios').fadeOut();       
            $('#bt_asociar_userPro').hide();
            $('#bt_desasociar_userPro').hide();
        }*/
    });
}

function listarUsuariosProInterno(idProceso) {
   
    if ($("#gview_userProInterno").length) {
        reloadGridUsersProInterno(idProceso);
    } else {
        $("#userProInterno").jqGrid({
            caption: 'Usuarios relacionados con el proceso',
            url: './controller?estado=Procesos&accion=Meta&opcion=18&idProceso='+idProceso,       
            cellsubmit: "clientArray",
            datatype: 'json',
            height:  384,
            width: 'auto',
            cellEdit: true,
           // editurl: 'clientArray',
            colNames: ['C�digo', 'Id usuario', 'Nombre', 'Moderador'],
            colModel: [             
                {name: 'codUsuario', index: 'codUsuario', hidden:true, sortable: true, align: 'center', width: '100px', key: true},
                {name: 'idusuario', index: 'idusuario', hidden:true, sortable: true, align: 'center', width: '100px'},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '250px'},
                {name: 'moderador', index: 'moderador', sortable: true, editable:true, align: 'center', width: '80px'}             
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            afterSaveCell: function(rowid, celname, value, iRow, iCol) {                
                if (celname === 'moderador') {
                    var idProinterno = $('#idProinternoAsoc').val();
                    var idusuario = $("#userProInterno").getRowData(rowid).codUsuario;
                    var moderador = $("#userProInterno").getRowData(rowid).moderador;    
                    
                    if ((value.toUpperCase() !== "S" && value.toUpperCase() !== "N")) {
                       
                        mensajesDelSistema("Especifique S o N, segun sea el caso", 250, 150);
                        $("#userProInterno").jqGrid("restoreCell", iRow, iCol);
                        return;

                    } else {
                        actualizarModerador(idProinterno, idusuario, moderador.toUpperCase());
                    }
                }
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {                                      
                    $('#bt_asociar_userPro').show();  
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
        
    }
}

function reloadGridUsersProInterno(idProceso){
    var url = './controller?estado=Procesos&accion=Meta&opcion=18&idProceso='+idProceso;
    jQuery("#userProInterno").setGridParam({
        datatype: 'json',
        url: url
    });    
    jQuery('#userProInterno').trigger("reloadGrid");
}

function listarUsuariosRelProInterno(){
    var idProceso = $('#idProinternoAsoc').val();
    $('#div_usuarios').fadeIn();
    $('#bt_listar_userPro').hide();
    $('#bt_asociar_userPro').show();
    idsOfSelectedRows = [];
    if ($("#gview_listUsuarios").length) {
        reloadGridUsuariosRelProInterno(idProceso);
    } else {
            var cbColModel;          
            jQuery("#listUsuarios").jqGrid({
                caption: 'Usuarios',
                url: './controller?estado=Procesos&accion=Meta&opcion=19&idProceso=' + idProceso,
                datatype: 'json',
                height: 368,
                width: 'auto',
                colNames: ['Cod Usuario', 'Nombre Usuario', 'Usuario Login'],
                colModel: [
                    {name: 'codUsuario', index: 'codUsuario', hidden:true, align: 'center', width: '100px', key: true},
                    {name: 'nombre', index: 'nombre', align: 'left', width: '300px'},
                    {name: 'idusuario', index: 'idusuario', hidden:true, align: 'center', width: '200px'}
                ],
                rowNum: 1000,
                rowTotal: 50000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                ignoreCase: true,
                multiselect: true,
                jsonReader: {
                    root: 'rows',
                    repeatitems: false,
                    id: '0'
                },
                onSelectRow: function (id, isSelected) {
                    var p = this.p, item = p.data[p._index[id]], i = $.inArray(id, idsOfSelectedRows);
                    item.cb = isSelected;
                    if (!isSelected && i >= 0) {
                        idsOfSelectedRows.splice(i,1); // remove id from the list
                    } else if (i < 0) {
                        idsOfSelectedRows.push(id);
                    }                    
                },     
                loadComplete: function () {
                    var p = this.p, data = p.data, item, $this = $(this), index = p._index, rowid, i, selCount;
                    for (i = 0, selCount = idsOfSelectedRows.length; i < selCount; i++) {
                        rowid = idsOfSelectedRows[i];
                        item = data[index[rowid]];
                        if ('cb' in item && item.cb) {
                            $this.jqGrid('setSelection', rowid, false);
                        }
                    }
                },
                gridComplete: function() {                 
                    $('#bt_desasociar_userPro').show();
                },
                loadError: function(xhr, status, error) {
                    mensajesDelSistema(error, 250, 150);
                }
            });
            cbColModel = jQuery("#listUsuarios").jqGrid('getColProp', 'cb');
            cbColModel.sortable = true;
            cbColModel.sorttype = function (value, item) {
                return 'cb' in item && item.cb ? 1 : 0;
            };
            jQuery("#listUsuarios").jqGrid('filterToolbar',
                    {
                        autosearch: true,
                        searchOnEnter: true,
                        defaultSearch: "cn",
                        stringResult: true,
                        ignoreCase: true,
                        multipleSearch: true
                    });                    
       
    }
}

function reloadGridUsuariosRelProInterno(idProceso){
    var url = './controller?estado=Procesos&accion=Meta&opcion=19&idProceso='+idProceso;
    jQuery("#listUsuarios").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#listUsuarios").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    
    jQuery('#listUsuarios').trigger("reloadGrid");
}

function asociarUsuariosProinterno(){
    
    var idProinterno = $('#idProinternoAsoc').val();
    var jsonObj = [];
    var filasId =jQuery('#listUsuarios').jqGrid('getGridParam', 'selarrrow');
    if (filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            var id = filasId[i];
            var j = $.inArray(id, idsOfSelectedRows);
            idsOfSelectedRows.splice(j,1); // remove id from the list
            var idUsuario = jQuery("#listUsuarios").getRowData(id).idusuario;
            var item = {};
            item ["cod_usuario"] = id;
            item ["id_usuario"] = idUsuario;
            jsonObj.push(item);
        }
       var listUser = {};
       listUser ["usuarios"] = jsonObj;
      
        var url = './controller?estado=Procesos&accion=Meta';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 20,
                listado: JSON.stringify(listUser),
                idProInterno: idProinterno
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {                                           
                        reloadGridUsersProInterno(idProinterno);                       
                        reloadGridUsuariosRelProInterno(idProinterno);
                                          
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar los usuarios al proceso interno!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            } 
        });
        
    }else{
        if (jQuery("#listUsuarios").jqGrid('getGridParam', 'records')>0) {
             mensajesDelSistema("Debe seleccionar los usuarios a asociar al proceso interno!!", '250', '150');
        }else{
             mensajesDelSistema("No hay usuarios por asociar", '250', '150');
        }
             
    }
}



function desasociarUsuariosProinterno(){
    var idProinterno = $('#idProinternoAsoc').val();
    var listado = "";
    var filasId =jQuery('#userProInterno').jqGrid('getGridParam', 'selarrrow');
    if(filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }   
        $.ajax({
            url: './controller?estado=Procesos&accion=Meta',
            datatype:'json',
            type:'post',
            data:{
                opcion: 21,
                listado: listado,
                idProinterno: idProinterno
            },      
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {                     
                        reloadGridUsersProInterno(idProinterno);
                        reloadGridUsuariosRelProInterno(idProinterno);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo desasignar los usuarios del proceso interno!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            }          
        });
    }else{      
        if (jQuery("#userProInterno").jqGrid('getGridParam', 'records')>0) {           
            mensajesDelSistema("Debe seleccionar los usuarios a desasignar", '250', '150');
        } else {
            mensajesDelSistema("No hay usuarios por desasignar", '250', '150');
        }        
    }
      
    
}

function actualizarModerador(idProinterno, idusuario, moderador ){
    var url = './controller?estado=Procesos&accion=Meta';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 27,               
                idProinterno: idProinterno,
                idusuario: idusuario,
                moderador: moderador
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {                                           
                        reloadGridUsersProInterno(idProinterno);   
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar informaci�n del moderador!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            } 
        });
}

function existeUsuarioRelProcesoInterno(id){
    $.ajax({
        url: './controller?estado=Procesos&accion=Meta',
        datatype:'json',
        type:'post',
        data:{
            opcion: 22,
            tipo:"PINI",
            idProceso:id
        },          
        success: function(json) {
            if (!isEmptyJSON(json)) {
     
                if (json.respuesta === "SI") {
                    mensajeConfirmAnulacion('ALERTA!!! Existen usuarios asociados al proceso interno, desea continuar?','350','165', id);      
                }else{
                     anularProcesoInterno(id);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo realizar el proceso!!", '250', '150');
            }                 
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }  
    });
}


function verProcesosAnulados(cl){
    
    $('#div_procesos_internos_anul').fadeIn("slow");
    var fila = jQuery("#procesosMeta").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];  
    $('#idMetaAnul').val(cl);
    $('#nommetaprocesoAnul').val(nombre);
    $('#descmetaprocesoAnul').val(descripcion);
    AbrirDivPronternosAnulados();
    cargarProInternosAnulados(cl);
}

function AbrirDivPronternosAnulados(){
      $("#div_procesos_internos_anul").dialog({
        width: 700,
        height: 675,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'INFORMACION DEL META PROCESO',
        closeOnEscape: false,
        buttons: {    
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function cargarProInternosAnulados(id){   
    var url = './controller?estado=Procesos&accion=Meta&opcion=25&procesoMeta='+id;
    if ($("#gview_procesos_internos_anul").length) {
         refrescarGridProInternosAnulados();
     }else {
        jQuery("#procesos_internos_anul").jqGrid({
            caption: 'Procesos Internos',
            url: url,
            datatype: 'json',
            height: 330,
            width: 450,
            colNames: ['Id',  'Id Proceso Meta', 'Mega Proceso', 'Proceso Interno', 'Descripcion', 'Activar'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key:true},
                {name: 'id_tabla_rel', index: 'id_tabla_rel', hidden:true},
                {name: 'descripcionTablaRel', index: 'descripcionTablaRel', sortable: true, align: 'center', width: '600px'},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600px'},
                {name: 'descripcion', index: 'descripcion', hidden:true, sortable: true, align: 'center', width: '600px'},                
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '200px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager:'#page_tabla_procesos_internos_anul',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },   
            gridComplete: function() {
                    var ids = jQuery("#procesos_internos_anul").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        act = "<img src='/fintra/images/botones/iconos/check-blue.png' align='absbottom'  name='activar' id='activar' width='15' height='15' title ='Activar proceso interno'  onclick=\"mensajeConfirmAction('Esta seguro de activar el proceso interno seleccionado?','250','150',activarProcesoInterno,'" + cl + "');\">";
                        jQuery("#procesos_internos_anul").jqGrid('setRowData', ids[i], {actions:act});
                       
                    }
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
     }
}

function refrescarGridProInternosAnulados(){   
    var idMeta = $('#idMetaAnul').val();
    var url = './controller?estado=Procesos&accion=Meta&opcion=25&procesoMeta='+idMeta;
    jQuery("#procesos_internos_anul").setGridParam({
        datatype: 'json',
        url: url
    });    
    jQuery('#procesos_internos_anul').trigger("reloadGrid");
}

function activarProcesoInterno(cl){
    var url = './controller?estado=Procesos&accion=Meta';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 26,            
            idProinterno: cl
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridProInternosAnulados();
                    mensajesDelSistema("El proceso interno fue activado", '250', '150', true);                   
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo activar el proceso interno!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}



function mensajeConfirmAnulacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj").html("<span style='background: url(/fintra/images/warning03.png); height:32px; width:32px; float: left; margin: 0 7px 20px 0'></span> " + msj );
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);               
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajeConfirmAction(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    mostrarContenido('dialogMsgMeta');
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}
