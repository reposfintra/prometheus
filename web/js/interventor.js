/*
 * Created by: Ing. Jos� Castro
 * and open the template in the editor.
 */


// JavaScript Document
var estado=4;

/**
* Metodo que retorna el objecto ajax
* @autor: Jcastro
* @Solicitud: creaci�n de categorias
* @fecha: 15-07-2010
*/
function objetoAjax(){
    var xmlhttp = null;
    try{
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e){
        try{
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (ex){
            xmlhttp = null;
        }
    }

    if (!xmlhttp && typeof XMLHttpRequest != 'undefined'){
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}


/**
    * Metodo que retorna el cuerpo del mensaje mientras esta cargando una accion
    * @parameter: msg, BASEURL
    * @autor: Jcastro
    * @Solicitud:  Plantilla Cotizaciones
    * @fecha: 16-06-2010
*/
function loading(msg, BASEURL){
    var html = "";
    html += "<table border=\"1\" align=\"center\">";
    html += "<tr>";
    html += "<td>";
    html += "<table width=\"400\" height=\"50\"  align=\"center\"  bordercolor=\"#F7F5F4\" bgcolor=\"#FFFFFF\">";
    html += "<tr>";
    html += "<td align=\"center\"><img src=\""+BASEURL+"/images/cargando.gif\" border=\"0\" align=\"absmiddle\"></td>";
    html += "<td class=\"mensajes\" align=\"center\">"+msg+"</td>";
    html += "</tr>";
    html += "</table>";
    html += "</td>";
    html += "</tr>";
    html += "</table>";
    return(html);
}

/**
	* Metodo que retorna el cuerpo del mensaje
	* @parameter: msg, BASEURL
	 * @autor: Jcastro
	* @Solicitud: Plantilla Cotizaciones
	* @fecha: 16-06-2010
	*/
function message(msg, BASEURL){
    var html = "";
    html += "<table border=\"2\" align=\"center\">";
    html += "<tr>";
    html += "<td>";
    html += "<table width=\"400\" height=\"73\" border=\"1\" align=\"center\"  bordercolor=\"#F7F5F4\" bgcolor=\"#FFFFFF\">";
    html += "<tr>";
    html += "<td width=\"360\" align=\"center\" class=\"mensajes\">"+msg+"</td>";
    html += "<td width=\"40\" background=\""+BASEURL+"/images/cuadronaranja.JPG\">&nbsp;</td>";
    html += "</tr>";
    html += "</table>";
    html += "</td>";
    html += "</tr>";
    html += "</table>";
    return(html);
}



/**
 * Comment
 */
function name0() {
    alert("prueba");
}


/**
 *
 */
function buscarInterventores(CONTROLLER,idcargado, opt){

    var interventor = document.getElementById("interventor");

    if ((interventor != "")){
        var ajax = objetoAjax();
        var parametros = "opcion=0";

        ajax.open("POST", CONTROLLER+"?estado=Interventor&accion=Orden", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){
                interventor.options.length = 0;
                interventor.options[0] = new Option("Cargando...", "");
                interventor.disabled = true;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    var cat = ajax.responseXML.getElementsByTagName("interventor");

                    if (cat[0].childNodes[0].nodeValue != "null"){
                        cargarInterventor(interventor, cat, idcargado);
                       
                        buscarDepartamentos(CONTROLLER,idcargado, 'listadepartamentos', 'dptos');

                }
                    else{
                        interventor.options.length = 0;
                        interventor.options[0] = new Option("Seleccione un Interventor", "");
                        interventor.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }

}



function cargarInterventor(combobox, cod, idcargado){

    var option ;
    try{
        if (cod.length > 1){
            combobox.disabled = false;
            var i = 0;

            for (i; i<cod.length; i++){

                var nomente = cod[i].childNodes[0].nodeValue;
                var codente = cod[i].getAttribute("codente");

                if(codente==idcargado){
                    option = new Option(nomente, codente, codente,"selected");
                }else{
                    if(((codente==0)&&(idcargado!=""))||((codente==0)&&idcargado == undefined) ){
                        option = new Option(nomente, codente, codente,"selected");

                    }else{
                        option = new Option(nomente, codente, codente, "");
                    }
                }
                combobox.options[i] = option;
            }
        }
        else{
            var option = new Option("No hay Interventor", "");
            combobox.options[0] = option;
        }
    }catch (e){
        alert("error___________"+e);

    }

}



//LISTADO DE DEPARTAMENTOS SEGUN CONDICION

/**
 *
 */
function buscarDepartamentos(CONTROLLER,cod_intercargado, op, valp){



     borradorOptions(document.getElementById('dptos'));
     borradorOptions(document.getElementById('dpto_asignados'));

if((cod_intercargado=="0")||(cod_intercargado=="")){
    
     borradorOptions(document.getElementById('dptos'));
     borradorOptions(document.getElementById('dpto_asignados'));

}else{

    

    var departamento = document.getElementById(valp);

    if ((departamento != "")){
        var ajax = objetoAjax();
        var parametros = "opcion=1&op="+op+"&cod_inter="+cod_intercargado;

        ajax.open("POST", CONTROLLER+"?estado=Interventor&accion=Orden", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){
                departamento.options.length = 0;
                departamento.options[0] = new Option("Cargando...", "");
                departamento.disabled = true;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    var cat = ajax.responseXML.getElementsByTagName("departamento");

                    if (cat[0].childNodes[0].nodeValue != "null"){
                        cargarDepartamentos(departamento, cat, cod_intercargado);
                        buscarDepartamentosAsig(CONTROLLER,cod_intercargado, 'listadepartamentosasociados', 'dpto_asignados');
                }
                    else{
                        departamento.options.length = 0;
                        departamento.options[0] = new Option("Seleccione un Departamento", "");
                        departamento.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }

}

    //buscarDepartamentosAsig(CONTROLLER,cod_intercargado, 'listadepartamentosasociados', 'dpto_asignados');
    //buscarDepartamentosAsig(CONTROLLER,cod_intercargado, op, valp);


}



function cargarDepartamentos(combobox, cod, idcargado){



    var option ;
 try{
        if (cod.length > 0){
            borradorOptions(document.getElementById('dptos'));
            combobox.disabled = false;
var i = 0;

for (i; i<cod.length; i++){

                var nomente = cod[i].childNodes[0].nodeValue;
                var codente = cod[i].getAttribute("codente");
                if(codente==idcargado){

                    option = new Option(nomente, codente, codente,"selected");
                }else{
                    if(((codente==0)&&(idcargado!=""))||((codente==0)&&idcargado == undefined) ){

                   option = new Option(nomente, codente, codente,"selected");

                    }else{
                        option = new Option(nomente, codente, codente);
                    }
                }
                combobox.options[i] = option;
            }
        }
        else{
            borradorOptions(document.getElementById('dptos'));
            var option = new Option("No hay Departamentos", "");
            combobox.options[0] = option;
        }
    }catch (e){
        alert("error___________"+e);

    }

}


////-------------------------------


/**
 *
 */
function buscarDepartamentosAsig(CONTROLLER,cod_intercargado, op, valp){



    var departamento = document.getElementById(valp);

    if ((departamento != "")){
        var ajax = objetoAjax();
        var parametros = "opcion=1&op="+op+"&cod_inter="+cod_intercargado;

        ajax.open("POST", CONTROLLER+"?estado=Interventor&accion=Orden", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){
                departamento.options.length = 0;
                departamento.options[0] = new Option("Cargando...", "");
                departamento.disabled = true;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    var cat = ajax.responseXML.getElementsByTagName("departamento");
                    
                    
                    if (cat[0].childNodes[0].nodeValue != "null"){

                    cargarDepartamentosAsig(departamento, cat, cod_intercargado);

                }
                    else{
                        departamento.options.length = 0;
                        departamento.options[0] = new Option("Seleccione un Departamento", "");
                        departamento.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }

}



function cargarDepartamentosAsig(combobox, cod, idcargado){



    var option ;
 try{
        if (cod.length > 0){
            borradorOptions(document.getElementById('dpto_asignados'));
            combobox.disabled = false;
var i = 0;

for (i; i<cod.length; i++){

                var nomente = cod[i].childNodes[0].nodeValue;
                var codente = cod[i].getAttribute("codente");
                if(codente==idcargado){

                    option = new Option(nomente, codente, codente,"selected");
                }else{
                    if(((codente==0)&&(idcargado!=""))||((codente==0)&&idcargado == undefined) ){

                   option = new Option(nomente, codente, codente,"selected");

                    }else{
                        option = new Option(nomente, codente, codente);
                    }
                }
                combobox.options[i] = option;
            }
        }
        else{
            borradorOptions(document.getElementById('dpto_asignados'));
            var option = new Option("No hay Departamentos", "");
            combobox.options[0] = option;
        }
    }catch (e){
        alert("error___________"+e);

    }

}





function borradorOptions(selection){
        var ind = 0;
        var tm = selection.length;
        var elim = tm-1;
        while(ind<tm){
                selection.remove(elim);
                elim = elim - 1;
                ind = ind + 1;
        }
}



        function move(cmbO, cmbD){



//alert("D______"+cmbD.length);

           for (i=cmbD.length-1; i>=0 ;i--)
            if (cmbD[i].selected){
               addOption(cmbD, cmbD[i].value, cmbD[i].text)
               cmbD.remove(i);
            }
           order(cmbD);
           deleteRepeat(cmbD);
        }


    function addOption(Comb,valor,texto){
        var Ele = document.createElement("OPTION");
        Ele.value=valor;
        Ele.text=texto;
        Comb.add(Ele);
    }



function order(cmb){
    for (i=0; i<cmb.length;i++)
        for (j=i+1; j<cmb.length;j++)
            if (cmb[i].text > cmb[j].text){
                var temp = document.createElement("OPTION");
                temp.value = cmb[i].value;
                temp.text  = cmb[i].text;
                cmb[i].value = cmb[j].value;
                cmb[i].text  = cmb[j].text;
                cmb[j].value = temp.value;
                cmb[j].text  = temp.text;
            }
}


        function deleteRepeat(cmb){
          var ant='';
          for (i=0;i<cmb.length;i++){
             if(ant== cmb[i].value)
                cmb.remove(i);
             ant = cmb[i].value;
          }
        }

        cmbO, cmbD

 function pasarIzqDer() {

	obj=document.getElementById('dptos');
	if (obj.selectedIndex==-1) return;
  for (i=0; opt=obj.options[i]; i++)
    if (opt.selected) {
    	valor=opt.value; // almacenar value
    	txt=obj.options[i].text; // almacenar el texto
    	obj.options[i]=null; // borrar el item si esta seleccionado
    	obj2=document.getElementById('dpto_asignados');
      if (obj2.options[0].value=='-') // si solo está la opcion inicial borrarla
        obj2.options[0]=null;
    	opc = new Option(txt,valor);
    	eval(obj2.options[obj2.options.length]=opc);
  }
orderDepto(obj);
orderDepto(obj2);
}


 function pasarDerIzq() {
         
	obj=document.getElementById('dpto_asignados');
	if (obj.selectedIndex==-1) return;
  for (i=0; opt=obj.options[i]; i++)
    if (opt.selected) {
    	valor=opt.value; // almacenar value
    	txt=obj.options[i].text; // almacenar el texto
    	obj.options[i]=null; // borrar el item si esta seleccionado
    	obj2=document.getElementById('dptos');
      if (obj2.options[0].value=='-') // si solo está la opcion inicial borrarla
        obj2.options[0]=null;
    	opc = new Option(txt,valor);
    	eval(obj2.options[obj2.options.length]=opc);
  }
orderDepto(obj);
orderDepto(obj2);

}


function orderDepto(cmb){

    for (i=0; i<cmb.length;i++)
        for (j=i+1; j<cmb.length;j++)
            if (cmb[i].text > cmb[j].text){
                var temp = document.createElement("OPTION");
                temp.value = cmb[i].value;
                temp.text  = cmb[i].text;
                cmb[i].value = cmb[j].value;
                cmb[i].text  = cmb[j].text;
                cmb[j].value = temp.value;
                cmb[j].text  = temp.text;
            }
}




function saveSels(CONTROLLER, BASEURL){



    var interventor = document.getElementById("interventor");

    

 
    if((interventor.value!="")&&(interventor.value!="0")){
        var sels;
        sels = [];
        obj=document.getElementById('dpto_asignados');

        var param = "opcion=2&cod_inter="+interventor.value;

        var opts = obj.options, e = 0;
        param += "&tam_items="+opts.length;
  
        for (var i=0 ;i < opts.length; i++){
            sels[i] = opts[i].value;
            param += "&iddpto"+i+"="+sels[i];
        }
        //alert(param);

        //-----------------

                                if (estado == 4){

                            var ajax = objetoAjax();
                            ajax.open("POST", CONTROLLER+"?estado=Interventor&accion=Orden", true);
                            ajax.onreadystatechange = function(){
                                if (ajax.readyState == 1){
                                    celda.style.display = "block";
                                    celda.innerHTML = loading("Cargando informacion...", BASEURL);
                                    estado = 1;
                                }
                                else if (ajax.readyState == 4){
                                    if( ajax.status == 200){
                                        estado = 4;
                                        alert("Datos Guardados con Exito...");
                                        //location.href = BASEURL+ajax.responseText;
                                        buscarDepartamentos(CONTROLLER,interventor.value, 'listadepartamentos', 'dptos')


                                    }
                                }
                            }
                            ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                            ajax.send(param);
                        }
                        else{
                            alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
                        }
        //-----------------


    }else{

        alert("Seleccione un Interventor");

    }


}





    
//obj2=document.getElementById('dptos');
//var sels2;
//sels2 = [];
//var opts2 = obj2.options, e = 0;
//    for (var i=0 ;i < opts2.length; i++){
//            sels2[i] = opts2[i].value;
//    param = param+"_"+sels2[i];
//
//    }
//    alert(param);
//    document.getElementById("TotalDist").value= sels.join("|");






///////--------------------------PROCESO DE SOLICITUES INTERVENTORES


//LISTADO DE DEPARTAMENTOS SEGUN CONDICION

/**
 *
 */
function buscarSolicitudes(CONTROLLER,cod_intercargado, op, valp){





if(cod_intercargado=="0"){
    
     borradorOptions(document.getElementById('dptos'));
     borradorOptions(document.getElementById('dpto_asignados'));

}else{

    var departamento = document.getElementById(valp);

    if ((departamento != "")){
        var ajax = objetoAjax();
        var parametros = "opcion=3&op="+op+"&cod_inter="+cod_intercargado;

        ajax.open("POST", CONTROLLER+"?estado=Interventor&accion=Orden", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){
                departamento.options.length = 0;
                departamento.options[0] = new Option("Cargando...", "");
                departamento.disabled = true;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                   // alert("after200");
                    var cat = ajax.responseXML.getElementsByTagName("sol_cliente");
//alert("after200___________  "+cat[0].childNodes[0].nodeValue);
                    if (cat[0].childNodes[0].nodeValue != "null"){
                        cargarSolicitudes(departamento, cat, cod_intercargado);
                        buscarSolicitudesAsig(CONTROLLER,cod_intercargado, 'listadepartamentosasociados', 'dpto_asignados');
                }
                    else{
                        departamento.options.length = 0;
                        departamento.options[0] = new Option("Seleccione un Departamento", "");
                        departamento.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }

}

    //buscarDepartamentosAsig(CONTROLLER,cod_intercargado, 'listadepartamentosasociados', 'dpto_asignados');
    //buscarDepartamentosAsig(CONTROLLER,cod_intercargado, op, valp);


}



function cargarSolicitudes(combobox, cod, idcargado){

//alert("cargarSolicitudes");

    var option ;
 try{
        if (cod.length > 0){
            borradorOptions(document.getElementById('dptos'));
            combobox.disabled = false;
var i = 0;

for (i; i<cod.length; i++){

                var nomente = cod[i].childNodes[0].nodeValue;
                var codente = cod[i].getAttribute("codente");
                if(codente==idcargado){

                    option = new Option(nomente, codente, codente,"selected");
                }else{
                    if(((codente==0)&&(idcargado!=""))||((codente==0)&&idcargado == undefined) ){

                   option = new Option(nomente, codente, codente,"selected");

                    }else{
                        option = new Option(nomente, codente, codente);
                    }
                }
                combobox.options[i] = option;
            }
        }
        else{
            borradorOptions(document.getElementById('dptos'));
            var option = new Option("No hay Departamentos", "");
            combobox.options[0] = option;
        }
    }catch (e){
        alert("error___________"+e);

    }

}


////-------------------------------


/**
 *
 */
function buscarSolicitudesAsig(CONTROLLER,cod_intercargado, op, valp){

    var departamento = document.getElementById(valp);
    //alert("op_____________"+op);

    if ((departamento != "")){
        var ajax = objetoAjax();
        var parametros = "opcion=3&op="+op+"&cod_inter="+cod_intercargado;

        ajax.open("POST", CONTROLLER+"?estado=Interventor&accion=Orden", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){
                departamento.options.length = 0;
                departamento.options[0] = new Option("Cargando...", "");
                departamento.disabled = true;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    var cat = ajax.responseXML.getElementsByTagName("sol_cliente");


                    if (cat[0].childNodes[0].nodeValue != "null"){

                    cargarSolicitudesAsig(departamento, cat, cod_intercargado);

                }
                    else{
                        departamento.options.length = 0;
                        departamento.options[0] = new Option("Seleccione un Departamento", "");
                        departamento.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }

}



function cargarSolicitudesAsig(combobox, cod, idcargado){



    var option ;
 try{
        if (cod.length > 0){
            borradorOptions(document.getElementById('dpto_asignados'));
            combobox.disabled = false;
var i = 0;

for (i; i<cod.length; i++){

                var nomente = cod[i].childNodes[0].nodeValue;
                var codente = cod[i].getAttribute("codente");
                if(codente==idcargado){

                    option = new Option(nomente, codente, codente,"selected");
                }else{
                    if(((codente==0)&&(idcargado!=""))||((codente==0)&&idcargado == undefined) ){

                   option = new Option(nomente, codente, codente,"selected");

                    }else{
                        option = new Option(nomente, codente, codente);
                    }
                }
                combobox.options[i] = option;
            }
        }
        else{
            borradorOptions(document.getElementById('dpto_asignados'));
            var option = new Option("No hay Departamentos", "");
            combobox.options[0] = option;
        }
    }catch (e){
        alert("error___________"+e);

    }

}

//---------------SAVE SOLICITUDES


function saveSelSolicitudes(CONTROLLER, BASEURL){

    var interventor = document.getElementById("interventor");

    if((interventor.value!="")&&(interventor.value!="0")){
        var sels;
        sels = [];
        obj=document.getElementById('dpto_asignados');

        var param = "opcion=4&cod_inter="+interventor.value;

        var opts = obj.options, e = 0;
        param += "&tam_items="+opts.length;

        for (var i=0 ;i < opts.length; i++){
            sels[i] = opts[i].value;
            param += "&iddpto"+i+"="+sels[i];
        }
        //-----------------
                                if (estado == 4){

                            var ajax = objetoAjax();
                            ajax.open("POST", CONTROLLER+"?estado=Interventor&accion=Orden", true);
                            ajax.onreadystatechange = function(){
                                if (ajax.readyState == 1){
                                    celda.style.display = "block";
                                    celda.innerHTML = loading("Cargando informacion...", BASEURL);
                                    estado = 1;
                                }
                                else if (ajax.readyState == 4){
                                    if( ajax.status == 200){
                                        estado = 4;
                                        alert("Datos Guardados con Exito...");
                                        //location.href = BASEURL+ajax.responseText;
                                        buscarSolicitudes(CONTROLLER,interventor.value, 'listadepartamentos', 'dptos')


                                    }
                                }
                            }
                            ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                            ajax.send(param);
                        }
                        else{
                            alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
                        }
        //-----------------


    }else{
        alert("Seleccione un Interventor");
    }

}




//------------------


 function pasarIzqDerSol() {

	obj=document.getElementById('dptos');
	if (obj.selectedIndex==-1) return;
  for (i=0; opt=obj.options[i]; i++)
    if (opt.selected) {
    	valor=opt.value; // almacenar value
    	txt=obj.options[i].text; // almacenar el texto
    	obj.options[i]=null; // borrar el item si esta seleccionado
    	obj2=document.getElementById('dpto_asignados');
      if (obj2.options[0].value=='-') // si solo está la opcion inicial borrarla
        obj2.options[0]=null;
    	opc = new Option(txt,valor);
    	eval(obj2.options[obj2.options.length]=opc);
  }
//orderDepto(obj);
orderDepto(obj2);
}


 function pasarDerIzqSol() {

	obj=document.getElementById('dpto_asignados');
	if (obj.selectedIndex==-1) return;
  for (i=0; opt=obj.options[i]; i++)
    if (opt.selected) {
    	valor=opt.value; // almacenar value
    	txt=obj.options[i].text; // almacenar el texto
    	obj.options[i]=null; // borrar el item si esta seleccionado
    	obj2=document.getElementById('dptos');
      if (obj2.options[0].value=='-') // si solo está la opcion inicial borrarla
        obj2.options[0]=null;
    	opc = new Option(txt,valor);
    	eval(obj2.options[obj2.options.length]=opc);
  }
orderDepto(obj);
//orderDepto(obj2);

}


function buscarInterventoresSoli(CONTROLLER,idcargado){


    var interventor = document.getElementById("interventor");


    if ((interventor != "")){
        var ajax = objetoAjax();
        var parametros = "opcion=0";

        ajax.open("POST", CONTROLLER+"?estado=Interventor&accion=Orden", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){
                interventor.options.length = 0;
                interventor.options[0] = new Option("Cargando...", "");
                interventor.disabled = true;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    var cat = ajax.responseXML.getElementsByTagName("interventor");

                    if (cat[0].childNodes[0].nodeValue != "null"){
                        cargarInterventor(interventor, cat, idcargado);
                        buscarSolicitudes(CONTROLLER,idcargado, 'listadepartamentos', 'dptos');

                }
                    else{
                        interventor.options.length = 0;
                        interventor.options[0] = new Option("Seleccione un Interventor", "");
                        interventor.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }

}



//----------------------Asignar Interventor a la Solicitud


function asignarInterventor(CONTROLLER, id_solicitud){

    if (estado == 4){
        if(id_solicitud.value != ""){

            var param = "opcion=5&id="+id_solicitud;
            var ajax = objetoAjax();
            ajax.open("POST", CONTROLLER+"?estado=Interventor&accion=Orden", true);
            ajax.onreadystatechange = function(){

if (ajax.readyState == 1){
                    estado = 1;
                }
                else if (ajax.readyState == 4){
                    if( ajax.status == 200){
                        estado = 4;

//                        var value = ajax.responseXML.getElementsByTagName("respuesta");
//
//                        if (value[0].childNodes[0].nodeValue == "null"){
//                            alert("Este material no tiene un precio asignado");
//                        }
//                        else{
//
//                        var val = value[0].childNodes[0].nodeValue;
//                        val = formatNumber(val);
//                        document.getElementById("precio"+index).value = val;
//                        }
                    }
                }
            }
            ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            ajax.send(param);
        }
//    }
//    else{
//        alert("Un momento Por Favor Los Datos Estan Siendo consultados en el servidor...");
    }
}