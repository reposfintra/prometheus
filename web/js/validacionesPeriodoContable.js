function updFechas(){
    var anio=document.form1.anio.value;
    var mes=document.form1.mes.value;
    if(anio != "0" && mes != "0"){
        document.form1.fpiva.value=anio+"-"+mes+"-01";
        document.form1.fpret.value=anio+"-"+mes+"-01";
        document.form1.fpcom.value=anio+"-"+mes+"-01";
        document.form1.fpren.value=anio+"-"+mes+"-01";
    }    
}

function validateFechas(){
    var anio=document.form1.anio.value;
    var mes=document.form1.mes.value;
    
    
        var f1 = document.form1.fpiva.value.substring(0,7);
        var f2 = document.form1.fpret.value.substring(0,7);
        var f3 = document.form1.fpcom.value.substring(0,7);
        var f4 = document.form1.fpren.value.substring(0,7);
        var fecha = anio+"-"+mes;

        var iva = document.getElementById("iva").checked;
        var ret = document.getElementById("retencion").checked;
        var com = document.getElementById("comercio").checked;
        var ren = document.getElementById("renta").checked;
        
        if(f1 != fecha && iva ){
            alert("El a�o y mes del Impuesto IVA no coincide con la fecha del Periodo");
            return false;
        }
        if(f2 != fecha && ret ){
            alert("El a�o y mes del Impuesto RETENCION no coincide con la fecha del Periodo");
            return false;
        }
        if(f3 != fecha && com ){
            alert("El a�o y mes del Impuesto COMERCIO no coincide con la fecha del Periodo");
            return false;
        }
        if(f4 != fecha && ren ){
            alert("El a�o y mes del Impuesto RENTA no coincide con la fecha del Periodo");
            return false;
        }    
    return true;
}

function disableFecha(val, id){    
   
   if(id == 1){ 
       if(val.value == "N"  ){
           document.form1.fpiva.disabled=true;           
       }
       else{
           document.form1.fpiva.disabled=false;           
       }
   }
   else if(id == 2){ 
       if(val.value == "N"){
           document.form1.fpret.disabled=true;
       }
       else{
           document.form1.fpret.disabled=false;
       }
   }
   else if(id == 3){ 
       if(val.value == "N"){
           document.form1.fpcom.disabled=true;
       }
       else{
           document.form1.fpcom.disabled=false;
       }
   }
   else{
       if(val.value == "N"){
           document.form1.fpren.disabled=true;
       }
       else{
           document.form1.fpren.disabled=false;
       } 
   }
}

