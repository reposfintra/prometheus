function _onmousemove(item) { item.className='select';   }
function _onmouseout (item) { item.className='unselect'; }

function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPagina(url, nombrePagina)
{
  var wdth = screen.width - screen.width * 0.20;
  var hght = screen.height - screen.height * 0.40;
  var lf = screen.width * 0.1;
  var tp = screen.height * 0.2;
  var options = "menubar=yes,scrollbars=yes,resizable=yes,status=yes,titlebar=no," +
                "toolbar=no," + setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( document.window != null && !hWnd.opener )
    hWnd.opener = document.window;
}

function SelAll(){
    for(i=0;i<FormularioListado.length;i++)
       FormularioListado.elements[i].checked=FormularioListado.All.checked;
}
function ActAll(){
    FormularioListado.All.checked = true;
    for(i=0;i<FormularioListado.length;i++)	
      if (FormularioListado.elements[i].type=='checkbox' && !FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All'){
          FormularioListado.All.checked = false;
          break;
      }
} 
function validarListado(form){
    for(i=0;i<FormularioListado.length;i++)	
        if (FormularioListado.elements[i].type=='checkbox' && FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All')
            return true;
    alert('Por favor seleccione un item para poder continuar');
    return false;
}   
function redireccionar(url){
  abrirPagina(url, "wWebVuImpl");
}
