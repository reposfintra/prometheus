/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


 $(document).ready(function() {
        
   if ($('#div_configPrecioProd').is(':visible')) {
        listarConfigProd('tbl_config_prices',"N");
    }
    
   $('.solo-numeric').live('keypress',function (event) {
       return numbersonly(this,event);
    });
    
    $('.solo-numeric').live('blur',function (event) {
           this.value = numberConComas(this.value);            
    });        
    
    $('#btn_config_prices').click(function(){
        actualizarProductPrice();
    });
 });


 function listarConfigProd(id,preload) {  
        var token =  localStorage.getItem("token");
        $.ajax({
            type: "POST",
            crossDomain: true,
            dataType: "json",            
            contentType: "application/x-www-form-urlencoded",
            headers: {'token': token },
            data: {'data': JSON.stringify({
                    'preload': preload
            })},
            async: false,
            url: 'http://prometheus.fintra.co:8094/ApiServerEds/webresources/configurar/producto', 
            success: function(jsonData) {
                    if(jsonData.success===true ){
                        var json = jsonData.info.data; 
                        $('#' + id).html('');
                        $('#' + id).append('<tr><th>Id</th> <th width="60%" align="left">Nombre Producto</th> <th>Precio</th> </tr>');
                        for (var key in json) {
                            var ro = (json[key].id_producto == 1) ? "readOnly" : "";
                            $('#' + id).append('<tr>');
                            $('#' + id + ' tr:last').append('<td align="center">' + json[key].id_producto + '</td>');
                            $('#' + id + ' tr:last').append('<td align="left">' + json[key].nombre_producto + '</td>');
                            $('#' + id + ' tr:last').append('<td align="right">$ <input name="price" type="text" id=' + 'price_' + json[key].id_producto + ' value= ' + numberConComas(json[key].precio_producto) + ' ' + ro + ' class="solo-numeric" ></td>');
                           // $('#price_' + json[key].id_producto).onkeypress = function(evt){alert('entro')/*return numbersonly(this, evt);*/};
                            $('#' + id).append('</tr>');
                        }                   
                    } else {
                        mensajesDelSistema(jsonData.message.error, '320', '165');
                    }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });                    
  }
  
 function actualizarProductPrice() {
    var token = localStorage.getItem("token");
    $.ajax({
        type: "PUT",
        crossDomain: true,
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        headers: {'token': token},
        data: {'data': actualizarPrecioToJson()},
        async: false,
        url: 'http://prometheus.fintra.co:8094/ApiServerEds/webresources/configurar/precio',
        success: function(jsonData) {
            if (jsonData.success === true) {
                mensajesDelSistema(jsonData.info.data, '320', '165', true);
                setTimeout(function() {
                    if ($('#div_configIniPrecioProd').is(':visible')) {
                        obtenerMenu("#menu-pagina");
                        $("#div_configIniPrecioProd").dialog('close');
                    }else{
                        listarConfigProd('tbl_config_prices',"N");
                    }     
                }, 1000);   
            } else {
                mensajesDelSistema(jsonData.message.data, '320', '165');
            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
        
}

function actualizarPrecioToJson() { 
        var formId = ($('#div_configPrecioProd').is(':visible')) ? 'tbl_config_prices': 'tbl_config_ini_prices';
        var jsonObj = [];
        $('#' + formId + ' tbody tr').each(function() {
            if ($(this).find("td:first").length > 0) {
                var id = $(this).find("td").eq(0).html();
                var precio = $(this).find("td input")[0].value;
                var item = {};
                item ["id_producto"] = id;
                item ["precio_producto"] = numberSinComas(precio);
                jsonObj.push(item);           
            }
        });    
        return JSON.stringify(jsonObj); 
  } 


function ventanaConfigIniProd() {    
    $("#div_configIniPrecioProd").dialog({
        width: 550,
        height: 400,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,       
        closeOnEscape: true, 
        dialogClass: "no-close",
        buttons: {          
            "Actualizar": function () {                
                actualizarProductPrice();                	
            },
            "Salir":function(){
                obtenerMenu("#menu-pagina");
                $("#div_configIniPrecioProd").dialog('close');
            }
        }
    });
   
}

function validarProductPrice(){    
    var formId = ($('#div_configPrecioProd').is(':visible')) ? 'tbl_config_prices' : 'tbl_config_ini_prices';  
    $('#' + formId + ' tbody tr').each(function() {
        if ($(this).find("td:first").length > 0) {          
            var precio = $(this).find("td input")[0].value;
            if (!validarPrice(precio)){
                return false;               
            }          
        }
    });    
    return true;
}

function validarPrice(price) {
    var filter = /^[1-9]\d*(\.\d+)?$/;
    return filter.test(price);
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function numberSinComas(x) {
    return x.toString().replace(",", "");
}

function numbersonly(myfield, e, dec)
{
    var key;
    var keychar;
    
    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

// control keys
    if ((key == null) || (key == 0) || (key == 8) ||
            (key == 9) || (key == 13) || (key == 27))
        return true;

// numbers
    else if ((("0123456789").indexOf(keychar) > -1))
        return true;

// decimal point jump
    else if (dec && (keychar == "."))
    {
        myfield.form.elements[dec].focus();
        return false;
    }
    else
        return false;
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: "Mensaje",
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}
