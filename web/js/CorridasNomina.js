/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function init() {
    
    var date =new Date();
    var anio = date.getFullYear();
    $("#anio").val(anio);
    
    buscarBanco();

    var tabla = jQuery("#documentos_tabla");
    tabla.jqGrid({
        caption: "Facturas nomina",
        datatype: 'local',
        width: 1750,
        height: 500,
        rowNum: 1000,
        pager: $('#documentos_page'),
        viewrecords: true,
        gridview: true,
        hidegrid: true,
        shrinkToFit: true,
        loadonce: true,
        rownumbers: true,
        multiselect: true,
        footerrow: true,
        colNames: ['Distrito', 'Base', 'Tipo documento', 'Factura', 'Nit', 'Proveedor', 'Valor', 'Aprobador',
            'Numero cuenta', 'Tipo cuenta', 'Cedula cuenta', 'Nombre cuenta', 'Banco Trans', 'Sucursal Trans',
            'Moneda', 'Agencia', 'Descripcion', 'Planilla',
            'Tipo pago', 'Banco', 'Sucursal', 'Placa'],
        colModel: [
            {name: 'distrito', index: 'distrito', hidden: true},
            {name: 'base', index: 'base', hidden: true},
            {name: 'tipodoc', index: 'tipodoc', hidden: true},
            {name: 'factura', index: 'factura', key: true, sortable: true},
            {name: 'nit', index: 'nit', sortable: true, align: 'center'},
            {name: 'proveedor', index: 'proveedor', sortable: true},
            {name: 'saldo', index: 'saldo', sortable: true, formatter: 'number', align: 'right'},
            {name: 'usuario_aprobacion', index: 'usuario_aprobacion', sortable: true},
            {name: 'no_cuenta', index: 'no_cuenta', sortable: true, align: 'right', hidden: true},
            {name: 'tipo_cuenta', index: 'tipo_cuenta', sortable: true, hidden: true},
            {name: 'cedula_cuenta', index: 'cedula_cuenta', sortable: true, hidden: true},
            {name: 'nombre_cuenta', index: 'nombre_cuenta', sortable: true, hidden: true},
            {name: 'banco_transfer', index: 'banco_transfer', sortable: true, hidden: true},
            {name: 'suc_transfer', index: 'suc_transfer', sortable: true, hidden: true},
            {name: 'moneda', index: 'moneda', hidden: true},
            {name: 'agencia', index: 'agencia', hidden: true},
            {name: 'descripcion', index: 'descripcion', hidden: true},
            {name: 'planilla', index: 'planilla', hidden: true},
            {name: 'tipo_pago', index: 'tipo_pago', hidden: true},
            {name: 'banco', index: 'banco', hidden: true},
            {name: 'sucursal', index: 'sucursal', hidden: true},
            {name: 'placa', index: 'placa', hidden: true}
        ]
    });
    tabla.jqGrid('gridResize', {minWidth: 1750, minHeight: 500});
}

function sumaTotales(tabla){
    var saldo=tabla.jqGrid('getCol', 'saldo', false, 'sum');
    
    tabla.jqGrid('footerData', 'set', {
        saldo: saldo
    });
}

function getFacturas() {
    var tabla = jQuery("#documentos_tabla")
      , fil = jQuery("#fil_gerencia").val();
    var periodo=jQuery("#anio").val()+jQuery("#mes").val();
    if(jQuery("#mes").val()==='')return ;
    
    if(fil !== '' && jQuery("#anio").val()!=='' && jQuery("#mes").val() ) {
        
        tabla.setGridParam({
            url: '/fintra/controller?estado=Corridas&accion=Nomina&opcion=0&filGerencia='+fil+'&periodo='+periodo,
            mtype: 'get',
            datatype: 'json',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: 'factura'
            },
            loadComplete: function () {
                sumaTotales(tabla);
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });
        tabla.trigger("reloadGrid");
    }else{
        
        alert("dsasaas");
    }
}

function generarEgresos() {
    //var filas = jQuery("#documentos_tabla").jqGrid('getRowData');
    var myGrid = jQuery("#documentos_tabla"), i, selRowIds = myGrid.jqGrid("getGridParam", "selarrrow"), n, filas=[];
    for (i = 0, n = selRowIds.length; i < n; i++) {
        filas[i] = myGrid.jqGrid("getLocalRow", selRowIds[i]);
    }
    if(filas.length === 0) {
        alert("no hay filas seleccionadas"); return;       
    }
    $("#lui_documentos_tabla,#load_documentos_tabla").show();
    var banco = jQuery("#banco_trans").val();
    var sucursal = jQuery("#sucursal_trans").val();

    $.ajax({
        url: '/fintra/controller?estado=Corridas&accion=Nomina&opcion=1',
        datatype: 'json',
        type: 'post',
        data: {informacion: JSON.stringify({banco: banco, sucursal: sucursal, datos: filas})},
        async: true,
        success: function (json) {
            try {
                if (json.mensaje) {
                    alert(json.mensaje);
                    return;
                }
                cargarPrevio(json);
            } finally {
                $("#lui_documentos_tabla,#load_documentos_tabla").hide();
            }
        },
        error: function (error) {
            alert(error);
            $("#lui_documentos_tabla,#load_documentos_tabla").hide();
        }
    });
}

function generarPrevio() {
    var fil = jQuery("#fil_gerencia").val();
    var banco = jQuery("#banco_trans").val();
    var sucursal = jQuery("#sucursal_trans").val();
    if(fil === '') return;
    $.ajax({
        url: '/fintra/controller?estado=Corridas&accion=Nomina&opcion=4',
        datatype: 'json',
        type: 'post',
        data: {informacion: JSON.stringify({banco: banco, sucursal: sucursal, filGerencia: fil})},
        async: true,
        success: function (json) {
            try {
                if (json.mensaje) {
                    alert(json.mensaje);
                    return;
                }
                cargarPrevio(json);
            } finally {
                $("#lui_documentos_tabla,#load_documentos_tabla").hide();
            }
        },
        error: function (error) {
            alert(error);
            $("#lui_documentos_tabla,#load_documentos_tabla").hide();
        }
    });
}

function buscarBanco() {
    var banco = jQuery("#banco_trans");
    $.ajax({
        url: '/fintra/controller?estado=Corridas&accion=Nomina&opcion=2',
        datatype: 'json',
        type: 'get',
        data: {informacion: JSON.stringify({banco: ''})},
        async: false,
        success: function (json) {
            try {
                if (json.datos && json.datos.length > 0) {
                    banco.html('');
                    for (var i in json.datos) {
                        banco.append("<option value='" + json.datos[i] + "'>" + json.datos[i] + "</option>");
                    }
                    buscarSucursal();
                }
            } finally {
            }
        },
        error: function (error) {
            alert(error);
        }
    });
}

function buscarSucursal() {
    var banco = jQuery("#banco_trans");
    var sucursal = jQuery("#sucursal_trans");

    $.ajax({
        url: '/fintra/controller?estado=Corridas&accion=Nomina&opcion=2',
        datatype: 'json',
        type: 'get',
        data: {informacion: JSON.stringify({banco: banco.val()})},
        async: false,
        success: function (json) {
            try {
                sucursal.html('');
                for (var i in json.datos) {
                    sucursal.append("<option value='" + json.datos[i] + "'>" + json.datos[i] + "</option>");
                }
            } finally {
            }
        },
        error: function (error) {
            alert(error);
        }
    });
}

function cargarPrevio(json) {
    $("#capaPreviaExportacion").dialog({
        width: 1000,
        height: 800,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });
    if (json) {
        $("#banco_trans_prev").html(json.cabecera.banco);
        $("#sucursal_trans_prev").html(json.cabecera.sucursal);
        $("#cuenta_trans_prev").html(json.cabecera.cuenta);
        $("#nit_trans_prev").html(json.cabecera.nit);

        $("#documentos_prev tbody").html('');
        var tr, cantidad = 0, suma = 0;
        for (var fila in json.rows) {
            cantidad++;
            suma += parseInt(json.rows[fila].valor);
            tr = '<tr><td>' 
               + json.rows[fila].egreso + '</td><td>' 
               + json.rows[fila].nit + '</td><td>' 
               + json.rows[fila].nombre + '</td><td title="'
               + json.rows[fila].banco_cod +'">' 
               + json.rows[fila].banco_nom + '</td><td style="text-align: right;">' 
               + json.rows[fila].cuenta + '</td><td style="text-align: right;">' 
               + json.rows[fila].valor.replace(/\d(?=(\d{3})+\.)/g, '$&,') + '</td></tr>'
            $("#documentos_prev tbody").append(tr);
        }

        $("#cant_doc").html(cantidad);
        $("#sum_doc").html(suma.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
    }
}

function generarArchivo() {
    var datos = [];
    
    var cuerpo = document.getElementById("documentos_prev");
    var fila;
    
    for(var i = 1; i < cuerpo.rows.length-1; i++) {
        fila = cuerpo.rows[i];
        datos[i-1] = {
             nit:fila.cells[1].innerHTML
            ,nombre:fila.cells[2].innerHTML.toUpperCase()
            ,banco_cod:fila.cells[3].title
            ,cuenta:fila.cells[4].innerHTML
            ,valor:fila.cells[5].innerHTML.replace(/,/g,'').replace('.','')
        };
    }        
    
    $.ajax({
        url: '/fintra/controller?estado=Corridas&accion=Nomina&opcion=3',
        datatype: 'json',
        type: 'post',
        data: { informacion: JSON.stringify({
                 cabecera: {
                     nit:$("#nit_trans_prev").html()
                    ,clase_trans:$("#tipo_pago").attr("title")
                    ,descr_trans:$("#descripcion").val().toUpperCase()
                    ,fecha_trans:$("#fec_transmision").val().replace(/-/g,'')
                    ,secuencia:($("#secuencia").val()).toUpperCase()
                    ,fecha_aplica:$("#fec_aplicacion").val().replace(/-/g,'')
                    ,total_filas:$("#cant_doc").html()
                    ,total_credito:$("#sum_doc").html().replace(/,/g,'').replace('.','')
                    ,cuenta:$("#cuenta_trans_prev").html()
                    ,codigo:$("#cod_trans_prev").html()
                    ,tipo_cuenta:$("#sucursal_trans_prev").html()
                 }
                , datos: datos
                })
              },
        async: false,
        success: function (json) {
            try {
                alert(json.mensaje);
            } finally {
            }
        },
        error: function (error) {
            alert(error);
        }
    });
}