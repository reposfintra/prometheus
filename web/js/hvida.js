/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller , baseurl
 * proposito: validar identidad
 */
$(document).ready(function() {    
    cargarVias('BQ', "via_princip_dir");
    cargarVias('BQ', "via_genera_dir");

    $("#ciu_dir").change(function() {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarVias(op, "via_princip_dir");
        cargarVias(op, "via_genera_dir");
    });
    $("#via_princip_dir").change(function() {
          $("#via_genera_dir").val('');
    });
 });
 
 function resetAddressValues(){
       $("#dir_resul").val('');
       $("#nom_princip_dir").val('');
       $("#nom_genera_dir").val('');
       $("#placa_dir").val('');
       $("#cmpl_dir").val('');
 }
 
function validarIdentidad(CONTROLLER,BASEURL){

    var campos = new Array("c_tipdoc","c_doc","c_nom1", "c_ape1", "c_ape2", "c_genero", "c_fecha","c_dir", "pais","est","ciudad" );
	var ncampos = new Array("Tipo Documento","Documento","Primer Nombre", "Primer Apellido", "Segundo Apellido", "Genero", "Fecha Nacimineto","Direccion", "Pais","Estado","Ciudad" );

    var c_empleado = new Array("c_cargo" );
	var nempleado = new Array("Cargo" );

	var c_conductor = new Array("c_tel11","c_tel12","c_tel13","c_cel");
	var nconductor = new Array("Telefono","Telefono","Telefono","Celular");

    var npropietario = new Array("Telefono","Telefono","Telefono");
	var c_propietario = new Array("c_tel11","c_tel12","c_tel13");

    var nproveedor = new Array("Tipo Documento","Documento","Nombre","Direccion","Telefono","Telefono","Telefono");
	var c_proveedor = new Array("c_tipdoc","c_doc","c_nom","c_dir","c_tel11","c_tel12","c_tel13");

	var carbon = new Array("c_tipdoc","c_doc","c_nom");
	var ncarbon = new Array("Tipo Documento","Documento","Nombre");
    var campo = "";

     if (!forma.empleado.checked && !forma.conductor.checked && !forma.propietario.checked && !forma.proveedor.checked && !forma.procarbon.checked){
		alert("Debe seleccionar una Clasificaci�n!");
		return (false);
	}

    //valido campos comunes de empleado,conductor, propietario
	if (forma.empleado.checked || forma.conductor.checked || forma.propietario.checked){
    	for (i = 0; i < campos.length; i++){
      		campo = campos[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+ncampos[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
        if(forma.c_nom2.value == ''){
	    	forma.c_nom2.value = ' ';
    	}
		//validar si es femenino masculino debe llenar libmilitar
	    if(forma.c_genero.value == 'M' && forma.c_libreta.value == '' && forma.empleado.checked ){
	    	alert("El Campo Libreta Militar esta vacio.!");
            forma.c_libreta.focus();
            return (false);
      	}

	}
	if (forma.empleado.checked){
    	for (i = 0; i < c_empleado.length; i++){
      		campo = c_empleado[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+nempleado[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	if (forma.conductor.checked){
    	for (i = 0; i < c_conductor.length; i++){
      		campo = c_conductor[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+nconductor[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	if (forma.propietario.checked){
    	for (i = 0; i < c_propietario.length; i++){
      		campo = c_propietario[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+npropietario[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	if (forma.proveedor.checked){
    	for (i = 0; i < c_proveedor.length; i++){
      		campo = c_proveedor[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+nproveedor[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	if (forma.procarbon.checked){
    	for (i = 0; i < carbon.length; i++){
      		campo = carbon[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+ncarbon[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}

    if(forma.c_tel21.value == '' && forma.c_tel22.value == '' && forma.c_tel23.value == ''){
   	     forma.c_tel21.value = ' ';
 	     forma.c_tel22.value = ' ';
   	     forma.c_tel23.value = ' ';
      }
      else{
          if(forma.c_tel21.value == ''){
      		alert('Digite el codigo del pais');
        	forma.c_tel21.focus();
	  	    return (false);
      	  }
          if(forma.c_tel22.value == ''){
      		alert('Digite el codigo de l area');
	        forma.c_tel22.focus();
	  	    return (false);
          }
      	  if(forma.c_tel23.value == ''){
      		alert('Digite el numero telefonico');
            forma.c_tel23.focus();
	  	    return (false);
          }
      }
 	  if  (forma.c_email.value != '') {
         if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(forma.c_email.value) ){}
		 else{
			alert("La direcci�n de email es incorrecta.");
   				return (false);
			}
      }
	  forma.submit();
}

/****************************
 * autor :campos
 * parametros:
 * proposito: validar la seleccion para la clasificacion
 */
function campos( insert ){
	if (forma.proveedor.checked){
		forma.empleado.disabled=true;
		forma.conductor.disabled=true;
		forma.propietario.disabled=true;
		forma.procarbon.disabled=true;
		//Oculto campos
		nombre.style.display="block";
		titnom.style.display="none";
		nombres.style.display="none";

                apellidos.style.display="none";
		otros.style.display="none";
		gen.style.display="none";
		senal.style.display="none";
	}
	else if (forma.procarbon.checked){
		forma.empleado.disabled=true;
		forma.conductor.disabled=true;
		forma.propietario.disabled=true;
		forma.proveedor.disabled=true;
		//Oculto campos
		nombre.style.display="block";
		titnom.style.display="none";
		nombres.style.display="none";
                apellidos.style.display="none";
		otros.style.display="none";
		gen.style.display="none";
		senal.style.display="none";
	}
	else {
	    if (forma.empleado.checked || forma.conductor.checked || forma.propietario.checked ){
//			alert("Empleado � Conductor � Propietario");
			forma.proveedor.disabled=true;
			forma.procarbon.disabled=true;
			nombre.style.display="none";
			titnom.style.display="block";
			nombres.style.display="block";
	        apellidos.style.display="block";
			otros.style.display="block";
			gen.style.display="block";
			senal.style.display="block";
			forma.c_nom.value='';
		}
		else{
			//alert("No tiene seleccion los 3");
			forma.proveedor.disabled=false;
			forma.procarbon.disabled=false;
		}
		if(forma.empleado.checked && forma.conductor.checked ){
			//alert("Empleado - Conductor");
			forma.propietario.disabled=true;
		}
		else if(forma.empleado.checked && forma.propietario.checked){
			//alert("Empleado - Propietario");
			forma.conductor.disabled=true;
		}
		else if(forma.conductor.checked && forma.propietario.checked){
			//alert("Conductor - Propietario");
			forma.empleado.disabled=true;
		}
		else{
			forma.propietario.disabled=false;
			forma.conductor.disabled=false;
			forma.empleado.disabled=false;
		}

	}

}
/****************************
 * autor :Diogenes Bastidas
 * parametros:  direccion
 * proposito: llamar a un action para cargal los selects|
 */
function cargarSelects ( dir ){
	forma.action = dir;
	forma.submit();
}
/****************************
 * autor :Diogenes Bastidas
 * parametros:  BASEURL , opcion
 * proposito: abrir una ventana donde puede escojer digitar los telefonos
 */
function modTelefono( BASEURL ,opc ) {
	if (opc == 1 ) {
		window.open(BASEURL+"/jsp/hvida/identidad/registrarTelefono.jsp?opc="+opc+"&codp="+forma.c_tel11.value+"&ind="+forma.c_tel12.value+"&num="+forma.c_tel13.value,'tel','status=yes,width=430,height=190,resizable=yes');
	}
	else{
		window.open(BASEURL+'/jsp/hvida/identidad/registrarTelefono.jsp?opc='+opc+'&codp='+forma.c_tel21.value+'&ind='+forma.c_tel22.value+'&num='+forma.c_tel23.value,'tel2','status=yes,width=430,height=190,resizable=yes');
	}
}
/****************************
 * autor :Diogenes Bastidas
 * parametros:  telefono
 * proposito: setear los campos correspondientes al formulario de identidad
 */
function setearTelefono(tel) {
    var telefono = new Array("cpais","ind","num");
	var ntelefono = new Array("Pais","Ciudad","Nro. Telefono");
	var campo = "";
	for (i = 0; i < telefono.length; i++){
      		campo = telefono[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+ntelefono[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
    }
    var formulario = parent.opener.document.forma;
	if (tel == 1){
		formulario.c_tel11.value = forma.cpais.value;
		formulario.c_tel12.value = forma.ind.value;
		formulario.c_tel13.value = forma.num.value;
	}
	else{
		formulario.c_tel21.value = forma.cpais.value;
		formulario.c_tel22.value = forma.ind.value;
		formulario.c_tel23.value = forma.num.value;
	}
    parent.close();
}

/****************************
 * autor :Diogenes Bastidas
 * parametros:  evento
 * proposito: validar el formulario de conductor CGA
 */
function Conductor(evento){
  var fecha = formulario.hoy.value;
  var campos = new Array("grupoSanguineo","rh","numeroPase", "categoriaPase", "VencePase", "respase", "Judicial", "VenceJudicial" );
  var ncampos = new Array("Grupo Sangu�neo","RH","N�mero Pase", "Categoria", "Fecha Vencimiento Pase", "Restricciones del pase", "Certificado Judicial", "Fecha Vencimiento Certificado Judicial");
  formulario.evento.value=evento;
  if ( evento == 'INSERT'  ||  evento == 'UPDATE' ) {
     for (i = 0; i < campos.length; i++){
      		campo = campos[i];
      		if (formulario.elements[campo].value == ''){
            	alert("El campo "+ncampos[i]+" esta vacio.!");
                formulario.elements[campo].focus();
                return (false);
                break;
            }
      }
  }

  if(formulario.VencePase.value!=''){
	 if(formulario.VencePase.value <= fecha){
	  alert("El campo vencimiento pase debe ser mayor a la actual!");
      formulario.VencePase.focus();
	  return (false);
	 }
  }
  if(formulario.VencePasaporte.value!=''){
	 if(formulario.VencePasaporte.value <= fecha){
	  alert("El campo vencimiento pasaporte debe ser mayor a la actual!");
      formulario.VencePasaporte.focus();
	  return (false);
	 }
  }
  if(formulario.VenceJudicial.value!=''){
	 if(formulario.VenceJudicial.value <= fecha){
	  alert("El campo vencimiento certificado judicial debe ser mayor a la actual!");
      formulario.VenceJudicial.focus();
	  return (false);
	 }
  }
  if(formulario.VenceVisa.value!=''){
	 if(formulario.VenceVisa.value <= fecha){
	  alert("El campo vencimiento visa debe ser mayor a la actual!");
      formulario.VenceVisa.focus();
	  return (false);
	 }
  }
  if(formulario.VenceLibTripulante.value!=''){
	 if(formulario.VenceLibTripulante.value <= fecha){
	  alert("El campo vencimiento libreta tripulante debe ser mayor a la actual!");
      formulario.VenceLibTripulante.focus();
	  return (false);
	 }
  }
  formulario.submit();
}

/****************************
 * autor :Diogenes Bastidas
 * parametros:  evento
 * proposito: validar el formulario de Buscar Identidad
 */
function BuscarIdentidad(){
  if(forma.c_documento.value=='' && forma.c_nombre.value=='' &&forma.c_cia.value=='' ){
	  alert("Debe digitar algun parametro para la Busqueda");
	  return (false);
  }
  forma.submit();
}




function busquedaHV(){
  if(form1.conductor.value=='' && form1.propietario.value=='' && form1.placa.value=='' ){
	  alert("Debe digitar algun parametro para la Busqueda");
	  return (false);
  }
  form1.submit();
}


function imgProveedor(){
	var img = document.getElementById('logo');
	var img1 = document.getElementById('logo1');
	var img2 = document.getElementById('logo2');
	var img3 = document.getElementById('logo3');
	var img4 = document.getElementById('logo4');
	if ( forma.pago.checked ){
        //Muestra las filas
        forma.tipo_pago.value = "T";
		titulo.style.display="block";
        fila1.style.display="block";
		fila2.style.display="block";
		fila3.style.display="block";
		fila4.style.display="block";
	}
	else{
        forma.tipo_pago.value = "B";
		titulo.style.display="none";
        fila1.style.display="none";
		fila2.style.display="none";
		fila3.style.display="none";
		fila4.style.display="none";

		/*forma.c_banco_transfer.value ='';
		forma.c_sucursal_transfer.value='';
		forma.c_tipo_cuenta.value = '';
		forma.c_numero_cuenta.value = '';
		forma.c_codciudad_cuenta.value = '';
		forma.cedula_cuenta.value = '';
		forma.nombre_cuenta.value = ''; */

	}

}

function Autoretenedor(){
	if ( forma.c_agente_retenedor.value == 'S' ){
        forma.c_autoretenedor_iva.value = "N";
        forma.c_autoretenedor_ica.value = "N";
        forma.c_autoretenedor_rfte.value = "N";
	    forma.c_autoretenedor_iva.disabled=true;
	    forma.c_autoretenedor_ica.disabled=true;
		forma.c_autoretenedor_rfte.disabled=true;
	}
	else{
		forma.c_autoretenedor_iva.disabled=false;
	    forma.c_autoretenedor_ica.disabled=false;
		forma.c_autoretenedor_rfte.disabled=false;
	}
}

/****************************
 * autor :Diogenes Bastidas
 * parametros:
 * proposito: validar el formulario Proveedor
 */
 function validarProveedor(){

  var campos = "";
  var ncampos = "";

  if ( !document.getElementById('pago').checked ){
	  campos = new Array("c_agency_id","c_autoretenedor_ica","c_branch_code","c_bank_account","nit_ben","c_clasificacion","handle_code","plazo","concept_code"  );
	  ncampos = new Array("Sede de Pago ","Autoretenedor ICA ","Banco","Agencia","Nit del beneficiario","Clasificacion","Handle Code","Plazo","Concepto" );
	  forma.tipo_pago.value = "B";
  }
  else{
          forma.tipo_pago.value = "T";
	  campos = new Array("tipo_proveedor","c_agency_id","c_autoretenedor_ica","c_branch_code","c_bank_account","nit_ben","c_clasificacion","handle_code","plazo","concept_code", "c_banco_transfer","c_sucursal_transfer", "c_tipo_cuenta","c_numero_cuenta","c_codciudad_cuenta", "cedula_cuenta", "nombre_cuenta"  );
	  ncampos = new Array("Tipo Proveedor","Sede de Pago ","Autoretenedor ICA ","Banco","Agencia","Nit del beneficiario","Clasificacion","Handle Code","Plazo","Concepto", "Banco Transferencia","Sucursal Transferencia" ,"Tipo de Cuenta", "No. Cuenta", "Ciudad", "Cedula Cuenta", "Nombre Cuenta" );
  }
     for (i = 0; i < campos.length; i++){
      		campo = campos[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+ncampos[i]+" esta vacio.");
                forma.elements[campo].focus();
                return (false);
                break;
            }
      }
      if(forma.pag.value=="S"){
          if (forma.codigo.value != ''&&forma.codigo1.value != ''&&forma.codigo2.value != ''){
            var cod=forma.codigo.value+forma.codigo1.value+forma.codigo2.value;
            if(cod.length<9){
             alert("El codigo debe ser de 9 digitos");
                forma.codigo.focus();
                return (false);
            }else{
            forma.action=forma.action+"&sede="+forma.sede.value+"&afil=S";
            }
          }else{
              alert("El campo codigo esta vacio.!");
                forma.codigo.focus();
                return (false);
          }
      }
  forma.submit();
}

function Posicionar_div(id_objeto,e){
  //alert(e);
  //alert(window.event);
  obj = document.getElementById(id_objeto);
  var posx = 0;
  var posy = 0;
  if (!e) var e = window.event;
  if (e.pageX || e.pageY) {
   //alert('page');
   posx = e.pageX;
   posy = e.pageY;
  }
  else if (e.clientX || e.clientY) {
        //alert('client'); 
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
      }
      else
       alert('ninguna de las anteriores');
 //alert('posx='+posx + ' posy=' + posy);
 obj.style.left = posx+'px';
 obj.style.top = posy+'px';
 //obj.css("margin")="192px 51px 506px 83px";
 
 //alert('objleft='+posx + ' objtop=' + posy);
 //document.getElementById('posicion').innerHTML = 'scrollLeft='+document.body.scrollLeft+' scrollTop='+document.body.scrollTop+' cientX'+e.clientX +' clientY'+e.clientY;
}


function genDireccion(elemento,e) {
//alert('entro');
dptoResidencia();
    var contenedor = document.getElementById("direccion_dialogo")
      , res = document.getElementById("dir_resul");
    $("#direccion_dialogo").draggable({ handle: "#drag_direcciones"});  
    Posicionar_div("direccion_dialogo",e);  //192px 51px 506px 83px
    contenedor.style.display = "block";
    
    res.name = elemento;    
    res.value = (elemento.value) ? elemento.value : '';/*
    $j("#direccion_dialogo").dialog({
        // open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
        width: "auto",
        height: "auto",
        show: "scale",
        hide: "scale",
        title: "Direccion",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {  
                $j('#'+elemento).val($j('#dir_resul').val());
                setDireccion(0);
             },
            "Salir": function () { 
                setDireccion(0);                
            }
        }
    });*/
}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3:
            var res = $('#dir_resul').val();
            $('#c_dir2').val(res);
            document.getElementById("direccion_dialogo").style.display = "none";
            break;
        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");

            document.getElementById("direccion_dialogo").style.display = "none";
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
              , g = document.getElementById('via_genera_dir')
              , opcion;
              //alert('DireccionPrincipal: '+p);
            for (var i = 0; i < g.length; i++) {
                
                opcion = g[i];
                
                if (opcion.value === p) {
                    
                    opcion.style.display = 'none';
                    opcion.disabled = true;
                    
                } else {
                    
                    opcion.disabled = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if(!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                || !jQuery('#placa_dir').val() ) {
                  jQuery('#dir_resul').val("");
                  jQuery('#dir_resul').attr("class","validation-failed");
              } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                    jQuery('#via_princip_dir option:selected').text()
                   + ' '  + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                   + ' '  + jQuery('#via_genera_dir option:selected').text().trim()
                   + ' '  + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                   + ((jQuery('#via_genera_dir option:selected').text().toUpperCase()==='#' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='TRANSVERSAL') ?  '-':  ' ')
                   + jQuery('#placa_dir').val().trim().toUpperCase()
                   + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
              }
            break;
    }
}

function dptoResidencia() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 56
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#dpto').html('');
                $('#dpto').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#dpto').append('<option value=' + json[i].department_code + '>' + json[i].department_name + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ciudad(department_code) {
    //var dpto_expedicion = $("#" + department_code).val();
    //var dpto_expedicion = $('#dpto_expedicion').val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 57,
            dpto: department_code

        },
        success: function (json) {
            //console.log(json[i].codciu);
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                
                
                $('#ciu_dir').html('');
                $('#ciu_dir').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    //alert(json[i].nomciu);
                    $('#ciu_dir').append('<option value=' + json[i].codciu + '>' + json[i].nomciu + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarVias(codciu) {
    //alert(codciu);
    $.ajax({
            async: false,
            type: 'POST',
            url: "/fintra/controller?estado=GestionSolicitud&accion=Aval",
            dataType: 'json',
            data: {
                opcion: 'cargarvias',
                ciu: codciu
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#via_princip_dir').empty();
                        $('#via_genera_dir').empty();
                        $('#via_princip_dir').append("<option value=''>...</option>");
                        $('#via_genera_dir').append("<option value=''>...</option>");

                        for (var key in json) {
                            $('#via_princip_dir').append('<option value=' + key + '>' + json[key] + '</option>');
                            $('#via_genera_dir').append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } 
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

/**
 * Carga las opciones departamento y ciudad para asignar el valor correspondiente
 * @param {string} dpto valor del departamento
 * @param {string} ciu valor de la ciudad
 */
function cargarDptCiudad(dpto, ciu) {
    var pais = $('#pais').val();
    var xhr;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhr.onreadystatechange = function () {
        if (xhr.status === 200 && xhr.readyState === 4) {
            document.getElementById("est_1").innerHTML = xhr.response;
            document.getElementById("est").value = dpto;
            //Cargar el select ciudad y seleccionar la opcion correspondiente
            var xhr2;
            if (window.XMLHttpRequest) {
                xhr2 = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                xhr2 = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xhr2.onreadystatechange = function () {
                if (xhr2.status === 200 && xhr2.readyState === 4) {
                    document.getElementById("ciudad_1").innerHTML = xhr2.response;
                    document.getElementById("ciudad").value = ciu;
                    cargarIndicativos(pais, ciu);
                }
            }
            xhr2.open("POST", "/fintra/controller?estado=Proveedor&accion=Ingresar&opcion=cargarciu&cod=" + dpto + "&pais=" + pais, true);
            xhr2.setRequestHeader("Accept", "text/html, text/plain");
            xhr2.send();
        }
    }
    xhr.open("POST", "/fintra/controller?estado=Proveedor&accion=Ingresar&opcion=cargarest&cod=" + pais, true);
    xhr.setRequestHeader("Accept", "text/html, text/plain");
    xhr.send();
}

/**
 * Carga los datos b�sicos de un cliente y los asignas en sus respectivos
 * campos del formulario
 * @author JDBERMUDEZ
 * @param {number} documento
 */
function cargarDatosPersona(documento) {
    if (documento){
        var xhr;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xhr.onreadystatechange = function () {
            if (xhr.status === 200 && xhr.readyState === 4) {
                var json = JSON.parse(xhr.response.trim());
                if (json.data === "Error") {
                    alert("Hubo error en el servidor, no se pudo procesar la petici�n.");
                } else if (json.data === 0) {
                    document.getElementById("c_nom").value = "";
                    document.getElementById("c_dir2").value = "";
                    document.getElementById("c_barrio").value = "";
                    document.getElementById("pais").selectedIndex = 0;
                    document.getElementById("est").selectedIndex = 0;
                    document.getElementById("ciudad").selectedIndex = 0;
                    document.getElementById("c_cel2").value = "";
                    document.getElementById("c_email").value = "";
                    document.getElementsByName("c_expe")[0].selectedIndex = 0;
                    document.getElementById("c_tel112").value = "";
                    document.getElementById("c_tel122").value = "";
                    document.getElementById("c_tel132").value = "";
                    alert("No se encontr� una identidad con el documento ingresado.");
                } else {
                    document.getElementById("pais").value = json.pais;
                    cargarDptCiudad(json.departamento, json.ciudad);
                    document.getElementById("c_nom").value = json.nombre;
                    document.getElementById("c_dir2").value = json.direccion;
                    document.getElementById("c_barrio").value = json.barrio;
                    document.getElementById("c_cel2").value = json.celular;
                    document.getElementById("c_email").value = json.email;
                    document.getElementsByName("c_expe")[0].value = json.ciudadExpedicion;                    
                    document.getElementById("c_tel132").value = json.telefono;                    
                }
            }
        }
        xhr.open("GET", "/fintra/controller?estado=GestionSolicitud&accion=Aval&opcion=buscarPersona&documento=" + documento, true);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.send();
    }
}

/**
 * Carga los indicativos de una ciudad para asignarlo en los
 * campos del formulario
 * @author JDBERMUDEZ
 * @param {string} pais codigo del pais 
 * @param {string} ciudad codigo de la ciudad 
 * @returns {undefined}
 */
function cargarIndicativos(pais, ciudad) {
    var xhr;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhr.onreadystatechange = function () {
        if (xhr.status === 200 && xhr.readyState === XMLHttpRequest.DONE) {
            var json = JSON.parse(xhr.response);
            document.getElementById("c_tel112").value = json.indicativoPais;
            document.getElementById("c_tel122").value = json.indicativoCiudad;
        }
    }
    xhr.open("GET", "/fintra/controller?estado=Identidad&accion=Buscarindicativo&opcion=obtenerIndicativo&pais=" + pais + "&ciudad=" + ciudad, true);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.send();
}