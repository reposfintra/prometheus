function buscarFacturas( controller, opcion, x, maxfila ) {
	var fact =document.getElementById("c_factura"+x);
	var factura = fact.value;
	if(factura=="") {		
		alert("Digite el Numero de la Factura");
	} else {
		sinCamposFormato("c_valor_rica",maxfila);
		sinCamposFormato("c_valor_rfte",maxfila);
		sinCamposFormato("c_valor_total_factura",maxfila);
		document.form1.action += "&opcion="+opcion;
		form1.submit();
	}
}

function cargarFacturas( controller, x, maxfila ){
	if (window.event.keyCode==13){ 
		var factura =document.getElementById("c_factura"+x);
		if( factura.value.length != 0 ){
			buscarFacturas( controller , '2' , x, maxfila);
		}
		else{
			alert('Debe LLenar el Campo de la Factura');
		}
	}
}

function enviarFacturas( controller, x, maxfila ){
	var factura =document.getElementById("c_factura"+x);
	if( factura.value.length != 0 ){
		buscarFacturas( controller , '2' , x, maxfila);
	}
	else{
		alert('Debe LLenar el Campo de la Factura');
	}
}

function buscarTipoImpuesto(tipo, BASEURL, x, valor, tam, ag){
	var dir =BASEURL+"/jsp/cxcobrar/ingreso_detalle/Buscar_impuesto.jsp?tipo="+tipo+"&id="+x+"&valor="+valor+"&tam="+tam+"&agencia="+ag;
	window.open(dir,'Impuesto','width=700,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
}

function formato (num, decimales){
	var nums = ( new String (num) ).split('.');
	var salida = new String();
	for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	return rellenar(salida + (nums.length > 1 && decimales > 0 ? '.' + (nums[1].substr(0, (nums[1].length>decimales?decimales:nums[1].length))) : ''));
}

function rellenar(val){
   var longitud = 2;
   var cantDEcimal = val.indexOf('.');
   if( cantDEcimal >-1  ){
	   var decimal = val.substr(cantDEcimal+1, val.length );
	   var cntDec  =   decimal.length;
	   if ( cntDec < longitud){
			for( var i= 1;i< longitud;i++)
				val +='0'
	   }
   }else{
	  val +='.00';
   }

   return val;
}

function sinformato(element){
   return element.replace( new RegExp(",","g") ,'');
}

function sinCamposFormato( nombre, tam ){
	for (j=0; j<tam; j++){
		var campo = document.getElementById( nombre + j );
		if( campo.value != '' && campo.value.length >1 )
			campo.value = sinformato(campo.value, 2);
	}
}

function obtenerSaldoTotalFactura( tam ){
	var saldo_total = 0;
	for (j=0; j<tam; j++){
		var valor_rfte = document.getElementById("c_valor_rfte"+j);
		var valor_total_factura = document.getElementById("c_valor_total_factura"+j);
		var valor_rica = document.getElementById("c_valor_rica"+j);
		var valor_factura = document.getElementById("c_valor_abono"+j);
		var valor_total = document.getElementById("c_valor_total");
		var impuestos_rfte = document.getElementById("c_impuestos_rfte"+j);
		var impuestos_rica = document.getElementById("c_impuestos_rica"+j);
		
		//validación si existen factura, retefuente y reteica
		if( valor_factura.value != '' && valor_factura.value.length>1 )
			var vlr_factura = sinformato(valor_factura.value,2);
		else
			var vlr_factura = 0;
			
		if( valor_rfte.value != '' && valor_rfte.value.length>1 )
			var vlr_rfte = parseFloat(vlr_factura) * ( parseFloat(impuestos_rfte.value) / 100);
		else
			var vlr_rfte = 0;
		
		if( valor_rica.value != '' && valor_rica.value.length>1 )
			var vlr_rica = parseFloat(vlr_factura) * ( parseFloat(impuestos_rica.value) / 100);
		else
			var vlr_rica = 0;
		
		if( valor_total_factura.value != '' && valor_total_factura.value.length>1 )
			var vlr_tfactura = sinformato(valor_total_factura.value,2);
		else
			var vlr_tfactura = 0;
		
		//asigno valor al total de la factura
		valor_total_factura.value = parseFloat(vlr_factura) - ( parseFloat(vlr_rica) + parseFloat(vlr_rfte) );
		saldo_total = parseFloat(saldo_total) + parseFloat(valor_total_factura.value);
		valor_total_factura.value = formato(valor_total_factura.value, 2);
		
		if(vlr_rfte > 0)
			valor_rfte.value = formato( vlr_rfte, 2);
		if(vlr_rica >0)
			valor_rica.value = formato( vlr_rica, 2);
		
	}
	valor_total.value = formato(saldo_total, 2);
}

function guardarDatos ( controller, opcion, maxfila ) {
	sinCamposFormato("c_valor_rica",maxfila);
	sinCamposFormato("c_valor_rfte",maxfila);
	sinCamposFormato("c_valor_total_factura",maxfila);
	document.form1.action += "&opcion="+opcion;
	form1.submit();
}

function llenarDatosAlmacenados ( controller, opcion, maxfila ) {
	sinCamposFormato("c_valor_rica",maxfila);
	sinCamposFormato("c_valor_rfte",maxfila);
	sinCamposFormato("c_valor_total_factura",maxfila);
	document.form1.action += "&abrir_detalles=ok&opcion="+opcion;
	form1.submit();
}
//jose 2007-03-10
function ajustarItems ( controller, opcion, maxfila ) {
	sinCamposFormato("c_valor_rica",maxfila);
	sinCamposFormato("c_valor_rfte",maxfila);
	sinCamposFormato("c_valor_total_factura",maxfila);
	document.form1.action += "&abrir_ajuste=ok&opcion="+opcion;
	form1.submit();
}
//jose 2007-03-10
function abrirPaginaAjuste(BASEURL){
	var dir =BASEURL+"/jsp/cxcobrar/ingreso_detalle/AjusteIngreso.jsp";
	window.open(dir,'Ajuste','width=750,height=450,scrollbars=no,resizable=no,status=no');	
}

//jose 2007-03-10
function abrirPaginaAjusteUpdate(BASEURL){
	var dir =BASEURL+"/jsp/cxcobrar/ingreso_detalle/AjusteIngresoUpdate.jsp";
	window.open(dir,'Ajuste','width=750,height=450,scrollbars=no,resizable=no,status=no');	
}

function abrirPaginaItems(BASEURL){
	var dir =BASEURL+"/jsp/cxcobrar/ingreso_detalle/ItemsIngreso.jsp";
	window.open(dir,'Impuesto','width=950,height=650,scrollbars=no,resizable=yes,status=no');	
}

function abrirPaginaUpdateItems(BASEURL){
	var dir =BASEURL+"/jsp/cxcobrar/ingreso_detalle/ItemsIngresoUpdate.jsp";
	window.open(dir,'Impuesto','width=950,height=650,scrollbars=no,resizable=yes,status=no');	
}

function eliminarDatos ( controller, opcion, maxfila, x ) {
	sinCamposFormato("c_valor_rica",maxfila);
	sinCamposFormato("c_valor_rfte",maxfila);
	sinCamposFormato("c_valor_abono",maxfila);
	sinCamposFormato("vlr_tasa");
	sinCamposFormato("c_valor_total_factura",maxfila);
	document.form1.action += "&opcion="+opcion+"&pos="+x;
	form1.submit();
}
function eliminarTodos(){
	document.form1.action += "&opcion=0";
	form1.submit();
}

function obtenerSaldoTotal( tam ){
	var saldo_total = 0;
	var valor_total = document.getElementById("c_valor_total");
	var moneda = document.getElementById("mon_ingreso");
	for (j=0; j<tam; j++){
		var valor_rfte = document.getElementById("c_valor_rfte"+j);
		var valor_total_factura = document.getElementById("c_valor_total_factura"+j);
		var valor_rica = document.getElementById("c_valor_rica"+j);
		var valor_factura = document.getElementById("c_valor_abono"+j);
		var saldo_factura_ori = document.getElementById("c_saldo_factura_ori"+j);
		var saldo_smlf = document.getElementById("c_valor_saldo"+j);
		
		//validación si existen factura, retefuente y reteica
		if( valor_factura.value != '' && valor_factura.value.length>0 )
			var vlr_factura = sinformato(valor_factura.value,2);
		else
			var vlr_factura = 0;
			
		if( valor_rfte.value != '' && valor_rfte.value.length>0 )
			var vlr_rfte = sinformato (valor_rfte.value, 2);
		else
			var vlr_rfte = 0;
		
		if( valor_rica.value != '' && valor_rica.value.length>0 )
			var vlr_rica = sinformato(valor_rica.value, 2);
		else
			var vlr_rica = 0;
			
		if( valor_total_factura.value != '' && valor_total_factura.value.length>0 )
			var vlr_tfactura = sinformato(valor_total_factura.value,2);
		else
			var vlr_tfactura = 0;						
		
		//asigno valor al total de la factura
		valor_total_factura.value = parseFloat( sinformato(saldo_smlf.value)  ) - parseFloat( vlr_factura );
		saldo_total = parseFloat(saldo_total) + parseFloat(vlr_factura) - ( parseFloat(vlr_rica) + parseFloat(vlr_rfte) );
		if( parseFloat( valor_total_factura.value)==0 )
			valor_total_factura.value = "";
		else{
			if(moneda.value == 'DOL'){
				formatoDolar( valor_total_factura,2 );
			}
			else{
				formatear(valor_total_factura);
			}
		}
	}
	if(moneda.value == 'DOL'){
		valor_total.value = formato(saldo_total, 2);
	}else{
		valor_total.value = formato(saldo_total, 2);
		formatear(valor_total);
	}
}

function obtenerSaldoTasa( tam ){
	var saldo_total = 0;
	var valor_total = document.getElementById("c_valor_total");
	var moneda_ingreso = document.getElementById("mon_ingreso");
	var tasa = document.getElementById("vlr_tasa");
	for (j=0; j<tam; j++){
		var valor_rfte = document.getElementById("c_valor_rfte"+j);
		var valor_total_factura = document.getElementById("c_valor_total_factura"+j);
		var valor_rica = document.getElementById("c_valor_rica"+j);
		var valor_factura = document.getElementById("c_valor_abono"+j);
		var moneda_factura = document.getElementById("mon_factura"+j);
		var saldo_factura_ori = document.getElementById("c_saldo_factura_ori"+j);
		var saldo_smlf = document.getElementById("c_valor_saldo"+j);
		//validación si existen factura, retefuente y reteica
		if( valor_factura.value != '' && valor_factura.value.length>1 )
			var vlr_factura = sinformato(valor_factura.value,2);
		else
			var vlr_factura = 0;
			
		if( valor_rfte.value != '' && valor_rfte.value.length>1 )
			var vlr_rfte = sinformato (valor_rfte.value, 2);
		else
			var vlr_rfte = 0;
		
		if( valor_rica.value != '' && valor_rica.value.length>1 )
			var vlr_rica = sinformato(valor_rica.value, 2);
		else
			var vlr_rica = 0;
			
		if( valor_total_factura.value != '' && valor_total_factura.value.length>1 )
			var vlr_tfactura = sinformato(valor_total_factura.value,2);
		else
			var vlr_tfactura = 0;						
		if( moneda_factura.value != moneda_ingreso.value ){
			//valor saldo mon ingreso
			saldo_smlf.value = parseFloat( sinformato(saldo_factura_ori.value)  ) * parseFloat( sinformato(tasa.value) );
			if(moneda_ingreso.value == 'DOL'){
				formatoDolar( saldo_smlf,2 );
			}
			else{
				saldo_smlf.value = Math.round( saldo_smlf.value );
				formatear(saldo_smlf);
			}
			//asigno valor al total de la factura
			if( parseFloat( sinformato( saldo_smlf.value ) ) < parseFloat( vlr_factura )  ){
				valor_factura.value = sinformato( saldo_smlf.value );
				if(moneda_ingreso.value == 'DOL')
					formatoDolar( valor_factura.value,2 );
				else{
					valor_factura.value = Math.round( valor_factura.value );
					formatear(valor_factura);
				}
			}
			valor_total_factura.value = parseFloat( sinformato( saldo_smlf.value ) ) - parseFloat( sinformato( valor_factura.value ) );
			saldo_total = parseFloat(saldo_total) + parseFloat( sinformato( valor_factura.value ) ) - ( parseFloat(vlr_rica) + parseFloat(vlr_rfte) );
			if( parseFloat( valor_total_factura.value ) == 0 )
				valor_total_factura.value = "";
			else{
				if(moneda_ingreso.value == 'DOL')
					formatoDolar(valor_total_factura, 2);
				else{
					formatear(valor_total_factura);
				}
			}
		}
		else{
			//asigno valor al total de la factura
			valor_total_factura.value = parseFloat( sinformato(saldo_factura_ori.value)  ) - parseFloat( vlr_factura );
			saldo_total = parseFloat(saldo_total) + parseFloat(vlr_factura);
			if( parseFloat( valor_total_factura.value)==0 )
				valor_total_factura.value = "";
			else{
				if(moneda_ingreso.value == 'DOL')
					formatoDolar(valor_total_factura.value, 2);
				else{
					valor_total_factura.value = formato(valor_total_factura.value, 2);
					formatear(valor_total_factura);
				}
			}
		}
	}
	if(moneda_ingreso.value == 'DOL'){
		valor_total.value = formato(saldo_total, 2);
	}else{
		valor_total.value = formato(saldo_total, 2);
		formatear(valor_total);
	}
	formato(tasa.value,2);
}

function formatear(objeto){
	 var n= parseFloat(objeto.value.replace(/,/g,''));
	 if(isNaN(n)){
		 n=0;
	 }
	 objeto.value=formatearNumero(n); 
}
function formatearNumero(n){
	  // para estar seguros que vamos a trabajar con un string
	  var valor = ""+n;
	  var res = "";
	  for( var i=valor.length-1,j = 0; i >= 0; i--,j++ ){
		   if ( j % 3 == 0 && j != 0 ){
			  res += ",";
		   }
		   res += valor.charAt(i);
	  }
	  //ahora nos toca invertir el numero;
	  var aux = "";
	  for( var i=res.length-1; i >= 0; i-- ){
		   aux += res.charAt(i);
	  }
	  return aux;
}
function formatoDolar (obj, decimales){
    var numero = obj.value.replace( new RegExp(",","g"), "");
    var nums = ( new String (numero) ).split('.');
	var salida = new String();
	var TieneDec = numero.indexOf('.');
	var dato = new String();
	if( TieneDec !=-1  ){
	   var deci = numero.split('.');
	   var dec       = (deci[1].length >2)?deci[1].charAt(2):deci[1].substr(0,deci[1].length);
	   
	   if(dec > 5){
	       dato =  (parseInt(deci[1].substr(0,2)) + 1);
		   if(dato>99){
		     nums[0] = new String (parseInt(nums[0])+1);
			 obj.value = nums[0]+'.00';
		   }else{
		     for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	         obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
		   
		   }
	   }else{
	       for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	       obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
	   } 
    }else{
  	   for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	   obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
	}
 }
 
 //jose 2007-02-13
function VerificarCueConta(id,modulo){
	var cuenta = document.getElementById("cuenta"+id); 
	var tipo = document.getElementById("tipo"+id); 
	if(!cuenta.value == ''){
		enviar('?estado=CuentaContable&accion=Verificar&evento=Cuenta&cuenta=' + cuenta.value+'&campo='+id+'&tipo='+tipo.value+'&modulo='+modulo );
	}
}

function validarAuxiliar( id ){
    var cuen = document.getElementById("cuenta"+id);
	var tip = document.getElementById("tipo"+id);
    var aux = document.getElementById("auxiliar"+id);
    if( aux.value != '' ){ 
    	if( cuen.value == '' || tip.value =='' ){
			alert("Verifique que la cuenta y el tipo tenga datos");
	    }
		else{
			enviar('?estado=IngresoMiscelaneoDetalle&accion=Buscar&evento=Auxiliar&cuenta=' + cuen.value+'&tipo='+tip.value+'&auxiliar='+aux.value+'&campo='+id )
		}
	}
}

function buscarAuxiliar(BASEURL, id, cuenta, tipo){
    var cuen = document.getElementById("cuenta"+id);
	var tip = document.getElementById("tipo"+id); 
    if(cuen.value == '' || tip.value =='' ){
		alert("Verifique que la cuenta y el tipo tenga datos");
    }
	else{
		var dir =BASEURL+"/jsp/finanzas/contab/subledger/consultasIdSubledger.jsp?idcampo="+id+"&cuenta="+cuenta+"&tipo="+tipo+"&reset=ok";
		window.open(dir,'Auxiliar','width=700,height=520,scrollbars=yes,resizable=yes,top=10,left=65,status=yes');
	}
}