// JavaScript Document
function buscar2(lista,campo){
	var texto = campo.value.toLowerCase();
    for( var i=0; i<lista.length; i++ ){
	    var v = lista[i].innerText.toLowerCase();
		if ( v.indexOf(texto) >= 0 ){
			lista.selectedIndex = i;
			break;
		}
	}
}

function buscarcuentaequivalente(lista,campo){
	var texto = campo.value.toLowerCase();
    for( var i=0; i<lista.length; i++ ){
	    var v = lista[i].innerText.toLowerCase();
		if ( v.indexOf(texto) == 0 ){
			lista.selectedIndex = i;
			break;
		}
	}
}

function MostrarFiltroUpdate(){
	var y = document.getElementById("filtro1");
	var z = document.getElementById("filtro2");
	if (document.forma.clientes1.value!=''){
		y.checked = true;
		z.checked = false;
		document.forma.clientes1.disabled = false;
		document.forma.area1.disabled = true;
		return false;
	}else if (document.forma.area1.value!='NADA'){
		z.checked = true;
		y.checked = false;
		document.forma.area1.disabled = false;
		document.forma.clientes1.disabled = true;
		document.forma.text.disabled = true;
		return false;	
	}
	
}
function validarCuentaValidaUpdate(sw, cliente_area, clase, agencia, unidad, elemento, cuentaequivalente){
	var y = document.getElementById("filtro1");
	var z = document.getElementById("filtro2");
	if (document.forma.clase1.value=='NADA'){
		alert('Especifique la clase de cuenta');
		return false;
	}else if (document.forma.agencia1.value=='NADA'){
		alert('Especifique la agencia');
		return false;	
	}else if (document.forma.unidad1.value=='NADA'){
		alert('Especifique la unidad de negocio');
		return false;	
	}else if (document.forma.elemento1.value=='NADA'){
		alert('Especifique el elemento de gasto');
		return false;	
	}else if((y.checked==false)&&(z.checked==false)){
		alert("Seleccione una opcion entre cliente y area");
		return false;
	} if ((y.checked==true)&&(document.forma.clientes1.value=='')){
		alert('Especifique el cliente');
		return false;	
	}else if ((z.checked==true)&&(document.forma.area1.value=='NADA')){
		alert('Especifique el area');
		return false;	
	}else{
		if(sw=='Modificar'){ 
			if (y.checked==true){
				forma.action = 'controllercontab?estado=CuentaValida&accion=Update&sw=Modificar&cliente_area2='+document.forma.clientes1.value+'&cliente_area='+cliente_area+'&clase='+clase+'&agencia='+agencia+'&unidad='+unidad+'&elemento='+elemento+'&cuentaequivalente='+cuentaequivalente;
			}else if (z.checked==true){
				forma.action = 'controllercontab?estado=CuentaValida&accion=Update&sw=Modificar&cliente_area2='+document.forma.area1.value+'&cliente_area='+cliente_area+'&clase='+clase+'&agencia='+agencia+'&unidad='+unidad+'&elemento='+elemento+'&cuentaequivalente='+cuentaequivalente;	
			}
			forma.submit();
		}else if (sw=='Eliminar'){
		if (y.checked==true){
				forma.action = 'controllercontab?estado=CuentaValida&accion=Update&sw=Eliminar&cliente_area='+document.forma.clientes1.value;
			}else if (z.checked==true){
				forma.action = 'controllercontab?estado=CuentaValida&accion=Update&sw=Eliminar&cliente_area='+document.forma.area1.value;	
			}
			forma.submit();
		}
	}
}
function validarFiltroinsertar(){
	var y = document.getElementById("filtro1");
	var z = document.getElementById("filtro2");
	if (y.checked==true){
		document.forma.clientes.disabled=false;
		document.forma.text.disabled=false;
		document.forma.area.disabled=true;
		return false;
	}
	else if (z.checked==true){
	    document.forma.area.disabled=false;
		document.forma.clientes.disabled=true;
		document.forma.text.disabled=true;
		return false;
	}
}

function validarFiltroUpdate(){
	var y = document.getElementById("filtro1");
	var z = document.getElementById("filtro2");
	if (y.checked==true){
		document.forma.clientes1.disabled=false;
		document.forma.text.disabled=false;
		document.forma.area1.disabled=true;
		return false;
	}
	else if (z.checked==true){
	    document.forma.area1.disabled=false;
		document.forma.clientes1.disabled=true;
		document.forma.text.disabled=true;
		return false;
	}
}

function validarCuentaValidaSearch(cont){
	var y = document.getElementById("filtro1");
	var z = document.getElementById("filtro2");
	if((y.checked==false)&&(z.checked==false)&&(document.forma.clase.value=='NADA')&&(document.forma.agencia.value=='NADA')&&(document.forma.unidad.value=='NADA')&&(document.forma.elemento.value=='NADA')&&(document.forma.agencia.value=='NADA')){
		alert("Seleccione al menos un criterio de busqueda");
		return false;
	}else{
		if (y.checked==true){
		    if (document.forma.clientes.value==''){
				alert('Seleccione el Cliente para la busqueda');
				return false;
			}else{
				forma.action = cont+'?estado=CuentaValida&accion=List&cliente_area='+document.forma.clientes.value;
			}
		}else if (z.checked==true){
		if (document.forma.area.value=='NADA'){
				alert('Seleccione el Area para la busqueda');
				return false;
			}else{
				forma.action = cont+'?estado=CuentaValida&accion=List&cliente_area='+document.forma.area.value;
			}	
		}else{
			forma.action = cont+'?estado=CuentaValida&accion=List&cliente_area=NADA';	
		}
		forma.submit();
	}
}

function validarCuentaValidaInsert(){
	var y = document.getElementById("filtro1");
	var z = document.getElementById("filtro2");
	if (document.forma.clase.value=='NADA'){
		alert('Especifique la clase de cuenta');
		return false;
	}else if (document.forma.agencia.value=='NADA'){
		alert('Especifique la agencia');
		return false;	
	}else if (document.forma.unidad.value=='NADA'){
		alert('Especifique la unidad de negocio');
		return false;	
	}else if (document.forma.elemento.value=='NADA'){
		alert('Especifique el elemento de gasto');
		return false;	
	}else if((y.checked==false)&&(z.checked==false)){
		alert("Seleccione una opcion entre cliente y area");
		return false;
	} if ((y.checked==true)&&(document.forma.clientes.value=='')){
		alert('Especifique el cliente');
		return false;	
	}else if ((z.checked==true)&&(document.forma.area.value=='NADA')){
		alert('Especifique el area');
		return false;	
	}else{
		if (y.checked==true){
			forma.action = 'controllercontab?estado=CuentaValida&accion=Insert&cliente_area='+document.forma.clientes.value;
		}else if (z.checked==true){
			forma.action = 'controllercontab?estado=CuentaValida&accion=Insert&cliente_area='+document.forma.area.value;	
		}
		forma.submit();
	}
}