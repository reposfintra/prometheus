window.onload = function () {
    cargarPagadurias();
    document.getElementById("buscar").addEventListener("click", cargarNegocioLibranza);
    document.getElementById("buscar").addEventListener("change", cargarNegocioLibranza);
    document.getElementById("actualizar").addEventListener("click", actualizarPagaduria);
    document.getElementById("historico").addEventListener("click", cargarTrazabilidad);
    document.getElementById("salir").addEventListener("click", function () {
        window.close();
    });
};

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function hideGrid() {
    $("#gbox_tabla_NegocioLibranza").hide();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsg").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function loading(msj, width, height) {
    
    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function cargarPagadurias() {
    var listaPagadurias = document.getElementById("listaPagadurias");
    $.ajax({
        type: "POST",
        async: true,
        url: "./controller?estado=Administracion&accion=Fintra",
        dataType: "json",
        data: {
            opcion: 6
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, 400, 180);
                    return;
                }
                try {
                    json.forEach(function (element) {
                        listaPagadurias.innerHTML += "<option value='" + element.documento + "'>" + element.razon_social + "</option>";
                    });
                } catch (exception) {
                    mensajesDelSistema("Error al cargar las pagadur�as", 400, 180);
                }
            } else {
                mensajesDelSistema("No se obtuvieron los datos del servidor", 400, 150);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error en la comunicacion con el servidor", 400, 150);
        }
    });
}

function cargarNegocioLibranza() {
    var negocio = document.getElementById("negocio");
    var pagaduria = document.getElementById("pagaduria");
    var cedula = document.getElementById("cedula");
    var cliente = document.getElementById("cliente");

    if (negocio.value == "") {
        alert("Debe ingresar el c�digo del negocio");
    } else {
        $.ajax({
            type: "POST",
            async: true,
            url: "./controller?estado=Administracion&accion=Fintra",
            dataType: "json",
            data: {
                opcion: 7,
                negocio: negocio.value
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, 400, 180);
                        return;
                    }
                    try {
                        pagaduria.value = json.pagaduria;
                        cliente.value = json.cliente;
                        cedula.value = json.cedula;
                        hideGrid();
                    } catch (exception) {
                        mensajesDelSistema("Error al cargar los datos", 400, 180);
                    }
                } else {
                    mensajesDelSistema("No se obtuvieron los datos del servidor", 400, 150);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mensajesDelSistema("Error en la comunicacion con el servidor", 400, 150);
            }
        });
    }
}

function actualizarPagaduria() {
    var negocio = document.getElementById("negocio").value;
    var nuevaPagaduria = document.getElementById("listaPagadurias").value;

    if (negocio == "") {
        mensajedelsistema("Debe ingresar el c�digo del negocio");
    } else if (nuevaPagaduria == "" || nuevaPagaduria == null || nuevaPagaduria == undefined) {
        alert("Debe escoger una pagadur�a");
    } else {
        $.ajax({
            type: "POST",
            async: true,
            url: "./controller?estado=Administracion&accion=Fintra",
            dataType: "json",
            data: {
                opcion: 8,
                negocio: negocio,
                pagaduria: nuevaPagaduria
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, 400, 180);
                        return;
                    }
                    negocio.value = "";
                    nuevaPagaduria.value = "";
                    document.getElementById("pagaduria").value = "";
                    document.getElementById("cliente").value = "";
                    document.getElementById("cedula").value = "";
                    hideGrid();
                    mensajesDelSistema(json.respuesta, 400, 150);
                } else {
                    mensajesDelSistema("No se pudo actualizar la pagadur�a", 400, 150);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mensajesDelSistema("Error en la comunicacion con el servidor", 400, 150);
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: "json",
        url: "./controller?estado=Administracion&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                negocio: $("#negocio").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
    $("#gbox_tabla_NegocioLibranza").show();
}

function cargarTrazabilidad() {
    var negocio = document.getElementById("negocio").value;
    if (negocio == "") {
        alert("Debe ingresar el c�digo del negocio");
    } else {
        var grid_tabla_ = jQuery("#tabla_NegocioLibranza");
        if ($("#gview_tabla_NegocioLibranza").length) {
            reloadGridMostrar(grid_tabla_, 9);
        } else {
            grid_tabla_.jqGrid({
                caption: "Hist�rico pagadur�as",
                url: "./controller?estado=Administracion&accion=Fintra",
                mtype: "POST",
                datatype: "json",
                height: "250",
                width: "500",
                autowidth: false,
                colNames: ["Negocio", "Pagadur�a", "Usuario", "Fecha"],
                colModel: [
                    {name: "cod_neg", index: "cod_neg", width: 100, align: "center", hidden: false},
                    {name: "razon_social", index: "razon_social", width: 100, align: "center", hidden: false},
                    {name: "usuario", index: "usuario", width: 100, align: "left", hidden: false},
//                {name: "comentarios", index: "comentarios", width: 200, align: "left", hidden: false},
                    {name: "fecha", index: "fecha", width: 150, align: "left", hidden: false}
                ],
                rownumbers: true,
                rownumWidth: 25,
                rowNum: 1000,
                rowTotal: 1000,
                loadonce: true,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                shrinkToFit: true,
                footerrow: false,
                pager: "#pager",
                multiselect: false,
                multiboxonly: false,
                pgtext: null,
                pgbuttons: false,
                cmTemplate: {sortable: false},
                jsonReader: {
                    root: "json",
                    repeatitems: false,
                    id: "0"
                }, ajaxGridOptions: {
                    dataType: "json",
                    type: "POST",
                    data: {
                        opcion: 9,
                        negocio: negocio
                    }
                },
                gridComplete: function () {
//                  $("#dialogLoading").hide();
                },
                loadComplete: function (data) {
                  $("#dialogLoading").hide();
//                  $("#dialogLoading").dialog('close'); 
                },
                loadError: function (xhr, status, error) {
//                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Error en la comunicacion con el servidor", 400, 150);
                }
            }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: true});
        }
    }
    $("#gbox_tabla_NegocioLibranza").show();
}