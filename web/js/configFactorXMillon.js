/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
   cargarConfiguracionFactorXMillon();
   cargarComboGenerico('und_negocio', 5);
   cargarComboGenerico('nit_empresa_fianza', 30);
   cargarComboGenerico('producto', 46);
   cargarComboGenerico('cobertura', 47);
   maximizarventana();
   
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });   
    
    $('.solo-numeric').live('keypress', function(event) {
        return numbersonly(this, event);
    });
    
    $('.solo-numeric').live('blur',function (event) {  
           this.value = numberConComas(this.value);            
    });
   
});

function cargarConfiguracionFactorXMillon(){

    var grid_listar_config_libranza = $("#tabla_config_factor");
    if ($("#gview_tabla_config_factor").length) {
         refrescarGridConfigFactorXMillon();
     }else {
         grid_listar_config_libranza.jqGrid({        
            caption:'CONFIGURACIÓN FACTOR POR MILLÓN',
            url: "./controller?estado=Garantias&accion=Comunitarias",
            mtype: "POST",
            datatype: "json",
            height: '380',
            width: '1080',
            colNames: ['Id',  'Nro. Convenio', 'Id Und Negocio', 'Unidad de Negocio', 'Nit Empresa', 'Empresa', 'Financiado', 'Plazo Inicial',
                'Plazo Final', '% Comisión', 'Valor Comisión', '% IVA','% Dto Aval cliente','% Dto Aval Financiado', 'Estado',
                'Producto','Id Producto','Cobertura',' Id Cobertura','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, resizable:false, sortable: true, align: 'center', key: true, hidden:true},   
                {name: 'numero_convenio', index: 'numero_convenio', resizable:false, sortable: true, width: 80, align: 'center'},
                {name: 'id_unidad_negocio', index: 'id_unidad_negocio', resizable:false, sortable: true, width: 80, align: 'center', hidden:true},
                {name: 'unidad_negocio', index: 'unidad_negocio', resizable:false, sortable: true, width: 100, align: 'left'},
                {name: 'nit_empresa', index: 'nit_empresa', resizable:false, sortable: true, width: 80, align: 'center', hidden:true},
                {name: 'nombre_empresa', index: 'nombre_empresa', resizable:false, sortable: true, width: 250, align: 'left'},               
                {name: 'financiado', index: 'financiado', resizable:false, sortable: true, width: 50, align: 'left'},
                {name: 'plazo_inicial', index: 'plazo_inicial', width: 80, align: 'center', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'plazo_final', index: 'plazo_final', width: 80, align: 'center', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'porcentaje_comision', index: 'porcentaje_comision', width: 80, align: 'right', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, suffix:" %"}},
                {name: 'valor_comision', index: 'valor_comision', sortable: true, width: 80, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'porcentaje_iva', index: 'porcentaje_aval', width: 60, align: 'right', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, suffix:" %"}},
                {name: 'porcentaje_aval', index: 'porcentaje_iva', width: 100, align: 'right', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, suffix:" %"}},
                {name: 'porcentaje_aval_finan', index: 'porcentaje_aval_finan', width: 110, align: 'right', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, suffix:" %"}},
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden:true}, 
                {name: 'producto', index: 'producto', resizable:false, sortable: true, width: 80, align: 'left'},
                {name: 'id_producto', index: 'id_producto', resizable:false, sortable: true, width: 80, align: 'left',hidden:true},
                {name: 'cobertura', index: 'cobertura', resizable:false, sortable: true, width: 80, align: 'left'},
                {name: 'id_cobertura', index: 'id_cobertura', resizable:false, sortable: true, width: 80, align: 'left',hidden:true},
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'} 
              
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            pager:'#page_tabla_config_factor',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {                
                dataType: "json",
                type: "POST",  
                async:false,
                data: {
                    opcion: 4
                }
            },    
            gridComplete: function() {
                var cant = jQuery("#tabla_config_factor").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_config_factor").getRowData(cant[i]).cambio;                 
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoConfFactor('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_config_factor").jqGrid('setRowData', cant[i], {cambio: be});

                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_config_factor"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;

                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                    editarConfigFactor(id);
                }

            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_config_factor",{search:false,refresh:false,edit:false,add:false,del:false});      
        jQuery("#tabla_config_factor").jqGrid("navButtonAdd", "#page_tabla_config_factor", {
            caption: "Nuevo", 
            title: "Agregar nuevo factor",           
            onClickButton: function() {
                crearConfigFactor();           
            }
        });           

     }
}


function refrescarGridConfigFactorXMillon(){    
    jQuery("#tabla_config_factor").setGridParam({
        url: "./controller?estado=Garantias&accion=Comunitarias",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async:false,
            data: {
                opcion: 4
            }
        }
    }).trigger("reloadGrid");
}


function cargarComboGenerico(idCombo, option) {
    $('#'+idCombo).html('');
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Garantias&accion=Comunitarias",
        dataType: 'json',
        async:false,
        data: {
            opcion: option
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#'+idCombo).append("<option value=''>Seleccione</option>");
                    for (var key in json) {              
                       $('#'+idCombo).append('<option value=' + key + '>' + json[key] + '</option>');                
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#'+idCombo).append("<option value=''>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function crearConfigFactor(){   
    $('#div_config_factor').fadeIn('slow');   
    resetearValoresConfigFactor();
    AbrirDivCrearConfigFactor();
}

function AbrirDivCrearConfigFactor(){
      $("#div_config_factor").dialog({
        width: 'auto',
        height: 420,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR CONFIGURACION FACTOR POR MILLÓN',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {         
                guardarConfigFactor(6,'insert');             
            },
            "Salir": function () {                  
                resetearValoresConfigFactor();         
                $(this).dialog("destroy");
            }
        }
    });    
}

function editarConfigFactor(cl){

    $('#div_config_factor').fadeIn("slow");
    var fila = jQuery("#tabla_config_factor").getRowData(cl);
    var codigo_convenio = fila['numero_convenio'];  
    var und_negocio = fila['id_unidad_negocio'];   
    var nit_empresa = fila['nit_empresa'];  
    var plazo_inicial = fila['plazo_inicial'];
    var plazo_final = fila['plazo_final'];
    var porcentaje_comision = fila['porcentaje_comision'];
    var valor_comision = fila['valor_comision']; 
    var porcentaje_iva = fila['porcentaje_iva'];
    var financiado = fila['financiado'];
    var porcentaje_aval = fila['porcentaje_aval'];
    var porcentaje_aval_finan = fila['porcentaje_aval_finan'];
    var id_cobertura = fila['id_cobertura'];
    var id_producto = fila['id_producto'];
  
    $('#id_config').val(cl);
    $('#codigo_convenio').val(codigo_convenio);
    $('#und_negocio').val(und_negocio);
    $('#nit_empresa_fianza').val(nit_empresa);
    $('#plazo_inicial').val(numberConComas(plazo_inicial));
    $('#plazo_final').val(numberConComas(plazo_final)); 
    $('#porcentaje_comision').val(numberConComas(porcentaje_comision));
    $('#valor_comision').val(numberConComas(valor_comision));
    $('#porcentaje_iva').val(numberConComas(porcentaje_iva));
    $('#financiado').attr('checked', financiado === 'Si' ? true : false);
    $('#porcentaje_aval').val(numberConComas(porcentaje_aval));
    $('#porcentaje_aval_finan').val(numberConComas(porcentaje_aval_finan));
    $('#cobertura').val(id_cobertura);
    $('#producto').val(id_producto);
   
    AbrirDivEditarConfigFactor();
}


function AbrirDivEditarConfigFactor(){
      $("#div_config_factor").dialog({
        width: 'auto',
        height: 420,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR CONFIGURACION FACTOR',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              guardarConfigFactor(7,'update');
            },
            "Salir": function () {                
                resetearValoresConfigFactor();              
                $(this).dialog("destroy");
            }
        }
    });       
}

function guardarConfigFactor(option, action){  
    var id = $('#id_config').val();   
    var codigo_convenio = $('#codigo_convenio').val();
    var und_negocio = $('#und_negocio').val();
    var nit_empresa_fianza = $('#nit_empresa_fianza').val();
    var plazo_inicial = numberSinComas($('#plazo_inicial').val());
    var plazo_final = numberSinComas($('#plazo_final').val());  
    var porcentaje_comision = numberSinComas($('#porcentaje_comision').val());
    var valor_comision = numberSinComas($('#valor_comision').val());
    var porcentaje_iva = numberSinComas($('#porcentaje_iva').val());
    var financiado = document.getElementById('financiado').checked;
    var porcentaje_aval= numberSinComas($('#porcentaje_aval').val());
    var porcentaje_aval_finan=numberSinComas($('#porcentaje_aval_finan').val());
    var producto = $('#producto').val();
    var cobertura = $('#cobertura').val();
    
    var url = './controller?estado=Garantias&accion=Comunitarias';
   if(validaCamposConfigFactor(codigo_convenio, und_negocio, nit_empresa_fianza, plazo_inicial, plazo_final, porcentaje_comision, valor_comision, porcentaje_iva,porcentaje_aval,porcentaje_aval_finan,producto,cobertura)){
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        opcion: option, 
                        id: id,        
                        codigo_convenio: codigo_convenio,
                        id_unidad: und_negocio,
                        nit_empresa_fianza: nit_empresa_fianza,
                        plazo_inicial: plazo_inicial,
                        plazo_final: plazo_final,                      
                        porc_comision: porcentaje_comision,
                        valor_comision: valor_comision,
                        iva: porcentaje_iva,
                        financiado: financiado,
                        porcentaje_aval: porcentaje_aval,
                        porcentaje_aval_finan: porcentaje_aval_finan,
                        producto:producto,
                        cobertura:cobertura
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '270', '165');                          
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                resetearValoresConfigFactor();
                                refrescarGridConfigFactorXMillon();  
                                (action === 'update') ? $("#div_config_factor").dialog('close'): mensajesDelSistema("Cambios guardados satisfactoriamente", '250', '150', true);

                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo guardar la configuración!!", '250', '150');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                              "Message: " + xhr.statusText + "\n" +
                              "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                }); 
            },500);
    }

}

function CambiarEstadoConfFactor(rowid){
    var grid_tabla = jQuery("#tabla_config_factor");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Garantias&accion=Comunitarias",
        data: {
            opcion: 8,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarGridConfigFactorXMillon();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado de la configuracion!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function validaCamposConfigFactor(codigo_convenio, und_negocio, nit_empresa_fianza, plazo_inicial, plazo_final, porcentaje_comision, valor_comision, porcentaje_iva,producto,cobertura){
    var sw = false;
    if (codigo_convenio === '' || und_negocio === '' || nit_empresa_fianza === '' || plazo_inicial === '' 
            || plazo_final === '' || porcentaje_comision === '' || valor_comision === '' || porcentaje_iva === ''
            || producto === ''|| cobertura === '') {
        mensajesDelSistema("Debe diligenciar todos los campos", '250', '150');
        return;
    }else if(parseInt(plazo_final) < parseInt(plazo_inicial)){
        mensajesDelSistema('El plazo final no puede ser inferior al plazo inicial', '250', '150');
        return;
    }else if(parseFloat(porcentaje_comision)===0 && parseFloat(valor_comision)===0){
        mensajesDelSistema('Por favor ingrese el porcentaje o valor de la comisión', '250', '150');
        return;
    }else if(porcentaje_iva===""){
        mensajesDelSistema('Por favor ingrese el porcentaje de iva', '250', '150');
        return;   
    }else if($("#financiado").is(':checked') && $("#porcentaje_aval").val()>=100){
        mensajesDelSistema('Si el aval es financiado el porcentaje aval del cliente debe ser menor al 100%', '250', '160');
        return;   
    }else if($("#financiado").not(':checked') && $("#porcentaje_aval").val()>100){
        mensajesDelSistema('El porcentaje aval del cliente debe ser menor o igual a 100%', '250', '160');
        return;   
    }else{
         sw = true;
    }
    return sw;
}

function changeStatusFactorValue(e, fieldId, value) {
    if (e.keyCode !== 9) {
        var vlr = (value !== '' && parseFloat(value)>0) ? 0 : '';
        if (fieldId === 'porcentaje_comision') {
            $('#valor_comision').val(vlr); 
        } else {           
            $('#porcentaje_comision').val(vlr);
        }
    }
}

function resetearValoresConfigFactor(){    
    $('#codigo_convenio').val('');
    $('#und_negocio').val('');
    $('#nit_empresa_fianza').val('');
    $('#plazo_inicial').val('');
    $('#plazo_final').val('');   
    $('#porcentaje_comision').val('');
    $('#valor_comision').val('');   
    $('#porcentaje_iva').val('');
    $('#porcentaje_aval').val('');
    $('#porcentaje_aval_finan').val('');
}

function numbersonly(myfield, e, dec)
{
    var key;
    var keychar;
    
    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

// control keys
    if ((key == null) || (key == 0) || (key == 8) ||
            (key == 9) || (key == 13) || (key == 27))
        return true;

// numbers
    else if ((("0123456789.").indexOf(keychar) > -1))
        return true;

// decimal point jump
    else if (dec && (keychar == "."))
    {
        myfield.form.elements[dec].focus();
        return false;
    }
    else
        return false;
}

function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajeConfirmAction(msj, width, height, okAction, id) {  
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear botón de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}


function porc_aval_cliente(){
    
    if(($("#porcentaje_aval").val() < 0 || $("#porcentaje_aval").val() > 100)){
        
        $("#porcentaje_aval").val(0);
        
    }  
    
    let porc=100-$("#porcentaje_aval").val();

    $("#porcentaje_aval_finan").val(porc);
    
}


function isFinanciado(){
        let myGrid = jQuery("#tabla_config_factor"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
        filas = myGrid.jqGrid("getLocalRow", selRowIds);
        let id = filas.id;
        let fila = jQuery("#tabla_config_factor").getRowData(id);
        let porcentaje_aval = fila['porcentaje_aval'];
        let porcentaje_aval_finan = fila['porcentaje_aval_finan'];
       
        if ($("#financiado").is(':checked')) {
            
            $("#porcentaje_aval").removeAttr('disabled');
            if (porcentaje_aval>0){
                $('#porcentaje_aval').val(porcentaje_aval);
                $('#porcentaje_aval_finan').val(porcentaje_aval_finan);
            }else{
                $('#porcentaje_aval').val(0);
                $('#porcentaje_aval_finan').val(100);
            } 
        }else{
            $("#porcentaje_aval").attr('disabled','disabled');
            $('#porcentaje_aval').val(100);
           $('#porcentaje_aval_finan').val(0);
        }
    };