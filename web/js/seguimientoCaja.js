/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $j = jQuery.noConflict();
    
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $("#fecha").val('');
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date('2016/10/26'),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $('#anio').val(myDate.getFullYear());
    $("#fecha").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

    cargarCuentas();    
    cargarCuentasMovAux();
    cargarSeguimientoCaja();
    $('#buscar_').click(function () {
        var fechaBuscar = $('#fecha').val();
        var f = new Date();
        var fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
        if (fecha !== fechaBuscar) {
            $('#guardar').hide();
        } else {
            $('#guardar').show();
        }
        if($('#anio').val()===''){
             mensajesDelSistema('Debe ingresar el a�o', 250, 150);
        }else{
              cargarSeguimientoCaja();
        }
      
    });


});

function cargarCuentas() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 47
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#cuenta').html('');
                $('#cuenta').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#cuenta').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarSeguimientoCaja() {
    //$('#tabla_seguimiento_caja').jqGrid('GridUnload');
    var grid_tabla_ = $("#tabla_seguimiento_caja");
    if ($j("#gview_tabla_seguimiento_caja").length) {
        console.log('entro');
        reloadGridMostrar(grid_tabla_, 50);
    } else {
        grid_tabla_.jqGrid({
            caption: "Posicion de Caja",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            width: '1050',
            colNames: ['Id_', 'Cuenta', 'Nombre Cuenta', 'Fecha Movimiento', 'Saldo Anterior', 'Valor Debito', 'Valor Credito', 'Saldo Actual Libro', 'Saldo Extracto', 'Diferencia'],
            colModel: [
                {name: 'id_', index: 'id_', width: 100, align: 'center', hidden: true},
                {name: 'cuenta', index: 'cuenta', width: 100, align: 'center'},
                {name: 'nombre_cuenta', index: 'nombre_cuenta', width: 320, align: 'left'},
                {name: 'fecha_movimiento', index: 'fecha_movimiento', width: 160, align: 'center', sortable: false, search: false, hidden: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'saldo_anterior', index: 'saldo_anterior', width: 200, align: 'right', hidden: true, search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'vlr_debito', index: 'valor_debito', width: 200, hidden: true, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'vlr_credito', index: 'valor_credito', width: 200, hidden: true, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'saldo_actual', index: 'saldo_actual', width: 200, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'saldo_extracto', index: 'saldo_extracto', width: 200, editable: true, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'diferencia', index: 'diferencia', width: 200, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: true,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                data: {
                    opcion: 50,
                    anio: $('#anio').val(),
                    fecha: $('#fecha').val(),
                    cuenta: $('#cuenta').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {
                var saldo_anterior = grid_tabla_.jqGrid('getCol', 'saldo_anterior', false, 'sum');
                var valor_debito = grid_tabla_.jqGrid('getCol', 'valor_debito', false, 'sum');
                var valor_credito = grid_tabla_.jqGrid('getCol', 'valor_credito', false, 'sum');
                var saldo_actual = grid_tabla_.jqGrid('getCol', 'saldo_actual', false, 'sum');
                var saldo_extracto = grid_tabla_.jqGrid('getCol', 'saldo_extracto', false, 'sum');
                var dif = grid_tabla_.jqGrid('getCol', 'diferencia', false, 'sum');
                grid_tabla_.jqGrid('footerData', 'set', {fecha_movimiento: 'TOTAL:', saldo_anterior: saldo_anterior, valor_debito: valor_debito, valor_credito: valor_credito, saldo_actual: saldo_actual, saldo_extracto: saldo_extracto, diferencia: dif});
            },
            gridComplete: function (index) {

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = $("#tabla_seguimiento_caja"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var f = new Date();
                var dia;
                if (f.getDate() <= 9) {
                    dia = '0' + f.getDate();
                } else {
                    dia = f.getDate();
                }
                var fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + dia;
                var fecha_movimiento = filas.fecha_movimiento;
                console.log('fecha: ' + fecha + ' fecha_movimiento: ' + fecha_movimiento);
                if (fecha == fecha_movimiento) {
                    grid_tabla_.jqGrid('editRow', rowid, true, function () {
                    }, null, null, {}, function (rowid) {
                        var diferencia;
                        var saldo_actual = $("#tabla_seguimiento_caja").getRowData(rowid).saldo_actual;
                        var saldo_extracto = $("#tabla_seguimiento_caja").getRowData(rowid).saldo_extracto;
                        diferencia = saldo_actual - saldo_extracto;
                        grid_tabla_.jqGrid('setCell', rowid, 'diferencia', diferencia);
                    });
                    return;
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_seguimiento_caja").navButtonAdd('#pager', {
            caption: "Guardar",
            id: 'guardar',
            onClickButton: function () {
                guardarSeguimientoCaja();
            }
        });
        $("#tabla_seguimiento_caja").navButtonAdd('#pager', {
            caption: "Exportar Excel",
            onClickButton: function () {
                exportarExcel();
            }
        });
        $("#tabla_seguimiento_caja").navButtonAdd('#pager', {
            caption: "Exportar MovimientoAux",
            onClickButton: function () {
                ventanaMovAux();
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                fecha: $('#fecha').val(),
                cuenta: $('#cuenta').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function guardarSeguimientoCaja() {
    var grid = $("#tabla_seguimiento_caja"), filas = grid.jqGrid('getDataIDs');
    var data = $("#tabla_seguimiento_caja").jqGrid('getRowData');
    for (var i = 0; i < filas.length; i++) {
        console.log(filas[i]);
        grid.saveRow(filas[i]);
    }
    if (data.length === 0) {
        mensajesDelSistema('No informacion para guardar', '300', 'auto', false);
    } else {
        $.ajax({
            async: false,
            url: "./controller?estado=Admin&accion=Fintra",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 51,
                informacion: JSON.stringify({json: data})
            },
            success: function (json) {
                if (json.respuesta === 'Guardado') {
                    mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);
                    cargarSeguimientoCaja();
                } else {
                    mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }


}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center", modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function  exportarExcel() {
    var fullData = $("#tabla_seguimiento_caja").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Admin&accion=Fintra",
        data: {
            listado: myJsonString,
            opcion: 52
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function ventanaMovAux() {
    $('#periodoInicio').val('');
    $('#periodoFin').val('');    
    $("#dialogMsjMoviAux").dialog({
        width: '677',
        height: '353',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Descargar Movimiento Auxiliar',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
            $("#dialogMsjMoviAux").siblings('div.ui-dialog-titlebar').remove();
        },
        buttons: {
            "Generar": function () {
                generarMovimientoAux();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });  
    cargarCuentas_();
}

function cargarCuentas_() {

    console.log($('#cuentas').html());
    $j('#cuentas').multiselect({
        columns: 1,
        placeholder: 'Seleccione',
        search: true,
        selectAll: true
    });
}

function cargarCuentasMovAux() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 47
        },
        success: function (json) {
            if (json.error) {              
                return;
            }
            try {
                $('#cuentas').html('');             
                for (var datos in json) {
                    $('#cuentas').append('<option value=' + datos + '>' + datos + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function generarMovimientoAux() {
    var variable = "";
    var cuentas = $('#cuentas').val();
    console.log(cuentas);
    variable = variable + cuentas;
   if($('#cuentas').val() !=='' && $('#periodoInicio').val() !=='' && $('#periodoFin').val() !==''){
        var opt = {
            autoOpen: false,
            modal: true,
            width: 200,
            height: 150,
            title: 'Descarga'
        };
        $("#divSalidaEx").dialog(opt);
        $("#divSalidaEx").dialog("open");
        $("#imgloadEx").show();
        $("#msjEx").show();
        $("#respEx").hide();
        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Admin&accion=Fintra",
                type: 'POST',
                dataType: "html",
                data: {
                    opcion: 58,
                    periodo_inicio:$('#periodoInicio').val(),
                    periodo_fin:$('#periodoFin').val(),
                    listadocuentas:variable
                },
                success: function (resp) {
                    $("#imgloadEx").hide();
                    $("#msjEx").hide();
                    $("#respEx").show();
                    var boton = "<div style='text-align:center'>\n\ </div>";
                    document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;


                }, error: function (xhr, ajaxOptions, thrownError) {                
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);
    }else{
        mensajesDelSistema('Debe llenar todos los campos', '250', '150', false)
    }

}
