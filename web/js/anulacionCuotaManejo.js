/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $('#buscar').click(function () {
        cargarNegocios();
    });
});

function cargarNegocios() {
    var grid_tabla = $("#tabla_negocios");
    if ($("#gview_tabla_negocios").length) {
        reloadGridTabla(grid_tabla, 20);
    } else {
        grid_tabla.jqGrid({
            caption: "Negocios",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '390',
            width: '800',
            colNames: ['Id convenio', 'No de cuota', 'Cliente', 'Prefijo cxc cuota admin', 'cod_cli', 'Nombre cuenta', 'Fecha', 'Valor cuota admin', 'Cuota admin causada', 'Cuenta cuota admin',
                'Documento', 'Cmc', 'Fecha documento', 'cod_neg', 'Fecha vencimiento', 'Tipo', 'Saldo inicial', 'Tasa', 'fecha cuota admin'],
            colModel: [
                {name: 'id_convenio', index: 'id_convenio', width: 80, sortable: true, align: 'center', hidden: true},
                {name: 'item', index: 'item', width: 80, sortable: true, align: 'center', hidden: false, key: true},
                {name: 'cliente', index: 'cliente', width: 200, sortable: true, align: 'center', hidden: false},
                {name: 'prefijo_cxc_cuota_manejo', index: 'prefijo_cxc_cuota_manejo', width: 150, sortable: true, align: 'center', hidden: true},
                {name: 'cod_cli', index: 'cod_cli', width: 150, sortable: true, align: 'left', hidden: true},
                {name: 'nomb_cuenta', index: 'nomb_cuenta', width: 210, sortable: true, align: 'left', hidden: true},
                {name: 'fecha', index: 'fecha', width: 100, sortable: true, align: 'center', hidden: true},
                {name: 'cuota', index: 'cuota', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'cuota_manejo_causada', index: 'cuota_manejo_causada', width: 110, sortable: true, align: 'center', hidden: true},
                {name: 'cuenta_cuota_manejo', index: 'cuenta_cuota_manejo', width: 110, sortable: true, align: 'center', hidden: true},
                {name: 'documento', index: 'documento', width: 110, sortable: true, align: 'center', hidden: false},
                {name: 'cmc', index: 'cmc', width: 100, sortable: true, align: 'center', hidden: true},
                {name: 'fecha_doc', index: 'fecha_doc', width: 100, sortable: true, align: 'center', hidden: true},
                {name: 'cod_neg', index: 'cod_neg', width: 110, sortable: true, align: 'center', hidden: true},
                {name: 'fecha_ant', index: 'fecha_ant', width: 110, sortable: true, align: 'center', hidden: false},
                {name: 'tipo', index: 'tipo', width: 110, sortable: true, align: 'center', hidden: true},
                {name: 'saldo_inicial', index: 'saldo_inicial', width: 90, sortable: true, align: 'center', hidden: false},
                {name: 'tasa', index: 'tasa', width: 100, sortable: true, align: 'center', hidden: true},
                {name: 'fch_cuota_manejo_causada', index: 'fch_cuota_manejo_causada', width: 130, sortable: true, align: 'center', hidden: true}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pager: '#pager',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            sortname: 'invdate',
            ondblClickRow: function (rowid, iRow, iCol, e) {
//                
            }, gridComplete: function () {
//               
            },
            onSelectAll: function (rowid, e, aRowid) {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var allRowsInGrid = jQuery("#tabla_negocios").jqGrid('getGridParam', 'selarrrow');
                var allRowsId = jQuery("#tabla_negocios").jqGrid('getRowData');

            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
            },
            ajaxGridOptions: {
                data: {
                    opcion: 20,
                    negocio: $("#negocio").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_negocios").navButtonAdd('#pager', {
            caption: "Anular",
            id: 'anular',
            buttonicon: "ui-icon-del",
            onClickButton: function () {
//                var myGrid = $("#tabla_negocios"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
//                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                anularCuotamanejo();
            }
        });
    }
}

function reloadGridTabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                negocio: $("#negocio").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function anularCuotamanejo() {

    var selRowIds = $("#tabla_negocios").jqGrid('getGridParam', 'selarrrow'), rowData;
    if (selRowIds.length > 0) {
        var arrayJson = new Array();
        for (var i = 0; i < selRowIds.length; i++) {
            rowData = $("#tabla_negocios").jqGrid("getLocalRow", selRowIds[i]);
            //alert(rowData);
            arrayJson.push(rowData);
        }
    }

    if (selRowIds.length === 0) {
        mensajesDelSistema('No hay filas seleccionadas', '300', 'auto', false);
    } else {
        $.ajax({
            async: false,
            url: "./controller?estado=Admin&accion=Fintra",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 21,
                informacion: JSON.stringify({json: arrayJson})
            },
            success: function (json) {
                if (json.respuesta === 'GUARDADO') {
                    cargarNegocios();
                    mensajesDelSistema('Se realizo la anulacion', '300', 'auto', true);
                    
                } else {
                    mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}