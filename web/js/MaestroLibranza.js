/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//$(document).ready(function () {
//    cargarSalarioMinimo();
//});

function inicio1() {
    cargarSalarioMinimo();
}

function inicio2() {
    cargarOcupacionLaboral();
}

function inicio3() {
    cargarDescuentoLey();
}

function inicio4() {
    cargarExtraprimaLibranza();
}

function inicio5() {
    cargarOperacionesBancariasLibranzas();
}

function inicio6() {
    cargarDeduccionesLibranza();
}

function inicio7() {
    cargarFormalizarLibranza();
}

function inicio8() {
    cargarPerfeccionarCompraCortera();

}

function inicio9() {
    cargarDeduccionesMicrocredito();
}

function inicio10() {
    cargarEntidadesCompraCartera();
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function onKeyDecimal(e, num) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if (document.getElementById(num.id).value.indexOf('.') !== -1 && keynum === 46)
        return false;
    if ((keynum === 0 || keynum === 8 || keynum === 48 || keynum === 46)) {
        return true;
    }

    if (keynum >= 58) {
        return false;

    }

    return /\d/.test(String.fromCharCode(keynum));
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarSalarioMinimo() {
    var operacion;
    var grid_tabla_ = jQuery("#tabla_salarioMinimo");
    if ($("#gview_tabla_salarioMinimo").length) {
        reloadGridMostrar(grid_tabla_, 1);
    } else {
        grid_tabla_.jqGrid({
            caption: "Salario Minimo",
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '763',
            colNames: ['Id', 'Estado', 'A�o', 'Salario minimo diario', 'Salario minimo mensual', 'Variacion anual', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'ano', index: 'cod_controlcuentas', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'salario_minimo_diario', index: 'salario_minimo_diario', sortable: true, width: 120, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'salario_minimo_mensual', index: 'salario_minimo_mensual', sortable: true, width: 120, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'variacion_anual', index: 'variacion_anual', sortable: true, width: 120, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                //{name: 'salario_minimo_diario', index: 'descripcion', width: 120, sortable: true, align: 'left', hidden: false},
                //{name: 'salario_minimo_mensual', index: 'linea_neg', width: 120, sortable: true, align: 'left', hidden: false},
                //{name: 'variacion_anual', index: 'cuentaixm', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'cambio', index: 'cambio', width: 120, sortable: true, align: 'left', hidden: false}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 1
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_salarioMinimo").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_salarioMinimo").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoSalarioMinimo('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_salarioMinimo").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_salarioMinimo"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var ano = filas.ano;
                var salario_minimo_diario = filas.salario_minimo_diario;
                var salario_minimo_mensual = filas.salario_minimo_mensual;
                var variacion_anual = filas.variacion_anual;
                if (estado === 'Activo') {
                    ventanaSalarioMinimo(operacion, id, ano, salario_minimo_diario, salario_minimo_mensual, variacion_anual);
                } else if (estado === 'Inactivo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_salarioMinimo").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaSalarioMinimo(operacion);
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function ventanaSalarioMinimo(operacion, id, ano, salario_minimo_diario, salario_minimo_mensual, variacion_anual) {
    if (operacion === 'Nuevo') {
        $("#dialogsalarioMinimo").dialog({
            width: '895',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'SALARIO MINIMO',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarSalarioMinimo();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#ano").val('');
                    $("#smd").val('');
                    $("#smm").val('');
                    $("#va").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        $("#ano").val(ano);
        $("#smd").val(numberConComas(salario_minimo_diario));
        $("#smm").val(numberConComas(salario_minimo_mensual));
        $("#va").val(numberConComas(variacion_anual));

        $("#dialogsalarioMinimo").dialog({
            width: '895',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'SALARIO MINIMO',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarSalarioMinimo();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#ano").val('');
                    $("#smd").val('');
                    $("#smm").val('');
                    $("#va").val('');
                }
            }
        });
    }
}

function guardarSalarioMinimo() {
    var ano = $("#ano").val();
    var smd = ($("#smd").val());
    var smm = ($("#smm").val());
    var va = ($("#va").val());
    if (ano !== '' && smd !== '' && smm !== '' && va !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 2,
                ano: $("#ano").val(),
                smd: numberSinComas($("#smd").val()),
                smm: numberSinComas($("#smm").val()),
                va: numberSinComas($("#va").val())
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                    $("#ano").val('');
                    $("#smd").val('');
                    $("#smm").val('');
                    $("#va").val('');
                }
                cargarSalarioMinimo();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function ActualizarSalarioMinimo() {
    var ano = $("#ano").val();
    var smd = ($("#smd").val());
    var smm = ($("#smm").val());
    var va = ($("#va").val());
    if (ano !== '' && smd !== '' && smm !== '' && va !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 3,
                id: $("#id").val(),
                ano: $("#ano").val(),
                smd: numberSinComas($("#smd").val()),
                smm: numberSinComas($("#smm").val()),
                va: numberSinComas($("#va").val())
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#id").val('');
                    $("#ano").val('');
                    $("#smd").val('');
                    $("#smm").val('');
                    $("#va").val('');
                }
                cargarSalarioMinimo();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function CambiarEstadoSalarioMinimo(rowid) {
    var grid_tabla = jQuery("#tabla_salarioMinimo");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 4,
            id: id
        },
        success: function (data) {
            cargarSalarioMinimo();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function cargarOcupacionLaboral() {
    var operacion;
    var grid_tabla_ = jQuery("#tabla_ocupacionLaboral");
    if ($("#gview_tabla_ocupacionLaboral").length) {
        reloadGridMostrar(grid_tabla_, 9);
    } else {
        grid_tabla_.jqGrid({
            caption: "Ocupacion Laboral",
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '386',
            colNames: ['Id', 'Estado', 'Descripcion', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'cambio', index: 'cambio', width: 120, sortable: true, align: 'left', hidden: false}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 9
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_ocupacionLaboral").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_ocupacionLaboral").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoOcupacionLaboral('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_ocupacionLaboral").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_ocupacionLaboral"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var descripcion = filas.descripcion;

                if (estado === 'Activo') {
                    ventanaOcupacionLaboral(operacion, id, descripcion);
                } else if (estado === 'Incativo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_ocupacionLaboral").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaOcupacionLaboral(operacion);
            }
        });
    }
}

function ventanaOcupacionLaboral(operacion, id, descripcion) {
    if (operacion === 'Nuevo') {
        $("#dialogocupacionLaboral").dialog({
            width: '269',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'OCUPACION LABORAL',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarOcupacionLaboral();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#descripcion").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        $("#descripcion").val(descripcion);
        $("#dialogocupacionLaboral").dialog({
            width: '269',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'OCUPACION LABORAL',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarOcupacionLaboral();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#descripcion").val('');
                }
            }
        });
    }
}

function guardarOcupacionLaboral() {
    var descripcion = $("#descripcion").val();
    if (descripcion !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 10,
                descripcion: $("#descripcion").val()
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                    $("#id").val('');
                    $("#descripcion").val('');
                }
                cargarOcupacionLaboral();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function ActualizarOcupacionLaboral() {
    var descripcion = $("#descripcion").val();
    if (descripcion !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 11,
                id: $("#id").val(),
                descripcion: $("#descripcion").val()
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#id").val('');
                    $("#descripcion").val('');
                }
                cargarOcupacionLaboral();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }
}

function CambiarEstadoOcupacionLaboral(rowid) {
    var grid_tabla = jQuery("#tabla_ocupacionLaboral");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 12,
            id: id
        },
        success: function (data) {
            cargarOcupacionLaboral();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function cargarDescuentoLey() {
    var operacion;
    var grid_tabla_ = jQuery("#tabla_DescuentosLey");
    if ($("#gview_tabla_DescuentosLey").length) {
        reloadGridMostrar(grid_tabla_, 13);
    } else {
        grid_tabla_.jqGrid({
            caption: "Descuentos Ley",
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '893',
            colNames: ['Id', 'Estado', 'id_ocupacion_laboral', 'Ocupacion laboral', 'Descripcion', 'Smlv inicial', 'Smlv final', '% descuento', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'id_ocupacion_laboral', index: 'id_ocupacion_laboral', width: 80, sortable: true, align: 'center', hidden: true},
                {name: 'laboral', index: 'laboral', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 270, sortable: true, align: 'left', hidden: false},
                {name: 'smlv_inicial', index: 'smlv_inicial', sortable: true, width: 80, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'smlv_final', index: 'smlv_final', sortable: true, width: 80, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'total_descuento', index: 'total_descuento', sortable: true, width: 80, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                // {name: 'smlv_inicial', index: 'smlv_inicial', width: 80, sortable: true, align: 'right', hidden: false},
                // {name: 'smlv_final', index: 'smlv_final', width: 80, sortable: true, align: 'right', hidden: false},
                // {name: 'total_descuento', index: 'total_descuento', width: 80, sortable: true, align: 'right', hidden: false},
                {name: 'cambio', index: 'cambio', width: 120, sortable: true, align: 'left', hidden: false}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 13
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_DescuentosLey").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_DescuentosLey").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoDescuentosLey('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_DescuentosLey").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_DescuentosLey"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var descripcion = filas.descripcion;
                var ocupacion_laboral = filas.id_ocupacion_laboral;
                var smlv_inicial = filas.smlv_inicial;
                var smlv_final = filas.smlv_final;
                var total_descuento = filas.total_descuento;
                if (estado === 'Activo') {
                    ventanaDescuentoLey(operacion, id, ocupacion_laboral, descripcion, smlv_inicial, smlv_final, total_descuento);
                } else if (estado === 'Inactivo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_DescuentosLey").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaDescuentoLey(operacion);
            }
        });
    }
}

function ventanaDescuentoLey(operacion, id, ocupacion_laboral, descripcion, smlv_inicial, smlv_final, total_descuento) {
    if (operacion === 'Nuevo') {
        cargarComboOcupacionLab();
        $("#dialogDescuentosLey").dialog({
            width: '1156',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'DESCUENTO LEY',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarDescuentoLey();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#ocupacion_laboral").val('');
                    $("#descripcion").val('');
                    $("#smlv_inicial").val('');
                    $("#smlv_final").val('');
                    $("#totaldesc").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        cargarComboOcupacionLab();
        $("#ocupacion_laboral").val(ocupacion_laboral);
        $("#descripcion").val(descripcion);
        $("#smlv_inicial").val(numberConComas(smlv_inicial));
        $("#smlv_final").val(numberConComas(smlv_final));
        $("#totaldesc").val(numberConComas(total_descuento));
        $("#dialogDescuentosLey").dialog({
            width: '1156',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'DESCUENTO LEY',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarDescuentoLey();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#ocupacion_laboral").val('');
                    $("#descripcion").val('');
                    $("#smlv_inicial").val('');
                    $("#smlv_final").val('');
                    $("#totaldesc").val('');
                }
            }
        });
    }
}

function guardarDescuentoLey() {
    var ocupacion_laboral = $("#ocupacion_laboral").val();
    var descripcion = $("#descripcion").val();
    var smlv_inicial = numberSinComas($("#smlv_inicial").val());
    var smlv_final = numberSinComas($("#smlv_final").val());
    var totaldesc = numberSinComas($("#totaldesc").val());

    if (ocupacion_laboral !== '' && descripcion !== '' && smlv_inicial !== '' && smlv_final !== '' && totaldesc !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 14,
                ocupacion_laboral: $("#ocupacion_laboral").val(),
                descripcion: $("#descripcion").val(),
                smlv_inicial: numberSinComas($("#smlv_inicial").val()),
                smlv_final: numberSinComas($("#smlv_final").val()),
                totaldesc: numberSinComas($("#totaldesc").val())
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                    $("#id").val('');
                    $("#ocupacion_laboral").val('');
                    $("#descripcion").val('');
                    $("#smlv_inicial").val('');
                    $("#smlv_final").val('');
                    $("#totaldesc").val('');
                }
                cargarDescuentoLey();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }
}

function ActualizarDescuentoLey() {
    var ocupacion_laboral = $("#ocupacion_laboral").val();
    var descripcion = $("#descripcion").val();
    var smlv_inicial = numberSinComas($("#smlv_inicial").val());
    var smlv_final = numberSinComas($("#smlv_final").val());
    var totaldesc = numberSinComas($("#totaldesc").val());

    if (ocupacion_laboral !== '' && descripcion !== '' && smlv_inicial !== '' && smlv_final !== '' && totaldesc !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 15,
                id: $("#id").val(),
                ocupacion_laboral: $("#ocupacion_laboral").val(),
                descripcion: $("#descripcion").val(),
                smlv_inicial: numberSinComas($("#smlv_inicial").val()),
                smlv_final: numberSinComas($("#smlv_final").val()),
                totaldesc: numberSinComas($("#totaldesc").val())
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#id").val('');
                    $("#ocupacion_laboral").val('');
                    $("#descripcion").val('');
                    $("#smlv_inicial").val('');
                    $("#smlv_final").val('');
                    $("#totaldesc").val('');
                }
                cargarDescuentoLey();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }
}

function CambiarEstadoDescuentosLey(rowid) {
    var grid_tabla = jQuery("#tabla_DescuentosLey");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 16,
            id: id
        },
        success: function (data) {
            cargarDescuentoLey();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function cargarComboOcupacionLab() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 17
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#ocupacion_laboral').html('');
                $('#ocupacion_laboral').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#ocupacion_laboral').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarExtraprimaLibranza() {
    var operacion;
    var grid_tabla_ = jQuery("#tabla_ExtraprimaLibranza");
    if ($("#gview_tabla_ExtraprimaLibranza").length) {
        reloadGridMostrar(grid_tabla_, 18);
    } else {
        grid_tabla_.jqGrid({
            caption: "Extraprima Libranza",
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '893',
            colNames: ['Id', 'Estado', 'id_ocupacion_laboral', 'Ocupacion laboral', 'Descripcion', 'Edad inicial', 'Edad final', '% Extraprima', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'id_ocupacion_laboral', index: 'id_ocupacion_laboral', width: 80, sortable: true, align: 'center', hidden: true},
                {name: 'laboral', index: 'laboral', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 270, sortable: true, align: 'left', hidden: false},
                {name: 'edad_inicial', index: 'edad_inicial', width: 80, sortable: true, align: 'right', hidden: false},
                {name: 'edad_final', index: 'edad_final', width: 80, sortable: true, align: 'right', hidden: false},
                {name: 'perc_extraprima', index: 'perc_extraprima', sortable: true, width: 80, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                //  {name: 'perc_extraprima', index: 'perc_extraprima', width: 80, sortable: true, align: 'right', hidden: false},
                {name: 'cambio', index: 'cambio', width: 120, sortable: true, align: 'left', hidden: false}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 18
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_ExtraprimaLibranza").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_ExtraprimaLibranza").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoExtraprimaLibranza('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_ExtraprimaLibranza").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_ExtraprimaLibranza"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var descripcion = filas.descripcion;
                var ocupacion_laboral = filas.id_ocupacion_laboral;
                var edad_inicial = filas.edad_inicial;
                var edad_final = filas.edad_final;
                var perc_extraprima = filas.perc_extraprima;
                if (estado === 'Activo') {
                    ventanaExtraprimaLibranza(operacion, id, ocupacion_laboral, descripcion, edad_inicial, edad_final, perc_extraprima);
                } else if (estado === 'Inactivo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_ExtraprimaLibranza").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaExtraprimaLibranza(operacion);
            }
        });
    }
}

function ventanaExtraprimaLibranza(operacion, id, ocupacion_laboral, descripcion, edad_inicial, edad_final, perc_extraprima) {
    if (operacion === 'Nuevo') {
        cargarComboOcupacionLab();
        $("#dialogExtraprimaLibranza").dialog({
            width: '1160',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'EXTRAPRIMA LIBRANZA',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarExtraprimaLibranza();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#ocupacion_laboral").val('');
                    $("#descripcion").val('');
                    $("#edad_inicial").val('');
                    $("#edad_final").val('');
                    $("#perc_extraprima").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        cargarComboOcupacionLab();
        $("#ocupacion_laboral").val(ocupacion_laboral);
        $("#descripcion").val(descripcion);
        $("#edad_inicial").val(edad_inicial);
        $("#edad_final").val(edad_final);
        $("#perc_extraprima").val(perc_extraprima);
        $("#dialogExtraprimaLibranza").dialog({
            width: '1160',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'EXTRAPRIMA LIBRANZA',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarExtraprimaLibranza();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#ocupacion_laboral").val('');
                    $("#descripcion").val('');
                    $("#edad_inicial").val('');
                    $("#edad_final").val('');
                    $("#perc_extraprima").val('');
                }
            }
        });
    }
}

function guardarExtraprimaLibranza() {
    var ocupacion_laboral = $("#ocupacion_laboral").val();
    var descripcion = $("#descripcion").val();
    var edad_inicial = $("#edad_inicial").val();
    var edad_final = $("#edad_final").val();
    var perc_extraprima = $("#perc_extraprima").val();
    if (ocupacion_laboral !== '' && descripcion !== '' && edad_inicial !== '' && edad_final !== '' && perc_extraprima !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 19,
                ocupacion_laboral: $("#ocupacion_laboral").val(),
                descripcion: $("#descripcion").val(),
                edad_inicial: $("#edad_inicial").val(),
                edad_final: $("#edad_final").val(),
                perc_extraprima: $("#perc_extraprima").val()
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                    $("#id").val('');
                    $("#ocupacion_laboral").val('');
                    $("#descripcion").val('');
                    $("#edad_inicial").val('');
                    $("#edad_final").val('');
                    $("#perc_extraprima").val('');
                }
                cargarExtraprimaLibranza();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function ActualizarExtraprimaLibranza() {
    var ocupacion_laboral = $("#ocupacion_laboral").val();
    var descripcion = $("#descripcion").val();
    var edad_inicial = $("#edad_inicial").val();
    var edad_final = $("#edad_final").val();
    var perc_extraprima = $("#perc_extraprima").val();
    if (ocupacion_laboral !== '' && descripcion !== '' && edad_inicial !== '' && edad_final !== '' && perc_extraprima !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 20,
                id: $("#id").val(),
                ocupacion_laboral: $("#ocupacion_laboral").val(),
                descripcion: $("#descripcion").val(),
                edad_inicial: $("#edad_inicial").val(),
                edad_final: $("#edad_final").val(),
                perc_extraprima: $("#perc_extraprima").val()
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#id").val('');
                    $("#ocupacion_laboral").val('');
                    $("#descripcion").val('');
                    $("#edad_inicial").val('');
                    $("#edad_final").val('');
                    $("#perc_extraprima").val('');
                }
                cargarExtraprimaLibranza();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }
}

function CambiarEstadoExtraprimaLibranza(rowid) {
    var grid_tabla = jQuery("#tabla_ExtraprimaLibranza");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 21,
            id: id
        },
        success: function (data) {
            cargarExtraprimaLibranza();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function cargarOperacionesBancariasLibranzas() {
    var operacion;
    var grid_tabla_ = jQuery("#tabla_OpBancariaLib");
    if ($("#gview_tabla_OpBancariaLib").length) {
        reloadGridMostrar(grid_tabla_, 22);
    } else {
        grid_tabla_.jqGrid({
            caption: "OPERACIONES LIBRANZA",
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '1030',
            colNames: ['Id', 'Estado', 'id_tipo_operacion_libranza', 'Tipo operacion libranza', 'idop', 'Descripcion', 'Tipo documento', 'Hc cabecera', 'Cuenta cabecera', 'Cuenta detalle', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'id_tipo_operacion_libranza', index: 'id_tipo_operacion_libranza', width: 80, sortable: true, align: 'center', hidden: true},
                {name: 'tipo_operacion_libranza', index: 'tipo_operacion_libranza', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'idop', index: 'idop', width: 80, sortable: true, align: 'center', hidden: true},
                {name: 'descripcion', index: 'descripcion', width: 250, sortable: true, align: 'left', hidden: false},
                {name: 'tipo_documento', index: 'tipo_documento', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'hc_cabecera', index: 'hc_cxp', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'cuenta_cabecera', index: 'cuenta_cabecera', width: 80, sortable: true, align: 'right', hidden: false},
                {name: 'cuenta_detalle', index: 'cuenta_detalle', width: 80, sortable: true, align: 'right', hidden: false},
                {name: 'cambio', index: 'cambio', width: 120, sortable: true, align: 'left', hidden: false}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 22
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_OpBancariaLib").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_OpBancariaLib").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoOperacionesBancariasLibranzas('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_OpBancariaLib").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_OpBancariaLib"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var descripcion = filas.descripcion;
                var idop = filas.idop;
                var hc_cxp = filas.hc_cabecera;
                var cuenta_cxp = filas.cuenta_cabecera;
                var cuenta_detalle = filas.cuenta_detalle;
                var tipo_documento = filas.tipo_documento;
                var id_tipo_operacion_libranza = filas.id_tipo_operacion_libranza;
                if (estado === 'Activo') {
                    ventanaOperacionesBancariasLibranzas(operacion, id, idop, descripcion, hc_cxp, cuenta_cxp, cuenta_detalle, tipo_documento, id_tipo_operacion_libranza);
                } else if (estado === 'Inactivo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_OpBancariaLib").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaOperacionesBancariasLibranzas(operacion);
            }
        });
    }
}

//limpiar();
//    var polizas = $("#polizas option:selected").text();
//    if (polizas === 'SEGURO DE AUTOMOVIL') {
//        $("#convenio").attr("disabled", true);
//    } else {
//        $("#convenio").val('');
//        $("#convenio").attr("disabled", false);
//    }

function ventanaOperacionesBancariasLibranzas(operacion, id, idop, descripcion, hc_cxp, cuenta_cxp, cuenta_detalle, tipo_documento, id_tipo_operacion_libranza) {
    $('input[type=checkbox]').live('click', function () {
        var parent = $(this).parent().attr('id');
        $('#' + parent + ' input[type=checkbox]').removeAttr('checked');
        $(this).attr('checked', 'checked');
    });
    if (operacion === 'Nuevo') {
        cargarComboTipoDoc();
        cargarHC();
        cargarCuentaHC();
        cargarComboTipoOperacionBancaria();
        $("#dialogOpBancariaLib").dialog({
            width: '315',
            height: '300',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'OPERACIONES LIBRANZA',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    if (tipo_documento === '' && descripcion === '' && (hc_cxp === '' && cuenta_cxp === '')) {
                        mensajesDelSistema("Faltan datos", '230', '150', true);
                    } else {
                        guardarOperacionesBancariasLibranzas();
                    }
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#tipo_documento").val('');
                    $("#descripcion").val('');
                    $("#cmc").val('');
                    $("#cuenta_cxp").val('');
                    $("#cuenta").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        cargarComboTipoDoc();
        $("#tipo_documento").val(idop);
        cargarHC();
        $("#cmc").val(hc_cxp);
        cargarCuentaHC();
        checking(hc_cxp, cuenta_cxp);
        $("#descripcion").val(descripcion);
        $("#descripcion").val(descripcion);
        $("#cuenta_cxp").val(cuenta_cxp);
        $("#cuenta_detalle").val(cuenta_detalle);
        cargarComboTipoOperacionBancaria();
        $("#tipoperacion").val(id_tipo_operacion_libranza);
        $("#dialogOpBancariaLib").dialog({
            width: '315',
            height: '300',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'OPERACIONES LIBRANZA',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarOperacionesBancariasLibranzas();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#tipo_documento").val('');
                    $("#descripcion").val('');
                    $("#cmc").val('');
                    $("#cuenta_cxp").val('');
                    $("#cuenta").val('');
                }
            }
        });
    }
}

function checking(hc_cxp, cuenta_cxp) {
    if (hc_cxp === '') {
        $('#cuenta_cab').attr('checked', true);
        $('#hc_cab').attr('checked', false);
    } else if (cuenta_cxp === '') {
        $('#cuenta_cab').attr('checked', false);
        $('#hc_cab').attr('checked', true);
    }
}

function valor(id) {
    if (id === 'hc_cab') {
        $("#cuenta_cxp").attr('readOnly', true);
        $("#cuenta_cxp").val('');
        $("#cmc").attr("disabled", false);
    } else if (id === 'cuenta_cab') {
        $("#cuenta_cxp").attr('readOnly', false);
        $("#cmc").val('');
        $("#cmc").attr("disabled", true);
        $("#cuenta").val('');
    }
}

function cargarHC() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 31,
            tipodoc: $("#tipo_documento").val()
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#cmc').html('');
                $('#cmc').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#cmc').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
                cargarCuentaHC();
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarComboTipoDoc() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 26
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#tipo_documento').html('');
                $('#tipo_documento').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#tipo_documento').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function guardarOperacionesBancariasLibranzas() {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 23,
            tipo_documento: $("#tipo_documento").val(),
            descripcion: $("#descripcion").val(),
            cmc: $("#cmc").val(),
            cuenta_cxp: $("#cuenta_cxp").val(),
            cuenta_detalle: $("#cuenta_detalle").val(),
            tipo_operacion: $("#tipoperacion").val()
        },
        success: function (data, textStatus, jqXHR) {
            if (data.respuesta === 'Guardado') {
                mensajesDelSistema("Exito al guardar", '230', '150', true);
                $("#id").val('');
                $("#tipo_documento").val('');
                $("#descripcion").val('');
                $("#cmc").val('');
                $("#cuenta_cxp").val('');
                $("#cuenta").val('');
                $("#cuenta_detalle").val('');
                $("#tipoperacion").val('');
            }
            cargarOperacionesBancariasLibranzas();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function ActualizarOperacionesBancariasLibranzas() {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 24,
            id: $("#id").val(),
            tipo_documento: $("#tipo_documento").val(),
            descripcion: $("#descripcion").val(),
            cmc: $("#cmc").val(),
            cuenta_cxp: $("#cuenta_cxp").val(),
            cuenta_detalle: $("#cuenta_detalle").val(),
            tipo_operacion: $("#tipoperacion").val()
        },
        success: function (data, textStatus, jqXHR) {
            if (data.respuesta === 'Guardado') {
                mensajesDelSistema("Exito al actualizar", '230', '150', true);
                $("#id").val('');
                $("#tipo_documento").val('');
                $("#descripcion").val('');
                $("#cmc").val('');
                $("#cuenta_cxp").val('');
                $("#cuenta").val('');
                $("#cuenta_detalle").val('');
                $("#tipoperacion").val('');
            }
            cargarOperacionesBancariasLibranzas();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function CambiarEstadoOperacionesBancariasLibranzas(rowid) {
    var grid_tabla = jQuery("#tabla_OpBancariaLib");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 25,
            id: id
        },
        success: function (data) {
            cargarOperacionesBancariasLibranzas();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function cargarDeduccionesLibranza() {
    var operacion;
    var grid_tabla_ = jQuery("#tabla_DeduccionesLib");
    if ($("#gview_tabla_DeduccionesLib").length) {
        reloadGridMostrar(grid_tabla_, 27);
    } else {
        grid_tabla_.jqGrid({
            caption: "DEDUCCIONES BANCARIAS",
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '1135',
            colNames: ['Id', 'Estado', 'Id ocupacion_laboral', 'Ocupacion laboral', 'id_operacion_bancaria', 'Operacion libranza', 'Descripcion', 'Desembolso inicial', 'Desembolso final', 'Valor cobrar', '% Cobrar', 'Nxmil', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'id_ocupacion_laboral', index: 'id_ocupacion_laboral', width: 80, sortable: true, align: 'center', hidden: true},
                {name: 'ocupacion_laboral', index: 'ocupacion_laboral', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'id_operacion_libranza', index: 'id_operacion_bancaria', width: 80, sortable: true, align: 'center', hidden: true},
                {name: 'operacion_libranza', index: 'operacion_bancaria', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 200, sortable: true, align: 'left', hidden: false},
                {name: 'desembolso_inicial', index: 'desembolso_inicial', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'desembolso_final', index: 'desembolso_final', sortable: true, width: 80, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_cobrar', index: 'valor_cobrar', sortable: true, width: 80, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'perc_cobrar', index: 'perc_cobrar', sortable: true, width: 80, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'n_xmil', index: 'n_xmil', sortable: true, width: 80, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                {name: 'cambio', index: 'cambio', width: 120, sortable: true, align: 'left', hidden: false}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 27
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_DeduccionesLib").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_DeduccionesLib").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoDeduccionesLibranza('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_DeduccionesLib").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_DeduccionesLib"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var id_operacion_bancaria = filas.id_operacion_libranza;
                var descripcion = filas.descripcion;
                var desembolso_inicial = filas.desembolso_inicial;
                var desembolso_final = filas.desembolso_final;
                var valor_cobrar = filas.valor_cobrar;
                var perc_cobrar = filas.perc_cobrar;
                var n_xmil = filas.n_xmil;
                var cuenta_cabecera = filas.cuenta_cabecera;
                var cuenta_detalle = filas.cuenta_detalle;
                var id_ocupacion_laboral = filas.id_ocupacion_laboral;
                if (estado === 'Activo') {
                    ventanaDeduccionesLibranza(operacion, id, id_operacion_bancaria, descripcion, desembolso_inicial, desembolso_final, valor_cobrar, perc_cobrar, n_xmil, id_ocupacion_laboral);
                } else if (estado === 'Inactivo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_DeduccionesLib").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaDeduccionesLibranza(operacion);
            }
        });
    }
}

function ventanaDeduccionesLibranza(operacion, id, id_operacion_bancaria, descripcion, desembolso_inicial, desembolso_final, valor_cobrar, perc_cobrar, n_xmil, id_ocupacion_laboral) {
    if (operacion === 'Nuevo') {
        cargarComboOperacionesBancarias(operacion);
        cargarComboOcupacionLab();
        $("#dialogDeduccionesLib").dialog({
            width: '315',
            height: '320',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'OPERACIONES BANCARIAS LIBRANZAS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarDeduccionesLibranza();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#operacion_bancaria").val('');
                    $("#descripcion").val('');
                    $("#desembolso_inicial").val('');
                    $("#desembolso_final").val('');
                    $("#valor_cobrar").val('');
                    $("#perc_cobrar").val('');
                    $("#n_xmil").val('');
                    $("#cuenta_cabecera").val('');
                    $("#cuenta_detalle").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        cargarComboOperacionesBancarias(operacion);
        $("#operacion_bancaria").val(id_operacion_bancaria);
        cargarComboTipoDoc();
        $("#descripcion").val(descripcion);
        $("#desembolso_inicial").val(numberConComas(desembolso_inicial));
        $("#desembolso_final").val(numberConComas(desembolso_final));
        $("#valor_cobrar").val(numberConComas(valor_cobrar));
        $("#perc_cobrar").val(numberConComas(perc_cobrar));
        $("#n_xmil").val(numberConComas(n_xmil));
        cargarComboOcupacionLab();
        $("#ocupacion_laboral").val(id_ocupacion_laboral);
        $("#dialogDeduccionesLib").dialog({
            width: '315',
            height: '320',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'OPERACIONES BANCARIAS LIBRANZAS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarDeduccionesLibranza();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#operacion_bancaria").val('');
                    $("#descripcion").val('');
                    $("#desembolso_inicial").val('');
                    $("#desembolso_final").val('');
                    $("#valor_cobrar").val('');
                    $("#perc_cobrar").val('');
                    $("#n_xmil").val('');
                    $("#cuenta_cabecera").val('');
                    $("#cuenta_detalle").val('');
                }
            }
        });
    }
}

function guardarDeduccionesLibranza() {
    var operacion_bancaria = $("#operacion_bancaria").val();
    var descripcion = $("#descripcion").val();
    var desembolso_inicial = numberSinComas($("#desembolso_inicial").val());
    var desembolso_final = numberSinComas($("#desembolso_final").val());
    var valor_cobrar = numberSinComas($("#valor_cobrar").val());
    var perc_cobrar = numberSinComas($("#perc_cobrar").val());
    var n_xmil = numberSinComas($("#n_xmil").val());
    var ocupacion_laboral = $("#ocupacion_laboral").val();

    if (operacion_bancaria !== '' && descripcion !== '' && desembolso_inicial !== '' && desembolso_final !== '' && desembolso_final !== '' && ocupacion_laboral !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 28,
                operacion_bancaria: $("#operacion_bancaria").val(),
                descripcion: $("#descripcion").val(),
                desembolso_inicial: numberSinComas($("#desembolso_inicial").val()),
                desembolso_final: numberSinComas($("#desembolso_final").val()),
                valor_cobrar: numberSinComas($("#valor_cobrar").val()),
                perc_cobrar: numberSinComas($("#perc_cobrar").val()),
                n_xmil: numberSinComas($("#n_xmil").val()),
                ocupacion_laboral: $("#ocupacion_laboral").val()
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                    $("#id").val('');
                    $("#operacion_bancaria").val('');
                    $("#descripcion").val('');
                    $("#desembolso_inicial").val('');
                    $("#desembolso_final").val('');
                    $("#valor_cobrar").val('');
                    $("#perc_cobrar").val('');
                    $("#n_xmil").val('');
                    $("#cuenta_cabecera").val('');
                    $("#cuenta_detalle").val('');
                }
                cargarDeduccionesLibranza();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("faltan datos", '230', '150', true);
    }
}

function ActualizarDeduccionesLibranza() {
    var operacion_bancaria = $("#operacion_bancaria").val();
    var descripcion = $("#descripcion").val();
    var desembolso_inicial = numberSinComas($("#desembolso_inicial").val());
    var desembolso_final = numberSinComas($("#desembolso_final").val());
    var valor_cobrar = numberSinComas($("#valor_cobrar").val());
    var perc_cobrar = numberSinComas($("#perc_cobrar").val());
    var n_xmil = numberSinComas($("#n_xmil").val());
    var ocupacion_laboral = $("#ocupacion_laboral").val();
    if (operacion_bancaria !== '' && descripcion !== '' && desembolso_inicial !== '' && desembolso_final !== '' && desembolso_final !== '' && ocupacion_laboral !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 29,
                id: $("#id").val(),
                operacion_bancaria: $("#operacion_bancaria").val(),
                descripcion: $("#descripcion").val(),
                desembolso_inicial: numberSinComas($("#desembolso_inicial").val()),
                desembolso_final: numberSinComas($("#desembolso_final").val()),
                valor_cobrar: numberSinComas($("#valor_cobrar").val()),
                perc_cobrar: numberSinComas($("#perc_cobrar").val()),
                n_xmil: numberSinComas($("#n_xmil").val()),
                ocupacion_laboral: $("#ocupacion_laboral").val()
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#id").val('');
                    $("#operacion_bancaria").val('');
                    $("#descripcion").val('');
                    $("#desembolso_inicial").val('');
                    $("#desembolso_final").val('');
                    $("#valor_cobrar").val('');
                    $("#perc_cobrar").val('');
                    $("#n_xmil").val('');
                    $("#cuenta_cabecera").val('');
                    $("#cuenta_detalle").val('');
                }
                cargarDeduccionesLibranza();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("faltan datos", '230', '150', true);
    }

}

function CambiarEstadoDeduccionesLibranza(rowid) {
    var grid_tabla = jQuery("#tabla_DeduccionesLib");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 30,
            id: id
        },
        success: function (data) {
            cargarDeduccionesLibranza();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function cargarComboOperacionesBancarias(operacion) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 33,
            operacion: operacion
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#operacion_bancaria').html('');
                $('#operacion_bancaria').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#operacion_bancaria').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCuentaHC() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 32,
            tipodoc: $("#tipo_documento").val(),
            cmc: $("#cmc").val()
        },
        success: function (json) {
            $("#cuenta").val(json[0].cuenta);
            console.log(json[0].cuenta);
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function changeStatusCobraValues(e, fieldId, value) {
    if (e.keyCode !== 9) {
        var vlr = (value !== '') ? 0 : '';
        if (fieldId === 'valor_cobrar') {
            $('#perc_cobrar').val(vlr);
            $('#n_xmil').val(vlr);

        } else if (fieldId === 'perc_cobrar') {
            $('#valor_cobrar').val(vlr);
            $('#n_xmil').val(vlr);

        } else {
            $('#valor_cobrar').val(vlr);
            $('#perc_cobrar').val(vlr);

        }
    }
}

function cargarFormalizarLibranza() {
    var grid_tabla = jQuery("#tabla_FormalizarLib");
    if ($("#gview_tabla_FormalizarLib").length) {
        reloadGridMostrarTabla(grid_tabla, 10);
    } else {
        grid_tabla.jqGrid({
            caption: "Formalizar compra cartera",
            url: './controller?estado=Formulario&accion=FiltroLibranza',
            mtype: "POST",
            async: false,
            datatype: "json",
            height: '430',
            width: '857',
            colNames: ['Negocio', 'Numero solicitud', 'Cedula', 'Nombre cliente', 'Tasa interes', 'Cuotas', 'Vlr negocio', 'Fecha negocio', 'Acciones'],
            colModel: [
                {name: 'cod_neg', index: 'cod_neg', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'numero_solicitud', index: 'numero_solicitud', width: 90, sortable: true, align: 'center', hidden: true, search: true},
                {name: 'cod_cli', index: 'cod_cli', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 195, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'tasa_interes', index: 'tasa_interes', width: 80, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nro_docs', index: 'nro_docs', width: 80, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'vr_negocio', index: 'vr_negocio', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'fecha_negocio', index: 'fecha_negocio', width: 120, sortable: true, align: 'center', hidden: true, search: true},
                {name: 'acciones', index: 'acciones', width: 155, sortable: true, align: 'left', hidden: false, search: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {

            }, gridComplete: function () {
                var vr_negocio = grid_tabla.jqGrid('getCol', 'vr_negocio', false, 'sum');
                grid_tabla.jqGrid('footerData', 'set', {nro_docs: 'TOTAL', vr_negocio: vr_negocio});

                var cant = jQuery("#tabla_FormalizarLib").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cl = cant[i];
                    form = "<input style='height:20px;width:68px;margin-left: 8px;' type='button' value='Formalizar' onclick=\"ventana('" + cl + "');\" />";
                    camb = "<input style='height:20px;width:68px;margin-left: 8px;' type='button' value='Editar $' onclick=\"CambioValores('" + cl + "');\" />";
                    //camb = "<img src='/fintra/images/botones/iconos/editart.png' align='absbottom' style='height:20px;width:18px;margin-left: 8px;' value='cambio' title='Modificar obligaciones compra' onclick=\"CambioValores('" + cl + "');\" />";
                    jQuery("#tabla_FormalizarLib").jqGrid('setRowData', cant[i], {acciones: form + ' ' + camb});
                }

            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    //mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 10
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    }
}

function reloadGridMostrarTabla(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: './controller?estado=Formulario&accion=FiltroLibranza',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function ventana(cl) {
    cargarComboConcepto();
    informacionNegocio(cl);
    $("#dialogMsConcepto").dialog({
        width: '503',
        height: '450',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'CONCEPTO DE DECISION',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Formalizar": function () {
                formalizarLibranza();
            },
            "Salir": function () {
                $(this).dialog("close");
                $("#negocio").val('');
                $("#coment").val('');
            }
        }
    });
}

function cargarComboConcepto() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 44

        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#concepto').html('');
                $('#concepto').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#concepto').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function informacionNegocio(cl) {
    var grid_tabla = jQuery("#tabla_FormalizarLib");
    var cod_neg = grid_tabla.getRowData(cl).cod_neg;
    $("#negocio").val(cod_neg);
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 45,
            cod_neg: cod_neg
        },
        success: function (json) {
            $("#usuario").val(json[0].usuario);
            $("#fecha").val(json[0].fecha);
            $("#comentarios").val(json[0].comentarios);
            $("#decision").val(json[0].concepto);
        }

    });
}

function formalizarLibranza() {
    var concepto = $("#concepto").val();
    var coment = $("#coment").val();
    if (concepto !== '' && coment !== '') {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Maestro&accion=Libranza",
            dataType: 'text',
            async: false,
            data: {
                opcion: 46,
                negocio: $("#negocio").val(),
                concepto: $("#concepto").val(),
                coment: $("#coment").val()
            },
            success: function (json) {
                console.log(json);
                if (json === 'OK\n') {
                    mensajesDelSistema("Exito al formalizar", '204', '140', true);
                    $("#dialogMsConcepto").dialog("close");
                    cargarFormalizarLibranza();
                } else {
                    mensajesDelSistema("Error", '204', '140', false);
                }
            }
        });
    } else {
        mensajesDelSistema("Faltan datos", '204', '140', false);
    }
}

function cargarComboTipoOperacionBancaria() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 47
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#tipoperacion').html('');
                $('#tipoperacion').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#tipoperacion').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarPerfeccionarCompraCortera() {
    var grid_tabla = jQuery("#tabla_PerfeCC");
    if ($("#gview_tabla_PerfeCC").length) {
        reloadGridMostrarTabla(grid_tabla, 49);
    } else {
        grid_tabla.jqGrid({
            caption: "Perfecionamiento compra cartera",
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",
            datatype: "json",
            async: false,
            height: '430',
            width: '1000',
            colNames: ['Negocio', 'Documento cxp', 'Nombre cliente', 'Cedula', 'Vlr negocio', 'Fecha negocio', 'Cuotas', 'Tasa interes', 'Acciones'],
            colModel: [
                {name: 'cod_neg', index: 'cod_neg', width: 95, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'documento', index: 'documento', width: 95, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 225, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cod_cli', index: 'cod_cli', width: 95, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'vr_negocio', index: 'vr_negocio', sortable: true, width: 115, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'fecha_negocio', index: 'fecha_negocio', width: 125, sortable: true, align: 'center', hidden: true, search: true},
                {name: 'nro_docs', index: 'nro_docs', width: 85, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'tasa_interes', index: 'tasa_interes', width: 85, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'acciones', index: 'acciones', width: 115, sortable: true, align: 'left', hidden: false, search: false}
            ],
            subGrid: true,
            //subGridOptions: { reloadOnExpand : true},
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {

            }, gridComplete: function () {
                var vr_negocio = grid_tabla.jqGrid('getCol', 'vr_negocio', false, 'sum');
                grid_tabla.jqGrid('footerData', 'set', {cod_cli: 'TOTAL', vr_negocio: vr_negocio});

                //var cant = jQuery("#tabla_PerfeCC").jqGrid('getDataIDs');
                //for (var i = 0; i < cant.length; i++) {
                //    var cl = cant[i];
                //    form = "<input style='height:20px;width:68px;margin-left: 8px;' type='button' value='Formalizar' onclick=\"ventanaPerfeccionamiento('" + cl + "');\" />";
                //    jQuery("#tabla_PerfeCC").jqGrid('setRowData', cant[i], {acciones: form});
                //}
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    //mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 49
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    }
if(true){
        $("#tabla_PerfeCC").setGridParam({
            
            subGridRowExpanded: function (subgrid_id, row_id){
                var  pager_id;
                myrow_id = row_id;
                subgrid_table_id = subgrid_id + "_t";
                pager_id= "p_" + subgrid_table_id;
                $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                jQuery("#" + subgrid_table_id).jqGrid({
                    url: "./controller?estado=Maestro&accion=Libranza&doc_rel=" + row_id,
                    datatype: "json",
                    colNames: ['Documento', 'Nit Proveedor','Proveedor', 'Descripcion', 'Valor a transferir', 'Referencia 2', 'Tipo ref 2', 'Tipo ref 1', 'Archivo', ''],
                    colModel: [
                        {name: "documento", index: "documento", width: 60, align: "center", sortable: false,key: true},
                        {name: "proveedor", index: "proveedor", width: 65, sortable: false},
                        {name: "payment_name", index: "payment_name", width: 110, sortable: false},
                        {name: "descripcion", index: "descripcion", width: 250, align: "left", sortable: false},
                        {name: 'vlr_transferir', index: 'vlr_transferir', sortable: true, width: 95, align: 'right', search: true, sorttype: 'currency',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                        {name: "referencia_2", index: "referencia_2", width: 70, align: "left", sortable: false},
                        {name: "tipo_referencia_2", index: "tipo_referencia_2", width: 45, align: "left", sortable: false},
                        {name: "tipo_referencia_1", index: "tipo_referencia_1", width: 45, align: "left", sortable: false, hidden: true},
                        {name: 'archivo', index: 'archivo', width: '85px', align: 'center', fixed: true},
                        {name: 'verarchivo', index: 'verarchivo', width: 50, sortable: true, align: 'center', hidden: false, search: false}
                    ],
                    rowNum: 100,
                    //pager: pager_id,
                    sortname: 'num',
                    width: '950',
                    height: '80',
                    pgtext: null,
                    pgbuttons: false,
                    jsonReader: {
                        root: "rows",
                        repeatitems: false,
                        id: "0"
                    },
                    ajaxGridOptions: {
                         data: {
                            opcion: 64
                }
                    }, gridComplete: function () {
                        var cant = jQuery("#" + subgrid_id + "_t").jqGrid('getDataIDs');
                        
                        var listo = true;
                        for (var i = 0; i < cant.length; i++) {
                            var cl = cant[i];
                            myrow_id = cl;
                            console.log(cl);
                            var documento = jQuery("#" + subgrid_id + "_t").getRowData(cl).tipo_referencia_1;
                            if (documento === '') {
                                be = '<input style="height:20px;width:68px;margin-left: 8px;margin-right: 8px;" type="button" title="Subir" value="Subir" onclick="ventanaCargarPazySalvo(' + cl + ')" />';
                                listo = false;
                            } else {
                                be = '<input style="height:20px;width:68px;margin-left: 8px;margin-right: 8px;" type="button"disabled value="Subir" onclick="" />';
                                //be1 = '<input style="height:20px;width:68px;margin-left: 8px;margin-right: 8px;" type="button" value="Ver" onclick="ventanaVerArchivo(' + cl + ')" />';
                                be1 ='<img src = "/fintra/images/file_new.png"style = "margin-left: -4px; height: 19px; vertical-align: middle;" name="Descargar" id="Descargar" title="Descargar" onclick = "ventanaVerArchivo(' + cl + ')">'
                            jQuery("#" + subgrid_id + "_t").jqGrid('setRowData', cant[i], {verarchivo: be1});
                        }
                            jQuery("#" + subgrid_id + "_t").jqGrid('setRowData', cant[i], {archivo: be});
                       }
                            
                            
                        //var cant1 = jQuery("#tabla_PerfeCC").jqGrid('getDataIDs');
                       
                        if (listo === true) {
                            //form = " <span id ='" + row_id + "' class='form-submit-button form-submit-button-simple_green_apple ' style='font-size: 11px; padding: 3px;border: 1px solid #234684 none;' value='accion' onclick=\"preguntaPrefeccionar('" + row_id + "');\" >Perfeccionar</span> ";
                            form = "<input style='height:20px;width:73px;margin-left: 5px;margin-right: 5px;align: center;' type='button' value='Formalizar' onclick=\"ventanaPerfeccionamiento('" + row_id + "');\" />";
                        } else {
                            form = "<input style='height:20px;width:73px;margin-left: 5px;margin-right: 5px;align: center;' type='button'disabled value='Formalizar' onclick=\"ventanaPerfeccionamiento('" + row_id + "');\" />";
                        }
         
                        jQuery("#tabla_PerfeCC").jqGrid('setRowData', row_id, {acciones: form});
                        console.log('Si entra aqui');
                       
            }
            
                   
                    
                });
            } 
        });
    }    
}

var subgrid_table_id,myrow_id;


function reloadSubGridMostrarTabla(subgrid_table_id, opcion) {
    console.log(myrow_id);
    jQuery("#" + subgrid_table_id).setGridParam({
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza&doc_rel=" + myrow_id,
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    jQuery("#" + subgrid_table_id).trigger("reloadGrid");
}


function ventanaCargarPazySalvo(rowid) {
    console.log(rowid.id);
    $("#documento").val(rowid.id);
    $("#cargararchivo").dialog({
        width: 550,
        height: 200,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: true, 
        buttons: {          
            "Salir": function () {          
		$(this).dialog("close");               
            }
        }
    });
}


function ventanaPerfeccionamiento(cl) {
    informacionNegocioPerf(cl);
    $("#dialogMsConceptoPer").dialog({
        width: '503',
        height: '450',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'CONCEPTO DE DECISION',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Formalizar": function () {
                perfeccionamientoCompraCartera();
            },
            "Salir": function () {
                $(this).dialog("close");
                $("#negocio").val('');
                $("#coment").val('');
            }
        }
    });
}

function informacionNegocioPerf(cl) {
    var grid_tabla = jQuery("#tabla_PerfeCC");
    var cod_neg = grid_tabla.getRowData(cl).cod_neg;
    $("#negocio").val(cod_neg);
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 45,
            cod_neg: cod_neg
        },
        success: function (json) {
            $("#usuario").val(json[0].usuario);
            $("#fecha").val(json[0].fecha);
            $("#comentarios").val(json[0].comentarios);
            $("#decision").val(json[0].concepto);
        }

    });
}

function perfeccionamientoCompraCartera() {
    var concepto = $("#concepto").val();
    var coment = $("#coment").val();
    if (concepto !== '' && coment !== '') {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Maestro&accion=Libranza",
            dataType: 'json',
            async: false,
            data: {
                opcion: 50,
                negocio: $("#negocio").val(),
                coment: $('#coment').val()
            },
            success: function (json) {
                console.log(json.respuesta);
                var resp = json.respuesta;
                if (resp === 'Guardado') {
                    mensajesDelSistema("Formalizacion satisfactoria ", '204', '140', true);
                    $("#dialogMsConceptoPer").dialog("close");
                    cargarPerfeccionarCompraCortera();
                } else {
                    mensajesDelSistema("Error", '204', '140', false);
                }
            }
        });
    } else {
        mensajesDelSistema("Faltan datos", '204', '140', false);
    }
    
}

function cargarDeduccionesMicrocredito() {
    var operacion;
    var grid_tabla_ = jQuery("#tabla_DeduccionesMic");
    if ($("#gview_tabla_DeduccionesMic").length) {
        reloadGridMostrar(grid_tabla_, 51);
    } else {
        grid_tabla_.jqGrid({
            caption: "DEDUCCIONES MICROCREDITO",
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '1135',
            colNames: ['Id', 'Estado', 'Id ocupacion_laboral', 'Ocupacion laboral', 'id_operacion_bancaria', 'Operacion libranza', 'Descripcion', 'Desembolso inicial', 'Desembolso final', 'Valor cobrar', '% Cobrar', 'Nxmil', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'id_ocupacion_laboral', index: 'id_ocupacion_laboral', width: 80, sortable: true, align: 'center', hidden: true},
                {name: 'ocupacion_laboral', index: 'ocupacion_laboral', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'id_operacion_libranza', index: 'id_operacion_bancaria', width: 80, sortable: true, align: 'center', hidden: true},
                {name: 'operacion_libranza', index: 'operacion_bancaria', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 200, sortable: true, align: 'left', hidden: false},
                {name: 'desembolso_inicial', index: 'desembolso_inicial', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'desembolso_final', index: 'desembolso_final', sortable: true, width: 80, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_cobrar', index: 'valor_cobrar', sortable: true, width: 80, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'perc_cobrar', index: 'perc_cobrar', sortable: true, width: 80, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'n_xmil', index: 'n_xmil', sortable: true, width: 80, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                {name: 'cambio', index: 'cambio', width: 120, sortable: true, align: 'left', hidden: false}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 51
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_DeduccionesMic").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_DeduccionesMic").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoDeduccionesMicreocredito('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_DeduccionesMic").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_DeduccionesMic"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var id_operacion_bancaria = filas.id_operacion_libranza;
                var descripcion = filas.descripcion;
                var desembolso_inicial = filas.desembolso_inicial;
                var desembolso_final = filas.desembolso_final;
                var valor_cobrar = filas.valor_cobrar;
                var perc_cobrar = filas.perc_cobrar;
                var n_xmil = filas.n_xmil;
                var cuenta_cabecera = filas.cuenta_cabecera;
                var cuenta_detalle = filas.cuenta_detalle;
                var id_ocupacion_laboral = filas.id_ocupacion_laboral;
                if (estado === 'Activo') {
                    ventanaDeduccionesMicrocredito(operacion, id, id_operacion_bancaria, descripcion, desembolso_inicial, desembolso_final, valor_cobrar, perc_cobrar, n_xmil, id_ocupacion_laboral);
                } else if (estado === 'Inactivo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_DeduccionesMic").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaDeduccionesMicrocredito(operacion);
            }
        });
    }
}

function ventanaDeduccionesMicrocredito(operacion, id, id_operacion_bancaria, descripcion, desembolso_inicial, desembolso_final, valor_cobrar, perc_cobrar, n_xmil, id_ocupacion_laboral) {
    if (operacion === 'Nuevo') {
        cargarComboOperacionesBancarias(operacion);
        cargarComboOcupacionLabMic();
        $("#dialogDeduccionesMic").dialog({
            width: '315',
            height: '320',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'OPERACIONES BANCARIAS LIBRANZAS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarDeduccionesMicrocredito();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#operacion_bancaria").val('');
                    $("#descripcion").val('');
                    $("#desembolso_inicial").val('');
                    $("#desembolso_final").val('');
                    $("#valor_cobrar").val('');
                    $("#perc_cobrar").val('');
                    $("#n_xmil").val('');
                    $("#cuenta_cabecera").val('');
                    $("#cuenta_detalle").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        cargarComboOperacionesBancarias(operacion);
        $("#operacion_bancaria").val(id_operacion_bancaria);
        cargarComboTipoDoc();
        $("#descripcion").val(descripcion);
        $("#desembolso_inicial").val(numberConComas(desembolso_inicial));
        $("#desembolso_final").val(numberConComas(desembolso_final));
        $("#valor_cobrar").val(numberConComas(valor_cobrar));
        $("#perc_cobrar").val(numberConComas(perc_cobrar));
        $("#n_xmil").val(numberConComas(n_xmil));
        cargarComboOcupacionLabMic();
        $("#ocupacion_laboral").val(id_ocupacion_laboral);
        $("#dialogDeduccionesMic").dialog({
            width: '315',
            height: '320',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'OPERACIONES BANCARIAS LIBRANZAS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarDeduccionesMicrocredito();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#operacion_bancaria").val('');
                    $("#descripcion").val('');
                    $("#desembolso_inicial").val('');
                    $("#desembolso_final").val('');
                    $("#valor_cobrar").val('');
                    $("#perc_cobrar").val('');
                    $("#n_xmil").val('');
                    $("#cuenta_cabecera").val('');
                    $("#cuenta_detalle").val('');
                }
            }
        });
    }
}

function cargarComboOcupacionLabMic() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 52
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#ocupacion_laboral').html('');
                $('#ocupacion_laboral').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#ocupacion_laboral').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function CambiarEstadoDeduccionesMicreocredito(rowid) {
    var grid_tabla = jQuery("#tabla_DeduccionesMic");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 53,
            id: id
        },
        success: function (data) {
            cargarDeduccionesMicrocredito();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function ActualizarDeduccionesMicrocredito() {
    var operacion_bancaria = $("#operacion_bancaria").val();
    var descripcion = $("#descripcion").val();
    var desembolso_inicial = numberSinComas($("#desembolso_inicial").val());
    var desembolso_final = numberSinComas($("#desembolso_final").val());
    var valor_cobrar = numberSinComas($("#valor_cobrar").val());
    var perc_cobrar = numberSinComas($("#perc_cobrar").val());
    var n_xmil = numberSinComas($("#n_xmil").val());
    var ocupacion_laboral = $("#ocupacion_laboral").val();
    if (operacion_bancaria !== '' && descripcion !== '' && desembolso_inicial !== '' && desembolso_final !== '' && desembolso_final !== '' && ocupacion_laboral !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 54,
                id: $("#id").val(),
                operacion_bancaria: $("#operacion_bancaria").val(),
                descripcion: $("#descripcion").val(),
                desembolso_inicial: numberSinComas($("#desembolso_inicial").val()),
                desembolso_final: numberSinComas($("#desembolso_final").val()),
                valor_cobrar: numberSinComas($("#valor_cobrar").val()),
                perc_cobrar: numberSinComas($("#perc_cobrar").val()),
                n_xmil: numberSinComas($("#n_xmil").val()),
                ocupacion_laboral: $("#ocupacion_laboral").val()
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#id").val('');
                    $("#operacion_bancaria").val('');
                    $("#descripcion").val('');
                    $("#desembolso_inicial").val('');
                    $("#desembolso_final").val('');
                    $("#valor_cobrar").val('');
                    $("#perc_cobrar").val('');
                    $("#n_xmil").val('');
                    $("#cuenta_cabecera").val('');
                    $("#cuenta_detalle").val('');
                }
                cargarDeduccionesMicrocredito();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("faltan datos", '230', '150', true);
    }
}

function guardarDeduccionesMicrocredito() {
    var operacion_bancaria = $("#operacion_bancaria").val();
    var descripcion = $("#descripcion").val();
    var desembolso_inicial = numberSinComas($("#desembolso_inicial").val());
    var desembolso_final = numberSinComas($("#desembolso_final").val());
    var valor_cobrar = numberSinComas($("#valor_cobrar").val());
    var perc_cobrar = numberSinComas($("#perc_cobrar").val());
    var n_xmil = numberSinComas($("#n_xmil").val());
    var ocupacion_laboral = $("#ocupacion_laboral").val();

    if (operacion_bancaria !== '' && descripcion !== '' && desembolso_inicial !== '' && desembolso_final !== '' && desembolso_final !== '' && ocupacion_laboral !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 55,
                operacion_bancaria: $("#operacion_bancaria").val(),
                descripcion: $("#descripcion").val(),
                desembolso_inicial: numberSinComas($("#desembolso_inicial").val()),
                desembolso_final: numberSinComas($("#desembolso_final").val()),
                valor_cobrar: numberSinComas($("#valor_cobrar").val()),
                perc_cobrar: numberSinComas($("#perc_cobrar").val()),
                n_xmil: numberSinComas($("#n_xmil").val()),
                ocupacion_laboral: $("#ocupacion_laboral").val()
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                    $("#id").val('');
                    $("#operacion_bancaria").val('');
                    $("#descripcion").val('');
                    $("#desembolso_inicial").val('');
                    $("#desembolso_final").val('');
                    $("#valor_cobrar").val('');
                    $("#perc_cobrar").val('');
                    $("#n_xmil").val('');
                    $("#cuenta_cabecera").val('');
                    $("#cuenta_detalle").val('');
                }
                cargarDeduccionesMicrocredito();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("faltan datos", '230', '150', true);
    }
}

function cargarEntidadesCompraCartera() {
    var grid_tabla_ = jQuery("#tabla_entidadesCC");
    if ($("#gview_tabla_entidadesCC").length) {
        reloadGridMostrar(grid_tabla_, 56);
    } else {
        grid_tabla_.jqGrid({
            caption: "Entidades compra cartera",
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '805',
            colNames: ['Id', 'Estado', 'Nit', 'Nombre entidad', 'Tipo cuenta', 'Cuenta', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'nit', index: 'nit', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'payment_name', index: 'payment_name', width: 170, sortable: true, align: 'left', hidden: false},
                {name: 'tipo_cuenta_prov', index: 'tipo_cuenta_prov', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'cuenta_prov', index: 'cuenta_prov', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'cambio', index: 'cambio', width: 120, sortable: true, align: 'left', hidden: false}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 56
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_entidadesCC").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_entidadesCC").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoEntidadesCompraCartera('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_entidadesCC").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_entidadesCC").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                ventanaEntidades();
            }
        });
    }
}

function ventanaEntidades() {
    autocompletar();
    $("#dialogEntidades").dialog({
        width: '757',
        height: '200',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Entidades compra cartera',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Asociar": function () {
                guardarEntidadesCompraCartera();
            },
            "Salir": function () {
                $('#entidad').val('');
                $('#nit').val('');
                $('#digitov').val('');
                $(this).dialog("close");
            }
        }
    });
}


function autocompletar() {
    $("#entidad").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Maestro&accion=Libranza",
                dataType: "json",
                data: {
                    nombre: request.term,
                    opcion: 57
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.value,
                            mivar1: item.mivar1,
                            mivar2: item.mivar2
                        };
                    }));
                }
            });
        },
        minLength: 3,
        delay: 500,
        disabled: false,
        select: function (event, ui) {
            $("#nit").val(ui.item.mivar1);
            $("#digitov").val(ui.item.mivar2);

            console.log(ui.item ?
                    "Selected: " + ui.item.mivar1 :
                    "Nothing selected, input was " + ui.item.label);
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function CambiarEstadoEntidadesCompraCartera(rowid) {
    var grid_tabla = jQuery("#tabla_entidadesCC");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 58,
            id: id
        },
        success: function (data) {
            cargarEntidadesCompraCartera();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function guardarEntidadesCompraCartera() {
    var nombre = $("#entidad").val();
    var nit = $("#nit").val();
    var digver = numberSinComas($("#digitov").val());
    if (nombre !== '' && nit !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 59,
                nombre: nombre,
                nit: nit,
                digver: digver
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    $('#entidad').val('');
                    $('#nit').val('');
                    $('#digitov').val('');
                    $("#dialogEntidades").dialog("close");
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                }
                cargarEntidadesCompraCartera();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("faltan datos", '230', '150', true);
    }
}


var valor_comprarT;
var valor_modificadoT;
function CambioValores(rowid) {
    var grid_tabla = jQuery("#tabla_FormalizarLib");
    var numero_solicitud = grid_tabla.getRowData(rowid).numero_solicitud;
    cargarObligacionesCompra(numero_solicitud);
    $("#dialogMsFormalizarLib").dialog({
        width: '807',
        height: '430',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        //title: 'MODIFICAR OBLIGACIONES COMPRA',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Modificar": function () {
                guaradarcambiosObligacionesCompra(valor_comprarT, valor_modificadoT,numero_solicitud);
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarObligacionesCompra(numero_solicitud) {
    var grid_tabla = jQuery("#tabla_Valores");
    if ($("#gview_tabla_Valores").length) {
        reloadGridMostrarTablaOblig(grid_tabla, 60, numero_solicitud);
    } else {
        grid_tabla.jqGrid({
            caption: "Obligaciones compra cartera",
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",
            async: false,
            datatype: "json",
            height: '230',
            width: '780',
            colNames: ['Numero solicitud', 'Secuencia', 'Nit entidad', 'Entidad1','Entidad', 'Ttipo cuenta', 'Numero cuenta', 'Valor comprar', 'valor_modificado2', 'Valor modificado'],
            colModel: [
                {name: 'numero_solicitud', index: 'numero_solicitud', width: 100, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'secuencia', index: 'secuencia', width: 90, sortable: true, align: 'center', hidden: true, search: true ,key:true},
                {name: 'nit_entidad', index: 'nit_entidad', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'entidad', index: 'entidad', width: 90, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'nombre', index: 'nombre', width: 130, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'tipo_cuenta', index: 'tipo_cuenta', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'numero_cuenta', index: 'numero_cuenta', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'valor_comprar', index: 'valor_comprar', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_modificado2', index: 'valor_modificado2', sortable: true, width: 110, align: 'right', search: true, hidden: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_modificado', index: 'valor_modificado', sortable: true, width: 110, align: 'right', search: true, editable: true,
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "},
                    editoptions: {
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowid = e.target.id.replace("_valor_modificado", "");
                                        var val = e.target.value.replace("_", "");
                                        grid_tabla.jqGrid('setCell', rowid, "valor_modificado2", val);
                                        var total = grid_tabla.jqGrid('getCol', 'valor_modificado2', false, 'sum');
                                        jQuery("#tabla_Valores").jqGrid('footerData', 'set', {valor_modificado: total});
                                        valor_modificadoT = total;
                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            cellEdit: false,
            userDataOnFooter: true,
            reloadAfterSubmit: true,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            ignoreCase: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                grid_tabla.jqGrid('editRow', rowid, true, function () {
                    $("input, select", e.target).focus();
                });
                return;
            }, gridComplete: function () {
                var valor_comprar = grid_tabla.jqGrid('getCol', 'valor_comprar', false, 'sum');
                var valor_modificado = grid_tabla.jqGrid('getCol', 'valor_modificado', false, 'sum');
                grid_tabla.jqGrid('footerData', 'set', {valor_comprar: valor_comprar, valor_modificado: valor_modificado});
                valor_modificadoT = valor_modificado;
                valor_comprarT = valor_comprar;
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    //mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 60,
                    numero_solicitud: numero_solicitud
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, restoreAfterError: true
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridMostrarTablaOblig(grid_tabla, opcion, numero_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                numero_solicitud: numero_solicitud
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function guaradarcambiosObligacionesCompra(valor_comprarT, valor_modificadoT,numero_solicitud,rowid) {
    var operacion;
    var diferencia;
    if (valor_comprarT < valor_modificadoT) {
        operacion = 'MAYOR';
        diferencia = valor_modificadoT - valor_comprarT;
    } else if (valor_comprarT > valor_modificadoT) {
        operacion = 'MENOR';
        diferencia = valor_comprarT - valor_modificadoT;
    } else if (valor_comprarT === valor_modificadoT) {
        operacion = 'IGUAL';
    }
    //alert('valor_comprarT: ' + valor_comprarT + ' valor_modificadoT: ' + valor_modificadoT + ' numero_solicitud: ' + numero_solicitud +'diferencia: '+ diferencia);

    if (operacion !== 'IGUAL') {
        if (valor_modificadoT !== 0) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "./controller?estado=Maestro&accion=Libranza",
                data: {
                    opcion: 61,
                    operacion: operacion,
                    numero_solicitud: numero_solicitud,
                    diferencia:diferencia
                },
                success: function (data) {
                    //alert(data.respuesta);
                    if (data.respuesta === 'OK') {
                        actualizarObligacionesCompra(numero_solicitud);
                    } else if (data.respuesta === 'RECALCULAR') {
                        var grid_tabla = jQuery("#tabla_Valores");
                        var ids = grid_tabla.jqGrid('getDataIDs');
                        var allRowsInGrid = grid_tabla.jqGrid('getRowData');
                        for (var i = 0; i < allRowsInGrid.length; i++) {
                            var cl = ids[i];
                            var valor_comprar = grid_tabla.getRowData(cl).valor_comprar;
                            jQuery("#tabla_Valores").jqGrid('setCell', cl, "valor_modificado", valor_comprar);
                            jQuery("#tabla_Valores").jqGrid('setCell', cl, "valor_modificado2", valor_comprar);
                        }
                        var total = jQuery("#tabla_Valores").jqGrid('getCol', 'valor_modificado2', false, 'sum');
                        jQuery("#tabla_Valores").jqGrid('footerData', 'set', {valor_modificado: total});
                         mensajesDelSistema("El valor no cumple con la politica", '230', '150', true);
                    } else {
                        mensajesDelSistema("Vaya ha ocurrido un error", '230', '150', true);
                    }

                }, error: function (result) {
                    alert('error');
                }
            });
        } else {
            mensajesDelSistema("No ha modificado ningun valor ", '230', '150', false);
        }

    } else {
        mensajesDelSistema("Los valores son iguales", '230', '150', false);
    }
}

function actualizarObligacionesCompra() {
    var fullData = jQuery("#tabla_Valores").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    // alert(myJsonString);
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 63,
            info: myJsonString
        },
        success: function (data) {
            if (data.respuesta === 'Guardado') {
                $("#dialogMsFormalizarLib").dialog("close");
                mensajesDelSistema("Exito en la modificacion", '230', '150', true);
                cargarFormalizarLibranza();
            } else {
                mensajesDelSistema("Vaya ha ocurrido un error", '230', '150', true);
            }

        }, error: function (result) {
            alert('error');
        }
    });
}
function cargarPazySalvo() {
        
//      var extension = $('#archivo').val().split('.').pop().toLowerCase();
//      var filetype = document.getElementById('archivo').files[0].type;
        var fd = new FormData(document.getElementById('formulario'));
        var archivo = document.getElementById('archivo').value;
        //extensiones_permitidas = new Array(".gif", ".jpg", ".doc", ".pdf"); 
        if (archivo === "") {
            mensajesDelSistema("No ha seleccionado ning�n archivo. Por favor, seleccione uno!!", '250', '150');
            return;
        }
            
        var fsize=$('#archivo')[0].files[0].size;
        if (fsize > 4096 * 1024){
            $("#archivo").val("");
            mensajesDelSistema("Archivo no cargado. El tama�o del archivo no puede ser superior a los 4MB", '250', '175');
            return;
        }
        
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function(){
         
        /* //recupero la extensi�n de este nombre de archivo 
            extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase(); 
            alert (extension); 
            //compruebo si la extensi�n est� entre las permitidas 
            permitida = false; 
            for (var i = 0; i < extensiones_permitidas.length; i++) { 
                if (extensiones_permitidas[i] == extension) { 
                    permitida = true; 
                    break; 
                } 
            }
            if (!permitida) { 
                mensajesDelSistema("Comprueba la extensi�n de los archivos a subir. \nS�lo se pueden subir archivos con extensiones: ") + extensiones_permitidas;//.join();
                $("#cargararchivo").dialog('close');
                }else{  */  
            
        $.ajax({
            async: false,
            url: "./controller?estado=Maestro&accion=Libranza&opcion=65",
            type: 'POST',
            dataType: 'json',
            data: fd,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(json) {              
                $("#archivo").val("");
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema(json.error, '250', '175');
                        return;
                    }
                    console.log(subgrid_table_id);    
                    if (json.respuesta === "OK") {                       
                        $("#dialogLoading").dialog('close');
                        //obtenerArchivosCargados();
                        mensajesDelSistema("Archivo cargado exitosamente", '250', '150', true);
                        $("#cargararchivo").dialog('close');
                        reloadSubGridMostrarTabla(subgrid_table_id, 64);
                        
                        return;
                    }
                } else { 
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci�n!!", '250', '150');
                }
            }, error: function(xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
        
            //} 
    },500);
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function ventanaVerArchivo(rowid) {
    $("#documento").val(rowid.id);
    obtenerArchivoCargado();
    $("#verarchivo").dialog({
        width: 300,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: true, 
        buttons: {          
            "Salir": function () {          
		$(this).dialog("close");               
            }
        }
    });
}

function obtenerArchivoCargado() {
        
        var documento =  $('#documento').val();
        $.ajax({
            type: "POST",
            dataType: "json",
            data: {opcion: 67,
                   documento: documento},
            async: false,
            url: "./controller?estado=Maestro&accion=Libranza",
            success: function (jsonData) {
                if (!isEmptyJSON(jsonData)) {  
                    $('#tbl_archivos_cargados').html('');
                    for (var i = 0; i < jsonData.length; i++) {
                       var nomarchivo = jsonData[i]; 
                       $('#tbl_archivos_cargados').append("<tr class='fila'><td><a target='_blank' onClick=\"consultarNomarchivo('"+documento+"','"+nomarchivo+"');\n\
                       \" style='cursor:hand' ><strong>"+nomarchivo+"</strong></a> &nbsp;&nbsp;&nbsp;<a id='view_file' target='_blank' href='#' style='display:none'>Ver</a></td></tr>");                
                    }
                           
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }


function consultarNomarchivo(documento, nomarchivo) {
     $.ajax({
            type: "POST",
            dataType: "json",
            data: {opcion:68,
                documento: documento,
                nomarchivo: nomarchivo},
            async: false,
            url: "./controller?estado=Maestro&accion=Libranza",
            success: function (jsonData) {
                if (!isEmptyJSON(jsonData)) {

                    if (jsonData.error) {
                        mensajesDelSistema(jsonData.error, '270', '165');
                        return;
                    }
                    if (jsonData.respuesta === "SI") {
                        $('#view_file').attr("href", "/fintra/images/multiservicios/"+jsonData.login+"/"+nomarchivo);
                        $('#view_file').fadeIn();                      
                    } else {
                        mensajesDelSistema(".::ERROR AL OBTENER ARCHIVO::.", '250', '150');
                    }

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
   
