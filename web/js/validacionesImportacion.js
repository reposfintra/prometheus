  // FUNCIONES GENERALES DE IMPORTACION
  

  var BASEURL    = '';
  var CONTROLLER = '';


  function nuevoCampo(pk, cmp, tip, isdef, tipodef, valdef, valias, validacion, insercion, update, obs_v, obs_i){
    var tabla = document.getElementById('ListaCampos');
	if (tabla){
		

           if (tip.indexOf('::')!=-1 ){ tip = tip.substr (0, tip.indexOf('::')) ; }
           var checkbox = getElement ('CHK'          , 'INPUT'   , ''     , '', '');
		   var inputC1  = getElement ('LOVCMP'       , 'INPUT'   , cmp    , 'textbox', '');
		   var inputC2  = getElement ('LOVEST'       , 'INPUT'   , tip    , 'textbox', '');
		   var inputC3  = getElement ('LOVVAL'       , 'INPUT'   , valdef , 'textbox', '');
   		   var inputC4  = getElement ('LOVALIAS'     , 'INPUT'   , valias , 'textbox', '');
   		   var inputC5  = getElement ('LOVVALIDACION', 'INPUT'   , validacion , 'textbox', '0');
		   var inputC6  = getElement ('LOVINSERCION' , 'INPUT'   , insercion  , 'textbox', '0');
		   
		   var inputC7  = getElement ('LOV_OBSVALIDACION', 'INPUT'   , obs_v , 'textbox', '0');
		   var inputC8  = getElement ('LOV_OBSINSERCION' , 'INPUT'   , obs_i , 'textbox', '0');
		   

           var select1  = getElement ('LOVDEF', 'SELECT'  , ''  , 'select' , '');
               addOption (select1, 'S', 'SI');
               addOption (select1, 'N', 'NO');
               select1.value = isdef;
               select1.onchange = new Function ("validarCmb1(this);");

           var select2  = getElement ('LOVTDF', 'SELECT'  , ''  , 'select'  , '');
               addOption (select2, '_DSTRCT_SESSION_'  , 'Distrito en session' );
               addOption (select2, '_USER_SESSION_'    , 'Usuario en session'  );
               addOption (select2, '_USER_AGENCY_'     , 'Agencia usuario'     );
               addOption (select2, '_TODAY_'           , 'Fecha de Hoy'        );
               addOption (select2, '_CONST_'           , 'Valor Constante'     );
               addOption (select2, '_BASE_'            , 'Base usuario'        );
               addOption (select2, ''                  , 'Ninguno'             );
               select2.value = tipodef;
               select2.onchange = new Function ("validarCmb2(this);"); 
               
           var select3  = getElement ('LOVUPDATE', 'SELECT'  , ''  , 'select' , '');
               addOption (select3, 'S', 'SI');
               addOption (select3, 'N', 'NO');
               select3.value = (update=='true'?'S':'N');
			   select3.onchange = new Function ("validarCmb3(this);"); 
			   validarCmb3 ( select3 );
			
			   
			inputC1.style.width='100%'; inputC1.readOnly=true;
			inputC2.style.width='250';  inputC2.readOnly=true; 
			inputC3.style.width='100%'; inputC4.maxLength=100;
			inputC4.style.width='100%';	inputC4.maxLength=40;
			select1.style.width='100%';
			select2.style.width='100%';
            if (tipodef!='_CONST_')
                inputC3.readOnly = true;

	   var tr = tabla.insertRow(); 
	   tr.className = (tr.rowIndex%2==0 ? 'filagris' : 'filaazul');
	   var primaryKey = getElement('', 'B' , '','','');
	   if (pk == 'true') {
		   primaryKey.innerHTML = "<img src='"+ BASEURL +"/images/botones/iconos/chulo.gif' width='10' style='cursor:hand;' title='Llave primaria' >  ";	   
	   }
	   td = tr.insertCell();  td.className='bordereporte';  td.align='center'; td.appendChild ( primaryKey  ); 
	   td = tr.insertCell();  td.className='bordereporte';  td.appendChild ( inputC1     );
	   td = tr.insertCell();  td.className='bordereporte';  td.appendChild ( inputC2     );
	   td = tr.insertCell();  td.className='bordereporte';  td.align='center'; td.appendChild ( select1 );
	   td = tr.insertCell();  td.className='bordereporte';  td.align='center'; td.appendChild ( select2 );
	   td = tr.insertCell();  td.className='bordereporte';  td.appendChild ( inputC3 );
  	   td = tr.insertCell();  td.className='bordereporte';  td.appendChild ( inputC4 );
       
	   
	   
	   // validacion
   	   td = tr.insertCell();  td.className='bordereporte';  td.appendChild ( inputC5 ); td.appendChild ( inputC7 );
	   var div1 =  getElement('SQL', 'B' , '','','');
	   div1.innerHTML = "<img src='"+ BASEURL +"/images/botones/iconos/clasificar.gif' width='20' style='cursor:hand;' title='Editar Validacion' onclick=\" goValidacion(this.parentNode.parentNode.parentNode, 7); \" >  "
	   			      + "<b class='informacion' style='color:green' id='val"+ tr.rowIndex +"'>"+ (validacion==''?'':'&nbsp;SQL') +"</b>";
	   td.appendChild ( div1 );

	   // insercion
   	   td = tr.insertCell();  td.className='bordereporte';  td.appendChild ( inputC6 ); td.appendChild ( inputC8 );
	   var div2 =  getElement('SQL', 'B' , '','','');
	   div2.innerHTML = "<img src='"+ BASEURL +"/images/botones/iconos/clasificar.gif' width='20' style='cursor:hand;' title='Editar Insercion' onclick=\" goValidacion(this.parentNode.parentNode.parentNode, 8); \" >  "
					  + " <b class='informacion' style='color:green' id='ins"+ tr.rowIndex +"'>"+ (insercion==''?'':'&nbsp;SQL') +"</b>";
	   td.appendChild ( div2 );
	   td = tr.insertCell();  td.className='bordereporte';  td.appendChild ( select3 );
	   	   	   
       _cambiar (tr, isdef);
	 }
  }
  
  function getElement (name, tipo, value, className, width){
	var element       = document.createElement(tipo);
	element.id        = name;
	element.name      = name;
	element.value      = value;

	if (width    !='') element.width     = width;
	if (className!='') element.className = className;	
	return element;
  }
  
  function deleteRow (){
    var chk = document.getElementsByName('CHK'); 
	if ( chk ){
	    var tabla = document.getElementById('ListaCampos');
    	for (i= chk.length -1; i >= 0 ; i-- )
		   if ( chk[i].checked == true ) tabla.deleteRow (i+1);
	}
  }
  
  function _change(boton){
    var tope   = boton.parentNode.parentNode.parentNode.rows.length;
    var index  = boton.parentNode.parentNode.rowIndex;
	var accion = boton.name;
	
	
	var origen  = index;
	var destino = (accion=='Up' ? origen - 1 : origen + 1 );

	
	if (destino < 1 || destino >= tope)	return ;

  	var tabla = document.getElementById('ListaCampos');
	if (tabla){
	  var trMn = tabla.rows[origen ];
	  var trMy = tabla.rows[destino];
	  for (i=1;i<trMn.cells.length-1; i++){
	    var obj1 = trMn.cells[i].firstChild;
		var obj2 = trMy.cells[i].firstChild;
            var	tmp = obj1.value;
		obj1.value = obj2.value;
		obj2.value = tmp;
	  }
	}
  }
  
  
  function crearBoton(imagen, id){
	var img = document.createElement('IMG');
	img.src    = imagen;
	img.width  = 14;
	img.height = 14;
	return img;
  }


   function enviar(tform){
      if (validarGeneral())
        tform.submit();
   }

   function goEdicion (formato, Opcion) {
      var url = CONTROLLER + '?estado=Opciones&accion=Importacion&Opcion=' + Opcion + '&formato='+ formato;
      var winEdicion = open(url ,'Edicion','status=yes,scrollbars=no,width=900,height=650,resizable=yes, top=10, left=30');
      winEdicion.focus();
   }

   function goEdicion2 (formato, Opcion) {
      var url = CONTROLLER + '?estado=Opciones&accion=Importacion&Opcion='+ Opcion +'&formato='+ formato;
      window.document.location.href = url;
   }


   function goValidacion (fila, col) {
	  var campo       = fila.cells[6].firstChild.value;
	  var elementos   = fila.cells[col].childNodes;	  
	  var validacion  = elementos[0].value;
	  var observacion = elementos[1].value;
      var url = BASEURL + '/jsp/general/importacion/validacionSQL.jsp?campo=' + campo + '&validacion='+ validacion + "&fila=" + fila.rowIndex + '&col=' + col + "&obs=" + observacion;
      var winVal = open(url ,'Validacion','status=yes,scrollbars=no,width=600,height=500,resizable=yes, top='+ ((screen.height - 500) / 2) +', left=' + ((screen.width - 600) / 2));
      winVal.focus();
   }
   
   function asignar(valor, obs, fila, col){
	   
	   	var tab = window.opener.document.getElementById('ListaCampos');
		var row = tab.rows[fila];
	    var elementos   = row.cells[col].childNodes;	  
  	    elementos[0].value = valor.replace (/\n/gi, ' ' );
	    elementos[1].value = obs.replace (/\n/gi, ' ' );
		if (col==7){
			var v = window.opener.document.getElementById('val' + fila );
			v.innerHTML = (valor==""?"":"&nbsp; SQL");
		} else if (col==8){
			var v = window.opener.document.getElementById('ins' + fila );
			v.innerHTML = (valor==""?"":"&nbsp; SQL");
		}
		window.close();		
   }


    function addOption(Comb,valor,texto){
            var Ele = document.createElement("OPTION");
            Ele.value=valor;
            Ele.text=texto;
            Comb.add(Ele);
    }
	

    function validarCmb1 ( def ){
       var tr = def.parentNode.parentNode;
       var tipo = tr.cells[4].firstChild;
       var valor = tr.cells[5].firstChild;
       if (def.value=='N'){
           tipo.value = '';
           valor.value = '';
           valor.readOnly = true;
       }
       else if (tipo.value == '_CONST_')
           valor.readOnly = false;
       _cambiar (tr, def.value);

    }

    function validarCmb2 ( tdef ){
       var tr = tdef.parentNode.parentNode;
       var def = tr.cells[3].firstChild;
       var valor = tr.cells[5].firstChild;
       if (def.value=='N'){
           tdef.value = '';
           alert ('Para poder cambiar esta propiedad primero debe indica que este campo si tiene default');
       }
       else if (tdef.value == '_CONST_')
           valor.readOnly = false;
       else{
           valor.value = '';
           valor.readOnly = true;
       }
          
    }


    function validarCmb3 ( tdef ){
		   tdef.style.color = (tdef.value=='S'?'green':'orange');
    }
	
	
    function validarGeneral (){
         var tabla = document.getElementById('ListaCampos');       
         for (var i = 2; i< tabla.rows.length; i++){
            var tr   = tabla.rows[i];
            var def  = tr.cells[2].firstChild;
            var tdef = tr.cells[3].firstChild;
            if (def.value=='S' && tdef.value==''){
                 alert ('Esta indicando que el campo numero ('+ (i-1) +') tiene default, pero no le especifica \nel tipo de default, definalo para poder continuar');
                 return false;
            }
         }
         return validarAlias();
    }

    function encontrarTabla (){
       var url = CONTROLLER + '?estado=Opciones&accion=Importacion&Opcion=ListarTablas';
       var win = open (url,'ListaTablas',' top=100,left=100, width=700, height=500, scrollbar=no, status=yes  ');
       win.focus();
    } 

    function retornarTabla (valor) {
       var doc = window.opener.document.getElementById ('ntabla');
       doc.value = valor.replace(/public./,'');
       window.close();
       
    }


    function filtrarCombo(data, cmb){
       data = data.toLowerCase();
       for (i=0;i<cmb.length;i++){
          var cod = cmb[i].value;
          var txt = cmb[i].text.toLowerCase( );
          if( txt.indexOf(data)==0 ){
             cmb[i].selected = true;
             break;
          }
       }
    } 

    function _cambiar (tr, valor){
       for (var i=0; i<=6; i++){
         var campo = tr.cells[i].firstChild;
         campo.style.color = (valor=='S'?'red':'#003399');
       }
    }

    function validarAlias (){
      var alias = document.getElementsByName('LOVALIAS');
      if (alias!=null) {
          for (i = 0; i<alias.length; i++){
            for (j=i+1; j<alias.length; j++){

                if (alias[i].value.toUpperCase() ==  alias[j].value.toUpperCase()){
                    alert('No se pueden repetir los alias por favor cambie los repetidos');
                    alias[j].focus();
                    alias[j].select();
                    return false;
                }
            }
          }
      }
      return true;
    }
	
