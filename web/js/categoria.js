/*
 * Created by: Ing. Jos� Castro
 * and open the template in the editor.
 */


// JavaScript Document
var estado=4;

/**
* Metodo que retorna el objecto ajax
* @autor: Jcastro
* @Solicitud: creaci�n de categorias
* @fecha: 15-07-2010
*/
function objetoAjax(){
    var xmlhttp = null;
    try{
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e){
        try{
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (ex){
            xmlhttp = null;
        }
    }

    if (!xmlhttp && typeof XMLHttpRequest != 'undefined'){
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}


/**
    * Metodo que retorna el cuerpo del mensaje mientras esta cargando una accion
    * @parameter: msg, BASEURL
    * @autor: Jcastro
    * @Solicitud:  Plantilla Cotizaciones
    * @fecha: 16-06-2010
*/
function loading(msg, BASEURL){
    var html = "";
    html += "<table border=\"1\" align=\"center\">";
    html += "<tr>";
    html += "<td>";
    html += "<table width=\"400\" height=\"50\"  align=\"center\"  bordercolor=\"#F7F5F4\" bgcolor=\"#FFFFFF\">";
    html += "<tr>";
    html += "<td align=\"center\"><img src=\""+BASEURL+"/images/cargando.gif\" border=\"0\" align=\"absmiddle\"></td>";
    html += "<td class=\"mensajes\" align=\"center\">"+msg+"</td>";
    html += "</tr>";
    html += "</table>";
    html += "</td>";
    html += "</tr>";
    html += "</table>";
    return(html);
}

/**
	* Metodo que retorna el cuerpo del mensaje
	* @parameter: msg, BASEURL
	 * @autor: Jcastro
	* @Solicitud: Plantilla Cotizaciones
	* @fecha: 16-06-2010
	*/
function message(msg, BASEURL){
    var html = "";
    html += "<table border=\"2\" align=\"center\">";
    html += "<tr>";
    html += "<td>";
    html += "<table width=\"400\" height=\"73\" border=\"1\" align=\"center\"  bordercolor=\"#F7F5F4\" bgcolor=\"#FFFFFF\">";
    html += "<tr>";
    html += "<td width=\"360\" align=\"center\" class=\"mensajes\">"+msg+"</td>";
    html += "<td width=\"40\" background=\""+BASEURL+"/images/cuadronaranja.JPG\">&nbsp;</td>";
    html += "</tr>";
    html += "</table>";
    html += "</td>";
    html += "</tr>";
    html += "</table>";
    return(html);
}




function obtenerCategorias(CONTROLLER, BASEURL){

    if (estado == 4){
        var ajax = objetoAjax();
        ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 1){
                document.getElementById("capaCentral").innerHTML = loading("Cargando informacion...", BASEURL);
                estado = 1;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    estado = 4;
                    document.getElementById("capaCentral").innerHTML = ajax.responseText;

                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send("opcion=0");
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}


function enviarNuevo(CONTROLLER, BASEURL){

        if (estado == 4){


        var ajax = objetoAjax();
        ajax.open("POST", CONTROLLER+"?estado=Materiales&accion=Crear", true);
        ajax.onreadystatechange = function(){

            if (ajax.readyState == 1){

                document.getElementById("capaSuperior").innerHTML = loading("Cargando informacion...", BASEURL);
                estado = 1;
            }
            else if (ajax.readyState == 4){

                if( ajax.status == 200){
                   estado = 4;
                   alert("Informaci�n Ingresada con Exito...");
                   location.href = BASEURL+ajax.responseText;

                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send("opcion=3");
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }

}






function obtenerCategoriasAcciones(CONTROLLER, BASEURL, est, idcat){

    if (estado == 4){
        var ajax = objetoAjax();
        ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 1){
                document.getElementById("capaCentral").innerHTML = loading("Cargando informacion...", BASEURL);
                estado = 1;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    estado = 4;
                    document.getElementById("capaCentral").innerHTML = ajax.responseText;

                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send("opcion=1&est="+est+"&idcat="+idcat);
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}





function obtenerSubcategorias(CONTROLLER, BASEURL, index, idcat){



    var imagen = document.getElementById("ima"+index);
    var celda = document.getElementById("subcategorias"+index);
    var celda = document.getElementById("subcategorias"+index);
    var est = document.getElementById("status"+index).value;
    var visible = ((celda.style.display == "none")?false:true);
    if (estado == 4){
        if (visible){
            imagen.src = BASEURL+"/images/botones/iconos/expand.gif";
            celda.style.display="none";
        } else {
            if(celda.innerHTML == ""){
                var ajax = objetoAjax();
                ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
                ajax.onreadystatechange = function(){
                    if (ajax.readyState == 1){
                        celda.style.display = "block";
                        celda.innerHTML = loading("Cargando informacion...", BASEURL);
                        estado = 1;
                    }
                    else if (ajax.readyState == 4){
                        if( ajax.status == 200){
                            estado = 4;
                            celda.style.display = "block";
                            imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
                            celda.innerHTML = ajax.responseText;

                        }
                    }
                }
                ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                ajax.send("opcion=2&idcat="+idcat+"&index="+index+"&est="+est);
            }else{
                celda.style.display = "block";
                imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
            }
        }
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}




function obtenertipoSubcategorias(CONTROLLER, BASEURL, index, ind,  idcat){

    var id = index+"_"+ind;
    var imagen = document.getElementById("ima"+id);
    var celda = document.getElementById("tiposubcategorias"+id);
    var est = document.getElementById("status"+id).value;



    var visible = ((celda.style.display == "none")?false:true);
    if (estado == 4){
        if (visible){
            imagen.src = BASEURL+"/images/botones/iconos/expand.gif";
            celda.style.display="none";
        } else {
            if(celda.innerHTML == ""){
                var ajax = objetoAjax();
                ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
                ajax.onreadystatechange = function(){
                    if (ajax.readyState == 1){
                        celda.style.display = "block";
                        celda.innerHTML = loading("Cargando informacion...", BASEURL);
                        estado = 1;
                    }
                    else if (ajax.readyState == 4){
                        if( ajax.status == 200){
                            estado = 4;
                            celda.style.display = "block";
                            imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
                            celda.innerHTML = ajax.responseText;

                        }
                    }
                }
                ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                ajax.send("opcion=3&idsubcat="+idcat+"&index="+index+"&ind="+ind+"&est="+est);
            }else{
                celda.style.display = "block";
                imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
            }
        }
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}



function agregarSubcategoria(CONTROLLER, BASEURL, index,  idcat){
    var id = index;
    var celda = document.getElementById("subcategorias"+id);
    var est = document.getElementById("status"+index).value;

    var visible = ((celda.style.display == "none")?false:true);

    if (estado == 4){
        if (visible){
            if(celda.innerHTML != ""){
                var ajax = objetoAjax();
                ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
                ajax.onreadystatechange = function(){
                    if (ajax.readyState == 1){
                        celda.style.display = "block";
                        celda.innerHTML = loading("Cargando informacion...", BASEURL);
                        estado = 1;
                    }
                    else if (ajax.readyState == 4){
                        if( ajax.status == 200){
                            estado = 4;
                            celda.style.display = "block";
                            celda.innerHTML = ajax.responseText;
                        }
                    }
                }
                ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                ajax.send("opcion=6&idsubcat="+idcat+"&index="+index+"&est="+est);
            }else{

                celda.style.display = "block";
                imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
            }
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}
}



function eliminarSubcategoria(CONTROLLER, BASEURL, index,  idcat, borrar){



    var id = index;
    var celda = document.getElementById("subcategorias"+id);
    var est = document.getElementById("status"+index).value;
    var visible = ((celda.style.display == "none")?false:true);
    if (estado == 4){
        if (visible){
            if(celda.innerHTML != ""){
                var ajax = objetoAjax();
                ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
                ajax.onreadystatechange = function(){
                    if (ajax.readyState == 1){
                        celda.style.display = "block";
                        celda.innerHTML = loading("Cargando informacion...", BASEURL);
                        estado = 1;
                    }
                    else if (ajax.readyState == 4){
                        if( ajax.status == 200){
                            estado = 4;
                            celda.style.display = "block";
                            celda.innerHTML = ajax.responseText;
                        }
                    }
                }
                ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                ajax.send("opcion=7&idsubcat="+idcat+"&index="+index+"&borrar="+borrar+"&est="+est);
            }else{
                //alert("celda con info");
                celda.style.display = "block";
                imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
            }
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}
}





function agregarFila(CONTROLLER, BASEURL, index, ind,  idcat){

    var id = index+"_"+ind;
    var celda = document.getElementById("tiposubcategorias"+id);
    var est = document.getElementById("status"+id).value;
    var visible = ((celda.style.display == "none")?false:true);
    if (estado == 4){
        if (visible){
            if(celda.innerHTML != ""){
                var ajax = objetoAjax();
                ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
                ajax.onreadystatechange = function(){
                    if (ajax.readyState == 1){
                        celda.style.display = "block";
                        celda.innerHTML = loading("Cargando informacion...", BASEURL);
                        estado = 1;
                    }
                    else if (ajax.readyState == 4){
                        if( ajax.status == 200){
                            estado = 4;
                            celda.style.display = "block";
                            celda.innerHTML = ajax.responseText;

                        }
                    }
                }
                ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                ajax.send("opcion=4&idsubcat="+idcat+"&index="+index+"&ind="+ind+"&est="+est);
            }else{

                celda.style.display = "block";
                imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
            }

    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}
}




function eliminarFila(CONTROLLER, BASEURL, index, ind,  idcat, borrar){


    var id = index+"_"+ind;
    var sec = index;
    var celda = document.getElementById("tiposubcategorias"+id);
    var est = document.getElementById("status"+id).value;
    var visible = ((celda.style.display == "none")?false:true);

    if (estado == 4){
        if (visible){
            if(celda.innerHTML != ""){
                var ajax = objetoAjax();
                ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
                ajax.onreadystatechange = function(){
                    if (ajax.readyState == 1){
                        celda.style.display = "block";
                        celda.innerHTML = loading("Cargando informacion...", BASEURL);
                        estado = 1;
                    }
                    else if (ajax.readyState == 4){
                        if( ajax.status == 200){
                            estado = 4;
                            celda.style.display = "block";
                            celda.innerHTML = ajax.responseText;
                        }
                    }
                }
                ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                ajax.send("opcion=5&idsubcat="+idcat+"&index="+index+"&ind="+ind+"&borrar="+borrar+"&est="+est);
            }else{

                celda.style.display = "block";
                imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
            }
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}
}



/**
 * Comment
 */
function activar() {

    var tam_sub = document.getElementById("tam_sub");
    var sel = document.getElementById("status"+0).selectedIndex;
    var tam_tiposub = "";

    for(i=0;i<tam_sub.value;i++){
        document.getElementById("status0_"+i).selectedIndex = sel;

        if(sel==1)document.getElementById("status0_"+i).disabled = true;
        if(sel==0)document.getElementById("status0_"+i).disabled = false;

        if(document.getElementById("tam_tiposub0_"+i)){
            tam_tiposub = document.getElementById("tam_tiposub0_"+i).value;


            for(j=0;j<tam_tiposub;j++){
                document.getElementById("statust"+i+"_"+j).selectedIndex = sel;
                if(sel==1)document.getElementById("statust"+i+"_"+j).disabled = true;
                if(sel==0)document.getElementById("statust"+i+"_"+j).disabled = false;



            }
        }
    }
}

/**
 * Comment
 */
function activarSubcategorias(index, ind) {
    var tam_tiposub = document.getElementById("tam_tiposub"+index+"_"+ind).value;
    var sel = document.getElementById("status"+index+"_"+ind).selectedIndex;//index seleccionado
    for(j=0;j<tam_tiposub;j++){
        document.getElementById("statust"+ind+"_"+j).selectedIndex = sel;
                if(sel==1)document.getElementById("statust"+ind+"_"+j).disabled = true;
                if(sel==0)document.getElementById("statust"+ind+"_"+j).disabled = false;
    }
}







/**
 * Comment
 */
function guardarNuevo(CONTROLLER, BASEURL) {
   // alert("guardarNuevo");
   // alert(document.getElementById("idcat0").value);
   // alert(document.getElementById("status0").value);
    var tam_tiposub = "";
    var cant_c = "1";//size de las categorias en caso que hubieran varias para almacenar
    var cant_s = "";//size de subcategorias a almacenar
    var cant_t = "";//size de tiposubcategorias a almacenar
    var cat = "0";
    var tam_sub = document.getElementById("tam_sub");
    var param = "opcion=9";
    param += "&cant_c="+cant_c;
    param += "&cat="+cat;
    param += "&idcat"+cat+"="+document.getElementById("idcat"+cat).value;
    param += "&status"+cat+"="+document.getElementById("status"+cat).value;

    if(tam_sub!=null){
        cant_s = tam_sub.value;
        param += "&cant_s="+cant_s;
        for(i=0;i<tam_sub.value;i++){
            //alert("valid sub___"+document.getElementById("idcat"+cat+"_"+i).value);
            param += "&idcat"+cat+"_"+i+"="+document.getElementById("idcat"+cat+"_"+i).value;
            param += "&status"+cat+"_"+i+"="+document.getElementById("status"+cat+"_"+i).value;

            //tiposubcategorias
            if(document.getElementById("tam_tiposub"+cat+"_"+i)!=null){
                tam_tiposub = document.getElementById("tam_tiposub"+cat+"_"+i).value;

                cant_t = tam_tiposub;
                param += "&cant_t"+cat+"_"+i+"="+cant_t;
                for(j=0;j<tam_tiposub;j++){
                    //document.getElementById("statust"+i+"_"+j).selectedIndex = sel;
                    //alert("tam_tiposub_____"+document.getElementById("idcatts"+i+"_"+j).value);
                    param += "&idcatts"+i+"_"+j+"="+document.getElementById("idcatts"+i+"_"+j).value;
                    param += "&statust"+i+"_"+j+"="+document.getElementById("statust"+i+"_"+j).value;
                }
            }
            //tiposubcategorias

        }
    }

//alert(param);

//
    //var celda = document.getElementById("tiposubcategorias"+id);
    if (estado == 4){

        //alert("estado____"+estado);
        var ajax = objetoAjax();
        ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 1){
                document.getElementById("capaCentral").innerHTML = loading("Cargando informacion...", BASEURL);
                estado = 1;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                   estado = 4;
                   alert("Informaci�n Ingresada con Exito...");
                   location.href = BASEURL+ajax.responseText;

                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(param);
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }

//

}




/**
 * Comment
 */
function modificarCategoria(CONTROLLER, BASEURL) {

    var tam_tiposub = "";
    var cant_c = "1";//size de las categorias en caso que hubieran varias para almacenar
    var cant_s = "";//size de subcategorias a almacenar
    var cant_t = "";//size de tiposubcategorias a almacenar
    var cat = "0";
    var tam_sub = document.getElementById("tam_sub");
    var param = "opcion=10";
    param += "&cant_c="+cant_c;
    param += "&cat="+cat;
    param += "&idcat"+cat+"="+document.getElementById("idcat"+cat).value;
    param += "&codcat"+cat+"="+document.getElementById("codcat"+cat).value;
    param += "&status"+cat+"="+document.getElementById("status"+cat).value;

    if(tam_sub!=null){
        cant_s = tam_sub.value;
        param += "&cant_s="+cant_s;
        for(i=0;i<tam_sub.value;i++){
            //alert("valid sub___"+document.getElementById("idcat"+cat+"_"+i).value);
            param += "&idcat"+cat+"_"+i+"="+document.getElementById("idcat"+cat+"_"+i).value;
            param += "&codcatsub"+cat+"_"+i+"="+document.getElementById("codcatsub"+cat+"_"+i).value;
            param += "&status"+cat+"_"+i+"="+document.getElementById("status"+cat+"_"+i).value;

            //tiposubcategorias
            if(document.getElementById("tam_tiposub"+cat+"_"+i)!=null){
                tam_tiposub = document.getElementById("tam_tiposub"+cat+"_"+i).value;

                cant_t = tam_tiposub;
                param += "&cant_t"+cat+"_"+i+"="+cant_t;
                for(j=0;j<tam_tiposub;j++){

                    param += "&idcatts"+i+"_"+j+"="+document.getElementById("idcatts"+i+"_"+j).value;
                    param += "&codcatts"+i+"_"+j+"="+document.getElementById("codcatts"+i+"_"+j).value;
                    param += "&statust"+i+"_"+j+"="+document.getElementById("statust"+i+"_"+j).value;
                }
            }
            //tiposubcategorias

        }
    }

//alert(param);

//
    //var celda = document.getElementById("tiposubcategorias"+id);
    if (estado == 4){

        //alert("estado____"+estado);
        var ajax = objetoAjax();
        ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 1){
                document.getElementById("capaCentral").innerHTML = loading("Cargando informacion...", BASEURL);
                estado = 1;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                   estado = 4;
                   alert("Informacion Actualizada con Exito...");
                   location.href = BASEURL+ajax.responseText;

                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(param);
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }

//

}




function buscarCategorias(CONTROLLER, BASEURL, idcatcargado, idsub, idts){


try{
    var categoria = document.getElementById("categoria");
    var subcategoria = document.getElementById("subcategoria");
    var tiposubcategoria = document.getElementById("tiposubcategoria");



}catch (e){
    alert("Error___...."+e);
}


    if(idcatcargado=='0') document.getElementById("pagina").value = "modificar";
    var pag = document.getElementById("pagina");

var valorcategoria = "";
if(pag.value=="consulta"){

     idcatcargado = idcatcargado;

}else{


    if((pag.value=="crear") &&(categoria ==null)){
       idcatcargado = 0;
    }else{
    idcatcargado = categoria.value;
    }


}

if(pag.value=="filtro"){

}





    if ((categoria != "")){
        
        var ajax = objetoAjax();
        var parametros = "opcion=11";

        ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){

                categoria.options.length = 0;
                categoria.options[0] = new Option("Cargando...", "");
                categoria.disabled = true;

                subcategoria.options.length = 0;
                subcategoria.options[0] = new Option("Cargando...", "");
                subcategoria.disabled = true;

                tiposubcategoria.options.length = 0;
                tiposubcategoria.options[0] = new Option("Selecione Categoria", "");
                tiposubcategoria.disabled = true;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                   
                    var cat = ajax.responseXML.getElementsByTagName("categoria");
                    
                 
                    if (cat[0].childNodes[0].nodeValue != "null"){

                        cargarCategorias(categoria, cat, idcatcargado);
                        if(((pag.value=="consulta")||(pag.value=="crear")) && (idcatcargado!=undefined)){
                        buscarSubcategorias(CONTROLLER, BASEURL, idcatcargado, idsub, idts);
                        }
                }
                    else{
                        alert("");
                        categoria.options.length = 0;
                        categoria.options[0] = new Option("Seleccione una Categoria", "");
                        categoria.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }





}


function cargarCategorias(combobox, categoria, idcatcargado){



 var option ;
 try{
    if (categoria.length > 1){

        combobox.disabled = false;
        for (i=0; i<categoria.length; i++){


          var nomciu = categoria[i].childNodes[0].nodeValue;
            var codcat = categoria[i].getAttribute("codcat");

            if(codcat==idcatcargado){


              
                 option = new Option(nomciu, codcat, codcat,"selected");
            }else{
               
                if(((codcat==0)&&(idcatcargado!="")&&(idcatcargado < 0)&&(idcatcargado > 0))||((codcat==0)&&idcatcargado == undefined) ){
                

                    option = new Option(nomciu, codcat, codcat,"selected");
                }else{


                    option = new Option(nomciu, codcat, codcat,"");//20100914pm
                }

            }

            combobox.options[i] = option;

        }
    }
    else{
        var option = new Option("No hay Categorias", "");
        combobox.options[0] = option;
    }
 }catch (e){
     alert("Error______......"+e);

 }


}

function findLoading(BASEURL, id, estado){
    if (estado == 1){
        id.src = BASEURL+"/images/cargando.gif";
    }
    else if (estado == 4){
        id.src = BASEURL+"/images/botones/iconos/lupa.gif";
    }
}




function buscarSubcategorias(CONTROLLER, BASEURL, idcatcargado, idsub, idts){

    var categoria = document.getElementById("categoria");



    var subcategoria = document.getElementById("subcategoria");
    var tiposubcategoria = document.getElementById("tiposubcategoria");


if(idcatcargado=='0') document.getElementById("pagina").value = "modificar";
var pag = document.getElementById("pagina");


var valorcategoria = "";
if(pag.value=="consulta"){
     valorcategoria = idcatcargado;
}else{
    valorcategoria = categoria.value;
}


    if ((categoria != "")){

        var ajax = objetoAjax();
        var parametros = "opcion=12&cat="+valorcategoria;

        ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){

                subcategoria.options.length = 0;
                subcategoria.options[0] = new Option("Cargando...", "");
                subcategoria.disabled = true;

                tiposubcategoria.options.length = 0;
                tiposubcategoria.options[0] = new Option("Selecione Categoria", "");
                tiposubcategoria.disabled = true;
            }
            else if (ajax.readyState == 4){


                if( ajax.status == 200){

                    var cat = ajax.responseXML.getElementsByTagName("subcategoria");

                    if (cat[0].childNodes[0].nodeValue != "null"){

                        cargarSubCategorias(subcategoria, cat, idsub);
                                if((pag.value=="consulta")||(pag.value=="crear")){
                                buscarTipoSubcategorias(CONTROLLER, BASEURL, idcatcargado, idsub, idts);
                                }else{
                                buscarTipoSubcategorias(CONTROLLER, BASEURL, idcatcargado, idsub, '0');
                                }

                    }
                    else{

                        subcategoria.options.length = 0;
                        subcategoria.options[0] = new Option("Seleccione una Categoria", "");
                        subcategoria.options [0].selected;
                        subcategoria.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }
}


function cargarSubCategorias(combobox, subcategoria, idsub){


    if (subcategoria.length > 0){
        combobox.disabled = false;
        borradorOptions(document.getElementById('subcategoria'));
        for (i=0; i<subcategoria.length; i++){
            var nomciu = subcategoria[i].childNodes[0].nodeValue;
            var codcat = subcategoria[i].getAttribute("codsubcat");

            //////////////////


            if(codcat==idsub){

                var option = new Option(nomciu, codcat, codcat,"selected");
            }else{

                if(((codcat==0)&&(idsub!="")&&(idsub < 0)&&(idsub > 0)) ||((codcat==0)&&idsub == undefined)   ){

                    var option = new Option(nomciu, codcat, codcat,"selected");
                }else{

                    var option = new Option(nomciu, codcat, codcat,"");//20100914
                }

            }


            combobox.options[i] = option;
        }
    }
    else{
        borradorOptions(document.getElementById('subcategoria'));
        var option = new Option("No hay SubCategorias", "");
        combobox.options[0] = option;
        combobox.disabled = true;
    }
}



///

function buscarTipoSubcategorias(CONTROLLER, BASEURL, idcatcargado,  idsub, idtsub){

    var categoria = document.getElementById("categoria");
    var subcategoria = document.getElementById("subcategoria");
    var tiposubcategoria = document.getElementById("tiposubcategoria");


    if(idcatcargado=='0') document.getElementById("pagina").value = "modificar";
        var pag = document.getElementById("pagina").value;

    var valorcategoria = "";
    var valorsub = "";
    if(pag=="consulta"){
        valorcategoria = idcatcargado;
        valorsub = idsub;
    }else{
        valorcategoria = categoria.value;
        valorsub = subcategoria.value
     }



    if ((categoria != "")){
        var ajax = objetoAjax();
        var parametros = "opcion=13&cat="+valorcategoria+"&subcat="+valorsub;

        ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){
                tiposubcategoria.options.length = 0;
                tiposubcategoria.options[0] = new Option("Selecione Categoria", "");
                tiposubcategoria.disabled = true;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    var cat = ajax.responseXML.getElementsByTagName("tiposubcategoria");

                    if (cat[0].childNodes[0].nodeValue != "null"){
                        cargarTipoSubCategorias(tiposubcategoria, cat, idtsub);
                    }
                    else{
                        tiposubcategoria.options.length = 0;
                        tiposubcategoria.options[0] = new Option("Seleccione una Subcategoria", "");
                        tiposubcategoria.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }
}


function cargarTipoSubCategorias(combobox, tiposubcategoria, idtsub){



try{
if (tiposubcategoria.length > 0){
        combobox.disabled = false;
        borradorOptions(document.getElementById('tiposubcategoria'));
        for (i=0; i<tiposubcategoria.length; i++){
            var nomciu = tiposubcategoria[i].childNodes[0].nodeValue;
            var codciu = tiposubcategoria[i].getAttribute("codtiposubcat");
            var option = new Option(nomciu, codciu, codciu,"selected");


            //////////////////


            if(codciu==idtsub){

                var option = new Option(nomciu, codciu, codciu,"selected");
            }else{

                if(((codciu==0)&&(idtsub!="")&&(idtsub < 0)&&(idtsub > 0))   ||((codciu==0)&&idtsub == undefined)     ){

                    var option = new Option(nomciu, codciu, codciu,"selected");
                }else{

                    var option = new Option(nomciu, codciu, codciu,"");//20100914
                }

            }
            //////////////////

            combobox.options[i] = option;
        }
    }
    else{
        borradorOptions(document.getElementById('tiposubcategoria'));
        var option = new Option("No hay TiposubCategorias", "");
        combobox.options[0] = option;
        combobox.disabled = true;
    }
}catch (e){
    alert("Error...tiposub____"+e);
}


}

///


				function borradorOptions(selection){
					var ind = 1;
					var tm = selection.length;
					var elim = tm-1;
					while(ind<tm){
						selection.remove(elim);
						elim = elim - 1;
						ind = ind + 1;
					}
				}



function ListadoMateriales(CONTROLLER, BASEURL){


var filtro = document.getElementById("filtro").value;
var parametro = document.getElementById("parametro").value;
var cat = document.getElementById("categoria").value;
var subcat = "";
var tscat = "";
var tipomat = document.getElementById("tipomat").value;


if(cat==""){cat=0;}
if(subcat==""){subcat=0;}
if(tscat==""){tscat=0;}




    if (estado == 4){
        var ajax = objetoAjax();
        ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 1){
                document.getElementById("capaLista").innerHTML = loading("Cargando informacion...", BASEURL);
                estado = 1;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    estado = 4;
                    document.getElementById("capaLista").innerHTML = ajax.responseText;

                }
            }
        }
       ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send("opcion=14&filtro="+filtro+"&parametro="+parametro+"&cat="+cat+"&subcat="+subcat+"&tscat="+tscat+"&tipomat="+tipomat);
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}





function generarExcel(CONTROLLER, BASEURL){


var filtro = document.getElementById("filtro").value;

var parametro = document.getElementById("parametro").value;

var cat = document.getElementById("categoria").value;

var subcat = '';

var tscat = '';



if(cat==""){cat=0;}
if(subcat==""){subcat=0;}
if(tscat==""){tscat=0;}


    if (estado == 4){

        var ajax = objetoAjax();
        ajax.open("POST", CONTROLLER+"?estado=Materiales&accion=Crear", true);
        ajax.onreadystatechange = function(){

        if (ajax.readyState == 1){

                document.getElementById("capaLista").innerHTML = loading("Cargando informacion...", BASEURL);
                estado = 1;
            }
            else if (ajax.readyState == 4){

                if( ajax.status == 200){

                    estado = 4;
                    document.getElementById("capaLista").innerHTML = ajax.responseText;

                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send("opcion=4&filtro="+filtro+"&parametro="+parametro+"&cat="+cat+"&subcat="+subcat+"&tscat="+tscat);
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}





function generarExcelCategorias(CONTROLLER, BASEURL){

    if (estado == 4){
        var ajax = objetoAjax();
        ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 1){
                document.getElementById("capaCentral").innerHTML = loading("Cargando informacion...", BASEURL);
                estado = 1;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    estado = 4;

                   alert("Excel Generado con Exito...");
                   location.href = BASEURL+ajax.responseText;
                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send("opcion=15");
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}


            function formatNumber(num,prefix){

              prefix = prefix || '';
                num += '';
                var splitStr = num.split('.');
                var splitLeft = splitStr[0];
                var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
                splitRight = splitRight.substring(0,3);//090929
                var regx = /(\d+)(\d{3})/;
                while (regx.test(splitLeft)) {
                    splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
                }
                return prefix + splitLeft + splitRight;
            }


/**
 * Comment
 */
function mensaje(msg) {
//alert("msg");
   alert(msg);

}



            /**
             * Comment
             */
            function enviarAction(CONTROLLER, opcion) {



                if((opcion == "crear")||(opcion =="")){
                    document.getElementById("forma").action = CONTROLLER+"?estado=Materiales&accion=Crear&opcion=0";
                }else{
                    document.getElementById("forma").action = CONTROLLER+"?estado=Materiales&accion=Crear&opcion=1&opcion_pagina=modificar";
                }
                document.getElementById("forma").submit();
            }

//function guardarNuevo() {
//    alert("guardarNuevo");
//    alert(document.getElementById("idcat0").va    lue);
//    alert(document.getElementById("status0").value);
//    var tam_tiposub = "";
//    var cant_c = "0";//size de las categorias en caso que hubieran varias para almacenar
//    var cant_s = "";//size de subcategorias a almacenar
//    var cant_t = "";//size de tiposubcategorias a almacenar
//    var cat = "0";
//    var tam_sub = document.getElementById("tam_sub");
//
//
//    if(tam_sub!=null){
//         alert("tam_sub___"+tam_sub.value);
//         cant_s = tam_sub.value;
//        for(i=0;i<tam_sub.value;i++){
//
//            alert("valid sub___"+document.getElementById("idcat"+cat+"_"+i).value);
//
//            tiposubcategorias
//            if(document.getElementById("tam_tiposub"+cat+"_"+i)!=null){
//                tam_tiposub = document.getElementById("tam_tiposub"+cat+"_"+i).value;
//
//                for(j=0;j<tam_tiposub;j++){
//                    document.getElementById("statust"+i+"_"+j).selectedIndex = sel;
//                    alert("tam_tiposub_____"+document.getElementById("idcatts"+i+"_"+j).value);
//
//                }
//            }
//        tiposubcategorias
//
//        }
//    }
//
//
//
//
//    /*var sel = document.getElementById("status"+0).selectedIndex;
//
//    var tam_tiposub = "";
//
//    for(i=0;i<tam_sub.value;i++){
//        document.getElementById("status0_"+i).selectedIndex = sel;
//        alert(document.getElementById("idcat0_"+i).value);
//
//
///*        if(document.getElementById("tam_tiposub0_"+i)){
//            tam_tiposub = document.getElementById("tam_tiposub0_"+i).value;
//            for(j=0;j<tam_tiposub;j++){
//                document.getElementById("statust"+i+"_"+j).selectedIndex = sel;
//            }
//        }
//    }*/
//}








function obtenerCotizacion(CONTROLLER, BASEURL, index, id_accion){

    var imagen = document.getElementById("ima"+index);
    var celda = document.getElementById("solicitud"+index);
    var visible = ((celda.style.display == "none")?false:true);
    if (estado == 4){
        if (visible){
            imagen.src = BASEURL+"/images/botones/iconos/expand.gif";
            celda.style.display="none";
        } else {
            if(celda.innerHTML == ""){
                var ajax = objetoAjax();
                ajax.open("POST", CONTROLLER+"?estado=Cotizacion&accion=Orden", true);
                ajax.onreadystatechange = function(){
                    if (ajax.readyState == 1){
                        celda.style.display = "block";
                        celda.innerHTML = loading("Cargando informacion...", BASEURL);
                        estado = 1;
                    }
                    else if (ajax.readyState == 4){
                        if( ajax.status == 200){
                            estado = 4;
                            celda.style.display = "block";
                            imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
                            celda.innerHTML = ajax.responseText;

                        }
                    }
                }
                ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                ajax.send("opcion=1&id_accion="+id_accion+"&index="+index);
            }else{
                celda.style.display = "block";
                imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
            }
        }
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}

///////////////////////////////// Categorias para cargar en el filtro de cotizacion


/**
 *
 */
function buscarCategoriasCotizacion(CONTROLLER, BASEURL, idcatcargado){

    try{

        var categoria = document.getElementById("categoria");

    }catch (e){
        alert("error___"+e);
    }



    if ((categoria != "")){
        var ajax = objetoAjax();
        var parametros = "opcion=11";

        ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){
                categoria.options.length = 0;
                categoria.options[0] = new Option("Cargando...", "");
                categoria.disabled = true;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    var cat = ajax.responseXML.getElementsByTagName("categoria");

                    if (cat[0].childNodes[0].nodeValue != "null"){
                        cargarCategorias(categoria, cat, idcatcargado);
                       // document.getElementById("categoria").selectIndex=0;
                        if(idcatcargado != undefined ){

                        //Si viene con una categoria seleccinada consultar enseguida

                        buscarSubcategorias(CONTROLLER, BASEURL, idcatcargado);

                    }
                }
                    else{
                        categoria.options.length = 0;
                        categoria.options[0] = new Option("Seleccione una Categoria", "");
                        categoria.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }





}



function cargarCategoriasCotizacion(combobox, categoria, idcatcargado){

    var option ;
    try{
        if (categoria.length > 1){
            combobox.disabled = false;
            for (i=0; i<categoria.length; i++){

                var nomciu = categoria[i].childNodes[0].nodeValue;
                var codcat = categoria[i].getAttribute("codcat");
                if(codcat==idcatcargado){

                    option = new Option(nomciu, codcat, codcat,"selected");
                }else{
                    if(((codcat==0)&&(idcatcargado!="")&&(idcatcargado < 0)&&(idcatcargado > 0))||((codcat==0)&&idcatcargado == undefined) ){

                        option = new Option(nomciu, codcat, codcat,"selected");
                    }else{
                        option = new Option(nomciu, codcat, codcat,"");//20100914
                    }
                }
                combobox.options[i] = option;
            }
        }
        else{
            var option = new Option("No hay Categorias", "");
            combobox.options[0] = option;
        }
    }catch (e){
        alert("error___________"+e);

    }


}






function ListadoSubcategoriasTipoSub(CONTROLLER, BASEURL, ind, tipo){



var cat = document.getElementById("categoria").value;


if(cat==""){cat=0;}



    if (estado == 4){
        var ajax = objetoAjax();
        ajax.open("POST", CONTROLLER+"?estado=Categoria&accion=Orden", true);
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 1){
                document.getElementById("capaLista").innerHTML = loading("Cargando informacion...", BASEURL);
                estado = 1;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    estado = 4;
                    document.getElementById("capaLista").innerHTML = ajax.responseText;

                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send("opcion=16&cat="+cat+"&ind="+ind+"&tipo="+tipo);
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}








function setCheckedValue(CONTROLLER, BASEURL, ind, idsub, idtiposub, idcat, descat, tipo,tipomat) {
try{
alert("Filtro Asignado");

//window.opener.document.getElementById("textfieldx"+ind).value="hola";
window.opener.document.getElementById("idsubcategoria"+ind).value=idsub;
window.opener.document.getElementById("idtiposubcategoria"+ind).value=idtiposub;






window.opener.document.getElementById("categoria"+ind).options.length = 0;
window.opener.document.getElementById("categoria"+ind).value="idtiposub";
window.opener.document.getElementById("categoria"+ind).options[0] = new Option(descat, idcat);

}catch (e ){
    alert("Error___"+e)

}


buscarMaterialesFiltro(CONTROLLER, BASEURL,ind, tipo,  idcat, 0,tipomat);//>>>>>> 


}

/**
 *
 */
function pru2() {
    alert("pru2")

}


//////////




 function color_fila(id,color){
     //alert("color");

                    //document.getElementById(id).style.backgroundColor=color;
                }

 function uno(a, d, ind){
     a.style.cursor="hand";
     document.getElementById("fila"+ind).style.backgroundColor= d;
           }









function buscarCategoriasCotnew(CONTROLLER, BASEURL, idcatcargado, tipo){

    try{
        //alert("buscarCategorias___pagconsulta");

        //alert("tipo__"+tipo);
        var categoria = document.getElementById("categoria");
        //Dependiendo del tipo debe dejar modificar el precio

if ((categoria!= "")){
            var ajax = objetoAjax();
            var parametros = "opcion=4&tipo="+tipo;

            ajax.open("POST", CONTROLLER+"?estado=Cotizacion&accion=Orden", true);
            ajax.onreadystatechange = function() {
                if (ajax.readyState == 1){

                    categoria.options.length = 0;
                    categoria.options[0] = new Option("Cargando...", "");
                    categoria.disabled = true;

                }
                else if (ajax.readyState == 4){
                    if( ajax.status == 200){
                        var cat = ajax.responseXML.getElementsByTagName("categoria");

                        if (cat[0].childNodes[0].nodeValue != "null"){
                            cargarCategorias(categoria, cat, idcatcargado);
                        /*                        if(idcatcargado != undefined ){

                        //Si viene con una categoria seleccinada consultar enseguida
                        buscarSubcategorias(CONTROLLER, BASEURL, idcatcargado);

                    }*/
                        }
                        else{
                            categoria.options.length = 0;
                            categoria.options[0] = new Option("Seleccione una Categoria", "");
                            categoria.disabled = true;
                        }


                    }
                }
            }
            ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            ajax.send(parametros);
        }

    }catch (e){
        alert("Error___"+e);
    }

}



////////////////

function buscarMaterialesFiltro(CONTROLLER, BASEURL,indice, tipoprevio,  categoria, idmatcargado){




try{



    var material = window.opener.document.getElementById("material"+indice);

    var tipo = window.opener.document.getElementById("tipo"+indice).value;
    var idcat = window.opener.document.getElementById("categoria"+indice).value;

    //var idsubcat = 0;
    //var idtiposubcat = 0;

    	var idsubcat = window.opener.document.getElementById("idsubcategoria"+indice).value;//OJO CAMBIO A PASAR A IVAN
        var idtiposubcat = window.opener.document.getElementById("idtiposubcategoria"+indice).value;


    //alert(tipo);
    //alert(idcat);

    if ((tipo!= "")){
        var ajax = objetoAjax();
        var parametros = "opcion=5&tipo="+tipo+"&idcat="+idcat+"&idsubcat="+idsubcat+"&idtiposubcat="+idtiposubcat;

        ajax.open("POST", CONTROLLER+"?estado=Cotizacion&accion=Orden", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){

                material.options.length = 0;
                material.options[0] = new Option("Cargando...", "");
                material.disabled = true;

            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    var cat = ajax.responseXML.getElementsByTagName("material");

                    if (cat[0].childNodes[0].nodeValue != "null"){
                        cargarMaterialesFiltro(material, cat, idmatcargado, indice);
                        /*if((pag.value=="consulta")||(pag.value=="crear")){
                        }
                        buscarSubcategorias(CONTROLLER, BASEURL, idcatcargado, idsub, idts);
                        }*/
                }
                    else{
                        material.options.length = 0;
                        material.options[0] = new Option("Seleccione un Material", "");
                        material.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }

}catch (e){
    alert("error___"+e);
}

}


function cargarMaterialesFiltro(combobox, material, idcatcargado, indice){






    var option ;
    try{
        if (material.length > 1){
            combobox.disabled = false;
            borradorOptions(window.opener.document.getElementById("material"+indice));
//            borradorOptions(document.getElementById("material"+indice));
            for (i=0; i<material.length; i++){

                var nomciu = material[i].childNodes[0].nodeValue;
                var codcat = material[i].getAttribute("codcat");
                if(codcat==idcatcargado){
                    option = new Option(nomciu, codcat, codcat,"selected");
                }else{
                    if(((codcat==0)&&(idcatcargado!="")&&(idcatcargado < 0)&&(idcatcargado > 0))||((codcat==0)&&idcatcargado == undefined) ){
                        option = new Option(nomciu, codcat, codcat,"selected");
                    }else{
                        option = new Option(nomciu, codcat, codcat,"");//20100914
                    }

                }
                combobox.options[i] = option;
            }
        }
        else{
   borradorOptions(window.opener.document.getElementById("material"+indice));
            var option = new Option("No hay Materiales", "");
            combobox.options[0] = option;
        }
    }catch (e){
        alert("error______"+e);

    }


}

////////////////////////////////


/**
 * Comment
 */


/**
 * Comment
 */




function buscarCertificadores(CONTROLLER, BASEURL, idcargado){

    var certificador = document.getElementById("ente_certi");


    if ((certificador != "")){
        var ajax = objetoAjax();
        var parametros = "opcion=5";

        ajax.open("POST", CONTROLLER+"?estado=Materiales&accion=Crear", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){
                certificador.options.length = 0;
                certificador.options[0] = new Option("Cargando...", "");
                certificador.disabled = true;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    var cat = ajax.responseXML.getElementsByTagName("certificador");

                    if (cat[0].childNodes[0].nodeValue != "null"){
                        cargarCertificadores(certificador, cat, idcargado);

                }
                    else{
                        certificador.options.length = 0;
                        certificador.options[0] = new Option("Seleccione un Certificador", "");
                        certificador.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }

}





function cargarCertificadores(combobox, cod, idcargado){

    var option ;
 try{
        if (cod.length > 1){
            combobox.disabled = false;
var i = 0;

for (i; i<cod.length; i++){

                var nomente = cod[i].childNodes[0].nodeValue;
                var codente = cod[i].getAttribute("codente");
                if(codente==idcargado){

                    option = new Option(nomente, codente, codente,"selected");
                }else{
                    if(((codente==0)&&(idcargado!=""))||((codente==0)&&idcargado == undefined) ){

                   option = new Option(nomente, codente, codente,"selected");

                    }else{
                        option = new Option(nomente, codente, codente,"");//20100914
                    }
                }
                combobox.options[i] = option;
            }
        }
        else{
            var option = new Option("No hay Certificadores", "");
            combobox.options[0] = option;
        }
    }catch (e){
        alert("error___________"+e);

    }

     var certificado = document.getElementById("certificado").value;

     if(certificado== "N"){
         document.getElementById("ente_certi").disabled =true;
     }else{
         document.getElementById("ente_certi").disabled =false;
     }

}




/**
 * Comment
 */
function habilitarCertificador() {



    var certificado = document.getElementById("certificado").value;

    if(certificado== "N"){
        document.getElementById("ente_certi").disabled =true;
    }else{
        document.getElementById("ente_certi").disabled =false;
    }
}

/**
 * Comment
 */




function buscarCertificadores(CONTROLLER, BASEURL, idcargado){

    var certificador = document.getElementById("ente_certi");


    if ((certificador != "")){
        var ajax = objetoAjax();
        var parametros = "opcion=5";

        ajax.open("POST", CONTROLLER+"?estado=Materiales&accion=Crear", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){
                certificador.options.length = 0;
                certificador.options[0] = new Option("Cargando...", "");
                certificador.disabled = true;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    var cat = ajax.responseXML.getElementsByTagName("certificador");

                    if (cat[0].childNodes[0].nodeValue != "null"){
                        cargarCertificadores(certificador, cat, idcargado);

                }
                    else{
                        certificador.options.length = 0;
                        certificador.options[0] = new Option("Seleccione un Certificador", "");
                        certificador.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }

}





function cargarCertificadores(combobox, cod, idcargado){

    var option ;
 try{
        if (cod.length > 1){
            combobox.disabled = false;
var i = 0;

for (i; i<cod.length; i++){

                var nomente = cod[i].childNodes[0].nodeValue;
                var codente = cod[i].getAttribute("codente");
                if(codente==idcargado){

                    option = new Option(nomente, codente, codente,"selected");
                }else{
                    if(((codente==0)&&(idcargado!=""))||((codente==0)&&idcargado == undefined) ){

                   option = new Option(nomente, codente, codente,"selected");

                    }else{
                        option = new Option(nomente, codente, codente);
                    }
                }
                combobox.options[i] = option;
            }
        }
        else{
            var option = new Option("No hay Certificadores", "");
            combobox.options[0] = option;
        }
    }catch (e){
        alert("error___________"+e);

    }

     var certificado = document.getElementById("certificado").value;

     if(certificado== "N"){
         document.getElementById("ente_certi").disabled =true;
     }else{
         document.getElementById("ente_certi").disabled =false;
     }

}




/**
 * Comment
 */
function habilitarCertificador() {



    var certificado = document.getElementById("certificado").value;

    if(certificado== "N"){
        document.getElementById("ente_certi").disabled =true;
    }else{
        document.getElementById("ente_certi").disabled =false;
    }
}

/**
 * Comment
 */
function deshabilitarPorTipo() {

    var tipo;
    tipo = document.getElementById("tipo").value;

    if((tipo == "D")||(tipo == "O")){
    //alert("tipo___"+tipo);
    document.getElementById("categoria").disabled = true;
    document.getElementById("subcategoria").disabled = true;
    document.getElementById("tiposubcategoria").disabled = true;
    document.getElementById("uni_empaque").disabled = true;


    document.getElementById("certificado").disabled = true;
    document.getElementById("ente_certi").disabled = true;


    }else{
    document.getElementById("categoria").disabled = false;
    document.getElementById("uni_empaque").disabled = false;


    document.getElementById("certificado").disabled = false;
    document.getElementById("ente_certi").disabled = false;

    }


}

