/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){   
    $("#tabs_contrato").tabs();  
    initializeEditor('editor1');  
    initializeEditor('editor2');  
    // $("#editor1").val("MANUEL CASTILLO");
          
});

function initializeEditor (editor){
     var ckEditor = CKEDITOR.replace(editor, {
        height: 420,
        language: 'es',
        skin: 'office2013',
        resize_enabled: false,
        // Define the toolbar groups as it is a more accessible solution.
        toolbarGroups: [
            {name: 'document', groups: ['document']},
            {name: 'clipboard', groups: ['clipboard', 'undo']},
            {name: 'editing', groups: ['find', 'selection'/*, 'spellchecker'*/]},
            {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align']},
            {name: 'links'},
            {name: 'insert'},
            {name: 'styles'},
            {name: 'colors'},
            {name: 'tools'},
            {name: 'others'}

        ],
        // Remove the redundant buttons from toolbar groups defined above.
        removeButtons: 'NewPage,Preview,Print,Maximize,Flash,Iframe,CreateDiv,ShowBlocks',
        removePlugins: 'elementspath'
    });      
                
    ckEditor.on("instanceReady", function () {

        // overwrite the default save function
        ckEditor.addCommand("save", {
            modes: {wysiwyg: 1, source: 1},
            exec: function () {

                // get the editor content
                var theData = ckEditor.getData();
                console.log(theData);
                alert("insert your code here");
            }
        });
    });
}
//Get Content from editor
//console.log(CKEDITOR.instances['editor1'].getData());

