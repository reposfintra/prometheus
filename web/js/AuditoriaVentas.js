/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $("#fecha").val('');
    $("#fechafin").val('');
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    // $("#fecha").datepicker("setDate", myDate);
    //$("#fechafin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

    cargarComboEds();
    $("#buscar").click(function () {
        var fechainicio = $("#fecha").val();
        var fechafin = $("#fechafin").val();
        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
            cargarVentas();
        } else {
            mensajesDelSistema("Por favor revice el rango de fechas", '410', '150', false);
        }

    });

});


function cargarVentas() {
    var grid_tabla = jQuery("#tabla_auditoria_ventas");
    if ($("#gview_tabla_auditoria_ventas").length) {
        reloadGridVENTAS(grid_tabla, 46);
    } else {
        grid_tabla.jqGrid({
            caption: "VENTAS",
            url: "./controller?estado=Administracion&accion=Eds",
            mtype: "POST",
            datatype: "json",
            height: '550',
            width: '1650',
            colNames: ['Id', 'Periodo', 'Planilla', 'Fecha Envio Fintra', 'Fecha Venta', 'Diferencia Fechas', 'Origen', 'Destino', 'Valor Planilla', 'Valor Neto Anticipo', 'Valor Descuento',
                'Valor Desembolsar', 'Total Venta', 'Disponible', 'Reanticipo', 'Fecha Pago Fintra', 'Fecha Corrida', 'Id Transportadora',
                'Transportadoras', 'Id Agencias', 'Nombre Agencias', 'Id Conductor', 'Conductor', 'Id Propietario', 'Cedula Propietario', 'Propietario', 'Id Vehiculo', 'Placa',
                'Sucursal', 'Numero Venta', 'Fecha Venta', 'Nombre eds', 'Kilometraje', 'Cantidad Registros Ventas', 'Total Venta', 'Valor Comision'],
            colModel: [
                {name: 'id', index: 'id', width: 60, sortable: true, align: 'center', hidden: true},
                {name: 'periodo', index: 'periodo', width: 70, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'planilla', index: 'planilla', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_envio_fintra', index: 'fecha_envio_fintra', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'fecha_vent', index: 'fecha_vent', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'diferencia_fechas', index: 'diferencia_fechas', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'origen', index: 'origen', width: 110, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'destino', index: 'destino', width: 110, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'valor_planilla', index: 'valor_planilla', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_neto_anticipo', index: 'valor_neto_anticipo', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_descuentos_fintra', index: 'valor_descuentos_fintra', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_desembolsar', index: 'valor_desembolsar', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total_venta', index: 'total_venta', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'disponible', index: 'disponible', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'reanticipo', index: 'reanticipo', width: 95, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_pago_fintra', index: 'fecha_pago_fintra', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'fecha_corrida', index: 'fecha_corrida', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_transportadoras', index: 'id_transportadoras', width: 95, sortable: true, align: 'left', hidden: true},
                {name: 'transportadora', index: 'transportadora', width: 120, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_agencias', index: 'id_agencias', width: 95, sortable: true, align: 'left', hidden: true},
                {name: 'nombre_agencia', index: 'nombre_agencia', width: 95, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_conductors', index: 'id_conductors', width: 95, sortable: true, align: 'left', hidden: true},
                {name: 'conductor', index: 'conductor', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_propietarios', index: 'id_propietarios', width: 95, sortable: true, align: 'left', hidden: true},
                {name: 'cedula_propietario', index: 'cedula_propietario', width: 95, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'propietario', index: 'propietario', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_vehiculos', index: 'id_vehiculos', width: 95, sortable: true, align: 'left', hidden: true},
                {name: 'placa', index: 'placa', width: 95, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'sucursal', index: 'sucursal', width: 95, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'num_venta', index: 'num_venta', width: 95, sortable: true, align: 'center', hidden: false, search: true,
                    formatter: 'currencyFmatter'},
                {name: 'fecha_vent', index: 'fecha_vent', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_eds', index: 'nombre_eds', width: 120, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'kilometraje', index: 'kilometraje', width: 95, sortable: true, align: 'left', hidden: false},
                {name: 'cant_reg_ventas', index: 'cant_reg_ventas', width: 95, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'total_venta', index: 'total_venta', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_comision_fintra', index: 'valor_comision_fintra', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            onCellSelect: function (rowid, index, contents, event) {
                var celda = grid_tabla.jqGrid('getGridParam', 'colModel');
//                var numventa = grid_tabla.getRowData(rowid).num_venta;
//                alert(numventa);
                if (celda[index].name === "num_venta") {
                    var numventa = grid_tabla.getRowData(rowid).num_venta;
                    ventanaDetalleVenta(numventa);
                }

            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 46,
                    eds: $("#eds").val(),
                    fechainicio: $("#fecha").val(),
                    fechafin: $("#fechafin").val(),
                    planilla: $("#planilla").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, gridComplete: function (index) {
                var ids = grid_tabla.jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var valorDesembolsar = grid_tabla.getRowData(cl).valor_desembolsar;
                    var totalventa = grid_tabla.getRowData(cl).total_venta;
                    var num_venta = grid_tabla.getRowData(cl).num_venta;
                    if ((valorDesembolsar !== totalventa)&& (num_venta !== '')) {
                        $("#tabla_auditoria_ventas").jqGrid('setRowData', ids[i], false, {weightfont: 'bold', background: '#F5A9A9'});
                    }

                }
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
        $("#tabla_auditoria_ventas").navButtonAdd('#pager1', {
            caption: "Exportar Excel",
            onClickButton: function () {
                var info = grid_tabla.getGridParam('records');
                if (info > 0) {
                    exportarventas();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }

            }
        });
    }
}

function reloadGridVENTAS(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Eds",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                eds: $("#eds").val(),
                fechainicio: $("#fecha").val(),
                fechafin: $("#fechafin").val(),
                planilla: $("#planilla").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function reloadGridVENTASDetalle(grid_tabla, op, numventa) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Eds",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                numventa: numventa
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function  exportarventas() {
    var fullData = jQuery("#tabla_auditoria_ventas").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Administracion&accion=Eds",
        data: {
            listado: myJsonString,
            opcion: 48
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function cargarComboEds() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 47
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#eds').append("<option value=''> Seleccione.. </option>");
                for (var datos in json) {
                    $('#eds').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
                $("#fecha").val('');
                $("#fechafin").val('');
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}
function ventanaDetalleVenta(numventa) {
    cargarVentasDetalle(numventa);
    $("#dialogMdetalle").dialog({
        width: '870',
        height: '450',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'VENTAS',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarVentasDetalle(numventa) {
    var grid_tabla = jQuery("#tabla_auditoria_ventas_detalle");
    if ($("#gview_tabla_auditoria_ventas_detalle").length) {
        reloadGridVENTASDetalle(grid_tabla, 49, numventa);
    } else {
        grid_tabla.jqGrid({
            //caption: "VENTAS",
            url: "./controller?estado=Administracion&accion=Eds",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '850',
            colNames: ['Numero Venta', 'Fecha Venta', 'Producto', 'Kilometraje', 'Precio por Producto', 'Cantidad Suministrada', 'Total Venta'],
            colModel: [
                {name: 'num_venta', index: 'inum_ventad', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'fecha_vent', index: 'fecha_vent', width: 150, sortable: true, align: 'center', hidden: false},
                {name: 'producto', index: 'producto', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'kilometraje', index: 'kilometraje', width: 80, sortable: true, align: 'left', hidden: false},
                {name: 'precio_xproducto', index: 'precio_xproducto', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cantidad_suministrada', index: 'cantidad_suministrada', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'total_venta', index: 'total_venta', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager2',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            ondblClickRow: function (rowid, iRow, iCol, e) {
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 49,
                    numventa: numventa
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, gridComplete: function () {
            }
        }).navGrid("#pager2", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}