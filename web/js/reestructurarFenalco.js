/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
loading("Espere un momento se estan cargando los descuentos.", "300", "140");
$("#campoVisible").val(0);
    //boton buscar.    
    $("#buscar").click(function () {
        var cedula = $("#inptBuscar").val();
        $("#totalResumen").val(0);
        $("#totalSubResumen").val(0);
        $("#totalItem").val(0);        
        $("#totalResumenPagoInicial").val(0);   
        $("#campoVisible").val(0);       
        $("#totalaPagar").val(0);
        $("#totalsaldoVencidoRee").val(0);
        $("#totalsaldoCorrienteRee").val(0);
        $("#totalReestructuracion").html("0.00");
        $("#divSaldoNegocio").hide();
      
        borrarSaldos();
        if (cedula !== "") {
            buscarNegociosCliente(cedula);
            $("#divListaNegocios").show();
        } else {
            mensajesDelSistema("Por favor debe llenar al menos un campo. <br/> Gracias.", 283, 150);
        }
    });

    //boton calcular liqidacion.
    $("#calLiquidacion").click(function () {
        $("#descuentoCapital").val(0);
        $("#descuentoInteres").val(0);
        $("#descuentoIntxMora").val(0);
        $("#descuentoGac").val(0);
        $("#totalResumen").val(0);
        $("#totalSubResumen").val(0);
        $("#totalResumenPagoInicial").val(0);
        calcularSaldoNegocios();
        $("#divSaldos").show();
        $("#btnoculto").show();
    });

    //panel deslizante.
    $("#boton").click(function () {
        $("#divListaNegocios").toggle('slide', {direction: 'left'}, 400);

        if ($("#campoVisible").val() === "0") {
            $('#divSaldos').css({'left': '10px'});
            $("#campoVisible").val(1);
        } else {
            $('#divSaldos').css({'left': '1194px'});
            $("#campoVisible").val(0);
        }

    });

    //simulador de cuotas
    //boton calcular liqidacion.
    $("#simuladoCuotas").click(function () {
        abrirSimuladorFenalco();
    });

    //calcular fechas de pagos para el simulador.
    carcularFecha();
    
     $("#totalReestructuracion").click(function () {
         $("#totalReestructuracion").css({'background':'rgba(222, 211, 132, 0.78)'});
         $("#totalsaldoVencidoRee").css({'background':'rgba(222, 211, 132, 0.78)'});
         $("#totalsaldoCorrienteRee").css({'background-color':'rgba(222, 211, 132, 0.78)'});
    });
    
    $("#limpiar").click(function () {
           location.reload();
    });

   cargardescuentos();
   //metodo para cargar unidades de negocio en el futuro.
  // cargarUnidadNegocio();
});

function DeleteRowOfProductTable(productID) {
    $('#cuerpoTablaConsolidado tr').each(function (i, row) {
        var $row = $(row);
        var productLabelId = $row.find('span');
        // alert(productLabelId.text());
        var productLabelIdValue = productLabelId.text();
        if (productID === productLabelIdValue || productLabelIdValue === 'Totales') {
            $row.remove();
        }
    });
}

function DeleteRowOfProductTableInicial(productID) {
    $('#cuerpoTablaInicial tr').each(function (i, row) {
        var $row = $(row);
        var productLabelId = $row.find('span');
        var productLabelIdValue = productLabelId.text();
        if (productID === productLabelIdValue) {
            $row.remove();
        }
    });
}


function borrarSaldos() {
    $("#cuerpoTablaConsolidado").html("");
    $("#cuerpoTablaInicial").html("");
    $("#valorFactura").val(0);
    $("#valorFactura").val(0);
    $("#valorAbono").val(0);
    $("#valorSaldo").val(0);
    $("#saldoCapital").val(0);
    $("#saldoInteres").val(0);
    $("#inteXmora").val(0);
    $("#totalItem").val(0);


}

/***************************************************
 * Funcion que busca los negocios vivos en cartera.*
 * * parametro:cedula                              *
 * *************************************************/
function buscarNegociosCliente(cedula) {

    var grid_listar_negocios = jQuery("#tabla_negocios_clientes");

    if ($("#gview_tabla_negocios_clientes").length) {
        reloadGridListarNegocios(grid_listar_negocios, cedula);
    } else {

        grid_listar_negocios.jqGrid({
            caption: "Negocios",
            url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=1&cedula=" + cedula,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: '200',
            width: '878',
            colNames: ['Cedula', 'Cliente', 'Neg', 'NrCuotas', 'Fecha Negocio', 'Estado', 'Convenio', 'Idconvenio', 'Afiliado', 'Tipo', 'Valor Neg', 'Saldo Cartera'],
            colModel: [
                {name: 'valor_01', index: 'valor_01', width: 70, align: 'right'},
                {name: 'valor_02', index: 'valor_02', sortable: true, width: 170, align: 'center'},
                {name: 'valor_03', index: 'valor_03', sortable: true, width: 70, align: 'center', key: true},
                {name: 'valor_09', index: 'valor_09', width: 75, align: 'center', hidden: true},
                {name: 'valor_04', index: 'valor_04', sortable: false, width: 110, align: 'center'},
                {name: 'valor_05', index: 'valor_05', width: 100, align: 'center', hidden: false},
                {name: 'valor_06', index: 'valor_06', width: 100, align: 'center', hidden: true},
                {name: 'valor_07', index: 'valor_07', width: 90, align: 'center', hidden: false},
                {name: 'valor_11', index: 'valor_11', width: 90, align: 'center', hidden: true},
                {name: 'valor_12', index: 'valor_12', width: 90, align: 'center', hidden: true},
                {name: 'valor_08', index: 'valor_08', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_10', index: 'valor_10', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 50,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, loadComplete: function () {

                if (grid_listar_negocios.jqGrid('getGridParam', 'records') > 0) {
                    cacularTotalesListarNegocios(grid_listar_negocios);

                } else {
                    mensajesDelSistema("Lo sentimos ne se encontraron resultados.<br/> Verifique los datos de entrada.", "330", "150");
                    grid_listar_negocios.jqGrid("clearGridData", true).trigger("reloadGrid");
                }
            }, beforeSelectRow: function (rowid, e) {
                var tipo = grid_listar_negocios.jqGrid('getCell', rowid, 'valor_12');

                if (tipo === 'NEGOCIO_SEGURO' || tipo === 'NEGOCIO_GPS') {

                    var allRowsInGrid = grid_listar_negocios.jqGrid('getRowData');
                    for (var i = 0; i < allRowsInGrid.length; i++) {
                        var tipo1 = allRowsInGrid[i].valor_12;
                        var ids = allRowsInGrid[i].valor_03;
                        if ((tipo1 === 'NEGOCIO_SEGURO') && tipo !== 'NEGOCIO_GPS' && ids !== rowid) {
                            jQuery("#tabla_negocios_clientes").jqGrid('setSelection', ids);
                        }
                        if (tipo1 === 'NEGOCIO_GPS' && tipo === 'NEGOCIO_GPS' && ids !== rowid) {
                            jQuery("#tabla_negocios_clientes").jqGrid('setSelection', ids);
                        }
                    }

                    return true;
                } else if (tipo === 'NEGOCIO_AVAL' || tipo === 'NEGOCIO_PADRE'|| tipo === 'NEGOCIO_EDUCATIVO') {
                    
                    var allRowsInGrid = grid_listar_negocios.jqGrid('getRowData');
                    var convenioSelect = grid_listar_negocios.getRowData(rowid).valor_07;

                    if ($("#idconvenio").val() !== '16' && $("#idconvenio").val() !== '30') {
                        $("#idconvenio").val(convenioSelect);
                        $("#afiliado").val(grid_listar_negocios.getRowData(rowid).valor_11);
                    }

                    for (var i = 0; i < allRowsInGrid.length; i++) {
                        var tipo2 = allRowsInGrid[i].valor_12;
                        var ids = allRowsInGrid[i].valor_03;
                        var convenio=allRowsInGrid[i].valor_07;                   
                       
                        if ((tipo2 === 'NEGOCIO_AVAL' || tipo2 === 'NEGOCIO_PADRE'|| tipo === 'NEGOCIO_EDUCATIVO') && convenioSelect===convenio && ids !== rowid) {
                            jQuery("#tabla_negocios_clientes").jqGrid('setSelection', ids);
                        }
                    }

                    return true;
                }
            }, onSelectAll: function () {
                $("#totalItem").val(0);    
                
                var allRowsInGrid = grid_listar_negocios.jqGrid('getRowData');
                  for (var i = 0; i < allRowsInGrid.length; i++) {
                        var tipo2 = allRowsInGrid[i].valor_12;
                        var ids = allRowsInGrid[i].valor_03;                        
                        if ((tipo2 === 'NEGOCIO_AVAL' || tipo2 === 'NEGOCIO_PADRE'|| tipo2 === 'NEGOCIO_EDUCATIVO')) {
                            if ($("#idconvenio").val() !== '16' && $("#idconvenio").val() !== '30') {
                                $("#idconvenio").val(allRowsInGrid[i].valor_07);
                                $("#afiliado").val(allRowsInGrid[i].valor_11);
                            }
                        }
                    }

            }, loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });
    }

}

function reloadGridListarNegocios(grid_listar_negocios, cedula, negocio) {
    grid_listar_negocios.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=1&cedula=" + cedula
    });

    grid_listar_negocios.trigger("reloadGrid");
}

function cacularTotalesListarNegocios(grid_listar_negocios) {

    var totalItem = grid_listar_negocios.jqGrid('getCol', 'valor_10', false, 'sum');
    var totalItem2 = grid_listar_negocios.jqGrid('getCol', 'valor_08', false, 'sum');
    grid_listar_negocios.jqGrid('footerData', 'set', {
        valor_05: 'Total:',
        valor_08: totalItem2,
        valor_10: totalItem

    });

}

function calcularSaldoNegocios() {
   // loading("Espere un momento por favor...", "270", "140");
    var selectRows;
    var timeOut = 0;
    var neg_negocio = "";
    var neg_seguro = "";
    var neg_gps = "";
    var checkNegocio = false;
    var checkSeguro = false;
    var checkGPS = false;
   
    $("#divSaldoNegocio").show();
   
    selectRows = jQuery("#tabla_negocios_clientes").jqGrid('getGridParam', 'selarrrow');

    for (var i = 0; i < selectRows.length; i++) {
        var negocio = selectRows[i];

        var cbIsChecked = jQuery("#jqg_tabla_negocios_clientes_" + negocio).attr('checked');

        var tipo = jQuery("#tabla_negocios_clientes").jqGrid('getCell', selectRows[i], 'valor_12');

        if ((tipo === 'NEGOCIO_PADRE' || tipo === 'NEGOCIO_AVAL' || tipo === 'NEGOCIO_EDUCATIVO') && cbIsChecked) {
            checkNegocio = true;
            neg_negocio += negocio + ";";

        }

        if ((negocio.substring(0, 2) !== 'NG' && tipo === 'NEGOCIO_SEGURO') && cbIsChecked) {
            checkSeguro = true;
            neg_seguro += negocio + ";";
        }
        if (tipo === 'NEGOCIO_GPS' && cbIsChecked) {
            checkGPS = true;
            neg_gps += negocio + ";";
        }
        if (negocio.substring(0, 2) === 'NG') {      
            
            checkSeguro = true;
            neg_seguro += negocio + ";";

        }
    }

    //esto es para los negocios de seguro..
    if (checkNegocio) {
        $('#tabla_saldos_negocio_padre').jqGrid('GridUnload');
        saldosNegociosTipo("tabla_saldos_negocio_padre", neg_negocio, "FACTURAS PENDIENTES NEGOCIO PRINCIPAL", 3);
    }
    if (checkGPS) {
        $('#tabla_saldos_negocio_gps').jqGrid('GridUnload');
        saldosNegociosTipo("tabla_saldos_negocio_gps", neg_gps, "FACTURAS PENDIENTES NEGOCIO GPS", 3);
    }

    if (checkSeguro) {
        $('#tabla_saldos_negocio_seguro').jqGrid('GridUnload');
         saldosNegociosTipo("tabla_saldos_negocio_seguro", neg_seguro, "FACTURAS PENDIENTES NEGOCIO SEGURO", 3);
    }

    jQuery("#tabla_negocios_clientes").trigger("reloadGrid");

    sumartablas("resumenSaldos");
    sumarSubtotal("resumenSaldos");
    sumartablas("tablaPagoInicial");

   // closeLoading();

}

function saldosNegociosTipo(grid, negocio, captions, option) {


    var grid_facturas_negocio = jQuery("#" + grid);

    if ($("#gview_" + grid).length) {
        reloadGridFacturasNegocio(grid_facturas_negocio, negocio, option);
    } else {
        grid_facturas_negocio.jqGrid({
            caption: captions,
            url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=" + option + "&codneg=" + negocio,
            async: false,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: 'auto',
            width: '1252',
            colNames: ['Factura', 'Negocio', 'Cuota', 'Fecha Vencimiento', 'Dias Mora', 'Estado', 'Tipo Neg', 'Valor Factura', 'Valor Abono', 'Valor Saldo', 'Saldo Capital', 'Saldo Interes', 'IntxMora', 'GAC', 'Total Item'],
            colModel: [
                {name: 'documento', index: 'documento', width: 80, align: 'center'},
                {name: 'cod_neg', index: 'cod_neg', sortable: true, width: 70, align: 'center'},
                {name: 'cuota', index: 'cuota', sortable: true, width: 70, align: 'center', key: true},
                {name: 'fecha_vencimiento', index: 'fecha_vencimiento', sortable: false, width: 120, align: 'center'},
                {name: 'dias_mora', index: 'dias_mora', width: 80, align: 'center'},
                {name: 'estado', index: 'estado', width: 95, align: 'center'},
                {name: 'tipo_negocio', index: 'tipo_negocio', width: 95, align: 'center', hidden: true},
                {name: 'valor_factura', index: 'valor_factura', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                    , summaryType: sumValorFactura},
                {name: 'valor_abono', index: 'valor_abono', sortable: true, width: 95, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                    , summaryType: sumValorAbono},
                {name: 'valor_saldo', index: 'valor_saldo', width: 95, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                    , summaryType: sumValorSaldo},
                {name: 'saldo_capital', index: 'saldo_capital', sortable: false, width: 95, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                    , summaryType: sumSaldoCapital},
                {name: 'saldo_interes', index: 'saldo_interes', sortable: false, width: 95, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                    , summaryType: sumSaldoInteres},
                {name: 'interes_mora', index: 'interes_mora', sortable: false, width: 95, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                    , summaryType: sumInteresMora},
                {name: 'gasto_cobranza', index: 'gasto_cobranza', sortable: false, width: 95, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                    , summaryType: sumGaC},
                {name: 'total_item', index: 'total_item', sortable: false, width: 95, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                    , summaryType: sumTotalItems}
            ],
            rowNum: 300,
            rowTotal: 1000,
            // pager: ('#page_detalles_cartera'),
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rowTotals: true,
            colTotals: true,
            rownumbers: true,
            grouping: true,
           
            
            groupingView: {
                groupField: ['cod_neg', 'estado'],
                groupSummary: [false, true],
                groupColumnShow: [true, false],
                groupText: ['<b>{0}</b>', '<b>{0}, suma total: {total_item}</b>'],
                groupCollapse: false,
                groupOrder: ['asc', 'desc'],
                groupDataSorted: true

            }, jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {async: false},
            loadComplete: function () {
                cacularTotalesFacturasNegocio(grid_facturas_negocio, negocio);
                $("#divSimulador").css("display", "block");
            }, loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });

    }
}

;
function sumValorFactura(val, name, record) {
    if (record['estado'] === 'VENCIDO') {
        var valor = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor;
    } else {
        var valor2 = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor2;
    }
}

function sumValorAbono(val, name, record) {
    if (record['estado'] === 'VENCIDO') {
        var valor = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor;
    } else {
        var valor2 = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor2;
    }
}

function sumValorSaldo(val, name, record) {
    if (record['estado'] === 'VENCIDO') {
        var valor = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor;
    } else {
        var valor2 = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor2;
    }
}

function sumSaldoCapital(val, name, record) {
    if (record['estado'] === 'VENCIDO') {
        var valor = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor;
    } else {
        var valor2 = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor2;
    }
}

function sumSaldoInteres(val, name, record) {
    if (record['estado'] === 'VENCIDO') {
        var valor = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor;
    } else {
        var valor2 = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor2;
    }
}

function sumInteresMora(val, name, record) {
    if (record['estado'] === 'VENCIDO') {
        var valor = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor;
    } else {
        var valor2 = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor2;
    }
}

function sumGaC(val, name, record) {
    if (record['estado'] === 'VENCIDO') {
        var valor = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor;
    } else {
        var valor2 = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor2;
    }
}
function sumTotalItems(val, name, record) {

    if (record['estado'] === 'VENCIDO') {
        var valor = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor;
    } else {
        var valor2 = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor2;
    }
}

function reloadGridFacturasNegocio(grid_facturas_negocio, negocio, option) {
    grid_facturas_negocio.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=" + option + "&codneg=" + negocio
    });
    grid_facturas_negocio.jqGrid('groupingGroupBy', ['cod_neg', 'estado'], {
        groupSummary: [false, true],
        groupColumnShow: [true, false],
        groupText: ['<b>{0}</b>', '<b>{0}, suma total: {total_item}</b>'],
        groupCollapse: false,
        groupOrder: ['asc', 'desc']
    });
    grid_facturas_negocio.trigger("reloadGrid");
}

function agregarFilaResumenVencido(listaNegocios, estado_cartera, tipo_neg, sumCapitalVencido, suminteresVencido, sumItnxMoraVencido, sumGacVencido, sumItemsVencido, i, style) {
    var tds = "<tr id='fila_" + listaNegocios + "_" + i + "' class='" + style + "'>";
    tds += "<td style='text-align: center'><span>" + listaNegocios + "<span></td>";
    tds += "<td style='text-align: center'>" + estado_cartera + "</td>";
    tds += "<td id='tipoNegocio_" + listaNegocios + "_" + i + "' style='text-align: center'>" + tipo_neg + "</td>";
    tds += "<td id='saldoCapital_" + listaNegocios + "_" + i + "'>" + numberConComas(sumCapitalVencido) + "</td>";
    tds += "<td id='saldoInteres_" + listaNegocios + "_" + i + "'>" + numberConComas(suminteresVencido) + "</td>";
    tds += "<td id='saldoIntxMora_" + listaNegocios + "_" + i + "'>" + numberConComas(sumItnxMoraVencido) + "</td>";
    tds += "<td id='saldoGac_" + listaNegocios + "_" + i + "'>" + numberConComas(sumGacVencido) + "</td>";
    tds += "<td class='subtotales' >" + numberConComas(sumItemsVencido) + "</td>";

    tds += "<td><input min='0' max='100' value='0' class='descuento' \n\
            onkeyup='aplicarDescuentosEnter(this.id, \"" + listaNegocios + "_" + i + "\",\"CAPITAL\",event);' \n\
            type='number' style='width: 70px' id='dtoCapital_" + listaNegocios + "_" + i + "' name='dtoCapital_" + listaNegocios + "_" + i + "'/></td>";

    tds += "<td><input min='0' max='100' value='0' class='descuento' \n\
            onkeyup='aplicarDescuentosEnter(this.id, \"" + listaNegocios + "_" + i + "\",\"INTERES\",event);' \n\
            type='number' style='width: 70px' id='dtoInteres_" + listaNegocios + "_" + i + "' name='dtoInteres_" + listaNegocios + "_" + i + "' /></td>";

    tds += "<td><input min='0' max='100' value='0' class='descuento' \n\
             onkeyup='aplicarDescuentosEnter(this.id, \"" + listaNegocios + "_" + i + "\",\"INTXMORA\",event);' \n\
             type='number' style='width: 70px' id='dtointxMora_" + listaNegocios + "_" + i + "' name='dtointxMora_" + listaNegocios + "_" + i + "'/></td>";

    tds += "<td><input min='0' max='100' value='0' class='descuento'  \n\
            onkeyup='aplicarDescuentosEnter(this.id, \"" + listaNegocios + "_" + i + "\",\"GAC\",event);' \n\
            type='number' style='width: 70px' id='dtogac_" + listaNegocios + "_" + i + "' name='dtogac_" + listaNegocios + "_" + i + "' /></td>";

    tds += "<td><input class='inputTable' value='0' style='width: 70px' id='dtoGeneral_" + listaNegocios + "_" + i + "' name='dtoGeneral_" + listaNegocios + "_" + i + "'/></td>";
    tds += "<td id='total_" + listaNegocios + "_" + i + "' class='rowDataSd'>" + numberConComas(sumItemsVencido) + "</td>";
    tds += '</tr>';

    $("#resumenSaldos").append(tds);

    var inputs = "<input type='hidden' value='0' name='descuentoCapital_" + listaNegocios + "_" + i + "' id='descuentoCapital_" + listaNegocios + "_" + i + "' />";
    inputs += "<input type='hidden' value='0' name='descuentoInteres_" + listaNegocios + "_" + i + "' id='descuentoInteres_" + listaNegocios + "_" + i + "' />";
    inputs += "<input type='hidden' value='0' name='descuentoIntxMora_" + listaNegocios + "_" + i + "' id='descuentoIntxMora_" + listaNegocios + "_" + i + "'/>";
    inputs += "<input type='hidden' value='0' name='descuentoGac_" + listaNegocios + "_" + i + "' id='descuentoGac_" + listaNegocios + "_" + i + "'/>";

    $("#camposOcultos").append(inputs);
}



function agregarFilaResumenCorriente(listaNegocios, estado_cartera,tipo_neg, sumCapitalVencido, suminteresVencido, sumItnxMoraVencido, sumGacVencido, sumItemsVencido, i, style) {
    var tds = "<tr id='fila_" + listaNegocios + "_" + i + "' class='" + style + "'>";
    tds += "<td style='text-align: center'><span>" + listaNegocios + "<span></td>";
    tds += "<td style='text-align: center'>" + estado_cartera + "</td>";
    tds += "<td style='text-align: center'>" + tipo_neg + "</td>";
    tds += "<td id='saldoCapital_" + listaNegocios + "_" + i + "'>" + numberConComas(sumCapitalVencido) + "</td>";
    tds += "<td id='saldoInteres_" + listaNegocios + "_" + i + "'>" + numberConComas(suminteresVencido) + "</td>";
    tds += "<td id='saldoIntxMora_" + listaNegocios + "_" + i + "'>" + numberConComas(sumItnxMoraVencido) + "</td>";
    tds += "<td id='saldoGac_" + listaNegocios + "_" + i + "'>" + numberConComas(sumGacVencido) + "</td>";
    tds += "<td class='subtotales'>" + numberConComas(sumItemsVencido) + "</td>";
    tds += "<td>0</td>";
    tds += "<td>0</td>";
    tds += "<td>0</td>";
    tds += "<td>0</td>";
    tds += "<td><input class='inputTable' value='0' readonly='true' style='width: 70px' id='dtoGeneral_" + listaNegocios + "_" + i + "' name='dtoGeneral_" + listaNegocios + "_" + i + "'/></td>";
    tds += "<td id='total_" + listaNegocios + "_" + i + "' class='rowDataSd'>" + numberConComas(sumItemsVencido) + "</td>";


    tds += '</tr>';

    $("#resumenSaldos").append(tds);

}

function agregarFilaPagoInicial(listaNegocios,tipo_neg, sumCapitalVencido, suminteresVencido, sumItnxMoraVencido, sumGacVencido, sumItemsVencido, i, porcentaje, saldo,corriente) {
    var tds = "<tr id='fila_pago_" + listaNegocios + "_" + i + "' class='miTd' >";
    tds += "<td style='text-align: center'><span>" + listaNegocios + "<span></td>";
    tds += "<td id='tipoInicial_" + listaNegocios + "_" + i + "' style='text-align: center'>" + tipo_neg + "</td>";
    tds += "<td id='capitalInicial_" + listaNegocios + "_" + i + "'>" + numberConComas(sumCapitalVencido) + "</td>";
    tds += "<td id='interesInicial_" + listaNegocios + "_" + i + "'>" + numberConComas(suminteresVencido) + "</td>";
    tds += "<td id='intxMoraInicial_" + listaNegocios + "_" + i + "'>" + numberConComas(sumItnxMoraVencido) + "</td>";
    tds += "<td id='gacInicial_" + listaNegocios + "_" + i + "'>" + numberConComas(sumGacVencido) + "</td>";
    tds += "<td id='total_inicial_" + listaNegocios + "_" + i + "' class='rowDataSd' >" + numberConComas(sumItemsVencido) + "</td>";
    tds += "<td contenteditable='true' class='descuento' id='pct_a_pagar_" + listaNegocios + "_" + i + "' \n\\n\
             onclick='guardarvalor(this.id);'   \n\\n\
             onkeypress ='aplicarDescuentosEnter2(this.id, \"" + listaNegocios + "_" + i + "\",\"CI\",event);' >" + porcentaje + "</td>";
    tds += "<td id='ValorPagar_" + listaNegocios + "_" + i + "' class='rowValor' >" + numberConComas(saldo) + "</td>";
    tds += "<td id='SaldoVencido_" + listaNegocios + "_" + i + "' class='rowSaldo' >" +numberConComas(sumItemsVencido-saldo) + "</td>";
    tds += "<td id='SaldoCorriente_" + listaNegocios + "_" + i + "' class='rowSaldoCorriente' >" +numberConComas(corriente) + "</td>";
    tds += '</tr>';

    $("#tablaPagoInicial").append(tds);

}

function aplicarDescuentosEnter(id, negocio, tipo_descuento, e) {
    var tipo_neg=$("#tipoNegocio_"+negocio).html();
    var porcentaje=parseInt($("#" + id).val());
    if (e.keyCode === 13) {
         if(validarDescuentos(tipo_descuento,porcentaje,tipo_neg,"N")===true){
            aplicarDescuentos(id, negocio, tipo_descuento);
        } else {
            mensajesDelSistema("Porcenteje de descuento no autorizado", "300", "150");
            $("#" + id).val(0);
            $("#dtoGeneral_"+negocio).val(0);
        }
    }

}


function aplicarDescuentosEnter2(id, negocio, tipo_descuento, e) {
    if (e.keyCode === 13) {
       
       var result = $("#" + id).html().replace(/(<br>)*/g, '');
       $("#" + id).html(result);
        if (parseInt(result) <= 100) {
            aplicarDescuentos2(id, negocio);
        } else {
            mensajesDelSistema("Porcenteje de descuento no autorizado", "300", "150");

        }
        e.preventDefault();
    }

}


function aplicarDescuentos(id, negocio, tipo_descuento) {

    var base = 0;
    var porcentaje = 0;
    var descuento = 0;
    var capital = parseInt($("#saldoCapital_" + negocio).html().replace(/\,/g, ""));
    var interes = parseInt($("#saldoInteres_" + negocio).html().replace(/\,/g, ""));
    var intXmora = parseInt($("#saldoIntxMora_" + negocio).html().replace(/\,/g, ""));
    var saldoGac = parseInt($("#saldoGac_" + negocio).html().replace(/\,/g, ""));
    var capitalInicial = 0;
    var interesInicial = 0;
    var intXmoraInicial = 0;
    var gacInicial = 0;

    var total = 0;

    if (tipo_descuento === 'CAPITAL') {
        base = capital;
        porcentaje = parseInt($("#" + id).val());       
        descuento = Math.round(base * (porcentaje / 100));
        $("#descuentoCapital_" + negocio).val(descuento);
        //descuento saldo iniciales.
        capitalInicial = capital - descuento;
        $("#capitalInicial_" + negocio).html(numberConComas(capitalInicial));             
     
    }
    if (tipo_descuento === 'INTERES') {
        base = interes;
        porcentaje = parseInt($("#" + id).val());
        descuento = Math.round(base * (porcentaje / 100));
        $("#descuentoInteres_" + negocio).val(descuento);
        //descuento saldo iniciales.
        interesInicial = interes - descuento;
        $("#interesInicial_" + negocio).html(numberConComas(numberConComas(interesInicial)));
    }
    if (tipo_descuento === 'INTXMORA') {
        base = intXmora;
        porcentaje = parseInt($("#" + id).val());
        descuento = Math.round(base * (porcentaje / 100));
        $("#descuentoIntxMora_" + negocio).val(descuento);
        //descuento saldo iniciales.
        intXmoraInicial = intXmora - descuento;
        $("#intxMoraInicial_" + negocio).html(numberConComas(intXmoraInicial));

    }
    if (tipo_descuento === 'GAC') {
        base = saldoGac;
        porcentaje = parseInt($("#" + id).val());
        descuento = Math.round(base * (porcentaje / 100));
        $("#descuentoGac_" + negocio).val(descuento);
        //descuento saldo iniciales.
        gacInicial = saldoGac - descuento;
        $("#gacInicial_" + negocio).html(numberConComas(gacInicial));
    }

    var cap = parseInt($("#descuentoCapital_" + negocio).val());
    var inte = parseInt($("#descuentoInteres_" + negocio).val());
    var inteMora = parseInt($("#descuentoIntxMora_" + negocio).val());
    var gasto_cobranza = parseInt($("#descuentoGac_" + negocio).val());
    var descuentoTotal = cap + inte + inteMora + gasto_cobranza;

    total = (capital + interes + intXmora + saldoGac) - descuentoTotal;

    $("#dtoGeneral_" + negocio).val(numberConComas(descuentoTotal));
    $("#total_" + negocio).html(numberConComas(total));
  
    //descuento total saldo inicial.
    calcularTotalInicialxNegocio(negocio);
   
    var id2="pct_a_pagar_"+negocio;
    aplicarDescuentos2(id2, negocio);    
    
    sumartablas("tablaPagoInicial");
    sumartablas("resumenSaldos");


}

function calcularTotalInicialxNegocio(negocio){    
    var capital = parseInt($("#capitalInicial_" + negocio).html().replace(/\,/g, ""));
    var interes = parseInt($("#interesInicial_" + negocio).html().replace(/\,/g, ""));
    var intXmora = parseInt($("#intxMoraInicial_" + negocio).html().replace(/\,/g, ""));
    var Gac = parseInt($("#gacInicial_" + negocio).html().replace(/\,/g, ""));
    var total_inicial=capital+interes+intXmora+Gac;
     $("#total_inicial_" + negocio).html(numberConComas(total_inicial));
}

function guardarvalor(id){
    var result = $("#" + id).html().replace(/(<br>)*/g, '');
    $("#porcentajeDescuento").val(result);
}
var estadoDescuento=false;
function aplicarDescuentos2(id, negocio) {
    estadoDescuento=false;
    var base = 0;
    var porcentaje = parseInt($("#" + id).html());
    var descuentoCapital = 0;
    var descuentoInteres = 0;
    var descuentoIxmora = 0;
    var descuentoGac = 0;
    var totaldescuentos=0;
    var capital = parseInt($("#capitalInicial_" + negocio).html().replace(/\,/g, ""));
    var interes = parseInt($("#interesInicial_" + negocio).html().replace(/\,/g, ""));
    var intXmora = parseInt($("#intxMoraInicial_" + negocio).html().replace(/\,/g, ""));
    var Gac = parseInt($("#gacInicial_" + negocio).html().replace(/\,/g, ""));
    var total_inicial= parseInt($("#total_inicial_" + negocio).html().replace(/\,/g, ""));
    var tipo_neg=$("#tipoInicial_"+negocio).html();
    
 
    
    if(validarDescuentos("CAP",porcentaje,tipo_neg,"S")===true){
         descuentoCapital = Math.round(capital * (porcentaje / 100));
    }else{
        descuentoCapital = capital;
    }
     if(validarDescuentos("INT",porcentaje,tipo_neg,"S")===true){
         descuentoInteres = Math.round(interes * (porcentaje / 100));
    }else{
        descuentoInteres=interes;
    }
     if(validarDescuentos("IXM",porcentaje,tipo_neg,"S")===true){
         descuentoIxmora = Math.round(intXmora * (porcentaje / 100));
    }else{
        descuentoIxmora=intXmora;
    }
     if(validarDescuentos("GAC",porcentaje,tipo_neg,"S")===true){
         descuentoGac = Math.round(Gac * (porcentaje / 100));
    }else{
        descuentoGac=Gac;
    }
      
    totaldescuentos = descuentoCapital + descuentoInteres + descuentoIxmora + descuentoGac;
    if(this.estadoDescuento!==true){  
        $("#ValorPagar_" + negocio).html(numberConComas(totaldescuentos));
        //descuento saldo iniciales.
        total_inicial=total_inicial-totaldescuentos;      
        $("#SaldoVencido_" + negocio).html(numberConComas(total_inicial));
        if (total_inicial === 0) {            
             $("#porcentajeDescuento").val(100);
             $("#" + id).html($("#porcentajeDescuento").val());   
        } else {
            $("#porcentajeDescuento").val(porcentaje);
        }
    }else{
        $("#" + id).html($("#porcentajeDescuento").val());         
         mensajesDelSistema("Porcenteje de descuento no autorizado", "300", "150");        
    }
    sumartablas("tablaPagoInicial");

}


function sumartablas(nameTable) {

    var $dataRows = $("#" + nameTable + " tr");
    var total = 0;
    $dataRows.each(function () {
        $(this).find('.rowDataSd').each(function (i) {
            total += parseInt($(this).html().replace(/\,/g, ""));
        });
    });

    if (nameTable === "resumenSaldos") {
        $("#totalResumen").val(numberConComas(total));
       
    }
    if (nameTable === "tablaPagoInicial") {
        $("#totalResumenPagoInicial").val(numberConComas(total));
        var total1 = 0;
        $dataRows.each(function () {
            $(this).find('.rowValor').each(function (i) {
                total1 += parseInt($(this).html().replace(/\,/g, ""));
            });
        });
        $("#totalaPagar").val(numberConComas(total1));
       
        var total2 = 0;
        $dataRows.each(function () {
            $(this).find('.rowSaldo').each(function (i) {
                total2 += parseInt($(this).html().replace(/\,/g, ""));
            });
        });
        $("#totalsaldoVencidoRee").val(numberConComas(total2));
        
        var total3 = 0;
        $dataRows.each(function () {
            $(this).find('.rowSaldoCorriente').each(function (i) {
                total3 += parseInt($(this).html().replace(/\,/g, ""));
            });
        });
        
        $("#totalsaldoCorrienteRee").val(numberConComas(total3));
        
        //calculamos el valor final
        var valor_reestructuracion=total2+total3;
        $("#totalReestructuracion").html(numberConComas(valor_reestructuracion));
        $("#totalItem").val(valor_reestructuracion);
    }

}

function sumarSubtotal(nameTable) {

    var $dataRows = $("#" + nameTable + " tr");
    var total = 0;
    $dataRows.each(function () {
        $(this).find('.subtotales').each(function (i) {
            total += parseInt($(this).html().replace(/\,/g, ""));
        });
    });

    if (nameTable === "resumenSaldos") {
        $("#totalSubResumen").val(numberConComas(total));
    }
}

function cacularTotalesFacturasNegocio(grid_facturas_negocio, misNegocios) {

    var allRowsInGrid = grid_facturas_negocio.jqGrid('getRowData');
    //variables vencidas
    var sumCapitalVencido = 0;
    var suminteresVencido = 0;
    var sumItnxMoraVencido = 0;
    var sumGacVencido = 0;
    var sumItemsVencido = 0;
    var estado_cartera_vencido = 0;
    //variables corrientes
    var sumCapital = 0;
    var suminteres = 0;
    var sumItnxMora = 0;
    var sumGac = 0;
    var sumItems = 0;
    var estado_cartera = "";
    var porcentaje = 0;
    var saldo=0;
    var saldoCorriente=0;
    var vldti="";
    var status_corriente = false;
    var status_vencido = false;
    var listaNegocios = allRowsInGrid[0].cod_neg;
    var tipo_neg = allRowsInGrid[0].tipo_negocio;
    


    if ($('#resumenSaldos >tbody >tr').length > 1) {
        var lastrow = $('#resumenSaldos >tbody >tr:last').attr("id");
        $("#" + lastrow).attr('class', 'miTd3');
    }
    for (var i = 0; i < allRowsInGrid.length; i++) {
        var negocio = allRowsInGrid[i].cod_neg;
        DeleteRowOfProductTable(negocio);
        DeleteRowOfProductTableInicial(negocio);
        if (listaNegocios === negocio) {
            if (allRowsInGrid[i].estado === 'VENCIDO') {
                status_vencido = true;
                estado_cartera_vencido = allRowsInGrid[i].estado;
                sumCapitalVencido = parseInt(sumCapitalVencido) + parseInt(allRowsInGrid[i].saldo_capital);
                suminteresVencido = parseInt(suminteresVencido) + parseInt(allRowsInGrid[i].saldo_interes);
                sumItnxMoraVencido = parseInt(sumItnxMoraVencido) + parseInt(allRowsInGrid[i].interes_mora);
                sumGacVencido = parseInt(sumGacVencido) + parseInt(allRowsInGrid[i].gasto_cobranza);
                sumItemsVencido = parseInt(sumItemsVencido) + parseInt(allRowsInGrid[i].total_item);
            } else {
                status_corriente = true;
                estado_cartera = allRowsInGrid[i].estado;
                sumCapital = parseInt(sumCapital) + parseInt(allRowsInGrid[i].saldo_capital);
                suminteres = parseInt(suminteres) + parseInt(allRowsInGrid[i].saldo_interes);
                sumItnxMora = parseInt(sumItnxMora) + parseInt(allRowsInGrid[i].interes_mora);
                sumGac = parseInt(sumGac) + parseInt(allRowsInGrid[i].gasto_cobranza);
                sumItems = parseInt(sumItems) + parseInt(allRowsInGrid[i].total_item);
            }

        } else {
           //alert("1:"+tipo_neg.replace("NEGOCIO_", ""));
            vldti= validarTablaInicial(tipo_neg.replace("NEGOCIO_", "")); 
            if (status_vencido) {
                if (status_corriente) {                    
                    agregarFilaResumenVencido(listaNegocios, estado_cartera_vencido, tipo_neg.replace("NEGOCIO_", ""), sumCapitalVencido, suminteresVencido, sumItnxMoraVencido, sumGacVencido, sumItemsVencido, 1, "miTd");
                    //Aqui validamos las configuraciones
                    if(vldti==="OK"){
                        sumCapitalVencido = sumCapitalVencido + sumCapital;
                        suminteresVencido = suminteresVencido + suminteres;
                        sumItemsVencido = sumItemsVencido + sumItems;
                    }
                } else {
                    agregarFilaResumenVencido(listaNegocios, estado_cartera_vencido, tipo_neg.replace("NEGOCIO_", ""), sumCapitalVencido, suminteresVencido, sumItnxMoraVencido, sumGacVencido, sumItemsVencido, 1, "miTd2");
                }
                              
                if (vldti === "OK") {
                    porcentaje = 100;
                    saldo = sumItemsVencido;
                    saldoCorriente = 0;
                } else {
                    porcentaje = 100;
                    var split = vldti.split(";");
                    saldo = 0;

                    saldo=sumCapitalVencido+suminteresVencido+sumItnxMoraVencido+sumGacVencido;
                    saldoCorriente = sumItems;
                }
                
                
                agregarFilaPagoInicial(listaNegocios, tipo_neg.replace("NEGOCIO_", ""), sumCapitalVencido, suminteresVencido, sumItnxMoraVencido, sumGacVencido, sumItemsVencido, 1, porcentaje, saldo, saldoCorriente);
                  
            }

            if (status_corriente) {
                porcentaje = 0;
                agregarFilaResumenCorriente(listaNegocios, estado_cartera, tipo_neg.replace("NEGOCIO_", ""), sumCapital, suminteres, sumItnxMora, sumGac, sumItems, 2, "miTd2");
        
                if (vldti === "OK" && status_vencido === false) {
                    agregarFilaPagoInicial(listaNegocios, tipo_neg.replace("NEGOCIO_", ""), sumCapital, suminteres, sumItnxMora, sumGac, sumItems, 2, 100, sumItems, 0);
                } else if (status_vencido === false) {//agregar negocios corrientes sin pago total
                    agregarFilaPagoInicial(listaNegocios, tipo_neg.replace("NEGOCIO_", ""), sumCapital, suminteres, sumItnxMora, sumGac, sumItems, 2, 100, sumItems, 0);
                }

            }

            sumCapitalVencido = 0;
            suminteresVencido = 0;
            sumItnxMoraVencido = 0;
            sumGacVencido = 0;
            sumItemsVencido = 0;
            status_corriente = false;
            status_vencido = false;
            sumCapital = 0;
            suminteres = 0;
            sumItnxMora = 0;
            sumGac = 0;
            sumItems = 0;

            if (allRowsInGrid[i].estado === 'VENCIDO') {
                status_vencido = true;
                estado_cartera_vencido = allRowsInGrid[i].estado;
                sumCapitalVencido = parseInt(sumCapitalVencido) + parseInt(allRowsInGrid[i].saldo_capital);
                suminteresVencido = parseInt(suminteresVencido) + parseInt(allRowsInGrid[i].saldo_interes);
                sumItnxMoraVencido = parseInt(sumItnxMoraVencido) + parseInt(allRowsInGrid[i].interes_mora);
                sumGacVencido = parseInt(sumGacVencido) + parseInt(allRowsInGrid[i].gasto_cobranza);
                sumItemsVencido = parseInt(sumItemsVencido) + parseInt(allRowsInGrid[i].total_item);
            } else {
                status_corriente = true;
                estado_cartera = allRowsInGrid[i].estado;
                sumCapital = parseInt(sumCapital) + parseInt(allRowsInGrid[i].saldo_capital);
                suminteres = parseInt(suminteres) + parseInt(allRowsInGrid[i].saldo_interes);
                sumItnxMora = parseInt(sumItnxMora) + parseInt(allRowsInGrid[i].interes_mora);
                sumGac = parseInt(sumGac) + parseInt(allRowsInGrid[i].gasto_cobranza);
                sumItems = parseInt(sumItems) + parseInt(allRowsInGrid[i].total_item);
            }
            tipo_neg = allRowsInGrid[i].tipo_negocio;
            listaNegocios = negocio;
        }
    }
 
    vldti= validarTablaInicial(tipo_neg.replace("NEGOCIO_", "")); 
    if (status_vencido) {
        agregarFilaResumenVencido(listaNegocios, estado_cartera_vencido, tipo_neg.replace("NEGOCIO_", ""), sumCapitalVencido, suminteresVencido, sumItnxMoraVencido, sumGacVencido, sumItemsVencido, 1, "miTd");

        //Aqui validamos las configuraciones
        if (status_corriente && vldti === "OK") {
        //    alert("fuera del for: "+tipo_neg.replace("NEGOCIO_", ""));
            porcentaje = 100;
            saldo = sumItemsVencido + sumItems;
            sumCapitalVencido = sumCapitalVencido + sumCapital;
            suminteresVencido = suminteresVencido + suminteres;
            sumItemsVencido = sumItemsVencido + sumItems;
        
        } else if (vldti === "OK") {
            porcentaje = 100;
            saldo = sumItemsVencido;
            saldoCorriente = 0;
        } else {
            porcentaje = 100;
            var split = vldti.split(";");
            saldo = 0;

            saldo=sumCapitalVencido+suminteresVencido+sumItnxMoraVencido+sumGacVencido;
            saldoCorriente = sumItems;
               
        }
         agregarFilaPagoInicial(listaNegocios, tipo_neg.replace("NEGOCIO_", ""), sumCapitalVencido, suminteresVencido, sumItnxMoraVencido, sumGacVencido, sumItemsVencido, 1, porcentaje,saldo,saldoCorriente);
          
    }    

    if (status_corriente) {
        agregarFilaResumenCorriente(listaNegocios, estado_cartera, tipo_neg.replace("NEGOCIO_", ""), sumCapital, suminteres, sumItnxMora, sumGac, sumItems, 2, "miTd");

        if(vldti==="OK" && status_vencido ===false){
            agregarFilaPagoInicial(listaNegocios, tipo_neg.replace("NEGOCIO_", ""), sumCapital, suminteres, sumItnxMora, sumGac, sumItems, 2, 100, sumItems, 0);

        }else if(status_vencido ===false){//agregar negocios corrientes sin pago total
             agregarFilaPagoInicial(listaNegocios, tipo_neg.replace("NEGOCIO_", ""), sumCapital, suminteres, sumItnxMora, sumGac, sumItems, 2, 100, sumItems, 0);
        }
    
    }
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function replaceAll(text, busca, reemplaza) {
    while (text.toString().indexOf(busca) !== - 1)
        text = text.toString().replace(busca, reemplaza);
    return text;
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function mensajesDelSistema2(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
                location.reload();
            }
        }
    });

}


function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();
}

function closeLoading() {
    //dialogo de espera 
    $("#dialogo2").dialog('close');
}

function abrirSimuladorFenalco() {

    if ($("#totalItem").val() !== '0') {
        $("#valor_negocio").val(numberConComas($("#totalReestructuracion").html()));

        $("#simulador_credito").dialog({
            width: 1029,
            height: 600,
            show: "scale",
            hide: "scale",
            resizable: true,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n de cerrar

                "Liquidar": function () {
                     calcularLiquidacion();
                },
                "Solicitar Aprobacion": function () {
                    generarEstracto();                    
                },
                "Salir": function () {
                    $("#cuota").val(1);
                    $("#tabla_simulador_Credito").jqGrid("clearGridData", true).trigger("reloadGrid");
                    $("#grid_liquidacion").hide();
                    jQuery("#tabla_simulador_Credito").jqGrid("clearGridData", true);
//                    jQuery("#tabla_saldos_negocio_padre").jqGrid("clearGridData", true);
//                    jQuery("#tabla_saldos_negocio_seguro").jqGrid("clearGridData", true);
//                    jQuery("#tabla_saldos_negocio_gps").jqGrid("clearGridData", true);
//                    jQuery("#tabla_negocios_clientes").jqGrid("clearGridData", true);
//                    $("#cuerpoTablaConsolidado").html("");
//                    $("#cuerpoTablaInicial").html("");
//                    $("#totalResumenPagoInicial").val(0);
//                    $("#totalResumen").val(0);
//                    $("#totalSubResumen").val(0);
//                    $("#totalaPagar").val(0);
//                    $("#totalsaldoVencidoRee").val(0);
//                    $("#totalsaldoCorrienteRee").val(0);
//                    $("#totalReestructuracion").html("0.00");
//                    $("#divSaldoNegocio").hide();
                    $(this).dialog("close");
                }

            }
        });

    } else {

        mensajesDelSistema("No se puede iniciar el simulador de credito <br/> Saldos en cero. Gracias.", "320", "150");
    }

}

function calcularLiquidacion() {

    if ($("#cuota").val() !== '' && $("#valor_negocio").val() !== ''
            && $("#primeracuota").val() !== '' && $("#titulo_valor").val() !== ''
            && $("#idconvenio").val() !== '' && $("#afiliado").val() !== '') {
        
        $("#grid_liquidacion").show();

        if ($("#valor_negocio").val() !== '0') {
            SimuladorCreditoFenalco($("#cuota").val(), $("#valor_negocio").val(), $("#primeracuota").val(), $("#titulo_valor").val(), $("#idconvenio").val(), $("#afiliado").val());
        } else {
            mensajesDelSistema("Valor Negocio debe ser mayor a cero.", "298", "150");
        }
    } else {
        mensajesDelSistema("Debe diligenciar todos los campos.", "298", "150");
    }

}

function generarEstracto() {
    if (jQuery("#tabla_simulador_Credito").getGridParam("records") > 0) {
         loading("Espere un momento por favor...", "270", "140");
         var tablePagoInicial = $('#tablaPagoInicial').tableToJSON();
         var tableSaldos = $('#resumenSaldos').tableToJSON();
         var formData =JSON.stringify(jQuery('#form_resumen_saldos').serializeArray());
         
         var fullData = jQuery("#tabla_simulador_Credito").jqGrid('getRowData');
         var datosLiquidacion = JSON.stringify(fullData);
         
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Reestructurar&accion=Fenalco",
            dataType: 'json',
            data: {
               opcion: 5,
               tablaInicial:JSON.stringify(tablePagoInicial),
               tablaSaldos:JSON.stringify(tableSaldos),
               formulario:formData, 
               liquidador:datosLiquidacion
            },
            success: function (json) {
               closeLoading();
               if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
                  }
               if (json.respuesta === "OK") {
                   mensajesDelSistema2("Se ha generado una solicitud de aprobacion<br>Por favor verificar en el modulo de solicitudes pendientes", '300', '180');
                  
               }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    } else {
       mensajesDelSistema("Debe liquidar el negocio para continuar.", "270", "150");
    }

}


function SimuladorCreditoFenalco(cuota, valor_negocio, primeracuota, titulo_valor,id_convenio,afiliado) {
    var grid_simulador_credito = jQuery("#tabla_simulador_Credito");

    if ($("#gview_tabla_simulador_Credito").length) {
        reloadGridSimuladorCreditoFenalco(grid_simulador_credito, cuota, valor_negocio, primeracuota, titulo_valor,id_convenio,afiliado);
    } else {

        grid_simulador_credito.jqGrid({
            caption: "Liquidacion Negocio",
            url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=4&cuota=" + cuota + "&valor_negocio=" + valor_negocio + "&primeracuota=" + primeracuota + "&titulo_valor=" + titulo_valor + "&idconvenio=" + id_convenio + "&afiliado=" + afiliado,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: '340',
            width: '995',
            colNames: ['Fecha','Dias', 'Cuota', 'Saldo Inicial', 'Capital', 'Interes', 'Custodia', 'Seguro', 'Remesa','Valor Cuota', 'Saldo Final'],
            colModel: [
                {name: 'fecha', index: 'fecha', width: 80, align: 'center'},
                {name: 'dias', index: 'dias', width: 80, align: 'center',hidden: true},
                {name: 'item', index: 'item', sortable: true, width: 50, align: 'center', key: true},
                {name: 'saldo_inicial', index: 'saldo_inicial', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'capital', index: 'capital', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes', index: 'interes', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'custodia', index: 'custodia', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'seguro', index: 'seguro', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'remesa', index: 'remesa', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor', index: 'valor', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'saldo_final', index: 'saldo_final', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            // pager: ('#page_detalles_cartera'),
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            userDataOnFooter: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function() {

                cacularTotalesSimuladorCredito(grid_simulador_credito);
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });
    }
}

function  reloadGridSimuladorCreditoFenalco(grid_simulador_credito, cuota, valor_negocio, primeracuota, titulo_valor,id_convenio,afiliado) {
    grid_simulador_credito.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=4&cuota=" + cuota + "&valor_negocio=" + valor_negocio + "&primeracuota=" + primeracuota + "&titulo_valor=" + titulo_valor + "&idconvenio=" + id_convenio + "&afiliado=" + afiliado
    });
    grid_simulador_credito.trigger("reloadGrid");
}

function  cacularTotalesSimuladorCredito(grid_simulador_credito) {

    var capital = grid_simulador_credito.jqGrid('getCol', 'capital', false, 'sum');
    var interes = grid_simulador_credito.jqGrid('getCol', 'interes', false, 'sum');
    var custodia = grid_simulador_credito.jqGrid('getCol', 'custodia', false, 'sum');
    var seguro = grid_simulador_credito.jqGrid('getCol', 'seguro', false, 'sum');
    var remesa = grid_simulador_credito.jqGrid('getCol', 'remesa', false, 'sum');
    var valor = grid_simulador_credito.jqGrid('getCol', 'valor', false, 'sum');
 
    grid_simulador_credito.jqGrid('footerData', 'set', {
        item: 'Total:',
        capital: capital,
        interes: interes,
        custodia: custodia,
        seguro: seguro,
        remesa: remesa,        
        valor: valor

    });

}

function carcularFecha() {

    //var aux = replaceAll("2014-12-25", "-", "/");
    var date = new Date();
    var days = date.getDate();
    var fecha = "0099-01-01";
    var mes = date.getMonth() + 1;
    var anio = date.getFullYear();



    if (days >= 1 && days <= 2) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion3 = new Option(fecha, fecha);


        }

    }

    if (days > 2 && days <= 12) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-12";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion2 = new Option(fecha, fecha);
            if (mes === 11) {
                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion3 = new Option(fecha, fecha);
            } else {
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion3 = new Option(fecha, fecha);
            }

        }
    }

    if (days > 12 && days <= 17) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion3 = new Option(fecha, fecha);


        } else {
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion1 = new Option(fecha, fecha);

            if (mes === 11) {

                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion3 = new Option(fecha, fecha);

            } else {

                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion2 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
                opcion3 = new Option(fecha, fecha);
            }
        }

    }

    if (days > 17 && days <= 22) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-17";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion0 = new Option(fecha, fecha, "defauldSelected");

            if (mes === 11) {

                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion1 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-17";
                opcion3 = new Option(fecha, fecha);

            } else {

                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion1 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
                opcion2 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-17";
                opcion3 = new Option(fecha, fecha);
            }
        }

    }


    if (days > 22 && days <= 31) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-22";
            opcion3 = new Option(fecha, fecha);


        } else {

            if (mes === 11) {

                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion0 = new Option(fecha, fecha, "defauldSelected");
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion1 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-17";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-22";
                opcion3 = new Option(fecha, fecha);


            } else {

                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion0 = new Option(fecha, fecha, "defauldSelected");
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
                opcion1 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-17";
                opcion2 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-22";
                opcion3 = new Option(fecha, fecha);

            }

        }
    }

    document.formulario.primeracuota.options[0] = opcion0;
    document.formulario.primeracuota.options[1] = opcion1;
    document.formulario.primeracuota.options[2] = opcion2;
    document.formulario.primeracuota.options[3] = opcion3;


}

function cargarUnidadNegocio() {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructurar&accion=Fenalco",
        dataType: 'json',
        data: {
            opcion: 2
        },
        success: function(json) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                     $('#unidad_negocio').append("<option value=''>Seleccione</option>");
                    for (var key in json) {
                        $('#unidad_negocio').append('<option value=' + key + '>' + json[key] + '</option>');
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargardescuentos() {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructurar&accion=Fenalco",
        dataType: 'json',
        data: {
            opcion: 6
        },
        success: function(json) {
             setTimeout(function() {
                closeLoading();
           
               
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }  
                
                if (json.respuesta==='NO') {
                    mensajesDelSistema2("No se encontraron descuentos configurados para este mes.", '290', '150');                 
                    return;
                }    
                
                  }, 1500);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function validarDescuentos(tipo_descuento,porcentaje,tipo,inicial){
    
    if (tipo_descuento === 'CAPITAL') {
        tipo_descuento="CAP";
    }
    if (tipo_descuento === 'INTERES') {
        tipo_descuento="INT";
    }
    if (tipo_descuento === 'INTXMORA') {
         tipo_descuento="IXM";
    }
    if (tipo_descuento === 'GAC') {
        tipo_descuento="GAC";       
    }
    //estadoDescuento=false;
    
    var validar=false;
    var valor_minimo;
     $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructurar&accion=Fenalco",
        dataType: 'json',
        async: false,
        data: {
            opcion: 7,
            tipo_negocio:tipo,
            tipo_descuento:tipo_descuento,
            porcentaje:porcentaje,
            inicial:inicial
        },
        success: function(json) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }   
                
                if (json.respuesta === "OK") {  
                     validar=true; 
                }else if(json.respuesta === "NO"){
                   validar=false; 
                   estadoDescuento=true;    
                }else if(json.respuesta === "PI"){
                   estadoDescuento=true;                    
                }else if(json.respuesta === "NOT"){
                    validar=false; 
                  // estadoDescuento=true;   
                }
                
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    return validar ;
    
}


function validarTablaInicial(tipo){
    
    var validar="";
    var valor_minimo;
     $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructurar&accion=Fenalco",
        dataType: 'json',
        async: false,
        data: {
            opcion: 8,
            tipo_negocio:tipo            
        },
        success: function(json) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }   
                 
                if (json.respuesta === "OK") {
                   
                     validar=json.respuesta; 
                }else{
                     validar=json.respuesta; 
                }
                
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    return validar ;
    
}