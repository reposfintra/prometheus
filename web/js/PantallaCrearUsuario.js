function soloDigitosKeypress()
{
  var isKeyDigit = true;
  if (event.keyCode < 48 || event.keyCode > 57)
    isKeyDigit = false;
  return isKeyDigit;
}

function createUserFrmReset()
{
  var frm = document.createUserFrm;
  frm.nombre.value = "";
  frm.direccion.value = "";
  frm.ciudad.value = "";
  frm.pais.selectedIndex = 0;
  frm.estadoUsuario.selectedIndex = 0;
  frm.email.value = "";
  frm.telefono.value = "";
  frm.nit.value = "";
  frm.idUser.value = "";
  frm.pass1.value = "";
  frm.pass2.value = "";
}
function email(){
			var frm = document.createUserFrm;
			if  (frm.email.value != '') {
				 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(frm.email.value) ){
					 /*var e = frm.email.value;
					 if(frm.tipoUsuario.value=='ADMIN'||frm.tipoUsuario.value=='TSPUSER'){
					    
						 if(e.indexOf('mail.tsp.com')==-1){
							 alert("Favor digitar correo de interno de la empresa '@mail.tsp.com' "); 
							 frm.email.value=''; 
							 return (false);
						 }	
					 }*/
					 return (true);
				 }
				 else{
					 alert("La direcci�n de email es incorrecta.");
					 frm.email.value=''; 
					 return (false);
				 }
			 }   
			 
		}

function enviarDatosBtnClick()
{
	
  email();	  
  var frm = document.createUserFrm;
    frm.salvarDatos.value = "N";
  if( frm.nombre.value == "" ){
    alert("Digite el nombre del usuario.");
    return false;
  }else if( frm.direccion.value == "" ){
    alert("Digite la direcci�n del usuario.");
    return false;
  }else if( frm.ciudad.value == "" ){
    alert("Digite la ciudad de origen del usuario.");
    return false;
  }else if( frm.email.value == "" ){
    alert("Digite la direcci�n electr�nica del usuario.");
    return false;
  }else if( frm.telefono.value == "" ){
    alert("Digite el tel�fono del usuario.");
    return false;
  }else if( frm.nit.value == "" ){
    alert("Digite el nit o CC del usuario.");
    return false;
  }
  else if( frm.perfil.value == "" ){
    alert("Selecione el perfil o los perfiles del usuario.");
    return false;
  }
  else if( frm.idUser.value == "" ){
    alert("Digite el ID del usuario.");
    return false;
  }else if( frm.pass1.value == "" ){
    alert("Digite la clave del usuario.");
    return false;
  }else if( frm.pass2.value == "" ){
    alert("Debe confirmar la clave del usuario.");
    return false;
  }else if(frm.pass1.value != frm.pass2.value){
    frm.pass1.value = "";
    frm.pass2.value = "";
    alert("La claves escritas no coinciden.\nRectifique, por favor.");
    return false;
          
  }else if((frm.agencia)&&(frm.agencia.disabled == false)&&(frm.agencia.value == "")){
    alert("Debe seleccionar una agencia.");
    return false;
  }else{
    for( var i=0; i<usuarios.length; i++ ){
        if ( usuarios[i] == frm.idUser.value ){
            alert("El id de usuario '"+frm.idUser.value+"' ya se encuentra registrado en el sistema!\ncorrijalo e intente de nuevo");
            return false;
        }
    }
  }

  frm.salvarDatos.value = "S";
  frm.submit();
  
}