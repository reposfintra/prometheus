var tabla_de_proyectos = '';

$(document).ready(function() {
 $.fn.dataTable.ext.errMode = 'none';

    $('#table').on( 'error.dt', function ( e, settings, techNote, message ) {
    console.log( 'An error has been reported by DataTables: ', message );
    } ) ;    
    cargarItems('otros');    
    $('#select_bodega').on('change', function() {          
        cargarItems('salida');
    })
                              
} );
function cargarProyectos() {
  var array_de_proyectos = []
  var opcion = 31;            
    $.ajax({
          type: 'POST',
          url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
          dataType: 'json',
          async: false,
          data: {
            opcion: 31
          },
          success: function(json) {     
            if (json.error) {
              //  mensajesDelSistema(json.error, '250', '180');
              return;
            }          
              for (var datos in json) {
                array_de_proyectos.push(Object.values(json[datos]));
              }
          }
        });

    tabla = $('#tabla_de_proyectos').DataTable({
    destroy: true,
    processing: false,
    pageLength: 25,
    dom: '<"toolbar">frtip',
    data: array_de_proyectos,
    columns: [{
        title: "FOMS"
      }, {
        title: "ID SOLICITUD"
      }, {
        title: "DESCRIPCION PROYECTO"
      }, {
        title: "SELECCIONAR"
      }] ,
    columnDefs: [{
      targets: -1,
      data: null,
      defaultContent: "<IMG SRC='./images/select_ad-x.png' WIDTH=20 HEIGHT=20 BORDER=2 ALT='Visualizar' id='img_seleccionar' onclick='obtenerFila(this)'> \n\ "
    }]
  }); 
  tabla_de_proyectos = tabla;
  $('#div_proyectos').fadeIn('slow');       
};

function obtenerFila(e) {    
    var data = tabla_de_proyectos.row($(e).parents('tr')).data();    
    $('#foms').val(data[0]);
    id_solicitud = data[1];
    $('#id_solicitud').val(id_solicitud);
    $('#div_proyectos').fadeOut('slow');              
    cargarBodegasTraspaso(id_solicitud, '#select_bodega');
};

function abrirHistorico(e){
    var id_solicitud = $(e).data('id_solicitud');
    var id_bodega = $(e).data('id_bodega');
    var cod_material = $(e).data('cod_material');    
    
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
        dataType: 'json',
        async: false,
        data: {
            id_solicitud: id_solicitud,
            id_bodega: id_bodega,
            cod_material: cod_material,
            opcion: 38
        },
        success: function (json) {                
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $( ".dimmer" ).remove();
                if (jQuery.isEmptyObject(json)){
                    toastr.error("Sin registros", "Error");
                }else{
                    construirModal(json);
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            
        }
    });
    
    $('.ui.basic.modal').modal('show');   
};

function construirModal(json){
    console.log(json);
    data_movimiento = "";

    var items = "";
    for (var i = 0; i < json.length; i++){
        var obj = json[i];        
        switch (obj.nombre){            
        case 'ENTRADA':
                        data_movimiento += `
                                <div class="ui relaxed divided list">
                                  <div class="item">
                                    <i class="long arrow alternate right icon green"></i>
                                    <div class="content">
                                      <div class="description white">`+obj.descripcion+`</div>
                                    </div>
                                  </div>
                                </div>                                
                            `;   

          break;
        case 'TRASLADO':
            data_movimiento += `
                                <div class="ui relaxed divided list">
                                  <div class="item">
                                    <i class="exchange blue icon"></i>
                                    <div class="content">
                                      <div class="description white">`+obj.descripcion+`</div>
                                    </div>
                                  </div>
                                </div>                                
                            `;            
          break;
        case 'SALIDA':
            data_movimiento += `
                                <div class="ui relaxed divided list">
                                  <div class="item">
                                    <i class="long arrow alternate left icon"></i>
                                    <div class="content">
                                      <div class="description white">`+obj.descripcion+`</div>
                                    </div>
                                  </div>
                                </div>                                
                            `;                 
          break;
        }        
    }    
    
    var data = `
        <div>
            <div class="ui basic modal" style="overflow: scroll;">
              <div class="ui icon header">
                <i class="warehouse icon"></i>
                `+json[0].bodega+`
                <p>`+json[0].descripcion_insumo+`</p>
              </div>
              <div class="scrolling content">
              `+data_movimiento+`
                          
              </div>
              <div class="actions">
                <div class="ui green ok inverted button">
                  <i class="checkmark icon"></i>
                  Yes
                </div>
              </div>
            </div>
        </div>
    `;
     $(".modal-semantic").append(data);
};

function cargarBodegasTraspaso(identificador_solicitud, cbx_lista_bodega) {
    
    $(cbx_lista_bodega).prop("disabled", false);
    $(cbx_lista_bodega).html('');
    $(cbx_lista_bodega).append('<option value="0">Selecciona bodega...</option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Orden&accion=Compra',
        dataType: 'json',
        async: false,
        data: {
            id_solicitud: identificador_solicitud,
            opcion: 5
        },
        success: function (json) {                
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                for (var i in json) {
                    $(cbx_lista_bodega).append('<option value="'+json[i].id+'">' + json[i].direccion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    $('select[id=casa] option:eq(3)').attr('selected', 'selected');    
};


function cargarItems(tipo_movimiento) { 
  array_de_items=[];
  var id_bodega = $("#select_bodega").val();    
  var id_solicitud = $('#id_solicitud').val();
  tabla = '#tabla_items2';
  columnas = [{
        title: "BODEGA"
      }, {
        title: "COD MATERIAL"
      }, {
        title: "DISPONIBLE"        
      }, {
        title: "UNIDADES"
      }, {
        title: "DESCRIPCION"       
      },{
        title: "PROYECTO"       
      },{
        title: "FOMS"       
      },{
        title: "HISTORICO"       
      }];
  
    $.ajax({
      type: 'POST',
      url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
      dataType: 'json',
      async: false,
      data: {
        opcion: 32,
        id_bodega: id_bodega,
        tipo_movimiento: tipo_movimiento,
        id_solicitud: id_solicitud
      },
      success: function(json) {
          for (var datos in json) {
            array_de_items.push(Object.values(json[datos]));
          }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert("Error: " + xhr.status + "\n" +
          "Message: " + xhr.statusText + "\n" +
          "Response: " + xhr.responseText + "\n" + thrownError);
      }
    });
    if (tipo_movimiento == 'salida'){  
     $('#tabla_items2').hide();   
    if ( $.fn.DataTable.isDataTable('#tabla_items2')) {
      $('#tabla_items2').DataTable().destroy();
    }

    $('#tabla_items2').empty();      
     tabla = '#tabla_items'
     columnas = [{
        title: "COD MATERIAL"
      }, {
        title: "DESCRIPCION"
      }, {
        title: "UNIDADES"
      }, {
        title: "DISPONIBLE"
      }]; 
    }

    $('#show_items tbody').empty();       
    tabla = $(tabla).DataTable({
    destroy: true,
    processing: false,
    pageLength: 25,
    responsive: true,
    dom: '<"toolbar">frtip',
    data: array_de_items,
    columns: columnas,
    columnDefs: [{
      targets: -1,
      data: null,
      render: function(data, type, full, meta){
          return "<IMG SRC='./images/historico.png' WIDTH=20 HEIGHT=20 BORDER=2 ALT='Visualizar' id='img_seleccionar' onclick='abrirHistorico(this)' data-cod_material='"+data[1]+"' data-id_solicitud='"+data[7]+"' data-id_bodega='"+data[8]+"'> \n\ ";
      } 
    }]
  });
  
};
