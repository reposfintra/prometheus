/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    maximizarventana();
    GridCotizacion($('#idaccion').val());
    cargaDetalleCotizacion($('#idaccion').val());
    cargarinfo($('#idaccion').val());
    //$("#fecha").val('');
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        //minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        minDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $('#ui-datepicker-div').css('clip', 'auto');
    cargando_toggle();
//    formatoJ('valcotizacion');

    $('#guardar_').click(function () {
        cargando_toggle();
        guardarCotizacion();
        cargarinfo($('#idaccion').val());
        cargando_toggle();
    });


});


function cargarTblTotales() {
    var grid_tbl_totales = jQuery("#tbl_totales");
    if ($("#gview_tbl_totales").length) {
        refrescargarSolicitudesPendientes();
    } else {
        grid_tbl_totales.jqGrid({
            caption: "Totales",
            url: "./controlleropav?estado=Cotizacion&accion=Sl" ,
            datatype: "json",
            height: '300',
            width: '600',
            colNames: ['Id', 'Tipo Insumos', 'Total'],
            colModel: [
                {name: 'id', index: 'id', width: 100, align: 'center', key: true, hidden:true},
                    {name: 'descripcion', index: 'descripcion', width: 370, align: 'left'},
                    {name: 'total', index: 'total', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                        formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: '#page_totales',
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
//            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            userDataOnFooter: true,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            multiselect: false,
            subGrid: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 29,
                    idaccion : $('#idaccion').val()
                }
            },
            gridComplete: function () {
                var total = grid_tbl_totales.jqGrid('getCol', 'total', false, 'sum');
                grid_tbl_totales.jqGrid('footerData', 'set', { descripcion : 'Total : ' , total: total});

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tbl_totales", {add: false, edit: false, del: false, search: false, refresh: false}, {});
    }
    $("#tbl_totales").setGridParam({
        subGridRowExpanded: function (subgrid_id, row_id) {
            var pager_id, subgrid_table_id;
            subgrid_table_id = subgrid_id + "_t";
            pager_id = "p_" + subgrid_table_id;
            $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");

            jQuery("#" + subgrid_table_id).jqGrid({
                url: "./controlleropav?estado=Cotizacion&accion=Sl" ,
                datatype: "json",
                colNames: ['Id', 'Categoria', 'Total'],
                colModel: [
                    {name: 'id', index: 'id', width: 100, align: 'center', key: true, hidden:true},
                    {name: 'descripcion', index: 'descripcion', width: 350, align: 'left'},
                    {name: 'total', index: 'total', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                        formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
                ],
                rowNum: 100000,
                width: '500',
                height: '140',
                footerrow: true,
                userDataOnFooter: true,
                pgtext: null,
                loadonce: true,
                pgbuttons: false,
                gridview: true,
                viewrecords: true,
                rowTotal: 1000,
                ajaxGridOptions: {
                    dataType: "json",
                    type: "post",
                    data: {
                        opcion: 30,
                        idtipoinsumo: row_id,
                        idaccion : $('#idaccion').val()
                    }

                },
                jsonReader: {
                    root: "rows",
                    repeatitems: false,
                    id: "0"
                }, gridComplete: function () {
            
                    var total = $("#" + subgrid_table_id).jqGrid('getCol', 'total', false, 'sum');
                     $("#" + subgrid_table_id).jqGrid('footerData', 'set', { descripcion : 'Total : ' , total: total});

            
                }
            });

        }

    });
    
}

function refrescargarSolicitudesPendientes() {
    jQuery("#tbl_totales").setGridParam({
        url: "./controlleropav?estado=Cotizacion&accion=Sl" ,
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 29,
                idaccion : $('#idaccion').val()
            }
        }
    });

    jQuery('#tbl_totales').trigger("reloadGrid");
}
function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function cargarComboGrupo1(filtro) {
    var elemento = $('#grupo_apu1'), sql = 'ConsultaGruposAPUS';
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'GET',
        data: {opcion: 1, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '" onclick="CargarGridApus();">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function reloadGridCotizacion(apugrupo, metcalc, accion) {
    var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=1&apugrupo=' + apugrupo + '&metcalc=' + metcalc + '&id=' + accion;
    jQuery("#tbl_cotizacion").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#tbl_cotizacion').trigger("reloadGrid");

}

function listarMetodosCal() {
    var Result = {};
    $.ajax({
        type: 'GET',
        url: "/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=6",
        dataType: 'json',
        async: false,
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    Result = {};
                } else {
                    Result = json;
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return Result;
}

function calcularMet(metodo, codigo) {
    var Result = {};
    $.ajax({
        type: 'GET',
        url: "/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=7&metodo=" + metodo + "&codigo=" + codigo,
        dataType: 'json',
        async: false,
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    Result = {};
                } else {
                    Result = json.valor;
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return Result;
}

function CalcularPareto() {
    var grid = jQuery("#tbl_cotizacion"),
            filas = grid.jqGrid('getDataIDs'),
            data;

    var preciototal1 = grid.jqGrid('getCol', 'preciototal', false, 'sum');

    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);

        grid.jqGrid('setCell', filas[i], "pareto", (data.preciototal / preciototal1) * 100);

    }
}

function GridCotizacion(accion) {

    var grid_crear_cotizacion = jQuery("#tbl_cotizacion");
    var apugrupo = obtenerAPUAsoc();
    var metcalc = $('#metcalc').val();

    var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=1&apugrupo=' + apugrupo + '&metcalc=' + metcalc + '&id=' + accion;
    //grid_crear_cotizacion.jqGrid('GridDestroy');
    if ($("#gview_tbl_cotizacion").length) {
        reloadGridCotizacion(apugrupo, metcalc, accion);
    } else {
        grid_crear_cotizacion.jqGrid({
            url: url,
            datatype: 'json',
            caption: "Creaci�n de Presupuesto",
            height: '550',
            width: '1815',
            colNames: ['Id', 'Met.Calculo', 'Tipo Insumo', 'Id Tipo Insumo', 'Id Insumo', 'Cod. Insumo', 'Descripcion Insumo', 'Cantidad', 'Unidad Medida', 'Id Unidad Medida', 'Precio Predictivo',
                'Predictivo Total', 'Peso', 'Ultimo Proveedor Compra', 'Nit Proveedor', 'Val. Maximo. Comprado', 'Precio Unitario', 'Total', 'Proveedor', 'Rent.Prov.', 'Val.Rent.Prov.', 'Rent.Sel.', 'Val.Rent.Sel.',
                'Total Cliente', 'Estado'],
            colModel: [
                {name: 'id', index: 'id', align: 'center', width: 100, hidden: true},
                {name: 'metodocalculo', index: 'metodocalculo', align: 'center', width: 100, search: false, editable: true, edittype: 'select', editrule: {required: true},
                    editoptions: {
                        value: listarMetodosCal(),
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {

                                        var rowid = e.target.id.replace("_metodocalculo", "");
                                        var val = e.target.value.replace("_", "");
                                        var codinsumo = grid_crear_cotizacion.jqGrid('getCell', rowid, 'codinsumo');
                                        var cantidad = grid_crear_cotizacion.jqGrid('getCell', rowid, 'cantidad');

                                        var val_calculo = calcularMet(val, codinsumo);
                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "preciounitario", val_calculo);
                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "preciototal", val_calculo * cantidad);

                                        var preciototal = grid_crear_cotizacion.jqGrid('getCol', 'preciototal', false, 'sum');
                                        jQuery("#tbl_cotizacion").jqGrid('footerData', 'set', {preciototal: preciototal});

                                        CalcularPareto();

                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]

                    }
                },
                {name: 'tipoinsumo', index: 'tipoinsumo', align: 'center', width: 100},
                {name: 'id_tipo_insumo', index: 'id_tipo_insumo', hidden: true, align: 'center', width: 100},
                {name: 'id_insumo', index: 'id_insumo', hidden: true, align: 'center', width: 100, sorttype: 'currency'},
                {name: 'codinsumo', index: 'codinsumo', align: 'center', width: 100},
                {name: 'descripcionins', index: 'descripcionins', width: 350, edittype: 'text', resizable: false, align: 'center'},
                {name: 'cantidad', index: 'cantidad', align: 'center', width: 50, sorttype: 'currency'},
                {name: 'unidadmedida', index: 'unidadmedida', align: 'center', width: 80},
                {name: 'idunidadmedida', hidden: true, index: 'idunidadmedida', align: 'center', width: 80},
                {name: 'preciounitario', index: 'preciounitario', align: 'center', width: 100, formatter: 'currency', sorttype: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'preciototal', index: 'preciototal', align: 'center', width: 100, formatter: 'currency', sorttype: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'pareto', index: 'pareto', align: 'center', width: 100, formatter: 'currency', formatoptions: {suffix: '%', decimalPlaces: 1}, sorttype: 'currency'},
                {name: 'proveedor', index: 'proveedor', align: 'center', width: 250},
                {name: 'nitproveedor', index: 'nitproveedor', hidden: true, align: 'center', width: 130, sorttype: 'currency', },
                {name: 'preciohistorico', index: 'preciohistorico', align: 'center', width: 120, formatter: 'currency', sorttype: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'preciounitariofinal', index: 'preciounitariofinal', align: 'center', width: 100, editable: true, formatter: 'currency', sorttype: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "},
                    editoptions: {
                        dataEvents: [{type: 'change', fn: function (e) {
//                                    try {
//
//                                        var rowid = e.target.id.replace("_preciounitariofinal", "");
//                                        var val = e.target.value.replace("_", "");
//                                        var valor = val * grid_crear_cotizacion.jqGrid('getCell', rowid, 'cantidad');
//                                        var val_prov = grid_crear_cotizacion.jqGrid('getCell', rowid, 'rentabilidadprov');
//                                        var val_sel = grid_crear_cotizacion.jqGrid('getCell', rowid, 'rentabilidadsel');
//                                        var idinsumo = grid_crear_cotizacion.jqGrid('getCell', rowid, 'id_insumo');
//                                        
//                                        
//                                        console.log(rowid);
//
//                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "totalfinal", valor);
//
//                                        var totalProv = (valor * val_prov / 100);
//
//                                        var totalSel = ((valor + totalProv) * val_sel / 100);
//
//                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "valrentabilidadprov", totalProv);
//
//                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "valrentabilidadsel", totalSel);
//
//                                        var totalparc = (valor * val_prov / 100) + valor;
//
//                                        var total = (totalparc * val_sel / 100) + totalparc;
//
//                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "totalcliente", total);
//
//                                        var totalfinal = grid_crear_cotizacion.jqGrid('getCol', 'totalfinal', false, 'sum');
//                                        jQuery("#tbl_cotizacion").jqGrid('footerData', 'set', {totalfinal: totalfinal});
//
//                                        var totalcliente = grid_crear_cotizacion.jqGrid('getCol', 'totalcliente', false, 'sum');
//                                        jQuery("#tbl_cotizacion").jqGrid('footerData', 'set', {totalcliente: totalcliente});
//
//                                        //$('#valcotizacion').val('$ ' + numberConComas(totalcliente.toFixed(2)));
//                                        $('#valcotizacion').val(totalcliente.toFixed(2));
//                                        resta();
//                                        ActualizarValDetalle(idinsumo, val);
//
//                                    } catch (exc) {
//                                    }
//                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]

                    }

                },
                {name: 'totalfinal', index: 'totalfinal', align: 'center', width: 100, formatter: 'currency', sortable: true, sorttype: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'proveedorfinal', index: 'proveedorfinal', hidden: true, align: 'center', width: 160},
                {name: 'rentabilidadprov', index: 'rentabilidadprov', hidden: true, align: 'center', width: 50, formatter: 'currency', formatoptions: {suffix: '%', decimalPlaces: 0}, sorttype: 'currency'},
                {name: 'valrentabilidadprov', index: 'valrentabilidadprov', hidden: true, align: 'center', width: 100, editable: true, formatter: 'currency', sorttype: 'currency'},
                {name: 'rentabilidadsel', index: 'rentabilidadsel', hidden: true, align: 'center', width: 50, formatter: 'currency', formatoptions: {suffix: '%', decimalPlaces: 0}, sorttype: 'currency'},
                {name: 'valrentabilidadsel', index: 'valrentabilidadsel', hidden: true, align: 'center', width: 100, editable: true, formatter: 'currency', sorttype: 'currency'},
                {name: 'totalcliente', index: 'totalcliente', hidden: true, align: 'center', width: 100, formatter: 'currency', sorttype: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'estado', index: 'estado', search: false, align: 'center', width: 50}
            ],
            rowNum: 10000,
            rowTotal: 10000000,
            pager: '#page_cotizacion',
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            rownumbers: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            userDataOnFooter: true,
            reloadAfterSubmit: true,
            multiselect: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            afterSaveCell: function (rowid, name, val, iRow, iCol) {
                alert(1);

            },
            gridComplete: function () {
                //suma();
                var cantidad = grid_crear_cotizacion.jqGrid('getCol', 'cantidad', false, 'sum');
                var preciototal = grid_crear_cotizacion.jqGrid('getCol', 'preciototal', false, 'sum');
                var pareto = grid_crear_cotizacion.jqGrid('getCol', 'pareto', false, 'sum');
                var totalcliente = grid_crear_cotizacion.jqGrid('getCol', 'totalcliente', false, 'sum');
                var totalfinal = grid_crear_cotizacion.jqGrid('getCol', 'totalfinal', false, 'sum');
                $('#valcotizacion').val(totalcliente.toFixed(2));
                resta();
                grid_crear_cotizacion.jqGrid('footerData', 'set', {descripcionins: 'TOTAL', cantidad: cantidad, preciototal: preciototal, pareto: pareto, totalfinal: totalfinal, totalcliente: totalcliente});

                var ids = grid_crear_cotizacion.jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];

                    var estado = grid_crear_cotizacion.getRowData(cl).estado;
                    var id_insumo = grid_crear_cotizacion.getRowData(cl).id_insumo;
                    if (estado === '0') {
                        be = "<img src='/fintra/images/botones/iconos/chulo.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Pendiente'  onclick=\"mensajeConfirmAction1('Esta seguro de cambiar el estado a terminado?','250','150',ActualizarEstadoTerminadoDetalle,'" + id_insumo + "');\">";
                    } else {
                        be = "<img src='/fintra/images/botones/iconos/obligatorio.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Terminado'  onclick=\"mensajeConfirmAction1('Esta seguro de cambiar el estado a pendiente?','250','150',ActualizarEstadoPendienteDetalle,'" + id_insumo + "');\">";
                    }

                    grid_crear_cotizacion.jqGrid('setRowData', ids[i], {estado: be});
                }

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                grid_crear_cotizacion.jqGrid('editRow', rowid, true, function () {
                    $("input, select", e.target).focus();
                }, null, null, {}, function (rowid) {
//                    var rowid = e.target.id.replace("_preciounitariofinal", "");
                    var val = grid_crear_cotizacion.jqGrid('getCell', rowid, 'preciounitariofinal');
                    var valor = val * grid_crear_cotizacion.jqGrid('getCell', rowid, 'cantidad');
                    var val_prov = grid_crear_cotizacion.jqGrid('getCell', rowid, 'rentabilidadprov');
                    var val_sel = grid_crear_cotizacion.jqGrid('getCell', rowid, 'rentabilidadsel');
                    var idinsumo = grid_crear_cotizacion.jqGrid('getCell', rowid, 'id_insumo');


                    console.log(rowid);

                    grid_crear_cotizacion.jqGrid('setCell', rowid, "totalfinal", valor);

                    var totalProv = (valor * val_prov / 100);

                    var totalSel = ((valor + totalProv) * val_sel / 100);

                    grid_crear_cotizacion.jqGrid('setCell', rowid, "valrentabilidadprov", totalProv);

                    grid_crear_cotizacion.jqGrid('setCell', rowid, "valrentabilidadsel", totalSel);

                    var totalparc = (valor * val_prov / 100) + valor;

                    var total = (totalparc * val_sel / 100) + totalparc;

                    grid_crear_cotizacion.jqGrid('setCell', rowid, "totalcliente", total);

                    var totalfinal = grid_crear_cotizacion.jqGrid('getCol', 'totalfinal', false, 'sum');
                    jQuery("#tbl_cotizacion").jqGrid('footerData', 'set', {totalfinal: totalfinal});

                    var totalcliente = grid_crear_cotizacion.jqGrid('getCol', 'totalcliente', false, 'sum');
                    jQuery("#tbl_cotizacion").jqGrid('footerData', 'set', {totalcliente: totalcliente});

                    //$('#valcotizacion').val('$ ' + numberConComas(totalcliente.toFixed(2)));
                    $('#valcotizacion').val(totalcliente.toFixed(2));
                    resta();
                    ActualizarValDetalle(idinsumo, val);
                });
                return;
            },
            restoreAfterError: true

        });
        grid_crear_cotizacion.navGrid("#page_cotizacion", {add: false, edit: false, del: false, search: false, refresh: true});
        /*grid_crear_cotizacion.navButtonAdd('#page_cotizacion', {
         caption: "Agregar Insumo",
         title: "Agregar Insumo",
         buttonicon: "ui-icon-new",
         onClickButton: function () {
         //guardarAPU();
         }
         });*/
        grid_crear_cotizacion.navButtonAdd('#page_cotizacion', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                cargando_toggle();
                guardarCotizacion();
                cargarinfo($('#idaccion').val());
                cargando_toggle();
            },
            position: "first"
        });
        /*grid_crear_cotizacion.navButtonAdd('#page_cotizacion', {
         caption: "Cargar APUs",
         title: "Cargar APUs",
         buttonicon: "ui-icon-new",
         onClickButton: function () {
         CargarAPU();
         },
         position: "first"
         });*/
        grid_crear_cotizacion.navButtonAdd('#page_cotizacion', {
            caption: "",
            title: "Categoria",
            buttonicon: "ui-icon-bullet",
            onClickButton: function () {
                cargando_toggle();
                cargarTblTotales();
                $('#div_totales').dialog({
                title:'Totales',
                show: 'blind',
                hide: 'explode',
                width: 650,
                height: 430
                });
//                $('#div_totales').siblings('.ui-dialog-titlebar').css("display","none");
                cargando_toggle();
            }
        });
        grid_crear_cotizacion.jqGrid('setGroupHeaders', {
            useColSpanStyle: false,
            groupHeaders: [
                {startColumnName: 'id', numberOfColumns: 16, titleText: '<H3><em>COLUMNAS DE ANALISIS</em></H3>'},
                {startColumnName: 'preciounitariofinal', numberOfColumns: 9, titleText: '<H3><em>COTIZACION FINAL - OFERTA</em></H3>'}
            ]
        });

    }
}

function guardarCotizacion() {
    var grid = jQuery("#tbl_cotizacion"),
            grid_detalle = jQuery("#tbl_cotizacion_detalle"),
            filas = grid.jqGrid('getDataIDs'),
            filas_detalle = grid_detalle.jqGrid('getDataIDs'),
            data, error = false,
            CodCliente = $('#CodCliente').val(),
            NombreCliente = $('#NombreCliente').val(),
            CodCotizacion = $('#CodCotizacion').val(),
            fecha = $('#fecha').val(),
            visualizacion = $('#visualizacion').val(),
            modalidad = $('#modalidad').val(),
            valcotizacion = $('#valcotizacion').val(),
            valdesc = $('#valdesc').val(),
            subtotal = $('#subtotal').val(),
            perc_iva = $('#perc_iva').val(),
            valiva = $('#valiva').val(),
            perc_admon = $('#perc_admon').val(),
            val_admon = $('#val_admon').val(),
            perc_imprevisto = $('#perc_imprevisto').val(),
            val_imprevisto = $('#val_imprevisto').val(),
            perc_utilidad = $('#perc_utilidad').val(),
            val_utilidad = $('#val_utilidad').val(),
            perc_aiu = $('#perc_aiu').val(),
            val_aiu = $('#val_aiu').val(),
            val_total = $('#val_total').val(),
            anticipo = $('#anticipo').val(),
            perc_anticipo = $('#perc_anticipo').val(),
            val_anticipo = $('#val_anticipo').val(),
            retegarantia = $('#retegarantia').val(),
            perc_rete = $('#perc_rete').val(),
            idaccion = $('#idaccion').val(),
            val_material = 0,
            val_mano_obra = 0,
            val_equipos = 0,
            val_herramientas = 0,
            val_transporte = 0,
            val_tramites = 0,
            existe = $('#existe').val(),
            id = $('#id').val();

    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);
        if (data.tipoinsumo === 'MATERIAL') {
            val_material = val_material * 1 + data.totalcliente * 1;
        }
        if (data.tipoinsumo === 'EQUIPO') {
            val_equipos = val_equipos * 1 + data.totalcliente * 1;
        }
        if (data.tipoinsumo === 'MANO DE OBRA') {
            val_mano_obra = val_mano_obra * 1 + data.totalcliente * 1;
        }
        if (data.tipoinsumo === 'HERRAMIENTAS') {
            val_herramientas = val_herramientas * 1 + data.totalcliente * 1;
        }
        if (data.tipoinsumo === 'TRANSPORTES') {
            val_transporte = val_transporte * 1 + data.totalcliente * 1;
        }
        if (data.tipoinsumo === 'PERMISOS Y TRAMITES') {
            val_tramites = val_tramites * 1 + data.totalcliente * 1;
        }

        grid.saveRow(filas[i]);
    }

    filas = grid.jqGrid('getRowData');
    filas_detalle = grid_detalle.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }

    /*if ($('#fecha').val() === '') {
     error = true;
     mensajesDelSistema('Escoja la Vigencia de la Cotizacion', '300', 'auto', false);
     $('#fecha').focus();
     }*/

    if (error)
        return;

    $.ajax({
        url: '/fintra/controlleropav?estado=Cotizacion&accion=Sl',
        datatype: 'json',
        type: 'POST',
        data: {opcion: 5, informacion: JSON.stringify({rows: filas}), informacion_detallada: JSON.stringify({rows: filas_detalle}),
            CodCliente: CodCliente,
            NombreCliente: NombreCliente,
            CodCotizacion: CodCotizacion,
            fecha: fecha,
            visualizacion: visualizacion,
            modalidad: modalidad,
            valcotizacion: valcotizacion,
            valdesc: valdesc,
            subtotal: subtotal,
            perc_iva: perc_iva,
            valiva: valiva,
            perc_admon: perc_admon,
            val_admon: val_admon,
            perc_imprevisto: perc_imprevisto,
            val_imprevisto: val_imprevisto,
            perc_utilidad: perc_utilidad,
            val_utilidad: val_utilidad,
            perc_aiu: perc_aiu,
            val_aiu: val_aiu,
            val_total: val_total,
            anticipo: anticipo,
            perc_anticipo: perc_anticipo,
            val_anticipo: val_anticipo,
            retegarantia: retegarantia,
            perc_rete: perc_rete,
            idaccion: idaccion,
            val_material: val_material,
            val_mano_obra: val_mano_obra,
            val_equipos: val_equipos,
            val_herramientas: val_herramientas,
            val_transporte: val_transporte,
            val_tramites: val_tramites,
            existe: existe,
            id: id},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                }
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {

        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    mostrarContenido('dialogMsgMeta');
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
                window.close();
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function CargarAPU() {

    $('#div_apus').fadeIn("slow");
    AbrirDivApus();
    cargarComboGrupo1([]);
    CargarGridApus();
    CargarGridApusAsoc();
}

function AbrirDivApus() {
    $("#div_apus").dialog({
        width: 1000,
        height: 545,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'APUs',
        closeOnEscape: false,
        buttons: {
            "Agregar": function () {
                GridCotizacion();
                $(this).dialog("destroy");
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function obtenerAPUAsoc() {
    var grid = jQuery("#tbl_apus_asoc")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false, concat = '', coma = '';
    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);
        concat = concat + coma + data.id;
        coma = ',';
    }
    //alert(concat);
    return concat;
}

function CargarGridApus() {
    var grupo_apu = $('#grupo_apu1').val();
    var apugrupo = obtenerAPUAsoc();
    var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=4';
    //$('#tbl_apus').jqGrid('GridUnload');
    if ($("#gview_tbl_apus").length) {
        refrescarGridGrupoAPU(grupo_apu);
    } else {
        jQuery("#tbl_apus").jqGrid({
            caption: 'Grid Apu',
            url: url,
            datatype: 'json',
            height: 300,
            width: 450,
            colNames: ['Id', 'Descripcion APU', 'Unidad de Medida'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombreapu', index: 'nombreapu', sortable: true, align: 'center', width: '600'},
                {name: 'nombre_unidad', index: 'nombre_unidad', search: false, sortable: true, align: 'center', width: '250'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            multiselect: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_apus',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                sync: false,
                data: {
                    grupo_apu: grupo_apu,
                    apugrupo: apugrupo
                }
            },
            ondblClickRow: function (id) {
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });
        jQuery("#tbl_apus").navGrid("#page_apus", {add: false, edit: false, del: false, search: false, refresh: false});
        jQuery("#tbl_apus").navButtonAdd('#page_apus', {
            caption: "Crear APU",
            title: "Crear APU",
            buttonicon: "ui-icon-new",
            onClickButton: function () {
                crearAPU();
            }
        });
        jQuery("#tbl_apus").jqGrid('filterToolbar', {stringResult: true, searchOnEnter: false, searchOperators: true});
        document.getElementById("gs_nombreapu").style.width = '258px';
        //document.getElementById("gs_nombreapu").style.toUpperCase();
    }
}

/*function crearAPU() {
 var url = '/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/&pagina=CrearAPU.jsp';
 //?idaccion='+idaccion;
 window.showModalDialog(url);
 }*/

function refrescarGridGrupoAPU(grupo_apu) {
    var apugrupo = obtenerAPUAsoc();
    var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=4&grupo_apu=' + grupo_apu + '&apugrupo=' + apugrupo;
    jQuery("#tbl_apus").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#tbl_apus').trigger("reloadGrid");

}

function CargarGridApusAsoc() {
    //var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=3';
    $('#tbl_apus_asoc').jqGrid('GridUnload');
    if ($("#gview_tbl_apus_asoc").length) {
        //refrescarGridProcesosMeta();
    } else {
        jQuery("#tbl_apus_asoc").jqGrid({
            caption: 'Grid Apu Asociados',
            url: '',
            datatype: 'local',
            height: 326,
            width: 450,
            colNames: ['Id', 'Descripcion APU', 'Unidad de Medida'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombreapu', index: 'nombreapu', sortable: true, align: 'center', width: '600'},
                {name: 'nombre_unidad', index: 'nombre_unidad', sortable: true, align: 'center', width: '250'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            multiselect: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_apus_asoc',
            /*jsonReader: {
             root: 'rows',
             repeatitems: false,
             id: '0'
             },*/
            ajaxGridOptions: {
                async: false,
                data: {
                    //subcategoria: $('#sub').val()
                }
            },
            ondblClickRow: function (id) {
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });

    }
}

function suma() {

    var grid = jQuery("#tbl_cotizacion")
            , filas = grid.jqGrid('getDataIDs'),
            sum = 0, data;

    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);

        grid.jqGrid('setCell', filas[i], "preciototal", data.preciounitario * data.cantidad);

    }

}

function formatoJ(input)
{
    var num = input.value.replace(/\./g, '');
    if (!isNaN(num)) {
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/, '');
        input.value = num;
    }

    else {
        alert('Solo se permiten numeros');
        input.value = input.value.replace(/[^\d\.]*/g, '');
    }
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function resta() {
    var rest = numberSinComas($('#valcotizacion').val().replace('$', '')) - numberSinComas($('#valdesc').val());
    $('#subtotal').val(rest.toFixed(2));
    $('#valiva').val((rest * $('#perc_iva').val() / 100).toFixed(2));
    $('#val_admon').val(($('#subtotal').val() * ($('#perc_admon').val() * 1) / 100).toFixed(2));
    $('#val_imprevisto').val(($('#subtotal').val() * ($('#perc_imprevisto').val() * 1) / 100).toFixed(2));
    $('#val_utilidad').val(($('#subtotal').val() * ($('#perc_utilidad').val() * 1) / 100).toFixed(2));
    $('#perc_aiu').val(($('#perc_admon').val() * 1) + ($('#perc_imprevisto').val() * 1) + ($('#perc_utilidad').val() * 1));
    $('#val_aiu').val((rest * $('#perc_aiu').val() / 100).toFixed(2));
    $('#val_total').val(($('#subtotal').val() * 1 + $('#valiva').val() * 1 + $('#val_aiu').val() * 1).toFixed(2));

    $('#val_anticipo').val(($('#val_total').val() * $('#perc_anticipo').val() / 100).toFixed(2));

}

function cargarinfo(accion) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Cotizacion&accion=Sl',
        dataType: 'json',
        async: false,
        data: {
            opcion: 2,
            id_accion: accion
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                //alert(json.existe);
                $('#existe').val(json.existe);
                if (json.existe === '0') {
                    $('#CodCliente').val(json.id_cliente);
                    $('#NombreCliente').val(json.nomcli);
                    $('#CodCotizacion').val(json.serie_cot);
                } else {
                    $('#id').val(json.id);
                    $('#CodCliente').val(json.cod_cli);
                    $('#NombreCliente').val(json.nonmbre_cliente);
                    $('#CodCotizacion').val(json.no_cotizacion);
                    $('#fecha').val(json.vigencia_cotizacion);
                    $('#visualizacion').val(json.forma_visualizacion);
                    $('#modalidad').val(json.modalidad_comercial);
                    $('#valcotizacion').val(json.valor_cotizacion);
                    $('#valdesc').val(json.valor_descuento);
                    $('#subtotal').val(json.subtotal);
                    $('#perc_iva').val(json.perc_iva);
                    $('#valiva').val(json.valor_iva);
                    $('#perc_admon').val(json.perc_administracion);
                    $('#val_admon').val(json.administracion);
                    $('#perc_imprevisto').val(json.perc_imprevisto);
                    $('#val_imprevisto').val(json.imprevisto);
                    $('#perc_utilidad').val(json.perc_utilidad);
                    $('#val_utilidad').val(json.utilidad);
                    $('#perc_aiu').val(json.perc_aiu);
                    $('#val_aiu').val(json.valor_aiu);
                    $('#val_total').val(json.total);
                    $('#anticipo').val(json.anticipo);
                    $('#perc_anticipo').val(json.perc_anticipo);
                    $('#val_anticipo').val(json.valor_anticipo);
                    $('#retegarantia').val(json.retegarantia);
                    $('#perc_rete').val(json.perc_retegarantia);

                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function habdesMod() {

    if ($('#modalidad').val() === '0') {
        $('#perc_iva').val(16);
        $('#perc_admon').val(0);
        $('#perc_imprevisto').val(0);
        $('#perc_utilidad').val(0);
        document.getElementById("perc_admon").disabled = true;
        document.getElementById("perc_imprevisto").disabled = true;
        document.getElementById("perc_utilidad").disabled = true;

    } else {
        $('#perc_iva').val(0);
        document.getElementById("perc_admon").disabled = false;
        document.getElementById("perc_imprevisto").disabled = false;
        document.getElementById("perc_utilidad").disabled = false;
    }

    resta();


}

function habdesAnt() {

    if ($('#anticipo').val() === '1') {
        $('#perc_anticipo').val(0);
        $('#val_anticipo').val(0);
        document.getElementById("perc_anticipo").disabled = false;

    } else {
        $('#perc_anticipo').val(0);
        $('#val_anticipo').val(0);
        document.getElementById("perc_anticipo").disabled = true;
    }

    resta();

}

function habdesRet() {

    if ($('#retegarantia').val() === '1') {
        $('#perc_rete').val(0);
        document.getElementById("perc_rete").disabled = false;

    } else {
        $('#perc_rete').val(0);
        document.getElementById("perc_rete").disabled = true;
    }

}

function AgregarGridAso() {

    var selectRows = jQuery("#tbl_apus").jqGrid('getGridParam', 'selarrrow');
    for (var i = 0; i < selectRows.length; i++) {

        var fila = jQuery("#tbl_apus").jqGrid('getRowData', selectRows[i]);
        jQuery("#tbl_apus_asoc").jqGrid('addRowData', selectRows[i], fila);
    }
    QuitarRowGridVal(selectRows);
}


function QuitarRowGridVal(selectRows) {

    for (var k = 0; k < selectRows.length; k++) {
        jQuery("#tbl_apus").jqGrid('delRowData', selectRows[k]);

        selectRows = jQuery("#tbl_apus").jqGrid('getGridParam', 'selarrrow');


        k = -1;
    }

}

function AgregarGridNoAso() {

    var selectRows = jQuery("#tbl_apus_asoc").jqGrid('getGridParam', 'selarrrow'), cadena = "", coma = "";
    for (var i = 0; i < selectRows.length; i++) {

        var fila = jQuery("#tbl_apus_asoc").jqGrid('getRowData', selectRows[i]);
        //////////////////////////
        var fila1 = jQuery("#tbl_apus_asoc").getRowData(selectRows[i]);

        /*if (fila1['idrel'] !== "") {
         cadena = cadena + coma + fila1['valor_xdefecto'];
         coma = ",";
         }
         
         //////////////////////////
         if (cadena === "") {*/
        jQuery("#tbl_apus").jqGrid('addRowData', selectRows[i], fila);
        //}
    }

    if (cadena === "") {
        QuitarRowGridValAso(selectRows);
    } else {
        alert('No se pueden desasociar los registros ' + cadena);
    }
}


function QuitarRowGridValAso(selectRows) {

    for (var k = 0; k < selectRows.length; k++) {
        jQuery("#tbl_apus_asoc").jqGrid('delRowData', selectRows[k]);

        selectRows = jQuery("#tbl_apus_asoc").jqGrid('getGridParam', 'selarrrow');


        k = -1;
    }

}

function NumCheck(e, field) {
    key = e.keyCode ? e.keyCode : e.which;
    if (key === 8)
        return true;
    if (key > 47 && key < 58) {
        if (field.value === "")
            return true;
        regexp = /.[0-9]{5}$/;
        return !(regexp.test(field.value));
    }
    if (key === 46) {
        if (field.value === "")
            return false;
        regexp = /^[0-9]+$/;
        return regexp.test(field.value);
    }
    return false;
}

function crearAPU() {
    $('#nomapu').val('');
    $('#tbl_insumos').jqGrid('GridUnload');
    $('#div_apu').fadeIn('slow');
    cargarComboGrupo([]);
    cargarComboUnidadM([]);
    AbrirDivAPU();
    GridInsumos();
}

function decimales(e, thi) {
// Backspace = 8, Enter = 13, ?0? = 48, ?9? = 57, ?.? = 46
    var field = thi;
    key = e.keyCode ? e.keyCode : e.which;

    if (key === 8)
        return true;
    if (key > 47 && key < 58) {
        if (field.val() === "")
            return true;
        var existePto = (/[.]/).test(field.val());
        if (existePto === false) {
            regexp = /.[0-9]{10}$/;
        }
        else {
            regexp = /.[0-9]{2}$/;
        }

        return !(regexp.test(field.val()));
    }
    if (key === 46) {
        if (field.val() === "")
            return false;
        regexp = /^[0-9]+$/;
        return regexp.test(field.val());
    }
    return false;
}

function cargaDetalleCotizacion(accion) {
    var grid_detalle = jQuery("#tbl_cotizacion_detalle");
    var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=8&id=' + accion;
    //grid_crear_cotizacion.jqGrid('GridDestroy');
    if ($("#gview_tbl_cotizacion_detalle").length) {
        reloadcargaDetalleCotizacion(accion);
    } else {
        grid_detalle.jqGrid({
            url: url,
            datatype: 'json',
            caption: "Detalle Cotizacion",
            height: '550',
            width: '1805',
            colNames: ['Id', 'Id Actividad', 'Id Apu', 'Id Insumo', 'Cantidad Insumo', 'Rendimiento Insumo', 'Cantidad Apu', 'Valor Insumo', 'Estado',
                'Costo Personalizado', 'Porc. Esquema', 'Valor Esquema', 'Porc. Contratista', 'Valor Contratista', 'Valor Total', 'id_unidad_medida'],
            colModel: [
                {name: 'id', index: 'id', align: 'center', width: 160, key: true},
                {name: 'id_actividad', index: 'id_actividad', align: 'center', width: 160},
                {name: 'id_apu', index: 'id_apu', align: 'center', width: 50},
                {name: 'id_insumo', index: 'id_insumo', align: 'center', width: 100},
                {name: 'cantidad_insumo', index: 'cantidad_insumo', align: 'center', width: 50},
                {name: 'rendimiento_insumo', index: 'rendimiento_insumo', align: 'center', width: 100},
                {name: 'cantidad_apu', index: 'cantidad_apu', align: 'center', width: 100},
                {name: 'valor_insumo', index: 'valor_insumo', align: 'center', width: 100},
                {name: 'estado', index: 'estado', align: 'center', width: 100},
                {name: 'costoper', index: 'costoper', align: 'center', width: 100},
                {name: 'porcesq', index: 'porcesq', align: 'center', width: 100},
                {name: 'valoresq', index: 'valoresq', align: 'center', width: 100},
                {name: 'porccont', index: 'porccont', align: 'center', width: 100},
                {name: 'valorcont', index: 'valorcont', align: 'center', width: 100},
                {name: 'valortotal', index: 'valortotal', align: 'center', width: 100},
                {name: 'id_unidad_medida', index: 'valortotal', align: 'center', width: 100}
            ],
            rowNum: 10000,
            rowTotal: 10000000,
            pager: '#page_cotizacion_detalle',
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            rownumbers: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            userDataOnFooter: true,
            reloadAfterSubmit: true,
            multiselect: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            restoreAfterError: true

        });

    }
}

function reloadcargaDetalleCotizacion(accion) {
    var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=8&id=' + accion;
    jQuery("#tbl_cotizacion_detalle").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#tbl_cotizacion_detalle').trigger("reloadGrid");

}

function ActualizarValDetalle(codinsumo, val_insumo) {
    var grid = jQuery("#tbl_cotizacion_detalle"),
            filas = grid.jqGrid('getDataIDs'),
            data;

    //var preciototal1 = grid.jqGrid('getCol', 'preciototal', false, 'sum');

    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);

        var id_insumo = grid.jqGrid('getCell', filas[i], 'id_insumo');

        if (id_insumo === codinsumo) {

            grid.jqGrid('setCell', filas[i], "valor_insumo", val_insumo);

        }

    }
}

function ActualizarEstadoPendienteDetalle(codinsumo) {
    var grid = jQuery("#tbl_cotizacion_detalle"),
            filas = grid.jqGrid('getDataIDs'),
            data;

    //var preciototal1 = grid.jqGrid('getCol', 'preciototal', false, 'sum');

    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);

        var id_insumo = grid.jqGrid('getCell', filas[i], 'id_insumo');

        if (id_insumo === codinsumo) {

            grid.jqGrid('setCell', filas[i], "estado", 0);

        }

    }

    var grid_crear_cotizacion = jQuery("#tbl_cotizacion");
    var ids = grid_crear_cotizacion.jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var cl = ids[i];

        var id_insumo = grid_crear_cotizacion.getRowData(cl).id_insumo;

        if (id_insumo === codinsumo) {

            be = "<img src='/fintra/images/botones/iconos/chulo.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Pendiente'  onclick=\"mensajeConfirmAction1('Esta seguro de cambiar el estado a terminado?','250','150',ActualizarEstadoTerminadoDetalle,'" + id_insumo + "');\">";

            grid_crear_cotizacion.jqGrid('setRowData', ids[i], {estado: be});
        }
    }

}


function ActualizarEstadoTerminadoDetalle(codinsumo) {
    var grid = jQuery("#tbl_cotizacion_detalle"),
            filas = grid.jqGrid('getDataIDs'),
            data;

    //var preciototal1 = grid.jqGrid('getCol', 'preciototal', false, 'sum');

    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);

        var id_insumo = grid.jqGrid('getCell', filas[i], 'id_insumo');

        if (id_insumo === codinsumo) {

            grid.jqGrid('setCell', filas[i], "estado", 1);

        }

    }

    var grid_crear_cotizacion = jQuery("#tbl_cotizacion");
    var ids = grid_crear_cotizacion.jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var cl = ids[i];

        var id_insumo = grid_crear_cotizacion.getRowData(cl).id_insumo;

        if (id_insumo === codinsumo) {

            be = "<img src='/fintra/images/botones/iconos/obligatorio.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Terminado'  onclick=\"mensajeConfirmAction1('Esta seguro de cambiar el estado a pendiente?','250','150',ActualizarEstadoPendienteDetalle,'" + id_insumo + "');\">";

            grid_crear_cotizacion.jqGrid('setRowData', ids[i], {estado: be});
        }
    }

}

function mensajeConfirmAction1(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta1');
    $("#msj1").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta1").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}