$(function () {
    maximizarventana();
    cargarLineasNegocio();
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $("#buscar_solicitud").click(function () {
        cargarInfoSolicitudes();
    });
    $('#solicitud').keyup(function (e) {
        if (e.keyCode == 13)
        {
            cargarInfoSolicitudes();
        }
    });
    $('#nom_insumo').keyup(function (e) {
        if (e.keyCode == 13)
        {
            grid_insumo();
        }
    });
    $('#txt_foms').keyup(function (e) {
        if (e.keyCode == 13)
        {
            cargarInfoSolicitudes();
        }
    });
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    });
    ordenarCombo('linea_negocio');
    autocompletar("txt_nom_cliente", 1);
    autocompletar("txt_nom_proyecto", 2);


    //limpiarCombos(0);

    $('#area').change(function () {
        limpiarCombos(1);
        cargarComboGenerico('disciplina', 2, $('#area').val());

    });

    $('#disciplina').change(function () {
        limpiarCombos(2);
        cargarComboGenerico('capitulo', 3, $('#disciplina').val());

    });

    $('#capitulo').change(function () {
        limpiarCombos(3);
        cargarComboGenerico('actividad', 4, $('#capitulo').val());

    });

    $('#buscar_insumo').click(function () {
        grid_insumo();
    });

    cargarComboCausales(1);
    cargando_toggle();

});


function cargarInfoSolicitudes() {
    var grid_tabla = jQuery("#tabla_infoSolicitud");
    if ($("#gview_tabla_infoSolicitud").length) {
        reloadGridInfoSolicitudes(grid_tabla, 39);
    } else {
        grid_tabla.jqGrid({
            caption: "SOLICITUDES",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '450',
            width: '1600',
            colNames: ['Idsolicitud', 'Fecha creacion', 'Responsable', 'Nombre Cliente', 'Nombre Proyecto', 'Foms', 'Costo Contratista', 'Valor Cotizacion', 'Etapa', 'Estado',
                'Codigo cliente', 'Tipo solicitud', 'Estado_cartera', 'Fecha validacion cartera', 'Interventor', 'Flag', 'id_trazabilidad', 'Id estado', 'presupuesto_terminado', 'Acciones'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'creation_date', index: 'creation_date', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'responsable', index: 'responsable', width: 180, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nomcli', index: 'nomcli', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 230, sortable: true, align: 'left', search: true},
                {name: 'num_os', index: 'num_os', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'valor_cotizacion', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'nombre_etapa', index: 'nombre_etapa', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_estado', index: 'nombre_estado', width: 160, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'codcli', index: 'codcli', width: 90, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'estado_cartera', index: 'estado_cartera', width: 100, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'fecha_validacion_cartera', index: 'fecha_validacion_cartera', width: 200, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'interventor2', index: 'interventor2', width: 110, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'flag', index: 'flag', width: 40, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'trazabilidad', index: 'trazabilidad', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_estado', index: 'id_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'presupuesto_terminado', index: 'presupuesto_terminado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 138, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ignoreCase:true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                mostrarFormulario(rowid);
            }, gridComplete: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila;
                var estado_trazabilidad;
                var id_estado, iva_aiu = '';
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);
                    estado_trazabilidad = fila.trazabilidad;
                    id_estado = fila.id_estado;
                    presupuesto_terminado = fila.presupuesto_terminado;


                    //var Cotizar = "<img src='/fintra/images/opav/Cotizacion_grey.png' align='absbottom'   style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Definicion de Rentabilidad y Esquema' );\">";
                    ca = "<img src='/fintra/images/opav/canasta.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Canasta'  onclick=\"abrir_div_modo_liberacion('" + cl + "');\">";

                    grid_tabla.jqGrid('setRowData', ids[i], {actions: ca});

                }
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 39,
                    lineaNegocio: $('#linea_negocio').val(),
                    responsable: $('#responsable').val(),
                    solicitud: $('#solicitud').val(),
                    estadoCartera: '', //$('#estadocartera').val(),
                    fechaInicio: $('#fechaini').val(),
                    fechafin: $('#fechafin').val(),
                    trazabilidad: '1',
                    etapaActual: $('#etapaActual').val(),
                    id_cliente: $('#id_cliente').val(),
                    nom_proyecto: $('#txt_nom_proyecto').val(),
                    foms: $('#txt_foms').val(),
                    tipo_proyecto: $('#tipo_proyecto').val()

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
    }
}



function reloadGridInfoSolicitudes(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                lineaNegocio: $('#linea_negocio').val(),
                responsable: $('#responsable').val(),
                solicitud: $('#solicitud').val(),
                estadoCartera: '', //$('#estadocartera').val(),
                fechaInicio: $('#fechaini').val(),
                fechafin: $('#fechafin').val(),
                trazabilidad: '1',
                etapaActual: $('#etapaActual').val(),
                id_cliente: $('#id_cliente').val(),
                nom_proyecto: $('#txt_nom_proyecto').val(),
                foms: $('#txt_foms').val(),
                tipo_proyecto: $('#tipo_proyecto').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function abrir_div_modo_liberacion(id_solicitud) {

    $("#div_modo_liberacion").dialog({
        title: "Modo De Liberacion",
        width: 350,
        height: 140,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
                abrir_canasta(id_solicitud, $('#modo_liberacion').val());
            }
        }
    });
}

function abrir_canasta(id_solicitud, op) {
    $('#id_solicitud').val(id_solicitud);
    limpiarCombos(0);
    console.log(op);
    switch (op) {
        case '0':
            mostrarContenedor(5);
            break;
        case '1':
            mostrarContenedor(2);
            break;
        default :

            break;
    }
    //
}

function mostrarContenedor(i) {
    $("#contenedor1").removeClass("show");
    $("#contenedor2").removeClass("show");
    $("#contenedor3").removeClass("show");
    $("#contenedor4").removeClass("show");
    $("#contenedor5").removeClass("show");
    $("#contenedor6").removeClass("show");

    setTimeout(function () {
        $("#contenedor" + i).addClass("show");
    }, 600);


}

function cargarComboGenerico(idCombo, op, param) {
    if (param == '') {
        limpiarCombos(op);
    } else {
        var elemento = $('#' + idCombo);
        $.ajax({
            url: "/fintra/controlleropav?estado=Modulo&accion=Canasta",
            datatype: 'json',
            type: 'GET',
            data: {opcion: 1, op: op, param: param},
            async: false,
            success: function (json) {
                try {
                    if (json.error) {
                        mensajesDelSistema(json.error, '300', 'auto', false);
                    } else {
                        elemento.html('');
                        elemento.append('<option value="">' + "Todos" + '</option>');
                        console.log(json);
                        for (var e in json) {
                            elemento.append('<option value="' + e + '" >' + json[e] + '</option>');
                        }
                    }
                } finally {
                }
            }
        });
    }
}


function limpiarCombos(x) {

    switch (x) {
        case 0:
            cargarComboGenerico('area', 1, $('#id_solicitud').val());
            $("#disciplina").html('');
            $("#capitulo").html('');
            $("#actividad").html('');
            $("#disciplina").append('<option value="">' + "Todos" + '</option>');
            $("#capitulo").append('<option value="">' + "Todos" + '</option>');
            $("#actividad").append('<option value="">' + "Todos" + '</option>');

        case 1:
            $("#disciplina").html('');
            $("#capitulo").html('');
            $("#actividad").html('');
            $("#disciplina").append('<option value="">' + "Todos" + '</option>');
            $("#capitulo").append('<option value="">' + "Todos" + '</option>');
            $("#actividad").append('<option value="">' + "Todos" + '</option>');

            break;
        case 2:


            $("#capitulo").html('');
            $("#actividad").html('');
            $("#capitulo").append('<option value="">' + "Todos" + '</option>');
            $("#actividad").append('<option value="">' + "Todos" + '</option>')
            break;

        case 3:

            $("#actividad").html('');
            $("#actividad").append('<option value="">' + "Todos" + '</option>');
            break;

    }


}

function cargarContenidoProyecto(proceso) {


    var opc, cond = '';
    if ($('#area').val() == '') {
        opc = 0;
    } else if ($('#disciplina').val() === '') {
        opc = 1;
        cond = $('#area').val();
    } else if ($('#capitulo').val() === '') {
        opc = 2;
        cond = $('#disciplina').val();
    } else if ($('#actividad').val() === '') {
        opc = 3;
        cond = $('#capitulo').val();
    } else {
        opc = 4;
        cond = $('#actividad').val();
    }

    switch (proceso) {
        case 1:
            grid_general(proceso, opc, cond);
            break;
        case 2 :
            grid_apus_bloquear(proceso, 0, '');
            break;

    }

}

//grilla General.
function grid_general(proceso, opc, cond) {

    $('#tabla_apu').jqGrid('GridUnload');

    var grid_tabla = jQuery("#tabla_apu");
    grid_tabla.jqGrid({
        caption: "Proyecto - Selectrick",
        url: '/fintra/controlleropav?estado=Modulo&accion=Canasta',
        datatype: "json",
        height: '550',
        width: '1590',
        colNames: ['id', 'id_disciplina_area', 'Area', 'id_disciplina', 'Displina', 'id_capitulo', 'Capitulo', 'id_actividad', 'Actividad',
            'id_actividades_capitulo', 'id_rel_actividades_apu', 'id_apu', 'APU', 'unidad_medida_apu', 'Unidad Medida', 'Cant. APU',
            'Valor APU', 'Total APU'],
        colModel: [
            {name: 'id', index: 'id', width: 10, sortable: true, align: 'left', hidden: true, search: true, key: true},
            {name: 'id_disciplina_area', index: 'id_disciplina_area', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_area', index: 'descripcion_area', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_disciplina', index: 'id_disciplina', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_displina', index: 'descripcion_displina', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_capitulo', index: 'id_capitulo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_capitulo', index: 'descripcion_capitulo', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_actividad', index: 'id_actividad', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_actividad', index: 'descripcion_actividad', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_actividades_capitulo', index: 'id_actividades_capitulo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'id_rel_actividades_apu', index: 'id_rel_actividades_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'id_apu', index: 'id_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_apu', index: 'descripcion_apu', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'unidad_medida_apu', index: 'unidad_medida_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'nombre_unidad_medida_apu', index: 'nombre_unidad_medida_apu', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'cantidad_apu', index: 'cantidad_apu', width: 5, sortable: true, align: 'right', hidden: true, search: true, sorttype: 'numeric'},
            {name: 'valor_apu_presupuesto', index: 'valor_apu_presupuesto', editable: false, align: 'right', width: 10, sorttype: 'currency', hidden: true, formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'total_apu_presupuesto', index: 'total_apu_presupuesto', editable: false, align: 'right', width: 10, sorttype: 'currency', hidden: true, formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$"}}
        ],
        rowNum: 1000000,
        rowTotal: 1000000,
        loadonce: true,
        rownumWidth: 60,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: false,
        rownumbers: false,
        pager: '#pager_tabla_ejecucion',
        multiselect: false,
        multiboxonly: false,
        pgtext: null,
        pgbuttons: false,
        editurl: 'clientArray',
        subGrid: false,
        ignoreCase:true,
        ondblClickRow: function (rowid, iCol, e) {


            cargar_insumos_apu(rowid);
            $("#id_apu").val(grid_tabla.getRowData(rowid).id_apu);
            mostrarContenedor(3);





        }, gridComplete: function () {

        },
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 2,
                id_solicitud: $('#id_solicitud').val(),
                opc: opc,
                cond: cond,
                proceso: proceso

            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 150);
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                mensajesDelSistema("No se encontraron registros", '204', '140', false);
            }
        }

    }).navGrid("#pager_tabla_apu", {add: false, edit: false, del: false, search: false, refresh: false}, {
    });
    grid_tabla.jqGrid('setGroupHeaders', {
        useColSpanStyle: false,
        groupHeaders: [
            {startColumnName: 'id_disciplina_area', numberOfColumns: 14, titleText: '<H5  style="text-align: center;">WORK BREAKDOWN STRUCTURE</H5>'},
            {startColumnName: 'cantidad_apu', numberOfColumns: 3, titleText: '<H5 style="text-align: center;">PRESUPUESTADO</H5>'},
            {startColumnName: 'cantidad_apu_actual', numberOfColumns: 2, titleText: '<H5 style="text-align: center;">ACTUAL</H5>'},
            {startColumnName: 'cantidad_apu_ejecutado', numberOfColumns: 2, titleText: '<H5 style="text-align: center;">EJECUTADO</H5>'}
        ]
    });




}

////////////////////////////////////////////////////////////////////////////OPCION DE LIBERACION POR APU /////////////////////////////////////////////////////////////////////////////////////////////////////////////

//grilla de insumos.
function cargar_insumos_apu(rowid) {
    $("#tabla_insumo").jqGrid('GridUnload');
    var grid_tabla = $("#tabla_insumo");
    var grid_tabla2 = $("#tabla_apu");


    grid_tabla.jqGrid({
        caption: "LISTA DE INSUMOS : " + grid_tabla2.getRowData(rowid).descripcion_apu,
        url: '/fintra/controlleropav?estado=Modulo&accion=Canasta',
        datatype: "json",
        height: '270',
        width: '1024',
        colNames: ['id_relacion_cotizacion_detalle_apu', 'Id Insumo', 'tipo_insumo', 'Descripcion', 'unidad_medida_insumo',
            'Unidad Medida', 'Cantidad', 'Rendimiento', 'Valor', 'Cantidad Insumo',
            'Valor Insumo'],
        colModel: [
            {name: 'id_relacion_cotizacion_detalle_apu', index: 'id_relacion_cotizacion_detalle_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true, key: true},
            {name: 'id_insumo', index: 'id_insumo', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'tipo_insumo', index: 'tipo_insumo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_insumo', index: 'descripcion_insumo', width: 25, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'unidad_medida_insumo', index: 'unidad_medida_insumo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'nombre_unidad_insumo', index: 'nombre_unidad_insumo', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'cantidad_insumo', index: 'cantidad_insumo', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'rendimiento_insumo', index: 'rendimiento_insumo', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'costo_personalizado', index: 'costo_personalizado', editable: false, align: 'left', width: 10, sorttype: 'currency', hidden: true, formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'cantidad_insumo_total', index: 'cantidad_insumo_total', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'valor_insumo_total', index: 'valor_insumo_total', editable: false, align: 'left', width: 10, sorttype: 'currency', hidden: true, formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}


        ],
        rowNum: 1000000,
        rowTotal: 1000000,
        loadonce: true,
        rownumWidth: 60,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: false,
        rownumbers: false,
        pager: '#pager_tabla_insumo',
        multiselect: true,
        multiboxonly: false,
        pgtext: null,
        pgbuttons: false,
        editurl: 'clientArray',
        subGrid: false,
        ignoreCase:true,
        ondblClickRow: function (rowid, iRow, iCol, e) {




        }, gridComplete: function () {
//            footersum(grid_tabla);
//                var ids = grid_tabla.jqGrid('getDataIDs');
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    ed = "<img src='/fintra/images/link_new.png' align='absbottom'  name='editar' id='editar' width='16' height='16' title ='editar cantidades'  onclick=\"editar_cantidades('" + cl + "');\">";
////                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='16' height='16' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan datos asociados a la subcategoria, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
//                    grid_tabla.jqGrid('setRowData', ids[i], {actions: ed});
//                }

        },
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        ajaxGridOptions: {
            data: {
                opcion: 9,
                id_solicitud: $('#id_solicitud').val(),
                id_rel_actividades_apu: grid_tabla2.getRowData(rowid).id_rel_actividades_apu,
                id_apu: grid_tabla2.getRowData(rowid).id_apu,
                unidad_medida_apu: grid_tabla2.getRowData(rowid).unidad_medida_apu

            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 150);
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                mensajesDelSistema("No se encontraron registros", '204', '140', false);
            }
        }

    }).navGrid('#pager_tabla_insumo', {add: false, edit: false, del: false, search: false, refresh: false}, {});
    grid_tabla.navButtonAdd('#pager_tabla_insumo', {
        caption: "Atras",
        title: "Atras",
        buttonicon: "ui-icon-seek-prev",
        onClickButton: function () {
            limpiarCombos(0);
            mostrarContenedor(2);
        }

    });

    grid_tabla.navButtonAdd('#pager_tabla_insumo', {
        caption: "Liberar",
        title: "Liberar",
        buttonicon: "ui-icon-minusthick",
        onClickButton: function () {
            var x = $("#tabla_insumo").jqGrid('getGridParam', 'selarrrow')
            var y = [];
            for (i = 0; i < x.length; i++) {
                y.push($("#tabla_insumo").getRowData(x[i]).id_insumo);
            }
            $('#id_insumos').val(y);
            cargarContenidoProyecto(2);
            mostrarContenedor(4);



        }

    });

    grid_tabla.jqGrid('setGroupHeaders', {
        useColSpanStyle: false,
        groupHeaders: [
            {startColumnName: 'id_relacion_cotizacion_detalle_apu', numberOfColumns: 9, titleText: '<H6  style="text-align: center;">INSUMO</H6>'},
            {startColumnName: 'cantidad_insumo_total', numberOfColumns: 2, titleText: '<H6  style="text-align: center;">PRESUPUESTADO</H6>'}
//            {startColumnName: 'cantidad_insumo_actual', numberOfColumns: 2, titleText: '<H6 style="text-align: center;">ACTUAL</H6>'},
//            {startColumnName: 'cantidad_insumo_ejecutado', numberOfColumns: 2, titleText: '<H6 style="text-align: center;">EJECUTADO</H6>'}
        ]
    });




}

//grilla para escoger los apus que van hacer anulados.
function grid_apus_bloquear(proceso, opc, cond) {

    $('#tabla_apu_bloqueo').jqGrid('GridUnload');

    var grid_tabla = jQuery("#tabla_apu_bloqueo");

    grid_tabla.jqGrid({
        caption: "Proyecto - Selectrick",
        url: '/fintra/controlleropav?estado=Modulo&accion=Canasta',
        datatype: "json",
        height: '550',
        width: '1590',
        colNames: ['id', 'id_disciplina_area', 'Area', 'id_disciplina', 'Displina', 'id_capitulo', 'Capitulo', 'id_actividad', 'Actividad',
            'id_actividades_capitulo', 'id_rel_actividades_apu', 'id_apu', 'APU', 'unidad_medida_apu', 'Unidad Medida', 'Cant. APU',
            'Valor APU', 'Total APU'],
        colModel: [
            {name: 'id', index: 'id', width: 10, sortable: true, align: 'left', hidden: true, search: true, key: true},
            {name: 'id_disciplina_area', index: 'id_disciplina_area', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_area', index: 'descripcion_area', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_disciplina', index: 'id_disciplina', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_displina', index: 'descripcion_displina', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_capitulo', index: 'id_capitulo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_capitulo', index: 'descripcion_capitulo', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_actividad', index: 'id_actividad', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_actividad', index: 'descripcion_actividad', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_actividades_capitulo', index: 'id_actividades_capitulo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'id_rel_actividades_apu', index: 'id_rel_actividades_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'id_apu', index: 'id_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_apu', index: 'descripcion_apu', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'unidad_medida_apu', index: 'unidad_medida_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'nombre_unidad_medida_apu', index: 'nombre_unidad_medida_apu', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'cantidad_apu', index: 'cantidad_apu', width: 5, sortable: true, align: 'right', hidden: true, search: true, sorttype: 'numeric'},
            {name: 'valor_apu_presupuesto', index: 'valor_apu_presupuesto', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', hidden: true, sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'total_apu_presupuesto', index: 'total_apu_presupuesto', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', hidden: true, sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$"}}
        ],
        rowNum: 1000000,
        rowTotal: 1000000,
        loadonce: true,
        rownumWidth: 60,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: false,
        rownumbers: false,
        pager: '#pager_tabla_apu_bloqueo',
        multiselect: true,
        multiboxonly: false,
        pgtext: null,
        pgbuttons: false,
        editurl: 'clientArray',
        subGrid: false,
        ignoreCase:true,
        ondblClickRow: function (rowid, iCol, e) {



        }, gridComplete: function () {

        },
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 3,
                id_solicitud: $('#id_solicitud').val(),
                id_apu: $('#id_apu').val()

            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 150);
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                mensajesDelSistema("No se encontraron registros", '204', '140', false);
            }
        }

    }).navGrid("#pager_tabla_apu_bloqueo", {add: false, edit: false, del: false, search: false, refresh: false}, {
    });
    grid_tabla.jqGrid('setGroupHeaders', {
        useColSpanStyle: false,
        groupHeaders: [
            {startColumnName: 'id_disciplina_area', numberOfColumns: 14, titleText: '<H5  style="text-align: center;">WORK BREAKDOWN STRUCTURE</H5>'},
            {startColumnName: 'cantidad_apu', numberOfColumns: 3, titleText: '<H5 style="text-align: center;">PRESUPUESTADO</H5>'},
            {startColumnName: 'cantidad_apu_actual', numberOfColumns: 2, titleText: '<H5 style="text-align: center;">ACTUAL</H5>'},
            {startColumnName: 'cantidad_apu_ejecutado', numberOfColumns: 2, titleText: '<H5 style="text-align: center;">EJECUTADO</H5>'}
        ]
    });

    grid_tabla.navButtonAdd('#pager_tabla_apu_bloqueo', {
        caption: "Atras",
        title: "Atras",
        buttonicon: "ui-icon-seek-prev",
        onClickButton: function () {
            mostrarContenedor(3);
        }

    });

    grid_tabla.navButtonAdd('#pager_tabla_apu_bloqueo', {
        caption: "Liberar",
        title: "Liberar",
        buttonicon: "ui-icon-minusthick",
        onClickButton: function () {
            var x = $("#tabla_apu_bloqueo").jqGrid('getGridParam', 'selarrrow');
            var y = [];
            if ((x.length > 0) && ($('#cbx_causal').val() != '')) {
                for (i = 0; i < x.length; i++) {
                    y.push($("#tabla_apu_bloqueo").getRowData(x[i]));
                    bloquear_apus(y);
                }
            }
            else {
                toastr.error("Por favor seleccione una por lo menos un apu y una Causal", "Error");
            }

        }

    });




}

function bloquear_insumos_apu(y) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Modulo&accion=Canasta',
        dataType: 'json',
        async: false,
        data: {
            opcion: 12,
            informacion: JSON.stringify({id_solicitud: $('#id_solicitud').val(), id_causal: $('#cbx_causal_2').val(), id_insumo: $('#id_insumo').val(), unidad_medida_insumo: $('#id_unidad_medida_insumo').val(), apus_insumos_bloquear: y})
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }

            if (json.respuesta == 'OK') {
                toastr.success("Liberacion exitosa.", "Exito");
                $('#tbl_insumos').jqGrid('GridUnload');
                $('#tbl_apu_bloqueo').jqGrid('GridUnload');
                $('#nom_insumo').val('');

                mostrarContenedor(1);
            }


        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
function bloquear_apus(y) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Modulo&accion=Canasta',
        dataType: 'json',
        async: false,
        data: {
            opcion: 4,
            informacion: JSON.stringify({id_solicitud: $('#id_solicitud').val(), id_causal: $('#cbx_causal').val(), id_insumos: $('#id_insumos').val(), apus_bloquear: y})
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }

            if (json.respuesta == 'OK') {
                toastr.success("Liberacion exitosa.", "Exito");
                mostrarContenedor(1);
            }


        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

///////////////////////////////////////////////////////////////////////FIN OPCION DE LIBERACION POR APU /////////////////////////////////////////////////////////////////////////////////////////////////////////////
function cargarComboCausales(op) {

    if (op == 0) {
        var elemento = $('#cbx_causal');
    } else {
        var elemento = $('#cbx_causal_2');
    }

    $.ajax({
        url: "/fintra/controlleropav?estado=Modulo&accion=Canasta",
        datatype: 'json',
        type: 'GET',
        data: {opcion: 1, op: 5, param: ''},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    elemento.append('<option value="">' + "Seleccione una causal" + '</option>');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '" >' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

////////////////////////////////////////////////////////////////////////////OPCION DE LIBERACION POR INSUMOS /////////////////////////////////////////////////////////////////////////////////////////////////////////////
//grilla Insumo.
function grid_insumo() {

    $('#tbl_insumos').jqGrid('GridUnload');

    var grid_tabla = jQuery("#tbl_insumos");
    grid_tabla.jqGrid({
        caption: "INSUMOS DEL PROYECTO - SELECTRICK",
        url: '/fintra/controlleropav?estado=Modulo&accion=Canasta',
        datatype: "json",
        height: '550',
        width: '1590',
        colNames: ['id_insumo', 'Codigo', 'Nombre', 'id_unidad_medida', 'Unidad De Medida', 'Cantidad', 'Precio', 'Total'],
        colModel: [
            {name: 'id', index: 'id', width: 5, sortable: true, align: 'left', hidden: true, search: true, key: true},
            {name: 'codigo_insumo', index: 'codigo_insumo', width: 5, sortable: true, align: 'center', hidden: false, search: true},
            {name: 'descripcion_insumo', index: 'descripcion_insumo', width: 13, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_unidad_medida', index: 'id_unidad_medida', width: 5, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'nombre_unidad_insumo', index: 'nombre_unidad_insumo', width: 5, sortable: true, align: 'center', hidden: false, search: true},
            {name: 'cantidad_presupuestada', index: 'cantidad_presupuestada', editable: false, align: 'right', width: 5, sorttype: 'currency', hidden: false, formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ","}},
            {name: 'costo_personalizado', index: 'costo_personalizado', editable: false, align: 'right', width: 5, sorttype: 'currency', hidden: false, formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$"}},
            {name: 'total', index: 'total', editable: false, align: 'right', width: 5, sorttype: 'currency', hidden: false, formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$"}}
        ],
        rowNum: 1000000,
        rowTotal: 1000000,
        loadonce: true,
        rownumWidth: 60,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: false,
        rownumbers: false,
        pager: '#pager_tbl_insumos',
        multiselect: false,
        multiboxonly: false,
        pgtext: null,
        pgbuttons: false,
        editurl: 'clientArray',
        subGrid: false,
        ignoreCase:true,
        ondblClickRow: function (rowid, iCol, e) {

            var fila = grid_tabla.getRowData(rowid);
            $("#id_insumo").val(fila.id);
            $("#id_unidad_medida_insumo").val(fila.id_unidad_medida);
            $("#codigo_insumo").val(fila.codigo_insumo);
            $("#descripcion_insumo").val(fila.descripcion_insumo);
            $("#nombre_unidad_insumo").val(fila.nombre_unidad_insumo);
            cargarComboCausales(0);
            grid_apu_bloqueo();
            mostrarContenedor(6);






        }, gridComplete: function () {

        },
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 10,
                id_solicitud: $('#id_solicitud').val(),
                nom_insumo: $('#nom_insumo').val()

            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 150);
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                mensajesDelSistema("No se encontraron registros", '204', '140', false);
            }
        }

    }).navGrid("#pager_tabla_apu", {add: false, edit: false, del: false, search: false, refresh: false}, {
    });
    grid_tabla.jqGrid('setGroupHeaders', {
        useColSpanStyle: false,
        groupHeaders: [
            {startColumnName: 'id_disciplina_area', numberOfColumns: 14, titleText: '<H5  style="text-align: center;">WORK BREAKDOWN STRUCTURE</H5>'},
            {startColumnName: 'cantidad_apu', numberOfColumns: 3, titleText: '<H5 style="text-align: center;">PRESUPUESTADO</H5>'},
            {startColumnName: 'cantidad_apu_actual', numberOfColumns: 2, titleText: '<H5 style="text-align: center;">ACTUAL</H5>'},
            {startColumnName: 'cantidad_apu_ejecutado', numberOfColumns: 2, titleText: '<H5 style="text-align: center;">EJECUTADO</H5>'}
        ]
    });




}


//grilla apu_bloqueo.
function grid_apu_bloqueo() {

    $('#tbl_apu_bloqueo').jqGrid('GridUnload');

    var grid_tabla = jQuery("#tbl_apu_bloqueo");
    grid_tabla.jqGrid({
        caption: "Proyecto - Selectrick",
        url: '/fintra/controlleropav?estado=Modulo&accion=Canasta',
        datatype: "json",
        height: '550',
        width: '1590',
        colNames: ['id', 'id_disciplina_area', 'Area', 'id_disciplina', 'Displina', 'id_capitulo', 'Capitulo', 'id_actividad', 'Actividad',
            'id_actividades_capitulo', 'id_rel_actividades_apu', 'id_apu', 'APU', 'unidad_medida_apu', 'Unidad Medida', 'Cantidad Presupuestada', 'Cantidad Disponible', 'A Liberar'],
        colModel: [
            {name: 'id', index: 'id', width: 10, sortable: true, align: 'left', hidden: true, search: true, key: true},
            {name: 'id_disciplina_area', index: 'id_disciplina_area', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_area', index: 'descripcion_area', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_disciplina', index: 'id_disciplina', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_displina', index: 'descripcion_displina', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_capitulo', index: 'id_capitulo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_capitulo', index: 'descripcion_capitulo', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_actividad', index: 'id_actividad', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_actividad', index: 'descripcion_actividad', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_actividades_capitulo', index: 'id_actividades_capitulo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'id_rel_actividades_apu', index: 'id_rel_actividades_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'id_apu', index: 'id_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_apu', index: 'descripcion_apu', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'unidad_medida_apu', index: 'unidad_medida_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'nombre_unidad_medida_apu', index: 'nombre_unidad_medida_apu', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'cantidad_presupuestada', index: 'cantidad_presupuestada', editable: false, align: 'right', width: 10, sorttype: 'currency', hidden: false, formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
            {name: 'cantidad_disponible', index: 'cantidad_disponible', editable: false, align: 'right', width: 10, sorttype: 'currency', hidden: false, formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
            {name: 'liberar', index: 'liberar', editable: true, align: 'right', width: 10, sorttype: 'currency', hidden: false, formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}}
        ],
        rowNum: 1000000,
        rowTotal: 1000000,
        loadonce: true,
        rownumWidth: 60,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: false,
        rownumbers: false,
        pager: '#pager_tbl_apu_bloqueo',
        multiselect: true,
        multiboxonly: false,
        pgtext: null,
        pgbuttons: false,
        editurl: 'clientArray',
        subGrid: false,
        ignoreCase:true,
        ondblClickRow: function (rowid, iCol, e) {

            jQuery("#tbl_apu_bloqueo").jqGrid('editRow', rowid, true, function () {
            }, null, null, {}, function (rowid) {
                cargando_toggle();

                var fila = grid_tabla.getRowData(rowid);
                console.log(fila['liberar']);
                console.log(fila['cantidad_disponible']);
                if (fila['liberar'] > fila['cantidad_disponible']) {
                    grid_tabla.jqGrid('setCell', rowid, "liberar", 0);
                    toastr.error("Era mayor la cantidad a liberar que la cantidad disponible", "Error");
                }
                cargando_toggle();
            });
            return;




        }, gridComplete: function () {

        },
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 11,
                id_solicitud: $('#id_solicitud').val(),
                id_insumo: $('#id_insumo').val(),
                id_unidad_medida_insumo: $('#id_unidad_medida_insumo').val()

            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 150);
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                mensajesDelSistema("No se encontraron registros", '204', '140', false);
            }
        }

    }).navGrid("#pager_tbl_apu_bloqueo", {add: false, edit: false, del: false, search: false, refresh: false}, {
    });
    grid_tabla.jqGrid('setGroupHeaders', {
        useColSpanStyle: false,
        groupHeaders: [
            {startColumnName: 'id_disciplina_area', numberOfColumns: 14, titleText: '<H5  style="text-align: center;">WORK BREAKDOWN STRUCTURE</H5>'},
            {startColumnName: 'cantidad_apu', numberOfColumns: 3, titleText: '<H5 style="text-align: center;">PRESUPUESTADO</H5>'},
            {startColumnName: 'cantidad_apu_actual', numberOfColumns: 2, titleText: '<H5 style="text-align: center;">ACTUAL</H5>'}
        ]
    });
    grid_tabla.navButtonAdd('#pager_tbl_apu_bloqueo', {
        caption: "Liberar",
        title: "Liberar",
        buttonicon: "ui-icon-minusthick",
        onClickButton: function () {
            var x = $("#tbl_apu_bloqueo").jqGrid('getGridParam', 'selarrrow');
            var y = [];
            if ((x.length > 0) && ($('#cbx_causal_2').val() != '')) {
                for (i = 0; i < x.length; i++) {
                    y.push($("#tbl_apu_bloqueo").getRowData(x[i]));
                }
                bloquear_insumos_apu(y);
            }
            else {
                toastr.error("Por favor seleccione una por lo menos un apu y una Causal", "Error");
            }

        }

    });

    grid_tabla.navButtonAdd('#pager_tbl_apu_bloqueo', {
        caption: "ALL IN",
        title: "ALL IN",
        buttonicon: "ui-icon-minusthick",
        onClickButton: function () {
            var x = $("#tbl_apu_bloqueo").jqGrid('getDataIDs');
            var fila ;
            for (i = 0; i < x.length; i++) {
              fila = $("#tbl_apu_bloqueo").getRowData(x[i]);
              $("#tbl_apu_bloqueo").jqGrid('setCell', fila.id, "liberar", fila.cantidad_disponible);

            }

        }

    });




}


////////////////////////////////////////////////////////////////////////FIN OPCION DE LIBERACION POR INSUMOS /////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**********************************************************************************************************************************************************
 Utiles Generales
 ***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = [];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function comas_numeros(campo) {
    var variable = $('#' + campo);
    variable.val(numberConComas(variable.val()));
}

function cargarCombo(id, op) {
    var elemento = $('#' + id);
    $.ajax({
        url: "/fintra/controlleropav?estado=Cotizacion&accion=Sl",
        datatype: 'json',
        type: 'GET',
        data: {opcion: 23, op: op},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    elemento.append('<option value=0>' + "..." + '</option>');
                    for (var e in json) {
                        //elemento.append('<option value="' + e + '" onclick="CargarGridApus>' + json[e] + '</option>');
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function mensajeConfirmacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajeConfirmacion2(msj, width, height, okAction, id, id2) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id, id2);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}
/**********************************************************************************************************************************************************
 Fin Utiles Generales
 ***********************************************************************************************************************************************************/
