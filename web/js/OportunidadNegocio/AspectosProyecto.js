/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    cargando_toggle();
    alert($("#idsolicitud").val());
//    maximizarventana();
//    $("#fechaini").val('');
//    $("#fechafin").val('');
//    $("#fecha").val('');
//    $("#fechaini").datepicker({
//        dateFormat: 'yy-mm-dd',
//        changeMonth: true,
//        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
//        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
//        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
//    });
//    $("#fechafin").datepicker({
//        dateFormat: 'yy-mm-dd',
//        changeMonth: true,
//        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
//        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
//        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
//    });
//    $("#fecha").datepicker({
//        dateFormat: 'yy-mm-dd',
//        changeMonth: true,
//        //minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
//        minDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
//        //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
//        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
//    });
//    var myDate = new Date();
//    $('#ui-datepicker-div').css('clip', 'auto');
//    cargarLineasNegocio();
//    agregarclases();
//    $('.solo-numero').keyup(function () {
//        this.value = (this.value + '').replace(/[^0-9]/g, '');
//    });
//    $("#buscar_solicitud").click(function () {
//        var fechainicio = $("#fechaini").val();
//        var fechafin = $("#fechafin").val();
//        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
//            cargarInfoSolicitudes();
//        } else {
//            mensajesDelSistema("Por favor revice el rango de fechas", '410', '150', false);
//        }
//    });
//    $('.mayuscula').change(function () {
//        this.value = this.value.toUpperCase();
//    });
//    $('.mayuscula').css({
//        'text-transform': 'uppercase'
//    });
//    ordenarCombo('linea_negocio');
//    autocompletar("txt_nom_cliente", 1);
//    autocompletar("txt_nom_proyecto", 2);
});




/**********************************************************************************************************************************************************
                                                            Tabla Solicitudes
***********************************************************************************************************************************************************/
function cargarInfoSolicitudes() {
    var grid_tabla = jQuery("#tabla_infoSolicitud");
    if ($("#gview_tabla_infoSolicitud").length) {
        reloadGridInfoSolicitudes(grid_tabla, 39);
    } else {
        grid_tabla.jqGrid({
            caption: "SOLICITUDES",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '500',
            width: '1590',
            colNames: ['Fecha creacion', 'Nombre Cliente', 'Nombre Proyecto', 'Foms', 'Idsolicitud', 'Costo Contratista','Valor Cotizacion','Etapa', 'Estado',
                'Codigo cliente', 'Tipo solicitud', 'Estado_cartera', 'Fecha validacion cartera', 'Responsable', 'Interventor', 'Flag', 'id_trazabilidad', 'Id estado',  'presupuesto_terminado', 'Acciones'],
            colModel: [
                {name: 'creation_date', index: 'creation_date', width: 130, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nomcli', index: 'nomcli', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 230, sortable: true, align: 'left', search: true},
                {name: 'num_os', index: 'num_os', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'valor_cotizacion', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'nombre_etapa', index: 'nombre_etapa', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_estado', index: 'nombre_estado', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'codcli', index: 'codcli', width: 90, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'estado_cartera', index: 'estado_cartera', width: 100, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'fecha_validacion_cartera', index: 'fecha_validacion_cartera', width: 200, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'responsable', index: 'responsable', width: 180, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'interventor2', index: 'interventor2', width: 110, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'flag', index: 'flag', width: 40, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'trazabilidad', index: 'trazabilidad', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_estado', index: 'id_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'presupuesto_terminado', index: 'presupuesto_terminado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 185, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila;
                var estado_trazabilidad;
                var id_estado, iva_aiu ='';
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
//                    fila = grid_tabla.jqGrid("getLocalRow", cl);
//                    estado_trazabilidad = fila.trazabilidad;
//                    id_estado = fila.id_estado;
//                    presupuesto_terminado = fila.presupuesto_terminado;
//                                        
//                    
//                    var Cotizar = '', accion = '', generar_cot;
//                    if (id_estado == '4' || id_estado == '5' || id_estado == '8') {
//                        Cotizar = "<img src='/fintra/images/opav/Cotizacion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Cotizar'  onclick=\"principalAsignacionCostos('" + cl + "');\">";
//                            iva_aiu ="<img src='/fintra/images/opav/iva_aiu.png' align='absbottom' name='IVA_AIU' id='AYF'value='IVA_AIU'  width='26' height='26' title ='IVA - AIU'  onclick=\"abrir_Iva_Aiu('" + cl + "');\">";
//                    }else{
//                        Cotizar = "<img src='/fintra/images/opav/Cotizacion_grey.png' align='absbottom'   style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Cotizar' );\">";
//                        iva_aiu ="<img src='/fintra/images/opav/iva_aiu_grey.png' align='absbottom' name='IVA_AIU' id='AYF'value='IVA - AIU'  width='26' height='26' title ='IVA - AIU'>";
//                        
//                    }
//                    
//                    //generar_cot = "<img src='/fintra/images/opav/Contrato.png' align='absbottom' name='contrato' id='Generar_COT' value='Generar_COT'  width='26' height='26' title ='Generar Cotizaci�n'  onclick=\"generarCotizacion('" + cl + "');\">";
//                    
//                    
//                    if (fila.trazabilidad=='1') {
//                        accion = "<img src='/fintra/images/opav/Transicion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";    
//                    }else{
//                        accion = "<img src='/fintra/images/opav/Transicion_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n' >";    
//                    }
//                    
//                    
//                    var Cargar_Archivos = "<img src='/fintra/images/opav/File_add.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Cargar Archivos'  onclick=\"divemergente_Carga_Archivos('" + cl + "');\">";

//                    grid_tabla.jqGrid('setRowData', ids[i], {actions: accion +  Cotizar + iva_aiu /*+ generar_cot */+ Cargar_Archivos });
                      //wbs = "<img src='/fintra/images/opav/Wbs.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='19' height='19' title ='WBS Ejecucion'  onclick=\"abrirWbs('" + cl + "');\">";
                      accion = "<img src='/fintra/images/opav/Transicion_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n' >";    
                      ExpT = "<img src='/fintra/images/opav/Exportar.png' align='absbottom' name='exportar_WBS_Project_Tareas' id='Exportar Project' value='Exportar_Project'  width='26' height='26' title ='Exportar Project Tareas'  onclick=\"exportarWBSProjectTareas('" + cl + "');\">";
                      ExpA = "<img src='/fintra/images/opav/Exportar.png' align='absbottom' name='exportar_WBS_Project_Asignacion' id='Exportar Project' value='Exportar_Project'  width='26' height='26' title ='Exportar Project Asignacion'  onclick=\"exportarWBSProjectAsignacion('" + cl + "');\">";
                      asp = "<img src='/fintra/images/opav/Exportar.png' align='absbottom' name='aspectos_Proyectos' id='aspectos_Proyectos' value='aspectos_Proyectos'  width='26' height='26' title ='Aspectos del Proyectos'  onclick=\"aspectos_Proyectos('" + cl + "');\">";
                    grid_tabla.jqGrid('setRowData', ids[i], {actions:accion +  ExpT +  ExpA});
                    
                }
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 39,
                    lineaNegocio: $('#linea_negocio').val(),
                    responsable: $('#responsable').val(),
                    solicitud: $('#solicitud').val(),
                    estadoCartera: '',
                    fechaInicio: $('#fechaini').val(),
                    fechafin: $('#fechafin').val(),
                    trazabilidad: '1',
                    etapaActual: $('#etapaActual').val(),
                    id_cliente: $('#id_cliente').val(),
                    nom_proyecto: $('#txt_nom_proyecto').val(),
                    foms: $('#txt_foms').val(),
                    tipo_proyecto: $('#tipo_proyecto').val()

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
    }
}

function exportarExcelTareas() {
    var fullData = jQuery("#tabla_exp_tareas").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga Tareas'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "/fintra/controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            listado: myJsonString,
            opcion: 24
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function aspectos_Proyectos(idsolicitud) {
        window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/OportunidadNegocio/&pagina=AspectosProyectos.jsp?idsolicitud=" + idsolicitud , '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
}

function exportarExcelAsignacion() {
    var fullData = jQuery("#tabla_exp_asignacion").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga Asignacion'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "/fintra/controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            listado: myJsonString,
            opcion: 25
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function ReloadexportarWBSProjectTareas(grid_tabla, op, id_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Modulo&accion=Planeacion',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: id_solicitud
            }
        },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }else{
                    exportarExcelTareas();
                }
            }
    });
    grid_tabla.trigger("reloadGrid");

}


function exportarWBSProjectTareas(id_solicitud){
    toastr.success(''+ id_solicitud,'Function Exportar');
    
    var grid_tabla = jQuery("#tabla_exp_tareas");
    if ($("#gview_tabla_exp_tareas").length) {
        ReloadexportarWBSProjectTareas(grid_tabla, 22, id_solicitud);
    } else {
        grid_tabla.jqGrid({
            caption: "Tareas",
            url: '/fintra/controlleropav?estado=Modulo&accion=Planeacion',
            datatype: "json",
            height: '500',
            width: '1590',
            colNames: ['Nombre', 'Nivel de Esquema', 'Costo WBS'],
            colModel: [
                {name: 'nombre', index: 'nombre', width: 130, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nivel', index: 'nivel', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'valor', index: 'valor', width: 230, sortable: true, align: 'left', hidden: false, search: true}
   
            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager_tarea',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 22,
                    id_solicitud: id_solicitud

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }else{
                    exportarExcelTareas();
                }
            }

        }).navGrid("#pager_tarea", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
    }
    //exportarExcelTareas();
    
}

function ReloadexportarWBSProjectAsignacion(grid_tabla, op, id_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Modulo&accion=Planeacion',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: id_solicitud
            }
        },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }else{
                    exportarExcelAsignacion();
                }
            }
    });
    grid_tabla.trigger("reloadGrid");
    
}

function exportarWBSProjectAsignacion(id_solicitud){
    //toastr.success(''+ id_solicitud,'Function Exportar');
    
    var grid_tabla = jQuery("#tabla_exp_asignacion");
    if ($("#gview_tabla_exp_asignacion").length) {
        ReloadexportarWBSProjectAsignacion(grid_tabla, 23, id_solicitud);
        //exportarExcelAsignacion();
    } else {
        grid_tabla.jqGrid({
            caption: "Asignacion",
            url: '/fintra/controlleropav?estado=Modulo&accion=Planeacion',
            datatype: "json",
            height: '500',
            width: '1590',
            colNames: ['Nombre', 'Nombre del Recurso', 'Cantidad','Rendimiento', 'Unidad','Cantidad de APU'],
            colModel: [
                {name: 'nombre', index: 'nombre', width: 130, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'recurso', index: 'recurso', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cantidad', index: 'valor', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'rendimiento', index: 'rendimiento', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'unidad', index: 'unidad', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cantidad_apu', index: 'cantidad_apu', width: 230, sortable: true, align: 'left', hidden: false, search: true}   
            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager_asignacion',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 23,
                    id_solicitud: id_solicitud

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }else{
                    exportarExcelAsignacion();
                }
            }

        }).navGrid("#pager_asignacion", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
    }
    //exportarExcelAsignacion();
}


/**********************************************************************************************************************************************************
                                                            Utiles Generales
***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon', '#perc_rete'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle(){
    $('#loader-wrapper').toggle();
}
/**********************************************************************************************************************************************************
                                                           Fin Utiles Generales
***********************************************************************************************************************************************************/