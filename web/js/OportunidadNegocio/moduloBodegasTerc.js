$(function () {
    $( "#dialog" ).dialog();
    maximizarventana();
    cargar_contratistas();
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    });
    //ordenarCombo('linea_negocio');
    //autocompletar("txt_nom_cliente", 1);
    
    
    $("#dep_dir").change(function () {        
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciu_dir");
    });
    
    // direcciones 
    cargarDepartamentos('CO', 'dep_dir');
    $('#dep_dir').val('ATL');
    cargarCiudad('ATL', "ciu_dir");
    $('#ciu_dir').val('BQ');
    cargarVias('BQ', "via_princip_dir");
    cargarVias('BQ', "via_genera_dir");
    cargando_toggle();
});


function cargar_contratistas() {
    var grid_tabla = jQuery("#tabla_contratista");
    if ($("#gview_tabla_contratista").length) {
        reloadGridContratistas(grid_tabla, 2);
    } else {
        grid_tabla.jqGrid({
            caption: "SELECTRIK - CONTRATISTAS",
            url: '/fintra/controlleropav?estado=Modulo&accion=Bodegas',
            datatype: "json",
            height: '450',
            width: '1600',
            colNames: ['Codigo Contratista', 'Contratista' , 'Nit' , 'Acciones'],
            colModel: [
                {name: 'id_contratista', index: 'id_contratista', width: 50, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'descripcion', index: 'descripcion', width: 300, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nit', index: 'nir', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 100, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: false,
            ignoreCase: true,
            pager: '#pager_tabla_contratista',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila , cargar_Bodegas;
                
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];                    
                    cargar_Bodegas= "<img src='/fintra/images/opav/File_add.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Agregar Bodega'  onclick=\"cargarBodegas('" + cl + "');\">";    
                    //cargar_Bodegas= "<img src='/fintra/images/opav/File_add.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Agregar Bodega'  onclick=\"AgregarBodega('" + cl + "');\">";
                    grid_tabla.jqGrid('setRowData', ids[i], {actions:  cargar_Bodegas});

                }
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 2
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager_tabla_contratista", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
    }
}

function reloadGridContratistas(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Modulo&accion=Bodegas',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function cargarBodegas(codigo_contratista) {
    $( "#modal_tabla_bodega" ).dialog({
        
    autoOpen: false,
    height: 450,
    width: 1600,
    modal: true,
    buttons: {
        'Crear bodega': function() {
            AgregarBodega(codigo_contratista);
        },
        'Cerrar': function() {
            $(this).dialog('close');
        }
    }}).dialog('open');

    $('#codigo_contratista').val(codigo_contratista);
    var grid_tabla = jQuery("#tabla_bodega");
    if ($("#gview_tabla_bodega").length) {
        reloadGridBodegas(grid_tabla, 3);
    } else {
        grid_tabla.jqGrid({
            caption: "SELECTRIK - BODEGAS",
            url: '/fintra/controlleropav?estado=Modulo&accion=Bodegas',
            datatype: "json",
            height: '200',
            width: '1580',
            colNames: ['id','reg_status','Tipo bodega','Descripcion','Id Contratista','Ciudad','Direccion','Nombre contacto','Cargo Contacto','Telefono1 contacto','Telefono contacto', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'reg_status', index: 'reg_status', width: 300, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'tipo_bodega', index: 'descripcion', width: 300, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'descripcion', index: 'descripcion', width: 300, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_contratista', index: 'id_contratista', width: 300, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cod_ciudad', index: 'cod_ciudad', width: 300, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'direccion', index: 'direccion', width: 300, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_contacto', index: 'nombre_contacto', width: 300, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cargo_contacto', index: 'cargo_contacto', width: 300, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'telefono1_contacto', index: 'telefono1_contacto', width: 300, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'telefono2_contacto', index: 'telefono2_contacto', width: 300, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 100, search: false, hidden: true}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            //shrinkToFit: false,
            //autowidth: true,
            footerrow: false,
            rownumbers: true,
            pager: '#pager_tabla_bodega',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 3,
                    codigo_contratista : $('#codigo_contratista').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    $("#gview_tabla_bodega").jqGrid('addRow',"new");                   
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager_tabla_bodega", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
    }
}

function reloadGridBodegas(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Modulo&accion=Bodegas',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                codigo_contratista : $('#codigo_contratista').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function AgregarBodega(id_contratista){
    $('#id_contratista').val(id_contratista);
    $("#div_tabla_bodegas").dialog({
        title : 'Bodegas de solicitud : '+ id_contratista ,
        width: 500,
        height: 600,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        show: "scale",
        closeOnEscape: false,
        resizable: false,
         show: { effect: "blind", duration: 500 },
        buttons: {
            "Aceptar": function () {
                campos = [$('#desc_bodega_terc').val(), $('#dir_resul').val(),
                          $('#nombre_contacto').val(), $('#cargo_contacto').val(),
                          $('#telefono1_contacto').val(), $('#telefono2_contacto').val(),
                          $('#ciu_dir').val()
                      ];
                campos = campos.map(function(x){ return x.toUpperCase() })
                if (campos.includes('')){
                  toastr.error("Faltan datos en el formulario");
                  return false;
                };
                $.ajax({
                    type: 'POST',
                    url: '/fintra/controlleropav?estado=Modulo&accion=Bodegas',
                    dataType: 'json',
                    async: false,
                    data: {
                        opcion: 4,
                        informacion: JSON.stringify({
                            descripcion: campos[0],
                            id_contratista: id_contratista,
                            cod_ciudad: campos[6],
                            direccion: campos[1],
                            nombre_contacto: campos[2],
                            cargo_contacto: campos[3],
                            telefono1_contacto: campos[4],
                            telefono2_contacto: campos[5]                   
                        })
                    },
                    success: function (json) {
                        if (!isEmptyJSON(json)) {
                            if (json.error) {
                                toastr.error(json.error, "Error");
                                return;
                            }
                            if (json.respuesta === "OK") {
                                toastr.success("Se genero de forma correcta la bodega", "Exito");                   
                                reloadGridBodegas(jQuery("#tabla_bodega"), 3);
                            } else {
                                toastr.error("Ha ocurrido un error", "Error");
                            }
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                                "Message: " + xhr.statusText + "\n" +
                                "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                });
                resetAddressValues();
                $(this).dialog("close");
                
            },
            "Salir": function () {
                resetAddressValues();
                $(this).dialog("close");
            }
            
        }
    });
}

function cargarDepartamentos(codigo, combo) {
    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            type: 'POST',
            async: false,
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 13,
                cod_pais: codigo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function cargarVias(codciu, combo) {
    //alert(codciu);

    $('#' + combo).empty();
    $.ajax({
        async: false,
        type: 'POST',
        url: "/fintra/controller?estado=GestionSolicitud&accion=Aval",
        dataType: 'json',
        data: {
            opcion: 'cargarvias',
            ciu: codciu
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    alert(json.error, '250', '180');
                    return;
                }
                try {
                    $('#' + combo).empty();
                    $('#' + combo).append("<option value=''></option>");

                    for (var key in json) {
                        $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    alert('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3:
            var res = document.getElementById('dir_resul')
                    , des = document.getElementById(res.name);
            des.value = res.value;


        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");

            document.getElementById("direccion_dialogo").style.display = "none";
            $("#div_pagaduria").css({
                'width': 'auto'
            });
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
                    , g = document.getElementById('via_genera_dir')
                    , opcion;
            for (var i = 0; i < g.length; i++) {

                opcion = g[i];

                if (opcion.value === p) {

                    opcion.style.display = 'none';
                    opcion.disabled = true;

                } else {

                    opcion.disabled = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if (!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                    || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                    || !jQuery('#placa_dir').val()) {
                jQuery('#dir_resul').val("");
                jQuery('#dir_resul').attr("class", "validation-failed");
            } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                        jQuery('#via_princip_dir option:selected').text()
                        + ' ' + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                        + ' ' + jQuery('#via_genera_dir option:selected').text().trim()
                        + ' ' + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                        + ((jQuery('#via_genera_dir option:selected').text().toUpperCase() === '#' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'TRANSVERSAL') ? '-' : ' ')
                        + jQuery('#placa_dir').val().trim().toUpperCase()
                        + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
            }
            break;
    }
}

function resetAddressValues() {
    $("#dir_resul").val('');
    $("#nom_princip_dir").val('');
    $("#nom_genera_dir").val('');
    $("#placa_dir").val('');
    $("#cmpl_dir").val('');
    $("#via_princip_dir").val('');
    $("#via_genera_dir").val('');
    $('#dep_dir').val('ATL');
    $('#ciu_dir').val('BQ');
}

/**********************************************************************************************************************************************************
 Utiles Generales
 ***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = [];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function comas_numeros(campo) {
    var variable = $('#' + campo);
    variable.val(numberConComas(variable.val()));
}

function cargarCombo(id, op) {
    var elemento = $('#' + id);
    $.ajax({
        url: "/fintra/controlleropav?estado=Cotizacion&accion=Sl",
        datatype: 'json',
        type: 'GET',
        data: {opcion: 23, op: op},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    elemento.append('<option value=0>' + "..." + '</option>');
                    for (var e in json) {
                        //elemento.append('<option value="' + e + '" onclick="CargarGridApus>' + json[e] + '</option>');
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function mensajeConfirmacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajeConfirmacion2(msj, width, height, okAction, id, id2) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id, id2);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}
/**********************************************************************************************************************************************************
 Fin Utiles Generales
 ***********************************************************************************************************************************************************/
