/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $("#fechaini").val('');
    $("#fechafin").val('');
    $("#fechaini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $('#ui-datepicker-div').css('clip', 'auto');

    //cargarLineasNegocio();
    //cargarInfoSolicitudes();


    $("#buscar").click(function () {
        var fechainicio = $("#fechaini").val();
        var fechafin = $("#fechafin").val();
        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
            cargarInfoSolicitudes();
        } else {
            mensajesDelSistema("Por favor revice el rango de fechas", '410', '150', false);
        }
    });

//    var opcion = $('#opcion_').val();
//    var id_solicitud = $('#idsolicitud_').val();
//    if (opcion === '1') {
//        cargarCuadroControl(id_solicitud);
//        $('#dialogMsjCuadroControl').show();
//        $('#tabla_cuadroControl').show();
//    } else if (opcion === '2') {
//        cargarFacturacion(id_solicitud);
//        cargarCotizacion(id_solicitud);
//        $('#dialogMsjFacturacion').show();
//        $('#tabla_facturacion').show();
//    } else if (opcion === '3') {
//        cargarInfoAnticipo(id_solicitud);
//        cargarAnticipo(id_solicitud);
//        $('#dialogMsjAnticipo').show();
//        $('#tabla_anticipo').show();
//    }
    var ids = jQuery("#tabla_anticipo").jqGrid('getDataIDs');
    if (ids > '0') {
        $('#pager3_left').hide();
    }


    maximizarventana();
    ordenarCombo('linea_negocio');
    autocompletar("txt_nom_cliente", 1);
    autocompletar("txt_nom_proyecto", 2);

});

function numberConComas2() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo'];

    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).val(numberConComas($(campos[i]).val()));
    }
}

function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function calculateAmortizacion(rowid) {
    // var num_records = jQuery("#tabla_facturacion").jqGrid('getGridParam', 'records');
    // = ((parseInt(num_records) > 0) ? (parseFloat(numberSinComas($('#val_anticipo').val())) / parseInt(num_records)) : 0);
    var ids = jQuery("#tabla_facturacion").jqGrid('getDataIDs');
    var total = parseFloat(numberSinComas($('#val_total').val()));
    var valor_amort, valor_retegarantia;
    var valor_total;
    var valor;
    var valor_pago;
    var valor_saldo;


    for (var i = 0; i < ids.length; i++) {
        var mandado_cliente = $("#tabla_facturacion").getRowData(ids[i]).mandado_cliente;
        if (mandado_cliente === 'NO' || mandado_cliente === '') {
            valor = $("#tabla_facturacion").getRowData(ids[i]).valor;
            valor_pago = $("#tabla_facturacion").getRowData(ids[i]).valor_pago;
            valor_saldo = valor - valor_pago;
            valor_amort = (valor * parseFloat(numberSinComas($('#perc_anticipo').val()))) / 100;
            valor_retegarantia = (valor * parseFloat(numberSinComas($('#perc_rete').val()))) / 100;
            valor_total = valor - valor_amort - valor_retegarantia;
            jQuery("#tabla_facturacion").jqGrid('setCell', ids[i], 'amortizacion', valor_amort);
            jQuery("#tabla_facturacion").jqGrid('setCell', ids[i], 'retegarantia', valor_retegarantia);
            jQuery("#tabla_facturacion").jqGrid('setCell', ids[i], 'total', valor_total);
            jQuery("#tabla_facturacion").jqGrid('setCell', ids[i], 'valor_saldo', valor_saldo);
            //jQuery("#tabla_facturacion").jqGrid('setCell', ids[i], 'total', valor_total);
            //calcularRetegarantia(rowid, ids[i], valor_total, valor);
            //alert('valor: ' + valor + ' amortizacion: ' + valor_amort + ' valor_total: ' + valor_total);
            var total_facturacion = jQuery("#tabla_facturacion").jqGrid('getCol', 'total', false, 'sum');
            if (total_facturacion > total) {
                setTimeout(function () {
                    mensajesDelSistema("EL VALOR DE LAS CUOTAS EXCEDE EL TOTAL", '350', '150');
                    recalcularAmortizacion(rowid);
                }, 500);
                $("#tabla_facturacion").jqGrid('delRowData', rowid);
                //alert(rowid);
                jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum');
                jQuery("#tabla_facturacion").jqGrid('getCol', 'amortizacion', false, 'sum');
                jQuery("#tabla_facturacion").jqGrid('getCol', 'retegarantia', false, 'sum');
            }
        }
    }
}

function recalcularAmortizacion(rowid) {
    var ids = jQuery("#tabla_facturacion").jqGrid('getDataIDs');
    var total = parseFloat(numberSinComas($('#val_total').val()));
    var valor_amort, valor_retegarantia;
    var valor_total;
    var valor;
    for (var i = 0; i < ids.length; i++) {
        var mandado_cliente = $("#tabla_facturacion").getRowData(ids[i]).mandado_cliente;
        if (mandado_cliente === 'NO' || mandado_cliente === '') {
            valor = $("#tabla_facturacion").getRowData(ids[i]).valor;
            valor_amort = (valor * parseFloat(numberSinComas($('#perc_anticipo').val()))) / 100;
            valor_retegarantia = (valor * parseFloat(numberSinComas($('#perc_rete').val()))) / 100;
            valor_total = valor - valor_amort - valor_retegarantia;
            jQuery("#tabla_facturacion").jqGrid('setCell', ids[i], 'amortizacion', valor_amort);
            jQuery("#tabla_facturacion").jqGrid('setCell', ids[i], 'retegarantia', valor_retegarantia);
            jQuery("#tabla_facturacion").jqGrid('setCell', ids[i], 'total', valor_total);
        }
    }
    calculateFooter();
}

function calculateFooter() {
    var colSumValor = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum');
    var colSumAmort = jQuery("#tabla_facturacion").jqGrid('getCol', 'amortizacion', false, 'sum');
    var colSumValorPagado = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor_pago', false, 'sum');
    var colSumValorSaldo = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor_saldo', false, 'sum');
    var colSumRet = jQuery("#tabla_facturacion").jqGrid('getCol', 'retegarantia', false, 'sum');
    var total = jQuery("#tabla_facturacion").jqGrid('getCol', 'total', false, 'sum');
    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor: colSumValor});
    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {amortizacion: colSumAmort});
    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {retegarantia: colSumRet});
    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor_pago: colSumValorPagado});
    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor_saldo: colSumValorSaldo});
    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {total: total});
}

function habdesAnt() {

    if ($('#anticipo').val() === '1') {
        $('#perc_anticipo').val();
        $('#val_anticipo').val();
        document.getElementById("perc_anticipo").disabled = false;

    } else {
        $('#perc_anticipo').val();
        $('#val_anticipo').val();
        document.getElementById("perc_anticipo").disabled = true;
    }
}

function cargarInfoSolicitudes() {
    var grid_tabla = jQuery("#tabla_infoSolicitud");
    if ($("#gview_tabla_infoSolicitud").length) {
        reloadGridInfoSolicitudes(grid_tabla, 39);
    } else {
        grid_tabla.jqGrid({
            caption: "SOLICITUDES",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '450',
            width: '1317',
            colNames: ['Fecha creacion', 'Nombre cliente', 'Nombre proyecto', 'Foms', 'Idsolicitud', 'Etapa', 'Valor Cotizacion',
                'Codigo cliente', 'Tipo solicitud', 'Estado_cartera', 'Fecha validacion cartera', 'Responsable', 'Interventor', 'Flag', 'id_trazabilidad', 'Id estado', 'Estado', 'presupuesto_terminado', 'Acciones'],
            colModel: [
                {name: 'creation_date', index: 'creation_date', width: 130, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nomcli', index: 'nomcli', width: 270, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 260, sortable: true, align: 'left', search: true},
                {name: 'num_os', index: 'num_os', width: 139, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'nombre_etapa', index: 'nombre_etapa', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'valor_cotizacion', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'codcli', index: 'codcli', width: 90, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'estado_cartera', index: 'estado_cartera', width: 100, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'fecha_validacion_cartera', index: 'fecha_validacion_cartera', width: 200, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'responsable', index: 'responsable', width: 180, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'interventor2', index: 'interventor2', width: 110, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'flag', index: 'flag', width: 40, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'trazabilidad', index: 'trazabilidad', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_estado', index: 'id_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'nombre_estado', index: 'nombre_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'presupuesto_terminado', index: 'presupuesto_terminado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 100, search: false}
            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: true,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                // mostrarFormulario2(rowid);
            }, gridComplete: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila;
                var estado_trazabilidad;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);
                    estado_trazabilidad = fila.trazabilidad;

                    //ed = "<img src='/fintra/images/opav/Transicion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
                    ed = "<img src='/fintra/images/opav/Transicion_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado' );\">";
                    grid_tabla.jqGrid('setRowData', ids[i], {actions: ed});
                }
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 39,
                    lineaNegocio: $("#linea_negocio").val(),
                    responsable: $("#responsable").val(),
                    solicitud: $("#solicitud").val(),
                    fechaInicio: $("#fechaini").val(),
                    fechafin: $("#fechafin").val(),
                    trazabilidad: '4',
                    etapaActual: $("#etapaActual").val(),
                    id_cliente: $('#id_cliente').val(),
                    nom_proyecto: $('#txt_nom_proyecto').val(),
                    foms: $('#txt_foms').val(),
                    tipo_proyecto: $('#tipo_proyecto').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
                $("tr.jqgrow", this).contextMenu('menu', {
                    bindings: {
                        'cuadro_control': function (trigger) {
                            var filas = $("#tabla_infoSolicitud").getRowData(trigger.id);
                            var id_solicitud = filas.id_solicitud;
                            mostrarCuadroControl(id_solicitud);
                        },
                        'anticipo': function (trigger) {
                            var filas = $("#tabla_infoSolicitud").getRowData(trigger.id);
                            var id_solicitud = filas.id_solicitud;
                            mostrarAnticipo(id_solicitud);
                        },
                        'facturacion': function (trigger) {
                            var filas = $("#tabla_infoSolicitud").getRowData(trigger.id);
                            var id_solicitud = filas.id_solicitud;
                            mostrarFacturacion(id_solicitud);
                        }
                    }, onContextMenu: function (event/*, menu*/) {
                        return true;
                    }
                });
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });

    }
}

function reloadGridInfoSolicitudes(grid_tablac, op) {
    grid_tablac.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                lineaNegocio: $("#linea_negocio").val(),
                responsable: $("#responsable").val(),
                solicitud: $("#solicitud").val(),
                fechaInicio: $("#fechaini").val(),
                fechafin: $("#fechafin").val(),
                trazabilidad: '4',
                etapaActual: $('#etapaActual').val(),
                id_cliente: $('#id_cliente').val(),
                nom_proyecto: $('#txt_nom_proyecto').val(),
                foms: $('#txt_foms').val(),
                tipo_proyecto: $('#tipo_proyecto').val()
            }
        }
    });
    grid_tablac.trigger("reloadGrid");
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function  mostrarCuadroControl(id_solicitud) {
    cargarCuadroControl(id_solicitud);
    $("#dialogMsjCuadroControl").dialog({
        width: '1065',
        height: '600',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function  mostrarFacturacion(id_solicitud) {
    cargarFacturacion(id_solicitud);
    cargarCotizacion(id_solicitud);
    $("#dialogMsjFacturacion").dialog({
        width: '1255',
        height: '500',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarCuadroControl(id_solicitud) {
    var grid_tabla = $("#tabla_cuadroControl");
    if ($("#gview_tabla_cuadroControl").length) {
        reloadGridCuadroControl(grid_tabla, 44, id_solicitud);
    } else {
        grid_tabla.jqGrid({
            caption: "Facturas Parciales",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '1000',
            colNames: ['Id', 'Valor', 'Dias Ejecucion', 'F. Facturacion', 'Dia Pago', 'Amortizaci�n', 'Retegarantia'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'valor', index: 'valor', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'dias_ejecucion', index: 'dias_ejecucion', width: 180, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_facturacion', index: 'fecha_facturacion', width: 180, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'dia_pago', index: 'dia_pago', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'valor_amortizacion', index: 'valor_amortizacion', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'valor_retegarantia', index: 'valor_retegarantia', width: 180, sortable: true, align: 'left', hidden: false, search: true}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    //  mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 99, //44,
                    id_solicitud: id_solicitud
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridCuadroControl(grid_tabla, op, id_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: id_solicitud
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function cargarFacturacion(id_solicitud) {
    var grid_tabla = jQuery("#tabla_facturacion");
    if ($("#gview_tabla_facturacion").length) {
        reloadGridFactuacion(grid_tabla, 50, id_solicitud);
    } else {
        grid_tabla.jqGrid({
            caption: "Facturas",
            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
            datatype: "json",
            height: '250',
            width: '1490',
            colNames: ['Id', 'Id solicitud', 'Cortes Viejos', '# Corte', '# Factura', 'F. Facturacion', 'F. Vencimiento', 'Valor', 'Valor pago', 'Valor saldo', 'Amortizaci�n', 'Retegarantia', 'Total factura', 'Mandado al cliente', 'Enviado al cliente', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 120, align: 'left', key: true, hidden: true},
                {name: 'id_solicitud', index: 'id_solicitud', width: 120, align: 'left', hidden: true},
                {name: 'foms_corte', index: 'id_solicitud', width: 120, align: 'left', hidden: false},
                {name: 'num_corte', index: 'num_corte', width: 120, align: 'left', hidden: false},
                {name: 'num_factura', index: 'num_factura', width: 120, align: 'left', hidden: false},
                {name: 'fecha_facturacion', index: 'fecha_facturacion', width: 110, align: 'center', sortable: false, search: false, editable: true, hidden: true, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'fecha_vencimiento', index: 'dia_pago', width: 110, align: 'center', sortable: false, search: false, editable: true, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'valor', index: 'valor', sortable: true, editable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'valor_pago', index: 'valor_pago', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'valor_saldo', index: 'valor_saldo', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'amortizacion', index: 'amortizacion', sortable: true, width: 140, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'retegarantia', index: 'retegarantia', sortable: true, width: 140, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'total', index: 'total', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'mandado_cliente', index: 'mandado_cliente', width: 100, align: 'center', hidden: true},
                {name: 'mandar_cliente', index: 'mandar_cliente', width: 100, align: 'left', hidden: false},
                {name: 'acciones', index: 'acciones', width: 100, align: 'left', hidden: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            pager: 'pager2',
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 50,
                    id_solicitud: id_solicitud
                }
            },
            loadComplete: function (rowid, e, iRow, iCol) {
                $("#tabla_facturacion").contextMenu('menu2', {
                    bindings: {
                        'eliminar': function (rowid) {
                            var myGrid = jQuery("#tabla_facturacion"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                            var mandado_cliente = $("#tabla_facturacion").getRowData(selRowIds).mandado_cliente;
                            if (mandado_cliente === '') {
                                $('#tabla_facturacion').jqGrid('delRowData', selRowIds);
                                $('#eliminar').removeAttr("disabled").removeClass('ui-state-disabled');
                            }
                        }
                    }, onContexMenu: function (event/*, menu*/) {

                    }
                });
            },
            gridComplete: function () {
                var grid_tabla_ = jQuery("#tabla_facturacion");
                var ids = jQuery("#tabla_facturacion").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var mandado_cliente = grid_tabla_.getRowData(cl).mandado_cliente;
                    var valor = grid_tabla_.getRowData(cl).valor;
                    var valor_pago = grid_tabla_.getRowData(cl).valor_pago;
                    var valor_saldo = valor - valor_pago;
                    jQuery("#tabla_facturacion").jqGrid('setCell', ids[i], 'valor_saldo', valor_saldo);
                    if (mandado_cliente === 'NO') {
                        be = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: 38px; height: 19px; vertical-align: middle;"onclick = "">';
                        man = "<img src='/fintra/images/cliente.png' style='height:16px;width:21px;margin-left: 43px;' type='button' id='mandar" + cl + "' title ='Enviar al cliente' onclick=\"preguntaMandarCliente('" + cl + "');\" />";
                        jQuery("#tabla_facturacion").jqGrid('setRowData', ids[i], {mandar_cliente: be});
                        jQuery("#tabla_facturacion").jqGrid('setRowData', ids[i], {acciones: man});
                    } else if (mandado_cliente === 'SI') {
                        be = '<img src = "/fintra/images/flag_green.gif"style = "margin-left: 38px; height: 19px; vertical-align: middle;"onclick = "">';
                        man = "<img src='/fintra/images/botones/iconos/delete.png' style='height:16px;width:21px;margin-left: 43px;' type='button' id='mandar" + cl + "' title ='Enviar al cliente' onclick=\"preguntaEliminarFactura('" + cl + "');\" />";
                        jQuery("#tabla_facturacion").jqGrid('setRowData', ids[i], {mandar_cliente: be});
                        jQuery("#tabla_facturacion").jqGrid('setRowData', ids[i], {acciones: man});
                    } else if (mandado_cliente === '') {
                        be = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: 38px; height: 19px; vertical-align: middle;"onclick = "">';
                        jQuery("#tabla_facturacion").jqGrid('setRowData', ids[i], {mandar_cliente: be});
                    }
                }
                var colSumValor = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum');
                var colSumAmort = jQuery("#tabla_facturacion").jqGrid('getCol', 'amortizacion', false, 'sum');
                var colSumValorPagado = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor_pago', false, 'sum');
                var colSumValorSaldo = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor_saldo', false, 'sum');
                var colSumRet = jQuery("#tabla_facturacion").jqGrid('getCol', 'retegarantia', false, 'sum');
                var total = jQuery("#tabla_facturacion").jqGrid('getCol', 'total', false, 'sum');
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor: colSumValor});
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {amortizacion: colSumAmort});
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {retegarantia: colSumRet});
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor_pago: colSumValorPagado});
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor_saldo: colSumValorSaldo});
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {total: total});
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var mandado_cliente = $("#tabla_facturacion").getRowData(rowid).mandado_cliente;
                if (mandado_cliente !== 'SI') {
                    jQuery("#tabla_facturacion").jqGrid('editRow', rowid, true, function () {
                    }, null, null, {}, function (rowid) {
                        var valor = $("#tabla_facturacion").getRowData(rowid).valor;
                        calculateAmortizacion(rowid);
                        calculateFooter();
                    });
                    return;
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid('#pager2', {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        jQuery("#tabla_facturacion").navButtonAdd('#pager2', {
            caption: "Agregar",
            title: "Agregar Facturacion",
            onClickButton: function () {
                var f = new Date();
                var fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
                var grid = $("#tabla_facturacion");
                var rowid = 'neo_' + grid.getRowData().length;
                var defaultData = {valor: 0, dias_ejecucion: 0, id_solicitud: id_solicitud, fecha_facturacion: fecha, fecha_vencimiento: fecha, amortizacion: 0, retegarantia: 0, mandado_cliente: '', total: 0, valor_saldo: 0, valor_pago: 0};
                grid.addRowData(rowid, defaultData);
                grid.jqGrid('editRow', rowid, true, function () {
                }, null, null, {}, function (rowid) {
                    var valor = $("#tabla_facturacion").getRowData(rowid).valor;
                    var valor_pago = $("#tabla_facturacion").getRowData(rowid).valor_pago;
                    var valor_saldo = valor - valor_pago;
                    jQuery("#tabla_facturacion").jqGrid('setCell', rowid, 'valor_saldo', valor_saldo);
//                    var total_facturacion = jQuery("#tabla_facturacion").jqGrid('getCol', 'total', false, 'sum');
//                    var total = parseFloat(numberSinComas($('#val_total').val()));
//                    alert('total_facturacion: ' + total_facturacion + ' total: ' + total);
//                    if (total_facturacion > total) {
//                        setTimeout(function () {
//                            mensajesDelSistema("EL VALOR DE LAS CUOTAS EXCEDE EL TOTAL", '320', '165');
//                        }, 500);
//                        $("#tabla_facturacion").jqGrid('delRowData', rowid);
//                        jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum');
//                        jQuery("#tabla_facturacion").jqGrid('getCol', 'amortizacion', false, 'sum');
//                        jQuery("#tabla_facturacion").jqGrid('getCol', 'retegarantia', false, 'sum');
//                        calculateAmortizacion(rowid);
//                        return;
//                    }
                    calculateAmortizacion(rowid);
                    //  jQuery("#tabla_facturacion").jqGrid('setCell', rowid, 'retegarantia', parseFloat((valor * $('#perc_rete').val()) / 100));
                    calculateFooter();
                });
            }
        });
        jQuery("#tabla_facturacion").navButtonAdd('#pager2', {
            caption: "Guardar",
            title: "Guardar Facturacion",
            onClickButton: function () {
                guardarFacturacion(id_solicitud);
            }
        });
    }
}

function reloadGridFactuacion(grid_tabla, op, id_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: id_solicitud
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function cargarCotizacion(id_solicitud) {
    $.ajax({
        async: false,
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 51,
            id_solicitud: id_solicitud
        },
        success: function (json) {
            document.getElementById('lb_facturacion').innerHTML = ('FACTURACION PROYECTO ' + json[0].nombre_proyecto + ' (' + json[0].num_os + ')');
            $('#val_total').val(numberConComas(json[0].total));
            if (json[0].anticipo === "1") {
                console.log('entro');
                $('#anticipo_f').val('No');
                $('#perc_anticipo').val((json[0].perc_anticipo));
                $('#val_anticipo').val(numberConComas(json[0].valor_anticipo));
                document.getElementById("perc_anticipo").disabled = false;
            } else {
                $('#anticipo').val('No');
                $('#perc_anticipo').val(0);
                $('#valor_anticipo').val(0);
                document.getElementById("perc_anticipo").disabled = true;
            }

            if (json[0].retegarantia === "1") {
                $('#retegarantia').val('Si');
                var porcretegarantia = json[0].perc_retegarantia;
                var valor_retegarantia = (json[0].total * porcretegarantia) / 100;
                $('#perc_rete').val(porcretegarantia);
                $('#val_retegarantia').val(numberConComas(valor_retegarantia));

                document.getElementById("perc_rete").disabled = false;
            } else {
                $('#retegarantia').val('No');
                $('#perc_rete').val(0);
                $('#val_retegarantia').val(0);
                document.getElementById("perc_rete").disabled = true;
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function preguntaMandarCliente(id) {
    mensajes("�Desea enviar al cliente?", '400', '150', true, id);
}

function mensajes(msj, width, height, swHideDialog, id) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                EnviarFacturaCliente(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function guardarFacturacion(id_solicitud) {
    var grid = jQuery("#tabla_facturacion"), filas = grid.jqGrid('getDataIDs');
    var data = jQuery("#tabla_facturacion").jqGrid('getRowData');
    for (var i = 0; i < filas.length; i++) {
        console.log(filas[i]);
        grid.saveRow(filas[i]);
    }
    if (data.length === 0) {
        mensajesDelSistema('Inserte al menos una fatura', '300', 'auto', false);
    } else {
        $.ajax({
            async: false,
            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 52,
                informacion: JSON.stringify({json: data})
            },
            success: function (json) {
                if (json.respuesta === 'Guardado') {
                    mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);
                    cargarCotizacion(id_solicitud);
                    cargarFacturacion(id_solicitud);
                } else {
                    mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }

}

function EnviarFacturaCliente(id) {
    var num_factura = $("#tabla_facturacion").getRowData(id).num_factura;
    var valor = $("#tabla_facturacion").getRowData(id).valor;
    var amortizacion = $("#tabla_facturacion").getRowData(id).amortizacion;
    var retegarantia = $("#tabla_facturacion").getRowData(id).retegarantia;
    var id_solicitud = $("#tabla_facturacion").getRowData(id).id_solicitud;
    $.ajax({
        async: false,
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 53,
            id: id,
            num_factura: num_factura,
            valor: valor,
            amortizacion: amortizacion,
            retegarantia: retegarantia
        },
        success: function (json) {
            if (json.respuesta === 'Guardado') {
                cargarCotizacion(id_solicitud);
                cargarFacturacion(id_solicitud);
                mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);
            } else {
                mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function preguntaEliminarFactura(id) {
    mensajeELiminacion("�Desea eliminar la factura?", '400', '150', true, id);
}

function mensajeELiminacion(msj, width, height, swHideDialog, id) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                eliminarFacturaCliente(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function eliminarFacturaCliente(id) {
    var num_factura = $("#tabla_facturacion").getRowData(id).num_factura;
    var id_solicitud = $("#tabla_facturacion").getRowData(id).id_solicitud;
    $.ajax({
        async: false,
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 54,
            id: id,
            num_factura: num_factura
        },
        success: function (json) {
            if (json.respuesta === 'Eliminado') {
                cargarCotizacion(id_solicitud);
                cargarFacturacion(id_solicitud);
                mensajesDelSistema('Exito al elimiar la factura numero: ' + num_factura, '300', 'auto', false);
            } else {
                cargarCotizacion(id_solicitud);
                cargarFacturacion(id_solicitud);
                mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function  mostrarAnticipo(id_solicitud) {
    cargarAnticipo(id_solicitud);
    cargarInfoAnticipo(id_solicitud);
    $("#dialogMsjAnticipo").dialog({
        width: '1550',
        height: '520',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarInfoAnticipo(id_solicitud) {
    $.ajax({
        async: false,
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 51,
            id_solicitud: id_solicitud
        },
        success: function (json) {
            $('#anticipo_').val(json[0].anticipo);
            if (json[0].anticipo === "1") {
                $('#perc_anticipo_').val((json[0].perc_anticipo));
                $('#val_anticipo_').val(numberConComas(json[0].valor_anticipo));
                document.getElementById("perc_anticipo_").disabled = false;
            } else {
                $('#perc_anticipo_').val(0);
                $('#valor_anticipo_').val(0);
                document.getElementById("perc_anticipo_").disabled = true;
            }
            $('#nombre_proyecto_').val(json[0].nombre_proyecto);
            $('#codcli_').val(json[0].cod_cli);
            $('#nomcli_').val(json[0].nonmbre_cliente);
            $('#val_subtotal_').val(numberConComas(json[0].subtotal));
            $('#codcot_').val(json[0].no_cotizacion);

        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarAnticipo(id_solicitud) {
    var grid_tabla = jQuery("#tabla_anticipo");
    if ($("#gview_tabla_anticipo").length) {
        reloadGridAnticipo(grid_tabla, 62, id_solicitud);
    } else {
        grid_tabla.jqGrid({
            caption: "Anticipos",
            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
            datatype: "json",
            height: '150',
            width: '1250',
            colNames: ['Id', 'Id solicitud', 'Numero Anticpo', 'Fecha anticipo', '# CXC', 'Valor CXC', 'F. CXC', '# CXP', 'Valor CXP', 'F. CXP', 'Valor anticpo', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 120, align: 'left', key: true, hidden: true},
                {name: 'id_solicitud', index: 'id_solicitud', width: 120, align: 'center', hidden: false},
                {name: 'cod_anticipo', index: 'cod_anticipo', width: 120, align: 'center', hidden: false},
                {name: 'fecha_anticipo', index: 'fecha_anticipo', width: 110, align: 'center', sortable: false, search: false, editable: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'num_factura', index: 'num_factura', width: 120, align: 'center', hidden: false},
                {name: 'valor', index: 'valor', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'fecha_factura', index: 'fecha_factura', width: 110, align: 'center', sortable: false, search: false, editable: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
//                {name: 'fecha_vencimiento', index: 'dia_pago', width: 110, align: 'center', sortable: false, search: false, editable: true, editrules: {date: true},
//                    editoptions: {dataInit: function (elem) {
//                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
//                        }}},
                {name: 'num_cxp', index: 'num_cxp', width: 120, align: 'center', hidden: false},
                {name: 'valorcxp', index: 'valorcxp', sortable: true, editable: false, width: 110, align: 'right', search: false, hidden: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'fecha_cxp', index: 'fecha_cxp', width: 110, align: 'center', sortable: false, search: false, editable: false, hidden: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'total', index: 'total', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'acciones', index: 'acciones', width: 100, align: 'left', hidden: true}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            pager: 'pager3',
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 62,
                    id_solicitud: id_solicitud
                }
            },
            loadComplete: function (rowid, e, iRow, iCol) {

            },
            gridComplete: function () {
                var grid_tabla_ = jQuery("#tabla_anticipo");
                var ids = jQuery("#tabla_anticipo").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    be = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: 38px; height: 19px; vertical-align: middle;"onclick = "">';
                    jQuery("#tabla_facturacion").jqGrid('setRowData', ids[i], {mandar_cliente: be});
                }
                var colSumValor = jQuery("#tabla_anticipo").jqGrid('getCol', 'valor', false, 'sum');
                var total = jQuery("#tabla_anticipo").jqGrid('getCol', 'total', false, 'sum');
                jQuery("#tabla_anticipo").jqGrid('footerData', 'set', {valor: colSumValor});
                jQuery("#tabla_anticipo").jqGrid('footerData', 'set', {total: total});
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

                jQuery("#tabla_anticipo").jqGrid('editRow', rowid, true, function () {
                }, null, null, {}, function (rowid) {

                    calculateFooterAnticipo();
                });
                return;
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid('#pager3', {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        jQuery("#tabla_anticipo").navButtonAdd('#pager3', {
            caption: "Agregar",
            title: "Agregar Anticipo",
            onClickButton: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    var f = new Date();
                    var fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
                    var grid = $("#tabla_anticipo");
                    var rowid = 'neo_' + grid.getRowData().length;
                    var valor_anticipo = numberSinComas($('#val_anticipo_').val());

                    var defaultData = {valor: valor_anticipo, id_solicitud: id_solicitud, fecha_factura: fecha, fecha_vencimiento: fecha, mandado_cliente: '', total: valor_anticipo, fecha_anticipo: fecha};
                    grid.addRowData(rowid, defaultData);
                    grid.jqGrid('editRow', rowid, true, function () {
                    }, null, null, {}, function (rowid) {
                        calculateFooterAnticipo();
                    });
                } else {
                    mensajesDelSistema("Solo se permite agregar una linea", '314', '140', false);
                }
            }
        });
        jQuery("#tabla_anticipo").navButtonAdd('#pager3', {
            caption: "Guardar",
            title: "Guardar Anticipos",
            onClickButton: function () {
                var respuesta = validarGuardado();
                if (respuesta === true) {
                    guardarAnticipo(id_solicitud);
                    //generarCuantasCobro();
                    //ventanaEmpresa();
                }
            }
        });
    }
}

function reloadGridAnticipo(grid_tabla, op, id_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: id_solicitud
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function calculateFooterAnticipo() {
    var sumValor = jQuery("#tabla_anticipo").jqGrid('getCol', 'valor', false, 'sum');
    var total = jQuery("#tabla_anticipo").jqGrid('getCol', 'total', false, 'sum');
    jQuery("#tabla_anticipo").jqGrid('footerData', 'set', {valor: sumValor});
    jQuery("#tabla_anticipo").jqGrid('footerData', 'set', {total: total});
}

function validarGuardado() {
    var estado = true;
    var ids = jQuery("#tabla_anticipo").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = jQuery("#tabla_anticipo").getRowData(ids[i]).id;
        //alert(id);
        if (id !== '') {
            estado = false;
            mensajesDelSistema('El registro ya ha sido guardado', '250', '150');
        } else {
            estado = true;
        }
    }
    return estado;
}

function guardarAnticipo(id_solicitud) {
    var cod_cli = $('#codcli_').val();
    var nomcli = $('#nomcli_').val();
    var codcot = $('#codcot_').val();
    var perc_anticipo = $('#perc_anticipo_').val();
    var val_anticipo = $('#val_anticipo_').val();
    var grid = jQuery("#tabla_anticipo"), filas = grid.jqGrid('getDataIDs');
    var data = jQuery("#tabla_anticipo").jqGrid('getRowData');
    for (var i = 0; i < filas.length; i++) {
        console.log(filas[i]);
        grid.saveRow(filas[i]);
    }
    if (data.length === 0) {
        mensajesDelSistema('Inserte al menos una fatura', '300', 'auto', false);
    } else {
        console.log('guardo');
//        $.ajax({
//            async: false,
//            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
//            type: 'POST',
//            dataType: 'json',
//            data: {
//                opcion: 63,
//                informacion: JSON.stringify({json: data, cod_cli: cod_cli, nomcli: nomcli, codcot: codcot, perc_anticipo: perc_anticipo, val_anticipo: numberSinComas(val_anticipo)})
//            },
//            success: function (json) {
//                if (json.respuesta === 'SI') {
//                    mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);
//                    cargarAnticipo(id_solicitud);
//                    cargarInfoAnticipo(id_solicitud);
//                    ventanaEmpresa();
//                    //generarCuantasCobro();
//                } else {
//                    mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
//                }
//            }, error: function (xhr, ajaxOptions, thrownError) {
//                $("#dialogLoading").dialog('close');
//                alert("Error: " + xhr.status + "\n" +
//                        "Message: " + xhr.statusText + "\n" +
//                        "Response: " + xhr.responseText + "\n" + thrownError);
//            }
//        });
    }

}

function generarCuantasCobro() {
    var cod_cli = $('#codcli_').val();
    var nomcli = $('#nomcli_').val();
    var codcot = $('#codcot_').val();
    var empresa = $('#empresa').val();
    var firma = $('#firma').val();
    var cuenta = $('#cuenta').val();
    var nombre_proyecto = $('#nombre_proyecto_').val();
    var perc_anticipo = $('#perc_anticipo_').val();
    var val_anticipo = $('#val_anticipo_').val();
    var grid = jQuery("#tabla_anticipo"), filas = grid.jqGrid('getDataIDs');
    var data = jQuery("#tabla_anticipo").jqGrid('getRowData');
    for (var i = 0; i < filas.length; i++) {
        console.log(filas[i]);
        grid.saveRow(filas[i]);
    }
    if (data.length === 0) {
        mensajesDelSistema('Inserte al menos una fatura', '300', 'auto', false);
    }
    $.ajax({
        type: 'POST',
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        dataType: 'json',
        data: {
            opcion: 64,
            informacion: JSON.stringify({json: data, cod_cli: cod_cli, nomcli: nomcli, codcot: codcot, perc_anticipo: perc_anticipo, val_anticipo: numberSinComas(val_anticipo), nombre_proyecto: nombre_proyecto, empresa: empresa, firma: firma, cuenta: cuenta})
        },
        success: function (data) {
            var res = data.respuesta;
            console.log(res);
            if (res === 'OK') {
                mensajesDelSistema("Exito al generar cuenta de cobro, Ver pdf en descargas", '400', '120', true);
            } else {

                mensajesDelSistema("Error al generar la cuenta de cobro", '363', '140', false);
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function ventanaEmpresa() {
    $("#dialogEmpresa").dialog({
        width: '513',
        height: '200',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        //title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
            $('.ui-dialog-titlebar').hide();
        },
        buttons: {
            "Generar pdf": function () {
                generarCuantasCobro();
                $(this).dialog("close");
            }
        }
    });
}
function funcionesEnticipos() {
    return {
        ventanaAnticipo: function (id_solicitud) {
            cargarAnticipo(id_solicitud);
            $("#dialogAnticipo").dialog({
                width: '1330',
                height: '400',
                show: "scale",
                hide: "scale",
                resizable: false,
                position: "center",
                modal: true,
                closeOnEscape: false,
                //title: '',
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                    $('.ui-dialog-titlebar').hide();
                },
                buttons: {
                    "cerra": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    };
}


