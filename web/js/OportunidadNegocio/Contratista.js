
$(document).ready(function () {
    maximizarventana();
    $("#fechaini").val('');
    $("#fechafin").val('');
    $("#fechaini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $('#ui-datepicker-div').css('clip', 'auto');

    limpiarFormularioC();
    $('#solicitud').keyup(function (e) {
        if (e.keyCode == 13)
        {
            cargarInfoSolicitudesP();
        }
    });
    $('#txt_foms').keyup(function (e) {
        if (e.keyCode == 13)
        {
            cargarInfoSolicitudesP();
        }
    });
    solonumeros();
    cargarCbxLineaNegocio();
    //cargarCbxTipoSolicitud();
    //cargarCbxResponsable();
    cargarCbx_Cartera();
    cargarCbxInterventor();
    cargarLineasNegocio();
    cargarEstadoCartera();
    var myDate = new Date();


    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $("#btn_Aceptar_tipoClienteP").click(function () {
        var fechainicio = $("#fechaini").val();
        var fechafin = $("#fechafin").val();
        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
            cargarInfoSolicitudesP();
        } else {
            mensajesDelSistema("Por favor revice el rango de fechas", '410', '150', false);
        }
    });



    $("#fecha_planeada_visita").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha_planeada_visita").datepicker("setDate", myDate);
    $("#fecha_ejecucion_visita").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha_ejecucion_visita").datepicker("setDate", myDate);

    $("#subirArchivo").click(function (e) {
        e.preventDefault();
        cargar_Archivos();
    });

    ordenarCombo('linea_negocio');
    ordenarCombo('estadocartera');
    autocompletar("txt_nom_cliente", 1);
    autocompletar("txt_nom_proyecto", 2);
    cargando_toggle();
});





function CargarDivDefAccionesAlcances() {
    $("#div_def_acciones_alcances").dialog({
        width: 830,
        height: 665,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Datos Solicitud',
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {

                asignarAlcances('', $('#cbx_TipoTrabajo2 option:selected').text(), $('#descripcion_accion').val());
                cargarGridAccionesAsoc($('#idSolicitud').val());

            },
            "Cancelar": function () {
                $(this).dialog("destroy");


            }

        }
    });
}

function AbriDivAccion() {
    $("#div_accion").dialog({
        width: 650,
        height: 300,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Agregar Accion',
        closeOnEscape: false,
        buttons: {
            "Agregrar": function () {

                crearAccion('', $('#cbx_TipoTrabajo2').val(), $('#descripcion_accion').val());
                cargarGridAccionesAsoc($('#idSolicitud').val());
                $('#descripcion_accion').val('');

            },
            "Cancelar": function () {
                $('#descripcion_accion').val('');
                $(this).dialog("destroy");



            }

        }
    });
}
function AbriDivAsociacionAccionApu() {
    $("#div_asociacion_accion_apu").dialog({
        width: 950,
        height: 400,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Asociacion De APU',
        closeOnEscape: false,
        buttons: {
            "Cancelar": function () {
                $("#escoger_apu").jqGrid("resetSelection");
                $("#apu_asociados").jqGrid("resetSelection");
                $(this).dialog("destroy");

            }

        }
    });
}

function AgregarAccion() {

    $('#div_accion').fadeIn("slow");
    AbriDivAccion();
    cargarCbxTipoTrabajo2();

}

function AsociarAPU(idaccion) {

    $('#div_asociacion_accion_apu').fadeIn("slow");
    AbriDivAsociacionAccionApu();
    cargarListaApu(idaccion);
    cargarApuAsociados(idaccion);
    $('#idaccion').val(idaccion);

}

function AgregarContratistas(cl) {
    $('#idaccion').val(cl);
    $('#div_escoger_contratistas').fadeIn("slow");
    AbrirDivEscogerContratistas();
    cargarListaContratistas();

}

function AbrirDivEscogerContratistas() {
    $("#div_escoger_contratistas").dialog({
        width: 500,
        height: 420,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Contratistas',
        closeOnEscape: false,
        buttons: {
            "Asignar": function () {
                var rowKey = $('#escoger_contratistas').getGridParam("selrow");
                var fila = jQuery("#escoger_contratistas").getRowData(rowKey);
                var idcontratista = fila['id_contratista'];
                var nomcontratista = fila['descripcion'];

                asignarContratista($('#idaccion').val(), idcontratista, nomcontratista);

                cargarGridAccionesAsoc($('#idSolicitud').val());
            },
            "Cancelar": function () {
                $('#descripcion_contratista').val('');
                $("#escoger_contratistas").jqGrid("resetSelection");
                $(this).dialog("destroy");


            }

        }
    });
}

function refrescarGridListaContratistas() {
    var ts = $('#cbx_TipoSolicitud option:selected').text();
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=21&ts=' + ts;
    jQuery("#escoger_contratistas").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#escoger_contratistas').trigger("reloadGrid");

}

function cargarListaContratistas() {
    var ts = $('#cbx_TipoSolicitud option:selected').text();
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=21&ts=' + ts;
    if ($("#gview_escoger_contratistas").length) {
        refrescarGridListaContratistas();
    } else {
        jQuery("#escoger_contratistas").jqGrid({
            caption: 'Contratistas',
            url: url,
            datatype: 'json',
            height: 214,
            width: 400,
            colNames: ['Id', 'Nombre'],
            colModel: [
                {name: 'id_contratista', index: 'id_contratista', sortable: true, align: 'center', width: '50px', key: true},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'center', width: '300px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            ignoreCase: true,
            pager: '#page_tabla_escoger_contratistas',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                var ids = jQuery("#escoger_contratistas").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/link_new.png' align='absbottom'  name='editar' id='editar' width='16' height='16' title ='editar especificacion'  onclick=\"CargarDivDefAccionesAlcances('" + cl + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='16' height='16' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan datos asociados a la subcategoria, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
                    jQuery("#subcategoria").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_escoger_contratistas", {edit: false, add: false, del: false});
        /*jQuery("#escoger_contratistas").jqGrid("navButtonAdd", "#page_tabla_escoger_contratistas", {
         caption: "Nuevo",
         title: "Agregar nuevo proceso interno",
         onClickButton: function () {
         crearProcesoInterno();
         }
         });*/
    }
}

function refrescarGridListaApu(idaccion) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=26&idaccion=' + idaccion;
    jQuery("#escoger_apu").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#escoger_apu').trigger("reloadGrid");
}

function cargarListaApu(idaccion) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=26&idaccion=' + idaccion;
    if ($("#gview_escoger_apu").length) {
        refrescarGridListaApu(idaccion);
    } else {
        jQuery("#escoger_apu").jqGrid({
            caption: 'APU',
            url: url,
            datatype: 'json',
            height: 214,
            width: 400,
            colNames: ['Id', 'Nombre'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden: true, align: 'center', width: '50px', key: true},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'center', width: '300px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            ignoreCase: true,
            hidegrid: false,
            multiselect: true,
            pager: '#page_tabla_escoger_apu',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                var ids = jQuery("#escoger_apu").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/link_new.png' align='absbottom'  name='editar' id='editar' width='16' height='16' title ='editar especificacion'  onclick=\"CargarDivDefAccionesAlcances('" + cl + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='16' height='16' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan datos asociados a la subcategoria, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
                    jQuery("#subcategoria").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_escoger_apu", {edit: false, add: false, del: false});

    }
}

function refrescarGridApuAsociados(idaccion) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=27&idaccion=' + idaccion;
    jQuery("#apu_asociados").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#apu_asociados').trigger("reloadGrid");
}

function cargarApuAsociados(idaccion) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=27&idaccion=' + idaccion;
    if ($("#gview_apu_asociados").length) {
        refrescarGridApuAsociados(idaccion);
    } else {
        jQuery("#apu_asociados").jqGrid({
            caption: 'APU Asociados',
            url: url,
            datatype: 'json',
            height: 240,
            width: 400,
            colNames: ['Id', 'IdAPU', 'Nombre'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden: true, align: 'center', width: '50px', key: true},
                {name: 'id_apu', index: 'id_apu', hidden: true, sortable: true, align: 'center', width: '300px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'center', width: '300px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            ignoreCase: true,
            pager: '#page_tabla_apu_asociados',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_apu_asociados", {edit: false, add: false, del: false});

    }
}

function crearAccion(idcontratista, idtipotrabajo, descripcion) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 22,
            idSolicitud: $('#idSolicitud').val(),
            idcontratista: idcontratista,
            descripcion: descripcion,
            idtipotrabajo: idtipotrabajo
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (json.respuesta === "OK") {

                    mensajesDelSistema("se asocio la contratista de forma correcta", '250', '150', true);
                    $('#acciones').trigger("reloadGrid");

                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function asignarAlcances(idcontratista, idtipotrabajo, descripcion) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 22,
            idSolicitud: $('#idSolicitud').val(),
            idcontratista: idcontratista,
            descripcion: descripcion,
            idtipotrabajo: idtipotrabajo
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (json.respuesta === "OK") {

                    mensajesDelSistema("se asocio la contratista de forma correcta", '250', '150', true);
                    //Recargar();

                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
function asignarContratista(idaccion, idcontratista, nomcontratista) {

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 33,
            idcontratista: idcontratista,
            idaccion: idaccion,
            nomcontratista: nomcontratista

        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (json.respuesta === "OK") {

                    mensajesDelSistema("se asocio la contratista de forma correcta", '250', '150', true);


                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function refrescarGridAccionesAsoc(idsolicitud) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=23&idsolicitud=' + idsolicitud;
    jQuery("#acciones").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#acciones').trigger("reloadGrid");
}

function cargarGridAccionesAsoc(idsolicitud) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=23&idsolicitud=' + idsolicitud;
    if ($("#gview_Acciones").length) {
        refrescarGridAccionesAsoc(idsolicitud);
    } else {
        jQuery("#acciones").jqGrid({
            caption: 'Listado De Acciones',
            url: url,
            type: "POST",
            datatype: 'json',
            height: 214,
            width: 850,
            scrollOffset: 30,
            colNames: ['Id', 'Contratista', 'Descripcion', 'Fecha Creacion', 'Estado', 'Tipo Trabajo', 'Acciones'],
            colModel: [
                {name: 'id_accion', index: 'id_accion', hidden: true, sortable: true, align: 'center', width: '100px', key: true},
                {name: 'nomcontratista', hidden: true, index: 'nomcontratista', sortable: true, align: 'center', width: '300px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'center', width: '300px'},
                {name: 'creation_date', index: 'creation_date', sortable: true, align: 'center', width: '150px'},
                {name: 'estado', index: 'estado', sortable: true, align: 'center', width: '100px'},
                {name: 'tipo_trabajo', clienteindex: 'tipo_trabajo', sortable: true, align: 'center', width: '150px'},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '135px'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            ignoreCase: true,
            pager: '#page_tabla_acciones',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            }, /* loadComplete: function () {
             $('#acciones').contextMenu('myMenu1', {
             bindings: {
             ver_detalle_pago: function (trigger, currentTarget) {
             /*var rowKey = $('#acciones').getGridParam("selrow");
             var fila = jQuery("#acciones").getRowData(rowKey);
             var procesado = fila['procesado_cartera'];
             if (procesado === "Si") {
             verInformacionPago(rowKey);
             } 
             alert("Entro 1");
             },
             ver_causal_dev: function (trigger, currentTarget) {
             /*var rowKey = $('#tabla_detalle_recaudo').getGridParam("selrow");
             var fila = jQuery("#tabla_detalle_recaudo").getRowData(rowKey);
             var encontrado = fila['encontrado'];
             var procesado = fila['procesado_cartera'];
             var causal = fila['causal_devolucion_proc'];
             if (encontrado === "Si" && procesado === "No") {                                    
             if (causal !== "") {
             obtenerCausalDevolucion(causal);
             }
             }
             
             alert("Entro 2");
             }
             }
             });
             },*/
            gridComplete: function () {
                var ids = jQuery("#acciones").jqGrid('getDataIDs');
                var co2, ed2, cont2, an2;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    co2 = "<img src='/fintra/images/botones/iconos/apu.png' align='absbottom'  name='apu' id='apu2' width='19' height='19' title ='APU'  onclick=\"Cotizacion('" + cl + "');\">";
                    /*ed2 = "<img src='/fintra/images/botones/iconos/adds.png' align='absbottom'  name='editar' id='editar2' width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"CargarDefAccionesAlcances('" + cl + "');\">";
                     cont2 = "<img src='/fintra/images/botones/iconos/contratista.png' align='absbottom'  name='contratista' id='contratista2' width='19' height='19' title ='Asignar Contratistas'  onclick=\"AgregarContratistas('" + cl + "');\">";
                     an2 = "<img src='/fintra/images/botones/iconos/delete2.png' align='absbottom'  name='anular' id='anular2' width='19' height='19' title ='Anular'  onclick=\"crearAPU();\">";*/
                    jQuery("#acciones").jqGrid('setRowData', ids[i], {actions: co2});
                }
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_acciones", {edit: false, add: false, del: false});
        /*jQuery("#acciones").jqGrid("navButtonAdd", "#page_tabla_acciones", {
         caption: "Agregar",
         title: "Agregar nueva acciones",
         onClickButton: function () {
         AgregarAccion();
         }
         });
         /*jQuery("#acciones").jqGrid('filterToolbar', "#searchacciones",
         {
         autosearch: false,
         searchOnEnter: false,
         defaultSearch: "cn",
         stringResult: true,
         ignoreCase: true,
         multipleSearch: true,
         gs_nombe: false
         
         });*/

        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function Cotizacion(cl) {
    var idaccion = cl;
    var url = '/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/&pagina=CotizacionSl.jsp&idaccion=' + idaccion;
//?idaccion='+idaccion;
    window.showModalDialog(url);
}

function principalAsignacionCostos(idsolicitud) {
    var url = "/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/OportunidadNegocio/Solicitud/&pagina=CotizacionHistorico.jsp?num_solicitud=" + idsolicitud;
    var win = window.open(url, '_blank');
    win.focus();
}

function abrir_Iva_Aiu(id_solicitud) {

    window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/OportunidadNegocio/&pagina=modalidadProyecto.jsp?num_solicitud=" + id_solicitud, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');

}



function CargarDefAccionesAlcances(cl) {
    var fila = jQuery("#acciones").getRowData(cl);
    $('#fechaAsignacion').val(fila['creation_date']);
    var idaccion = cl;
    var idsolicitud = $('#idSolicitud').val();
    cargarCbxTipoTrabajo();
    cargarAlcancesSolicitud(idaccion, idsolicitud);
    CargarDivDefAccionesAlcances();




}

function cargarCbxTipoTrabajo() {
    $('#cbx_TipoTrabajo').html('');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 24,
            tiposolicitud: $('#cbx_TipoSolicitud option:selected').text()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_TipoTrabajo').append('<option value="' + json[key].value + '">' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxTipoTrabajo2() {
    $('#cbx_TipoTrabajo2').html('');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 24,
            tiposolicitud: $('#cbx_TipoSolicitud option:selected').text()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {

                        $('#cbx_TipoTrabajo2').append('<option value="' + json[key].value + '">' + json[key].label + '</option>');

                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarAlcancesSolicitud(idaccion, idsolicitud) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 25,
            idsolicitud: idsolicitud,
            idaccion: idaccion
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }

                $('#descripcionn').val(json.acciones);
                $('#cbx_TipoTrabajo').val(json.tipo_trabajo);

                if (json.observacion === "") {
                    $('#row_obsAdicional').css({
                        'display': 'none'
                    });
                    $('.fecha_ejecu_visita').css({
                        'display': 'none'
                    });
                } else {
                    $('#observacion_accion').val(json.observacion);
                    $('#observacion_accion').attr('readonly', 'true');
                    $('#row_obsAdicional').css({
                        'display': 'none'
                    });
                    $('.fecha_ejecu_visita').css({
                        'display': ''
                    });

                }



            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxLineaNegocio() {
    $('#cbx_LineaDeNegocio').html('');
    $('#cbx_LineaDeNegocio').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 12
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_LineaDeNegocio').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function cargarCbxTipoSolicitud() {
    $('#cbx_TipoSolicitud').html('');
    $('#cbx_TipoSolicitud').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 13,
            idLineaDeNegocio: $("#cbx_LineaDeNegocio").val()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_TipoSolicitud').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxResponsable() {
    $('#cbx_Responsable').html('');
    $('#cbx_Responsable').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 14
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Responsable').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxInterventor() {
    $('#cbx_Interventor').html('');
    $('#cbx_Interventor').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 15
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Interventor').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbx_Cartera() {
    $('#cbx_Interventor').html('');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 20
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Cartera').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxNics(idcliente) {

    $('#cbx_nic').html('');
    $('#cbx_nic').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 11,
            idcliente: idcliente
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json.rows) {
                        $('#cbx_nic').append('<option value=' + json.rows[key].id_cliente + '>' + json.rows[key].nic + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }


        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function buscar() {
    if ($('#filtro').val() === "1") {
        var busqueda = $('#busqueda').val();
        cargarSolicitud(busqueda);


    } else if ($('#filtro').val() === "2") {
        var busqueda = $.trim($('#busqueda').val());
        if (!(busqueda === "")) {
            $.ajax({
                type: 'POST',
                url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
                dataType: 'json',
                async: false,
                data: {
                    opcion: 9,
                    busqueda: busqueda
                },
                success: function (json) {

                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        $('#idcliente').val(json.idcliente);
                        BuscarSolicitudesidcliente();
                        listarvalorespredeterminados(json.idcliente);
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }

    } else {
        BuscarSolicitudesidcliente();
        listarvalorespredeterminados($('#idcliente').val());
    }
}
function BuscarSolicitudesidcliente() {
    divemergente_Solicitudes();
    cargarSolicitudesIdcli($('#idcliente').val());
}
;

function divemergente_Solicitudes() {
    $('#div_Solicitidudes').dialog({
        width: 900,
        height: 385,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Solicitudes',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $('#div_Solicitidudes').show();
}

function refrescarGridSolicitudesNom(idcli) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=36&idcli=' + idcli;
    jQuery("#solicitudes").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#solicitudes').trigger("reloadGrid");
}

function cargarSolicitudesIdcli(idcli) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=36&idcli=' + idcli;
    if ($("#gview_solicitudes").length) {
        refrescarGridSolicitudesNom(idcli);
    } else {
        jQuery("#solicitudes").jqGrid({
            caption: 'Listado De Solicitudes',
            url: url,
            type: "POST",
            datatype: 'json',
            height: 214,
            width: 850,
            scrollOffset: 30,
            colNames: ['Id Solicitud', 'Tipo Solicitud', 'Fecha Creacion', 'Responsable'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', sortable: true, align: 'center', width: '280px'},
                {name: 'creation_date', index: 'creation_date', sortable: true, align: 'center', width: '150px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'center', width: '300px'}
                //{name: 'actions', index: 'actions', resizable: false, align: 'center', width: '135px'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            ignoreCase: true,
            pager: '#page_tabla_solicitudes',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                /*var ids = jQuery("#acciones").jqGrid('getDataIDs');
                 for (var i = 0; i < ids.length; i++) {
                 var cl = ids[i];
                 co = "<img src='/fintra/images/botones/iconos/apu.png' align='absbottom'  name='apu' id='apu' width='19' height='19' title ='APU'  onclick=\"AsociarAPU('" + cl + "');\">";
                 ed = "<img src='/fintra/images/botones/iconos/adds.png' align='absbottom'  name='editar' id='editar' width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"CargarDefAccionesAlcances('" + cl + "');\">";
                 cont = "<img src='/fintra/images/botones/iconos/contratista.png' align='absbottom'  name='contratista' id='contratista' width='19' height='19' title ='Asignar Contratistas'  onclick=\"AgregarContratistas('" + cl + "');\">";
                 an = "<img src='/fintra/images/botones/iconos/delete2.png' align='absbottom'  name='anular' id='anular' width='19' height='19' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan datos asociados a la subcategoria, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
                 jQuery("#acciones").jqGrid('setRowData', ids[i], {actions: co + '   ' + ed + '   ' + cont + '   ' + an});
                 }*/
            },
            loadError: function (xhr, status, error) {
                alert(error);
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                cargarSolicitud(rowid);
            }
        }).navGrid("#page_tabla_solicitudes", {edit: false, add: false, del: false});
        jQuery("#solicitudes").jqGrid("navButtonAdd", "#page_tabla_solicitudes", {
            caption: "Exportar",
            title: "Exportar a Excel",
            onClickButton: function () {
                alert('Exportar a Excel aun no esta listo');
            }
        });
        /*jQuery("#acciones").jqGrid('filterToolbar', "#searchacciones",
         {
         autosearch: false,
         searchOnEnter: false,
         defaultSearch: "cn",
         stringResult: true,
         ignoreCase: true,
         multipleSearch: true,
         gs_nombe: false
         
         });*/

        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}
function cargarSolicitud(busqueda) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 17,
            busqueda: busqueda
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (!($('#idSolicitud').val(json.id_solicitud) === '')) {
                    $('#idSolicitud').val(json.id_solicitud);
                    $('#idcliente').val(json.id_cliente);
                    cargarGridAccionesAsoc(json.id_solicitud);
                    if (json.estado_cartera === ' ') {
                        $('#cbx_Cartera').val('');
                    } else {
                        $('#cbx_Cartera').val(json.estado_cartera);
                    }
                    ;

                    $('#fechaCreacion').val(json.creation_date);
                    $('#historiasolicitud').val(json.descripcion);
                    $('#txt_responsable').val(json.nomresponsable);
                    $('#idresponsable').val(json.responsable);
                    $('#cbx_Interventor').val(json.interventor);
                    $('#fecha_limite3').val((json.fecha_limite).substring(0,11));
                    


                    buscarclienteid(json.id_cliente);
                    cargarCbxNics(json.id_cliente);
                    $('#cbx_LineaDeNegocio  ').val(json.lineanegocio);
                    //cargarLinieaNegocioTP(json.tipo_solicitud);
                    //cargarCbxTipoSolicitud();
                    $('#cbx_TipoSolicitud >option:contains(' + json.tipo_solicitud + ')').attr('selected', 'selected');
                    $('#cbx_nic >option:contains(' + json.nic + ')').attr('selected', 'selected');
                    if (json.aviso === '') {
                        $('#div_opd').css({'display': 'none'});
                    } else {
                        $('#div_opd').css({'display': ''});
                    }
                    $('#OPD').val(json.aviso);
                    $('#fformPrincipal').css({'display': ''});



                } else {
                    $('#fformPrincipal').css({'display': 'none'});
                    limpiarFormularioC();
                }


            } else {
                alert('No existe informacion relacionada con ');
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function buscarclienteid(idcliente) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 6,
            idcliente: idcliente
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }

//                console.log(json.nomcli);
                $('#idcliente').val(json.codcli);
                $('#txt_NombreCliente').val(json.nomcli);
                $('#NombreCliente').val(json.nomcli);
                $('#NitCliente').val(json.nit);
                $('#DigitoVerificacion').val(json.digito_verificacion);
                $('#NombreContacto').val(json.nomcontacto);
                $('#CelularContacto').val(json.celContacto);
                $('#DireccionCliente').val(json.direccion);
                cargarDepCiuid(json.ciudad);




            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function cargarLinieaNegocioTP(tiposolicitud) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 18,
            tiposolicitud: tiposolicitud
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }


                $('#cbx_LineaDeNegocio').val(json.value);



            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function cargarDepCiuid(idCiudad) {

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 8,
            idCiudad: idCiudad
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                for (var key in json) {
                    $('#Departamento').val(key);
                    $('#Ciudad').val(json[key]);

                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function autocompletarNombre() {
    $('#idcliente').val('');
    limpiarFormularioC();
    if (!($('#filtro').val() === "3")) {
        $("#busqueda").autocomplete({
            source: "",
            minLength: 2
        });

    } else {

        $("#busqueda").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: 'POST',
                    url: "./controlleropav?estado=Procesos&accion=Cliente",
                    dataType: "json",
                    data: {
                        q: request.term,
                        opcion: 4
                    },
                    success: function (data) {
                        // response( data );
                        response($.map(data, function (item) {
                            return {
                                label: item.label,
                                value: item.label,
                                mivar: item.value
                            };
                        }));
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $("#idcliente").val(ui.item.mivar);
                console.log(ui.item ?
                        "Selected: " + ui.item.mivar :
                        "Nothing selected, input was " + ui.item.label);
            },
            open: function () {
                //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
            },
            close: function () {

                // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
            }
        });
    }


}

function habilitar_form_principal(x) {
    $('#formPrincipal').css({
        'display': ''});

    $('#tipoCliente').attr('readonly', 'true');

    if (x === 1) {
        $('#div_clientepadre').css({
            'display': 'none'});
    } else {
        $('#div_clientepadre').css({
            'display': ''});
    }
    limpiarFormularioC();



}

function desabilitar_form() {
    limpiarFormularioC();
    $('#formPrincipal').css({
        'display': 'none'});
}

function cargarComboTipoCliente() {
    $('#tipoCliente').html('');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 0
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#tipoCliente').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });



}
//Carga los nombre del autocompletar Cliente Padre
function cargarClientesPadre() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    var clientesPadre = [];
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 4
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                for (j in json.rows) {
                    clientesPadre.push(json.rows[j].valor_02);
                }
                $("#ClientePadre").autocomplete({
                    source: clientesPadre,
                    minLength: 2
                });

            }
        }
    });
}

function modificarSolicitud() {
    if (CamposObligatorios()) {


        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            dataType: 'json',
            async: false,
            data: {
                opcion: 19,
                idcliente: $('#idCliente').val(),
                nic: $('#cbx_nic option:selected').text(),
                descripcion: $('#descripcion').val(),
                opd: $('#OPD').val(),
                responsable: $('#cbx_Responsable').val(),
                interventor: $('#cbx_Interventor').val(),
                lineanegocio: $('#cbx_LineaDeNegocio').val(),
                tiposolicitud: $('#cbx_TipoSolicitud').val(),
                idSolicitud: $('#idSolicitud').val(),
                estadoCartera: $('#cbx_Cartera').val()

            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    if (json.respuesta === "OK") {

                        mensajesDelSistema("Se Modifico la Solicitud de forma correcta", '250', '150', true);

                    }

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });


    } else {
        mensajesDelSistema("Datos Incompletos", '250', '180');
    }
}

function CamposObligatorios() {

    var campos = '';
    var DC = true;

    campos = ['#cbx_Responsable', '#cbx_Interventor', '#descripcion'];


    for (var i = 0; i < campos.length; i++) {
        if (($(campos[i]).val()) === '') {
            alert(campos[i]);
            DC = false;
            break;
        }

    }


    return DC;
}

function divemergente_nic() {
    $('#div_Nic').dialog({
        width: 400,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'N i cs',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $('#div_valorespredeterminados').show();
}

function limpiarFormularioC() {
    $('#NombreCliente').val('');
    $('#Ciudad').html('');
    $('#NitCliente').val('');
    $('#DireccionCliente').val('');
    $('#NombreContacto').val('');
    $('#EmailContacto').val('');
    $('#TelefonoContacto').val('');
    $('#CargoContacto').val('');
    $('#CelularContacto').val('');
    $('#NombreRepresentateLegal').val('');
    $('#TelRepresentateLegal').val('');
    $('#Nic').val('');
    $('#DigitoVerificacion').val('');
    $('#idDepartamento').val('');
    $('#Departamento').val('');
    $('#idCiudad').val('');
    $('#Ciudad').val('');
    $('#dep_dir').val('ATL');
    $('#ciu_dir').val('BQ');
    $('#via_princip_dir').val('');
    $('#via_genera_dir').val('');
    $('#nom_princip_dir').val('');
    $('#nom_genera_dir').val('');
    $('#placa_dir').val('');
    $('#ClientePadre').val('');
    $('#idcliente').val('');
    $('#EmailRepresentanteLegal').val('');
    $('#CelRepresentateLegal').val('');
    $('#busqueda').val('');






}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function Posicionar_div(id_objeto, e) {
    obj = document.getElementById(id_objeto);
    var posx = 0;
    var posy = 0;
    if (!e)
        var e = window.event;
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    }
    else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    else
        alert('ninguna de las anteriores');

    obj.style.left = posx + 'px';
    obj.style.top = posy + 'px';
    obj.style.zIndex = 1300;
}

function guardarValp() {
    var grid = jQuery("#valorespredeterminados")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;
    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);
        if (data.valor_xdefecto === '') {
            error = true;
            mensajesDelSistema('Digite el valor', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else {
            if (data.descripcion === '') {

                error = true;
                mensajesDelSistema('Digite la descripcion', '300', 'auto', false);
                grid.jqGrid('editRow', filas[i], true, function () {
                    $("input, select", e.target).focus();
                });
                break;
            }

        }

        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }
    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 41, informacion: JSON.stringify({rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    //alert(json.mensaje);
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    refrescarGridvalorespredeterminados();
                    //refrescarGridvalorespredeterminados();
                    //AgregarGridAso();
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function mensajeConfirmAction(msj, width, height, okAction, id, id2) {
    $("#msj2").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id, id2);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function Recargar() {
    cargarPagina('jsp/opav/OportunidadNegocio/Contratista/Contratista.jsp');
}

function tabbs(x)
{
    if (x === 1) {
        $("#definicion_alcances").removeClass("in active");
        $("#definicion_acciones").addClass("in active");
        $("#li_definicion_alcances").removeClass("active");
        $("#li_definicion_acciones").addClass("active");
    } else {
        $("#definicion_acciones").removeClass("in active");
        $("#definicion_alcances").addClass("in active");
        $("#li_definicion_acciones").removeClass("active");
        $("#li_definicion_alcances").addClass("active");
    }
}

function crearAPU() {
    $('#nomapu').val('');
    $('#tbl_insumos').jqGrid('GridUnload');
    $('#div_apu').fadeIn('slow');
    AbrirDivAPU();
    GridInsumos();
}


function cargarInfoSolicitudesP() {
    var grid_tablap = jQuery("#tabla_infoSolicitudP");
    if ($("#gview_tabla_infoSolicitudP").length) {
        reloadGridInfoSolicitudes3(grid_tablap, 39);
    } else {
        grid_tablap.jqGrid({
            caption: "SOLICITUDES",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '500',
            width: '1590',
            colNames: ['Idsolicitud', 'Fecha creacion', 'Nombre Cliente', 'Nombre Proyecto', 'Foms', 'Costo Contratista', 'Valor Cotizacion', 'Etapa', 'Estado',
                'Codigo cliente', 'Tipo solicitud', 'Estado_cartera', 'Fecha validacion cartera', 'Responsable', 'Interventor', 'Flag', 'id_trazabilidad', 'Id estado', 'presupuesto_terminado', 'Acciones'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'creation_date', index: 'creation_date', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'nomcli', index: 'nomcli', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 230, sortable: true, align: 'left', search: true},
                {name: 'num_os', index: 'num_os', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'valor_cotizacion', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'nombre_etapa', index: 'nombre_etapa', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_estado', index: 'nombre_estado', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'codcli', index: 'codcli', width: 90, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'estado_cartera', index: 'estado_cartera', width: 100, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'fecha_validacion_cartera', index: 'fecha_validacion_cartera', width: 200, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'responsable', index: 'responsable', width: 180, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'interventor2', index: 'interventor2', width: 110, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'flag', index: 'flag', width: 40, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'trazabilidad', index: 'trazabilidad', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_estado', index: 'id_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'presupuesto_terminado', index: 'presupuesto_terminado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 200, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: false,
            pager: '#pagerP',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ignoreCase: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                mostrarFormulario3(rowid);

            }, gridComplete: function () {
                var ids = grid_tablap.jqGrid('getDataIDs');
                var fila;
                var estado_trazabilidad;


                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tablap.jqGrid("getLocalRow", cl);
                    estado_trazabilidad = fila.trazabilidad;
                    id_estado = fila.id_estado;
                    presupuesto_terminado = fila.presupuesto_terminado;
                    //var enviar_Presupuesto='';


                    var accion = "<img src='/fintra/images/opav/Transicion_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n' >";
                    var wbs = "<img src='/fintra/images/opav/Wbs_grey.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='19' height='19' title ='WBS'  );\">";
                    var costos_Indirectos = "<img src='/fintra/images/opav/costos_indirectos_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='costos_admon' id='costos_admon' value=''  width='24' height='24' title ='Costos Indirectos' );\">";
                    var Cotizar = "<img src='/fintra/images/opav/Cotizacion_grey.png' align='absbottom'   style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Definicion de Rentabilidad y Esquema' );\">";
                    var iva_aiu = "<img src='/fintra/images/opav/iva_aiu_grey.png' align='absbottom' name='IVA_AIU' id='AYF'value='IVA - AIU'  width='26' height='26' title ='Cotizacion Definitiva'>";
                    var Cargar_Archivos = "<img src='/fintra/images/opav/File_add_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Cargar Archivos'  onclick=\"divemergente_Carga_Archivos('" + cl + "');\">";
                    
                    var clonacion = "<img src='/fintra/images/opav/Transicion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;'  id='clonacion'value=''  width='19' height='19' title ='Clonacion'  onclick=\"mostrarVentanaClonacion('" + cl + "');\">";
                    if (fila.trazabilidad == '3') {
                        accion = "<img src='/fintra/images/opav/Transicion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
                        if (!((id_estado == '310') || (id_estado == '340')|| (id_estado == '350') || (id_estado == '130'))) {
                            wbs = "<img src='/fintra/images/opav/Wbs.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='19' height='19' title ='WBS'  onclick=\"abrirWbs('" + cl + "');\">";
                            costos_Indirectos = "<img src='/fintra/images/opav/costos_indirectos.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='costos_admon' id='costos_admon' value=''  width='24' height='24' title ='Costos Indirectos'  onclick=\"abrirVentanaCostosAdmon('" + cl + "');\">";
                            Cargar_Archivos = "<img src='/fintra/images/opav/File_add.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Cargar Archivos'  onclick=\"divemergente_Carga_Archivos('" + cl + "');\">";

                        }
                    }else if(fila.trazabilidad > 3 || (id_estado == '145')){
                            wbs = "<img src='/fintra/images/opav/Wbs.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='19' height='19' title ='WBS'  onclick=\"abrirWbs2('" + cl + "');\">";
                            costos_Indirectos = "<img src='/fintra/images/opav/costos_indirectos.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='costos_admon' id='costos_admon' value=''  width='24' height='24' title ='Costos Indirectos'  onclick=\"abrirVentanaCostosAdmonReadOnly('" + cl + "');\">";
                    }
                    if (id_estado == '140') {
                        Cotizar = "<img src='/fintra/images/opav/Cotizacion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Definicion de Rentabilidad y Esquema'  onclick=\"principalAsignacionCostos('" + cl + "');\">";
                        iva_aiu = "<img src='/fintra/images/opav/iva_aiu.png' align='absbottom' name='IVA_AIU' id='AYF'value='IVA_AIU'  width='26' height='26' title ='Cotizacion Definitiva'  onclick=\"abrir_Iva_Aiu('" + cl + "');\">";
                        accion = "<img src='/fintra/images/opav/Transicion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
                    }
                    grid_tablap.jqGrid('setRowData', ids[i], {actions: accion +  wbs + costos_Indirectos +   Cotizar + iva_aiu + Cargar_Archivos + clonacion});




                }
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 39,
                    lineaNegocio: $("#linea_negocio").val(),
                    responsable: $("#responsable").val(),
                    solicitud: $("#solicitud").val(),
                    estadoCartera: $("#estadocartera").val(),
                    fechaInicio: $("#fechaini").val(),
                    fechafin: $("#fechafin").val(),
                    trazabilidad: '3',
                    etapaActual: $('#etapaActual').val(),
                    id_cliente: $('#id_cliente').val(),
                    nom_proyecto: $('#txt_nom_proyecto').val(),
                    foms: $('#txt_foms').val(),
                    tipo_proyecto: $('#tipo_proyecto').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tablap.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }

            }

        }).navGrid("#pagerP", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
        grid_tablap.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });

    }
}

function reloadGridInfoSolicitudes3(grid_tablap, op) {
    grid_tablap.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                lineaNegocio: $("#linea_negocio").val(),
                responsable: $("#responsable").val(),
                solicitud: $("#solicitud").val(),
                estadoCartera: $("#estadocartera").val(),
                fechaInicio: $("#fechaini").val(),
                fechafin: $("#fechafin").val(),
                trazabilidad: '3',
                etapaActual: $('#etapaActual').val(),
                id_cliente: $('#id_cliente').val(),
                nom_proyecto: $('#txt_nom_proyecto').val(),
                foms: $('#txt_foms').val(),
                tipo_proyecto: $('#tipo_proyecto').val()
            }
        }
    });
    grid_tablap.trigger("reloadGrid");
}

function abrirWbs(id_solicitud) {
    window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/&pagina=WBS.jsp?num_solicitud=" + id_solicitud, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
}

function abrirWbs2(id_solicitud) {

    window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/&pagina=WBSPresupuesto.jsp?num_solicitud=" + id_solicitud, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarEstadoCartera() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 38
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#estadocartera').html('');
                $('#estadocartera').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#estadocartera').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function  mostrarFormulario3(id_solicitud) {
    cargarSolicitud(id_solicitud);
    $("#fformPrincipal3").dialog({
        width: '994',
        height: '480',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        //title: 'FORMULARIO',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function  mostrarFormularioCrearSolicitud() {
    cargarLineaNegocioCS();
    cargarTipoSolicitudCS();
    cargarResponsableCS();
    cargarInterventorCS();
    autocompetarcliente();
    $("#formulario").dialog({
        width: '827',
        height: '540',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'FORMULARIO',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Crear": function () {
                crearSolicitud();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarLineaNegocioCS() {
    $('#LineaDeNegocioCS').html('');
    $('#LineaDeNegocioCS').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 12
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#LineaDeNegocioCS').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTipoSolicitudCS() {
    $('#TipoSolicitudCS').html('');
    $('#TipoSolicitudCS').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 13,
            idLineaDeNegocio: $("#LineaDeNegocioCS").val()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#TipoSolicitudCS').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarResponsableCS() {
    $('#ResponsableCS').html('');
    $('#ResponsableCS').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 14
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#ResponsableCS').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarInterventorCS() {
    $('#InterventorCS').html('');
    $('#InterventorCS').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 15
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#InterventorCS').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function autocompetarcliente() {
    $("#txt_NombreClienteCS").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 4
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            var idcliente = ui.item.mivar;
            $('#idClienteCS').val(idcliente);
            cargarNicsCS(idcliente);
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);

        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarNicsCS(idcliente) {
    $('#cbx_nicCS').html('');
    $('#cbx_nicCS').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 11,
            idcliente: idcliente
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json.rows) {
                        $('#cbx_nicCS').append('<option value=' + json.rows[key].id_cliente + '>' + json.rows[key].nic + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}






function  mostrarVentanaClonacion(id_solicitud) {
    $('#clonacion_id_solicitud_destino').val('');
    $('#clonacion_id_solicitud_origen').val(id_solicitud);
    $("#ventanaClonacion").dialog({
        width: '350',
        height: '175',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Clonacion de proyectos.',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Clonar": function () {
                cargando_toggle();
                $.ajax({
                    type: 'POST',
                    url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
                    dataType: 'json',
                    async: false,
                    data: {
                        opcion: 106,
                        clonacion_id_solicitud_origen: $('#clonacion_id_solicitud_origen').val(),
                        clonacion_id_solicitud_destino: $('#clonacion_id_solicitud_destino').val()
                        
                    },
                    success: function (json) {
                        toastr.success(json.respuesta, 'Notificacion');
                        $('#ventanaClonacion').dialog("close");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr);
                    },complete:function(){
                        cargando_toggle();
                    }
                });
            },
            "Salir": function () {
                $(this).dialog("close");

            }
        }
    });
}


function  mostrarVentanaAccion(id_solicitud) {
    $('#descripcion_causal').val('');
    var grid_tabla = jQuery("#tabla_infoSolicitudP");
    var trazabilidad = grid_tabla.getRowData(id_solicitud).trazabilidad;
    var id_estado = grid_tabla.getRowData(id_solicitud).id_estado;
    var nombre_estado = grid_tabla.getRowData(id_solicitud).nombre_estado;
    $('#idsolicitud').val(id_solicitud);
    $('#estadoActual').val(nombre_estado);
    $('#idestadoActual').val(id_estado);
    $('#descripcion_causal').val('');
    cargarEtapas(trazabilidad);
    cargarTrazabilidadOferta(id_solicitud);
    cargarEstadoEtapas(id_estado);
    $("#cambioEtapa").dialog({
        width: '800',
        height: '475',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Cambio de Estado',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Actualizar": function () {
                cambiarEstado();
            },
            "Salir": function () {
                $(this).dialog("close");

            }
        }
    });
}

function  mostrarVentanaAccion2(id_solicitud) {


    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + "Esta seguro que a culminado con el presupuesto?");
    $("#dialogMsj").dialog({
        width: 250,
        height: 200,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                //insertar_cabecera_tabla_temporal(id_solicitud);
                update_presupuesto_terminado(id_solicitud, 'S');
                $("#btn_Aceptar_tipoClienteP").click();
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function  insertar_cabecera_tabla_temporal(id_solicitud) {


    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 76,
            id_solicitud: id_solicitud

        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (json.respuesta === "OK") {

                    mensajesDelSistema("Se envio de forma correcta esta solicitud", '250', '150', true);

                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}
function  update_presupuesto_terminado(id_solicitud, estado) {


    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 76,
            id_solicitud: id_solicitud,
            estadoo: estado

        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (json.respuesta === "OK") {

                    mensajesDelSistema("Se envio de forma correcta esta solicitud", '250', '150', true);
                    cargarInfoSolicitudesP();

                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function  usuario_admin() {


    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 77
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (json.respuesta === "OK") {

                    return true;

                } else {
                    return false;
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}



function cargarEtapas(trazabilidad) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 40,
            etapa: trazabilidad
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#etapa').val('');
                $('#idetapa').val('');
                for (var datos in json) {
                    $('#etapa').val(json[datos]);
                    $('#idetapa').val(datos);
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarEstadoEtapas(id_estado) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 41,
            etapa: 3,
            id_estado: id_estado
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#estado').html('');
                $('#estado').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#estado').append('<option value=' + json[datos].id_estado_destino + '>' + json[datos].nombre_estado + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cambiarEstado() {

    var estado = $("#estado").val();

    if ((estado !== '') && ($("#descripcion_causal").val().length > 10)) {

        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            dataType: 'json',
            async: false,
            data: {
                opcion: 42,
                etapa: $("#idetapa").val(),
                estado_actual: $("#idestadoActual").val(),
                estados: $("#estado").val(),
                causal: $('#causal').val(),
                observacion: $('#descripcion_causal').val(),
                idsolicitud: $('#idsolicitud').val()
            },
            success: function (json) {
                console.log(json.respuesta);
                var resp = json.respuesta;
                if (resp === 'Guardado') {
                    toastr.success('Cambio satisfactorio', 'Enviado');
                    $("#cambioEtapa").dialog("close");
                    cargarInfoSolicitudesP();
                } else {
                    mensajesDelSistema("Error", '204', '140', false);
                }
            }
        });
    } else {
        toastr.error('Por favor llene todos los campos', 'Error');
    }
}



function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function cargar_Archivos() {
    //grab all form data  
    var fd = (new FormData(document.getElementById('form_Cargar_Archivos')));
    var archivo = document.getElementById('examinar').value;

    if (archivo === "") {
        mensajesDelSistema("No ha seleccionado ning&uacute;n archivo. Por favor, seleccione uno!!", '250', '150');
        return;
    }
    if (!validarNombresArchivos()) {
        return;
    }

    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function () {

        $.ajax({
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=56',
            type: 'POST',
            data: fd,
            dataType: 'json',
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            success: function (json)
            {
                $("#examinar").val("");
                obtenerArchivosCargados();
                if (json.respuesta === "OK") {
                    $("#dialogLoading").dialog('close');
//                        obtenerArchivosCargados();                
                } else {
                    $("#dialogLoading").dialog('close');
                    alert(".::ERROR AL CARGAR ARCHIVO::.");
                }
            }
        });

    }, 500);
}

function obtenerArchivosCargados() {
    $('#tbl_archivos_cargados').html('');
    var num_sol = $('#idsolicitud_carga_archivo').val();
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {opcion: 57,
            id_solicitud: num_sol},
        async: false,
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        success: function (jsonData) {
            if (!isEmptyJSON(jsonData)) {
                $('#tbl_archivos_cargados').append("<thead><tr><td>#</td><td>Nombre Archivo</td><td>Accion</td></thead>");
                for (var i = 0; i < jsonData.length; i++) {
                    var nomarchivo = jsonData[i];
                    var Eliminar = "<img src='/fintra/images/botones/iconos/anular.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('�Esta seguro de Eliminar este Archivo?, se eliminara de forma permanente','230','190',EliminarArchivo,'" + num_sol + "','" + nomarchivo + "');\">";
                    var Abrir = "<img src='/fintra/images/botones/iconos/cotizacion.png' align='absbottom'  name='Abrir' id='Abrir' width='15' height='15' title ='Abrir'  onclick=\"AbrirArchivo('" + num_sol + "','" + nomarchivo + "');\">";
                    $('#tbl_archivos_cargados').append("<tr><th scope='row'>" + (i + 1) + "</th><td>" + nomarchivo + "</td><td>" + Abrir + "  " + Eliminar + "</td></tr>");

                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function AbrirArchivo(num_sol, nomarchivo) {

    $.ajax({
        type: "POST",
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        async: false,
        dataType: "json",
        data: {
            opcion: 59,
            num_solicitud: num_sol,
            nomarchivo: nomarchivo
        },
        success: function (json) {
            //alert(data);
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "SI") {

                    window.open("/fintra/images/multiservicios/" + json.login + "/" + nomarchivo);
                } else {
                    mensajesDelSistema(".::ERROR AL OBTENER ARCHIVO::.", '250', '150');
                }

            }
        }
    });

}

function EliminarArchivo(num_sol, nomarchivo) {
    $.ajax({
        type: "POST",
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        async: false,
        dataType: "json",
        data: {
            opcion: 58,
            num_solicitud: num_sol,
            nomarchivo: nomarchivo
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {
                    obtenerArchivosCargados();
                } else {
                    mensajesDelSistema(".::ERROR AL ELIMINAR ARCHIVO::.", '250', '150');
                }

            }
        }
    });
}


function divemergente_Carga_Archivos(id_solicitud) {
    $("#examinar").val("");
    $('#idsolicitud_carga_archivo').val(id_solicitud);
    obtenerArchivosCargados();
    $('#div_Cargar_Archivos').dialog({
        width: 600,
        height: 385,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Carga de Archivos',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $('#div_Solicitidudes').show();
}

function validarNombresArchivos() {
    var archivo = document.getElementById('examinar');
    for (var i = 0; i < archivo.files.length; i++) {
        var file = archivo.files[i].name;
        var fname = file.substring(0, file.lastIndexOf('.'));
        if (!fname.match(/^[0-9a-zA-Z\_\-\ ]*$/)) {
            alert('Nombre de archivo no v�lido. por favor, Verifique. Archivo: ' + file);
            return false;
        }
    }
    return true;
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}


function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function abrirVentanaCostosAdmon(id_solicitud) {
    window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/&pagina=costosAdmon.jsp?num_solicitud=" + id_solicitud, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
}

function abrirVentanaCostosAdmonReadOnly(id_solicitud) {
    window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/&pagina=costosAdmonReadOnly.jsp?num_solicitud=" + id_solicitud, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
}
function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarTrazabilidadOferta(id_solicitud) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 81,
            idsolicitud: id_solicitud
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#trazabilidad').val(json.respuesta);
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function generar_Pdf_Presupuesto_Detalle(id_solicitud) {
    cargando_toggle();
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        async: false,
        data: {
            opcion: 90,
            id_solicitud: id_solicitud
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    toastr.error(json.error, 'Error')
                    //mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    toastr.success('Al generar Pdf', 'Exito');
                    window.open('.' + json.Ruta);
                }
            } else {
                mensajesDelSistema("Lo sentimos ocurri� un error al generar cotizaci�n!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    cargando_toggle();
}
function cargando_toggle() {
    $('#loader-wrapper').toggle();
}


function xxx(id_solicitud) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        async: false,
        data: {
            opcion: 90,
            id_solicitud: id_solicitud
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {


                if (json.respuesta === "OK") {
                    window.open('.' + json.Ruta);
                }
            }

        }, error: function (xhr, ajaxOptions, thrownError) {

        }
    });
}

function solonumeros(){
  //solo numeros
  $('.soloNumeros').keypress(function (e,x) {
  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    return false;
    }   
  });
  //Formato de miles
//   $('.soloNumeros').keyup(function(e){
//     $(this).val($(this).val().replace(/,/g, "").toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
//  });
}