
$(document).ready(function () {
    maximizarventana();
    cargarCbxLineaNegocio();
    cargarCbxTipoSolicitud();
    cargarCbxInterventor();
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $("#txt_NombreCliente").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 4
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }


            });
        },
        minLength: 1,
        select: function (event, ui) {
            var idcliente = ui.item.mivar;
            $('#idCliente').val(idcliente);
            cargarCbxNics(idcliente);
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
});
function crearSolicitud() {
    if (CamposObligatorios()) {
        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            dataType: 'json',
            async: false,
            data: {
                opcion: 16,
                idcliente: $('#idCliente').val(),
                nic: $('#cbx_nic option:selected').text(),
                descripcion: $('#descripcion').val(),
                opd: $('#OPD').val(),
                responsable: $('#cbx_Responsable').val(),
                interventor: $('#cbx_Interventor').val(),
                lineanegocio: $('#cbx_LineaDeNegocio').val(),
                tiposolicitud: $('#cbx_TipoSolicitud option:selected').text(),
                EmailContacto: $('#EmailContacto').val(),
                CargoContacto: $('#CargoContacto').val(),
                nomproyecto: $('#txt_Nomproyecto').val(),
                enviar : $('#chx_envio_cartera').prop('checked')
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    if (json.respuesta === "OK") {

                        mensajesDelSistema("Se Creo la solicitud de forma correcta", '250', '150', true);
                        Recargar();
                    }

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        alert('Datos Incompletos');
    }
}

function limpiarFormulario() {
    cargarCbxLineaNegocio();
    cargarCbxTipoSolicitud();
    cargarCbxResponsable();
    cargarCbxInterventor();
    $('#cbx_nic').html('');
    $('#OPD').val('');
    $('#txt_NombreCliente').val('');
    $('#idCliente').val('');
    $('#descripcion').val('');
}

function cargarCbxLineaNegocio() {
    $('#cbx_LineaDeNegocio').html('');
    $('#cbx_LineaDeNegocio').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 12
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_LineaDeNegocio').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxTipoSolicitud() {
    $('#cbx_TipoSolicitud').html('');
    $('#cbx_TipoSolicitud').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 13,
            idLineaDeNegocio: $("#cbx_LineaDeNegocio").val()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_TipoSolicitud').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxResponsable() {
    $('#cbx_Responsable').html('');
    $('#cbx_Responsable').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 14
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Responsable').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxInterventor() {
    $('#cbx_Interventor').html('');
    $('#cbx_Interventor').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 15
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Interventor').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxNics(idcliente) {

    $('#cbx_nic').html('');
    $('#cbx_nic').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 11,
            idcliente: idcliente
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json.rows) {
                        $('#cbx_nic').append('<option value=' + json.rows[key].id_cliente + '>' + json.rows[key].nic + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }


        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function CamposObligatorios() {

    var campos = '';
    var DC = true;
    campos = ['#txt_Nomproyecto','#cbx_nic',  '#cbx_LineaDeNegocio', '#cbx_TipoSolicitud', '#cbx_Responsable', '#cbx_Interventor', '#descripcion'];
    for (var i = 0; i < campos.length; i++) {
        if (($(campos[i]).val()) === '') {
            $(campos[i]).css({
                'box-shadow': '0 0 5px red',
                'border-width': '1px',
                'border-style': 'solid',
                'border-color': 'red'

            });
            DC = false;
        }

    }


    return DC;
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}
function Recargar() {
    cargarPagina('jsp/opav/OportunidadNegocio/Solicitud/Solicitud.jsp');
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}