/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    
    var modalidad_ = $('#modalidad_').val();
    if (modalidad_ === 'WBS') {
        document.getElementById("contenedor_").style.visibility = "visible";
    } else if (modalidad_ === 'APU') {
        document.getElementById("contenedor_").style.visibility = "hidden";
    }

    cargarInformacionModalidadProyecto();
    autocompletarNombreProyecto();



    $('#guardar').click(function () {
        var modalidad = $('#modalidad').val();
        var porc_administracion = $('#porc_administracion').val();
        var porc_imprevisto = $('#porc_imprevisto').val();
        var porc_utilidad = $('#porc_utilidad').val();
        var iva = $('#iva_').val();
        if (modalidad === 'AIU') {
            if (porc_administracion === '' || porc_imprevisto === '' || porc_utilidad === '' || porc_administracion <= 0 || porc_imprevisto <= 0 || porc_utilidad <= 0) {
                mensajesDelSistema('Falta digitar datos', '300', 'auto', false);
            } else {
                guardarInformacionModalidadProyecto();
            }
        } else {
            if (iva === '' || iva <= 0) {
                mensajesDelSistema('Valor del iva no valido', '300', 'auto', false);
            } else {
                guardarInformacionModalidadProyecto();
            }
        }

    });
    $('#detalle').click(function () {
        var modalidad = $('#modalidad').val();
        var porc_administracion = $('#porc_administracion').val();
        var porc_imprevisto = $('#porc_imprevisto').val();
        var porc_utilidad = $('#porc_utilidad').val();
        var iva = $('#iva_').val();
        var porc_iva_compensar = 0;
        if (modalidad === 'AIU') {
            if (!$('#myonoffswitch_').is(":checked"))
            {
                $('#administracion_t').val($('#administracion').val());
                $('#cia_t').val($('#cia').val());
                $('#administracion').val($('#cia').val());
                $('#cia').val(0);

                $('#porc_administracion').propAttr('readonly', true);
                //porc_administracion
            } else {
                $('#administracion').val($('#administracion_t').val());
                $('#cia').val($('#cia_t').val());
                $('#porc_administracion').propAttr('readonly', false);


            }
            $('#subtotal11').val(numberConComas(parseFloat(numberSinComas($('#subtotal').val())) + parseFloat(numberSinComas($('#iva_material').val())) + parseFloat(numberSinComas($('#cia').val()))));
            obtener_iva_compensar();
            porc_iva_compensar = $('#porc_iva_compensado').val();
            $('#iva_compensado').val(numberConComas(parseFloat(numberSinComas(porc_iva_compensar) / 100 * numberSinComas($('#subtotal11').val())).toFixed(2)));
            $('#subtotal1').val(numberConComas(parseFloat((((porc_iva_compensar / 100) + 1) * numberSinComas($('#subtotal11').val())).toFixed(2))));
            $('#porc_administracion').val(numberConComas(parseFloat(numberSinComas($('#administracion').val()) * 100 / numberSinComas($('#subtotal1').val())).toFixed(2)));

            var valor_administracion, valor_imprevisto, valor_utilidad, valor_iva_utlidad;
            var perc_administracion = numberSinComas($('#porc_administracion').val());
            var perc_imprevisto = numberSinComas($('#porc_imprevisto').val());
            var perc_utilidad = numberSinComas($('#porc_utilidad').val());
            var subtotal_1 = parseFloat(numberSinComas($('#subtotal1').val()));


            valor_administracion = (subtotal_1 * perc_administracion) / 100;
            valor_imprevisto = (subtotal_1 * perc_imprevisto) / 100;
            valor_utilidad = (subtotal_1 * perc_utilidad) / 100;
            valor_iva_utlidad = (valor_utilidad * 0.19).toFixed(2);
            $('#administracion').val(numberConComas(valor_administracion.toFixed(2)));
            $('#utilidad').val(numberConComas(valor_utilidad.toFixed(2)));
            $('#imprevisto').val(numberConComas(valor_imprevisto.toFixed(2)));
            $('#iva_utilidad').val(numberConComas(valor_iva_utlidad));



            perc_aiu = parseFloat(perc_administracion) + parseFloat(perc_imprevisto) + parseFloat(perc_utilidad);
            valor_aiu = parseFloat(valor_administracion) + parseFloat(valor_imprevisto) + parseFloat(valor_utilidad * 1.19);
            total = parseFloat(subtotal_1) + parseFloat(valor_aiu);
            $('#total').val(numberConComas(total.toFixed(2)));
            $('#porc_aiu').val(perc_aiu);
            $('#aiu').val(valor_aiu);



        } else {

        }

    });
    cargando_toggle(); 
});

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function mensajesDelSistema(msj, width, height) {
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarInformacionModalidadProyecto() {
    $.ajax({
        async: false,
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 82,
            id_solicitud: $('#id_solicitud').val(),
            nombre_proyecto: $('#nombre_proyecto').val()
        },
        success: function (json) {
            console.log(json);
            var total = 0;
            if (json.length === 0) {
                $('#nombre_proyecto').val('');
                $('#costo_proyecto').val('');
                $('#rentabilidad_contratista').val('');
                $('#rentabilidad_esquema').val('');
                $('#subtotal').val('');
                $('#cia').val('');
                $('#porc_administracion').val('');
                $('#administracion').val('');
                $('#porc_imprevisto').val('');
                $('#imprevisto').val('');
                $('#porc_utilidad').val('');
                $('#utilidad').val('');
                $('#iva_').val('');
                $('#total').val('');
                mensajesDelSistema('No se encontro informacion', '300', 'auto', false);
            } else {
                $('#nombre_proyecto').val(numberConComas(json[0].nombre_proyecto));
                $('#costo_proyecto').val(numberConComas(parseFloat(json[0].valor_cotizacion).toFixed(2)));
                $('#rentabilidad_contratista').val(numberConComas(parseFloat(json[0].valor_rentabilidad_contratista).toFixed(2)));
                $('#rentabilidad_esquema').val(numberConComas(parseFloat(json[0].valor_rentabilidad_esquema).toFixed(2)));
                $('#subtotal').val(numberConComas(parseFloat(json[0].subtotal).toFixed(2)));
                $('#cia').val(numberConComas(parseFloat(json[0].cia).toFixed(2)));
                $('#cia_t').val(numberConComas(parseFloat(json[0].cia).toFixed(2)));
                $('#iva_material').val(numberConComas(parseFloat(json[1].iva_material).toFixed(2)));

                if (json[0].modalidad_comercial == 1) {
                    $('#modalidad').val('AIU');
                    $('#porc_administracion').val(parseFloat(json[0].perc_administracion).toFixed(2));
                    $('#administracion').val(numberConComas(parseFloat(json[0].administracion).toFixed(2)));
                    $('#porc_imprevisto').val(parseFloat(json[0].perc_imprevisto).toFixed(2));
                    $('#imprevisto').val(numberConComas(parseFloat(json[0].imprevisto).toFixed(2)));
                    $('#porc_utilidad').val(parseFloat(json[0].perc_utilidad).toFixed(2));
                    $('#utilidad').val(numberConComas(parseFloat(json[0].utilidad).toFixed(2)));
                    $('#porc_aiu').val(numberConComas(parseFloat(json[0].perc_aiu).toFixed(2)));
                    $('#aiu').val(numberConComas(parseFloat(json[0].valor_aiu).toFixed(2)));

                    var subtotal11 = numberConComas(parseFloat(json[0].subtotal) + parseFloat(json[0].cia) + parseFloat(json[1].iva_material));
                    var iva_compensar = json[0].iva_compensar;
                    $('#subtotal11').val(numberConComas(subtotal11));
                    $('#iva_utilidad').val(numberConComas(parseFloat(json[0].utilidad * 0.19).toFixed(2)));
                    $('#iva_compensado').val(numberConComas(iva_compensar));
                    $("#subtotal1").val(numberConComas(parseFloat(numberSinComas(subtotal11)) + parseFloat(iva_compensar)));
                    $("#porc_iva_compensado").val(parseFloat(iva_compensar * 100 / parseFloat(numberSinComas(subtotal11))).toFixed(2));


                    $(".porct").css("display", "true");
                    $(".lab").css("display", "true");
                    $(".iva_").css("display", "none");

                } else if (json[0].modalidad_comercial !== 1) {
                    $('#modalidad').val('IVA');
                    $('#iva_').val(numberConComas(parseFloat(json[0].valor_iva).toFixed(2)));
                    $('#subtotal1').val(numberConComas(parseFloat(numberSinComas(($('#subtotal').val())) + parseFloat(numberSinComas($('#cia').val()))).toFixed(2)));

                    $(".porct").css("display", "none");
                    $(".lab").css("display", "none");
                    $(".iva_").css("display", "true");

                }
                $('#total').val(numberConComas(json[0].total));
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function visualizacion() {

    calcularIva();
    $(".iva_").toggle();
    $('#porc_administracion').toggle();
    $('#administracion').toggle();
    $('#porc_imprevisto').toggle();
    $('#imprevisto').toggle();
    $('#porc_utilidad').toggle();
    $('#utilidad').toggle();
    $('.porct').toggle();
    $('.lab').toggle();


}

function calcularIva() {

    if ($("#modalidad").val() == 'IVA') {
        obtener_CIA();
        var iva;
        var valor_subtotal = parseFloat(numberSinComas($('#subtotal').val())) + parseFloat(numberSinComas($('#cia').val()));
        iva = (parseFloat(valor_subtotal) * 19) / 100;
        $('#iva_').val(numberConComas(iva.toFixed(3)));
        $('#subtotal1').val(numberConComas(valor_subtotal.toFixed(2)));
        $('#cia').val($('#cia_t').val());
        $('#total').val(numberConComas(parseFloat(valor_subtotal) + parseFloat(iva)));

    } else {

        $('#myonoffswitch_').propAttr('checked', false);
        calcular_iva_compensar();
    }

}

function autocompletarNombreProyecto() {
    $("#nombre_proyecto").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: 2
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.value,
                            mivar1: item.mivar1
                        };
                    }));
                }
            });
        },
        minLength: 3,
        delay: 500,
        disabled: false,
        select: function (event, ui) {
            $('#nombre_proyecto').val(ui.item.label);
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar1 :
                    "Nothing selected, input was " + ui.item.label);
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function calculosAIU() {
    var valor_administracion, valor_imprevisto, valor_utilidad, perc_aiu, valor_aiu, total, iva;
    var valor_subtotal = numberSinComas($('#subtotal').val());
    var perc_administracion = numberSinComas($('#porc_administracion').val());
    var perc_imprevisto = numberSinComas($('#porc_imprevisto').val());
    var perc_utilidad = numberSinComas($('#porc_utilidad').val());
    var cia = numberSinComas($('#cia').val());
    var modalidad = $('#modalidad').val();
    if (modalidad === 'AIU') {
//        valor_administracion = (valor_subtotal * perc_administracion) / 100;
//        valor_imprevisto = (valor_subtotal * perc_imprevisto) / 100;
//        valor_utilidad = (valor_subtotal * perc_utilidad) / 100;
//        $('#administracion').val(numberConComas(valor_administracion));
//        $('#utilidad').val(numberConComas(valor_utilidad));
//        $('#imprevisto').val(numberConComas(valor_imprevisto));
//        perc_aiu = parseFloat(perc_administracion) + parseFloat(perc_imprevisto) + parseFloat(perc_utilidad);
//        valor_aiu = valor_administracion + valor_imprevisto + (valor_utilidad + ((valor_utilidad * 19) / 100));
//        total = parseFloat(valor_subtotal) + parseFloat(cia) + parseFloat(valor_aiu);
    } else {
        iva = numberSinComas($('#iva_').val());//   = valor_subtotal + ((valor_subtotal * 19) / 100);
        total = parseFloat(valor_subtotal) + parseFloat(cia) + parseFloat(iva);
    }
    $('#porc_aiu').val(perc_aiu);
    $('#aiu').val(numberConComas(valor_aiu.toFixed(3)));
    $('#total').val(numberConComas(total.toFixed(3)));
}

function guardarInformacionModalidadProyecto() {
    cargando_toggle();
    var modalidad = $('#modalidad').val();
    var total = numberSinComas($('#total').val());
    var porc_administracion, administracion, porc_imprevisto, imprevisto, porc_utilidad, utilidad, iva, perc_iva, modalidad_comercial, porc_aiu, aiu, iva_compensado, porc_iva_compensado;
    if (modalidad === 'AIU') {
        porc_administracion = numberSinComas($('#porc_administracion').val());
        administracion = numberSinComas($('#administracion').val());
        porc_imprevisto = numberSinComas($('#porc_imprevisto').val());
        imprevisto = numberSinComas($('#imprevisto').val());
        porc_utilidad = numberSinComas($('#porc_utilidad').val());
        utilidad = numberSinComas($('#utilidad').val());
        iva = 0;
        perc_iva = 0;
        modalidad_comercial = 1;
        porc_aiu = numberSinComas($('#porc_aiu').val());
        aiu = numberSinComas($('#aiu').val());
        //iva_compensar = numberSinComas($('#iva_compensar').val());
        porc_iva_compensado = numberSinComas($('#porc_iva_compensado').val());
        iva_compensado = numberSinComas($('#iva_compensado').val());

    } else {
        porc_administracion = 0;
        administracion = 0;
        porc_imprevisto = 0;
        imprevisto = 0;
        porc_utilidad = 0;
        utilidad = 0;
        porc_aiu = 0;
        aiu = 0;
        iva = numberSinComas($('#iva_').val());
        perc_iva = 19;
        modalidad_comercial = 0;
        iva_compensado = 0;
        porc_iva_compensado = 0;
        //iva_compensar=0;
    }

    $.ajax({
        async: false,
        type: 'POST',
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        dataType: "json",
        data: {
            opcion: 83,
            id_solicitud: $('#id_solicitud').val(),
            porc_administracion: porc_administracion,
            administracion: administracion,
            porc_imprevisto: porc_imprevisto,
            imprevisto: imprevisto,
            porc_utilidad: porc_utilidad,
            utilidad: utilidad,
            iva: iva,
            perc_iva: perc_iva,
            total: total,
            modalidad_comercial: modalidad_comercial,
            porc_aiu: porc_aiu,
            aiu: aiu,
            //iva_compensar: iva_compensar
            iva_compensado: iva_compensado,
            porc_iva_compensado: porc_iva_compensado
        },
        success: function (json) {
            if (json.respuesta === 'Guardado') {
                mensajesDelSistema('Se ha actualizado la informacion de manera exitosa', '300', 'auto', false);
                $('#perc_cia').val(parseFloat(numberSinComas($('#cia').val())) * 100 / parseFloat(numberSinComas($('#subtotal').val())));


//                mostrarImpresion($('#id_solicitud').val());
//                $('#nombre_proyecto_').val($('#nombre_proyecto').val());
//                $('#id_solicitud_').val($('#id_solicitud').val());
                generarCotizacion($('#id_solicitud').val());





            } else if (json.respuesta === 'ERROR') {
                mensajesDelSistema('Se ha producido un error', '300', 'auto', false);
            } else {
                mensajesDelSistema(json.respuesta, '300', 'auto', false);
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function  mostrarImpresion() {
    $('input[type=checkbox]').live('click', function () {
        var parent = $(this).parent().attr('id');
        $('#' + parent + ' input[type=checkbox]').removeAttr('checked');
        $(this).attr('checked', 'checked');
    });



    $("#dialogMsjImpresion").dialog({
        width: '1024',
        height: '580',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Imprimir": function () {
                alert("Mari PDF");
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });

    carga_alltablas();
}

function filtro_buscar() {
    var modalidad_ = $('#modalidad_').val();
    if (modalidad_ === 'WBS') {
        document.getElementById("contenedor_").style.visibility = "visible";
        cargar_tabla(0);
        $('.check').removeAttr('checked');
    } else if (modalidad_ === 'APU') {
        document.getElementById("contenedor_").style.visibility = "hidden";
        cargar_tabla(6);
    }
}

function cargar_tabla(id) {
    $('#opcion_tbl').val(id);
    $('#div_tbl_Area').hide();
    $('#div_tbl_Disciplina').hide();
    $('#div_tbl_Capitulo').hide();
    $('#div_tbl_Actividad').hide();
    $('#div_tbl_Apu').hide();
    $('#div_tbl_Apu_Exp').hide();

    switch (id) {
        case 1:
            $('#div_tbl_Area').show();
            break;
        case 2:
            $('#div_tbl_Disciplina').show();
            break;
        case 3:
            $('#div_tbl_Capitulo').show();
            break;
        case 4:
            $('#div_tbl_Actividad').show();
            break;
        case 5:
            $('#div_tbl_Apu').show();
            break;
        case 6:
            $('#div_tbl_Apu_Exp').show();
            break;
        default:

    }
}

function carga_alltablas() {
    cargar_tbl_Area();
    cargar_tbl_Disciplina();
    cargar_tbl_Capitulo();
    cargar_tbl_Actividad();
    cargar_tbl_Apu();
    cargar_tbl_Apu_Exp();
}

function cargar_tbl_Area() {
    var grid_tabla = jQuery("#tbl_Area");
    if ($("#gview_tbl_Area").length) {
        reloadGrid_tbl_Area(grid_tabla, 84, 5);
    } else {
        grid_tabla.jqGrid({
            caption: "AREAS",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '330',
            width: '590',
            colNames: ['id_area_proy', 'Area', 'Valor', 'Total'],
            colModel: [
                {name: 'id_area_proy', index: 'id_area_proy', width: 130, sortable: true, align: 'left', hidden: true, search: true, key: true},
                {name: 'descripcion', index: 'descripcion', width: 440, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'sum_act_esquema', index: 'sum_act_esquema', editable: false, align: 'left', hidden: true, width: 140, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'total', editable: false, align: 'left', width: 140, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}


            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {

                //$('#costo_proyecto').val(grid_tabla.jqGrid('getCol', 'valor', false, 'sum'));

                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila, total;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);

                    valor = grid_tabla.jqGrid('getCell', cl, 'sum_act_esquema');
                    total = (($('#perc_cia').val() / 100) + 1) * parseFloat(numberSinComas(valor));
                    grid_tabla.jqGrid('setCell', cl, 'total', total);

                }
                var colSumValor = grid_tabla.jqGrid('getCol', 'total', false, 'sum');

                grid_tabla.jqGrid('footerData', 'set', {valor: colSumValor});

            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 84,
                    idsolicitud: $('#id_solicitud').val(),
                    opc: 5

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
//        grid_tabla.jqGrid('filterToolbar',
//                {
//                    autosearch: true,
//                    searchOnEnter: true,
//                    defaultSearch: "cn",
//                    stringResult: true,
//                    ignoreCase: true,
//                    multipleSearch: true
//
//                });
    }
}

function reloadGrid_tbl_Area(grid_tabla, op, opc) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                idsolicitud: $('#id_solicitud').val(),
                opc: opc
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function cargar_tbl_Disciplina() {
    var grid_tabla = jQuery("#tbl_Disciplina");
    if ($("#gview_tbl_Disciplina").length) {
        reloadGrid_tbl_Disciplina(grid_tabla, 84, 4);
    } else {
        grid_tabla.jqGrid({
            caption: "DISCIPLINAS",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '330',
            width: '590',
            colNames: ['id_area_proyecto', 'id_disciplina_area', 'id_disciplina', 'Disciplina', 'Valor', 'Total'],
            colModel: [
                {name: 'id_area_proyecto', index: 'id_area_proy', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_disciplina_area', index: 'id_disciplina_area', width: 110, sortable: true, align: 'center', hidden: true, search: true, key: true},
                {name: 'id_disciplina', index: 'id_disciplina', width: 110, sortable: true, align: 'center', hidden: true, search: true},
                {name: 'descripcion', index: 'num_os', width: 440, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'sum_act_esquema', index: 'sum_act_esquema', editable: false, align: 'left', width: 140, sorttype: 'currency', formatter: 'currency', sortable: true, hidden: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'total', editable: false, align: 'left', width: 140, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}




            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {
                //$('#costo_proyecto').val(grid_tabla.jqGrid('getCol', 'valor', false, 'sum'));

                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila, total;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);

                    valor = grid_tabla.jqGrid('getCell', cl, 'sum_act_esquema');
                    total = (($('#perc_cia').val() / 100) + 1) * parseFloat(numberSinComas(valor));
                    grid_tabla.jqGrid('setCell', cl, 'total', total);

                }
                var colSumValor = grid_tabla.jqGrid('getCol', 'total', false, 'sum');

                grid_tabla.jqGrid('footerData', 'set', {valor: colSumValor});
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 84,
                    idsolicitud: $('#id_solicitud').val(),
                    opc: 4

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
//        grid_tabla.jqGrid('filterToolbar',
//                {
//                    autosearch: true,
//                    searchOnEnter: true,
//                    defaultSearch: "cn",
//                    stringResult: true,
//                    ignoreCase: true,
//                    multipleSearch: true
//
//                });
    }

}

function reloadGrid_tbl_Disciplina(grid_tabla, op, opc) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                idsolicitud: $('#id_solicitud').val(),
                opc: opc
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function cargar_tbl_Capitulo() {
    var grid_tabla = jQuery("#tbl_Capitulo");
    if ($("#gview_tbl_Capitulo").length) {
        reloadGrid_tbl_Capitulo(grid_tabla, 84, 3);
    } else {
        grid_tabla.jqGrid({
            caption: "CAPITULOS",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '330',
            width: '590',
            colNames: ['id_disciplina_area', 'id_capitulo', 'Capitulo', 'Valor', 'Total'],
            colModel: [
                {name: 'id_disciplina_area', index: 'id_disciplina_area', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_capitulo', index: 'id_capitulo', width: 110, sortable: true, align: 'center', hidden: true, search: true, key: true},
                {name: 'descripcion', index: 'num_os', width: 440, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'sum_act_esquema', index: 'sum_act_esquema', editable: false, align: 'left', width: 140, sorttype: 'currency', formatter: 'currency', sortable: true, hidden: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'total', editable: false, align: 'left', width: 140, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                mostrarFormulario(rowid);
            }, gridComplete: function () {
                //$('#costo_proyecto').val(grid_tabla.jqGrid('getCol', 'valor', false, 'sum'));

                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila, total;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);

                    valor = grid_tabla.jqGrid('getCell', cl, 'sum_act_esquema');
                    total = (($('#perc_cia').val() / 100) + 1) * parseFloat(numberSinComas(valor));
                    grid_tabla.jqGrid('setCell', cl, 'total', total);

                }
                var colSumValor = grid_tabla.jqGrid('getCol', 'total', false, 'sum');

                grid_tabla.jqGrid('footerData', 'set', {valor: colSumValor});


            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 84,
                    idsolicitud: $('#id_solicitud').val(),
                    opc: 3

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
//        grid_tabla.jqGrid('filterToolbar',
//                {
//                    autosearch: true,
//                    searchOnEnter: true,
//                    defaultSearch: "cn",
//                    stringResult: true,
//                    ignoreCase: true,
//                    multipleSearch: true
//
//                });
    }
}

function reloadGrid_tbl_Capitulo(grid_tabla, op, opc) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                idsolicitud: $('#id_solicitud').val(),
                opc: opc
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function cargar_tbl_Actividad() {
    var grid_tabla = jQuery("#tbl_Actividad");
    if ($("#gview_tbl_Actividad").length) {
        reloadGrid_tbl_Actividad(grid_tabla, 84, 2);
    } else {
        grid_tabla.jqGrid({
            caption: "ACTIVIDADES",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '330',
            width: '590',
            colNames: ['id', 'id_capitulo', 'id_actividad', 'Actividad', 'Valor', 'Total'],
            colModel: [
                {name: 'id', index: 'id', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_capitulo', index: 'id_capitulo', width: 110, sortable: true, align: 'center', hidden: true, search: true},
                {name: 'id_actividad', index: 'id_actividad', width: 110, sortable: true, align: 'center', hidden: true, search: true, key: true},
                {name: 'descripcion', index: 'num_os', width: 440, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'sum_act_esquema', index: 'sum_act_esquema', editable: false, align: 'left', width: 140, sorttype: 'currency', formatter: 'currency', sortable: true, hidden: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'total', editable: false, align: 'left', width: 140, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}



            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                mostrarFormulario(rowid);
            }, gridComplete: function () {
                //$('#costo_proyecto').val(grid_tabla.jqGrid('getCol', 'valor', false, 'sum'));

                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila, total;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);

                    valor = grid_tabla.jqGrid('getCell', cl, 'sum_act_esquema');
                    total = (($('#perc_cia').val() / 100) + 1) * parseFloat(numberSinComas(valor));
                    grid_tabla.jqGrid('setCell', cl, 'total', total);

                }
                var colSumValor = grid_tabla.jqGrid('getCol', 'total', false, 'sum');

                grid_tabla.jqGrid('footerData', 'set', {valor: colSumValor});
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 84,
                    idsolicitud: $('#id_solicitud').val(),
                    opc: 2

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
//        grid_tabla.jqGrid('filterToolbar',
//                {
//                    autosearch: true,
//                    searchOnEnter: true,
//                    defaultSearch: "cn",
//                    stringResult: true,
//                    ignoreCase: true,
//                    multipleSearch: true
//
//                });
    }
}

function reloadGrid_tbl_Actividad(grid_tabla, op, opc) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                idsolicitud: $('#id_solicitud').val(),
                opc: opc
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function cargar_tbl_Apu() {
    var grid_tabla = jQuery("#tbl_Apu");
    if ($("#gview_tbl_Apu").length) {
        reloadGrid_tbl_Apu(grid_tabla, 84, 1);
    } else {
        grid_tabla.jqGrid({
            caption: "APUS",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '330',
            width: '590',
            colNames: ['id', 'Apu', 'Valor', 'Total'],
            colModel: [
                {name: 'id', index: 'id', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'nombre', index: 'num_os', width: 440, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'sum_act_esquema', index: 'sum_act_esquema', editable: false, align: 'left', width: 140, sorttype: 'currency', formatter: 'currency', sortable: true, hidden: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'total', editable: false, align: 'left', width: 140, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}



            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                mostrarFormulario(rowid);
            }, gridComplete: function () {
                //$('#costo_proyecto').val(grid_tabla.jqGrid('getCol', 'valor', false, 'sum'));

                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila, total;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);

                    valor = grid_tabla.jqGrid('getCell', cl, 'sum_act_esquema');
                    total = (($('#perc_cia').val() / 100) + 1) * parseFloat(numberSinComas(valor));
                    grid_tabla.jqGrid('setCell', cl, 'total', total);

                }
                var colSumValor = grid_tabla.jqGrid('getCol', 'total', false, 'sum');

                grid_tabla.jqGrid('footerData', 'set', {valor: colSumValor});
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 84,
                    idsolicitud: $('#id_solicitud').val(),
                    opc: 1

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
//        grid_tabla.jqGrid('filterToolbar',
//                {
//                    autosearch: true,
//                    searchOnEnter: true,
//                    defaultSearch: "cn",
//                    stringResult: true,
//                    ignoreCase: true,
//                    multipleSearch: true
//
//                });
    }
}

function cargar_tbl_Apu_Exp() {
    var grid_tabla = jQuery("#tbl_Apu_Exp");
    if ($("#gview_tbl_Apu_Exp").length) {
        reloadGrid_tbl_Apu_Exp(grid_tabla, 84, 6);
    } else {
        grid_tabla.jqGrid({
            caption: "APUS",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '310',
            width: '800',
            colNames: [/*'id',*/ 'Nombre Area', 'Nombre Capitulo', 'Nombre APU', 'Nombre Unidad', 'Cantidad', 'Valor Unitario', 'Total'],
            colModel: [
//                {name: 'id', index: 'id', width: 130, sortable: true, align: 'left', hidden: true, search: false},
                {name: 'nombre_area', index: 'nombre_area', width: 440, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'nombre_capitulo', index: 'nombre_capitulo', width: 440, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'nombre_apu', index: 'nombre_area', width: 440, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'nombre_unidad', index: 'nombre_unidad', width: 440, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'cantidad_apu', index: 'cantidad_apu', width: 200, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'valor_unitario', index: 'valor_unitario', editable: false, align: 'left', width: 140, sorttype: 'currency', formatter: 'currency', sortable: true, hidden: false, search: false,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'total', editable: false, align: 'left', width: 140, sorttype: 'currency', formatter: 'currency', sortable: true, search: false,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: false,
            pager: '#page_Apu_Exp',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                mostrarFormulario(rowid);
            }, gridComplete: function () {
//                $('#costo_proyecto').val(grid_tabla.jqGrid('getCol', 'valor', false, 'sum'));
//
//                var ids = grid_tabla.jqGrid('getDataIDs');
//                var fila, total;
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    fila = grid_tabla.jqGrid("getLocalRow", cl);
//
//                    valor = grid_tabla.jqGrid('getCell', cl, 'sum_act_esquema');
//                    total = (($('#perc_cia').val()/100)+1)* parseFloat(numberSinComas(valor));
//                    grid_tabla.jqGrid('setCell', cl, 'total', total);
//
//                }
//                var colSumValor = grid_tabla.jqGrid('getCol', 'total', false, 'sum');
//
//                grid_tabla.jqGrid('footerData', 'set', {valor: colSumValor});
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 84,
                    idsolicitud: $('#id_solicitud').val(),
                    opc: 6

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#page_Apu_Exp", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
        grid_tabla.navButtonAdd('#page_Apu_Exp', {
            caption: "Exportar Ecxel",
            title: "Exportar Ecxel",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                exportarExcel(grid_tabla);
            },
            position: "first"
        });

    }
}

function reloadGrid_tbl_Apu_Exp(grid_tabla, op, opc) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                idsolicitud: $('#id_solicitud').val(),
                opc: opc
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function exportarExcel(grid_facturas) {
    console.log(JSON.stringify(grid_facturas.jqGrid('getRowData')));

    var opt = {
        autoOpen: false,
        modal: true,
        width: 220,
        height: 150,
        title: 'Descarga Reporte'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    setTimeout(function () {
        $.ajax({
            async: false,
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            type: 'POST',
            dataType: "html",
            data: {
                opcion: 86,
                listado: JSON.stringify(grid_facturas.jqGrid('getRowData')),
            },
            success: function (resp) {
                $("#imgloadEx").hide();
                $("#msjEx").hide();
                $("#respEx").show();
                var boton = "<div style='text-align:center'>\n\ </div>";
                document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;


            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    }, 500);
}

/**
 * Calcular el iva a compensar y los valores de AIU
 */
function calcular_iva_compensar() {
    var subtotal_1;
    $.ajax({
        async: false,
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 87,
            id_solicitud: $('#id_solicitud').val(),
            subtotal: parseFloat(numberSinComas($('#subtotal').val())) + parseFloat(numberSinComas($('#cia').val())) + parseFloat(numberSinComas($('#iva_material').val()))
        },
        success: function (json) {

            console.log(json);
            $('#cia').val(numberConComas(json[0].administracion));
            $('#iva_material').val(numberConComas(json[0].iva_material));
            $('#subtotal11').val(numberConComas(json[0].subtotal));
            $('#porc_iva_compensado').val((json[0].porc_iva_compensar * 100).toFixed(2));
            $('#subtotal1').val(numberConComas(json[0].subtotal_2));
            $('#iva_compensado').val(numberConComas(json[0].iva_compensar));


            var valor_administracion, valor_imprevisto, valor_utilidad, valor_iva_utlidad, valor_Iva_compesado;
            var perc_administracion = numberSinComas($('#porc_administracion').val());
            var perc_imprevisto = numberSinComas($('#porc_imprevisto').val());
            var perc_utilidad = numberSinComas($('#porc_utilidad').val());
            subtotal_1 = parseFloat(json[0].subtotal_2);


            valor_administracion = (subtotal_1 * perc_administracion) / 100;
            valor_imprevisto = (subtotal_1 * perc_imprevisto) / 100;
            valor_utilidad = (subtotal_1 * perc_utilidad) / 100;
            valor_iva_utlidad = (valor_utilidad * 0.19).toFixed(2);
            $('#administracion').val(numberConComas(valor_administracion.toFixed(2)));
            $('#utilidad').val(numberConComas(valor_utilidad.toFixed(2)));
            $('#imprevisto').val(numberConComas(valor_imprevisto.toFixed(2)));
            $('#iva_utilidad').val(numberConComas(valor_iva_utlidad));



            perc_aiu = parseFloat(perc_administracion) + parseFloat(perc_imprevisto) + parseFloat(perc_utilidad);
            valor_aiu = parseFloat(valor_administracion) + parseFloat(valor_imprevisto) + parseFloat(valor_utilidad * 1.19);
            total = parseFloat(subtotal_1) + parseFloat(valor_aiu);
            $('#total').val(numberConComas(total.toFixed(2)));
            $('#porc_aiu').val(perc_aiu);
            $('#aiu').val(valor_aiu);
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function generarCotizacion(id_solicitud) {
    
    var detallado = $('#myonoffswitch_').is(":checked") === true ? 'SI' : 'NO';
    console.log(detallado);
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 85,
            id_solicitud: id_solicitud,
            detallado: detallado
        },
        success: function (json) {
            cargando_toggle();
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    window.open('.' + json.Ruta);
                }
            } else {
                mensajesDelSistema("Lo sentimos ocurri� un error al generar cotizaci�n!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function obtener_iva_compensar() {
    var detallado = $('#myonoffswitch_').is(":checked") === true ? 'SI' : 'NO';
    console.log(detallado);
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        async: false,
        data: {
            opcion: 94,
            id_solicitud: $('#id_solicitud').val()

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                $('#porc_iva_compensado').val(json.porc_iva_compensar);



            } else {
                mensajesDelSistema("Lo sentimos ocurri� un error al generar cotizaci�n!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
function obtener_CIA() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        async: false,
        data: {
            opcion: 95,
            id_solicitud: $('#id_solicitud').val()

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                $('#cia').val(numberConComas(json.cotos_indirectos));



            } else {
                mensajesDelSistema("Lo sentimos ocurri� un error al generar cotizaci�n!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}
    