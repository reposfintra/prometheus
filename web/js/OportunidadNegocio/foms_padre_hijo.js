/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () {
    cargando_toggle();
    maximizarventana();
//    msj_confimarcion("Titulo", "Mensaje " ,  "wast('uno','2','III','Cuatro','5','VI','Siete','8','IX')", 30 );
    //cargando_toggle();
    $("#btn_Aceptar_tipoCliente").click(function () {
        cargarInfoSolicitudes();
    });
    cargando_toggle();

});


function wast(param1, param2, param3, param4, param5, param6, param7, param8, param9){
    toastr.success(param1, "Parametro numero 1");
    toastr.warning(param2, "Parametro numero 2");
    toastr.error(param3, "Parametro numero 3");
    toastr.success(param4, "Parametro numero 4");
    toastr.warning(param5, "Parametro numero 5");
    toastr.error(param6, "Parametro numero 6");
    toastr.success(param7, "Parametro numero 7");
    toastr.warning(param8, "Parametro numero 8");
    toastr.error(param9, "Parametro numero 9");
    
}



function cargarInfoSolicitudes() {
    var grid_tabla = jQuery("#tabla_infoSolicitud");
    if ($("#gview_tabla_infoSolicitud").length) {
        reloadGridInfoSolicitudes(grid_tabla, 0);
    } else {
        grid_tabla.jqGrid({
            caption: "SOLICITUDES",
            url: '/fintra/controlleropav?estado=Procesos&accion=FomsPadreHijo',
            datatype: "json",
            height: '450',
            width: '1600',
            colNames: ['Idsolicitud', 'Fecha creacion', 'Responsable', 'Nombre Cliente', 'Nombre Proyecto', 'Foms', 'Costo Contratista', 'Valor Cotizacion', 'Etapa', 'Estado',
                'Codigo cliente', 'Tipo solicitud', 'Estado_cartera', 'Fecha validacion cartera', 'Interventor', 'Flag', 'id_trazabilidad', 'Id estado', 'Relacionado', 'Acciones'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'creation_date', index: 'creation_date', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'responsable', index: 'responsable', width: 180, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nomcli', index: 'nomcli', width: 380, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 380, sortable: true, align: 'left', search: true},
                {name: 'num_os', index: 'num_os', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'valor_cotizacion', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true, hidden: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true, hidden: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'nombre_etapa', index: 'nombre_etapa', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_estado', index: 'nombre_estado', width: 160, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'codcli', index: 'codcli', width: 90, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'estado_cartera', index: 'estado_cartera', width: 100, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'fecha_validacion_cartera', index: 'fecha_validacion_cartera', width: 200, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'interventor2', index: 'interventor2', width: 110, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'flag', index: 'flag', width: 40, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'trazabilidad', index: 'trazabilidad', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_estado', index: 'id_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'relacionado', index: 'relacionado', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 140, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ignoreCase: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                
            }, gridComplete: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila;
                var estado_trazabilidad;
                var id_estado, iva_aiu = '';
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);
                    accion = "<img src='/fintra/images/opav/Transicion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Relacionar Foms'  onclick=\"abrirDivAsociacionFoms('" + cl + "');\">";
                    grid_tabla.jqGrid('setRowData', ids[i], {actions: accion });

                }
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 0,
                    id_solicitud: $('#id_solicitud').val(),
                    foms: $('#txt_foms').val()             

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        })
        .navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {});
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    multipleSearch: true
                });
    }
}


function reloadGridInfoSolicitudes(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=FomsPadreHijo',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                id_solicitud: $('#id_solicitud').val(),
                foms: $('#txt_foms').val()       
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function abrirDivAsociacionFoms(id_solicitud) {
    cargando_toggle();
    $('#id_solicitud_ok').val(id_solicitud);
    $("#div_asociacion").dialog({
        width: 1400,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Asociacion Foms Padre Hijos',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    listarfomsrelacionados();
    listarfomsdisponibles();
    cargando_toggle();
}






/**********************************************************************************************************************************************************
 Grilla de Lista de Foms Disponible
 ***********************************************************************************************************************************************************/

function reloadlistarfomsdisponibles(grid_tabla) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=FomsPadreHijo',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 1,
                id_solicitud: $('#id_solicitud_ok').val()     
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function listarfomsdisponibles() {

    var grid_tabla = jQuery("#tbl_foms");
    //var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    if ($("#gview_tbl_foms").length) {
        reloadlistarfomsdisponibles(grid_tabla);
    } else {
        grid_tabla.jqGrid({
            caption: 'Foms Disponibles',
            url: '/fintra/controlleropav?estado=Procesos&accion=FomsPadreHijo',
            datatype: 'json',
            height: 280,
            width: 600,
            colNames: ['IdSolicitud', 'Foms', 'Nombre Proyecto'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', hidden: false, key: true, widtg:'100'},
                {name: 'num_os', index: 'num_os', sortable: true, editable: true, align: 'left', width: '300'},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', hidden: false, sortable: true, editable: true, align: 'left', width: '300'}
                
            ],
            rowNum: 99999,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tbl_foms',
            multiselect: true,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: '',
            editurl: '',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },ajaxGridOptions: {
                type: "POST",
                //async: false,                
                data: {
                    opcion: 1,
                    id_solicitud: $('#id_solicitud_ok').val()       

                }
            },
            gridComplete: function () {
                

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}







/**********************************************************************************************************************************************************
 Grilla de Foms Relacionados
 ***********************************************************************************************************************************************************/

function reloadlistarfomsrelacionados(grid_tabla) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=FomsPadreHijo',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 2,
                //async: false, 
                id_solicitud: $('#id_solicitud_ok').val()     
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function listarfomsrelacionados() {

    var grid_tabla = jQuery("#tbl_foms_r");
    if ($("#gview_tbl_foms_r").length) {
        reloadlistarfomsrelacionados(grid_tabla);
    } else {
        grid_tabla.jqGrid({
            caption: 'Foms Relacionados',
            url: '/fintra/controlleropav?estado=Procesos&accion=FomsPadreHijo',
            datatype: 'json',
            height: 280,
            width: 600,
            colNames: ['IdSolicitud', 'Foms', 'Nombre Proyecto'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', hidden: false, key: true, widtg:'100'},
                {name: 'num_os', index: 'num_os', sortable: true, editable: true, align: 'left', width: '300'},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', hidden: false, sortable: true, editable: true, align: 'left', width: '300'}
                
            ],
            rowNum: 99999,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tbl_foms_r',
            multiselect: true,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: '',
            editurl: '',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },ajaxGridOptions: {
                type: "POST",
                async: false, 
                data: {
                    opcion: 2,
                    //async: false, 
                    id_solicitud: $('#id_solicitud_ok').val()       

                }
            },
            gridComplete: function () {


            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}









/**********************************************************************************************************************************************************
 Asociaciones
 ***********************************************************************************************************************************************************/

function asociarFoms() {
    
    var jsonObj = [];
    var filasId = jQuery('#tbl_foms').jqGrid('getGridParam', 'selarrrow');
    var id,j;
    if (filasId.length>0) {
        for (var i = 0; i < filasId.length; i++) {
            var item = {};
            item["id_solicitud"] = filasId[i];;
            jsonObj.push(item);
        }
        var lista = {};
        lista["id_solicitudes_a_relacionar"] = jsonObj;
        $.ajax({
            type: "POST",
            url: '/fintra/controlleropav?estado=Procesos&accion=FomsPadreHijo',
            dataType: "json",
            data: {
                opcion: 3,
                lista: JSON.stringify(lista),
                id_solicitud: $('#id_solicitud_ok').val()
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    console.log(json)
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.status === "200") {
                        toastr.success("Asociado con Exito", "Correcto");
                        listarfomsrelacionados();
                        listarfomsdisponibles();
                    }

                } else {
                    toastr.error("Lo sentimos no se pudierin asociar los Foms!!", "Error");
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        toastr.error("Debe Seleccionar por lo menos un foms para asociar", "Error");

    }
}

function desasociarFoms() {
    var jsonObj = [];
    var filasId = jQuery('#tbl_foms_r').jqGrid('getGridParam', 'selarrrow');
    var id,j;
    if (filasId.length>0) {
        for (var i = 0; i < filasId.length; i++) {
            var item = {};
            item["id_solicitud"] = filasId[i];;
            jsonObj.push(item);
        }
        var lista = {};
        lista["id_solicitudes_a_desasociar"] = jsonObj;
        $.ajax({
            type: "POST",
            url: '/fintra/controlleropav?estado=Procesos&accion=FomsPadreHijo',
            dataType: "json",
            data: {
                opcion: 4,
                lista: JSON.stringify(lista),
                id_solicitud: $('#id_solicitud_ok').val()
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    console.log(json);
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.status === "200") {
                        toastr.success("desasociado con Exito", "Correcto");
                        listarfomsrelacionados();
                        listarfomsdisponibles();
                    }

                } else {
                    toastr.error("Lo sentimos no se pudieron desasociar los Foms!!", "Error");
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        toastr.error("Debe Seleccionar por lo menos un foms para desasociar", "Error");

    }
}











/**********************************************************************************************************************************************************
 Utiles Generales
 ***********************************************************************************************************************************************************/

function msj_confimarcion(title , content , callback , width ) {
    $.confirm({
        
        title: title,
        content: content,
        type: 'blue',
        icon: 'glyphicon glyphicon-question-sign',
        boxWidth: width+'%',
        useBootstrap: false,
        animation: 'Rotate',
        closeAnimation: 'top',
        animationSpeed: 800,
        buttons: {
            confirmar: {
                text: 'Confirmar',
                btnClass: 'btn-blue',
                keys: ['enter', 'shift'],
                action: function () {
                    eval(callback);
                }
            },
            cancelar: {
                text: 'Cancelar',
                btnClass: 'btn-red',
                keys: ['enter', 'shift'],
                action: function () {
//                    $.alert("Cancelado");
                }
            }
        }
    });
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = [];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function comas_numeros(campo) {
    var variable = $('#' + campo);
    variable.val(numberConComas(variable.val()));
}

function cargarCombo(id, op) {
    var elemento = $('#' + id);
    $.ajax({
        url: "/fintra/controlleropav?estado=Cotizacion&accion=Sl",
        datatype: 'json',
        type: 'GET',
        data: {opcion: 23, op: op},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    elemento.append('<option value=0>' + "..." + '</option>');
                    for (var e in json) {
                        //elemento.append('<option value="' + e + '" onclick="CargarGridApus>' + json[e] + '</option>');
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function mensajeConfirmacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajeConfirmacion2(msj, width, height, okAction, id, id2) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id, id2);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}
/**********************************************************************************************************************************************************
 Fin Utiles Generales
 ***********************************************************************************************************************************************************/

