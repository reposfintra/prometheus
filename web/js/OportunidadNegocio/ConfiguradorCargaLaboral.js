/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    cargando_toggle();
    maximizarventana();
    listarActividades();
    cargar_unidades_medida();
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
});



function listarActividades() {
    var grid_tbl_maestro = jQuery("#tbl_Actividades");
    if ($("#gview_tbl_Actividades").length) {
        refrescarGridActividades();
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Actividades",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '400',
            width: '1000',
            cellEdit: true,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'nombre', index: 'nombre', width: 420, align: 'left'},
                {name: 'descripcion', index: 'descripcion', width: 420, align: 'left'},
                {name: 'actions', index: 'actions', width: 105, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tbl_Actividades'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 30
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var ids = jQuery("#tbl_Actividades").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarActividad('" + cl + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion('Esta seguro de anular la Actividad?','250','150',AnularActividad,'" + cl + "');\">";
                    jQuery("#tbl_Actividades").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                }


            },
            ondblClickRow: function (rowid, iRow, iCol, e) {


            }
        }).navGrid("#page_tbl_Actividades", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tbl_Actividades").jqGrid("navButtonAdd", "#page_tbl_Actividades", {
            caption: "Nuevo",
            onClickButton: function () {
                crearActividad();
            }
        });
    }

}

function refrescarGridActividades() {
    jQuery("#tbl_Actividades").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 30
            }
        }
    });

    jQuery('#tbl_Actividades').trigger("reloadGrid");
}
function cargar_unidades_medida() {

    $('#cbx_Unidad_medida').html('');

    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        dataType: 'json',
        async: false,
        data: {
            opcion: 12
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Unidad_medida').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}


function crearActividad() {
    $('#div_Control').fadeIn('slow');
    $('#idActividad').val('');
    $('#nombre').val('');
    $('#descripcion').val('');
    $('#wast').hide();
    AbrirDivActividad();
}

function AbrirDivActividad() {
    $("#div_Control").dialog({
        width: '500',
        height: '260',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR ACTIVIDAD',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardarActividad();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarActividad(cl) {

    $('#div_Control').fadeIn("slow");
    var fila = jQuery("#tbl_Actividades").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];

    $('#idActividad').val(cl);
    $('#nombre').val(nombre);
    $('#nombre_').val(nombre);
    $('#descripcion').val(descripcion);
    CargaLaboral();
    $('#wast').show();
    AbrirDivEditarActividad();

}

function AbrirDivEditarActividad() {
    $("#div_Control").dialog({
        width: '650',
        height: '450',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR ACTIVIDAD',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarActividad();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}



function guardarActividad() {
    var nombre = $('#nombre').val();
    var descripcion = $('#descripcion').val();

    if (nombre !== '' && descripcion !== '') {


        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            data: {
                opcion: ($('#idActividad').val() === '') ? 31 : 32,
                id: $('#idActividad').val(),
                nombre: $('#nombre').val(),
                descripcion: $('#descripcion').val()
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '270', '165');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridActividades();
                        $("#div_Control").dialog('close');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se pudo guardar la Actividad!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    } else {
        mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');
    }
}

function AnularActividad(cl) {

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            opcion: 33,
            id: cl

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {

                    refrescarGridActividades();
                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo Anular la actividad!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function CargaLaboral() {
    var grid_tbl_maestro = jQuery("#tbl_CargaLaboral");
    if ($("#gview_tbl_CargaLaboral").length) {
        refrescarGrid_CargaLaboral();
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Carga Laboral",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '120',
            width: '500',
            cellEdit: true,
            colNames: ['Id', 'Nivel', 'Rango Incial', 'Rango Final', 'id_Unidad_Medida', 'Unidad De Medida', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 5, align: 'left', key: true, hidden: true},
                {name: 'nivel', index: 'nivel', width: 10, align: 'left'},
                {name: 'rango_ini', index: 'rango_ini', sortable: true, editable: false, width: '10', align: 'left', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                {name: 'rango_fin', index: 'rango_fin', sortable: true, editable: false, width: '10', align: 'left', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                {name: 'id_unidad_medida_general', index: 'id_unidad_medida_general', hidden: true, width: 10, align: 'left'},
                {name: 'nombre_unidad', index: 'nombre_unidad', width: 10, align: 'left'},
                {name: 'actions', index: 'actions', width: 15, align: 'center'}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tbl_CargaLaboral'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 14,
                    idActividad: $('#idActividad').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var ids = jQuery("#tbl_CargaLaboral").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarCargaLaboral('" + cl + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion('Esta seguro de anular el Indicador De Gestion?','250','150',AnularCargaLaboral,'" + cl + "');\">";
                    jQuery("#tbl_CargaLaboral").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                }


            },
            ondblClickRow: function (rowid, iRow, iCol, e) {


            }
        }).navGrid("#page_tbl_CargaLaboral", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tbl_CargaLaboral").jqGrid("navButtonAdd", "#page_tbl_CargaLaboral", {
            caption: "Nuevo",
            onClickButton: function () {
                CrearCargaLaboral();
            }
        });
    }

}

function CrearCargaLaboral() {
    $('#div_Control_').fadeIn('slow');
    $('#idCargaLaboral').val('');
    $('#descripcion_').val('');
    $('#cbx_nivel').val('');
    $('#txt_desde').val('');
    $('#txt_hasta').val('');
    $('#cbx_Unidad_medida').val('');
    AbrirDivCargaLaboral();
}

function AbrirDivCargaLaboral() {
    $("#div_Control_").dialog({
        width: '700',
        height: '180',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Carga Laboral',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardar_CargaLaboral();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarCargaLaboral(cl) {

    $('#div_Control').fadeIn("slow");
    var fila = jQuery("#tbl_CargaLaboral").getRowData(cl);
    
    
    var peso = fila['peso'];
    var nivel = fila['nivel'];
    var rango_ini = fila['rango_ini'];
    var rango_fin = fila['rango_fin'];
    var id_unidad_medida_general = fila['id_unidad_medida_general'];
    var porc_nivel = fila['porc_nivel'];

    $('#idCargaLaboral').val(cl);
    $('#cbx_nivel').val(nivel);

    $('#txt_desde').val(rango_ini);
    $('#txt_hasta').val(rango_fin);
    $('#cbx_Unidad_medida').val(id_unidad_medida_general);
    AbrirDivCargaLaboral();


}




function guardar_CargaLaboral() {
    var rang_ini = $('#txt_desde').val();
    var rang_final = $('#txt_hasta').val();
    var unidad_medida = $('#cbx_Unidad_medida').val();

    if (rang_ini !== '' && rang_final !== '') {
        if (parseInt(rang_ini) < parseInt(rang_final)) {


            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "./controlleropav?estado=Modulo&accion=Planeacion",
                data: {
                    opcion: ($('#idCargaLaboral').val() === '') ? 13 : 15,
                    id: $('#idCargaLaboral').val(),
                    idActividad: $('#idActividad').val(),
                    nivel: $('#cbx_nivel').val(),
                    rang_ini: $('#txt_desde').val(),
                    rang_final: $('#txt_hasta').val(),
                    unidad_medida: $('#cbx_Unidad_medida').val()
                },
                success: function (json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '270', '165');
                            return;
                        }

                        if (json.respuesta === "OK") {
                            refrescarGrid_CargaLaboral();
                            $("#div_Control_").dialog('close');
                        }

                    } else {

                        mensajesDelSistema("Lo sentimos no se pudo guardar la Carga  laboral!!", '250', '150');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        } else {
            mensajesDelSistema("Lo sentimos no se pudo guardar la Carga  laboral por que los rangos con erroneos!!", '250', '150');
        }

    } else {
        mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');
    }
}

function AnularCargaLaboral(cl) {

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            opcion: 16,
            id: cl

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {

                    refrescarGrid_CargaLaboral();

                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo Anular la cargar laboral!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function refrescarGrid_CargaLaboral() {
    jQuery("#tbl_CargaLaboral").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 14,
                idActividad: $('#idActividad').val()
            }
        }
    });

    jQuery('#tbl_CargaLaboral').trigger("reloadGrid");
}

/**********************************************************************************************************************************************************
 Utiles Generales
 ***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon', '#perc_rete'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}


function mensajeConfirmacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}
/**********************************************************************************************************************************************************
 Fin Utiles Generales
 ***********************************************************************************************************************************************************/