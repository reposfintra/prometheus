
$(document).ready(function () {
    maximizarventana();
    cargarComboTipoCliente();
    limpiarFormulario();
    cargarDepartamentos('CO', 'dep_dir');
    $('#dep_dir').val('ATL');
    cargarCiudad('ATL', "ciu_dir");
    $('#ciu_dir').val('BQ');
    cargarVias('BQ', "via_princip_dir");
    cargarVias('BQ', "via_genera_dir");

    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $("#departamento").change(function () {
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciudad");
    });

    $("#dep_dir").change(function () {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciu_dir");
    });
    $("#ciu_dir").change(function () {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarVias(op, "via_princip_dir");
        cargarVias(op, "via_genera_dir");
    });
    $("#via_princip_dir").change(function () {
        $("#via_genera_dir").val('');
    });


    $("#ClientePadre").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 4
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            $("#idClientePadre").val(ui.item.mivar);
            alert($("#idClientePadre").val());
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });




});
function Nics() {
    if ($('#estadonic').val() === "1") {
        $('#div_Nic').css({
            'display': ''});
        $('#estadonic').val("2");
        $('#estadoxx').html('-');

    } else {
        $('#div_Nic').css({
            'display': 'none'});
        $('#estadonic').val("1");
        $('#estadoxx').html('+');
    }

    listarvalorespredeterminados($('#idcliente').val());

}


function buscar() {


    if ($('#filtro').val() === "1") {
        var busqueda = $.trim($('#busqueda').val());
        if (!(busqueda === "")) {
            $.ajax({
                type: 'POST',
                url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
                dataType: 'json',
                async: false,
                data: {
                    opcion: 9,
                    busqueda: busqueda
                },
                success: function (json) {

                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        $('#valorespredeterminados').jqGrid('GridUnload');
                        buscarclienteid(json.idcliente);
                        listarvalorespredeterminados(json.idcliente);
                    }else{
                            mensajesDelSistema("No existe Cliente asociado con el parametro ingresado", '250', '180');
                            return;                        
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }




    } else {
        buscarclienteid($('#idcliente').val());
        listarvalorespredeterminados($('#idcliente').val());

    }
}


function buscarclienteid(idcliente) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 30,
            idcliente: idcliente
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }

                console.log(json.nomcli);
                $('#idcliente').val(json.codcli);
                $('#NombreCliente').val(json.nomcli);
                $('#NitCliente').val(json.nit);
                $('#Nic').val("");
                $('#idCiudad').val(json.ciudad);
                $('#DireccionCliente').val(json.direccion);
                $('#NombreContacto').val(json.nomcontacto);
                $('#TelefonoContacto').val(json.telcontacto);
                $('#CelularContacto').val(json.celContacto);
                $('#EmailContacto').val(json.email);
                $('#CargoContacto').val(json.cargocontacto);
                $('#NombreRepresentateLegal').val(json.nomrepresentante);
                $('#TelRepresentateLegal').val(json.telrepresentante);
                $('#CelRepresentateLegal').val(json.celrepresentante);
                $('#EmailRepresentanteLegal').val(json.emailrepresentantelega);
                $('#DigitoVerificacion').val(json.digito_verificacion);
                $('#idDepartamento').val(json.iddepartamento);
                if (json.hijo === "S") {
                    habilitar_form_principal2(2);
                    cargarPadreid(json.codcli);


                } else {
                    habilitar_form_principal2(1);
                }
                cargarDepCiuid(json.ciudad);





            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function cargarDepCiuid(idCiudad) {

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 8,
            idCiudad: idCiudad
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                for (var key in json) {
                    $('#Departamento').val(key);
                    $('#Ciudad').val(json[key]);

                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarPadreid(idClientePadre) {

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 7,
            idClientePadre: idClientePadre
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                for (var key in json) {
                    $('#idClientePadre').val(key);
                    $('#ClientePadre').val(json[key]);

                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function autocompletarNombre() {
    limpiarFormulario();
    if ($('#filtro').val() === "1") {
        $("#busqueda").autocomplete({
            source: "",
            minLength: 2
        });

    } else {

            $("#busqueda").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: 'POST',
                        url: "./controlleropav?estado=Procesos&accion=Cliente",
                        dataType: "json",
                        data: {
                            q: request.term,
                            opcion: 29
                        },
                        success: function (data) {
                            // response( data );
                            response($.map(data, function (item) {
                                return {
                                    label: item.label,
                                    value: item.label,
                                    mivar: item.value
                                };
                            }));
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    $("#idcliente").val(ui.item.mivar);
                    console.log(ui.item ?
                            "Selected: " + ui.item.mivar :
                            "Nothing selected, input was " + ui.item.label);
                },
                change: function (event, ui) {
                    if (ui.item == null) {
                        //here is null if entered value is not match in suggestion list
                        $(this).val((ui.item ? ui.item.id : ""));
                    }
                },
                open: function () {
                    //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
                },
                close: function () {

                    // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
                }
            });
        }


}

function habilitar_form_principal(x) {
    $('#formPrincipal').css({
        'display': ''});

    $('#tipoCliente').attr('readonly', 'true');

    if (x === 1) {
        $('#div_clientepadre').css({
            'display': 'none'});
    } else {
        $('#div_clientepadre').css({
            'display': ''});
    }
    limpiarFormulario();



}

function desabilitar_form() {
    limpiarFormulario();
    $('#formPrincipal').css({
        'display': 'none'});
}


function habilitar_form_principal2(x) {
    $('#formPrincipal').css({
        'display': ''});

    $('#tipoCliente').attr('readonly', 'true');

    if (x === 1) {
        $('#div_clientepadre').css({
            'display': 'none'});
    } else {
        $('#div_clientepadre').css({
            'display': ''});
    }

}



function cargarComboTipoCliente() {
    $('#tipoCliente').html('');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 0
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#tipoCliente').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });



}

function cargarComboDepartamento() {
    $('#Departamento').html('');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 1
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#Departamento').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarComboCiudad() {
    $('#Ciudad').html('');
    var dep = $('#Departamento').val();

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 2,
            Departamento: dep

        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#Ciudad').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function CalcularDv() {
    var vpri, x, y, z, i, nit1, dv1;
    nit1 = document.getElementById("NitCliente").value;
    if (isNaN(nit1)) {
        //document.form1.dv.value="X";
        alert('El valor digitado no es un numero valido');
    } else {
        vpri = new Array(16);
        x = 0;
        y = 0;
        z = nit1.length;
        vpri[1] = 3;
        vpri[2] = 7;
        vpri[3] = 13;
        vpri[4] = 17;
        vpri[5] = 19;
        vpri[6] = 23;
        vpri[7] = 29;
        vpri[8] = 37;
        vpri[9] = 41;
        vpri[10] = 43;
        vpri[11] = 47;
        vpri[12] = 53;
        vpri[13] = 59;
        vpri[14] = 67;
        vpri[15] = 71;
        for (i = 0; i < z; i++) {
            y = (nit1.substr(i, 1));
            //document.write(y+"x"+ vpri[z-i] +":");
            x += (y * vpri[z - i]);
            //document.write(x+"<br>");     
        }
        y = x % 11;
        //document.write(y+"<br>");
        if (y > 1) {
            dv1 = 11 - y;
        } else {
            dv1 = y;
        }
        //document.form1.dv.value=dv1;
        document.getElementById("DigitoVerificacion").value = dv1;
    }
}

//Carga los nombre del autocompletar Cliente Padre
function cargarClientesPadre() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    var clientesPadre = [];
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 4
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                for (j in json.rows) {
                    clientesPadre.push(json.rows[j].valor_02);
                }
                $("#ClientePadre").autocomplete({
                    source: clientesPadre,
                    minLength: 2
                });

            }
        }
    });
}

//Carga los nombre del autocompletar Cliente Padre
function cargarClientesPadre2() {
    var info = {};
    $.ajax({
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 4
        },
        success: function (json) {
            info = json;
            $("#ClientePadre").autocomplete({
                source: info,
                minLength: 2
            });


        }
    });
}

function crearCliente() {
    var x;
    if ($('#rpadre').prop('checked')) {
        x = 1;
    } else {
        x = 2;
    }
    if (CamposObligatorios(x)) {
        if (x === 1) {

            $.ajax({
                type: 'POST',
                url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
                dataType: 'json',
                async: false,
                data: {
                    opcion: 3,
                    NombreCliente: $('#NombreCliente').val(),
                    NitCliente: $('#NitCliente').val(),
                    Nic: $('#Nic').val(),
                    Departamento: $('#idDepartamento').val(),
                    Ciudad: $('#idCiudad').val(),
                    DireccionCliente: $('#DireccionCliente').val(),
                    NombreContacto: $('#NombreContacto').val(),
                    TelefonoContacto: $('#TelefonoContacto').val(),
                    CelularContacto: $('#CelularContacto').val(),
                    EmailContacto: $('#EmailContacto').val(),
                    CargoContacto: $('#CargoContacto').val(),
                    NombreRepresentateLegal: $('#NombreRepresentateLegal').val(),
                    TelRepresentateLegal: $('#TelRepresentateLegal').val(),
                    CelRepresentateLegal: $('#CelRepresentateLegal').val(),
                    EmailRepresentanteLegal: $('#EmailRepresentanteLegal').val(),
                    idClientePadre: '',
                    padre: 'S'
                },
                success: function (json) {

                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        if (json.respuesta === "OK") {

                            mensajesDelSistema("Se Creo el Cliente de forma correcta", '250', '150', true);
                        }

                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        } else {

            $.ajax({
                type: 'POST',
                url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
                dataType: 'json',
                async: false,
                data: {
                    opcion: 3,
                    NombreCliente: $('#NombreCliente').val(),
                    NitCliente: $('#NitCliente').val(),
                    Nic: $('#Nic').val(),
                    Departamento: $('#idDepartamento').val(),
                    Ciudad: $('#idCiudad').val(),
                    DireccionCliente: $('#DireccionCliente').val(),
                    NombreContacto: $('#NombreContacto').val(),
                    TelefonoContacto: $('#TelefonoContacto').val(),
                    CelularContacto: $('#CelularContacto').val(),
                    EmailContacto: $('#EmailContacto').val(),
                    CargoContacto: $('#CargoContacto').val(),
                    NombreRepresentateLegal: $('#NombreRepresentateLegal').val(),
                    TelRepresentateLegal: $('#TelRepresentateLegal').val(),
                    CelRepresentateLegal: $('#CelRepresentateLegal').val(),
                    EmailRepresentanteLegal: $('#EmailRepresentanteLegal').val(),
                    idClientePadre: $('#idClientePadre').val(),
                    padre: 'N'


                },
                success: function (json) {

                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        if (json.respuesta === "OK") {

                            mensajesDelSistema("Se Creo el Cliente de forma correcta", '250', '150', true);
                        }

                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }

    } else {
        alert('Datos Incompletos');
    }
}
;

function modificarCliente() {
    var x;
    if ($('#idClientePadre').val() === "") {
        x = 1;
    } else {
        x = 2;
    }

    ////////////////////////////////////////////////
    var grid = jQuery("#valorespredeterminados")
            , filas = grid.jqGrid('getDataIDs')
            , data;
    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);
        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    ////////////////////////////////////////////////
    if (CamposObligatorios(x)) {
        if (x === 1) {

            $.ajax({
                type: 'POST',
                url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
                dataType: 'json',
                async: false,
                data: {
                    opcion: 10,
                    NombreCliente: $('#NombreCliente').val(),
                    NitCliente: $('#NitCliente').val(),
                    Nic: $('#Nic').val(),
                    Departamento: $('#idDepartamento').val(),
                    Ciudad: $('#idCiudad').val(),
                    DireccionCliente: $('#DireccionCliente').val(),
                    NombreContacto: $('#NombreContacto').val(),
                    TelefonoContacto: $('#TelefonoContacto').val(),
                    CelularContacto: $('#CelularContacto').val(),
                    EmailContacto: $('#EmailContacto').val(),
                    CargoContacto: $('#CargoContacto').val(),
                    NombreRepresentateLegal: $('#NombreRepresentateLegal').val(),
                    TelRepresentateLegal: $('#TelRepresentateLegal').val(),
                    CelRepresentateLegal: $('#CelRepresentateLegal').val(),
                    EmailRepresentanteLegal: $('#EmailRepresentanteLegal').val(),
                    DigitoVerificacion: $('#DigitoVerificacion').val(),
                    idClientePadre: '',
                    idcliente: $('#idcliente').val(),
                    padre: 'S',
                    nics: JSON.stringify({rows: filas})
                },
                success: function (json) {

                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        if (json.respuesta === "OK") {

                            mensajesDelSistema("Se Modifico el Cliente de forma correcta", '250', '150', true);
                            desabilitar_form();
                        }

                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        } else {

            $.ajax({
                type: 'POST',
                url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
                dataType: 'json',
                async: false,
                data: {
                    opcion: 10,
                    NombreCliente: $('#NombreCliente').val(),
                    NitCliente: $('#NitCliente').val(),
                    Nic: $('#Nic').val(),
                    Departamento: $('#idDepartamento').val(),
                    Ciudad: $('#idCiudad').val(),
                    DireccionCliente: $('#DireccionCliente').val(),
                    NombreContacto: $('#NombreContacto').val(),
                    TelefonoContacto: $('#TelefonoContacto').val(),
                    CelularContacto: $('#CelularContacto').val(),
                    EmailContacto: $('#EmailContacto').val(),
                    CargoContacto: $('#CargoContacto').val(),
                    NombreRepresentateLegal: $('#NombreRepresentateLegal').val(),
                    TelRepresentateLegal: $('#TelRepresentateLegal').val(),
                    CelRepresentateLegal: $('#CelRepresentateLegal').val(),
                    EmailRepresentanteLegal: $('#EmailRepresentanteLegal').val(),
                    DigitoVerificacion: $('#DigitoVerificacion').val(),
                    idClientePadre: $('#idClientePadre').val(),
                    idcliente: $('#idcliente').val(),
                    padre: 'N',
                    nics: JSON.stringify({rows: filas})


                },
                success: function (json) {

                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        if (json.respuesta === "OK") {

                            mensajesDelSistema("Se Modifico el Cliente de forma correcta", '250', '150', true);
                            desabilitar_form();
                        }

                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }

    } else {
        alert('Datos Incompletos');
    }
}

function CamposObligatorios(tipo) {


    var campos = '';
    var DC = true;
    if (tipo === 1) {
        campos = ['#NombreCliente', '#idDepartamento', '#idCiudad', '#NitCliente', '#DigitoVerificacion', '#DireccionCliente', '#NombreContacto',
            '#CargoContacto', '#EmailContacto', '#NombreRepresentateLegal'];
    } else {
        campos = ['#NombreCliente', '#idDepartamento', '#idCiudad', '#NitCliente', '#DigitoVerificacion', '#DireccionCliente', '#NombreContacto',
            '#CargoContacto', '#EmailContacto', '#NombreRepresentateLegal'
                    , '#ClientePadre'];
    }
    ;
    if (($('#TelefonoContacto').val() === '') && ($('#CelularContacto').val() === '')) {
        DC = false;
    }
    if (($('#TelRepresentateLegal').val() === '') && ($('#CelRepresentateLegal').val() === '')) {
        DC = false;
    }

    for (var i = 0; i < campos.length; i++) {
        if (($(campos[i]).val()) === '') {
            DC = false;
            break;
        }

    }
    
    return DC;

}

function divemergente_nic() {
    $('#div_Nic').dialog({
        width: 400,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Nics',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $('#div_valorespredeterminados').show();
}

function limpiarFormulario() {
    $('#NombreCliente').val('');
    $('#Ciudad').html('');
    $('#NitCliente').val('');
    $('#DireccionCliente').val('');
    $('#NombreContacto').val('');
    $('#EmailContacto').val('');
    $('#TelefonoContacto').val('');
    $('#CargoContacto').val('');
    $('#CelularContacto').val('');
    $('#NombreRepresentateLegal').val('');
    $('#TelRepresentateLegal').val('');
    $('#Nic').val('');
    $('#DigitoVerificacion').val('');
    $('#idDepartamento').val('');
    $('#Departamento').val('');
    $('#idCiudad').val('');
    $('#Ciudad').val('');
    $('#dep_dir').val('ATL');
    $('#ciu_dir').val('BQ');
    $('#via_princip_dir').val('');
    $('#via_genera_dir').val('');
    $('#nom_princip_dir').val('');
    $('#nom_genera_dir').val('');
    $('#placa_dir').val('');
    $('#ClientePadre').val('');
    $('#idcliente').val('');
    $('#EmailRepresentanteLegal').val('');
    $('#CelRepresentateLegal').val('');
    $('#busqueda').val('');





}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargarDepartamentos(codigo, combo) {
    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            type: 'POST',
            async: false,
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 13,
                cod_pais: codigo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function AbrirDivDirecciones() {
    $("#direccion_dialogo").dialog({
        width: 550,
        height: 350,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'INGRESAR DIRECCION',
        closeOnEscape: false
    });
}

function Posicionar_div(id_objeto, e) {
    obj = document.getElementById(id_objeto);
    var posx = 0;
    var posy = 0;
    if (!e)
        var e = window.event;
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    }
    else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    else
        alert('ninguna de las anteriores');

    obj.style.left = posx + 'px';
    obj.style.top = posy + 'px';
    obj.style.zIndex = 1300;

}

function genDireccion(elemento, e) {

    var contenedor = document.getElementById("direccion_dialogo")
            , res = document.getElementById("dir_resul");

    //$("#direccion_dialogo").draggable({ handle: "#drag_direcciones"});
    // Posicionar_div("direccion_dialogo",e); 
    contenedor.style.left = 618 + 'px';
    contenedor.style.top = 111 + 'px';
    contenedor.style.display = "block";

    $("#div_pagaduria").css({
        'width': '950px'
    });
    res.name = elemento;
    res.value = (elemento.value) ? elemento.value : '';

}

function sendDireccion() {

    var x = document.getElementById("dep_dir").selectedIndex;
    var y = document.getElementById("dep_dir").options;
    $('#Departamento').val(y[x].text);
    var x = document.getElementById("ciu_dir").selectedIndex;
    var y = document.getElementById("ciu_dir").options;
    $('#Ciudad').val(y[x].text);
    $('#DireccionCliente').val($('#dir_resul').val());
    $('#idDepartamento').val($('#dep_dir').val());
    $('#idCiudad').val($('#ciu_dir').val());
    $('#direccion_dialogo').css({'display': 'none'});
}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3:
            var res = document.getElementById('dir_resul')
                    , des = document.getElementById(res.name);
            des.value = res.value;


        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");

            document.getElementById("direccion_dialogo").style.display = "none";
            $("#div_pagaduria").css({
                'width': 'auto'
            });
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
                    , g = document.getElementById('via_genera_dir')
                    , opcion;
            for (var i = 0; i < g.length; i++) {

                opcion = g[i];

                if (opcion.value === p) {

                    opcion.style.display = 'none';
                    opcion.readonly = true;

                } else {

                    opcion.readonly = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if (!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                    || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                    || !jQuery('#placa_dir').val()) {
                jQuery('#dir_resul').val("");
                jQuery('#dir_resul').attr("class", "validation-failed");
            } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                        jQuery('#via_princip_dir option:selected').text()
                        + ' ' + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                        + ' ' + jQuery('#via_genera_dir option:selected').text().trim()
                        + ' ' + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                        + ((jQuery('#via_genera_dir option:selected').text().toUpperCase() === '#' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'TRANSVERSAL') ? '-' : ' ')
                        + jQuery('#placa_dir').val().trim().toUpperCase()
                        + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
            }
            break;
    }
}

function cargarVias(codciu, combo) {
    //alert(codciu);

    $('#' + combo).empty();
    $.ajax({
        async: false,
        type: 'POST',
        url: "/fintra/controller?estado=GestionSolicitud&accion=Aval",
        dataType: 'json',
        data: {
            opcion: 'cargarvias',
            ciu: codciu
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    alert(json.error, '250', '180');
                    return;
                }
                try {
                    $('#' + combo).empty();
                    $('#' + combo).append("<option value=''></option>");

                    for (var key in json) {
                        $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    alert('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3:
            var res = document.getElementById('dir_resul')
                    , des = document.getElementById(res.name);
            des.value = res.value;
        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");

            document.getElementById("direccion_dialogo").style.display = "none";
            $("#div_pagaduria").css({
                'width': 'auto'
            });
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
                    , g = document.getElementById('via_genera_dir')
                    , opcion;
            for (var i = 0; i < g.length; i++) {

                opcion = g[i];

                if (opcion.value === p) {

                    opcion.style.display = 'none';
                    opcion.readonly = true;

                } else {

                    opcion.readonly = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if (!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                    || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                    || !jQuery('#placa_dir').val()) {
                jQuery('#dir_resul').val("");
                jQuery('#dir_resul').attr("class", "validation-failed");
            } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                        jQuery('#via_princip_dir option:selected').text()
                        + ' ' + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                        + ' ' + jQuery('#via_genera_dir option:selected').text().trim()
                        + ' ' + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                        + ((jQuery('#via_genera_dir option:selected').text().toUpperCase() === '#' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'TRANSVERSAL') ? '-' : ' ')
                        + jQuery('#placa_dir').val().trim().toUpperCase()
                        + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
            }
            break;
    }
}

function refrescarvalorespredeterminadosespedit(idcliente) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=11' + idcliente;
    jQuery("#valorespredeterminados").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#valorespredeterminados').trigger("reloadGrid");
}
function numeros(e)
{
    var tecla = (document.all) ? e.keyCode : e.which;
    if (tecla === 8)
        return true;
    var patron = /\d/;
    var te = String.fromCharCode(tecla);
    return patron.test(te);
}
function listarvalorespredeterminados(idcliente) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=11&idcliente=' + idcliente;
    if ($("#gview_valorespredeterminados").length) {
        refrescarvalorespredeterminadosespedit(idcliente);
    } else {
        jQuery("#valorespredeterminados").jqGrid({
            caption: 'Valores',
            url: url,
            datatype: 'json',
            height: 50,
            width: 250,
            colNames: ['Id', 'Nics'],
            colModel: [
                {name: 'id_cliente', index: 'id_cliente', hidden: true, align: 'left', width: '600px'},
                {name: 'nic', index: 'nic', sortable: true, editable: true, align: 'left', width: '600px',
                    editoptions: {size: 50, dataInit: function (elem) {
                            $(elem).bind("keypress", function (e) {
                                return numeros(e);
                            });
                        }
                    }}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_valorespredeterminados',
            multiselect: false,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_valorespredeterminados", {add: false, edit: false, del: false, search: false, refresh: false});
        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function guardarValp() {
    var grid = jQuery("#valorespredeterminados")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;
    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);
        if (data.valor_xdefecto === '') {
            error = true;
            mensajesDelSistema('Digite el valor', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else {
            if (data.descripcion === '') {

                error = true;
                mensajesDelSistema('Digite la descripcion', '300', 'auto', false);
                grid.jqGrid('editRow', filas[i], true, function () {
                    $("input, select", e.target).focus();
                });
                break;
            }

        }

        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }
    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 41, informacion: JSON.stringify({rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    //alert(json.mensaje);
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    refrescarGridvalorespredeterminados();
                    //refrescarGridvalorespredeterminados();
                    //AgregarGridAso();
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}

function validarTelefono(phone) {
    var filter = /^[0-9-()+]+$/;
    return filter.test(phone);
}

function validarEmail(email) {
    if (email.indexOf('@', 0) === -1 || email.indexOf('.', 0) === -1) {

        return false;
    } else {
        return true;
    }
}

function validarEmail2(x) {
    if (x === 1) {
        if (!validarEmail($('#EmailContacto').val())) {
            alert('Por favor verifique email del contacto');
        }
    } else
    if (!validarEmail($('#EmailRepresentanteLegal').val())) {
        alert('Por favor verifique email del Representante legal');
    }



}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

$('.mayuscula').keyup(function () {
    this.value = this.value.toUpperCase();
});

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}


function ordenarCombo(cboId){
    var valor =$('#'+cboId).val();
    var options = $("#"+ cboId +" option");                  
    options.detach().sort(function (a, b) {              
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#"+cboId);   
    $("#"+ cboId).val(valor);
}

