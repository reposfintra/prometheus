$(document).ready(function () {
    maximizarventana();
    jsonDiasPago = listarComboGrid();
    $("#fechaini").val('');
    $("#fechafin").val('');
    $("#fecha").val('');
    $("#fechaini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        //minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        minDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $('#solicitud').keyup(function (e) {
        if (e.keyCode == 13)
        {
            cargarInfoSolicitudes();
        }
    });
    $('#txt_foms').keyup(function (e) {
        if (e.keyCode == 13)
        {
            cargarInfoSolicitudes();
        }
    });
    var myDate = new Date();
    $('#ui-datepicker-div').css('clip', 'auto');
    limpiarFormulario();
    cargarCbxLineaNegocio();
    //cargarCbxTipoSolicitud();
    //cargarCbxResponsable();
    cargarCbx_Cartera();
    cargarCbxInterventor();
    cargarLineasNegocio();
    cargarComboGenerico('tipoNegocio',0,'');
    cargarComboGenerico('tipoTrabajo',1,'');
    cargarEstadoCartera();
    agregarclases();
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $("#btn_Aceptar_tipoCliente").click(function () {
        var fechainicio = $("#fechaini").val();
        var fechafin = $("#fechafin").val();
        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
            cargarInfoSolicitudes();
        } else {
            mensajesDelSistema("Por favor revice el rango de fechas", '410', '150', false);
        }
    });
    $("#btn_nuevo_tipoCliente").click(function () {
        mostrarFormularioCrearSolicitud();
    });
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    });
    $("#subirArchivo").click(function (e) {
        e.preventDefault();
        cargar_Archivos();
    });
    ordenarCombo('estadocartera');
    ordenarCombo('linea_negocio');
    autocompletar("txt_nom_cliente", 1);
    autocompletar("txt_nom_proyecto", 2);
});
function cargarCbxLineaNegocio() {
    $('#cbx_LineaDeNegocio').html('');
    $('#cbx_LineaDeNegocio').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 12
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_LineaDeNegocio').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxTipoSolicitud() {
    $('#cbx_TipoSolicitud').html('');
    $('#cbx_TipoSolicitud').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 13,
            idLineaDeNegocio: $("#cbx_LineaDeNegocio").val()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_TipoSolicitud').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxResponsable() {
    $('#cbx_Responsable').html('');
    $('#cbx_Responsable').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 14
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Responsable').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarResponsable() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 43
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#txt_responsable2').val(json.nombre);
                    $('#idresponsable2').val(json.cedula);
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxInterventor() {
    $('#cbx_Interventor').html('');
    $('#cbx_Interventor').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 15
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Interventor').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbx_Cartera() {
    $('#cbx_Interventor').html('');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 20
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Cartera').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxNics(idcliente) {

    $('#cbx_nic').html('');
    $('#cbx_nic').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 11,
            idcliente: idcliente
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json.rows) {
                        $('#cbx_nic').append('<option value=' + json.rows[key].id_cliente + '>' + json.rows[key].nic + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }


        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function buscar() {

    if ($('#filtro').val() === "1") {
        var busqueda = $('#busqueda').val();
        cargarSolicitud(busqueda);
    } else if ($('#filtro').val() === "2") {
        var busqueda = $.trim($('#busqueda').val());
        if (!(busqueda === "")) {
            $.ajax({
                type: 'POST',
                url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
                dataType: 'json',
                async: false,
                data: {
                    opcion: 9,
                    busqueda: busqueda
                },
                success: function (json) {

                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        $('#idcliente').val(json.idcliente);
                        BuscarSolicitudesidcliente();
                        listarvalorespredeterminados(json.idcliente);
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }

    } else {
        BuscarSolicitudesidcliente();
        listarvalorespredeterminados($('#idcliente').val());
    }
}

function cargarSolicitud(busqueda) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 17,
            busqueda: busqueda
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (!($('#idSolicitud').val(json.id_solicitud) === '')) {
                    $('#idSolicitud').val(json.id_solicitud);
                    $('#idcliente').val(json.id_cliente);
                    if (json.estado_cartera === ' ') {
                        $('#cbx_Cartera').val('');
                    } else {
                        $('#cbx_Cartera').val(json.estado_cartera);
                    }
                    ;
                    $('#fechaCreacion').val(json.creation_date);
                    $('#historiasolicitud').val(json.descripcion);
                    $('#txt_responsable').val(json.nomresponsable);
                    $('#idresponsable').val(json.responsable);
                    $('#cbx_Interventor').val(json.interventor);
                    
                    $('#fecha_limite2').val((json.fecha_limite).substring(0,11));
                    buscarclienteid(json.id_cliente);
                    cargarCbxNics(json.id_cliente);
                    $('#cbx_LineaDeNegocio').val(json.lineanegocio);
                    //cargarLinieaNegocioTP(json.tipo_solicitud);
                    //cargarCbxTipoSolicitud();
                    $('#cbx_TipoSolicitud >option:contains(' + json.tipo_solicitud + ')').attr('selected', 'selected');
                    $('#cbx_nic >option:contains(' + json.nic + ')').attr('selected', 'selected');
                    if (json.aviso === '') {
                        $('#div_opd').css({'display': 'none'});
                    } else {
                        $('#div_opd').css({'display': ''});
                    }
                    $('#OPD').val(json.aviso);
                    $('#fformPrincipal').css({'display': ''});                   
                } else {
                    alert('entro');
                    $('#fformPrincipal').css({'display': 'none'});
                    limpiarFormulario();
                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarComboGenerico(idCombo, op, param) {
    
    var elemento = $('#' + idCombo);
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        datatype: 'json',
        type: 'GET',
        data: {opcion: 102, op: op, param: param},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    elemento.append('<option value="">' + "..." + '</option>');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '" >' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}
///////////////////////////////////////////////////////////////////////////// Agregar Al Resto
function BuscarSolicitudesidcliente() {
    divemergente_Solicitudes();
    cargarSolicitudesIdcli($('#idcliente').val());
}


function divemergente_Solicitudes() {
    $('#div_Solicitidudes').dialog({
        width: 900,
        height: 385,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Solicitudes',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $('#div_Solicitidudes').show();
}

function refrescarGridSolicitudesNom(idcli) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=36&idcli=' + idcli;
    jQuery("#solicitudes").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#solicitudes').trigger("reloadGrid");
}

function cargarSolicitudesIdcli(idcli) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=36&idcli=' + idcli;
    if ($("#gview_solicitudes").length) {
        refrescarGridSolicitudesNom(idcli);
    } else {
        jQuery("#solicitudes").jqGrid({
            caption: 'Listado De Solicitudes',
            url: url,
            type: "POST",
            datatype: 'json',
            height: 214,
            width: 850,
            scrollOffset: 30,
            colNames: ['Id Solicitud', 'Tipo Solicitud', 'Fecha Creacion', 'Responsable'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', sortable: true, align: 'center', width: '280px'},
                {name: 'creation_date', index: 'creation_date', sortable: true, align: 'center', width: '150px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'center', width: '300px'}
                //{name: 'actions', index: 'actions', resizable: false, align: 'center', width: '135px'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            ignoreCase: true,
            pager: '#page_tabla_solicitudes',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                /*var ids = jQuery("#acciones").jqGrid('getDataIDs');
                 for (var i = 0; i < ids.length; i++) {
                 var cl = ids[i];
                 co = "<img src='/fintra/images/botones/iconos/apu.png' align='absbottom'  name='apu' id='apu' width='19' height='19' title ='APU'  onclick=\"AsociarAPU('" + cl + "');\">";
                 ed = "<img src='/fintra/images/botones/iconos/adds.png' align='absbottom'  name='editar' id='editar' width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"CargarDefAccionesAlcances('" + cl + "');\">";
                 cont = "<img src='/fintra/images/botones/iconos/contratista.png' align='absbottom'  name='contratista' id='contratista' width='19' height='19' title ='Asignar Contratistas'  onclick=\"AgregarContratistas('" + cl + "');\">";
                 an = "<img src='/fintra/images/botones/iconos/delete2.png' align='absbottom'  name='anular' id='anular' width='19' height='19' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan datos asociados a la subcategoria, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
                 jQuery("#acciones").jqGrid('setRowData', ids[i], {actions: co + '   ' + ed + '   ' + cont + '   ' + an});
                 }*/
            },
            loadError: function (xhr, status, error) {
                alert(error);
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                cargarSolicitud(rowid);
            }
        }).navGrid("#page_tabla_solicitudes", {edit: false, add: false, del: false});
        jQuery("#solicitudes").jqGrid("navButtonAdd", "#page_tabla_solicitudes", {
            caption: "Exportar",
            title: "Exportar a Excel",
            onClickButton: function () {
                alert('Exportar a Excel aun no esta listo');
            }
        });
        /*jQuery("#acciones").jqGrid('filterToolbar', "#searchacciones",
         {
         autosearch: false,
         searchOnEnter: false,
         defaultSearch: "cn",
         stringResult: true,
         ignoreCase: true,
         multipleSearch: true,
         gs_nombe: false
         
         });*/

        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}


function buscarclienteid(idcliente) {

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 6,
            idcliente: idcliente
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }

//                console.log(json.nomcli);
                $('#idcliente').val(json.codcli);
                $('#txt_NombreCliente').val(json.nomcli);
                $('#NombreCliente').val(json.nomcli);
                $('#NitCliente').val(json.nit);
                $('#DigitoVerificacion').val(json.digito_verificacion);
                $('#NombreContacto').val(json.nomcontacto);
                $('#CelularContacto').val(json.celContacto);
                $('#DireccionCliente').val(json.direccion);
                cargarDepCiuid(json.ciudad);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarLinieaNegocioTP(tiposolicitud) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 18,
            tiposolicitud: tiposolicitud
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }


                $('#cbx_LineaDeNegocio').val(json.value);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarDepCiuid(idCiudad) {

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 8,
            idCiudad: idCiudad
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                for (var key in json) {
                    $('#Departamento').val(key);
                    $('#Ciudad').val(json[key]);
                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
function autocompletarNombre() {
    limpiarFormulario();
    if (!($('#filtro').val() === "3")) {
        alert("entro")
        $("#busqueda").autocomplete({
            source: "",
            minLength: 2
        });
    } else {

        $("#busqueda").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: 'POST',
                    url: "./controlleropav?estado=Procesos&accion=Cliente",
                    dataType: "json",
                    data: {
                        q: request.term,
                        opcion: 4
                    },
                    success: function (data) {
                        // response( data );
                        response($.map(data, function (item) {
                            return {
                                label: item.label,
                                value: item.label,
                                mivar: item.value
                            };
                        }));
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $("#idcliente").val(ui.item.mivar);
                console.log(ui.item ?
                        "Selected: " + ui.item.mivar :
                        "Nothing selected, input was " + ui.item.label);
            },
            open: function () {
                //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
            },
            close: function () {

                // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
            }
        });
    }


}

function habilitar_form_principal(x) {
    $('#formPrincipal').css({
        'display': ''});
    $('#tipoCliente').attr('readonly', 'true');
    if (x === 1) {
        $('#div_clientepadre').css({
            'display': 'none'});
    } else {
        $('#div_clientepadre').css({
            'display': ''});
    }
    limpiarFormulario();
}

function desabilitar_form() {
    limpiarFormulario();
    $('#formPrincipal').css({
        'display': 'none'});
}

function cargarComboTipoCliente() {
    $('#tipoCliente').html('');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 0
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        console.log(key + " : " + json[key]);
                        $('#tipoCliente').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
//Carga los nombre del autocompletar Cliente Padre
function cargarClientesPadre() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    var clientesPadre = [];
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 4
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                for (j in json.rows) {
                    clientesPadre.push(json.rows[j].valor_02);
                }
                $("#ClientePadre").autocomplete({
                    source: clientesPadre,
                    minLength: 2
                });
            }
        }
    });
}


function modificarSolicitud() {
    if (CamposObligatorios()) {
        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            dataType: 'json',
            async: false,
            data: {
                opcion: 19,
                idcliente: $('#idCliente').val(),
                nic: $('#cbx_nic option:selected').text(),
                descripcion: $('#descripcion').val(),
                opd: $('#OPD').val(),
                responsable: $('#idresponsable').val(),
                interventor: $('#cbx_Interventor').val(),
                lineanegocio: $('#cbx_LineaDeNegocio').val(),
                //tiposolicitud: $('#cbx_TipoSolicitud option:selected').text(),
                idSolicitud: $('#idSolicitud').val(),
                estadoCartera: $('#cbx_Cartera').val(),
                fecha_limite: $('#fecha_limite').val()

            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    if (json.respuesta === "OK") {

                        mensajesDelSistema("Se Modifico la Solicitud de forma correcta", '250', '150', true);
                        $("#fformPrincipal").dialog("close");
                        limpiarFormulario();
                    }

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        alert('Datos Incompletos');
    }
}

function CamposObligatorios() {

    var campos = '';
    var DC = true;
    campos = ['#descripcion'];
    for (var i = 0; i < campos.length; i++) {
        if (($(campos[i]).val()) === '') {
//            $(campos[i]).css({
//                'box-shadow': '0 0 5px red',
//                'border-width': '1px',
//                'border-style': 'solid',
//                'border-color': 'red'
//            });
            DC = false;
        }

    }


    return DC;
}

function CamposObligatorios2() {

    var campos = '';
    var DC = true;
    campos = ['#txt_Nomproyecto', '#idClienteCS', '#tipoNegocio', '#tipoTrabajo', '#idresponsable2', '#descripcionCS', '#ResponsableCS'];
    if ($("#chx_OPD").prop("checked")) {
        if ($('#OPD2').val() === '') {
            DC = false;
        }
    }

    for (var i = 0; i < campos.length; i++) {
        if (($(campos[i]).val()) === '') {
//            $(campos[i]).css({
//                'box-shadow': '0 0 5px red',
//                'border-width': '1px',
//                'border-style': 'solid',
//                'border-color': 'red'
//
//            });
            DC = false;
        }

    }



    return DC;
}

function divemergente_nic() {
    $('#div_Nic').dialog({
        width: 400,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'N i cs',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $('#div_valorespredeterminados').show();
}

function limpiarFormulario() {
    $('#idClienteCS').val('');
    $('#txt_NombreClienteCS').val('');
    $('#cbx_nicCS').html('');
    $('#descripcionCS').val('');
    $('#descripcion').val('');
    $('#txt_responsable2').val('');
    $('#idresponsable2').val('');
    $('#OPD2').val('');
    $('#EmailContacto').val(''),
    $('#CargoContacto').val(''),
    $('#txt_Nomproyecto').val('');
    //cargarLineaNegocioCS();
    cargarTipoSolicitudCS();
    cargarResponsableCS();
    cargarInterventorCS();
    $("#chx_OPD").prop("checked", "");
    cargarComboGenerico('tipoNegocio',0,'');
    cargarComboGenerico('tipoTrabajo',1,'');
    adOPD();
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function Posicionar_div(id_objeto, e) {
    obj = document.getElementById(id_objeto);
    var posx = 0;
    var posy = 0;
    if (!e)
        var e = window.event;
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    }
    else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    else
        alert('ninguna de las anteriores');
    obj.style.left = posx + 'px';
    obj.style.top = posy + 'px';
    obj.style.zIndex = 1300;
}

function refrescarvalorespredeterminadosespedit(idcliente) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=11&idcliente=' + idcliente;
    jQuery("#valorespredeterminados").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#valorespredeterminados').trigger("reloadGrid");
}

function listarvalorespredeterminados(idcliente) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=11&idcliente=' + idcliente;
    if ($("#gview_valorespredeterminados").length) {
        refrescarvalorespredeterminadosespedit(idcliente);
    } else {
        jQuery("#valorespredeterminados").jqGrid({
            caption: 'Valores',
            url: url,
            datatype: 'json',
            height: 220,
            width: 300,
            colNames: ['Id', 'Nics'],
            colModel: [
                {name: 'id_cliente', index: 'id_cliente', hidden: true, align: 'left', width: '600px'},
                {name: 'nic', index: 'nic', sortable: true, editable: true, align: 'left', width: '600px'}

            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_valorespredeterminados',
            multiselect: false,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows', repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false},
            gridComplete: function () {

                /*var ids = jQuery("#valorespredeterminados").jqGrid('getDataIDs');
                 for (var i = 0; i < ids.length; i++) {
                 var cl = ids[i];
                 ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' align: 'center' height='15' title ='Editar'  onclick=\"pruebas('" + cl + "');\">";
                 jQuery("#valorespredeterminados").jqGrid('setRowData', ids[i], {actions: ed});
                 }*/
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }}).navGrid("#page_tabla_valorespredeterminados", {add: false, edit: false, del: false, search: false, refresh: false});
        jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
            caption: "Nuevo",
            title: "Agregar nueva fila",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {

                var grid = $("#valorespredeterminados")
                        , rowid = 'neo_' + grid.getRowData().length;
                grid.addRowData(rowid, {});
                grid.jqGrid('editRow', rowid, true, function () {
                    //  $("input, select", e.target).focus();
                }, null, null, {}, null, null, function (id) {
                    var g = $('#valorespredeterminados')
                            , r = g.jqGrid('getLocalRow', id);
                    if (!r.id) {
                        g.jqGrid('delRowData', id);
                    }
                });
            }
//position: "first"
        }); /*
         jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
         caption: "Guardar",
         title: "Guardar cambios",
         buttonicon: "ui-icon-save",
         onClickButton: function () {
         guardarValp("valorespredeterminados");
         }             //position: "first"
         });*/
        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function guardarValp() {
    var grid = jQuery("#valorespredeterminados")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;
    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);
        if (data.valor_xdefecto === '') {
            error = true;
            mensajesDelSistema('Digite el valor', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else {
            if (data.descripcion === '') {

                error = true;
                mensajesDelSistema('Digite la descripcion', '300', 'auto', false);
                grid.jqGrid('editRow', filas[i], true, function () {
                    $("input, select", e.target).focus();
                });
                break;
            }

        }

        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }
    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 41, informacion: JSON.stringify({rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    //alert(json.mensaje);
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    refrescarGridvalorespredeterminados();
                    //refrescarGridvalorespredeterminados();
                    //AgregarGridAso();
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}


function Recargar() {
    cargarPagina('jsp/opav/OportunidadNegocio/Solicitud/BuscarEditarSolicitud.jsp');
}
function refrescarGridSolicitudesIdcli(idcli) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=36&idcli=' + idcli;
    jQuery("#solicitudes").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#solicitudes').trigger("reloadGrid");
}


function cargarInfoSolicitudes() {
    var grid_tabla = jQuery("#tabla_infoSolicitud");
    if ($("#gview_tabla_infoSolicitud").length) {
        reloadGridInfoSolicitudes(grid_tabla, 39);
    } else {
        grid_tabla.jqGrid({
            caption: "SOLICITUDES",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '450',
            width: '1600',
            colNames: ['Idsolicitud', 'Fecha creacion', 'Responsable', 'Nombre Cliente', 'Nombre Proyecto', 'Foms', 'Costo Contratista', 'Valor Cotizacion', 'Etapa', 'Estado',
                'Codigo cliente', 'Tipo solicitud', 'Estado_cartera', 'Fecha validacion cartera', 'Interventor', 'Flag', 'id_trazabilidad', 'Id estado', 'presupuesto_terminado', 'Acciones'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'creation_date', index: 'creation_date', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'responsable', index: 'responsable', width: 180, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nomcli', index: 'nomcli', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 230, sortable: true, align: 'left', search: true},
                {name: 'num_os', index: 'num_os', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'valor_cotizacion', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'nombre_etapa', index: 'nombre_etapa', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_estado', index: 'nombre_estado', width: 160, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'codcli', index: 'codcli', width: 90, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'estado_cartera', index: 'estado_cartera', width: 100, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'fecha_validacion_cartera', index: 'fecha_validacion_cartera', width: 200, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'interventor2', index: 'interventor2', width: 110, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'flag', index: 'flag', width: 40, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'trazabilidad', index: 'trazabilidad', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_estado', index: 'id_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'presupuesto_terminado', index: 'presupuesto_terminado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 140, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ignoreCase: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                mostrarFormulario(rowid);
            }, gridComplete: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila;
                var estado_trazabilidad;
                var id_estado, iva_aiu = '';
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);
                    estado_trazabilidad = fila.trazabilidad;
                    id_estado = fila.id_estado;
                    presupuesto_terminado = fila.presupuesto_terminado;


                    var Cotizar = "<img src='/fintra/images/opav/Cotizacion_grey.png' align='absbottom'   style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Definicion de Rentabilidad y Esquema' );\">";
                    var iva_aiu = "<img src='/fintra/images/opav/iva_aiu_grey.png' align='absbottom' name='IVA_AIU' id='AYF'value='IVA - AIU'  width='26' height='26' title ='Cotizacion Definitiva'>";
                    var accion = "<img src='/fintra/images/opav/Transicion_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n' >";
                    var Cargar_Archivos = "<img src='/fintra/images/opav/File_add_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Cargar Archivos'  onclick=\"divemergente_Carga_Archivos('" + cl + "');\">";

//----------------------------------------
                    if (fila.trazabilidad == '1') {

                        if (id_estado == '140') {
                            Cotizar = "<img src='/fintra/images/opav/Cotizacion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Definicion de Rentabilidad y Esquema'  onclick=\"principalAsignacionCostos('" + cl + "');\">";
                            iva_aiu = "<img src='/fintra/images/opav/iva_aiu.png' align='absbottom' name='IVA_AIU' id='AYF'value='IVA_AIU'  width='26' height='26' title ='Cotizacion Definitiva'  onclick=\"abrir_Iva_Aiu('" + cl + "');\">";
                        }

                        if (!((id_estado == '110') || (id_estado == '190'))) {
                            accion = "<img src='/fintra/images/opav/Transicion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
                        }

                        if (!((id_estado == '110') || (id_estado == '190') || (id_estado == '194') || (id_estado == '195') || (id_estado == '130') || (id_estado == '180'))) {
                            Cargar_Archivos = "<img src='/fintra/images/opav/File_add.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Cargar Archivos'  onclick=\"divemergente_Carga_Archivos('" + cl + "');\">";
                        }
                    }


//----------------------------------------



                    grid_tabla.jqGrid('setRowData', ids[i], {actions: accion + Cargar_Archivos});

                }
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 39,
                    lineaNegocio: $('#linea_negocio').val(),
                    responsable: $('#responsable').val(),
                    solicitud: $('#solicitud').val(),
                    estadoCartera: $('#estadocartera').val(),
                    fechaInicio: $('#fechaini').val(),
                    fechafin: $('#fechafin').val(),
                    trazabilidad: '1',
                    etapaActual: $('#etapaActual').val(),
                    id_cliente: $('#id_cliente').val(),
                    nom_proyecto: $('#txt_nom_proyecto').val(),
                    foms: $('#txt_foms').val(),
                    tipo_proyecto: $('#tipo_proyecto').val()

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    multipleSearch: true,
                });
    }
}

function reloadGridInfoSolicitudes(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                lineaNegocio: $('#linea_negocio').val(),
                responsable: $('#responsable').val(),
                solicitud: $('#solicitud').val(),
                estadoCartera: $('#estadocartera').val(),
                fechaInicio: $('#fechaini').val(),
                fechafin: $('#fechafin').val(),
                trazabilidad: '1',
                etapaActual: $('#etapaActual').val(),
                id_cliente: $('#id_cliente').val(),
                nom_proyecto: $('#txt_nom_proyecto').val(),
                foms: $('#txt_foms').val(),
                tipo_proyecto: $('#tipo_proyecto').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function abrir_Iva_Aiu(id_solicitud) {

    window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/OportunidadNegocio/&pagina=modalidadProyecto.jsp?num_solicitud=" + id_solicitud, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');

}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarEstadoCartera() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 38
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#estadocartera').html('');
                $('#estadocartera').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#estadocartera').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function  mostrarFormulario(id_solicitud) {
    cargarSolicitud(id_solicitud);
    $("#fformPrincipal").dialog({
        width: '994',
        height: '870',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'FORMULARIO',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Actualizar": function () {
                modificarSolicitud();
            },
            "Salir": function () {
                $(this).dialog("close");
                limpiarFormulario();
            }
        }
    });
}

function  mostrarFormularioCrearSolicitud() {
    //cargarLineaNegocioCS();
    ordenarCombo('tipoNegocio');
    cargarTipoSolicitudCS();
    cargarResponsableCS();
    cargarInterventorCS();
    autocompetarcliente();
    establecerFechaLimite();
    $("#formulario").dialog({
        width: '827',
        height: '540',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'FORMULARIO',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Crear": function () {
                crearSolicitud();
            },
            "Salir": function () {
                mensajeConfirmAction("Recuerde guardar los cambios. Esta seguro que desea salir ?", "230", "150", cerrarDialogo, "formulario");
            }
        }
    });
}

function cargarLineaNegocioCS() {
    $('#tipoNegocio').html('');
    $('#tipoNegocio').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 12
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#tipoNegocio').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTipoSolicitudCS() {
    $('#TipoSolicitudCS').html('');
    $('#TipoSolicitudCS').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 13,
            idLineaDeNegocio: $("#tipoNegocio").val()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#TipoSolicitudCS').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarResponsableCS() {
    $('#ResponsableCS').html('');
    $('#ResponsableCS').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 14
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#ResponsableCS').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarInterventorCS() {
    $('#InterventorCS').html('');
    $('#InterventorCS').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 15
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#InterventorCS').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function autocompetarcliente() {
    $("#txt_NombreClienteCS").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 4
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            var idcliente = ui.item.mivar;
            $('#idClienteCS').val(idcliente);
            cargarNicsCS(idcliente);
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function establecerFechaLimite(){
    $("#fecha_limite").datepicker({
        dateFormat: 'yy-mm-dd',
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });    
    var fecha = new Date();
    var dd = fecha.getDate();
    var mm = fecha.getMonth()+1; //January is 0!
    var aaaa = fecha.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    } 

    if(mm<10) {
        mm = '0'+mm
    } 

    output = aaaa + '-' + mm + '-' + dd ;         
    $('#fecha_limite').val(output);
}

function cargarNicsCS(idcliente) {
    $('#cbx_nicCS').html('');
    $('#cbx_nicCS').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 11,
            idcliente: idcliente
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json.rows) {
                        $('#cbx_nicCS').append('<option value=' + json.rows[key].id_cliente + '>' + json.rows[key].nic + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function crearSolicitud() {
    if (CamposObligatorios2()) {
        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            dataType: 'json',
            async: false,
            data: {
                opcion: 16,
                idcliente: $('#idClienteCS').val(),
                nic: $('#cbx_nicCS option:selected').text(),
                descripcion: encodeURIComponent($('#descripcionCS').val()),
                opd: $('#OPD2').val(),
                responsable: encodeURIComponent($('#ResponsableCS').val()),
                interventor: encodeURIComponent($('#InterventorCS').val()),
                tipoNegocio: $('#tipoNegocio').val(),
                tipoTrabajo: $('#tipoTrabajo').val(),
                fecha_limite: $('#fecha_limite').val(),
                //tiposolicitud: $('#TipoSolicitudCS option:selected').text(),
                EmailContacto: $('#EmailContacto').val(),
                CargoContacto: $('#CargoContacto').val(),
                nomproyecto: encodeURIComponent($('#txt_Nomproyecto').val()),
                enviar: $('#chx_envio_cartera').prop('checked')
            },
            success: function (json) {
                
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    if (json.respuesta === "OK") {
                        mensajesDelSistema("Se Creo la solicitud de forma correcta", '250', '150', true);
                        limpiarFormulario();
                        $('#solicitud').val(json.id_solicitud);
                        cargarInfoSolicitudes();
                        $('#formulario').dialog("close");
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Datos Incompletos", '250', '180');
    }
}


function  mostrarVentanaAccion(id_solicitud) {
    $('#descripcion_causal').val('');
    var grid_tabla = $("#tabla_infoSolicitud");
    var trazabilidad = grid_tabla.getRowData(id_solicitud).trazabilidad;
    var id_estado = grid_tabla.getRowData(id_solicitud).id_estado;
    var nombre_estado = grid_tabla.getRowData(id_solicitud).nombre_estado;
    $('#idsolicitud').val(id_solicitud);
    $('#estadoActual').val(nombre_estado);
    $('#idestadoActual').val(id_estado);
    cargarEtapas(trazabilidad);
    cargarTrazabilidadOferta(id_solicitud);
    cargarEstadoEtapas(id_estado);
    $("#cambioEtapa").dialog({
        width: '800',
        height: '475',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Cambio de Estado',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Actualizar": function () {
                cambiarEstado();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarEtapas(trazabilidad) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 40,
            etapa: trazabilidad
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#etapa').val('');
                $('#idetapa').val('');
                for (var datos in json) {
                    $('#etapa').val(json[datos]);
                    $('#idetapa').val(datos);
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarEstadoEtapas(id_estado) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 41,
            etapa: 1,
            id_estado: id_estado
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#estado').html('');
                $('#estado').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#estado').append('<option value=' + json[datos].id_estado_destino + '>' + json[datos].nombre_estado + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cambiarEstado() {

    var estado = $("#estado").val();

    if ((estado !== '') && ($("#descripcion_causal").val().length > 10)) {

        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            dataType: 'json',
            async: false,
            data: {
                opcion: 42,
                etapa: $("#idetapa").val(),
                estado_actual: $("#idestadoActual").val(),
                estados: $("#estado").val(),
                causal: $('#causal').val(),
                observacion: $('#descripcion_causal').val(),
                idsolicitud: $('#idsolicitud').val()
            },
            success: function (json) {
                console.log(json.respuesta);
                var resp = json.respuesta;
                if (resp === 'Guardado') {
                    toastr.success('Cambio satisfactorio', 'Enviado');
                    $("#cambioEtapa").dialog("close");
                    cargarInfoSolicitudes();
                } else {
                    mensajesDelSistema("Error", '204', '140', false);
                }
            }
        });
    } else {
        toastr.error('Por favor llene todos los campos', 'Error');
    }
}

function generarCartaAceptacionCliente(id_solicitud) {
    var url = '/fintra/controlleropav?estado=Minutas&accion=Contratacion';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 15,
            num_solicitud: id_solicitud
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    window.open('.' + json.Ruta);
                }
            } else {
                mensajesDelSistema("Lo sentimos ocurri� un error al generar carta de aceptacion!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function generarContrato(id_solicitud) {
    var url = '/fintra/controlleropav?estado=Minutas&accion=Contratacion';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 16,
            num_solicitud: id_solicitud
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    window.open('.' + json.Ruta);
                }
            } else {
                mensajesDelSistema("Lo sentimos ocurri� un error al generar contrato!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function adOPD() {

    if ($("#chx_OPD").prop("checked")) {
        $('#OPD2').css({'display': ''});
        $('#OPD2').val('');
    } else {
        $('#OPD2').css({'display': 'none'});
    }

}


function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}


function cargarDivCosteProyecto() {
    $("#div_coste_proyecto").dialog({
        width: 1317,
        height: 750,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Facturacion Parcial',
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Guardar": function () {
                guardarCosteProyecto();
                $(this).dialog("destroy");
            },
            "Cancelar": function () {
                mensajeConfirmAction("Recuerde guardar los cambios. Esta seguro que desea salir ?", "230", "150", cerrarDialogo, "div_coste_proyecto");
                //$(this).dialog("destroy");


            }

        }
    });
}

function guardarCosteProyecto() {
    var filasFact = jQuery("#tabla_facturacion").jqGrid('getRowData');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 49,
            id_accion: $('#idaccion').val(),
            fecha: $('#fecha').val(),
            visualizacion: $('#visualizacion').val(),
            modalidad: $('#modalidad').val(),
            valcotizacion: numberSinComas($('#valcotizacion').val()),
            valdesc: numberSinComas($('#valdesc').val()),
            subtotal: numberSinComas($('#subtotal').val()),
            perc_iva: numberSinComas($('#perc_iva').val()),
            valiva: numberSinComas($('#valiva').val()),
            perc_admon: numberSinComas($('#perc_admon').val()),
            val_admon: numberSinComas($('#val_admon').val()),
            perc_imprevisto: numberSinComas($('#perc_imprevisto').val()),
            val_imprevisto: numberSinComas($('#val_imprevisto').val()),
            perc_utilidad: numberSinComas($('#perc_utilidad').val()),
            val_utilidad: numberSinComas($('#val_utilidad').val()),
            perc_aiu: numberSinComas($('#perc_aiu').val()),
            val_aiu: numberSinComas($('#val_aiu').val()),
            val_total: numberSinComas($('#val_total').val()),
            anticipo: numberSinComas($('#anticipo').val()),
            perc_anticipo: numberSinComas($('#perc_anticipo').val()),
            val_anticipo: numberSinComas($('#val_anticipo').val()),
            retegarantia: numberSinComas($('#retegarantia').val()),
            perc_rete: numberSinComas($('#perc_rete').val()),
            idsolicitud: $('#num_solicitud').val(),
            perc_descuento: numberSinComas($('#perc_desc').val()),
            listadofacturas: JSON.stringify({facturas: filasFact})
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (json.respuesta === "OK") {
                    mensajesDelSistema("Se actualizo de forma correcta la cotizacion", '250', '150', true);
                    $('#formulario').dialog("close");
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function principal(idsolicitud) {
    $('#num_solicitud').val(idsolicitud);
    ordenarCombo('visualizacion');
    ordenarCombo('modalidad');
    buscaraccionprincipal(idsolicitud);
    cargarCotizacion($('#idaccion').val());
    cargarFacturacionParcial(idsolicitud);
    numberConComas2();
    cargarDivCosteProyecto();
}

function buscaraccionprincipal(idsolicitud) {

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 46,
            idsolicitud: idsolicitud
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }

                for (var key in json.rows) {
                    $('#idaccion').val(json.rows[key].id_accion);
                }


            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarFacturacionParcial(id_solicitud) {


    var grid_tbl_facturacion = jQuery("#tabla_facturacion");
    if ($("#gview_tabla_facturacion").length) {
        refrescarGridFacturacionParcial();
    } else {
        grid_tbl_facturacion.jqGrid({
            caption: "Facturas Proyectadas",
            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
            datatype: "json",
            height: '150',
            width: '900',
            colNames: ['Id', 'Valor', 'Dias Ejecucion', 'F. Facturacion', 'Dia Pago', 'Amortizaci�n', 'Retegarantia', 'Borrar'],
            colModel: [
                {name: 'id', index: 'id', width: 120, align: 'left', key: true, hidden: true},
                {name: 'valor', index: 'valor', sortable: true, editable: true, width: 140, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'dias_ejecucion', index: 'dias_ejecucion', editable: true, width: 120, align: 'center',
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'fecha_facturacion', index: 'fecha_facturacion', editable: false, width: 120, align: 'left'},
                {name: 'dia_pago', index: 'dia_pago', editable: true, width: 110, resizable: false, sortable: true, align: 'center', edittype: 'select',
                    editoptions: {
                        value: jsonDiasPago,
                        defaultValue: 1
                    }
                },
                {name: 'amortizacion', index: 'amortizacion', sortable: true, width: 140, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'retegarantia', index: 'retegarantia', sortable: true, width: 140, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'Borrar', index: 'Borrar', resizable: false, align: 'center', width: '50px', search: false}


            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_facturacion'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 48,
                    num_solicitud: id_solicitud
                }
            },
            gridComplete: function () {
                var grid_tabla = jQuery("#tabla_facturacion");
                var colSumValor = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum');
                var colSumAmort = jQuery("#tabla_facturacion").jqGrid('getCol', 'amortizacion', false, 'sum');
                var colSumRet = jQuery("#tabla_facturacion").jqGrid('getCol', 'retegarantia', false, 'sum');
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor: colSumValor});
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {amortizacion: colSumAmort});
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {retegarantia: colSumRet});
                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);
                    var wi = "<img src='/fintra/images/botones/iconos/anular.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('�Esta seguro de anular esta factura parcial?, se eliminara de forma permanente','230','190',anularfaturaparcial,'" + cl + "');\">";
                    grid_tabla.jqGrid('setRowData', ids[i], {Borrar: wi});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                jQuery("#tabla_facturacion").jqGrid('editRow', rowid, true, function () {
                    //$("input, select", e.target).focus();
                }, null, null, {}, function (rowid) {
                    var valor = $("#tabla_facturacion").getRowData(rowid).valor;
                    var dias_ejecucion = $("#tabla_facturacion").getRowData(rowid).dias_ejecucion;
                    var fecha_facturacion = sumaFecha(dias_ejecucion);
                    jQuery("#tabla_facturacion").jqGrid('setCell', rowid, 'fecha_facturacion', fecha_facturacion);
                    jQuery("#tabla_facturacion").jqGrid('setCell', rowid, 'fecha_facturacion', fecha_facturacion);
                    var sumValor = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum');
                    var total = parseFloat(numberSinComas($('#val_total').val()));
                    //total ingresa el valor con AIU o con IVA

                    if (sumValor > total) {
                        setTimeout(function () {
                            mensajesDelSistema("EL VALOR DE LAS CUOTAS EXCEDE EL TOTAL", '320', '165');
                        }, 500);
                        $("#tabla_facturacion").jqGrid('delRowData', rowid);
                        calculateAmortizacion();
                        return;
                    }
                    calculateAmortizacion();
                    jQuery("#tabla_facturacion").jqGrid('setCell', rowid, 'retegarantia', parseFloat((valor * $('#perc_rete').val()) / 100));
                    calculateFooter();
                    //alert(rowid);
                });
                return;
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_facturacion", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_facturacion").jqGrid("navButtonAdd", "#page_tabla_facturacion", {
            caption: "Agregar",
            title: "Agregar Facturacion",
            onClickButton: function () {

                var grid = $("#tabla_facturacion");
                var rowid = 'neo_' + grid.getRowData().length;
                var defaultData = {valor: (numberSinComas($("#val_total").val()) - jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum'))
                    , dias_ejecucion: 0, dia_pago: 0, amortizacion: 0, retegarantia: 0};
                grid.addRowData(rowid, defaultData);
                grid.jqGrid('editRow', rowid, true, function () {
                    //  $("input, select", e.target).focus();
                }, null, null, {}, function (rowid) {

                    var valor = $("#tabla_facturacion").getRowData(rowid).valor;
                    var dias_ejecucion = $("#tabla_facturacion").getRowData(rowid).dias_ejecucion;
                    var fecha_facturacion = sumaFecha(dias_ejecucion);
                    jQuery("#tabla_facturacion").jqGrid('setCell', rowid, 'fecha_facturacion', fecha_facturacion);
                    var sumValor = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum');
                    var total = parseFloat(numberSinComas($('#val_total').val()));
                    //total ingresa el valor con AIU o con IVA
                    if (sumValor > total) {
                        setTimeout(function () {
                            mensajesDelSistema("EL VALOR DE LAS CUOTAS EXCEDE EL TOTAL", '320', '165');
                        }, 500);
                        $("#tabla_facturacion").jqGrid('delRowData', rowid);
                        calculateAmortizacion();
                        return;
                    }

                    calculateAmortizacion();
                    jQuery("#tabla_facturacion").jqGrid('setCell', rowid, 'retegarantia', parseFloat((valor * $('#perc_rete').val()) / 100));
                    calculateFooter();
                    //alert(rowid);      

                });
                //alert('Ha pulsado sobre el boton Agregar');
            }
        });
    }
}


function refrescarGridFacturacionParcial() {
    jQuery("#tabla_facturacion").setGridParam({
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 48,
                num_solicitud: $('#num_solicitud').val()
            }
        }
    });
    jQuery('#tabla_facturacion').trigger("reloadGrid");
}


function anularfaturaparcial(id) {

    if (id.startsWith("neo_")) {
        $("#tabla_facturacion").jqGrid('delRowData', id);
    } else {
        $.ajax({
            type: 'POST',
            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
            dataType: 'json',
            async: false,
            data: {
                opcion: 55,
                id_factura_parcial: id
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }

                    $("#tabla_facturacion").jqGrid('delRowData', id);
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }



}

function listarComboGrid() {
    var Result = {};
    $.ajax({
        type: 'GET',
        url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion&opcion=9",
        dataType: 'json',
        async: false,
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    Result = {};
                } else {
                    Result = json;
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return Result;
}


function numberConComas2() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).val(numberConComas($(campos[i]).val()));
    }
}
function agregarclases() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon', '#perc_rete'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}
function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}


function sumaFecha(d, fecha)
{
    var Fecha = new Date();
    var sFecha = fecha || (Fecha.getDate() + "-" + (Fecha.getMonth() + 1) + "-" + Fecha.getFullYear());
    var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
    var aFecha = sFecha.split(sep);
    var fecha = aFecha[2] + '/' + aFecha[1] + '/' + aFecha[0];
    fecha = new Date(fecha);
    fecha.setDate(fecha.getDate() + parseInt(d));
    var anno = fecha.getFullYear();
    var mes = fecha.getMonth() + 1;
    var dia = fecha.getDate();
    mes = (mes < 10) ? ("0" + mes) : mes;
    dia = (dia < 10) ? ("0" + dia) : dia;
    var fechaFinal = anno + sep + mes + sep + dia;
    return (fechaFinal);
}

function calculateAmortizacion() {
    var num_records = jQuery("#tabla_facturacion").jqGrid('getGridParam', 'records');
    var valor_amort = ((parseInt(num_records) > 0) ? (parseFloat(numberSinComas($('#val_anticipo').val())) / parseInt(num_records)) : 0);
    var ids = jQuery("#tabla_facturacion").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        jQuery("#tabla_facturacion").jqGrid('setCell', ids[i], 'amortizacion', valor_amort);
    }
}

function calculateretegarantia() {

//var num_records = jQuery("#tabla_facturacion").jqGrid('getGridParam', 'records');
    var valor = 0; //((parseInt(num_records) > 0) ? (parseFloat(numberSinComas($('#perc_rete').val()))) : 0);
    var ids = jQuery("#tabla_facturacion").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {

        valor = numberSinComas(jQuery("#tabla_facturacion").jqGrid('getCell', ids[i], 'valor'));
        jQuery("#tabla_facturacion").jqGrid('setCell', ids[i], 'retegarantia', parseFloat((valor * $('#perc_rete').val()) / 100));
    }
    calculateFooter();
}
function calculateFooter() {
    var sumValor = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum');
    var sumAmort = jQuery("#tabla_facturacion").jqGrid('getCol', 'amortizacion', false, 'sum');
    var sumumRet = jQuery("#tabla_facturacion").jqGrid('getCol', 'retegarantia', false, 'sum');
    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor: sumValor});
    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {amortizacion: sumAmort});
    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {retegarantia: sumumRet});
}

function validateGridFacturacion() {
    var estado = true;
    var ids = jQuery("#tabla_facturacion").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var valor = jQuery("#tabla_facturacion").getRowData(ids[i]).valor;
        if (parseFloat(valor) === 0) {
            estado = false;
            mensajesDelSistema('Facturas Parciales: Informacion invalida en fila ' + (i + 1), '250', '150');
        }
    }
    return estado;
}
function cargarCotizacion(idaccion) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 47,
            idaccion: idaccion
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }

                $('#CodCliente').val(json.cod_cli);
                $('#NombreCliente2').val(json.nonmbre_cliente);
                $('#CodCotizacion').val(json.no_cotizacion);
                $('#fecha').val(json.vigencia_cotizacion);
                $('#modalidad').val(json.modalidad_comercial);
                $('#visualizacion').val(json.forma_visualizacion);
                $('#valcotizacion').val(json.valor_cotizacion);
                $('#valdesc').val(json.valor_descuento);
                $('#subtotal').val(json.subtotal);
                $('#perc_desc').val(json.perc_descuento);
                habdesMod();
                if (json.modalidad_comercial === "0") {
                    $('#perc_iva').val(json.perc_iva);
                    $('#valiva').val(json.valor_iva);
                } else {
                    $('#perc_admon').val(json.perc_administracion);
                    $('#perc_imprevisto').val(json.perc_imprevisto);
                    $('#perc_utilidad').val(json.perc_utilidad);
                }
                $('#anticipo').val(json.anticipo);
                // habdesAnt();
                if (json.anticipo === "1") {
                    $('#perc_anticipo').val(json.perc_anticipo);
                    $('#val_anticipo').val(json.valor_anticipo);
                    resta();
                } else {
                    $('#perc_anticipo').val(0);
                    $('#valor_anticipo').val(0);
                }

                $('#retegarantia').val(json.retegarantia);
                habdesRet();
                if (json.retegarantia === "1") {
                    $('#perc_rete').val(json.perc_retegarantia);
                } else {
                    $('#perc_retegarantia').val(0);
                }




            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

///////

function cargarinfo(accion) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Cotizacion&accion=Sl',
        dataType: 'json',
        async: false,
        data: {
            opcion: 2,
            id_accion: accion
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }

                $('#CodCliente').val(json.id_cliente);
                $('#NombreCliente2').val(json.nomcli);
                $('#CodCotizacion').val(json.serie_cot);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function habdesMod() {

    if ($('#modalidad').val() === '0') {
        $('#perc_iva').val(16);
        $('#perc_admon').val(0);
        $('#perc_imprevisto').val(0);
        $('#perc_utilidad').val(0);
        document.getElementById("perc_admon").disabled = true;
        document.getElementById("perc_imprevisto").disabled = true;
        document.getElementById("perc_utilidad").disabled = true;
    } else {
        $('#perc_iva').val(0);
        document.getElementById("perc_admon").disabled = false;
        document.getElementById("perc_imprevisto").disabled = false;
        document.getElementById("perc_utilidad").disabled = false;
    }

    resta();
}

function habdesAnt() {

    if ($('#anticipo').val() === '1') {
        $('#perc_anticipo').val(0);
        $('#val_anticipo').val(0);
        document.getElementById("perc_anticipo").disabled = false;
    } else {
        $('#perc_anticipo').val(0);
        $('#val_anticipo').val(0);
        document.getElementById("perc_anticipo").disabled = true;
    }

    resta();
}

function habdesRet() {

    if ($('#retegarantia').val() === '1') {
        $('#perc_rete').val(0);
        document.getElementById("perc_rete").disabled = false;
    } else {
        $('#perc_rete').val(0);
        document.getElementById("perc_rete").disabled = true;
    }

}

function resta() {

    $('#valdesc').val((numberSinComas($('#perc_desc').val())) * (numberSinComas($('#valcotizacion').val())) / 100);
    var rest = numberSinComas($('#valcotizacion').val().replace('$', '')) - numberSinComas($('#valdesc').val());
    $('#subtotal').val(rest.toFixed(2));
    $('#valiva').val((rest * $('#perc_iva').val() / 100).toFixed(2));
    $('#val_admon').val(($('#subtotal').val() * ($('#perc_admon').val() * 1) / 100).toFixed(2));
    $('#val_imprevisto').val(($('#subtotal').val() * ($('#perc_imprevisto').val() * 1) / 100).toFixed(2));
    $('#val_utilidad').val(($('#subtotal').val() * ($('#perc_utilidad').val() * 1) / 100).toFixed(2));
    $('#perc_aiu').val(($('#perc_admon').val() * 1) + ($('#perc_imprevisto').val() * 1) + ($('#perc_utilidad').val() * 1));
    $('#val_aiu').val((rest * $('#perc_aiu').val() / 100).toFixed(2));
    $('#val_total').val(($('#subtotal').val() * 1 + $('#valiva').val() * 1 + $('#val_aiu').val() * 1).toFixed(2));
    $('#valdesc').val(numberConComas($('#valdesc').val()));
    $('#val_anticipo').val(($('#subtotal').val() * $('#perc_anticipo').val() / 100).toFixed(2));
    $('#val_admon').val(numberConComas($('#val_admon').val()));
    $('#val_imprevisto').val(numberConComas($('#val_imprevisto').val()));
    $('#val_utilidad').val(numberConComas($('#val_utilidad').val()));
    $('#val_aiu').val(numberConComas($('#val_aiu').val()));
    $('#val_total').val(numberConComas($('#val_total').val()));
    $('#val_anticipo').val(numberConComas($('#val_anticipo').val()));
    $('#subtotal').val(numberConComas($('#subtotal').val()));
    $('#valiva').val(numberConComas($('#valiva').val()));
}

function mensajeConfirmAction(msj, width, height, okAction, id) {
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                limpiarFormulario();
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}
function mensajeConfirmAction(msj, width, height, okAction, id, id2) {
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                limpiarFormulario();
                okAction(id, id2);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function cerrarDialogo(Nom_div) {
    $("#" + Nom_div).dialog("destroy");
}

function cargar_Archivos() {
//grab all form data  
    var fd = (new FormData(document.getElementById('form_Cargar_Archivos')));
    var archivo = document.getElementById('examinar').value;
    if (archivo === "") {
        mensajesDelSistema("No ha seleccionado ning&uacute;n archivo. Por favor, seleccione uno!!", '250', '150');
        return;
    }
    if (!validarNombresArchivos()) {
        return;
    }

    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function () {

        $.ajax({
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=56',
            type: 'POST',
            data: fd,
            dataType: 'json',
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            success: function (json)
            {
                $("#examinar").val("");
                obtenerArchivosCargados();
                if (json.respuesta === "OK") {
                    $("#dialogLoading").dialog('close');
//                        obtenerArchivosCargados();                
                } else {
                    $("#dialogLoading").dialog('close');
                    alert(".::ERROR AL CARGAR ARCHIVO::.");
                }
            }
        });
    }, 500);
}

function obtenerArchivosCargados() {
    $('#tbl_archivos_cargados').html('');
    var num_sol = $('#idsolicitud_carga_archivo').val();
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {opcion: 57,
            id_solicitud: num_sol},
        async: false,
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        success: function (jsonData) {
            if (!isEmptyJSON(jsonData)) {
                $('#tbl_archivos_cargados').append("<thead><tr><td>#</td><td>Nombre Archivo</td><td>Accion</td></thead>");
                for (var i = 0; i < jsonData.length; i++) {
                    var nomarchivo = jsonData[i];
                    var Eliminar = "<img src='/fintra/images/botones/iconos/anular.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('�Esta seguro de Eliminar este Archivo?, se eliminara de forma permanente','230','190',EliminarArchivo,'" + num_sol + "','" + nomarchivo + "');\">";
                    var Abrir = "<img src='/fintra/images/botones/iconos/cotizacion.png' align='absbottom'  name='Abrir' id='Abrir' width='15' height='15' title ='Abrir'  onclick=\"AbrirArchivo('" + num_sol + "','" + nomarchivo + "');\">";
                    $('#tbl_archivos_cargados').append("<tr><th scope='row'>" + (i + 1) + "</th><td>" + nomarchivo + "</td><td>" + Abrir + "  " + Eliminar + "</td></tr>");
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function AbrirArchivo(num_sol, nomarchivo) {

    $.ajax({
        type: "POST",
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        async: false,
        dataType: "json",
        data: {
            opcion: 59,
            num_solicitud: num_sol,
            nomarchivo: nomarchivo
        },
        success: function (json) {
            //alert(data);
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "SI") {

                    window.open("/fintra/images/multiservicios/" + json.login + "/" + nomarchivo);
                } else {
                    mensajesDelSistema(".::ERROR AL OBTENER ARCHIVO::.", '250', '150');
                }

            }
        }
    });
}

function EliminarArchivo(num_sol, nomarchivo) {
    $.ajax({
        type: "POST",
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        async: false,
        dataType: "json",
        data: {
            opcion: 58,
            num_solicitud: num_sol,
            nomarchivo: nomarchivo
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {
                    obtenerArchivosCargados();
                } else {
                    mensajesDelSistema(".::ERROR AL ELIMINAR ARCHIVO::.", '250', '150');
                }

            }
        }
    });
}


function divemergente_Carga_Archivos(id_solicitud) {
    $("#examinar").val("");
    $('#idsolicitud_carga_archivo').val(id_solicitud);
    obtenerArchivosCargados();
    $('#div_Cargar_Archivos').dialog({
        width: 600,
        height: 385,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Carga de Archivos',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $('#div_Solicitidudes').show();
}

function validarNombresArchivos() {
    var archivo = document.getElementById('examinar');
    for (var i = 0; i < archivo.files.length; i++) {
        var file = archivo.files[i].name;
        var fname = file.substring(0, file.lastIndexOf('.'));
        if (!fname.match(/^[0-9a-zA-Z\_\-\ ]*$/)) {
            alert('Nombre de archivo no v�lido. por favor, Verifique. Archivo: ' + file);
            return false;
        }
    }
    return true;
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });
    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}


function cargarDivAsignacionCostos() {
    $("#div_Coste_Proyecto").dialog({
        width: 1317,
        height: 750,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Coste Proyecto',
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Guardar": function () {
                //
                $(this).dialog("destroy");
            },
            "Cancelar": function () {
                mensajeConfirmAction("Recuerde guardar los cambios. Esta seguro que desea salir ?", "230", "150", cerrarDialogo, "div_Coste_Proyecto");
                //$(this).dialog("destroy");


            }

        }
    });
}

function cargarInsumosGrid() {
    var grid_tabla = jQuery("#tabla_Coste_Proyecto");
    if ($("#gview_tabla_Coste_Proyecto").length) {
        reloadGridcargarInsumosGrid(grid_tabla, 60);
    } else {
        grid_tabla.jqGrid({
            caption: "Insumos",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '330',
            width: '1024',
            colNames: ['id_solicitud', 'id_tipo_insumo', 'descripcion_insumo', 'id_categoria', 'nombre_categoria', 'id_subcategoria', 'nombre_subcategoria',
                'id_insumo', 'nombre_insumo', 'condigo_material', 'id_apu', 'nombre_apu', 'id_grupo_apu', 'nombre_grupo_apu', 'id_actividad_wbs', 'nombre_actividad',
                'id_capitulo_wbs', 'nombre_capitulo', 'id_disciplina_wbs', 'nombre_disciplina', 'id_area_proyecto_wbs', 'nombre_area', 'nombre_proyecto', 'actions'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'id_tipo_insumo', index: 'id_tipo_insumo', width: 200, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'descripcion_insumo', index: 'descripcion_insumo', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_categoria', index: 'id_categoria', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_categoria', index: 'nombre_categoria', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_subcategoria', index: 'id_subcategoria', width: '200px', sortable: true, align: 'center', hidden: false, search: true},
                {name: 'nombre_subcategoria', index: 'nombre_subcategoria', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_insumo', index: 'id_insumo', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_insumo', index: 'nombre_insumo', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'condigo_material', index: 'condigo_material', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_apu', index: 'id_apu', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_apu', index: 'nombre_apu', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_grupo_apu', index: 'id_grupo_apu', width: '200px', sortable: true, align: 'center', hidden: false, search: false},
                {name: 'nombre_grupo_apu', index: 'nombre_grupo_apu', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_actividad_wbs', index: 'id_actividad_wbs', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_actividad', index: 'nombre_actividad', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_capitulo_wbs', index: 'id_capitulo_wbs', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_capitulo', index: 'nombre_capitulo', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_disciplina_wbs', index: 'id_disciplina_wbs', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_disciplina', index: 'nombre_disciplina', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_area_proyecto_wbs', index: 'id_area_proyecto_wbs', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_area', index: 'nombre_area', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '160px', search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: false,
            pager: '#pager_Coste_Proyecto',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                alert(rowid);
            }, //gridComplete: function () {
            //                var ids = grid_tabla.jqGrid('getDataIDs');
            //                var fila;
            //                var estado_trazabilidad;
            //                var id_estado;
            //                for (var i = 0; i < ids.length; i++) {
            //                    var cl = ids[i];
            //                    fila = grid_tabla.jqGrid("getLocalRow", cl);
            //                    estado_trazabilidad = fila.trazabilidad;
            //                    id_estado = fila.id_estado;
            //                    if (id_estado === '7') {
            ////                        img src='/fintra/images/payment_card_credit-128.png' align='absbottom' 
            //                        var ca = "<img src='/fintra/images/botones/iconos/carta.png' align='absbottom' name='carta' id='editar'value='Carta'  width='19' height='19' title ='Generar carta aceptacion del cliente'  onclick=\"generarCartaAceptacionCliente('" + cl + "');\">";
            //                        //ca = "<input style='height:25px;width:50px;margin-left: 8px;' type='button' name='carta' id='editar'value='Carta'  width='19' height='19' title ='Generar carta aceptacion del cliente'  onclick=\"generarCartaAceptacionCliente('" + cl + "');\">";
            //                        //ed = "<input style='height:25px;width:67px;margin-left: 8px;' type='button' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
            //                        var ed = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
            //                        grid_tabla.jqGrid('setRowData', ids[i], {actions: ed + ' ' + ca});
            //                    } else if (id_estado === '14') {
            //                        var co = "<img src='/fintra/images/botones/iconos/contrato.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='19' height='19' title ='Generar Contrato'  onclick=\"generarContrato('" + cl + "');\">";
            //                        //co = "<input style='height:25px;width:69px;margin-left: 8px;' type='button' name='contrato' id='editar'value='Contrato'  width='19' height='19' title ='Generar Contrato'  onclick=\"funcion('" + cl + "');\">";
            //                        //ed = "<input style='height:25px;width:67px;margin-left: 8px;' type='button' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
            //                        var ed = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
            //                        grid_tabla.jqGrid('setRowData', ids[i], {actions: ed + ' ' + co});
            //                    } else {
            //                        var ed = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
            //                        var wi = "<img src='/fintra/images/botones/iconos/verHistorico.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Facturacion Parcial'  onclick=\"principal('" + cl + "');\">";
            //                        var lol = "<img src='/fintra/images/payment_card.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Facturacion Parcial'  onclick=\"principalAsignacionCostos('" + cl + "');\">";
            //                        var ca = "<img src='/fintra/images/botones/iconos/procesos.gif' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Cargar Archivos'  onclick=\"divemergente_Carga_Archivos('" + cl + "');\">";
            //                        //ed = "<input style='height:25px;width:67px;margin-left: 8px;' type='button' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
            //                        grid_tabla.jqGrid('setRowData', ids[i], {actions: ed + lol +wi + ca});
            //                    }
            //
            //                }
            //  },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 60,
                    idsolicitud: $('#num_solicitud').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager_Coste_Proyecto", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
    }
}


function reloadGridcargarInsumosGrid(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                idsolicitud: $('#num_solicitud').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}



function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function  mostrarVentanaAccion2(id_solicitud) {


    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + "Esta seguro que desea enviar a Presupuesto?");
    $("#dialogMsj").dialog({
        width: 250,
        height: 200,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                //insertar_cabecera_tabla_temporal(id_solicitud);
                update_presupuesto_terminado(id_solicitud, 'N');
                $("#btn_Aceptar_tipoClienteP").click();
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}


function  update_presupuesto_terminado(id_solicitud, estado) {


    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 79,
            id_solicitud: id_solicitud,
            estadoo: estado

        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (json.respuesta === "OK") {

                    mensajesDelSistema("Se envio de forma correcta esta solicitud", '250', '150', true);
                    cargarInfoSolicitudes();
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function  mostrarVentanaAccion3(id_solicitud) {


    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + "Esta seguro que desea enviar a Presupuesto?");
    $("#dialogMsj").dialog({
        width: 250,
        height: 200,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                //insertar_cabecera_tabla_temporal(id_solicitud);
                Generar_OT(id_solicitud);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}


function  Generar_OT(id_solicitud) {


    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 80,
            id_solicitud: id_solicitud

        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (json.respuesta === "OK") {

                    mensajesDelSistema("Se Genero de forma Correcta la OT.", '250', '150', true);
                    cargarInfoSolicitudes();
                } else {
                    mensajesDelSistema("Ocurrio un problema al generar la OT.", '250', '150', true);

                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTrazabilidadOferta(id_solicitud) {
   $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 81,
            idsolicitud: id_solicitud
        },
        success: function (json) {
            if (json.error) {
                
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#trazabilidad').val(json.respuesta);

            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            $('#trazabilidad').val(xhr.responseText);
        }
    });
}

function generarCotizacion(id_solicitud) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 85,
            id_solicitud: id_solicitud
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    window.open('.' + json.Ruta);
                }
            } else {
                mensajesDelSistema("Lo sentimos ocurri� un error al generar cotizaci�n!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


