
$(document).ready(function () {
    maximizarventana();
    $("#fechaini").val('');
    $("#fechafin").val('');
    $("#fechaini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $('#ui-datepicker-div').css('clip', 'auto');

    limpiarFormulario();
    cargarCbxLineaNegocio();
    //cargarCbxTipoSolicitud();
    //cargarCbxResponsable();
    cargarCbx_Cartera();
    cargarCbxInterventor();
    cargarLineasNegocio();
    cargarEstadoCartera();
    cargarInfoSolicitudesC();



    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $("#btn_Aceptar_tipoClienteC").click(function () {
        var fechainicio = $("#fechaini").val();
        var fechafin = $("#fechafin").val();
        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
            cargarInfoSolicitudesC();
        } else {
            mensajesDelSistema("Por favor revice el rango de fechas", '410', '150', false);
        }
    });

    ordenarCombo('linea_negocio');
    autocompletar("txt_nom_cliente", 1);
    autocompletar("txt_nom_proyecto", 2);

});

function cargarCbxLineaNegocio() {
    $('#cbx_LineaDeNegocio').html('');
    $('#cbx_LineaDeNegocio').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 12
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_LineaDeNegocio').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function cargarCbxTipoSolicitud() {
    $('#cbx_TipoSolicitud').html('');
    $('#cbx_TipoSolicitud').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 13,
            idLineaDeNegocio: $("#cbx_LineaDeNegocio").val()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_TipoSolicitud').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxResponsable() {
    $('#cbx_Responsable').html('');
    $('#cbx_Responsable').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 14
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Responsable').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxInterventor() {
    $('#cbx_Interventor').html('');
    $('#cbx_Interventor').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 15
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Interventor').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
function cargarResponsable() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 43
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#txt_responsable2').val(json.nombre);
                    $('#idresponsable2').val(json.cedula);


                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
function cargarCbx_Cartera() {
    $('#cbx_Interventor').html('');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 20
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Cartera').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCbxNics(idcliente) {

    $('#cbx_nic').html('');
    $('#cbx_nic').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 11,
            idcliente: idcliente
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json.rows) {
                        $('#cbx_nic').append('<option value=' + json.rows[key].id_cliente + '>' + json.rows[key].nic + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }


        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function buscar() {
    if ($('#filtro').val() === "1") {
        var busqueda = $('#busqueda').val();
        cargarSolicitud(busqueda);


    } else if ($('#filtro').val() === "2") {
        var busqueda = $.trim($('#busqueda').val());
        if (!(busqueda === "")) {
            $.ajax({
                type: 'POST',
                url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
                dataType: 'json',
                async: false,
                data: {
                    opcion: 9,
                    busqueda: busqueda
                },
                success: function (json) {

                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        $('#idcliente').val(json.idcliente);
                        BuscarSolicitudesidcliente();
                        listarvalorespredeterminados(json.idcliente);
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }

    } else {
        BuscarSolicitudesidcliente();
        listarvalorespredeterminados($('#idcliente').val());
    }
}
function cargarSolicitud(busqueda) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 17,
            busqueda: busqueda
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (!($('#idSolicitud').val(json.id_solicitud) === '')) {
                    $('#idSolicitud').val(json.id_solicitud);
                    $('#idcliente').val(json.id_cliente);
                    if (json.estado_cartera === ' ') {
                        $('#cbx_Cartera').val('');
                    } else {
                        $('#cbx_Cartera').val(json.estado_cartera);
                    }
                    ;

                    $('#fechaCreacion').val(json.creation_date);
                    $('#historiasolicitud').val(json.descripcion);
                    $('#txt_responsable').val(json.nomresponsable);
                    $('#idresponsable').val(json.responsable);
                    $('#cbx_Interventor').val(json.interventor);


                    buscarclienteid(json.id_cliente);
                    cargarCbxNics(json.id_cliente);
                    $('#cbx_LineaDeNegocio').val(json.lineanegocio);
                    //cargarLinieaNegocioTP(json.tipo_solicitud);
                    //cargarCbxTipoSolicitud();
                    $('#cbx_TipoSolicitud >option:contains(' + json.tipo_solicitud + ')').attr('selected', 'selected');
                    $('#cbx_nic >option:contains(' + json.nic + ')').attr('selected', 'selected');
                    $('#cbx_nic >option:contains(' + json.nic + ')').attr('selected', 'selected');
                    if (json.aviso === '') {
                        $('#div_opd').css({'display': 'none'});
                    } else {
                        $('#div_opd').css({'display': ''});
                    }
                    $('#OPD').val(json.aviso);
                    $('#fformPrincipal2').css({'display': ''});


                } else {

                    $('#fformPrincipal2').css({'display': 'none'});
                    limpiarFormulario();
                }









            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

///////////////////////////////////////////////////////////////////////////// Agregar Al Resto
function BuscarSolicitudesidcliente() {
    cargarSolicitudesIdcli($('#idcliente').val());
}
;

function divemergente_Solicitudes() {
    $('#div_Solicitidudes').dialog({
        width: 900,
        height: 385,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Solicitudes',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $('#div_Solicitidudes').show();
}

function refrescarGridSolicitudesNom(idcli) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=36&idcli=' + idcli;
    jQuery("#solicitudes").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#solicitudes').trigger("reloadGrid");
}
function refrescarGridSolicitudesNom(idcli) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=36&idcli=' + idcli;
    jQuery("#solicitudes").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#solicitudes').trigger("reloadGrid");
}

function cargarSolicitudesIdcli(idcli) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=36&idcli=' + idcli;
    if ($("#gview_solicitudes").length) {
        refrescarGridSolicitudesNom(idcli);
    } else {
        jQuery("#solicitudes").jqGrid({
            caption: 'Listado De Solicitudes',
            url: url,
            type: "POST",
            datatype: 'json',
            height: 214,
            width: 850,
            scrollOffset: 30,
            colNames: ['Id Solicitud', 'Tipo Solicitud', 'Fecha Creacion', 'Responsable'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', sortable: true, align: 'center', width: '280px'},
                {name: 'creation_date', index: 'creation_date', sortable: true, align: 'center', width: '150px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'center', width: '300px'}
                //{name: 'actions', index: 'actions', resizable: false, align: 'center', width: '135px'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            ignoreCase: true,
            pager: '#page_tabla_solicitudes',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                /*var ids = jQuery("#acciones").jqGrid('getDataIDs');
                 for (var i = 0; i < ids.length; i++) {
                 var cl = ids[i];
                 co = "<img src='/fintra/images/botones/iconos/apu.png' align='absbottom'  name='apu' id='apu' width='19' height='19' title ='APU'  onclick=\"AsociarAPU('" + cl + "');\">";
                 ed = "<img src='/fintra/images/botones/iconos/adds.png' align='absbottom'  name='editar' id='editar' width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"CargarDefAccionesAlcances('" + cl + "');\">";
                 cont = "<img src='/fintra/images/botones/iconos/contratista.png' align='absbottom'  name='contratista' id='contratista' width='19' height='19' title ='Asignar Contratistas'  onclick=\"AgregarContratistas('" + cl + "');\">";
                 an = "<img src='/fintra/images/botones/iconos/delete2.png' align='absbottom'  name='anular' id='anular' width='19' height='19' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan datos asociados a la subcategoria, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
                 jQuery("#acciones").jqGrid('setRowData', ids[i], {actions: co + '   ' + ed + '   ' + cont + '   ' + an});
                 }*/
            },
            loadError: function (xhr, status, error) {
                alert(error);
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                cargarSolicitud(rowid);
            }
        }).navGrid("#page_tabla_solicitudes", {edit: false, add: false, del: false});
        jQuery("#solicitudes").jqGrid("navButtonAdd", "#page_tabla_solicitudes", {
            caption: "Exportar",
            title: "Exportar a Excel",
            onClickButton: function () {
                alert('Exportar a Excel aun no esta listo');
            }
        });
        /*jQuery("#acciones").jqGrid('filterToolbar', "#searchacciones",
         {
         autosearch: false,
         searchOnEnter: false,
         defaultSearch: "cn",
         stringResult: true,
         ignoreCase: true,
         multipleSearch: true,
         gs_nombe: false
         
         });*/

        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function buscarclienteid(idcliente) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 6,
            idcliente: idcliente
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }

//                console.log(json.nomcli);
                $('#idcliente').val(json.codcli);
                $('#txt_NombreCliente').val(json.nomcli);
                $('#NombreCliente').val(json.nomcli);
                $('#NitCliente').val(json.nit);
                $('#DigitoVerificacion').val(json.digito_verificacion);
                $('#NombreContacto').val(json.nomcontacto);
                $('#CelularContacto').val(json.celContacto);
                $('#DireccionCliente').val(json.direccion);
                cargarDepCiuid(json.ciudad);




            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function cargarLinieaNegocioTP(tiposolicitud) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 18,
            tiposolicitud: tiposolicitud
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }


                $('#cbx_LineaDeNegocio').val(json.value);



            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function cargarDepCiuid(idCiudad) {

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 8,
            idCiudad: idCiudad
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                for (var key in json) {
                    $('#Departamento').val(key);
                    $('#Ciudad').val(json[key]);

                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
function autocompletarNombre() {
    $('#idcliente').val('');
    limpiarFormulario();
    if ($('#filtro').val() === "1") {
        $("#busqueda").autocomplete({
            source: "",
            minLength: 2
        });

    } else {

        $("#busqueda").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: 'POST',
                    url: "./controlleropav?estado=Procesos&accion=Cliente",
                    dataType: "json",
                    data: {
                        q: request.term,
                        opcion: 4
                    },
                    success: function (data) {
                        // response( data );
                        response($.map(data, function (item) {
                            return {
                                label: item.label,
                                value: item.label,
                                mivar: item.value
                            };
                        }));
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $("#idcliente").val(ui.item.mivar);
                console.log(ui.item ?
                        "Selected: " + ui.item.mivar :
                        "Nothing selected, input was " + ui.item.label);
            },
            open: function () {
                //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
            },
            close: function () {

                // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
            }
        });
    }


}

function habilitar_form_principal(x) {
    $('#formPrincipal').css({
        'display': ''});

    $('#tipoCliente').attr('readonly', 'true');

    if (x === 1) {
        $('#div_clientepadre').css({
            'display': 'none'});
    } else {
        $('#div_clientepadre').css({
            'display': ''});
    }
    limpiarFormulario();



}

function desabilitar_form() {
    limpiarFormulario();
    $('#formPrincipal').css({
        'display': 'none'});
}

function cargarComboTipoCliente() {
    $('#tipoCliente').html('');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 0
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#tipoCliente').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });



}
//Carga los nombre del autocompletar Cliente Padre
function cargarClientesPadre() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    var clientesPadre = [];
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 4
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                for (j in json.rows) {
                    clientesPadre.push(json.rows[j].valor_02);
                }
                $("#ClientePadre").autocomplete({
                    source: clientesPadre,
                    minLength: 2
                });

            }
        }
    });
}


function actualizarCartera() {
    if (CamposObligatorios()) {


        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            dataType: 'json',
            async: false,
            data: {
                opcion: 35,
                idcliente: $('#idCliente').val(),
                nic: $('#cbx_nic option:selected').text(),
                descripcion: $('#descripcion').val(),
                opd: $('#OPD').val(),
//                responsable: $('#cbx_Responsable').val(),
                interventor: $('#cbx_Interventor').val(),
                lineanegocio: $('#cbx_LineaDeNegocio').val(),
                //tiposolicitud: $('#cbx_TipoSolicitud option:selected').text(),
                idSolicitud: $('#idSolicitud').val(),
                estadoCartera: $('#cbx_Cartera').val()


            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    if (json.respuesta === "OK") {

                        mensajesDelSistema("Se Modifico la Solicitud de forma correcta", '250', '150', true);
                        $('#descripcion').val("");
                        cargarInfoSolicitudesC();
                        $("#fformPrincipal2").dialog("destroy");

                    }

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });


    } else {
        mensajesDelSistema('Datos Incompletos', '250', '180');
    }
}

function CamposObligatorios() {

    var campos = '';
    var DC = true;

    campos = ['#descripcion'];


    for (var i = 0; i < campos.length; i++) {
        if (($(campos[i]).val()) === '') {
            DC = false;
            break;
        }

    }


    return DC;
}

function divemergente_nic() {
    $('#div_Nic').dialog({
        width: 400,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'N i cs',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $('#div_valorespredeterminados').show();
}

function limpiarFormulario() {
    $('#NombreCliente').val('');
    $('#Ciudad').html('');
    $('#NitCliente').val('');
    $('#DireccionCliente').val('');
    $('#NombreContacto').val('');
    $('#EmailContacto').val('');
    $('#TelefonoContacto').val('');
    $('#CargoContacto').val('');
    $('#CelularContacto').val('');
    $('#NombreRepresentateLegal').val('');
    $('#TelRepresentateLegal').val('');
    $('#Nic').val('');
    $('#DigitoVerificacion').val('');
    $('#idDepartamento').val('');
    $('#Departamento').val('');
    $('#idCiudad').val('');
    $('#Ciudad').val('');
    $('#dep_dir').val('ATL');
    $('#ciu_dir').val('BQ');
    $('#via_princip_dir').val('');
    $('#via_genera_dir').val('');
    $('#nom_princip_dir').val('');
    $('#nom_genera_dir').val('');
    $('#placa_dir').val('');
    $('#ClientePadre').val('');
    $('#idcliente').val('');
    $('#EmailRepresentanteLegal').val('');
    $('#CelRepresentateLegal').val('');
    $('#busqueda').val('');






}

function Recargar() {
    cargarPagina('jsp/opav/OportunidadNegocio/Cartera/Cartera.jsp');
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function Posicionar_div(id_objeto, e) {
    obj = document.getElementById(id_objeto);
    var posx = 0;
    var posy = 0;
    if (!e)
        var e = window.event;
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    }
    else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    else
        alert('ninguna de las anteriores');

    obj.style.left = posx + 'px';
    obj.style.top = posy + 'px';
    obj.style.zIndex = 1300;
}

function refrescarvalorespredeterminadosespedit(idcliente) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=11&idcliente=' + idcliente;
    jQuery("#valorespredeterminados").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#valorespredeterminados').trigger("reloadGrid");
}

function listarvalorespredeterminados(idcliente) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=11&idcliente=' + idcliente;
    if ($("#gview_valorespredeterminados").length) {
        refrescarvalorespredeterminadosespedit(idcliente);
    } else {
        jQuery("#valorespredeterminados").jqGrid({
            caption: 'Valores',
            url: url,
            datatype: 'json',
            height: 220,
            width: 300,
            colNames: ['Id', 'Nics'],
            colModel: [
                {name: 'id_cliente', index: 'id_cliente', hidden: true, align: 'left', width: '600px'},
                {name: 'nic', index: 'nic', sortable: true, editable: true, align: 'left', width: '600px'}

            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_valorespredeterminados',
            multiselect: false,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            ignoreCase:true,
            jsonReader: {
                root: 'rows', repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false},
            gridComplete: function () {

                /*var ids = jQuery("#valorespredeterminados").jqGrid('getDataIDs');
                 for (var i = 0; i < ids.length; i++) {
                 var cl = ids[i];
                 ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' align: 'center' height='15' title ='Editar'  onclick=\"pruebas('" + cl + "');\">";
                 jQuery("#valorespredeterminados").jqGrid('setRowData', ids[i], {actions: ed});
                 }*/
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }}).navGrid("#page_tabla_valorespredeterminados", {add: false, edit: false, del: false, search: false, refresh: false});
        jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
            caption: "Nuevo",
            title: "Agregar nueva fila",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {

                var grid = $("#valorespredeterminados")
                        , rowid = 'neo_' + grid.getRowData().length;

                grid.addRowData(rowid, {});
                grid.jqGrid('editRow', rowid, true, function () {
                    //  $("input, select", e.target).focus();
                }, null, null, {}, null, null, function (id) {
                    var g = $('#valorespredeterminados')
                            , r = g.jqGrid('getLocalRow', id);
                    if (!r.id) {
                        g.jqGrid('delRowData', id);
                    }
                });
            }
            //position: "first"
        });/*
         jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
         caption: "Guardar",
         title: "Guardar cambios",
         buttonicon: "ui-icon-save",
         onClickButton: function () {
         guardarValp("valorespredeterminados");
         }             //position: "first"
         });*/
        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function guardarValp() {
    var grid = jQuery("#valorespredeterminados")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;
    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);
        if (data.valor_xdefecto === '') {
            error = true;
            mensajesDelSistema('Digite el valor', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else {
            if (data.descripcion === '') {

                error = true;
                mensajesDelSistema('Digite la descripcion', '300', 'auto', false);
                grid.jqGrid('editRow', filas[i], true, function () {
                    $("input, select", e.target).focus();
                });
                break;
            }

        }

        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }
    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 41, informacion: JSON.stringify({rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    //alert(json.mensaje);
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    refrescarGridvalorespredeterminados();
                    //refrescarGridvalorespredeterminados();
                    //AgregarGridAso();
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

////////////////Modificacion mmedina
function cargarInfoSolicitudesC() {
    var grid_tablac = jQuery("#tabla_infoSolicitudC");
    if ($("#gview_tabla_infoSolicitudC").length) {
        reloadGridInfoSolicitudes2(grid_tablac, 39);
    } else {
        grid_tablac.jqGrid({
            caption: "SOLICITUDES",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '500',
            width: '1605',
            colNames: ['Idsolicitud','Fecha creacion', 'Nombre Cliente', 'Nombre Proyecto', 'Foms', 'Costo Contratista','Valor Cotizacion','Etapa', 'Estado',
                'Codigo cliente', 'Tipo solicitud', 'Estado_cartera', 'Fecha validacion cartera', 'Responsable', 'Interventor', 'Flag', 'id_trazabilidad', 'Id estado',  'presupuesto_terminado', 'Acciones'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'creation_date', index: 'creation_date', width: 130, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nomcli', index: 'nomcli', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 230, sortable: true, align: 'left', search: true},
                {name: 'num_os', index: 'num_os', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'valor_cotizacion', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'nombre_etapa', index: 'nombre_etapa', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_estado', index: 'nombre_estado', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'codcli', index: 'codcli', width: 90, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'estado_cartera', index: 'estado_cartera', width: 100, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'fecha_validacion_cartera', index: 'fecha_validacion_cartera', width: 200, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'responsable', index: 'responsable', width: 180, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'interventor2', index: 'interventor2', width: 110, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'flag', index: 'flag', width: 40, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'trazabilidad', index: 'trazabilidad', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_estado', index: 'id_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'presupuesto_terminado', index: 'presupuesto_terminado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 185, search: false, hidden: true}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: false,
            pager: '#pagerC',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ignoreCase:true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                mostrarFormulario2(rowid);
            }, gridComplete: function () {
                var ids = grid_tablac.jqGrid('getDataIDs');
                var fila;
                var estado_trazabilidad;

                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tablac.jqGrid("getLocalRow", cl);
                    estado_trazabilidad = fila.trazabilidad;
                    ed = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
                    //ed = "<input style='height:25px;width:67px;margin-left: 8px;' type='button' value = 'Cambiar' name='editar' id='editar' width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
                    grid_tablac.jqGrid('setRowData', ids[i], {actions: ed});

                }
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 39,
                    lineaNegocio: $('#linea_negocio').val(),
                    responsable: $('#responsable').val(),
                    solicitud: $('#solicitud').val(),
                    estadoCartera: $('#estadocartera').val(),
                    fechaInicio: $('#fechaini').val(),
                    fechafin: $('#fechafin').val(),
                    trazabilidad: '2',
                    etapaActual: $('#etapaActual').val(),
                    id_cliente: $('#id_cliente').val(),
                    nom_proyecto: $('#txt_nom_proyecto').val(),
                    foms: $('#txt_foms').val(),
                    tipo_proyecto: $('#tipo_proyecto').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tablac.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pagerC", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tablac.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });

    }
}

function reloadGridInfoSolicitudes2(grid_tablac, op) {
    grid_tablac.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                lineaNegocio: $("#linea_negocio").val(),
                responsable: $("#responsable").val(),
                solicitud: $("#solicitud").val(),
                estadoCartera: $("#estadocartera").val(),
                fechaInicio: $("#fechaini").val(),
                fechafin: $("#fechafin").val(),
                trazabilidad: '2',
                etapaActual: $('#etapaActual').val(),
                id_cliente: $('#id_cliente').val(),
                nom_proyecto: $('#txt_nom_proyecto').val(),
                foms: $('#txt_foms').val(),
                tipo_proyecto: $('#tipo_proyecto').val()
            }
        }
    });
    grid_tablac.trigger("reloadGrid");
}


function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarEstadoCartera() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 38
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#estadocartera').html('');
                $('#estadocartera').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#estadocartera').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function  mostrarFormulario2(id_solicitud) {
    cargarSolicitud(id_solicitud);
    $("#fformPrincipal2").dialog({
        width: '994',
        height: '870',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'FORMULARIO',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Actualizar": function () {
                actualizarCartera();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function  mostrarFormularioCrearSolicitud() {
    cargarLineaNegocioCS();
    //cargarTipoSolicitudCS();
    cargarResponsable();
    cargarResponsableCS();
    cargarInterventorCS();
    autocompetarcliente();
    $("#formulario").dialog({
        width: '827',
        height: '540',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'FORMULARIO',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Crear": function () {
                crearSolicitud();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarLineaNegocioCS() {
    $('#LineaDeNegocioCS').html('');
    $('#LineaDeNegocioCS').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 12
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#LineaDeNegocioCS').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTipoSolicitudCS() {
    $('#TipoSolicitudCS').html('');
    $('#TipoSolicitudCS').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 13,
            idLineaDeNegocio: $("#LineaDeNegocioCS").val()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#TipoSolicitudCS').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarResponsableCS() {
    $('#ResponsableCS').html('');
    $('#ResponsableCS').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 14
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#ResponsableCS').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarInterventorCS() {
    $('#InterventorCS').html('');
    $('#InterventorCS').append('<option value=' + "" + '>... </option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 15
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#InterventorCS').append('<option value=' + json[key].value + '>' + json[key].label + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function autocompetarcliente() {
    $("#txt_NombreClienteCS").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 4
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            var idcliente = ui.item.mivar;
            $('#idClienteCS').val(idcliente);
            cargarNicsCS(idcliente);
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);

        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarNicsCS(idcliente) {
    $('#cbx_nicCS').html('');
    $('#cbx_nicCS').append('<option value=' + "" + '>... </option>');
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 11,
            idcliente: idcliente
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json.rows) {
                        $('#cbx_nicCS').append('<option value=' + json.rows[key].id_cliente + '>' + json.rows[key].nic + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
function CamposObligatorios3() {

    var campos = '';
    var DC = true;
    campos = ['#descripcion'];
    for (var i = 0; i < campos.length; i++) {
        if (($(campos[i]).val()) === '') {
            alert(campos[i]);
//            $(campos[i]).css({
//                'box-shadow': '0 0 5px red',
//                'border-width': '1px',
//                'border-style': 'solid',
//                'border-color': 'red'
//
//            });
            DC = false;
        }

    }


    return DC;
}
function crearSolicitud() {
    if (CamposObligatorios3()) {
        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            dataType: 'json',
            async: false,
            data: {
                opcion: 16,
                idcliente: $('#idClienteCS').val(),
                nic: $('#cbx_nicCS option:selected').text(),
                descripcion: $('#descripcionCS').val(),
                opd: $('#OPD').val(),
                responsable: $('#ResponsableCS').val(),
                interventor: $('#InterventorCS').val(),
                lineanegocio: $('#LineaDeNegocioCS').val(),
                tiposolicitud: $('#TipoSolicitudCS option:selected').text(),
                EmailContacto: $('#EmailContacto').val(),
                CargoContacto: $('#CargoContacto').val(),
                nomproyecto: $('#txt_Nomproyecto').val(),
                enviar: $('#chx_envio_cartera').prop('checked')
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    if (json.respuesta === "OK") {
                        mensajesDelSistema("Se Creo la solicitud de forma correcta", '250', '150', true);
                        // Recargar();
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        alert('Datos Incompletos');
    }
}

//////MMEDINA
function  mostrarVentanaAccion(id_solicitud) {
    var grid_tabla = jQuery("#tabla_infoSolicitudC");
    var trazabilidad = grid_tabla.getRowData(id_solicitud).trazabilidad;
    var id_estado = grid_tabla.getRowData(id_solicitud).id_estado;
    var nombre_estado = grid_tabla.getRowData(id_solicitud).nombre_estado;
    $('#idsolicitud').val(id_solicitud);
    $('#estadoActual').val(nombre_estado);
    cargarEtapas(trazabilidad);
    cargarEstadoEtapas(id_estado);
    $("#cambioEtapa").dialog({
        width: '800',
        height: '370',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Cambio de etapa',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Actualizar": function () {
                cambiarEstado();
            },
            "Salir": function () {
                $(this).dialog("close");

            }
        }
    });
}

function cargarEtapas(trazabilidad) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 40,
            etapa: trazabilidad
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#etapa').val('');
                $('#idetapa').val('');
                for (var datos in json) {
                    $('#etapa').val(json[datos]);
                    $('#idetapa').val(datos);
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarEstadoEtapas(id_estado) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 41,
            etapa: $('#idetapa').val(),
            id_estado: id_estado
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#estado').html('');
                $('#estado').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#estado').append('<option value=' + json[datos].id_estado + '>' + json[datos].nombre_estado + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cambiarEstado() {

    var estado = $("#estado").val();
    if (estado !== '') {
        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            dataType: 'json',
            async: false,
            data: {
                opcion: 42,
                etapa: $("#etapa").val(),
                estados: $("#estado").val(),
                causal: $('#causal').val(),
                idsolicitud: $('#idsolicitud').val()
            },
            success: function (json) {
                console.log(json.respuesta);
                var resp = json.respuesta;
                if (resp === 'Guardado') {
                    mensajesDelSistema("Cambio satisfactorio", '204', '140', true);
                    $("#cambioEtapa").dialog("close");
                    cargarInfoSolicitudesC();
                } else {
                    mensajesDelSistema("Error", '204', '140', false);
                }
            }
        });
    } else {
        mensajesDelSistema("Faltan datos", '204', '140', false);
    }


}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}