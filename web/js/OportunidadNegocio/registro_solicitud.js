/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    cargando_toggle();
    maximizarventana();
    $("#fechaini").val('');
    $("#fechafin").val('');
    $("#fecha").val('');
    $("#fechaini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        //minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        minDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $('#ui-datepicker-div').css('clip', 'auto');
    cargarLineasNegocio();
    agregarclases();
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $("#buscar_solicitud").click(function () {
        var fechainicio = $("#fechaini").val();
        var fechafin = $("#fechafin").val();
        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
            cargarInfoSolicitudes();
        } else {
            mensajesDelSistema("Por favor revice el rango de fechas", '410', '150', false);
        }
    });
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    });
    ordenarCombo('linea_negocio');
    autocompletar("txt_nom_cliente", 1);
    autocompletar("txt_nom_proyecto", 2);
    autocompletar("responsable_", 3);

});








/**********************************************************************************************************************************************************
 Tabla Solicitudes
 ***********************************************************************************************************************************************************/
function cargarInfoSolicitudes() {
    var grid_tabla = jQuery("#tabla_infoSolicitud");
    if ($("#gview_tabla_infoSolicitud").length) {
        reloadGridInfoSolicitudes(grid_tabla, 39);
    } else {
        grid_tabla.jqGrid({
            caption: "SOLICITUDES",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '500',
            width: '1590',
            colNames: ['Fecha creacion', 'Nombre Cliente', 'Nombre Proyecto', 'Foms', 'Idsolicitud', 'Costo Contratista', 'Valor Cotizacion', 'Etapa', 'Estado',
                'Codigo cliente', 'Tipo solicitud', 'Estado_cartera', 'Fecha validacion cartera', 'Responsable', 'Interventor', 'Flag', 'id_trazabilidad', 'Id estado', 'presupuesto_terminado', 'Acciones'],
            colModel: [
                {name: 'creation_date', index: 'creation_date', width: 130, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nomcli', index: 'nomcli', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 230, sortable: true, align: 'left', search: true},
                {name: 'num_os', index: 'num_os', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'valor_cotizacion', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'nombre_etapa', index: 'nombre_etapa', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_estado', index: 'nombre_estado', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'codcli', index: 'codcli', width: 90, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'estado_cartera', index: 'estado_cartera', width: 100, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'fecha_validacion_cartera', index: 'fecha_validacion_cartera', width: 200, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'responsable', index: 'responsable', width: 180, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'interventor2', index: 'interventor2', width: 110, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'flag', index: 'flag', width: 40, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'trazabilidad', index: 'trazabilidad', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_estado', index: 'id_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'presupuesto_terminado', index: 'presupuesto_terminado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 185, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {

                var ids = grid_tabla.jqGrid('getDataIDs');
                var id_estado;
                for (var i = 0; i < ids.length; i++) {

                    var cl = ids[i];
                    id_estado = grid_tabla.getRowData(cl).id_estado;
                    //console.log(id_estado);
                    //
                    //ing.William siado se comenta momentaneamente 
//                    if ((id_estado == 510) || (id_estado == 610) || (id_estado == 620)) {
//                        hc = "<img src='/fintra/images/buynow.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='30' height='30' title ='Solicitar'  onclick=\"abrir_hc('" + cl + "');\">";
//                        grid_tabla.jqGrid('setRowData', cl, {actions: hc});
//                    }
                    //ing.William siado FIN se comenta momentaneamente 
                    hc = "<img src='/fintra/images/buynow.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='30' height='30' title ='Solicitar'  onclick=\"abrir_hc('" + cl + "');\">";
                    grid_tabla.jqGrid('setRowData', cl, {actions: hc});

                }
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 39,
                    lineaNegocio: $('#linea_negocio').val(),
                    responsable: $('#responsable').val(),
                    solicitud: $('#solicitud').val(),
                    estadoCartera: '',
                    fechaInicio: $('#fechaini').val(),
                    fechafin: $('#fechafin').val(),
                    trazabilidad: '1',
                    etapaActual: $('#etapaActual').val(),
                    id_cliente: $('#id_cliente').val(),
                    nom_proyecto: $('#txt_nom_proyecto').val(),
                    foms: $('#txt_foms').val(),
                    tipo_proyecto: $('#tipo_proyecto').val()

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
    }
}

function reloadGridInfoSolicitudes(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                lineaNegocio: $('#linea_negocio').val(),
                responsable: $('#responsable').val(),
                solicitud: $('#solicitud').val(),
                estadoCartera: '',
                fechaInicio: $('#fechaini').val(),
                fechafin: $('#fechafin').val(),
                trazabilidad: '1',
                etapaActual: $('#etapaActual').val(),
                id_cliente: $('#id_cliente').val(),
                nom_proyecto: $('#txt_nom_proyecto').val(),
                foms: $('#txt_foms').val(),
                tipo_proyecto: $('#tipo_proyecto').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


/**********************************************************************************************************************************************************
 Fin Tabla Solicitudes
 ***********************************************************************************************************************************************************/
/**********************************************************************************************************************************************************
 Utiles
 ***********************************************************************************************************************************************************/

function abrir_hc(id_solicitud) {

    window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/solicitud_insumos/&pagina=registro_solicitud.jsp?num_solicitud=" + id_solicitud, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
}

/**********************************************************************************************************************************************************
 Fin Utiles
 ***********************************************************************************************************************************************************/


/**********************************************************************************************************************************************************
 Utiles Generales
 ***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            if (opp == 3) {
                $('#usuario_').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon', '#perc_rete'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}
/**********************************************************************************************************************************************************
 Fin Utiles Generales
 ***********************************************************************************************************************************************************/


function abrirFacturacionApu(id_solicitud) {
    window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/OportunidadNegocio/Facturacion/&pagina=facturacionApuInsumo.jsp?num_solicitud=" + id_solicitud, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
}

function abrirEjecucionApuInsumo(id_solicitud) {
    window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/OportunidadNegocio/Ejecucion/&pagina=EjecucionApuInsumo.jsp?num_solicitud=" + id_solicitud, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
}
function abrirResponsables_ejecucion(id_solicitud) {
    $('#id_solicitud_').val(id_solicitud);
    listarResponables_ejecucion();
    $("#ventana_Responsables").dialog({
        width: '560',
        height: '475',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Responsables',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function listarResponables_ejecucion() {
    var grid_tbl_maestro = jQuery("#tbl_Responsables");
    if ($("#gview_tbl_Responsables").length) {
        refrescarGrid_Responsables();
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Responsables del Proyecto",
            url: "./controlleropav?estado=Modulo&accion=Ejecucion",
            datatype: "json",
            height: '250',
            width: '505',
            cellEdit: true,
            colNames: ['Id', 'responsable', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 10, align: 'left', key: true, hidden: true},
                {name: 'responsable', index: 'responsable', width: 380, align: 'left'},
                {name: 'actions', index: 'actions', width: 110, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tbl_Responsables'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 2,
                    id_solicitud: $('#id_solicitud_').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var ids = jQuery("#tbl_Responsables").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion('Esta seguro de anular el Riesgo?','250','150',AnularResponsables,'" + cl + "');\">";
                    jQuery("#tbl_Responsables").jqGrid('setRowData', ids[i], {actions: an});
                }


            },
            ondblClickRow: function (rowid, iRow, iCol, e) {


            }
        }).navGrid("#page_tbl_Responsables", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tbl_Responsables").jqGrid("navButtonAdd", "#page_tbl_Responsables", {
            caption: "Asignar Responsable",
            onClickButton: function () {
                AsignarResponsable();
            }
        });
    }

}

function refrescarGrid_Responsables() {
    jQuery("#tbl_Responsables").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Ejecucion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 2,
                id_solicitud: $('#id_solicitud_').val()

            }
        }
    });

    jQuery('#tbl_Responsables').trigger("reloadGrid");
}


function AsignarResponsable() {
    $('#div_Control').fadeIn('slow');
    $('#responsable_').val('');
    $('#usuario_').val()
    $('#id_').val('');
    AbrirDivResponsables();
}

function AbrirDivResponsables() {
    $("#div_Control").dialog({
        width: '500',
        height: '150',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Asignar Responsable',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardarResponsables();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarResponsables(cl) {

    $('#div_Control').fadeIn("slow");
    var fila = jQuery("#tbl_Responsables").getRowData(cl);
    var nombre = fila['nombre'];

    $('#id_').val(cl);
    $('#nombre').val(nombre);
    AbrirDivEditarConcepto();
}

function AbrirDivEditarConcepto() {
    $("#div_Control").dialog({
        width: '500',
        height: '260',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Editar Asignacion',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarResponsables();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}



function guardarResponsables() {
    var nombre = $('#nombre').val();

    if (nombre !== '') {


        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controlleropav?estado=Modulo&accion=Ejecucion",
            data: {
                opcion: ($('#id_').val() === '') ? 3 : 4,
                id: $('#id_').val(),
                id_solicitud: $('#id_solicitud_').val(),
                id_usuario_: $('#usuario_').val()

            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '270', '165');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGrid_Responsables();
                        $("#div_Control").dialog('close');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se pudo asignar responsagles!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    } else {
        mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');
    }
}

function AnularResponsables(cl) {

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Ejecucion",
        data: {
            opcion: 5,
            id: cl

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {

                    refrescarGrid_Responsables();

                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo Anular el Riesgo!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function mensajeConfirmacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsj');
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}


function abrirDivPrepararfacturacion(id_solicitud) {
    window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/OportunidadNegocio/&pagina=prepararFacturacion.jsp?num_solicitud=" + id_solicitud, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
}
