/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    cargando_toggle();
    maximizarventana();
    listarIndicador();
});


function listarIndicador() {
    var grid_tbl_maestro = jQuery("#tbl_indicador");
    if ($("#gview_tbl_hitos").length) {
        refrescarGridIndicador();
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Indicador",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '400',
            width: '680',
            cellEdit: true,
            colNames: ['#', 'Nombre Indicador', 'Descripcion', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true},
                {name: 'nombre', index: 'nombre', width: 430, align: 'left'},
                {name: 'descripcion', index: 'descripcion', width: 430, align: 'left', hidden: true},
                {name: 'actions', index: 'actions', width: 110, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tbl_indicador'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 44
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var ids = jQuery("#tbl_indicador").jqGrid('getDataIDs');
                var fila;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = jQuery("#tbl_indicador").jqGrid("getLocalRow", cl);
                    
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarIndicador('" + cl + "');\">";
                        an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion('Esta seguro de anular el Indicador?','250','150',AnularIndicador,'" + cl + "');\">";
                        jQuery("#tbl_indicador").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                    
                }                

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {


            }
        }).navGrid("#page_tbl_indicador", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tbl_indicador").jqGrid("navButtonAdd", "#page_tbl_indicador", {
            caption: "Nuevo",
            onClickButton: function () {
                crearIndicador();
            }
        });
    }

}

function refrescarGridIndicador() {
    jQuery("#tbl_indicador").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 44
            }
        }
    });

    jQuery('#tbl_indicador').trigger("reloadGrid");
}


function crearIndicador() {
    $('#div_Control').fadeIn('slow');
    $('#idIndicador').val('');
    $('#nombre').val('');
    $('#descripcion').val('');
    AbrirDivIndicador();
}

function AbrirDivIndicador() {
    $("#div_Control").dialog({
        width: '420',
        height: '230',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR INDICADOR',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardarIndicador();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarIndicador(cl) {

    $('#div_Control').fadeIn("slow");
    var fila = jQuery("#tbl_indicador").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];

    $('#idIndicador').val(cl);
    $('#nombre').val(nombre);
    $('#descripcion').val(descripcion);
    AbrirDivEditarIndicador();
}

function AbrirDivEditarIndicador() {
    $("#div_Control").dialog({
        width: '420',
        height: '230',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR INDICADOR',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarIndicador();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}



function guardarIndicador() {
    var nombre = $('#nombre').val();
    var descripcion = $('#descripcion').val();

    if (nombre !== '' && descripcion !== '') {


        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            data: {
                opcion: ($('#idIndicador').val() === '') ? 45 : 46,
                id: $('#idIndicador').val(),
                nombre: $('#nombre').val(),
                descripcion: $('#descripcion').val()
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '270', '165');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridIndicador();
                        $("#div_Control").dialog('close');
                    }

                } else {
                    
                    mensajesDelSistema("Lo sentimos no se pudo guardar el Indicador!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    } else {
        mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');
    }
}

function AnularIndicador(cl){
    
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            opcion: 47,
            id: cl

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {
                    
                    refrescarGridIndicador();
                }

            } else {
                
                mensajesDelSistema("Lo sentimos no se pudo Anular el Indicador!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
        
}

/**********************************************************************************************************************************************************
                                                            Utiles Generales
***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon', '#perc_rete'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle(){
    $('#loader-wrapper').toggle();
}


function mensajeConfirmacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}
/**********************************************************************************************************************************************************
                                                           Fin Utiles Generales
***********************************************************************************************************************************************************/