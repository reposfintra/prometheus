/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    cargando_toggle();
    maximizarventana();
    $("#fechaini").val('');
    $("#fechafin").val('');
    $("#fecha").val('');
    $("#fecha_hitos").datepicker({dateFormat: 'yy-mm-dd'}).val();
    $("#fechaini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        //minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        minDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha_periodo").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        //minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        //minDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $('#ui-datepicker-div').css('clip', 'auto');
    cargarLineasNegocio();
    agregarclases();
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $("#buscar_solicitud").click(function () {
        var fechainicio = $("#fechaini").val();
        var fechafin = $("#fechafin").val();
        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
            cargarInfoSolicitudes();
        } else {
            mensajesDelSistema("Por favor revice el rango de fechas", '410', '150', false);
        }
    });
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    });
    ordenarCombo('linea_negocio');
    autocompletar("txt_nom_cliente", 1);
    autocompletar("txt_nom_proyecto", 2);
});




/**********************************************************************************************************************************************************
 Tabla Solicitudes
 ***********************************************************************************************************************************************************/
function cargarInfoSolicitudes() {
    var grid_tabla = jQuery("#tabla_infoSolicitud");
    if ($("#gview_tabla_infoSolicitud").length) {
        reloadGridInfoSolicitudes(grid_tabla, 39);
    } else {
        grid_tabla.jqGrid({
            caption: "SOLICITUDES",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '500',
            width: '1590',
            colNames: ['Fecha creacion', 'Nombre Cliente', 'Nombre Proyecto', 'Foms', 'Idsolicitud', 'Costo Contratista', 'Valor Cotizacion', 'Etapa', 'Estado',
                'Codigo cliente', 'Tipo solicitud', 'Estado_cartera', 'Fecha validacion cartera', 'Responsable', 'Interventor', 'Flag', 'id_trazabilidad', 'Id estado', 'presupuesto_terminado', 'Acciones'],
            colModel: [
                {name: 'creation_date', index: 'creation_date', width: 130, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nomcli', index: 'nomcli', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 230, sortable: true, align: 'left', search: true},
                {name: 'num_os', index: 'num_os', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'valor_cotizacion', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'nombre_etapa', index: 'nombre_etapa', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_estado', index: 'nombre_estado', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'codcli', index: 'codcli', width: 90, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'estado_cartera', index: 'estado_cartera', width: 100, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'fecha_validacion_cartera', index: 'fecha_validacion_cartera', width: 200, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'responsable', index: 'responsable', width: 180, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'interventor2', index: 'interventor2', width: 110, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'flag', index: 'flag', width: 40, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'trazabilidad', index: 'trazabilidad', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_estado', index: 'id_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'presupuesto_terminado', index: 'presupuesto_terminado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 185, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ignoreCase: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila;
                var estado_trazabilidad;
                var id_estado, iva_aiu = '';
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);
                    //var Cargar_Archivos = "<img src='/fintra/images/opav/File_add.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Cargar Archivos'  onclick=\"divemergente_Carga_Archivos('" + cl + "');\">";


                    if ((fila.trazabilidad == '6')) {
                        accion = "<img src='/fintra/images/opav/Transicion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
                        ExpT = "<img src='/fintra/images/opav/Exportar.png' align='absbottom' name='exportar_WBS_Project_Tareas' id='Exportar Project' value='Exportar_Project'  width='26' height='26' title ='Exportar Project Tareas'  onclick=\"exportarWBSProjectTareas('" + cl + "');\">";
                        ExpA = "<img src='/fintra/images/opav/Exportar.png' align='absbottom' name='exportar_WBS_Project_Asignacion' id='Exportar Project' value='Exportar_Project'  width='26' height='26' title ='Exportar Project Asignacion'  onclick=\"exportarWBSProjectAsignacion('" + cl + "');\">";
                        Hit = "<img src='/fintra/images/opav/File_add.png' align='absbottom' name='Hitos_linea' id='Hitos Linea' value='Hitos_linea'  width='26' height='26' title ='Hitos Linea Base'  onclick=\"hitos('" + cl + "');\">";
                        FlCaja = "<img src='/fintra/images/opav/Facturacion.png' align='absbottom' name='control_interno' id='control interno' value='control_interno'  width='26' height='26' title ='Control Interno'  onclick=\"controlinterno('" + cl + "');\">";
                        aspro = "<img src='/fintra/images/opav/Wbs.png' align='absbottom' name='aspectosproyecto'  value='Contrato'  width='19' height='19' title ='Aspectos Proyectos'  onclick=\"abrirAspectosProyecto('" + cl + "');\">";
                        asig_punt = "<img src='/fintra/images/opav/Wbs.png' align='absbottom' name='aspectosproyecto'  value='Contrato'  width='19' height='19' title ='Aspectos Proyectos'  onclick=\"abrirasingacion_puntaje('" + cl + "');\">";
                        grid_tabla.jqGrid('setRowData', ids[i], {actions: accion + ExpT + ExpA + Hit + FlCaja + aspro + asig_punt});
                    } else {
                        accion = "<img src='/fintra/images/opav/Transicion_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n';\">";
                        ExpT = "<img src='/fintra/images/opav/Exportar_grey.png' align='absbottom' name='exportar_WBS_Project_Tareas' id='Exportar Project' value='Exportar_Project'  width='26' height='26' title ='Exportar Project Tareas' ;\">";
                        ExpA = "<img src='/fintra/images/opav/Exportar_grey.png' align='absbottom' name='exportar_WBS_Project_Asignacion' id='Exportar Project' value='Exportar_Project'  width='26' height='26' title ='Exportar Project Asignacion' ;\">";
                        Hit = "<img src='/fintra/images/opav/File_add_grey.png' align='absbottom' name='Hitos_linea' id='Hitos Linea' value='Hitos_linea'  width='26' height='26' title ='Hitos Linea Base';\">";
                        FlCaja = "<img src='/fintra/images/opav/Facturacion_grey.png' align='absbottom' name='control_interno' id='control interno' value='control_interno'  width='26' height='26' title ='Control Interno';\">";
                        aspro = "<img src='/fintra/images/opav/Wbs_grey.png' align='absbottom' name='aspectosproyecto'  value='Contrato'  width='19' height='19' title ='Aspectos Proyectos' \">";
                        asig_punt = "<img src='/fintra/images/opav/Wbs_grey.png' align='absbottom' name='aspectosproyecto'  value='Contrato'  width='19' height='19' title ='Aspectos Proyectos';\">";
                        grid_tabla.jqGrid('setRowData', ids[i], {actions: accion + Hit + FlCaja + aspro + asig_punt});
                    }
                }
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 39,
                    lineaNegocio: $('#linea_negocio').val(),
                    responsable: $('#responsable').val(),
                    solicitud: $('#solicitud').val(),
                    estadoCartera: '',
                    fechaInicio: $('#fechaini').val(),
                    fechafin: $('#fechafin').val(),
                    trazabilidad: '6',
                    etapaActual: $('#etapaActual').val(),
                    id_cliente: $('#id_cliente').val(),
                    nom_proyecto: $('#txt_nom_proyecto').val(),
                    foms: $('#txt_foms').val(),
                    tipo_proyecto: $('#tipo_proyecto').val()

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
    }
}


function reloadGridInfoSolicitudes(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                lineaNegocio: $('#linea_negocio').val(),
                responsable: $('#responsable').val(),
                solicitud: $('#solicitud').val(),
                estadoCartera: '',
                fechaInicio: $('#fechaini').val(),
                fechafin: $('#fechafin').val(),
                trazabilidad: '6',
                etapaActual: $('#etapaActual').val(),
                id_cliente: $('#id_cliente').val(),
                nom_proyecto: $('#txt_nom_proyecto').val(),
                foms: $('#txt_foms').val(),
                tipo_proyecto: $('#tipo_proyecto').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

//////////////////////////////////////////////////

function hitos(id_solicitud) {
    listarHitos(id_solicitud);
    $('#idSolicitud').val(id_solicitud);
    $("#Hitos").dialog({
        width: '810',
        height: '650',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Hitos',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function listarHitos(idaccion) {
    var grid_tbl_maestro = jQuery("#tbl_hitos");
    var idaccion = idaccion;
    if ($("#gview_tbl_hitos").length) {
        refrescarGridHitos(idaccion);
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Hitos",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '400',
            width: '780',
            cellEdit: true,
            colNames: ['#', 'Nombre Hito', 'Fecha', 'Estado', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true},
                {name: 'nombre', index: 'razon_social', width: 430, align: 'left'},
                {name: 'fecha', index: 'fecha', width: 100, align: 'left'},
                {name: 'estado', index: 'estado', width: 100, align: 'left', hidden: true},
                {name: 'actions', index: 'actions', width: 110, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tbl_hitos'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            ignoreCase: true,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 26,
                    idaccion: idaccion
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var ids = jQuery("#tbl_hitos").jqGrid('getDataIDs');
                var fila;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = jQuery("#tbl_hitos").jqGrid("getLocalRow", cl);

                    if (fila.estado != '1') {
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarHito('" + cl + "');\">";
                        an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion('Esta seguro de anular el Hito?','250','150',AnularHito,'" + cl + "');\">";
                        jQuery("#tbl_hitos").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                    }
                }

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {


            }
        }).navGrid("#page_tbl_hitos", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tbl_hitos").jqGrid("navButtonAdd", "#page_tbl_hitos", {
            caption: "Nuevo",
            onClickButton: function () {
                crearHitos();
            }
        });
    }

}

function refrescarGridHitos(idaccion) {
    jQuery("#tbl_hitos").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 26,
                idaccion: idaccion
            }
        }
    });

    jQuery('#tbl_hitos').trigger("reloadGrid");
}


function crearHitos() {
    $('#div_Control_hitos').fadeIn('slow');
    $('#idHito').val('');
    $('#nombre_hitos').val('');
    $('#fecha_hitos').val('');
    var idaccion = $('#idSolicitud').val();
    AbrirDivHito(idaccion);
}

function AbrirDivHito(idaccion) {
    $("#div_Control_hitos").dialog({
        width: '420',
        height: '180',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR HITO',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardarHito($('#idSolicitud').val());
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarHito(cl) {

    $('#div_Control_hitos').fadeIn("slow");
    var fila = jQuery("#tbl_hitos").getRowData(cl);
    var nombre = fila['nombre'];
    var fecha = fila['fecha'];
    var idaccion = $('#idSolicitud').val();

    $('#idHito').val(cl);
    $('#nombre_hitos').val(nombre);
    $('#fecha_hitos').val(fecha);
    AbrirDivEditarHito(idaccion);
}

function AbrirDivEditarHito(idaccion) {
    $("#div_Control_hitos").dialog({
        width: '420',
        height: '180',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR HITO',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarHito(idaccion);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}



function guardarHito(idaccion) {
    var nombre = $('#nombre_hitos').val();
    var fecha = $('#fecha_hitos').val();

    if (nombre !== '' && fecha !== '') {


        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            data: {
                opcion: ($('#idHito').val() === '') ? 27 : 28,
                id: $('#idHito').val(),
                nombre: $('#nombre_hitos').val(),
                fecha: $('#fecha_hitos').val(),
                idaccion: idaccion
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '270', '165');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridHitos();
                        $("#div_Control_hitos").dialog('close');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se pudo guardar el Hito!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    } else {
        mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');
    }
}

function AnularHito(cl) {

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            opcion: 29,
            id: cl

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {

                    refrescarGridHitos()();
                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo Anular el Concepto!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}
//////////////////////////////////////////////////

function  controlinterno(id_solicitud) {

    $("#ControlInt").dialog({
        width: '550',
        height: '195',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Herramientas - Control Interno',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Abrir": function () {
                AbrirOpciones(id_solicitud);
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function AbrirOpciones(id_solicitud) {

    var opcion = $('#Proceso').val();
    //messagebox(opcion);
    $('#idSolicitud').val(id_solicitud);
    if (opcion === '01') {
        flujocaja(id_solicitud);
    } else {
        if (opcion === '05') {
            gestion(id_solicitud);
        } else {
            if (opcion === '03') {
                facturacion(id_solicitud);
            } else {
                if (opcion === '02') {
                    histograma(id_solicitud);
                } else {
                    if (opcion === '04') {
                        compra_materiales(id_solicitud);
                    } else {
                        if (opcion === '06') {
                            riesgo(id_solicitud);
                        }
                    }

                }

            }

        }

    }

}

///////////////////////////////////////

function riesgo(id_solicitud) {
    cargarriesgo(id_solicitud);
    $("#riesgo").dialog({
        width: '830',
        height: '550',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Riesgos',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarriesgo(id_solicitud) {
    var grid_tbl_maestro = jQuery("#tbl_riesgo");
    if ($("#gview_tbl_riesgo").length) {
        refrescarGridRiesgo(id_solicitud);
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Riesgos",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '300',
            width: '790',
            cellEdit: true,
            colNames: ['#', 'Id Categoria', 'Categoria', 'Descripcion', 'Id Impacto', 'Impacto',
                'Id Probabilidad', 'Probabilidad', 'Puntaje', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'id_tipo_categoria', index: 'id_tipo_categoria', width: 80, align: 'left', hidden: true},
                {name: 'tipo_categoria', index: 'tipo_categoria', width: 100, align: 'center'},
                {name: 'descripcion', index: 'descripcion', width: 250, align: 'center'},
                {name: 'id_impacto', index: 'id_impacto', width: 100, align: 'left', hidden: true},
                {name: 'impacto', index: 'impacto', width: 100, align: 'center'},
                {name: 'id_probabilidad', index: 'id_probabilidad', width: 100, align: 'left', hidden: true},
                {name: 'probabilidad', index: 'probabilidad', width: 100, align: 'center'},
                {name: 'puntaje', index: 'puntaje', width: 100, align: 'center'},
                {name: 'actions', index: 'actions', width: 100, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#pager_riesgo'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            ignoreCase: true,
            
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 63,
                    idaccion: id_solicitud
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var ids = jQuery("#tbl_riesgo").jqGrid('getDataIDs');
                var fila;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = jQuery("#tbl_riesgo").jqGrid("getLocalRow", cl);

                    if (fila.estado != '1') {
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarRiesgo('" + cl + "', '" + id_solicitud + "');\">";
                        an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion2('Esta seguro de anular el Riesgo?','250','150',AnularRiesgo,'" + cl + "', '" + id_solicitud + "');\">";
                        jQuery("#tbl_riesgo").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                    }
                }

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {


            }
        }).navGrid("#pager_riesgo", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tbl_riesgo").jqGrid("navButtonAdd", "#pager_riesgo", {
            caption: "Nuevo",
            onClickButton: function () {
                crearRiesgo($('#idSolicitud').val());
            }
        });
    }

}

function AnularRiesgo(cl, id_solicitud) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            opcion: 66,
            id: cl

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {

                    refrescarGridRiesgo(id_solicitud);
                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo Anular el Riesgo!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function editarRiesgo(cl, idaccion) {
    $('#div_Control_riesgo').fadeIn("slow");
    var fila = jQuery("#tbl_riesgo").getRowData(cl);
    cargarComboCateg();
    var categoria = fila['id_tipo_categoria'];
    var descripcion = fila['descripcion'];
    var impacto = fila['id_impacto'];
    var proba = fila['id_propbabilidad'];
    var puntaje = fila['puntaje'];

    $('#idRiesgo').val(cl);
    $('#categoria').val(categoria);
    cargarComboImpacto();
    $('#impacto').val(impacto);
    $('#probabilidad').val(proba);
    $('#descripcion1').val(descripcion);
    $('#puntaje22').val(puntaje);
    AbrirDivEditarRiesgo(idaccion);
}

function AbrirDivEditarRiesgo(idaccion) {
    $("#div_Control_riesgo").dialog({
        width: '550',
        height: '280',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR FLUJO',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarRiesgo(idaccion);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}

function crearRiesgo(id_solicitud) {
    $('#div_Control_riesgo').fadeIn('slow');
    $('#idRiesgo').val('');
    $('#id_tipo_categoria').val(0);
    $('#descripcion').val('');
    $('#id_impacto').val(0);
    $('#puntaje').val('');
    var idaccion = id_solicitud;
    AbrirDivRiesgo(idaccion);
}

function AbrirDivRiesgo(idaccion) {
    cargarComboCateg();
    $("#div_Control_riesgo").dialog({
        width: '550',
        height: '300',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR RIESGOS',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardarRiesgo(idaccion);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control_riesgo").parent().find(".ui-dialog-titlebar-close").hide();
}

function cargarComboCateg(filtro) {
    var elemento = $('#categoria'), sql = 'ConsultaTiposCategoria';
    $.ajax({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        type: 'get',
        data: {opcion: 59, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    console.log(JSON.stringify(json));
                    elemento.append("<option value=''>...</option>");
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function cargarComboImpacto() {
    $('#impacto').html('');
    $('#impacto').append("<option value=''>...</option>");
    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        dataType: 'json',
        async: false,
        data: {
            opcion: 67,
            categoria: $('#categoria option:selected').val()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#impacto').html('');
                    $('#impacto').append("<option value=''>...</option>");
                    for (var key in json) {
                        $('#impacto').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function guardarRiesgo(idaccion) {
    var categoria = $('#categoria').val();
    var descripcion = $('#descripcion1').val();
    var impacto = $('#impacto').val();
    var probabilidad = $('#probabilidad').val();

    if (categoria !== '' && descripcion !== '' && impacto !== '' && probabilidad !== '') {


        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            data: {
                opcion: ($('#idRiesgo').val() === '') ? 64 : 65,
                id: $('#idRiesgo').val(),
                categoria: $('#categoria').val(),
                descripcion: $('#descripcion1').val(),
                impacto: $('#impacto').val(),
                probabilidad: $('#probabilidad').val(),
                puntaje: $('#puntaje22').val(),
                idaccion: idaccion
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '270', '165');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridRiesgo(idaccion);
                        $("#div_Control_riesgo").dialog('close');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se pudo guardar el Riesgo!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    } else {
        mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');
    }
}

function refrescarGridRiesgo(idaccion) {
    jQuery("#tbl_riesgo").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 63,
                idaccion: idaccion
            }
        }
    });

    jQuery('#tbl_riesgo').trigger("reloadGrid");
}
////////////////////////////////////////

function compra_materiales(id_solicitud) {
    cargarMaterial(id_solicitud);
    $("#Compra_materiales").dialog({
        width: '880',
        height: '630',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Proyeccion de Compra de Materiales',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarMaterial(id_solicitud) {
    var grid_tbl_maestro = jQuery("#tbl_material");
    var lastSel;
    if ($("#gview_tbl_material").length) {
        refrescarGridMaterial(id_solicitud);
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Proyeccion de Compra Material",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '400',
            width: '830',
            cellEdit: true,
            colNames: ['#', 'Id Tipo Insumo', 'Id Insumo', 'Material', 'Cantidad', 'Valor Unitario', 'Id Unidad Medida', 'Unidad Medida', 'F Compra', 'Criticidad'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'id_tipo_insumo', index: 'id_tipo_insumo', width: 90, align: 'left', hidden: true},
                {name: 'id_insumo', index: 'id_insumo', width: 90, align: 'left', hidden: true},
                {name: 'insumo', index: 'insumo', width: 300, align: 'left'},
                {name: 'cantidad', index: 'cantidad', width: 100, align: 'center', formatter: 'number',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}
                },
                {name: 'costo', index: 'costo', width: 100, align: 'center', formatter: 'number',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$"}
                },
                {name: 'id_unidad_medida', index: 'id_unidad_medida', width: 90, align: 'left', hidden: true},
                {name: 'nombre_unidad_insumo', index: 'nombre_unidad_insumo', width: 100, align: 'center'},
                {name: 'fecha_com', index: 'fecha_com', width: 100, align: 'center', editable: true, sorttype: "date", formatter: 'date', formatoptions: {newformat: 'Y-m-d'},
                    editoptions: {
                        dataInit: function (elem) {
                            $(elem).datepicker({
                                dateFormat: "yy-mm-dd",
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                onSelect: function (dateText, inst) {
                                    //CalcularInteresesCausacion(lastSel);

                                }

                            });
                        }
                    }
                },
                {name: 'criticidad', index: 'criticidad', width: 80, align: 'center', editable: true, edittype: "select",
                    editoptions: {value: "RC:Ruta Critica;RM:Ruta Media;RB:Ruta Baja"}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#pager_material'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            ignoreCase: true,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 57,
                    idaccion: id_solicitud
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            onCellSelect: function (rowid, iCol, cellcontent, e) {
                if (iCol == 5) {
                    if (rowid && rowid !== lastSel) {
                        jQuery('#tbl_material').restoreRow(lastSel);
                        lastSel = rowid;
                    }
                    jQuery('#tbl_material').jqGrid('editRow', rowid, true);
                    jQuery('#tbl_material').editRow(rowid, true);
                }
            }
        }).navGrid("#pager_material", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        grid_tbl_maestro.navButtonAdd('#pager_material', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                guardarMaterial($('#idSolicitud').val());
            }
        });
    }

}

function refrescarGridMaterial(idaccion) {
    jQuery("#tbl_material").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 57,
                idaccion: idaccion
            }
        }
    });

    jQuery('#tbl_material').trigger("reloadGrid");
}

function guardarMaterial(id_solicitud) {
    var grid = jQuery("#tbl_material")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;



    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);


        if (data.fecha_com === '' || data.fecha_com === '&#160;' || data.fecha_com === undefined) {
            error = true;
            mensajesDelSistema('Debe Ingresar la Fecha de Compra', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else if (data.criticidad === '' || data.criticidad === undefined) {
            error = true;
            mensajesDelSistema('Debe ingresar la Criticidad', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        }


        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }
    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 58, informacion: JSON.stringify({idaccion: id_solicitud, rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    if (json.mensaje != "Algunos Registros se Encuentran Duplicados...") {
                        refrescarGridMaterial(id_solicitud);
                    }
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}

function histograma(id_solicitud) {
    cargarEquipos(id_solicitud);
    cargarPersonal(id_solicitud);
    $("#Histograma").dialog({
        width: '1200',
        height: '630',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Histograma de Equipos/Personal',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarEquipos(id_solicitud) {
    var grid_tbl_maestro = jQuery("#tbl_equipo");
    var lastSel;
    if ($("#gview_tbl_equipo").length) {
        refrescarGridEquipo(id_solicitud);
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Histograma Equipo",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '400',
            width: '530',
            cellEdit: true,
            colNames: ['#', 'Id Tipo Insumo', 'Id Insumo', 'Equipo', 'Periodo Ini', 'Periodo Fin', 'H Trabajo'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'id_tipo_insumo', index: 'id_tipo_insumo', width: 90, align: 'left', hidden: true},
                {name: 'id_insumo', index: 'id_insumo', width: 90, align: 'left', hidden: true},
                {name: 'insumo', index: 'insumo', width: 200, align: 'left'},
                {name: 'periodo_ini', index: 'periodo_ini', width: 100, align: 'left', editable: true, sorttype: "date", formatter: 'date', formatoptions: {newformat: 'Y-m-d'},
                    editoptions: {
                        dataInit: function (elem) {
                            $(elem).datepicker({
                                dateFormat: "yy-mm-dd",
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                onSelect: function (dateText, inst) {
                                    //CalcularInteresesCausacion(lastSel);

                                }

                            });
                        }
                    }
                },
                {name: 'periodo_fin', index: 'periodo_fin', width: 90, align: 'left', editable: true, sorttype: "date", formatter: 'date',
                    formatoptions: {newformat: 'Y-m-d'},
                    editoptions: {
                        dataInit: function (elem) {
                            $(elem).datepicker({
                                dateFormat: "yy-mm-dd",
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                onSelect: function (dateText, inst) {
                                    //CalcularInteresesCausacion(lastSel);

                                }

                            });
                        }
                    }
                },
                {name: 'h_trabajo', index: 'h_trabajo', width: 100, align: 'center', editable: true, formatter: 'number',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""},
                    editoptions: {size: 5, dataInit: function (elem) {
                            $(elem).bind("keypress", function (e) {
                                //alert(soloNumeros(e));
                                return decimales(e, $(this));
                            });
                        }
                    }}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#pager_equipo'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            ignoreCase: true,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 53,
                    idaccion: id_solicitud
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            onCellSelect: function (rowid, iCol, cellcontent, e) {
                if (iCol == 5) {
                    if (rowid && rowid !== lastSel) {
                        jQuery('#tbl_equipo').restoreRow(lastSel);
                        lastSel = rowid;
                    }
                    jQuery('#tbl_equipo').jqGrid('editRow', rowid, true);
                    jQuery('#tbl_equipo').editRow(rowid, true);
                }
            }
        }).navGrid("#pager_equipo", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        grid_tbl_maestro.navButtonAdd('#pager_equipo', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                guardarEquipos($('#idSolicitud').val());
            }
        });
    }

}

function cargarPersonal(id_solicitud) {
    var grid_tbl_maestro = jQuery("#tbl_personal");
    var lastSel;
    if ($("#gview_tbl_personal").length) {
        refrescarGridPersonal(id_solicitud);
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Histograma Personal",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '400',
            width: '530',
            cellEdit: true,
            colNames: ['#', 'Id Tipo Insumo', 'Id Insumo', 'Ocupacion', 'Periodo Ini', 'Periodo Fin', 'H Trabajo'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'id_tipo_insumo', index: 'id_tipo_insumo', width: 90, align: 'left', hidden: true},
                {name: 'id_insumo', index: 'id_insumo', width: 90, align: 'left', hidden: true},
                {name: 'insumo', index: 'insumo', width: 200, align: 'left'},
                {name: 'periodo_ini', index: 'periodo_ini', width: 100, align: 'left', editable: true, sorttype: "date", formatter: 'date', formatoptions: {newformat: 'Y-m-d'},
                    editoptions: {
                        dataInit: function (elem) {
                            $(elem).datepicker({
                                dateFormat: "yy-mm-dd",
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                onSelect: function (dateText, inst) {
                                    //CalcularInteresesCausacion(lastSel);

                                }

                            });
                        }
                    }
                },
                {name: 'periodo_fin', index: 'periodo_fin', width: 90, align: 'left', editable: true, sorttype: "date", formatter: 'date',
                    formatoptions: {newformat: 'Y-m-d'},
                    editoptions: {
                        dataInit: function (elem) {
                            $(elem).datepicker({
                                dateFormat: "yy-mm-dd",
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                onSelect: function (dateText, inst) {
                                    //CalcularInteresesCausacion(lastSel);

                                }

                            });
                        }
                    }
                },
                {name: 'h_trabajo', index: 'h_trabajo', width: 100, align: 'center', editable: true, formatter: 'number',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""},
                    editoptions: {size: 5, dataInit: function (elem) {
                            $(elem).bind("keypress", function (e) {
                                //alert(soloNumeros(e));
                                return decimales(e, $(this));
                            });
                        }
                    }}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#pager_personal'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            ignoreCase: true,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 54,
                    idaccion: id_solicitud
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            onCellSelect: function (rowid, iCol, cellcontent, e) {
                if (iCol == 5) {
                    if (rowid && rowid !== lastSel) {
                        jQuery('#tbl_personal').restoreRow(lastSel);
                        lastSel = rowid;
                    }
                    jQuery('#tbl_personal').jqGrid('editRow', rowid, true);
                    jQuery('#tbl_personal').editRow(rowid, true);
                }
            }
        }).navGrid("#pager_personal", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        grid_tbl_maestro.navButtonAdd('#pager_personal', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                guardarPersonal($('#idSolicitud').val());
            }
        });
    }

}

function guardarEquipos(id_solicitud) {
    var grid = jQuery("#tbl_equipo")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;



    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);


        if (data.periodo_ini === '' || data.periodo_ini === '&#160;' || data.periodo_ini === undefined) {
            error = true;
            mensajesDelSistema('Debe Ingresar la Fecha Inicial', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else if (data.periodo_fin === '' || data.periodo_fin === '&#160;' || data.periodo_fin === undefined) {
            error = true;
            mensajesDelSistema('Debe Ingresar la Fecha Final', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else if (parseFloat(data.h_trabajo) === 0 || data.h_trabajo === undefined) {
            error = true;
            mensajesDelSistema('Debe ingresar las horas', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        }


        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }
    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 55, informacion: JSON.stringify({idaccion: id_solicitud, rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    if (json.mensaje != "Algunos Registros se Encuentran Duplicados...") {
                        refrescarGridEquipo(id_solicitud);
                    }
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}

function guardarPersonal(id_solicitud) {
    var grid = jQuery("#tbl_personal")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;



    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);


        if (data.periodo_ini === '' || data.periodo_ini === '&#160;' || data.periodo_ini === undefined) {
            error = true;
            mensajesDelSistema('Debe Ingresar la Fecha Inicial', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else if (data.periodo_fin === '' || data.periodo_fin === '&#160;' || data.periodo_fin === undefined) {
            error = true;
            mensajesDelSistema('Debe Ingresar la Fecha Final', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else if (parseFloat(data.h_trabajo) === 0 || data.h_trabajo === undefined) {
            error = true;
            mensajesDelSistema('Debe ingresar las horas', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        }


        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }
    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 56, informacion: JSON.stringify({idaccion: id_solicitud, rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    if (json.mensaje != "Algunos Registros se Encuentran Duplicados...") {
                        refrescarGridPersonal(id_solicitud);
                    }
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}

function decimales(e, thi) {
// Backspace = 8, Enter = 13, ?0? = 48, ?9? = 57, ?.? = 46
    var field = thi;
    key = e.keyCode ? e.keyCode : e.which;

    if (key === 8)
        return true;
    if (key > 47 && key < 58) {
        if (field.val() === "")
            return true;
        var existePto = (/[.]/).test(field.val());
        if (existePto === false) {
            regexp = /.[0-9]{10}$/;
        }
        else {
            regexp = /.[0-9]{2}$/;
        }

        return !(regexp.test(field.val()));
    }
    if (key === 46) {
        if (field.val() === "")
            return false;
        regexp = /^[0-9]+$/;
        return regexp.test(field.val());
    }
    return false;
}

function refrescarGridEquipo(idaccion) {
    jQuery("#tbl_equipo").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 53,
                idaccion: idaccion
            }
        }
    });

    jQuery('#tbl_equipo').trigger("reloadGrid");
}

function refrescarGridPersonal(idaccion) {
    jQuery("#tbl_personal").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 54,
                idaccion: idaccion
            }
        }
    });

    jQuery('#tbl_personal').trigger("reloadGrid");
}
function facturacion(id_solicitud) {
    cargarFacturacion(id_solicitud);
    $("#Factura").dialog({
        width: '350',
        height: '400',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Proyeccion de Facturacion',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarFacturacion(id_solicitud) {
    var grid_tbl_maestro = jQuery("#tbl_factura");
    if ($("#gview_tbl_factura").length) {
        refrescarGridFactura(id_solicitud);
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Proyeccion de Facturacion",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '150',
            width: '314',
            cellEdit: true,
            colNames: ['#', 'Periodo', 'Valor', 'Concepto', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'periodo', index: 'periodo', width: 90, align: 'left'},
                {name: 'valor', index: 'valor', width: 100, align: 'left', formatter: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'concepto', index: 'concepto', width: 90, align: 'left'},
                {name: 'actions', index: 'actions', width: 100, align: 'center', hidden: true}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#pager_factura'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            ignoreCase: true,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 52,
                    idaccion: id_solicitud
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                /* var ids = jQuery("#tbl_gestion").jqGrid('getDataIDs');
                 var fila;
                 for (var i = 0; i < ids.length; i++) {
                 var cl = ids[i];
                 fila = jQuery("#tbl_gestion").jqGrid("getLocalRow", cl);
                 
                 
                 ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarGestion('" + cl + "', '" + id_solicitud + "');\">";
                 an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion2('Esta seguro de anular el Indicador de Gestion?','250','150',AnularGestion,'" + cl + "', '" + id_solicitud + "');\">";
                 jQuery("#tbl_gestion").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                 
                 }   */

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {


            }
        }).navGrid("#pager_factura", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        /*jQuery("#tbl_gestion").jqGrid("navButtonAdd", "#pager_gestion", {
         caption: "Nuevo",
         onClickButton: function () {
         crearGestion($('#idSolicitud').val());
         }
         });*/
    }

}

function refrescarGridFactura(idaccion) {
    jQuery("#tbl_factura").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 52,
                idaccion: idaccion
            }
        }
    });

    jQuery('#tbl_factura').trigger("reloadGrid");
}

function gestion(id_solicitud) {
    cargarGestion(id_solicitud);
    $("#Gestion").dialog({
        width: '655',
        height: '550',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Indicadores de Gestion',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarGestion(id_solicitud) {
    var grid_tbl_maestro = jQuery("#tbl_gestion");
    if ($("#gview_tbl_gestion").length) {
        refrescarGridGestion(id_solicitud);
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Indicador de Gestion",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '300',
            width: '620',
            cellEdit: true,
            colNames: ['#', 'Id Indicador', 'Indicador', 'Valor Minimo', 'Valor Maximo', 'Alerta Minima', 'Alerta Maxima', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'id_indicador', index: 'id_indicador', width: 80, align: 'left', hidden: true},
                {name: 'indicador', index: 'indicador', width: 80, align: 'left'},
                {name: 'valor_min', index: 'valor_min', width: 100, align: 'left', formatter: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_max', index: 'valor_max', width: 100, align: 'left', formatter: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'alerta_min', index: 'alerta_min', width: 100, align: 'left', formatter: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'alerta_max', index: 'alerta_max', width: 100, align: 'left', formatter: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'actions', index: 'actions', width: 100, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#pager_gestion'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            ignoreCase: true,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 48,
                    idaccion: id_solicitud
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var ids = jQuery("#tbl_gestion").jqGrid('getDataIDs');
                var fila;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = jQuery("#tbl_gestion").jqGrid("getLocalRow", cl);


                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarGestion('" + cl + "', '" + id_solicitud + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion2('Esta seguro de anular el Indicador de Gestion?','250','150',AnularGestion,'" + cl + "', '" + id_solicitud + "');\">";
                    jQuery("#tbl_gestion").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});

                }

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {


            }
        }).navGrid("#pager_gestion", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tbl_gestion").jqGrid("navButtonAdd", "#pager_gestion", {
            caption: "Nuevo",
            onClickButton: function () {
                crearGestion($('#idSolicitud').val());
            }
        });
    }

}

function refrescarGridGestion(idaccion) {
    jQuery("#tbl_gestion").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 48,
                idaccion: idaccion
            }
        }
    });

    jQuery('#tbl_gestion').trigger("reloadGrid");
}

function crearGestion(id_solicitud) {
    $('#div_Control_Gestion').fadeIn('slow');
    $('#idGestion').val('');
    $('#indicador').val('');
    $('#valor_min').val('');
    $('#valor_max').val('');
    $('#alerta_min').val('');
    $('#alerta_max').val('');
    var idaccion = id_solicitud;
    AbrirDivGestion(idaccion);
}

function AbrirDivGestion(idaccion) {
    cargarCombo('indicador', 3);
    $("#div_Control_Gestion").dialog({
        width: '400',
        height: '260',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR INDICADOR DE GESTION',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardarGestion(idaccion);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}

function guardarGestion(idaccion) {
    var indicador = $('#indicador').val();
    var valor_min = $('#valor_min').val();
    var valor_max = $('#valor_max').val();
    var alerta_min = $('#alerta_min').val();
    var alerta_max = $('#alerta_max').val();

    if (indicador !== '' && valor_min !== '' && valor_max !== '' && alerta_min !== '' && alerta_max !== '') {


        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            data: {
                opcion: ($('#idGestion').val() === '') ? 49 : 50,
                id: $('#idGestion').val(),
                indicador: $('#indicador').val(),
                valor_min: numberSinComas($('#valor_min').val()),
                valor_max: numberSinComas($('#valor_max').val()),
                alerta_min: numberSinComas($('#alerta_min').val()),
                alerta_max: numberSinComas($('#alerta_max').val()),
                idaccion: idaccion
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '270', '165');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridGestion(idaccion);
                        $("#div_Control_Gestion").dialog('close');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se pudo guardar el Hito!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    } else {
        mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');
    }
}

function editarGestion(cl, idaccion) {
    cargarCombo('indicador', 3);
    $('#div_Control_Gestion').fadeIn("slow");
    var fila = jQuery("#tbl_gestion").getRowData(cl);
    var indicador = fila['id_indicador'];
    var valor_min = numberConComas(fila['valor_min']);
    var valor_max = numberConComas(fila['valor_max']);
    var alerta_min = numberConComas(fila['alerta_min']);
    var alerta_max = numberConComas(fila['alerta_max']);


    $('#idGestion').val(cl);
    $('#indicador').val(indicador);
    $('#valor_min').val(valor_min);
    $('#valor_max').val(valor_max);
    $('#alerta_min').val(alerta_min);
    $('#alerta_max').val(alerta_max);
    AbrirDivEditarGestion(idaccion);
}

function AbrirDivEditarGestion(idaccion) {
    $("#div_Control_Gestion").dialog({
        width: '550',
        height: '280',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR INDICADOR DE GESTION',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarGestion(idaccion);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_ControlGestion").parent().find(".ui-dialog-titlebar-close").hide();
}

function AnularGestion(cl, id_solicitud) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            opcion: 51,
            id: cl

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {

                    refrescarGridGestion(id_solicitud);
                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo Anular el Concepto!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function flujocaja(id_solicitud) {
    cargarflujocaja(id_solicitud);
    $("#FlujoCaja").dialog({
        width: '550',
        height: '550',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Flujo de Caja',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarflujocaja(id_solicitud) {
    var grid_tbl_maestro = jQuery("#tbl_Flujo_Caja");
    if ($("#gview_tbl_Flujo_Caja").length) {
        refrescarGridFlujo(id_solicitud);
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Flujo de Caja",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '300',
            width: '520',
            cellEdit: true,
            colNames: ['#', 'Periodo', 'Valor', 'Concepto', 'Concepto', 'Descripcion', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'periodo', index: 'periodo', width: 80, align: 'left'},
                {name: 'valor', index: 'valor', width: 100, align: 'left', formatter: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'id_concepto', index: 'id_concepto', hidden: true},
                {name: 'concepto', index: 'concepto', width: 100, align: 'left'},
                {name: 'descripcion', index: 'descripcion', width: 100, align: 'left'},
                {name: 'actions', index: 'actions', width: 100, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#pager_flujo_caja'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: true,
            ignoreCase: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila;
                var estado_trazabilidad;
                var id_estado, iva_aiu = '';
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
//                    fila = grid_tabla.jqGrid("getLocalRow", cl);
//                    estado_trazabilidad = fila.trazabilidad;
//                    id_estado = fila.id_estado;
//                    presupuesto_terminado = fila.presupuesto_terminado;
//                                        
//                    
//                    var Cotizar = '', accion = '', generar_cot;
//                    if (id_estado == '4' || id_estado == '5' || id_estado == '8') {
//                        Cotizar = "<img src='/fintra/images/opav/Cotizacion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Cotizar'  onclick=\"principalAsignacionCostos('" + cl + "');\">";
//                            iva_aiu ="<img src='/fintra/images/opav/iva_aiu.png' align='absbottom' name='IVA_AIU' id='AYF'value='IVA_AIU'  width='26' height='26' title ='IVA - AIU'  onclick=\"abrir_Iva_Aiu('" + cl + "');\">";
//                    }else{
//                        Cotizar = "<img src='/fintra/images/opav/Cotizacion_grey.png' align='absbottom'   style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Cotizar' );\">";
//                        iva_aiu ="<img src='/fintra/images/opav/iva_aiu_grey.png' align='absbottom' name='IVA_AIU' id='AYF'value='IVA - AIU'  width='26' height='26' title ='IVA - AIU'>";
//                        
//                    }
//                    
//                    //generar_cot = "<img src='/fintra/images/opav/Contrato.png' align='absbottom' name='contrato' id='Generar_COT' value='Generar_COT'  width='26' height='26' title ='Generar Cotizaci�n'  onclick=\"generarCotizacion('" + cl + "');\">";
//                    
//                    
//                    if (fila.trazabilidad=='1') {
//                        accion = "<img src='/fintra/images/opav/Transicion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";    
//                    }else{
//                        accion = "<img src='/fintra/images/opav/Transicion_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n' >";    
//                    }
//                    
//                    
//                    var Cargar_Archivos = "<img src='/fintra/images/opav/File_add.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Cargar Archivos'  onclick=\"divemergente_Carga_Archivos('" + cl + "');\">";

//                    grid_tabla.jqGrid('setRowData', ids[i], {actions: accion +  Cotizar + iva_aiu /*+ generar_cot */+ Cargar_Archivos });
                    //wbs = "<img src='/fintra/images/opav/Wbs.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='19' height='19' title ='WBS Ejecucion'  onclick=\"abrirWbs('" + cl + "');\">";
                    accion = "<img src='/fintra/images/opav/Transicion_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n' >";
                    ExpT = "<img src='/fintra/images/opav/Exportar.png' align='absbottom' name='exportar_WBS_Project_Tareas' id='Exportar Project' value='Exportar_Project'  width='26' height='26' title ='Exportar Project Tareas'  onclick=\"exportarWBSProjectTareas('" + cl + "');\">";
                    ExpA = "<img src='/fintra/images/opav/Exportar.png' align='absbottom' name='exportar_WBS_Project_Asignacion' id='Exportar Project' value='Exportar_Project'  width='26' height='26' title ='Exportar Project Asignacion'  onclick=\"exportarWBSProjectAsignacion('" + cl + "');\">";
                    aspro = "<img src='/fintra/images/opav/Wbs.png' align='absbottom' name='aspectosproyecto'  value='Contrato'  width='19' height='19' title ='Aspectos Proyectos'  onclick=\"abrirAspectosProyecto('" + cl + "');\">";
                    asig_punt = "<img src='/fintra/images/opav/Wbs.png' align='absbottom' name='aspectosproyecto'  value='Contrato'  width='19' height='19' title ='Aspectos Proyectos'  onclick=\"abrirasingacion_puntaje('" + cl + "');\">";
                    grid_tabla.jqGrid('setRowData', ids[i], {actions: accion + ExpT + ExpA + aspro + asig_punt});

                }
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 40,
                    idaccion: id_solicitud
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var ids = jQuery("#tbl_Flujo_Caja").jqGrid('getDataIDs');
                var fila;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = jQuery("#tbl_Flujo_Caja").jqGrid("getLocalRow", cl);

                    if (fila.estado != '1') {
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarFlujo('" + cl + "', '" + id_solicitud + "');\">";
                        an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion2('Esta seguro de anular el Flujo de Caja?','250','150',AnularFlujo,'" + cl + "', '" + id_solicitud + "');\">";
                        jQuery("#tbl_Flujo_Caja").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                    }
                }

            },
                    ondblClickRow: function (rowid, iRow, iCol, e) {


                    }
        }).navGrid("#pager_flujo_caja", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tbl_Flujo_Caja").jqGrid("navButtonAdd", "#pager_flujo_caja", {
            caption: "Nuevo",
            onClickButton: function () {
                crearFlujo($('#idSolicitud').val());
            }
        });
    }

}

function crearFlujo(id_solicitud) {
    $('#div_Control').fadeIn('slow');
    $('#idFlujo').val('');
    $('#fecha_periodo').val('');
    $('#valor').val('');
    $('#concepto').val('');
    $('#descripcion').val('');
    var idaccion = id_solicitud;
    AbrirDivFlujo(idaccion);
}

function AbrirDivFlujo(idaccion) {
    cargarCombo('concepto', 2);
    $("#div_Control").dialog({
        width: '550',
        height: '280',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR FLUJO DE CAJA',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardarFlujo(idaccion);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}

function guardarFlujo(idaccion) {
    var periodo = $('#fecha_periodo').val();
    var valor = $('#valor').val();
    var concepto = $('#concepto').val();
    var descripcion = $('#descripcion').val();

    if (periodo !== '' && valor !== '' && concepto !== '' & descripcion !== '') {


        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            data: {
                opcion: ($('#idFlujo').val() === '') ? 41 : 42,
                id: $('#idFlujo').val(),
                periodo: $('#fecha_periodo').val(),
                valor: numberSinComas($('#valor').val()),
                concepto: $('#concepto').val(),
                descripcion: $('#descripcion').val(),
                idaccion: idaccion
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '270', '165');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridFlujo(idaccion);
                        $("#div_Control").dialog('close');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se pudo guardar el Hito!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    } else {
        mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');
    }
}

function editarFlujo(cl, idaccion) {
    cargarCombo('concepto', 2);
    $('#div_Control').fadeIn("slow");
    var fila = jQuery("#tbl_Flujo_Caja").getRowData(cl);
    var periodo = fila['periodo'];
    var valor = numberConComas(fila['valor']);
    var concepto = fila['id_concepto'];
    var descripcion = fila['descripcion'];

    $('#idFlujo').val(cl);
    $('#fecha_periodo').val(periodo);
    $('#valor').val(valor);
    $('#concepto').val(concepto);
    $('#descripcion').val(descripcion);
    AbrirDivEditarFlujo(idaccion);
}

function AbrirDivEditarFlujo(idaccion) {
    $("#div_Control").dialog({
        width: '550',
        height: '280',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR FLUJO',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarFlujo(idaccion);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}

function AnularFlujo(cl, id_solicitud) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            opcion: 43,
            id: cl

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {

                    refrescarGridFlujo(id_solicitud);
                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo Anular el Concepto!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function refrescarGridFlujo(idaccion) {
    jQuery("#tbl_Flujo_Caja").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 40,
                idaccion: idaccion
            }
        }
    });

    jQuery('#tbl_Flujo_Caja').trigger("reloadGrid");
}

function exportarExcelTareas() {
    var fullData = jQuery("#tabla_exp_tareas").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga Tareas'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "/fintra/controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            listado: myJsonString,
            opcion: 24
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function aspectosProyectos(idsolicitud) {
    alert(idsolicitud);
    window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/OportunidadNegocio/ModuloPlaneacion/&pagina=AspectosProyecto.jsp?idsolicitud=" + idsolicitud, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
}

function exportarExcelAsignacion() {
    var fullData = jQuery("#tabla_exp_asignacion").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga Asignacion'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "/fintra/controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            listado: myJsonString,
            opcion: 25
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function ReloadexportarWBSProjectTareas(grid_tabla, op, id_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Modulo&accion=Planeacion',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: id_solicitud
            }
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                mensajesDelSistema("No se encontraron registros", '204', '140', false);
            } else {
                exportarExcelTareas();
            }
        }
    });
    grid_tabla.trigger("reloadGrid");

}


function exportarWBSProjectTareas(id_solicitud) {
    toastr.success('' + id_solicitud, 'Function Exportar');

    var grid_tabla = jQuery("#tabla_exp_tareas");
    if ($("#gview_tabla_exp_tareas").length) {
        ReloadexportarWBSProjectTareas(grid_tabla, 22, id_solicitud);
    } else {
        grid_tabla.jqGrid({
            caption: "Tareas",
            url: '/fintra/controlleropav?estado=Modulo&accion=Planeacion',
            datatype: "json",
            height: '500',
            width: '1590',
            colNames: ['Nombre', 'Nivel de Esquema', 'Costo WBS'],
            colModel: [
                {name: 'nombre', index: 'nombre', width: 130, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nivel', index: 'nivel', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'valor', index: 'valor', width: 230, sortable: true, align: 'left', hidden: false, search: true}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager_tarea',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ignoreCase: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 22,
                    id_solicitud: id_solicitud

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                } else {
                    exportarExcelTareas();
                }
            }

        }).navGrid("#pager_tarea", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
    }
    //exportarExcelTareas();

}

function ReloadexportarWBSProjectAsignacion(grid_tabla, op, id_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Modulo&accion=Planeacion',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: id_solicitud
            }
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                mensajesDelSistema("No se encontraron registros", '204', '140', false);
            } else {
                exportarExcelAsignacion();
            }
        }
    });
    grid_tabla.trigger("reloadGrid");

}

function exportarWBSProjectAsignacion(id_solicitud) {
    //toastr.success(''+ id_solicitud,'Function Exportar');

    var grid_tabla = jQuery("#tabla_exp_asignacion");
    if ($("#gview_tabla_exp_asignacion").length) {
        ReloadexportarWBSProjectAsignacion(grid_tabla, 23, id_solicitud);
        //exportarExcelAsignacion();
    } else {
        grid_tabla.jqGrid({
            caption: "Asignacion",
            url: '/fintra/controlleropav?estado=Modulo&accion=Planeacion',
            datatype: "json",
            height: '500',
            width: '1590',
            colNames: ['Nombre', 'Nombre del Recurso', 'Cantidad', 'Rendimiento', 'Unidad', 'Cantidad de APU'],
            colModel: [
                {name: 'nombre', index: 'nombre', width: 130, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'recurso', index: 'recurso', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cantidad', index: 'valor', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'rendimiento', index: 'rendimiento', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'unidad', index: 'unidad', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cantidad_apu', index: 'cantidad_apu', width: 230, sortable: true, align: 'left', hidden: false, search: true}
            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager_asignacion',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ignoreCase: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 23,
                    id_solicitud: id_solicitud

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                } else {
                    exportarExcelAsignacion();
                }
            }

        }).navGrid("#pager_asignacion", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
    }
    //exportarExcelAsignacion();
}

function abrirAspectosProyecto(id_solicitud) {

    window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/OportunidadNegocio/ModuloPlaneacion/&pagina=AspectosProyecto.jsp?num_solicitud=" + id_solicitud + "&nombre_proyecto=" + jQuery("#tabla_infoSolicitud").jqGrid("getCell", 924037, "nombre_proyecto"), '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
}

function abrirasingacion_puntaje(id_solicitud) {

    window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/OportunidadNegocio/ModuloPlaneacion/&pagina=IngresoCargaLaboral.jsp?num_solicitud=" + id_solicitud + "&nombre_proyecto=" + jQuery("#tabla_infoSolicitud").jqGrid("getCell", 924037, "nombre_proyecto"), '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
}

/**********************************************************************************************************************************************************
 Fin Tabla Solicitudes
 ***********************************************************************************************************************************************************/


function aa() {
    console.log("lol");
}

function  mostrarVentanaAccion(id_solicitud) {
    $('#descripcion_causal').val('');
    var grid_tabla = $("#tabla_infoSolicitud");
    var trazabilidad = grid_tabla.getRowData(id_solicitud).trazabilidad;
    var id_estado = grid_tabla.getRowData(id_solicitud).id_estado;
    var nombre_estado = grid_tabla.getRowData(id_solicitud).nombre_estado;
    $('#idsolicitud').val(id_solicitud);
    $('#estadoActual').val(nombre_estado);
    $('#idestadoActual').val(id_estado);
    cargarEtapas(trazabilidad);
    cargarTrazabilidadOferta(id_solicitud);
    cargarEstadoEtapas(id_estado);
    $("#cambioEtapa").dialog({
        width: '800',
        height: '475',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Cambio de Estado',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Actualizar": function () {
                cambiarEstado();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarEtapas(trazabilidad) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 40,
            etapa: trazabilidad
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#etapa').val('');
                $('#idetapa').val('');
                for (var datos in json) {
                    $('#etapa').val(json[datos]);
                    $('#idetapa').val(datos);
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTrazabilidadOferta(id_solicitud) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 81,
            idsolicitud: id_solicitud
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#trazabilidad').val(json.respuesta);

            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarEstadoEtapas(id_estado) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 41,
            etapa: 1,
            id_estado: id_estado
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#estado').html('');
                $('#estado').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#estado').append('<option value=' + json[datos].id_estado_destino + '>' + json[datos].nombre_estado + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cambiarEstado() {

    var estado = $("#estado").val();

    if ((estado !== '') && ($("#descripcion_causal").val().length > 10)) {

        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            dataType: 'json',
            async: false,
            data: {
                opcion: 42,
                etapa: $("#idetapa").val(),
                estado_actual: $("#idestadoActual").val(),
                estados: $("#estado").val(),
                causal: $('#causal').val(),
                observacion: $('#descripcion_causal').val(),
                idsolicitud: $('#idsolicitud').val()
            },
            success: function (json) {
                console.log(json.respuesta);
                var resp = json.respuesta;
                if (resp === 'Guardado') {
                    toastr.success('Cambio satisfactorio', 'Enviado');
                    $("#cambioEtapa").dialog("close");
                    cargarInfoSolicitudes();
                } else {
                    mensajesDelSistema("Error", '204', '140', false);
                }
            }
        });
    } else {
        toastr.error('Por favor llene todos los campos', 'Error');
    }
}
/**********************************************************************************************************************************************************
 Utiles Generales
 ***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon', '#perc_rete'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function comas_numeros(campo) {
    var variable = $('#' + campo);
    variable.val(numberConComas(variable.val()));
}

function cargarCombo(id, op) {
    var elemento = $('#' + id);
    $.ajax({
        url: "/fintra/controlleropav?estado=Cotizacion&accion=Sl",
        datatype: 'json',
        type: 'GET',
        data: {opcion: 23, op: op},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    elemento.append('<option value=0>' + "..." + '</option>');
                    for (var e in json) {
                        //elemento.append('<option value="' + e + '" onclick="CargarGridApus>' + json[e] + '</option>');
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function mensajeConfirmacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajeConfirmacion2(msj, width, height, okAction, id, id2) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id, id2);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}
/**********************************************************************************************************************************************************
 Fin Utiles Generales
 ***********************************************************************************************************************************************************/
