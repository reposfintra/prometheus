$(function () {
    //opcion: ($('#idConcepto').val() === '') ? 1 : 2,
    maximizarventana();
    grid_Causales();
    cargando_toggle();

});


function grid_Causales() {

    $('#tbl_causales_canasta').jqGrid('GridUnload');

    var grid_tabla = jQuery("#tbl_causales_canasta");

    grid_tabla.jqGrid({
        caption: "Proyecto - Selectrick",
        url: '/fintra/controlleropav?estado=Modulo&accion=Canasta',
        datatype: "json",
        height: '550',
        width: '1590',
        colNames: ['id', 'Nombre', 'Descripcion', 'Acciones'],
        colModel: [
            {name: 'id', index: 'id', width: 10, sortable: true, align: 'left', hidden: true, search: true, key: true},
            {name: 'nombre', index: 'nombre', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'descripcion', index: 'descripcion', width: 30, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'acciones', index: 'acciones', width: 5, sortable: true, align: 'center', hidden: false, search: true}
        ],
        rowNum: 1000000,
        rowTotal: 1000000,
        loadonce: true,
        rownumWidth: 60,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: false,
        rownumbers: false,
        pager: '#pager_tbl_causales_canasta',
        multiselect: false,
        multiboxonly: false,
        pgtext: null,
        pgbuttons: false,
        editurl: 'clientArray',
        subGrid: false,
        ondblClickRow: function (rowid, iCol, e) {



        }, gridComplete: function () {
            var ids = jQuery("#tbl_causales_canasta").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarCausal('" + cl + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion('Esta seguro de anular el Concepto?','250','150',EliminarCausal,'" + cl + "');\">";
                    jQuery("#tbl_causales_canasta").jqGrid('setRowData', ids[i], {acciones: ed + '   ' + an});
                }
        },
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 5

            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 150);
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                toastr.warning("No se encontraron registros", "Lo sentimos");
            }
        }

    }).navGrid("#pager_tbl_causales_canasta", {add: false, edit: false, del: false, search: false, refresh: false}, {
    });

    grid_tabla.navButtonAdd('#pager_tbl_causales_canasta', {
        caption: "Nuevo",
        title: "Nuevo",
        buttonicon: "ui-icon-pluss",
        onClickButton: function () {
            AbrirDivCausal();
        }

    });

}

function editarCausal(cl){
    var fila = jQuery("#tbl_causales_canasta").getRowData(cl);
    $('#id_causal').val(fila['id']);
    $('#nombre').val(fila['nombre']);
    $('#descripcion').val(fila['descripcion']);
    AbrirDivCausal();
}

function AbrirDivCausal() {
    $("#divCausal").dialog({
        width: '650',
        height: '280',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CAUSAL',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardarCausal();
            },
            "Salir": function () {
                limpiar_formulario();   
                $(this).dialog("destroy");
            }
        }
    });

    $("#divCausal").parent().find(".ui-dialog-titlebar-close").hide();
    $("#divCausal").css('overflow-x', 'hidden');
}

function guardarCausal() {
    $.ajax({
        url: "/fintra/controlleropav?estado=Modulo&accion=Canasta",
        datatype: 'json',
        type: 'GET',
        data: {
            opcion: ($('#id_causal').val() === '') ? 6 : 7, 
            id: $('#id_causal').val() , 
            nombre: $('#nombre').val() , 
            descripcion: $('#descripcion').val()  
        },
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    toastr.error(json.error, "Error");
                } else {
                    toastr.success("Guardado","Exito")
                    grid_Causales();
                    limpiar_formulario();
                    $("#divCausal").dialog('destroy');
                    
                }
            } finally {
            }
        }
    });
}

function EliminarCausal(cl) {
    
    $.ajax({
        url: "/fintra/controlleropav?estado=Modulo&accion=Canasta",
        datatype: 'json',
        type: 'GET',
        data: {
            opcion: 8, 
            id: cl 
        },
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    toastr.error(json.error, "Error");
                } else {
                    toastr.success("Se a elimininado de eforma correcta","Exito")
                    grid_Causales();
                    limpiar_formulario();
                    $("#divCausal").dialog('destroy');
                    
                }
            } finally {
            }
        }
    });
}

function limpiar_formulario(){
    $('#form_causales_canasta').trigger("reset");
}






/**********************************************************************************************************************************************************
                                                            Utiles Generales
***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon', '#perc_rete'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle(){
    $('#loader-wrapper').toggle();
}


function mensajeConfirmacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsj');
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}
/**********************************************************************************************************************************************************
                                                           Fin Utiles Generales
***********************************************************************************************************************************************************/