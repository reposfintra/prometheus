/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    cargando_toggle();
    maximizarventana();
    listarHitos();
    $( "#fecha" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
});


function listarHitos() {
    var grid_tbl_maestro = jQuery("#tbl_hitos");
    if ($("#gview_tbl_hitos").length) {
        refrescarGridHitos();
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Hitos",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '400',
            width: '780',
            cellEdit: true,
            colNames: ['#', 'Nombre Hito', 'Fecha', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true},
                {name: 'nombre', index: 'razon_social', width: 430, align: 'left'},
                {name: 'fecha', index: 'fecha', width: 100, align: 'left'},
                {name: 'actions', index: 'actions', width: 110, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tbl_hitos'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 26
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var ids = jQuery("#tbl_hitos").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarHito('" + cl + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion('Esta seguro de anular el Hito?','250','150',AnularHito,'" + cl + "');\">";
                    jQuery("#tbl_hitos").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                }
                

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {


            }
        }).navGrid("#page_tbl_hitos", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tbl_hitos").jqGrid("navButtonAdd", "#page_tbl_hitos", {
            caption: "Nuevo",
            onClickButton: function () {
                crearHitos();
            }
        });
    }

}

function refrescarGridHitos() {
    jQuery("#tbl_hitos").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 26
            }
        }
    });

    jQuery('#tbl_hitos').trigger("reloadGrid");
}


function crearHitos() {
    $('#div_Control').fadeIn('slow');
    $('#idHito').val('');
    $('#nombre').val('');
    $('#fecha').val('');
    AbrirDivHito();
}

function AbrirDivHito() {
    $("#div_Control").dialog({
        width: '420',
        height: '180',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR HITO',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardarHito();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarHito(cl) {

    $('#div_Control').fadeIn("slow");
    var fila = jQuery("#tbl_hitos").getRowData(cl);
    var nombre = fila['nombre'];
    var fecha = fila['fecha'];

    $('#idHito').val(cl);
    $('#nombre').val(nombre);
    $('#fecha').val(fecha);
    AbrirDivEditarHito();
}

function AbrirDivEditarHito() {
    $("#div_Control").dialog({
        width: '420',
        height: '180',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR HITO',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarHito();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}



function guardarHito() {
    var nombre = $('#nombre').val();
    var fecha = $('#fecha').val();

    if (nombre !== '' && fecha !== '') {


        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            data: {
                opcion: ($('#idHito').val() === '') ? 27 : 28,
                id: $('#idHito').val(),
                nombre: $('#nombre').val(),
                fecha: $('#fecha').val()
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '270', '165');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridHitos();
                        $("#div_Control").dialog('close');
                    }

                } else {
                    
                    mensajesDelSistema("Lo sentimos no se pudo guardar el Hito!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    } else {
        mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');
    }
}

function AnularHito(cl){
    
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            opcion: 29,
            id: cl

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {
                    
                    refrescarGridHitos()();
                }

            } else {
                
                mensajesDelSistema("Lo sentimos no se pudo Anular el Concepto!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
        
}

/**********************************************************************************************************************************************************
                                                            Utiles Generales
***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon', '#perc_rete'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle(){
    $('#loader-wrapper').toggle();
}


function mensajeConfirmacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}
/**********************************************************************************************************************************************************
                                                           Fin Utiles Generales
***********************************************************************************************************************************************************/