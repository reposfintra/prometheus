/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    maximizarventana();
    //cargarComboInsumos();
    cargarComboCateg();
   
    $("#C_categoria").click(function () {
        cargarImpacto();
        //cargarCategoriasAC();
        $("#idcategoria").val(document.getElementById("categoria").value);
    });
    $("#boton_guardar_proInterno").click(function () {
        actualizarProInterno();
    });
    $("#buscarnumos").click(function () {
        busqueda();
    });
    /////////////////////////////////////
    $("#buscarfitro").click(function () {
        var valor = $('#b_idespecificacion').val();
        refrescarGridvalPredFiltro(valor);
    });
    /////////////////////////////////////


    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    });
    $('.solo-numeric').live('keypress', function (event) {
        return numbersonly(this, event);
    });
    ordenarCombo('insumos');
    cargando_toggle();

});

function cargarComboCateg(filtro) {
    var elemento = $('#categoria'), sql = 'ConsultaTiposCategoria';
    $.ajax({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        type: 'get',
        data: {opcion: 59, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    console.log(JSON.stringify(json));
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function cargarComboInsumos(filtro) {
    var elemento = $('#insumos'), sql = 'ConsultaTiposInsumos';
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'get',
        data: {opcion: 52, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    console.log(JSON.stringify(json));
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function cargarCombo(id, filtro) {
    var elemento = $('#' + id)
            , sql = (id === 'b_idsubcategoria') ? 'subcategorias' : 'especificacionxsubcategoria';
    //$("#lui_tabla_productos,#load_tabla_productos").show();
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'get',
        data: {opcion: 40, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}


function busqueda() {
    buscarMultiservicios();
    $("#dialogMsjmultiservicio").dialog({
        width: '400',
        height: '270',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'BUSCAR CATEGORIA',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Asignar": function () {
                var categoria = $("#multiser").val();
                $('#idcategoria').val(categoria);
                $("#multiser").val('');
                buscarCategoria(categoria);
                $(this).dialog("close");
            },
            "Salir": function () {
                $("#multiser").val('');
                $(this).dialog("close");
            }
        }
    });
}

function buscarCategoria(id) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 36,
            idCategoria: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridProcesosMeta();
                    refrescarGridProcesosInterno(idProceso);
                    cargarComboCategorias();
                    mensajesDelSistema("Se actualiz� la Categoria", '250', '150', true);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo actualizar la Categoria!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}




function buscarMultiservicios() {
    var info = {};
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 35
                    // multiservicio:$('#multiservicio').val()
        },
        success: function (json) {
            info = json;
            $("#multiser").autocomplete({
                source: info,
                minLength: 2
            });
        }
    });
}

//Carga los nombre del autocompletar de Categoria
function cargarCategoriasAC() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    var insumo = document.getElementById("insumos").value;
    var nomCategorias = [];
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 1,
            insumo: insumo
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                for (j in json.rows) {
                    nomCategorias.push(json.rows[j].nombre);
                }
                $("#nommeta").autocomplete({
                    source: nomCategorias,
                    minLength: 2
                });

            }
        }
    });
}

function cargarImpacto() {

    var categoria = document.getElementById("categoria").value;
    var url = './controlleropav?estado=Modulo&accion=Planeacion&opcion=61&categoria=' + categoria;
    if ($("#gview_Impacto").length) {
        refrescarGridImpacto(categoria);
    } else {
        jQuery("#Impacto").jqGrid({
            caption: 'Impacto',
            url: url,
            datatype: 'json',
            height: 400,
            width: 800,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Puntaje', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600'},
                {name: 'descripcion', index: 'descripcion', hidden: true, sortable: true, align: 'center', width: '700px'},
                {name: 'puntaje', index: 'puntaje', sortable: true, align: 'center', width: '60'},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '100'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_impacto',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                var ids = jQuery("#Impacto").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarImpacto('" + cl + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el Impacto?','250','150',anularImpacto,'" + cl + "');\">";
                    jQuery("#Impacto").jqGrid('setRowData', ids[i], {actions: ed + '   ' + '   ' + an});
                }
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_impacto", {edit: false, add: false, del: false});
        jQuery("#Impacto").jqGrid("navButtonAdd", "#page_tabla_impacto", {
            caption: "Nuevo",
            title: "Agregar nuevo Impacto",
            onClickButton: function () {
                crearMetaProceso();
            }
        });
        jQuery("#Impacto").jqGrid('filterToolbar', "#searchImpacto",
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true,
                    gs_nombe: false

                });
        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function crearMetaProceso() {
    $('#nommeta').val('');
    $('#descmeta').val('');
    $('#puntaje11').val('');
    $('#div_categoria').fadeIn('slow');
    AbrirDivCrearMetaProceso();
}

function AbrirDivCrearMetaProceso() {
    $("#div_categoria").dialog({
        width: 700,
        height: 260,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR IMPACTO',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarMetaProceso();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function editarImpacto(cl) {
    $('#div_categoria').fadeIn("slow");
    var fila = jQuery("#Impacto").getRowData(cl);
    var nombre=fila['nombre'];
    var descripcion=fila['descripcion'];
    var puntaje=fila['puntaje'];

    $('#idImpacto').val(cl);
    $('#nommeta').val(nombre);
    $('#descmeta').val(descripcion);
    $('#puntaje11').val(puntaje);
    AbrirDivEditarImpacto();
}

function AbrirDivEditarImpacto() {
    $("#div_categoria").dialog({
        width: 700,
        height: 260,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR IMPACTO',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarMetaProceso();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarMetaProceso() {
    var idinsumo = $('#idcategoria').val();
    var nomMeta = $('#nommeta').val();
    var descMeta = $('#descmeta').val();
    var puntaje = $('#puntaje11').val();
    var url = './controlleropav?estado=Modulo&accion=Planeacion';
    if (nomMeta !== '' && descMeta !== '' && puntaje !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: ($('#idImpacto').val() === '') ? 62 : 68,
                id: $('#idImpacto').val(),
                nombre: nomMeta,
                descripcion: descMeta,
                categoria: idinsumo,
                puntaje: puntaje
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        //cargarComboCategorias();
                        //refrescarGridCategoria(idinsumo);
                        cargarImpacto();
                        mensajesDelSistema("Se cre� el impacto", '250', '150', true);
                        $('#nommeta').val('');
                        $('#descmeta').val('');
                        $('#puntaje11').val('');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear el impacto!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }
}

function anularImpacto(cl) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            opcion: 69,
            id: cl

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {

                    refrescarGridImpacto(document.getElementById("categoria").value);
                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo Anular el Impacto!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function refrescarGridUnidadMedida() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=39';
    jQuery("#unidadmedida").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#unidadmedida').trigger("reloadGrid");
}

function refrescarGridImpacto(categoria) {
    var url = './controlleropav?estado=Modulo&accion=Planeacion&opcion=61&categoria=' + categoria;
    jQuery("#Impacto").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#Impacto').trigger("reloadGrid");
}

function editarProcesosMeta(cl) {

    $('#div_editar_categoria').fadeIn("slow");
    var fila = jQuery("#Categorias").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    $('#idMetaEdit').val(cl);
    $('#nommetaEdit').val(nombre);
    $('#descmetaEdit').val(descripcion);
    AbrirDivEditarMetaProceso();
    cargarProcesosInternos();
}

function AbrirDivEditarMetaProceso() {
    $("#div_editar_categoria").dialog({
        width: 700,
        height: 675,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'EDITAR CATEGORIA',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarMetaProceso() {
    var nombre = $('#nommetaEdit').val();
    var descripcion = $('#descmetaEdit').val();
    var idProceso = $('#idMetaEdit').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    if (nombre !== '' && descripcion !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 3,
                nombre: nombre,
                descripcion: descripcion,
                idProceso: idProceso
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridProcesosMeta();
                        refrescarGridProcesosInterno(idProceso);
                        cargarComboCategorias();
                        mensajesDelSistema("Se actualiz� la Categoria", '250', '150', true);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar la Categoria!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');
    }

}

function anularMetaProceso(cl) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 15,
            idProceso: cl
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridCategoria(document.getElementById("insumos").value);
                    cargarComboCategorias();
                    mensajesDelSistema("Se anul� la Categoria", '250', '150', true);
                    $('#div_categoria').fadeOut();
                } else if (json.respuesta === "OK1") {
                    mensajesDelSistema("Esta categoria ya tiene datos, no se puede anular...", '250', '150', true);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular la Categoria!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function existeUsuarioRelMetaProceso(id) {
    $.ajax({
        url: '/fintra/controlleropav?estado=Procesos&accion=Catalogo',
        datatype: 'json',
        type: 'post',
        data: {
            opcion: 22,
            tipo: "META",
            idProceso: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.respuesta === "SI") {
                    mensajeConfirmAnulacion('Alerta!!! Existen usuarios asociados al Meta proceso, desea continuar?', '350', '165', id);
                } else {
                    anularMetaProceso(id);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo realizar el proceso!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function crearProcesoInterno() {
    var idMetaProceso = $('#idMetaEdit').val();
    $('#idProMeta').val(idMetaProceso);
    $('#nomProceso').val('');
    $('#descProceso').val('');
    $('#div_Subcategoria').fadeIn("slow");
    AbrirDivCrearSubCategoria();
    cargarSubcategoriaAC();
}



function cargarSubcategoriaAC() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    var insumo = $('#idinsumo').val();
    var nomSubCategoria = [];
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 55,
            insumo: insumo
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                for (j in json) {
                    nomSubCategoria.push(json[j]);
                }
                $("#nomProceso").autocomplete({
                    source: nomSubCategoria,
                    minLength: 2
                });

            }
        }
    });
}


function AbrirDivCrearSubCategoria() {
    $("#div_Subcategoria").dialog({
        width: 675,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarSubCategoria();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

//Carga los nombre del autocompletar de subcategoria
function cargarEspecificacionAC() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    var categoria = $('#idMetaEdit').val();
    var nomEspecificacion = [];
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 56,
            categoria: categoria
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                for (j in json) {
                    nomEspecificacion.push(json[j]);
                }
                $("#nomEspecificacion").autocomplete({
                    source: nomEspecificacion,
                    minLength: 2
                });

            }
        }
    });
}

function cargarProcesosInternos() {
    var idProcesoMeta = $('#idMetaEdit').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=4&procesoMeta=' + idProcesoMeta;
    if ($("#gview_subcategoria").length) {
        refrescarGridProcesosInterno(idProcesoMeta);
    } else {
        jQuery("#subcategoria").jqGrid({
            caption: 'Subcategorias',
            url: url,
            datatype: 'json',
            height: 330,
            width: 620,
            colNames: ['Id', 'Id Proceso Meta', 'Categoria', 'Subcategoria', 'Descripcion', 'Acciones', 'Id Subcategoria'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key: true},
                {name: 'id_tabla_rel', index: 'id_tabla_rel', hidden: true},
                {name: 'descripcionTablaRel', index: 'descripcionTablaRel', sortable: true, align: 'center', width: '600px', hidden: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600px'},
                {name: 'descripcion', index: 'descripcion', hidden: true, sortable: true, align: 'center', width: '600px'},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '200px'},
                {name: 'idsubcategoria', index: 'idsubcategoria', hidden: true, sortable: true, align: 'center', width: '600px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_subcategorias',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                var ids = jQuery("#subcategoria").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/link_new.png' align='absbottom'  name='editar' id='editar' width='16' height='16' title ='editar especificacion'  onclick=\"editarSubCategoria('" + cl + "');\">";
                    //us = "<img src='/fintra/images/botones/iconos/users.png' align='absbottom'  name='asociar' id='asociar' width='16' height='16' title ='Asociar usuarios'  onclick=\"asociarUsuarioProceso('" + cl + "');\">";
//                      an = "<img src='/fintra/images/botones/iconos/anular.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el proceso interno seleccionado?','250','150',existeUsuarioRelProcesoInterno,'" + cl + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='16' height='16' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan datos asociados a la subcategoria, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
                    jQuery("#subcategoria").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_subcategorias", {edit: false, add: false, del: false});
        jQuery("#subcategoria").jqGrid("navButtonAdd", "#page_tabla_subcategorias", {
            caption: "Nuevo",
            title: "Agregar nuevo proceso interno",
            onClickButton: function () {
                crearProcesoInterno();
            }
        });
    }
}

function refrescarGridProcesosInterno(id) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=4&procesoMeta=' + id;
    jQuery("#subcategoria").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#subcategoria').trigger("reloadGrid");
}

function guardarSubCategoria() {
    var idProcesoMetaAct = $('#idMetaEdit').val();
    var idProMeta = $('#idProMeta').val();
    var nomProceso = $('#nomProceso').val();
    var descProceso = $('#descProceso').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    if (nomProceso !== '' && descProceso !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 34,
                nombre: nomProceso,
                descripcion: descProceso,
                procesoMeta: idProcesoMetaAct
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        mensajesDelSistema("Se cre� la Relacion", '250', '150', true);
                        $('#nomProceso').val('');
                        $('#descProceso').val('');
                        refrescarGridProcesosInterno(idProcesoMetaAct);
                    }
                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear la Subcategoria", '250', '150');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');
    }
}


function anularProcesoInterno(cl) {
    var idProcesoMeta = $('#idMetaEdit').val();
    $.ajax({
        url: '/fintra/controlleropav?estado=Procesos&accion=Catalogo',
        datatype: 'json',
        type: 'get',
        data: {
            opcion: 16,
            idProinterno: cl,
            idProcesoMeta: idProcesoMeta

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridProcesosInterno(idProcesoMeta);
                    mensajesDelSistema("Se anul� la subcategoria", '250', '150', true);
                }
                else if (json.respuesta === "OK1") {
                    mensajesDelSistema("No se puede anular subcategoria, tiene datos asociados...", '250', '150', true);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular la Subcategoria!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function editarSubCategoria(id) {
    var nomProinterno = jQuery('#subcategoria').getCell(id, 'nombre');
    var descProinterno = jQuery('#subcategoria').getCell(id, 'descripcion');
    var idMetaProceso = jQuery('#subcategoria').getCell(id, 'id_tabla_rel');
    var idSubcategoria = jQuery('#subcategoria').getCell(id, 'idsubcategoria');
    $('#nomProinterno').val(nomProinterno);
    $('#descProinterno').val(descProinterno);
    $('#idProMetaEdit').val(idMetaProceso);
    $('#idProinterno').val(id);
    $('#idSubcategoria').val(idSubcategoria);
    $('#div_editar_especificacion').fadeIn();
    $('#div_und_prointerno').fadeIn();
    AbrirDivEditarProcesoInterno();
    listarUndNegocioPro(id);
    listarUndNegocio();
}




function AbrirDivEditarProcesoInterno() {
    $("#div_editar_especificacion").dialog({
        width: 800,
        height: 768,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'EDITAR SUBCATEGORIA',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    }).navGrid("#page_tabla_especificacion", {edit: false, add: false, del: false, search: false});
    ;
}

function listarUndNegocioPro(id) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=8&idProinterno=' + id;
    if ($("#gview_UndNegocioPro").length) {
        refrescarGridUndNegocioPro(id);
    } else {
        jQuery("#UndNegocioPro").jqGrid({
            caption: 'Especificaciones de la Subcategoria',
            url: url,
            datatype: 'json',
            height: 380,
            width: 300,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Tipo Dato', 'R', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden: true, align: 'center', width: '100px', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '600px'},
                {name: 'descripcion', index: 'descripcion', hidden: true, sortable: true, align: 'left', width: '600px'},
                /*{name: 'obligatorio', index: 'obligatorio', sortable: true, align: 'center', width: '100px'},*/
                {name: 'tipo_dato', index: 'tipo_dato', hidden: true, sortable: true, align: 'left', width: '600px'},
                {name: 'obligatorio', index: 'obligatorio', width: 75, align: 'center', formatter: 'checkbox',
                    edittype: 'checkbox', editoptions: {value: '1:0'}},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '100'}

            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                $('#bt_asociar_especificacion').show();
                var ids = jQuery("#UndNegocioPro").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' align: 'center' height='15' title ='Editar'  onclick=\"crearEspecificacionEdit2(" + cl + ");\">";
                    jQuery("#UndNegocioPro").jqGrid('setRowData', ids[i], {actions: ed});
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}

function crearEspecificacionEdit2(id) {
    $('#div_especificacion').fadeIn("slow");

    var fila = jQuery("#UndNegocioPro").getRowData(id);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    var tipo_dato = fila['tipo_dato'];
    $('#idEsp').val(id);
    $('#nomEspecificacion').val(nombre);
    $('#descEspecificacion').val(descripcion);
    $('#tipo_dato').val(tipo_dato);
    $('#tipodatos').val(tipo_dato);


    //cargarEspecificacionesedit();
    //cargarProcesosInternos();
    redisenareditesp(tipo_dato, id);
    cargarCombo('b_idsubcategoria', []);
    $("#div_especificacion").dialog("destroy");
    if (tipo_dato === '0') {
        AbrirDivEditEspecificacion("715", "790");
    } else {
        AbrirDivEditEspecificacion("715", "255");
    }



}

function refrescarGridUndNegocioPro(id) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=8&idProinterno=' + id;
    jQuery("#UndNegocioPro").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#UndNegocioPro').trigger("reloadGrid");
}

function listarUndNegocio() {
    var idProInterno = $('#idProinterno').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=6&idProceso=' + idProInterno;
    $('#div_undNegocios').fadeIn();
    $('#bt_listar_undPro').hide();
    $('#bt_asociar_especificacion').show();
    if ($("#gview_UndadesNegocios").length) {
        refrescarGridUndNegocio(idProInterno);
    } else {
        jQuery("#UndadesNegocios").jqGrid({
            caption: 'Especificaciones',
            url: url,
            datatype: 'json',
            height: 380,
            width: 300,
            colNames: ['Id', 'Descripcion', 'Nombre', 'Tipo Dato', 'R', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden: true, align: 'center', width: '100px', key: true},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'left', width: '600px'},
                {name: 'nombre', index: 'nombre', hidden: true, sortable: true, align: 'left', width: '600px'},
                {name: 'tipo_dato', index: 'tipo_dato', hidden: true, sortable: true, align: 'left', width: '600px'},
                {name: 'obligatorio', index: 'obligatorio', sortable: true, width: 75, align: 'center', formatter: 'checkbox',
                    edittype: 'checkbox', formatoptions: {disabled: false}, editoptions: {value: '1:0'}},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '100'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            pgbuttons: false,
            pgtext: null,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            pager: '#page_tabla_especificacion',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            gridComplete: function () {
                $('#bt_desasociar_especificacion').show();
                var ids = jQuery("#UndadesNegocios").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' align: 'center' height='15' title ='Editar'  onclick=\"crearEspecificacionEdit(" + cl + ");\">";
                    jQuery("#UndadesNegocios").jqGrid('setRowData', ids[i], {actions: ed});
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_especificacion", {
            first_page: false,
            l_page: false,
            refresh: false,
            search: true,
            edit: false,
            add: false,
            del: false});
        jQuery("#UndadesNegocios").jqGrid("navButtonAdd", "#page_tabla_especificacion", {
            caption: "Nuevo",
            title: "Agregar nuev Especificacion",
            onClickButton: function () {
                cargarEspecificacionAC();
                crearEspecificacion();
            }
        });
        jQuery("#UndadesNegocios").jqGrid('filterToolbar', "#searchEspecificaciones",
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true,
                    gs_nombe: false

                });
        $("#gs_obligatorio").attr('hidden', true);
    }
}

function refrescarvalorespredeterminadosespedit(id) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=49&id=' + id;
    jQuery("#valorespredeterminados_aso").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#valorespredeterminados_aso').trigger("reloadGrid");
}

function listarvalorespredeterminadosespedit() {

    var id = $('#idEsp').val();

    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=49&id=' + id;
    //var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    if ($("#gview_valorespredeterminados_aso").length) {
        refrescarvalorespredeterminadosespedit(id);
    } else {
        jQuery("#valorespredeterminados_aso").jqGrid({
            caption: 'Valores Predeterminados',
            url: url,
            datatype: 'json',
            height: 280,
            width: 300,
            colNames: ['Id', 'Nombre', 'Id Relacion', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, key: true},
                {name: 'valor_xdefecto', index: 'valor_xdefecto', sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'idrel', index: 'idrel', hidden: true, sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, editable: true, align: 'left', width: '600px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_valorespredeterminados_aso',
            multiselect: true,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {


            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}

function refrescarGridvalorespredeterminadosedit() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=38&id=' + 0;
    jQuery("#valorespredeterminados").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#valorespredeterminados').trigger("reloadGrid");
}

function listarvalorespredeterminadosedit() {
    var id = $('#idEsp').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=38';
    //var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    if ($("#gview_valorespredeterminados").length) {
        refrescarGridvalorespredeterminadosedit();
    } else {
        jQuery("#valorespredeterminados").jqGrid({
            caption: 'Valores Predeterminados',
            url: url,
            datatype: 'json',
            height: 280,
            width: 300,
            colNames: ['Id', 'Nombre', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, key: true},
                {name: 'valor_xdefecto', index: 'valor_xdefecto', sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, editable: true, align: 'left', width: '600px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_valorespredeterminados',
            multiselect: true,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {

                var ids = jQuery("#valorespredeterminados").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' align: 'center' height='15' title ='Editar'  onclick=\"pruebas('" + cl + "');\">";
                    jQuery("#valorespredeterminados").jqGrid('setRowData', ids[i], {actions: ed});
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_valorespredeterminados", {add: false, edit: false, del: false, search: true, refresh: false});
        jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
            caption: "Nuevo",
            title: "Agregar nueva fila",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {

                var grid = $("#valorespredeterminados")
                        , rowid = 'neo_' + grid.getRowData().length;
                //alert(rowid);
                grid.addRowData(rowid, {});
                grid.jqGrid('editRow', rowid, true, function () {
                    //  $("input, select", e.target).focus();
                }, null, null, {}, null, null, function (id) {
                    var g = $('#valorespredeterminados')
                            , r = g.jqGrid('getLocalRow', id);
                    if (!r.id) {
                        g.jqGrid('delRowData', id);
                    }
                });
            }
            //position: "first"
        });
        jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                guardarValp();
            }
            //position: "first"
        });
        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function cargardivseleccionedit(id) {
    //listarvalorespredeterminadosesp();
    //listarvalorespredeterminados();
    listarvalorespredeterminadosespedit();
    listarvalorespredeterminadosedit();
    $('#rtipodato').hide();
    $('#div_valorespredeterminados').show();
    $('#div_valorespredeterminados_aso').show();
    $("#div_seleccion").show();
//    $("#div_especificacion").dialog({
//        position: {'my': 'center', 'at': 'center'}
//    });

}

function cargardigitacionedit() {
    $('#rtipodato').show();
    $("#div_seleccion").hide();
    /*$("#div_especificacion").dialog({
     position: {'my': 'center', 'at': 'center'}
     });*/
}

function redisenareditesp(tipo_dato, id) {

    if (tipo_dato === "0") {
        redisenardivespecificaciones(1);
        cargardivseleccionedit(id);
        $('#idtiposeleccion').val(0);
        $('#radio1').attr('checked', true);
        $('#radio2').attr('checked', false);

    } else {
        redisenardivespecificaciones(2);
        cargardigitacionedit();
        $('#idtiposeleccion').val('');
        $('#radio1').attr('checked', false);
        $('#radio2').attr('checked', true);

    }
    document.getElementById('radio1').disabled = true;
    document.getElementById('radio2').disabled = true;

}

function crearEspecificacionEdit(id) {
    $('#div_especificacion').fadeIn("slow");

    var fila = jQuery("#UndadesNegocios").getRowData(id);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    var tipo_dato = fila['tipo_dato'];
    $('#idEsp').val(id);
    $('#nomEspecificacion').val(nombre);
    $('#descEspecificacion').val(descripcion);
    $('#tipo_dato').val(tipo_dato);
    $('#tipodatos').val(tipo_dato);


    //cargarEspecificacionesedit();
    //cargarProcesosInternos();
    redisenareditesp(tipo_dato, id);
    cargarCombo('b_idsubcategoria', []);
    $("#div_especificacion").dialog("destroy");
    if (tipo_dato === '0') {
        AbrirDivEditEspecificacion("715", "790");
    } else {
        AbrirDivEditEspecificacion("715", "255");
    }



}

function AbrirDivEditEspecificacion(width, height) {
    $("#div_especificacion").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'EDITAR ESPECIFICACION',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarEspecificacionedit();
                document.getElementById('radio1').disabled = false;
                document.getElementById('radio2').disabled = false;
            },
            "Salir": function () {
                $("#div_seleccion").hide();
                $("#rtipodato").hide();
                $("#radio1").attr('checked', false);
                $("#radio2").attr('checked', false);
                cargarCombo('b_idespecificacion', [0]);
                jQuery('#valorespredeterminados_aso').jqGrid('clearGridData');
                document.getElementById('radio1').disabled = false;
                document.getElementById('radio2').disabled = false;
                $(this).dialog("destroy");


            }
        },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        }
    });

}

function guardarEspecificacionedit() {
    var grid = jQuery("#valorespredeterminados_aso")
            , nomesp = $('#nomEspecificacion').val()
            , idSubcategoria = $('#idProinterno').val()
            , descesp = $('#descEspecificacion').val()
            , sel = $('#tipodatos').val()
            , idtiposeleccion = "0"
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false, vara = 0,
            id = $('#idEsp').val();

    if ($('#idtiposeleccion').val() !== "0") {
        idtiposeleccion = sel;
    }


    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);
        grid.saveRow(filas[i]);
    }

    filas = grid.jqGrid('getRowData');

    if (nomesp === '' && vara === 0) {

        error = true;
        mensajesDelSistema('Ingrese el nombre de la especificacion', '300', 'auto', false);
        vara = 1;
    }

    if (idtiposeleccion === '' && vara === 0) {

        error = true;
        mensajesDelSistema('Debe escoger un Tipo de Ingreso', '300', 'auto', false);
        vara = 1;

    }

    if (filas.length === 0 && idtiposeleccion === "0" && vara === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
        vara = 1;
    }


    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 51, informacion: JSON.stringify({id: id, nomesp: nomesp, descesp: descesp, idtiposeleccion: idtiposeleccion, rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    //alert(json.mensaje);
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    $('#nomEspecificacion').val('');
                    $('#descEspecificacion').val('');
                    $("#div_seleccion").hide();
                    $('#idtiposeleccion').val('');
                    $("#rtipodato").hide();
                    $("#radio1").attr('checked', false);
                    $("#radio2").attr('checked', false);
                    cargarCombo('b_idespecificacion', [0]);
                    jQuery('#valorespredeterminados_aso').jqGrid('clearGridData');
                    refrescarGridUndNegocio(idSubcategoria);
                    $("#div_especificacion").dialog("destroy");

                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}


//este es el que hay que modificar para la creacion de los item select y digitar
function crearEspecificacion() {
    $('#nomEspecificacion').val('');
    $('#descEspecificacion').val('');
    $('#div_especificacion').fadeIn("slow");
    AbrirDivCrearEspecificacion();
    cargarCombo('b_idsubcategoria', []);
    ordenarCombo('b_idsubcategoria');
}

function AbrirDivCrearEspecificacion() {
    $("#div_especificacion").dialog({
        width: "auto",
        height: "auto",
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarEspecificacion();
            },
            "Salir": function () {
                $(this).dialog("destroy");
                $("#div_seleccion").hide();
                $("#rtipodato").hide();
                $("#radio1").attr('checked', false);
                $("#radio2").attr('checked', false);
                cargarCombo('b_idespecificacion', [0]);
                jQuery('#valorespredeterminados_aso').jqGrid('clearGridData');


            }
        },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        }
    });
}

function guardarEspecificacion() {
    var grid = jQuery("#valorespredeterminados_aso")
            , nomesp = $('#nomEspecificacion').val()
            , idSubcategoria = $('#idProinterno').val()
            , descesp = $('#descEspecificacion').val()
            , sel = $('#tipodatos').val()
            , idtiposeleccion = "0"
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false, vara = 0;

    if ($('#idtiposeleccion').val() !== "0") {
        idtiposeleccion = sel;
    }


    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);
        grid.saveRow(filas[i]);
    }

    filas = grid.jqGrid('getRowData');

    if (nomesp === '' && vara === 0) {

        error = true;
        mensajesDelSistema('Ingrese el nombre de la especificacion', '300', 'auto', false);
        vara = 1;
    }

    if (idtiposeleccion === '' && vara === 0) {

        error = true;
        mensajesDelSistema('Debe escoger un Tipo de Ingreso', '300', 'auto', false);
        vara = 1;

    }

    if (filas.length === 0 && idtiposeleccion === "0" && vara === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
        vara = 1;
    }


    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 33, informacion: JSON.stringify({nomesp: nomesp, descesp: descesp, idtiposeleccion: idtiposeleccion, rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    //alert(json.mensaje);
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    $('#nomEspecificacion').val('');
                    $('#descEspecificacion').val('');
                    $("#div_seleccion").hide();
                    $('#idtiposeleccion').val('');
                    $("#rtipodato").hide();
                    $("#radio1").attr('checked', false);
                    $("#radio2").attr('checked', false);
                    cargarCombo('b_idespecificacion', [0]);
                    jQuery('#valorespredeterminados_aso').jqGrid('clearGridData');
                    refrescarGridUndNegocio(idSubcategoria);
                    $("#div_especificacion").dialog("destroy");

                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}

function refrescarGridUndNegocio(id) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=6&idProceso=' + id;
    jQuery("#UndadesNegocios").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#UndadesNegocios').trigger("reloadGrid");
}

function actualizarProInterno() {
    var idProcesoMetaAct = $('#idMetaEdit').val();
    var idProinterno = $('#idProinterno').val();
    var nomProinterno = $('#nomProinterno').val();
    var descProinterno = $('#descProinterno').val();
    var idProMeta = $('#idProMetaEdit').val();
    var idSubcategoria = $('#idSubcategoria').val();
    if (idProMeta !== '' && nomProinterno !== '' && descProinterno !== '') {
        $.ajax({
            url: '/fintra/controlleropav?estado=Procesos&accion=Catalogo',
            datatype: 'json',
            type: 'post',
            data: {
                opcion: 9,
                idProinterno: idProinterno,
                nomProinterno: nomProinterno,
                descProinterno: descProinterno,
                idProMeta: idProMeta,
                idSubcategoria: idSubcategoria
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridProcesosInterno(idProcesoMetaAct);
                        mensajesDelSistema("El proceso se actualiz� correctamente!!", '250', '150', true);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar el proceso!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }

}

function desasociarUndProinterno() {
    var idSubcategoria = $('#idSubcategoria').val();
    var idProinterno = $('#idProinterno').val();
    var listado = "";
    var filasId = jQuery('#UndNegocioPro').jqGrid('getGridParam', 'selarrrow');
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        $.ajax({
            url: '/fintra/controlleropav?estado=Procesos&accion=Catalogo',
            datatype: 'json',
            type: 'post',
            data: {
                opcion: 10,
                listado: listado,
                idProinterno: idProinterno,
                idSubcategoria: idSubcategoria
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridUndNegocioPro(idProinterno);
                        refrescarGridUndNegocio(idProinterno);
                    } else if (json.respuesta === "OK1") {

                        mensajesDelSistema("No se puede desasociar especificaciones, existes datos asociados...", '250', '150');

                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo desasignar la especificacion", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#UndNegocioPro").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las especificaciones", '250', '150');
        } else {
            mensajesDelSistema("No hay categorias por desasignar", '250', '150');
        }
    }
}

function asociarUndProinterno() {
    var idSubcategoria = $('#idSubcategoria').val();
    var idProinterno = $('#idProinterno').val();
    var listado = "";
    var filasId = jQuery('#UndadesNegocios').jqGrid('getGridParam', 'selarrrow');
    var coma = "";
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            var ret = jQuery("#UndadesNegocios").jqGrid('getRowData', filasId[i]);
            //listado += filasId[i] + ",";
            listado += coma + ret.id + "-" + ret.obligatorio;
            coma = ",";
        }

        var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 7,
                listado: listado,
                idProinterno: idSubcategoria
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        $('#div_Subcategoria').fadeOut();
                        refrescarGridUndNegocioPro(idProinterno);
                        refrescarGridUndNegocio(idProinterno);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar las Especificaciones a la Subcategoria!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#UndadesNegocios").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las especificaciones a asociar", '250', '150');
        } else {
            mensajesDelSistema("No hay especificaciones por asociar", '250', '150');
        }
    }
}



function cargarComboCategorias() {
    $('#idProMeta').html('');
    $('#idProMetaEdit').html('');
    ;
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Catalogo',
        dataType: 'json',
        data: {
            opcion: 17
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#idProMeta').append("<option value=''>Seleccione</option>");
                    $('#idProMetaEdit').append("<option value=''>Seleccione</option>");
                    for (var key in json) {
                        $('#idProMeta').append('<option value=' + key + '>' + json[key] + '</option>');
                        $('#idProMetaEdit').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                $('#idProMeta').append("<option value=''>Seleccione</option>");
                $('#idProMetaEdit').append("<option value=''>Seleccione</option>");
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function asociarUsuarioProceso(id) {
    var nomProinterno = jQuery('#subcategoria').getCell(id, 'nombre');
    var metaProceso = jQuery('#subcategoria').getCell(id, 'descripcionTablaRel');
    var descProinterno = jQuery('#subcategoria').getCell(id, 'descripcion');
    $('#idProinternoAsoc').val(id);
    $('#nomProinternoAsoc').val(nomProinterno);
    $('#nommetaproceso').val(metaProceso);
    $('#descProinternoAsoc').val(descProinterno);
    $('#div_asoc_user_prointerno').fadeIn("slow");
    $('#div_user_prointerno').fadeIn();
    AbrirDivAsociarUsuarioProInterno();
    listarUsuariosProInterno(id);
    listarUsuariosRelProInterno();
}

function AbrirDivAsociarUsuarioProInterno() {
    $("#div_asoc_user_prointerno").dialog({
        width: 850,
        height: 675,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ASOCIAR USUARIOS A PROCESOS INTERNOS',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }/*,
         close: function(event, ui){ 
         $('#div_asoc_user_prointerno').fadeOut('slow');
         $('#div_usuarios').fadeOut();       
         $('#bt_asociar_userPro').hide();
         $('#bt_desasociar_userPro').hide();
         }*/
    });
}

function listarUsuariosProInterno(idProceso) {

    if ($("#gview_userProInterno").length) {
        reloadGridUsersProInterno(idProceso);
    } else {
        $("#userProInterno").jqGrid({
            caption: 'Usuarios relacionados con el proceso',
            url: '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=18&idProceso=' + idProceso,
            cellsubmit: "clientArray",
            datatype: 'json',
            height: 384,
            width: 'auto',
            cellEdit: true,
            // editurl: 'clientArray',
            colNames: ['C�digo', 'Id usuario', 'Nombre', 'Moderador'],
            colModel: [
                {name: 'codUsuario', index: 'codUsuario', hidden: true, sortable: true, align: 'center', width: '100px', key: true},
                {name: 'idusuario', index: 'idusuario', hidden: true, sortable: true, align: 'center', width: '100px'},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '250px'},
                {name: 'moderador', index: 'moderador', sortable: true, editable: true, align: 'center', width: '80px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            afterSaveCell: function (rowid, celname, value, iRow, iCol) {
                if (celname === 'moderador') {
                    var idProinterno = $('#idProinternoAsoc').val();
                    var idusuario = $("#userProInterno").getRowData(rowid).codUsuario;
                    var moderador = $("#userProInterno").getRowData(rowid).moderador;
                    if ((value.toUpperCase() !== "S" && value.toUpperCase() !== "N")) {

                        mensajesDelSistema("Especifique S o N, segun sea el caso", 250, 150);
                        $("#userProInterno").jqGrid("restoreCell", iRow, iCol);
                        return;
                    } else {
                        actualizarModerador(idProinterno, idusuario, moderador.toUpperCase());
                    }
                }
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                $('#bt_asociar_userPro').show();
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}

function reloadGridUsersProInterno(idProceso) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=18&idProceso=' + idProceso;
    jQuery("#userProInterno").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#userProInterno').trigger("reloadGrid");
}

function listarUsuariosRelProInterno() {
    var idProceso = $('#idProinternoAsoc').val();
    $('#div_usuarios').fadeIn();
    $('#bt_listar_userPro').hide();
    $('#bt_asociar_userPro').show();
    idsOfSelectedRows = [];
    if ($("#gview_listUsuarios").length) {
        reloadGridUsuariosRelProInterno(idProceso);
    } else {
        var cbColModel;
        jQuery("#listUsuarios").jqGrid({
            caption: 'Usuarios',
            url: '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=19&idProceso=' + idProceso,
            datatype: 'json',
            height: 368,
            width: 'auto',
            colNames: ['Cod Usuario', 'Nombre Usuario', 'Usuario Login'],
            colModel: [
                {name: 'codUsuario', index: 'codUsuario', hidden: true, align: 'center', width: '100px', key: true},
                {name: 'nombre', index: 'nombre', align: 'left', width: '300px'},
                {name: 'idusuario', index: 'idusuario', hidden: true, align: 'center', width: '200px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            ignoreCase: true,
            multiselect: true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            onSelectRow: function (id, isSelected) {
                var p = this.p, item = p.data[p._index[id]], i = $.inArray(id, idsOfSelectedRows);
                item.cb = isSelected;
                if (!isSelected && i >= 0) {
                    idsOfSelectedRows.splice(i, 1); // remove id from the list
                } else if (i < 0) {
                    idsOfSelectedRows.push(id);
                }
            },
            loadComplete: function () {
                var p = this.p, data = p.data, item, $this = $(this), index = p._index, rowid, i, selCount;
                for (i = 0, selCount = idsOfSelectedRows.length; i < selCount; i++) {
                    rowid = idsOfSelectedRows[i];
                    item = data[index[rowid]];
                    if ('cb' in item && item.cb) {
                        $this.jqGrid('setSelection', rowid, false);
                    }
                }
            },
            gridComplete: function () {
                $('#bt_desasociar_userPro').show();
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
        cbColModel = jQuery("#listUsuarios").jqGrid('getColProp', 'cb');
        cbColModel.sortable = true;
        cbColModel.sorttype = function (value, item) {
            return 'cb' in item && item.cb ? 1 : 0;
        };
        jQuery("#listUsuarios").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    }
}

function reloadGridUsuariosRelProInterno(idProceso) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=19&idProceso=' + idProceso;
    jQuery("#listUsuarios").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#listUsuarios").jqGrid('filterToolbar',
            {
                autosearch: true,
                searchOnEnter: true,
                defaultSearch: "cn",
                stringResult: true,
                ignoreCase: true,
                multipleSearch: true
            });
    jQuery('#listUsuarios').trigger("reloadGrid");
}

function asociarUsuariosProinterno() {

    var idProinterno = $('#idProinternoAsoc').val();
    var jsonObj = [];
    var filasId = jQuery('#listUsuarios').jqGrid('getGridParam', 'selarrrow');
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            var id = filasId[i];
            var j = $.inArray(id, idsOfSelectedRows);
            idsOfSelectedRows.splice(j, 1); // remove id from the list
            var idUsuario = jQuery("#listUsuarios").getRowData(id).idusuario;
            var item = {};
            item ["cod_usuario"] = id;
            item ["id_usuario"] = idUsuario;
            jsonObj.push(item);
        }
        var listUser = {};
        listUser ["usuarios"] = jsonObj;
        var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 20,
                listado: JSON.stringify(listUser),
                idProInterno: idProinterno
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        reloadGridUsersProInterno(idProinterno);
                        reloadGridUsuariosRelProInterno(idProinterno);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar los usuarios al proceso interno!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#listUsuarios").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar los usuarios a asociar al proceso interno!!", '250', '150');
        } else {
            mensajesDelSistema("No hay usuarios por asociar", '250', '150');
        }

    }
}



function desasociarUsuariosProinterno() {
    var idProinterno = $('#idProinternoAsoc').val();
    var listado = "";
    var filasId = jQuery('#userProInterno').jqGrid('getGridParam', 'selarrrow');
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        $.ajax({
            url: '/fintra/controlleropav?estado=Procesos&accion=Catalogo',
            datatype: 'json',
            type: 'post',
            data: {
                opcion: 21,
                listado: listado,
                idProinterno: idProinterno
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        reloadGridUsersProInterno(idProinterno);
                        reloadGridUsuariosRelProInterno(idProinterno);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo desasignar los usuarios del proceso interno!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#userProInterno").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar los usuarios a desasignar", '250', '150');
        } else {
            mensajesDelSistema("No hay usuarios por desasignar", '250', '150');
        }
    }


}

function actualizarModerador(idProinterno, idusuario, moderador) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 27,
            idProinterno: idProinterno,
            idusuario: idusuario,
            moderador: moderador
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    reloadGridUsersProInterno(idProinterno);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo actualizar informaci�n del moderador!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function existeUsuarioRelProcesoInterno(id) {
    $.ajax({
        url: '/fintra/controlleropav?estado=Procesos&accion=Catalogo',
        datatype: 'json',
        type: 'post',
        data: {
            opcion: 22,
            tipo: "PINI",
            idProceso: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.respuesta === "SI") {
                    mensajeConfirmAnulacion('ALERTA!!! Existen usuarios asociados al proceso interno, desea continuar?', '350', '165', id);
                } else {
                    anularProcesoInterno(id);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo realizar el proceso!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensajeConfirmAnulacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj").html("<span style='background: url(/fintra/images/warning03.png); height:32px; width:32px; float: left; margin: 0 7px 20px 0'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajeConfirmAction(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    mostrarContenido('dialogMsgMeta');
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}


function prueba(valor) {

    if (valor === '1') {
        redisenardivespecificaciones(1);
        cargardivseleccion();
        $('#idtiposeleccion').val(0);
    } else if (valor === '2') {
        redisenardivespecificaciones(2);
        cargardigitacion();
    }

}



function redisenardivespecificaciones(val) {

    if (val === 1) { //es seleccion
        $('#div_especificacion').css({
            'width': '670px',
            'height': '734px'

        });

    } else if (val === 2) {
        $('#div_especificacion').css({
            'height': '157px'
        });
    } else {
        $('#div_especificacion').css({
            'height': '600px'
        });
    }

}


function cargaritemseleccion() {

    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=38';
    if ($("#gview_procesosMeta").length) {
        refrescarGridProcesosMeta();
    } else {
        jQuery("#tablaseleccion").jqGrid({
            caption: 'Seleccion',
            url: url,
            datatype: 'json',
            height: 207,
            width: 300,
            colNames: ['Id', 'Nombre', 'Acciones'],
            colModel: [
                {name: 'iditemseleccion', index: 'iditemseleccion', sortable: true, align: 'center', width: '25%', key: true},
                {name: 'nombre', indeidunidadmedicionx: 'nombre', sortable: true, align: 'center', width: '45%'},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '30%'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            pager: '#page_tabla_selecion',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                var ids = jQuery("#tablaseleccion").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarProcesosMeta('" + cl + "');\">";
                    //proc_an = "<img src='/fintra/images/botones/iconos/detail1.png' align='absbottom'  name='proc_anul' id='proc_anul' width='15' height='15' title ='Ver procesos internos anulados'  onclick=\"verProcesosAnulados('" + cl + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el meta proceso seleccionado?','250','150',existeUsuarioRelMetaProceso,'" + cl + "');\">";
//                      an = "<img src='/fintra/images/botones/iconos/anular.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAnulacion('Alerta!!! Puede que existan usuarios asociados al Meta proceso, desea continuar?','350','165',anularMetaProceso,'" + cl + "');\">";
                    jQuery("#tablaseleccion").jqGrid('setRowData', ids[i], {actions: ed + '   ' + '   ' + an});
                }
                $('#div_editar_categoria').fadeIn("slow");
                var fila = jQuery("#Categorias").getRowData(cl);
                var nombre = fila['nombre'];
                var descripcion = fila['descripcion'];
                $('#idMetaEdit').val(cl);
                $('#nommetaEdit').val(nombre);
                $('#descmetaEdit').val(descripcion);
                AbrirDivEditarMetaProceso();
                cargarProcesosInternos();
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_selecion", {edit: false, add: false, del: false});
    }
}

function cargardigitacion() {
    $('#rtipodato').show();
    $("#div_seleccion").hide();
    $("#div_especificacion").dialog({
        position: {'my': 'center', 'at': 'center'}
    });
}

function guardarEspecificacion2() {
    var idSubcategoria = $('#idProinterno').val();
    var nomEspecificacion = $('#nomEspecificacion').val();
    var descEspecificacion = $('#descEspecificacion').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    if (idSubcategoria !== '' && nomEspecificacion !== '' && descEspecificacion !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 33,
                nomEspecificacion: nomEspecificacion,
                descEspecificacion: descEspecificacion
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        mensajesDelSistema("Se cre� Especificacion", '250', '150', true);
                        $('#nomEspecificacion').val('');
                        $('#descEspecificacion').val('');
                        refrescarGridUndNegocio(idSubcategoria);
                    }
                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear especificacion", '250', '150');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');
    }
}


function asociarvalorpredeterminado() {
    var idSubcategoria = $('#idSubcategoria').val();
    var idProinterno = $('#idProinterno').val();
    var listado = "";
    var filasId = jQuery('#UndadesNegocios').jqGrid('getGridParam', 'selarrrow');
    var coma = "";
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            var ret = jQuery("#UndadesNegocios").jqGrid('getRowData', filasId[i]);
            //listado += filasId[i] + ",";
            listado += coma + ret.id + "-" + ret.obligatorio;
            coma = ",";
        }

        var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 7,
                listado: listado,
                idProinterno: idSubcategoria
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        $('#div_Subcategoria').fadeOut();
                        refrescarGridUndNegocioPro(idProinterno);
                        refrescarGridUndNegocio(idProinterno);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar las Especificaciones a la Subcategoria!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#UndadesNegocios").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las especificaciones a asociar", '250', '150');
        } else {
            mensajesDelSistema("No hay especificaciones por asociar", '250', '150');
        }
    }
}

function desasociarvalorpredeterminado() {

    var idSubcategoria = $('#idSubcategoria').val();
    var idProinterno = $('#idProinterno').val();
    var listado = "";
    var filasId = jQuery('#UndNegocioPro').jqGrid('getGridParam', 'selarrrow');
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        $.ajax({
            url: '/fintra/controlleropav?estado=Procesos&accion=Catalogo',
            datatype: 'json',
            type: 'post',
            data: {
                opcion: 10,
                listado: listado,
                idProinterno: idProinterno,
                idSubcategoria: idSubcategoria
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridUndNegocioPro(idProinterno);
                        refrescarGridUndNegocio(idProinterno);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo desasignar la especificacion", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#UndNegocioPro").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las especificaciones", '250', '150');
        } else {
            mensajesDelSistema("No hay categorias por desasignar", '250', '150');
        }
    }
}

function cargardivseleccion() {
    listarvalorespredeterminados();
    listarvalorespredeterminadosesp();
    $('#rtipodato').hide();
    $('#div_valorespredeterminados').show();
    $('#div_valorespredeterminados_aso').show();
    $("#div_seleccion").show();
    $("#div_especificacion").dialog({
        position: {'my': 'center', 'at': 'center'}
    });
}

function listarvalorespredeterminados() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=38';
    //var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    if ($("#gview_valorespredeterminados").length) {
        refrescarGridvalorespredeterminados();
    } else {
        jQuery("#valorespredeterminados").jqGrid({
            caption: 'Valores Predeterminados',
            url: url,
            datatype: 'json',
            height: 280,
            width: 300,
            colNames: ['Id', 'Nombre', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, key: true},
                {name: 'valor_xdefecto', index: 'valor_xdefecto', sortable: true, editable: true, align: 'left', width: '600px'/*,
                 editoptions: {
                 dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                 try {
                 var rowid = e.target.id.replace("_valor_xdefecto","");
                 var valor = e.target.value.replace("_", "");
                 
                 jQuery("#valorespredeterminados").jqGrid('setCell', rowid, "descripcion", valor);
                 
                 } catch (exc) {
                 }
                 return;
                 }}]
                 }*/
                },
                {name: 'descripcion', index: 'descripcion', sortable: true, editable: true, align: 'left', width: '600px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_valorespredeterminados',
            multiselect: true,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {

                var ids = jQuery("#valorespredeterminados").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' align: 'center' height='15' title ='Editar'  onclick=\"pruebas('" + cl + "');\">";
                    jQuery("#valorespredeterminados").jqGrid('setRowData', ids[i], {actions: ed});
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_valorespredeterminados", {add: false, edit: false, del: false, search: false, refresh: false});
        jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
            caption: "Nuevo",
            title: "Agregar nueva fila",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {

                var grid = $("#valorespredeterminados")
                        , rowid = 'neo_' + grid.getRowData().length;
                //alert(rowid);
                grid.addRowData(rowid, {});
                grid.jqGrid('editRow', rowid, true, function () {
                    //  $("input, select", e.target).focus();
                }, null, null, {}, null, null, function (id) {
                    var g = $('#valorespredeterminados')
                            , r = g.jqGrid('getLocalRow', id);
                    if (!r.id) {
                        g.jqGrid('delRowData', id);
                    }
                });
            }
            //position: "first"
        });
        jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                guardarValp();
            }
            //position: "first"
        });
        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function guardarValp() {
    var grid = jQuery("#valorespredeterminados")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;
    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);
        if (data.valor_xdefecto === '') {
            error = true;
            mensajesDelSistema('Digite el valor', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else {
            if (data.descripcion === '') {

                error = true;
                mensajesDelSistema('Digite la descripcion', '300', 'auto', false);
                grid.jqGrid('editRow', filas[i], true, function () {
                    $("input, select", e.target).focus();
                });
                break;
            }

        }

        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }
    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 41, informacion: JSON.stringify({rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    //alert(json.mensaje);
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    refrescarGridvalorespredeterminados();
                    //refrescarGridvalorespredeterminados();
                    //AgregarGridAso();
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}

function refrescarGridvalorespredeterminados() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=38';
    jQuery("#valorespredeterminados").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#valorespredeterminados').trigger("reloadGrid");
}

function refrescarGridvalPredFiltro(id) {

    var ids = jQuery("#valorespredeterminados_aso").jqGrid('getDataIDs');
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=48&id=' + id + '&ids=' + ids;
    jQuery("#valorespredeterminados").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#valorespredeterminados').trigger("reloadGrid");
    //var ids = jQuery("#valorespredeterminados_aso").jqGrid('getDataIDs');
    //QuitarRowGridVal(ids);

}

function listarvalorespredeterminadosesp() {
    var idProInterno = $('#idProinterno').val();
    //var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=6&idProceso=' + idProInterno;
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    $('#div_valorespredeterminados_aso').fadeIn();
    $('#bt_listar_undPro').hide();
    $('#bt_asociar_valorpredeterminado').show();
    if ($("#gview_valorespredeterminados_aso").length) {
//refrescarGridUndNegocio(idProInterno);
    } else {
        jQuery("#valorespredeterminados_aso").jqGrid({
            caption: 'Valores Predeterminados',
            url: url,
            datatype: 'json',
            height: 329,
            width: 300,
            colNames: ['Id', 'Nombre', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden: true, align: 'center', width: '100px', key: true},
                {name: 'valor_xdefecto', index: 'valor_xdefecto', sortable: true, align: 'left', width: '600px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'left', width: '600px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            pager: '#page_tabla_valorespredeterminados_aso',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            gridComplete: function () {

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}

function ajaxSelect(sql) {
    var resultado;
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'get',
        data: {opcion: 40, informacion: JSON.stringify({query: sql, filtros: []})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '300', 'auto', false);
                    resultado = {};
                } else {
                    resultado = json;
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            console.log('error');
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
    return resultado;
}

function crearvalorpredeterminado() {
    $('#div_valpre').fadeIn('slow');
    AbrirDivCrearvalpre();
}

function AbrirDivCrearvalpre() {
    $("#div_valpre").dialog({
        width: 700,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR VALOR PREDETERMINADO',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarValorPredeterminado();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarValorPredeterminado() {
    var empresa = $('#nomempresa').val();
    var nomMeta = $('#nomvalorpredeterminado').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    if (empresa !== '' && nomMeta !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 41,
                nombre: nomMeta

            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {

                        cargarComboCategorias();
                        mensajesDelSistema("Se cre� el valor predeterminado", '250', '150', true);
                        $('#nomvalorpredeterminado').val('');
                        $('#descvalorpredeterminado').val('');
                        refrescarGridvalorespredeterminados();
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear el valor predeterminado!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }
}

function asociarespecificacionvalorpre(idProceso, idSubcategoria) {
    var idProinterno = idProceso;
    var listado = "";
    //var filasId = jQuery('#valorespredeterminados_aso').jqGrid('getGridParam', 'selarrrow');
    var filasId = jQuery("#valorespredeterminados_aso").jqGrid('getDataIDs');
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            var ret = jQuery("#valorespredeterminados_aso").jqGrid('getRowData', filasId[i]);
            //var ret = jQuery("#valorespredeterminados_aso").jqGrid('getDataIDs');
            listado += filasId[i] + ",";
        }

        var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&idProinterno=' + idProinterno;
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 42,
                listado: listado
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        $('#div_Subcategoria').fadeOut();
                        refrescarGridUndNegocioPro(idSubcategoria);
                        refrescarGridUndNegocio(idSubcategoria);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar las Especificaciones a la Subcategoria!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#UndadesNegocios").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las especificaciones a asociar", '250', '150');
        } else {
            mensajesDelSistema("No hay especificaciones por asociar", '250', '150');
        }
    }
}

function guardarvalordigitado() {
    var empresa = $('#nomempresa').val();
    var idinsumo = $('#idinsumo').val();
    var nomMeta = $('#nommeta').val();
    var descMeta = $('#descmeta').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo';
    if (empresa !== '' && nomMeta !== '' && descMeta !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 2,
                nombre: nomMeta,
                descripcion: descMeta,
                insumo: idinsumo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        //refrescarGridProcesosMeta();
                        cargarComboCategorias();
                        mensajesDelSistema("Se cre� la Categoria", '250', '150', true);
                        $('#nommeta').val('');
                        $('#descmeta').val('');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear la categoria!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }

}

function igualarcampo(campo, campo2) {
    var valor = $('#' + campo2).val();
    $('#' + campo).val(valor);
}

function AgregarGridAso() {

    var selectRows = jQuery("#valorespredeterminados").jqGrid('getGridParam', 'selarrrow');
    for (var i = 0; i < selectRows.length; i++) {

        var fila = jQuery("#valorespredeterminados").jqGrid('getRowData', selectRows[i]);
        jQuery("#valorespredeterminados_aso").jqGrid('addRowData', selectRows[i], fila);
    }
    QuitarRowGridVal(selectRows);
}


function QuitarRowGridVal(selectRows) {

    for (var k = 0; k < selectRows.length; k++) {
        jQuery("#valorespredeterminados").jqGrid('delRowData', selectRows[k]);

        selectRows = jQuery("#valorespredeterminados").jqGrid('getGridParam', 'selarrrow');


        k = -1;
    }

}


function AgregarGridNoAso() {

    var selectRows = jQuery("#valorespredeterminados_aso").jqGrid('getGridParam', 'selarrrow'), cadena = "", coma = "";
    for (var i = 0; i < selectRows.length; i++) {

        var fila = jQuery("#valorespredeterminados_aso").jqGrid('getRowData', selectRows[i]);
        //////////////////////////
        var fila1 = jQuery("#valorespredeterminados_aso").getRowData(selectRows[i]);

        if (fila1['idrel'] !== "") {
            cadena = cadena + coma + fila1['valor_xdefecto'];
            coma = ",";
        }

        //////////////////////////
        if (cadena === "") {
            jQuery("#valorespredeterminados").jqGrid('addRowData', selectRows[i], fila);
        }
    }

    if (cadena === "") {
        QuitarRowGridValAso(selectRows);
    } else {
        alert('No se pueden desasociar los valores predeterminados ' + cadena);
    }
}


function QuitarRowGridValAso(selectRows) {

    for (var k = 0; k < selectRows.length; k++) {
        jQuery("#valorespredeterminados_aso").jqGrid('delRowData', selectRows[k]);

        selectRows = jQuery("#valorespredeterminados_aso").jqGrid('getGridParam', 'selarrrow');


        k = -1;
    }

}

function crearTipoCategoria() {
    $('#nomcategoria').val('');
    $('#puntaje').val('');
    $('#div_tipo_categoria').fadeIn('slow');
    AbrirDivCrearTipoCategoria();
}

function AbrirDivCrearTipoCategoria() {
    $("#div_tipo_categoria").dialog({
        width: 480,
        height: 200,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR CATEGORIA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarTipoCategoria();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarTipoCategoria() {
    var nomCategoria = $('#nomcategoria').val();
    var puntaje = $('#puntaje').val();
    var url = './controlleropav?estado=Modulo&accion=Planeacion';
    if (nomCategoria !== '' && puntaje !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: false,
            data: {
                opcion: 60,
                nombre: nomCategoria,
                puntaje: puntaje
            },
            success: function (json) {
                //if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        cargarComboCateg();
                        mensajesDelSistema("Se creo la Categoria", '250', '150', true);
                        $('#nomcategoria').val('');
                        $('#puntaje').val('');
                    }

                /*} else {
                    mensajesDelSistema("Lo sentimos no se pudo crear la Categoria!!", '250', '150');
                }*/

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos obligatorios", '250', '150');
    }

}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}


function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function numbersonly(myfield, e, dec)
{
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

// control keys
    if ((key == null) || (key == 0) || (key == 8) ||
            (key == 9) || (key == 13) || (key == 27))
        return true;

// numbers
    else if ((("0123456789.").indexOf(keychar) > -1))
        return true;

// decimal point jump
    else if (dec && (keychar == "."))
    {
        myfield.form.elements[dec].focus();
        return false;
    }
    else
        return false;
}
function cargando_toggle() {
    $('#loader-wrapper').toggle();
}