/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    maximizarventana();
    listarCargasLaborales();
    cargando_toggle();


});

function cargar_cargos() {

    $('#cbx_Cargos').html('');

    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        dataType: 'json',
        async: false,
        data: {
            opcion: 17
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    toastr.error(json.error, "Error");

                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Cargos').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}


function listarCargasLaborales() {
    var grid_tabla = jQuery("#tbl_ingreso_carga_laboral");
    if ($("#gview_tbl_ingreso_carga_laboral").length) {
        refrescarGridCargaLaboral();
    } else {
        grid_tabla.jqGrid({
            caption: "Selectrik",
            url: "/fintra/controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '400',
            width: '1000',
            cellEdit: true,
            colNames: ['Id', 'id_cargo', 'Cargo', 'Carga Real', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 10, align: 'left', key: true, hidden: true},
                {name: 'id_cargo', index: 'id_cargo', width: 10, align: 'left', hidden: true},
                {name: 'descripcion_cargo', index: 'descripcion_cargo', width: 50, align: 'left'},
                {name: 'carga_real', index: 'carga_real', sortable: true, editable: false, width: '20', align: 'center', search: false, sorttype: 'number',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "% "}},
                {name: 'actions', index: 'actions', width: 20, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#pager_tbl_ingreso_carga_laboral'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 76,
                    id_solicitud: $('#id_solicitud').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var ids = jQuery("#tbl_ingreso_carga_laboral").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarasignacion_puntaje('" + cl + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion('Esta seguro de anular este item?','250','150',anular_Asignacion,'" + cl + "');\">";
                    jQuery("#tbl_ingreso_carga_laboral").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                }


            },
            ondblClickRow: function (rowid, iRow, iCol, e) {


            }
        }).navGrid("#pager_tbl_ingreso_carga_laboral", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tbl_ingreso_carga_laboral").jqGrid("navButtonAdd", "#pager_tbl_ingreso_carga_laboral", {
            caption: "Nuevo",
            onClickButton: function () {
                asignacion_puntaje();
            }
        });
    }

}

function refrescarGridCargaLaboral() {
    jQuery("#tbl_ingreso_carga_laboral").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 76,
                id_solicitud: $('#id_solicitud').val()
            }
        }
    });

    jQuery('#tbl_ingreso_carga_laboral').trigger("reloadGrid");
}


function asignacion_puntaje() {
    $('#div_Control').fadeIn('slow');
    $('#id_ind_carga_laboral').val('');
    cargar_cargos();
    listarinfomacion_actividades();
    listarCargasLaborales_detalle();
    AbrirDivasignacion_puntaje();
    $('#cbx_Cargos').prop('disabled', false);
}


function listarinfomacion_actividades() {
    $('#tbl_informacion').jqGrid('GridUnload');

    var grid_tabla = jQuery("#tbl_informacion");

    grid_tabla.jqGrid({
        caption: "Selectrik - Informacion",
        url: "/fintra/controlleropav?estado=Modulo&accion=Planeacion",
        datatype: "json",
        height: '250',
        width: '700',
        cellEdit: true,
        colNames: ['id', 'id_actividad', 'Actividad', 'Nivel', 'Rango Incial', 'Rango Final', 'id_unidad_medida', 'Unidad de Medida'],
        colModel: [
            {name: 'id', index: 'id', width: 10, align: 'left', key: true, hidden: true},
            {name: 'id_actividad', index: 'id_actividad', width: 10, align: 'left', key: true, hidden: true},
            {name: 'nombre_actividad', index: 'descripcion_actividad', width: 25, align: 'left', hidden: false},
            {name: 'nivel', index: 'nivel', width: 25, align: 'left'},
            {name: 'rango_ini', index: 'rango_ini', width: 15, align: 'left'},
            {name: 'rango_fin', index: 'rango_fin', width: 15, align: 'center'},
            {name: 'id_unidad_medida_general', index: 'id_unidad_medida_general', width: 20, align: 'center'},
            {name: 'nombre_unidad', index: 'nombre_unidad', width: 20, align: 'center'}
        ],
        rowNum: 1000,
        rowTotal: 1000,
        pager: ('#pager_tbl_informacion'),
        loadonce: true,
        rownumWidth: 30,
        gridview: true,
        viewrecords: true,
        hidegrid: true,
        hiddengrid: true,
        shrinkToFit: true,
        footerrow: false,
        rownumbers: true,
        pgtext: null,
        pgbuttons: false,
        //multiselect: true,
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 77
            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 200);
        },
        gridComplete: function (index) {
//                var ids = jQuery("#tbl_ingreso_carga_laboral").jqGrid('getDataIDs');
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarConcepto('" + cl + "');\">";
//                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion('Esta seguro de anular el Concepto?','250','150',anular_Asignacion,'" + cl + "');\">";
//                    jQuery("#tbl_ingreso_carga_laboral").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
//                }


        },
        ondblClickRow: function (rowid, iRow, iCol, e) {


        }
    }).navGrid("#pager_tbl_informacion", {add: false, edit: false, del: false, search: false, refresh: false}, {});


}




function listarCargasLaborales_detalle() {
    var grid_tabla = jQuery("#tbl_ingreso_carga_laboral_detalle");
    if ($("#gview_tbl_ingreso_carga_laboral_detalle").length) {
        refrescarGridCargaLaboral_detalle();
    } else {
        grid_tabla.jqGrid({
            caption: "Selectrik",
            url: "/fintra/controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '250',
            width: '700',
            colNames: ['Id', 'id_cargo', 'id_actividad', 'Actividad', 'Peso', 'Puntaje', 'Carga Real'],
            colModel: [
                {name: 'id', index: 'id', width: 10, align: 'left', key: true, hidden: true},
                {name: 'id_cargos', index: 'id_cargo', width: 10, align: 'left', hidden: true},
                {name: 'id_actividad', index: 'id_actividad', width: 50, align: 'left', hidden: true},
                {name: 'nombre_actividad', index: 'nombre_actividad', width: 30, align: 'left'},
                {name: 'peso', index: 'peso', width: 20, align: 'center'},
                    {name: 'puntaje', index: 'puntaje', sortable: true, editable: true, width: '20', align: 'center', search: false, sorttype: 'number',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "% "},
                        editoptions: {dataInit: function (elem) {
                                $(elem).keypress(function (e) {
                                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                        return false;
                                    }
                                });
                            }
                        }
                    },
                {name: 'carga_real', index: 'carga_real', width: 20, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#pager_tbl_ingreso_carga_laboral_detalle'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            editurl: 'clientArray',
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 78,
                    id_ind_carga_laboral: $('#id_ind_carga_laboral').val(),
                    id_cargo: $('#cbx_Cargos').val()

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
//                var ids = jQuery("#tbl_ingreso_carga_laboral").jqGrid('getDataIDs');
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarConcepto('" + cl + "');\">";
//                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion('Esta seguro de anular el Concepto?','250','150',anular_Asignacion,'" + cl + "');\">";
//                    jQuery("#tbl_ingreso_carga_laboral").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
//                }


            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                grid_tabla.jqGrid('editRow', rowid, true, function () {
                }, null, null, {}, function (rowid) {

                    var fila = grid_tabla.getRowData(rowid);
                    grid_tabla.jqGrid('setCell', rowid, 'carga_real', fila.peso * fila.puntaje / 100);



                });
                return;

            }
        }).navGrid("#tbl_ingreso_carga_laboral_detalle", {add: false, edit: false, del: false, search: false, refresh: false}, {});

    }

}

function refrescarGridCargaLaboral_detalle() {
    jQuery("#tbl_ingreso_carga_laboral_detalle").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 78,
                id_ind_carga_laboral: $('#id_ind_carga_laboral').val(),
                id_cargo: $('#cbx_Cargos').val()
            }
        }
    });

    jQuery('#tbl_ingreso_carga_laboral_detalle').trigger("reloadGrid");
}



function AbrirDivasignacion_puntaje() {
    $("#div_Control").dialog({
        width: '800',
        height: '550',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Asginacion de Puntaje',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardarasignacion_puntaje();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}



function editarasignacion_puntaje(cl) {
    alert(cl);
    $('#div_Control').fadeIn('slow');
    $('#id_ind_carga_laboral').val(cl);
    cargar_cargos();
    listarinfomacion_actividades();
    listarCargasLaborales_detalle();
    AbrirDivEditarConcepto();
    $('#cbx_Cargos').prop('disabled', true);
    $("#cbx_Cargos").val($("#tbl_ingreso_carga_laboral").jqGrid('getCell',cl , 'id_cargo')); 
    listarCargasLaborales_detalle();
}

function AbrirDivEditarConcepto() {

    $("#div_Control").dialog({
        width: '800',
        height: '550',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Asginacion de Puntaje',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarasignacion_puntaje();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}


function guardarasignacion_puntaje() {
    var cargo = $('#cbx_Cargos').val();
    var id_solicitud = $('#id_solicitud').val();
    var datosGrillas = $('#tbl_ingreso_carga_laboral_detalle').jqGrid('getRowData');

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            opcion: ($('#id_ind_carga_laboral').val() === '') ? 79 : 80,
            id_solicitud: id_solicitud,
            id: $('#id_ind_carga_laboral').val(),
            cargo: cargo,
            datosGrillas: JSON.stringify({datosGrillas: datosGrillas})
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridCargaLaboral();
                    $("#div_Control").dialog('close');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo guardar el Concepto!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function anular_Asignacion(cl) {

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            opcion: 81,
            id: cl

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {

                    refrescarGridCargaLaboral();
                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo Anular el Concepto!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

/**********************************************************************************************************************************************************
 Utiles Generales
 ***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon', '#perc_rete'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}


function mensajeConfirmacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}
/**********************************************************************************************************************************************************
 Fin Utiles Generales
 ***********************************************************************************************************************************************************/