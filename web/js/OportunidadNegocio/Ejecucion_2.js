/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    maximizarventana();
    inicializar();
    cargarComboGenerico('area', 1, $('#id_solicitud').val());
    
    $('#area').change(function () {
        limpiarCombos(1);
        cargarComboGenerico('disciplina', 2, $('#area').val());

    });

    $('#disciplina').change(function () {
        limpiarCombos(2);
        cargarComboGenerico('capitulo', 3, $('#disciplina').val());

    });

    $('#capitulo').change(function () {
        limpiarCombos(3);
        cargarComboGenerico('actividad', 4, $('#capitulo').val());

    });
    
    $('#area_').change(function () {
        limpiarCombos(1);
        cargarComboGenerico('disciplina_', 2, $('#area_').val());

    });

    $('#disciplina_').change(function () {
        limpiarCombos(2);
        cargarComboGenerico('capitulo_', 3, $('#disciplina_').val());

    });

    $('#capitulo_').change(function () {
        limpiarCombos(3);
        cargarComboGenerico('actividad_', 4, $('#capitulo_').val());

    });
    cargando_toggle();
    $('.fecha_actual').val(fecha());
    eventos();
    cargarComboGrupo1([]);

});

function  filtro_insumo(buscar) {
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    jQuery("#tbl_filtro_insumos").setGridParam({
        datatype: 'json',
        url: url,
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: 74,
                buscar: buscar,
                categoria: $('#categoria').val(),
                subcategoria: $('#sub').val(),
                id_tipo_insumo: $('#tipo_insumo').val()

            }
        }

    });
    jQuery('#tbl_filtro_insumos').trigger("reloadGrid");
}

function eventos() {
    $('#btn_filtro_apu').on('click', function () {
        filtro_apu($('#filtro_nom_apu').val());
    });
    $('#filtro_nom_apu').keypress(function (e) {
        if (e.which == 13) {
            filtro_apu($('#filtro_nom_apu').val());
        }
    });
    $('#btn_filtro_insumo').on('click', function () {
        filtro_insumo($('#filtro_insumo').val());
    });

    $('#filtro_insumo').keypress(function (e) {
        if (e.which == 13) {
            filtro_insumo($('#filtro_insumo').val());
        }
    });
    $("#buscar").click(function () {
        filtro_insumo($('#filtro_insumo').val());
    });
}



function inicializar() {
    $('#id_lote').val('');
    limpiarCombos(1);
    menu(1);
}

function limpiarCombos(x) {

    switch (x) {
        case 0:
            cargarComboGenerico('area', 1, $('#id_solicitud').val());
            $("#disciplina").html('');
            $("#capitulo").html('');
            $("#actividad").html('');
            $("#disciplina").append('<option value="">' + "Todos" + '</option>');
            $("#capitulo").append('<option value="">' + "Todos" + '</option>');
            $("#actividad").append('<option value="">' + "Todos" + '</option>');

        case 1:
            $("#disciplina").html('');
            $("#capitulo").html('');
            $("#actividad").html('');
            $("#disciplina").append('<option value="">' + "Todos" + '</option>');
            $("#capitulo").append('<option value="">' + "Todos" + '</option>');
            $("#actividad").append('<option value="">' + "Todos" + '</option>');

            break;
        case 2:


            $("#capitulo").html('');
            $("#actividad").html('');
            $("#capitulo").append('<option value="">' + "Todos" + '</option>');
            $("#actividad").append('<option value="">' + "Todos" + '</option>')
            break;

        case 3:

            $("#actividad").html('');
            $("#actividad").append('<option value="">' + "Todos" + '</option>');
            break;

    }
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

}

function limpiarCombosEje(x) {

    switch (x) {
        case 0:
            cargarComboGenerico('area_', 1, $('#id_solicitud').val());
            $("#disciplina_").html('');
            $("#capitulo_").html('');
            $("#actividad_").html('');
            $("#disciplina_").append('<option value="">' + "Todos" + '</option>');
            $("#capitulo_").append('<option value="">' + "Todos" + '</option>');
            $("#actividad_").append('<option value="">' + "Todos" + '</option>');

        case 1:
            $("#disciplina_").html('');
            $("#capitulo_").html('');
            $("#actividad_").html('');
            $("#disciplina_").append('<option value="">' + "Todos" + '</option>');
            $("#capitulo_").append('<option value="">' + "Todos" + '</option>');
            $("#actividad_").append('<option value="">' + "Todos" + '</option>');

            break;
        case 2:


            $("#capitulo_").html('');
            $("#actividad_").html('');
            $("#capitulo_").append('<option value="">' + "Todos" + '</option>');
            $("#actividad_").append('<option value="">' + "Todos" + '</option>')
            break;

        case 3:

            $("#actividad").html('');
            $("#actividad_").append('<option value="">' + "Todos" + '</option>');
            break;

    }
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $('#txt_cantidad_apu_eje').val(0);

}

function menu(x) {

    $('#id_proceso').val(x);
    $(".tab-pane ").animate({left: '500px'});



    $("#div_principal").fadeOut("slow");
    $("#div_General").fadeOut("slow");
    $("#div_registro_ejecucion").fadeOut("slow");
    $("#div_actas").fadeOut("slow");
    $("#div_actas_detalle").fadeOut("slow");

    $("#div_principal_").removeClass('active');
    $("#div_General_").removeClass('active');
    $("#div_registro_ejecucion_").removeClass('active');
    $("#div_actas_").removeClass('active');
    $("#div_actas_detalle_").removeClass('active');

    $("#div_principal_").removeClass('active');
    $("#div_General").removeClass('active');
    $("#div_registro_ejecucion").removeClass('active');
    $("#div_actas").removeClass('active');
    $("#div_actas_detalle").removeClass('active');


    if (x == 2 || x == 3 || x == 0) {
        $('#div_busqueda').fadeOut("slow")
    } else {
        $('#div_busqueda').fadeIn('slow')
    }
    ;
    setTimeout(function () {

        switch (x) {

            case 0:
                $("#div_principal_").addClass('active');
                $("#div_principal").addClass('active');
                $("#div_principal").fadeIn("slow");
                cargar_lotes();
                break;
            case 1:
                $("#div_General_").addClass('active');
                $("#div_General").addClass('active');
                $("#div_General").fadeIn("slow");

                break;
            case 2:
                $("#div_registro_ejecucion_").addClass('active');
                $("#div_registro_ejecucion").addClass('active');

                $("#div_registro_ejecucion").fadeIn("slow");
                grid_registro_ejecucion(2, 0, '');

                break;
            case 3:

                $("#div_actas_").addClass('active');
                $("#div_actas").addClass('active');
                $("#div_actas").fadeIn("slow");

                break;
            case 4:

                $("#div_actas_detalle_").addClass('active');
                $("#div_actas_detalle").addClass('active');
                $("#div_actas_detalle").fadeIn("slow");


                break;
        }

        $(".tab-pane ").animate({left: '0'});
    }, 1000);

}

function cargarComboGenerico(idCombo, op, param) {
    if (param == '') {
        limpiarCombos(op);
    } else {
        var elemento = $('#' + idCombo);
        $.ajax({
            url: "/fintra/controlleropav?estado=Modulo&accion=Ejecucion",
            datatype: 'json',
            type: 'GET',
            data: {opcion: 1, op: op, param: param},
            async: false,
            success: function (json) {
                try {
                    if (json.error) {
                        mensajesDelSistema(json.error, '300', 'auto', false);
                    } else {
                        elemento.html('');
                        elemento.append('<option value="">' + "Todos" + '</option>');
                        for (var e in json) {
                            elemento.append('<option value="' + e + '" >' + json[e] + '</option>');
                        }
                    }
                } finally {
                }
            }
        });
    }
}


function cargarComboGenerico2(idCombo, op, param) {

    var elemento = $('#' + idCombo);
    $.ajax({
        url: "/fintra/controlleropav?estado=Modulo&accion=Ejecucion",
        datatype: 'json',
        type: 'GET',
        data: {opcion: 1, op: op, param: param},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '" >' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function cargarComboTipoInsumo() {

    var elemento = $('#' + idCombo);
    $.ajax({
        url: "/fintra/controlleropav?estado=Modulo&accion=Ejecucion",
        datatype: 'json',
        type: 'GET',
        data: {opcion: 1, op: op, param: param},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    elemento.append('<option value="">' + "Todos" + '</option>');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '" >' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function cargarContenidoProyecto() {


    var opc, cond = '', proceso = $('#id_proceso').val();
    if ($('#area').val() == '') {
        opc = 0;
    } else if ($('#disciplina').val() === '') {
        opc = 1;
        cond = $('#area').val();
    } else if ($('#capitulo').val() === '') {
        opc = 2;
        cond = $('#disciplina').val();
    } else if ($('#actividad').val() === '') {
        opc = 3;
        cond = $('#capitulo').val();
    } else {
        opc = 4;
        cond = $('#actividad').val();
    }

    switch ($("#id_proceso").val()) {
        case "1":
            grid_general(proceso, opc, cond);
            break;
        case "2":
            grid_registro_ejecucion(proceso, opc, cond);
            break;

        case "3":
            grid_actas(proceso, opc, cond)
            break;

    }

}



function cargar_lotes() {
    $('#tbl_lote').jqGrid('GridUnload');

    var grid_tabla = jQuery("#tbl_lote");
    grid_tabla.jqGrid({
        caption: "Selectrik - Informe Diario De Avance",
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        datatype: "json",
        height: '350',
        width: '700',
        colNames: ['Id', 'Numero De Informe', 'Descripcion', 'Fecha Ingreso', 'Acciones'],
        colModel: [
            {name: 'id', index: 'id', width: 10, sortable: true, align: 'left', hidden: true, search: true, key: true},
            {name: 'no_lote', index: 'no_lote', width: 10, sortable: true, align: 'center', hidden: false, search: true},
            {name: 'descripcion', index: 'descripcion', width: 30, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'creation_date', index: 'creation_date', width: 10, sortable: true, align: 'center', hidden: false, search: true},
            {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 10, search: false, hidden: true}

        ],
        rowNum: 1000000,
        rowTotal: 1000000,
        loadonce: true,
        rownumWidth: 60,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: false,
//        firstsortorder : 'no_lote',
//        sortname:"no_lote",
//        sortorder : 'desc',
        rownumbers: false,
        pager: '#pager_tbl_lote',
        multiselect: false,
        multiboxonly: false,
        pgtext: null,
        pgbuttons: false,
        editurl: 'clientArray',
        subGrid: false,
        ondblClickRow: function (rowid, iCol, e) {
            limpiarCombos(0);
            editar_lote(rowid);
            //$('#id_tabla_ejecucion').val(rowid);



//                jQuery("#tabla_ejecucion").jqGrid('editRow', rowid, true, function () {
//                }, null, null, {}, function (rowid) {
//
//
//                    var id = $("#tabla_ejecucion").getRowData(rowid).id;
//                    var cantidad_apu_actual = $("#tabla_ejecucion").getRowData(rowid).cantidad_apu_actual;
//                    var valor_apu_presupuesto = $("#tabla_ejecucion").getRowData(rowid).valor_apu_presupuesto;
//
//                    grid_tabla.jqGrid('setCell', rowid, "valor_apu_actual", cantidad_apu_actual * valor_apu_presupuesto);
////                    cargando_toggle();
////                    insertar_rentabilidad_contratista_global(id, porcentaje);
////                    actualizarGrillas();
////                    cargando_toggle();
//
//
//
//                });
//                return;
        }, gridComplete: function () {
//                var ids = grid_tabla.jqGrid('getDataIDs');
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    ed = "<img src='/fintra/images/link_new.png' align='absbottom'  name='editar' id='editar' width='16' height='16' title ='editar cantidades'  onclick=\"editar_cantidades('" + cl + "');\">";
////                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='16' height='16' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan datos asociados a la subcategoria, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
//                    grid_tabla.jqGrid('setRowData', ids[i], {actions: ed});
//                }

        },
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 10,
                id_solicitud: $('#id_solicitud').val()

            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 150);
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                mensajesDelSistema("No se encontraron registros", '204', '140', false);
            }
        }

    }).navGrid("#pager_tbl_lote", {add: false, edit: false, del: false, search: false, refresh: false}, {
    });
    grid_tabla.navButtonAdd('#pager_tbl_lote', {
        caption: "Nuevo.",
        title: "Nuevo Informe Diario De Avance",
        buttonicon: "ui-icon-save",
        onClickButton: function () {
            $('#id_lote').val('');
            limpiarCombos(1);
            nuevo_lote();
            menu(1);
        }

    });


}




function nuevo_lote() {
    cargando_toggle();
    $('#tabla_ejecucion').jqGrid('GridUnload');
    $.ajax({
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        datatype: 'json',
        type: 'POST',
        data: {
            opcion: 16,
            id_solicitud: $('#id_solicitud').val()
        },
        async: false,
        success: function (json) {
            try {

                if (json.error) {
                    mensajesDelSistema1(json.error, '204', '140');
                } else {
                    if (json.respuesta == "OK") {
                        //console.log('valores seteados a 0');
                        limpiarCombos(1);

                    }


                }
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {

        }
    });
    cargando_toggle();
}



function guardar_lote() {
    cargando_toggle();
    var filas = $('#tabla_ejecucion_1').jqGrid('getRowData');
    $.ajax({
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        datatype: 'json',
        type: 'POST',
        data: {
            opcion: $('#id_lote').val() == "" ? 14 : 15,
            id_solicitud: $('#id_solicitud').val(),
            id_lote: $('#id_lote').val(),
            informacion: JSON.stringify({rows: filas}),
            descripcion: $('#txt_descripcion_registro_ejecucion').val()


        },
        async: false,
        success: function (json) {
            try {

                if (json.error) {
                    mensajesDelSistema1(json.error, '204', '140');
                } else {
                    if (json.respuesta == "OK") {
                        $('#tabla_ejecucion').jqGrid('GridUnload');
                        menu(0);
                        limpiarCombos(1);

                    }


                }
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {

        }
    });
    cargando_toggle();
}


function editar_lote(rowid) {
    cargando_toggle();
    $('#id_lote').val(rowid);
    $('#txt_descripcion_registro_ejecucion').val('');

    $.ajax({
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        datatype: 'json',
        type: 'POST',
        data: {
            opcion: 11,
            id_solicitud: $('#id_solicitud').val(),
            id_lote: $('#id_lote').val()
        },
        async: false,
        success: function (json) {
            try {

                if (json.error) {
                    mensajesDelSistema1(json.error, '204', '140');
                } else {
                    if (json.respuesta == "OK") {
                        $('#tabla_ejecucion').jqGrid('GridUnload');
                        menu(1);

                    }


                }
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {

        }
    });
    cargando_toggle();
}

function cargar_actas() {
    $('#tbl_actas').jqGrid('GridUnload');

    var grid_tabla = jQuery("#tbl_actas");
    grid_tabla.jqGrid({
        caption: "Selectrik - -Informe Diario De Avance",
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        datatype: "json",
        height: '350',
        width: '700',
        colNames: ['Id', 'Numero De Informe', 'Descripcion', 'Fecha Ingreso', 'Acciones'],
        colModel: [
            {name: 'id', index: 'id', width: 10, sortable: true, align: 'left', hidden: true, search: true, key: true},
            {name: 'no_lote', index: 'id_disciplina_area', width: 10, sortable: true, align: 'center', hidden: false, search: true},
            {name: 'descripcion', index: 'descripcion', width: 30, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'creation_date', index: 'creation_date', width: 10, sortable: true, align: 'center', hidden: false, search: true},
            {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 10, search: false, hidden: true}

        ],
        rowNum: 1000000,
        rowTotal: 1000000,
        loadonce: true,
        rownumWidth: 60,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: false,
        rownumbers: false,
        pager: '#pager_tbl_actas',
        multiselect: true,
        multiboxonly: false,
        pgtext: null,
        pgbuttons: false,
        editurl: 'clientArray',
        subGrid: false,
        ondblClickRow: function (rowid, iCol, e) {
            limpiarCombos(0);
            editar_lote(rowid);

        }, gridComplete: function () {

//                var ids = grid_tabla.jqGrid('getDataIDs');
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    ed = "<img src='/fintra/images/link_new.png' align='absbottom'  name='editar' id='editar' width='16' height='16' title ='editar cantidades'  onclick=\"editar_cantidades('" + cl + "');\">";
////                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='16' height='16' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan datos asociados a la subcategoria, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
//                    grid_tabla.jqGrid('setRowData', ids[i], {actions: ed});
//                }

        },
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 10,
                id_solicitud: $('#id_solicitud').val()

            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 150);
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                mensajesDelSistema("No se encontraron registros", '204', '140', false);
            }
        }

    }).navGrid("#pager_tbl_actas", {add: false, edit: false, del: false, search: false, refresh: false}, {
    });
    grid_tabla.navButtonAdd('#pager_tbl_actas', {
        caption: "Imprimir Actas.",
        title: "Imprimir Actas.",
        buttonicon: "ui-icon-save",
        onClickButton: function () {

            AbrirDivImpresiones();
            /*
             cargando_toggle();
             var datos = grid_tabla.jqGrid('getGridParam', 'selarrrow')
             , fila;
             var size = datos.length;
             concadid = "", coma = '';
             for (var i = 0; i < size; i++) {
             fila = datos[i];
             concadid = concadid + coma + fila;
             coma = ',';
             }
             
             if (concadid !== "") {
             console.log(concadid);
             imprimir_actas(concadid);
             } else {
             console.log("esta vacia");
             }
             
             
             cargando_toggle();
             */
        }

    });


}

function imprimir_actas(ids_actas) {

    $.ajax({
        type: "POST",
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        dataType: "json",
        data: {
            opcion: 18,
            id_solicitud: $('#id_solicitud').val(),
            ids_actas: ids_actas,
            opc_impresion : $("#cbx_impresion").val()
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    window.open('.' + json.Ruta);
                }
            } else {
                mensajesDelSistema("Lo sentimos ocurri� un error al generar cotizaci�n!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

//grilla General
function grid_general(proceso, opc, cond) {

    $('#tabla_ejecucion').jqGrid('GridUnload');

    var grid_tabla = jQuery("#tabla_ejecucion");
    grid_tabla.jqGrid({
        caption: "Proyecto - Selectrick",
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        datatype: "json",
        height: '550',
        width: '1590',
        colNames: ['id', 'id_disciplina_area', 'Area', 'id_disciplina', 'Displina', 'id_capitulo', 'Capitulo', 'id_actividad', 'Actividad',
            'id_actividades_capitulo', 'id_rel_actividades_apu', 'id_apu', 'APU', 'unidad_medida_apu', 'Unidad Medida', 'Cant. APU',
            'Valor APU', 'Total APU', 'Cantidad APU', 'Valor APU', 'Cantidad APU', 'Valor APU', 'actions', 'edito'],
        colModel: [
            {name: 'id', index: 'id', width: 10, sortable: true, align: 'left', hidden: true, search: true, key: true},
            {name: 'id_disciplina_area', index: 'id_disciplina_area', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_area', index: 'descripcion_area', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_disciplina', index: 'id_disciplina', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_displina', index: 'descripcion_displina', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_capitulo', index: 'id_capitulo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_capitulo', index: 'descripcion_capitulo', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_actividad', index: 'id_actividad', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_actividad', index: 'descripcion_actividad', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_actividades_capitulo', index: 'id_actividades_capitulo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'id_rel_actividades_apu', index: 'id_rel_actividades_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'id_apu', index: 'id_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_apu', index: 'descripcion_apu', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'unidad_medida_apu', index: 'unidad_medida_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'nombre_unidad_medida_apu', index: 'nombre_unidad_medida_apu', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'cantidad_apu', index: 'cantidad_apu', width: 5, sortable: true, align: 'right', hidden: false, search: true, sorttype: 'numeric'},
            {name: 'valor_apu_presupuesto', index: 'valor_apu_presupuesto', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'total_apu_presupuesto', index: 'total_apu_presupuesto', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$"}},
            {name: 'cantidad_apu_actual', index: 'cantidad_apu_actual', sortable: true, editable: true, width: '10', align: 'right', search: false, sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""},
                editoptions: {dataInit: function (elem) {
                        $(elem).keypress(function (e) {
                            if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
                                return false;
                            }
                        });
                    }
                }
            },
            {name: 'valor_apu_actual', index: 'valor_apu_actual', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'cantidad_apu_ejecutado', index: 'cantidad_apu_ejecutado', width: 10, sortable: true, align: 'right', hidden: false, search: true},
            {name: 'valor_apu_ejecutado', index: 'valor_apu_ejecutado', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 10, search: false},
            {name: 'edito', index: 'edito', width: 10, sortable: true, align: 'left', hidden: true, search: true}
        ],
        rowNum: 1000000,
        rowTotal: 1000000,
        loadonce: true,
        rownumWidth: 60,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: false,
        rownumbers: false,
        pager: '#pager_tabla_ejecucion',
        multiselect: false,
        multiboxonly: false,
        pgtext: null,
        pgbuttons: false,
        editurl: 'clientArray',
        subGrid: false,
        ondblClickRow: function (rowid, iCol, e) {
            $('#opcion_').val(1);
            $('#id_tabla_ejecucion').val(rowid);
            editar_Cantidades_Actual(rowid, 1);




//                jQuery("#tabla_ejecucion").jqGrid('editRow', rowid, true, function () {
//                }, null, null, {}, function (rowid) {
//
//
//                    var id = $("#tabla_ejecucion").getRowData(rowid).id;
//                    var cantidad_apu_actual = $("#tabla_ejecucion").getRowData(rowid).cantidad_apu_actual;
//                    var valor_apu_presupuesto = $("#tabla_ejecucion").getRowData(rowid).valor_apu_presupuesto;
//
//                    grid_tabla.jqGrid('setCell', rowid, "valor_apu_actual", cantidad_apu_actual * valor_apu_presupuesto);
////                    cargando_toggle();
////                    insertar_rentabilidad_contratista_global(id, porcentaje);
////                    actualizarGrillas();
////                    cargando_toggle();
//
//
//
//                });
//                return;
        }, gridComplete: function () {
//                var ids = grid_tabla.jqGrid('getDataIDs');
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    ed = "<img src='/fintra/images/link_new.png' align='absbottom'  name='editar' id='editar' width='16' height='16' title ='editar cantidades'  onclick=\"editar_cantidades('" + cl + "');\">";
////                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='16' height='16' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan datos asociados a la subcategoria, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
//                    grid_tabla.jqGrid('setRowData', ids[i], {actions: ed});
//                }

        },
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 7,
                id_solicitud: $('#id_solicitud').val(),
                opc: opc,
                cond: cond,
                proceso:  0,//proceso,
                id_lote: $('#id_lote').val()

            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 150);
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                mensajesDelSistema("No se encontraron registros", '204', '140', false);
            }
        }

    }).navGrid("#pager_tabla_ejecucion", {add: false, edit: false, del: false, search: false, refresh: false}, {
        
    });
    jQuery("#tabla_ejecucion").jqGrid("navButtonAdd", "#pager_tabla_ejecucion" , {
            caption: "Agregar",
            title: "Agregar APU",
            onClickButton: function () {
                agregarApu();
            }
        });   
    grid_tabla.jqGrid('setGroupHeaders', {
        useColSpanStyle: false,
        groupHeaders: [
            {startColumnName: 'id_disciplina_area', numberOfColumns: 14, titleText: '<H5  style="text-align: center;">WORK BREAKDOWN STRUCTURE</H5>'},
            {startColumnName: 'cantidad_apu', numberOfColumns: 3, titleText: '<H5 style="text-align: center;">PRESUPUESTADO</H5>'},
            {startColumnName: 'cantidad_apu_actual', numberOfColumns: 2, titleText: '<H5 style="text-align: center;">ACTUAL</H5>'},
            {startColumnName: 'cantidad_apu_ejecutado', numberOfColumns: 2, titleText: '<H5 style="text-align: center;">EJECUTADO</H5>'}
        ]
    });


}
//grilla de registro_ejecucion
function grid_registro_ejecucion(proceso, opc, cond) {
    $('#tabla_ejecucion_1').jqGrid('GridUnload');

    var grid_tabla = jQuery("#tabla_ejecucion_1");
    grid_tabla.jqGrid({
        caption: "Proyecto - Selectrick",
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        datatype: "json",
        height: '550',
        width: '1590',
        colNames: ['id', 'id_disciplina_area', 'Area', 'id_disciplina', 'Displina', 'id_capitulo', 'Capitulo', 'id_actividad', 'Actividad',
            'id_actividades_capitulo', 'id_rel_actividades_apu', 'id_apu', 'APU', 'unidad_medida_apu', 'Unidad Medida', 'Cant. APU',
            'Valor APU', 'Total APU', 'Cantidad APU', 'Valor APU', 'Cantidad APU', 'Valor APU', 'actions', 'edito'],
        colModel: [
            {name: 'id', index: 'id', width: 10, sortable: true, align: 'left', hidden: true, search: true, key: true},
            {name: 'id_disciplina_area', index: 'id_disciplina_area', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_area', index: 'descripcion_area', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_disciplina', index: 'id_disciplina', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_displina', index: 'descripcion_displina', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_capitulo', index: 'id_capitulo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_capitulo', index: 'descripcion_capitulo', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_actividad', index: 'id_actividad', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_actividad', index: 'descripcion_actividad', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_actividades_capitulo', index: 'id_actividades_capitulo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'id_rel_actividades_apu', index: 'id_rel_actividades_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'id_apu', index: 'id_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_apu', index: 'descripcion_apu', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'unidad_medida_apu', index: 'unidad_medida_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'nombre_unidad_medida_apu', index: 'nombre_unidad_medida_apu', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'cantidad_apu', index: 'cantidad_apu', width: 5, sortable: true, align: 'right', hidden: false, search: true, hidden: true, sorttype: 'numeric'},
            {name: 'valor_apu_presupuesto', index: 'valor_apu_presupuesto', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'total_apu_presupuesto', index: 'total_apu_presupuesto', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$"}},
            {name: 'cantidad_apu_actual', index: 'cantidad_apu_actual', sortable: true, editable: true, width: '10', align: 'right', search: false, sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""},
                editoptions: {dataInit: function (elem) {
                        $(elem).keypress(function (e) {
                            if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
                                return false;
                            }
                        });
                    }
                }
            },
            {name: 'valor_apu_actual', index: 'valor_apu_actual', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'cantidad_apu_ejecutado', index: 'cantidad_apu_ejecutado', width: 10, sortable: true, align: 'right', hidden: false, search: true},
            {name: 'valor_apu_ejecutado', index: 'valor_apu_ejecutado', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 10, search: false},
            {name: 'edito', index: 'edito', width: 10, sortable: true, align: 'left', hidden: true, search: true}
        ],
        rowNum: 1000000,
        rowTotal: 1000000,
        loadonce: true,
        rownumWidth: 60,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: false,
        rownumbers: false,
        pager: '#pager_tabla_ejecucion_1',
        multiselect: false,
        multiboxonly: false,
        pgtext: null,
        pgbuttons: false,
        editurl: 'clientArray',
        subGrid: false,
        ondblClickRow: function (rowid, iCol, e) {
            $('#opcion_').val(2);
            $('#id_tabla_ejecucion').val(rowid);
            editar_Cantidades_Actual(rowid, 2);




//                jQuery("#tabla_ejecucion").jqGrid('editRow', rowid, true, function () {
//                }, null, null, {}, function (rowid) {
//
//
//                    var id = $("#tabla_ejecucion").getRowData(rowid).id;
//                    var cantidad_apu_actual = $("#tabla_ejecucion").getRowData(rowid).cantidad_apu_actual;
//                    var valor_apu_presupuesto = $("#tabla_ejecucion").getRowData(rowid).valor_apu_presupuesto;
//
//                    grid_tabla.jqGrid('setCell', rowid, "valor_apu_actual", cantidad_apu_actual * valor_apu_presupuesto);
////                    cargando_toggle();
////                    insertar_rentabilidad_contratista_global(id, porcentaje);
////                    actualizarGrillas();
////                    cargando_toggle();
//
//
//
//                });
//                return;
        }, gridComplete: function () {
//                var ids = grid_tabla.jqGrid('getDataIDs');
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    ed = "<img src='/fintra/images/link_new.png' align='absbottom'  name='editar' id='editar' width='16' height='16' title ='editar cantidades'  onclick=\"editar_cantidades('" + cl + "');\">";
////                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='16' height='16' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan datos asociados a la subcategoria, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
//                    grid_tabla.jqGrid('setRowData', ids[i], {actions: ed});
//                }

        },
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 7,
                id_solicitud: $('#id_solicitud').val(),
                opc: opc,
                cond: cond,
                proceso: proceso,
                id_lote: $('#id_lote').val()


            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 150);
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                mensajesDelSistema("No se encontraron registros", '204', '140', false);
            }
        }

    }).navGrid("#pager_tabla_ejecucion_1", {add: false, edit: false, del: false, search: false, refresh: false}, {
    });
    grid_tabla.jqGrid('setGroupHeaders', {
        useColSpanStyle: false,
        groupHeaders: [
            {startColumnName: 'id_disciplina_area', numberOfColumns: 14, titleText: '<H5  style="text-align: center;">WORK BREAKDOWN STRUCTURE</H5>'},
            {startColumnName: 'cantidad_apu', numberOfColumns: 3, titleText: '<H5 style="text-align: center;">PRESUPUESTADO</H5>'},
            {startColumnName: 'cantidad_apu_actual', numberOfColumns: 2, titleText: '<H5 style="text-align: center;">ACTUAL</H5>'},
            {startColumnName: 'cantidad_apu_ejecutado', numberOfColumns: 2, titleText: '<H5 style="text-align: center;">EJECUTADO</H5>'}
        ]
    });


}
//grilla de actas
function grid_actas(proceso, opc, cond) {
    $('#tabla_ejecucion_2').jqGrid('GridUnload');

    var grid_tabla = jQuery("#tabla_ejecucion_2");
    grid_tabla.jqGrid({
        caption: "Proyecto - Selectrick",
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        datatype: "json",
        height: '550',
        width: '1590',
        colNames: ['id', 'id_disciplina_area', 'Area', 'id_disciplina', 'Displina', 'id_capitulo', 'Capitulo', 'id_actividad', 'Actividad',
            'id_actividades_capitulo', 'id_rel_actividades_apu', 'id_apu', 'APU', 'unidad_medida_apu', 'Unidad Medida', 'Cant. APU',
            'Valor APU', 'Total APU', 'Cantidad APU', 'Valor APU', 'Cantidad APU', 'Valor APU', 'actions', 'edito'],
        colModel: [
            {name: 'id', index: 'id', width: 10, sortable: true, align: 'left', hidden: true, search: true, key: true},
            {name: 'id_disciplina_area', index: 'id_disciplina_area', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_area', index: 'descripcion_area', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_disciplina', index: 'id_disciplina', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_displina', index: 'descripcion_displina', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_capitulo', index: 'id_capitulo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_capitulo', index: 'descripcion_capitulo', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_actividad', index: 'id_actividad', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_actividad', index: 'descripcion_actividad', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'id_actividades_capitulo', index: 'id_actividades_capitulo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'id_rel_actividades_apu', index: 'id_rel_actividades_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'id_apu', index: 'id_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_apu', index: 'descripcion_apu', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'unidad_medida_apu', index: 'unidad_medida_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'nombre_unidad_medida_apu', index: 'nombre_unidad_medida_apu', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'cantidad_apu', index: 'cantidad_apu', width: 5, sortable: true, align: 'right', hidden: false, search: true, sorttype: 'numeric'},
            {name: 'valor_apu_presupuesto', index: 'valor_apu_presupuesto', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'total_apu_presupuesto', index: 'total_apu_presupuesto', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$"}},
            {name: 'cantidad_apu_actual', index: 'cantidad_apu_actual', sortable: true, editable: true, width: '10', align: 'right', search: false, sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""},
                editoptions: {dataInit: function (elem) {
                        $(elem).keypress(function (e) {
                            if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
                                return false;
                            }
                        });
                    }
                }
            },
            {name: 'valor_apu_actual', index: 'valor_apu_actual', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'cantidad_apu_ejecutado', index: 'cantidad_apu_ejecutado', width: 10, sortable: true, align: 'right', hidden: false, search: true},
            {name: 'valor_apu_ejecutado', index: 'valor_apu_ejecutado', editable: false, align: 'right', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 10, search: false},
            {name: 'edito', index: 'edito', width: 10, sortable: true, align: 'left', hidden: true, search: true}
        ],
        rowNum: 1000000,
        rowTotal: 1000000,
        loadonce: true,
        rownumWidth: 60,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: false,
        rownumbers: false,
        pager: '#pager_tabla_ejecucion',
        multiselect: false,
        multiboxonly: false,
        pgtext: null,
        pgbuttons: false,
        editurl: 'clientArray',
        subGrid: false,
        ondblClickRow: function (rowid, iCol, e) {
            editar_Cantidades_Actual(rowid, 2);
            $('#id_tabla_ejecucion').val(rowid);



//                jQuery("#tabla_ejecucion").jqGrid('editRow', rowid, true, function () {
//                }, null, null, {}, function (rowid) {
//
//
//                    var id = $("#tabla_ejecucion").getRowData(rowid).id;
//                    var cantidad_apu_actual = $("#tabla_ejecucion").getRowData(rowid).cantidad_apu_actual;
//                    var valor_apu_presupuesto = $("#tabla_ejecucion").getRowData(rowid).valor_apu_presupuesto;
//
//                    grid_tabla.jqGrid('setCell', rowid, "valor_apu_actual", cantidad_apu_actual * valor_apu_presupuesto);
////                    cargando_toggle();
////                    insertar_rentabilidad_contratista_global(id, porcentaje);
////                    actualizarGrillas();
////                    cargando_toggle();
//
//
//
//                });
//                return;
        }, gridComplete: function () {
//                var ids = grid_tabla.jqGrid('getDataIDs');
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    ed = "<img src='/fintra/images/link_new.png' align='absbottom'  name='editar' id='editar' width='16' height='16' title ='editar cantidades'  onclick=\"editar_cantidades('" + cl + "');\">";
////                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='16' height='16' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan datos asociados a la subcategoria, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
//                    grid_tabla.jqGrid('setRowData', ids[i], {actions: ed});
//                }

        },
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 7,
                id_solicitud: $('#id_solicitud').val(),
                opc: opc,
                cond: cond,
                id_lote: $('#id_lote').val()

            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 150);
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                mensajesDelSistema("No se encontraron registros", '204', '140', false);
            }
        }

    }).navGrid("#pager_tabla_ejecucion_2", {add: false, edit: false, del: false, search: false, refresh: false}, {
    });
    grid_tabla.jqGrid('setGroupHeaders', {
        useColSpanStyle: false,
        groupHeaders: [
            {startColumnName: 'id_disciplina_area', numberOfColumns: 14, titleText: '<H5  style="text-align: center;">WORK BREAKDOWN STRUCTURE</H5>'},
            {startColumnName: 'cantidad_apu', numberOfColumns: 3, titleText: '<H5 style="text-align: center;">PRESUPUESTADO</H5>'},
            {startColumnName: 'cantidad_apu_actual', numberOfColumns: 2, titleText: '<H5 style="text-align: center;">ACTUAL</H5>'},
            {startColumnName: 'cantidad_apu_ejecutado', numberOfColumns: 2, titleText: '<H5 style="text-align: center;">EJECUTADO</H5>'}
        ]
    });


}



//function reloadGridContenidoProyecto(grid_tabla, op, opc, cond) {
//    grid_tabla.setGridParam({
//        datatype: 'json',
//        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
//        ajaxGridOptions: {
//            type: "POST",
//            data: {
//                opcion: op,
//                id_solicitud: $('#id_solicitud').val(),
//                opc: opc,
//                cond: cond
//            }
//        }
//    });
//    grid_tabla.trigger("reloadGrid");
//}



function editar_Cantidades_Actual(rowid, opc) {
    var height = 490;
//    if (opc == 2) {
//        height = 396;
//    }
    $('#div_tbl_ejecucion_insumos').fadeIn("slow");
    $('#div_tbl_ejecucion_insumos').dialog({
        width: 1080,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Insumos - APU ',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $('#div_tbl_ejecucion_insumos').fadeOut();
                $(this).dialog("destroy");
                $('#txt_cantidad_apu').val('');
                $('#txt_apu_equi').val('');
                $('#id_tabla_ejecucion').val('');

            }
        }
    });

    cargar_insumos_apu(rowid, opc);
}

function agregarApu() {
    var height = 550;
    $('#div_tbl_ejecucion_apus').css({"display": "Flex"});   
    $('#div_tbl_ejecucion_apus').dialog({
        width: 1080,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Agregar APU ',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $('#div_tbl_ejecucion_apus').fadeOut("fast");
                $(this).dialog("destroy");
            }
        }
    });
    limpiarCombosEje(0);
    CargarGridApus();
}


function cargar_insumos_apu(rowid, opc) {
    $('#tbl_ejecucion_insumos').jqGrid('GridUnload');

    if (opc == 1) {
        var grid_tabla2 = $("#tabla_ejecucion");
//        $('#div_cantidad_apu').fadeIn();

    } else if (opc == 2) {
        var grid_tabla2 = $("#tabla_ejecucion_1");
//        $('#div_cantidad_apu').fadeOut();
    }


    var grid_tabla = $("#tbl_ejecucion_insumos");


    grid_tabla.jqGrid({
        caption: " ",
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        datatype: "json",
        height: '270',
        width: '1024',
        colNames: ['id', 'id_relacion_cotizacion_detalle_apu', 'Id Insumo', 'tipo_insumo', 'Descripcion', 'unidad_medida_insumo',
            'Unidad Medida', 'Cant.APU','Cantidad', 'Rendimiento', 'Valor', 'Cantidad Insumo',
            'Valor Insumo', 'Cantidad Insumo', 'Valor Insumo', 'Cantidad Insumo', 'Valor Insumo', 'Cant.APU Act.', '% Avance.'],
        colModel: [
            {name: 'id', index: 'id', width: 10, sortable: true, align: 'left', hidden: true, search: true, key: true},
            {name: 'id_relacion_cotizacion_detalle_apu', index: 'id_relacion_cotizacion_detalle_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'id_insumo', index: 'id_insumo', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'tipo_insumo', index: 'tipo_insumo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'descripcion_insumo', index: 'descripcion_insumo', width: 25, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'unidad_medida_insumo', index: 'unidad_medida_insumo', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'nombre_unidad_insumo', index: 'nombre_unidad_insumo', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'cantidad_apu', index: 'cantidad_apu',  sortable: true, editable: true, width: '10', align: 'right', search: false, sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""},
                editoptions: {dataInit: function (elem) {
                        $(elem).keypress(function (e) {
                            if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {    
                                return false;
                            }
                        });
                    }
                }
            },
            {name: 'cantidad_insumo', index: 'cantidad_insumo', sortable: true, editable: true, width: '10', align: 'right', search: false, sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""},
                editoptions: {dataInit: function (elem) {
                        $(elem).keypress(function (e) {
                            if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
                                return false;
                            }
                        });
                    }
                }
            },
            {name: 'rendimiento_insumo', index: 'rendimiento_insumo', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'costo_personalizado', index: 'costo_personalizado', editable: false, align: 'left', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'cantidad_insumo_total', index: 'cantidad_insumo_total', width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'valor_insumo_total', index: 'valor_insumo_total', editable: false, align: 'left', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'cantidad_insumo_actual', index: 'cantidad_insumo_actual', editable: false, width: 10, sortable: true, align: 'left', hidden: false, search: true},
            {name: 'valor_insumo_actual', index: 'valor_insumo_actual', editable: false, align: 'left', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'cantidad_insumo_ejecutado', index: 'cantidad_insumo_ejecutado', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'valor_insumo_ejecutado', index: 'valor_insumo_ejecutado', editable: false, align: 'left', width: 10, sorttype: 'currency', formatter: 'currency', sortable: true,
                formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'cantidad_apu_actual', index: 'cantidad_apu_actual', width: 10, sortable: true, align: 'left', hidden: true, search: true},
            {name: 'porc_avance_apu', index: 'porc_avance_apu', width: 10, sortable: true, align: 'left', hidden: true, search: true}


        ],
        rowNum: 1000000,
        rowTotal: 1000000,
        loadonce: true,
        rownumWidth: 60,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: true,
        rownumbers: false,
        pager: '#pager_tbl_ejecucion_insumos',
        multiselect: false,
        multiboxonly: false,
        pgtext: null,
        pgbuttons: false,
        editurl: 'clientArray',
        subGrid: false,
        ondblClickRow: function (rowid, iCol, e) {
                grid_tabla.jqGrid('editRow', rowid, true, function () {
                }, null, null, {}, function (rowid) {
                      actualizarDatosTblEjecucionInsumos(grid_tabla.getRowData(rowid).cantidad_apu);
                });
                return;    
        }, gridComplete: function () {
            footersum(grid_tabla);
        },
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        ajaxGridOptions: {
            data: {
                opcion: 8,
                id_solicitud: $('#id_solicitud').val(),
                id_rel_actividades_apu: grid_tabla2.getRowData(rowid).id_rel_actividades_apu,
                id_apu: grid_tabla2.getRowData(rowid).id_apu,
                unidad_medida_apu: grid_tabla2.getRowData(rowid).unidad_medida_apu,
                master: 'TRUE'

            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 150);
        },
        loadComplete: function () {
            var info = grid_tabla.getGridParam('records');
            if (info === 0) {
                mensajesDelSistema("No se encontraron registros", '204', '140', false);
            }
        }

    }).navGrid("#pager_tbl_ejecucion_insumos", {add: false, edit: false, del: false, search: false, refresh: false}, {
    });
    if ($('#id_lote').val() == '') {
        if (opc == 1) {

            grid_tabla.navButtonAdd('#pager_tbl_ejecucion_insumos', {
                caption: "Agregar insumo",
                title: "Agregar insumo",
                buttonicon: "ui-icon-plus",
                onClickButton: function () {
                    $('#filtro_insumo').val('');
                    $('#txt_cantidad_insu_eje').val(0);
                    buscarInsumoxFiltro();
                    
                }

            });

            grid_tabla.navButtonAdd('#pager_tbl_ejecucion_insumos', {
                caption: "Guardar",
                title: "Guardar",
                buttonicon: "ui-icon-save",
                onClickButton: function () {
                    guardar_cantidades_insumos_actuales();
                }

            });

        }
    }




    grid_tabla.jqGrid('setGroupHeaders', {
        useColSpanStyle: false,
        groupHeaders: [
            {startColumnName: 'id_relacion_cotizacion_detalle_apu', numberOfColumns: 10, titleText: '<H6  style="text-align: center;">INSUMO</H6>'},
            {startColumnName: 'cantidad_insumo_total', numberOfColumns: 2, titleText: '<H6  style="text-align: center;">PRESUPUESTADO</H6>'},
            {startColumnName: 'cantidad_insumo_actual', numberOfColumns: 2, titleText: '<H6 style="text-align: center;">ACTUAL</H6>'},
            {startColumnName: 'cantidad_insumo_ejecutado', numberOfColumns: 2, titleText: '<H6 style="text-align: center;">EJECUTADO</H6>'}
        ]
    });


}

function actualizarDatosTblEjecucionInsumos(cantidad_apu){
    var grid_tabla = $("#tbl_ejecucion_insumos");
    var filas = grid_tabla.jqGrid('getDataIDs');
    var row;
    var cantidad_insumo_total=0;
    var valor_insumo_total=0;
    
    
     for (var i = 0; i < filas.length; i++) {
        row = grid_tabla.getRowData(filas[i]);
        
        cantidad_insumo_total = cantidad_apu * row.cantidad_insumo * row.rendimiento_insumo;
        valor_insumo_total = cantidad_insumo_total * row.costo_personalizado;
        
//        console.log("Fila numero >>>>>>>>>" , i);
//        console.log(cantidad_apu);
//        console.log(cantidad_insumo_total);
//        console.log(valor_insumo_total);
        
//        console.log(valor_insumo_total);
        
        grid_tabla.jqGrid('setCell', filas[i], "cantidad_apu", cantidad_apu);
        grid_tabla.jqGrid('setCell', filas[i], "cantidad_insumo_total", cantidad_insumo_total);
        grid_tabla.jqGrid('setCell', filas[i], "valor_insumo_total", valor_insumo_total);
        

    }
    footersum(grid_tabla);   
    
}

function refrescarGrid_tl_ejecucion_insumos(rowid) {
    var grid_tabla2 = $("#tabla_ejecucion");
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 8,
                id_solicitud: $('#id_solicitud').val(),
                id_rel_actividades_apu: grid_tabla2.getRowData(rowid).id_rel_actividades_apu,
                id_apu: grid_tabla2.getRowData(rowid).id_apu,
                unidad_medida_apu: grid_tabla2.getRowData(rowid).unidad_medida_apu,
                master: 'TRUE'
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function refrescarGrid_tl_ejecucion_insumos2() {
    var opc = $('#opcion_').val();
    if (opc == 1) {
        var grid_tabla2 = $("#tabla_ejecucion");
    } else if (opc == 2) {
        var grid_tabla2 = $("#tabla_ejecucion_1");
    }
    var rowid = $('#id_tabla_ejecucion').val();
    var grid_tabla = $("#tbl_ejecucion_insumos");
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 8,
                id_solicitud: $('#id_solicitud').val(),
                id_rel_actividades_apu: grid_tabla2.getRowData(rowid).id_rel_actividades_apu,
                id_apu: grid_tabla2.getRowData(rowid).id_apu,
                unidad_medida_apu: grid_tabla2.getRowData(rowid).unidad_medida_apu,
                master: 'TRUE'
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function footersum(grid_tabla) {
    var valor_insumo_total = grid_tabla.jqGrid('getCol', 'valor_insumo_total', false, 'sum');
    var valor_insumo_actual = grid_tabla.jqGrid('getCol', 'valor_insumo_actual', false, 'sum');
    var valor_insumo_ejecutado = grid_tabla.jqGrid('getCol', 'valor_insumo_ejecutado', false, 'sum');

    grid_tabla.jqGrid('footerData', 'set', {cantidad_insumo_total: "Sub-Total : ", cantidad_insumo_actual: "Sub-Total : ", cantidad_insumo_ejecutado: "Sub-Total : "});
    grid_tabla.jqGrid('footerData', 'set', {valor_insumo_total: valor_insumo_total, valor_insumo_actual: valor_insumo_actual, valor_insumo_ejecutado: valor_insumo_ejecutado});
}

function insertar_cantidades_actuales_grid() {
    if ($('#txt_cantidad_apu').val() == '') {
        toastr.error("Por favor el campo cantidad de apu es requerido", 'Error');
    } else if ($('#txt_apu_equi').val() == '') {
        toastr.error("Por favor el campo apu equivalente es requerido", 'Error');
    } else {



        var grid_tabla = $("#tbl_ejecucion_insumos");


        var ids = grid_tabla.jqGrid('getDataIDs');
        var id_fila, cantidad_actual, valor_actual, fila;

        for (var i = 0; i < ids.length; i++) {

            id_fila = ids[i];
            fila = grid_tabla.getRowData(id_fila);
            cantidad_actual = (numberSinComas(fila['cantidad_insumo']) * numberSinComas($('#txt_cantidad_apu').val()) * numberSinComas(fila['rendimiento_insumo'])).toFixed(2);
            valor_actual = (cantidad_actual * numberSinComas(fila['costo_personalizado'])).toFixed(2);
            grid_tabla.jqGrid('setCell', id_fila, "cantidad_insumo_actual", numberConComas(cantidad_actual));
            grid_tabla.jqGrid('setCell', id_fila, "valor_insumo_actual", valor_actual);
            grid_tabla.jqGrid('setCell', id_fila, "porc_avance_apu", $('#txt_apu_equi').val());


        }
        calcular_cantidad_apu_actual();
        footersum(grid_tabla);
    }


}

function guardar_cantidades_insumos_actuales() {
    filas = $("#tbl_ejecucion_insumos").jqGrid('getRowData');

    $.ajax({
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        datatype: 'json',
        type: 'POST',
        data: {
            opcion: 9,
            master: 'TRUE',
            informacion: JSON.stringify({rows: filas})
        },
        async: false,
        success: function (json) {
            try {

                if (json.error) {
                    mensajesDelSistema1(json.error, '204', '140');
                } else {
                    if (json.respuesta == "OK") {
                        //console.log(json);
                        var id_fila = $('#id_tabla_ejecucion').val();

                        var valor_insumo_total = $('#tbl_ejecucion_insumos').jqGrid('getCol', 'valor_insumo_total', false, 'sum');
                        var ids = $('#tbl_ejecucion_insumos').jqGrid('getDataIDs');
                        var cantidad_apu = $('#tbl_ejecucion_insumos').getRowData(ids[0]).cantidad_apu;

                        console.log("valor =" + valor_insumo_total);
                        console.log("cantidad =" + cantidad_apu);

                        //setiar valores a la grilla general
                        $('#tabla_ejecucion').jqGrid('setCell', id_fila, "cantidad_apu", cantidad_apu);
                        $('#tabla_ejecucion').jqGrid('setCell', id_fila, "valor_apu_presupuesto", (valor_insumo_total/cantidad_apu));
                        $('#tabla_ejecucion').jqGrid('setCell', id_fila, "total_apu_presupuesto", (valor_insumo_total));
                        $('#txt_cantidad_apu').val('');
                        $('#txt_apu_equi').val('');
                        toastr.success("Se actualizo de forma correcta la cantidad de insumos.", 'OK');
                    }


                }
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {

        }
    });
}


function calcular_cantidad_apu_actual() {
    var grid_tabla = $("#tbl_ejecucion_insumos");


    var ids = grid_tabla.jqGrid('getDataIDs');
    var id_fila, cantidad_actual, valor_actual, fila, subtotal = 0;

    for (var i = 0; i < ids.length; i++) {

        id_fila = ids[i];
        fila = grid_tabla.getRowData(id_fila);
        subtotal = parseFloat(subtotal.toFixed(2)) + parseFloat((numberSinComas(fila['cantidad_insumo_actual']) / numberSinComas(fila['cantidad_insumo']) / numberSinComas(fila['rendimiento_insumo'])).toFixed(2));

    }

    total = (subtotal / ids.length).toFixed(2);
    $("#txt_cantidad_apu").val(total);
    $("#txt_cantidad_apu_").val(total);
    for (var i = 0; i < ids.length; i++) {

        grid_tabla.jqGrid('setCell', ids[i], "cantidad_apu_actual", total);


    }

}


function AbrirDivImpresiones() {
    $("#div_impresiones").dialog({
        width: 300,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Selectrik',
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {
                cargando_toggle();
                var grid_tabla = jQuery("#tbl_actas");
                var datos = grid_tabla.jqGrid('getGridParam', 'selarrrow')
                        , fila;
                var size = datos.length;
                concadid = "", coma = '';
                for (var i = 0; i < size; i++) {
                    fila = datos[i];
                    concadid = concadid + coma + fila;
                    coma = ',';
                }

                if (concadid !== "") {
                    console.log(concadid);
                    imprimir_actas(concadid);
                } else {
                    console.log("esta vacia");
                }


                cargando_toggle();

            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}


function CargarGridApus() {
    var grupo_apu = $('#grupo_apu1').val();
    var id = $('#id').val();

    if ($("#gview_tbl_ejecucion_apus").length) {
        refrescarGridGrupoAPU(grupo_apu, id);
    } else {
        jQuery("#tbl_ejecucion_apus").jqGrid({
            caption: 'Grid Apu',
            url: './controlleropav?estado=Maestro&accion=Proyecto',
            datatype: 'json',
            height: 500,
            width: 689,
            colNames: ['Id', 'Descripcion APU', 'Unidad de Medida', 'Id Grupo', 'Id Unidad', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', hidden: false, sortable: true, align: 'center', width: '50', sorttype: 'currency', key: true},
                {name: 'nombreapu', index: 'nombreapu', sortable: true, align: 'left', width: '600'},
                {name: 'nombre_unidad', index: 'nombre_unidad', search: false, sortable: true, align: 'center', width: '100'},
                {name: 'idgrupo', index: 'idgrupo', hidden: true, sortable: true, align: 'center', width: '600'},
                {name: 'idunidad', index: 'idunidad', hidden: true, sortable: true, align: 'center', width: '600'},
                {name: 'action', index: 'action', search: false, align: 'center', width: '100', hidden : true}
            ],
            rowNum: 10000,
            rowTotal: 50000,
            loadonce: true,
            scroll: 1,
            rownumWidth: 40,
            gridview: true,
            multiselect: false,
            viewrecords: true,
            hidegrid: false,
            pager: '#pager_tbl_ejecucion_apus',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false,
                data: {
                    opcion: 25,
                    grupo_apu: grupo_apu,
                    id: id
                }
            },
            gridComplete: function () {
                /*var ids = jQuery("#tbl_apus").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    el = "<img src='/fintra/images/botones/iconos/editDoc2.png' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarAPU('" + cl + "', 'tbl_apus', 0);\">";
                    clonar = "<img src='/fintra/images/opav/Copia.png' align='absbottom'  name='Clonar' id='Clonar' width='15' height='15' title ='Clonar'  onclick=\"mensajeConfirmAction('Esta seguro que desea Clonar Apu', '270', '180', ClonarAPU, '" + cl + "');\">";
                    jQuery("#tbl_apus").jqGrid('setRowData', ids[i], {action: el + ' ' + clonar});
                }*/
            },
            ondblClickRow: function (id) {
                 agregarApuEjecucion(id);
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });
/*
        jQuery("#tbl_apus").jqGrid('navGrid', '#page_apus', {del: false, add: false, edit: false, search: false, refresh: false});
        jQuery("#tbl_apus").navButtonAdd('#page_apus', {
            caption: "Crear APU",
            title: "Crear APU",
            buttonicon: "ui-icon-new",
            onClickButton: function () {
                crearAPU();
            }
        });
*/
    }
}

function agregarApuEjecucion(id){
    var json = {};
    json.id_solicitud = $("#id_solicitud").val();
    json.apu = $("#tbl_ejecucion_apus").getRowData(id);
    json.id_area = $('#area_').val();
    json.id_disciplina_area = $('#disciplina_').val();
    json.id_capitulo = $('#capitulo_').val();
    json.id_actividades_capitulo = $('#actividad_').val();
    json.cantidad = $('#txt_cantidad_apu_eje').val();
    var validacion = true;
    var mensaje = "Debe diligenciar completamente todos los campos : \n";
    
    
    if(json.id_actividades_capitulo == ''){
        mensaje +="la ubicacion donde se ubicara el apu. \n"
        validacion = false;
    }
    if(json.cantidad <= 0){
        mensaje +="ingrese la cantidad de apu que desea recuerde que debe ser mayor a 0. \n"
        validacion = false;
    }
    if(validacion){
        //mensaje = 'Esta seguro que desea agregar el apu : ' + 'XXXXXX' + ' a : \n actividad ' + 'XXXXXX' + ', \n disciplina ' + 'XXXXXXX' + ', \n capitulo ' +'XXXXXXXX' 
        $.ajax({
            url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
            datatype: 'json',
            type: 'POST',
            data: {
                opcion: 20,
                informacion: JSON.stringify(json)
            },
            async: false,
            success: function (json) {
                try {

                    if (json.error) {
                        mensajesDelSistema1(json.error, '204', '140');
                    } else {
                        if (json.respuesta == "OK") {
                            console.log(json);
                            toastr.success("Se agrego de forma correcta su APU.", 'OK');
                        }
                    }
                } catch (exc) {
                    console.error(exc);
                } finally {

                }
            },
            error: function () {

            }
        });
    }else{
        
        toastr.error(mensaje, 'Error');
    }
                
    
    //$(#).val()
    
}

function refrescarGridGrupoAPU(grupo_apu, id) {
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    jQuery("#tbl_ejecucion_apus").setGridParam({
        datatype: 'json',
        url: url,
        opcion: 25,
        grupo_apu: grupo_apu,
        id: id
    });
    jQuery('#tbl_ejecucion_apus').trigger("reloadGrid");
}

function filtro_apu(buscar) {

    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    jQuery("#tbl_apus").setGridParam({
        datatype: 'json',
        url: url,
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: 73,
                buscar: buscar,
                apu_proyecto: $('#apu_proyecto').prop('checked'),
                id_solicitud: $('#id_solicitud').val()

            }
        }
    });
    $('#tbl_apus').trigger("reloadGrid");
}

function cargarComboGrupo1(filtro) {
    var elemento = $('#grupo_apu1'), sql = 'ConsultaGruposAPUS';
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'get',
        data: {opcion: 1, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '" onclick="CargarGridApus();">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });
}

/********************************************insertar insumo*******************************************/
function buscarInsumoxFiltro() {

    limpiarCombos2();
    AbrirDivFiltroInsumos();
    cargarComboGenerico2('tipo_insumo', 5, '');
    cargarComboGenerico2('_unidad_medida', 6, '');
    cargarCombo('categoria', [$('#tipo_insumo').val()]);
    CargarInsumosxFiltro();
//    $('#idregistro').val(cl);
    $('#_unidad_medida').val(19);
}

function AbrirDivFiltroInsumos() {
    $("#div_filtro_insumos").dialog({
        width: 750,
        height: 631,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'FILTROS DE BUSQUEDA DE INSUMOS',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                limpiarCombos();
                $(this).dialog("destroy");
            }
        }
    });
}

function filtro_apu(buscar) {
    
    var url = './controlleropav?estado=Maestro&accion=Proyecto&opcion=73&buscar=' + buscar;
    jQuery("#tbl_ejecucion_apus").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#tbl_ejecucion_apus').trigger("reloadGrid");
}

function limpiarCombos2() {
    $('#categoria').html('');
    $('#categoria').append('<option value="0">...</option>');
    $('#sub').html('');
    $('#sub').append('<option value="0">...</option>');
}


function cargarCombo(id, filtro) {
    var elemento = $('#' + id)
            , sql = (id === 'categoria') ? 'categorias' : 'subcategoriaxcategoria';
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'get',
        data: {opcion: 2, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {

            }
        },
        error: function () {

        }
    });
}


function CargarInsumosxFiltro() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=APU&opcion=6';
    $('#tbl_filtro_insumos').jqGrid('GridUnload');

    jQuery("#tbl_filtro_insumos").jqGrid({
        caption: 'Filtro Insumos',
        url: url,
        datatype: 'json',
        height: 300,
        width: 650,
        colNames: ['Id', 'Descripcion'],
        colModel: [
            {name: 'id', index: 'id', sortable: true, align: 'center', width: '100', key: true},
            {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600'}
        ],
        rowNum: 1000,
        rowTotal: 50000,
        loadonce: true,
        rownumWidth: 40,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        pager: '#page_filtro_insumos',
        jsonReader: {
            root: 'rows',
            repeatitems: false,
            id: '0'
        },
        ajaxGridOptions: {
            async: false,
            data: {
                subcategoria: $('#sub').val()
            }
        },
        ondblClickRow: function (id) {
            insertarInsumoAdicional(id);
        },
        loadError: function (xhr, status, error) {
            alert(error);
        }
    });

}

function insertarInsumoAdicional(id) {
    var grid = $('#tbl_ejecucion_insumos');
    var ids = grid.jqGrid('getDataIDs');
    var fila, existe = false;
    var cantidad_insumo = $('#txt_cantidad_insu_eje').val()
    if(!cantidad_insumo >0) {
        toastr.error("por favor verificar que la cantidad de suministro sea mayor que ceo.", 'Error');
        return false;
    }
    for (var i = 0; i < ids.length; i++) {
        fila = grid.jqGrid('getRowData', ids[i]);
        if ((fila.id_insumo == id)&&($('#_unidad_medida').val() == fila.unidad_medida_insumo)) {
            existe = true;
            break;
        }
    }
    if (existe) {
        toastr.error("Ya se encuentra este insumo en el APU.", 'Error');
    } else {
        fila = grid.jqGrid('getRowData', ids[0]);
        $.ajax({
            url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
            datatype: 'json',
            type: 'POST',
            data: {
                opcion: 19,
                id_insumo: id,
                id_unidad_medida: $('#_unidad_medida').val(),
                id_wbs_ejecucion: fila.id,
                cantidad : cantidad_insumo


            },
            async: false,
            success: function (json) {
                try {

                    if (json.error) {
                        mensajesDelSistema1(json.error, '204', '140');
                    } else {
                        if (json.respuesta == "OK") {
//                            console.log(json);
//                            var id_fila = $('#id_tabla_ejecucion').val();
//
//                            var valor_insumo_actual = $('#tbl_ejecucion_insumos').jqGrid('getCol', 'valor_insumo_actual', false, 'sum');
//                            var ids = $('#tbl_ejecucion_insumos').jqGrid('getDataIDs');
//                            var cantidad_apu_actual = $('#tbl_ejecucion_insumos').getRowData(ids[0]).cantidad_apu_actual;
//
//                            console.log("valor =" + valor_insumo_actual);
//                            console.log("cantidad =" + cantidad_apu_actual);
//
//                            //setiar valores a la grilla general
//                            $('#tabla_ejecucion').jqGrid('setCell', id_fila, "cantidad_apu_actual", cantidad_apu_actual);
//                            $('#tabla_ejecucion').jqGrid('setCell', id_fila, "valor_apu_actual", valor_insumo_actual);
                            toastr.success("Se agrego el insumo adicional al apu.", 'OK');
                            refrescarGrid_tl_ejecucion_insumos2();
//                            $('#div_filtro_insumos').dialog("close");

                        }


                    }
                } catch (exc) {
                    console.error(exc);
                } finally {

                }
            },
            error: function () {

            }
        });
    }

//    var ret = jQuery("#tbl_filtro_insumos").jqGrid('getRowData', id);
//    var nombre = ret.nombre;
//    var idcl = $('#idregistro').val();
//    jQuery("#tbl_insumos").jqGrid('setCell', idcl, "descripcionins", nombre);
//    jQuery("#tbl_insumos").jqGrid('setCell', idcl, "iddescripcionins", ret.id);
//    document.getElementById("sub").length = 0;
//    $("#div_filtro_insumos").dialog("destroy");
}

/********************************************insertar insumo*******************************************/


function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/**********************************************************************************************************************************************************
 Utiles Generales
 ***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon', '#perc_rete'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function fecha() {
    var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    var diasSemana = new Array("Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado");
    var f = new Date();
    return ("Hoy " + diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
}


/**********************************************************************************************************************************************************
 Fin Utiles Generales
 ***********************************************************************************************************************************************************/

//
//
//function mensajeConfirmacion(msj, width, height ) {
//    mostrarContenido('dialogMsgMeta');
//    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
//    $("#dialogMsgMeta").dialog({
//        width: width,
//        height: height,
//        show: "scale",
//        hide: "scale",
//        resizable: false,
//        position: "center",
//        modal: true,
//        closeOnEscape: false,
//        buttons: {
//            "Si": function () {
//                $(this).dialog("destroy");
//                return  true;
//            },
//            "No": function () {
//                $(this).dialog("destroy");
//                return  false;
//            }
//        }
//    });
//}
//
//function mostrarContenido(Id_Contenido) {
//    document.getElementById(Id_Contenido).style.display = "block";
//    document.getElementById(Id_Contenido).style.visibility = "visible";
//}