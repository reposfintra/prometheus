/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    cargarReporteAnticiposClientes();
});


function cargarReporteAnticiposClientes() {
    var grid_tabla_ = jQuery("#tabla_anticipoClientesMS");
    if ($("#gview_tabla_anticipoClientesMS").length) {
        reloadGridMostrar(grid_tabla_, 88);
    } else {
        grid_tabla_.jqGrid({
            caption: "Reporte Anticipos",
            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
            mtype: "POST",
            datatype: "json",
            height: '495',
            width: '1600',
            colNames: ['Id', 'Id solicitud', 'Multiservicio', 'Nombre Proyecto', 'Anticipo', 'Valor Anticipo', '#CXC', 'Valor CXC', 'Valor Abono CXC', 'Valor Saldo CXC', 'Fecha CXC', '#CXP', 'Valor CXP', 'Fecha CXP'],
            colModel: [
                {name: 'id', index: 'id', width: 100, align: 'center', hidden: true},
                {name: 'id_solicitud', index: 'id_solicitud', width: 150, align: 'center', hidden: false},
                {name: 'multiservicio', index: 'multiservicio', width: 250, align: 'center', hidden: false},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 400, align: 'left', hidden: false},
                {name: 'cod_anticipo', index: 'cod_anticipo', width: 150, align: 'center'},
                {name: 'total_anticipo', index: 'total_anticipo', width: 200, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'num_factura', index: 'num_factura', width: 150, align: 'center'},
                {name: 'valor_cxc', index: 'valor_cxc', width: 200, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_abono_cxc', index: 'valor_abono_cxc', width: 200, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_saldo_cxc', index: 'valor_saldo_cxc', width: 200, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'fecha_cxc', index: 'fecha_cxc', width: 200, align: 'center'},
                {name: 'num_cxp', index: 'num_cxp', width: 150, align: 'center'},
                {name: 'valorcxp', index: 'valorcxp', width: 200, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'fecha_cxp', index: 'fecha_cxp', width: 200, align: 'center'}
            ],
            rownumbers: true,
            rownumWidth: 20,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: false,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                data: {
                    opcion: 88
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function (id, rowid) {
                var ids = jQuery("#tabla_anticipoClientesMS").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var num_cxp = grid_tabla_.getRowData(cl).num_cxp;
                    if (num_cxp === 'PENDIENTE') {
                        grid_tabla_.jqGrid('setCell', cl, "num_cxp", "", {color: 'red'});
                    }
                }
            },
            gridComplete: function (index) {

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_anticipoClientesMS").navButtonAdd('#pager', {
            caption: "Exportar Excel",
            id: 'excel',
            onClickButton: function () {
                exportarExcel();
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center", modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function  exportarExcel() {
    var fullData = $("#tabla_anticipoClientesMS").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        data: {
            listado: myJsonString,
            opcion: 89
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}