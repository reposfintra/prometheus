
$(document).ready(function () {
    maximizarventana();
    listarvalorespredeterminados();
    cargarComboTipoCliente();
    limpiarFormulario();
    //cargarClientesPadre2();
    cargarDepartamentos('CO', 'dep_dir');
    $('#dep_dir').val('ATL');
    cargarCiudad('ATL', "ciu_dir");
    $('#ciu_dir').val('BQ');
    cargarVias('BQ', "via_princip_dir");
    cargarVias('BQ', "via_genera_dir");
    habilitar_form_principal(1);

    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $("#departamento").change(function () {
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciudad");
    });

    $("#dep_dir").change(function () {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciu_dir");
    });
    $("#ciu_dir").change(function () {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarVias(op, "via_princip_dir");
        cargarVias(op, "via_genera_dir");
    });
    $("#via_princip_dir").change(function () {
        $("#via_genera_dir").val('');
    });
    $("#categoriaCliente").change(function () {
        if (this.value === '1') {
            $('#lbl_padre').fadeOut("fast");
            $('#div_padre').fadeOut("fast");
            $('#lbl_tipo').fadeIn();
            $('#div_tipo').fadeIn();
            cargarComboTipoCliente();
            habilitar_form_principal(this.value);
            ordenarCombo('tipoCliente');
        } else {
            $('#lbl_tipo').fadeOut("fast");
            $('#div_tipo').fadeOut("fast");
            $('#lbl_padre').fadeIn();
            $('#div_padre').fadeIn();
            habilitar_form_principal(this.value);
        }
    });


    $("#ClientePadre").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 4
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label.split(" - ")[0],
                            mivar: item.value,
                            id_tipo_cliente: item.id_tipo_cliente,
                            tipo_cliente: item.tipo_cliente
                        };
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {

            var res = ui.item.label.split(" - ");
            $('#nitPadre').val(res[1]);
            $('#ClientePadre').val(res[0]);
            $("#idClientePadre").val(ui.item.mivar);
            $("#tipo_cliente").val(ui.item.tipo_cliente);
            $("#idTipoCliente").val(ui.item.id_tipo_cliente);
            if ($("#categoriaCliente").val() === '3') {
                $('#NitCliente').val(res[1]);
                $('#NitCliente').attr('readonly', true);
                CalcularDv();
            } else {
                $('#NitCliente').attr('readonly', false);
            }

            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item === null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
$('.mayuscula').change(function () {
    this.value = this.value.toUpperCase();
});
$('.mayuscula').css({
    'text-transform': 'uppercase'
});

ordenarCombo('categoriaCliente');
ordenarCombo('tipoCliente');

});

function Nics() {
    if ($('#estadonic').val() === "1") {
        $('#div_Nic').css({
            'display': ''});
        $('#estadonic').val("2");
        $('#estadoxx').html('-');

    } else {
        $('#div_Nic').css({
            'display': 'none'});
        $('#estadonic').val("1");
        $('#estadoxx').html('+');
    }



}


function habilitar_form_principal(x) {

    $('#formPrincipal').css({
        'display': ''});

    //$('#tipoCliente').attr('disabled', 'true');

    if (x === '1') {
        $('#div_clientepadre').css({
            'display': 'none'});
    } else if (x === '2') {
        $('#div_clientepadre').css({
            'display': ''});
    } else if (x === '3') {
        $('#div_clientepadre').css({
            'display': ''});
    }
    limpiarFormulario();



}

function VerificarNitExiste() {
    if (!($('#NitCliente').val() === '')) {
        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            dataType: 'json',
            async: false,
            data: {
                opcion: 9,
                busqueda: $('#NitCliente').val()
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {

                        mensajesDelSistema('Existe un Cliente Asociado a este NIT', '250', '180');



                    } catch (exception) {

                    }

                } else {

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema('Por favor digite el nit, este es un campo obligatorio', '250', '180');
    }




}

function cargarComboTipoCliente() {
    $('#tipoCliente').html('');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 0
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#tipoCliente').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });



}

function cargarComboDepartamento() {
    $('#Departamento').html('');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 1
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#Departamento').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarComboCiudad() {
    $('#Ciudad').html('');
    var dep = $('#Departamento').val();

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 2,
            Departamento: dep

        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#Ciudad').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function CalcularDv() {
    var vpri, x, y, z, i, nit1, dv1;
    nit1 = document.getElementById("NitCliente").value;
    if (isNaN(nit1)) {
        //document.form1.dv.value="X";
        alert('El valor digitado no es un numero valido');
    } else {
        vpri = new Array(16);
        x = 0;
        y = 0;
        z = nit1.length;
        vpri[1] = 3;
        vpri[2] = 7;
        vpri[3] = 13;
        vpri[4] = 17;
        vpri[5] = 19;
        vpri[6] = 23;
        vpri[7] = 29;
        vpri[8] = 37;
        vpri[9] = 41;
        vpri[10] = 43;
        vpri[11] = 47;
        vpri[12] = 53;
        vpri[13] = 59;
        vpri[14] = 67;
        vpri[15] = 71;
        for (i = 0; i < z; i++) {
            y = (nit1.substr(i, 1));
            //document.write(y+"x"+ vpri[z-i] +":");
            x += (y * vpri[z - i]);
            //document.write(x+"<br>");     
        }
        y = x % 11;
        //document.write(y+"<br>");
        if (y > 1) {
            dv1 = 11 - y;
        } else {
            dv1 = y;
        }
        //document.form1.dv.value=dv1;
        document.getElementById("DigitoVerificacion").value = dv1;
    }
}

//Carga los nombre del autocompletar Cliente Padre
function cargarClientesPadre() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    var clientesPadre = [];
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 4
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                for (var j in json.rows) {
                    clientesPadre.push(json.rows[j].valor_02);
                }
                $("#ClientePadre").autocomplete({
                    source: clientesPadre,
                    minLength: 2
                });

            }
        }
    });
}

//Carga los nombre del autocompletar Cliente Padre
function cargarClientesPadre2() {
    var info = {};
    $.ajax({
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 4
        },
        success: function (json) {
            info = json;
            $("#ClientePadre").autocomplete({
                source: info,
                minLength: 2
            });


        }
    });
}

function crearCliente() {
    var x;
    x = parseInt($('#categoriaCliente').val());
    /* if ($('#rpadre').prop('checked')) {
     x = 1;
     } else if ($('#rhijo').prop('checked')) {
     x = 2;
     } else {
     x = 3;
     }*/

    ////////////////////////////////////////////////
    var grid = jQuery("#valorespredeterminados")
            , filas = grid.jqGrid('getDataIDs')
            , data;
    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);
        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    ////////////////////////////////////////////////

    if (CamposObligatorios(x)) {
        if (x === 1) {
            $.ajax({
                type: 'POST',
                url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
                dataType: 'json',
                async: false,
                data: {
                    opcion: 3,
                    NombreCliente: $('#NombreCliente').val(),
                    NitCliente: $('#NitCliente').val(),
                    Nic: $('#Nic').val(),
                    Departamento: $('#idDepartamento').val(),
                    Ciudad: $('#idCiudad').val(),
                    DireccionCliente: $('#DireccionCliente').val(),
                    NombreContacto: $('#NombreContacto').val(),
                    TelefonoContacto: $('#TelefonoContacto').val(),
                    CelularContacto: $('#CelularContacto').val(),
                    EmailContacto: $('#EmailContacto').val(),
                    CargoContacto: $('#CargoContacto').val(),
                    NombreRepresentateLegal: $('#NombreRepresentateLegal').val(),
                    TelRepresentateLegal: $('#TelRepresentateLegal').val(),
                    CelRepresentateLegal: $('#CelRepresentateLegal').val(),
                    EmailRepresentateLegal: $('#EmailRepresentanteLegal').val(),
                    DigitoVerificacion: $('#DigitoVerificacion').val(),
                    idClientePadre: '',
                    Clasificacion: '0',
                    nics: JSON.stringify({rows: filas}),
                    tipoCliente: $('#tipoCliente').val()
                },
                success: function (json) {

                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        if (json.respuesta === "OK") {

                            mensajesDelSistema("Se Creo el Cliente de forma correcta", '250', '150', true);
                            limpiarFormulario();

                        }

                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        } else if (x === 2) {

            $.ajax({
                type: 'POST',
                url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
                dataType: 'json',
                async: false,
                data: {
                    opcion: 3,
                    NombreCliente: $('#NombreCliente').val(),
                    NitCliente: $('#NitCliente').val(),
                    Nic: $('#Nic').val(),
                    Departamento: $('#idDepartamento').val(),
                    Ciudad: $('#idCiudad').val(),
                    DireccionCliente: $('#DireccionCliente').val(),
                    NombreContacto: $('#NombreContacto').val(),
                    TelefonoContacto: $('#TelefonoContacto').val(),
                    CelularContacto: $('#CelularContacto').val(),
                    EmailContacto: $('#EmailContacto').val(),
                    CargoContacto: $('#CargoContacto').val(),
                    NombreRepresentateLegal: $('#NombreRepresentateLegal').val(),
                    TelRepresentateLegal: $('#TelRepresentateLegal').val(),
                    CelRepresentateLegal: $('#CelRepresentateLegal').val(),
                    EmailRepresentateLegal: $('#EmailRepresentanteLegal').val(),
                    DigitoVerificacion: $('#DigitoVerificacion').val(),
                    idClientePadre: $('#idClientePadre').val(),
                    padre: 'N',
                    nics: JSON.stringify({rows: filas}),
                    Clasificacion: '1',
                    tipoCliente: $('#tipoCliente').val()



                },
                success: function (json) {

                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        if (json.respuesta === "OK") {

                            mensajesDelSistema("Se Creo el Cliente de forma correcta", '250', '150', true);
                            limpiarFormulario();

                        }

                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        } else {
            $.ajax({
                type: 'POST',
                url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
                dataType: 'json',
                async: false,
                data: {
                    opcion: 3,
                    NombreCliente: $('#NombreCliente').val(),
                    NitCliente: $('#NitCliente').val(),
                    Nic: $('#Nic').val(),
                    Departamento: $('#idDepartamento').val(),
                    Ciudad: $('#idCiudad').val(),
                    DireccionCliente: $('#DireccionCliente').val(),
                    NombreContacto: $('#NombreContacto').val(),
                    TelefonoContacto: $('#TelefonoContacto').val(),
                    CelularContacto: $('#CelularContacto').val(),
                    EmailContacto: $('#EmailContacto').val(),
                    CargoContacto: $('#CargoContacto').val(),
                    NombreRepresentateLegal: $('#NombreRepresentateLegal').val(),
                    TelRepresentateLegal: $('#TelRepresentateLegal').val(),
                    CelRepresentateLegal: $('#CelRepresentateLegal').val(),
                    EmailRepresentateLegal: $('#EmailRepresentanteLegal').val(),
                    DigitoVerificacion: $('#DigitoVerificacion').val(),
                    idClientePadre: $('#idClientePadre').val(),
                    padre: 'N',
                    nics: JSON.stringify({rows: filas}),
                    Clasificacion: '3'



                },
                success: function (json) {

                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        if (json.respuesta === "OK") {

                            mensajesDelSistema("Se Creo la agencia de forma correcta", '250', '150', true);
                            limpiarFormulario();

                        }

                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }

    } else {
        mensajesDelSistema("Datos Incompletos", '250', '150', true);
    }
}
;

function CamposObligatorios(tipo) {

    var campos = '';
    var DC = true;
    if (tipo === 1) {
        campos = ['#NombreCliente', '#idDepartamento', '#idCiudad', '#NitCliente', '#DigitoVerificacion', '#DireccionCliente', '#NombreContacto',
            '#CargoContacto', '#EmailContacto', '#NombreRepresentateLegal'];
    } else {
        campos = ['#NombreCliente', '#idDepartamento', '#idCiudad', '#NitCliente', '#DigitoVerificacion', '#DireccionCliente', '#NombreContacto',
            '#CargoContacto', '#EmailContacto', '#NombreRepresentateLegal'
                    , '#ClientePadre'];
    }
    ;
    if (($('#TelefonoContacto').val() === '') && ($('#CelularContacto').val() === '')) {
        DC = false;
    }
    if (($('#TelRepresentateLegal').val() === '') && ($('#CelRepresentateLegal').val() === '')) {
        DC = false;
    }

    for (var i = 0; i < campos.length; i++) {
        if (($(campos[i]).val()) === '') {
            DC = false;
            break;
        }

    }

    return DC;

}

function divemergente_nic() {
    $('#div_Nic').dialog({
        width: 400,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Nics',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    listarvalorespredeterminados();

}

function limpiarFormulario() {
    $('#NitCliente').attr('readonly', false);
    $('#NombreCliente').val('');
    $('#Ciudad').html('');
    $('#NitCliente').val('');
    $('#DireccionCliente').val('');
    $('#NombreContacto').val('');
    $('#EmailContacto').val('');
    $('#TelefonoContacto').val('');
    $('#CargoContacto').val('');
    $('#CelularContacto').val('');
    $('#NombreRepresentateLegal').val('');
    $('#TelRepresentateLegal').val('');
    //$('#Nic').val('');
    $('#DigitoVerificacion').val('');
    $('#idDepartamento').val('');
    $('#Departamento').val('');
    $('#idCiudad').val('');
    $('#Ciudad').val('');
    $('#dep_dir').val('ATL');
    $('#ciu_dir').val('BQ');
    $('#via_princip_dir').val('');
    $('#via_genera_dir').val('');
    $('#nom_princip_dir').val('');
    $('#nom_genera_dir').val('');
    $('#placa_dir').val('');
    $('#ClientePadre').val('');
    $('#EmailRepresentanteLegal').val('');
    $('#CelRepresentateLegal').val('');
    $('#nitPadre').val('');
    $('#tipo_cliente').val('');
    $('#idTipoCliente').val('');
    $('#valorespredeterminados').jqGrid('GridUnload');
    listarvalorespredeterminados();


}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargarDepartamentos(codigo, combo) {
    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            type: 'POST',
            async: false,
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 13,
                cod_pais: codigo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function AbrirDivDirecciones() {
    $("#direccion_dialogo").dialog({
        width: 550,
        height: 350,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'INGRESAR DIRECCION',
        closeOnEscape: false
    });
}

function Posicionar_div(id_objeto, e) {
    obj = document.getElementById(id_objeto);
    var posx = 0;
    var posy = 0;
    if (!e)
        var e = window.event;
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    }
    else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    else
        alert('ninguna de las anteriores');

    obj.style.left = posx + 'px';
    obj.style.top = posy + 'px';
    obj.style.zIndex = 1300;

}

function genDireccion(elemento, e) {

    var contenedor = document.getElementById("direccion_dialogo")
            , res = document.getElementById("dir_resul");

    //$("#direccion_dialogo").draggable({ handle: "#drag_direcciones"});
    // Posicionar_div("direccion_dialogo",e); 
    contenedor.style.left = 618 + 'px';
    contenedor.style.top = 111 + 'px';
    contenedor.style.display = "block";

    $("#div_pagaduria").css({
        'width': '950px'
    });
    res.name = elemento;
    res.value = (elemento.value) ? elemento.value : '';

}

function sendDireccion() {

    var x = document.getElementById("dep_dir").selectedIndex;
    var y = document.getElementById("dep_dir").options;
    $('#Departamento').val(y[x].text);
    var x = document.getElementById("ciu_dir").selectedIndex;
    var y = document.getElementById("ciu_dir").options;
    $('#Ciudad').val(y[x].text);
    $('#DireccionCliente').val($('#dir_resul').val());
    $('#idDepartamento').val($('#dep_dir').val());
    $('#idCiudad').val($('#ciu_dir').val());
    $('#direccion_dialogo').css({'display': 'none'});
}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3:
            var res = document.getElementById('dir_resul')
                    , des = document.getElementById(res.name);
            des.value = res.value;


        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");

            document.getElementById("direccion_dialogo").style.display = "none";
            $("#div_pagaduria").css({
                'width': 'auto'
            });
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
                    , g = document.getElementById('via_genera_dir')
                    , opcion;
            for (var i = 0; i < g.length; i++) {

                opcion = g[i];

                if (opcion.value === p) {

                    opcion.style.display = 'none';
                    opcion.disabled = true;

                } else {

                    opcion.disabled = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if (!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                    || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                    || !jQuery('#placa_dir').val()) {
                jQuery('#dir_resul').val("");
                jQuery('#dir_resul').attr("class", "validation-failed");
            } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                        jQuery('#via_princip_dir option:selected').text()
                        + ' ' + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                        + ' ' + jQuery('#via_genera_dir option:selected').text().trim()
                        + ' ' + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                        + ((jQuery('#via_genera_dir option:selected').text().toUpperCase() === '#' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'TRANSVERSAL') ? '-' : ' ')
                        + jQuery('#placa_dir').val().trim().toUpperCase()
                        + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
            }
            break;
    }
}

function cargarVias(codciu, combo) {
    //alert(codciu);

    $('#' + combo).empty();
    $.ajax({
        async: false,
        type: 'POST',
        url: "/fintra/controller?estado=GestionSolicitud&accion=Aval",
        dataType: 'json',
        data: {
            opcion: 'cargarvias',
            ciu: codciu
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    alert(json.error, '250', '180');
                    return;
                }
                try {
                    $('#' + combo).empty();
                    $('#' + combo).append("<option value=''></option>");

                    for (var key in json) {
                        $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    alert('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3:
            var res = document.getElementById('dir_resul')
                    , des = document.getElementById(res.name);
            des.value = res.value;
        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");

            document.getElementById("direccion_dialogo").style.display = "none";
            $("#div_pagaduria").css({
                'width': 'auto'
            });
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
                    , g = document.getElementById('via_genera_dir')
                    , opcion;
            for (var i = 0; i < g.length; i++) {

                opcion = g[i];

                if (opcion.value === p) {

                    opcion.style.display = 'none';
                    opcion.disabled = true;

                } else {

                    opcion.disabled = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if (!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                    || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                    || !jQuery('#placa_dir').val()) {
                jQuery('#dir_resul').val("");
                jQuery('#dir_resul').attr("class", "validation-failed");
            } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                        jQuery('#via_princip_dir option:selected').text()
                        + ' ' + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                        + ' ' + jQuery('#via_genera_dir option:selected').text().trim()
                        + ' ' + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                        + ((jQuery('#via_genera_dir option:selected').text().toUpperCase() === '#' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'TRANSVERSAL') ? '-' : ' ')
                        + jQuery('#placa_dir').val().trim().toUpperCase()
                        + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
            }
            break;
    }
}
function refrescarGridvalorespredeterminados() {
    jQuery('#valorespredeterminados').trigger("reloadGrid");
}
function numeros(e)
{
    var tecla = (document.all) ? e.keyCode : e.which;
    if (tecla === 8)
        return true;
    var patron = /\d/;
    var te = String.fromCharCode(tecla);
    return patron.test(te);
}
function listarvalorespredeterminados() {

    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente&opcion=5';
    if ($("#gview_valorespredeterminados").length) {
        refrescarGridvalorespredeterminados();
    } else {

        jQuery("#valorespredeterminados").jqGrid({
            //caption: 'Valores',
            url: url,
            datatype: 'json',
            height: 50,
            width: 250,
            colNames: ['Id', 'Nics'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, align: 'left', width: '600px'},
                {name: 'valor_01', index: 'valor_01', sortable: true, editable: true, align: 'left', width: '600px',
                    editoptions: {size: 50, dataInit: function (elem) {
                            $(elem).bind("keypress", function (e) {
                                //alert(soloNumeros(e));
                                return numeros(e);
                            });
                        }
                    }}

            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            pager: '#page_tabla_valorespredeterminados',
            multiselect: false,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {


            }, ondblClickRow: function (rowid, iRow, iCol, e) {
                jQuery("#valorespredeterminados").jqGrid('editRow', rowid, true, function () {
                    $("input, select", e.target).focus();
                }, null, null, {}, function (rowid) {
                    var nic = jQuery("#valorespredeterminados").getRowData(rowid).valor_01;
                    if (ExisteNic(rowid, nic) || ExisteNicBd(nic)) {
                        $("#valorespredeterminados").jqGrid('delRowData', rowid);
                    }
                });
                return;
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_valorespredeterminados", {add: false, edit: false, del: false, search: false, refresh: false});
        jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
            caption: "Nuevo",
            title: "Agregar nueva fila",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {

                var grid = $("#valorespredeterminados")
                        , rowid = 'neo_' + grid.getRowData().length;

                grid.addRowData(rowid, {});
                grid.jqGrid('editRow', rowid, true, function () {
                    //  $("input, select", e.target).focus();
                }, null, null, {}, function (rowid) {
                    var nic = jQuery("#valorespredeterminados").getRowData(rowid).valor_01;
                    if (ExisteNic(rowid, nic) || ExisteNicBd(nic)) {
                        $("#valorespredeterminados").jqGrid('delRowData', rowid);
                    }
                }, null, function (id) {
                    var g = $('#valorespredeterminados')
                            , r = g.jqGrid('getLocalRow', id);
                    if (!r.id) {
                        g.jqGrid('delRowData', id);
                    }
                });
            }

        });
    }
    $("#gs_id").attr('hidden', true);
    $("#gs_actions").attr('hidden', true);
    $('#div_valorespredeterminados').show();

}

function validarTelefono(phone) {
    var filter = /^[0-9-()+]+$/;
    return filter.test(phone);
}

function validarEmail3(email) {
    var emailReg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    return emailReg.test(email);
}
function validarEmail(email) {
    if (email.indexOf('@', 0) === -1 || email.indexOf('.', 0) === -1) {

        return false;
    } else {
        return true;
    }



}
function validarEmail2(x) {
    if (x === 1) {
        if (!validarEmail($('#EmailContacto').val())) {
            alert('Por favor verifique email del contacto');
        }
    } else
    if (!validarEmail($('#EmailRepresentanteLegal').val())) {
        alert('Por favor verifique email del Representante legal');
    }



}

function ExisteNic(rowid, valor) {
    var estado = false;
    var ids = jQuery("#valorespredeterminados").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        if (id !== rowid && jQuery("#valorespredeterminados").jqGrid('getCell', id, 'valor_01') === valor) {
            estado = true;
        }
    }
    return estado;
}

function ExisteNicBd(nic) {
    var estado = false;
    $.ajax({
        url: './controlleropav?estado=Procesos&accion=Cliente',
        datatype: 'json',
        type: 'post',
        async: false,
        data: {
            opcion: 28,
            valor_nic: nic
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.respuesta === "SI") {
                    mensajesDelSistema('Este nic ya se encuentra asociado a otro cliente', '250', '150');
                    estado = true;
                }

            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return estado;
}


function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function Recargar() {
    cargarPagina('jsp/opav/OportunidadNegocio/Cliente/Cliente.jsp');
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function resetAddressValues() {
    $("#dir_resul").val('');
    $("#nom_princip_dir").val('');
    $("#nom_genera_dir").val('');
    $("#placa_dir").val('');
    $("#cmpl_dir").val('');
    $("#via_princip_dir").val('');
    $("#via_genera_dir").val('');
}


function ordenarCombo(cboId){
    var valor =$('#'+cboId).val();
    var options = $("#"+ cboId +" option");                  
    options.detach().sort(function (a, b) {              
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#"+cboId);   
    $("#"+ cboId).val(valor);
}