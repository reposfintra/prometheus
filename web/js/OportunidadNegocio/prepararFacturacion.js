/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $('#aceptar').click(function(){
        prepararFacturacion(); 
    });
});

function prepararFacturacion(){
    $.ajax({
        type: 'POST',
        url: "/fintra/controlleropav?estado=Modulo&accion=Ejecucion",
        dataType: 'json',
        data: {
            opcion: 3,
            id_solicitud: $('#id_solicitud').val(),          
            valor_facturar: numberSinComas($('#valor_facturar').val()),        
            valor_material: numberSinComas($('#valor_material').val())          
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {              
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }

                if (json.respuesta === "OK") {                  
                    mensajesDelSistema("Proceso exitoso", '363', '145', true);
                } else {                  
                    mensajesDelSistema("Ha ocurrido un error dentro del proceso", '363', '140');
                }

            } 
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });  
}

function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function mensajesDelSistema(msj, width, height) {
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

 function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}