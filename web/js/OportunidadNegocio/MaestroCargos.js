/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    cargando_toggle();
    maximizarventana();
    cargar_cargos();
    refrescrargrillas();

});
function refrescrargrillas() {
    cargando_toggle();
    cargar_cargas_Laborales();
    cargar_cargas_Laborales_asociados();
    cargando_toggle();
}

function cargar_cargos() {

    $('#cbx_Cargos').html('');

    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        dataType: 'json',
        async: false,
        data: {
            opcion: 17
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    toastr.error(json.error, "Error");
                    
                    return;
                }
                try {
                    for (var key in json) {
                        $('#cbx_Cargos').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}
function cargar_cargas_Laborales() {
    var grid_tbl_maestro = jQuery("#tbl_Cargas_Laborales");
    if ($("#gview_tbl_Cargas_Laborales").length) {
        refrescarGrid_Cargos();
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Listado Cargas Laborales",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '400',
            width: '725',
            cellEdit: true,
            colNames: ['Id', 'Nombre', 'Descripcion',],
            colModel: [
                {name: 'id', index: 'id', width: 5, align: 'left', key: true, hidden: true},
                {name: 'nombre', index: 'nombre', width: 20, align: 'left'},
                {name: 'descripcion', index: 'descripcion', width: 20, align: 'left'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tbl_Cargas_Laborales'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            multiselect: true,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 18,
                    id: $('#cbx_Cargos').val()
                }
            },
            loadError: function (xhr, status, error) {
                toastr.error(error, "Error");
            },
            gridComplete: function (index) {
//                var ids = jQuery("#tbl_Cargas_Laborales").jqGrid('getDataIDs');
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarCargos('" + cl + "');\">";
//                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion('Esta seguro de anular el Cargo?','250','150',AnularCargos,'" + cl + "');\">";
//                    jQuery("#tbl_Cargas_Laborales").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
//                }
//                

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {


            }
        }).navGrid("#page_tbl_Cargas_Laborales", {add: false, edit: false, del: false, search: false, refresh: false}, {});
//        jQuery("#tbl_Cargas_Laborales").jqGrid("navButtonAdd", "#page_tbl_Cargas_Laborales", {
//            caption: "Nuevo",
//            onClickButton: function () {
//                CrearCargos();
//            }
//        });
    }

}

function refrescarGrid_Cargos() {
    jQuery("#tbl_Cargas_Laborales").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 18,
                id: $('#cbx_Cargos').val()
            }
        }
    });

    jQuery('#tbl_Cargas_Laborales').trigger("reloadGrid");
}
function cargar_cargas_Laborales_asociados() {
    var grid_tbl_maestro = jQuery("#tbl_Cargas_Laborales_asociado");
    if ($("#gview_tbl_Cargas_Laborales_asociado").length) {
        refrescarGrid_Cargos_asociados();
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Listado Cargas Laborales Asociadas",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '400',
            width: '725',
            cellEdit: true,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Peso'],
            colModel: [
                {name: 'id', index: 'id', width: 5, align: 'left', key: true},
                {name: 'nombre', index: 'nombre', width: 20, align: 'left'},
                {name: 'descripcion', index: 'descripcion', width: 20, align: 'left'},
                {name: 'peso', index: 'peso', sortable: true, width: 10,editable: true,  align: 'center', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "% "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tbl_Cargas_Laborales_asociado'),
            loadonce: false,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            multiselect: true,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 19,
                    id: $('#cbx_Cargos').val()
                }
            },
            loadError: function (xhr, status, error) {
                //mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
//                var ids = jQuery("#tbl_Cargas_Laborales_asociado").jqGrid('getDataIDs');
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarCargos('" + cl + "');\">";
//                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion('Esta seguro de anular el Cargo?','250','150',AnularCargos,'" + cl + "');\">";
//                    jQuery("#tbl_Cargas_Laborales_asociado").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
//                }
//                

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                
            

            }
        }).navGrid("#page_tbl_Cargas_Laborales_asociado", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tbl_Cargas_Laborales_asociado").jqGrid("navButtonAdd", "#page_tbl_Cargas_Laborales_asociado", {
            caption: "Guardar",
            onClickButton: function () {
                ingresar_peso_actividad_aso_cargo();
            }
        });
    }

}

function refrescarGrid_Cargos_asociados() {
    jQuery("#tbl_Cargas_Laborales_asociado").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 19,
                id: $('#cbx_Cargos').val()
            }
        }
    });

    jQuery('#tbl_Cargas_Laborales_asociado').trigger("reloadGrid");
}

function ingresar_peso_actividad_aso_cargo(){
    filas = $("#tbl_Cargas_Laborales_asociado").jqGrid('getRowData');
    console.log("jaja");
    
    $.ajax({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        type: 'POST',
        data: {
            opcion: 34, 
            informacion: JSON.stringify({rows: filas})
        },
        async: false,
        success: function (json) {
            try {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '204', '140');
                } else {
                    
                    mensajesDelSistema(json.mensaje, '204', '140');
                    refrescarGrid_Cargos_asociados();

                }
                
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {

        }
    });
}


function CrearCargos() {
    $('#div_Control').fadeIn('slow');
    $('#idCargos').val('');
    $('#nombre').val('');
    $('#descripcion').val('');
    AbrirDivIndicadoreGestion();
}

function AbrirDivIndicadoreGestion() {
    $("#div_Control").dialog({
        width: '500',
        height: '260',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR ',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardar_IndicadoreGestion();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarCargos(cl) {

    $('#div_Control').fadeIn("slow");
    var fila = jQuery("#tbl_Cargas_Laborales").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];

    $('#idCargos').val(cl);
    $('#nombre').val(nombre);
    $('#descripcion').val(descripcion);
    abrirDivIndicadoreGestion();
}

function abrirDivIndicadoreGestion() {
    $("#div_Control").dialog({
        width: '500',
        height: '260',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR INDICADOR DE GESTION',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardar_IndicadoreGestion();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}



function guardar_IndicadoreGestion() {
    var nombre = $('#nombre').val();
    var descripcion = $('#descripcion').val();

    if (nombre !== '' && descripcion !== '') {


        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            data: {
                opcion: ($('#idCargos').val() === '') ? 9 : 10,
                id: $('#idCargos').val(),
                nombre: $('#nombre').val(),
                descripcion: $('#descripcion').val()
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        toastr.error(json.error, "Error");
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGrid_Cargos();
                        $("#div_Control").dialog('close');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se pudo guardar el Indicador de Gestion!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    } else {
        mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');
    }
}

function AnularCargos(cl) {

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            opcion: 11,
            id: cl

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    toastr.error(json.error, "Error");
                    return;
                }

                if (json.respuesta === "OK") {

                    refrescarGrid_Cargos();

                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo Anular el Indicador de Gestion!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}


function AsociarCargaLaboral() {
    var id = $('#cbx_Cargos').val();
    var filasId = jQuery('#tbl_Cargas_Laborales').jqGrid('getGridParam', 'selarrrow');
    var coma = '', listado = '';
    if (filasId !== '') {

        for (var i = 0; i < filasId.length; i++) {
            var ret = jQuery("#tbl_Cargas_Laborales").jqGrid('getRowData', filasId[i]);
            listado += coma + ret.id;
            coma = ",";
        }

        var url = "./controlleropav?estado=Modulo&accion=Planeacion";
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 20,
                listado: listado,
                id: id
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        toastr.error(json.error, "Error");
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescrargrillas();
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar las cargas laborales del cargo!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#tbl_Cargas_Laborales").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las cargas laborales  a asociar", '250', '150');
        } else {
            mensajesDelSistema("No hay cargas laborales por asociar", '250', '150');
        }
    }
}

function DesAsociarCargaLaboral() {
    var id = $('#cbx_Cargos').val();
    var filasId = jQuery('#tbl_Cargas_Laborales_asociado').jqGrid('getGridParam', 'selarrrow');
    var coma = '', listado = '';
    if (filasId !== '') {

        for (var i = 0; i < filasId.length; i++) {
            var ret = jQuery("#tbl_Cargas_Laborales_asociado").jqGrid('getRowData', filasId[i]);
            listado += coma + ret.id;
            coma = ",";
        }

        var url = "./controlleropav?estado=Modulo&accion=Planeacion";
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 21,
                listado: listado,
                id: id
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        toastr.error(json.error, "Error");
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescrargrillas();
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo Desasociar las cargas laborales del cargo", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#tbl_Cargas_Laborales").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las cargas laborales a Desasociar", '250', '150');
        } else {
            mensajesDelSistema("No hay  cargas laborales por Desasociar", '250', '150');
        }
    }
}
/**********************************************************************************************************************************************************
 Utiles Generales
 ***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                toastr.error(json.error, "Error");
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon', '#perc_rete'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}


function mensajeConfirmacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}
/**********************************************************************************************************************************************************
 Fin Utiles Generales
 ***********************************************************************************************************************************************************/


