/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    cargarComboGenerico('area', 1, $('#id_solicitud').val());
    limpiarCombos(1);
    $('#area').change(function () {
        limpiarCombos(1);
        cargarComboGenerico('disciplina', 2, $('#area').val());
        
    });

    $('#disciplina').change(function () {
        limpiarCombos(2);
        cargarComboGenerico('capitulo', 3, $('#disciplina').val());
        
    });

    $('#capitulo').change(function () {
        limpiarCombos(3);
        cargarComboGenerico('actividad', 4, $('#capitulo').val());
        
    });
    
    $('#buscar_insumo_apu').click(function(){
        cargarInfoFacturas();
    });
});

function limpiarCombos(x) {
    switch (x) {
        case 1:
            $("#disciplina").html('');
            $("#capitulo").html('');
            $("#actividad").html('');
            $("#disciplina").append('<option value="">' + "Todos" + '</option>');
            $("#capitulo").append('<option value="">' + "Todos" + '</option>');
            $("#actividad").append('<option value="">' + "Todos" + '</option>');
            break;
        case 2:
            
            
            $("#capitulo").html('');
            $("#actividad").html('');
            $("#capitulo").append('<option value="">' + "Todos" + '</option>');
            $("#actividad").append('<option value="">' + "Todos" + '</option>')
            break;
            
        case 3:
            
            $("#actividad").html('');
            $("#actividad").append('<option value="">' + "Todos" + '</option>');
            break;

    }
}

function cargarComboGenerico(idCombo, op, param) {
    if (param == '') {
        limpiarCombos(op);
    } else {
        var elemento = $('#' + idCombo);
        $.ajax({
            url: "/fintra/controlleropav?estado=Modulo&accion=Ejecucion",
            datatype: 'json',
            type: 'GET',
            data: {opcion: 1, op: op, param: param},
            async: false,
            success: function (json) {
                try {
                    if (json.error) {
                        mensajesDelSistema(json.error, '300', 'auto', false);
                    } else {
                        elemento.html('');
                        elemento.append('<option value="">' + "Todos" + '</option>');
                        for (var e in json) {
                            elemento.append('<option value="' + e + '" >' + json[e] + '</option>');
                        }
                    }
                } finally {
                }
            }
        });
    }    
}

function cargarInfoFacturas() {
    var grid_tabla = jQuery("#tabla_infoFacturar");
    if ($("#gview_tabla_infoFacturar").length) {
        reloadGridInfoFacturas(grid_tabla, 2);
    } else {
        grid_tabla.jqGrid({
            caption: "PENDIENTES POR FACTURAR",
            url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
            datatype: "json",
            height: '500',
            width: '1590',
            colNames: ['Fecha creacion', 'IdApu', 'APU', 'IdInsumo', 'Insumo', 'Cant. Proyectada', 'Cant. Ejecutada', 
                       'Presupuesto', 'Cant. a Solicitar', 'Pendientes', 'Total Pendiente', 'Acciones'],
            colModel: [
                {name: 'creation_date', index: 'creation_date', width: 130, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_apu', index: 'id_apu', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'nombre_apu', index: 'nombre_apu', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_insumo', index: 'id_insumo', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'descripcion_insumo', index: 'descripcion_insumo', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cant_proyectada', index: 'cant_proyectada', width: 120, align: 'center'},
                {name: 'cant_ejecutada', index: 'cant_ejecutada', width: 120, align: 'center'},
                {name: 'valor_presupuesto', index: 'valor_presupuesto', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cant_solicitar', index: 'cant_solicitar', width: 120, align: 'center'},
                {name: 'cant_pendiente', index: 'cant_pendiente', width: 120, align: 'center'},
                {name: 'total_pendiente', index: 'total_pendiente', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 185, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {
               /* var ids = grid_tabla.jqGrid('getDataIDs');
                var fila;
                var estado_trazabilidad;
                var id_estado, iva_aiu ='';
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                      wbs = "<img src='/fintra/images/opav/Wbs.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='19' height='19' title ='WBS Ejecucion'  onclick=\"abrirWbs('" + cl + "');\">";
                      accion = "<img src='/fintra/images/opav/Transicion_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n' >";    
                      Exp = "<img src='/fintra/images/opav/Contrato.png' align='absbottom' name='contrato' id='Exportar Excel' value='Exportar_Excel'  width='26' height='26' title ='Generar Cotizaci�n'  onclick=\"generar_Pdf_Presupuesto_Detalle('" + cl + "');\">";
                      fact = "<img src='/fintra/images/opav/Facturacion.png' align='absbottom' name='facturar' id='facturar'value='Facturar'  width='19' height='19' title ='Facturar Apu y/o Insumos'  onclick=\"abrirFacturacionApu('" + cl + "');\">";
                   grid_tabla.jqGrid('setRowData', ids[i], {actions:accion +  Exp + wbs + fact});
                    
                }*/
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 2,
                    id_solicitud: $('#id_solicitud').val(),
                    id_area: $('#area').val(),
                    id_disciplina: $('#disciplina').val(),
                    id_capitulo: $('capitulo').val(),
                    id_actividad: $('#actividad').val(),
                    modalidad: $('#modo_busqueda').val()

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
    }
}

function reloadGridInfoFacturas(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Modulo&accion=Ejecucion',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: $('#id_solicitud').val(),
                id_area: $('#area').val(),
                id_disciplina: $('#disciplina').val(),
                id_capitulo: $('capitulo').val(),
                id_actividad: $('#actividad').val(),
                modalidad: $('#modo_busqueda').val()  
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}


