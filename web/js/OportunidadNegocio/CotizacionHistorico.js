/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    Cargar_Pantalla_Principal_Cotizacion();
    //panel deslizante.
    $("#boton").click(function () {
        $("#divListaNegocios").toggle('slide', {direction: 'left'}, 400);

        if ($("#campoVisible").val() === "0") {
            $('#divSaldos').css({'left': '10px'});
            $("#campoVisible").val(1);
        } else {
            $('#divSaldos').css({'left': '1194px'});
            $("#campoVisible").val(0);
        }

    });

    get_Idaccion();
    //cargartodo();

    $("#percesquema").keyup(function (event) {
        if (event.keyCode == 13) {
            ActualizarPorcentajes(1)
        }
    });
    $("#percecontratista").keyup(function (event) {
        if (event.keyCode == 13) {
            ActualizarPorcentajes(2)
        }
    });
    cargar_Tabla_Porcentajes();
    cargartodo(1, '0');
    cargarCombo_w("cbx_rent_esquema", 1);
    cargarinfo($('#idaccion').val());
    cargando_toggle();

});
function cargando_toggle() {
    $('#loader-wrapper').toggle();
}
function cargartodo(op, filtro) {
    GridCotizacion($('#idaccion').val(), op, filtro);
    cargaDetalleCotizacion($('#idaccion').val());
    cargarinfo($('#idaccion').val());
//    formatoJ('valcotizacion');
}

function onKeyDecimal(e, num) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if (document.getElementById(num.id).value.indexOf('.') !== -1 && keynum === 46)
        return false;
    if ((keynum === 8 || keynum === 0 || keynum === 48 || keynum === 46))
        return true;
    if (keynum >= 58)
        return false;
    return /\d/.test(String.fromCharCode(keynum));
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function cargarHistoricoCotizacion() {
    var grid_tabla = jQuery("#tabla_Historico_cotizacion");
    if ($("#gview_tabla_Historico_cotizacion").length) {
        reloadGridHistoricoCotizacion(grid_tabla, 68);
    } else {
        grid_tabla.jqGrid({
            caption: "Cotizaciones",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '330',
            width: '768',
            colNames: ['id_historico_cotizacion', 'creation_date', 'last_update', 'total'],
            colModel: [
                {name: 'id_historico_cotizacion', index: 'id_historico_cotizacion', width: '20', sortable: true, align: 'center', hidden: false, search: false, key: true},
                {name: 'creation_date', index: 'creation_date', width: '40', sortable: true, align: 'left', hidden: false, search: false},
                {name: 'last_update', index: 'last_update', width: '40', sortable: true, align: 'left', hidden: false, search: false},
                {name: 'total', index: 'total', width: '40', sortable: true, align: 'left', hidden: false, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                Cargar_Pantalla_Principal_Cotizacion(rowid);
            }, //gridComplete: function () {
//                var ids = grid_tabla.jqGrid('getDataIDs');
//                var fila;
//                var estado_trazabilidad;
//                var id_estado;
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    fila = grid_tabla.jqGrid("getLocalRow", cl);
//                    estado_trazabilidad = fila.trazabilidad;
//                    id_estado = fila.id_estado;
//                    if (id_estado === '7') {
////                        img src='/fintra/images/payment_card_credit-128.png' align='absbottom' 
//                        var ca = "<img src='/fintra/images/botones/iconos/carta.png' align='absbottom' name='carta' id='editar'value='Carta'  width='19' height='19' title ='Generar carta aceptacion del cliente'  onclick=\"generarCartaAceptacionCliente('" + cl + "');\">";
//                        //ca = "<input style='height:25px;width:50px;margin-left: 8px;' type='button' name='carta' id='editar'value='Carta'  width='19' height='19' title ='Generar carta aceptacion del cliente'  onclick=\"generarCartaAceptacionCliente('" + cl + "');\">";
//                        //ed = "<input style='height:25px;width:67px;margin-left: 8px;' type='button' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        var ed = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        grid_tabla.jqGrid('setRowData', ids[i], {actions: ed + ' ' + ca});
//                    } else if (id_estado === '14') {
//                        var co = "<img src='/fintra/images/botones/iconos/contrato.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='19' height='19' title ='Generar Contrato'  onclick=\"generarContrato('" + cl + "');\">";
//                        //co = "<input style='height:25px;width:69px;margin-left: 8px;' type='button' name='contrato' id='editar'value='Contrato'  width='19' height='19' title ='Generar Contrato'  onclick=\"funcion('" + cl + "');\">";
//                        //ed = "<input style='height:25px;width:67px;margin-left: 8px;' type='button' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        var ed = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        grid_tabla.jqGrid('setRowData', ids[i], {actions: ed + ' ' + co});
//                    } else {
//                        var ed = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        var wi = "<img src='/fintra/images/botones/iconos/verHistorico.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Facturacion Parcial'  onclick=\"principal('" + cl + "');\">";
//                        var lol = "<img src='/fintra/images/payment_card.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Facturacion Parcial'  onclick=\"principalAsignacionCostos('" + cl + "');\">";
//                        var ca = "<img src='/fintra/images/botones/iconos/procesos.gif' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Cargar Archivos'  onclick=\"divemergente_Carga_Archivos('" + cl + "');\">";
//                        //ed = "<input style='height:25px;width:67px;margin-left: 8px;' type='button' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        grid_tabla.jqGrid('setRowData', ids[i], {actions: ed + lol +wi + ca});
//                    }
//
//                }
            //  },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 68,
                    idsolicitud: $('#idsolicitud').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema1(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema1("No se encontraron registros", '204', '140');
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: false,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
        grid_tabla.navButtonAdd('#pager', {
            caption: "Nuevo",
            title: "Nueva Cotizacion",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {
                Crear_Cotizacion_Historico();
            }

        });

    }
}


function reloadGridHistoricoCotizacion(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                idsolicitud: $('#idsolicitud').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function actualizarGrillas() {
    var grid_tabla = jQuery("#tabla_Areas");
    var datos = grid_tabla.jqGrid('getDataIDs')
            , fila;
    var size = datos.length;
    concadid = '', coma = '';
    for (var i = 0; i < size; i++) {
        fila = datos[i];
        concadid = concadid + coma + fila;
        coma = ',';
    }
    $('#idarea').val(concadid);
    obtener_id_rel_actividades_apu("1", concadid);
    $("#tabla_Disciplina").jqGrid("GridUnload");
    $("#tabla_Capitulo").jqGrid("GridUnload");
    $("#tabla_Actividades").jqGrid("GridUnload");
    cargar_Tabla_Porcentajes();
    cargar_Tabla_Areas();
    cargartodo(1, concadid);



}
function actualizarGrillas2() {
    var grid_tabla = jQuery("#tabla_Areas");
    var datos = grid_tabla.jqGrid('getDataIDs')
            , fila;
    var size = datos.length;
    concadid = '', coma = '';
    for (var i = 0; i < size; i++) {
        fila = datos[i];
        concadid = concadid + coma + fila;
        coma = ',';
    }
    $('#idarea').val(concadid);
    obtener_id_rel_actividades_apu("1", concadid);
    $("#tabla_Disciplina").jqGrid("GridUnload");
    $("#tabla_Capitulo").jqGrid("GridUnload");
    $("#tabla_Actividades").jqGrid("GridUnload");
    cargar_Tabla_Porcentajes();
    cargar_Tabla_Areas();

}

function cargar_Tabla_Porcentajes() {
    var grid_tabla = jQuery("#tabla_porcentajes");
    if ($("#gview_tabla_porcentajes").length) {
        reloadGridTabla_Porcentajes(grid_tabla, 24);
    } else {
        grid_tabla.jqGrid({
            caption: "General",
            url: '/fintra/controlleropav?estado=Cotizacion&accion=Sl',
            datatype: "json",
            height: '150',
            width: '435',
            colNames: ['Id', 'Descripcion', 'Porcentaje (%)'],
            colModel: [
                {name: 'id', index: 'id', width: '20', sortable: true, align: 'center', hidden: true, search: false, key: true},
                {name: 'descripcion', index: 'descripcion', width: '250px', sortable: true, align: 'left', hidden: false, search: false},
                {name: 'valor', index: 'valor', sortable: true, editable: true, width: '100px', align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 5, prefix: ""},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if ($(this).val().includes('.')) {
                                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                        return false;
                                    }
                                } else {
                                    if (e.which != 8 && e.which != 0 && (e.which < 45 || e.which == 47 || e.which > 57)) {
                                        return false;
                                    }
                                }

                            });
                        }
                    }
                }
            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: true,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager_porcentajes',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            editurl: 'clientArray',
            ondblClickRow: function (rowid, iRow, iCol, e) {
                jQuery("#tabla_porcentajes").jqGrid('editRow', rowid, true, function () {
                }, null, null, {}, function (rowid) {


//                    var id = $("#tabla_porcentajes").getRowData(rowid).id;
//                    var porcentaje = $("#tabla_porcentajes").getRowData(rowid).valor;
//                    cargando_toggle();
//                    insertar_rentabilidad_contratista_global(id, porcentaje);
//                    actualizarGrillas();
//                    cargando_toggle();



                });
                return;
            }, gridComplete: function () {

            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 24,
                    idaccion: $('#idaccion').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema1(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    //mensajesDelSistema1("No se encontraron registros", '204', '140');
                }
            }

        }).navGrid("#pager_porcentajes", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla.navButtonAdd('#pager_porcentajes', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                cargando_toggle();
                guardar_rentabilidad_contratista();
                cargando_toggle();
            },
            position: "first"
        });



    }
}

function guardar_rentabilidad_contratista() {
    filas = $("#tabla_porcentajes").jqGrid('getRowData');

    $.ajax({
        url: '/fintra/controlleropav?estado=Cotizacion&accion=Sl',
        datatype: 'json',
        type: 'POST',
        data: {
            opcion: 28,
            idaccion: $('#idaccion').val(),
            informacion: JSON.stringify({rows: filas})
        },
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema1(json.error, '204', '140');
                } else {

                    actualizarGrillas();
                    insertar_rentabilidades_cabecera();

                }
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {

        }
    });
}


function reloadGridTabla_Porcentajes(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Cotizacion&accion=Sl',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                idaccion: $('#idaccion').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function cargar_Tabla_Areas() {
    var grid_tabla = jQuery("#tabla_Areas");
    if ($("#gview_tabla_Areas").length) {
        reloadGridTabla_Areas(grid_tabla, 70);
    } else {
        grid_tabla.jqGrid({
            caption: "Areas",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '330',
            width: '850',
            colNames: ['id_area_proy', 'Descripcion', 'Valor', 'Rent. Contratista', 'Valor Contratista', 'Pareto Contratista', 'Rent. Esquema', 'Valor Esquema', 'Pareto', 'Total', 'Rent.Global', 'idsolicitud'],
            colModel: [
                {name: 'id_area_proy', index: 'id_area_proy', width: '20', sortable: true, align: 'center', hidden: true, search: true, key: true},
                {name: 'descripcion', index: 'descripcion', width: '250px', sortable: true, align: 'left', hidden: false, search: false},
                {name: 'valor', index: 'valor', sortable: true, editable: false, width: '100px', align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'Rent_Contratista', index: 'Rent_Contratista', sortable: true, editable: true, width: '80px', align: 'center', search: false,
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% ", sorttype: 'currency'},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'val_Contratista', index: 'val_Contratista', width: '100px', formatter: 'currency', sorttype: 'currency', sortable: true, align: 'right',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'pareto_Contratista', index: 'pareto_Contratista', sortable: true, editable: true, width: '80px', align: 'center', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% "}
                },
                {name: 'Rent_Esquema', index: 'Rent_Esquema', sortable: true, editable: false, width: '80px', align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'val_Esquema', index: 'val_Esquema', width: '100px', formatter: 'currency', sortable: true, align: 'center', align: 'right', sorttype: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'Pareto', index: 'Pareto', width: '80px', sortable: true, align: 'left', hidden: true, search: false},
                {name: 'Total', index: 'Total', width: '100px', formatter: 'currency', sortable: true, align: 'right', hidden: true, sorttype: 'number',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'rent_global', index: 'rent_global', sortable: true, editable: true, width: '80px', align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% "}
                },
                {name: 'id_solicitud', index: 'id_  solicitud', width: '40', sortable: true, align: 'left', hidden: true, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager_Areas',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            editurl: 'clientArray',
//            ondblClickRow: function (rowid, iRow, iCol, e) {
//                jQuery("#tabla_Areas").jqGrid('editRow', rowid, true, function () {
//                }, null, null, {}, function (rowid) {
//
//                    var id_area_proy = $("#tabla_Areas").getRowData(rowid).id_area_proy;
//                    obtener_id_rel_actividades_apu2("1", id_area_proy);
//
//                    //var Rent_Esquema = $("#tabla_Areas").getRowData(rowid).Rent_Esquema;
//
//                    var Rent_Contratista = $("#tabla_Areas").getRowData(rowid).Rent_Contratista;
//
//                    //ingresar_rentabilidad_global($('#id_rel_actividades_apu2').val(), Rent_Contratista, Rent_Esquema);
//                    ingresar_rentabilidad_global($('#id_rel_actividades_apu2').val(), Rent_Contratista);
//                    actualizarGrillas();
//
//
//
//                });
//                return;
//            }, 
            gridComplete: function () {

                $('#costo_proyecto').val(grid_tabla.jqGrid('getCol', 'valor', false, 'sum'));

                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila, valor, valor_Contratista, valor_Esquema, grantotal;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);

                    valor = grid_tabla.jqGrid('getCell', cl, 'valor');
                    valor_Contratista = grid_tabla.jqGrid('getCell', cl, 'val_Contratista');
                    grid_tabla.jqGrid('setCell', cl, 'Rent_Contratista', ((valor_Contratista * 100 / valor) - 100));
                    grid_tabla.jqGrid('getCol', 'val_Contratista', false, 'sum');
                    //grid_tabla.jqGrid('setCell', cl,'pareto_Contratista', (valor_Contratista*100/grid_tabla.jqGrid('getCol', 'val_Contratista', false, 'sum')));
                    $('#valor_contratista_proyecto').val(grid_tabla.jqGrid('getCol', 'val_Contratista', false, 'sum'));
                    grid_tabla.jqGrid('setCell', cl, 'pareto_Contratista', (valor_Contratista * 100 / $('#valor_contratista_proyecto').val()));
                    valor_Esquema = grid_tabla.jqGrid('getCell', cl, 'val_Esquema');

                    if ((valor_Esquema > 0) && (valor_Esquema < 1)) {
                        grantotal = valor_Contratista;
                    } else {
                        grantotal = valor_Esquema;
                    }



                    grid_tabla.jqGrid('setCell', cl, 'rent_global', ((valor_Esquema * 100 / valor) - 100));
                    grid_tabla.jqGrid('setRowData', cl, {Total: grantotal});



                }


                var colSumValor = grid_tabla.jqGrid('getCol', 'valor', false, 'sum');
                var colSumValorEsquema = grid_tabla.jqGrid('getCol', 'val_Esquema', false, 'sum');
                var colSumValorContratista = grid_tabla.jqGrid('getCol', 'val_Contratista', false, 'sum');
                var tot = grid_tabla.jqGrid('getCol', 'Total', false, 'sum');
                var paretoo = grid_tabla.jqGrid('getCol', 'pareto_Contratista', false, 'sum');

                var rrent_global = ((colSumValorEsquema * 100 / colSumValor) - 100);

                grid_tabla.jqGrid('footerData', 'set', {descripcion: "Total :"});
                grid_tabla.jqGrid('footerData', 'set', {valor: colSumValor, val_Esquema: colSumValorEsquema, val_Contratista: colSumValorContratista, Total: tot, pareto_Contratista: paretoo, rent_global: rrent_global});

            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 70,
                    idsolicitud: $('#idsolicitud').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema1(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema1("No se encontraron registros", '204', '140');
                }
            }

        }).navGrid("#pager_Areas", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
//        grid_tabla.jqGrid('filterToolbar',
//                {
//                    autosearch: false,
//                    searchOnEnter: true,
//                    defaultSearch: "cn",
//                    stringResult: true,
//                    ignoreCase: true,
//                    multipleSearch: true
//
//                });
        grid_tabla.navButtonAdd('#pager_Areas', {
            caption: "DRILL DOWN - DISCIPLINAS",
            title: "DRILL DOWN - DISCIPLINAS",
            buttonicon: "ui-icon-search",
            onClickButton: function () {
                cargando_toggle();
                var datos = grid_tabla.jqGrid('getGridParam', 'selarrrow')
                        , fila;
                var size = datos.length;
                concadid = '', coma = '';
                for (var i = 0; i < size; i++) {
                    fila = datos[i];
                    concadid = concadid + coma + fila;
                    coma = ',';
                }
                $('#idarea').val(concadid);
                cargar_Tabla_Disciplina();
                cargando_toggle();
            }

        });
        grid_tabla.navButtonAdd('#pager_Areas', {
            caption: "VER DETALLE",
            title: "VER DETALLE",
            onClickButton: function () {
                cargando_toggle();
                var datos = grid_tabla.jqGrid('getGridParam', 'selarrrow')
                        , fila;
                var size = datos.length;
                concadid = '', coma = '';
                for (var i = 0; i < size; i++) {
                    fila = datos[i];
                    concadid = concadid + coma + fila;
                    coma = ',';
                }
                $('#idarea').val(concadid);
                obtener_id_rel_actividades_apu("1", concadid);
                cargartodo(1, concadid);
                cargando_toggle();

            }

        });

    }
}


function reloadGridTabla_Areas(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                idsolicitud: $('#idsolicitud').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function obtener_id_rel_actividades_apu2(op, filtro) {
    $.ajax({
        type: 'POST',
        async: false,
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        data: {
            opcion: 74,
            id_solicitud: $('#idsolicitud').val(),
            op: op,
            filtro: filtro
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    var concat = "", coma = '';
                    for (var key in json) {
                        concat = concat + coma + json[key].id_rel_actividades_apu;
                        coma = ',';
                    }
                    $('#id_rel_actividades_apu2').val(concat);


                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function set_rent_esquema() {
    cargando_toggle();
    ingresar_rentabilidad_global($('#idaccion').val(), '', $('#cbx_rent_esquema').val(), '1');
    actualizarGrillas();
    cargando_toggle();

}



function ingresar_rentabilidad_global(filtro, perc_contratista, perc_esquema, tipo) {
    $.ajax({
        type: 'POST',
        async: false,
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        data: {
            opcion: 75,
            id_solicitud: $('#idsolicitud').val(),
            filtro: filtro,
            perc_contratista: perc_contratista,
            distribucion_rentabilidad_esquema: $('#cbx_rent_esquema option:selected').text(),
            perc_esquema: perc_esquema,
            tipo: tipo

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    if (json.respuesta == "OK") {
                        mensajesDelSistema("se ingreso el porcentaje global de forma correcta", '250', '180');
                    }


                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function cargar_Tabla_Disciplina() {
    var grid_tabla = jQuery("#tabla_Disciplina");
    if ($("#gview_tabla_Disciplina").length) {
        cargando_toggle();
        reloadGridTabla_Disciplina(grid_tabla, 71);
        cargando_toggle();
    } else {
        grid_tabla.jqGrid({
            caption: "Disciplinas ",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '330',
            width: '850',
            colNames: ['id_area_proyecto', 'id_disciplina_area', 'Descripcion', 'Valor', 'Rent. Contratista', 'Valor Contratista', 'Pareto Contratista', 'Rent. Esquema', 'Varlo Esquema', 'Pareto', 'Total', 'id_disciplina'],
            colModel: [
                {name: 'id_area_proyecto', index: 'id_area_proyecto', width: '20', sortable: true, align: 'center', hidden: true, search: false},
                {name: 'id_disciplina_area', index: 'id_disciplina_area', width: '40', sortable: true, align: 'left', hidden: true, search: false, key: true},
                {name: 'descripcion', index: 'descripcion', width: '250px', sortable: true, align: 'left', hidden: false, search: false},
                {name: 'valor', index: 'valor', sortable: true, editable: false, width: '100px', align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'Rent_Contratista', index: 'Rent_Contratista', sortable: true, editable: true, width: '80px', align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'val_Contratista', index: 'val_Contratista', width: '100px', formatter: 'currency', sortable: true, align: 'right', sorttype: 'number',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'pareto_Contratista', index: 'pareto_Contratista', sortable: true, editable: true, width: '80px', align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% "}
                },
                {name: 'Rent_Esquema', index: 'Rent_Esquema', sortable: true, editable: false, width: '80px', align: 'center', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'val_Esquema', index: 'val_Esquema', width: '100px', formatter: 'currency', sorttype: 'currency', sortable: true, align: 'right',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'Pareto', index: 'Pareto', width: '80px', sortable: true, align: 'left', hidden: true, search: false},
                {name: 'Total', index: 'Total', width: '100px', formatter: 'currency', sortable: true, align: 'right', sorttype: 'number',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'id_disciplina', index: 'id_disciplina', width: '40', sortable: true, align: 'left', hidden: true, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager_Disciplina',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            editurl: 'clientArray',
//            ondblClickRow: function (rowid, iRow, iCol, e) {
//                grid_tabla.jqGrid('editRow', rowid, true, function () {
//                }, null, null, {}, function (rowid) {
//
//                    var id_disciplina_area = grid_tabla.getRowData(rowid).id_disciplina_area;
//                    obtener_id_rel_actividades_apu2("2", id_disciplina_area);
//
//                    var Rent_Esquema = grid_tabla.getRowData(rowid).Rent_Esquema;
//
//                    var Rent_Contratista = grid_tabla.getRowData(rowid).Rent_Contratista;
//
//                    ingresar_rentabilidad_global($('#id_rel_actividades_apu2').val(), Rent_Contratista, Rent_Esquema, '');
//                    actualizarGrillas();
//
//
//
//                });
//                return;
//            }, 
            gridComplete: function () {

                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila, valor, valor_Contratista, valor_Esquema, grantotal;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);

                    valor = grid_tabla.jqGrid('getCell', cl, 'valor');
                    valor_Contratista = grid_tabla.jqGrid('getCell', cl, 'val_Contratista');
                    grid_tabla.jqGrid('setCell', cl, 'Rent_Contratista', ((valor_Contratista * 100 / valor) - 100));
//                    grid_tabla.jqGrid('setCell', cl,'pareto_Contratista', (valor_Contratista*100/grid_tabla.jqGrid('getCol', 'val_Contratista', false, 'sum')));
                    grid_tabla.jqGrid('setCell', cl, 'pareto_Contratista', (valor_Contratista * 100 / $('#valor_contratista_proyecto').val()));
                    valor_Esquema = grid_tabla.jqGrid('getCell', cl, 'val_Esquema');

                    if ((valor_Esquema > 0) && (valor_Esquema < 1)) {
                        grantotal = valor_Contratista;
                    } else {
                        grantotal = valor_Esquema;
                    }

                    grid_tabla.jqGrid('setRowData', cl, {Total: grantotal});

                }


                var colSumValor = grid_tabla.jqGrid('getCol', 'valor', false, 'sum');
                var colSumValorEsquema = grid_tabla.jqGrid('getCol', 'val_Esquema', false, 'sum');
                var colSumValorContratista = grid_tabla.jqGrid('getCol', 'val_Contratista', false, 'sum');
                var tot = grid_tabla.jqGrid('getCol', 'Total', false, 'sum');
                var paretoo = grid_tabla.jqGrid('getCol', 'pareto_Contratista', false, 'sum');


                grid_tabla.jqGrid('footerData', 'set', {descripcion: "Total :"});
                grid_tabla.jqGrid('footerData', 'set', {valor: colSumValor, val_Esquema: colSumValorEsquema, val_Contratista: colSumValorContratista, Total: tot, pareto_Contratista: paretoo});

            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 71,
                    idarea: $('#idarea').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema1(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');

                if (info === 0) {
                    mensajesDelSistema1("No se encontraron Disciplinas relacionadas", '204', '140');
                }
            }

        }).navGrid("#pager_Disciplina", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
//        grid_tabla.jqGrid('filterToolbar',
//                {
//                    autosearch: false,
//                    searchOnEnter: true,
//                    defaultSearch: "cn",
//                    stringResult: true,
//                    ignoreCase: true,
//                    multipleSearch: true
//
//                });
        grid_tabla.navButtonAdd('#pager_Disciplina', {
            caption: "DRILL DOWN - CAPITULOS",
            title: "DRILL DOWN - CAPITULOS",
            buttonicon: "ui-icon-search",
            onClickButton: function () {
                cargando_toggle();
                var datos = grid_tabla.jqGrid('getGridParam', 'selarrrow')
                        , fila;
                var size = datos.length;
                concadid = '', coma = '';
                for (var i = 0; i < size; i++) {
                    fila = datos[i];
                    concadid = concadid + coma + fila;
                    coma = ',';
                }
                $('#iddisciplina').val(concadid);
                cargar_Tabla_Capitulo();
                cargando_toggle();
            }

        });
        grid_tabla.navButtonAdd('#pager_Disciplina', {
            caption: "VER DETALLE",
            title: "VER DETALLE",
            onClickButton: function () {
                cargando_toggle();
                var datos = grid_tabla.jqGrid('getGridParam', 'selarrrow')
                        , fila;
                var size = datos.length;
                concadid = '', coma = '';
                for (var i = 0; i < size; i++) {
                    fila = datos[i];
                    concadid = concadid + coma + fila;
                    coma = ',';
                }
                $('#idarea').val(concadid);
                obtener_id_rel_actividades_apu("2", concadid);
                cargartodo(2, concadid);
                cargando_toggle();

            }

        });

    }
}


function reloadGridTabla_Disciplina(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                idarea: $('#idarea').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function cargar_Tabla_Capitulo() {
    var grid_tabla = jQuery("#tabla_Capitulo");
    if ($("#gview_tabla_Capitulo").length) {
        cargando_toggle();
        reloadGrid_Tabla_Capitulo(grid_tabla, 72);
        cargando_toggle();
    } else {
        grid_tabla.jqGrid({
            caption: "Capitulos ",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '330',
            width: '850',
            colNames: ['id_disciplina_area', 'id_capitulo', 'Descripcion', 'Valor', 'Rent. Contratista', 'Valor Contratista', 'Pareto Contratista', 'Rent. Esquema', 'Valor Esquema', 'Pareto', 'Total'],
            colModel: [
                {name: 'id_disciplina_area', index: 'id_disciplina_area', width: '20', sortable: true, align: 'center', hidden: true, search: false},
                {name: 'id_capitulo', index: 'id_capitulo', width: '40', sortable: true, align: 'left', hidden: true, search: true, key: true},
                {name: 'descripcion', index: 'descripcion', width: '250px', sortable: true, align: 'left', hidden: false, search: false},
                {name: 'valor', index: 'valor', sortable: true, editable: false, width: '100px', align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'Rent_Contratista', index: 'Rent_Contratista', sortable: true, editable: true, width: '80px', align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'val_Contratista', index: 'val_Contratista', width: '100px', formatter: 'currency', sortable: true, align: 'right', sorttype: 'number',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'pareto_Contratista', index: 'pareto_Contratista', sortable: true, editable: true, width: '80px', align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% "}
                },
                {name: 'Rent_Esquema', index: 'Rent_Esquema', sortable: true, editable: true, width: '80px', align: 'center', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'val_Esquema', index: 'val_Esquema', width: '100px', formatter: 'currency', sortable: true, align: 'right', sorttype: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'Pareto', index: 'Pareto', width: '80px', sortable: true, align: 'left', hidden: true, search: false},
                {name: 'Total', index: 'Total', width: '100px', formatter: 'currency', sortable: true, align: 'right', sorttype: 'number',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                }


            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager_Capitulo',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            editurl: 'clientArray',
//            ondblClickRow: function (rowid, iRow, iCol, e) {
//                grid_tabla.jqGrid('editRow', rowid, true, function () {
//                }, null, null, {}, function (rowid) {
//
//                    var id_capitulo = grid_tabla.getRowData(rowid).id_capitulo;
//                    obtener_id_rel_actividades_apu2("3", id_capitulo);
//
//                    var Rent_Esquema = grid_tabla.getRowData(rowid).Rent_Esquema;
//
//                    var Rent_Contratista = grid_tabla.getRowData(rowid).Rent_Contratista;
//
//                    ingresar_rentabilidad_global($('#id_rel_actividades_apu2').val(), Rent_Contratista, Rent_Esquema);
//                    actualizarGrillas();
//
//
//
//                });
//                return;
//            }, 
            gridComplete: function () {

                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila, valor, valor_Contratista, valor_Esquema, grantotal;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);

                    valor = grid_tabla.jqGrid('getCell', cl, 'valor');
                    valor_Contratista = grid_tabla.jqGrid('getCell', cl, 'val_Contratista');
                    grid_tabla.jqGrid('setCell', cl, 'Rent_Contratista', ((valor_Contratista * 100 / valor) - 100));
//                    grid_tabla.jqGrid('setCell', cl, 'pareto_Contratista', (valor_Contratista * 100 / grid_tabla.jqGrid('getCol', 'val_Contratista', false, 'sum')));
                    grid_tabla.jqGrid('setCell', cl, 'pareto_Contratista', (valor_Contratista * 100 / $('#valor_contratista_proyecto').val()));
                    valor_Esquema = grid_tabla.jqGrid('getCell', cl, 'val_Esquema');

                    if ((valor_Esquema > 0) && (valor_Esquema < 1)) {
                        grantotal = valor_Contratista;
                    } else {
                        grantotal = valor_Esquema;
                    }

                    grid_tabla.jqGrid('setRowData', cl, {Total: grantotal});

                }

                var colSumValor = grid_tabla.jqGrid('getCol', 'valor', false, 'sum');
                var colSumValorEsquema = grid_tabla.jqGrid('getCol', 'val_Esquema', false, 'sum');
                var colSumValorContratista = grid_tabla.jqGrid('getCol', 'val_Contratista', false, 'sum');
                var tot = grid_tabla.jqGrid('getCol', 'Total', false, 'sum');
                var paretoo = grid_tabla.jqGrid('getCol', 'pareto_Contratista', false, 'sum');

                grid_tabla.jqGrid('footerData', 'set', {descripcion: "Total :"});
                grid_tabla.jqGrid('footerData', 'set', {valor: colSumValor, val_Esquema: colSumValorEsquema, val_Contratista: colSumValorContratista, Total: tot});
                grid_tabla.jqGrid('footerData', 'set', {valor: colSumValor, val_Esquema: colSumValorEsquema, val_Contratista: colSumValorContratista, Total: tot, pareto_Contratista: paretoo});
//              
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 72,
                    iddisciplina: $('#iddisciplina').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema1(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema1("No se encontraron Areas relacionadas ", '204', '140');
                }
            }

        }).navGrid("#pager_Capitulo", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
//        grid_tabla.jqGrid('filterToolbar',
//                {
//                    autosearch: false,
//                    searchOnEnter: true,
//                    defaultSearch: "cn",
//                    stringResult: true,
//                    ignoreCase: true,
//                    multipleSearch: true
//
//                });
        grid_tabla.navButtonAdd('#pager_Capitulo', {
            caption: "DRILL DOWN - ACTIVIDADES",
            title: "DRILL DOWN - ACTIVIDADES",
            buttonicon: "ui-icon-search",
            onClickButton: function () {
                cargando_toggle();
                var datos = grid_tabla.jqGrid('getGridParam', 'selarrrow')
                        , fila;
                var size = datos.length;
                concadid = '', coma = '';
                for (var i = 0; i < size; i++) {
                    fila = datos[i];
                    concadid = concadid + coma + fila;
                    coma = ',';
                }
                $('#idcapitulo').val(concadid);
                cargar_Tabla_Actividades();
                cargando_toggle();
            }

        });
        grid_tabla.navButtonAdd('#pager_Capitulo', {
            caption: "VER DETALLE",
            title: "VER DETALLE",
            onClickButton: function () {
                cargando_toggle();
                var datos = grid_tabla.jqGrid('getGridParam', 'selarrrow')
                        , fila;
                var size = datos.length;
                concadid = '', coma = '';
                for (var i = 0; i < size; i++) {
                    fila = datos[i];
                    concadid = concadid + coma + fila;
                    coma = ',';
                }
                $('#idarea').val(concadid);
                obtener_id_rel_actividades_apu("3", concadid);
                cargartodo(3, concadid);
                cargando_toggle();

            }

        });

    }
}


function reloadGrid_Tabla_Capitulo(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                iddisciplina: $('#iddisciplina').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function cargar_Tabla_Actividades() {
    var grid_tabla = jQuery("#tabla_Actividades");
    if ($("#gview_tabla_Actividades").length) {
        cargando_toggle();
        reloadGridTabla_Actividades(grid_tabla, 73);
        cargando_toggle();
    } else {
        grid_tabla.jqGrid({
            caption: "Actividades",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '330',
            width: '850',
            colNames: ['id', 'id_actividad', 'Descripcion', 'Valor', 'Rent. Contratista', 'Valor Contratista', 'Pareto Contratista', 'Rent. Esquema', 'Valor Esquema', 'Pareto', 'Total'],
            colModel: [
                {name: 'id', index: 'id', width: '20', sortable: true, align: 'center', hidden: true, search: false, key: true},
                {name: 'id_actividad', index: 'id_actividad', width: '40', sortable: true, align: 'left', hidden: true, search: false},
                {name: 'descripcion', index: 'descripcion', width: '250px', sortable: true, align: 'left', hidden: false, search: false},
                {name: 'valor', index: 'valor', sortable: true, editable: false, width: '100px', align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'Rent_Contratista', index: 'Rent_Contratista', sortable: true, editable: true, width: '80px', align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'val_Contratista', index: 'val_Contratista', width: '100px', formatter: 'currency', sortable: true, align: 'right', sorttype: 'number',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'pareto_Contratista', index: 'pareto_Contratista', sortable: true, editable: true, width: '80px', align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% "}
                },
                {name: 'Rent_Esquema', index: 'Rent_Esquema', sortable: true, editable: false, width: '80px', align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'val_Esquema', index: 'val_Esquema', width: '100px', formatter: 'currency', sortable: true, align: 'right', sorttype: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'Pareto', index: 'Pareto', width: '80px', sortable: true, align: 'left', hidden: true, search: false},
                {name: 'Total', index: 'Total', width: '100px', formatter: 'currency', sortable: true, align: 'right', sorttype: 'number',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                }

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager_Actividades',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            editurl: 'clientArray',
//            ondblClickRow: function (rowid, iRow, iCol, e) {
//                grid_tabla.jqGrid('editRow', rowid, true, function () {
//                }, null, null, {}, function (rowid) {
//
//                    var id = grid_tabla.getRowData(rowid).id;
//                    obtener_id_rel_actividades_apu2("4", id);
//
//                    var Rent_Esquema = grid_tabla.getRowData(rowid).Rent_Esquema;
//
//                    var Rent_Contratista = grid_tabla.getRowData(rowid).Rent_Contratista;
//
//                    ingresar_rentabilidad_global($('#id_rel_actividades_apu2').val(), Rent_Contratista, Rent_Esquema);
//                    actualizarGrillas();
//
//
//
//                });
//                return;
//            }, 
            gridComplete: function () {

                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila, valor, valor_Contratista, valor_Esquema, grantotal;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);

                    valor = grid_tabla.jqGrid('getCell', cl, 'valor');
                    valor_Contratista = grid_tabla.jqGrid('getCell', cl, 'val_Contratista');
                    grid_tabla.jqGrid('setCell', cl, 'Rent_Contratista', ((valor_Contratista * 100 / valor) - 100));
//                    grid_tabla.jqGrid('setCell', cl, 'pareto_Contratista', (valor_Contratista * 100 / grid_tabla.jqGrid('getCol', 'val_Contratista', false, 'sum
                    grid_tabla.jqGrid('setCell', cl, 'pareto_Contratista', (valor_Contratista * 100 / $('#valor_contratista_proyecto').val()));
                    valor_Esquema = grid_tabla.jqGrid('getCell', cl, 'val_Esquema');

                    if ((valor_Esquema > 0) && (valor_Esquema < 1)) {
                        grantotal = valor_Contratista;
                    } else {
                        grantotal = valor_Esquema;
                    }

                    grid_tabla.jqGrid('setRowData', cl, {Total: grantotal});

                }


                var colSumValor = grid_tabla.jqGrid('getCol', 'valor', false, 'sum');
                var colSumValorEsquema = grid_tabla.jqGrid('getCol', 'val_Esquema', false, 'sum');
                var colSumValorContratista = grid_tabla.jqGrid('getCol', 'val_Contratista', false, 'sum');
                var tot = grid_tabla.jqGrid('getCol', 'Total', false, 'sum');
                var paretoo = grid_tabla.jqGrid('getCol', 'pareto_Contratista', false, 'sum');

                grid_tabla.jqGrid('footerData', 'set', {descripcion: "Total :"});
                grid_tabla.jqGrid('footerData', 'set', {valor: colSumValor, val_Esquema: colSumValorEsquema, val_Contratista: colSumValorContratista, Total: tot, pareto_Contratista: paretoo});
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 73,
                    idcapitulo: $('#idcapitulo').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema1(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema1("No se encontraron Actividades relacionadas", '204', '140');
                }
            }

        }).navGrid("#pager_Actividades", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
//        grid_tabla.jqGrid('filterToolbar',
//                {
//                    autosearch: false,
//                    searchOnEnter: true,
//                    defaultSearch: "cn",
//                    stringResult: true,
//                    ignoreCase: true,
//                    multipleSearch: true
//
//                });
//        grid_tabla.navButtonAdd('#pager_Actividades', {
//            caption: "Buscar",
//            title: "Buscar",
//            buttonicon: "ui-icon-search",
//            onClickButton: function () {
//                var datos = grid_tabla.jqGrid('getGridParam', 'selarrrow')
//                        , fila;
//                var size = datos.length;
//                concadid = '', coma = '';
//                for (var i = 0; i < size; i++) {
//                    fila = datos[i];
//                    concadid = concadid + coma + fila;
//                    coma = ',';
//                }
//                alert(concadid);
//            }
//
//        });
        grid_tabla.navButtonAdd('#pager_Actividades', {
            caption: "VER DETALLE",
            title: "VER DETALLE",
            onClickButton: function () {
                cargando_toggle();
                var datos = grid_tabla.jqGrid('getGridParam', 'selarrrow')
                        , fila;
                var size = datos.length;
                concadid = '', coma = '';
                for (var i = 0; i < size; i++) {
                    fila = datos[i];
                    concadid = concadid + coma + fila;
                    coma = ',';
                }
                $('#idarea').val(concadid);
                obtener_id_rel_actividades_apu("4", concadid);
                cargartodo(4, concadid);
                cargando_toggle();

            }

        });

    }
}


function reloadGridTabla_Actividades(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                idcapitulo: $('#idcapitulo').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function obtener_id_rel_actividades_apu(op, filtro) {
    $.ajax({
        type: 'POST',
        async: false,
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        data: {
            opcion: 74,
            id_solicitud: $('#idsolicitud').val(),
            op: op,
            filtro: filtro
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    var concat = "", coma = '';
                    for (var key in json) {
                        concat = concat + coma + json[key].id_rel_actividades_apu;
                        coma = ',';
                    }
                    $('#id_rel_actividades_apu').val(concat);


                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////VOY POR AQUIIIIIIIIIIII//////////////////////////////////////////////////////////////////////

function cargar_Tabla_Apu() {
    var grid_tabla = jQuery("#Tabla_Apu");
    if ($("#gview_tabla_Apu").length) {
        reloadGridTabla_Apu(grid_tabla, 74);
    } else {
        grid_tabla.jqGrid({
            caption: "APUS",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '330',
            width: '768',
            colNames: ['id_historico_cotizacion', 'creation_date', 'total'],
            colModel: [
                {name: 'id_historico_cotizacion', index: 'id_historico_cotizacion', width: '20', sortable: true, align: 'center', hidden: false, search: false, key: true},
                {name: 'creation_date', index: 'creation_date', width: '40', sortable: true, align: 'left', hidden: false, search: false},
                {name: 'total', index: 'total', width: '40', sortable: true, align: 'left', hidden: false, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: false,
            pager: '#pager_Apu',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                alert(rowid);
            }, //gridComplete: function () {
//                var ids = grid_tabla.jqGrid('getDataIDs');
//                var fila;
//                var estado_trazabilidad;
//                var id_estado;
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    fila = grid_tabla.jqGrid("getLocalRow", cl);
//                    estado_trazabilidad = fila.trazabilidad;
//                    id_estado = fila.id_estado;
//                    if (id_estado === '7') {
////                        img src='/fintra/images/payment_card_credit-128.png' align='absbottom' 
//                        var ca = "<img src='/fintra/images/botones/iconos/carta.png' align='absbottom' name='carta' id='editar'value='Carta'  width='19' height='19' title ='Generar carta aceptacion del cliente'  onclick=\"generarCartaAceptacionCliente('" + cl + "');\">";
//                        //ca = "<input style='height:25px;width:50px;margin-left: 8px;' type='button' name='carta' id='editar'value='Carta'  width='19' height='19' title ='Generar carta aceptacion del cliente'  onclick=\"generarCartaAceptacionCliente('" + cl + "');\">";
//                        //ed = "<input style='height:25px;width:67px;margin-left: 8px;' type='button' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        var ed = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        grid_tabla.jqGrid('setRowData', ids[i], {actions: ed + ' ' + ca});
//                    } else if (id_estado === '14') {
//                        var co = "<img src='/fintra/images/botones/iconos/contrato.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='19' height='19' title ='Generar Contrato'  onclick=\"generarContrato('" + cl + "');\">";
//                        //co = "<input style='height:25px;width:69px;margin-left: 8px;' type='button' name='contrato' id='editar'value='Contrato'  width='19' height='19' title ='Generar Contrato'  onclick=\"funcion('" + cl + "');\">";
//                        //ed = "<input style='height:25px;width:67px;margin-left: 8px;' type='button' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        var ed = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        grid_tabla.jqGrid('setRowData', ids[i], {actions: ed + ' ' + co});
//                    } else {
//                        var ed = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        var wi = "<img src='/fintra/images/botones/iconos/verHistorico.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Facturacion Parcial'  onclick=\"principal('" + cl + "');\">";
//                        var lol = "<img src='/fintra/images/payment_card.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Facturacion Parcial'  onclick=\"principalAsignacionCostos('" + cl + "');\">";
//                        var ca = "<img src='/fintra/images/botones/iconos/procesos.gif' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Cargar Archivos'  onclick=\"divemergente_Carga_Archivos('" + cl + "');\">";
//                        //ed = "<input style='height:25px;width:67px;margin-left: 8px;' type='button' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        grid_tabla.jqGrid('setRowData', ids[i], {actions: ed + lol +wi + ca});
//                    }
//
//                }
            //  },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 68,
                    idsolicitud: $('#idsolicitud').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager_Apu", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: false,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
        grid_tabla.navButtonAdd('#pager_Apu', {
            caption: "Nuevo",
            title: "Nueva Cotizacion",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {

                Cargar_Pantalla_Principal_Cotizacion();
            }

        });

    }
}


function reloadGridTabla_Capitulo(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                idsolicitud: $('#idsolicitud').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



function Cargar_Pantalla_Principal_Cotizacion() {
    $('#tipo_Wbs').hide();
    $('#tipo_Insumo').hide();
    $('#divwbs').show();
    //divemergente_filro();
    cargar_Tabla_Areas();

//    setTimeout(function () {cargar_Tabla_Disciplina();}, 1000);
//    setTimeout(function(){ cargar_Tabla_Capitulo(); }, 1000);
//    setTimeout(function(){ cargar_Tabla_Actividades(); }, 1000);

}




function cargarInsumosGrid() {
    var grid_tabla = jQuery("#tabla_Coste_Proyecto");
    if ($("#gview_tabla_Coste_Proyecto").length) {
        reloadGridcargarInsumosGrid(grid_tabla, 60);
    } else {
        grid_tabla.jqGrid({
            caption: "Insumos",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '330',
            width: '1024',
            colNames: ['id_solicitud', 'id_tipo_insumo', 'descripcion_insumo', 'id_categoria', 'nombre_categoria', 'id_subcategoria', 'nombre_subcategoria',
                'id_insumo', 'nombre_insumo', 'condigo_material', 'id_apu', 'nombre_apu', 'id_grupo_apu', 'nombre_grupo_apu', 'id_actividad_wbs', 'nombre_actividad',
                'id_capitulo_wbs', 'nombre_capitulo', 'id_disciplina_wbs', 'nombre_disciplina', 'id_area_proyecto_wbs', 'nombre_area', 'nombre_proyecto', 'actions'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'id_tipo_insumo', index: 'id_tipo_insumo', width: 200, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'descripcion_insumo', index: 'descripcion_insumo', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_categoria', index: 'id_categoria', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_categoria', index: 'nombre_categoria', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_subcategoria', index: 'id_subcategoria', width: '200px', sortable: true, align: 'center', hidden: false, search: true},
                {name: 'nombre_subcategoria', index: 'nombre_subcategoria', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_insumo', index: 'id_insumo', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_insumo', index: 'nombre_insumo', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'condigo_material', index: 'condigo_material', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_apu', index: 'id_apu', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_apu', index: 'nombre_apu', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_grupo_apu', index: 'id_grupo_apu', width: '200px', sortable: true, align: 'center', hidden: false, search: false},
                {name: 'nombre_grupo_apu', index: 'nombre_grupo_apu', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_actividad_wbs', index: 'id_actividad_wbs', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_actividad', index: 'nombre_actividad', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_capitulo_wbs', index: 'id_capitulo_wbs', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_capitulo', index: 'nombre_capitulo', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_disciplina_wbs', index: 'id_disciplina_wbs', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_disciplina', index: 'nombre_disciplina', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_area_proyecto_wbs', index: 'id_area_proyecto_wbs', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_area', index: 'nombre_area', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: '200px', sortable: true, align: 'left', hidden: false, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '160px', search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: false,
            pager: '#pager_Coste_Proyecto',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                alert(rowid);
            }, //gridComplete: function () {
//                var ids = grid_tabla.jqGrid('getDataIDs');
//                var fila;
//                var estado_trazabilidad;
//                var id_estado;
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    fila = grid_tabla.jqGrid("getLocalRow", cl);
//                    estado_trazabilidad = fila.trazabilidad;
//                    id_estado = fila.id_estado;
//                    if (id_estado === '7') {
////                        img src='/fintra/images/payment_card_credit-128.png' align='absbottom' 
//                        var ca = "<img src='/fintra/images/botones/iconos/carta.png' align='absbottom' name='carta' id='editar'value='Carta'  width='19' height='19' title ='Generar carta aceptacion del cliente'  onclick=\"generarCartaAceptacionCliente('" + cl + "');\">";
//                        //ca = "<input style='height:25px;width:50px;margin-left: 8px;' type='button' name='carta' id='editar'value='Carta'  width='19' height='19' title ='Generar carta aceptacion del cliente'  onclick=\"generarCartaAceptacionCliente('" + cl + "');\">";
//                        //ed = "<input style='height:25px;width:67px;margin-left: 8px;' type='button' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        var ed = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        grid_tabla.jqGrid('setRowData', ids[i], {actions: ed + ' ' + ca});
//                    } else if (id_estado === '14') {
//                        var co = "<img src='/fintra/images/botones/iconos/contrato.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='19' height='19' title ='Generar Contrato'  onclick=\"generarContrato('" + cl + "');\">";
//                        //co = "<input style='height:25px;width:69px;margin-left: 8px;' type='button' name='contrato' id='editar'value='Contrato'  width='19' height='19' title ='Generar Contrato'  onclick=\"funcion('" + cl + "');\">";
//                        //ed = "<input style='height:25px;width:67px;margin-left: 8px;' type='button' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        var ed = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        grid_tabla.jqGrid('setRowData', ids[i], {actions: ed + ' ' + co});
//                    } else {
//                        var ed = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Cambiar estado'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        var wi = "<img src='/fintra/images/botones/iconos/verHistorico.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Facturacion Parcial'  onclick=\"principal('" + cl + "');\">";
//                        var lol = "<img src='/fintra/images/payment_card.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='wi'value=''  width='19' height='19' title ='Facturacion Parcial'  onclick=\"principalAsignacionCostos('" + cl + "');\">";
//                        var ca = "<img src='/fintra/images/botones/iconos/procesos.gif' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Cargar Archivos'  onclick=\"divemergente_Carga_Archivos('" + cl + "');\">";
//                        //ed = "<input style='height:25px;width:67px;margin-left: 8px;' type='button' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Agregar Acciones Alcances'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
//                        grid_tabla.jqGrid('setRowData', ids[i], {actions: ed + lol +wi + ca});
//                    }
//
//                }
            //  },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 60,
                    idsolicitud: $('#num_solicitud').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager_Coste_Proyecto", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
    }
}


function reloadGridcargarInsumosGrid(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                idsolicitud: $('#num_solicitud').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function Crear_Cotizacion_Historico() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 69,
            idsolicitud: $('#idsolicitud').val()
        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                if (json.respuesta == "OK")
                {
                    mensajesDelSistema('Se creo una nueva Cotizacion', '250', '180');
                    cargarHistoricoCotizacion();

                }

            } catch (exception) {
                mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });


}

function mensajesDelSistema1(msj, width, height) {

    $("#msj1").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj1").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}



function tipo_Filtro() {
    if ($('#tipo_Filtro').val() == 0) {
        $('#ww').hide();
    } else
    {
        if ($('#tipo_Filtro').val() == 1) {
            $('#tipo_Insumo').show();
            $('#ww').show();
            $('#tipo_Wbs').hide();

        } else
        {
            $('#tipo_Wbs').show();
            $('#ww').show();
            $('#tipo_Insumo').hide();
            Cargar_Pantalla_Principal_Cotizacion(12);
        }
    }


}


function buscar() {
    if ($('#tipo_Filtro').val() == 1) {

        alert("Definiendolo");
    } else if ($('#tipo_Filtro').val() == 2)
    {
        cargarHistoricoCotizacion();
        //cargar_Tabla_Areas();
        //cargar_Tabla_Disciplina();
        //cargar_Tabla_Capitulo();
        //cargar_Tabla_Actividades();

    }
}
//

function divemergente_filro() {
    $('#divemergente_filro').dialog({
        width: 1558,
        height: 570,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Filtro',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $('#divemergente_filro').show();
}

function get_Idaccion() {
    $.ajax({
        type: 'POST',
        async: false,
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        data: {
            opcion: 46,
            idsolicitud: $('#idsolicitud').val()
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json.rows) {
                        $('#idaccion').val(json.rows[key].id_accion);

                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}




//cotizacionsl.js


function cargarComboGrupo1(filtro) {
    var elemento = $('#grupo_apu1'), sql = 'ConsultaGruposAPUS';
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'GET',
        data: {opcion: 15, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '" onclick="CargarGridApus();">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function cargarCombo_w(id, op) {
    var elemento = $('#' + id);
    $.ajax({
        url: "/fintra/controlleropav?estado=Cotizacion&accion=Sl",
        datatype: 'json',
        type: 'GET',
        data: {opcion: 23, op: op},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    elemento.append('<option value=0 onclick="CargarGridApus();">' + "..." + '</option>');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '" onclick="CargarGridApus();">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function reloadGridCotizacion(apugrupo, metcalc, accion, op, filtro, id_rel_actividades_apu) {
    var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=15&apugrupo=' + apugrupo + '&metcalc=' + metcalc + '&id=' + accion + '&op=' + op + '&filtro=' + filtro + '&id_rel_actividades_apu=' + id_rel_actividades_apu;
    jQuery("#tbl_cotizacion").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#tbl_cotizacion').trigger("reloadGrid");

}

function listarMetodosCal() {
    var Result = {};
    $.ajax({
        type: 'GET',
        url: "/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=20",
        dataType: 'json',
        async: false,
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    Result = {};
                } else {
                    Result = json;
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return Result;
}

function calcularMet(metodo, codigo) {
    var Result = {};
    $.ajax({
        type: 'GET',
        url: "/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=21&metodo=" + metodo + "&codigo=" + codigo,
        dataType: 'json',
        async: false,
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    Result = {};
                } else {
                    Result = json.valor;
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return Result;
}

function CalcularPareto() {
    var grid = jQuery("#tbl_cotizacion"),
            filas = grid.jqGrid('getDataIDs'),
            data;

    var preciototal1 = grid.jqGrid('getCol', 'preciototal', false, 'sum');

    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);

        grid.jqGrid('setCell', filas[i], "pareto", (data.preciototal / preciototal1) * 100);

    }
}

function GridCotizacion(accion, op, filtro) {

    var grid_crear_cotizacion = jQuery("#tbl_cotizacion");
    var apugrupo = obtenerAPUAsoc();
    var metcalc = $('#metcalc').val();
    var id_rel_actividades_apu = $('#id_rel_actividades_apu').val();
    var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=15&apugrupo=' + apugrupo + '&metcalc=' + metcalc + '&id=' + accion + '&op=' + op + '&filtro=' + filtro + '&id_rel_actividades_apu=' + id_rel_actividades_apu;
    //grid_crear_cotizacion.jqGrid('GridDestroy');
    if ($("#gview_tbl_cotizacion").length) {
        reloadGridCotizacion(apugrupo, metcalc, accion, op, filtro, id_rel_actividades_apu);
    } else {
        grid_crear_cotizacion.jqGrid({
            url: url,
            datatype: 'json',
            caption: "Cotizacion",
            height: '550',
            width: '1608',
            colNames: ['Id', 'Met.Calculo', 'Tipo Insumo', 'Id Tipo Insumo', 'Id Insumo', 'Cod. Insumo', 'Descripcion Insumo', 'Cantidad', 'Unidad Medida', 'Id Unidad Medida', 'Precio Predictivo',
                'Predictivo Total', 'Peso', 'Ultimo Proveedor Compra', 'Nit Proveedor', 'Val. Maximo. Comprado', 'Costo Unitario', 'Costo Personalizado', 'SubTotal', 'Proveedor', 'Rent.Prov.', 'Val.Rent.Prov.', 'Rent.Sel.', 'Val.Rent.Sel.',
                'Total Cliente', 'Estado', 'Rent. Contratista', 'Valor Contratista', 'Pareto Contratista', 'Rent. Esquema', 'Valor Esquema', 'Rent.Global', 'Valor Total'],
            colModel: [
                {name: 'id', index: 'id', align: 'center', width: 100, hidden: true},
                {name: 'metodocalculo', index: 'metodocalculo', hidden: true, align: 'center', width: 100, search: false, editable: true, edittype: 'select', editrule: {required: true},
                    editoptions: {
                        value: listarMetodosCal(),
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {

                                        var rowid = e.target.id.replace("_metodocalculo", "");
                                        var val = e.target.value.replace("_", "");
                                        var codinsumo = grid_crear_cotizacion.jqGrid('getCell', rowid, 'codinsumo');
                                        var cantidad = grid_crear_cotizacion.jqGrid('getCell', rowid, 'cantidad');

                                        var val_calculo = calcularMet(val, codinsumo);
                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "preciounitario", val_calculo);
                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "preciototal", val_calculo * cantidad);

                                        var preciototal = grid_crear_cotizacion.jqGrid('getCol', 'preciototal', false, 'sum');
                                        jQuery("#tbl_cotizacion").jqGrid('footerData', 'set', {preciototal: preciototal});

                                        CalcularPareto();

                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]

                    }
                },
                {name: 'tipoinsumo', index: 'tipoinsumo', align: 'center', width: 100},
                {name: 'id_tipo_insumo', index: 'id_tipo_insumo', hidden: true, align: 'center', width: 100, sorttype: 'currency'},
                {name: 'id_insumo', index: 'id_insumo', align: 'center', width: 80, sorttype: 'numeric', hidden: true},
                {name: 'codinsumo', index: 'codinsumo', align: 'center', width: 100},
                {name: 'descripcionins', index: 'descripcionins', width: 350, edittype: 'text', resizable: false, align: 'center'},
                {name: 'cantidad', index: 'cantidad', align: 'center', width: 50, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}
                },
                {name: 'unidadmedida', index: 'unidadmedida', align: 'center', width: 80},
                {name: 'idunidadmedida', hidden: true, index: 'idunidadmedida', align: 'center', width: 80},
                {name: 'pareto', hidden: true, index: 'pareto', align: 'center', width: 100, formatter: 'currency', formatoptions: {suffix: '%', decimalPlaces: 1}, sorttype: 'currency'},
                {name: 'preciounitario', hidden: true, index: 'preciounitario', align: 'center', width: 100, formatter: 'currency', sorttype: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'preciototal', hidden: true, index: 'preciototal', align: 'center', width: 100, formatter: 'currency', sorttype: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'proveedor', index: 'proveedor', hidden: true, align: 'center', width: 250},
                {name: 'nitproveedor', index: 'nitproveedor', hidden: true, align: 'center', width: 130},
                {name: 'preciohistorico', hidden: true, index: 'preciohistorico', align: 'center', width: 120, formatter: 'currency', sorttype: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'preciounitariofinal', index: 'preciounitariofinal', align: 'center', width: 100, sorttype: 'numeric', formatter: 'currency', sorttype: 'currency',
                            formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "},
                    editoptions: {
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                           console.log(e.target.id);
                                        var rowid = e.target.id.replace("_preciounitariofinal", "");
                                        var val = e.target.value.replace("_", "");
                                        var valor = val * grid_crear_cotizacion.jqGrid('getCell', rowid, 'cantidad');
                                        var val_prov = grid_crear_cotizacion.jqGrid('getCell', rowid, 'rentabilidadprov');
                                        var val_sel = grid_crear_cotizacion.jqGrid('getCell', rowid, 'rentabilidadsel');
                                        var idinsumo = grid_crear_cotizacion.jqGrid('getCell', rowid, 'id_insumo');

                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "totalfinal", valor);

                                        var totalProv = (valor * val_prov / 100);

                                        var totalSel = ((valor + totalProv) * val_sel / 100);

                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "valrentabilidadprov", totalProv);

                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "valrentabilidadsel", totalSel);

                                        var totalparc = (valor * val_prov / 100) + valor;

                                        var total = (totalparc * val_sel / 100) + totalparc;

                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "totalcliente", total);

                                        var totalfinal = grid_crear_cotizacion.jqGrid('getCol', 'totalfinal', false, 'sum');
                                        jQuery("#tbl_cotizacion").jqGrid('footerData', 'set', {totalfinal: totalfinal});

                                        var totalcliente = grid_crear_cotizacion.jqGrid('getCol', 'totalcliente', false, 'sum');
                                        jQuery("#tbl_cotizacion").jqGrid('footerData', 'set', {totalcliente: totalcliente});

                                        //$('#valcotizacion').val('$ ' + numberConComas(totalcliente.toFixed(2)));
                                        $('#valcotizacion').val(totalcliente.toFixed(2));
                                        resta();
                                        ActualizarValDetalle(idinsumo, val);


                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]

                    }

                },
                {name: 'costoperso', index: 'costoperso', editable: true, align: 'center', width: 100, sorttype: 'numeric', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "},
                    editoptions: {
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {

                                        var rowid = e.target.id.replace("_costoperso", "");
                                        var val = e.target.value.replace("_", "");
                                        var valor = val * grid_crear_cotizacion.jqGrid('getCell', rowid, 'cantidad');

                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "totalfinal", valor);

                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }

                },
                {name: 'totalfinal', index: 'totalfinal', align: 'center', width: 100, sorttype: 'numeric', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'proveedorfinal', index: 'proveedorfinal', hidden: true, align: 'center', width: 160},
                {name: 'rentabilidadprov', index: 'rentabilidadprov', hidden: true, align: 'center', width: 50, formatter: 'currency', formatoptions: {suffix: '%', decimalPlaces: 0}, sorttype: 'currency'},
                {name: 'valrentabilidadprov', index: 'valrentabilidadprov', hidden: true, align: 'center', width: 100, editable: true, formatter: 'currency', sorttype: 'currency'},
                {name: 'rentabilidadsel', index: 'rentabilidadsel', hidden: true, align: 'center', width: 50, formatter: 'currency', formatoptions: {suffix: '%', decimalPlaces: 0}, sorttype: 'currency'},
                {name: 'valrentabilidadsel', index: 'valrentabilidadsel', hidden: true, align: 'center', width: 100, editable: true, formatter: 'currency', sorttype: 'currency'},
                {name: 'totalcliente', index: 'totalcliente', hidden: true, align: 'center', width: 100, formatter: 'currency', sorttype: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'estado', hidden: true, index: 'estado', search: false, align: 'center', width: 50},
                {name: 'perccont', index: 'perccont', editable: true, search: false, align: 'center', sorttype: 'numeric', width: 80,
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "%  "},
                    editoptions: {
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {

                                        var rowid = e.target.id.replace("_perccont", "");
                                        var val = e.target.value.replace("_", "");
                                        var valor = (((val * 1) + 100) * grid_crear_cotizacion.jqGrid('getCell', rowid, 'totalfinal')) / 100;


                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "valcont", valor);



                                        var totalfinal = grid_crear_cotizacion.jqGrid('getCell', rowid, 'totalfinal');
                                        var valesq = grid_crear_cotizacion.jqGrid('getCell', rowid, 'valesq');
                                        var valcont = grid_crear_cotizacion.jqGrid('getCell', rowid, 'valcont');

                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "valortotal", (totalfinal * 1) + (valesq * 1) + (valcont * 1));

                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }


                },
                {name: 'valcont', index: 'valcont', search: false, align: 'center', width: 80, sorttype: 'currency', formatter: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'pareto_Contratista', index: 'pareto_Contratista', search: false, align: 'center', sorttype: 'number', width: 80,
                    formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "%  "}

                },
                {name: 'percesq', index: 'percesq', editable: false, search: false, align: 'center', sorttype: 'currency', width: 80,
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "%  "},
                    editoptions: {
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {

                                        var rowid = e.target.id.replace("_percesq", "");
                                        var val = e.target.value.replace("_", "");
                                        var valor = (val * grid_crear_cotizacion.jqGrid('getCell', rowid, 'totalfinal')) / 100;


                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "valesq", valor);

                                        var totalfinal = grid_crear_cotizacion.jqGrid('getCell', rowid, 'totalfinal');
                                        var valesq = grid_crear_cotizacion.jqGrid('getCell', rowid, 'valesq');
                                        var valcont = grid_crear_cotizacion.jqGrid('getCell', rowid, 'valcont');

                                        grid_crear_cotizacion.jqGrid('setCell', rowid, "valortotal", (totalfinal * 1) + (valesq * 1) + (valcont * 1));

                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }




                },
                {name: 'valesq', index: 'valesq', search: false, align: 'center', width: 80, sorttype: 'numeric', formatter: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                },
                {name: 'rent_global', index: 'rent_global', sortable: true, editable: false, width: '80px', align: 'center', sorttype: 'numeric', search: false,
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: "% "}
                },
                {name: 'valortotal', index: 'valortotal', align: 'center', width: 100, formatter: 'currency', sorttype: 'numeric', sortable: true, hidden: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
                }
            ],
            rowNum: 10000,
            rowTotal: 10000000,
            pager: '#page_cotizacion',
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            rownumbers: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            userDataOnFooter: true,
            reloadAfterSubmit: true,
            multiselect: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            afterSaveCell: function (rowid, name, val, iRow, iCol) {
                alert(1);

            },
            gridComplete: function () {


                var ids = grid_crear_cotizacion.jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];

                    var estado = grid_crear_cotizacion.getRowData(cl).estado;
                    var id_insumo = grid_crear_cotizacion.getRowData(cl).id_insumo;
                    if (estado === '0') {
                        be = "<img src='/fintra/images/botones/iconos/chulo.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Pendiente'  onclick=\"mensajeConfirmAction1('Esta seguro de cambiar el estado a terminado?','250','150',ActualizarEstadoTerminadoDetalle,'" + id_insumo + "');\">";
                    } else {
                        be = "<img src='/fintra/images/botones/iconos/obligatorio.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Terminado'  onclick=\"mensajeConfirmAction1('Esta seguro de cambiar el estado a pendiente?','250','150',ActualizarEstadoPendienteDetalle,'" + id_insumo + "');\">";
                    }

                    grid_crear_cotizacion.jqGrid('setRowData', ids[i], {estado: be});

                    if (parseFloat(grid_crear_cotizacion.getRowData(cl).costoperso) > parseFloat(grid_crear_cotizacion.getRowData(cl).preciounitariofinal)) {
                        grid_crear_cotizacion.jqGrid('setCell', cl, "costoperso", "", {'background-color': '#90CC00', 'background-image': 'none'});
                    } else if (parseFloat(grid_crear_cotizacion.getRowData(cl).costoperso) < parseFloat(grid_crear_cotizacion.getRowData(cl).preciounitariofinal)) {
                        grid_crear_cotizacion.jqGrid('setCell', cl, "costoperso", "", {'background-color': '#EE3030', 'background-image': 'none'});
                    }

                    var valortotal = grid_crear_cotizacion.getRowData(cl).valortotal;
                    var totalfinal = grid_crear_cotizacion.getRowData(cl).totalfinal;

                    var valesq = grid_crear_cotizacion.getRowData(cl).valesq;
                    var valcont = grid_crear_cotizacion.getRowData(cl).valcont;
                    var grantotal = 0;
                    if ((valesq >= 0) && (valesq < 1)) {
                        grantotal = valcont;
                    } else {
                        grantotal = valesq;
                    }
                    grid_crear_cotizacion.jqGrid('setCell', cl, 'pareto_Contratista', (valcont * 100 / $('#valor_contratista_proyecto').val()));
//                    var totalfinal = grid_crear_cotizacion.jqGrid('getCell', rowid, 'totalfinal');
//                    var valesq = grid_crear_cotizacion.jqGrid('getCell', rowid, 'valesq');
//                    var valcont = grid_crear_cotizacion.jqGrid('getCell', rowid, 'valcont');



                    grid_crear_cotizacion.jqGrid('setCell', cl, 'rent_global', ((valesq * 100 / totalfinal) - 100));


                    grid_crear_cotizacion.jqGrid('setCell', cl, "valortotal", grantotal);


                    //grid_crear_cotizacion.jqGrid('setRowData', ids[i], {estado: be});
                }
                //suma();
                var cantidad = grid_crear_cotizacion.jqGrid('getCol', 'cantidad', false, 'sum');
                var preciototal = grid_crear_cotizacion.jqGrid('getCol', 'preciototal', false, 'sum');
                var pareto = grid_crear_cotizacion.jqGrid('getCol', 'pareto', false, 'sum');
                var totalcliente = grid_crear_cotizacion.jqGrid('getCol', 'totalcliente', false, 'sum');
                totalfinal = grid_crear_cotizacion.jqGrid('getCol', 'totalfinal', false, 'sum');
                var valortotall = grid_crear_cotizacion.jqGrid('getCol', 'valortotal', false, 'sum');
                var valesqq = grid_crear_cotizacion.jqGrid('getCol', 'valesq', false, 'sum');
                var valcontt = grid_crear_cotizacion.jqGrid('getCol', 'valcont', false, 'sum');
                var paretoo = grid_crear_cotizacion.jqGrid('getCol', 'pareto_Contratista', false, 'sum');

                var rrent_global = ((valesqq * 100 / totalfinal) - 100);
                $('#valcotizacion').val(totalcliente.toFixed(2));
                resta();
                grid_crear_cotizacion.jqGrid('footerData', 'set', {descripcionins: 'TOTAL', cantidad: cantidad, preciototal: preciototal, pareto: pareto, totalfinal: totalfinal, totalcliente: totalcliente, valortotal: valortotall, valesq: valesqq, valcont: valcontt, pareto_Contratista: paretoo, rent_global: rrent_global});



            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                grid_crear_cotizacion.jqGrid('editRow', rowid, true, function () {
                    $("input, select", e.target).focus();
                }, null, null, {}, function (rowid) {

                    var percesq = grid_crear_cotizacion.jqGrid('getCell', rowid, 'percesq');
                    var perccont = grid_crear_cotizacion.jqGrid('getCell', rowid, 'perccont');

                    var totalfinal = grid_crear_cotizacion.jqGrid('getCell', rowid, 'totalfinal');

                    grid_crear_cotizacion.jqGrid('setCell', rowid, "valcont", (totalfinal * ((perccont * 1) + 100)) / 100);



                    var valcont = grid_crear_cotizacion.jqGrid('getCell', rowid, 'valcont');
                    grid_crear_cotizacion.jqGrid('setCell', rowid, "valesq", (valcont * (percesq * 1 + 100) / 100));
                    var valesq = grid_crear_cotizacion.jqGrid('getCell', rowid, 'valesq');


                    grid_crear_cotizacion.jqGrid('setCell', rowid, "valortotal", (totalfinal * 1) + (valesq * 1) + (valcont * 1));

                    var valtotal = grid_crear_cotizacion.jqGrid('getCell', rowid, 'valortotal');
                    var costoperso = grid_crear_cotizacion.jqGrid('getCell', rowid, 'costoperso');
                    var idinsumo = grid_crear_cotizacion.jqGrid('getCell', rowid, 'id_insumo');
                    var idunidadmedida = grid_crear_cotizacion.jqGrid('getCell', rowid, 'idunidadmedida');

                    //suma();
                    var cantidad = grid_crear_cotizacion.jqGrid('getCol', 'cantidad', false, 'sum');
                    var preciototal = grid_crear_cotizacion.jqGrid('getCol', 'preciototal', false, 'sum');
                    var pareto = grid_crear_cotizacion.jqGrid('getCol', 'pareto', false, 'sum');
                    var totalcliente = grid_crear_cotizacion.jqGrid('getCol', 'totalcliente', false, 'sum');
                    var totalfinal = grid_crear_cotizacion.jqGrid('getCol', 'totalfinal', false, 'sum');
                    var valortotall = grid_crear_cotizacion.jqGrid('getCol', 'valortotal', false, 'sum');
                    var valesqq = grid_crear_cotizacion.jqGrid('getCol', 'valesq', false, 'sum');
                    var valcontt = grid_crear_cotizacion.jqGrid('getCol', 'valcont', false, 'sum');
                    var paretoo = grid_crear_cotizacion.jqGrid('getCol', 'pareto_Contratista', false, 'sum');
                    var rrent_global = ((valesqq * 100 / totalfinal) - 100);

                    grid_crear_cotizacion.jqGrid('setCell', rowid, 'rent_global', rrent_global);

                    $('#valcotizacion').val(totalcliente.toFixed(2));
                    resta();
                    grid_crear_cotizacion.jqGrid('footerData', 'set', {descripcionins: 'TOTAL', cantidad: cantidad, preciototal: preciototal, pareto: pareto, totalfinal: totalfinal, totalcliente: totalcliente, valortotal: valortotall, valesq: valesqq, valcont: valcontt, pareto_Contratista: paretoo, rent_global: rrent_global});


                    if (parseFloat(grid_crear_cotizacion.getRowData(rowid).costoperso) > parseFloat(grid_crear_cotizacion.getRowData(rowid).preciounitariofinal)) {
                        grid_crear_cotizacion.jqGrid('setCell', rowid, "costoperso", "", {'background-color': '#90CC00', 'background-image': 'none'});
                    } else if (parseFloat(grid_crear_cotizacion.getRowData(rowid).costoperso) < parseFloat(grid_crear_cotizacion.getRowData(rowid).preciounitariofinal)) {
                        grid_crear_cotizacion.jqGrid('setCell', rowid, "costoperso", "", {'background-color': '#EE3030', 'background-image': 'none'});
                    } else {
                        grid_crear_cotizacion.jqGrid('setCell', rowid, "costoperso", "", {'background-color': '', 'background-image': 'none'});
                    }


                    ActualizarCamposNuevos(idinsumo, costoperso, percesq, valesq, perccont, valcont, valtotal, idunidadmedida);
                    actualizarGrillas2();


                });
                return;
            },
            restoreAfterError: true

        });
        grid_crear_cotizacion.navGrid("#page_cotizacion", {add: false, edit: false, del: false, search: false, refresh: true});
        grid_crear_cotizacion.navButtonAdd('#page_cotizacion', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                cargando_toggle();
                guardarCotizacion();
                cargando_toggle();
            },
            position: "first"
        });

    }
}

function guardarCotizacion() {
    var grid = jQuery("#tbl_cotizacion"),
            grid_detalle = jQuery("#tbl_cotizacion_detalle"),
            filas = grid.jqGrid('getDataIDs'),
            filas_detalle = grid_detalle.jqGrid('getDataIDs'),
            data, error = false,
            CodCliente = $('#CodCliente').val(),
            NombreCliente = $('#NombreCliente').val(),
            CodCotizacion = $('#CodCotizacion').val(),
            fecha = $('#fecha').val(),
            visualizacion = $('#visualizacion').val(),
            modalidad = $('#modalidad').val(),
            valcotizacion = $('#valcotizacion').val(),
            valdesc = $('#valdesc').val(),
            subtotal = $('#subtotal').val(),
            perc_iva = $('#perc_iva').val(),
            valiva = $('#valiva').val(),
            perc_admon = $('#perc_admon').val(),
            val_admon = $('#val_admon').val(),
            perc_imprevisto = $('#perc_imprevisto').val(),
            val_imprevisto = $('#val_imprevisto').val(),
            perc_utilidad = $('#perc_utilidad').val(),
            val_utilidad = $('#val_utilidad').val(),
            perc_aiu = $('#perc_aiu').val(),
            val_aiu = $('#val_aiu').val(),
            val_total = $('#val_total').val(),
            anticipo = $('#anticipo').val(),
            perc_anticipo = $('#perc_anticipo').val(),
            val_anticipo = $('#val_anticipo').val(),
            retegarantia = $('#retegarantia').val(),
            perc_rete = $('#perc_rete').val(),
            idaccion = $('#idaccion').val(),
            val_material = 0,
            val_mano_obra = 0,
            val_equipos = 0,
            val_herramientas = 0,
            val_transporte = 0,
            val_tramites = 0,
            existe = $('#existe').val(),
            id = $('#id').val();

    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);
        if (data.tipoinsumo === 'MATERIAL') {
            val_material = val_material * 1 + data.totalcliente * 1;
        }
        if (data.tipoinsumo === 'EQUIPO') {
            val_equipos = val_equipos * 1 + data.totalcliente * 1;
        }
        if (data.tipoinsumo === 'MANO DE OBRA') {
            val_mano_obra = val_mano_obra * 1 + data.totalcliente * 1;
        }
        if (data.tipoinsumo === 'HERRAMIENTAS') {
            val_herramientas = val_herramientas * 1 + data.totalcliente * 1;
        }
        if (data.tipoinsumo === 'TRANSPORTES') {
            val_transporte = val_transporte * 1 + data.totalcliente * 1;
        }
        if (data.tipoinsumo === 'PERMISOS Y TRAMITES') {
            val_tramites = val_tramites * 1 + data.totalcliente * 1;
        }

        grid.saveRow(filas[i]);
    }

    filas = grid.jqGrid('getRowData');
    filas_detalle = grid_detalle.jqGrid('getRowData');
    if (filas_detalle.length === 0) {
        error = true;
        mensajesDelSistema('Existe un error en el detalle de la cotizacion.', '300', 'auto', false);
    }


    if (error)
        return;

    $.ajax({
        url: '/fintra/controlleropav?estado=Cotizacion&accion=Sl',
        datatype: 'json',
        type: 'POST',
        data: {opcion: 19, informacion: JSON.stringify({rows: filas}), informacion_detallada: JSON.stringify({rows: filas_detalle}),
            CodCliente: CodCliente,
            NombreCliente: NombreCliente,
            CodCotizacion: CodCotizacion,
            fecha: fecha,
            visualizacion: visualizacion,
            modalidad: modalidad,
            valcotizacion: valcotizacion,
            valdesc: valdesc,
            subtotal: subtotal,
            perc_iva: perc_iva,
            valiva: valiva,
            perc_admon: perc_admon,
            val_admon: val_admon,
            perc_imprevisto: perc_imprevisto,
            val_imprevisto: val_imprevisto,
            perc_utilidad: perc_utilidad,
            val_utilidad: val_utilidad,
            perc_aiu: perc_aiu,
            val_aiu: val_aiu,
            val_total: val_total,
            anticipo: anticipo,
            perc_anticipo: perc_anticipo,
            val_anticipo: val_anticipo,
            retegarantia: retegarantia,
            perc_rete: perc_rete,
            idaccion: idaccion,
            val_material: val_material,
            val_mano_obra: val_mano_obra,
            val_equipos: val_equipos,
            val_herramientas: val_herramientas,
            val_transporte: val_transporte,
            val_tramites: val_tramites,
            existe: existe,
            id: id},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema1(json.error, '204', '140');
                } else {

                    actualizarGrillas();
                    insertar_rentabilidades_cabecera();

                }
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {

        }
    });
}

//function mensajesDelSistema(msj, width, height, swHideDialog) {
//    mostrarContenido('dialogMsgMeta');
//    if (swHideDialog) {
//        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
//    } else {
//        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
//    }
//    $("#dialogMsgMeta").dialog({
//        width: width,
//        height: height,
//        show: "scale",
//        hide: "scale",
//        resizable: false,
//        position: "center",
//        modal: true,
//        closeOnEscape: false,
//        buttons: {//crear bot�n cerrar
//            "Aceptar": function () {
//                $(this).dialog("destroy");
//                window.close();
//            }
//        }
//    });
//}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function CargarAPU() {

    $('#div_apus').fadeIn("slow");
    AbrirDivApus();
    cargarComboGrupo1([]);
    CargarGridApus();
    CargarGridApusAsoc();
}

function AbrirDivApus() {
    $("#div_apus").dialog({
        width: 1000,
        height: 545,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'APUs',
        closeOnEscape: false,
        buttons: {
            "Agregar": function () {
                GridCotizacion();
                $(this).dialog("destroy");
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function obtenerAPUAsoc() {
    var grid = jQuery("#tbl_apus_asoc")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false, concat = '', coma = '';
    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);
        concat = concat + coma + data.id;
        coma = ',';
    }
    //alert(concat);
    return concat;
}

function CargarGridApus() {
    var grupo_apu = $('#grupo_apu1').val();
    var apugrupo = obtenerAPUAsoc();
    var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=18';
    //$('#tbl_apus').jqGrid('GridUnload');
    if ($("#gview_tbl_apus").length) {
        refrescarGridGrupoAPU(grupo_apu);
    } else {
        jQuery("#tbl_apus").jqGrid({
            caption: 'Grid Apu',
            url: url,
            datatype: 'json',
            height: 300,
            width: 450,
            colNames: ['Id', 'Descripcion APU', 'Unidad de Medida'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombreapu', index: 'nombreapu', sortable: true, align: 'center', width: '600'},
                {name: 'nombre_unidad', index: 'nombre_unidad', search: false, sortable: true, align: 'center', width: '250'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            multiselect: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_apus',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                sync: false,
                data: {
                    grupo_apu: grupo_apu,
                    apugrupo: apugrupo
                }
            },
            ondblClickRow: function (id) {
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });
        jQuery("#tbl_apus").navGrid("#page_apus", {add: false, edit: false, del: false, search: false, refresh: false});
        jQuery("#tbl_apus").navButtonAdd('#page_apus', {
            caption: "Crear APU",
            title: "Crear APU",
            buttonicon: "ui-icon-new",
            onClickButton: function () {
                crearAPU();
            }
        });
        jQuery("#tbl_apus").jqGrid('filterToolbar', {stringResult: true, searchOnEnter: false, searchOperators: true});
        document.getElementById("gs_nombreapu").style.width = '258px';
        //document.getElementById("gs_nombreapu").style.toUpperCase();
    }
}

/*function crearAPU() {
 var url = '/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/&pagina=CrearAPU.jsp';
 //?idaccion='+idaccion;
 window.showModalDialog(url);
 }*/

function refrescarGridGrupoAPU(grupo_apu) {
    var apugrupo = obtenerAPUAsoc();
    var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=18&grupo_apu=' + grupo_apu + '&apugrupo=' + apugrupo;
    jQuery("#tbl_apus").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#tbl_apus').trigger("reloadGrid");

}

function CargarGridApusAsoc() {
    //var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=17';
    $('#tbl_apus_asoc').jqGrid('GridUnload');
    if ($("#gview_tbl_apus_asoc").length) {
        //refrescarGridProcesosMeta();
    } else {
        jQuery("#tbl_apus_asoc").jqGrid({
            caption: 'Grid Apu Asociados',
            url: '',
            datatype: 'local',
            height: 326,
            width: 450,
            colNames: ['Id', 'Descripcion APU', 'Unidad de Medida'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombreapu', index: 'nombreapu', sortable: true, align: 'center', width: '600'},
                {name: 'nombre_unidad', index: 'nombre_unidad', sortable: true, align: 'center', width: '250'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            multiselect: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_apus_asoc',
            /*jsonReader: {
             root: 'rows',
             repeatitems: false,
             id: '0'
             },*/
            ajaxGridOptions: {
                async: false,
                data: {
                    //subcategoria: $('#sub').val()
                }
            },
            ondblClickRow: function (id) {
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });

    }
}

function suma() {

    var grid = jQuery("#tbl_cotizacion")
            , filas = grid.jqGrid('getDataIDs'),
            sum = 0, data;

    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);

        grid.jqGrid('setCell', filas[i], "preciototal", data.preciounitario * data.cantidad);

    }

}

function formatoJ(input)
{
    var num = input.value.replace(/\./g, '');
    if (!isNaN(num)) {
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/, '');
        input.value = num;
    }

    else {
        alert('Solo se permiten numeros');
        input.value = input.value.replace(/[^\d\.]*/g, '');
    }
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function resta() {
    var rest = numberSinComas($('#valcotizacion').val().replace('$', '')) - numberSinComas($('#valdesc').val());
    $('#subtotal').val(rest.toFixed(2));
    $('#valiva').val((rest * $('#perc_iva').val() / 100).toFixed(2));
    $('#val_admon').val(($('#subtotal').val() * ($('#perc_admon').val() * 1) / 100).toFixed(2));
    $('#val_imprevisto').val(($('#subtotal').val() * ($('#perc_imprevisto').val() * 1) / 100).toFixed(2));
    $('#val_utilidad').val(($('#subtotal').val() * ($('#perc_utilidad').val() * 1) / 100).toFixed(2));
    $('#perc_aiu').val(($('#perc_admon').val() * 1) + ($('#perc_imprevisto').val() * 1) + ($('#perc_utilidad').val() * 1));
    $('#val_aiu').val((rest * $('#perc_aiu').val() / 100).toFixed(2));
    $('#val_total').val(($('#subtotal').val() * 1 + $('#valiva').val() * 1 + $('#val_aiu').val() * 1).toFixed(2));

    $('#val_anticipo').val(($('#val_total').val() * $('#perc_anticipo').val() / 100).toFixed(2));

}

function cargarinfo(accion) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Cotizacion&accion=Sl',
        dataType: 'json',
        async: false,
        data: {
            opcion: 16,
            id_accion: accion
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                //alert(json.existe);
                $('#existe').val(json.existe);
                if (json.existe === '0') {
                    $('#CodCliente').val(json.id_cliente);
                    $('#NombreCliente').val(json.nomcli);
                    $('#CodCotizacion').val(json.serie_cot);
                } else {
                    $('#id').val(json.id);
                    $('#CodCliente').val(json.cod_cli);
                    $('#NombreCliente').val(json.nonmbre_cliente);
                    $('#CodCotizacion').val(json.no_cotizacion);
                    $('#fecha').val(json.vigencia_cotizacion);
                    $('#visualizacion').val(json.forma_visualizacion);
                    $('#modalidad').val(json.modalidad_comercial);
                    $('#valcotizacion').val(json.valor_cotizacion);
                    $('#valdesc').val(json.valor_descuento);
                    $('#subtotal').val(json.subtotal);
                    $('#perc_iva').val(json.perc_iva);
                    $('#valiva').val(json.valor_iva);
                    $('#perc_admon').val(json.perc_administracion);
                    $('#val_admon').val(json.administracion);
                    $('#perc_imprevisto').val(json.perc_imprevisto);
                    $('#val_imprevisto').val(json.imprevisto);
                    $('#perc_utilidad').val(json.perc_utilidad);
                    $('#val_utilidad').val(json.utilidad);
                    $('#perc_aiu').val(json.perc_aiu);
                    $('#val_aiu').val(json.valor_aiu);
                    $('#val_total').val(json.total);
                    $('#anticipo').val(json.anticipo);
                    $('#perc_anticipo').val(json.perc_anticipo);
                    $('#val_anticipo').val(json.valor_anticipo);
                    $('#retegarantia').val(json.retegarantia);
                    $('#perc_rete').val(json.perc_retegarantia);


                    var sel = document.getElementById("cbx_rent_esquema");

                    for (var i = 0; i < sel.length; i++) {

                        var opt = sel[i];

                        console.log(json.distribucion_rentabilidad_esquema.includes(opt.text));
                        if (json.distribucion_rentabilidad_esquema.includes(opt.text))
                        {
                            $('#cbx_rent_esquema').val(opt.value);

                        }
                    }

                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function habdesMod() {

    if ($('#modalidad').val() === '0') {
        $('#perc_iva').val(16);
        $('#perc_admon').val(0);
        $('#perc_imprevisto').val(0);
        $('#perc_utilidad').val(0);
        document.getElementById("perc_admon").disabled = true;
        document.getElementById("perc_imprevisto").disabled = true;
        document.getElementById("perc_utilidad").disabled = true;

    } else {
        $('#perc_iva').val(0);
        document.getElementById("perc_admon").disabled = false;
        document.getElementById("perc_imprevisto").disabled = false;
        document.getElementById("perc_utilidad").disabled = false;
    }

    resta();


}

function habdesAnt() {

    if ($('#anticipo').val() === '1') {
        $('#perc_anticipo').val(0);
        $('#val_anticipo').val(0);
        document.getElementById("perc_anticipo").disabled = false;

    } else {
        $('#perc_anticipo').val(0);
        $('#val_anticipo').val(0);
        document.getElementById("perc_anticipo").disabled = true;
    }

    resta();

}

function habdesRet() {

    if ($('#retegarantia').val() === '1') {
        $('#perc_rete').val(0);
        document.getElementById("perc_rete").disabled = false;

    } else {
        $('#perc_rete').val(0);
        document.getElementById("perc_rete").disabled = true;
    }

}

function AgregarGridAso() {

    var selectRows = jQuery("#tbl_apus").jqGrid('getGridParam', 'selarrrow');
    for (var i = 0; i < selectRows.length; i++) {

        var fila = jQuery("#tbl_apus").jqGrid('getRowData', selectRows[i]);
        jQuery("#tbl_apus_asoc").jqGrid('addRowData', selectRows[i], fila);
    }
    QuitarRowGridVal(selectRows);
}


function QuitarRowGridVal(selectRows) {

    for (var k = 0; k < selectRows.length; k++) {
        jQuery("#tbl_apus").jqGrid('delRowData', selectRows[k]);

        selectRows = jQuery("#tbl_apus").jqGrid('getGridParam', 'selarrrow');


        k = -1;
    }

}

function AgregarGridNoAso() {

    var selectRows = jQuery("#tbl_apus_asoc").jqGrid('getGridParam', 'selarrrow'), cadena = "", coma = "";
    for (var i = 0; i < selectRows.length; i++) {

        var fila = jQuery("#tbl_apus_asoc").jqGrid('getRowData', selectRows[i]);
        //////////////////////////
        var fila1 = jQuery("#tbl_apus_asoc").getRowData(selectRows[i]);

        /*if (fila1['idrel'] !== "") {
         cadena = cadena + coma + fila1['valor_xdefecto'];
         coma = ",";
         }
         
         //////////////////////////
         if (cadena === "") {*/
        jQuery("#tbl_apus").jqGrid('addRowData', selectRows[i], fila);
        //}
    }

    if (cadena === "") {
        QuitarRowGridValAso(selectRows);
    } else {
        alert('No se pueden desasociar los registros ' + cadena);
    }
}


function QuitarRowGridValAso(selectRows) {

    for (var k = 0; k < selectRows.length; k++) {
        jQuery("#tbl_apus_asoc").jqGrid('delRowData', selectRows[k]);

        selectRows = jQuery("#tbl_apus_asoc").jqGrid('getGridParam', 'selarrrow');


        k = -1;
    }

}

function NumCheck(e, field) {
    key = e.keyCode ? e.keyCode : e.which;
    if (key === 8)
        return true;
    if (key > 47 && key < 58) {
        if (field.value === "")
            return true;
        regexp = /.[0-9]{5}$/;
        return !(regexp.test(field.value));
    }
    if (key === 46) {
        if (field.value === "")
            return false;
        regexp = /^[0-9]+$/;
        return regexp.test(field.value);
    }
    return false;
}

function crearAPU() {
    $('#nomapu').val('');
    $('#tbl_insumos').jqGrid('GridUnload');
    $('#div_apu').fadeIn('slow');
    cargarComboGrupo([]);
    cargarComboUnidadM([]);
    AbrirDivAPU();
    GridInsumos();
}

function decimales(e, thi) {
// Backspace = 8, Enter = 13, ?0? = 48, ?9? = 57, ?.? = 46
    var field = thi;
    key = e.keyCode ? e.keyCode : e.which;

    if (key === 8)
        return true;
    if (key > 47 && key < 58) {
        if (field.val() === "")
            return true;
        var existePto = (/[.]/).test(field.val());
        if (existePto === false) {
            regexp = /.[0-9]{10}$/;
        }
        else {
            regexp = /.[0-9]{2}$/;
        }

        return !(regexp.test(field.val()));
    }
    if (key === 46) {
        if (field.val() === "")
            return false;
        regexp = /^[0-9]+$/;
        return regexp.test(field.val());
    }
    return false;
}

function cargaDetalleCotizacion(accion) {
    var grid_detalle = jQuery("#tbl_cotizacion_detalle");
    var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=22&id=' + accion;
    //grid_crear_cotizacion.jqGrid('GridDestroy');
    if ($("#gview_tbl_cotizacion_detalle").length) {
        reloadcargaDetalleCotizacion(accion);
    } else {
        grid_detalle.jqGrid({
            url: url,
            datatype: 'json',
            caption: "Detalle Cotizacion",
            height: '550',
            width: '1805',
            colNames: ['Id', 'Id Actividad', 'Id Apu', 'Id Insumo', 'Cantidad Insumo', 'Rendimiento Insumo', 'Cantidad Apu', 'Valor Insumo', 'Estado',
                'Costo Personalizado', 'Porc. Esquema', 'Valor Esquema', 'Porc. Contratista', 'Valor Contratista', 'Valor Total', 'id_unidad_medida'],
            colModel: [
                {name: 'id', index: 'id', align: 'center', width: 160, key: true},
                {name: 'id_actividad', index: 'id_actividad', align: 'center', width: 160},
                {name: 'id_apu', index: 'id_apu', align: 'center', width: 50},
                {name: 'id_insumo', index: 'id_insumo', align: 'center', width: 100},
                {name: 'cantidad_insumo', index: 'cantidad_insumo', align: 'center', width: 50},
                {name: 'rendimiento_insumo', index: 'rendimiento_insumo', align: 'center', width: 100},
                {name: 'cantidad_apu', index: 'cantidad_apu', align: 'center', width: 100},
                {name: 'valor_insumo', index: 'valor_insumo', align: 'center', width: 100},
                {name: 'estado', index: 'estado', align: 'center', width: 100},
                {name: 'costoper', index: 'costoper', align: 'center', width: 100},
                {name: 'porcesq', index: 'porcesq', align: 'center', width: 100},
                {name: 'valoresq', index: 'valoresq', align: 'center', width: 100},
                {name: 'porccont', index: 'porccont', align: 'center', width: 100},
                {name: 'valorcont', index: 'valorcont', align: 'center', width: 100},
                {name: 'valortotal', index: 'valortotal', align: 'center', width: 100},
                {name: 'id_unidad_medida', index: 'valortotal', align: 'center', width: 100}
            ],
            rowNum: 10000,
            rowTotal: 10000000,
            pager: '#page_cotizacion_detalle',
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            rownumbers: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            userDataOnFooter: true,
            reloadAfterSubmit: true,
            multiselect: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            restoreAfterError: true

        });

    }
}

function reloadcargaDetalleCotizacion(accion) {
    var url = '/fintra/controlleropav?estado=Cotizacion&accion=Sl&opcion=22&id=' + accion;
    jQuery("#tbl_cotizacion_detalle").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#tbl_cotizacion_detalle').trigger("reloadGrid");

}

function ActualizarValDetalle(codinsumo, val_insumo) {
    var grid = jQuery("#tbl_cotizacion_detalle"),
            filas = grid.jqGrid('getDataIDs'),
            data;

    //var preciototal1 = grid.jqGrid('getCol', 'preciototal', false, 'sum');

    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);

        var id_insumo = grid.jqGrid('getCell', filas[i], 'id_insumo');

        if (id_insumo === codinsumo) {

            grid.jqGrid('setCell', filas[i], "valor_insumo", val_insumo);

        }

    }
}

function ActualizarCamposNuevos(codinsumo, costoper, percesq, valesq, perccont, valcont, valtotal, idunidadmedida) {
    var grid = jQuery("#tbl_cotizacion_detalle"),
            filas = grid.jqGrid('getDataIDs'),
            data;

    //var preciototal1 = grid.jqGrid('getCol', 'preciototal', false, 'sum');

    var ids_apu_ok = $('#id_rel_actividades_apu').val();
    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);

        var id_insumo = grid.jqGrid('getCell', filas[i], 'id_insumo');
        var id_actividad = grid.jqGrid('getCell', filas[i], 'id_actividad');
        var id_unidad_medida = grid.jqGrid('getCell', filas[i], 'id_unidad_medida');
        var cantidad_insumo_ = parseFloat(numberSinComas(grid.jqGrid('getCell', filas[i], 'cantidad_insumo')));
        var rendimiento_insumo = parseFloat(numberSinComas(grid.jqGrid('getCell', filas[i], 'rendimiento_insumo')));
        var cantidad_apu = parseFloat(numberSinComas(grid.jqGrid('getCell', filas[i], 'cantidad_apu')));
        var xx = cantidad_insumo_ * rendimiento_insumo * cantidad_apu;
        var valor_con;
        var varlor_ess;
        if ((id_insumo == codinsumo) && (idunidadmedida == id_unidad_medida)) {
            var vids_apu_ok = ids_apu_ok.split(",");

            for (var j = 0; j < vids_apu_ok.length; j++) {
                if (vids_apu_ok[j] == id_actividad) {
                    grid.jqGrid('setCell', filas[i], "porccont", perccont);
                    valor_con = (costoper * xx * ((perccont * 1) + 100) / 100);
                    console.log('valor Contratista =' + valor_con);
                    grid.jqGrid('setCell', filas[i], "valorcont", valor_con);
                    varlor_ess = ((((grid.jqGrid('getCell', filas[i], 'porcesq') * 1) + 100) / 100) * valor_con);
                    console.log('valor valor es =' + varlor_ess);
                    grid.jqGrid('setCell', filas[i], "valoresq", varlor_ess);
                }
                grid.jqGrid('setCell', filas[i], "costoper", costoper);
            }
        }
    }
}

function ActualizarPorcentajes(tipo) {
    var grid = jQuery("#tbl_cotizacion"),
            filas = grid.jqGrid('getDataIDs'),
            data, valor = '';

    //var preciototal1 = grid.jqGrid('getCol', 'preciototal', false, 'sum');
    //alert(tipo);
    var percesq = $('#percesquema').val(), percont = $('#percecontratista').val();

    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);

        valor = '';

        if (tipo === 1) {
            grid.jqGrid('setCell', filas[i], "percesq", percesq);

            valor = (percesq * grid.jqGrid('getCell', filas[i], 'totalfinal')) / 100;

            grid.jqGrid('setCell', filas[i], "valesq", valor);


        } else {
            grid.jqGrid('setCell', filas[i], "perccont", percont);

            valor = (percont * grid.jqGrid('getCell', filas[i], 'totalfinal')) / 100;

            grid.jqGrid('setCell', filas[i], "valcont", valor);

        }

        var totalfinal = grid.jqGrid('getCell', filas[i], 'totalfinal');
        var valesq = grid.jqGrid('getCell', filas[i], 'valesq');
        var valcont = grid.jqGrid('getCell', filas[i], 'valcont');
        var codinsumo = grid.jqGrid('getCell', filas[i], 'id_insumo');
        var costoper = grid.jqGrid('getCell', filas[i], 'costoperso');
        var perccont = grid.jqGrid('getCell', filas[i], 'perccont');
        var valtotal = grid.jqGrid('getCell', filas[i], 'valortotal');
        var idunidadmedida = grid.jqGrid('getCell', filas[i], 'idunidadmedida');

        grid.jqGrid('setCell', filas[i], "valortotal", (totalfinal * 1) + (valesq * 1) + (valcont * 1));

        //}

        ActualizarCamposNuevos(codinsumo, costoper, percesq, valesq, perccont, valcont, valtotal, idunidadmedida);

    }
}

function ActualizarEstadoPendienteDetalle(codinsumo) {
    var grid = jQuery("#tbl_cotizacion_detalle"),
            filas = grid.jqGrid('getDataIDs'),
            data;

    //var preciototal1 = grid.jqGrid('getCol', 'preciototal', false, 'sum');

    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);

        var id_insumo = grid.jqGrid('getCell', filas[i], 'id_insumo');

        if (id_insumo === codinsumo) {

            grid.jqGrid('setCell', filas[i], "estado", 0);

        }

    }

    var grid_crear_cotizacion = jQuery("#tbl_cotizacion");
    var ids = grid_crear_cotizacion.jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var cl = ids[i];

        var id_insumo = grid_crear_cotizacion.getRowData(cl).id_insumo;

        if (id_insumo === codinsumo) {

            be = "<img src='/fintra/images/botones/iconos/chulo.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Pendiente'  onclick=\"mensajeConfirmAction1('Esta seguro de cambiar el estado a terminado?','250','150',ActualizarEstadoTerminadoDetalle,'" + id_insumo + "');\">";

            grid_crear_cotizacion.jqGrid('setRowData', ids[i], {estado: be});
        }
    }

}


function ActualizarEstadoTerminadoDetalle(codinsumo) {
    var grid = jQuery("#tbl_cotizacion_detalle"),
            filas = grid.jqGrid('getDataIDs'),
            data;

    //var preciototal1 = grid.jqGrid('getCol', 'preciototal', false, 'sum');

    for (var i = 0; i < filas.length; i++) {
        data = grid.jqGrid("getLocalRow", filas[i]);

        var id_insumo = grid.jqGrid('getCell', filas[i], 'id_insumo');

        if (id_insumo === codinsumo) {

            grid.jqGrid('setCell', filas[i], "estado", 1);

        }

    }

    var grid_crear_cotizacion = jQuery("#tbl_cotizacion");
    var ids = grid_crear_cotizacion.jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var cl = ids[i];

        var id_insumo = grid_crear_cotizacion.getRowData(cl).id_insumo;

        if (id_insumo === codinsumo) {

            be = "<img src='/fintra/images/botones/iconos/obligatorio.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Terminado'  onclick=\"mensajeConfirmAction1('Esta seguro de cambiar el estado a pendiente?','250','150',ActualizarEstadoPendienteDetalle,'" + id_insumo + "');\">";

            grid_crear_cotizacion.jqGrid('setRowData', ids[i], {estado: be});
        }
    }

}

function mensajeConfirmAction1(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta1');
    $("#msj1").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta1").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function reiniciar_filtro() {
    cargar_Tabla_Areas();
    $("#tabla_Disciplina").jqGrid("GridUnload");
    $("#tabla_Capitulo").jqGrid("GridUnload");
    $("#tabla_Actividades").jqGrid("GridUnload");

}


function insertar_rentabilidad_contratista_global(id, porcentaje) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Cotizacion&accion=Sl',
        dataType: 'json',
        async: false,
        data: {
            opcion: 25,
            idaccion: $('#idaccion').val(),
            id: id,
            porcentaje: porcentaje
        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                if (json.respuesta == "OK")
                {
                    mensajesDelSistema('Se creo una nueva Cotizacion', '250', '180');
                    cargarHistoricoCotizacion();

                }

            } catch (exception) {
                mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function insertar_rentabilidades_cabecera() {

    var subtotal = $("#tabla_Areas").jqGrid('getCol', 'valor', false, 'sum');
    var valor_contratista = $("#tabla_Areas").jqGrid('getCol', 'val_Contratista', false, 'sum');
    var valor_esquema = $("#tabla_Areas").jqGrid('getCol', 'val_Esquema', false, 'sum');
    var perc_esquema = 0;
    var perc_contratista = 0;

    if (valor_esquema >= valor_contratista) {
        perc_esquema = (valor_esquema * 100 / valor_contratista) - 100;
        valor_esquema = valor_esquema - valor_contratista;
    } else {
        valor_esquema = 0;
    }

    if (valor_contratista >= subtotal) {
        perc_contratista = (valor_contratista * 100 / subtotal) - 100;
        valor_contratista = valor_contratista - subtotal;
    } else {
        valor_contratista = 0;
    }





    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Cotizacion&accion=Sl',
        dataType: 'json',
        async: false,
        data: {
            opcion: 26,
            idaccion: $('#idaccion').val(),
            subtotal: subtotal,
            valor_contratista: valor_contratista,
            perc_contratista: perc_contratista,
            valor_esquema: valor_esquema,
            perc_esquema: perc_esquema
        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                if (json.respuesta == "OK")
                {
                    mensajesDelSistema1("Exito al Guardar", '204', '140');


                }

            } catch (exception) {
                mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });



}