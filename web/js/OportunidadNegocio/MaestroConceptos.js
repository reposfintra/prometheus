/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    cargando_toggle();
    maximizarventana();
    listarConceptos();
});


function listarConceptos() {
    var grid_tbl_maestro = jQuery("#tbl_Conceptos");
    if ($("#gview_tbl_Conceptos").length) {
        refrescarGridConceptos();
    } else {
        grid_tbl_maestro.jqGrid({
            caption: "Conceptos",
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            datatype: "json",
            height: '400',
            width: '1000',
            cellEdit: true,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Factura', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'nombre', index: 'razon_social', width: 430, align: 'left'},
                {name: 'descripcion', index: 'descripcion', width: 430, align: 'left'},
                {name: 'factura', index: 'factura', width: 430, align: 'left', hidden: true},
                {name: 'actions', index: 'actions', width: 110, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tbl_Conceptos'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 0
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var ids = jQuery("#tbl_Conceptos").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarConcepto('" + cl + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmacion('Esta seguro de anular el Concepto?','250','150',AnularConcepto,'" + cl + "');\">";
                    jQuery("#tbl_Conceptos").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                }
                

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {


            }
        }).navGrid("#page_tbl_Conceptos", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tbl_Conceptos").jqGrid("navButtonAdd", "#page_tbl_Conceptos", {
            caption: "Nuevo",
            onClickButton: function () {
                crearConcepto();
            }
        });
    }

}

function refrescarGridConceptos() {
    jQuery("#tbl_Conceptos").setGridParam({
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 0
            }
        }
    });

    jQuery('#tbl_Conceptos').trigger("reloadGrid");
}


function crearConcepto() {
    $('#div_Control').fadeIn('slow');
    $('#idConcepto').val('');
    $('#nombre').val('');
    $('#descripcion').val('');
    $('#factura').val('');
    $('#factura').prop('checked',false);
    AbrirDivConcepto();
}

function AbrirDivConcepto() {
    $("#div_Control").dialog({
        width: '500',
        height: '280',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR CONCEPTO',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardarConcepto();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarConcepto(cl) {

    $('#div_Control').fadeIn("slow");
    var fila = jQuery("#tbl_Conceptos").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    var factura = fila['factura'];

    $('#idConcepto').val(cl);
    $('#nombre').val(nombre);
    $('#descripcion').val(descripcion);
    $('#factura').prop('checked',(factura==='1' ? true : false));
    //var elemento = document.getElementById("factura");
    //messagebox(elemento.value);
    //$('#factura')
    AbrirDivEditarConcepto();
}

function AbrirDivEditarConcepto() {
    $("#div_Control").dialog({
        width: '500',
        height: '280',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR CONCEPTO',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarConcepto();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}



function guardarConcepto() {
    var nombre = $('#nombre').val();
    var descripcion = $('#descripcion').val();
    var factura = ($('#factura').prop('checked')===true) ? 1 : 0;

    if (nombre !== '' && descripcion !== '') {


        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controlleropav?estado=Modulo&accion=Planeacion",
            data: {
                opcion: ($('#idConcepto').val() === '') ? 1 : 2,
                id: $('#idConcepto').val(),
                nombre: $('#nombre').val(),
                descripcion: $('#descripcion').val(),
                factura: factura
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '270', '165');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridConceptos();
                        $("#div_Control").dialog('close');
                    }

                } else {
                    
                    mensajesDelSistema("Lo sentimos no se pudo guardar el Concepto!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    } else {
        mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');
    }
}

function AnularConcepto(cl){
    
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        data: {
            opcion: 3,
            id: cl

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {
                    
                    refrescarGridConceptos();
                }

            } else {
                
                mensajesDelSistema("Lo sentimos no se pudo Anular el Concepto!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
        
}

/**********************************************************************************************************************************************************
                                                            Utiles Generales
***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon', '#perc_rete'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle(){
    $('#loader-wrapper').toggle();
}


function mensajeConfirmacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}
/**********************************************************************************************************************************************************
                                                           Fin Utiles Generales
***********************************************************************************************************************************************************/