/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    cargarUnidadesNegocio();
});

function cargarUnidadesNegocio() {
    var url = './controller?estado=Unidades&accion=Negocio&opcion=1';
    if ($("#gview_refrescarGridUnidadNegocio").length) {
         refrescarGridUnidadNegocio();
     }else {
        jQuery("#unidadesNegocio").jqGrid({
            caption: 'Unidades de Negocios',
            url: url,
            datatype: 'json',
            height: 400,
            width: 600,
            colNames: ['Id', 'Codigo', 'Descripcion','Cod Central Riesgo','Editar'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key:true},
                {name: 'codigo', index: 'codigo', sortable: true, align: 'center', width: '200px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'center', width: '700px'},
                {name: 'codCentralRiesgo', index: 'codCentralRiesgo', sortable: true, align: 'center', width: '250px'},
                {name: 'editar', index: 'editar', align: 'center', width: '150px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            gridComplete: function() {
                    var ids = jQuery("#unidadesNegocio").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarUnidadNegocio('" + cl + "');\">";
                        jQuery("#unidadesNegocio").jqGrid('setRowData', ids[i], {editar: ed});

                    }
                },
            loadError: function(xhr, status, error) {
                alert(error);
            }
        });
     }
}

function guardarUnidNegocio(){
    var nomUnidad = $('#nombre').val();
    var codUnidad = $('#codigo').val();
    var codCentral = $('#codCentral').val();
    var url = './controller?estado=Unidades&accion=Negocio';
            $.ajax({
                type: "POST",
                url: url,
                dataType: "text",
                data: {
                    opcion: 2,
                    nomUnidad: nomUnidad,
                    codUnidad: codUnidad,
                    codCentral: codCentral
                   },
                success: function(data) {
                   var resp = data.trim();
                   if (resp == "OK") {
                        alert("Se cre� la unidad de negocio");
                        $('#div_convenio').fadeIn();
                        $('#boton_asociar').show();
                        $('#boton_guardar').hide();
                        cargarConvenio();
                    }else{
                        alert("No se cre� la unidad de negocio, puede que el nombre o el c�digo ya exista");
                    }

                }

            });
    
}

function crearUnidNegocios(){
     $('#popupNegocios').fadeIn('slow');
     
}

function cargarConvenio(){
    var url = './controller?estado=Unidades&accion=Negocio&opcion=3';
    jQuery("#convenios").jqGrid({
            caption: 'Convenios de Negocios',
            url: url,
            datatype: 'json',
            height: 400,
            width: 500,
            colNames: ['Id', 'Nombre'],
            colModel: [
                {name: 'id_convenio', index: 'id', sortable: true, align: 'center', width: '100px'},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '500px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect:true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            loadError: function(xhr, status, error) {
                alert(error);
            }
        });
}

function asociarConvenios(){
    var nomUnidad = $('#nombre').val();
    var listado = "";
    var filasId =jQuery('#convenios').jqGrid('getGridParam', 'selarrrow');
    if(filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        
        var url = './controller?estado=Unidades&accion=Negocio';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "text",
            data: {
                opcion: 4,
                listado: listado,
                nomUnidad: nomUnidad
            },
            success: function(data){
                var resp = data.trim();
                if (resp == "OK") {
                    alert("Se asociaron los convenios a la Unidad de Negocio");
                     $('#boton_asociar').hide();
                     $('#popupNegocios').fadeOut();
                     refrescarGridUnidadNegocio();
                }
            }
        });
        
    }else{
        alert("Debe seleccionar los convenios a asociar");
    }
    
}

function editarUnidadNegocio(cl){
    cargarDatosUnidad(cl);
    $('#div_editar').fadeIn();
    var url = './controller?estado=Unidades&accion=Negocio&opcion=5&idUnidad='+cl;
     if ($("#gview_conveniosNegocio").length) {
         refrescarGridConveniosNegocio(cl);
     }else {
         jQuery("#conveniosNegocio").jqGrid({
            caption: 'Convenios de Negocios',
            url: url,
            datatype: 'json',
            height: 100,
            width: 600,
            colNames: ['Id', 'Nombre','Eliminar'],
            colModel: [
                {name: 'id_convenio', index: 'id', sortable: true, align: 'center', width: '100px'},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '500px'},
                {name: 'eliminar', index: 'eliminar', sortable: true, align: 'center', width: '60px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            gridComplete: function() {
                    var ids = jQuery("#conveniosNegocio").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        ed = "<img src='/fintra/images/stop.png' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Eliminar'  onclick=\"eliminarConvenio('" + cl + "');\">";
                        jQuery("#conveniosNegocio").jqGrid('setRowData', ids[i], {eliminar: ed});

                    }
                    $('#bt_listar_conv').show();
                },
            loadError: function(xhr, status, error) {
                alert(error);
            }
        });
    }
}

function cargarDatosUnidad(cl){
    $.ajax({
        url: './controller?estado=Unidades&accion=Negocio&opcion=6&idUnidad='+cl,
        datatype:'json',
        type:'get',
        success: function(json) {
            $('#nombreUnd').val(json['descripcion']);
            $('#codigoUnd').val(json['codigo']);
            $('#codCentralR').val(json['codCentralRiesgo']);
            $('#idUnidad').val(json['id']);
        }
    });
}

function actualizarUnidNegocio(){
    var idUnidad = $('#idUnidad').val();
    var nomUnidad = $('#nombreUnd').val();
    var codigoUnd = $('#codigoUnd').val();
    var codCentralR = $('#codCentralR').val();
    $.ajax({
        url: './controller?estado=Unidades&accion=Negocio',
        datatype:'json',
        type:'get',
        data:{
            opcion: 8,
            idUnidad: idUnidad,
            nomUnidad: nomUnidad,
            codCentralR: codCentralR,
            codigoUnd: codigoUnd
            
        },                
        success: function(data) {
            alert("Se actualizaron los datos");
            refrescarGridUnidadNegocio();
        }
    });
}

function refrescarGridConveniosNegocio(cl){
    var url = './controller?estado=Unidades&accion=Negocio&opcion=5&idUnidad='+cl;
    jQuery("#conveniosNegocio").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#conveniosNegocio').trigger("reloadGrid");
}

function eliminarConvenio(cl){
    var resp = confirm("Esta seguro que desea eliminar este convenio?");
    if(resp == true){
    var idUnidad = $('#idUnidad').val();
    $.ajax({
        url: './controller?estado=Unidades&accion=Negocio',
        datatype:'json',
        type:'get',
        data:{
            opcion: 7,
            idUnidad: idUnidad,
            idConvenio: cl
        },                
        success: function(data) {
            refrescarGridConveniosNegocio(idUnidad);
           
        }
    });
    }
    
}

function listarConveniosUnidad(){
    $('#div_convenio_und').fadeIn();
    $('#bt_listar_conv').hide();
    $('#bt_asociar_conv').show();
    var url = './controller?estado=Unidades&accion=Negocio&opcion=3';
    jQuery("#conveniosUnd").jqGrid({
            caption: 'Convenios de Negocios',
            url: url,
            datatype: 'json',
            height: 300,
            width: 600,
            colNames: ['Id', 'Nombre'],
            colModel: [
                {name: 'id_convenio', index: 'id', sortable: true, align: 'center', width: '100px'},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '500px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect:true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            loadError: function(xhr, status, error) {
                alert(error);
            }
        });
}

function asociarConveniosUnd(){
    var varUnidad = $('#idUnidad').val();
    var nomUnidad = $('#nombreUnd').val();
    var listado = "";
    var filasId =jQuery('#conveniosUnd').jqGrid('getGridParam', 'selarrrow');
    if(filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        
        var url = './controller?estado=Unidades&accion=Negocio';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "text",
            data: {
                opcion: 4,
                listado: listado,
                nomUnidad: nomUnidad
            },
            success: function(data){
                var resp = data.trim();
                if (resp == "OK") {
                    alert("Se asociaron los convenios a la Unidad de Negocio");
                    $('#div_convenio_und').fadeOut();
                    refrescarGridConveniosNegocio(varUnidad);
                    $('#bt_listar_conv').show();
                    $('#bt_asociar_conv').hide();
                }
            }
        });
        
    }else{
        alert("Debe seleccionar los convenios a asociar");
    }
}

function refrescarGridUnidadNegocio(){
    var url = './controller?estado=Unidades&accion=Negocio&opcion=1';
    jQuery("#unidadesNegocio").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#unidadesNegocio').trigger("reloadGrid");
}

