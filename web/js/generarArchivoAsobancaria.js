/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {    
    let fecha = new Date();
    let fullfecha;
    fecha.setDate(fecha.getDate());
    fullfecha = fecha.getFullYear()+"-"+('0' + (fecha.getMonth()+1)).slice(-2)+"-"+('0' + fecha.getDate()).slice(-2);
    $('#select_fecha').val(fullfecha);
    
    cargarEntidadRecaudo();
});


function cargarEntidadRecaudo(){
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 33
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
                $('#id_entidad_recaudo').html('');
                $('#id_entidad_recaudo').append('<option value="" >...</option>');
                for (var datos in json) {
                    $('#id_entidad_recaudo').append('<option value="' + datos + '" >' + json[datos] + '</option>');
                }

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                
                $(this).dialog("close");
            }
        }
    });

}

function cargarRecaudo() {
    $("#div_detalle_recaudo").hide();
    if ($("#id_entidad_recaudo").val()!=""){
        
    var grid_tabla = $("#tabla_recaudo");
    if ($("#gview_tabla_recaudo").length) {
        reloadGridTabla(grid_tabla);
    } else {
        
        grid_tabla.jqGrid({
            caption: "Reporte",
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '150',
            width: '1170',
            colNames: ['Entidad','Codigo Recaudadora','Valor Total','Id Archivo','Fecha Recaudo','Cuenta Cliente','Fecha Subida Archivo','Numero Filas','Generado','Seleccionar'],
            
           colModel: [
                {name: 'entidad',        index: 'entidad',        width: 120,sortable: true, align: 'center', hidden: false},
                {name: 'codigo_recaudadora',index: 'codigo_recaudadora',width: 90, sortable: true, align: 'center',   hidden: false},
                {name: 'valor_total', index: 'valor_total', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}}, 
                {name: 'id_archivo',       index: 'id_archivo',       width: 120, sortable: true, align: 'center',   hidden: false} , 
                {name: 'fecha_recaudo',       index: 'fecha_recaudo',       width: 120, sortable: true, align: 'center',   hidden: false}, 
                {name: 'cuenta_cliente',       index: 'cuenta_cliente',       width: 120, sortable: true, align: 'center',   hidden: false}, 
                {name: 'fecha_archivo',       index: 'fecha_archivo',       width: 120, sortable: true, align: 'center',   hidden: false},
                {name: 'numero_filas',       index: 'numero_filas',       width: 90, sortable: true, align: 'center',   hidden: false},
                {name: 'archivo_generado',       index: 'archivo_generado',       width: 90, sortable: true, align: 'center',   hidden: false},
                {name: 'select', index: 'select', width: 100, sortable: true, align: 'center', hidden: false, search: false}
                
            ],
            rowNum: 100000,
            rowTotal: 100000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#pager',
            pgtext: null,
            pgbuttons: false,
            multiselect:false,           
            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
                
            },
            loadComplete: function () {
                
                if (grid_tabla.jqGrid('getGridParam', 'records') <= 0) {
                                           
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            ajaxGridOptions: {
                
                data: {
                    opcion: 34,
                    id_entidad_recaudo: $("#id_entidad_recaudo").val(),             
                    select_fecha: $("#select_fecha").val()             

                }                
                
            },
            gridComplete: function() { 
                
                let colSumTotal = $("#tabla_recaudo").jqGrid('getCol', 'valor_total', false, 'sum'); 
                $("#tabla_recaudo").jqGrid('footerData', 'set', {valor_total: colSumTotal});
                
                let cant = $("#tabla_recaudo").jqGrid('getDataIDs');
                
                    for (let i = 0; i < cant.length; i++) {
                        let id_archivo = $("#tabla_recaudo").getRowData(cant[i]).id_archivo;
                        let archivo_generado = $("#tabla_recaudo").getRowData(cant[i]).archivo_generado;
                        let btn="";
                        if (archivo_generado==='N'){
                        btn = "<button class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' style='font-size: 12px; padding: 5px; margin: 4px;' onclick=\"cargarRecaudoDetalle('" + id_archivo + "');\">Ver Detalle</button>"
                       }
                        $("#tabla_recaudo").jqGrid('setRowData', cant[i], {select: btn});
                    }
                             
            }, 
            
           
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
                
            }
        }).navGrid('#pager', {add: false, edit: false, del: false, search: false, refresh: false});
           
    }
     }else {
            mensajesDelSistema("Por favor seleccione entidad de recaudo",'250', '150');
            }
    
}

function reloadGridTabla(grid_tabla) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",
       
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 34,
                id_entidad_recaudo: $("#id_entidad_recaudo").val(),             
                select_fecha: $("#select_fecha").val()    
            }
        }
    });
    grid_tabla.trigger("reloadGrid");    
     
}


function cargarRecaudoDetalle(id_archivo){
    $("#id_archivo").val(id_archivo);
    $("#div_detalle_recaudo").fadeIn();
        
    var grid_tabla = $("#tabla_detalle_recaudo");
    if ($("#gview_tabla_detalle_recaudo").length) {
        reloadGridTablaDetalle(grid_tabla);
    } else {
        
        grid_tabla.jqGrid({
            caption: "Reporte",
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '1260',
            
            colNames: ['Id Archivo','Referencia','Valor Recaudado','Procedencia Pago','Medio Pago','Numero Operacion','Numero Autorizacion','Entidad Debitada',
                       'Sucursal','Secuencia','Causal Devolucion'],            
            colModel: [
                {name: 'id_archivo',        index: 'id_archivo',        width: 120,sortable: true, align: 'center', hidden: false},
                {name: 'referencia_pricipal',        index: 'referencia_pricipal',        width: 120,sortable: true, align: 'center', hidden: false},
                {name: 'valor_recaudado', index: 'valor_recaudado', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}}, 
                {name: 'procedencia_pago',       index: 'procedencia_pago',       width: 120, sortable: true, align: 'center',   hidden: false} , 
                {name: 'medio_pago',       index: 'medio_pago',       width: 120, sortable: true, align: 'center',   hidden: false}, 
                {name: 'num_operacion',       index: 'num_operacion',       width: 120, sortable: true, align: 'center',   hidden: false}, 
                {name: 'num_autorizacion',       index: 'num_autorizacion',       width: 120, sortable: true, align: 'center',   hidden: false},
                {name: 'cod_entidad_debitada',       index: 'cod_entidad_debitada',       width: 90, sortable: true, align: 'center',   hidden: false},
                {name: 'cod_sucursal',       index: 'cod_sucursal',       width: 90, sortable: true, align: 'center',   hidden: false},
                {name: 'secuencia',       index: 'secuencia',       width: 90, sortable: true, align: 'center',   hidden: false},
                {name: 'causal_devolucion',       index: 'causal_devolucion',       width: 90, sortable: true, align: 'center',   hidden: false}
                
            ],
            rowNum: 100000,
            rowTotal: 100000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#pager',
            pgtext: null,
            pgbuttons: false,
            multiselect:false,           
            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
                
            },
            loadComplete: function () {
                
                if (grid_tabla.jqGrid('getGridParam', 'records') <= 0) {
                                           
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            ajaxGridOptions: {
                
                data: {
                    opcion: 35,
                    id_archivo:id_archivo         

                }                
                
            },
            gridComplete: function() { 
                
                let colSumTotal = $("#tabla_detalle_recaudo").jqGrid('getCol', 'valor_recaudado', false, 'sum'); 
                $("#tabla_detalle_recaudo").jqGrid('footerData', 'set', {valor_recaudado: colSumTotal});
            }, 
            
           
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
                
            }
        }).navGrid('#pager', {add: false, edit: false, del: false, search: false, refresh: false});
           
    }
    
}

function reloadGridTablaDetalle(grid_tabla) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",
       
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 35,
                id_archivo: $("#id_archivo").val()    
            }
        }
    });
    grid_tabla.trigger("reloadGrid");    
     
}

function generarArchivoAsobancaria(){
   loading("Esta operaci�n puede tardar varios minutos, por favor espere.", 450);     
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",
        data: {
            opcion: 32,
            id_archivo: $("#id_archivo").val() 
            
        },
        success: function (json) {
         if (json.success ===true){
            $("#dialogLoading").dialog("close");
             mensajesDelSistema ("Documento generado con exito, puede consultarlo en la carpeta descargas", '250', '180', true);
              $("#tabla_negocios").jqGrid("GridUnload");
              $("#div_detalle_recaudo").fadeIn();
             cargarRecaudo();
         }else{
              $("#dialogLoading").dialog("close");
            mensajesDelSistema("Ocurrio un error enviando los datos al servidor", '250', '150', true);
         }
        },
        error: function () {
           $("#dialogLoading").dialog("close");
            mensajesDelSistema("Ocurrio un error enviando los datos al servidor", '250', '150', true);
        }
    });
}



function loading(msj, width) {
                $("#msj2").html(msj);
                $("#dialogLoading").dialog({
                    width: width,
                    height: 130,
                    show: "scale",
                    hide: "scale",
                    resizable: false,
                    position: "center",
                    modal: true,
                    closeOnEscape: true
                });

                $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}