/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    cargarControlCuentas();
});

function cargarControlCuentas() {
    var operacion;
    var grid_tabla_ = jQuery("#tabla_control_cuentas_ap");
    if ($("#gview_tabla_control_cuentas_ap").length) {
        reloadGridMostrar(grid_tabla_, 65);
    } else {
        grid_tabla_.jqGrid({
            caption: "Documentos",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '1000',
            colNames: ['Id', 'Estado', 'Codigo control cuentas', 'Unidad negocio', 'Linea negocio', 'Cuenta IxM', 'Cuenta Gac', 'Cuenta cab ingreso', 'Cuenta det ingreso', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'cod_controlcuentas', index: 'cod_controlcuentas', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'linea_neg', index: 'linea_neg', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'cuentaixm', index: 'cuentaixm', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'cuentagac', index: 'cuentagac', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'cuentacabingreso', index: 'cuentacabingreso', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'cuentadetingreso', index: 'cuentadetingreso', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'cambio', index: 'cambio', width: 110, sortable: true, align: 'left', hidden: false}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 65
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_control_cuentas_ap").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_control_cuentas_ap").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstado('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_control_cuentas_ap").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_control_cuentas_ap"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var cod_controlcuentas = filas.cod_controlcuentas;
                var linea_neg = filas.linea_neg;
                var cuentaixm = filas.cuentaixm;
                var cuentagac = filas.cuentagac;
                var cuentacabingreso = filas.cuentacabingreso;
                var cuentadetingreso = filas.cuentadetingreso;
                if (estado === 'Activo') {
                    ventanaControlCuentas(operacion, id, cod_controlcuentas, linea_neg, cuentaixm, cuentagac, cuentacabingreso, cuentadetingreso);
                } else if (estado === 'Incativo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_control_cuentas_ap").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaControlCuentas(operacion);
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function CambiarEstado(rowid) {
    var grid_tabla = jQuery("#tabla_control_cuentas_ap");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 66,
            id: id
        },
        success: function (data) {
            cargarControlCuentas();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function valor(id) {
    if (id === 'cmc1') {
        $("#ixm").val('cmc_factura');
        $("#ixm").attr('readOnly', true);
    } else if (id === 'cuenta1') {
        $("#ixm").val('');
        $("#ixm").attr('readOnly', false);
    }

    if (id === 'cmc2') {
        $("#gac").val('cmc_factura');
        $("#gac").attr('readOnly', true);
    } else if (id === 'cuenta2') {
        $("#gac").val('');
        $("#gac").attr('readOnly', false);
    }

    if (id === 'cmc3') {
        $("#cabing").val('cmc_factura');
        $("#cabing").attr('readOnly', true);
    } else if (id === 'cuenta3') {
        $("#cabing").val('');
        $("#cabing").attr('readOnly', false);
    }
    if (id === 'cmc4') {
        $("#deting").val('cmc_factura');
        $("#deting").attr('readOnly', true);
    } else if (id === 'cuenta4') {
        $("#deting").val('');
        $("#deting").attr('readOnly', false);
    }
}

function checking(cuentaixm, cuentagac, cuentacabingreso, cuentadetingreso) {
    //   $('#cmc1').attr('checked', true);
    if (cuentaixm === 'cmc_factura') {
        $('#cmc1').attr('checked', true);
    } else if (cuentaixm !== 'cmc_factura') {
        $('#cmc1').attr('checked', false);
        $('#cuenta1').attr('checked', true);
    }
    if (cuentagac === 'cmc_factura') {
        $('#cmc2').attr('checked', true);
    } else if (cuentaixm !== 'cmc_factura') {
        $('#cmc2').attr('checked', false);
        $('#cuenta2').attr('checked', true);
    }
    if (cuentacabingreso === 'cmc_factura') {
        $('#cmc3').attr('checked', true);
    } else if (cuentaixm !== 'cmc_factura') {
        $('#cmc3').attr('checked', false);
        $('#cuenta3').attr('checked', true);
    }
    if (cuentadetingreso === 'cmc_factura') {
        $('#cmc4').attr('checked', true);
    } else if (cuentaixm !== 'cmc_factura') {
        $('#cmc4').attr('checked', false);
        $('#cuenta4').attr('checked', true);
    }


}

function ventanaControlCuentas(operacion, id, cod_controlcuentas, linea_neg, cuentaixm, cuentagac, cuentacabingreso, cuentadetingreso) {

    $('input[type=checkbox]').live('click', function () {
        var parent = $(this).parent().attr('id');
        $('#' + parent + ' input[type=checkbox]').removeAttr('checked');
        $(this).attr('checked', 'checked');
    });

    if (operacion === 'Nuevo') {
        $("#ixm").attr('readOnly', true);
        $("#gac").attr('readOnly', true);
        $("#cabing").attr('readOnly', true);
        $("#deting").attr('readOnly', true);
        cargarUnidadesNegocio();
        $("#dialogControlCuentas").dialog({
            width: '368',
            height: '570',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'CONTRAL CUENTAS AP',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarControlCuentas();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#cod_controlcuentas").val('');
                    $("#ixm").val('');
                    $("#gac").val('');
                    $("#cabing").val('');
                    $("#deting").val('');
                    $("#tiponeg").val('');
                    $('#cmc1').attr('checked', false);
                    $('#cuenta1').attr('checked', true);
                    $('#cmc2').attr('checked', false);
                    $('#cuenta2').attr('checked', true);
                    $('#cmc3').attr('checked', false);
                    $('#cuenta3').attr('checked', true);
                    $('#cmc4').attr('checked', false);
                    $('#cuenta4').attr('checked', true);
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        $("#cod_controlcuentas").val(cod_controlcuentas);
        $("#ixm").val(cuentaixm);
        $("#gac").val(cuentagac);
        $("#cabing").val(cuentacabingreso);
        $("#deting").val(cuentadetingreso);
        cargarUnidadesNegocio();
        $("#tiponeg").val(linea_neg);
        checking(cuentaixm, cuentagac, cuentacabingreso, cuentadetingreso);
        $("#dialogControlCuentas").dialog({
            width: '368',
            height: '570',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'CONTRAL CUENTAS AP',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    updateControlCuentas();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#cod_controlcuentas").val('');
                    $("#ixm").val('');
                    $("#gac").val('');
                    $("#cabing").val('');
                    $("#deting").val('');
                    $("#tiponeg").val('');
                    $('#cmc1').attr('checked', false);
                    $('#cuenta1').attr('checked', true);
                    $('#cmc2').attr('checked', false);
                    $('#cuenta2').attr('checked', true);
                    $('#cmc3').attr('checked', false);
                    $('#cuenta3').attr('checked', true);
                    $('#cmc4').attr('checked', false);
                    $('#cuenta4').attr('checked', true);
                }
            }
        });
    }
}

function cargarUnidadesNegocio() {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        async: false,
        data: {
            opcion: 67
        },
        success: function (data, textStatus, jqXHR) {
            $('#tiponeg').html('');
            $('#tiponeg').append("<option value=''>  </option>");
            for (var datos in data) {
                $('#tiponeg').append('<option value=' + datos + '>' + data[datos] + '</option>');
            }
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function guardarControlCuentas() {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 68,
            tiponegocio: $("#tiponeg").val(),
            ixm: $("#ixm").val(),
            gac: $("#gac").val(),
            cabing: $("#cabing").val(),
            deting: $("#deting").val()
        },
        success: function (data, textStatus, jqXHR) {
            if (data.respuesta === 'Guardado') {
                mensajesDelSistema("Exito al guardar", '230', '150', true);
                $("#id").val('');
                $("#cod_controlcuentas").val('');
                $("#ixm").val('');
                $("#gac").val('');
                $("#cabing").val('');
                $("#deting").val('');
                $("#tiponeg").val('');
            }
            cargarControlCuentas();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function updateControlCuentas() {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 69,
            tiponegocio: $("#tiponeg").val(),
            ixm: $("#ixm").val(),
            gac: $("#gac").val(),
            cabing: $("#cabing").val(),
            deting: $("#deting").val(),
            id: $("#id").val(),
            cod_controlcuentas: $("#cod_controlcuentas").val()
        },
        success: function (data, textStatus, jqXHR) {
            if (data.respuesta === 'Guardado') {
                mensajesDelSistema("Exito al actualizar", '230', '150', true);
                $("#id").val('');
                $("#cod_controlcuentas").val('');
                $("#ixm").val('');
                $("#gac").val('');
                $("#cabing").val('');
                $("#deting").val('');
                $("#tiponeg").val('');
            }
            cargarControlCuentas();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}