/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    cargarEntidadesRecaudo();
    //cargar paises.
    cargarPaises();
    
    $('input[name="estado"]:radio').change(
        function() {
            if($("input[name=estado]:checked").val()==="A"){
                DeshabilitarControles(false);
            }else{
                DeshabilitarControles(true);
            }
    });          

    
    $("#pais").change(function() {
        var op = $(this).find("option:selected").val();
        cargarDepartamentos(op, "departamento");
    });
    
    $("#departamento").change(function() {
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciudad");
    });

});

function cargarEntidadesRecaudo(){ 
    if ($("#gview_entidadesRecaudo").length) {
         refrescarGridEntidadesRecaudo();
     }else {
        jQuery("#entidadesRecaudo").jqGrid({
            caption: 'Entidades Recaudo',
            url: "./controller?estado=Archivo&accion=Asobancaria&opcion=9",          
            datatype: 'json',
            height: 400,
            width: 650,
            colNames: ['C&oacute;digo', 'Nombre', 'Nit', 'Direccion', 'Telefono', 'Email', 'Cuenta', 'Pais', 'Dpto', 'Ciudad', 'Bancaria', 'Pago Autom&aacute;tico', 'Estado', 'Acciones'],
            colModel: [
                {name: 'valor_01', index: 'valor_01', sortable: true, align: 'center', width: '60px', key:true},
                {name: 'valor_02', index: 'valor_02', sortable: true, width: '300px'},
                {name: 'valor_03', index: 'valor_03', sortable: true, hidden:true, align: 'center'},           
                {name: 'valor_04', index: 'valor_04', sortable: true, hidden: true, align: 'center'},
                {name: 'valor_05', index: 'valor_05', sortable: true, hidden: true, align: 'center'},
                {name: 'valor_06', index: 'valor_06', sortable: true, hidden: true, align: 'center'},
                {name: 'valor_07', index: 'valor_07', sortable: true, hidden: true, align: 'center'},
                {name: 'valor_08', index: 'valor_08', sortable: true, hidden: true, align: 'center'},
                {name: 'valor_09', index: 'valor_09', sortable: true, hidden: true, align: 'center'},
                {name: 'valor_10', index: 'valor_10', sortable: true, hidden:true, align: 'center'},
                {name: 'valor_11', index: 'valor_11', sortable: true, align: 'center', width: '90px'},
                {name: 'valor_12', index: 'valor_12', sortable: true, align: 'center', width: '100px'},
                {name: 'reg_status', index: 'reg_status', hidden:true, sortable: true, align: 'center', width: '90px'},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '50px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
            pager:'#page_tabla_entidades_recaudo',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {
                    var ids = jQuery("#entidadesRecaudo").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        var estado = jQuery("#entidadesRecaudo").getRowData(cl).reg_status;
                        if (estado==='A'){
                            $("#entidadesRecaudo").jqGrid('setRowData',ids[i],false, {weightfont:'bold',background:'#F6CECE'});            
                        }
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarEntidadRecaudadora('" + cl + "');\">";           
                        jQuery("#entidadesRecaudo").jqGrid('setRowData', ids[i], {actions: ed});       
                    }
            },
            loadError: function(xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_entidades_recaudo",{edit:false,add:false,del:false});
        jQuery("#entidadesRecaudo").jqGrid("navButtonAdd", "#page_tabla_entidades_recaudo", {
            caption: "Nuevo", 
            title: "Agregar nueva entidad",           
            onClickButton: function() {
               crearEntidadRecaudo(); 
            }
        });
     }
}

function refrescarGridEntidadesRecaudo(){    
    var url = './controller?estado=Archivo&accion=Asobancaria&opcion=9';
    jQuery("#entidadesRecaudo").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#entidadesRecaudo').trigger("reloadGrid");
}

function crearEntidadRecaudo(){   
    $('#div_entidadRecaudo').fadeIn('slow');
    AbrirDivCrearEntidadRecaudo();
}

function AbrirDivCrearEntidadRecaudo(){
      $("#div_entidadRecaudo").dialog({
        width: 700,
        height: 390,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR ENTIDAD RECAUDADORA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {         
                guardarEntidadRecaudo();             
            },
            "Salir": function () {   
                DeshabilitarControles(false);
                resetearValores();
                $(this).dialog("destroy");
            }
        }
    });
}

function editarEntidadRecaudadora(cl){
    
    $('#div_entidadRecaudo').fadeIn("slow");
    var fila = jQuery("#entidadesRecaudo").getRowData(cl);  
    var descripcion = fila['valor_02'];
    var nit = fila['valor_03'];
    var direccion = fila['valor_04'];
    var telefono = fila['valor_05'];
    var email = fila['valor_06'];
    var cuenta = fila['valor_07'];
    var pais = fila['valor_08'];
    var dpto = fila['valor_09'];    
    var ciudad = fila['valor_10'];    
    var is_bank = fila['valor_11'];
    var pago_automatico = fila['valor_12'];  
    var estado = (fila['reg_status']==='A') ? "I":"A";  
    if (estado==="A"){
        DeshabilitarControles(false);
    }else{
        DeshabilitarControles(true);
    }
    $('#codigo').attr({disabled:true});
    $('#codigo').val(cl);
    $('#descripcion').val(descripcion);
    $('#nit').val(nit);
    $('#direccion').val(direccion);
    $('#telefono').val(telefono);
    $('#email').val(email);
    $('#cuenta').val(cuenta);
    $('#pais').val(pais);
    cargarDepartamentos(pais, "departamento");
    $('#departamento').val(dpto);
    cargarCiudad(dpto,"ciudad");
    $('#ciudad').val(ciudad);
    $('input[name=tipoent]').val([is_bank]);
    $('input[name=pauto]').val([pago_automatico]); 
    $('input[name=estado]').val([estado]); 
    AbrirDivEditarEntidadRecaudo();
}

function AbrirDivEditarEntidadRecaudo(){
      $("#div_entidadRecaudo").dialog({
        width: 700,
        height: 390,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR ENTIDAD RECAUDADORA',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              actualizarEntidadRecaudo();
            },
            "Salir": function () {
                DeshabilitarControles(false);
                resetearValores();
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarInfoEntidadRecaudadora(cl){
    
    $('#div_VerEntidadRecaudo').fadeIn("slow");
    var fila = jQuery("#entidadesRecaudo").getRowData(cl);  
    var descripcion = fila['valor_02'];
    var nit = fila['valor_03'];
    var direccion = fila['valor_04'];
    var telefono = fila['valor_05'];
    var email = fila['valor_06'];
    var cuenta = fila['valor_07'];
    var pais = fila['valor_08'];
    var dpto = fila['valor_09'];    
    var ciudad = fila['valor_10'];    
    var is_bank = fila['valor_11'];
    var pago_automatico = fila['valor_12'];  
   
    $('#codigoRO').val(cl);
    $('#descripcionRO').val(descripcion);
    $('#nitRO').val(nit);
    $('#direccionRO').val(direccion);
    $('#telefonoRO').val(telefono);
    $('#emailRO').val(email);
    $('#cuentaRO').val(cuenta);
    $('#paisRO').val(pais);
    cargarDepartamentos(pais, "departamentoRO");
    $('#departamentoRO').val(dpto);
    cargarCiudad(dpto,"ciudadRO");
    $('#ciudadRO').val(ciudad);
    $('input[name=tipoentRO]').val([is_bank]);
    $('input[name=pautoRO]').val([pago_automatico]); 
    AbrirDivVerInfoEntidadRecaudo();
}

function AbrirDivVerInfoEntidadRecaudo(){
      $("#div_VerEntidadRecaudo").dialog({
        width: 700,
        height: 400,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'INFO ENTIDAD RECAUDADORA',
        closeOnEscape: false,
        buttons: {  
            "Salir": function () {
                resetearValores();
                $(this).dialog("destroy");
            }
        }
    });
}


function guardarEntidadRecaudo(){  
    var codigo = $('#codigo').val();
    var descripcion = $('#descripcion').val();
    var nit = $('#nit').val();
    var direccion = $('#direccion').val();
    var telefono = $('#telefono').val();
    var email = $('#email').val();
    var ciudad = $('#ciudad').val();
    var cuenta = $('#cuenta').val();
    var is_bank = $("input[name=tipoent]:checked").val();
    var pago_automatico = $("input[name=pauto]:checked").val();
    var estado = ($("input[name=estado]:checked").val()==='A')?"":"A";
    var url = './controller?estado=Archivo&accion=Asobancaria';
    if(codigo!=='' && descripcion!==''){
        if(telefono!=='' && !validarTelefono(telefono))
        {
            mensajesDelSistema("Por favor, ingrese un numero de telefono valido", '250', '150'); 
        }else if(!validarEmail(email)){
            mensajesDelSistema("El email ingresado es incorrecto. Por favor, Verifique", '250', '150');       
        }else{
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 10 ,                
                    codigo: codigo,
                    descripcion: descripcion,
                    nit: nit,
                    direccion: direccion,
                    telefono: telefono,
                    email: email,
                    ciudad: ciudad,
                    cuenta: cuenta,
                    is_bank: is_bank,
                    pago_automatico: pago_automatico,
                    reg_status: estado
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {
                            refrescarGridEntidadesRecaudo();   
                            resetearValores();
                            mensajesDelSistema("Se cre&oacute; la entidad", '250', '150', true);
                            $('#codigo').val('');
                            $('#descripcion').val('');
                            $('#nit').val('');
                            $('#direccion').val('');
                            $('#telefono').val('');
                            $('#email').val('');
                            $('#ciudad').val('');
                            $('#cuenta').val(''); 
                        }
                        
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear la entidad!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            }); 
        }
    }else{
       mensajesDelSistema("El codigo y la descripci&oacuten son campos obligatorios", '250', '150');
    }
}

function actualizarEntidadRecaudo(){
    var codigo = $('#codigo').val();
    var descripcion = $('#descripcion').val();
    var nit = $('#nit').val();
    var direccion = $('#direccion').val();
    var telefono = $('#telefono').val();
    var email = $('#email').val();
    var ciudad = $('#ciudad').val();
    var cuenta = $('#cuenta').val();
    var is_bank = $("input[name=tipoent]:checked").val();
    var pago_automatico = $("input[name=pauto]:checked").val();
    var estado = ($("input[name=estado]:checked").val()==='A')?"":"A";
    var url = './controller?estado=Archivo&accion=Asobancaria';
    if(descripcion!==''){ 
        if(telefono!=='' && !validarTelefono(telefono))
        {
            mensajesDelSistema("Por favor, ingrese un numero de telefono valido", '250', '150'); 
        }else if(!validarEmail(email)){
            mensajesDelSistema("El email ingresado es incorrecto. Por favor, Verifique", '250', '150');       
        }else{
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 11,
                    codigo: codigo,
                    descripcion: descripcion,
                    nit: nit,
                    direccion: direccion,
                    telefono: telefono,
                    email: email,
                    ciudad: ciudad,
                    cuenta: cuenta,
                    is_bank: is_bank,
                    pago_automatico: pago_automatico,
                    reg_status: estado
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }

                        if (json.respuesta === "OK") {
                            refrescarGridEntidadesRecaudo();                  
                            mensajesDelSistema("Se actualiz&oacute la entidad de recaudo", '250', '150', true); 
                        }

                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo actualizar la entidad recaudadora!!", '250', '150');
                    }

                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }
    }else{
         mensajesDelSistema("La descripci&oacute;n es obligatoria!!", '250', '150');      
    }    
}


function anularEntidadRecaudo(cl){
    var url = './controller?estado=Archivo&accion=Asobancaria';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 12,            
            idEntidad: cl
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridEntidadesRecaudo();                 
                    mensajesDelSistema("Se anul&oacute; la entidad recaudadora", '250', '150', true);                         
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular la entidad recaudadora!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function activarEntidadRecaudo(cl){
    var url = './controller?estado=Archivo&accion=Asobancaria';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 13,            
            idEntidad: cl
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridEntidadesRecaudo();
                    mensajesDelSistema("La entidad fue activada", '250', '150', true);                   
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo activar la entidad recaudadora seleccionada!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function validarTelefono(phone) {
    var filter = /^[0-9-()+]+$/;
    return filter.test(phone);
}

function validarEmail(email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( email );
}

function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}


function DeshabilitarControles(estado){
        $('#codigo').attr({readonly: estado});
        $('#descripcion').attr({readonly: estado});
        $('#nit').attr({readonly: estado});
        $('#direccion').attr({readonly: estado});
        $('#telefono').attr({readonly: estado});
        $('#email').attr({readonly: estado});
        $('#pais').attr({disabled: estado});
        $('#departamento').attr({disabled: estado});
        $('#ciudad').attr({disabled: estado});
        $('#cuenta').attr({readonly: estado});
        $('input[name=tipoent]').attr({disabled: estado});
        $('input[name=pauto]').attr({disabled: estado});        
}


function resetearValores(){
    $('#codigo').attr({disabled:false});
    $('#codigo').val('');
    $('#descripcion').val('');
    $('#nit').val('');
    $('#direccion').val('');
    $('#telefono').val('');
    $('#email').val('');
    $('#ciudad').val('');
    $('#cuenta').val('');
    $('#pais').val('');
    $('#departamento').empty();
    $('#ciudad').empty();
    $('input[name=tipoent]').val(['S']);
    $('input[name=pauto]').val(['N']);
    $('input[name=estado]').val(['A']);
}

function cargarPaises() {
  
    $.ajax({
        type: 'POST',
        async:false,
        url: "./controller?estado=Archivo&accion=Asobancaria",
        dataType: 'json',
        data: {
            opcion: 12
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#pais').append("<option value=''>Seleccione</option>"); 
                    $('#paisRO').append("<option value=''></option>"); 

                    for (var key in json) {
                        $('#pais').append('<option value=' + key + '>' + json[key] + '</option>');  
                        $('#paisRO').append('<option value=' + key + '>' + json[key] + '</option>');   
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarDepartamentos(codigo, combo) {
  if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 13,
                cod_pais: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#'+combo).append("<option value=''>Seleccione</option>");                 

                        for (var key in json) {
                            $('#'+combo).append('<option value=' + key + '>' + json[key] + '</option>');                      
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
  }
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}



function mensajeConfirmAnulacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsj');
    $("#msj").html("<span style='background: url(/fintra/images/warning03.png); height:32px; width:32px; float: left; margin: 0 7px 20px 0'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);               
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajeConfirmAction(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsj');
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    mostrarContenido('dialogMsj');
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}
