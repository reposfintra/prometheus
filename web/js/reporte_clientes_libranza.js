/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {    
    
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $('.solo-numeric').live('keypress', function (event) {
        return numbersonly(this, event);
    });
    
    cargarConveniosPagaduria();

    $("#fecha_ini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    $("#fecha_fin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
        
    var myDate = new Date();
    $("#fecha_ini").datepicker("setDate", myDate);
    $("#fecha_fin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
      
             
    $('#listarClientes').click(function() {         
       DeshabilitarFiltro(true);           
       cargarListadoClientes();        
    });

    $("#clearClientes").click(function () {
        DeshabilitarFiltro(false);
        resetSearchValues();          
        $("#tabla_reporte_clientes").jqGrid('GridUnload');
    });
    
});


function cargarListadoClientes() {      
    var grid_tbl_reporte_clientes = jQuery("#tabla_reporte_clientes"); 
     if ($("#gview_tabla_reporte_clientes").length) {
        refrescarGridClientesLibranza();
     }else {
        grid_tbl_reporte_clientes.jqGrid({
            caption: "CLIENTES DE LIBRANZA",
            url: "./controller?estado=Maestro&accion=Libranza",           	 
            datatype: "json",  
            height: '350',
            width: 'auto',          
            colNames: ['Id', 'Fecha', 'C�dula', 'Nombres', 'Apellidos', 'Celular', 'Empresa', 'Convenio', 'Monto', 'Cuota', 'Plazo', 'Viabilidad'],
            colModel: [            
                {name: 'id', index: 'id', width: 90, align: 'center', key: true, hidden:true},
                {name: 'fecha', index: 'fecha', width: 90, align: 'center'},         
                {name: 'identificacion', index: 'identificacion', width: 110, align: 'left'},               
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 130, align: 'left'}, 
                {name: 'apellidos_cliente', index: 'apellidos_cliente', width: 130, align: 'left'}, 
                {name: 'celular', index: 'celular', width: 100, align: 'left'}, 
                {name: 'empresa', index: 'empresa', width: 130, align: 'left'},
                {name: 'convenio', index: 'convenio', width: 160, align: 'left'},
                {name: 'monto', index: 'monto', sortable: true, editable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_cuota', index: 'valor_cuota', sortable: true, editable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},              
                {name: 'plazo', index: 'plazo', width: 80, align: 'center'},
                {name: 'viable', index: 'viable', width: 80, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_reporte_clientes'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            /*pgtext:null,
            pgbuttons:false,*/
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion:73,                      
                       fecha_inicio:$('#fecha_ini').val(),
                       fecha_fin:$('#fecha_fin').val(),
                       id_convenio: $('#convenio').val()
                    }
            },   
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_reporte_clientes", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_reporte_clientes").jqGrid("navButtonAdd", "#page_tabla_reporte_clientes", {
            caption: "Exportar excel",
            onClickButton: function () {
                var info = jQuery('#tabla_reporte_clientes').getGridParam('records');
                if (info > 0) {
                    exportarExcel(74, 'tabla_reporte_clientes');
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }
            }

        });
        
       /* jQuery("#tabla_reporte_clientes").jqGrid('filterToolbar',
        {
            autosearch: true,
            searchOnEnter: true,
            defaultSearch: "cn",
            stringResult: true,
            ignoreCase: true,
            multipleSearch: true
        }); */
    }  
       
}

function refrescarGridClientesLibranza(){   
    jQuery("#tabla_reporte_clientes").setGridParam({
        url: "./controller?estado=Maestro&accion=Libranza",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: {opcion: 73,                  
                   fecha_inicio: $('#fecha_ini').val(),
                   fecha_fin: $('#fecha_fin').val(),
                   id_convenio: $('#convenio').val()
            }
        }       
    });
    
    jQuery('#tabla_reporte_clientes').trigger("reloadGrid");
}

function  exportarExcel(op, tableid) {
    var fullData = jQuery("#"+tableid).jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: './controller?estado=Maestro&accion=Libranza',
        data: {
            listado: myJsonString,
            opcion: op
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function resetSearchValues(){     
    $('#fecha_ini').datepicker("setDate", new Date()); 
    $('#fecha_fin').datepicker("setDate", new Date());  
    $('#convenio').val('');
}


function DeshabilitarFiltro(estado){         
        (estado) ? $('#fecha_ini').datepicker("destroy") : $('#fecha_ini').datepicker({dateFormat: 'yy-mm-dd',
                                                                                       changeMonth: true,
                                                                                       changeYear: true,
                                                                                       maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
                                                                                       defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())});
        (estado) ? $('#fecha_fin').datepicker("destroy") : $('#fecha_fin').datepicker({dateFormat: 'yy-mm-dd',
                                                                                       changeMonth: true,
                                                                                       changeYear: true,
                                                                                       maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
                                                                                       defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())});    
        $('#convenio').attr({disabled: estado});      
} 

function cargarConveniosPagaduria() {

        $('#convenio').empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Maestro&accion=Libranza",
            dataType: 'json',
            data: {
               opcion: 75
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error);
                        return;
                    }
                    try {
                        $('#convenio').append("<option value=''>...</option>");                 
                     
                        for (var key in json) { 
                           $('#convenio').append('<option value=' + key + '>' + json[key] + '</option>');                      
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key]);
                    }

                } else {

                    alert("Lo sentimos no se encontraron convenios!!");

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}