/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
  $(document).ready(function () {   

       //    
    $('#listar_Anticipos').click(function() {
       cargarAnticipos();
    });
    
        $('#limpiar').click(function () {
        $("#conductor").val('');
        $("#placa").val('');
        $("#planilla").val('');
    });

});

function mensajesDelSistema(msj, width, height, swHideDialog) {   
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}

function cargarAnticipos() {
    //console.log('Entra aqui');
    var grid_tabla_ = jQuery("#tabla_Anticipos");
    if ($("#gview_tabla_Anticipos").length) {
        reloadGridMostrar(grid_tabla_, 110);
    } else {
    //alert('Entra aqui');    
        grid_tabla_.jqGrid({
            
            caption: "Listado anticipos estaciones",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '1430',
            colNames: ['Id','Planilla','Fecha','Valor','Valor Anticipos','Valor CXP','CXP1','CXP2','Conductor','Valor Gasolina','Valor Efectivo','Propietario','Nit Propietario',
                'Estacion','Autorizado','Accion'],
            colModel: [
                {name: 'id', index: 'id', width: 90, sortable: true, align: 'center', hidden: true, search: true, key: true},
                {name: 'planilla', index: 'planilla', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_anticipo', index: 'fecha_anticipo', width: 100, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'valor', index: 'valor', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'plata_anticipos_de_planilla', index: 'plata_anticipos_de_planilla', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'plata_cxp_de_planilla', index: 'plata_cxp_de_planilla', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cxp_posible_corrida_cheque', index: 'cxp_posible_corrida_cheque', width: 220, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cxp_posible2_corrida_cheque', index: 'cxp_posible2_corrida_cheque', width: 220, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'conductor', index: 'conductor', width: 220, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'vlr_gasolina', index: 'vlr_gasolina', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'vlr_efectivo', index: 'vlr_efectivo', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'propietario', index: 'propietario', width: 220, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nit_propietario', index: 'nit_propietario', width: 100, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'estacion', index: 'estacion', width: 200, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'autorizado', index: 'autorizado', width: 100, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'left', hidden: false}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 110,
                    conductor:$('#conductor').val(),
                    placa:$('#placa').val(),
                    planilla:$('#planilla').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_Anticipos").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var autorizado = $("#tabla_Anticipos").getRowData(cant[i]).autorizado;
                    var cl = cant[i];
                if (autorizado === 'N') {
                    //be = '<img src = "/fintra/images/check.png" style = "margin-left: 20px;horizontal-align: middle; height: 20px; width: 20px;vertical-align: middle;"onclick ="autorizarAnticipo(' + cl + ')" title="Autorizar">';
                    be = '<input style="height:20px;width:68px;margin-left: 8px;margin-right: 8px;" type="button" title="Subir" value="Autorizar" onclick="autorizarAnticipo(' + cl + ')" />';
                } 
                else {
                    be = '<input style="height:20px;width:68px;margin-left: 8px;margin-right: 8px;" type="button"disabled value="Autorizar" onclick="" />';
                }
                jQuery("#tabla_Anticipos").jqGrid('setRowData', cant[i], {estado: be});                
                
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                /*operacion = 'Editar';
                var myGrid = jQuery("#tabla_Anticipos"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var nombre_poliza = filas.nombre_poliza;
                var descripcion = filas.descripcion;
                if (estado === 'Activo') {
                    ventanaPolizas(operacion, id, nombre_poliza, descripcion);
                } else if (estado === 'Inactivo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }*/
            }
        })/*.navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_Anticipos").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaPolizas(operacion);
            }
        });*/
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                conductor:$('#conductor').val(),
                placa:$('#placa').val(),
                planilla:$('#planilla').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
    
}



function autorizarAnticipo(rowid) {
    console.log('Entra aqui');
    var grid_tabla = jQuery("#tabla_Anticipos");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 111,
            id: id
        },
        success: function (data) {
            cargarAnticipos();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}