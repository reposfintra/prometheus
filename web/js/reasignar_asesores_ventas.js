$(document).ready(function () {
    ComboAsesores();
    CargarSolicitudesAsignar();
    
});


function CargarSolicitudesAsignar() {
    var grid_tabla = $("#tabla_solicitudes_x_asignar"); 
    
    if ($("#gview_tabla_solicitudes_x_asignar").length) {
        reloadGridTabla(grid_tabla, 16);
    } else {
        grid_tabla.jqGrid({
            caption: "Reasignar Asesores",
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '1060',
            colNames: ['Numero Solicitud','Identificacion','Nombre','Asesor','Convenio','Estado','Fecha Creacion','Valor'],
            colModel: [
                {name: 'numero_solicitud', index: 'numero_solicitud', width: 90,sortable: true, align: 'center', hidden: false},
                {name: 'identificacion', index: 'identificacion', width: 90, sortable: true, align: 'center',   hidden: false},
                {name: 'nombre', index: 'nombre', width: 200, sortable: true, align: 'center',   hidden: false},
                {name: 'asesor', index: 'asesor', width: 110, sortable: true, align: 'center',   hidden: false}, 
                {name: 'convenio', index: 'asesor', width: 110, sortable: true, align: 'center',   hidden: false},
                {name: 'estado_sol', index: 'estado_sol', width: 110, sortable: true, align: 'center',   hidden: false},
                {name: 'creation_date', index: 'creation_date', width: 150, sortable: true, align: 'center',   hidden: false},
                {name: 'monto_credito', index: 'monto_credito', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#pager',
            pgtext: null,
            pgbuttons: false,
            multiselect:true,           
            
            jsonReader:{
                root: "rows",
                repeatitems: false,
                id: "0"
                
            },
            loadComplete: function () {
                
                if (grid_tabla.jqGrid('getGridParam', 'records') <= 0) {
                    mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            ajaxGridOptions: {
                
                data: {
                    opcion: 16  
                }                
                
            },
            gridComplete: function() { 
                
                var colSumTotal = jQuery("#tabla_solicitudes_x_asignar").jqGrid('getCol', 'monto_credito', false, 'sum'); 
                jQuery("#tabla_solicitudes_x_asignar").jqGrid('footerData', 'set', {monto_credito: colSumTotal});
                
            },            
           
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);                
            }
        }).navGrid('#pager', {add: false, edit: false, del: false, search: false, refresh: false});
        $("#tabla_solicitudes_x_asignar").navButtonAdd('#pager', {
            caption: "Asignar Asesor",
            title: "Asignar Asesor",
            onClickButton: function () {
                if ($("#asesor").val()!="") {
                     AsignarAsesor();
                }else {
                   mensajesDelSistema("Por favor seleccione un asesor.", '250', '150', true);   
                }
               
            }
        });      
           
    }
    
}

function reloadGridTabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",
       
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op   
            }
        }
    });
    grid_tabla.trigger("reloadGrid");    
     
}

function loading(msj, width, height) {

    $("#msj").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                
                $(this).dialog("close");
            }
        }
    });

}


function ComboAsesores() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 17
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
            $('#asesor').html('');
            $('#asesor').append('<option value="" >...</option>');
            for (var datos in json) {
                $('#asesor').append('<option value=' + datos + ' >' + json[datos] + '</option>');
            }
           

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function AsignarAsesor(){
     
    var jsonsolicitudes =[];
    var filasId =jQuery('#tabla_solicitudes_x_asignar').jqGrid('getGridParam', 'selarrrow');
    if (filasId != ''){
        for (var i = 0; i < filasId.length; i++) {        
            var num_solicitud = jQuery("#tabla_solicitudes_x_asignar").getRowData(filasId[i]).numero_solicitud;   
           
            var solicitudes = {};
            solicitudes ["numero_solicitud"] = num_solicitud;           
            jsonsolicitudes.push(solicitudes); 
            
        }    
        var listsolicitudes = {};
        listsolicitudes ["solicitudes"] = jsonsolicitudes;       
      
       loading("Espere un momento por favor...", "270", "140");

            $.ajax({
                type: 'POST',
                url: "./controller?estado=Negocios&accion=Fintra",
                dataType: 'json',
                data: {
                    opcion: 18,
                    asesor : $("#asesor").val(),
                    listadosolicitudes: JSON.stringify(listsolicitudes),
                   
                },
                success: function (json) {             
                    if (!isEmptyJSON(json)) {
                       
                         if (json.respuesta === "OK"){
                            mensajesDelSistema("Exito al asignar asesores", '250', '150', true);
                            $("#dialogLoading").dialog('close');
                            CargarSolicitudesAsignar();
                        }
                        
                        else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Error al asignar asesores" , '250', '150');
                        }

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos ocurri� un error al asinar el asesor!!", '250', '150');

                    }
                },
                error: function (xhr) {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Error al asignar al asesor " + "\n" +
                             xhr.responseText, '650', '250', true);
                }
            });     
        
    }else{
        if (jQuery("#tabla_solicitudes_x_asignar").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las solicitudes a asignar!!", '250', '150');
        } else {
            mensajesDelSistema("No hay solicitudes para procesar", '250', '150');
        }             
    } 
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}