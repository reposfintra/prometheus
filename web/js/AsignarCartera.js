/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    cargarComboUnidNegocio();
    cargarCboTerceros();
});


function cargarComboUnidNegocio(){
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 27
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
                $('#id_unidad_negocio').html('');
                $('#id_unidad_negocio').append('<option value="" >...</option>');
                for (var datos in json) {
                    $('#id_unidad_negocio').append('<option value="' + datos + '" >' + json[datos] + '</option>');
                }
           

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    
}

function cargarNegociosPorAsignar() {
    if ($("#id_unidad_negocio").val()!="" && $("#id_agencia").val()!="" && $("#id_altura_mora").val()!=""){
        
    var grid_tabla = $("#tabla_negocios");
    if ($("#gview_tabla_negocios").length) {
        reloadGridTabla(grid_tabla);
    } else {
        
        grid_tabla.jqGrid({
            caption: "Reporte",
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '500',
            autowidth: true,
            colNames: ['Cedula','Nombre','Direccion domicilio','Ciudad domicilio','Barrio domicilio','Telefono','Celular','Direccion Negocio','Barrio Negocio',
                       'Telefono Negocio','Negocio', 'Unidad Negocio','Agencia','Altura Mora Actual','Dia Pago','Saldo Actual','Saldo vencido','Interes Mora',
                       'Gac','Total A Pagar','Juridica','Reestructuracion','Fecha Ultimo Compromiso','Responsable Cuenta' ],
            colModel: [
                {name: 'cedula',        index: 'cedula',        width: 90,sortable: true, align: 'center', hidden: false},
                {name: 'nombre_cliente',index: 'nombre_cliente',width: 300, sortable: true, align: 'center',   hidden: false},
                {name: 'direccion_domicilio',index: 'direccion_domicilio',width: 180, sortable: true, align: 'center',   hidden: false},
                {name: 'ciudad_domicilio',       index: 'ciudad_domicilio',       width: 120, sortable: true, align: 'center',   hidden: false},
                {name: 'barrio_domicilio',       index: 'barrio_domicilio',       width: 120, sortable: true, align: 'center',   hidden: false},
                {name: 'telefono',       index: 'telefono',       width: 110, sortable: true, align: 'center',   hidden: false},                
                {name: 'celular',       index: 'celular',       width: 110, sortable: true, align: 'center',   hidden: false},                
                {name: 'direccion_negocio',       index: 'direccion_negocio',       width: 110, sortable: true, align: 'center',   hidden: false},                
                {name: 'barrio_negocio',       index: 'barrio_negocio',       width: 110, sortable: true, align: 'center',   hidden: false},                
                {name: 'telefon_negocio',       index: 'telefon_negocio',       width: 90, sortable: true, align: 'center',   hidden: false},              
                {name: 'negocio',       index: 'negocio',       width: 120, sortable: true, align: 'center',   hidden: false},              
                {name: 'unidad_negocio',       index: 'unidad_negocio',       width: 120, sortable: true, align: 'center',   hidden: false},   
                {name: 'agencia',       index: 'agencia',       width: 120, sortable: true, align: 'center',   hidden: false},
                {name: 'altura_mora_actual',       index: 'altura_mora_actual',       width: 120, sortable: true, align: 'center',   hidden: false}, 
                {name: 'dia_pago',       index: 'dia_pago',       width: 80, sortable: true, align: 'center',   hidden: false}, 
                {name: 'saldo_actual', index: 'saldo_actual', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}}, 
                {name: 'valor_saldo_vencido', index: 'valor_saldo_vencido', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}}, 
                {name: 'interes_mora', index: 'interes_mora', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'gac', index: 'gac', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'total_a_pagar', index: 'total_a_pagar', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'juridica',       index: 'juridica',       width: 120, sortable: true, align: 'center',   hidden: false} ,
                {name: 'reestructuracion',       index: 'reestructuracion',       width: 120, sortable: true, align: 'center',   hidden: false}, 
                {name: 'fecha_ult_compromiso',       index: 'fecha_ult_compromiso',       width: 120, sortable: true, align: 'center',   hidden: false}, 
                {name: 'responsable_cuenta',       index: 'responsable_cuenta',       width: 120, sortable: true, align: 'center',   hidden: false} 
                
                 
            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#pager',
            pgtext: null,
            pgbuttons: false,
            multiselect:true,           
            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
                
            },
            loadComplete: function () {
                
               var saldo_actual = $("#tabla_negocios").jqGrid('getCol', 'saldo_actual', false, 'sum'); 
                var valor_saldo_vencido = $("#tabla_negocios").jqGrid('getCol', 'valor_saldo_vencido', false, 'sum'); 
                var interes_mora = $("#tabla_negocios").jqGrid('getCol', 'interes_mora', false, 'sum'); 
                var gac = $("#tabla_negocios").jqGrid('getCol', 'gac', false, 'sum'); 
                var total_a_pagar = $("#tabla_negocios").jqGrid('getCol', 'total_a_pagar', false, 'sum'); 
                $("#tabla_negocios").jqGrid('footerData', 'set', {saldo_actual: saldo_actual,
                    valor_saldo_vencido:valor_saldo_vencido,interes_mora:interes_mora,gac:gac,total_a_pagar:total_a_pagar});
            },
            ajaxGridOptions: {
                
                data: {
                    opcion: 30,
                    id_unidad_negocio: $("#id_unidad_negocio").val(),             
                    id_agencia: $("#id_agencia").val(),             
                    id_altura_mora: $("#id_altura_mora").val(),             
                    id_responsable_cuenta: $("#id_responsable_cuenta").val(),             
                    id_ciudad: $("#id_ciudad").val(),     
                    id_barrio: $("#id_barrio").val()             

                }                
                
            },
            gridComplete: function() { 
                
                var colSumTotal = $("#tabla_detalle_caja_recaudo").jqGrid('getCol', 'vlr_ingreso', false, 'sum'); 
                $("#tabla_negocios").jqGrid('footerData', 'set', {vlr_ingreso: colSumTotal});
                             
            }, 
            
           
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
                
            }
        }).navGrid('#pager', {add: false, edit: false, del: false, search: false, refresh: false});
           
    }
     }else {
            mensajesDelSistema("Por favor seleccione todas las opciones",'250', '150');
            }
    
}


function reloadGridTabla(grid_tabla) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",
       
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 30,
                id_unidad_negocio: $("#id_unidad_negocio").val(),             
                id_agencia: $("#id_agencia").val(),             
                id_altura_mora: $("#id_altura_mora").val(),             
                id_responsable_cuenta: $("#id_responsable_cuenta").val(),             
                id_ciudad: $("#id_ciudad").val(),     
                id_barrio: $("#id_barrio").val()  
            }
        }
    });
    grid_tabla.trigger("reloadGrid");    
     
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                
                $(this).dialog("close");
            }
        }
    });

}

function  exportarNegocios() {
    var fullData = $("#tabla_negocios").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Negocios&accion=Fintra",
        data: {
            opcion: 29,
            listado: myJsonString
            
        },
        success: function () {
           
            cerrarDiv("#divSalidaEx");
             mensajesDelSistema ("Documento generado con exito, puede consultarlo en la carpeta descargas");
        },
        error: function () {
            cerrarDiv("#divSalidaEx");
            mensajesDelSistema("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function cerrarDiv(div)
{
    $(div).dialog('close');
}

function cargarCboTerceros(){
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 36
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
                $('#id_recaudador_tercero').html('');
                $('#id_recaudador_tercero').append('<option value="" >...</option>');
                for (var datos in json) {
                    $('#id_recaudador_tercero').append('<option value="' + datos + '" >' + json[datos] + '</option>');
                }

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    
}

function asignarCarteraTercero(){
    if (document.getElementById("tabla_negocios").rows.length <= 1){
         mensajesDelSistema ("No hay registros por asignar",'250', '150');
    }else if($("#id_recaudador_tercero").val()===""){
         mensajesDelSistema ("Por favor seleccione recaudador",'250', '150');
    }else{
        var jsonnegocios =[];
        var filasId =$('#tabla_negocios').jqGrid('getGridParam', 'selarrrow');
        if (filasId !== ''){
        for (var i = 0; i < filasId.length; i++) {        
            var negocio = $("#tabla_negocios").getRowData(filasId[i]).negocio;   
           
            var negocios = {};
            negocios ["negocio"] = negocio;           
            jsonnegocios.push(negocios); 
        }    
        var listnegocios = {};
        listnegocios ["negocio"] = jsonnegocios;   
         loading("Espere un momento por favor...", "270", "140");
         
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Negocios&accion=Fintra",
                dataType: 'json',
                data: {
                    opcion: 31,
                    id_unidad_negocio: $("#id_unidad_negocio").val(),             
                    id_agencia: $("#id_agencia").val(),             
                    id_altura_mora: $("#id_altura_mora").val(),             
                    empresa_responsable: $("#id_recaudador_tercero").val(),             
                    id_ciudad: $("#id_ciudad").val(),     
                    id_barrio: $("#id_barrio").val(),
                    listnegocios: JSON.stringify(listnegocios)

                },
                success: function (json) {             
                                       
                         if (json.respuesta==="OK") {
                             $("#dialogLoading").dialog('close');
                             $("#tabla_negocios").jqGrid("GridUnload");
                            mensajesDelSistema ("Asignaci�n exitosa",'250', '150');
                        }else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema ("No se asigno la cartera",'250', '150');
                        }
                },
                error: function (xhr) {
                     $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Error al refinanciar " + "\n" +
                             xhr.responseText, '650', '250', true);
                }
            }); 
            }else{
        if ($("#tabla_negocios").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar el(los) negocio(s) a asignar!!", '250', '150');
        } else {
            mensajesDelSistema("No hay negocios para asignar", '250', '150');
        }             
    }
        
    }
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}


function cargarCboResponsable(sucursal){
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 39,
            sucursal:sucursal 
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
                $('#id_responsable_cuenta').html('');
                $('#id_responsable_cuenta').append('<option value="" >...</option>');
                for (var datos in json) {
                    $('#id_responsable_cuenta').append('<option value="' + datos + '" >' + json[datos] + '</option>');
                }

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    
}

function cargarCboCiudad(sucursal){
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 40,
            sucursal:sucursal 
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           var data=[];
            $('#id_ciudad').html('');
            $('#id_ciudad').append('<option value="" >...</option>');
            for (var datos in json) {
                data.push({
                    id: datos,
                    value:json[datos]
                });
                
            }
            data = data.sort(function(a,b){
                if(a.value < b.value) { return -1; }
                if(a.value > b.value) { return 1; }
                return 0;
            });
            for(var i= 0;i < data.length; i++){
                $('#id_ciudad').append('<option value="' + data[i].id + '" >' + data[i].value + '</option>');
            }

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    
}


function cargarCboBarrio(id_ciudad){
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 41,
            id_ciudad:id_ciudad 
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
                $('#id_barrio').html('');
                $('#id_barrio').append('<option value="" >...</option>');
                for (var datos in json) {
                    $('#id_barrio').append('<option value="' + datos + '" >' + json[datos] + '</option>');
                }

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    
}