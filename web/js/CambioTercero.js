/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $("#contenedor1").hide();
    $("#buscar").click(function () {
        var comprobante = $("#comprobante").val();
        if (comprobante === '') {
            mensajesDelSistema("Digitar Comprobante", '230', '150', false);
        } else {
            // BuscarComprobantes();
            loading("Espere un momento se estan cargando la informacion.", "300", "140");
            setTimeout(function () {
                BuscarComprobantes();
                $("#comprobante").attr('readOnly', true);

            }, 200);

            $("#contenedor1").show();
        }
    });
    $("#limpiar").click(function () {
        $("#comprobante").val('');
        $("#comprobante").attr('readOnly', false);
        $("#contenedor1").hide();
    });

});

function loading(msj, width, height) {
    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });
    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();
}

function BuscarComprobantes() {
    var grid_tabla_ = jQuery("#tabla_comprobantes");


    if ($("#gview_tabla_comprobantes").length) {
        reloadGridComprobantes(grid_tabla_, 1);
    } else {
        grid_tabla_.jqGrid({
            caption: "Detalle Comprobante",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '400',
            width: '1500',
            colNames: ['# Documento', 'Tipo Documento', 'Tercero', 'Grupo Transaccion', 'Periodo', 'Transaccion', 'Cuenta', 'Detalle', 'Nuevo Tercero', 'Nuevo Detalle', 'Valor Debito', 'Valor Credito', 'Fecha Creacion', 'Usuario Creacion'],
            colModel: [
                {name: 'numdoc', index: 'numdoc', width: 90, sortable: true, align: 'left', hidden: false},
                {name: 'tipodoc', index: 'tipodoc', width: 90, sortable: true, align: 'left', hidden: false},
                {name: 'tercero', index: 'tercero', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'grupo_transaccion', index: 'grupo_transaccion', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'periodo', index: 'periodo', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'transaccion', index: 'transaccion', width: 100, sortable: true, align: 'left', hidden: false, key: true},
                {name: 'cuenta', index: 'cuenta', width: 130, sortable: true, align: 'left', hidden: false},
                {name: 'detalle', index: 'detalle', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'nit_tercero', index: 'nit_tercero', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'nuevo_detalle', index: 'nuevo_detalle', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'valor_debito', index: 'valor_debito', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_credito', index: 'valor_credito', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'creation_date', index: 'creation_date', width: 200, sortable: true, align: 'left', hidden: true},
                {name: 'creation_user', index: 'creation_user', width: 200, sortable: true, align: 'left', hidden: true}
            ],
            rowNum: 100000,
            rowTotal: 100000,
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            loadComplete: function (id, rowid) {
                $("#dialogo2").dialog("close");
                if (grid_tabla_.jqGrid('getGridParam', 'records') > 0) {

                    $("#contenedor1").show();
                    var allRowsInGrid = grid_tabla_.jqGrid('getRowData');
                    for (var i = 0; i < allRowsInGrid.length; i++) {
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].transaccion, "nuevo_detalle", "", {'background-color': 'rgba(255, 255, 0, 0.57)', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].transaccion, "nit_tercero", "", {'background-color': 'rgba(245, 140, 112, 0.53)', 'background-image': 'none'});
                    }
                    var valor_debito = grid_tabla_.jqGrid('getCol', 'valor_debito', false, 'sum');
                    var valor_credito = grid_tabla_.jqGrid('getCol', 'valor_credito', false, 'sum');

                    grid_tabla_.jqGrid('footerData', 'set', {nuevo_detalle: 'TOTAL', valor_debito: valor_debito, valor_credito: valor_credito});
                } else {
                    mensajesDelSistema("No hay resultados", '250', '150', false);
                }

            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 1,
                    comprobante: $("#comprobante").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_comprobantes").navButtonAdd('#pager1', {
            caption: "Actualizar",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                var con = 0;
                if (info > 0) {
                    var ids = grid_tabla_.jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        var nit_tercero = grid_tabla_.getRowData(cl).nit_tercero;
                        var nuevo_detalle = grid_tabla_.getRowData(cl).nuevo_detalle;
                        if ((nuevo_detalle === '') || (nit_tercero === '')) {
                            con = con + 1;
                            break;
                        }
                    }
                    if (con > 0) {
                        mensajesDelSistema("No se puede realizar la actualizacion", '250', '150', false);
                    } else {
                        actualizarComprobante();
                    }
                } else {
                    mensajesDelSistema("No hay informacion para actualizar porque no cumple el formato", '250', '150', false);
                }
            }
        });
        $("#tabla_comprobantes").navButtonAdd('#pager1', {
            caption: "Eliminar",
            onClickButton: function () {
                var comprobante = $("#comprobante").val();
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    Confirmacion("�Desea eliminar el coprobante?", '300', '150', comprobante);
                } else {
                    mensajesDelSistema("No hay informacion para eliminar", '250', '150', false);
                }
            }
        });
    }
}

function reloadGridComprobantes(grid_tabla_, opcion) {
    grid_tabla_.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: opcion,
                comprobante: $("#comprobante").val()
            }
        }
    });
    grid_tabla_.trigger("reloadGrid");
}

function actualizarComprobante() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 2,
            comprobante: $("#comprobante").val()
        }, success: function (json) {
            if (json.respuesta === 'Guardado') {
                mensajesDelSistema("Exito al actualizar el comprobante", '230', '150', true);
                reloadGridComprobantes(jQuery("#tabla_comprobantes"), 1);
            } else if (json.respuesta === 'ERROR') {
                mensajesDelSistema("No se realizaron cambios", '230', '150', false);
            }
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}
function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        title:"Mensaje",
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
    //  $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function Confirmacion(msj, width, height, comprobante) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "SI": function () {
                eliminarComprobante(comprobante);
                $(this).dialog("close");
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });
}

function eliminarComprobante(comprobante) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 3,
            comprobante: comprobante
        },
        success: function (json) {
            if (json.respuesta === 'Eliminado') {
                mensajesDelSistema("Comprobante Eliminado.", '230', '150', true);
                jQuery("#tabla_comprobantes").jqGrid("clearGridData", true);
                $("#contenedor1").hide();
            } else if (json.respuesta === 'ERROR') {
                mensajesDelSistema("No se realizaron cambios", '230', '150', false);
            }
        },
        error: function (result) {
            alert('ERROR');
        }
    });
}

