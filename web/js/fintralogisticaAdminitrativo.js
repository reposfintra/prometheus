/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    var myDate = new Date();
    // $("#fecha").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

    $("#buscar").click(function () {
        var empresa = $("#empresa").val();
        if (empresa !== '') {
            cargarcontrolDocumento();
        } else {
            mensajesDelSistema("Debe seleccionar una empresa", 250, 150);
        }
    });
    cargarEmpresa();

});

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarcontrolDocumento() {
    var grid_tabla = jQuery("#tabla_control_documentos");
    if ($("#gview_tabla_control_documentos").length) {
        reloadGridcontrol(grid_tabla, 1);
    } else {
        grid_tabla.jqGrid({
            caption: "Documentos",
            url: "./controller?estado=fintralogistica&accion=Administrativo",
            mtype: "POST",
            datatype: "json",
            height: '400',
            width: '1500',
            colNames: ['Empresa', 'Planilla','Fecha Anticipo', 'Venta','Fecha Venta', 'Reanticipo', 'Fecha Corrida', 'Corrida', 'Documento', 'Fecha Referencia', 'Tipo documento', '# Ingreso'],
            colModel: [
                {name: 'empresa', index: 'empresa', width: 190, sortable: true, align: 'left', hidden: false},
                {name: 'planilla', index: 'planilla', width: 120, sortable: true, align: 'left', hidden: false, key: true},
                {name: 'fecha_anticipo', index: 'fecha_anticipo', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'venta', index: 'venta', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'fecha_venta', index: 'fecha_venta', width: 150, sortable: true, align: 'center', hidden: false},
                {name: 'reanticipo', index: 'reanticipo', width: 60, sortable: true, align: 'center', hidden: false},
                {name: 'fecha_corrida', index: 'fecha_corrida', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'cxc_corrida', index: 'cxc_corrida', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'documento', index: 'documento', width: 150, sortable: true, align: 'center', hidden: false},
                {name: 'referencia1', index: 'referencia1', width: 110, sortable: true, align: 'center', hidden: false},
                {name: 'tipdoc_ultimoingreso', index: 'tipdoc_ultimoingreso', width: 110, sortable: true, align: 'center', hidden: false},
                {name: 'numingreso_ultimoingreso', index: 'numingreso_ultimoingreso', width: 110, sortable: true, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 100000,
            loadonce: true,
            rownumWidth: 50,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            onCellSelect: function (rowid, index, contents, event) {
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
            },
            ajaxGridOptions: {
                data: {
                    opcion: 1,
                    planilla: $("#planilla").val(),
                    ventas: $("#ventas").val(),
                    corrida: $("#corrida").val(),
                    fecha: $("#fecha").val(),
                    empresa: $("#empresa").val(),
                    ingreso: $("#ingreso").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, gridComplete: function (index) {
                var ids = grid_tabla.jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var venta = grid_tabla.getRowData(cl).venta;
                    var documento = grid_tabla.getRowData(cl).documento;
                    var ingreso = grid_tabla.getRowData(cl).numingreso_ultimoingreso;
                    var tipodoc = grid_tabla.getRowData(cl).tipdoc_ultimoingreso;
                    if ((venta === 'No')) {
                        $("#tabla_control_documentos").jqGrid('setRowData', ids[i], false, {weightfont: 'bold', background: '#F5A9A9'});
                    }
                    if ((documento !== '') && (ingreso === '')) {
                        $("#tabla_control_documentos").jqGrid('setRowData', ids[i], false, {weightfont: 'bold', background: '#F1ED1A'});
                    }
                    if (documento === '' && ingreso === '' && venta==='Si') {
                        $("#tabla_control_documentos").jqGrid('setRowData', ids[i], false, {weightfont: 'bold', background: 'rgba(236, 134, 19, 0.81)'});
                    }
                    if ((tipodoc !== '') && (ingreso === '')) {
                        $("#tabla_control_documentos").jqGrid('setRowData', ids[i], false, {weightfont: 'bold', background: '#ADF080'});
                    }
                }
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_control_documentos").navButtonAdd('#pager1', {
            caption: "Exportar Excel",
            onClickButton: function () {
                var info = grid_tabla.getGridParam('records');
                if (info > 0) {
                    exportarInfo();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
        $("#tabla_control_documentos").navButtonAdd('#pager1', {
            caption: "Generar IA Pendientes",
            onClickButton: function () {
                Mostrar();
            }
        });
    }
}

function reloadGridcontrol(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=fintralogistica&accion=Administrativo",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                planilla: $("#planilla").val(),
                ventas: $("#ventas").val(),
                corrida: $("#corrida").val(),
                fecha: $("#fecha").val(),
                empresa: $("#empresa").val(),
                ingreso: $("#ingreso").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function cargarEmpresa() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=fintralogistica&accion=Administrativo",
        dataType: 'json',
        async: false,
        data: {
            opcion: 2
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    return;
                }
                try {
                    $('#empresa').append("<option value=''>  </option>");
                    for (var datos1 in json) {
                        $('#empresa').append('<option value=' + datos1 + '>' + json[datos1] + '</option>');
                    }
                } catch (exception) {
                }
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function  exportarInfo() {
    var fullData = jQuery("#tabla_control_documentos").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=fintralogistica&accion=Administrativo",
        data: {
            listado: myJsonString,
            opcion: 3
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

//function listarIAPendientes() {
//    $.ajax({
//        type: 'POST',
//        dataType: 'json',
//        url: "./controller?estado=fintralogistica&accion=Administrativo",
//        data: {
//            opcion: 4,
//            empresa: $("#empresa").val()
//        },
//        success: function (data, textStatus, jqXHR) {
//            var info = data.rows.length;
//            alert(info);
//        },
//        error: function (result) {
//            alert('ERROR');
//        }
//    });
//}


function Mostrar() {
    cargarResultadoIA();
    $("#dialogIA").dialog({
        width: '730',
        height: '600',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title:'Documentos',
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                cargarcontrolDocumento();
                $(this).dialog("destroy");
            }
        }
    });
}

function cargarResultadoIA() {
    var grid_tabla = jQuery("#tabla_resultado_IA");
    if ($("#gview_tabla_resultado_IA").length) {
        reloadGridresultado(grid_tabla, 4);
    } else {
        grid_tabla.jqGrid({
            //caption: "Documentos",
            url: "./controller?estado=fintralogistica&accion=Administrativo",
            mtype: "POST",
            datatype: "json",
            height: '400',
            width: '700',
            colNames: ['Empresa', 'Fecha Corrida', 'Corrida', 'Generado'],
            colModel: [
                {name: 'empresa', index: 'empresa', width: 190, sortable: true, align: 'left', hidden: false},
                {name: 'fecha_corrida', index: 'fecha_corrida', width: 150, sortable: true, align: 'center', hidden: false},
                {name: 'cxc_corrida', index: 'cxc_corrida', width: 150, sortable: true, align: 'center', hidden: false},
                {name: 'respuesta', index: 'respuesta', width: 110, sortable: true, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 100000,
            loadonce: true,
            rownumWidth: 50,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pager: '#pager2',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
            },
            ajaxGridOptions: {
                data: {
                    opcion: 4,
                    empresa: $("#empresa").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}

function reloadGridresultado(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=fintralogistica&accion=Administrativo",
        ajaxGridOptions: {
            type: "POST",
            data: {
                 opcion: op,
                empresa: $("#empresa").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}