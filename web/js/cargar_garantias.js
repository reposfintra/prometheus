/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    loading("Verificando si existen archivos sin procesar...", "270", "140");
    $("#ctrl_carga").val("");
    setTimeout(function () {
        mostrarArchivoPrecarga();
    }, 500);

    $('#cargarArchivo').click(function () {
        if ($("#ctrl_carga").val()===''){
            cargarArchivoFasecolda();
        }else{
           mensajesDelSistema("Ya existe un archivo precargado en el sistema.", '250', '150'); 
        }
    });

});

function cargarArchivoFasecolda() {

    var fd = new FormData(document.getElementById('formulario'));
    var archivo = document.getElementById('archivo').value;

    if (archivo === "") {
        mensajesDelSistema("No ha seleccionado ning&uacute;n archivo. Por favor, seleccione uno!!", '250', '150');
        return;
    }

    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function () {
        $.ajax({
            async: false,
            url: "./controller?estado=Garantias&accion=Creditos&opcion=1",
            type: 'POST',
            dataType: 'json',
            data: fd,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (json) {
                $("#archivo").val("");
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema(json.error, '250', '175');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Archivo cargado exitosamente", '250', '150', true);
                        mostrarArchivoPrecarga();
                        return;
                    }

                } else {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci&oacute;n!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }, 500);
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}
function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function mostrarArchivoPrecarga(){
    var grid_fasecolda = $("#archivo_fasecolda");
    if ($("#gview_archivo_fasecolda").length) {
            reloadGridFasecolda(grid_fasecolda);
        } else {

            grid_fasecolda.jqGrid({
                caption: "Informacion fasecolda",
                url: "./controller?estado=Garantias&accion=Creditos",
                mtype: "POST",
                datatype: "json",
                height: '500',
                width: '1800',
                colNames: ['Estado','Codigo Fasecolda', 'Marca', 'Clase', 'Servicio', 'Referencia 1', 'Referencia 2', 'Referencia 3','Peso',
                    '1970', '1971', '1972', '1973', '1974', '1975', '1976', '1977', '1978', '1979', '1980', '1981', '1982',
                    '1983', '1984', '1985', '1986', '1987', '1988', '1989', '1990', '1991', '1992', '1993', '1994', '1995',
                    '1996', '1997', '1998', '1999', '2000', '2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008',
                    '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020', '2021',
                    '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030', 'Bcpp'
                            , 'Nacionalidad', 'Potencia', 'Tipo caja', 'Cilindraje', 'Pasajeros', 'Cap.Carga', 'Puerta', 'Aire', 'Ejes',
                    'Combustible', 'Transmision'],
                colModel: [
                    {name: 'estado', index: 'estado', width: 100, sortable: true, align: 'center'},
                    {name: 'codigo', index: 'codigo', width: 100, sortable: true, align: 'center', key: true},
                    {name: 'marca', index: 'marca', width: 100, align: 'center'},
                    {name: 'clase', index: 'clase', sortable: true, width: 120, align: 'center'},
                    {name: 'servicio', index: 'servicio', width: 120, align: 'center'},
                    {name: 'referencia1', index: 'ref1', width: 120, align: 'center'},
                    {name: 'referencia2', index: 'ref2', width: 120, align: 'center'},
                    {name: 'referencia3', index: 'ref3', width: 120, align: 'center'},
                    {name: 'peso', index: 'peso', width: 120, align: 'center'},
                    {name: '1970', index: '1970', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1971', index: '1971', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1972', index: '1972', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1973', index: '1973', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1974', index: '1974', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1975', index: '1975', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1976', index: '1976', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1977', index: '1977', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1978', index: '1978', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1979', index: '1979', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1980', index: '1980', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1981', index: '1981', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1982', index: '1982', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1983', index: '1983', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1984', index: '1984', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1985', index: '1985', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1986', index: '1986', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1987', index: '1987', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1988', index: '1988', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1989', index: '1989', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1990', index: '1990', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1991', index: '1991', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1992', index: '1992', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1993', index: '1993', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1994', index: '1994', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1995', index: '1995', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1996', index: '1996', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1997', index: '1997', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1998', index: '1998', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '1999', index: '1999', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2000', index: '2000', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2001', index: '2001', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2002', index: '2002', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2003', index: '2003', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2004', index: '2004', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2005', index: '2005', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2006', index: '2006', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2007', index: '2007', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2008', index: '2008', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2009', index: '2009', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2010', index: '2010', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2011', index: '2011', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2012', index: '2012', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2013', index: '2013', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2014', index: '2014', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2015', index: '2015', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2016', index: '2016', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2017', index: '2017', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2018', index: '2018', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2019', index: '2019', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2020', index: '2020', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2021', index: '2021', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2022', index: '2022', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2023', index: '2023', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2024', index: '2024', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2025', index: '2025', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2026', index: '2026', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2027', index: '2027', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2028', index: '2028', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2029', index: '2029', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: '2030', index: '2030', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'bcpp', index: 'bcpp', width: 120, align: 'center'},
                    {name: 'nacionalidad', index: 'nacionalidad', width: 120, align: 'center'},
                    {name: 'potencia', index: 'potencia', width: 120, align: 'center'},
                    {name: 'tipocaja', index: 'tipo_caja', width: 120, align: 'center'},
                    {name: 'cilindraje', index: 'cilinadraje', width: 120, align: 'center'},
                    {name: 'capacidadpasajeros', index: 'pasajeros', width: 120, align: 'center'},
                    {name: 'capacidadcarga', index: 'carga', width: 120, align: 'center'},
                    {name: 'puertas', index: 'puertas', width: 120, align: 'center'},
                    {name: 'aireacondicionado', index: 'aire', width: 120, align: 'center'},
                    {name: 'ejes', index: 'ejes', width: 120, align: 'center'},
                    {name: 'combustible', index: 'combustible', width: 120, align: 'center'},
                    {name: 'transmision', index: 'transmision', width: 120, align: 'center'}
                ],
                rowNum:1000,
                rowTotal: 200000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                pager: '#page_archivo_fasecolda',
                viewrecords: true,
                hidegrid: false,
                shrinkToFit: false,
                footerrow: false,
                multiselect: false
                ,ajaxGridOptions: {
                    async: false,
                    data: {
                        opcion: 9
                    }
                },loadComplete: function () {     
                    $("#dialogLoading").dialog('close');
                    if (grid_fasecolda.jqGrid('getGridParam', 'records') > 0) {
                        $("#contenedor").show();
                        $("#ctrl_carga").val("Existe");
                    }

                },
                loadError: function (xhr, status, error) {
                    alert(error, 250, 150);
                }
            }).navGrid("#page_archivo_fasecolda", {add: false, edit: false, del: false, search: false, refresh: false}, {});
            
            $("#archivo_fasecolda").navButtonAdd('#page_archivo_fasecolda', {
                caption: "Procesar Archivo",
                onClickButton: function (e) {
                    generarMaestrosFasecolda();
                }
            });
            
              $("#archivo_fasecolda").navButtonAdd('#page_archivo_fasecolda', {
                caption: "Limpiar",
                onClickButton: function (e) {
                  limpiarPreFasecolda();
                }
            });
        }
    
}

function reloadGridFasecolda(grid_fasecolda) {
    grid_fasecolda.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Garantias&accion=Creditos"
        ,ajaxGridOptions: {
            async: false,
            data: {
                opcion:9
            }
        }
    });
    grid_fasecolda.trigger("reloadGrid");
}

function  generarMaestrosFasecolda() {
    loading("Espere un momento procesando archivo.", "270", "140");
    setTimeout(function () {
        $.ajax({
            async: false,
            url: "./controller?estado=Garantias&accion=Creditos&opcion=10",
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $("#dialogLoading").dialog('close');
                if(json.respuesta==='OK'){
                       mensajesDelSistema("Archivo cargado exitosamente", '250', '150', true);
                       $("#contenedor").hide();
                       jQuery("#archivo_fasecolda").jqGrid("clearGridData", true);
                }else{
                      mensajesDelSistema("Lo sentimos algo salio mal al procesar el archivo.", '250', '150', true);
                }
                
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    }, 500);

}


function  limpiarPreFasecolda(){
    loading("Espere un momento borrando archivo.", "270", "140");
    setTimeout(function () {
        $.ajax({
            async: false,
            url: "./controller?estado=Garantias&accion=Creditos&opcion=11",
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $("#dialogLoading").dialog('close');
                if(json.respuesta==='OK'){
                       mensajesDelSistema("Archivo borrado exitosamente.!!", '250', '150', true);
                       $("#contenedor").hide();
                       $("#ctrl_carga").val("");
                       jQuery("#archivo_fasecolda").jqGrid("clearGridData", true);
                }else{
                      mensajesDelSistema("Lo sentimos algo salio mal al borrar el archivo.", '250', '150', true);
                }
                
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    }, 500);

}