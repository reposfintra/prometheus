/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    });
});

function initTipoActores() {
    listarTipoActores();
    maximizarventana();
}

function initActores() {
    //Caqrgamos la grilla de actores
    cargarActores();
    //Cargamos combo tipo de actores
    cargarCboTipoActores();
    //cargamos combo tipos de identificacion
    cargarTiposIdentificacion();
    //cargar paises.
    cargarPaises();
    cargarDepartamentos('CO', 'dep_dir');
    $('#dep_dir').val('ATL');
    cargarCiudad('ATL', "ciu_dir");
    $('#ciu_dir').val('BQ');
    cargarVias('BQ', "via_princip_dir");
    cargarVias('BQ', "via_genera_dir");

    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $("#pais").change(function () {
        var op = $(this).find("option:selected").val();
        cargarDepartamentos(op, "departamento");
    });

    $("#departamento").change(function () {
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciudad");
    });

    $("#dep_dir").change(function () {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciu_dir");
    });

    $("#ciu_dir").change(function () {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarVias(op, "via_princip_dir");
        cargarVias(op, "via_genera_dir");
    });

    $("#via_princip_dir").change(function () {
        $("#via_genera_dir").val('');
    });

    maximizarventana();

}

function listarTipoActores() {
    var grid_tbl_tipo_actores = jQuery("#tabla_tipo_actores");
    if ($("#gview_tabla_tipo_actores").length) {
        refrescarGridTipoActores();
    } else {
        grid_tbl_tipo_actores.jqGrid({
            caption: "TIPO DE ACTORES",
            url: "./controlleropav?estado=Maestro&accion=Proyecto",
            datatype: "json",
            height: '290',
            width: '480',
            cellEdit: true,
            colNames: ['Id', 'Descripcion', 'Estado', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'descripcion', index: 'descripcion', width: 320, align: 'left'},
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden: true},
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_tipo_actores'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 29,
                    mostrarTodos: 'S'
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_tipo_actores").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_tipo_actores").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoTipoActor('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_tipo_actores").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_tipo_actores"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;

                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                    editarTipoActor(id);
                }

            }
        }).navGrid("#page_tabla_tipo_actores", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_tipo_actores").jqGrid("navButtonAdd", "#page_tabla_tipo_actores", {
            caption: "Nuevo",
            onClickButton: function () {
                crearTipoActor();
            }
        });
    }

}

function refrescarGridTipoActores() {
    jQuery("#tabla_tipo_actores").setGridParam({
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 29,
                mostrarTodos: 'S'
            }
        }
    });

    jQuery('#tabla_tipo_actores').trigger("reloadGrid");
}

function crearTipoActor() {
    $('#div_tipo_actor').fadeIn('slow');
    $('#idTipoActor').val('');
    $('#descripcion').val('');
    AbrirDivCrearTipoActor();
}

function AbrirDivCrearTipoActor() {
    $("#div_tipo_actor").dialog({
        width: 'auto',
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR TIPO ACTOR',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarTipoActor();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_tipo_actor").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarTipoActor(cl) {

    $('#div_tipo_actor').fadeIn("slow");
    var fila = jQuery("#tabla_tipo_actores").getRowData(cl);
    var descripcion = fila['descripcion'];

    $('#idTipoActor').val(cl);
    $('#descripcion').val(descripcion);
    AbrirDivEditarTipoActor();
}

function AbrirDivEditarTipoActor() {
    $("#div_tipo_actor").dialog({
        width: 'auto',
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR TIPO ACTOR',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarTipoActor();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_tipo_actor").parent().find(".ui-dialog-titlebar-close").hide();
}

function guardarTipoActor() {
    var descripcion = $('#descripcion').val();

    if (descripcion !== '') {
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function () {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "./controlleropav?estado=Maestro&accion=Proyecto",
                data: {
                    opcion: ($('#idTipoActor').val() === '') ? 30 : 31,
                    id: $('#idTipoActor').val(),
                    descripcion: $('#descripcion').val()
                },
                success: function (json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '270', '165');
                            return;
                        }

                        if (json.respuesta === "OK") {
                            $("#dialogLoading").dialog('close');
                            refrescarGridTipoActores();
                            $("#div_tipo_actor").dialog('close');
                        }

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se pudo guardar el tipo de actor!!", '250', '150');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }, 500);
    } else {
        mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');
    }
}


function CambiarEstadoTipoActor(rowid) {
    var grid_tabla = jQuery("#tabla_tipo_actores");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        data: {
            opcion: 32,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridTipoActores();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado del tipo de actor!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarActores() {
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    if ($("#gview_tabla_actores").length) {
        refrescarGridActores();
    } else {
        jQuery("#tabla_actores").jqGrid({
            caption: 'Actores',
            url: url,
            datatype: 'json',
            height: 250,
            width: 750,
            colNames: ['Id', 'IdTipoActor', 'Tipo Actor', 'Tipo Documento', 'Documento', 'Nombre', 'idCiudad', 'idDpto', 'idPais', 'Direccion', 'Telefono', 'Extension', 'Celular', 'Email', 'Tarjeta Profesional', 'Lugar exp. cedula', 'Estado', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '100px', key: true},
                {name: 'id_tipo_actor', index: 'id_tipo_actor', hidden: true, sortable: true, align: 'center', width: '100px'},
                {name: 'tipo_actor', index: 'tipo_actor', sortable: true, align: 'left', width: '150px'},
                {name: 'tipo_documento', index: 'tipo_documento', sortable: true, align: 'center', width: '100px'},
                {name: 'documento', index: 'documento', sortable: true, align: 'center', width: '120px'},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '280px'},
                {name: 'codciu', index: 'codciu', hidden: true, sortable: true, align: 'center', width: '100px'},
                {name: 'coddpto', index: 'coddpto', hidden: true, sortable: true, align: 'center', width: '100px'},
                {name: 'codpais', index: 'codpais', hidden: true, sortable: true, align: 'center', width: '100px'},
                {name: 'direccion', index: 'direccion', hidden: true, sortable: true, align: 'center', width: '100px'},
                {name: 'telefono', index: 'telefono', hidden: true, sortable: true, align: 'center', width: '100px'},
                {name: 'tel_extension', index: 'tel_extension', hidden: true, sortable: true, align: 'center', width: '100px'},
                {name: 'celular', index: 'celular', hidden: true, sortable: true, align: 'center', width: '100px'},
                {name: 'email', index: 'email', hidden: true, sortable: true, align: 'center', width: '170px'},
                {name: 'tarjeta_profesional', index: 'tarjeta_profesional', hidden: true, sortable: true, align: 'center', width: '100px'},
                {name: 'doc_lugar_exped', index: 'doc_lugar_exped', hidden: true, sortable: true, align: 'center', width: '100px'},
                {name: 'reg_status', index: 'reg_status', hidden: true, sortable: true, align: 'center', width: '90px'},
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            pager: '#page_tabla_actores',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 35,
                    mostrarTodos: 'S'
                }
            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_actores").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_actores").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoActor('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_actores").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_actores"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;

                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                    editarActor(id);
                }

            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_actores", {search: false, refresh: false, edit: false, add: false, del: false});
        jQuery("#tabla_actores").jqGrid("navButtonAdd", "#page_tabla_actores", {
            caption: "Nuevo",
            title: "Agregar nuevo Actor",
            onClickButton: function () {
                crearActores();
            }
        });
    }
}


function refrescarGridActores() {
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    jQuery("#tabla_actores").setGridParam({
        url: url,
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 35,
                mostrarTodos: 'S'
            }
        }
    });

    jQuery('#tabla_actores').trigger("reloadGrid");
}

function crearActores() {
    $('#div_actores').fadeIn('slow');
    AbrirDivCrearActores();
}

function AbrirDivCrearActores() {
    $('#idTipoActor').val('');
    $('#idActor').val('');
    $('#tipo').val('');
    $('#tipo_doc').val('');
    $('#doc_lugar_exp').val('');
    $('#documento').val('');
    $('#nombre').val('');
    $('#direccion').val('');
    $('#email').val('');
    $('#celular').val('');
    $('#telefono').val('');
    $('#ext').val('');
    $('#pais').val('CO');
    cargarDepartamentos('CO', "departamento");
    $('#departamento').val('ATL');
    cargarCiudad('ATL', "ciudad");
    $('#ciudad').val('BQ');
    $('#tarjeta_prof').val('');
    $('#div_actores').fadeIn('slow');
    $("#div_actores").dialog({
        width: 'auto',
        height: 330,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR ACTOR',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarActor();
            },
            "Salir": function () {
                setDireccion(0);
                $(this).dialog("destroy");
            }
        }
    });
}

function editarActor(cl) {

    $('#div_actores').fadeIn("slow");
    var fila = jQuery("#tabla_actores").getRowData(cl);
    var tipoActor = fila['id_tipo_actor'];
    var tipoDoc = fila['tipo_documento'];
    var documento = fila['documento'];
    var nombre = fila['nombre'];
    var pais = fila['codpais'];
    var dpto = fila['coddpto'];
    var ciudad = fila['codciu'];
    var direccion = fila['direccion'];
    var email = fila['email'];
    var celular = fila['celular'];
    var telefono = fila['telefono'];
    var extension = fila['tel_extension'];
    var lugarExp = fila['doc_lugar_exped'];
    var tarj_prof = fila['tarjeta_profesional'];

    $('#idActor').val(cl);
    $('#tipo').val(tipoActor);
    $('#tipo_doc').val(tipoDoc);
    $('#doc_lugar_exp').val(lugarExp);
    $('#documento').val(documento);
    $('#nombre').val(nombre);
    $('#direccion').val(direccion);
    $('#email').val(email);
    $('#celular').val(celular);
    $('#telefono').val(telefono);
    $('#ext').val(extension);
    $('#pais').val(pais);
    cargarDepartamentos(pais, "departamento");
    $('#departamento').val(dpto);
    cargarCiudad(dpto, "ciudad");
    $('#ciudad').val(ciudad);
    $('#tarjeta_prof').val(tarj_prof);
    AbrirDivEditarActor();
}

function AbrirDivEditarActor() {
    $("#div_actores").dialog({
        width: 'auto',
        height: 330,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR ACTOR',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarActor();
            },
            "Salir": function () {
                setDireccion(0);
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarActor() {
    var tipoActor = $('#tipo').val();
    var tipoDoc = $('#tipo_doc').val();
    var documento = $('#documento').val();
    var lugarExp = $('#doc_lugar_exp').val();
    var nombre = $('#nombre').val();
    var direccion = $('#direccion').val();
    var email = $('#email').val();
    var celular = $('#celular').val();
    var telefono = $('#telefono').val();
    var extension = $('#ext').val();
    var pais = $('#pais').val();
    var departamento = $('#departamento').val();
    var ciudad = $('#ciudad').val();
    var tarj_prof = $('#tarjeta_prof').val();
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    if (tipoActor !== '' && tipoDoc !== '' && documento !== '' && nombre !== '' && ciudad !== '' && (telefono !== '' || celular !== '')) {
        if (telefono !== '' && !validarTelefono(telefono))
        {
            mensajesDelSistema("Por favor, ingrese un numero de telefono valido", '250', '150');
        } else if (!validarEmail(email)) {
            mensajesDelSistema("El email ingresado es incorrecto. Por favor, Verifique", '250', '150');
        } else {
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: ($('#idActor').val() === '') ? 36 : 37,
                    id: $('#idActor').val(),
                    tipo_actor: tipoActor,
                    tipo_doc: tipoDoc,
                    documento: documento,
                    nombre: nombre,
                    lugar_expedicion: lugarExp,
                    direccion: direccion,
                    email: email,
                    celular: celular,
                    telefono: telefono,
                    ext: extension,
                    pais: pais,
                    departamento: departamento,
                    ciudad: ciudad,
                    tarjeta_prof: tarj_prof
                },
                success: function (json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }

                        if (json.respuesta === "OK") {
                            refrescarGridActores();
                            mensajesDelSistema("Se guardaron los cambios satisfactoriamente", '250', '150', true);
                            $("#div_actores").dialog('close');

                        }

                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo guardar el actor!!", '250', '150');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }
    } else {
        mensajesDelSistema("Hay campos obligatorios que faltan por llenarse", '250', '150');
    }
}

function CambiarEstadoActor(rowid) {
    var grid_tabla = jQuery("#tabla_actores");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        data: {
            opcion: 38,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridActores();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado del actor!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCboTipoActores() {

    $.ajax({
        type: 'POST',
        async: false,
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        dataType: 'json',
        data: {
            opcion: 33
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#tipo').append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#tipo').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTiposIdentificacion() {

    $.ajax({
        type: 'POST',
        async: false,
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        dataType: 'json',
        data: {
            opcion: 34
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#tipo_doc').append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#tipo_doc').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarPaises() {

    $.ajax({
        type: 'POST',
        async: false,
        url: "./controller?estado=Archivo&accion=Asobancaria",
        dataType: 'json',
        data: {
            opcion: 12
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#pais').append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#pais').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarDepartamentos(codigo, combo) {
    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            type: 'POST',
            async: false,
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 13,
                cod_pais: codigo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function cargarVias(codciu, combo) {

    $('#' + combo).empty();
    $.ajax({
        async: false,
        type: 'POST',
        url: "./controller?estado=GestionSolicitud&accion=Aval",
        dataType: 'json',
        data: {
            opcion: 'cargarvias',
            ciu: codciu
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    alert(json.error, '250', '180');
                    return;
                }
                try {
                    $('#' + combo).empty();
                    $('#' + combo).append("<option value=''></option>");

                    for (var key in json) {
                        $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    alert('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function Posicionar_div(id_objeto, e) {
    obj = document.getElementById(id_objeto);
    var posx = 0;
    var posy = 0;
    if (!e)
        var e = window.event;
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    }
    else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    else
        alert('ninguna de las anteriores');

    obj.style.left = posx + 'px';
    obj.style.top = posy + 'px';
    obj.style.zIndex = 1900;

}

function genDireccion(elemento, e) {

    var contenedor = document.getElementById("direccion_dialogo")
            , res = document.getElementById("dir_resul");

    /* $("#direccion_dialogo").draggable({ handle: "#drag_direcciones"});
     Posicionar_div("direccion_dialogo",e); */
    contenedor.style.left = 750 + 'px';
    contenedor.style.display = "block";

    $("#div_actores").css({
        'width': '1150px'
    });
    res.name = elemento;
    res.value = (elemento.value) ? elemento.value : '';

}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3:
            var res = document.getElementById('dir_resul')
                    , des = document.getElementById(res.name);
            des.value = res.value;
        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");

            document.getElementById("direccion_dialogo").style.display = "none";
            $("#div_actores").css({
                'width': 'auto'
            });
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
                    , g = document.getElementById('via_genera_dir')
                    , opcion;
            for (var i = 0; i < g.length; i++) {

                opcion = g[i];

                if (opcion.value === p) {

                    opcion.style.display = 'none';
                    opcion.disabled = true;

                } else {

                    opcion.disabled = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if (!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                    || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                    || !jQuery('#placa_dir').val()) {
                jQuery('#dir_resul').val("");
                jQuery('#dir_resul').attr("class", "validation-failed");
            } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                        jQuery('#via_princip_dir option:selected').text()
                        + ' ' + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                        + ' ' + jQuery('#via_genera_dir option:selected').text().trim()
                        + ' ' + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                        + ((jQuery('#via_genera_dir option:selected').text().toUpperCase() === '#' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'TRANSVERSAL') ? '-' : ' ')
                        + jQuery('#placa_dir').val().trim().toUpperCase()
                        + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
            }
            break;
    }
}

function validarTelefono(phone) {
    var filter = /^[0-9-()+]+$/;
    return filter.test(phone);
}

function validarEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test(email);
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}


function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}


function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}



