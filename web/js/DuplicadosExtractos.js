/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
   // buscarlineaNegocio();
    $("#generar").click(function () {
        exportarpdf();
    });

    $("#buscar").click(function () {     
        cargarNEgociosCliente();
    });
});


function exportarpdf(pdf, id) {
    var idRop;
    if (pdf === 'bd') {
        idRop = id;
    } else {
        idRop = $("#codigo").val();
    }

    $.ajax({
        datatype: 'json',
        url: "./controller?estado=Reestructurar&accion=Fenalco",
        data: {
            opcion: 22,
            idRop: idRop
        },
        success: function (json) {
            if (json.respuesta === "OK") {
                mensajesDelSistema("Exito en la descarga", '200', '150', true);
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
   // $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargarNEgociosCliente() {
    //linea_neg
    var grid_tabla = jQuery("#tabla_negocios");
    if ($("#gview_tabla_negocios").length) {
        reloadGridMostrartabla(grid_tabla, 1);
    } else {
        grid_tabla.jqGrid({
            caption: "Negocios",
            url: "./controller?estado=extracto&accion=Duplicado",
            mtype: "POST",
            datatype: "json",
            height: '100',
            width: 'auto',
            colNames: ['Cedula', 'Nombre', 'Negocio','Linea negocio','Unidad negocio', 'Direccion', 'Ciudad', 'Departamento','Saldo catera'],
            colModel: [
                {name: 'cedula', index: 'cedula', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'nombre', index: 'nombre', width: 270, sortable: true, align: 'left', hidden: false},
                {name: 'negocio', index: 'negocio', width: 130, sortable: true, align: 'center', hidden: false, key: true},
                {name: 'linea_negocio', index: 'linea_negocio', width: 130, sortable: true, align: 'center', hidden: false},
                {name: 'unidad_negocio', index: 'unidad_negocio', width: 130, sortable: true, align: 'center', hidden: false},
                {name: 'direccion', index: 'direccion', width: 130, sortable: true, align: 'center', hidden: false},
                {name: 'ciudad', index: 'ciudad', width: 130, sortable: true, align: 'center', hidden: false},
                {name: 'departamento', index: 'departamento', width: 130, sortable: true, align: 'center', hidden: false},
                {name: 'valor_saldo', index: 'valor_saldo', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                data: {
                    opcion: 1,
                    cedula: $("#cedula").val()                   
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {});
    
        $("#tabla_negocios").navButtonAdd('#pager', {
            caption: "Crear Extracto",
            title: "Crear Extracto",
            onClickButton: function (e) {
                var selectRows = jQuery("#tabla_negocios").jqGrid('getGridParam', 'selarrrow');
                if (selectRows.length > 0) {
                    $("#ixmora").val("0");
                    $("#gacobranza").val("0");
                    $("#totalExtracto").val("0");
                    
                    $("#valorsaldo").val("0");
                    $("#valorMora").val("0");
                    $("#valorGac").val("0");
                    
                    valors_saldo=0;
                    totals = 0;
                    totalsIxmora = 0;
                    totalsGac = 0;
                    ventanaDetalleNegocio();
                } else {
                    mensajesDelSistema("Debe selecionar al menos un negocio de la lista", '250', '150', true);
                }
            }
         });
    }
}

function reloadGridMostrartabla(grid_tabla, opcion) {//,linea_neg
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=extracto&accion=Duplicado",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                cedula: $("#cedula").val()
               // cartera_en:linea_neg
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function ventanaDetalleNegocio(negocio) {
    cargarDetalleNEgocio();
    $("#dialogMsjdetalle").dialog({
        width: 'auto',
        height: 'auto',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'DETALLE DEL NEGOCIO',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Generar extracto": function () {
             documentosExt();
            },
            "Salir": function () {
                $(this).dialog("close");
                $("#unidad_negocio").val("");
                jQuery("#tabla_detalle_negocio").jqGrid("clearGridData", true);
            }
        }
    });
}

function cargarDetalleNEgocio() {
    
    //empanada para buscar todos los negoios.
    var selectRows = jQuery("#tabla_negocios").jqGrid('getGridParam', 'selarrrow');
    var negocio = "(";
    for (var i = 0; i < selectRows.length; i++) {
        var unidad_negocio = jQuery('#tabla_negocios').getCell(selectRows[i], 'unidad_negocio');
      
        if (i === selectRows.length-1) {
            negocio += "'"+selectRows[i]+ "')";
        }else{
            negocio += "'"+selectRows[i] + "',";
            
        }
    }
    
    //alert("negocio:" + negocio);

    $("#unidad_negocio").val(unidad_negocio);
    var grid_tabla = jQuery("#tabla_detalle_negocio");
    if ($("#gview_tabla_detalle_negocio").length) {
        reloadGridMostrartablaDetalle(grid_tabla, 2, negocio);
    } else {
        grid_tabla.jqGrid({
            // caption: "Negocios",
            url: "./controller?estado=extracto&accion=Duplicado",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: 'auto',
            colNames: ['Documento', 'Negocio', 'Cuota', 'Fecha vencimiento', 'Dias mora', 'Estado'/*,'valor_factura','valor abono',*/, 'Valor saldo',
                'Saldo Capital', 'Saldo Interes','Interes x mora', 'GaC', 'Total'],
            colModel: [
                {name: 'documento', index: 'documento', width: 100, sortable: true, align: 'center', hidden: false, key: true},
                {name: 'negocio', index: 'negocio', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'cuota', index: 'cuota', width: 50, sortable: true, align: 'left', hidden: false},
                {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'dias_mora', index: 'dias_mora', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'estado_obligacion', index: 'estado_obligacion', width: 130, sortable: true, align: 'center', hidden: false},
               /* {name: 'valor_factura', index: 'valor_factura', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_abono', index: 'valor_abono', sortable: true, width: 95, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},*/
                {name: 'valor_saldo', index: 'valor_saldo', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                
                {name: 'valor_saldo_capital', index: 'valor_saldo_capital', sortable: false, width: 95, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                
                {name: 'valor_saldo_interes', index: 'valor_saldo_interes', sortable: false, width: 95, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                
                {name: 'IxM', index: 'IxM', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'GaC', index: 'GaC', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'suma_saldos', index: 'suma_saldos', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false, //'35116286'
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager1',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            userDataOnFooter: true,
            grouping: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
               // type: "post",
                data: {
                    opcion: 2,
                    negocio: negocio,
                    accionf: 'Visualizar'
                }
            }, groupingView: {
                groupField: ['negocio'],
                groupSummary: [false],
                groupColumnShow: [true],
                groupText: ['<b>{0}</b>'],
                groupCollapse: false,
                groupOrder: ['asc'],
                groupDataSorted: true

            }, onSelectAll: function (rowIds, allChecked) {
               // $("input.groupHeader").attr('checked', allChecked);
                todasfilasSeleccionadas(rowIds);
            },
            onSelectRow: function (documento) {
                filasSeleccionadas(documento);
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });

    }
}

function reloadGridMostrartablaDetalle(grid_tabla, opcion, negocio) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=extracto&accion=Duplicado",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                negocio: negocio,
                accionf: 'Visualizar'
            }
        }

    });

    grid_tabla.jqGrid('groupingGroupBy', ['negocio'], {
        groupColumnShow: [true],
        groupText: ['<b>{0}</b>'],
        groupCollapse: false,
        groupOrder: ['asc'],
        groupDataSorted: true
    });
    
    valors_saldo=0;
    totals = 0;
    totalsIxmora = 0;
    totalsGac = 0;
    grid_tabla.trigger("reloadGrid");
}

function aplicarGenerar(negocio) {
    $.ajax({
        datatype: 'json',
        url: "./controller?estado=extracto&accion=Duplicado",
        data: {
            opcion: 3,
            negocio: negocio,
            accionf: 'Generar'
        },
        success: function (json) {
            if (json.respuesta === "OK") {
                mensajesDelSistema("Exito en la generacion", '200', '150', true);
                buscaridRop(negocio);
            }
        }
    });
}

function buscaridRop(negocio) {
    $.ajax({
        datatype: 'json',
        url: "./controller?estado=extracto&accion=Duplicado",
        data: {
            opcion: 4,
            negocio: negocio
        },
        success: function (json) {
            var id = json.rows;
            var pdf = 'bd';
            exportarpdf(pdf, id);
        }
    });
}

var valors_saldo=0;
var totals = 0;
var totalsIxmora = 0;
var totalsGac = 0;
function  filasSeleccionadas(id) {
    
    var jqgcell = jQuery('#tabla_detalle_negocio').getCell(id, 'documento');
    var suma_valor_saldo = jQuery('#tabla_detalle_negocio').getCell(id, 'valor_saldo');
    var suma_saldos = jQuery('#tabla_detalle_negocio').getCell(id, 'suma_saldos');
    var sumaIxmorra= jQuery('#tabla_detalle_negocio').getCell(id, 'IxM');
    var sumaGac= jQuery('#tabla_detalle_negocio').getCell(id, 'GaC');
    var cbIsChecked = (jQuery("#jqg_tabla_detalle_negocio_" + jqgcell).attr('checked'));
    if (cbIsChecked === true) {
        if (suma_saldos !== null) {
            totals = parseInt(totals) + parseInt(suma_saldos);
            totalsIxmora= parseInt(totalsIxmora) + parseInt(sumaIxmorra);
            totalsGac= parseInt(totalsGac) + parseInt(sumaGac);
            valors_saldo=parseInt(valors_saldo) + parseInt(suma_valor_saldo);
           
        }
    } else {
        if (suma_saldos !== null) {
            totals = parseInt(totals) - parseInt(suma_saldos);
            totalsIxmora= parseInt(totalsIxmora) - parseInt(sumaIxmorra);
            totalsGac= parseInt(totalsGac) - parseInt(sumaGac);
            valors_saldo=parseInt(valors_saldo) - parseInt(suma_valor_saldo);
        }
    }
    
   // granTotal = totals + totalsIxmora + totalsGac;
    $("#totalExtracto").val(numberConComas(totals));
    $("#valorMora").val(numberConComas(totalsIxmora));
    $("#valorGac").val(numberConComas(totalsGac));
    $("#valorsaldo").val(numberConComas(valors_saldo));
   
    aplicarDescuentos(totals,totalsIxmora,totalsGac) ;
    jQuery('#tabla_detalle_negocio').jqGrid('footerData', 'set', {
        estado_obligacion: 'Total :', 
        valor_saldo:valors_saldo ,
        suma_saldos: totals,
        IxM:totalsIxmora,
        GaC:totalsGac
    
    });
}

function   todasfilasSeleccionadas(id) {
    var valor_saldo=0;
    var total = 0;
    var totalIxmora=0;
    var totoalGaC=0;
    
    var filasId = jQuery("#tabla_detalle_negocio").jqGrid('getGridParam', 'selarrrow');
    if (filasId.length > 0) {

        for (var i = 0; i < filasId.length; i++) {
            var jqgcell = jQuery('#tabla_detalle_negocio').getCell(filasId[i], 'documento');
            var suma_valor_saldo = jQuery('#tabla_detalle_negocio').getCell(filasId[i], 'valor_saldo');
            var suma_saldos = jQuery('#tabla_detalle_negocio').getCell(filasId[i], 'suma_saldos');
            var sumaIxmorra = jQuery('#tabla_detalle_negocio').getCell(filasId[i], 'IxM');
            var sumaGac = jQuery('#tabla_detalle_negocio').getCell(filasId[i], 'GaC');
            
               // alert(valor_saldo);
            var cbIsChecked = (jQuery("#jqg_tabla_detalle_negocio_" + jqgcell).attr('checked'));
            if (cbIsChecked === true) {
                if (suma_saldos !== null) {
                    total = parseInt(total) + parseInt(suma_saldos);
                    totalIxmora= parseInt(totalIxmora) + parseInt(sumaIxmorra);
                    totoalGaC= parseInt(totoalGaC) + parseInt(sumaGac);
                    valor_saldo=parseInt(valor_saldo) + parseInt(suma_valor_saldo);
                }
            } else {
                if (suma_saldos !== null) {
                    total = parseInt(total) - parseInt(suma_saldos);
                    totalIxmora= parseInt(totalIxmora) - parseInt(sumaIxmorra);
                    totoalGaC= parseInt(totoalGaC) - parseInt(sumaGac);
                    valor_saldo=parseInt(valor_saldo) - parseInt(suma_valor_saldo);
                }
            }
        }
    } else {
        var total = 0;
    }
    
    valors_saldo=valor_saldo;
    totals=total;
    totalsIxmora=totalIxmora;
    totalsGac=totoalGaC;
    
   // granTotal = totals + totalsIxmora + totalsGac;
    $("#totalExtracto").val(numberConComas(totals));
    $("#valorMora").val(numberConComas(totalIxmora));
    $("#valorGac").val(numberConComas(totoalGaC));
    $("#valorsaldo").val(numberConComas(valors_saldo));
    aplicarDescuentos(totals,totalsIxmora,totalsGac) ;
    

    jQuery('#tabla_detalle_negocio').jqGrid('footerData', 'set', {
        estado_obligacion: 'Total :', 
        valor_saldo:valor_saldo ,
        suma_saldos: total,
        IxM:totalIxmora,
        GaC:totoalGaC
    
    });
}


function documentosExt() {
   loading("Espere un momento procesando facturas.", "270", "140");
    var i, selRowIds = $("#tabla_detalle_negocio").jqGrid("getGridParam", "selarrrow"), rowData;
    var arrayJson = new Array();
    for (i = 0; i < selRowIds.length; i++) {
        rowData = $("#tabla_detalle_negocio").jqGrid("getLocalRow", selRowIds[i]);
        arrayJson.push(rowData);
    }

    console.log(JSON.stringify(arrayJson));
    $.ajax({
        type: "POST",
        url: "./controller?estado=extracto&accion=Duplicado",
        dataType: 'json',
        async: false,
        data: {
            opcion: 5,
            listaJson: JSON.stringify(arrayJson),
            ixmora: $("#ixmora").val().replace(/\,/g, ''),
            gacobranza: $("#gacobranza").val().replace(/\,/g, ''),
            valorsaldo: $("#valorsaldo").val().replace(/\,/g, ''),
            valormora: $("#valorMora").val().replace(/\,/g, ''),
            valorgac: $("#valorGac").val().replace(/\,/g, ''),
            unidad_negocio: $("#unidad_negocio").val(),
            totalExtracto: $("#totalExtracto").val().replace(/\,/g, '')

        }, success: function (data) {
            //alert(data.respuesta);
            $("#dialogo2").dialog('close');
            if (data.respuesta === 'OK') {
                mensajesDelSistema('Se ha generado un nuevo extracto de pago revise su log de archivos ', '290', 'auto', true);              
            } else if (data.respuesta === 'ERROR') {
                mensajesDelSistema('Lo sentimos algo ha salido mal, comuniquese al area de soporte', '300', 'auto', false);
            }
        }
    });
}

function soloNumeros(id) {
    var precio = document.getElementById(id).value;
    precio = precio.replace(/[^0-9^.]+/gi, "");
    document.getElementById(id).value = precio;
}


function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function aplicarDescuentosEnterMora(id, e) {
    if (e.keyCode === 13) {
        var valortotal = 0;
        var valorMora = totalsIxmora;//parseInt($("#valorMora").val().replace(/\,/g, ''));
        if ($("#totalExtracto").val() !== '') {
            valortotal =totals;//parseInt($("#totalExtracto").val().replace(/\,/g, ''));
        }
        
        var porcentaje = parseInt($("#" + id).val().replace(/\,/g, ''));
        // alert(result+"ddd "+ (result / 100)+ " valortotal"+valortotal);
        var descuento = Math.round(valorMora * (porcentaje / 100));
       // alert(descuento);
        $("#valorMora").val(numberConComas(valorMora - descuento));
        
        var totalSuma = parseInt($("#valorsaldo").val().replace(/\,/g, '')) +parseInt($("#valorGac").val().replace(/\,/g, '')) +parseInt($("#valorMora").val().replace(/\,/g, ''));
       // $("#totalExtracto").val(numberConComas(valortotal - descuento));
        $("#totalExtracto").val(numberConComas(totalSuma));
     //   totals = valortotal - descuento;
        e.preventDefault();
    }

}

function aplicarDescuentosEnterGac(id, e) {
    if (e.keyCode === 13) {
        var valortotal = 0;
       // alert(totals);
        var valorGac =totalsGac; //parseInt($("#valorGac").val().replace(/\,/g, ''));
        if ($("#totalExtracto").val() !== '') {
            valortotal =totals;//arseInt($("#totalExtracto").val().replace(/\,/g, ''));
        }
        
        var porcentaje = parseInt($("#" + id).val().replace(/\,/g, ''));
        // alert(result+"ddd "+ (result / 100)+ " valortotal"+valortotal);
        var descuento = Math.round(valorGac * (porcentaje / 100));

        $("#valorGac").val(numberConComas(valorGac - descuento));
        
        var totalSuma = parseInt($("#valorsaldo").val().replace(/\,/g, '')) +parseInt($("#valorGac").val().replace(/\,/g, '')) +parseInt($("#valorMora").val().replace(/\,/g, ''));
        //$("#totalExtracto").val(numberConComas(valortotal - descuento));
        $("#totalExtracto").val(numberConComas(totalSuma));
        
      //  totals = valortotal - descuento;
        e.preventDefault();
    }

}



function aplicarDescuentos(valor_total, valor_mora, valor_gac) {
    
    var ixmora = parseInt($("#ixmora").val().replace(/\,/g, ''));
    var gac = parseInt($("#gacobranza").val().replace(/\,/g, ''));
    
   // alert("gac:"+$("#gacobranza").val());
  
    var Totaldescuentos=0;
    var descuentoMora = 0;
    var descuentoGac = 0;
    var porcentaje=0;
    

    if( $("#ixmora").val() !== ''){
        porcentaje = ixmora ;
        descuentoMora = Math.round(valor_mora * (porcentaje / 100));          
    }
    if($("#gacobranza").val() !== ''){        
        porcentaje = gac ;
        descuentoGac = Math.round(valor_gac * (porcentaje / 100));        
    }
     Totaldescuentos=descuentoMora+descuentoGac;
    // alert("valor_total :"+valor_total+" Totaldescuentos:"+Totaldescuentos);
     valor_total=valor_total-Totaldescuentos;
     $("#totalExtracto").val(numberConComas(valor_total));
     $("#valorMora").val(numberConComas(valor_mora-descuentoMora));
     $("#valorGac").val(numberConComas(valor_gac-descuentoGac));
}

function buscarlineaNegocio(){
    $.ajax({
        type: 'POST',
        url:"./controller?estado=extracto&accion=Duplicado",
        dataType: 'json',
        data: {
            opcion: 6
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                try {
                    $('#cartera_en').empty();
//                    $('#transportadora').append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#cartera_en').append('<option value=' + json[key].codigo + '>' + json[key].nombre + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                  mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();
}
