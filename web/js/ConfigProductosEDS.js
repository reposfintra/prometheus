/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function init() {
    cargarCombo('propietarios', []);
    initTabla();
    $("#buscar").click(function () {
        cargarProductos();
    });
}

function cargarCombo(id, filtro) {
    var elemento = $('#' + id)
            , sql = (id === 'propietarios') ? 'PROPIETARIOS_ESTACIONES' : 'EDSxPROVEEDOR';
    $("#lui_tabla_productos,#load_tabla_productos").show();
    $.ajax({
        url: '/fintra/controller?estado=Config&accion=ProductosEDS',
        datatype: 'json',
        type: 'get',
        data: {opcion: 2, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: true,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
                $("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            $("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });

}
function initTabla() {
    var tabla = jQuery("#tabla_productos");
    tabla.jqGrid({
        datatype: 'local',
        width: 1670,
        height: 500,
        rowNum: 1000,
        pager: $('#page_productos'),
        pgtext: null,
        pgbuttons: false,
        viewrecords: false,
        gridview: true,
        hidegrid: true,
        loadonce: true,
        rownumbers: true,
        cellEdit: false,
        userDataOnFooter: true,
        cellsubmit: 'clientArray',
        editurl: 'clientArray',
        colNames: ['Id', 'Control', 'Activo / Inactivo', 'id_producto', 'Producto', 'Inmodificable', 'Precio Actual', 'Aplica Comision',
            'Descripcion Descuento', 'Porcentaje Descuento', 'Valor Comision', 'Editar Precio',
            'Precio en Session', 'Periodo', 'Configuracion', 'Historico'],
        colModel: [
            {name: 'id', index: 'id', hidden: true, key: true},
            {name: 'control', index: 'control', hidden: true},
            {name: 'estado', index: 'estado', sortable: true, editable: true,
                formatter: 'checkbox', editoptions: {value: 'true:false', defaultValue: 'true',
                    dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                try {
                                    if (!e.target.checked) {
                                        var rowid = e.target.id.replace("_estado", "")
                                                , grid = jQuery("#tabla_productos")
                                                , data = grid.jqGrid('getLocalRow', rowid);
                                        if (data.control === '2') {
                                            //alert('Este producto tiene al menos una venta, no se puede anular');
                                            //jQuery("#tabla_productos").jqGrid('setCell', rowid, 'estado', true);
                                        }
                                    }
                                } catch (exc) {
                                }
                                return;
                            }}]}, align: 'center',
                stype: 'select', edittype: 'checkbox', width: '30px'},
            {name: 'id_producto', index: 'id_producto', hidden: true, editrules: {required: true}},
            {name: 'producto', index: 'producto', sortable: true, editable: true, width: '80px',
                edittype: 'select', editrules: {required: true},
                editoptions: {
                    value: ajaxSelect,
                    dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                try {
                                    var rowid = e.target.id.replace("_producto", "");
                                    jQuery("#tabla_productos").jqGrid('setCell', rowid, 'id_producto', e.target.value);
                                } catch (exc) {
                                }
                                return;
                            }}]
                }},
            {name: 'inmodificable', index: 'inmodificable', sortable: true, hidden: true},
            {name: 'precio_producto', index: 'precio_producto', sortable: true, width: '70px', formatter: 'number',
                editable: true, align: 'right'},
            {name: 'comision_aplica', index: 'comision_aplica', sortable: true, editable: true,
                formatter: 'checkbox', align: 'center', edittype: 'checkbox', width: '30px',
                editoptions: {value: 'true:false', defaultValue: 'true',
                    dataEvents: [{type: 'change', fn: function (e) {
                                try {
                                    var rowid = e.target.id.replace("_comision_aplica", "");
                                    if (e.target.checked) {
                                        $("#" + rowid + '_comision_descripcion').removeAttr('readonly');
                                        $("#" + rowid + '_comision_porcentaje').removeAttr('readonly');
                                        $("#" + rowid + '_comision_valor').removeAttr('readonly');
                                    } else {
                                        $("#" + rowid + '_comision_descripcion').attr('readonly', true);
                                        $("#" + rowid + '_comision_porcentaje').val('0.00');
                                        $("#" + rowid + '_comision_porcentaje').attr('readonly', true);
                                        $("#" + rowid + '_comision_valor').val('0.00');
                                        $("#" + rowid + '_comision_valor').attr('readonly', true);
                                    }
                                } catch (exc) {
                                }
                                return;
                            }}]}},
            {name: 'comision_descripcion', index: 'comision_descripcion',
                editrules: {required: true}, editable: true, sortable: true, width: '300px'},
            {name: 'comision_porcentaje', index: 'comision_porcentaje', sortable: true,
                editable: true, formatter: 'number', align: 'right', width: '70px',
                editoptions: {dataEvents: [{type: 'change', fn: function (e) {
                                try {
                                    var rowid = e.target.id.replace("_comision_porcentaje", "");
                                    if (parseInt(e.target.value) === 0) {
                                        $("#" + rowid + '_comision_valor').removeAttr('readonly');
                                    } else {
                                        $("#" + rowid + '_comision_valor').val('0.00');
                                        $("#" + rowid + '_comision_valor').attr('readonly', true);
                                    }
                                } catch (exc) {
                                    jQuery("#tabla_productos").jqGrid('setCell', rowid, 'comision_porcentaje', '0.00');
                                }
                                return;
                            }}]}},
            {name: 'comision_valor', index: 'comision_valor', sortable: true,
                editable: true, formatter: 'number', align: 'right', width: '70px',
                editoptions: {dataEvents: [{type: 'change', fn: function (e) {
                                try {
                                    var rowid = e.target.id.replace("_comision_valor", "");
                                    if (parseInt(e.target.value) === 0) {
                                        $("#" + rowid + '_comision_porcentaje').removeAttr('readonly');
                                    } else {
                                        $("#" + rowid + '_comision_porcentaje').val('0.00');
                                        $("#" + rowid + '_comision_porcentaje').attr('readonly', true);
                                    }
                                } catch (exc) {
                                    jQuery("#tabla_productos").jqGrid('setCell', rowid, 'comision_valor', '0.00');
                                }
                                return;
                            }}]}},
            {name: 'editar_precio', index: 'editar_precio', sortable: true, editable: true,
                formatter: 'checkbox', editoptions: {value: 'true:false', defaultValue: 'true'}, align: 'center',
                stype: 'select', edittype: 'checkbox', width: '30px'},
            {name: 'precio_ensession', index: 'precio_ensession', sortable: true, editable: true,
                formatter: 'checkbox', editoptions: {value: 'true:false', defaultValue: 'true'}, align: 'center',
                stype: 'select', edittype: 'checkbox', width: '30px'},
            {name: 'periodo', index: 'periodo', hidden: true, sortable: true, width: '30px'},
            {name: 'configuracion', index: 'configuracion', hidden: false, sortable: true, width: '35px', align: 'center'},
            {name: 'historico', index: 'historico', hidden: false, sortable: true, width: '35px', align: 'center'}
        ],
        loadComplete: function (rowid, e, iRow, iCol) {
            $("#tabla_productos").contextMenu('menu', {
                bindings: {
                    eliminar: function (rowid) {
                        var grid = jQuery("#tabla_productos"), selRowIds = grid.jqGrid("getGridParam", "selrow");
                        //  alert(selRowIds);
                        $('#tabla_productos').jqGrid('delRowData', selRowIds);
                    }
                }, onContexMenu: function (event/*, menu*/) {
                }
            });
        }, gridComplete: function (id) {
            var cant = jQuery("#tabla_productos").jqGrid('getDataIDs');
            for (var i = 0; i < cant.length; i++) {
                var cl = cant[i];
                var producto = jQuery("#tabla_productos").getRowData(cl).producto;
                if (producto === 'COMBUSTIBLE ACPM') {
                    icon = '<img src = "/fintra/images/botones/iconos/verHistorico.png"style = "margin-left: -4px; height: 23px; vertical-align: middle;"onclick = "mostrarHistorico(' + cl + ')">';
                    be = "<input style='height:20px;width:50px;' type='button' value='Rangos' onclick=\"mostrar('" + cl + "');\" />";
                    jQuery("#tabla_productos").jqGrid('setRowData', cant[i], {configuracion: be, historico: icon});
                } else {
                    jQuery("#tabla_productos").jqGrid('setRowData', cant[i], {configuracion: '---'});
                }
            }
        },
        ondblClickRow: function (rowid, iRow, iCol, e) {
            var grid = jQuery("#tabla_productos")
                    , data = grid.jqGrid('getLocalRow', rowid);

            switch (data.control) {
                case '2':
                case '0':
                    grid.jqGrid('editRow', rowid, true, function () {
                        $("input, select", e.target).focus();
                    });
                    break;
                    if (data.inmodificable === 'S') {

                    }
                case '1':
                    if (!data.estado) {
                        if (confirm('Desea activar este producto')) {
                            grid.jqGrid('setCell', rowid, 'estado', true);
                        }
                    } else {
                        grid.jqGrid('setCell', rowid, 'estado', false);
                    }
                    break;
                case '3':
                    if (data.estado) {
                        if (confirm('Este producto se encuentra anulado, �desea anular esta configuracion?')) {
                            grid.jqGrid('setCell', rowid, 'estado', false);
                        }
                    }
            }
            return;
        },
        restoreAfterError: true
    });
    tabla.navGrid("#page_productos", {add: false, edit: false, del: false, search: false, refresh: true});
    tabla.navButtonAdd('#page_productos', {
        caption: "Guardar",
        title: "Guardar cambios",
        buttonicon: "ui-icon-save",
        onClickButton: function () {
            guardarProductos();
        },
        position: "first"
    });
    tabla.navButtonAdd('#page_productos', {
        caption: "Nuevo",
        title: "Agregar nueva fila",
        buttonicon: "ui-icon-plus",
        onClickButton: function () {
            var eds = $("#eds").val();
            if (eds === '') {
                mensajesDelSistema('Debe escoger una estacion de servicio', '300', 'auto', false);
            } else {
                var grid = $("#tabla_productos")
                        , rowid = 'neo_' + grid.getRowData().length;
                grid.addRowData(rowid, {estado: true, id_producto: '', comision_aplica: true, periodo: '', control: '0'});
                grid.jqGrid('editRow', rowid, true, function () {
                    $("input, select", e.target).focus();
                }, null, null, {}, null, null, function (id) {
                    var g = $('#tabla_productos')
                            , r = g.jqGrid('getLocalRow', id);
                    if (!r.id) {
                        g.jqGrid('delRowData', id);
                    }
                });
            }
        },
        position: "first"
    });
}
function ajaxSelect() {
    var sql = 'PRODUCTOS_ES';
    var resultado;
    $("#lui_tabla_productos,#load_tabla_productos").show();
    $.ajax({
        url: '/fintra/controller?estado=Config&accion=ProductosEDS',
        datatype: 'json',
        type: 'get',
        data: {opcion: 2, informacion: JSON.stringify({query: sql, filtros: []})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '300', 'auto', false);
                    resultado = {};
                } else {
                    resultado = json;
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                $("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            console.log('error');
            $("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
    return resultado;
}
function cargarProductos() {
    var tabla = jQuery("#tabla_productos")
            , filtro = $('#eds').val();

    tabla.setGridParam({
        url: '/fintra/controller?estado=Config&accion=ProductosEDS'
                + '&opcion=0&informacion=' + JSON.stringify({id: filtro}),
        mtype: 'get',
        datatype: 'json',
        jsonReader: {
            root: 'rows',
            repeatitems: false,
            id: '0'
        },
        loadComplete: function () {
        },
        loadError: function (xhr, status, error) {
            console.log(error);
        }
    });
    tabla.trigger("reloadGrid");
}
function guardarProductos() {
    var eds = $('#eds').val()
            , grid = jQuery("#tabla_productos")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;
    for (var i = 0; i < filas.length; i++) {
        //console.log(filas[i]);
        data = grid.jqGrid("getLocalRow", filas[i]);
//        alert(data.producto);
        if (data.comision_aplica && data.estado) {
            if ((data.producto) === 'COMBUSTIBLE ACPM') {
                if (parseInt(data.comision_porcentaje) !== 0
                        || parseInt(data.comision_valor) !== 0) {
                    error = true;
                    mensajesDelSistema('Valores de comision deben ser 0', '300', 'auto', false);
                    grid.jqGrid('editRow', filas[i], true, function () {
                        $("input, select", e.target).focus();
                    });
                    break;
                }
            } else {
                if ((data.producto) !== 'COMBUSTIBLE ACPM') {
                    if ((data.producto) === 'DINERO EN EFECTIVO') {
                        if (parseInt(data.comision_porcentaje) !== 0
                                || parseInt(data.comision_valor) !== 0) {
                            error = true;
                            mensajesDelSistema('Valores de comision deben ser 0', '300', 'auto', false);
                            grid.jqGrid('editRow', filas[i], true, function () {
                                $("input, select", e.target).focus();
                            });
                            break;
                        }
                    } else {
                        if (parseInt(data.comision_porcentaje) === 0
                                && parseInt(data.comision_valor) === 0) {
                            error = true;
                            mensajesDelSistema('No registro valores de comision', '300', 'auto', false);
                            grid.jqGrid('editRow', filas[i], true, function () {
                                $("input, select", e.target).focus();
                            });
                            break;
                        } else if (parseInt(data.comision_porcentaje) !== 0
                                && parseInt(data.comision_valor) !== 0) {
                            error = true;
                            mensajesDelSistema('solo puede registrar un valor comision o porcentaje descuento', '300', 'auto', false);
                            grid.jqGrid('editRow', filas[i], true, function () {
                                $("input, select", e.target).focus();
                            });
                        }
                    }

                }
            }
        }
        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un producto', '300', 'auto', false);
    }
    if (error)
        return;
    $("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: '/fintra/controller?estado=Config&accion=ProductosEDS',
        datatype: 'json',
        type: 'get',
        data: {opcion: 1, informacion: JSON.stringify({eds: eds, rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    //alert(json.mensaje);
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    cargarProductos();
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                $("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            $("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}
function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function mostrar() {
    listarRangos();
    $("#dialogConfigRangos").dialog({
        width: '550',
        height: '290',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                guardarRangos();
            },
            "Salir": function () {
                $("#tablainterna").html('');
                $(this).dialog("close");
            }
        }
    });
    $("#dialogConfigRangos").siblings('div.ui-dialog-titlebar').remove();
}

function addElemento() {
    var cantidad = document.getElementsByName('rangoini').length;
    var numid = cantidad + 1;
    $("#tablainterna").append(
            '<tr id="fila_' + numid + '">' +
            '<td hidden><input type="text" id="id_' + numid + '" name="id" style="margin-left: 9px;" ></td>' +
            '<td><input type="text" id="rangoini_' + numid + '" name="rangoini" style="margin-left: 9px;"></td>' +
            '<td><label style = "margin-left:-10px;"> A </label></td>' +
            '<td><input type = "text" id = "rangofin_' + numid + '" name = "rangofin" style = "margin-left: 9px;"></td>' +
            '<td><label>$</label></td>' +
            '<td><input type = "text" id = "vlrdes_' + numid + '" name = "vlrdes" style = "margin-left: 9px;"></td>' +
            '<td> </td>' +
            '<td><input type = "text" id = "desc_' + numid + '" name = "desc" style = "margin-left: 9px;"></td>' +
            '<td><img id = "add_' + numid + '" src = "/fintra/images/botones/iconos/adds.png"' +
            'style = "margin-left: 5px; height: 19px; vertical-align: middle;" onclick = "addElemento()"></td>' +
            '<td><img id = "delete_' + numid + '" src = "/fintra/images/botones/iconos/delete.png"' +
            'style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "deleteElemento(' + numid + ')"></td>' +
            '</tr>'
            );
}

function deleteElemento(fila) {
    $("#fila_" + fila).remove();
}

function guardarRangos() {
    var cantidad = document.getElementsByName('rangoini').length;
    var json = [];
    var id_config, rangoini, rangofin, vlrdes, desc, id;
    var grid_tabla = jQuery("#tabla_productos");
    var ids = grid_tabla.jqGrid('getDataIDs');
    var guardar = 'true';
    var id_config_cam = $("#id_config").val();
    for (var i = 0; i < ids.length; i++) {
        var num = ids[i];
        var producto = grid_tabla.getRowData(num).producto;
        if (producto === 'COMBUSTIBLE ACPM') {
            id_config = grid_tabla.getRowData(num).id;
        }
    }

    if (id_config_cam === "") {
        for (var i = 1; i <= cantidad; i++) {
            if (i === 1) {
                rangoini = $("#rangoini_").val();
                rangofin = $("#rangofin_").val();
                vlrdes = $("#vlrdes_").val();
                desc = $("#desc_").val();
                id = $("#id_").val();
            } else {
                rangoini = $("#rangoini_" + i).val();
                rangofin = $("#rangofin_" + i).val();
                vlrdes = $("#vlrdes_" + i).val();
                desc = $("#desc_" + i).val();
                id = $("#id_" + i).val();
            }
            var llave = {};
            if (((parseFloat(vlrdes) === 0) && (parseFloat(desc) === 0)) || ((parseFloat(vlrdes) > 0) && (parseFloat(desc) > 0))) {
                guardar = 'false';
                mensajesDelSistema('Por favor verifique el valor y porcentaje descuento', '333', '100', false);
            } else {
                if ((parseFloat(desc) > 100)) {
                    guardar = 'false';
                    mensajesDelSistema('porcentaje descuento nop debe ser mayor de 100', '333', '100', false);
                } else {
                    if ((vlrdes !== '') || (desc !== '') || (rangofin !== '') || (rangoini !== '')) {
                        llave['id_configproductos'] = id_config;
                        llave['rango_inicial'] = rangoini;
                        llave ['rango_final'] = rangofin;
                        llave ['valor_descuento'] = vlrdes;
                        llave ['porcen_descuento'] = desc;
                        llave['id'] = id;
                        json.push(llave);
                    } else {
                        alert('faltan llenar campos');
                    }
                }


            }
            // alert("rango inicial: " + rangoini);
        }
    } else {
        for (var i = 1; i <= cantidad; i++) {
            rangoini = $("#rangoini_" + i).val();
            rangofin = $("#rangofin_" + i).val();
            vlrdes = $("#vlrdes_" + i).val();
            desc = $("#desc_" + i).val();
            id = $("#id_" + i).val();
            var llave = {};
            if (((parseFloat(vlrdes) < 0) || (parseFloat(desc) < 0)) || ((parseFloat(vlrdes) > 0) && (parseFloat(desc) > 0))) {
                guardar = 'false';
                mensajesDelSistema('Por favor verifique el valor y porcentaje descuento', '333', '55', false);
            } else {
                if ((vlrdes !== '') || (desc !== '') || (rangofin !== '') || (rangoini !== '')) {
                    if ((parseFloat(desc) > 100)) {
                        guardar = 'false';
                        mensajesDelSistema('porcentaje descuento no debe ser mmayor de 100', '333', '100', false);
                    } else {
                        llave['id_configproductos'] = id_config;
                        llave['rango_inicial'] = rangoini;
                        llave ['rango_final'] = rangofin;
                        llave ['valor_descuento'] = vlrdes;
                        llave ['porcen_descuento'] = desc;
                        llave['id'] = id;
                        json.push(llave);
                    }

                } else {
                    alert('faltan llenar campos');
                }
            }
            // alert("rango inicial: " + rangoini);
        }
    }

//    alert (guardar);
//    var info = JSON.stringify(json);
//    alert(info);
    if (guardar === 'true') {
        $.ajax({
            type: "POST",
            url: '/fintra/controller?estado=Config&accion=ProductosEDS',
            dataType: 'json',
            async: false,
            data: {
                opcion: 3,
                info: JSON.stringify({json: json})
            }, success: function (data) {
                //alert(data.respuesta);
                if (data.respuesta === 'Guardado:)') {
                    mensajesDelSistema('Exito al guardar', '200', 'auto', true);
                } else if (data.respuesta === 'ERROR') {
                    mensajesDelSistema('Error, por favo verifique los datos ', '300', '55', false);
                }
            }
        });
    } else {
        mensajesDelSistema('Por favor verifique el valor y porcentaje descuento', '333', '55', false);
    }
}

function listarRangos() {
    var id_config;
    var grid_tabla = jQuery("#tabla_productos");
    var ids = grid_tabla.jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var num = ids[i];
        var producto = grid_tabla.getRowData(num).producto;
        if (producto === 'COMBUSTIBLE ACPM') {
            id_config = grid_tabla.getRowData(num).id;
        }
    }
    $.ajax({
        async: false,
        type: "POST",
        url: '/fintra/controller?estado=Config&accion=ProductosEDS',
        dataType: 'json',
        data: {
            opcion: 4,
            id_config: id_config
        }, success: function (json) {
            var longitud = json.info.length;
            // alert(longitud);
            $("#tablainterna").html('');

            $("#tablainterna").append(
                    '<tr>' +
                    '<td><label>Galones Iniciales</label></td>' +
                    '<td></td>' +
                    '<td><label>Galones Finales</label></td>' +
                    '<td></td>' +
                    '<td><label>Valor Descuento</label></td>' +
                    '<td></td>' +
                    '<td><label style="margin-left: 10px;">% Descuento</label></td>' +
                    '</tr>'
                    );
            if (longitud === 0) {
                $("#id_config").val('');
                $("#tablainterna").append(
                        '<tr id="fila_">' +
                        '<td hidden><input type="text" id="id_" name="id" style="margin-left: 9px;" ></td>' +
                        '<td><input type="text" id="rangoini_" name="rangoini" style="margin-left: 9px;" ></td>' +
                        '<td><label style = "margin-left:-10px;"> A </label></td>' +
                        '<td><input type = "text" id = "rangofin_" name = "rangofin" style = "margin-left: 9px;"></td>' +
                        '<td><label>$</label></td>' +
                        '<td><input type = "text" id = "vlrdes_" name = "vlrdes" style = "margin-left: 9px;"></td>' +
                        '<td> </td>' +
                        '<td><input type = "text" id = "desc_" name = "desc" style = "margin-left: 9px;"></td>' +
                        '<td><img id = "add_" src = "/fintra/images/botones/iconos/adds.png"' +
                        'style = "margin-left: 5px; height: 19px; vertical-align: middle;" onclick = "addElemento()"></td>' +
                        '<td><img id = "delete_" src = "/fintra/images/botones/iconos/delete.png"' +
                        'style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "deleteElemento(' + i + ')"></td>' +
                        '</tr>'
                        );
            } else {
                for (var i = 1; i <= longitud; i++) {
                    $("#tablainterna").append(
                            '<tr id="fila_' + i + '">' +
                            '<td hidden><input type="text" id="id_' + i + '" name="id" style="margin-left: 9px;" ></td>' +
                            '<td><input type="text" id="rangoini_' + i + '" name="rangoini" style="margin-left: 9px;"></td>' +
                            '<td><label style = "margin-left:-10px;"> A </label></td>' +
                            '<td><input type = "text" id = "rangofin_' + i + '" name = "rangofin" style = "margin-left: 9px;"></td>' +
                            '<td><label>$</label></td>' +
                            '<td><input type = "text" id = "vlrdes_' + i + '" name = "vlrdes" style = "margin-left: 9px;"></td>' +
                            '<td> </td>' +
                            '<td><input type = "text" id = "desc_' + i + '" name = "desc" style = "margin-left: 9px;"></td>' +
                            '<td><img id = "add_' + i + '" src = "/fintra/images/botones/iconos/adds.png"' +
                            'style = "margin-left: 5px; height: 19px; vertical-align: middle;" onclick = "addElemento()"></td>' +
                            '<td><img id = "delete_' + i + '" src = "/fintra/images/botones/iconos/delete.png"' +
                            'style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "deleteElemento(' + i + ')"></td>' +
                            '</tr>'
                            );
                }

                for (var i = 0; i <= longitud; i++) {
                    $("#rangoini_" + (i + 1)).val(json.info[i].galonaje_inicial);
                    $("#rangofin_" + (i + 1)).val(json.info[i].galonaje_final);
                    $("#vlrdes_" + (i + 1)).val(json.info[i].vlr_descuento);
                    $("#desc_" + (i + 1)).val(json.info[i].porct_descuento);
                    $("#id_" + (i + 1)).val(json.info[i].id);
                    $("#id_config").val(json.info[0].id_config_productos);
                }
            }
        }

    });
}


function cargarHistorico(id_config) {
    var grid_tabla = jQuery("#tabla_historico");
    if ($("#gview_tabla_historico").length) {
        reloadGridMostrarHistorico(grid_tabla, 5, id_config);
    } else {
        grid_tabla.jqGrid({
            // caption: "HISTORICO",
            url: '/fintra/controller?estado=Config&accion=ProductosEDS',
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '895',
            colNames: ['Id', 'Periodo', 'Id Config productos', 'Galonaje Inicial', 'Galonaje Inicial', 'Valor Descuento', '% Descuento', 'Usuario', 'Fecha'],
            colModel: [
                {name: 'historico_id', index: 'historico_id', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'periodo', index: 'periodo', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'id_config_productos', index: 'id_config_productos', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'galonaje_inicial', index: 'galonaje_inicial', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'galonaje_final', index: 'galonaje_final', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'vlr_descuento', index: 'vlr_descuento', width: 190, sortable: true, align: 'left', hidden: false},
                {name: 'porct_descuento', index: 'porct_descuento', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'usuario', index: 'usuario', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'fecha', index: 'fecha', width: 100, sortable: true, align: 'left', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            //   pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            //   pgtext: null,
            //   pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                data: {
                    opcion: 5,
                    id_config: id_config
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_historico").navButtonAdd('#pager', {
            //caption: "Nuevo",
            onClickButton: function () {

            }
        });
    }
}

function reloadGridMostrarHistorico(grid_tabla, opcion, id_config) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controller?estado=Config&accion=ProductosEDS',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                id_config: id_config
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function mostrarHistorico() {
    var id_config;
    var grid_tabla = jQuery("#tabla_productos");
    var ids = grid_tabla.jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var num = ids[i];
        var producto = grid_tabla.getRowData(num).producto;
        if (producto === 'COMBUSTIBLE ACPM') {
            id_config = grid_tabla.getRowData(num).id;
        }
    }
    cargarHistorico(id_config);
    $("#dialogHistorico").dialog({
        width: '930',
        height: '400',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'HISTORICO COMBUSTIBLE ACPM',
        buttons: {//crear bot�n cerrar
            "Salir": function () {
                $("#tablainterna").html('');
                $(this).dialog("close");
            }
        }
    });
    // $("#dialogHistorico").siblings('div.ui-dialog-titlebar').remove();
}