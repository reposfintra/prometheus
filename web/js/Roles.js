

function ListarRoles() {

  
    $.ajax({
        type: 'POST',
        url: "controller?estado=Roles&accion=Administrar&carpeta=/jsp/roles&pagina=ListarRoles.jsp",
      
        dataType: 'json',
        data: {
            opcion: 1          
                },
        success: function(Resp) {
         ///////////////////////////////////////////////////////////////////////////////////////////


        for(var i in Resp) {
        for(var j in Resp[i]) {
            

            if(j=='eliminar'){
             Resp[i][j]="<a style=\"color:black;\" href='#' id='" +Resp[i]["nit"]+"' name='" +Resp[i]["nombre"]+"' onclick=\"ElimRol("+Resp[i]["id"]+")\">"+ Resp[i][j]+"</a>";
            }   
            
            if(j=='modificar'){
            // Resp[i][j]="<a href='#'  onclick='ModifReg("+Resp[i]["id"]+")'>"+ Resp[i][j]+"</a>";
                Resp[i][j]="<a style=\"color:black;\" style=\"color:black;\" href='/fintra/controller?estado=Roles&accion=Administrar&carpeta=/jsp/roles&pagina=ModificarRoles.jsp&id_rol="+Resp[i]["id"]+"')>"+ Resp[i][j]+"</a>";
            }
        //console.log(j, '-->' ,Resp[i][j]);
        }
        }
        
            $("#TabRoles").jqGrid({
                
                data: Resp,       
                datatype: "local",
                height: 350,
		width: 380,
                viewrecords: true, // show the current page, data rang and total records on the toolbar
                caption: "Lista de roles",
               // rowNum: 15,
                pager: "#CargarTablaRoles",
                
                colModel: [
                     {label: 'Id', name: 'id', index: 'id', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: ""}},

                    { label: 'Rol',   name: 'descripcion',    width: 75, key:true },
                    //{ label: 'Eliminar', name: 'eliminar',  width: 100 },
                    { label: 'Editar',   name: 'modificar', width: 100 }
                ]

            });
        },
        error: function(xhr, ajaxOptions, thrownError) {
             mensajesDelSistema("Error al mostrar los roles.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 

        }
    }); // fin $.ajax

}


function CargarDatosFormEdicion(id_rol) {


    $.ajax({
        type: 'POST',
               url: "/fintra/controller?estado=Roles&accion=Administrar",
      
      
        dataType: 'json',
        data: {
            id_rol:id_rol,
            opcion: 3           
                },
        success: function(Resp) {
          $("#descripcion").val(Resp[0]["descripcion"]);
        },
        
        error: function(xhr, ajaxOptions, thrownError) {
             mensajesDelSistema("Error al cragar la informacion.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax
     
     

}

function ModificarRol(id_rol){
    var descripcion = $("#descripcion").val();
   if(descripcion==""){
      mensajesDelSistema("Debe de ingresar la descripcion del rol", '300', '150',  true); 
}else{
   var msj="Desea Confirmar la modificacion del rol!";
   $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: 300,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Cancelar": function () {
                $(this).dialog("destroy");
                mensajesDelSistema("Modificacion Cancelada", '300', '150',  true); 
            }, "Aceptar": function () {    
                
                           $.ajax({
        type: 'POST',
        url: "controller?estado=Roles&accion=Administrar&carpeta=/jsp/roles&pagina=ListarRoles.jsp",
      
        dataType: 'json',
        data: {
            id_rol: id_rol,
            descripcion: descripcion,
             opcion: 4
                },
        success: function(Resp) {

              if(Resp=="ok"){
                 alertaExitosa("Se ha modificado correctamente el rol "+descripcion, '350', '150'); 
            }else{
                  mensajesDelSistema(Resp, '300', '150',  true); 
                // location.reload();
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
             mensajesDelSistema("Error al modificar el rol.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax   
            $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
} 
  
}

function ElimRol(id_rol){
    var msj="Desea confirmar la eliminacion del rol!";
   $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: 300,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Cancelar": function () {
                $(this).dialog("destroy");
                mensajesDelSistema("Eliminacion Cancelada", '300', '150',  true); 
            }, "Aceptar": function () {     
                               $.ajax({
        type: 'POST',
        url: "/fintra/controller?estado=Roles&accion=Administrar",
      
        dataType: 'json',
        data: {
            opcion: 5,
            id_rol:id_rol
                },
        success: function(Resp) {
             alertaExitosa(Resp, '350', '150'); 
                  
        },
        error: function(xhr, ajaxOptions, thrownError) {
              mensajesDelSistema("Error al eliminar el rol.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax     
              $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();

}


function CrearRol(){
    
    var descripcion = $("#descripcion").val();

if(descripcion===""){
    mensajesDelSistema("Debe de ingresar la descripcion del rol", '300', '150',  true); 
}else{
  var msj="Desa confirmar la creacion del rol!";
   $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: 300,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Cancelar": function () {
                $(this).dialog("destroy");
                mensajesDelSistema("Creacion cancelada", '300', '150',  true); 
            }, "Aceptar": function () {
                               $.ajax({
        type: 'POST',
       url: "/fintra/controller?estado=Roles&accion=Administrar&carpeta=/jsp/roles&pagina=CrearRoles.jsp",
      
        dataType: 'json',
        data: {
            descripcion: descripcion,
            opcion: 2
                },
        success: function(Resp) {
            
            if(Resp=="ok"){
                 alertaExitosa("Se a creado correctamente el rol", '300', '150'); 
            }else{
                  mensajesDelSistema(Resp, '300', '150',  true); 
                // location.reload();
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
             mensajesDelSistema("Error al crear el rol.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax 
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
  
}
 
    
}


function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}


function alertaExitosa(msj, width, height) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {
                $(this).dialog("destroy");
                window.location="/fintra/controller?estado=Roles&accion=Administrar&carpeta=/jsp/roles&pagina=ListarRoles.jsp";
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}