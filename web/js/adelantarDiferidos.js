window.onload = function () {
    cargarTipoDiferidos();
    document.getElementById("buscar").onclick = buscarDiferidos;
    document.getElementById("salir").addEventListener("click", function () {
        window.close();
    });
};

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function hideGrid() {
    $("#gbox_tabla_NegocioLibranza").hide();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html(msj);
    } else {
        $("#msj").html(msj);
    }
    $("#dialogMsg").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function cargarUnidadesNegocio() {
    var unegocio = document.getElementById("unegocio");
    $.ajax({
        type: "POST",
        async: true,
        url: "./controller?estado=Contabilidad&accion=General",
        dataType: "json",
        data: {
            opcion: 14
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, 400, 180);
                    return;
                }
                try {
                    json.forEach(function (element) {
                        unegocio.innerHTML += "<option value='" + element.value + "'>" + element.descripcion + "</option>";
                    });
                } catch (exception) {
                    mensajesDelSistema("Error al cargar las unidades de negocio", 400, 180);
                }
            } else {
                mensajesDelSistema("No se obtuvieron las unidades de negocio", 400, 150);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error en la comunicacion con el servidor", 400, 150);
        }
    });
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: "json",
        url: "./controller?estado=Contabilidad&accion=General",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                negocio: $("#negocio").val(),
                tipodif: $("#tipodif").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
    $("#gbox_tabla_NegocioLibranza").show();
}

function cargarTipoDiferidos() {
    var tipodif = document.getElementById("tipodif");
    $.ajax({
        type: "POST",
        async: true,
        url: "./controller?estado=Contabilidad&accion=General",
        dataType: "json",
        data: {
            opcion: 14            
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, 400, 180);
                    return;
                } else {
                    json.forEach(function (element) {
                       var option = document.createElement("option");
                        option.text = element.descripcion;
                        option.value = element.value;
                        tipodif.add(option);
                    });
                }
            } else {
                mensajesDelSistema("No se pudo cargar los tipos de diferidos", 400, 150);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error en la comunicacion con el servidor", 400, 150);
        }
    });
}

function buscarDiferidos() {
    var tipodif = document.getElementById("tipodif").value;
    var negocio = document.getElementById("negocio").value;
    if (negocio == "") {
        alert("Debe ingresar el c�digo del negocio");
    } else {
        var grid_tabla_ = jQuery("#tabla_diferidos");
        if ($("#gview_tabla_diferidos").length) {
            reloadGridMostrar(grid_tabla_, 15);
        } else {
            grid_tabla_.jqGrid({
                caption: "Diferidos",
                url: "./controller?estado=Contabilidad&accion=General",
                mtype: "POST",
                datatype: "json",
                height: "250",
                width: "1040",
                colNames: ["Estado", "Negocio", "Tipo diferido", "Diferido", "Periodo", "Valor", "Cuotas", "Fecha Contabilizacion", "Fecha vencimiento", "Endosado", "Apoteosys"],
                colModel: [
                    {name: "estado", index: "estado", width: 50, align: "center", hidden: false},
                    {name: "negocio", index: "negocio", width: 100, align: "center", hidden: false, key: true},
                    {name: "tipo_diferido", index: "tipo_diferido", width: 80, align: "center", hidden: false},
                    {name: "diferido", index: "diferido", width: 100, align: "center", hidden: false, key: true},
                    {name: "periodo", index: "periodo", width: 100, align: "center", hidden: false},                    
                    {name: 'valor', index: 'valor', width: 90, sortable: true, align: 'center', hidden: false,
                        formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                    {name: "numero_cuota", index: "numero_cuota", width: 50, align: "center", hidden: false},
                    {name: "fecha_contabilizacion", index: "fecha_contabilizacion", width: 170, align: "center", hidden: false},
                    {name: "fecha_vencimiento", index: "fecha_vencimiento", width: 170, align: "center", hidden: false},
                    {name: "endosado", index: "endosado", width: 70, align: "center", hidden: false},
                    {name: "procesado", index: "procesado", width: 70, align: "center", hidden: false}
                ],
                rownumbers: true,
                rownumWidth: 25,
                rowNum: 1000,
                loadonce: false,
                gridview: true,
                pager: '#pager',
                viewrecords: true,
                multiselect: true,
                ajaxGridOptions: {
                    dataType: "json",
                    type: "POST",
                    data: {
                        opcion: 15,
                        negocio: negocio,
                        tipodif: tipodif
                    }
                },
                loadComplete: function (data) {
                    $("#dialogLoading").hide();
                },
                loadError: function (xhr, status, error) {
                    mensajesDelSistema("Error en la comunicacion con el servidor", 400, 150);
                }
            }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: true}).navButtonAdd("#pager", {
                caption: "Adelantar",
                title: "Adelantar",
                onClickButton: function () {
                    adelantarDiferidos();
                }
            }).navButtonAdd("#pager", {
                caption: "Anular",
                title: "Anular",
                onClickButton: function () {
                    anularDiferidos();
                }});
        }
    }
    $("#gbox_tabla_diferidos").show();
}

function adelantarDiferidos() {
    var negocio = document.getElementById("negocio").value;   
    var grid = $("#tabla_diferidos");
    var rowKey = grid.getGridParam("selrow");
    console.log(grid.getGridParam("selarrrow"));
    if (!rowKey)
        mensajesDelSistema("No ha seleccionado ning�n registro", 400, 180);
    else {       
        $.ajax({
            type: "POST",
            async: true,
            url: "./controller?estado=Contabilidad&accion=General",
            dataType: "json",
            data: {
                opcion: 16,
                negocio: negocio,
                diferidos: grid.getGridParam("selarrrow")
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, 400, 180);
                        return;
                    } else if (json.mensaje) {
                        reloadGridMostrar(grid, 15);
                        mensajesDelSistema(json.mensaje, 400, 150);
                    }
                } else {
                    mensajesDelSistema("No se pudo adelantar los diferidos", 400, 150);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mensajesDelSistema("Error en la comunicacion con el servidor", 400, 150);
            }
        });
    }
}

function anularDiferidos() {
    var negocio = document.getElementById("negocio").value;   
    var grid = $("#tabla_diferidos");
    var rowKey = grid.getGridParam("selrow");
    console.log(grid.getGridParam("selarrrow"));
    if (!rowKey)
        mensajesDelSistema("No ha seleccionado ning�n registro", 400, 180);
    else {       
        $.ajax({
            type: "POST",
            async: true,
            url: "./controller?estado=Contabilidad&accion=General",
            dataType: "json",
            data: {
                opcion: 17,
                negocio: negocio,
                diferidos: grid.getGridParam("selarrrow")
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, 400, 180);
                        return;
                    } else if (json.mensaje) {
                        reloadGridMostrar(grid, 15);
                        mensajesDelSistema(json.mensaje, 400, 150);
                    }
                } else {
                    mensajesDelSistema("No se pudo anular los diferidos", 400, 150);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mensajesDelSistema("Error en la comunicacion con el servidor", 400, 150);
            }
        });
    }
}
