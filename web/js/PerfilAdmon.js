/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var empresas = [];

function cargarOpciones(json) {
    var titulos = $('#titulos')
            , opciones = $('#opciones')
            , tr, primero, asignaciones;
    var elemento, i = 0, j = 0, k = 0;
    console.log(json);
    for (i = 0; i < json.empresas.length; i++) {
        titulos.append(
                '<td id="' + json.empresas[i].dstrct + '" style="text-align: center; max-width: 100px;" '
                + 'onclick="espejarOpciones(this.id)"'+'>'
                + json.empresas[i].nombre + '</td>');
        empresas[json.empresas[i].dstrct] = i;
        empresas[i] = json.empresas[i].dstrct;
    }
    //console.log(empresas);
    if(json.asignaciones) {
        asignaciones = json.asignaciones;
    }
    opciones.empty();
    for (j = 0; j < json.opciones.length; j++) {
        elemento = json.opciones[j];
        tr = "<tr id='" + elemento.codigo + "' ";
        if(elemento.tipo_opcion) {
            tr += "class = 'opcion'";
        } else {
            tr += "class = 'menu'"; 
        }
        tr += "> <td style='padding-left: "+(elemento.nivel-0.6)+"em;'><label>"
           + elemento.nombre 
           +" ["+elemento.codigo+"]"
           + "</label></td>";
        for (k = 0; k < i; k++) {
            tr += "<td style='text-align: center;'>"
                    + "<input type='checkbox' "
                    + "id='chk_" + elemento.codigo + "_" + empresas[k] + "' "
                    + "name='" + elemento.padre + "_" + empresas[k] + "' ";
            
            var a = empresas[k] + "_" + elemento.codigo;
            if (asignaciones && asignaciones[a]) {
                tr += "checked ";
            }
            
            if (!elemento.tipo_opcion) {
                tr += " disabled ";
                //tr += "onclick='seleccionarDescendencia(\"" + empresas[k] + "\",\"" + elemento.codigo + "\")'";
            } else {
                tr += "onclick='seleccionarAscendencia(\"" + empresas[k] + "\",\"" + elemento.padre + "\")'";
            }
            tr += "></td>";
        }
        tr += "</tr>";
        if (elemento.padre === '0') {
            if (j === 0) {
                opciones.append(tr);
            } else {
                primero.before(tr);
            }
            primero = $('#' + elemento.codigo);
        } else {
            $('#' + elemento.padre).after(tr);
        }
    }
}
function seleccionarDescendencia(distrito, padre) {
    var hijos = document.getElementsByName( padre + "_" + distrito );
    for (var i = 0; i < hijos.length; i++) {
        hijos[i].checked = true;
    }
}
function seleccionarAscendencia(distrito, padre) {
    var id = 'chk_'+ padre + "_" + distrito;
    var chk = false;
    var elemento = document.getElementById( id );
    if(!elemento) return;
    
    var hijos = document.getElementsByName(padre+"_"+distrito);
    for ( var i = 0; i < hijos.length; i++ ) {
        //console.log(hijos[i]);
        chk = hijos[i].checked;
        if(chk) break;
    }
    
    elemento.checked = chk;
    var abuelo = elemento.name.replace('_'+distrito,'');
    //console.log( id + '::: '+ abuelo + ' :: ' +elemento);

    if(abuelo != '0' ) {
        seleccionarAscendencia(distrito, abuelo);
    }
}
function crearPerfil(opcion) {
    var codigo = $('#codigo').val()
        , estado = $('#estado').val()
        , nombre = $('#nombre').val()
        , opciones = $('#opciones').children()
        , filas = []
        , i = 0, v = 0, elemento;

    for (i = 0; i < empresas.length; i++) {
        filas.push({
            distrito: empresas[i]
            , opciones: []
        });
    }
    for (i = 0; i < opciones.length; i++) {
        for (v = 0; v < empresas.length; v++) {
            elemento = $('#chk_' + opciones[i].id + '_' + empresas[v]);
            if (elemento.is(':checked')) {
                filas[v].opciones.push(opciones[i].id);
            }
        }
    }
    if(!codigo || !nombre) {
        alert('Informacion basica obligatoria, esta incompleta');
        return;
    } 
    if (filas.length === 0){
        alert('Seleccione opciones para el perfil');
        return;
    }
    $.ajax({
        url: '/fintra/controller?estado=Perfil&accion=Admon',
        datatype: 'json',
        type: 'post',
        data: {opcion: 0
            , informacion: JSON.stringify({perfil: {estado: estado, codigo: codigo, nombre: nombre}, rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    alert(json.error);
                } else {
                    alert(json.mensaje);
                    if( json.mensaje == 'Perfil Creado' || json.mensaje == 'Perfil Actualizado' ) {
                        window.location =  '/fintra/jsp/trafico/perfil/PerfilAdmon.jsp?idp='+json.idp+'&tipo=objeto&cmd='+json.cmd;
                    }
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                
            }
        },
        error: function () {
            
        }
    });
}
function espejarOpciones(destino) {
    var origen = prompt('distrito origen');
    
    if(!origen) return;
    var opciones = $('#opciones').children()
      , filas = []
      , i = 0, elOrigen, elDestino;
    for (i = 0; i < opciones.length; i++) {
        elOrigen = $('#chk_' + opciones[i].id + '_' + origen);
        elDestino = $('#chk_' + opciones[i].id + '_' + destino);

        if (elOrigen.is(':checked')) {
            elDestino.attr( 'checked', true );
        }
    }
}