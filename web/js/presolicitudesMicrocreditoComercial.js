/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    //$("#fecha").val('');
    //$("#fechafin").val('');
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())

    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $("#fecha").datepicker("setDate", myDate);
    $("#fechafin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');


    $('#buscar').click(function () {
        cargaPresolicitudesMicrocredito();
    });

//    $('#exportar').click(function () {
//        exportarExcelAuditoria();
//    });

    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $('#limpiar').click(function () {
        $("#fecha").val('');
        $("#fechafin").val('');
    });
});

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargaPresolicitudesMicrocredito() {
    var grid_tabla = $("#tabla_Presolicitudes");
    if ($("#gview_tabla_Presolicitudes").length) {
        reloadGridTabla(grid_tabla, 3);
    } else {
        grid_tabla.jqGrid({
            caption: "PRESOLICITUDES MICROCREDITO",
            url: "./controller?estado=Administracion&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '600',
            width: '1650',
            colNames: ['Entidad', 'Numero solicitud', 'Negocio', 'Tipo Doc', 'Identificacion', 'Nombre cliente', 'Nombre', 'Apellido', 'Fecha Expedicion', 'Fecha Nacimiento', 'Celular', 'Email', 'Ciudad',
                'Total Ingresos', 'Obligaciones Financieras', 'Gastos Familiares', 'Estado inicial solicitud', 'Estado solicitud', 'Estado negocio', 'Causal','Ingresado por' ,'Asesor', 'id_convenio', 'Monto credito', 'Valor cuota', 'Numero cuotas',
                'Comentario', 'Etapa', 'Medio', 'Fecha presolicitud','Estado Operaciones', 'Causal','Estado Sol', 'Acciones'],
            colModel: [
                {name: 'entidad', index: 'entidad', width: 150, sortable: true, align: 'center', hidden: false},
                {name: 'numero_solicitud', index: 'numero_solicitud', width: 80, sortable: true, align: 'center', hidden: false, key: true},
                {name: 'cod_neg', index: 'cod_neg', width: 90, sortable: true, align: 'center', hidden: false},
                {name: 'tipo_identificacion', index: 'tipo_identificacion', width: 110, sortable: true, align: 'left', hidden: false},
                {name: 'identificacion', index: 'identificacion', width: 110, sortable: true, align: 'left', hidden: false},
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 110, sortable: true, align: 'left', hidden: false},
                {name: 'primer_nombre', index: 'primer_nombre', width: 110, sortable: true, align: 'left', hidden: true},
                {name: 'primer_apellido', index: 'primer_apellido', width: 110, sortable: true, align: 'left', hidden: true},
                {name: 'fecha_expedicion', index: 'fecha_expedicion', width: 110, sortable: true, align: 'left', hidden: true},
                {name: 'fecha_nacimiento', index: 'fecha_nacimiento', width: 110, sortable: true, align: 'left', hidden: true},
                {name: 'celular', index: 'celular', width: 110, sortable: true, align: 'left', hidden: false},
                {name: 'email', index: 'email', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'ciudad', index: 'ciudad', width: 150, sortable: true, align: 'left', hidden: true},
                {name: 'total_ingresos', index: 'total_ingresos', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total_obligaciones_financieras', index: 'total_obligaciones_financieras', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total_gastos_familiares', index: 'total_gastos_familiares', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'estado_inicial_solicitud', index: 'estado_inicial_solicitud', width: 110, sortable: true, align: 'left', hidden: false},
                {name: 'estado_solicitud', index: 'estado_solicitud', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'estado_negocio', index: 'estado_negocio', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'causal', index: 'causal', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'creation_user', index: 'creation_user', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'asesor', index: 'asesor', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'id_convenio', index: 'id_convenio', width: 70, sortable: true, align: 'left', hidden: false},
                {name: 'monto_credito', index: 'monto_credito', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_cuota', index: 'valor_cuota', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'numero_cuotas', index: 'numero_cuotas', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'comentario', index: 'comentario', width: 170, sortable: true, align: 'left', hidden: false},
                {name: 'etapa', index: 'etapa', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'medio', index: 'medio', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'fecha_presolicitud', index: 'fecha_presolicitud', width: 150, sortable: true, align: 'center', hidden: false},
                {name: 'rechazo_operaciones', index: 'rechazo_operaciones', width: 150, sortable: true, align: 'center', hidden: false},
                {name: 'causal_rechazo', index: 'causal_rechazo', width: 150, sortable: true, align: 'center', hidden: false},
                {name: 'estado_sol', index: 'estado_sol', width: 150, sortable: true, align: 'center', hidden: true},
                {name: 'acciones', index: 'acciones', width: 100, sortable: true, align: 'left', hidden: false, search: false}
            ],
            rowNum: 10000,
            rowTotal: 10000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            sortname: 'invdate',
            gridComplete: function () {
                var cant = jQuery("#tabla_Presolicitudes").jqGrid('getDataIDs');
                    for (var i = 0; i < cant.length; i++) {
                        //var cl = cant[i];
                        var num_solicitud = $("#tabla_Presolicitudes").getRowData(cant[i]).numero_solicitud;
                        var negocio = $("#tabla_Presolicitudes").getRowData(cant[i]).cod_neg;
                        var estado = $("#tabla_Presolicitudes").getRowData(cant[i]).estado_sol;
                        var rechazado_operaciones=$("#tabla_Presolicitudes").getRowData(cant[i]).rechazo_operaciones;
                        //var cl = cant[i];


                        if ((estado === "P" || estado === "Z") && rechazado_operaciones === "" && negocio === "") {
                            //form = "<img src='/fintra/images/botones/iconos/editDoc2.png' align='center' style='height:28px;width:20px;margin-left: 4px;padding:4px' value='cambio' title='Completar Formulario' onclick=\"cargarFormulario('" + num_solicitud + "');\" />";
                            form1 = "<img src='/fintra/images/botones/iconos/carpeta.png' align='center' style='height:28px;width:20px;margin-left: 4px;padding:4px' value='archivos' title='Ver Archivos' onclick=\"ventanaVerArchivo('" + num_solicitud + "');\" />";
                            form2 = "<img src='/fintra/images/botones/iconos/anular3.png' align='center' style='height:28px;width:20px;margin-left: 4px;padding:4px' value='Rechazar' title='Rechazar' onclick=\"rechazarpresolicitud('" + num_solicitud + "');\" />";

                            jQuery("#tabla_Presolicitudes").jqGrid('setRowData', cant[i], {acciones: form1 + form2});
                        }
                        
                        if(rechazado_operaciones==="Rechazado"){
                          
                            form1 = "<img src='/fintra/images/botones/iconos/carpeta.png' align='center' style='height:28px;width:20px;margin-left: 4px;padding:4px' value='archivos' title='Ver Archivos' onclick=\"ventanaVerArchivo('" + num_solicitud + "');\" />";
                            jQuery("#tabla_Presolicitudes").jqGrid('setRowData', cant[i], {acciones: form1});

                        }

                        if (rechazado_operaciones==="Stand By") {
                                form1 = "<img src='/fintra/images/botones/iconos/carpeta.png' align='center' style='height:28px;width:20px;margin-left: 4px;padding:4px' value='archivos' title='Ver Archivos' onclick=\"ventanaVerArchivo('" + num_solicitud + "');\" />";
                                //form2 = "<img src='/fintra/images/revert.png' align='center' style='height:28px;width:20px;margin-left: 4px;padding:4px' value='devolver' title='Devolver' onclick=\"ventanaDevolverSolicitud('" + num_solicitud + "');\" />";
                                jQuery("#tabla_Presolicitudes").jqGrid('setRowData', cant[i], {acciones: form1});
                            }

                        

                        if (negocio != "") {                        
                            form1 = "<img src='/fintra/images/botones/iconos/carpeta.png' align='center' style='height:28px;width:20px;margin-left: 4px;padding:4px' value='archivos' title='Ver Archivos' onclick=\"ventanaVerArchivo('" + num_solicitud + "');\" />";
                            jQuery("#tabla_Presolicitudes").jqGrid('setRowData', cant[i], {acciones: form1});
                        }
                }

            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
            },
            ajaxGridOptions: {
                
                data: {
                    opcion: 3,
                    fecha_inicio: $("#fecha").val(),
                    fecha_fin: $("#fechafin").val(),
                    identificacion: $("#identificacion").val(),
                    num_solicitud: $("#num_solicitud").val(),
                    estado_solicitud: $("#estado_solicitud").val(),
                    r_operaciones: $("#r_operaciones").val(),
                    etapa: $("#etapa").val()
                    

                }
                
                
            },
            
           
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
//        $("#tabla_Presolicitudes").navButtonAdd('#pager', {
//            /*caption: "Exportar Excel",
//             onClickButton: function () {
//             var info = grid_tabla.getGridParam('records');
//             if (info > 0) {
//             exportarExcelAuditoria();
//             } else {
//             mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
//             }
//             }*/
//        });
    }
}


function reloadGridTabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                fecha_inicio: $("#fecha").val(),
                fecha_fin: $("#fechafin").val(),
                identificacion: $("#identificacion").val(),
                num_solicitud: $("#num_solicitud").val(),
                estado_solicitud: $("#estado_solicitud").val(),
                r_operaciones: $("#r_operaciones").val(),
                etapa: $("#etapa").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

//function cargarFormulario(num_solicitud) {
//    var datos = $('#tabla_Presolicitudes').getRowData(num_solicitud);
//    //alert(num_solicitud)
//    //var p = "opcion=src_sol&dato="+num_solicitud+"&tipoconv=Microcredito&vista=1";
//    //new Ajax.Request(url, { parameters: p, method: 'post', onComplete: completo } );
//    $.ajax({
//        type: 'POST',
//        url: "./controller?estado=GestionSolicitud&accion=Aval",
//        dataType: 'text',
//        async: false,
//        method: 'post',
//        data: {
//            opcion: 'pre_solicitud',
//            datos: JSON.stringify({info: datos})
//                    //num_solicitud: num_solicitud
//        },
//        success: function (resp) {
//            console.log(resp);
//            window.open(resp);
//        }
//    });
//
//
//
//
//}

function ventanaVerArchivo(num_solicitud) {

    $("#num_solicitud1").val(num_solicitud);
    obtenerArchivoCargado(num_solicitud);
    $("#verarchivo").dialog({
        width: 500,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: true,
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}


function obtenerArchivoCargado(num_solicitud) {
    
//    var negocio =  $('#negocio1').val();
    $('#tbl_archivos_cargados').html('');
    $.ajax({
        type: 'POST',
        url: './controller?estado=Admin&accion=Fintra',
        dataType: 'json',
        async: false,
        data: {
            opcion: 95,
            num_solicitud1: num_solicitud
        },
        success: function (jsonData) {
                if (!isEmptyJSON(jsonData)) {                     
                   for (var i = 0; i < jsonData.length; i++) {
                       var nomarchivo = jsonData[i];
                       var numdoc = 'Doc'+i;
                       $('#tbl_archivos_cargados').append("<tr class='fila'><td><a target='_blank' onClick=\"consultarNomarchivo('"+num_solicitud+"','"+nomarchivo+"','"+numdoc+"');\n\
                       \" style='cursor:hand' ><strong>"+nomarchivo+"</strong></a> &nbsp;&nbsp;&nbsp;<a class='"+numdoc+" ocultar' target='_blank' href='#' style='display:none'>Ver</a></td></tr>");                
                    }
                           
                }
            },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function consultarNomarchivo(num_solicitud, nomarchivo,numdoc) {
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {opcion: 96,
            num_solicitud1: num_solicitud,
            nomarchivo: nomarchivo},
        async: false,
        url: "./controller?estado=Admin&accion=Fintra",
        success: function (jsonData) {
            //alert(JSON.stringify(jsonData))
            if (!isEmptyJSON(jsonData)) {

                if (jsonData.error) {
                    mensajesDelSistema(jsonData.error, '270', '165');
                    return;
                }
                if (jsonData.respuesta === "SI") {
                    $('.ocultar').css('display','none');
                    $('.'+numdoc).attr("href", "/fintra/images/multiservicios/" + jsonData.login + "/" + nomarchivo);
                    $('.'+numdoc).fadeIn();
                } else {
                    mensajesDelSistema(".::ERROR AL OBTENER ARCHIVO::.", '250', '150');
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function rechazarpresolicitud(num_solicitud,msj, width, height, swHideDialog){
    
//    if (swHideDialog) {
//        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
//    } else {
//        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
//    }
    $("#acciones").dialog({
        width: '500',
        height: '180',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'ACCIONES',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                ventanaRechazar(num_solicitud);
                $(this).dialog("close");
            },
            "No": function () {
                $(this).dialog("close");
            }
            
            
        }
    });
    
}

function ventanaRechazar(num_solicitud) {
    $("#num_solicitud1").val(num_solicitud);
    //$("#comentario").val(comentario);
    $("#rechazar").dialog({
        width: '500',
        height: '180',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'RECHAZAR',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                rechazarSolicitud(num_solicitud);
            },
            "No": function () {
                $(this).dialog("close");
                $("#num_solicitud1").val('');
                $("#causal2").val('');
                // $("#comentario").val('');
            }
        }
    });
    //}
}

function rechazarSolicitud(num_solicitud) {
    //var num_solicitud = $('#tabla_Presolicitudes').getRowData(num_solicitud);
    var causal = document.getElementById('causal2').value;
    if (causal !== '') {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controller?estado=Administracion&accion=Fintra",
        data: {
            opcion: 4
            ,num_solicitud: num_solicitud
            ,causal: causal
            ,estado_pre: 'RECHAZADO COMERCIAL'
        },
        success: function (data, textStatus, jqXHR) {
            //alert(data.respuesta)
            if (data.respuesta === 'Negocio Rechazado') {
                mensajesDelSistema("Exito al rechazar la solicitud", '230', '150',true);
                $("#rechazar").dialog('close');
                cargaPresolicitudesMicrocredito();
                
            }
        }, error: function (result) {
            alert('No se puede rechazar, intente nuevamente');
        }
    });
    }else {
        mensajesDelSistema("Debe escoger una causal de rechazo", '230', '150', false);
    }

}

//function ventanaStandBy(num_solicitud) {
//    $("#num_solicitud1").val(num_solicitud);
//    //$("#comentario").val(comentario);
//    $("#standby").dialog({
//        width: '500',
//        height: '180',
//        show: "scale",
//        hide: "scale",
//        resizable: false,
//        position: "center",
//        modal: true,
//        closeOnEscape: false,
//        title: 'STAND BY',
//        open: function (event, ui) {
//            $(".ui-dialog-titlebar-close").hide();
//        },
//        buttons: {
//            "StandBy": function () {
//                enviarSolicitudStandBy(num_solicitud);
//            },
//            "Salir": function () {
//                $(this).dialog("close");
//                $("#num_solicitud1").val('');
//                // $("#identificacion").val('');
//                // $("#comentario").val('');
//            }
//        }
//    });
//    //}
//}


//function enviarSolicitudStandBy(num_solicitud) {
//    //var num_solicitud = $('#tabla_Presolicitudes').getRowData(num_solicitud);
//    $("#num_solicitud1").val(num_solicitud);
//    var causal = document.getElementById('causal').value;
//    if (causal !== '') {
//    $.ajax({
//        type: 'POST',
//        dataType: 'json',
//        url: "./controller?estado=Admin&accion=Fintra",
//        data: {
//            opcion: 99
//            ,num_solicitud: num_solicitud
//            ,causal: causal
//            ,estado : 'STAND BY'
//        },
//        success: function (data, textStatus, jqXHR) {
//            //alert(data.respuesta)
//            if (data.respuesta === 'Negocio puesto en Stand By') {
//                mensajesDelSistema("Exito al enviar solicitud a Stand By", '230', '150',true);
//                $("#standby").dialog('close');
//                cargaPresolicitudesMicrocredito();
//            }
//        }, error: function (result) {
//            alert('No se puede devolver, intente nuevamente');
//        }
//    });
//}else {
//        mensajesDelSistema("Debe escoger una causal de Stand By", '230', '150', false);
//    }
//
//}
//
function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}
//
//function ventanaDevolverSolicitud(num_solicitud) {
//    
//    //alert(num_solicitud);
//    consultarCausalStandBy(num_solicitud);
//    $("#devolver").dialog({
//        width: '620',
//        height: '200',
//        show: "scale",
//        hide: "scale",
//        resizable: false,
//        position: "center",
//        modal: true,
//        closeOnEscape: false,
//        title: 'DEVOLVER STAND BY',
//        open: function (event, ui) {
//            $(".ui-dialog-titlebar-close").hide();
//        },
//        buttons: {
//            "Si": function () {
//                devolverSolicitud(num_solicitud);
//                $(this).dialog("close");
//            },
//            "No": function () {
//                $(this).dialog("close");
//                $("#num_solicitud").val('');
//            }
//        }
//    });
//}
//
//function consultarCausalStandBy(num_solicitud){
//    $("#num_solicitud2").val(num_solicitud);
//    $.ajax({
//        type: 'POST',
//        url: "./controller?estado=Admin&accion=Fintra",
//        dataType: 'text',
//        async: false,
//        data: {
//            opcion: 101,
//            num_solicitud: num_solicitud,
//        },
//        success: function (json) {
//            console.log(json);
//            if (json.error) {
//                console.log(json.error);
//                //mensajesDelSistema(json.error, '250', '180');
//                return;
//            }
//            try {
//                console.log(json);
//                $('#causal1').val(json);
//            } catch (exception) {
//                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
//            }
//
//        }, error: function (xhr, ajaxOptions, thrownError) {
//            alert("Error: " + xhr.status + "\n" +
//                    "Message: " + xhr.statusText + "\n" +
//                    "Response: " + xhr.responseText + "\n" + thrownError);
//        }
//    });
//}
//
//
//function devolverSolicitud(num_solicitud) {
//    //var num_solicitud = $('#tabla_Presolicitudes').getRowData(num_solicitud);
//    $.ajax({
//        type: 'POST',
//        dataType: 'json',
//        url: "./controller?estado=Admin&accion=Fintra",
//        data: {
//            opcion: 100
//            ,num_solicitud: num_solicitud
//        },
//        success: function (data, textStatus, jqXHR) {
//            //alert(data.respuesta)
//            if (data.respuesta === 'Negocio Devuelto') {
//                mensajesDelSistema("Exito al devolver negocio de Stand By", '230', '150');
//                cargaPresolicitudesMicrocredito();
//            }
//        }, error: function (result) {
//            alert('No se puede devolver solicitud, intente nuevamente');
//        }
//    });
//
//} 




//function mensajesDelSistema2(msj, width, height, swHideDialog) {
//    if (swHideDialog) {
//        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
//    } else {
//        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
//    }
//    $("#info").dialog({
//        width: width,
//        height: height,
//        show: "scale",
//        hide: "scale",
//        resizable: false,
//        position: "center",
//        modal: true,
//        closable: false,
//        closeOnEscape: false,
//        buttons: {//crear bot�n cerrar
//            "Aceptar": function () {
//                $(this).dialog("destroy");
//            }
//        }
//    });
//    $("#info").siblings('div.ui-dialog-titlebar').remove();
//}