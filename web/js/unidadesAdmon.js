/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    }); 
    
    listarUnidadesAdmon();
    maximizarventana();
});

function listarUnidadesAdmon() {      
    var grid_tbl_unds = jQuery("#tabla_unds_admon");
     if ($("#gview_tabla_unds_admon").length) {
        refrescarGridUndsAdmon();
     }else {
        grid_tbl_unds.jqGrid({
            caption: "Unidades Administración",
            url: "./controlleropav?estado=Maestro&accion=Proyecto",           	 
            datatype: "json",  
            height: '290',
            width: '710',         
            colNames: ['Id','Nombre', 'Descripción', 'Estado','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'nombre', index: 'nombre', width: 530, align: 'left'},      
                {name: 'descripcion', index: 'descripcion', width: 530, align: 'left', hidden:true},      
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden:true}, 
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}         
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_unds_admon'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext:null,
            pgbuttons:false,         
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion: 64
                     }
            },   
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_unds_admon").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_unds_admon").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoUnidad('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_unds_admon").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_unds_admon"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;
                                 
                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                    editarUnidad(id);
                }
                    
          }
        }).navGrid("#page_tabla_unds_admon", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_unds_admon").jqGrid("navButtonAdd", "#page_tabla_unds_admon", {
            caption: "Nuevo",
            onClickButton: function() {               
                crearUnidad();
            }
        });
    }  
       
}

function refrescarGridUndsAdmon(){   
    jQuery("#tabla_unds_admon").setGridParam({
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: {
                opcion: 64
            }
        }       
    });
    
    jQuery('#tabla_unds_admon').trigger("reloadGrid");
}

function crearUnidad(){   
    $('#div_unds_admon').fadeIn('slow'); 
    $('#idUnidad').val('');
    $('#nombre').val('');
    $('#descripcion').val('');
    AbrirDivCrearUnidad();
}

function AbrirDivCrearUnidad(){
      $("#div_unds_admon").dialog({
        width: 'auto',
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR UNIDAD',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {         
                guardarUnidad();             
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_unds_admon").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarUnidad(cl){
    
    $('#div_unds_admon').fadeIn("slow");
    var fila = jQuery("#tabla_unds_admon").getRowData(cl);  
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];

    $('#idUnidad').val(cl);
    $('#nombre').val(nombre); 
    $('#descripcion').val(descripcion); 
    AbrirDivEditarUnidad();
}

function AbrirDivEditarUnidad(){
      $("#div_unds_admon").dialog({
        width: 'auto',
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR UNIDAD',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              guardarUnidad();
            },
            "Salir": function () {              
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_unds_admon").parent().find(".ui-dialog-titlebar-close").hide();
}

function guardarUnidad(){   
    var nombre = $('#nombre').val();
    
    if(nombre!==''){
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: "./controlleropav?estado=Maestro&accion=Proyecto",
                    data: {
                        opcion:($('#idUnidad').val()==='')? 65:66,
                        id:$('#idUnidad').val(),
                        nombre:$('#nombre').val(),
                        descripcion:$('#descripcion').val()
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '270', '165');
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                refrescarGridUndsAdmon(); 
                                $("#div_unds_admon").dialog('close');
                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo guardar la unidad!!", '250', '150');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                                "Message: " + xhr.statusText + "\n" +
                                "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                });
             },500);
    }else{
         mensajesDelSistema("DEBE INGRESAR EL NOMBRE!!", '250', '150');      
    }  
}


function CambiarEstadoUnidad(rowid){
    var grid_tabla = jQuery("#tabla_unds_admon");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        data: {
            opcion: 67,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarGridUndsAdmon();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado de la unidad!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}


function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear botón de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}


function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}
