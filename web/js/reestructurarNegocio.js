/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//eventos onclik de los botones en la vista.
$(document).ready(function() {

    //boton buscar.
    $("#buscar").click(function() {
        var cedula = $("#cedula").val();
        var negocio = $("#codneg").val();

        if (negocio !== "" || cedula !== "") {

            listarNegocios(cedula, negocio);
            $("#grid_1").show();
        } else {
            mensajesDelSistema("Por favor debe llenar al menos un campo. <br/> Gracias.", 283, 150);
        }
    });

    //boton simular    
    $("#simular").click(function() {

        if ($("#valor_negocio").val() !== '0') {

            $("#simulador_credito").dialog({
                width: 1029,
                height: 480,
                show: "scale",
                hide: "scale",
                resizable: true,
                position: "center",
                modal: true,
                closeOnEscape: false,
                buttons: {//crear bot�n de cerrar


                    "Liquidar": function() {
                        calcularLiquidacion();
                    },
                    "Continuar": function() {
                        generarNegocioNuevoMicro();
                    },
                    "Salir": function() {
                        $("#cuota").val(1);
                        $("#tabla_simulador_Credito").jqGrid("clearGridData", true).trigger("reloadGrid");
                        $("#grid_liquidacion").hide();
                        $(this).dialog("close");
                    }

                }
            });

        } else {

            mensajesDelSistema("No se puede iniciar el simulador de credito <br/> Saldos en cero. Gracias.", "320", "150");
        }

    });
    //panel deslizante.
    $("#boton").click(function() {
        $("#desplegable").slideToggle("slow");
    });
    $("#desplegable").css({display: 'none'});

    //calcular fechas de pagos para el simulador.
    carcularFecha();

    //cargar departamento.
    cargarDepartamentos();

    //agregar eventos onchange
    $("#departamento_ng").change(function() {
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciudad_cg");
    });

    //agregar evento onchange
    $("#departamento").change(function() {
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciudad");
    });

});


function listarNegocios(cedula, negocio) {
    var grid_listar_negocios = jQuery("#tabla_negocios_clientes");

    if ($("#gview_tabla_negocios_clientes").length) {
        reloadGridListarNegocios(grid_listar_negocios, cedula, negocio);
    } else {

        grid_listar_negocios.jqGrid({
            caption: "Lista de negocios",
            url: "./controller?estado=Reestructuracion&accion=Negocios&opcion=1&cedula=" + cedula + "&codneg=" + negocio,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: '70',
            width: '1165',
            colNames: ['Cedula', 'Cliente', 'Neg', 'Fecha Negocio', 'Estado', 'Convenio', 'Renovacion', 'Valor Neg', 'NrCuotas', 'Saldo Cartera'],
            colModel: [
                {name: 'valor_01', index: 'valor_01', width: 80, align: 'right'},
                {name: 'valor_03', index: 'valor_03', sortable: true, width: 210, align: 'right'},
                {name: 'valor_02', index: 'valor_02', sortable: true, width: 80, align: 'right', key: true},
                {name: 'valor_04', index: 'valor_04', sortable: false, width: 140, align: 'center'},
                {name: 'valor_05', index: 'valor_05', width: 100, align: 'center'},
                {name: 'valor_06', index: 'valor_06', width: 105, align: 'center'},
                {name: 'valor_07', index: 'valor_07', width: 100, align: 'center'},
                {name: 'valor_08', index: 'valor_08', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_09', index: 'valor_09', width: 100, align: 'center'},
                {name: 'valor_10', index: 'valor_10', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 20,
            rowTotal: 1000,
            // pager: ('#page_detalles_cartera'),
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ondblClickRow: function(rowid, status, e) {
                var negocio = grid_listar_negocios.getRowData(rowid).valor_02;
                var renovacion = grid_listar_negocios.getRowData(rowid).valor_07;
               // if (renovacion !== "S") {
                    $("#cod_negocio").val(negocio);
                    facturas_negocio(negocio);
//                } else {
//                    mensajesDelSistema("Lo sentimos este negocio es una renovacion.<br/> no puede ser reestructurado.", "330", "150");
//                }
            },
            loadComplete: function() {
                if (grid_listar_negocios.jqGrid('getGridParam', 'records') > 0) {
                    cacularTotalesListarNegocios(grid_listar_negocios);

                } else {
                    mensajesDelSistema("Lo sentimos ne se encontraron resultados.<br/> Verifique los datos de entrada.", "330", "150");
                    grid_listar_negocios.jqGrid("clearGridData", true).trigger("reloadGrid");
                    jQuery("#tabla_facturas_negocio").jqGrid("clearGridData", true).trigger("reloadGrid");

                }
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });
    }


}

function reloadGridListarNegocios(grid_listar_negocios, cedula, negocio) {
    grid_listar_negocios.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructuracion&accion=Negocios&opcion=1&cedula=" + cedula + "&codneg=" + negocio
    });
    grid_listar_negocios.trigger("reloadGrid");
}

function cacularTotalesListarNegocios(grid_listar_negocios) {

    var totalItem = grid_listar_negocios.jqGrid('getCol', 'valor_10', false, 'sum');

    grid_listar_negocios.jqGrid('footerData', 'set', {
        valor_09: 'Total:',
        valor_10: totalItem

    });

}



function facturas_negocio(negocio) {


    var grid_facturas_negocio = jQuery("#tabla_facturas_negocio");

    if ($("#gview_tabla_facturas_negocio").length) {
        reloadGridFacturasNegocio(grid_facturas_negocio, negocio);
    } else {

        grid_facturas_negocio.jqGrid({
            caption: "Facturas Pendientes",
            url: "./controller?estado=Reestructuracion&accion=Negocios&opcion=2&codneg=" + negocio,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: '275',
            width: '1343',
            colNames: ['Factura Capital', 'Negocio', 'Cuota', 'Fecha Vencimiento', 'Dias Mora', 'Estado', 'Saldo Capital', 'Saldo Intereses', 'Saldo CAT', 'Seguro', 'IntxMora', 'GAC', 'Total Item'],
            colModel: [
                {name: 'documento', index: 'documento', width: 80, align: 'center'},
                {name: 'cod_neg', index: 'cod_neg', sortable: true, width: 80, align: 'center'},
                {name: 'cuota', index: 'cuota', sortable: true, width: 80, align: 'center', key: true},
                {name: 'fecha_vencimiento', index: 'fecha_vencimiento', sortable: false, width: 140, align: 'center'},
                {name: 'dias_mora', index: 'dias_mora', width: 100, align: 'center'},
                {name: 'estado', index: 'estado', width: 105, align: 'center'},
                {name: 'saldo_capital', index: 'saldo_capital', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'saldo_interes', index: 'saldo_interes', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'saldo_cat', index: 'saldo_cat', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'seguro', index: 'seguro', sortable: false, width: 70, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes_mora', index: 'interes_mora', sortable: false, width: 80, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'gasto_cobranza', index: 'gasto_cobranza', sortable: false, width: 80, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total_item', index: 'total_item', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 20,
            rowTotal: 1000,
            // pager: ('#page_detalles_cartera'),
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function() {

                if (grid_facturas_negocio.jqGrid('getGridParam', 'records') > 0) {
                    cacularTotalesFacturasNegocio(grid_facturas_negocio);
                    $("#divSimulador").css("display", "block");
                } else {
                    mensajesDelSistema("Lo sentimos ne se encontraron resultados.<br/> Verifique los datos de entrada.", "330", "150");
                }
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });


    }

}

function reloadGridFacturasNegocio(grid_facturas_negocio, negocio) {
    grid_facturas_negocio.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructuracion&accion=Negocios&opcion=2&codneg=" + negocio
    });
    grid_facturas_negocio.trigger("reloadGrid");
}

function cacularTotalesFacturasNegocio(grid_facturas_negocio) {

    var totalItem = grid_facturas_negocio.jqGrid('getCol', 'total_item', false, 'sum');
    var gastoCobranza = grid_facturas_negocio.jqGrid('getCol', 'gasto_cobranza', false, 'sum');
    var interesMora = grid_facturas_negocio.jqGrid('getCol', 'interes_mora', false, 'sum');
    var seguro = grid_facturas_negocio.jqGrid('getCol', 'seguro', false, 'sum');
    var saldoCat = grid_facturas_negocio.jqGrid('getCol', 'saldo_cat', false, 'sum');
    var saldoImp = grid_facturas_negocio.jqGrid('getCol', 'saldo_interes', false, 'sum');
    var saldoCapital = grid_facturas_negocio.jqGrid('getCol', 'saldo_capital', false, 'sum');
    $("#valor_negocio").val(numberConComas(totalItem));
    $("#saldo_capital").val(saldoCapital);
    $("#saldo_interes").val(saldoImp);
    $("#saldo_cat").val(saldoCat);
    $("#saldo_seguro").val(seguro);
    $("#intxmora").val(interesMora);
    $("#gac").val(gastoCobranza);
       
       
       
    grid_facturas_negocio.jqGrid('footerData', 'set', {
        estado: 'Total:',
        saldo_capital: saldoCapital,
        saldo_interes: saldoImp,
        saldo_cat: saldoCat,
        seguro: seguro,
        interes_mora: interesMora,
        gasto_cobranza: gastoCobranza,
        total_item: totalItem
    });

}

function replaceAll(text, busca, reemplaza) {
    while (text.toString().indexOf(busca) !== - 1)
        text = text.toString().replace(busca, reemplaza);
    return text;
}



function carcularFecha() {

    //var aux = replaceAll("2014-12-25", "-", "/");
    var date = new Date();
    var days = date.getDate();
    var fecha = "0099-01-01";
    var mes = date.getMonth() + 1;
    var anio = date.getFullYear();



    if (days >= 1 && days <= 2) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion3 = new Option(fecha, fecha);


        }

    }

    if (days > 2 && days <= 12) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-12";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion2 = new Option(fecha, fecha);
            if (mes === 11) {
                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion3 = new Option(fecha, fecha);
            } else {
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion3 = new Option(fecha, fecha);
            }

        }
    }

    if (days > 12 && days <= 17) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion3 = new Option(fecha, fecha);


        } else {
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion1 = new Option(fecha, fecha);

            if (mes === 11) {

                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion3 = new Option(fecha, fecha);

            } else {

                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion2 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
                opcion3 = new Option(fecha, fecha);
            }
        }

    }

    if (days > 17 && days <= 22) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-17";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion0 = new Option(fecha, fecha, "defauldSelected");

            if (mes === 11) {

                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion1 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-17";
                opcion3 = new Option(fecha, fecha);

            } else {

                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion1 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
                opcion2 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-17";
                opcion3 = new Option(fecha, fecha);
            }
        }

    }


    if (days > 22 && days <= 31) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-22";
            opcion3 = new Option(fecha, fecha);


        } else {

            if (mes === 11) {

                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion0 = new Option(fecha, fecha, "defauldSelected");
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion1 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-17";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-22";
                opcion3 = new Option(fecha, fecha);


            } else {

                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion0 = new Option(fecha, fecha, "defauldSelected");
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
                opcion1 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-17";
                opcion2 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-22";
                opcion3 = new Option(fecha, fecha);

            }

        }
    }


    document.formulario.primeracuota.options[0] = opcion0;
    document.formulario.primeracuota.options[1] = opcion1;
    document.formulario.primeracuota.options[2] = opcion2;
    document.formulario.primeracuota.options[3] = opcion3;


}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function SimuladorCreditoMicro(tipo_cuota, cuota, valor_negocio, primeracuota, titulo_valor) {
    var grid_simulador_credito = jQuery("#tabla_simulador_Credito");

    if ($("#gview_tabla_simulador_Credito").length) {
        reloadGridSimuladorCredito(grid_simulador_credito, tipo_cuota, cuota, valor_negocio, primeracuota, titulo_valor);
    } else {

        grid_simulador_credito.jqGrid({
            caption: "Liquidacion Negocio",
            url: "./controller?estado=Reestructuracion&accion=Negocios&opcion=3&tipo_cuota=" + tipo_cuota + "&cuota=" + cuota + "&valor_negocio=" + valor_negocio + "&primeracuota=" + primeracuota + "&titulo_valor=" + titulo_valor,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: '280',
            width: '995',
            colNames: ['Fecha', 'Cuota', 'Saldo Inicial', 'Valor Cuota', 'Capital', 'Interes', 'Capacitacion', 'Cat', 'Seguro', 'Saldo Final'],
            colModel: [
                {name: 'fecha', index: 'fecha', width: 80, align: 'center'},
                {name: 'item', index: 'item', sortable: true, width: 50, align: 'center', key: true},
                {name: 'saldo_inicial', index: 'saldo_inicial', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor', index: 'valor', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'capital', index: 'capital', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes', index: 'interes', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'capacitacion', index: 'capacitacion', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cat', index: 'cat', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'seguro', index: 'seguro', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'saldo_final', index: 'saldo_final', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            // pager: ('#page_detalles_cartera'),
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function() {

                cacularTotalesSimuladorCredito(grid_simulador_credito);
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });


    }


}


function  cacularTotalesSimuladorCredito(grid_simulador_credito) {

   
    var valor = grid_simulador_credito.jqGrid('getCol', 'valor', false, 'sum');
    var capital = grid_simulador_credito.jqGrid('getCol', 'capital', false, 'sum');
    var interes = grid_simulador_credito.jqGrid('getCol', 'interes', false, 'sum');
    var capacitacion = grid_simulador_credito.jqGrid('getCol', 'capacitacion', false, 'sum');
    var cat = grid_simulador_credito.jqGrid('getCol', 'cat', false, 'sum');
    var seguro = grid_simulador_credito.jqGrid('getCol', 'seguro', false, 'sum');
//    var saldo_final = grid_simulador_credito.jqGrid('getCol', 'saldo_final', false, 'sum');

    grid_simulador_credito.jqGrid('footerData', 'set', {
        cuota: 'Total:',
        valor: valor,
        capital: capital,
        interes: interes,
        capacitacion: capacitacion,
        cat: cat,
        seguro: seguro
    });

}

function  reloadGridSimuladorCredito(grid_simulador_credito, tipo_cuota, cuota, valor_negocio, primeracuota, titulo_valor) {
    grid_simulador_credito.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructuracion&accion=Negocios&opcion=3&tipo_cuota=" + tipo_cuota + "&cuota=" + cuota + "&valor_negocio=" + valor_negocio + "&primeracuota=" + primeracuota + "&titulo_valor=" + titulo_valor
    });
    grid_simulador_credito.trigger("reloadGrid");
}



function calcularLiquidacion() {

    if ($("#tipo_cuota").val() !== '' && $("#cuota").val() !== ''
            && $("#valor_negocio").val() !== '' && $("#primeracuota").val() !== '' && $("#titulo_valor").val() !== '') {

        $("#grid_liquidacion").show();

        if ($("#valor_negocio").val() !== '0') {

            SimuladorCreditoMicro($("#tipo_cuota").val(), $("#cuota").val(), $("#valor_negocio").val(), $("#primeracuota").val(), $("#titulo_valor").val());
        } else {

            mensajesDelSistema("Valor Negocio debe ser mayor a cero.", "298", "150");
        }
    } else {

        mensajesDelSistema("Debe diligenciar todos los campos.", "298", "150");

    }

}

function generarNegocioNuevoMicro() {
    if ($("#tipo_cuota").val() !== '' && $("#cuota").val() !== ''
            && $("#valor_negocio").val() !== '' && $("#primeracuota").val() !== '' && $("#titulo_valor").val() !== '') {

        if ($("#valor_negocio").val() !== '0' && $("#cod_negocio").val() !== '') {

            if (jQuery("#tabla_simulador_Credito").getGridParam("records") > 0) {


                mensajesDelSistema2("Espere un momento por favor...", "270", "140");


                $.ajax({
                    type: 'POST',
                    url: "./controller?estado=Reestructuracion&accion=Negocios",
                    dataType: 'json',
                    data: {
                        opcion: 4,
                        tipo_cuota: $("#tipo_cuota").val(),
                        cuota: $("#cuota").val(),
                        valor_negocio: $("#valor_negocio").val(),
                        primeracuota: $("#primeracuota").val(),
                        titulo_valor: $("#titulo_valor").val(),
                        codigo_negocio: $("#cod_negocio").val(),
                        saldo_capital:$("#saldo_capital").val(),
                        saldo_interes:$("#saldo_interes").val(),
                        saldo_cat: $("#saldo_cat").val(),
                        saldo_seguro: $("#saldo_seguro").val(),
                        intxmora: $("#intxmora").val(),
                        gac: $("#gac").val()
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {
                            if (json.error) {
                                mensajesDelSistema(json.error, '250', '180');
                                return;
                            }

                            var salida = json.respuesta.split(";");
                            if (salida[0] === 'OK') {

                                llenarFormulario(salida[1], salida[2]);
                                setTimeout(function() {
                                    closeDialog(salida[1], salida[2]);
                                }, 2000);

                            } else {
                                mensajesDelSistema("Lo sentimos no se pudo crear el nuevo negocio", '250', '150');
                            }


                        } else {
                            mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');
                        }

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                                "Message: " + xhr.statusText + "\n" +
                                "Response: " + xhr.responseText + "\n" + thrownError);
                         $("#dialogo2").dialog('close');
                    }
                }); // fin $.ajax
            } else {
                mensajesDelSistema("Debe liquidar el negocio para continuar.", "270", "150");
            }
        } else {

            mensajesDelSistema("Valor Negocio debe ser mayor a cero<br /> para el negocio :" + $("#cod_negocio").val(), "298", "150");
        }
    } else {

        mensajesDelSistema("Debe diligenciar todos los campos.", "298", "150");

    }
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function mensajesDelSistema2(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();


}

function closeDialog(numero_solicitud, cod_negocio) {
    //dialogo de espera 
    $("#dialogo2").dialog('close');
    editarNuevoFormulario(numero_solicitud, cod_negocio);
}

function editarNuevoFormulario(numero_solicitud, cod_negocio) {


    $("#editar_form").dialog({
        width: "auto",
        height: 750,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        dialogClass: 'no-close',
        buttons: {//crear bot�n 
            "Finalizar": function() {
                enviarFormulario(numero_solicitud, cod_negocio);

            }
        }
    });
   
}

function cargarDepartamentos() {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructuracion&accion=Negocios",
        dataType: 'json',
        data: {
            opcion: 5
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#departamento').append("<option value=''>Seleccione</option>");
                    $('#departamento_ng').append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#departamento').append('<option value=' + key + '>' + json[key] + '</option>');
                        $('#departamento_ng').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Reestructuracion&accion=Negocios",
            dataType: 'json',
            data: {
                opcion: 6,
                cod_ciudad: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function llenarFormulario(numero_solicitud, cod_negocio) {

    $.ajax({
        async: false,
        type: 'POST',
        url: "./controller?estado=Reestructuracion&accion=Negocios",
        dataType: 'json',
        data: {
            opcion: 7,
            numero_solicitud: numero_solicitud,
            cod_negocio: cod_negocio
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {

                    cargarCiudad(json.departamento_ng, "ciudad");
                    cargarCiudad(json.departamento, "ciudad_ng");

                    //buscamos los datos de json
                    $('#numero_solicitud').val(numero_solicitud);
                    $('#identificacion').val(json.identificacion);
                    $('#pr_apellido').val(json.primer_apellido);
                    $('#sg_apellido').val(json.segundo_apellido);
                    $('#pr_nombre').val(json.primer_nombre);
                    $('#sg_nombre').val(json.segundo_nombre);
                    $('#telefono').val(json.telefono);
                    $('#celular').val(json.celular);
                    $('#direccion').val(json.direccion);
                    $('#departamento').val(json.departamento);
                    $('#ciudad').val(json.ciudad);
                    $('#barrio').val(json.barrio);
                    $('#email').val(json.email);
                    $('#identificacion_cy').val(json.id_cony);
                    $('#pr_apellido_cy').val(json.primer_apellido_cony);
                    $('#sg_apellido_cy').val(json.segundo_apellido_cony);
                    $('#pr_nombre_cy').val(json.primer_nombre_cony);
                    $('#sg_nombre_cy').val(json.segundo_nombre_cony);
                    $('#telefono_cy').val(json.telefono_cony);
                    $('#celular_cy').val(json.celular_cony);
                    $('#direcion_cy').val(json.direccion_cony);
                    $('#nombre_negocio').val(json.nombre_ng);
                    $('#direccion_ng').val(json.direccion_ng);
                    $('#departamento_ng').val(json.departamento_ng);
                    $('#ciudad_ng').val(json.ciudad_ng);
                    $('#barrio_ng').val(json.barrio_ng);
                    $('#telefono_ng').val(json.tel_negocio);



                } catch (exception) {
                    alert('error : ' + json + '>' + json, '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function enviarFormulario(numero_solicitud, cod_negocio) {
    if ($("#observacion").val() !== '') {
        var datos_formulario = $("#formulario_cliente").serialize();
        $.ajax({
            async: false,
            url: "./controller?estado=Reestructuracion&accion=Negocios&opcion=8&negocio=" + cod_negocio,
            type: 'POST',
            dataType: 'json',
            data: datos_formulario,
            success: function(json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }

                    if (json.respuesta === "OK") {

                        $("#editar_form").dialog("close");
                        $("#simulador_credito").dialog('close');
                        $("#grid_1").hide();
                        $("#cedula").val('');
                        $("#codneg").val('');
                        $("#grid_liquidacion").hide();
                        $("#grid_1").hide();
                        $("#tabla_simulador_Credito").jqGrid("clearGridData", true).trigger("reloadGrid");
                        $("#tabla_negocios_clientes").jqGrid("clearGridData", true);
                        $("#tabla_facturas_negocio").jqGrid("clearGridData", true);
                        mensajesDelSistema("El negocio se ha creado exitosamente. <br/><br/> Formulario: " + numero_solicitud + ", Negocio: " + cod_negocio, '300', '175');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    } else {

        mensajesDelSistema("Bebe llenar el campo observacion para continuar.", '250', '150');

    }

}
