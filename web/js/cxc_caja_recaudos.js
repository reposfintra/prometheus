/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    Combocajas();
    ComboAno();
    Combomes();
    Combodia();
    $('#buscar').click(function () {
        if ($("#caja").val()!="" ){
        Cargar_detalle_caja_recaudo();
    }else {
        
            mensajesDelSistema("Por favor seleccione una caja a consultar",'250', '150');
    }
    });
});

function Cargar_detalle_caja_recaudo() {
    var grid_tabla = $("#tabla_detalle_caja_recaudo");
    var anio=$('#anio').val();
    var mes=$('#mes').val();
    var dia=$('#dia').val();
    var fecha=anio+'-'+mes+'-'+dia;
    
    if ($("#gview_tabla_detalle_caja_recaudo").length) {
        reloadGridTabla(grid_tabla,fecha);
    } else {
        
        grid_tabla.jqGrid({
            
           // i.tipo_documento,i.num_ingreso,i.nitcli,c.nomcli,i.fecha_consignacion,
//                   i.fecha_ingreso,i.bank_account_no,i.descripcion_ingreso,i.vlr_ingreso,i.creation_user 
            
            caption: "Cxc caja recaudo",
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '1720',
            colNames: ['Tipo Documento','No Ingreso','Nit Cliente','Cliente','Fecha Consignacion','Fecha Ingreso','Banco','Sucursal','Descripcion','Aplica pago','Valor Ingreso'],
            colModel: [
                {name: 'tipo_documento',        index: 'tipo_documento',        width: 90,sortable: true, align: 'center', hidden: false},
                {name: 'num_ingreso',index: 'num_ingreso',width: 120, sortable: true, align: 'center',   hidden: false},
                {name: 'nitcli',index: 'nitcli',width: 120, sortable: true, align: 'center',   hidden: false},
                {name: 'nomcli',index: 'nomcli',width: 300, sortable: true, align: 'center',   hidden: false},
                {name: 'fecha_consignacion',       index: 'fecha_consignacion',       width: 120, sortable: true, align: 'center',   hidden: false},
                {name: 'fecha_ingreso',       index: 'fecha_ingreso',       width: 120, sortable: true, align: 'center',   hidden: false},
                {name: 'branch_code',       index: 'branch_code',       width: 110, sortable: true, align: 'center',   hidden: false},                
                {name: 'bank_account_no',       index: 'bank_account_no',       width: 110, sortable: true, align: 'center',   hidden: false},                
                {name: 'descripcion_ingreso',       index: 'descripcion_ingreso',       width: 300, sortable: true, align: 'center',   hidden: false},              
                {name: 'creation_user',       index: 'creation_user',       width: 120, sortable: true, align: 'center',   hidden: false},              
                {name: 'vlr_ingreso', index: 'vlr_ingreso', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}}               
                
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#pager',
            pgtext: null,
            pgbuttons: false,
            multiselect:false,           
            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
                
            },
            loadComplete: function () {
                
                if (grid_tabla.jqGrid('getGridParam', 'records') <= 0) {
                                           
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            ajaxGridOptions: {
                
                data: {
                    opcion: 13,
                    caja: $("#caja").val(),
                    fecha: fecha                

                }                
                
            },
            gridComplete: function() { 
                
                var colSumTotal = jQuery("#tabla_detalle_caja_recaudo").jqGrid('getCol', 'vlr_ingreso', false, 'sum'); 
                jQuery("#tabla_detalle_caja_recaudo").jqGrid('footerData', 'set', {vlr_ingreso: colSumTotal});
                             
            }, 
            
           
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
                
            }
        }).navGrid('#pager', {add: false, edit: false, del: false, search: false, refresh: false});
        $("#tabla_detalle_caja_recaudo").navButtonAdd('#pager', {
            caption: "Generar Cxc",
            title: "Generar Cxc",
            onClickButton: function () {
                procesarCxc();
            }
        }); 
       
           
    }
    
}


function reloadGridTabla(grid_tabla, fecha) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",
       
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 13,
                caja: $("#caja").val(),
                fecha: fecha    
            }
        }
    });
    grid_tabla.trigger("reloadGrid");    
     
}


function Combocajas() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 12
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
                $('#caja').html('');
                $('#caja').append('<option value="" >...</option>');
                for (var datos in json) {
                    $('#caja').append('<option value="' + datos + '" >' + json[datos] + '</option>');
                }
           

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function ComboAno(){
   var n = (new Date()).getFullYear();
   var select = document.getElementById("anio");
   for(var i = n; i>=2010; i--)select.options.add(new Option(i,i)); 
};

function Combomes(){
   var dt = new Date();
   var m = dt.getMonth()+1;
   var n = 12;
   var select = document.getElementById("mes");
   for(var i = 1; i<=n; i++)
       if (i<10){
    select.options.add(new Option('0'+i,'0'+i)); 
       }else {       
    select.options.add(new Option(i,i));    
       }
       
      if (m<10){
          
          $("#mes").val('0'+m);
      }else{
          
          $("#mes").val(m);
      }
   
};

function Combodia(){
   var dt = new Date();
   var d = dt.getDate();
   var n = 31;
   var select = document.getElementById("dia");
   for(var i = 1; i<=n; i++)
    if (i<10){
        
    select.options.add(new Option('0'+i,'0'+i)); 
       }else {       
    select.options.add(new Option(i,i));    
       }
     if (d<10){
          
          $("#dia").val('0'+d);
      }else{
          
          $("#dia").val(d);
      }
};

function procesarCxc(){
    var anio=$('#anio').val();
    var mes=$('#mes').val();
    var dia=$('#dia').val();
    var fecha=anio+'-'+mes+'-'+dia;
     
    var jsondocumento =[];
    var filasId =$('#tabla_detalle_caja_recaudo').getGridParam('data');
    if (filasId != ''){
        for (var i = 0; i < filasId.length; i++) {        
            var num_ingreso = filasId[i].num_ingreso;      
            
            var num_ingresos = {};
            num_ingresos ["num_ingreso"] = num_ingreso;           
            jsondocumento.push(num_ingresos);  
            
        }    
        var listcxc = {};
        listcxc ["num_ingresos"] = jsondocumento;       
      
       loading("Espere un momento por favor...", "270", "140");
        setTimeout(function () {
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Negocios&accion=Fintra",
                dataType: 'json',
                data: {
                    opcion: 14,
                    caja : $("#caja").val(),
                    fecha: fecha,
                    listadoDetalle: JSON.stringify(listcxc)
                    
                },
                success: function (json) {             
                    if (!isEmptyJSON(json)) {
                       
                        if (json.respuesta === "OK") {
                            MsjresultadoCxc("Exito al generar cuenta por cobrar No:" +json.numCxC , '250', '150', true);
                            $("#dialogLoading").dialog('close');
                           
                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Error al generar la cuenta por cobrar" , '363', '140');
                        }

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos ocurri� un error al generar CXC!!", '250', '150');

                    }
                },
                error: function (xhr) {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Error al generar Cxc consolidada: " + "\n" +
                             xhr.responseText, '650', '250', true);
                }
            });
        }, 500);
        
    }else{
        if (jQuery("#tabla_cxp_vencidas_aseguradoras").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar el(los) ingreso(s) a procesar!!", '250', '150');
        } else {
            mensajesDelSistema("No hay ingresos para procesar", '250', '150');
        }             
    } 
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                
                $(this).dialog("close");
            }
        }
    });

}

function MsjresultadoCxc(msj, width, height) {
    
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
  
    $("#dialogMsj").dialog({
        dialogClass: 'hide-close',
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
                Cargar_detalle_caja_recaudo();
                
            }
        }
    });

}