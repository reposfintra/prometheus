/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 $(document).ready(function() {  
    $('#unidad_negocio').attr({disabled: false});   
    $('#periodo_preaprob').attr({disabled: false});
    var fechaHoy = new Date(); 
    anioActual = fechaHoy.getFullYear();
    mesActual = (parseInt(fechaHoy.getMonth()+1)<10)?'0'+(fechaHoy.getMonth()+1).toString():fechaHoy.getMonth()+1;   
    $('#periodo_preaprob').val(anioActual.toString()+mesActual.toString());
    cargarUnidadesNegocio();     
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
        
    $("#generar").click(function () {    
        if (!validaUserGeneratePreAprob()){
            mensajesDelSistema("Ud no cuenta con permisos para ejecutar esta acci�n", '250', '150');
            return;
        }
        else if ($('#unidad_negocio').val() === null || $('#unidad_negocio').val() === '') {
            mensajesDelSistema("Debe seleccionar la unidad de negocio", '250', '150');
            return;
        } else {
            generarPreAprobados();
        }       
    });
    
    $("#consultar_preaprobados").click(function () {    
        if ($('#unidad_negocio').val() === null || $('#unidad_negocio').val() === '') {
            mensajesDelSistema("Debe seleccionar la unidad de negocio", '250', '150');
            return;
        } else if($('#periodo_preaprob').val()===''){
            mensajesDelSistema("Debe ingresar el periodo", '250', '150');
            return;
        }else {
          consultarPreAprobados();
        }       
    });


 });
 
 function cargarUnidadesNegocio() {   
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Fintra&accion=Soporte",
                dataType: 'json',
                async:false,
                data: {
                    opcion: 59
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        try {
                            $('#unidad_negocio').append("<option value=''>Seleccione</option>");                  

                            for (var key in json) {
                                $('#unidad_negocio').append('<option value=' + key + '>' + json[key] + '</option>');                       
                            }

                        } catch (exception) {
                            mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                        }

                    } else {

                        mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                    }

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });    
}

function generarPreAprobados() {
    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function(){
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Fintra&accion=Soporte",
                dataType: 'json',
                data: {
                    opcion: 60,
                    unidad_negocio: $('#unidad_negocio').val(),
                    periodo:""
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '250', '180');                          
                            return;
                        }   
                        if(parseInt(json[0].id_unidad_negocio)===0){                           
                            mensajeConfirmAction(json[0].negasoc+'. DESEA VISUALIZARLO?','320','180',consultarPreAprobados);                                                 
                            return;
                        }else{
                            $('#unidad_negocio').attr({disabled: true});
                            $('#periodo_preaprob').attr({disabled: true});
                            cargarPreAprobados(json,'generar');
                            $("#dialogLoading").dialog('close');
                        }    
                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');              

                    }

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
    },500);
}

function consultarPreAprobados() {
    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function(){
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Fintra&accion=Soporte",
                dataType: 'json',
                async:false,
                data: {
                    opcion: 63,
                    unidad_negocio: $('#unidad_negocio').val(),
                    periodo: $('#periodo_preaprob').val()
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '250', '180');                          
                            return;
                        }   
                        $('#unidad_negocio').attr({disabled: true});
                        $('#periodo_preaprob').attr({disabled: true});
                        cargarPreAprobados(json, 'consultar');
                        $("#dialogLoading").dialog('close');
                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se han generado preaprobados para el periodo ingresado!!", '250', '165');              

                    }

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
    },500);
}


function cargarPreAprobados(jsonData, action) {    
    $('#div_preaprobados').fadeIn();
    var grid_tabla_ = jQuery("#tabla_preaprobados");
    if ($("#gview_tabla_preaprobados").length) {
        refrescarGridPreAprobados(jsonData);
    } else {
        grid_tabla_.jqGrid({
            caption: "Listado de PreAprobados",         
            mtype: "POST",
            cellsubmit: "clientArray",
            data: jsonData,         
            datatype: "local",
            height: '350',
            width: '1350',
            colNames: ['Id Unidad', 'Periodo', 'Negocio', 'Cedula Deudor', 'Nombre Deudor', 'Telefono', 'Celular', 'Direccion', 'Barrio', 'Ciudad', 'Email', 'Cedula Codeudor', 'Nombre Codeudor', 'Telefono Codeudor', 'Celular Codeudor', 'Cuotas', 'Id Convenio', 'Afiliado', 'Tipo', 'F. Desembolso', 'F. ult. Pago', 'Dia ult. pago', 'Vr. Negocio', 'Vr. Factura', 'Vr. Saldo', 'Porcentaje', 'Altura Mora', 'Vr. PreAprobado','Responsable cuenta'],
            colModel: [
                {name: 'id_unidad_negocio', index: 'id_unidad_negocio', width: 90, sortable: true, hidden:true, align: 'left'},
                {name: 'periodo', index: 'periodo', width: 90, sortable: true, align: 'center'},
                {name: 'negasoc', index: 'negasoc', width: 90, sortable: true, align: 'center', key: true},
                {name: 'cedula_deudor', index: 'cedula_deudor', width: 110, sortable: true, align: 'center'},
                {name: 'nombre_deudor', index: 'nombre_deudor', width: 180, sortable: true, align: 'left'},               
                {name: 'telefono', index: 'telefono', width: 100, sortable: true, align: 'center'},
                {name: 'celular', index: 'celular', width: 100, sortable: true, align: 'center'},
                {name: 'direccion', index: 'direccion', width: 130, sortable: true, align: 'left'},
                {name: 'barrio', index: 'barrio', width: 110, sortable: true, align: 'left'},
                {name: 'ciudad', index: 'ciudad', width: 110, sortable: true, align: 'center'},
                {name: 'email', index: 'email', width: 110, sortable: true, align: 'left'},
                {name: 'cedula_codeudor', index: 'cedula_codeudor', width: 110, sortable: true, align: 'center'},
                {name: 'nombre_codeudor', index: 'nombre_codeudor', width: 180, sortable: true, align: 'left'},
                {name: 'telefono_codeudor', index: 'telefono_codeudor', width: 100, sortable: true, align: 'center'},
                {name: 'celular_codeudor', index: 'celular_codeudor', width: 100, sortable: true, align: 'center'},                
                {name: 'cuotas', index: 'cuotas', width: 80, sortable: true, align: 'center'},
                {name: 'id_convenio', index: 'id_convenio', width: 80, sortable: true, align: 'center'},
                {name: 'afiliado', index: 'afiliado', width: 160, sortable: true, align: 'left'},
                {name: 'tipo', index: 'tipo', width: 110, sortable: true, align: 'left'},
                {name: 'fecha_desembolso', index: 'fecha_desembolso', width: 110, sortable: true, align: 'left'},
                {name: 'fecha_ult_pago', index: 'fecha_ult_pago', width: 110, sortable: true, align: 'left'},
                {name: 'dias_pagos', index: 'dias_pagos', width: 110, sortable: true, align: 'left'},
                {name: 'vr_negocio', index: 'vr_negocio', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_factura', index: 'valor_factura', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_saldo', index: 'valor_saldo', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},                   
                {name: 'porcentaje', index: 'porcentaje', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'altura_mora', index: 'altura_mora', width: 110, sortable: true, align: 'left'},
                {name: 'valor_preaprobado', index: 'valor_preaprobado', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                 {name: 'responsable_cuenta', index: 'responsable_cuenta', width: 110, sortable: true, align: 'left'},
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 5000,
            rowTotal: 5000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#page_tabla_preaprobados',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }
        }).navGrid("#page_tabla_preaprobados", {search:false,refresh:false, edit: false, add: false, del: false});
        if (action === 'generar'){
            jQuery("#tabla_preaprobados").jqGrid("navButtonAdd", "#page_tabla_preaprobados", {
                caption: "Guardar",
                title: "Guardar PreAprobados",
                onClickButton: function () {
                    var numRecords = grid_tabla_.getGridParam('records');
                    if (numRecords > 0) {
                        guardarPreAprobados();
                    } else {
                        mensajesDelSistema("No hay informacion para guardar ", '250', '150', false);
                    }
                }
            });
        }  
        $("#tabla_preaprobados").navButtonAdd('#page_tabla_preaprobados', {
            caption: "Limpiar",
            onClickButton: function() {
                var numRecords = grid_tabla_.getGridParam('records');
                if (numRecords > 0) {    
                   $('#unidad_negocio').attr({disabled: false});
                   $('#periodo_preaprob').attr({disabled: false});
                   $('#unidad_negocio').val('');
                   $('#periodo_preaprob').val(anioActual.toString()+mesActual.toString());                   
                   jQuery("#tabla_preaprobados").jqGrid("clearGridData", true);
                   $('#div_preaprobados').fadeOut();
                   $("#tabla_preaprobados").jqGrid('GridUnload');
                } else {
                    mensajesDelSistema("No hay informacion que limpiar", '250', '150', false);
                }
            }
        });
        $("#tabla_preaprobados").jqGrid("navButtonAdd", "#page_tabla_preaprobados", {
            caption: "Exportar excel",
            onClickButton: function () {
                var info = jQuery('#tabla_preaprobados').getGridParam('records');
                if (info > 0) {
                    exportarExcelPreAprobados();
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }
            }
        });
    }
}

function refrescarGridPreAprobados(data){   
    jQuery("#tabla_preaprobados").setGridParam({
        data: data,
        datatype: "local"
    });
    
    jQuery('#tabla_preaprobados').trigger("reloadGrid");
}

function  guardarPreAprobados() {
    var id_und_negocio = parseInt($('#unidad_negocio').val());  
    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function(){       
            $.ajax({
                async: false,
                url: "./controller?estado=Fintra&accion=Soporte",
                type: 'POST',
                data: {
                    opcion: 61,
                    unidad_negocio: id_und_negocio,
                    listadoDocs: guardarPreAprobadosToJSON()
                },
                success: function(json) {
                    if (json.respuesta === 'OK') {                      
                        jQuery("#tabla_preaprobados").jqGrid("clearGridData", true);
                        $('#div_preaprobados').fadeOut();
                        $("#tabla_preaprobados").jqGrid('GridUnload');
                      
                        $('#unidad_negocio').attr({disabled: false});
                        $('#periodo_preaprob').attr({disabled: false});                       
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("PREAPROBADOS ALMACENADOS SATISFACTORIAMENTE PARA LA UNIDAD DE NEGOCIO:  " + $('#unidad_negocio option:selected').text(), '280', '175', true);
                        $('#unidad_negocio').val('');
                        $('#periodo_preaprob').val(anioActual.toString()+mesActual.toString());
                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema(json.message, '250', '150', false);
                        return;
                    }
                }, error: function(result) {
                    $("#dialogLoading").dialog('close');
                    alert('Ocurri� un error al realizar la operaci�n. Por favor, intente m�s tarde');
                }
            });             
    },500);
}

function guardarPreAprobadosToJSON(){

       var jsonObj = {"preaprobados": []};
                  
        var filasId = jQuery('#tabla_preaprobados').jqGrid('getDataIDs');       
        if (filasId != '') {
            for (var i = 0; i < filasId.length; i++) {                 
                    var negocio = filasId[i];
                    var periodo = $("#tabla_preaprobados").getRowData(filasId[i]).periodo;
                    var cedula_deudor = $("#tabla_preaprobados").getRowData(filasId[i]).cedula_deudor;
                    var nombre_deudor = $("#tabla_preaprobados").getRowData(filasId[i]).nombre_deudor;  
                    var telefono = $("#tabla_preaprobados").getRowData(filasId[i]).telefono;
                    var celular = $("#tabla_preaprobados").getRowData(filasId[i]).celular;
                    var direccion = $("#tabla_preaprobados").getRowData(filasId[i]).direccion;
                    var barrio = $("#tabla_preaprobados").getRowData(filasId[i]).barrio;
                    var ciudad = $("#tabla_preaprobados").getRowData(filasId[i]).ciudad;
                    var email = $("#tabla_preaprobados").getRowData(filasId[i]).email;
                    var cedula_codeudor = $("#tabla_preaprobados").getRowData(filasId[i]).cedula_codeudor;                    
                    var nombre_codeudor = $("#tabla_preaprobados").getRowData(filasId[i]).nombre_codeudor;
                    var telefono_codeudor = $("#tabla_preaprobados").getRowData(filasId[i]).telefono_codeudor;
                    var celular_codeudor = $("#tabla_preaprobados").getRowData(filasId[i]).celular_codeudor;                       
                    var cuotas = $("#tabla_preaprobados").getRowData(filasId[i]).cuotas;
                    var id_convenio = $("#tabla_preaprobados").getRowData(filasId[i]).id_convenio;
                    var afiliado = $("#tabla_preaprobados").getRowData(filasId[i]).afiliado;
                    var tipo = $("#tabla_preaprobados").getRowData(filasId[i]).tipo;
                    var fecha_desembolso = $("#tabla_preaprobados").getRowData(filasId[i]).fecha_desembolso;
                    var fecha_ult_pago = $("#tabla_preaprobados").getRowData(filasId[i]).fecha_ult_pago;
                    var dias_pagos = $("#tabla_preaprobados").getRowData(filasId[i]).dias_pagos;
                    var vr_negocio = $("#tabla_preaprobados").getRowData(filasId[i]).vr_negocio;
                    var valor_factura = $("#tabla_preaprobados").getRowData(filasId[i]).valor_factura;
                    var valor_saldo = $("#tabla_preaprobados").getRowData(filasId[i]).valor_saldo;
                    var porcentaje = $("#tabla_preaprobados").getRowData(filasId[i]).porcentaje;
                    var altura_mora = $("#tabla_preaprobados").getRowData(filasId[i]).altura_mora;
                    var valor_preaprobado = $("#tabla_preaprobados").getRowData(filasId[i]).valor_preaprobado;
                      
                    var item = {};
                    item ["negocio"] = negocio;
                    item ["periodo"] = periodo;
                    item ["cedula_deudor"] = cedula_deudor;
                    item ["nombre_deudor"] = nombre_deudor;                
                    item ["telefono"] = telefono;                    
                    item ["celular"] = celular;
                    item ["direccion"] = direccion;
                    item ["barrio"] = barrio;
                    item ["ciudad"] = ciudad;
                    item ["email"] = email;
                    item ["cedula_codeudor"] = cedula_codeudor;
                    item ["nombre_codeudor"] = nombre_codeudor;
                    item ["telefono_codeudor"] = telefono_codeudor;
                    item ["celular_codeudor"] = celular_codeudor;                        
                    item ["cuotas"] = cuotas;                    
                    item ["id_convenio"] = id_convenio;
                    item ["afiliado"] = afiliado;
                    item ["tipo"] = tipo;
                    item ["fecha_desembolso"] = fecha_desembolso;
                    item ["fecha_ult_pago"] = fecha_ult_pago;
                    item ["dias_pagos"] = dias_pagos;
                    item ["vr_negocio"] = parseFloat(vr_negocio);
                    item ["valor_factura"] = parseFloat(valor_factura);                      
                    item ["valor_saldo"] = parseFloat(valor_saldo);
                    item ["porcentaje"] = parseFloat(porcentaje);
                    item ["altura_mora"] = altura_mora;
                    item ["valor_preaprobado"] = parseFloat(valor_preaprobado);  
                                 
                    jsonObj.preaprobados.push(item);                              
            }  
        }
        return JSON.stringify(jsonObj); 
     
}

function  exportarExcelPreAprobados() {
    var fullData = jQuery("#tabla_preaprobados").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: './controller?estado=Fintra&accion=Soporte',
        data: {
            listado: myJsonString,
            opcion: 62
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function validaUserGeneratePreAprob(){
    var estado = false;
    $.ajax({
        url: './controller?estado=Fintra&accion=Soporte',
        datatype:'json',
        type:'post',
        async:false,
        data:{
            opcion: 64
        },          
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.respuesta === "SI") {               
                   estado = true;                  
                }
                
            }              
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }  
    });
    return estado;
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajeConfirmAction(msj, width, height, okAction, id) { 
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $("#dialogLoading").dialog('close');
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: "Mensaje",
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("close");             
            }
        }
    });
}


