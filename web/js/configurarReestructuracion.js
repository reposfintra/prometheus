/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$('#capaCentral').ready(function() {
    listarActualConfig();  
});

function obtenerPeriodoActual() {

    var fechaActual = new Date();
    var month = fechaActual.getMonth() + 1;
    var year = fechaActual.getFullYear();

    if (month < 10) {
        month = '0' + month;
    }

    return year + month;

}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    mostrarContenido('dialogMsj');
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear botón de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
                if (typeof swHideDialog !== 'undefined') {
                    $('#dialogo').dialog("close");
                }
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function listarActualConfig() {
    var grid_listar_config_desc = $("#tabla_config_descuentos");

    if ($("#gview_tabla_config_descuentos").length) {
        reloadGridListarActualConfig(grid_listar_config_desc);
    } else {

        grid_listar_config_desc.jqGrid({
            caption: "Configuración de descuentos",
            url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=19",
            cellsubmit: "clientArray",
            mtype: "POST",
            datatype: "json",
            height: $("#gview_tabla_config_descuentos").height(),
            width: '525',
            cellEdit: true,
            colNames: ['Id', 'Tipo Negocio', 'IdConcepto', 'Concepto', '% max descuento', '% Cta Inicial', 'Aplica % cta'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, width: 80, sortable: true, align: 'center', key: true},
                {name: 'tipo_negocio', index: 'tipo_negocio', width: 80, sortable: true, align: 'center'},
                {name: 'concepto', index: 'concepto', hidden: true, width: 80, align: 'center'},
                {name: 'descripcion', index: 'descripcion', sortable: true, width: 120, align: 'center'},
                {name: 'descuento', index: 'descuento', editable: true, width: 100, align: 'center',
                    formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'porcentaje_cta_inicial', index: 'porcentaje_cta_inicial', editable: true, width: 100, align: 'center',
                    formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'aplica_inicial', index: 'aplica_inicial', width: 100, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            afterSaveCell: function(rowid, celname, value, iRow, iCol) {                
                if (celname === 'descuento' || celname === 'porcentaje_cta_inicial') {
                    var tiponeg = $("#tabla_config_descuentos").getRowData(rowid).tipo_negocio;
                    var descuento = $("#tabla_config_descuentos").getRowData(rowid).descuento;
                    var cuotaporc = $("#tabla_config_descuentos").getRowData(rowid).porcentaje_cta_inicial;
                    var tipocelda;
                    (celname === 'descuento') ? tipocelda = "desc" : tipocelda = "cini";
                    if ((value < 0 || value > 100)) {

                        var msj = "";
                        if (celname === 'descuento') {
                            msj = "El porcentaje de descuento debe estar comprendido entre 0 y 100";
                        } else {
                            msj = "El porcentaje de la cuota debe estar comprendido entre 0 y 100";
                        }
                        mensajesDelSistema(msj, 250, 150);
                        $("#tabla_config_descuentos").jqGrid("restoreCell", iRow, iCol);
                        return;

                    } else {
                        (isNaN(value)) ? $("#tabla_config_descuentos").jqGrid("restoreCell", iRow, iCol) : actualizarActualConfig(rowid, tiponeg, descuento, cuotaporc, tipocelda);
                    }
                }
            },
            loadComplete: function () {
                if (grid_listar_config_desc.jqGrid('getGridParam', 'records') >= 0) {
                  listarConfigPagoInicial();
                }
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });

    }

}

function reloadGridListarActualConfig(grid_listar_config_desc) {
    grid_listar_config_desc.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=19"
    });
    grid_listar_config_desc.trigger("reloadGrid");
}

function actualizarActualConfig(id, tiponeg, descuento, cuotaporc, tipo) {

    $.ajax({
        async: false,
        url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=20",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 20,
            id: id,
            tiponeg: tiponeg,
            descuento: descuento,
            cta_ini: cuotaporc,
            tipo: tipo

        },
        success: function(json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    if (tipo === "cini") {
                        reloadGridListarActualConfig($("#tabla_config_descuentos"));
                    }
                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo efectuar la operación!!", '250', '150');
            }

        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function listarConfigPagoInicial() {
    var grid_listar_config_pago_ini = $("#tabla_config_pago_inicial");

    if ($("#gview_tabla_config_pago_inicial").length) {
        reloadGridListarConfigPagoInicial(grid_listar_config_pago_ini);
    } else {

        grid_listar_config_pago_ini.jqGrid({
            caption: "Configuración de pago inicial",
            url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=21",
            cellsubmit: "clientArray",
            mtype: "POST",
            datatype: "json",
            height: $("#gview_tabla_config_pago_inicial").height(),
            width: '535',
            /*cellEdit: true,*/
            colNames: ['Id', 'Tipo Negocio', 'Pago capital', 'Pago interes', 'Pago intxmora', 'Pago gac', 'Pago total'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, width: 80, sortable: true, align: 'center', key: true},
                {name: 'tipo_negocio', index: 'tipo_negocio', width: 80, sortable: true, align: 'center'},
                {name: 'incluyeCapital', index: 'incluyeCapital', sortable: true, width: 85, align: 'center'},
                {name: 'incluyeInteres', index: 'incluyeInteres', sortable: true, width: 85, align: 'center'},
                {name: 'incluyeIntxmora', index: 'incluyeIntxmora', sortable: true, width: 85, align: 'center'},
                {name: 'incluyeGac', index: 'incluyeGac', sortable: true, width: 85, align: 'center'},
                {name: 'pagoTotal', index: 'pagoTotal', sortable: true, width: 85, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });

    }

}

function reloadGridListarConfigPagoInicial(grid_listar_config_pago_ini) {
    grid_listar_config_pago_ini.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=21"
    });
    grid_listar_config_pago_ini.trigger("reloadGrid");
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}