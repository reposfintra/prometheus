/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//var visual = true;
$(document).ready(function () {
    $("#f_consignacion").val('');
    $("#f_consignacion").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $('#ui-datepicker-div').css('clip', 'auto');

    cargarIngresoDetalle();
    cargarConcepto();
    cargarTipoIngreso();
    cargarBanco();
    cargarMoneda();
    cargarHc();
    $('#agregar_items').click(function () {
        ventanaAddFactura();
    });

    $('#buscar').click(function () {
        cargarFacturas();
    });
    $('#buscar_cliente').click(function () {
        ventanaCliente('ingreso');
    });
    $('#buscar_cliente_').click(function () {
        ventanaCliente('factura');
    });

});

function onKeyDecimal(e, num) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if (document.getElementById(num.id).value.indexOf('.') !== -1 && keynum === 46)
        return false;
    if ((keynum === 0 || keynum === 8 || keynum === 48 || keynum === 46)) {
        return true;
    }

    if (keynum >= 58) {
        return false;

    }

    return /\d/.test(String.fromCharCode(keynum));
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function numberConComas2(x) {
    console.log(x.value);
    var valorConformato = x.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    $('#' + x.id).val(valorConformato);
    console.log(valorConformato);
}

function cargarIngresoCabecera() {
    var grid_tabla = $("#tabla_ingreso_cabecera");
    if ($("#gview_tabla_ingreso_cabecera").length) {
        reloadGridtabla(grid_tabla, 0);
    } else {
        grid_tabla.jqGrid({
            //caption: "Documento cabecera",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '80',
            width: '1503',
            async: false,
            colNames: ['Id', 'Numero Documento', 'Tipo Documento', 'Codigo Cliente', 'Nit', 'Concepto', 'Tipo Ingreso', 'Fecha Consignacion', 'Fecha Ingreso', 'Banco', 'Sucursal', 'Descripcion', 'Valor', 'Cantidad Item'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', hidden: true, key: true},
                {name: 'num_ingreso', index: 'num_ingreso', width: 90, align: 'left', hidden: true},
                {name: 'tipo_documento', index: 'tipo_documento', width: 120, align: 'center', hidden: false},
                {name: 'codcli', index: 'codcli', width: 100, align: 'left', hidden: false},
                {name: 'nitcli', index: 'nitcli', width: 120, align: 'center', hidden: false},
                {name: 'concepto', index: 'concepto', width: 120, align: 'center', hidden: false},
                {name: 'tipo_ingreso', index: 'tipo_ingreso', width: 120, align: 'center', hidden: false},
                {name: 'fecha_consignacion', index: 'fecha_consignacion', width: 130, align: 'center', sortable: false, search: false, editable: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'fecha_ingreso', index: 'fecha_ingreso', width: 130, align: 'center', sortable: false, search: false, editable: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'banco', index: 'banco', width: 120, align: 'center', hidden: false},
                {name: 'sucursal', index: 'sucursal', width: 120, align: 'center', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 300, align: 'left', hidden: false, editable: true, classes: ''},
                {name: 'valor_ingreso', index: 'valor_ingreso', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cant_item', index: 'cant_item', width: 120, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: false,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            //pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            editurl: 'clientArray',
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function (rowid) {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    var f = new Date();
                    var fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
                    var grid = $("#tabla_ingreso_cabecera");
                    var rowid = 'neo_' + grid.getRowData().length;
                    var defaultData = {fecha_ingreso: fecha};
                    grid.addRowData(rowid, defaultData);
                }

            },
            gridComplete: function (rowid) {
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            },
            ajaxGridOptions: {
                data: {
                    opcion: 0
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}

function reloadGridtabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function cargarIngresoDetalle() {
    var acciones = new funciones();
    //  $('#tabla_ingreso_detalle').jqGrid('GridUnload');
    var grid_tabla = $("#tabla_ingreso_detalle");
    if ($("#gview_tabla_ingreso_detalle").length) {
        reloadGridtabla_detalle(grid_tabla, 0);
    } else {
        grid_tabla.jqGrid({
            //caption: "Documento cabecera",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '450',
            width: '1620',
            async: false,
            colNames: ['Id', 'Item', 'Nit', 'Factura', 'Fecha Vencimiento', 'Descripcion', 'Cuenta', 'Valor Factura', 'Valor Abono', 'Valor Saldo', 'Valor Pago'],
            colModel: [
                {name: 'documento_', index: 'documento_', width: 80, align: 'left', hidden: true, key: true},
                {name: 'item', index: 'item', width: 80, align: 'center', hidden: false},
                {name: 'nitcli', index: 'nitcli', width: 120, align: 'left', hidden: false},
                {name: 'documento', index: 'documento', width: 120, align: 'center', hidden: false},
                {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 130, align: 'center', sortable: false, search: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'descripcion', index: 'descripcion', width: 300, align: 'left', hidden: false, editable: false, classes: ''},
                {name: 'cuenta', index: 'cuenta', width: 120, align: 'center', hidden: false},
                {name: 'valor_factura', index: 'valor_factura', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_abono', index: 'valor_abono', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_saldo', index: 'valor_saldo', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_pago', index: 'valor_pago', editable: true, sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: false,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager1',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            editurl: 'clientArray',
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function (rowid) {
                var info = grid_tabla.getGridParam('records');
                var grid = $("#tabla_ingreso_detalle");
                var item_ = 0;
                if (info === 0) {
                    var rowid = 'neo_' + grid.getRowData().length;
                    var defaultData = acciones.addFactura();
                    grid.addRowData(rowid, defaultData);
                } else if (info > 0) {
                    var defaultData = acciones.addFactura();
                    grid.addRowData(rowid, defaultData);
                }
                var ids = grid_tabla.jqGrid('getDataIDs');
                var allRowsInGrid = grid_tabla.jqGrid('getRowData');
                for (var i = 0; i < allRowsInGrid.length; i++) {
                    var cl = ids[i];
                    item_ = item_ + 1;
                    if (item_ > 9) {
                        jQuery("#tabla_ingreso_detalle").jqGrid('setCell', cl, 'item', item_);
                    } else {
                        jQuery("#tabla_ingreso_detalle").jqGrid('setCell', cl, 'item', '0' + item_);
                    }
                }

                acciones.sumaFooterIngresoDetalle();
            },
            gridComplete: function (rowid) {

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                jQuery("#tabla_ingreso_detalle").jqGrid('editRow', rowid, true, function () {
                }, null, null, {}, function (rowid) {

                });
                return;
            },
            onSelectRow: function (id) {
            },
            beforeSelectRow: function (rowid, e) {

            },
            ajaxGridOptions: {
                data: {
                    opcion: 0
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        jQuery("#tabla_ingreso_detalle").navButtonAdd('#pager1', {
            caption: "Eliminar",
            title: "Eliminar ",
            onClickButton: function () {
                var infoid = jQuery('#tabla_ingreso_detalle').jqGrid('getGridParam', 'selarrrow');
                var longitud = infoid.length;
                var item_ = 0;
                if (infoid.length > 0) {
                    for (var i = 0; i < longitud; i++) {
                        var selRowIds = grid_tabla.jqGrid("getGridParam", "selrow");
                        $('#tabla_ingreso_detalle').jqGrid('delRowData', selRowIds);
                    }
                    acciones.sumaFooterIngresoDetalle();
                    var ids = grid_tabla.jqGrid('getDataIDs');
                    var allRowsInGrid = grid_tabla.jqGrid('getRowData');
                    for (var i = 0; i < allRowsInGrid.length; i++) {
                        var cl = ids[i];
                        item_ = item_ + 1;
                        if (item_ > 9) {
                            jQuery("#tabla_ingreso_detalle").jqGrid('setCell', cl, 'item', item_);
                        } else {
                            jQuery("#tabla_ingreso_detalle").jqGrid('setCell', cl, 'item', '0' + item_);
                        }
                    }
                } else {
                    mensajesDelSistema("Debe seleccionar fila", '314', '140', false);
                }

            }
        });
    }
}

function reloadGridtabla_detalle(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height) {
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function ventanaAddFactura() {
    $("#dialogMsjAddFacturas").dialog({
        width: '1298',
        height: '700',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Agregar Facturas',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Agregar": function () {
                cargarIngresoDetalle();
            },
            "Salir": function () {
                $('#tabla_facturas').jqGrid('GridUnload');
                $('#cliente_').val('');
                $(this).dialog("close");
            }
        }
    });
}

function cargarFacturas() {
    var acciones = new funciones();
    var grid_tabla = $("#tabla_facturas");
    if ($("#gview_tabla_facturas").length) {
        reloadGridFactura(grid_tabla, 60);
    } else {
        grid_tabla.jqGrid({
            //caption: "Documento cabecera",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '450',
            width: '1270',
            async: true,
            colNames: ['Nit Cliente', 'Negocio', 'Cuota', 'Factura', 'Fecha Vencimiento', 'Descripcion', 'Valor Factura', 'Valor Abono', 'Valor Saldo', 'Hc'],
            colModel: [
                {name: 'nitcli', index: 'nitcli', width: 120, align: 'center', hidden: true},
                {name: 'negocio', index: 'negocio', width: 80, align: 'left', hidden: false},
                {name: 'cuota', index: 'cuota', width: 80, align: 'center', hidden: false},
                {name: 'documento', index: 'documento', width: 90, align: 'left', hidden: false, key: true},
                {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 120, align: 'center', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 300, align: 'left', hidden: false, editable: true, classes: ''},
                {name: 'valor_factura', index: 'valor_factura', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_abono', index: 'valor_abono', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_saldo', index: 'valor_saldo', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cuenta', index: 'cuenta', width: 180, align: 'left', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: false,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager2',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            editurl: 'clientArray',
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function (rowid) {
            },
            gridComplete: function (rowid) {
                acciones.sumaFooterFactura();
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

            },
            onSelectRow: function (rowid) {
                acciones.sumaUnoaUnoFactura(rowid);
            },
            beforeSelectRow: function (rowid, e) {

            },
            onSelectAll: function (rowid) {
                acciones.sumaTotalFacturaCheck();
            },
            ajaxGridOptions: {
                data: {
                    opcion: 60,
                    nit: $('#cliente_').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}

function reloadGridFactura(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                nit: $('#cliente_').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

sumaTotalFactura = 0;
sumaTotalAbono = 0;
sumaTotalSaldo = 0;
sumaTotalFacturaT = 0;
sumaTotalAbonoT = 0;
sumaTotalSaldoT = 0;
function funciones() {
    return {
        addFactura: function () {
            var infoid = jQuery('#tabla_facturas').jqGrid('getGridParam', 'selarrrow');
            var tabla = jQuery("#tabla_facturas");
            var json = [];
            console.log(infoid);
            if (typeof (infoid) !== "undefined") {
                if (infoid.length > 0) {
                    for (var i = 0; i < infoid.length; i++) {
                        var llave = {};
                        var id = infoid[i];
                        var nitcli = tabla.getRowData(id).nitcli;
                        var negocio = tabla.getRowData(id).negocio;
                        var cuota = tabla.getRowData(id).cuota;
                        var documento = tabla.getRowData(id).documento;
                        var fecha_vencimiento = tabla.getRowData(id).fecha_vencimiento;
                        var descripcion = tabla.getRowData(id).descripcion;
                        var valor_factura = tabla.getRowData(id).valor_factura;
                        var valor_abono = tabla.getRowData(id).valor_abono;
                        var valor_saldo = tabla.getRowData(id).valor_saldo;
                        var moneda_factura = tabla.getRowData(id).moneda_factura;
                        var valor_tasa = tabla.getRowData(id).valor_tasa;
                        var cuenta = tabla.getRowData(id).cuenta;
                        llave['nitcli'] = nitcli;
                        llave['negocio'] = negocio;
                        llave['cuota'] = cuota;
                        llave['documento'] = documento;
                        llave['fecha_vencimiento'] = fecha_vencimiento;
                        llave['descripcion'] = descripcion;
                        llave['valor_factura'] = valor_factura;
                        llave['valor_abono'] = valor_abono;
                        llave['valor_saldo'] = valor_saldo;
                        llave['moneda_factura'] = moneda_factura;
                        llave['valor_tasa'] = valor_tasa;
                        llave['cuenta'] = cuenta;
                        json.push(llave);
                    }
                }
            }
            return json;
        },
        sumaFooterIngresoDetalle: function () {
            var grid_tabla = $("#tabla_ingreso_detalle");
            var valor_factura = grid_tabla.jqGrid('getCol', 'valor_factura', false, 'sum');
            var valor_abono = grid_tabla.jqGrid('getCol', 'valor_abono', false, 'sum');
            var valor_saldo = grid_tabla.jqGrid('getCol', 'valor_saldo', false, 'sum');
            var valor_pago = grid_tabla.jqGrid('getCol', 'valor_pago', false, 'sum');
            grid_tabla.jqGrid('footerData', 'set', {cuenta: 'TOTAL', valor_factura: valor_factura, valor_abono: valor_abono, valor_saldo: valor_saldo, valor_pago: valor_pago});
        },
        sumaFooterFactura: function () {
            var grid_tabla = $("#tabla_facturas");
            var valor_factura = grid_tabla.jqGrid('getCol', 'valor_factura', false, 'sum');
            var valor_abono = grid_tabla.jqGrid('getCol', 'valor_abono', false, 'sum');
            var valor_saldo = grid_tabla.jqGrid('getCol', 'valor_saldo', false, 'sum');
            grid_tabla.jqGrid('footerData', 'set', {descripcion: 'TOTAL', valor_factura: valor_factura, valor_abono: valor_abono, valor_saldo: valor_saldo});
        },
        sumaUnoaUnoFactura: function (id) {
            var valorFactura = jQuery('#tabla_facturas').getCell(id, 'valor_factura');
            var valorAbono = jQuery('#tabla_facturas').getCell(id, 'valor_abono');
            var valorSaldo = jQuery('#tabla_facturas').getCell(id, 'valor_saldo');
            var isChecked = (jQuery("#jqg_tabla_facturas_" + id).attr('checked'));

            if (isChecked == true) {
                sumaTotalFactura = parseFloat(sumaTotalFactura) + parseFloat(valorFactura);
                sumaTotalAbono = parseFloat(sumaTotalAbono) + parseFloat(valorAbono);
                sumaTotalSaldo = parseFloat(sumaTotalSaldo) + parseFloat(valorSaldo);
            } else {
                if (sumaTotalSaldoT > 0 && sumaTotalSaldo == 0) {
                    sumaTotalFactura = parseFloat(sumaTotalFacturaT) - parseFloat(valorFactura);
                    sumaTotalAbono = parseFloat(sumaTotalAbonoT) - parseFloat(valorAbono);
                    sumaTotalSaldo = parseFloat(sumaTotalSaldoT) - parseFloat(valorSaldo);
                } else {
                    sumaTotalFactura = parseFloat(sumaTotalFactura) - parseFloat(valorFactura);
                    sumaTotalAbono = parseFloat(sumaTotalAbono) - parseFloat(valorAbono);
                    sumaTotalSaldo = parseFloat(sumaTotalSaldo) - parseFloat(valorSaldo);
                }
            }
            console.log('valorSaldo: ' + valorSaldo + ' ' + 'isChecked: ' + isChecked + ' ' + ' sumaTotal:' + sumaTotalSaldo);
            //jQuery('#tabla_facturas').jqGrid('footerData', 'set', {descripcion: 'Total', valor_saldo: sumaTotalSaldo});
        },
        sumaTotalFacturaCheck: function () {
            var infoid = jQuery('#tabla_facturas').jqGrid('getGridParam', 'selarrrow');
            if (infoid.length > 0) {
                for (var i = 0; i < infoid.length; i++) {
                    var valorFactura = jQuery('#tabla_facturas').getCell(infoid[i], 'valor_factura');
                    var valorAbono = jQuery('#tabla_facturas').getCell(infoid[i], 'valor_abono');
                    var valorSaldo = jQuery('#tabla_facturas').getCell(infoid[i], 'valor_saldo');
                    var isChecked = (jQuery("#jqg_tabla_facturas_" + infoid[i]).attr('checked'));
                    if (isChecked == true) {
                        sumaTotalFacturaT = parseFloat(sumaTotalFacturaT) + parseFloat(valorFactura);
                        sumaTotalAbonoT = parseFloat(sumaTotalAbonoT) + parseFloat(valorAbono);
                        sumaTotalSaldoT = parseFloat(sumaTotalSaldoT) + parseFloat(valorSaldo);
                    } else {
                        sumaTotalFacturaT = parseFloat(sumaTotalFacturaT) - parseFloat(valorFactura);
                        sumaTotalAbonoT = parseFloat(sumaTotalAbonoT) - parseFloat(valorAbono);
                        sumaTotalSaldoT = parseFloat(sumaTotalSaldoT) - parseFloat(valorSaldo);
                    }
                    console.log('valorSaldo: ' + valorSaldo + ' ' + 'isChecked: ' + isChecked + ' ' + ' sumaTotal:' + sumaTotalSaldoT);
                    //jQuery('#tabla_facturas').jqGrid('footerData', 'set', {descripcion: 'Total', valor_saldo: sumaTotalSaldo});
                }
            } else {
                sumaTotalFacturaT = 0;
                sumaTotalAbonoT = 0;
                sumaTotalSaldoT = 0;
            }
        }
    };
}

function ventanaCliente(concepto) {
    autocompletarCliente(concepto);
    $("#dialogMsjCliente").dialog({
        width: '585',
        height: '200',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Buscar Cliente',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {"Salir": function () {
                $('#cliente_busqueda').val('');
                $(this).dialog("close");
            }
        }
    });
}

function autocompletarCliente(concepto) {
    $("#cliente_busqueda").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST', url: "./controller?estado=Admin&accion=Fintra",
                dataType: "json",
                data: {
                    informacion_: request.term,
                    opcion: 61,
                    busqueda: $('#tipo_busqueda_').val()
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.value,
                            mivar1: item.mivar1
                        };
                    }));
                }
            });
        },
        minLength: 3,
        delay: 500,
        disabled: false,
        select: function (event, ui) {
            if (concepto === 'ingreso') {
                $("#cliente").val(ui.item.mivar1);
            } else if (concepto === 'factura') {
                $("#cliente_").val(ui.item.value);
            }

            console.log(ui.item ?
                    "Selected: " + ui.item.mivar1 :
                    "Nothing selected, input was " + ui.item.label);
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

// $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarConcepto() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: "json",
        async: false,
        data: {
            opcion: 59
        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#concepto').html('');
                $('#concepto').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#concepto').append('<option value="' + datos + '">' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTipoIngreso() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: "json",
        async: false,
        data: {
            opcion: 62
        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#tipo_ingreso').html('');
                $('#tipo_ingreso').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#tipo_ingreso').append('<option value="' + datos + '">' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarHc() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: "json",
        async: false,
        data: {
            opcion: 63
        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#hc').html('');
                $('#hc').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#hc').append('<option value="' + datos + '">' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarBanco() {
    $.ajax({
        type: 'get',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 29
        },
        success: function (json) {
            try {
                $('#banco').html('');
                $('#banco').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#banco').append('<option value="' + datos + '">' + json[datos] + '</option>');
                }
            } catch (exception) {
                mensajesDelSistema('error : ', '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarSucursal() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 30,
            banco: $('#banco').val()
        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#sucursal').html('');
                $('#sucursal').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#sucursal').append('<option value="' + datos + '">' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarMoneda() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 64
        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#moneda').html('');
                $('#moneda').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#moneda').append('<option value="' + datos + '">' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mostrarCampos() {
    var tipo_ingreso = $('#tipo_ingreso').val();
    if (tipo_ingreso === 'ICA') {
        $('#labelcuenta').show();
        $('#cuenta').show();
        $('#labelhc').show();
        $('#hc').show();
        $('#titulo').attr("colspan", '12');
    } else {
        $('#labelcuenta').hide();
        $('#cuenta').hide();
        $('#labelhc').hide();
        $('#hc').hide();
        $('#titulo').attr("colspan", '10');
    }
    console.log(tipo_ingreso);
}