function ConfirmCreaHRutaManual(){
   var frm = document.frmplamanual;
   var val = frm.confirm[0].checked;
   if (val){
      window.location = "controller?estado=HRutaManual&accion=GetInfo";
   }
   else{
      window.location = "controller?estado=HReporte&accion=Search";
   }
}

//VALIDA EL FORMULARIO DE PANTALLAHOJARUTAGETINFO.JSP


function validarfrmpla(){
   var frm = document.frmpla;
   var pla = frm.planilla.value;
   if (pla == ""){
      alert("DIGITE UN NUMERO DE PLANILLA");
      return false;
   }
   frm.submit();
   return true;
}

function validarManual(){
      var sw = true;
      
      if (document.despmanual.planilla.value == "")
      {
         alert("Digite n�mero de la planilla");
         document.despmanual.planilla.focus();
         sw = false;
         return sw;
      }

      if (document.despmanual.placa.value == "")
      {
         alert("Digite placa del veh�culo");
         document.despmanual.placa.focus();
         sw = false;
         return sw;
      }

      if (document.despmanual.cedcon.value == "")
      {
         alert("Digite la cedula del conductor");
         document.despmanual.cedcon.focus();
         sw = false;
         return sw;
      }
    
      return sw;
}

function enviar(){
     var frm = document.forms[0];
     frm.submit();
     window.location="controller?estado=HReporte&accion=Search";
    // window.parent.focus();
    // window.parent.setActive();
}

function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPagina(url, nombrePagina)
{
  var wdth = screen.width - screen.width * 0.4;
  var hght = screen.height - screen.height * 0.5;
  var lf = screen.width * 0.2;
  var tp = screen.height * 0.25;
  var options = "status=yes,menubar=yes,scrollbars=yes,resizable=yes," +
                setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) )
    hWnd.opener = document.window;
}

function cargar()
{
  window.opener.document.forms[0].submit(); 
}
