/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#fecha").val('');
    $("#fechafin").val('');
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    // $("#fecha").datepicker("setDate", myDate);
    //$("#fechafin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

    cargarComboEds();
    $("#buscar").click(function () {
        var fechainicio = $("#fecha").val();
        var fechafin = $("#fechafin").val();
        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
            reporteRolocacion();
        } else {
            mensajesDelSistema("Por favor revice el rango de fechas", '410', '150', false);
        }

    });

});

function cargarComboEds() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 47
        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#eds').append("<option value=''> Seleccione.. </option>");
                for (var datos in json) {
                    $('#eds').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function reporteRolocacion() {
    var grid_tabla = jQuery("#tabla_reporte_colocacion");
    if ($("#gview_tabla_reporte_colocacion").length) {
        reloadGridTablas(grid_tabla, 52);
    } else {
        grid_tabla.jqGrid({
            caption: "VENTAS",
            url: "./controller?estado=Administracion&accion=Eds",
            mtype: "POST",
            datatype: "json",
            height: '550',
            width: '1650',
            colNames: ['Id', 'Periodo', 'Transportadora', 'Nombre Agencia', 'Cedula Propietario', 'Propietario', 'Cedula Conductor', 'Conductor', 'Placa', 'Sucursal', 'Origen', 'Destino',
                'Planilla', 'Fecha Venta', 'Fecha Anticipo', 'Tiempo Legalizacion', 'Valor Anticipo', 'Descuento Fintra', 'Valor Consignacion', 'Reanticipo',
                'Numero Venta', 'Nombre Eds', 'Kilometraje', 'Producto', 'Precio x Unidad', 'Cantidad Suministrada', 'Total Ventas', 'Dicponible'],
            colModel: [
                {name: 'id', index: 'id', width: 60, sortable: true, align: 'center', hidden: true},
                {name: 'periodo', index: 'periodo', width: 70, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'transportadora', index: 'transportadora', width: 190, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'nombre_agencia', index: 'nombre_agencia', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cedula_propietario', index: 'cedula_propietario', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'propietario', index: 'propietario', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cedula_conductor', index: 'cedula_conductor', width: 110, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'conductor', index: 'conductor', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'placa', index: 'placa', width: 110, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'sucursal', index: 'sucursal', width: 110, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'origen', index: 'origen', width: 110, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'destino', index: 'destino', width: 110, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'planilla', index: 'planilla', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_venta', index: 'fecha_venta', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_anticipo', index: 'fecha_anticipo', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'tiempo_legalizacion', index: 'tiempo_legalizacion', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'valor_anticipo', index: 'valor_anticipo', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'descuento_fintra', index: 'descuento_fintra', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_consignado', index: 'valor_consignado', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'reanticipo', index: 'reanticipo', width: 95, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'numero_venta', index: 'numero_venta', width: 95, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'nombre_eds', index: 'nombre_eds', width: 120, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'kilometraje', index: 'kilometraje', width: 95, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'producto', index: 'producto', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'precioxunidad', index: 'precioxunidad', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cantidad_suministrada', index: 'cantidad_suministrada', width: 150, sortable: true, align: 'right', hidden: false, search: true},
                {name: 'total_ventas', index: 'total_ventas', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 5, prefix: "$ "}},
                {name: 'disponible', index: 'disponible', width: 95, sortable: true, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 5, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            gridComplete: function () {
                var valor_anticipo = grid_tabla.jqGrid('getCol', 'valor_anticipo', false, 'sum');
                var descuento_fintra = grid_tabla.jqGrid('getCol', 'descuento_fintra', false, 'sum');
                var valor_consignado = grid_tabla.jqGrid('getCol', 'valor_consignado', false, 'sum');
                var total_ventas = grid_tabla.jqGrid('getCol', 'total_ventas', false, 'sum');
                grid_tabla.jqGrid('footerData', 'set', {tiempo_legalizacion: 'TOTAL', valor_anticipo: valor_anticipo, descuento_fintra: descuento_fintra, valor_consignado: valor_consignado, total_ventas: total_ventas});
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 52,
                    eds: $("#eds").val(),
                    fechainicio: $("#fecha").val(),
                    fechafin: $("#fechafin").val(),
                    planilla: $("#planilla").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
        $("#tabla_reporte_colocacion").navButtonAdd('#pager1', {
            caption: "Exportar Excel",
            onClickButton: function () {
                var info = grid_tabla.getGridParam('records');
                if (info > 0) {
                    exportarReporteColocacion();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }

            }
        });
    }
}

function reloadGridTablas(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Eds",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                eds: $("#eds").val(),
                fechainicio: $("#fecha").val(),
                fechafin: $("#fechafin").val(),
                planilla: $("#planilla").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function  exportarReporteColocacion() {
    var fullData = jQuery("#tabla_reporte_colocacion").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Administracion&accion=Eds",
        data: {
            listado: myJsonString,
            opcion: 53
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}