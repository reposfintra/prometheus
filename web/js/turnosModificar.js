function validar(CONTROLLER){                                                                                   
	if(isNaN (forma1.h_entrada.value) || forma1.h_entrada.value==""  ){
		alert ("La Hora de entrada debe ser un numero entre 1 y 12");
		forma1.h_entrada.focus();
		return false;
	}
	else if(isNaN (forma1.m_entrada.value) || forma1.m_entrada.value=="" ){
		alert ("Los Minutos de la Hora de entrada deben ser un numero entre 0 y 59");
		forma1.m_entrada.focus();
		return false;
	}
	else if(isNaN (forma1.h_salida.value) || forma1.h_salida.value==""  ) {
		alert ("La Hora de salida debe ser un numero entre 1 y 12");
		forma1.h_salida.focus();
		return false;
	}
	else if(isNaN (forma1.m_salida.value) || forma1.m_salida.value=="" ){
		alert ("Los Minutos de la Hora de salida deben ser un numero entre 0 y 59");
		forma1.m_salida.focus();
		return false;
	}
	else{
		var horaE=parseInt(forma1.h_entrada.value);
		var horaS=parseInt(forma1.h_salida.value);
		var mE =parseInt(forma1.m_salida.value);
		var mS =parseInt(forma1.m_salida.value);
		if(horaE >12|| horaS<0){
			alert ("La Hora de entrada deve ser un numero entre 1 y 12");
			forma1.h_entrada.focus();
			return false;
		}
		if(horaS >12 || horaE <0){
			alert ("La Hora de salida deve ser un numero entre 1 y 12");
			forma1.h_salida.focus();
			return false;
		}
		if(mE > 59 || mE <0){
			alert ("Los Minutos de la Hora de entrada deven ser un numero entre 0 y 59");
			forma1.m_entrada.focus();
			return false;
		}
		if(mS >59 || mS <0){
			alert ("Los Minutos de la Hora de salida deven ser un numero entre 0 y 59");
			forma1.m_entrada.focus();
			return false;
		} 
		
		var hora1 = parseInt(forma1.h_entrada.value + forma1.m_entrada.value);
		var hora2 = parseInt(forma1.h_salida.value + forma1.m_salida.value);
		if( forma1.h_e.value == "PM" ){
			if(hora1<1200){
				hora1=hora1+1200;
			}
		}
		if( forma1.h_s.value == "PM" ){
			if(hora2<1200){
				hora2=hora2+1200;
			}
		}
		//alert('........... HORA E: ' + hora1 + ' - HORA S: ' + hora2);
		//alert('.......... H_E: ' + forma1.h_e.value + ' H_S: ' + forma1.h_s.value);
		if(hora1 > hora2){
			alert("La hora de Entrada es Mayor que la de salida");
			return false;
		}
		if(hora1==hora2){
			var minuto1=parseInt(forma1.m_entrada.value);
			var minuto2=parseInt(forma1.m_salida.value);
			if (minuto1 > minuto2){
				alert("La hora de Entrada es Mayor que la de salida");
				return false;
			}
		}
		
		for(i=1;i<forma1.length;i++) 
			if (forma1.elements[i].checked){
				document.forma1.action =CONTROLLER+ "?estado=Turnos&accion=ModificarT"; 
        		document.forma1.submit(); 
				return true;
			}	
			alert('Por favor seleccione un registro para poder continuar');
				return false;
	}
}

function salir(){
	/*document.forma1.action = "<%= CONTROLLER %>?estado=Despachom&accion=Salir"; 
        document.forma1.submit(); */
		parent.close();

}

function insertar(){
		document.forma1.action = "<%= CONTROLLER %>?estado=Turnos&accion=Insertar"; 
        document.forma1.submit(); 
}

var isIE = document.all?true:false;
var isNS = document.layers?true:false;
function soloDigitos(e,decReq) {
    var key = (isIE) ? window.event.keyCode : e.which;
    var obj = (isIE) ? event.srcElement : e.target;
    var isNum = (key > 47 && key < 58) ? true:false;
    var dotOK =  (decReq=='decOK' && key ==46 && obj.value.indexOf('.')==-1) ? true:false;
    window.event.keyCode = (!isNum && !dotOK && isIE) ? 0:key;
    e.which = (!isNum && !dotOK && isNS) ? 0:key;   
    return (isNum || dotOK );
}