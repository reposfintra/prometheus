/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    cargarCasasCobranza();
});

function cargarCasasCobranza(){

    var grid_listar_casa_cobranza = $("#tabla_casa_cobranza");
    if ($("#gview_tabla_casa_cobranza").length) {
         refrescarCargarCasasCobranza();
     }else {
         grid_listar_casa_cobranza.jqGrid({        
            caption:'Casas Cobranza',
            url:  "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '300',
            autowidth: true,
            colNames: ['Id',  'Nit', 'Casa Cobranza',  'Direccion', 'Telefono',
                'Contacto 1', 'Telefono Contacto 1', 'Cargo Contacto 1', 'Email Contacto 1',
                'Contacto 2', 'Telefono Contacto 2', 'Cargo Contacto 2',  'Email Contacto 2',
                'Contacto 3', 'Telefono Contacto 3', 'Cargo Contacto 3',  'Email Contacto 3',
                'Activar/Inactivar'],
            colModel: [
                
                {name: 'id', index: 'id', width: 30, resizable:false, sortable: true, align: 'center', key: true, hidden:false},   
                {name: 'nit', index: 'nit', resizable:false, sortable: true, width: 80, align: 'center', hidden:false},
                {name: 'nombre', index: 'nombre', resizable:false, sortable: true, width: 180, align: 'center', hidden:false},
                {name: 'direccion', index: 'direccion', resizable:false, sortable: true, width: 180, align: 'left'},
                {name: 'telefono', index: 'telefono', resizable:false, sortable: true, width: 80, align: 'center', hidden:false},
                {name: 'nombre_contacto1', index: 'nombre_contacto1', resizable:false, sortable: true, width: 180, align: 'center', hidden:false},
                {name: 'telefono_contacto1', index: 'telefono_contacto1', resizable:false, sortable: true, width: 120, align: 'center', hidden:false},
                {name: 'cargo_contacto1', index: 'cargo_contacto1', resizable:false, sortable: true, width: 180, align: 'center', hidden:false},
                {name: 'email_contacto1', index: 'email_contacto1', resizable:false, sortable: true, width: 180, align: 'center', hidden:false},
                {name: 'nombre_contacto2', index: 'nombre_contacto2', resizable:false, sortable: true, width: 180, align: 'center', hidden:false},
                {name: 'telefono_contacto2', index: 'telefono_contacto2', resizable:false, sortable: true, width: 120, align: 'center', hidden:false},
                {name: 'cargo_contacto2', index: 'cargo_contacto2', resizable:false, sortable: true, width: 180, align: 'center', hidden:false},
                {name: 'email_contacto2', index: 'email_contacto2', resizable:false, sortable: true, width: 180, align: 'center', hidden:false},
                {name: 'nombre_contacto3', index: 'nombre_contacto3', resizable:false, sortable: true, width: 180, align: 'center', hidden:false},
                {name: 'telefono_contacto3', index: 'telefono_contacto3', resizable:false, sortable: true, width: 120, align: 'center', hidden:false},
                {name: 'cargo_contacto3', index: 'cargo_contacto3', resizable:false, sortable: true, width: 180, align: 'center', hidden:false},
                {name: 'email_contacto3', index: 'email_contacto3', resizable:false, sortable: true, width: 180, align: 'center', hidden:false},
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'} 
              
            ],
            rowNum: 10000,
            rowTotal: 10000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#page_tabla_casa_cobranza',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {                
                dataType: "json",
                type: "POST",  
                async:false,
                data: {
                    opcion: 42
                }
            },    
            gridComplete: function() {
                var cant = $("#tabla_casa_cobranza").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_casa_cobranza").getRowData(cant[i]).cambio;                 
                    var cl = cant[i];
                    var be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoCasaCobranza('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 18px;'></span> </label></div>";
                    $("#tabla_casa_cobranza").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_casa_cobranza"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                editarCasaCobranza(id);
               
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_casa_cobranza",{search:false,refresh:false,edit:false,add:false,del:false});      
        $("#tabla_casa_cobranza").jqGrid("navButtonAdd", "#page_tabla_casa_cobranza", {
            caption: "Nueva casa cobranza", 
            title: "Agregar nueva casa cobranza",           
            onClickButton: function() {
                crearCasaCobranza();           
            }
        });           

     }
}

function refrescarCargarCasasCobranza(){    
    $("#tabla_casa_cobranza").setGridParam({
        url: "./controller?estado=Negocios&accion=Fintra",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async:false,
            data: {
                opcion: 42
            }
        }
    }).trigger("reloadGrid");
}

function CambiarEstadoCasaCobranza(rowid){
    var grid_tabla = $("#tabla_casa_cobranza");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url:  "./controller?estado=Negocios&accion=Fintra",
        data: {
            opcion: 43,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarCargarCasasCobranza();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado de la configuracion!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function editarCasaCobranza(cl){
    $('#div_config_casa_cobranza').fadeIn("slow");
    var fila = $("#tabla_casa_cobranza").getRowData(cl);
    var nit = fila['nit'];  
    var nombre = fila['nombre'];   
    var direccion = fila['direccion'];  
    var telefono = fila['telefono'];
    var nombre_contacto1 = fila['nombre_contacto1'];
    var telefono_contacto1 = fila['telefono_contacto1'];
    var cargo_contacto1 = fila['cargo_contacto1']; 
    var email_contacto1 = fila['email_contacto1'];
    var nombre_contacto2 = fila['nombre_contacto2'];
    var telefono_contacto2 = fila['telefono_contacto2'];
    var cargo_contacto2 = fila['cargo_contacto2']; 
    var email_contacto2 = fila['email_contacto2'];
    var nombre_contacto3 = fila['nombre_contacto3'];
    var telefono_contacto3 = fila['telefono_contacto3'];
    var cargo_contacto3 = fila['cargo_contacto3']; 
    var email_contacto3 = fila['email_contacto3'];
  
    $('#id_casa').val(cl);
    $('#nit').val(nit);
    $('#nombre').val(nombre);
    $('#direccion').val(direccion);
    $('#telefono').val(telefono);
    $('#nombre_contacto1').val(nombre_contacto1);
    $('#telefono_contacto1').val(telefono_contacto1);
    $('#cargo_contacto1').val(cargo_contacto1);
    $('#email_contacto1').val(email_contacto1);
    $('#nombre_contacto2').val(nombre_contacto2);
    $('#telefono_contacto2').val(telefono_contacto2);
    $('#cargo_contacto2').val(cargo_contacto2);
    $('#email_contacto2').val(email_contacto2);
    $('#nombre_contacto3').val(nombre_contacto3);
    $('#telefono_contacto3').val(telefono_contacto3);
    $('#cargo_contacto3').val(cargo_contacto3);
    $('#email_contacto3').val(email_contacto3);
   
    AbrirDivEditarCasaCobranza();
}

function AbrirDivEditarCasaCobranza(){
      $("#div_config_casa_cobranza").dialog({
        width: 'auto',
        height: 470,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR CASA COBRANZA',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              guardarCasaCobranza(44,'update');
            },
            "Salir": function () {                
                resetearValores();              
                $(this).dialog("destroy");
            }
        }
    });       
}

function guardarCasaCobranza(option, action){  
    var id_casa=$('#id_casa').val();
    var nit=$('#nit').val();
    var nombre=$('#nombre').val();
    var direccion=$('#direccion').val();
    var telefono=$('#telefono').val();
    var nombre_contacto1=$('#nombre_contacto1').val();
    var telefono_contacto1=$('#telefono_contacto1').val();
    var cargo_contacto1=$('#cargo_contacto1').val();
    var email_contacto1=$('#email_contacto1').val();
    var nombre_contacto2=$('#nombre_contacto2').val();
    var telefono_contacto2=$('#telefono_contacto2').val();
    var cargo_contacto2=$('#cargo_contacto2').val();
    var email_contacto2=$('#email_contacto2').val();
    var nombre_contacto3=$('#nombre_contacto3').val();
    var telefono_contacto3=$('#telefono_contacto3').val();
    var cargo_contacto3=$('#cargo_contacto3').val();
    var email_contacto3=$('#email_contacto3').val();
    
    var url = "./controller?estado=Negocios&accion=Fintra";
   if(nit === '' || nombre === '' || direccion === '' || telefono === '' || nombre_contacto1 === '' || email_contacto1 === ''){
     mensajesDelSistema("Debe diligenciar todos los campos requeridos", '250', '150');
    }else{
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        opcion: option, 
                        action: action,
                        id_casa:id_casa,
                        nit:nit,
                        nombre:nombre,
                        direccion:direccion,
                        telefono:telefono,
                        nombre_contacto1:nombre_contacto1,
                        telefono_contacto1:telefono_contacto1,
                        cargo_contacto1:cargo_contacto1,
                        email_contacto1:email_contacto1,
                        nombre_contacto2:nombre_contacto2,
                        telefono_contacto2:telefono_contacto2,
                        cargo_contacto2:cargo_contacto2,
                        email_contacto2:email_contacto2,
                        nombre_contacto3:nombre_contacto3,
                        telefono_contacto3:telefono_contacto3,
                        cargo_contacto3:cargo_contacto3,
                        email_contacto3:email_contacto3
                        
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '270', '165');                          
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                refrescarCargarCasasCobranza();  
                                resetearValores();
                                (action === 'update') ? $("#div_config_casa_cobranza").dialog('close'): mensajesDelSistema("Cambios guardados satisfactoriamente", '250', '150', true);

                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo guardar la informacion!!", '250', '150');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                              "Message: " + xhr.statusText + "\n" +
                              "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                }); 
            },500);
    }
    
}
function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function crearCasaCobranza(){   
    $('#div_config_casa_cobranza').fadeIn('slow');   
    resetearValores();
    AbrirDivCrearCasaCobranza();
}
function AbrirDivCrearCasaCobranza(){
      $("#div_config_casa_cobranza").dialog({
        width: 'auto',
        height: 470,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR CASA COBRANZA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {         
                guardarCasaCobranza(44,'insert');             
            },
            "Salir": function () {                  
                resetearValores();         
                $(this).dialog("destroy");
            }
        }
    });    
}
function resetearValores(){    
    $('#id_casa').val("");
    $('#nit').val("");
    $('#nombre').val("");
    $('#direccion').val("");
    $('#telefono').val("");
    $('#nombre_contacto1').val("");
    $('#telefono_contacto1').val("");
    $('#cargo_contacto1').val("");
    $('#email_contacto1').val("");
    $('#nombre_contacto2').val("");
    $('#telefono_contacto2').val("");
    $('#cargo_contacto2').val("");
    $('#email_contacto2').val("");
    $('#nombre_contacto3').val("");
    $('#telefono_contacto3').val("");
    $('#cargo_contacto3').val("");
    $('#email_contacto3').val("");
}