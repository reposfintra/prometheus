function consultarMensaje( target ){ 
	
    var ajax = getAjax();
   	//alert("antes de open"+target);
    ajax.open ( 'POST', target, true );
	
	//alert("despues de open");
    ajax.onreadystatechange = function() {
        if ( ajax.readyState == 1 ) {
            //alert("estado1");
            //document.getElementById("working").innerText = "estado 1...";
       		//document.getElementById("Mensaje").innerText = "estado1";
         }else if ( ajax.readyState == 4 ){
            //document.getElementById("working").innerHTML = "estado 4...";
		    //document.getElementById("Mensaje").innerText = "estado4";
            if( ajax.status == 200 ){                
                document.getElementById("Mensaje").innerText = ""+ajax.responseText;                
                
            }
         }
    }
    ajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    ajax.send( null );
	//ajax=null;
	//ajax.close();
}
function actualizarMensaje( target ){ 

    var ajax = getAjax();
   	
    ajax.open ( 'POST', target, true );
	
    ajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    ajax.send( null );
	
}
function getAjax(){
    var ajax = false;
    try {
        ajax = new ActiveXObject( "Msxml2.XMLHTTP" );
    } catch ( e ) {
        try {
            ajax = new ActiveXObject( "Microsoft.XMLHTTP" );
        } catch ( E ) {
           ajax = false;
		   alert("error");
        }
    }
    if ( !ajax && typeof XMLHttpRequest!='undefined' ) {
        ajax = new XMLHttpRequest();
    }
    return ajax;
}
