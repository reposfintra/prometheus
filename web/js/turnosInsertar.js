
function validar(CONTROLLER){                                                                                   
	if(forma1.fecha.value=="" ){
		alert ("Digite la fecha del turno");
		return false;
	}

        
        if(forma1.fecha2.value=="" && forma1.semanal.checked){
            alert ("Digite la fecha Final del turno");
            return false;
        }
        

	else if(isNaN (forma1.h_entrada.value) || forma1.h_entrada.value==""  ){
		alert ("La Hora de entrada debe ser un numero entre 1 y 12");
		forma1.h_entrada.focus();
		return false;
	}
	else if(isNaN (forma1.m_entrada.value) || forma1.m_entrada.value=="" ){
		alert ("Los Minutos de la Hora de entrada deben ser un numero entre 0 y 59");
		forma1.m_entrada.focus();
		return false;
	}
	else if(isNaN (forma1.h_salida.value) || forma1.h_salida.value==""  ) {
		alert ("La Hora de salida debe ser un numero entre 1 y 12");
		forma1.h_salida.focus();
		return false;
	}
	else if(isNaN (forma1.m_salida.value) || forma1.m_salida.value=="" ){
		alert ("Los Minutos de la Hora de salida deben ser un numero entre 0 y 59");
		forma1.m_salida.focus();
		return false;
	}
	else{
		var horaE=parseInt(forma1.h_entrada.value);
		var horaS=parseInt(forma1.h_salida.value);
		var mE =parseInt(forma1.m_salida.value);
		var mS =parseInt(forma1.m_salida.value);
		if(horaE >12|| horaS<0){
			alert ("La Hora de entrada debe ser un numero entre 1 y 12");
			forma1.h_entrada.focus();
			return false;
		}
		if(horaS >12 || horaE <0){
			alert ("La Hora de salida debe ser un numero entre 1 y 12");
			forma1.h_salida.focus();
			return false;
		}
		if(mE > 59 || mE <0){
			alert ("Los Minutos de la Hora de entrada deben ser un numero entre 0 y 59");
			forma1.m_entrada.focus();
			return false;
		}
		if(mS >59 || mS <0){
			alert ("Los Minutos de la Hora de salida deben ser un numero entre 0 y 59");
			forma1.m_entrada.focus();
			return false;
		} 
		for (i=0;i<forma1.dia.length;i++){
                        var horavalue1;
                        var horavalue2; 
			if((forma1.dia[i].value == "a") && (forma1.dia[i].selected==true)){
                                
                                var first1 = (forma1.h_entrada.value).charAt(0);
                                var first2 = (forma1.h_salida.value).charAt(0);
                                horavalue1 = ( first1 == '0' )? (forma1.h_entrada.value).substring(1,2) : (forma1.h_entrada.value);
                                horavalue2 = ( first2 == '0' )? (forma1.h_salida.value).substring(1,2) : (forma1.h_salida.value);
                                
				var hora1  = parseInt(horavalue1);
				var hora2  = parseInt(horavalue2);
                                
				for(x=0;x <forma1.h_e.length;x++){
					if((forma1.h_e[x].value == "PM") && (forma1.h_e[x].selected==true)){
						if(hora1<12){
							hora1=hora1+12;
						}
					}
				}
				for (y=0;y <forma1.h_s.length;y++){
					if((forma1.h_s[y].value == "PM") && (forma1.h_s[y].selected==true)){
						if(hora2<12){
							hora2=hora2+12;
						}
					}
				}
				if(hora1 > hora2){
					alert("La hora de Entrada es Mayor que la de salida");
					return false;
				}
				if(hora1==hora2){
					var minuto1=parseInt(forma1.m_entrada.value);
					var minuto2=parseInt(forma1.m_salida.value);
					if (minuto1 > minuto2){
						alert("La hora de Entrada es Mayor que la de salida");
						return false;
					}
				}
			}
		}
		for(i=1;i<forma1.length;i++){ 
			if (forma1.elements[i].checked && forma1.elements[i].name != 'nozonas'){
				document.forma1.action =CONTROLLER+ "?estado=Turnos&accion=Insertar"; 
        		document.forma1.submit(); 
				return true;
			} else if (forma1.elements[i].checked && forma1.elements[i].name == 'nozonas'){
				alert('No se puede procesar la información. El usuario actual no tiene zonas asignadas.');
				return false;
			}
		}
			alert('Por favor seleccione un registro para poder continuar');
				return false;
	}
}

function salir(){
		parent.close();
}

function insertar(){
		document.forma1.action = "<%= CONTROLLER %>?estado=Turnos&accion=Insertar"; 
        document.forma1.submit(); 
}

function cambiarHora(cm){
	for(var i=0;i<cmd.length;i++){
		if(cmd[i].selected=true){
			cmd[i].selected=false;
		}
		else{
			cmd[i].selected=true;
		}
	}
}



var isIE = document.all?true:false;
var isNS = document.layers?true:false;
function soloDigitos(e,decReq) {
	//alert(window.event.keyCode);
	var key = (isIE) ? window.event.keyCode : e.which;
    var obj = (isIE) ? event.srcElement : e.target;
    var isNum = (key > 47 && key < 58) ? true:false;
    var dotOK =  (decReq=='decOK' && key ==46) ? true:false;
    window.event.keyCode = (!isNum && !dotOK && isIE) ? 0:key;
    e.which = (!isNum && !dotOK && isNS) ? 0:key;
    return (isNum || dotOK );
}

function actualizar(url){
    
    document.forma1.action = url + "?estado=Turnos&accion=IngresarRefresh"; 
    document.forma1.submit(); 

}

function enableFechaFin(){
    
    if( document.forma1.semanal.checked ){
        document.getElementById("fechafin").style.display = "block";
    }else{
        document.getElementById("fechafin").style.display = "none";
    }        
}

function selec(cmb, vlr){
	for( var i=0; i<cmb.length; i++	){
		if( cmb[i].value == vlr ){
			cmb[i].selected = true;
		}
	}
}


