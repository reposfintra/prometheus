/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    cargarComboUnidNegocio();
    cargarCboTerceros();
});


function cargarComboUnidNegocio(){
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 27
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
                $('#id_unidad_negocio').html('');
                $('#id_unidad_negocio').append('<option value="" >...</option>');
                for (var datos in json) {
                    $('#id_unidad_negocio').append('<option value="' + datos + '" >' + json[datos] + '</option>');
                }
           

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    
}

function cargarReporteNegocios() {
    if ($("#id_unidad_negocio").val()!="" && $("#id_recaudador_tercero").val()!="" ){
        
    var grid_tabla = $("#tabla_negocios");
    if ($("#gview_tabla_negocios").length) {
        reloadGridTabla(grid_tabla);
    } else {
        
        grid_tabla.jqGrid({
            caption: "Reporte",
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '1720',
            colNames: ['Empresa Responsable','Perido Carga','Estado','Cedula','Nombre Cliente','Direccion Domicilio','Ciudad Domicilio','Barrio Domicilio',
                       'Telefono','Celular','Telefono Direccion Negocio','Barrio Negocio','Telefono Negocio','Negocio','Unidad Negocio','Agencia','Altura Mora','Dia pago',
                       'Saldo Actual','Saldo Vencido','Interes Mora','Gac','Total A Pagar','Juridica','Reestructuracion','Fecha Ultimo Compromiso','Responsable Cuenta'],
                    
            colModel: [
                {name: 'empresa_responsable',        index: 'empresa_responsable',        width: 90,sortable: true, align: 'center', hidden: false},
                {name: 'periodo_carga',        index: 'periodo_carga',        width: 90,sortable: true, align: 'center', hidden: false},
                {name: 'estado',        index: 'estado',        width: 90,sortable: true, align: 'center', hidden: true},
                {name: 'cedula',        index: 'cedula',        width: 90,sortable: true, align: 'center', hidden: false},
                {name: 'nombre_cliente',index: 'nombre_cliente',width: 300, sortable: true, align: 'center',   hidden: false},
                {name: 'direccion_domicilio',index: 'direccion_domicilio',width: 180, sortable: true, align: 'center',   hidden: false},
                {name: 'ciudad_domicilio',       index: 'ciudad_domicilio',       width: 120, sortable: true, align: 'center',   hidden: false},
                {name: 'barrio_domicilio',       index: 'barrio_domicilio',       width: 120, sortable: true, align: 'center',   hidden: false},              
                {name: 'telefono',       index: 'telefono',       width: 90, sortable: true, align: 'center',   hidden: false},              
                {name: 'celular',       index: 'celular',       width: 120, sortable: true, align: 'center',   hidden: false},              
                {name: 'direccion_negocio',       index: 'direccion_negocio',       width: 120, sortable: true, align: 'center',   hidden: false},              
                {name: 'barrio_negocio',       index: 'barrio_negocio',       width: 120, sortable: true, align: 'center',   hidden: false},              
                {name: 'telefon_negocio',       index: 'telefon_negocio',       width: 120, sortable: true, align: 'center',   hidden: false},              
                {name: 'negocio',       index: 'negocio',       width: 120, sortable: true, align: 'center',   hidden: false},              
                {name: 'unidad_negocio',       index: 'unidad_negocio',       width: 120, sortable: true, align: 'center',   hidden: false},              
                {name: 'agencia',       index: 'agencia',       width: 120, sortable: true, align: 'center',   hidden: false},              
                {name: 'altura_mora_actual',       index: 'altura_mora_actual',       width: 120, sortable: true, align: 'center',   hidden: false},              
                {name: 'dia_pago',       index: 'dia_pago',       width: 80, sortable: true, align: 'center',   hidden: false}, 
                {name: 'saldo_actual', index: 'saldo_actual', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}}, 
                {name: 'valor_saldo_vencido', index: 'valor_saldo_vencido', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}}, 
                {name: 'interes_mora', index: 'interes_mora', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'gac', index: 'gac', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'total_a_pagar', index: 'total_a_pagar', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'juridica',       index: 'juridica',       width: 120, sortable: true, align: 'center',   hidden: false}, 
                {name: 'reestructuracion',       index: 'reestructuracion',       width: 120, sortable: true, align: 'center',   hidden: false}, 
                {name: 'fecha_ult_compromiso',       index: 'fecha_ult_compromiso',       width: 120, sortable: true, align: 'center',   hidden: false}, 
                {name: 'responsable_cuenta',       index: 'responsable_cuenta',       width: 120, sortable: true, align: 'center',   hidden: false} 
                
                
                
            ],
            rowNum: 100000,
            rowTotal: 100000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#pager',
            pgtext: null,
            pgbuttons: false,
            multiselect:false,           
            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
                
            },
            loadComplete: function () {
                
                if (grid_tabla.jqGrid('getGridParam', 'records') <= 0) {
                                           
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            ajaxGridOptions: {
                
                data: {
                    opcion: 28,
                    id_unidad_negocio: $("#id_unidad_negocio").val(),
                    id_recaudador_tercero: $("#id_recaudador_tercero").val()
                    

                }                
                
            },
            gridComplete: function() { 
                
                var colSumTotal = $("#tabla_detalle_caja_recaudo").jqGrid('getCol', 'vlr_ingreso', false, 'sum'); 
                $("#tabla_negocios").jqGrid('footerData', 'set', {vlr_ingreso: colSumTotal});
                             
            }, 
            
           
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
                
            }
        }).navGrid('#pager', {add: false, edit: false, del: false, search: false, refresh: false});
        $("#tabla_negocios").navButtonAdd('#pager', {
            caption: "Exportar",
            title: "Exportar a excel",
            onClickButton: function () {               
            exportarNegocios();		
            }
        }); 
       
           
    }
     }else {
            mensajesDelSistema("Por favor seleccione todas las opciones",'250', '150');
            }
    
}


function reloadGridTabla(grid_tabla) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",
       
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 28,
                id_unidad_negocio: $("#id_unidad_negocio").val()    
            }
        }
    });
    grid_tabla.trigger("reloadGrid");    
     
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                
                $(this).dialog("close");
            }
        }
    });

}

function  exportarNegocios() {
     loading("Esta operaci�n puede tardar varios minutos, por favor espere.", 450);     
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Negocios&accion=Fintra",
        data: {
            opcion: 29,
            id_unidad_negocio: $("#id_unidad_negocio").val(),
            id_recaudador_tercero: $("#id_recaudador_tercero").val()
            
        },
        success: function () {
           
            $("#dialogLoading").dialog("close");
             mensajesDelSistema ("Documento generado con exito, puede consultarlo en la carpeta descargas", '250', '150', true);
        },
        error: function () {
           $("#dialogLoading").dialog("close");
            mensajesDelSistema("Ocurrio un error enviando los datos al servidor", '250', '150', true);
        }
    });
}


function loading(msj, width) {
                $("#msj2").html(msj);
                $("#dialogLoading").dialog({
                    width: width,
                    height: 130,
                    show: "scale",
                    hide: "scale",
                    resizable: false,
                    position: "center",
                    modal: true,
                    closeOnEscape: true
                });

                $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}


function cargarCboTerceros(){
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 36
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
                $('#id_recaudador_tercero').html('');
                $('#id_recaudador_tercero').append('<option value="" >...</option>');
                for (var datos in json) {
                    $('#id_recaudador_tercero').append('<option value="' + datos + '" >' + json[datos] + '</option>');
                }

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    
}