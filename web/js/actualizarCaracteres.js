/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarInsumos() {
    
    var grid_tabla_ = jQuery("#tabla_insumos");
    if ($("#gview_tabla_insumos").length) {
       reloadGridMostrar(grid_tabla_, 94);
    } else {
    //alert('Entra aqui');  
    //console.log('Entra aqui');
        grid_tabla_.jqGrid({
            
            caption: "Insumos",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '850',
            colNames: ['Id', 'Codigo', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: false, key: true},
                {name: 'codigo_material', index: 'codigo_material', width: 100, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'descripcion', index: 'descripcion', width: 630, sortable: true, align: 'left', hidden: false, search: true}
                
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000000,
            rowTotal: 10000000,
            loadonce: true,
           // gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            pager: '#page_tabla_insumos',
//            multiselect: false,
//            multiboxonly: false,
//            pgtext: null,
//            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, 
            ajaxGridOptions: {               
                data: {
                    opcion: 94
                }
            },
            loadError: function (xhr, status, error) {
                console.log('Entra aqui'+error);
                mensajesDelSistema(error, 250, 150);

            }
            ,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                
                var myGrid = jQuery("#tabla_insumos"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var codigo_material = filas.codigo_material;
                var descripcion = filas.descripcion;
                ventanaInsumo(id, codigo_material, descripcion);
                
                
            }
            
        })
          .navGrid("#page_tabla_insumos", {add: false, edit: false, del: false, search: false, refresh: false}, 
        {multipleSearch: true, uniqueSearchFields: true, multipleGroup: true} );
        grid_tabla_.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    }
    
    document.getElementById("gs_descripcion").style.width = '610px';
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            mtype: "POST",
            datatype: "json",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaInsumo(id, codigo_material, descripcion) {
        console.log('Entra aqui');
        $("#id").val(id);
        $("#codigo_material").val(codigo_material);
        $("#descripcion").val(descripcion);
        $("#div_insumos").dialog({
            width: '700',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'INSUMOS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarInsumo();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#codigo_material").val('');
                    $("#descripcion").val('');
                }
            }
        });
    
}

function ActualizarInsumo() {
    var id = $("#id").val();
    var codigo_material = $("#codigo_material").val();
    var descripcion = $("descripcion").val();
    //alert(id_convenio);
    if (codigo_material !== '' && descripcion !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 95,
                id: $("#id").val(),
                codigo_material: $("#codigo_material").val(),
                descripcion: $("#descripcion").val()
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#div_insumos").dialog('close');
                    $("#id").val('');
                    $("#codigo_material").val('');
                    $("#descripcion").val('');
                }
                cargarInsumos();
            }, error: function (result) {
                alert('No se puede actualizar, verifique los datos e intente nuevamente');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}


function cargarValores() {
    var grid_tabla_ = jQuery("#tabla_valores_predeterminados");
    if ($("#gview_tabla_valores_predeterminados").length) {
       reloadGridMostrar1(grid_tabla_, 96);
    } else {
    //alert('Entra aqui');  
    console.log('Entra aqui');
        grid_tabla_.jqGrid({
            
            caption: "Valores predeterminados",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '900',
            colNames: ['Id', 'Valor','Descripcion'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: false, key: true},
                {name: 'valor_xdefecto', index: 'valor_xdefecto', width: 390, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'descripcion', index: 'descripcion', width: 390, sortable: true, align: 'left', hidden: false, search: true}
                
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            pager: '#page_valores_predeterminados',
//            multiselect: false,
//            multiboxonly: false,
//            pgtext: null,
//            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, 
            ajaxGridOptions: {               
                data: {
                    opcion: 96
                }
            },
            loadError: function (xhr, status, error) {
                console.log('Entra aqui'+error);
                mensajesDelSistema(error, 250, 150);

            }
            ,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                
                var myGrid = jQuery("#tabla_valores_predeterminados"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var valor_xdefecto = filas.valor_xdefecto;
                ventanaValor(id, valor_xdefecto);
                
                
            }
            
        })
          .navGrid("#page_tabla_valores_predeterminados", {add: false, edit: false, del: false, search: false, refresh: false}, 
        {multipleSearch: true, uniqueSearchFields: true, multipleGroup: true} );
        grid_tabla_.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    }

document.getElementById("gs_valor_xdefecto").style.width = '370px';
document.getElementById("gs_descripcion").style.width = '370px';
}

function reloadGridMostrar1(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            mtype: "POST",
            datatype: "json",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaValor(id, valor_xdefecto) {
        $("#id").val(id);
        $("#valor_xdefecto").val(valor_xdefecto);
        
        $("#div_valores_predeterminados").dialog({
            width: '700',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'VALORES PREDETERMINADOS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    actualizarValor();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#valor_xdefecto").val('');
                }
            }
        });
    
}

function actualizarValor() {
    var id = $("#id").val();
    var valor_xdefecto = $("#valor_xdefecto").val();
    
    if (valor_xdefecto !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 97,
                id: $("#id").val(),
                valor_xdefecto: $("#valor_xdefecto").val()
           },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#div_valores_predeterminados").dialog('close');
                    $("#id").val('');
                    $("#valor_xdefecto").val('');
}
                cargarValores();
            }, error: function (result) {
                alert('No se puede actualizar, verifique los datos e intente nuevamente');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function cargarSubCategorias() {
    var grid_tabla_ = jQuery("#tabla_subcategorias");
    if ($("#gview_tabla_subcategorias").length) {
       reloadGridMostrar2(grid_tabla_, 98);
    } else {
    //alert('Entra aqui');  
    console.log('Entra aqui');
        grid_tabla_.jqGrid({
            
            caption: "Subcategorias",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '920',
            colNames: ['Id','Nombre','Descripcion'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: false, key: true},
                {name: 'nombre', index: 'nombre', width: 400, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'descripcion', index: 'descripcion', width: 400, sortable: true, align: 'left', hidden: false, search: true}
                
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            pager: '#page_subcategorias',
//            multiselect: false,
//            multiboxonly: false,
//            pgtext: null,
//            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, 
            ajaxGridOptions: {               
                data: {
                    opcion: 98
                }
            },
            loadError: function (xhr, status, error) {
                console.log('Entra aqui'+error);
                mensajesDelSistema(error, 250, 150);

            }
            ,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                
                var myGrid = jQuery("#tabla_subcategorias"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var nombre = filas.nombre;
                //var descripcion = filas.descripcion;
                ventanaSubCategoria(id, nombre);
                
                
            }
            
        })
          .navGrid("#page_tabla_subcategorias", {add: false, edit: false, del: false, search: false, refresh: false}, 
        {multipleSearch: true, uniqueSearchFields: true, multipleGroup: true} );
        grid_tabla_.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    }

document.getElementById("gs_nombre").style.width = '330px';
document.getElementById("gs_descripcion").style.width = '330px';
}

function reloadGridMostrar2(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            mtype: "POST",
            datatype: "json",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaSubCategoria(id, nombre) {
        $("#id").val(id);
        $("#nombre").val(nombre);
        //$("#descripcion").val(descripcion);
        
        $("#div_subcategorias").dialog({
            width: '700',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'SUBCATEGORIA',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    actualizarSubCategoria();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#nombre").val('');
                    //$("#descripcion").val('');
                }
            }
        });
    
}

function actualizarSubCategoria() {
    var id = $("#id").val();
    var nombre = $("#nombre").val();
    //var descripcion = $("#descripcion").val();
    
    if (nombre !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 99,
                id: $("#id").val(),
                nombre: $("#nombre").val(),
                descripcion: $("#descripcion").val()
           },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#div_subcategorias").dialog('close');
                    $("#id").val('');
                    $("#nombre").val('');
                    //$("#descripcion").val('');
}
                cargarSubCategorias();
            }, error: function (result) {
                alert('No se puede actualizar, verifique los datos e intente nuevamente');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function cargarCategorias() {
    var grid_tabla_ = jQuery("#tabla_categorias");
    if ($("#gview_tabla_categorias").length) {
       reloadGridMostrar3(grid_tabla_, 100);
    } else {
    //alert('Entra aqui');  
    console.log('Entra aqui');
        grid_tabla_.jqGrid({
            
            caption: "categorias",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '920',
            colNames: ['Id','Nombre','Descripcion'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: false, key: true},
                {name: 'nombre', index: 'nombre', width: 400, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'descripcion', index: 'descripcion', width: 400, sortable: true, align: 'left', hidden: false, search: true}
                
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            pager: '#page_categorias',
//            multiselect: false,
//            multiboxonly: false,
//            pgtext: null,
//            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, 
            ajaxGridOptions: {               
                data: {
                    opcion: 100
                }
            },
            loadError: function (xhr, status, error) {
                console.log('Entra aqui'+error);
                mensajesDelSistema(error, 250, 150);

            }
            ,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                
                var myGrid = jQuery("#tabla_categorias"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var nombre = filas.nombre;
                //var descripcion = filas.descripcion;
                ventanaCategoria(id, nombre);
                
                
            }
            
        })
          .navGrid("#page_tabla_categorias", {add: false, edit: false, del: false, search: false, refresh: false}, 
        {multipleSearch: true, uniqueSearchFields: true, multipleGroup: true} );
        grid_tabla_.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    }

document.getElementById("gs_nombre").style.width = '330px';
document.getElementById("gs_descripcion").style.width = '330px';
}

function reloadGridMostrar3(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            mtype: "POST",
            datatype: "json",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaCategoria(id, nombre) {
        $("#id").val(id);
        $("#nombre").val(nombre);
        //$("#descripcion").val(descripcion);
        
        $("#div_categorias").dialog({
            width: '700',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'Categoria',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    actualizarCategoria();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#nombre").val('');
                    //$("#descripcion").val('');
                }
            }
        });
    
}

function actualizarCategoria() {
    var id = $("#id").val();
    var nombre = $("#nombre").val();
    //var descripcion = $("#descripcion").val();
    
    if (nombre !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 101,
                id: $("#id").val(),
                nombre: $("#nombre").val(),
                descripcion: $("#descripcion").val()
           },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#div_categorias").dialog('close');
                    $("#id").val('');
                    $("#nombre").val('');
                    //$("#descripcion").val('');
}
                cargarCategorias();
            }, error: function (result) {
                alert('No se puede actualizar, verifique los datos e intente nuevamente');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function cargarApu() {
    var grid_tabla_ = jQuery("#tabla_Apu");
    if ($("#gview_tabla_Apu").length) {
       reloadGridMostrar4(grid_tabla_, 102);
    } else {
    //alert('Entra aqui');  
    console.log('Entra aqui');
        grid_tabla_.jqGrid({
            
            caption: "Apu",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '860',
            colNames: ['Id','Nombre'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: false, key: true},
                {name: 'nombre', index: 'nombre', width: 750, sortable: true, align: 'left', hidden: false, search: true},
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            pager: '#page_Apu',
//            multiselect: false,
//            multiboxonly: false,
//            pgtext: null,
//            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, 
            ajaxGridOptions: {               
                data: {
                    opcion: 102
                }
            },
            loadError: function (xhr, status, error) {
                console.log('Entra aqui'+error);
                mensajesDelSistema(error, 250, 150);

            }
            ,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                
                var myGrid = jQuery("#tabla_Apu"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var nombre = filas.nombre;
                ventanaApu(id, nombre);
                
                
            }
            
        })
          .navGrid("#page_tabla_Apu", {add: false, edit: false, del: false, search: false, refresh: false}, 
        {multipleSearch: true, uniqueSearchFields: true, multipleGroup: true} );
        grid_tabla_.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    }

document.getElementById("gs_nombre").style.width = '680px';

}

function reloadGridMostrar4(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            mtype: "POST",
            datatype: "json",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaApu(id, nombre) {
        $("#id").val(id);
        $("#nombre").val(nombre);

        $("#div_Apu").dialog({
            width: '700',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'Apu',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    actualizarApu();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#nombre").val('');

                }
            }
        });
    
}

function actualizarApu() {
    var id = $("#id").val();
    var nombre = $("#nombre").val();

    if (nombre !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 103,
                id: $("#id").val(),
                nombre: $("#nombre").val(),
                descripcion: $("#descripcion").val()
           },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#div_Apu").dialog('close');
                    $("#id").val('');
                    $("#nombre").val('');
                    //$("#descripcion").val('');
}
                cargarApu();
            }, error: function (result) {
                alert('No se puede actualizar, verifique los datos e intente nuevamente');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}


function cargarEspecificaciones() {
    var grid_tabla_ = jQuery("#tabla_Especificaciones");
    if ($("#gview_tabla_Especificaciones").length) {
       reloadGridMostrar5(grid_tabla_, 104);
    } else {
    //alert('Entra aqui');  
    console.log('Entra aqui');
        grid_tabla_.jqGrid({
            
            caption: "Especificaciones",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '500',
            colNames: ['Id','Nombre'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: false, key: true},
                {name: 'nombre', index: 'nombre', width: 380, sortable: true, align: 'left', hidden: false, search: true},
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            pager: '#page_Especificaciones',
//            multiselect: false,
//            multiboxonly: false,
//            pgtext: null,
//            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, 
            ajaxGridOptions: {               
                data: {
                    opcion: 104
                }
            },
            loadError: function (xhr, status, error) {
                console.log('Entra aqui'+error);
                mensajesDelSistema(error, 250, 150);

            }
            ,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                
                var myGrid = jQuery("#tabla_Especificaciones"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var nombre = filas.nombre;
                ventanaEspecificaciones(id, nombre);
                
                
            }
            
        })
          .navGrid("#page_tabla_Especificaciones", {add: false, edit: false, del: false, search: false, refresh: false}, 
        {multipleSearch: true, uniqueSearchFields: true, multipleGroup: true} );
        grid_tabla_.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    }

document.getElementById("gs_nombre").style.width = '360px';

}

function reloadGridMostrar5(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            mtype: "POST",
            datatype: "json",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaEspecificaciones(id, nombre) {
        $("#id").val(id);
        $("#nombre").val(nombre);

        $("#div_Especificaciones").dialog({
            width: '500',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'Especificaciones',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    actualizarEspecificaciones();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#nombre").val('');

                }
            }
        });
    
}

function actualizarEspecificaciones() {
    var id = $("#id").val();
    var nombre = $("#nombre").val();

    if (nombre !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 105,
                id: $("#id").val(),
                nombre: $("#nombre").val(),
                descripcion: $("#descripcion").val()
           },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#div_Especificaciones").dialog('close');
                    $("#id").val('');
                    $("#nombre").val('');
                    //$("#descripcion").val('');
}
                cargarEspecificaciones();
            }, error: function (result) {
                alert('No se puede actualizar, verifique los datos e intente nuevamente');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}
