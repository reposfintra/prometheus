/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    tablaModificacion();

    $("#buscar").click(function () {
        tablaModificacion();
    });

});


function tablaModificacion() {
    var grid_tabla = jQuery("#tabla_cambio");
    if ($("#gview_tabla_cambio").length) {
        reloadGridTabla(grid_tabla, 67);
    } else {
        grid_tabla.jqGrid({
            //caption: "Facturas Parciales",
            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
            datatype: "json",
            height: '150',
            width: '1530',
            colNames: ['Id Accion', 'Precio Venta', '% Descuento', 'Valor Descuento', '% Rentabilidad Contratista', 'Valor Rentabilidad Contratista', '% Rentabilidad Esquema', 'Valor Rentabilidad Esquema', 'Valor Rentabilidad Contratista', 'Subtotal', '% Iva', 'Valor Iva', '% Administracion', 'Administracion', '% Imprevisto', 'Imprevisto', '% Utilidad', 'Utilidad', '% AIU', 'Valor AIU', 'Modalidad', 'Total',
                'Anticipo', 'Anticipo ', '% anticipo', 'Valor Anticipo', 'Retegarantia', 'Retegarantia ', '% Retegrantia', 'Presupuesto Terminado', 'Presupuesto Terminado'],
            colModel: [
                {name: 'id_accion', index: 'id_accion', width: 120, align: 'left', key: true, hidden: false},
                {name: 'valor_cotizacion', index: 'valor_cotizacion', sortable: true, editable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {//dataInit: function (elem) {
//                            $(elem).keypress(function (e) {
//                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
//                                    return true;
//                                }
//                                if (e.which >= 58) {
//                                    return false;
//                                }
//                            });
//                        }
                    }
                },
                {name: 'perc_descuento', index: 'perc_descuento', sortable: true, width: 140, align: 'right', editable: true, search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: " "}},
                {name: 'valor_descuento', index: 'valor_descuento', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'perc_rentabilidad_contratista', index: 'perc_rentabilidad_contratista', sortable: true, width: 140, align: 'right', editable: true, search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: " "}},
                {name: 'valor_rentabilidad_contratista', index: 'valor_rentabilidad_contratista', sortable: true, editable: false, width: 150, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'perc_rentabilidad_esquema', index: 'perc_rentabilidad_esquema', sortable: true, width: 140, align: 'right', editable: true, search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: " "}},
                {name: 'valor_rentabilidad_esquema', index: 'valor_rentabilidad_esquema', sortable: true, editable: false, width: 120, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'distribucion_rentabilidad_esquema', index: 'distribucion_rentabilidad_esquema', width: 120, align: 'left', hidden: false, editable: false},
                {name: 'subtotal', index: 'subtotal', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'perc_iva', index: 'perc_iva', sortable: true, width: 140, align: 'center', editable: true, edittype: 'select', editrules: {required: true},
                    editoptions: {value: {'0': '0', '16': '16'},
                        dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        //var rowid = e.target.id.replace("_anticipo_", "");
                                        //jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'anticipo', e.target.value);
                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }},
                {name: 'valor_iva', index: 'valor_iva', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'perc_administracion', index: 'perc_administracion', sortable: true, width: 140, align: 'right', editable: true, search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: " "}},
                {name: 'administracion', index: 'administracion', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'perc_imprevisto', index: 'perc_imprevisto', sortable: true, width: 140, align: 'right', editable: true, search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: " "}},
                {name: 'imprevisto', index: 'imprevisto', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'perc_utilidad', index: 'perc_utilidad', sortable: true, width: 140, align: 'right', editable: true, search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: " "}},
                {name: 'utilidad', index: 'utilidad', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'perc_aiu', index: 'perc_aiu', sortable: true, width: 140, align: 'right', search: false, editable: true, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: " "}},
                {name: 'valor_aiu', index: 'valor_aiu', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'modalidad', index: 'modalidad', width: 150, align: 'center', hidden: true, editable: false},
                {name: 'total', index: 'total', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'anticipo', index: 'anticipo', width: 150, align: 'center', hidden: true, editable: false},
                {name: 'anticipo_', index: 'anticipo_', width: 150, align: 'center', hidden: false, editable: true, edittype: 'select', editrules: {required: true},
                    editoptions: {value: {1: 'SI', 2: 'NO'},
                        dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        var rowid = e.target.id.replace("_anticipo_", "");
                                        jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'anticipo', e.target.value);
                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }},
                {name: 'perc_anticipo', index: 'perc_anticipo', sortable: true, width: 140, align: 'right', editable: true, search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: " "}},
                {name: 'valor_anticipo', index: 'valor_anticipo', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: " "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'retegarantia', index: 'retegarantia', width: 150, align: 'center', hidden: true, editable: false},
                {name: 'retegarantia_', index: 'retegarantia_', width: 150, align: 'center', hidden: false, editable: true, edittype: 'select', editrules: {required: true},
                    editoptions: {value: {1: 'SI', 2: 'NO'},
                        dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        var rowid = e.target.id.replace("_retegarantia_", "");
                                        jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'retegarantia', e.target.value);
                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }},
                {name: 'perc_retegarantia', index: 'perc_retegrantia', sortable: true, width: 140, align: 'right', editable: true, search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 4, prefix: ""}},
                {name: 'presupuesto_terminado', index: 'presupuesto_terminado', width: 150, align: 'center', hidden: true, editable: false},
                {name: 'presupuesto_terminado_', index: 'presupuesto_terminado_', sortable: true, editable: true,
                    formatter: 'checkbox', align: 'center', edittype: 'checkbox', width: 150,
                    editoptions: {value: 'true:false', defaultValue: 'true',
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowid = e.target.id.replace("_presupuesto_terminado_", "");

                                        if (e.target.checked) {
                                            jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'presupuesto_terminado', 'S');
                                        } else {
                                            jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'presupuesto_terminado', 'N');
                                        }
                                    } catch (exc) {
                                    }
                                    return;
                                }}]}}
            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            pager: 'pager',
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 67,
                    id_solicitud: $('#solicitud').val()
                }
            },
            loadComplete: function (rowid, e, iRow, iCol) {
                $("#tabla_cambio").contextMenu('menu2', {
                    bindings: {
                        'eliminar': function (rowid) {
                            var myGrid = jQuery("#tabla_cambio"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;

                            $('#tabla_cambio').jqGrid('delRowData', selRowIds);
                            $('#eliminar').removeAttr("disabled").removeClass('ui-state-disabled');

                        }
                    }, onContexMenu: function (event/*, menu*/) {

                    }
                });
            },
            gridComplete: function () {
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                jQuery("#tabla_cambio").jqGrid('editRow', rowid, true, function () {
                }, null, null, {}, function (rowid) {
                    var valor_descuento, valor_subtotal, valor_iva, valor_administracion, valor_imprevisto, valor_utilidad,
                            perc_aiu, valor_aiu, valor_total, perc_administracion, perc_imprevisto, perc_utilidad, valor_anticipo_, valor_rent_contratista, valor_rent_esquema;
                    var valor_cotizacion = $("#tabla_cambio").getRowData(rowid).valor_cotizacion, modalidad;
                    var perc_descuento = $("#tabla_cambio").getRowData(rowid).perc_descuento;
                    var perc_iva = $("#tabla_cambio").getRowData(rowid).perc_iva;
                    var perc_anticipo = $("#tabla_cambio").getRowData(rowid).perc_anticipo;
                    var perc_rentabilidad_contratista = $("#tabla_cambio").getRowData(rowid).perc_rentabilidad_contratista;
                    var perc_rentabilidad_esquema = $("#tabla_cambio").getRowData(rowid).perc_rentabilidad_esquema;
                    valor_descuento = (valor_cotizacion * perc_descuento) / 100;
                    valor_rent_contratista = (parseFloat(valor_cotizacion) * parseFloat(perc_rentabilidad_contratista)) / 100;
                    valor_rent_esquema = ((parseFloat(valor_cotizacion) + parseFloat(valor_rent_contratista)) * parseFloat(perc_rentabilidad_esquema)) / 100;
                    valor_subtotal = (valor_cotizacion - valor_descuento) + valor_rent_contratista + valor_rent_esquema;

                    valor_iva = (valor_subtotal * perc_iva) / 100;
                    if (perc_iva !== '0') {
                        jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'perc_administracion', 0);
                        jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'perc_imprevisto', 0);
                        jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'perc_utilidad', 0);
                        valor_total = valor_subtotal + valor_iva;
                        valor_anticipo_ = (valor_subtotal * perc_anticipo) / 100;
                        modalidad = 0;
                    } else {
                        perc_administracion = $("#tabla_cambio").getRowData(rowid).perc_administracion;
                        perc_imprevisto = $("#tabla_cambio").getRowData(rowid).perc_imprevisto;
                        perc_utilidad = $("#tabla_cambio").getRowData(rowid).perc_utilidad;
                        valor_administracion = (valor_subtotal * perc_administracion) / 100;
                        valor_imprevisto = (valor_subtotal * perc_imprevisto) / 100;
                        valor_utilidad = (valor_subtotal * perc_utilidad) / 100;
                        perc_aiu = parseFloat(perc_administracion) + parseFloat(perc_imprevisto) + parseFloat(perc_utilidad);
                        valor_aiu = valor_administracion + valor_imprevisto + (valor_utilidad + ((valor_utilidad * 16) / 100));
                        valor_total = valor_subtotal + valor_aiu;
                        valor_anticipo_ = (valor_total * perc_anticipo) / 100;
                        modalidad = 1;
                    }

                    //alert(valor_anticipo_);
                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'valor_descuento', valor_descuento);
                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'subtotal', valor_subtotal);
                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'valor_iva', valor_iva);
                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'administracion', valor_administracion);
                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'imprevisto', valor_imprevisto);
                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'utilidad', valor_utilidad);
                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'perc_aiu', perc_aiu);
                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'valor_aiu', valor_aiu);
                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'total', valor_total);
                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'valor_anticipo', valor_anticipo_);
                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'modalidad', modalidad);
                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'valor_rentabilidad_contratista', valor_rent_contratista);
                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'valor_rentabilidad_esquema', valor_rent_esquema);
                });
                return;
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid('#pager', {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
//        jQuery("#tabla_cambio").navButtonAdd('#pager', {
//            caption: "Agregar",
//            title: "Agregar fila",
//            onClickButton: function () {
//                var grid = $("#tabla_cambio");
//                var rowid = 'neo_' + grid.getRowData().length;
////                var solicitud_ = $('#solicitud').val();
////                if (solicitud_ !== '') {
////                    var id_accion = buscarAccion($('#solicitud').val());
////                } else {
////                    id_accion = 0;
////                }
//                var defaultData = {id_accion: 0, valor_cotizacion: 0, valor_descuento: 0, subtotal: 0, perc_iva: 0, valor_iva: 0, administracion: 0, imprevisto: 0, utilidad: 0, perc_aiu: 0, valor_aiu: 0, total: 0,
//                    anticipo: 0, perc_anticipo: 0, retegarantia: 0, perc_retegrantia: 0};
//                grid.addRowData(rowid, defaultData);
//                grid.jqGrid('editRow', rowid, true, function () {
//                }, null, null, {}, function (rowid) {
//                    var valor_descuento, valor_subtotal, valor_iva, valor_administracion, valor_imprevisto, valor_utilidad,
//                            perc_aiu, valor_aiu, valor_total, perc_administracion, perc_imprevisto, perc_utilidad, valor_anticipo;
//                    var valor_cotizacion = $("#tabla_cambio").getRowData(rowid).valor_cotizacion;
//                    var perc_descuento = $("#tabla_cambio").getRowData(rowid).perc_descuento;
//                    var perc_iva = $("#tabla_cambio").getRowData(rowid).perc_iva;
//                    var perc_anticipo = $("#tabla_cambio").getRowData(rowid).perc_anticipo;
//                    valor_descuento = (valor_cotizacion * perc_descuento) / 100;
//                    valor_subtotal = valor_cotizacion - valor_descuento;
//                    valor_iva = (valor_subtotal * perc_iva) / 100;
//                    if (perc_iva !== 0) {
//                        jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'perc_administracion', 0);
//                        jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'perc_imprevisto', 0);
//                        jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'perc_utilidad', 0);
//                        valor_total = valor_subtotal + valor_iva;
//                    } else {
//                        perc_administracion = $("#tabla_cambio").getRowData(rowid).perc_administracion;
//                        perc_imprevisto = $("#tabla_cambio").getRowData(rowid).perc_imprevisto;
//                        perc_utilidad = $("#tabla_cambio").getRowData(rowid).perc_utilidad;
//                        valor_total = valor_subtotal + valor_aiu;
//                    }
//                    valor_administracion = (valor_subtotal * perc_administracion) / 100;
//                    valor_imprevisto = (valor_subtotal * perc_imprevisto) / 100;
//                    valor_utilidad = (valor_subtotal * perc_utilidad) / 100;
//                    perc_aiu = parseFloat(perc_administracion) + parseFloat(perc_imprevisto) + parseFloat(perc_utilidad);
//                    valor_aiu = valor_administracion + valor_imprevisto + valor_utilidad;
//                    valor_anticipo = (valor_subtotal * perc_anticipo) / 100;
//                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'valor_descuento', valor_descuento);
//                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'subtotal', valor_subtotal);
//                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'valor_iva', valor_iva);
//                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'administracion', valor_administracion);
//                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'imprevisto', valor_imprevisto);
//                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'utilidad', valor_utilidad);
//                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'perc_aiu', perc_aiu);
//                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'valor_aiu', valor_aiu);
//                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'total', valor_total);
//                    jQuery("#tabla_cambio").jqGrid('setCell', rowid, 'valor_anticipo', valor_anticipo);
//                });
//            }
//        });
        jQuery("#tabla_cambio").navButtonAdd('#pager', {
            caption: "Actualizar",
            title: "Actualizar cotizacion",
            onClickButton: function () {
                var solicitud = $('#solicitud').val();
                if (solicitud === '') {
                    mensajesDelSistema('Digitar el numero de solicitud', '300', 'auto', false);
                } else {
                    guardarCambio();
                }
            }
        });
    }
}

function reloadGridTabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: $('#solicitud').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function buscarAccion(id_solicitud) {
    var id_accion_;
    $.ajax({
        async: false,
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 65,
            id_solicitud: id_solicitud
        },
        success: function (json) {
//            $('#val_total').val(numberConComas(json[0].total));
            id_accion_ = json[0].id_accion;

        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return id_accion_;
}

function guardarCambio() {
    var grid = jQuery("#tabla_cambio"), filas = grid.jqGrid('getDataIDs');
    var data = jQuery("#tabla_cambio").jqGrid('getRowData');
    for (var i = 0; i < filas.length; i++) {
        console.log(filas[i]);
        grid.saveRow(filas[i]);
    }
    if (data.length === 0) {
        mensajesDelSistema('Inserte al menos una linea', '300', 'auto', false);
    } else {
        $.ajax({
            async: false,
            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 66,
                informacion: JSON.stringify({json: data, id_solicitud: $('#solicitud').val()})
            },
            success: function (json) {
                if (json.respuesta === 'Guardado') {
                    tablaModificacion();
                    mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);

                } else if (json.respuesta === 'ERROR') {
                    mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
                } else {
                    mensajesDelSistema(json.respuesta, '300', 'auto', false);
                }
                    
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" + "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }

}

function mensajesDelSistema(msj, width, height) {
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}