/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//$(document).ready();
function init1() {
    resultadoPropietario();
}
function init2() {
    cargarEds();
}

function init3() {
    cargarPropietario();
    cargarProductos();
}

function init4() {
    cargarProductosEds();
}

function init5() {
    cargarBanderasEds();
}
function init6() {
    cargarGrillaUniMedida();
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

var usus;
var prov;
var validar;
function cargarPais() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 4
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#pais').html('');
                $('#paisedit').html('');
                $('#pais').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#pais').append('<option value=' + datos + '>' + json[datos] + '</option>');
                    $('#paisedit').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarUnidadMedida() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 16
        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#unidad').html('');
                $('#unidadpro').html('');
                for (var datos in json) {
                    $('#unidad').append('<option value=' + datos + '>' + json[datos] + '</option>');
                    $('#unidadpro').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarDepartamento(id) {
    var pais = $("#" + id).val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 5,
            pais: pais
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#departamento').html('');
                $('#departamentoedit').html('');
                $('#departamento').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#departamento').append('<option value=' + datos + '>' + json[datos] + '</option>');
                    $('#departamentoedit').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCiudad(id) {
    var departamento = $("#" + id).val();
    //var departamentoedit = $("#departamentoedit").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 7,
            departamento: departamento
                    // departamentoedit: departamentoedit
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#ciudad').html('');
                $('#ciudadedit').html('');
                $('#ciudad').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#ciudad').append('<option value=' + datos + '>' + json[datos] + '</option>');
                    $('#ciudadedit').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarBandera() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 8
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                for (var datos in json) {
                    $('#idbandera').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarPropietario() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 9
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#idpropietario').html('');
                for (var datos in json) {
                    $('#idpropietario').append('<option value=' + json[datos].id + '>' + json[datos].razon_social + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarProductos() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 14
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                for (var datos in json) {
                    $('#opciones').append(' <input type="checkbox" name="opcion"  value=' + datos + '>' + json[datos] + '<br>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarComboEds() {
    var idpropietario = $("#idpropietario").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 13,
            idpropietario: idpropietario
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#estaciones').html('');
                for (var datos in json) {
                    $('#estaciones').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function guardarEds() {
    var idpropietario = $("#idpropietario").val();
    var idbandera = $("#idbandera").val();
    var nombreds = $("#nombreds").val(); //eds
    var niteds = $("#nitestacion").val();
    var direccion = $("#direccioneds").val();
    var correo = $("#correo").val();
    var telefono = $("#telefonoeds").val(); //
    var idusuario = $("#idusu").val();
    var nit = $("#nit").val();
    var pais = $("#pais").val();
    var departamento = $("#departamento").val();
    var ciudad = $("#ciudad").val();
    var passw = $("#pas1").val();
    var passw2 = $("#pas2").val();
    var cambioclav = $("#actdes").val();
    var nombrencargado = $("#nombreencargado").val();
    // var dpagof = $("#dpagof").val();
    if ((nombreds != "") && (direccion != "") && (correo != "") && (telefono != "") && (nit != "") && (nombrencargado != "") && (nit != "") && (idusuario != "") && (passw != "") && (passw2 != "") && (pais != "") && (departamento != "") && (ciudad != "") && (niteds != "")) {
        expr = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!expr.test(correo)) {
            mensajesDelSistema("DEBE CAMBIAR EL CORREO", '230', '150', false);
        } else if (usus.trim() === "OK") {
            if ($("#dialogMsjSMS").html() === "") {
                $.ajax({
                    type: 'POST',
                    url: "./controller?estado=Administracion&accion=Eds",
                    data: {
                        opcion: 6,
                        idpropietario: idpropietario,
                        idbandera: idbandera,
                        nombreds: nombreds,
                        niteds: niteds,
                        direccion: direccion,
                        correo: correo,
                        telefono: telefono,
                        nit: nit,
                        idusuario: idusuario,
                        passw: passw,
                        cambioclav: cambioclav,
                        nombrencargado: nombrencargado,
                        pais: pais,
                        ciudad: ciudad
                                // dpagof: dpagof
                    },
                    success: function (data) {
                        mensajesDelSistema("EXITO AL GUARDAR EDS ", '230', '150', true);
                        $("#nombreds").val(""); //eds
                        $("#nitestacion").val("");
                        $("#direccioneds").val("");
                        $("#correo").val("");
                        $("#telefonoeds").val(""); //
                        $("#idusu").val("");
                        $("#nit").val("");
                        $("#pas1").val("");
                        $("#pas2").val("");
                        // $("#dpagof").val("");
                        $("#nombreencargado").val("");
                        cargarEds();
                    }, error: function (result) {
                        alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
                    }
                });
            } else {
                mensajesDelSistema("LAS CONTRASE�AS SON DIFERENTES", '300', '150', true);
            }
        } else {
            mensajesDelSistema("EL USUARIO YA EXISTE", '230', '150', false);
        }

    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function actualizarEds() {
    $("#nitestacion").attr('readOnly', true);
    $("#idusu").attr('readOnly', true);
    var nombreds = $("#nombreds").val();
    var niteds = $("#nitestacion").val();
    var id = $("#ideds").val();
    var direccion = $("#direccioneds").val();
    var encargado = $("#nombreencargado").val();
    var nit = $("#nit").val();
    var telefono = $("#telefonoeds").val();
    var correo = $("#correo").val();
    var clave = $("#pas1").val();
    var passw2 = $("#pas2").val();
    var idusuario = $("#idusu").val();
    var pais = $("#pais").val();
    var departamento = $("#departamento").val();
    var ciudad = $("#ciudad").val();
    var idusuarioedit = $("#idusuarioedit").val();
    // var dpagof = $("#dpagof").val();

    if ((nombreds != "") && (direccion != "") && (correo != "") && (telefono != "") && (nit != "") && (encargado != "") && (nit != "") && (idusuario != "") && (clave != "") && (passw2 != "") && (pais != "") && (departamento != "") && (ciudad != "") && (niteds != "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Eds",
            data: {
                opcion: 11,
                nombreds: nombreds,
                niteds: niteds,
                id: id,
                direccion: direccion,
                encargado: encargado,
                nit: nit,
                telefono: telefono,
                correo: correo,
                clave: clave,
                passw2: passw2,
                idusuario: idusuario,
                pais: pais,
                departamento: departamento,
                ciudad: ciudad,
                idusuarioedit: idusuarioedit
                        //  dpagof: dpagof
            },
            success: function (data) {
                $("#nitestacion").attr('readOnly', false);
                $("#idusu").attr('readOnly', false);
                cargarEds();
                mensajesDelSistema("EXITO AL ACTUALIZAR EDS ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function actualizarProducto() {
    var nombreproducto = $("#nombreprop").val();
    var unidadmedida = $("#unidadpro").val();
    var id = $("#idpro").val();
    if ((nombreproducto !== "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Eds",
            data: {
                opcion: 19,
                nombreproducto: nombreproducto,
                unidadmedida: unidadmedida,
                id: id
            },
            success: function (data) {
                cargarProductosEds();
                $("#nompro").val("");
                mensajesDelSistema("EXITO AL ACTUALIZAR PRODUCTO ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function guardarProductoEds() {
    var nomproducto = $("#nompro").val();
    var unidad = $("#unidad").val();
    if ((nomproducto != "") && (unidad != "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Eds",
            data: {
                opcion: 17,
                nomproducto: nomproducto,
                unidad: unidad
            },
            success: function (data) {
                cargarProductosEds();
                $("#nompro").val("");
                mensajesDelSistema("EXITO AL GUARDAR PRODUCTO DE EDS ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function cambiarEstadoEDS(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        data: {
            opcion: 12,
            id: id
        },
        success: function (data) {
            cargarEds();
            //mensajesDelSistema("EXITO EN EL CAMBIO DE ESTADO EDS", '230', '150', true);
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}

function cambiarEstadoProp(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        data: {
            opcion: 18,
            id: id
        },
        success: function (data) {
            cargarProductosEds();
            //mensajesDelSistema("EXITO EN EL CAMBIO DE ESTADO EDS", '230', '150', true);
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}

function guardarPropietarioEds() {
    $("#nitem").readOnly = false;
    var nombreprop = $("#nombreprop").val();
    var direccion = $("#direccion").val();
    var correo = $("#correo").val();
    var tipopersona = $("#tipopersona").val();
    var telefono = $("#telefono").val();
    var pais = $("#pais").val();
    var departamento = $("#departamento").val();
    var ciudad = $("#ciudad").val();
    var estadousu = $("#estadousu").val();
    var idusuario = $("#idusu").val();
    var passw = $("#password").val();
    var passw2 = $("#password2").val();
    var cambioclav = $("#actdes").val();
    var representante = $("#representante").val();
    var nit = $("#nit").val();
    //var nitem = $("#nitem").val();
    var tipodoc = $("#tipodoc").val();
    var gcontribuyente = $("#gcontribuyente").val();
    var iva = $("#iva").val();
    var retefuente = $("#retefuente").val();
    var ica = $("#ica").val();
    var autoretenedor = $("#autoretenedor").val();
    var numcuenta = $("#cuenta").val();
    var banco = $("#banco").val();
    var sedepago = $("#sedepago").val();
    var banagencia = $("#banagencia").val();
    var tcuenta = $("#tcuenta").val();
    var bancotransfer = $("#bancotransfer").val();
    var ptransfer = document.getElementById("ptransfer").checked; //$("input['checbox']:checked").length;
    var suctransfer = $("#sucursal").val();
    var hc = $("#hc").val();
    var regimen = $("#regimen").val();
    var nombre_cuenta = $("#nombre_cuenta").val();
    var cedula_cuenta = $("#cedula_cuenta").val();
    var operacion = $("#operacion").val();
    var existe = $("#existe").val();
    var codcli = $("#codcli").val();
    var digitover = $("#digitover").val();
    if (tipodoc === 'NIT') {
        var documento = $("#nitem").val();
        var dv = $("#digitover").val();
        var nitem = documento + dv;
    } else {
        var nitem = $("#nitem").val();
    }
    if (ptransfer === true) {
        ptransfer = 2;
    } else {
        ptransfer = 1;
    }
    if ((nombreprop !== "") && (direccion !== "") && (correo !== "") && (telefono !== "") && (nit !== "") && (idusuario !== "") && (passw !== "") && (passw2 !== "") && (representante !== "") && (nitem !== "") && (pais !== "") && (departamento !== "") && (ciudad !== "") && (tipodoc !== "") && (numcuenta !== "")) {
        expr = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!expr.test(correo)) {
            mensajesDelSistema("DEBE CAMBIAR EL CORREO", '230', '150', false);
        } else if (usus.trim() === "OK") {
            if ($("#dialogMsjSMS").html() === "") {
                $.ajax({
                    type: 'POST',
                    url: "./controller?estado=Administracion&accion=Eds",
                    data: {
                        opcion: 1,
                        nombreprop: nombreprop,
                        direccion: direccion,
                        correo: correo,
                        tipopersona: tipopersona,
                        telefono: telefono,
                        pais: pais,
                        ciudad: ciudad,
                        nit: nit,
                        perfil: "PROPIETARIO",
                        estadousu: estadousu,
                        idusuario: idusuario,
                        passw: passw,
                        cambioclav: cambioclav,
                        representante: representante,
                        nitem: nitem,
                        tipodoc: tipodoc,
                        gcontribuyente: gcontribuyente,
                        iva: iva,
                        retefuente: retefuente,
                        ica: ica,
                        autoretenedor: autoretenedor,
                        banco: banco,
                        sedepago: sedepago,
                        banagencia: banagencia,
                        tcuenta: tcuenta,
                        ptransfer: ptransfer,
                        bancotransfer: bancotransfer,
                        numcuenta: numcuenta,
                        hc: hc,
                        suctransfer: suctransfer,
                        regimen: regimen,
                        nombre_cuenta: nombre_cuenta,
                        cedula_cuenta: cedula_cuenta,
                        operacion: operacion,
                        existe: existe,
                        codcli: codcli,
                        digitover: digitover
                    },
                    success: function (data) {
                        resultadoPropietario();
                        mensajesDelSistema("EXITO AL GUARDAR PROPIETARIO EDS ", '230', '150', true);
                        $("#nombreprop").val("");
                        $("#direccion").val("");
                        $("#correo").val("");
                        $("#telefono").val("");
                        $("#nit").val("");
                        $("#idusu").val("");
                        $("#password").val("");
                        $("#password2").val("");
                        $("#representante").val("");
                        $("#nitem").val("");
                        $("#cuenta").val("");
                        $("#nombre_cuenta").val("");
                        $("#cedula_cuenta").val("");
                        $("#digitover").val("");
                        $("#operacion").val("");
                        $("#existe").val("");
                        $("#codcli").val("");
                        $("#digitover").val("");
                    },
                    error: function (result) {
                        alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
                    }
                });
            } else {
                mensajesDelSistema("LAS CONTRASE�AS SON DIFERENTES", '300', '150', true);
            }
        } else {
            mensajesDelSistema(" YA EXISTE", '230', '150', false);
        }
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargarEds() {
    var operacion;
    var grid_mostrar_eds = jQuery("#tabla_Eds");
    if ($("#gview_tabla_Eds").length) {
        reloadGridMostrarEds(grid_mostrar_eds, 3);
    } else {
        grid_mostrar_eds.jqGrid({
            caption: "GESTION ESTACIONES DE SERVICIOS",
            url: "./controller?estado=Administracion&accion=Eds",
            mtype: "POST",
            datatype: "json",
            height: '450',
            width: '1090',
            colNames: ['id Estacion', 'Id bandera', 'Bandera', 'Nombre Estacion', 'Nit Estacion', 'Nombre Propietario', 'Municipio', 'Cod_ciudad', 'departamento', 'pais', 'Direccion', 'Usuario EDS', 'Nombre encargado', 'Cc Encargado', 'Telefono', 'Correo', 'Password', 'Estado'],
            colModel: [
                {name: 'edsid', index: 'edsid', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'banId', index: 'banId', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'banRazonsocial', index: 'banRazonsocial', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'edsnombre', index: 'edsnombre', width: 200, sortable: true, align: 'left', hidden: false},
                {name: 'edsnitestacion', index: 'edsnitestacion', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'nombreprop', index: 'nombreprop', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'edsciudad', index: 'edsciudad', width: 100, align: 'left'},
                {name: 'codCiudad', index: 'codCiudad', width: 100, align: 'left', hidden: true},
                {name: 'codDepartamento', index: 'codDepartamento', width: 100, align: 'left', hidden: true},
                {name: 'codPais', index: 'codPais', width: 100, align: 'left', editable: true, hidden: true},
                {name: 'edsdireccion', index: 'edsdireccion', width: 170, align: 'left'},
                {name: 'edsusuario', index: 'edsusuario', sortable: true, width: 100, align: 'left'},
                {name: 'edsencargado', index: 'edsencargado', sortable: true, width: 100, align: 'left', hidden: true},
                {name: 'edsnitencargado', index: 'edsnitencargado', sortable: true, width: 100, align: 'left', hidden: true},
                {name: 'edstelefono', index: 'edstelefono', sortable: true, width: 100, align: 'left', hidden: true},
                {name: 'edscorreo', index: 'edscorreo', sortable: true, width: 100, align: 'left', hidden: true},
                {name: 'edspass', index: 'edspass', sortable: true, width: 100, align: 'left', hidden: true},
                {name: 'edsestado', index: 'edsestado', sortable: true, width: 100, align: 'left', hidden: false}
            ],
            gridComplete: function () {
                var cant = jQuery("#tabla_Eds").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cl = cant[i];
                    e = "<input style='height:20px;width:50px;' type='button' value='Editar' onclick=\"mostrar('" + cl + "');\" />";
                    jQuery("#tabla_Eds").jqGrid('setRowData', cant[i], {ver: e});
                }
            },
            subGrid: true,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 3
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_Eds").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 0;
                ventana(operacion);
                cargarPropietario();
                cargarBandera();
                cargarPais();
                cargarDepartamento('pais');
                cargarCiudad('departamento');
            }
        });
        $("#tabla_Eds").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_Eds"), i, selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                //jQuery("#tabla_Eds").jqGrid("getLocalRow", jQuery("#tabla_Eds").jqGrid("getGridParam", "selrow"))
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var nombre = filas.edsnombre;
                var niteds = filas.edsnitestacion;
                var direccion = filas.edsdireccion;
                var id = filas.edsid;
                var codpais = filas.codPais;
                var coddepartamento = filas.codDepartamento;
                var codciudad = filas.codCiudad;
                var estado = filas.edsestado;
                var idusuario = filas.edsusuario;
                var encargado = filas.edsencargado;
                var nitencargado = filas.edsnitencargado;
                var clave = filas.edspass;
                var correo = filas.edscorreo;
                var telefono = filas.edstelefono;
                var idusuarioedit = filas.edsusuario;
                // var dpagof = filas.dpagof;
                operacion = 1;//editar
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "Activo") {
                        ventana(operacion, nombre, niteds, direccion, id, idusuario, encargado, nitencargado, telefono, correo, clave, idusuarioedit, codpais, coddepartamento, codciudad);
                        // ventanaEditar(nombre, direccion, id, codpais, coddepartamento, codciudad, operacion);
                    } else {
                        mensajesDelSistema("PARA EDITAR LA EDS, EL ESTADO DEBE ESTAR ACTIVO", '300', '150', false);
                    }
                }
            }
        });
        $("#tabla_Eds").navButtonAdd('#pager', {
            caption: "Activar / Inactivar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_Eds"), i, selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.edsid;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    Confirmacion("�ESTA SEGURO QUE DESEA CAMBIAR EL ESTADO?", '300', '150', id);
                }
            }
        });
    }
    if (true) {
        $("#tabla_Eds").setGridParam({
            subGridRowExpanded: function (subgrid_id, row_id) {
                var subgrid_tabla_Eds, pager_id;
                subgrid_tabla_Eds = subgrid_id + "_t";
                pager_id = "p_" + subgrid_tabla_Eds;
                $("#" + subgrid_id).html("<table id='" + subgrid_tabla_Eds + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                jQuery("#" + subgrid_tabla_Eds).jqGrid({
                    url: "./controller?estado=Administracion&accion=Eds&id=" + row_id,
                    datatype: "json",
                    colNames: ['Producto', 'Descuento Fintra', 'Acuerdo Comercial', 'Procentaje Comision', 'Valor Comision'],
                    colModel: [
                        {name: "confproduc", index: "confproduc", width: 80, sortable: false},
                        {name: "confcomision", index: "confproduc", width: 65, sortable: false},
                        {name: "confdescripcion", index: "confdescripcion", width: 170, align: "left", sortable: false},
                        {name: "confporcentaje", index: "confporcentaje", width: 55, align: "left", sortable: false},
                        {name: "confvalor", index: "confvalor", width: 45, align: "left", sortable: false}
                    ],
                    rowNum: 100,
                    pager: pager_id,
                    sortname: 'num',
                    width: '944',
                    height: '119',
                    pgtext: null,
                    pgbuttons: false,
                    ajaxGridOptions: {
                        dataType: "json",
                        type: "get",
                        data: {
                            opcion: 24
                        }
                    }
                });
                // jQuery("#" + subgrid_tabla_Eds).jqGrid('navGrid', "#" + pager_id, {edit: false, add: false, del: false})
            }
        });
    }
}

function cargarProductosEds() {
    var grid_tabla_productos = jQuery("#tabla_productos");
    if ($("#gview_tabla_productos").length) {
        reloadGridMostrarEds(grid_tabla_productos, 15);
    } else {
        grid_tabla_productos.jqGrid({
            caption: "PRODUCTOS DE ESTACIONES DE SERVICIO",
            url: "./controller?estado=Administracion&accion=Eds",
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '720',
            colNames: ['id Producto', 'Id Unidad', 'Cod Producto', 'Producto', 'Unidad de Medida', 'Estado'],
            colModel: [
                {name: 'proId', index: 'proId', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'unid', index: 'unid', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'proCodProducto', index: 'proCodProducto', width: 130, sortable: true, align: 'left', hidden: false},
                {name: 'proDescripcion', index: 'proDescripcion', width: 200, sortable: true, align: 'left', hidden: false},
                {name: 'uniNombreUnidad', index: 'edsnombre', width: 230, sortable: true, align: 'left', hidden: false},
                {name: 'proEstado', index: 'proEstado', width: 120, sortable: true, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 15
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_productos").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                ventanaProducto();
                cargarUnidadMedida();
            }
        });
        $("#tabla_productos").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_productos"), i, selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.proId;
                var nombreprop = filas.proDescripcion;
                var coduni = filas.unid;
                var estado = filas.proEstado;

                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "Activo") {
                        ventanaEditarproducto(nombreprop, id, coduni);
                    } else {
                        mensajesDelSistema("PARA EDITAR LA EDS, EL ESTADO DEBE SER ACTIVO", '300', '150', false);
                    }
                }
            }
        });
        $("#tabla_productos").navButtonAdd('#pager', {
            caption: "Activar / Inactivar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_productos"), i, selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.proId;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    ConfirmacionPro("�ESTA SEGURO QUE DESEA CAMBIAR EL ESTADO?", '300', '150', id);
                }
            }
        });
    }
}

function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function ventana(operacion, nombre, niteds, direccion, id, idusuario, encargado, nitencargado, telefono, correo, clave, idusuarioedit, codpais, coddepartamento, codciudad) {
    if (operacion == 0) {
        $("#dialogMsjEds").dialog({
            width: '560',
            height: '580',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    guardarEds();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    } else {
        if (operacion == 1) {
            $("#nitestacion").attr('readOnly', true);
            $("#idusu").attr('readOnly', true);
            $("#nombreds").val(nombre);
            $("#nitestacion").val(niteds);
            $("#direccioneds").val(direccion);
            $("#ideds").val(id);
            $("#nombreencargado").val(encargado);
            $("#nit").val(nitencargado);
            $("#telefonoeds").val(telefono);
            $("#correo").val(correo);
            $("#pas1").val(clave);
            $("#pas2").val(clave);
            $("#idusu").val(idusuario);
            $("#idusuarioedit").val(idusuarioedit);
            // $("#dpagof").val(dpagof);
            cargarPais();
            $("#pais").val(codpais);
            cargarDepartamento('pais');
            $("#departamento").val(coddepartamento);
            cargarCiudad('departamento');
            $("#ciudad").val(codciudad);
            $("#labelprop").hide();
            $("#idpropietario").hide();
            $("#labelban").hide();
            $("#idbandera").hide();

            $("#dialogMsjEds").dialog({
                width: '560',
                height: '515',
                show: "scale",
                hide: "scale",
                resizable: false,
                position: "center",
                modal: true,
                closeOnEscape: false,
                buttons: {
                    "Editar": function () {
                        actualizarEds();
                    },
                    "Salir": function () {
                        $(this).dialog("close");
                        $("#nombreds").val("");
                        $("#nitestacion").val("");
                        $("#direccioneds").val("");
                        $("#ideds").val("");
                        $("#nombreeds").val("");
                        $("#direccion").val("");
                        $("#ideds").val("");
                        $("#nombreencargado").val("");
                        $("#nit").val("");
                        $("#telefonoeds").val("");
                        $("#correo").val("");
                        $("#pas1").val("");
                        $("#pas2").val("");
                        $("#idusu").val("");
                        //  $("#dpagof").val("");
                        $("#labelprop").show();
                        $("#idpropietario").show();
                        $("#labelban").show();
                        $("#idbandera").show();
                        $("#dialogMsjSMS").html("");
                        $("#sms2").html("");
                        $("#nitestacion").attr('readOnly', false);
                        $("#idusu").attr('readOnly', false);
                    }
                }
            });
        }
    }
    $("#dialogMsjEds").siblings('div.ui-dialog-titlebar').remove();
}

function ventanaProducto() {

    $("#dialogMsjproducto").dialog({
        width: '460',
        height: '230',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Guardar": function () {
                guardarProductoEds();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
    $("#dialogMsjproducto").siblings('div.ui-dialog-titlebar').remove();
}

function ventanaEditarproducto(nombreprop, id, coduni) {
    $("#nombreprop").val(nombreprop);
    $("#idpro").val(id);
    cargarUnidadMedida();
    $("#unidadpro").val(coduni);

    $("#dialogEdsEditarProp").dialog({
        width: '560',
        height: '220',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                actualizarProducto();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
    $("#dialogEdsEditarProp").siblings('div.ui-dialog-titlebar').remove();
}

function Confirmacion(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {
                cambiarEstadoEDS(id);
                $(this).dialog("destroy");
            },
            "Cancelar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function ConfirmacionPro(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                cambiarEstadoProp(id);
                $(this).dialog("destroy");
            },
            "Cancelar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function validarpass() {
    var clave = $("#pas1").val();
    var clave1 = $('#pas2').val();
    var clave2 = $('#password').val();
    var clave3 = $('#password2').val();
    if (clave !== clave1) {
        $("#dialogMsjSMS").html("La contrase�a es diferente");
        return false;
    }
    if (clave2 !== clave3) {
        $("#dialogMsjSMS").html("La contrase�a es diferente");
        return false;
    }
    $("#dialogMsjSMS").html("");
    return true;
}

function reloadGridMostrarEds(grid_mostrar_eds, opcion) {
    grid_mostrar_eds.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Eds",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_mostrar_eds.trigger("reloadGrid");
}

function verificarUsuario() {
    var idusuario = $("#idusu").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        datatype: 'text',
        data: {
            opcion: 10,
            idusuario: idusuario
        },
        success: function (data) {
            // $("#sms2").value = data;
            if (data.trim() !== "OK") {
                $("#sms2").html(data);
                $("#sms2").html("");
            } else {
                $("#sms2").html("");

            }
            if (validar === 0) {
                usus === 'OK';
                $("#sms2").html("");
            } else {
                usus = data;
                $("#sms2").html(data);
            }
            //usus = data;
        }
    });
}


function verificarUsuarioeds() {
    var nit = $("#nit").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        datatype: 'text',
        data: {
            opcion: 41,
            nit: nit
        },
        success: function (data) {
            console.log(data.length);
            if (data.length > 1) {
                $("#idusu").val(data);
                //document.getElementById("idusu").name = 'ex';
                $("#sms2").html("");
                usus = 'OK';
                validar = 0;
                $("#idusu").attr('readOnly', true);
            } else {
                validar = 1;
                $("#idusu").attr('readOnly', false);
                $("#idusu").val("");

//                document.getElementById("idusu").name = '';
            }

        }
    });
}

//function verificarUsuarioPropietario() {
//    var nitem = $("#nitem").val();
////    var nitem = numero;
////    var digito;
//    $.ajax({
//        type: 'POST',
//        url: "./controller?estado=Administracion&accion=Eds",
//        datatype: 'text',
//        data: {
//            opcion: 42,
//            nitem: nitem
//        },
//        success: function (data) {
//            console.log(data.length);
//            if (data.length > 1) {
//                usus = 'OK';
//                $("#idusu").val(data);
//                $("#sms2").html("");
//                // $("#nitem").val(numero);
//                $("#idusu").attr('readOnly', true);
////                if (tipodoc === 'NIT') {
////                    digito = nitem.length;
////                    if (digito === 9) {
////                        alert("el nit solo puede ser de nueve digitos " + data);
////                    }
////                } else {
////                    ////es cedula
////                }
//
//            } else {
//                $("#idusu").val("");
//                $("#idusu").attr('readOnly', false);
////                if (digito <= 9) {
////                    alert("debe tener 10 digitos ");
////                }
//            }
//
//        }
//    });
//}

function verificarUsuarioPropietario() {
    var nitem = $("#nitem").val();
    var tipodocumento = $("#tipodoc").val();

//    var nitem = numero;
//    var digito;
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        datatype: 'json',
        data: {
            opcion: 42,
            nitem: nitem,
            tipodocumento: tipodocumento
        },
        success: function (json) {
            // console.log(json.length);
            console.log(json);
            var valor = (json.length);
            //alert(valor);
//            usus = 'OK';
//            $("#idusu").val(data);
//            $("#sms2").html("");
//            $("#idusu").attr('readOnly', true);
            if (valor === 1) {
                usus = 'OK';
                $("#idusu").val(json);
                $("#sms2").html("");
                $("#idusu").attr('readOnly', true);

                $("#operacion").val("EDITAR");
                $("#existe").val(json[0].existente);
                $("#codcli").val(json[0].codcli);
                $("#nombreprop").val(json[0].nombreprop);
                $("#tipopersona").val(json[0].tipopersona);
                $("#tipodoc").val(json[0].tipodoc);
                $("#gcontribuyente").val(json[0].contribuyente);
                $("#iva").val(json[0].iva);
                $("#ica").val(json[0].ica);
                $("#retefuente").val(json[0].retefuente);
                $("#autoretenedor").val(json[0].autoretenedor);
                $("#hc").val(json[0].hc);
                $("#nombre_cuenta").val(json[0].nombre_cuenta);
                $("#cedula_cuenta").val(json[0].cedula_cuenta);
                $("#cuenta").val(json[0].numcuenta);
                cargarBanco('banco');
                $("#banco").val(json[0].banco);
                cargarAgencia('banco');
                $("#banagencia").val(json[0].agencia);
                $("#sedepago").val(json[0].sedebanco);
                $("#tcuenta").val(json[0].tipocuenta);
                $("#idusu").val(json[0].idusuario);
                if (json[0].tipopago === 'T') {
                    document.getElementById("ptransfer").checked = true;
                    $("#sucursal").attr('disabled', false);
                    $("#bancotransfer").attr('disabled', false);
                    cargarBancotransfer();
                    $("#bancotransfer").val(json[0].bancotransfer);
                    cargarSucursal();
                    $("#sucursal").val(json[0].sucursaltransfer);
                }
                // document.getElementById("ptransfer").checked;

            } else if (valor === 0) {
                $("#idedit").val("");
                $("#idusuarioedit").val("");
                $("#id").val("");
                $("#nombreprop").val("");
                $("#direccion").val("");
                $("#correo").val("");
                $("#tipopersona").val("");
                $("#telefono").val("");
                $("#nit").val("");
                $("#idusu").val("");
                $("#password").val("");
                $("#password2").val("");
                $("#representante").val("");
                $("#gcontribuyente").val("");
                $("#iva").val("");
                $("#retefuente").val("");
                $("#ica").val("");
                $("#autoretenedor").val("");
                $("#tcuenta").val("");
                $("#regimen").val("");
                $("#cuenta").val("");
                $("#nombre_cuenta").val("");
                $("#cedula_cuenta").val("");
                $("#codcli").val("");
                $("#existe").val("");
                $("#operacion").val("");
                $("#idusu").val("");
                $("#idusu").attr('readOnly', false);
            } else if (valor > 1) {
                mensajesDelSistema("Existe mas de un proveedor con el mismo numero de identificacion, por favor contacte con el departamento de sistemas", '417', '150', true);
            }

            // $("#regimen").val(json[0].nombreprop);


//   for (var key in json) {
//           alert("Key :"+key + ' Value : ' + json[key]) ;
//        }
            //if (data.length > 1) {
//               
//
//            } else {
//               
//            }




        }
    });
}

function validarEmail(email) {
    //expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    expr = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (!expr.test(email))
        alert("Error: La direcci�n de correo es incorrecta.");
}

function cargarBanderasEds() {
    var operacion;
    var grid_tabla_banderas = jQuery("#tabla_banderas");
    if ($("#gview_tabla_banderas").length) {
        reloadGridMostrarEds(grid_tabla_banderas, 20);
    } else {
        grid_tabla_banderas.jqGrid({
            caption: "BANDERAS",
            url: "./controller?estado=Administracion&accion=Eds",
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '1090',
            colNames: ['Id Bandera', 'Cod Bandera', 'Tipo Persona', 'Nit', 'Razon Social', 'Direccion', 'Correo', 'CC Representante ', 'Representante Legal', 'Estado'],
            colModel: [
                {name: 'banId', index: 'banId', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'bancodbandera', index: 'bancodbandera', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'bantippersona', index: 'bantippersona', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'banNit', index: 'banNit', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'banRazonsocial', index: 'banRazonsocial', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'bandireccion', index: 'bandireccion', width: 190, sortable: true, align: 'left', hidden: false},
                {name: 'banCorreo', index: 'banCorreo', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'banDocRep', index: 'banDocRep', width: 150, sortable: true, align: 'left', hidden: true},
                {name: 'banRepresentante', index: 'banRepresentante', width: 230, sortable: true, align: 'left', hidden: false},
                {name: 'banestado', index: 'banestado', width: 120, sortable: true, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 20
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_banderas").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 0;
                ventanabandera(operacion);
            }
        });
        $("#tabla_banderas").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_banderas"), i, selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                operacion = 1;
                var id = filas.banId;
                var razonsoc = filas.banRazonsocial;
                var nit = filas.banNit;
                var tipper = filas.bantippersona;
                var direccion = filas.bandireccion;
                var correo = filas.banCorreo;
                var replegal = filas.banRepresentante;
                var docreplegal = filas.banDocRep;
                var estado = filas.banestado;

                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "Activo") {
                        ventanabandera(operacion, id, razonsoc, nit, tipper, direccion, correo, replegal, docreplegal);

                    } else {
                        mensajesDelSistema("PARA EDITAR LA EDS, EL ESTADO DEBE SER ACTIVO", '300', '150', false);
                    }
                }
            }
        });
        $("#tabla_banderas").navButtonAdd('#pager', {
            caption: "Activar / Inactivar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_banderas"), i, selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.banId;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    ConfirmacionCambioban("�ESTA SEGURO QUE DESEA CAMBIAR EL ESTADO?", '300', '150', id);
                }
            }
        });
    }
}

function ConfirmacionCambioban(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                cambiarEstadoBandera(id);
                $(this).dialog("destroy");
            },
            "Cancelar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cambiarEstadoBandera(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        data: {
            opcion: 21,
            id: id
        },
        success: function (data) {
            cargarBanderasEds();
            //mensajesDelSistema("EXITO EN EL CAMBIO DE ESTADO EDS", '230', '150', true);
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}

function ventanabandera(operacion, id, razonsoc, nit, tipper, direccion, correo, replegal, docreplegal) {
    if (operacion == 0) {
        $("#dialogMsjbandera").dialog({
            width: '460',
            height: '330',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    guardarBanderas();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    } else {
        if (operacion == 1) {
            $("#razonsocial").val(razonsoc);
            $("#nit").val(nit);
            $("#tipper").val(tipper);
            $("#direccion").val(direccion);
            $("#correo").val(correo);
            $("#replegal").val(replegal);
            $("#docreplegal").val(docreplegal);
            $("#codbandera").val(id);

            $("#dialogMsjbandera").dialog({
                width: '460',
                height: '330',
                show: "scale",
                hide: "scale",
                resizable: false,
                position: "center",
                modal: true,
                closeOnEscape: false,
                buttons: {
                    "Guardar": function () {
                        actualizarBandera();
                    },
                    "Salir": function () {
                        $(this).dialog("close");
                        $("#razonsocial").val("");
                        $("#nit").val("");
                        $("#direccion").val("");
                        $("#correo").val("");
                        $("#replegal").val("");
                        $("#docreplegal").val("");
                    }
                }
            });
        }
    }
    $("#dialogMsjbandera").siblings('div.ui-dialog-titlebar').remove();
}

function guardarBanderas() {
    var razonsoc = $("#razonsocial").val();
    var nit = $("#nit").val();
    var tipper = $("#tipper").val();
    var direccion = $("#direccion").val();
    var correo = $("#correo").val();
    var replegal = $("#replegal").val();
    var docrep = $("#docreplegal").val();

    if ((razonsoc != "") && (nit != "") && (direccion != "") && (correo != "") && (replegal != "") && (docrep != "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Eds",
            data: {
                opcion: 22,
                razonsoc: razonsoc,
                nit: nit,
                tipper: tipper,
                direccion: direccion,
                correo: correo,
                replegal: replegal,
                docrep: docrep
            },
            success: function (data) {
                cargarBanderasEds();
                $("#razonsocial").val("");
                $("#nit").val("");
                $("#direccion").val("");
                $("#correo").val("");
                $("#replegal").val("");
                $("#docreplegal").val("");

                mensajesDelSistema("EXITO AL GUARDAR LA BANDERA ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });

    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function actualizarBandera() {
    var idbandera = $("#codbandera").val();
    var razonsoc = $("#razonsocial").val();
    var nit = $("#nit").val();
    var tipper = $("#tipper").val();
    var direccion = $("#direccion").val();
    var correo = $("#correo").val();
    var replegal = $("#replegal").val();
    var docrep = $("#docreplegal").val();

    if ((razonsoc != "") && (nit != "") && (direccion != "") && (correo != "") && (replegal != "") && (docrep != "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Eds",
            data: {
                opcion: 23,
                idbandera: idbandera,
                razonsoc: razonsoc,
                nit: nit,
                tipper: tipper,
                direccion: direccion,
                correo: correo,
                replegal: replegal,
                docrep: docrep
            },
            success: function (data) {
                cargarBanderasEds();
                $("#razonsocial").val("");
                $("#nit").val("");
                $("#direccion").val("");
                $("#correo").val("");
                $("#replegal").val("");
                $("#docreplegal").val("");

                mensajesDelSistema("EXITO AL ACTUALIZAR BANDERA ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });

    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function cargarGrillaUniMedida() {
    var operacion;
    var grid_tabla_productos = jQuery("#tabla_unidadmedida");
    if ($("#gview_tabla_unidadmedida").length) {
        reloadGridMostrarEds(grid_tabla_productos, 25);
    } else {
        grid_tabla_productos.jqGrid({
            caption: "PRODUCTOS DE MEDIDA",
            url: "./controller?estado=Administracion&accion=Eds",
            mtype: "POST",
            datatype: "json",
            height: '200',
            width: '405',
            colNames: ['Id Unidad', 'Nombre Unidad', 'Unidad Medicion', 'Estado'],
            colModel: [
                {name: 'unid', index: 'unid', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'uniNombreUnidad', index: 'uniNombreUnidad', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'unimedicion', index: 'unimedicion', width: 130, sortable: true, align: 'left', hidden: false},
                {name: 'uniestado', index: 'uniestado', width: 120, sortable: true, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 25
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_unidadmedida").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 0;
                ventanaUnidadMedida(operacion);
            }
        });
        $("#tabla_unidadmedida").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                operacion = 1;
                var myGrid = jQuery("#tabla_unidadmedida"), i, selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.unid;
                var uniNombreUnidad = filas.uniNombreUnidad;
                var unimedicion = filas.unimedicion;
                var estado = filas.uniestado;

                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "Activo") {
                        ventanaUnidadMedida(operacion, id, uniNombreUnidad, unimedicion);
                    } else {
                        mensajesDelSistema("PARA EDITAR LA EDS, EL ESTADO DEBE SER ACTIVO", '300', '150', false);
                    }
                }
            }
        });
        $("#tabla_unidadmedida").navButtonAdd('#pager', {
            caption: "A / I",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_unidadmedida"), i, selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.unid;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    ConfirmacionUnidadMedida("�ESTA SEGURO QUE DESEA CAMBIAR EL ESTADO?", '300', '150', id);
                }
            }
        });
    }
}

function ventanaUnidadMedida(operacion, id, uniNombreUnidad, unimedicion) {
    var opt = {
        width: '460',
        height: '130',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    };
    if (operacion === 0) {
        $("#dialogMsjMedicion").dialog(opt);
        $("#dialogMsjMedicion").dialog({
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    guardarUnidadMedida();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#nomunmed").val("");
                    $("#medicion").val("");
                }
            }
        });
    } else {
        if (operacion === 1) {
            $("#nomunmed").val(uniNombreUnidad);
            $("#medicion").val(unimedicion);
            $("#idum").val(id);
            $("#dialogMsjMedicion").dialog(opt);
            $("#dialogMsjMedicion").dialog({
                buttons: {
                    "Guardar": function () {
                        actualizarUnidadMedida();
                    },
                    "Salir": function () {
                        $(this).dialog("close");
                        $("#nomunmed").val("");
                        $("#medicion").val("");
                        $("#idum").val("");
                    }
                }
            });
        }
    }
    $("#dialogMsjMedicion").siblings('div.ui-dialog-titlebar').remove();
}

function guardarUnidadMedida() {
    var nomunmed = $("#nomunmed").val();
    var medicion = $("#medicion").val();
    if ((medicion !== "") && (nomunmed !== "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Eds",
            data: {
                opcion: 26,
                nomunmed: nomunmed,
                medicion: medicion
            },
            success: function (data) {
                cargarGrillaUniMedida();
                $("#nomunmed").val("");
                $("#medicion").val("");
                mensajesDelSistema("EXITO AL GUARDAR UNIDAD DE MEDIDA ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function actualizarUnidadMedida() {
    var nomunmed = $("#nomunmed").val();
    var medicion = $("#medicion").val();
    var idum = $("#idum").val();
    if ((nomunmed != "") && (medicion != "") && (idum != "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Eds",
            data: {
                opcion: 27,
                nomunmed: nomunmed,
                medicion: medicion,
                idum: idum
            },
            success: function (data) {
                cargarGrillaUniMedida();
                $("#nomunmed").val("");
                $("#medicion").val("");
                mensajesDelSistema("EXITO AL ACTUALIZAR UNIDAD DE MEDIDA ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });

    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function ConfirmacionUnidadMedida(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                cambiarEstadoUnidadMedida(id);
                $(this).dialog("destroy");
            },
            "Cancelar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cambiarEstadoUnidadMedida(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        data: {
            opcion: 28,
            id: id
        },
        success: function (data) {
            cargarGrillaUniMedida();
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}

function cargarBanco(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 29
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                if (id == 'banco') {
                    $('#banco').html('');
                    for (var datos in json) {
                        $('#banco').append('<option value="' + datos + '">' + json[datos] + '</option>');
                    }
                } else {
                    $('#addbanco').html('');
                    for (var datos in json) {
                        $('#addbanco').append('<option value="' + datos + '">' + json[datos] + '</option>');
                    }
                }

//                $('#' + id).html('');
//                for (var datos in json) {
                //                    $('#' + id).append('<option value="' + datos + '">' + json[datos] + '</option>');


            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarAgencia(id) {
    var banco = $("#" + id).val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 30,
            banco: banco
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                if (id === 'addbanco') {
                    $('#addbanagencia').html('');
                    for (var datos in json) {
                        $('#addbanagencia').append('<option value="' + datos + '">' + json[datos] + '</option>');
                    }
                } else {
                    $('#banagencia').html('');
                    for (var datos in json) {
                        $('#banagencia').append('<option value="' + datos + '">' + json[datos] + '</option>');
                    }
                }

            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarSedePago() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 31
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#sedepago').html('');
                for (var datos in json) {
                    $('#sedepago').append('<option value="' + datos + '">' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarBancotransfer() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 32
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#bancotransfer').html('');
                $('#bancotransfer').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#bancotransfer').append('<option value="' + datos + '">' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarHC() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 33
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#hc').html('');
                for (var datos in json) {
                    $('#hc').append('<option value="' + datos + '">' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarSucursal() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 34
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {

                $('#sucursal').html('');
                $('#sucursal').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#sucursal').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function validarcheck() {
    //document.getElementById("ptransfer").checked;
    //$("input['checbox']:checked").length;
    var bancotransfer = document.getElementById("ptransfer").checked;
    if (bancotransfer !== false) {
        $("#sucursal").attr('disabled', false);
        $("#bancotransfer").attr('disabled', false);
    } else {
        $("#sucursal").attr('disabled', true);
        $("#bancotransfer").attr('disabled', true);
        $("#sucursal").val("");
        $("#bancotransfer").val("");
    }
}

function ventanaPropietario(operacion, id, nombreprop, nit, direccion, correo, tipopersona, telefono, pais, departamento, ciudad, idusuario, passw, representante,
        nitem, tipodoc, gcontribuyente, iva, ica, retefuente, autoretenedor, numcuenta, banco, sedepago, banagencia,
        tcuenta, bancotransfer, regimen, suctransfer, hc, ptransfer, codcli, nombrecuenta, cedulacuenta) {

    if (operacion === 0) {
        Vercampodv();
        document.getElementById("actdes").checked = true;
        cargarPais();
        cargarDepartamento();
        cargarCiudad();
        cargarBanco('banco');
        cargarAgencia('banco');
        cargarSedePago();
        cargarBancotransfer();
        cargarHC();
        cargarSucursal();
        validarcheck();

        $("#dialogMsjPropietario").dialog({
            width: '605',
            height: '870',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'REGISTRO PROPIETARIO EDS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarPropietarioEds();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#codcli").val("");
                    $("#operacion").val("");
                    $("#existe").val("");
                    $("#idedit").val("");
                    $("#idusuarioedit").val("");
                    $("#id").val("");
                    $("#nombreprop").val("");
                    $("#direccion").val("");
                    $("#correo").val("");
                    $("#tipopersona").val("");
                    $("#telefono").val("");
                    $("#nit").val("");
                    $("#idusu").val("");
                    $("#password").val("");
                    $("#password2").val("");
                    $("#representante").val("");
                    $("#nitem").val("");
                    $("#tipodoc").val("");
                    $("#gcontribuyente").val("");
                    $("#iva").val("");
                    $("#retefuente").val("");
                    $("#ica").val("");
                    $("#autoretenedor").val("");
                    $("#tcuenta").val("");
                    $("#regimen").val("");
                    $("#cuenta").val("");
                    $("#nombre_cuenta").val("");
                    $("#cedula_cuenta").val("");
                    $("#digitover").val("");
                }
            }
        });
    } else {
        if (operacion === 1) {
            document.getElementById('actdes').checked = false;
            $("#nitem").attr('readOnly', true);
            $("#idusu").attr('readOnly', true);
            $("#tipodoc").attr('disabled', true);
            $("#codcli").val(codcli);
            $("#idedit").val(nit);
            $("#idusuarioedit").val(idusuario);
            $("#id").val(id);
            $("#nombreprop").val(nombreprop);
            $("#direccion").val(direccion);
            $("#correo").val(correo);
            $("#tipopersona").val(tipopersona);
            $("#telefono").val(telefono);
            $("#nit").val(nitem);
            $("#idusu").val(idusuario);
            $("#password").val(passw);
            $("#password2").val(passw);
            $("#representante").val(representante);
            $("#nitem").val(nit);
            $("#tipodoc").val(tipodoc);
            $("#gcontribuyente").val(gcontribuyente);
            $("#iva").val(iva);
            $("#retefuente").val(retefuente);
            $("#ica").val(ica);
            $("#autoretenedor").val(autoretenedor);
            $("#tcuenta").val(tcuenta);
            $("#regimen").val(regimen);
            $("#cuenta").val(numcuenta);
            $("#nombre_cuenta").val(nombrecuenta);
            $("#cedula_cuenta").val(cedulacuenta);
            cargarBanco('banco');
            $("#banco").val(banco);
            cargarSedePago();
            $("#sedepago").val(sedepago);
            cargarAgencia('banco');
            $("#banagencia").val(banagencia);
            cargarBancotransfer();
            $("#bancotransfer").val(bancotransfer);
            cargarSucursal();
            $("#sucursal").val(suctransfer);
            cargarHC();
            $("#hc").val(hc);
            cargarPais();
            $("#pais").val(pais);
            cargarDepartamento('pais');
            $("#departamento").val(departamento);
            cargarCiudad('departamento');
            $("#ciudad").val(ciudad);
            $("#dv").attr('hidden', true);
            $("#digitover").attr('hidden', true);

            if (ptransfer === "T") {
                $("#ptransfer").attr("checked", true);
            } else {
                if (ptransfer === "B") {
                    $("#ptransfer").attr("checked", false);
                } else {
                    $("#ptransfer").attr("checked", false);
                }
            }
            $("#dialogo2").dialog('close');

            $("#dialogMsjPropietario").dialog({
                width: '605',
                height: '870',
                show: "scale",
                hide: "scale",
                resizable: false,
                position: "center",
                modal: true,
                closeOnEscape: false,
                title: 'REGISTRO PROPIETARIO EDS',
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Modificar": function () {
                        editarPropietarioEds();
                    },
                    "Salir": function () {
                        $(this).dialog("close");
                        $("#idedit").val("");
                        $("#idusuarioedit").val("");
                        $("#id").val("");
                        $("#nombreprop").val("");
                        $("#direccion").val("");
                        $("#correo").val("");
                        $("#tipopersona").val("");
                        $("#telefono").val("");
                        $("#nit").val("");
                        $("#idusu").val("");
                        $("#password").val("");
                        $("#password2").val("");
                        $("#representante").val("");
                        $("#nitem").val("");
                        $("#tipodoc").val("");
                        $("#gcontribuyente").val("");
                        $("#iva").val("");
                        $("#retefuente").val("");
                        $("#ica").val("");
                        $("#autoretenedor").val("");
                        $("#tcuenta").val("");
                        $("#regimen").val("");
                        $("#cuenta").val("");
                        $("#nombre_cuenta").val("");
                        $("#cedula_cuenta").val("");
                        $("#nitem").attr('readOnly', false);
                        $("#idusu").attr('readOnly', false);
                        $("#tipodoc").attr('disabled', false);
                        $("#digitover").val("");
                    }
                }
            });
        }
    }

    //  $("#dialogMsjPropietario").siblings('div.ui-dialog-titlebar').remove();

}

function resultadoPropietario() {
    var identificacion = $("#identificacion").val();
    var idusuario = $("#idusuario").val();
    var operacion;
    var grid_mostrar_eds = jQuery("#tabla_cargar_propietario");
    if ($("#gview_tabla_cargar_propietario").length) {
        //reloadGridMostrarEds(grid_mostrar_eds, 35);
        grid_mostrar_eds.setGridParam({
            datatype: 'json',
            url: "./controller?estado=Administracion&accion=Eds",
            ajaxGridOptions: {
                type: "GET",
                data: {
                    opcion: 35,
                    identificacion: identificacion,
                    idusuario: idusuario
                }
            }
        });
        grid_mostrar_eds.trigger("reloadGrid");
    } else {
        grid_mostrar_eds.jqGrid({
            caption: "PROPIETARIO",
            url: "./controller?estado=Administracion&accion=Eds",
            mtype: "POST",
            datatype: "json",
            height: '200',
            width: '1060',
            colNames: ['Id propietario', 'Razon Social', 'Tipo Persona', 'Nit', 'Tipo Documento', 'Representante Legal',
                'Identificacion', 'Direccion', 'Correo', 'Telefono', 'Cod ciudad', 'Cod departamento', 'Cod pais',
                'Contribuyente', 'Tipo Regimen', 'Iva', 'Ica', 'Retefuente', 'Autoretenedor', 'Hc', 'Numero Cuenta', 'Tipo Cuenta', 'Banco', 'Sede Banco',
                'Agencia', 'Tipo Pago', 'Banco Tranferencia', 'Sucursal', 'Idusuario', 'Contrase�a', 'Codigo Cliente', 'nombre_cuenta', 'cedula_cuenta', 'cantidad'],
            colModel: [
                {name: 'idpropietario', index: 'idpropietario', width: 80, sortable: true, align: 'center', hidden: true, key: true},
                {name: 'nombreprop', index: 'nombreprop', width: 190, align: 'left'},
                {name: 'tipopersona', index: 'tipopersona', sortable: true, width: 75, align: 'center'},
                {name: 'nit', index: 'nit', width: 79, align: 'left'},
                {name: 'tipodoc', index: 'tipodoc', width: 85, align: 'center', hidden: true},
                {name: 'representante', index: 'representante', sortable: true, width: 200, align: 'left', hidden: true},
                {name: 'nitem', index: 'nitem', width: 80, align: 'left', hidden: true},
                {name: 'direccion', index: 'direccion', width: 150, align: 'left', hidden: true},
                {name: 'correo', index: 'correo', sortable: true, width: 150, align: 'center', hidden: true},
                {name: 'telefono', index: 'telefono', sortable: true, width: 75, align: 'center', hidden: true},
                {name: 'codCiudad', index: 'codCiudad', sortable: true, width: 70, align: 'center', hidden: true},
                {name: 'codDepartamento', index: 'codDepartamento', sortable: true, width: 90, align: 'center', hidden: true},
                {name: 'codPais', index: 'codPais', sortable: true, width: 70, align: 'center', hidden: true},
                {name: 'contribuyente', index: 'contribuyente', sortable: true, width: 90, align: 'center'},
                {name: 'tiporegimen', index: 'tiporegimen', sortable: true, width: 90, align: 'center'},
                {name: 'iva', index: 'iva', sortable: true, width: 70, align: 'center'},
                {name: 'ica', index: 'ica', sortable: true, width: 70, align: 'center'},
                {name: 'retefuente', index: 'retefuente', width: 90, align: 'center', sortable: false},
                {name: 'autoretenedor', index: 'autoretenedor', width: 100, align: 'center', sortable: false},
                {name: 'hc', index: 'hc', width: 100, align: 'center', sortable: false, hidden: true},
                {name: 'numcuenta', index: 'numcuenta', width: 100, align: 'center', sortable: false, hidden: true},
                {name: 'tipocuenta', index: 'tipocuenta', width: 150, align: 'center', sortable: false, hidden: true},
                {name: 'banco', index: 'banco', width: 100, align: 'center', sortable: false, hidden: true},
                {name: 'sedebanco', index: 'sedebanco', width: 100, align: 'center', sortable: false, hidden: true},
                {name: 'banagencia', index: 'banagencia', width: 100, align: 'center', sortable: false, hidden: true},
                {name: 'tipopago', index: 'tipopago', width: 100, align: 'center', sortable: false, hidden: true},
                {name: 'bancotransfer', index: 'bancotransfer', width: 150, align: 'center', sortable: false, hidden: true},
                {name: 'sucursaltransfer', index: 'sucursaltransfer', width: 100, align: 'center', sortable: false, hidden: true},
                {name: 'idusuario', index: 'idusuario', width: 100, align: 'left', sortable: false},
                {name: 'passw', index: 'passw', width: 100, align: 'center', sortable: false, hidden: true},
                {name: 'codcli', index: 'codcli', width: 100, align: 'center', sortable: false, hidden: true},
                {name: 'nombre_cuenta', index: 'nombre_cuenta', width: 100, align: 'center', sortable: false, hidden: true},
                {name: 'cedula_cuenta', index: 'cedula_cuenta', width: 100, align: 'center', sortable: false, hidden: true},
                {name: 'cantidad', index: 'cantidad', width: 100, align: 'center', sortable: false, hidden: true}
            ],
            gridComplete: function (id) {
                var cant = jQuery("#tabla_cargar_propietario").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cl = cant[i];
                    var cantidad = grid_mostrar_eds.getRowData(cl).cantidad;
                    be = "<input style='height:20px;width:50px;' type='button' value='ver' onclick=\"mostrar('" + cl + "');\" />";
                    jQuery("#tabla_cargar_propietario").jqGrid('setRowData', cant[i], {ver: be});

                    if (cantidad > 1) {
                        grid_mostrar_eds.jqGrid('setRowData', cant[i], false, {weightfont: 'bold', background: '#F5A9A9'});
                    }
                }

                //NOTA COLOCAR EL COLOR CUANDO ES MAS DE 1 :)
            },
            rownumbers: true,
            rownumWidth: 35,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                loading("Espere un momento se estan cargando la informacion del propietario.", "300", "140");
                operacion = 1;
                var myGrid = jQuery("#tabla_cargar_propietario"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.idpropietario;
                var nit = filas.nit;
                var nombreprop = filas.nombreprop;
                var direccion = filas.direccion;
                var correo = filas.correo;
                var tipopersona = filas.tipopersona;
                var telefono = filas.telefono;
                var pais = filas.codPais;
                var departamento = filas.codDepartamento;
                var ciudad = filas.codCiudad;
                var idusuario = filas.idusuario;
                var passw = filas.passw;
                var representante = filas.representante;
                var nitem = filas.nitem;
                var tipodoc = filas.tipodoc;
                var gcontribuyente = filas.contribuyente;
                var iva = filas.iva;
                var retefuente = filas.retefuente;
                var ica = filas.ica;
                var autoretenedor = filas.autoretenedor;
                var numcuenta = filas.numcuenta;
                var banco = filas.banco;
                var sedepago = filas.sedebanco;
                var banagencia = filas.banagencia;
                var tcuenta = filas.tipocuenta;
                var bancotransfer = filas.bancotransfer;
                var regimen = filas.tiporegimen;
                var suctransfer = filas.sucursaltransfer;
                var hc = filas.hc;
                var ptransfer = filas.tipopago;
                var codcli = filas.codcli;
                var nombrecuenta = filas.nombre_cuenta;
                var cedulacuenta = filas.cedula_cuenta;

                if (gcontribuyente === 'SI') {
                    gcontribuyente = 'S';
                } else if (gcontribuyente === 'NO') {
                    gcontribuyente = 'N';
                }
                if (iva === 'SI') {
                    iva = 'S';
                } else if (iva === 'NO') {
                    iva = 'N';
                }
                if (ica === 'SI') {
                    ica = 'S';
                } else if (ica === 'NO') {
                    ica = 'N';
                }
                if (autoretenedor === 'SI') {
                    autoretenedor = 'S';
                } else if (autoretenedor === 'NO') {
                    autoretenedor = 'N';
                }
                if (regimen === 'COMUN') {
                    regimen = 'C';
                } else if (regimen === 'SIMPLIFICADO') {
                    regimen = 'S';
                }
                if (tipopersona === 'NATURAL') {
                    tipopersona = 'PN';
                } else if (tipopersona === 'JURIDICA') {
                    tipopersona = 'PJ';
                }
                if (retefuente === 'SI') {
                    retefuente = 'S';
                } else if (retefuente === 'NO') {
                    retefuente = 'N';
                }
                setTimeout(function () {
                    ventanaPropietario(operacion, id, nombreprop, nit, direccion, correo, tipopersona, telefono, pais, departamento, ciudad, idusuario, passw, representante,
                            nitem, tipodoc, gcontribuyente, iva, ica, retefuente, autoretenedor, numcuenta, banco, sedepago, banagencia,
                            tcuenta, bancotransfer, regimen, suctransfer, hc, ptransfer, codcli, nombrecuenta, cedulacuenta);

                }, 600);
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                data: {
                    opcion: 35,
                    identificacion: identificacion,
                    idusuario: idusuario
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_cargar_propietario").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 0;
                ventanaPropietario(operacion);
            }
        });
    }
}

function editarPropietarioEds() {
    var codcli = $("#codcli").val();
    var id = $("#id").val();
    var idusuarioedit = $("#idusuarioedit").val();
    var nombreprop = $("#nombreprop").val();
    var direccion = $("#direccion").val();
    var correo = $("#correo").val();
    var tipopersona = $("#tipopersona").val();
    var telefono = $("#telefono").val();
    var pais = $("#pais").val();
    var departamento = $("#departamento").val();
    var ciudad = $("#ciudad").val();
    var nit = $("#nit").val();
    var idusuario = $("#idusu").val();
    var passw = $("#password").val();
    var passw2 = $("#password2").val();
    var cambioclav = $("#actdes").val();
    var representante = $("#representante").val();
    var nitem = $("#nitem").val();
    var tipodoc = $("#tipodoc").val();
    var gcontribuyente = $("#gcontribuyente").val();
    var iva = $("#iva").val();
    var retefuente = $("#retefuente").val();
    var ica = $("#ica").val();
    var autoretenedor = $("#autoretenedor").val();
    var numcuenta = $("#cuenta").val();
    var banco = $("#banco").val();
    var sedepago = $("#sedepago").val();
    var banagencia = $("#banagencia").val();
    var tcuenta = $("#tcuenta").val();
    var bancotransfer = $("#bancotransfer").val();
    var ptransfer = $("input['checbox']:checked").length;
    var suctransfer = $("#sucursal").val();
    var regimen = $("#regimen").val();
    var idedit = $("#idedit").val();
    var hc = $("#hc").val();
    var nombre_cuenta = $("#nombre_cuenta").val();
    var cedula_cuenta = $("#cedula_cuenta").val();

    if ((nombreprop !== "") && (direccion !== "") && (correo !== "") && (telefono !== "") && (nit !== "") && (idusuario !== "") &&
            (passw !== "") && (passw2 !== "") && (representante !== "") && (nitem !== "") && (pais !== "") && (departamento !== "") &&
            (ciudad !== "") && (tipodoc !== "") && (numcuenta !== "") && (nombre_cuenta !== "") && (cedula_cuenta !== "")) {
        expr = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!expr.test(correo)) {
            mensajesDelSistema("DEBE CAMBIAR EL CORREO", '230', '150', false);
        }//else if (usus.trim() == "OK") {
        if ($("#dialogMsjSMS").html() === "") {
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Administracion&accion=Eds",
                datatype: 'json',
                data: {
                    opcion: 36,
                    id: id,
                    nombreprop: nombreprop,
                    direccion: direccion,
                    correo: correo,
                    tipopersona: tipopersona,
                    telefono: telefono,
                    pais: pais,
                    ciudad: ciudad,
                    nit: nit,
                    idusuario: idusuario,
                    idusuarioedit: idusuarioedit,
                    passw: passw,
                    cambioclav: cambioclav,
                    representante: representante,
                    nitem: nitem,
                    tipodoc: tipodoc,
                    gcontribuyente: gcontribuyente,
                    iva: iva,
                    retefuente: retefuente,
                    ica: ica,
                    autoretenedor: autoretenedor,
                    banco: banco,
                    sedepago: sedepago,
                    banagencia: banagencia,
                    tcuenta: tcuenta,
                    ptransfer: ptransfer,
                    bancotransfer: bancotransfer,
                    numcuenta: numcuenta,
                    hc: hc,
                    suctransfer: suctransfer,
                    regimen: regimen,
                    idedit: idedit,
                    codcli: codcli,
                    nombre_cuenta: nombre_cuenta,
                    cedula_cuenta: cedula_cuenta
                },
                success: function (data) {
                    mensajesDelSistema("EXITO AL GUARDAR PROPIETARIO EDS ", '230', '150', true);
                    $("#nombreprop").val("");
                    $("#direccion").val("");
                    $("#correo").val("");
                    $("#telefono").val("");
                    $("#nit").val("");
                    $("#idusu").val("");
                    $("#password").val("");
                    $("#password2").val("");
                    $("#representante").val("");
                    $("#nitem").val("");
                    $("#cuenta").val("");
                    $("#dialogMsjSMS").html("");
                    $("#sms2").html("");
                    $("#nombre_cuenta").val("");
                    $("#cedula_cuenta").val("");
                    $("#nitem").attr('readOnly', false);
                    $("#idusu").attr('readOnly', false);
                    $("#tipodoc").attr('disabled', false);
                    $("#digitover").val("");
                    resultadoPropietario();
                },
                error: function (result) {
                    alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
                }
            });
        } else {
            mensajesDelSistema("LAS CONTRASE�AS SON DIFERENTES", '300', '150', true);
        }
//        } else {
//            mensajesDelSistema("EL USUARIO YA EXISTE", '230', '150', false);
        //        }
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function ventanaAddCuenta() {

    alert("En construccion");
//    var id = 'addbanco';
//    cargarBancotransfer();
//    cargarSucursal();
//    cargarBanco(id);
//    cargarSedePago();
//    cargarAgencia('addbanco');
//    $("#dialogMsjAddcuenta").dialog({
//        width: '350',
//        height: '350',
//        show: "scale",
//        hide: "scale",
//        resizable: false,
//        position: "center",
//        modal: true,
//        closeOnEscape: false,
//        buttons: {
//            "Guardar": function () {
//
//            },
//            "Salir": function () {
//                $(this).dialog("close");
//            }
//        }
//    });
//    $("#dialogMsjAddcuenta").siblings('div.ui-dialog-titlebar').remove();
}

function validacionDigitos() {
    var tipodoc = $("#tipodoc").val();
    var nitem = $("#nitem").val();
    var numero;
    if (tipodoc === 'NIT') {
        var digito = nitem.length;
        if (digito === 10) {
            numero = (nitem.substring(0, nitem.length - 1));
            verificarUsuarioPropietario(numero, tipodoc);
            alert(numero);
        } else {
            numero = nitem;
            verificarUsuarioPropietario(numero, tipodoc);

        }

    } else if (tipodoc === 'CED') {
        alert('hola');
    } else {
        mensajesDelSistema("Escoger tipo de documento", '230', '150', false);
    }
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();
}

function Vercampodv() {
    var tipodoc = $("#tipodoc").val();
    if (tipodoc === 'NIT') {
        $("#dv").attr('hidden', false);
        $("#digitover").attr('hidden', false);
    } else {
        $("#dv").attr('hidden', true);
        $("#digitover").attr('hidden', true);
    }
}

function CalcularDv() {
    var vpri, x, y, z, i, nit1, dv1;
    nit1 = $("#nitem").val();
    if (isNaN(nit1)) {
        //document.form1.dv.value="X";
        alert('El valor digitado no es un numero valido');
    } else {
        vpri = new Array(16);
        x = 0;
        y = 0;
        z = nit1.length;
        vpri[1] = 3;
        vpri[2] = 7;
        vpri[3] = 13;
        vpri[4] = 17;
        vpri[5] = 19;
        vpri[6] = 23;
        vpri[7] = 29;
        vpri[8] = 37;
        vpri[9] = 41;
        vpri[10] = 43;
        vpri[11] = 47;
        vpri[12] = 53;
        vpri[13] = 59;
        vpri[14] = 67;
        vpri[15] = 71;
        for (i = 0; i < z; i++) {
            y = (nit1.substr(i, 1));
            //document.write(y+"x"+ vpri[z-i] +":");
            x += (y * vpri[z - i]);
            //document.write(x+"<br>");     
        }
        y = x % 11;
        //document.write(y+"<br>");
        if (y > 1) {
            dv1 = 11 - y;
        } else {
            dv1 = y;
        }
        //document.form1.dv.value=dv1;
        $("#digitover").val(dv1);
    }
}
