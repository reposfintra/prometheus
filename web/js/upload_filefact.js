var lote_generado;
$(document).ready(function () {
    comboLotePorGenerar();
    comboLotePorEnviarSms();
    var f = new Date();
    var anio = f.getFullYear();
    var mes = f.getMonth();
    var dia = f.getDate();

    mes = ((mes < 9) ? "0" : "") + (mes + 1);

    if (dia < 10) {
        dia = "0" + dia;
    }
    $('#periodo').val(anio + mes);

    $("#tabs").tabs({
        show: function (event, ui) {
            $("#container").html("");
        }
    });

    $("#reversar").click(function () {
        reversar_lote();
    });

    //buscarTransportadoras();

    $("#upload").click(function () {
        var file_excel = $("#file_excel").val();
        var periodo = $("#periodo").val();
        var ciclo = $("#ciclo").val();

        if (file_excel == "" || periodo == "" || ciclo == "") {
            alert("Debes llenar todos los campos");
        } else {            
            validarExtension(file_excel);
            
        }
    });

}); //Documento on ready

function validarExtension(file_excel) {
    var allowedExtensions = /(.xls|.xlsx)$/i;
    var nombre = $("#file_excel").val().split('\\').pop();

    if (!allowedExtensions.exec(file_excel)) {
        mensajesDelSistema("la extension del archivo no es valida", '410', '150', false);
    } else {
        loading("Cargando Informacion...",'410','150');
        subirAchivo();

    }
}

function subirAchivo() {

    var fd = new FormData(document.getElementById('formulario'));

    $.ajax({
        async: true,
        url: "./controller?estado=Fintra&accion=Soporte&opcion=141",
        type: 'POST',
        dataType: 'json',
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        beforeSend: function (xhr) {
            //console.warn('Enviando informacion..');
        },
        success: function (json) {
            if (json.respuesta !== '') {
                lote_generado=json.respuesta ;
                buscarExtractos(lote_generado); 
                $("#dialogo2").dialog("close");
                mensajesDelSistema("Archivo Cargado Exitosamente, se genero el lote No: "+lote_generado, '410', '150', false);
                comboLotePorGenerar();
                comboLotePorEnviarSms();
            } else {
                $("#dialogo2").dialog("close");
                mensajesDelSistema("No pudimos cargar el Archivo ", '410', '150', false);
                $("#file_excel").val("");
            }

        }, error: function (xhr, ajaxOptions, thrownError) {

            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function buscarExtractos(lote) {
    $("#capaCentralprev").show();
    $("#capaCentralmsj").hide();
    lote_generado=lote;
    var grid_tbl_reversar_lote = jQuery("#tbl_prev_excel");
    
     if ($("#gview_tbl_prev_excel").length) {
        reloadBuscarExtractos(grid_tbl_reversar_lote,lote_generado);    
    } else {

    grid_tbl_reversar_lote.jqGrid({
        caption: "Exractos a Generar",
        url: "./controller?estado=Fintra&accion=Soporte",
        mtype: "POST",
        datatype: "json",
        height: '300',
        width: '890',
        colNames: ['Extracto', 'Generado el', 'Negocio', 'Cedula', 'Nombre', 'Linea', 'Periodo', 'Ciclo', 'Fecha creaci�n','Generado'],
        colModel: [
            {name: 'cod_rop', index: 'cod_rop', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'generado_el', index: 'generado_el', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'negocio', index: 'negocio', width: 65, sortable: true, align: 'center', hidden: false},
                {name: 'cedula', index: 'cedula', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 340, sortable: true, align: 'center', hidden: false},
                {name: 'linea_producto', index: 'linea_producto', width: 110, sortable: true, align: 'center', hidden: false},
                {name: 'periodo', index: 'periodo', width: 60, sortable: true, align: 'center', hidden: false},
                {name: 'ciclo', index: 'ciclo', width: 50, sortable: true, align: 'center', hidden: false},
                {name: 'fecha_creacion', index: 'fecha_creacion', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'generado', index: 'generado', width: 60, sortable: true, align: 'center', hidden: true}
        ],
        rowNum: 10000,
            rowTotal: 10000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            restoreAfterError: true,
            pager: '#page_prev_excel',
            pgtext: null,
            pgbuttons: false,
            multiselect: false,
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        }, ajaxGridOptions: {
            async: false,
            data: {
                opcion: 142,
                lote_generado:lote_generado
//                periodo: $("#periodo").val(),
//                ciclo: $("#ciclo").val()
            }
        },
        loadComplete: function () {
            if (grid_tbl_reversar_lote.jqGrid('getGridParam', 'records') <= 0) {
                mensajesDelSistema("No se encontraron resultados para los parametros seleccionados.", "270", "170");
            } else{
                 $('#capaCentralprev').css('display', 'block');
                 $('#gen').css('display', 'block');
            }
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 200);
        }
    }).navGrid("#page_prev_excel", {add: false, edit: false, del: false, search: false, refresh: false}, {});
    
    }

}

function reloadBuscarExtractos(grid_tabla,lote_generado) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 142,
                lote_generado:lote_generado
            }
        }
    });
    grid_tabla.trigger("reloadGrid");

}


//-----------------------------------------------------------------------
//                            Util functions
//-----------------------------------------------------------------------

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");

            }
        }
    });
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();
}



//-----------------------------------------------------------------------
//                            functions Manuel Camargo
//-----------------------------------------------------------------------

function generarExtractoSms() {
    loading("Espere un momento por favor...", "270", "140");
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        data: {
            opcion: 144,
            lote_generado:lote_generado
        },
        success: function (json) {
            if(json.status===200){
                comboLotePorGenerar();
                comboLotePorEnviarSms();
            mensajesDelSistema(json.data, '250', '150', true);
             $("#dialogo2").dialog("close");
             buscarExtractosPorEnviar(lote_generado);
            }
//               
        },
        error: function (xhr) {
            $("#dialogo2").dialog("close");
            mensajesDelSistema(xhr.responseText, '250', '150', true);
        }
    });
}

function comboLotePorGenerar() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 143
        },
        success: function (json) {
            if (json.error) {
                return;
            }
             $('#lote option').remove();
            $('#lote').append('<option value="" >...</option>');
            for (var datos in json) {
                $('#lote').append('<option value="' + datos + '" >' + json[datos] + '</option>');
            }

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function buscarExtractosPorEnviar(lote) {
    lote_generado=lote;
    $("#capaCentralprev").hide();
    $("#capaCentralmsj").show();
//    $("#dialogo2").dialog("close");
    var grid_tabla = $("#tbl_envio_sms");
    if ($("#gview_tbl_envio_sms").length) {
        reloadGridTabla(grid_tabla, 145);
    } else {

        grid_tabla.jqGrid({

            caption: "Negocios Refinanciar",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '780',
            colNames: ['Id Extracto', 'Negocio', 'Cedula', 'Nombre', 'Valor', 'Celular', 'Email', 'Lote'],
            colModel: [
                {name: 'id_extracto', index: 'id_extracto', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'cod_negocio', index: 'cod_negocio', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'cedula', index: 'cedula', width: 65, sortable: true, align: 'center', hidden: false},
                {name: 'nombre', index: 'nombre', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'valor_extracto', index: 'valor_extracto', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'celular', index: 'celular', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'email', index: 'email', width: 180, sortable: true, align: 'center', hidden: false},
                {name: 'lote', index: 'lote', width: 100, sortable: true, align: 'center', hidden: false}
            ],
            rowNum: 10000,
            rowTotal: 10000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            restoreAfterError: true,
            pager: '#page_prev_excel',
            pgtext: null,
            pgbuttons: false,
            multiselect: false,

            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "2"

            },
            loadComplete: function () {
                
                if (grid_tabla.jqGrid('getGridParam', 'records') <= 0) {

                    mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '200');
                }else{
                    $("#generar").show();
                }
                
            },
            ajaxGridOptions: {

                data: {
                   opcion: 145,
                   lote_generado:lote_generado
                }

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }
        });

    }
}

function reloadGridTabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
            opcion: 145,
            lote_generado:lote_generado
            }
        }
    });
    grid_tabla.trigger("reloadGrid");

}

function enviarSmsExtracto() {
    loading("Espere un momento por favor...", "270", "140");
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        data: {
            opcion: 146,
            lote_generado:lote_generado
        },
        success: function (json) {
            comboLotePorEnviarSms();
            $("#dialogo2").dialog('close');
                mensajesDelSistema(json.data, '250', '150');
           
        },
        error: function (xhr) {
            $("#dialogo2").dialog('close');
            mensajesDelSistema(xhr.responseText, '250', '150', true);
        }
    });

}

function comboLotePorEnviarSms() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 147
        },
        success: function (json) {
            if (json.error) {
                return;
            }
             $('#sms option').remove();
            $('#sms').append('<option value="" >...</option>');
            for (var datos in json) {
                $('#sms').append('<option value="' + datos + '" >' + json[datos] + '</option>');
            }

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
