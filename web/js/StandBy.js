/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 $(document).ready(function() {
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    }); 
 
     //$("#filtrar").click(function () {
     //    alert('hoal');
     //   CalcularDiasVig();
    //});
 });

function listarNegociosStandBy() {
    var unidad_negocio = $('#unidad_negocio').val();
    //alert(unidad_negocio");
    //console.log('Entra aqui');
    var grid_listar_negocios_standby = $("#tabla_negocios_standby");
    if ($("#gview_tabla_negocios_standby").length) {
        reloadGridListarNegociosStandBy(unidad_negocio);
    } else {
    
        grid_listar_negocios_standby.jqGrid({
            caption: "Lista de negocios StandBy",
            url: "./controller?estado=Negocio&accion=Trazabilidad",
            height:'500',
            width: '1150',
            datatype: 'json',
            colNames: ['Id Cliente', 'Cliente', 'Afiliado', 'Negocio', 'Id Solicitud', 'Valor Negocio', 'Valor Desembolso', 'Fecha Negocio', 'Fecha Desembolso', 'Cod. Actividad', 'Actividad', 'Usuario', 'Fecha StandBy', 'Causal', 'D�as Expira', 'Tiempo Restante', 'Comentarios', 'Asesor', 'Agencia', 'Acciones'],
            colModel: [
                {name: 'id_cliente', index: 'id_cliente', width: 80, sortable: true, align: 'center'},
                {name: 'cliente', index: 'cliente', width: 180, align: 'left'},
                {name: 'afiliado', index: 'afiliado', width: 180, align: 'left'},
                {name: 'negocio', index: 'negocio', sortable: true, width: 90, align: 'center', key: true},  
                {name: 'numero_solicitud', index: 'numero_solicitud', sortable: true, width: 90, align: 'center', hidden: true},  
                {name: 'vr_negocio', index: 'vr_negocio', width: 110, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'vr_desembolso', index: 'vr_desembolso', width: 110, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},   
                {name: 'fecha_negocio', index: 'fecha_negocio', sortable: true, width: 110, align: 'center'},   
                {name: 'fecha_desembolso', index: 'fecha_desembolso', sortable: true, width: 110, align: 'center'},                
                {name: 'cod_actividad_anterior', index: 'cod_actividad_anterior', sortable: true, hidden:true, width: 110, align: 'center'},
                {name: 'actividad', index: 'actividad', sortable: true, width: 110, align: 'center'},
                {name: 'usuario_standby', index: 'usuario_standby', sortable: true, width: 110, align: 'center'},
                {name: 'fecha_standby', index: 'fecha_standby', sortable: true, width: 110, align: 'center'},
                {name: 'causal_standby', index: 'causal_standby', sortable: true, width: 180, align: 'center'},
                {name: 'dias_exp', index: 'dias_exp', sortable: true, width: 80, align: 'center'},
                {name: 'dias_restantes', index: 'dias_restantes', sortable: true, width: 80, align: 'center'},
                {name: 'comentarios', index: 'comentarios', sortable: true, width: 230, align: 'left'},
                {name: 'asesor', index: 'asesor', sortable: true, width: 110, align: 'center'},
                {name: 'agencia', index: 'agencia', sortable: true, width: 110, align: 'center'},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: 110}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#page_tabla_negocios_standby',    
            pgbuttons:false,
            pgtext:null,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },ajaxGridOptions: { 
                async:false,              
                type: "POST",                
                data: {
                    opcion:'LISTAR_NEGOCIOS_STANDBY'
                   ,unidad_negocio:unidad_negocio
                }
            },gridComplete: function() {
                var ids = jQuery("#tabla_negocios_standby").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];                  
                                              
                    rev = "<img src='/fintra/images/edit.ico' align='absbottom'  name='genera_docs' id='genera_docs' width='15' height='15' title ='Quitar StandBy'  onclick=\"ventanaQuitarStandBy('" + cl + "');\">";
                    rev += "<img src='/fintra/images/buttons_add.png' align='absbottom'  name='agregar_comentario' id='agregar_comentario' width='15' height='15' title ='Mantener StandBy'  onclick=\"ventanaAgregarComentarioStandBy('" + cl + "');\">";
                    jQuery("#tabla_negocios_standby").jqGrid('setRowData', ids[i], {actions: rev});

                }
            },
            onCellSelect: function (rowid, index, contents, event)
            {
                var cm = $("#tabla_negocios_standby").jqGrid('getGridParam', 'colModel');
                if (cm[index].name == "comentarios")
                {
                   ventanaShowCommentStandBy(rowid);
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
        $("#tabla_negocios_standby").jqGrid('navGrid','#page_tabla_negocios_standby',{add:false,del:false,edit:false,search:false,refresh:false}); 
         jQuery("#tabla_negocios_standby").jqGrid("navButtonAdd", "#page_tabla_negocios_standby", {
            caption: "Exportar",
            title: "Exportar negocios en Stanby",
            onClickButton: function () {
                exportarExcel();
            }
        });

    }

}


function reloadGridListarNegociosStandBy(unidad_negocio) {
    jQuery("#tabla_negocios_standby").setGridParam({
        url: "./controller?estado=Negocio&accion=Trazabilidad",
        datatype: 'json',
        ajaxGridOptions: {
            async:false,
            type: "POST",
            data: {
                    opcion:'LISTAR_NEGOCIOS_STANDBY'
                    ,unidad_negocio:unidad_negocio
            }
        }
    });
    jQuery("#tabla_negocios_standby").trigger("reloadGrid");
}

function ventanaQuitarStandBy(rowid){
      $("#comment").val('');
      $('#comment').attr({readonly: false});
      $("#dialogAddComent").dialog({
        width: 580,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'QUITAR STANDBY',
        closeOnEscape: false,
        buttons: {  
            "Actualizar": function () {
                if ($('#comment').val() === '') {
                    mensajesDelSistema('Por favor, ingrese la anotaci�n correspondiente', '250', '150');
                } else {    
                    actualizarNegocioStandBy(rowid);                    
                }               
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarNegocioStandBy(rowid) {
        var num_solicitud = jQuery("#tabla_negocios_standby").getRowData(rowid).numero_solicitud;
        var cod_actividad = jQuery("#tabla_negocios_standby").getRowData(rowid).cod_actividad_anterior;   
        var actividad = jQuery("#tabla_negocios_standby").getRowData(rowid).actividad; 
        var user_stby = jQuery("#tabla_negocios_standby").getRowData(rowid).usuario_standby;
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function () {
            $.ajax({
              async: false,
              url: "./controller?estado=Negocio&accion=Trazabilidad",
              type: 'POST',
              dataType: 'json',
              data: {
                  opcion: 'REGRESAR_ACT_NEGOCIO_STANDBY',
                  cod_neg: rowid,
                  num_solicitud: num_solicitud,
                  cod_actividad: cod_actividad,
                  actividad: actividad,
                  comentario: $('#comment').val(),
                  usuario_stby: user_stby
              },
              success: function (json) {

                  if (!isEmptyJSON(json)) {

                      if (json.error) {
                         $("#dialogLoading").dialog('close');
                          mensajesDelSistema(json.error, '250', '150');
                          return;
                      }

                      if (json.respuesta === "OK") {
                          $("#dialogLoading").dialog('close');
                          mensajesDelSistema("Operaci�n exitosa.", '250', '150', true);
                          $("#dialogAddComent").dialog('close');
                          reloadGridListarNegociosStandBy();
                      }

                  } else {
                      $("#dialogLoading").dialog('close');
                      mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci�n!!", '250', '150');
                  }

              }, error: function (xhr, ajaxOptions, thrownError) {
                  alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
              }
          });
        }, 500);    
        
}

function listarCausalesStandBy() {
    var grid_listar_causales_standby = $("#tabla_causales_standby");
    if ($("#gview_tabla_causales_standby").length) {
        reloadGridListarCausalesStandBy();
    } else {

        grid_listar_causales_standby.jqGrid({
            caption: "Listado de Causales de StandBy",
            url: "./controller?estado=Negocio&accion=Trazabilidad",
            cellsubmit: "clientArray",
            height:'350',
            width: '550',
            datatype: 'json',
            colNames: ['Codigo', 'Descripcion', 'Plazo', 'Estado', 'Acciones'],
            colModel: [
                {name: 'table_code', index: 'table_code', sortable: true, hidden:true, width: 90, align: 'center', key: true},  
                {name: 'dato', index: 'dato', sortable: true, width: 330, align: 'left'},  
                {name: 'descripcion', index: 'descripcion', sortable: true, editable: true, width: 90, align: 'center'},  
                {name: 'reg_status', index: 'reg_status', sortable: true, hidden:true, width: 90, align: 'center'},  
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: 110}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#page_tabla_causales_standby',    
            pgbuttons:false,
            pgtext:null,
            cellEdit: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, afterSaveCell: function(rowid, celname, value, iRow, iCol) {                
                if (celname === 'descripcion') {                  
                   var plazo = $("#tabla_causales_standby").getRowData(rowid).descripcion;
                   (isNaN(value)) ? $("#tabla_causales_standby").jqGrid("restoreCell", iRow, iCol) : actualizarCausal(rowid, plazo); 
                }
            },ajaxGridOptions: { 
                async:false,              
                type: "POST",                
                data: {
                    opcion:'LISTAR_CONFIG_CAUSALES_STANDBY'
                }
            },gridComplete: function() {
                var ids = jQuery("#tabla_causales_standby").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];       
                    var estado = jQuery("#tabla_causales_standby").getRowData(cl).reg_status;
                    if (estado === 'A') {
                        ac = "<img src='/fintra/images/botones/iconos/check-blue.png' align='absbottom'  name='activar' id='activar' width='15' height='15' title ='Activar'  onclick=\"mensajeConfirmAction('Esta seguro de activar la causal seleccionada?','250','150',activarCausal,'" + cl + "');\">";
                    } else {
                        ac = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular la causal seleccionada?','250','150',anularCausal,'" + cl + "');\">";
                    }    
                    
                    jQuery("#tabla_causales_standby").jqGrid('setRowData', ids[i], {actions: ac});     

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
        $("#tabla_causales_standby").jqGrid('navGrid','#page_tabla_causales_standby',{add:false,del:false,edit:false,search:false,refresh:false});      
        jQuery("#tabla_causales_standby").jqGrid("navButtonAdd", "#page_tabla_causales_standby", {
            caption: "Nuevo",
            title: "Agregar nueva causal",
            onClickButton: function () {
                crearCausales();
            }
        });
    }

}


function reloadGridListarCausalesStandBy() {
    jQuery("#tabla_causales_standby").setGridParam({
        url: "./controller?estado=Negocio&accion=Trazabilidad",
        datatype: 'json',
        ajaxGridOptions: {
            async:false,
            type: "POST",
            data: {
                opcion:'LISTAR_CONFIG_CAUSALES_STANDBY'
            }
        }
    });
    jQuery("#tabla_causales_standby").trigger("reloadGrid");
}

function crearCausales(){
    $('#descripcion').val('');
    $('#plazo').val('');  
    $('#div_causales').fadeIn('slow');
    AbrirDivCrearCausales();
}

function AbrirDivCrearCausales(){
      $("#div_causales").dialog({
        width: 600,
        height: 190,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR CAUSALES STANDBY',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
              guardarCausal();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarCausal(){    
    var nombre = $('#descripcion').val();
    var plazo = $('#plazo').val();   
    var url = './controller?estado=Negocio&accion=Trazabilidad';
    if(nombre===''){
        mensajesDelSistema("Debe ingresar la descripcion de la causal", '250', '150');
        return;
    }else if(plazo===''){
        mensajesDelSistema("Debe ingresar el plazo para la causal", '250', '150');
        return;
    }else{
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: "INSERT_CAUSAL_STANDBY",
                    nombre: nombre,
                    descripcion: plazo
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {
                            reloadGridListarCausalesStandBy();                           
                            mensajesDelSistema("Se cre� la causal satisfactoriamente", '250', '150', true); 
                            $('#descripcion').val('');
                            $('#plazo').val('');                            
                        }
                        
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear la causal!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
    }
}

function actualizarCausal(id, plazo) {

    $.ajax({
        async: false,
        url: "./controller?estado=Negocio&accion=Trazabilidad",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: "UPDATE_CAUSAL_STANDBY",
            codigo: id,
            descripcion: plazo
        },
        success: function(json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   reloadGridListarCausalesStandBy();
                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci�n!!", '250', '150');
            }

        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function anularCausal(cl){
    var url = './controller?estado=Negocio&accion=Trazabilidad';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: "ACTIVAR_INACTIVAR_CAUSAL_STANDBY",            
            codigo: cl,
            estadoCausal: "A"
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    reloadGridListarCausalesStandBy(); 
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular la causal!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function activarCausal(cl){
    var url = './controller?estado=Negocio&accion=Trazabilidad';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: "ACTIVAR_INACTIVAR_CAUSAL_STANDBY",            
            codigo: cl,
            estadoCausal: ""
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    reloadGridListarCausalesStandBy();          
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo activar la causal!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function ventanaShowCommentStandBy(rowid){
      var comment = jQuery("#tabla_negocios_standby").getRowData(rowid).comentarios;
      $("#comment").val(comment);
      $('#comment').attr({readonly: true});
      $("#dialogAddComent").dialog({
        width: 580,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'COMENTARIO DE STANDBY',
        closeOnEscape: false,
        buttons: {       
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {   
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}

function mensajeConfirmAction(msj, width, height, okAction, id) {
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function ventanaAgregarComentarioStandBy(rowid){
      $("#comment").val('');
      $('#comment').attr({readonly: false});
      $("#dialogAddComent").dialog({
        width: 580,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'AGREGAR COMENTARIO STANDBY',
        closeOnEscape: false,
        buttons: {  
            "Guardar": function () {
                if ($('#comment').val() === '') {
                    mensajesDelSistema('Por favor, ingrese la anotaci�n correspondiente', '250', '150');
                } else {    
                    agregarComentarioStandBy(rowid);                    
                }               
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function agregarComentarioStandBy(rowid) {
    var num_solicitud = jQuery("#tabla_negocios_standby").getRowData(rowid).numero_solicitud;
    var cod_actividad = jQuery("#tabla_negocios_standby").getRowData(rowid).cod_actividad_anterior;
    var actividad = jQuery("#tabla_negocios_standby").getRowData(rowid).actividad;
    var user_stby = jQuery("#tabla_negocios_standby").getRowData(rowid).usuario_standby;
    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function () {
        $.ajax({
            async: false,
            url: "./controller?estado=Negocio&accion=Trazabilidad",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 'AGREGAR_COMENTARIO_STANDBY',
                cod_neg: rowid,
                num_solicitud: num_solicitud,
                cod_actividad: cod_actividad,
                actividad: actividad,
                comentario: $('#comment').val(),
                usuario_stby: user_stby
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Operaci�n exitosa.", '250', '150', true);
                        $("#dialogAddComent").dialog('close');
                        reloadGridListarNegociosStandBy();
                    }

                } else {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci�n!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }, 500);
}

function  exportarExcel() {
    var fullData = jQuery("#tabla_negocios_standby").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    
    
    $.ajax({
        datatype:'json',
        type:'post',
        async:false,      
        url: "./controller?estado=Negocio&accion=Trazabilidad",
        data: {
            listado: myJsonString,
            opcion: "EXPORTAR_NEGOCIO_STAMBY"
        },
        success: function (resp) {      
            console.log("Entro");
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
            $("#imgloadEx").hide();
            $("#msjEx").hide();
        }
    });
}




