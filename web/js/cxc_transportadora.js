/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    $("#fecha_corrida").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    var myDate = new Date();
    $("#fecha_corrida").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

    buscarTransportadoras();
    
    $("#buscar").click(function () {
       mostrarDatosCxc();
    });
    
    $("#generar").click(function () {
        generarCXCTransportadora();
    });

});

function mostrarDatosCxc() {

    if ($("#transportadora").val() !== '' && $("#fecha_corrida").val() !== '') {
          
        var grid_cxc =jQuery("#tblcxc");

        if ($("#gview_tblcxc").length > 0) {
            reloadGridcxc(grid_cxc);
        } else {

            grid_cxc.jqGrid({
                caption: "CXC Transportadoras",
                url: "./controller?estado=Administracion&accion=Logistica",
                mtype: "POST",
                datatype: "json",
                height: '500',
                width: '900',
                colNames: ['Id', 'Nit', 'Transportadora', 'Producto', 'Valor', 'Fecha Vencimiento', 'Fecha Corrida','# Planillas'],
                colModel: [
                    {name: 'id', index: 'id', width: 50, align: 'center', hidden: true},
                    {name: 'identificacion', index: 'identificacion', width: 100, align: 'center'},
                    {name: 'transportadora', index: 'transportadora', sortable: true, width: 130, align: 'center'},
                    {name: 'producto', index: 'producto', sortable: true, width: 130, align: 'center'},
                    {name: 'valor', index: 'valor', sortable: true, width: 120, align: 'right', search: false, sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'fecha_vencimiento', index: 'fecha_vencimiento', sortable: false, width: 120, align: 'center'},
                    {name: 'fecha_corrida', index: 'fecha_corrida', width: 120, align: 'center', key:true},
                    {name: 'total_corte', index: 'total_corte', width: 100, align: 'center'}
                ],
                rowNum: 10000,
                rowTotal: 10000000,
                pager: ('#pagecxc'),
                loadonce: true,
                rownumWidth: 30,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                shrinkToFit: false,
                footerrow: true,
                rownumbers: true,
                jsonReader: {
                    root: "rows",
                    repeatitems: false,
                    id: "0"
                }, ajaxGridOptions: {
                    async: false,
                    data: {
                        opcion: 20,
                        transportadora: $("#transportadora").val(),
                        fecha_corrida: $("#fecha_corrida").val()
                    }
                }, ondblClickRow: function (rowid, status, e) {
                    var fecha_corrida = grid_cxc.getRowData(rowid).fecha_corrida;
                    var id_trans = grid_cxc.getRowData(rowid).id;
                 
                    detallePlanillaCXC(id_trans,fecha_corrida);

                },
                loadComplete: function () {
                  if(grid_cxc.jqGrid('getGridParam', 'records') > 0){
                     cacularTotalesCxC(grid_cxc);
                     $("#contenedor").show();
                  }else{
                       mensajesDelSistema("En este momento no hay corridas pendientes por generar.", "330", "150");
                       $("#contenedor").hide();
                  } 
                  
                },
                loadError: function (xhr, status, error) {
                    mensajesDelSistema(error, 250, 200);
                }
            }).navGrid("#page_extracto_eds", {add: false, edit: false, del: false, search: false, refresh: false}, {});

        }
    } else {
         mensajesDelSistema("Debe selecionar la transportadora y la fecha de corte.", "330", "150");
    }
}


function cacularTotalesCxC(grid_cxc) {

    var valor = grid_cxc.jqGrid('getCol', 'valor', false, 'sum');
     var numplanillas = grid_cxc.jqGrid('getCol', 'total_corte', false, 'sum');
    grid_cxc.jqGrid('footerData', 'set', {
        producto: 'Totales',
        valor: valor,
        total_corte:numplanillas
    });

}

function reloadGridcxc(grid_cxc) {
    grid_cxc.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Logistica",
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: 20,
                transportadora: $("#transportadora").val(),
                fecha_corrida: $("#fecha_corrida").val()
            }
        }
    });
    grid_cxc.trigger("reloadGrid");
}

function detallePlanillaCXC(id_trans,fecha_corrida){
    
        var grid_detallecxc =jQuery("#tbldetallecxc");

        if ($("#gview_tbldetallecxc").length > 0) {
            reloadGridetallecxc(grid_detallecxc,id_trans,fecha_corrida);
        } else {

            grid_detallecxc.jqGrid({
                caption: "Planillas",
                url: "./controller?estado=Administracion&accion=Logistica",
                mtype: "POST",
                datatype: "json",
                height: '400',
                width: '735',
                colNames: ['Planilla', 'Reanticipo', 'Valor', 'Fecha creacion', 'Fecha Vencimiento', 'Fecha Corrida'],
                colModel: [
                    {name: 'planilla', index: 'planilla', width: 100, align: 'center'},
                    {name: 'reanticipo', index: 'reanticipo', width: 100, align: 'center'},
                    {name: 'valor_anticipos', index: 'valor_anticipos', sortable: true, width: 120, align: 'right', search: false, sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'fecha_generacion', index: 'fecha_generacion', sortable: true, width: 130, align: 'center'},    
                    {name: 'fecha_vencimiento', index: 'fecha_vencimiento', sortable: false, width: 120, align: 'center'},
                    {name: 'fecha_corrida', index: 'fecha_corrida', width: 120, align: 'center', key:true}
                    
                ],
                rowNum: 10000,
                rowTotal: 10000000,
                pager: ('#pagedetallecxc'),
                loadonce: true,
                rownumWidth: 30,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                shrinkToFit: false,
                footerrow: true,
                rownumbers: false,
                jsonReader: {
                    root: "rows",
                    repeatitems: false,
                    id: "0"
                }, ajaxGridOptions: {
                    async: false,
                    data: {
                        opcion: 22,
                        transportadora: id_trans,
                        fecha_corrida:fecha_corrida
                    }
                },loadComplete: function () {
                  cacularTotalesCxCdeta(grid_detallecxc);
                },
                loadError: function (xhr, status, error) {
                    mensajesDelSistema(error, 250, 200);
                }
            }).navGrid("#pagedetallecxc", {add: false, edit: false, del: false, search: false, refresh: false}, {});

        }
        
        $("#detalle").dialog({
        width: 'auto',
        height: 'auto',
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Salir": function() {
                $(this).dialog("close");               
            }
        }
    });
}

function cacularTotalesCxCdeta(gridetalle_cxc) {

    var valor = gridetalle_cxc.jqGrid('getCol', 'valor_anticipos', false, 'sum');

    gridetalle_cxc.jqGrid('footerData', 'set', {
        reanticipo: 'Totales',
        valor_anticipos: valor
    });

}

function reloadGridetallecxc(grid_detallecxc,id_trans,fecha_corrida) {
    grid_detallecxc.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Logistica",
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: 22,
                transportadora: id_trans,
                fecha_corrida: fecha_corrida
            }
        }
    });
    grid_detallecxc.trigger("reloadGrid");
}





function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function mensajesDelSistemaReload(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
                mostrarDatosCxc();
            }
        }
    });

}


function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();
}


function generarCXCTransportadora() {
    
    if ($("#transportadora").val() !== '' && $("#fecha_corrida").val() !== '') {
        loading("Espere un momento se esta generando el archivo transferencia.", "320", "140");
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Logistica",
            dataType: 'json',
            async: false,
            data: {
                opcion: 21,
                transportadora: $("#transportadora").val(),
                fecha_corrida: $("#fecha_corrida").val()
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {
                     $("#dialogo2").dialog('close');
                    if (json.respuesta === 'OK') {                      
                        mensajesDelSistemaReload('El proceso de generacion ha sido exitoso', '300', '150');                        
                    } else {
                        mensajesDelSistema('Lo sentimos no se pudo generar la corrida..', '280', '150');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    
      } else {
         mensajesDelSistema("Debe selecionar la transportadora y la fecha de corte.", "330", "150");
    }

}


function buscarTransportadoras() {

   
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Logistica",
            dataType: 'json',
            async: false,
            data: {
                opcion: 1
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    try {
                        $('#transportadora').empty();
                        for (var key in json) {
                            $('#transportadora').append('<option value=' + json[key].id + '>' + json[key].razon_social + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    
}
