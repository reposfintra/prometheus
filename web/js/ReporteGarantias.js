/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 $(document).ready(function() {
     $("#fechaini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())

    });
     $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())

    });
    $( "#datepicker" ).datepicker( "option", "dateFormat", 'yy-mm-dd');
    
    var myDate = new Date();
    $("#fechaini").datepicker("setDate", myDate);
    $("#fechafin").datepicker("setDate", myDate);
    
    
        $('#bucar_saldos_cartera').click(function() {
         
          ReporteSaldosCartera();
        });
        $('#bucar_negocios_nuevos').click(function() {
         
          ReporteNegociosFintraNuevos();
        });
    });
    

function ReporteSaldosCartera() {      
    var grid_tabla_saldos_cartera = jQuery("#tabla_saldos_cartera");
     if ($("#gview_tabla_saldos_cartera").length) {
        RefrescarReporteSaldosCartera();
     }else {
        grid_tabla_saldos_cartera.jqGrid({
            caption: "Actualizacion Saldos Cartera",
            url: "./controller?estado=Negocios&accion=Fintra",           	 
            datatype: "json",  
            height: '500',
            width: '1435',
            cellEdit: true,
            colNames: ["INTERMEDIARIO","NIT","COD_NEG","SALDO_CAPITAL_CORTE","TOTAL_OBLICACION","FECHA_CORTE","CUOTAS_PENDIENTES","INICIO_MORA","FECHA_DE_CANCELACION","ESTADO","NUM_PAGARE"],
            colModel: [
               
                {name: 'INTERMEDIARIO', index: 'INTERMEDIARIO', width: 90, align: 'center'},
                {name: 'NIT', index: 'NIT', width: 110, align: 'center'},
                {name: 'COD_NEG', index: 'COD_NEG', width: 120, align: 'center'},              
                {name: 'SALDO_CAPITAL_CORTE', index: 'SALDO_CAPITAL_CORTE', width: 120, align: 'center',formatter: 'currency', 
                    formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}}, 
                {name: 'TOTAL_OBLICACION', index: 'TOTAL_OBLICACION', width: 120,  align: 'center',formatter: 'currency', 
                    formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},  
                {name: 'FECHA_CORTE', index: 'FECHA_CORTE', width: 120, align: 'center'},  
                {name: 'CUOTAS_PENDIENTES', index: 'CUOTAS_PENDIENTES', width: 120, align: 'center'},  
                {name: 'INICIO_MORA', index: 'INICIO_MORA', width: 120, align: 'center'},
                {name: 'FECHA_DE_CANCELACION', index: 'FECHA_DE_CANCELACION', width: 120, align: 'center'},
                {name: 'ESTADO', index: 'ESTADO', width: 120, align: 'center'},                
                {name: 'NUM_PAGARE', index: 'NUM_PAGARE', width: 180, align: 'center'}
                
               
            ],
            rowNum: 10000,
            rowTotal: 10000,
            pager: ('#page_tabla_saldos_cartera'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                
                if ($('#tabla_saldos_cartera >tbody >tr').length <= 1){
                  // $('#tabla_desistir_negocio >tbody >tr').style.display = 'none';
                      mensajesDelSistema ( "No se encontraron registros");
                    }
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion:4,
                       periodo:$('#periodo').val(),
                       unidad_negocio:$('#unidad_negocio').val()
                     }
            },   
            loadError: function (xhr, status, error) {
                alert('error', 250, 200);
            }
        }).navGrid("#page_tabla_saldos_cartera", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_saldos_cartera").jqGrid("navButtonAdd", "#page_tabla_saldos_cartera", {
            caption: "Exportar excel",
           
            onClickButton: function() {
                var info = jQuery('#tabla_saldos_cartera').getGridParam('records');
                if (info > 0) {
                    ExportarReporteSaldosCartera();
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }

            }

        });
    }  
       
}

function RefrescarReporteSaldosCartera(){   
    jQuery("#tabla_saldos_cartera").setGridParam({
        url: "./controller?estado=Negocios&accion=Fintra",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: {    opcion:4,
                       periodo:$('#periodo').val(),
                       unidad_negocio:$('#unidad_negocio').val()
            }
        }       
    });
    
    jQuery('#tabla_saldos_cartera').trigger("reloadGrid");
}


function  ExportarReporteSaldosCartera() {
    var fullData = jQuery("#tabla_saldos_cartera").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Negocios&accion=Fintra",
        data: {
            opcion: 5,
            listado: myJsonString
            
        },
        success: function () {
           
            cerrarDiv("#divSalidaEx");
             mensajesDelSistema ("Documento generado con exito, puede consultarlo en la carpeta descargas");
        },
        error: function () {
            mensajesDelSistema("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function cerrarDiv(div)
{
    $(div).dialog('close');
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function ReporteNegociosFintraNuevos() {      
    var grid_tabla_saldos_cartera = jQuery("#tabla_saldos_cartera");
     if ($("#gview_tabla_saldos_cartera").length) {
        RefrescarReporteNegociosFintraNuevos();
     }else {
        grid_tabla_saldos_cartera.jqGrid({
            caption: "Actualizacion Saldos Cartera",
            url: "./controller?estado=Negocios&accion=Fintra",           	 
            datatype: "json",  
            height: '500',
            width: '1600',
            cellEdit: true,
            colNames: ["INTERMEDIARIO","COD_SUCURSAL","TITULAR","TIPO_IDENTIFICACION","CEDULA_TITULAR","GENERO_TITULAR","DIRECCION_TITULAR","COD_MUNICIPIO","TEL_TITULAR",
                        "TELEFONO2_TITULAR","FAX_TITULAR","COD_CI","COD_NEG","NUM_PAGARE","MONEDA","VALOR_MONTO","FECHA_DESEMBOLSO","NRO_DOCS","FECHA_VENCIMIENTO","PERIODO_GRACIA",
                        "TIPO_CARTERA","TIPO_INVERSION","TIPO_RECURSO","VALOR_REDESCUENTO","PORC_REDESCUENTO","NIT_ENTIDAD_REDESCUENTO","CONVENIO","PRODUCTO_GARANTIA","PORCENTAJE_AVAL",
                        "RESPONSABLE","DESCRIP_INTERMEDIARIO","IDENTIFICACION","TIPO_IDENTIFICACION_CODEUDOR","CODEUDOR","DIRECCION","COD_MUNICIPIO","CELULAR","TELEFONO2",
                        "CAMPO_RESERVADO","CAMPO_RESERVADO2","ACTIVIDAD_ECONOMICA","COD_MUNICIPIO","PAYMENT_NAME","TIENDA_SUCURSAL","ESTRATO","FECHA_NACIMIENTO","ESTADO_CIVIL"],
            colModel: [
               
                {name: 'INTERMEDIARIO', index: 'INTERMEDIARIO', width: 90, align: 'center'},
                {name: 'COD_SUCURSAL', index: 'COD_SUCURSAL', width: 110, align: 'center'},         
                {name: 'TITULAR', index: 'TITULAR', width: 180, align: 'center'},               
                {name: 'TIPO_IDENTIFICACION', index: 'TIPO_IDENTIFICACION', width: 120, align: 'center'}, 
                {name: 'CEDULA_TITULAR', index: 'CEDULA_TITULAR', width: 120,  align: 'center'},  
                {name: 'GENERO_TITULAR', index: 'GENERO_TITULAR', width: 120, align: 'center'},  
                {name: 'DIRECCION_TITULAR', index: 'DIRECCION_TITULAR', width: 120, align: 'center'},  
                {name: 'COD_MUNICIPIO', index: 'COD_MUNICIPIO', width: 120, align: 'center'},
                {name: 'TEL_TITULAR', index: 'TEL_TITULAR', width: 120, align: 'center'},
                {name: 'TELEFONO2_TITULAR', index: 'TELEFONO2_TITULAR', width: 120, align: 'center'},
                {name: 'FAX_TITULAR', index: 'FAX_TITULAR', width: 120, align: 'center'},
                {name: 'COD_CI', index: 'COD_CI', width: 120, align: 'center'},
                {name: 'COD_NEG', index: 'COD_NEG', width: 120, align: 'center'},
                {name: 'NUM_PAGARE', index: 'NUM_PAGARE', width: 120, align: 'center'},
                {name: 'MONEDA', index: 'MONEDA', width: 120, align: 'center'},
                {name: 'VALOR_MONTO', index: 'VALOR_MONTO', width: 120, align: 'center',formatter: 'currency', 
                    formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'FECHA_DESEMBOLSO', index: 'FECHA_DESEMBOLSO', width: 120, align: 'center'},
                {name: 'NRO_DOCS', index: 'NRO_DOCS', width: 120, align: 'center'},
                {name: 'FECHA_VENCIMIENTO', index: 'FECHA_VENCIMIENTO', width: 120, align: 'center'},
                {name: 'PERIODO_GRACIA', index: 'PERIODO_GRACIA', width: 120, align: 'center'},
                {name: 'TIPO_CARTERA', index: 'TIPO_CARTERA', width: 120, align: 'center'},
                {name: 'TIPO_INVERSION', index: 'TIPO_INVERSION', width: 120, align: 'center'},
                {name: 'TIPO_RECURSO', index: 'TIPO_RECURSO', width: 120, align: 'center'},
                {name: 'VALOR_REDESCUENTO', index: 'VALOR_REDESCUENTO', width: 120, align: 'center'},
                {name: 'PORC_REDESCUENTO', index: 'PORC_REDESCUENTO', width: 120, align: 'center'},
                {name: 'NIT_ENTIDAD_REDESCUENTO', index: 'NIT_ENTIDAD_REDESCUENTO', width: 120, align: 'center'},
                {name: 'CONVENIO', index: 'CONVENIO', width: 120, align: 'center'},
                {name: 'PRODUCTO_GARANTIA', index: 'PRODUCTO_GARANTIA', width: 120, align: 'center'},
                {name: 'PORCENTAJE_AVAL', index: 'PORCENTAJE_AVAL', width: 120, align: 'center'},
                {name: 'RESPONSABLE', index: 'RESPONSABLE', width: 120, align: 'center'},
                {name: 'DESCRIP_INTERMEDIARIO', index: 'DESCRIP_INTERMEDIARIO', width: 120, align: 'center'},
                {name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: 120, align: 'center'},
                {name: 'TIPO_IDENTIFICACION_CODEUDOR', index: 'TIPO_IDENTIFICACION_CODEUDOR', width: 120, align: 'center'},
                {name: 'CODEUDOR', index: 'CODEUDOR', width: 120, align: 'center'},
                {name: 'DIRECCION', index: 'DIRECCION', width: 120, align: 'center'},
                {name: 'COD_MUNICIPIO', index: 'COD_MUNICIPIO', width: 120, align: 'center'},
                {name: 'CELULAR', index: 'CELULAR', width: 120, align: 'center'},
                {name: 'TELEFONO2', index: 'TELEFONO2', width: 120, align: 'center'},
                {name: 'CAMPO_RESERVADO', index: 'CAMPO_RESERVADO', width: 120, align: 'center'},
                {name: 'CAMPO_RESERVADO2', index: 'CAMPO_RESERVADO2', width: 120, align: 'center'},
                {name: 'ACTIVIDAD_ECONOMICA', index: 'ACTIVIDAD_ECONOMICA', width: 120, align: 'center'},
                {name: 'COD_MUNICIPIO', index: 'COD_MUNICIPIO', width: 120, align: 'center'},
                {name: 'PAYMENT_NAME', index: 'PAYMENT_NAME', width: 120, align: 'center'},
                {name: 'TIENDA_SUCURSAL', index: 'TIENDA_SUCURSAL', width: 120, align: 'center'},
                {name: 'ESTRATO', index: 'ESTRATO', width: 120, align: 'center'},
                {name: 'FECHA_NACIMIENTO', index: 'FECHA_NACIMIENTO', width: 120, align: 'center'},
                {name: 'ESTADO_CIVIL', index: 'ESTADO_CIVIL', width: 120, align: 'center'} 
            ],
            rowNum: 10000,
            rowTotal: 10000,
            pager: ('#page_tabla_saldos_cartera'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                
                if ($('#tabla_saldos_cartera >tbody >tr').length <= 1){
                  // $('#tabla_desistir_negocio >tbody >tr').style.display = 'none';
                      mensajesDelSistema ( "No se encontraron registros");
                    }
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion:6,
                       fechaini:$('#fechaini').val(),
                       fechafin:$('#fechafin').val(),
                       unidad_negocio:$('#unidad_negocio').val()
                     }
            },   
            loadError: function (xhr, status, error) {
                alert('error', 250, 200);
            }
        }).navGrid("#page_tabla_saldos_cartera", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_saldos_cartera").jqGrid("navButtonAdd", "#page_tabla_saldos_cartera", {
            caption: "Exportar excel",
           
            onClickButton: function() {
                var info = jQuery('#tabla_saldos_cartera').getGridParam('records');
                if (info > 0) {
                    ExportarReporteNegociosFintraNuevos();
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }

            }

        });
    }  
       
}

function RefrescarReporteNegociosFintraNuevos(){   
    jQuery("#tabla_saldos_cartera").setGridParam({
        url: "./controller?estado=Negocios&accion=Fintra",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: {    opcion:6,
                       fechaini:$('#fechaini').val(),
                       fechafin:$('#fechafin').val(),
                       unidad_negocio:$('#unidad_negocio').val()
            }
        }       
    });
    
    jQuery('#tabla_saldos_cartera').trigger("reloadGrid");
}

function  ExportarReporteNegociosFintraNuevos() {
    var fullData = jQuery("#tabla_saldos_cartera").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Negocios&accion=Fintra",
        data: {
            opcion: 7,
            listado: myJsonString
            
        },
        success: function () {
           
            cerrarDiv("#divSalidaEx");
             mensajesDelSistema ("Documento generado con exito, puede consultarlo en la carpeta descargas");
        },
        error: function () {
            mensajesDelSistema("Ocurrio un error enviando los datos al servidor");
        }
    });
}



