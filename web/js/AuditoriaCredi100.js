/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#fecha").val('');
    $("#fechafin").val('');
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $('#ui-datepicker-div').css('clip', 'auto');
    cargarUnidadNegocio();

    $('#buscar').click(function () {
        cargarAuditoriaCredi100();
    });
    $('#exportar').click(function () {
        exportarExcelAuditoria();
    });

    $('#limpiar').click(function () {
        $("#fecha").val('');
        $("#fechafin").val('');
    });
});

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarAuditoriaCredi100() {
    var grid_tabla = $("#tabla_AuditoriaCredi100");
    if ($("#gview_tabla_AuditoriaCredi100").length) {
        reloadGridTabla(grid_tabla, 1);
    } else {
        grid_tabla.jqGrid({
            caption: "Auditoria Credi 100",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '530',
            width: '1650',
            colNames: ['Entidad', 'Numero solicitud', 'Negocio', 'Identificacion', 'Nombre cliente', 'Afiliado', 'Estado inicial solicitud', 'Estado solicitud', 'Estado negocio',
                'Causal', 'Asesor', 'id_convenio', 'Monto credito', 'Valor cuota', 'Numero cuotas', 'Score', 'Score lisim', 'Score total', 'Total obligaciones financieras',
                'Total gastos familiares', 'Total ingresos', 'Endeudamiento', 'Comentario', 'Cuotas pendientes', 'Etapa', 'Medio', 'Fecha presolicitud','Tipo Cliente','Qb','Qm','Qa'
                ,'Porc Endeudamiento','Acepta terminos','Fecha Acepta Terminos','Tipo Aprobacion','Rechazo en Operaciones','Causal Rechazo Operaciones'],
            colModel: [
                {name: 'entidad', index: 'entidad', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'numero_solicitud', index: 'numero_solicitud', width: 120, sortable: true, align: 'center', hidden: false, key: true, search: true},
                {name: 'cod_neg', index: 'cod_neg', width: 150, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'identificacion', index: 'identificacion', width: 100, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 200, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'afiliado', index: 'afiliado', width: 250, sortable: true, align: 'left', hidden: false},
                {name: 'estado_inicial_solicitud', index: 'estado_inicial_solicitud', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'estado_solicitud', index: 'estado_solicitud', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'estado_negocio', index: 'estado_negocio', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'causal', index: 'causal', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'asesor', index: 'asesor', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'id_convenio', index: 'id_convenio', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'monto_credito', index: 'monto_credito', sortable: true, width: 150, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}, search: false},
                {name: 'valor_cuota', index: 'valor_cuota', sortable: true, width: 150, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}, search: false},
                {name: 'numero_cuotas', index: 'numero_cuotas', width: 80, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'score', index: 'score', width: 100, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'acore_lisim', index: 'acore_lisim', width: 100, sortable: true, align: 'right', hidden: false, search: false},
                {name: 'score_total', index: 'score_total', width: 100, sortable: true, align: 'right', hidden: false, search: false},
                {name: 'total_obligaciones_financieras', index: 'total_obligaciones_financieras', sortable: true, width: 150, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}, search: false},
                {name: 'total_gastos_familiares', index: 'total_gastos_familiares', sortable: true, width: 150, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}, search: false},
                {name: 'total_ingresos', index: 'total_ingresos', sortable: true, width: 150, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}, search: false},
                {name: 'endeudamiento', index: 'endeudamiento', width: 150, sortable: true, align: 'right', hidden: false, search: false},
                {name: 'comentario', index: 'comentario', width: 200, sortable: true, align: 'left', hidden: false, search: false},
                {name: 'cuotas_pendientes', index: 'cuotas_pendientes', width: 120, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'etapa', index: 'etapa', width: 180, sortable: true, align: 'left', hidden: false},
                {name: 'medio', index: 'medio', width: 180, sortable: true, align: 'left', hidden: false},
                {name: 'fecha_presolicitud', index: 'fecha_presolicitud', width: 180, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'tipo_cliente', index: 'tipo_cliente', width: 150, sortable: true, align: 'center', hidden: false},
                {name: 'qb', index: 'qb', width: 100, sortable: true, align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}, hidden: false, search: false},
                {name: 'qm', index: 'qm', width: 100, sortable: true, align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}, hidden: false, search: false},
                {name: 'qa', index: 'qa', width: 150, sortable: true, align: 'center', formatter: 'currency',formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}, hidden: false, search: false},
                {name: 'porc_endeudamiento', index: 'porc_endeudamiento', width: 100,sortable: true, align: 'center', hidden: false},
                {name: 'acepta_terminos', index: 'acepta_terminos', width: 80,sortable: true, align: 'center', hidden: false},
                {name: 'fecha_aceptacion', index: 'fecha_aceptacion', width: 100,sortable: true, align: 'center', hidden: false},
                {name: 'tipo_aprobacion', index: 'tipo_aprobacion', width: 100,sortable: true, align: 'center', hidden: false},
                {name: 'rechazo_operaciones', index: 'rechazo_operaciones', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'causal_rechazo', index: 'causal_rechazo', width: 250, sortable: true, align: 'left', hidden: false}
            ],
            rowNum: 10000,
            rowTotal: 10000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = $("#tabla_ofertas"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id_solicitud = filas.id_solicitud;
                mostrarAcciones(id_solicitud);
            }, gridComplete: function () {
                var material = grid_tabla.jqGrid('getCol', 'material', false, 'sum');
                var mano_obra = grid_tabla.jqGrid('getCol', 'mano_obra', false, 'sum');
                var transporte = grid_tabla.jqGrid('getCol', 'transporte', false, 'sum');
                var administracion = grid_tabla.jqGrid('getCol', 'administracion', false, 'sum');
                var imprevisto = grid_tabla.jqGrid('getCol', 'imprevisto', false, 'sum');
                var utilidad = grid_tabla.jqGrid('getCol', 'utilidad', false, 'sum');


                grid_tabla.jqGrid('footerData', 'set', {esquema: 'TOTAL', material: material, mano_obra: mano_obra, transporte: transporte, administracion: administracion, imprevisto: imprevisto,
                    utilidad: utilidad});
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
            },
            ajaxGridOptions: {
                data: {
                    opcion: 1,
                    fecha_inicio: $("#fecha").val(),
                    fecha_fin: $("#fechafin").val(),
                    linea_negocio: $("#linea_negocio").val(),
                    identificacion: $("#identificacion").val(),
                    num_solicitud: $("#num_solicitud").val(),
                    estado_solicitud: $("#estado_solicitud").val(),
                    cod_neg: $("#cod_neg").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });

        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
        $("#tabla_AuditoriaCredi100").navButtonAdd('#pager', {
            caption: "Exportar Excel",
            onClickButton: function () {
                var info = grid_tabla.getGridParam('records');
                if (info > 0) {
                    exportarExcelAuditoria();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
    }
}


function reloadGridTabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                fecha_inicio: $("#fecha").val(),
                fecha_fin: $("#fechafin").val(),
                linea_negocio: $("#linea_negocio").val(),
                identificacion: $("#identificacion").val(),
                num_solicitud: $("#num_solicitud").val(),
                estado_solicitud: $("#estado_solicitud").val(),
                cod_neg: $("#cod_neg").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function  exportarExcelAuditoria() {
    var fullData = $("#tabla_AuditoriaCredi100").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Admin&accion=Fintra",
        data: {
            listado: myJsonString,
            opcion: 2
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function cargarUnidadNegocio() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 3
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}