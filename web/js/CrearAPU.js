/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    cargarComboGrupo1([]);
    cargarComboGrupo([]);
    cargarComboUnidadM([]);
    CargarGruposAPU();

    $("#buscar").click(function () {
        CargarInsumosxFiltro();
    });


    maximizarventana();

    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    });
});

function cargarComboUnidadM(filtro) {
    var elemento = $('#unmed'), sql = 'ConsultaUnidadM';
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'get',
        data: {opcion: 1, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function cargarComboGrupo1(filtro) {
    var elemento = $('#grupo_apu1'), sql = 'ConsultaGruposAPUS';
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'get',
        data: {opcion: 1, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '" onclick="CargarGruposAPU();">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function cargarComboGrupo(filtro) {
    var elemento = $('#grupo_apu'), sql = 'ConsultaGruposAPUS1';
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'get',
        data: {opcion: 1, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });

}

function CargarGruposAPU() {

    var grupo_apu = $('#grupo_apu1').val();
    $('#tbl_materiales_apu').jqGrid('GridUnload');
    $('#tbl_equipo_apu').jqGrid('GridUnload');
    $('#tbl_maobra_apu').jqGrid('GridUnload');
    $('#tbl_herramientas_apu').jqGrid('GridUnload');
    $('#tbl_transportes_apu').jqGrid('GridUnload');
    $('#tbl_pertra_apu').jqGrid('GridUnload');
    var url = '/fintra/controlleropav?estado=Procesos&accion=APU&opcion=8';
    //$('#tbl_grupos_apu').jqGrid('GridUnload');
    if ($("#gview_tbl_grupos_apu").length) {
        refrescarGridGrupoAPU(grupo_apu);
    } else {
        jQuery("#tbl_grupos_apu").jqGrid({
            caption: 'APU',
            url: url,
            datatype: 'json',
            height: true,
            width: '880Px',
            colNames: ['Id', 'Descripcion', 'Id Grupo', 'Id Unidad', 'Editar'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '800'},
                {name: 'idgrupo', index: 'idgrupo', hidden: true, sortable: true, align: 'center', width: '600'},
                {name: 'idunidad', index: 'idunidad', hidden: true, sortable: true, align: 'center', width: '600'},
                {name: 'action', index: 'action', search: false, align: 'center', width: '50'}
            ],
            rowNum: 10000,
            rowTotal: 50000,
            loadonce: true,
            scroll:1,
            rownumWidth: 40,
            gridview: true,
            rownumbers: true,
            footerrow: false,
            reloadAfterSubmit: true,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            multiselect: false,
            viewrecords: true,
            hidegrid: true,
            pager: '#page_grupos_apu',
            onSelectRow: function (ids) {
                CargarMaterialAPU(ids);
                CargarEquipoAPU(ids);
                CargarMaObraAPU(ids);
                CargarHerramientasAPU(ids);
                CargarTransporteAPU(ids);
                CargarPerTramAPU(ids);
            },
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false,
                data: {
                    grupo_apu: grupo_apu
                }
            },
            gridComplete: function () {

                var ids = jQuery("#tbl_grupos_apu").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    edit = "<img src='/fintra/images/botones/iconos/editDoc2.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular' onclick=\"editarAPU('" + cl + "');\">";
                    jQuery("#tbl_grupos_apu").jqGrid('setRowData', ids[i], {action: edit});
                }
            }, loadComplete: function () {

            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });
        jQuery("#tbl_grupos_apu").navGrid("#page_grupos_apu", {add: false, search: true, edit: false, del: false, refresh: true});
        //jQuery("#tbl_grupos_apu").jqGrid('filterToolbar', {stringResult: false, searchOnEnter: false});
        //document.getElementById("gs_nombre").style.width = '50px';
        jQuery("#tbl_grupos_apu").jqGrid("navButtonAdd", "#page_grupos_apu", {
            caption: "Nuevo APU",
            title: "Agregar nuevo APU",
            onClickButton: function () {
                crearAPU();
            }
        });
        $("#tbl_grupos_apu").parents('div.ui-jqgrid-bdiv').css("max-height", "200px");
    }
}

function CargarMaterialAPU(ids) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=APU&opcion=11&tipo=1&ids=' + ids;
    $('#tbl_materiales_apu').jqGrid('GridUnload');

    if ($("#gview_tbl_materiales_apu").length) {
        //refrescarGridGrupoAPU(grupo_apu);
    } else {
        jQuery("#tbl_materiales_apu").jqGrid({
            caption: '1. Materiales',
            url: url,
            datatype: 'json',
            height: true,
            width: 900,
            colNames: ['Id', 'Descripcion', 'Unidad', 'Cantidad', 'Rendimiento'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600'},
                {name: 'unidadmedida', index: 'unidadmedida', sortable: true, align: 'center', width: '200'},
                {name: 'cantidad', index: 'cantidad', search: false, align: 'center', width: '100'},
                {name: 'rendimiento', index: 'rendimiento', search: false, align: 'center', width: '100'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            rownumbers: true,
            footerrow: false,
            reloadAfterSubmit: true,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            multiselect: false,
            viewrecords: true,
            hidegrid: true,
            //pager: '#page_materiales_apu',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {

            },
            loadComplete: function () {

            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });
        $("#tbl_materiales_apu").parents('div.ui-jqgrid-bdiv').css("max-height", "200px");
    }
}

function CargarEquipoAPU(ids) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=APU&opcion=11&tipo=2&ids=' + ids;
    $('#tbl_equipo_apu').jqGrid('GridUnload');

    if ($("#gview_tbl_equipo_apu").length) {
        //refrescarGridGrupoAPU(grupo_apu);
    } else {
        jQuery("#tbl_equipo_apu").jqGrid({
            caption: '2. Equipos',
            url: url,
            datatype: 'json',
            height: true,
            width: 900,
            colNames: ['Id', 'Descripcion', 'Unidad', 'Cantidad', 'Rendimiento'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600'},
                {name: 'unidadmedida', index: 'unidadmedida', sortable: true, align: 'center', width: '200'},
                {name: 'cantidad', index: 'cantidad', search: false, align: 'center', width: '100'},
                {name: 'rendimiento', index: 'rendimiento', search: false, align: 'center', width: '100'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            rownumbers: true,
            footerrow: false,
            reloadAfterSubmit: true,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            multiselect: false,
            viewrecords: true,
            hidegrid: true,
            //pager: '#page_materiales_apu',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {

            },
            loadComplete: function () {

            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });
        $("#tbl_equipo_apu").parents('div.ui-jqgrid-bdiv').css("max-height", "200px");
    }
}

function CargarMaObraAPU(ids) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=APU&opcion=11&tipo=3&ids=' + ids;
    $('#tbl_maobra_apu').jqGrid('GridUnload');

    if ($("#gview_tbl_maobra_apu").length) {
        //refrescarGridGrupoAPU(grupo_apu);
    } else {
        jQuery("#tbl_maobra_apu").jqGrid({
            caption: '3. Mano de Obra',
            url: url,
            datatype: 'json',
            height: true,
            width: 900,
            colNames: ['Id', 'Descripcion', 'Unidad', 'Cantidad', 'Rendimiento'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600'},
                {name: 'unidadmedida', index: 'unidadmedida', sortable: true, align: 'center', width: '200'},
                {name: 'cantidad', index: 'cantidad', search: false, align: 'center', width: '100'},
                {name: 'rendimiento', index: 'rendimiento', search: false, align: 'center', width: '100'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            rownumbers: true,
            footerrow: false,
            reloadAfterSubmit: true,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            multiselect: false,
            viewrecords: true,
            hidegrid: true,
            //pager: '#page_materiales_apu',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {

            },
            loadComplete: function () {

            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });
        $("#tbl_maobra_apu").parents('div.ui-jqgrid-bdiv').css("max-height", "200px");
    }
}

function CargarHerramientasAPU(ids) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=APU&opcion=11&tipo=4&ids=' + ids;
    $('#tbl_herramientas_apu').jqGrid('GridUnload');

    if ($("#gview_tbl_herramientas_apu").length) {
        //refrescarGridGrupoAPU(grupo_apu);
    } else {
        jQuery("#tbl_herramientas_apu").jqGrid({
            caption: '4. Herramientas',
            url: url,
            datatype: 'json',
            height: true,
            width: 900,
            colNames: ['Id', 'Descripcion', 'Unidad', 'Cantidad', 'Rendimiento'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600'},
                {name: 'unidadmedida', index: 'unidadmedida', sortable: true, align: 'center', width: '200'},
                {name: 'cantidad', index: 'cantidad', search: false, align: 'center', width: '100'},
                {name: 'rendimiento', index: 'rendimiento', search: false, align: 'center', width: '100'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            rownumbers: true,
            footerrow: false,
            reloadAfterSubmit: true,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            multiselect: false,
            viewrecords: true,
            hidegrid: true,
            //pager: '#page_materiales_apu',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {

            },
            loadComplete: function () {

            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });
        $("#tbl_herramientas_apu").parents('div.ui-jqgrid-bdiv').css("max-height", "200px");
    }
}

function CargarTransporteAPU(ids) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=APU&opcion=11&tipo=5&ids=' + ids;
    $('#tbl_transportes_apu').jqGrid('GridUnload');

    if ($("#gview_tbl_transportes_apu").length) {
        //refrescarGridGrupoAPU(grupo_apu);
    } else {
        jQuery("#tbl_transportes_apu").jqGrid({
            caption: '5. Transportes',
            url: url,
            datatype: 'json',
            height: true,
            width: 900,
            colNames: ['Id', 'Descripcion', 'Unidad', 'Cantidad', 'Rendimiento'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600'},
                {name: 'unidadmedida', index: 'unidadmedida', sortable: true, align: 'center', width: '200'},
                {name: 'cantidad', index: 'cantidad', search: false, align: 'center', width: '100'},
                {name: 'rendimiento', index: 'rendimiento', search: false, align: 'center', width: '100'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            rownumbers: true,
            footerrow: false,
            reloadAfterSubmit: true,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            multiselect: false,
            viewrecords: true,
            hidegrid: true,
            //pager: '#page_materiales_apu',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {

            },
            loadComplete: function () {

            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });
        $("#tbl_transportes_apu").parents('div.ui-jqgrid-bdiv').css("max-height", "200px");
    }
}

function CargarPerTramAPU(ids) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=APU&opcion=11&tipo=6&ids=' + ids;
    $('#tbl_pertra_apu').jqGrid('GridUnload');

    if ($("#gview_tbl_pertra_apu").length) {
        //refrescarGridGrupoAPU(grupo_apu);
    } else {
        jQuery("#tbl_pertra_apu").jqGrid({
            caption: '6. Permisos y Tramites',
            url: url,
            datatype: 'json',
            height: true,
            width: 900,
            colNames: ['Id', 'Descripcion', 'Unidad', 'Cantidad', 'Rendimiento'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600'},
                {name: 'unidadmedida', index: 'unidadmedida', sortable: true, align: 'center', width: '200'},
                {name: 'cantidad', index: 'cantidad', search: false, align: 'center', width: '100'},
                {name: 'rendimiento', index: 'rendimiento', search: false, align: 'center', width: '100'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            rownumbers: true,
            footerrow: false,
            reloadAfterSubmit: true,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            multiselect: false,
            viewrecords: true,
            hidegrid: true,
            //pager: '#page_materiales_apu',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {

            },
            loadComplete: function () {

            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });
        $("#tbl_pertra_apu").parents('div.ui-jqgrid-bdiv').css("max-height", "200px");
    }
}

function refrescarGridGrupoAPU(grupo_apu) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=APU&opcion=8&grupo_apu=' + grupo_apu;
    jQuery("#tbl_grupos_apu").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#tbl_grupos_apu').trigger("reloadGrid");

}

function editarAPU(cl) {
    //alert(cl);
    var fila = jQuery("#tbl_grupos_apu").getRowData(cl);
    $('#grupo_apu').val(fila['idgrupo']);
    $('#nomapu').val(fila['nombre']);
    $('#unmed').val(fila['idunidad']);
    $('#tbl_insumos').jqGrid('GridUnload');
    $('#div_apu').fadeIn('slow');
    AbrirDivAPU();
    GridInsumosEdit(cl);
}

function crearAPU() {
    $('#nomapu').val('');
    $('#tbl_insumos').jqGrid('GridUnload');
    $('#div_apu').fadeIn('slow');
    AbrirDivAPU();
    GridInsumos();
}

function AbrirDivAPU() {
    $("#div_apu").dialog({
        width: 1080,
        height: 520,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Armado de APU',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                mensajeConfirmAction("Recuerde guardar los cambios. Esta seguro que desea salir ?","230","150",cerrarDialogo,"div_apu");
                //$(this).dialog("destroy");
            }
        }
    });
}

function crearGrupoApu() {
    $('#nomgrupo').val('');
    $('#descgrupo').val('');
    $('#div_grupo_apu').fadeIn('slow');
    AbrirDivCrearGrupoApu();
}

function AbrirDivCrearGrupoApu() {
    $("#div_grupo_apu").dialog({
        width: 700,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR GRUPO APU',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarGrupoApu();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarGrupoApu() {
    var nomGrupo = $('#nomgrupo').val();
    var descGrupo = $('#descgrupo').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=APU';
    if (nomGrupo !== '' && descGrupo !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 3,
                nombre: nomGrupo,
                descripcion: descGrupo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        cargarComboGrupo([]);
                        mensajesDelSistema("Se creo el Grupo", '250', '150', true);
                        $('#nomgrupo').val('');
                        $('#descgrupo').val('');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear el grupo!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    mostrarContenido('dialogMsgMeta');
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function CargarInsumosxFiltro() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=APU&opcion=6';
    $('#tbl_filtro_insumos').jqGrid('GridUnload');
    if ($("#gview_tbl_filtro_insumos").length) {
        //refrescarGridProcesosMeta();
    } else {
        jQuery("#tbl_filtro_insumos").jqGrid({
            caption: 'Filtro Insumos',
            url: url,
            datatype: 'json',
            height: 200,
            width: 450,
            colNames: ['Id', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_filtro_insumos',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false,
                data: {
                    subcategoria: $('#sub').val()
                }
            },
            ondblClickRow: function (id) {
                var ret = jQuery("#tbl_filtro_insumos").jqGrid('getRowData', id);
                var nombre = ret.nombre;
                var idcl = $('#idregistro').val();
                jQuery("#tbl_insumos").jqGrid('setCell', idcl, "descripcionins", nombre);
                jQuery("#tbl_insumos").jqGrid('setCell', idcl, "iddescripcionins", ret.id);
                document.getElementById("sub").length = 0;
                $("#div_filtro_insumos").dialog("destroy");

            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });

    }
}

function GridInsumos() {

    var grid_ingreso_insumos = jQuery("#tbl_insumos");
    var id = "0";

    /*if ($("#gview_tbl_insumos").length) {
     reloadGridIngresoCatalogo(grid_ingreso_catalogo, arrNames, arrColModel);
     } else {*/

    grid_ingreso_insumos.jqGrid({
        caption: "Lista de Insumos",
        datatype: "local",
        height: '240',
        width: '1050',
        colNames: ['ID', 'Tipo Insumo', 'Id Tipo Insumo', 'Descripcion Insumo', 'Find', 'Id Descripcion Insumo', 'Unidad de Medida', 'Id Unidad de Medida', 'Nueva UM', 'Cantidad', 'Rendimiento'],
        colModel: [
            {name: 'id', index: 'id', hidden: true, key: true},
            {name: 'tipoinsumo', index: 'tipoinsumo', align: 'center', width: 140, search: false, editable: true, edittype: 'select', editrules: {required: true},
                editoptions: {
                    //value: ajaxSelect('ConsultaTiposInsumos', ''),
                    value: listarTiposMaterial(),
                    style: "width: 140px",
                    dataInit: function (elem) {
                        if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
                            // we are NOT in the searching bar
                            $(elem).find("option[value=\"\"]").remove();
                            setTimeout(function () {
                                $(elem).trigger('change');
                            }, 500);
                        }
                    },
                    dataEvents: [{type: 'change', fn: function (e) {
                                try {

                                    var rowid = e.target.id.replace("_tipoinsumo", "");

                                    id = e.target.value.replace("_", "");

                                    grid_ingreso_insumos.jqGrid('setCell', rowid, "idtipoinsumo", id);

                                    ////////////////////////////
                                    /*var jsonIns = listarInsumosXTipo(e.target.value.replace("_", ""));
                                     cargaListaDependentSelect(rowid + "_descripcionins", jsonIns);
                                     var idinsumo = grid_ingreso_insumos.getRowData(rowid).iddescripcionins;
                                     $("select#" + rowid + "_descripcionins").val(idinsumo);
                                     $("select#" + rowid + "_descripcionins").trigger('change');*/
                                    ////////////////////////////

                                } catch (exc) {
                                }
                                return;
                            }
                        }, {type: "keyup", fn: function (e) {
                                $(e.target).trigger("change");
                            }
                        }]
                }
            },
            {name: 'idtipoinsumo', index: 'idtipoinsumo', hidden: true},
            {name: 'descripcionins', index: 'descripcionins', width: 380, edittype: 'text', resizable: false, align: 'center'},
            {name: 'finddescripcionins', index: 'finddescripcionins', width: 30, edittype: 'text', resizable: false, align: 'center'},
            {name: 'iddescripcionins', index: 'iddescripcionins', hidden: true},
            {name: 'unidadmedida', index: 'unidadmedida', align: 'center', width: 150, search: false, editable: true, edittype: 'select', editrules: {required: false},
                editoptions: {
                    value: ajaxSelect('ConsultaUnidadesMed', ''),
                    style: "width: 150px",
                    dataInit: function (elem) {
                        $(elem).addClass('unidadm');
                        if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
                            // we are NOT in the searching bar
                            $(elem).find("option[value=\"\"]").remove();
                            setTimeout(function () {
                                $(elem).trigger('change');
                            }, 500);
                        }
                    },
                    dataEvents: [{type: 'change', fn: function (e) {
                                try {

                                    var rowid = e.target.id.replace("_unidadmedida", "");

                                    id = e.target.value.replace("_", "");

                                    grid_ingreso_insumos.jqGrid('setCell', rowid, "idunidadmedida", id);

                                } catch (exc) {
                                }
                                return;
                            }
                        }, {type: "keyup", fn: function (e) {
                                $(e.target).trigger("change");
                            }
                        }]
                }
            },
            {name: 'idunidadmedida', index: 'idunidadmedida', hidden: true},
            {name: 'newunidadmedida', hidden: false, index: 'newunidadmedida', width: 50, edittype: 'text', resizable: false, align: 'center'},
            {name: 'cantidad', index: 'cantidad', align: 'center', width: 100, search: false, editable: true, formatter: 'number',
                formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 2, prefix: ""},
                editoptions: {size: 50, dataInit: function (elem) {
                        $(elem).bind("keypress", function (e) {
                            //alert(soloNumeros(e));
                            return decimales(e, $(this));
                        });
                    }
                }},
            {name: 'rendimiento', index: 'rendimiento', align: 'center', width: 95, search: false, editable: true, formatter: 'number',
                formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 2, prefix: ""},
                editoptions: {size: 15, dataInit: function (elem) {
                        $(elem).bind("keypress", function (e) {
                            return decimales(e, $(this));
                        });
                    }
                }
            }
        ],
        rowNum: 10000,
        rowTotal: 10000000,
        pager: '#page_insumos',
        loadonce: true,
        rownumWidth: 40,
        gridview: true,
        viewrecords: true,
        rownumbers: true,
        hidegrid: false,
        shrinkToFit: false,
        footerrow: false,
        reloadAfterSubmit: true,
        multiselect: true,
        cellsubmit: 'clientArray',
        editurl: 'clientArray',
        ajaxGridOptions: {
            async: false
        },
        gridComplete: function () {
            var ids = jQuery("#tbl_insumos").jqGrid('getDataIDs');
            for (var i = 0; i < ids.length; i++) {
                var cl = ids[i];
                fil = "<img src='/fintra/images/botones/iconos/lupaOver.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular' onclick=\"buscarInsumoxFiltro('" + cl + "');\">";
                new1 = "<img src='/fintra/images/botones/iconos/new.gif' align='absbottom'  name='nuevo' id='nuevo' width='15' height='15' title ='Nueva UM' onclick=\"crearUnidadMedida();\">";
                jQuery("#tbl_insumos").jqGrid('setRowData', ids[i], {finddescripcionins: fil});
                jQuery("#tbl_insumos").jqGrid('setRowData', ids[i], {newunidadmedida: new1});
            }
        },
        ondblClickRow: function (rowid, iRow, iCol, e) {
            grid_ingreso_insumos.jqGrid('editRow', rowid, true, function () {
                $("input, select", e.target).focus();
            });
            return;
        },
        restoreAfterError: true
    });
    grid_ingreso_insumos.navGrid("#page_insumos", {add: false, edit: false, del: false, search: false, refresh: false});
    grid_ingreso_insumos.navButtonAdd('#page_insumos', {
        caption: "Nuevo",
        title: "Agregar nueva fila",
        buttonicon: "ui-icon-plus",
        onClickButton: function () {

            var grid = $("#tbl_insumos")
                    , rowid = 'neo_' + grid.getRowData().length;
            grid.addRowData(rowid, {});
            grid.jqGrid('editRow', rowid, true, function () {
            }, null, null, {}, null, null, function (id) {
                var g = $('#tbl_insumos')
                        , r = g.jqGrid('getLocalRow', id);
                if (!r.id) {
                    g.jqGrid('delRowData', id);
                }
            });
            jQuery('#tbl_insumos').jqGrid('setSelection', rowid);

        },
        position: "first"
    });
    grid_ingreso_insumos.navButtonAdd('#page_insumos', {
        caption: "Guardar",
        title: "Guardar cambios",
        buttonicon: "ui-icon-save",
        onClickButton: function () {
            guardarAPU();
        }
    });
    grid_ingreso_insumos.navButtonAdd('#page_insumos', {
        caption: "",
        title: "Eliminar fila(s) seleccionada(s)",
        buttonicon: "ui-icon-trash",
        onClickButton: function () {
            var datos = jQuery("#tbl_insumos").jqGrid('getGridParam', 'selarrrow')
                    , fila;
            var size = datos.length;
            for (var i = 0; i < size; i++) {
                fila = datos[i];

                jQuery("#tbl_insumos").jqGrid('delRowData', fila);
                i = i - 1;
                size = size - 1;

            }
        },
        position: "last"
    });
    //}

}

function listarTiposMaterial() {
    var Result = {};
    $.ajax({
        type: 'GET',
        url: "/fintra/controlleropav?estado=Procesos&accion=APU&opcion=5",
        dataType: 'json',
        async: false,
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    Result = {};
                } else {
                    Result = json;
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return Result;
}

function ajaxSelect(sql, id) {

    var resultado;
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'get',
        data: {opcion: 2, informacion: JSON.stringify({query: sql, filtros: []})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '300', 'auto', false);
                    resultado = {};
                } else {
                    resultado = json;
                }
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {
            console.log('error');

        }
    });
    return resultado;
}

function buscarInsumoxFiltro(cl) {

    $('#div_filtro_insumos').fadeIn("slow");
    var fila = jQuery("#tbl_insumos").getRowData(cl).idtipoinsumo;
    AbrirDivFiltroInsumos();
    cargarCombo('categoria', [fila]);
    CargarInsumosxFiltro();
    $('#idregistro').val(cl);
}

function AbrirDivFiltroInsumos() {
    $("#div_filtro_insumos").dialog({
        width: 500,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'FILTROS DE BUSQUEDA DE INSUMOS',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function cargarCombo(id, filtro) {
    var elemento = $('#' + id)
            , sql = (id === 'categoria') ? 'categorias' : 'subcategoriaxcategoria';
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'get',
        data: {opcion: 2, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');

                    }
                }
            } finally {

            }
        },
        error: function () {

        }
    });

}

function guardarAPU() {
    var grid = jQuery("#tbl_insumos")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;
    var grupo_apu = $('#grupo_apu').val(), nomapu = $('#nomapu').val(), unidadm = $('#unmed').val();
    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);
        if (data.descripcionins === '') {
            error = true;
            mensajesDelSistema('Escoja Un Insumo', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else {
            if (data.unidadmedida === '') {

                error = true;
                mensajesDelSistema('Escoja una Unidad de Medida', '300', 'auto', false);
                grid.jqGrid('editRow', filas[i], true, function () {
                    $("input, select", e.target).focus();
                });
                break;
            } else {
                if (data.cantidad === '') {

                    error = true;
                    mensajesDelSistema('Digite la Cantidad', '300', 'auto', false);
                    grid.jqGrid('editRow', filas[i], true, function () {
                        $("input, select", e.target).focus();
                    });
                    break;
                } else {
                    if (data.rendimiento === '') {

                        error = true;
                        mensajesDelSistema('Digite el rendimiento', '300', 'auto', false);
                        grid.jqGrid('editRow', filas[i], true, function () {
                            $("input, select", e.target).focus();
                        });
                        break;
                    }
                }
            }

        }

        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }

    if ($('#nomapu').val() === '') {
        error = true;
        mensajesDelSistema('Digite el Nombre del APU', '300', 'auto', false);
    }

    if ($('#unidadm').val() === '') {
        error = true;
        mensajesDelSistema('Escoja una Unidad de Medida', '300', 'auto', false);
    }

    if (error)
        return;

    $.ajax({        
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 7, informacion: JSON.stringify({rows: filas}), grupo_apu: grupo_apu, nomapu: nomapu, unidadm: unidadm},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    CargarGruposAPU();
                    $('#nomapu').val('');
                    $('#tbl_insumos').jqGrid('GridUnload');
                    GridInsumos();
                    $('#tbl_materiales_apu').jqGrid('GridUnload');
                    $('#tbl_equipo_apu').jqGrid('GridUnload');
                    $('#tbl_maobra_apu').jqGrid('GridUnload');
                    $('#tbl_herramientas_apu').jqGrid('GridUnload');

                }
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {

        }
    });
}

function GridInsumosEdit(ids) {

    var grid_ingreso_insumos = jQuery("#tbl_insumos");
    //var id = "0";
    var url = '/fintra/controlleropav?estado=Procesos&accion=APU&opcion=9&ids=' + ids;

    /*if ($("#gview_tbl_insumos").length) {
     reloadGridIngresoCatalogo(grid_ingreso_catalogo, arrNames, arrColModel);
     } else {*/

    grid_ingreso_insumos.jqGrid({
        caption: "Lista de Insumos",
        url: url,
        datatype: 'json',
        height: '240',
        width: '1050',
        colNames: ['ID', 'Tipo Insumo', 'Id Tipo Insumo', 'Descripcion Insumo', 'Find', 'Id Descripcion Insumo', 'Unidad de Medida', 'Id Unidad de Medida', 'Nueva UM', 'Cantidad', 'Rendimiento'],
        colModel: [
            {name: 'id', index: 'id', hidden: true, key: true},
            {name: 'tipoinsumo', index: 'tipoinsumo', align: 'center', width: 140, search: false, editable: true, edittype: 'select', editrules: {required: true},
                editoptions: {
                    //value: ajaxSelect('ConsultaTiposInsumos', ''),
                    value: listarTiposMaterial(),
                    style: "width: 140px",
                    dataInit: function (elem) {
                        if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
                            // we are NOT in the searching bar
                            $(elem).find("option[value=\"\"]").remove();
                            setTimeout(function () {
                                $(elem).trigger('change');
                            }, 500);
                        }
                    },
                    dataEvents: [{type: 'change', fn: function (e) {
                                try {

                                    var rowid = e.target.id.replace("_tipoinsumo", "");

                                    id = e.target.value.replace("_", "");

                                    grid_ingreso_insumos.jqGrid('setCell', rowid, "idtipoinsumo", id);

                                    ////////////////////////////
                                    /*var jsonIns = listarInsumosXTipo(e.target.value.replace("_", ""));
                                     cargaListaDependentSelect(rowid + "_descripcionins", jsonIns);
                                     var idinsumo = grid_ingreso_insumos.getRowData(rowid).iddescripcionins;
                                     $("select#" + rowid + "_descripcionins").val(idinsumo);
                                     $("select#" + rowid + "_descripcionins").trigger('change');*/
                                    ////////////////////////////

                                } catch (exc) {
                                }
                                return;
                            }
                        }, {type: "keyup", fn: function (e) {
                                $(e.target).trigger("change");
                            }
                        }]
                }
            },
            {name: 'idtipoinsumo', index: 'idtipoinsumo', hidden: true},
            {name: 'descripcionins', index: 'descripcionins', width: 380, edittype: 'text', resizable: false, align: 'center'},
            {name: 'finddescripcionins', index: 'finddescripcionins', width: 30, edittype: 'text', resizable: false, align: 'center'},
            {name: 'iddescripcionins', index: 'iddescripcionins', hidden: true},
            {name: 'unidadmedida', index: 'unidadmedida', align: 'center', width: 150, search: false, editable: true, edittype: 'select', editrules: {required: false},
                editoptions: {
                    value: ajaxSelect('ConsultaUnidadesMed', ''),
                    style: "width: 150px",
                    dataInit: function (elem) {
                        if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
                            // we are NOT in the searching bar
                            $(elem).find("option[value=\"\"]").remove();
                            setTimeout(function () {
                                $(elem).trigger('change');
                            }, 500);
                        }
                    },
                    dataEvents: [{type: 'change', fn: function (e) {
                                try {

                                    var rowid = e.target.id.replace("_unidadmedida", "");

                                    id = e.target.value.replace("_", "");

                                    grid_ingreso_insumos.jqGrid('setCell', rowid, "idunidadmedida", id);

                                } catch (exc) {
                                }
                                return;
                            }
                        }, {type: "keyup", fn: function (e) {
                                $(e.target).trigger("change");
                            }
                        }]
                }
            },
            {name: 'idunidadmedida', index: 'idunidadmedida', hidden: true},
            {name: 'newunidadmedida', hidden: false, index: 'newunidadmedida', width: 50, edittype: 'text', resizable: false, align: 'center'},
            {name: 'cantidad', index: 'cantidad', align: 'center', width: 100, search: false, editable: true, formatter: 'number',
                formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 2, prefix: ""},
                editoptions: {size: 50, dataInit: function (elem) {
                        $(elem).bind("keypress", function (e) {
                            //alert(soloNumeros(e));
                            return decimales(e, $(this));
                        });
                    }
                }},
            {name: 'rendimiento', index: 'rendimiento', align: 'center', width: 95, search: false, editable: true, formatter: 'number',
                formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 2, prefix: ""},
                editoptions: {size: 15, dataInit: function (elem) {
                        $(elem).bind("keypress", function (e) {
                            return decimales(e, $(this));
                        });
                    }
                }
            }
        ],
        rowNum: 10000,
        rowTotal: 10000000,
        pager: '#page_insumos',
        loadonce: true,
        rownumWidth: 40,
        gridview: true,
        viewrecords: true,
        rownumbers: true,
        hidegrid: false,
        shrinkToFit: false,
        footerrow: false,
        reloadAfterSubmit: true,
        multiselect: true,
        cellsubmit: 'clientArray',
        editurl: 'clientArray',
        jsonReader: {
            root: 'rows',
            repeatitems: false,
            id: '0'
        },
        ajaxGridOptions: {
            async: false
        },
        gridComplete: function () {
            var ids = jQuery("#tbl_insumos").jqGrid('getDataIDs');
            for (var i = 0; i < ids.length; i++) {
                var cl = ids[i];
                fil = "<img src='/fintra/images/botones/iconos/lupaOver.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular' onclick=\"buscarInsumoxFiltro('" + cl + "');\">";
                new1 = "<img src='/fintra/images/botones/iconos/new.gif' align='absbottom'  name='nuevo' id='nuevo' width='15' height='15' title ='Nueva UM' onclick=\"crearUnidadMedida();\">";
                jQuery("#tbl_insumos").jqGrid('setRowData', ids[i], {finddescripcionins: fil});
                jQuery("#tbl_insumos").jqGrid('setRowData', ids[i], {newunidadmedida: new1});
            }
        },
        ondblClickRow: function (rowid, iRow, iCol, e) {
            grid_ingreso_insumos.jqGrid('editRow', rowid, true, function () {
                $("input, select", e.target).focus();
            });
            return;
        },
        restoreAfterError: true
    });
    grid_ingreso_insumos.navGrid("#page_insumos", {add: false, edit: false, del: false, search: false, refresh: false});
    grid_ingreso_insumos.navButtonAdd('#page_insumos', {
        caption: "Nuevo",
        title: "Agregar nueva fila",
        buttonicon: "ui-icon-plus",
        onClickButton: function () {

            var grid = $("#tbl_insumos")
                    , rowid = 'neo_' + grid.getRowData().length;
            grid.addRowData(rowid, {});
            grid.jqGrid('editRow', rowid, true, function () {
            }, null, null, {}, null, null, function (id) {
                var g = $('#tbl_insumos')
                        , r = g.jqGrid('getLocalRow', id);
                if (!r.id) {
                    g.jqGrid('delRowData', id);
                }
            });
            jQuery('#tbl_insumos').jqGrid('setSelection', rowid);

        },
        position: "first"
    });
    grid_ingreso_insumos.navButtonAdd('#page_insumos', {
        caption: "Guardar",
        title: "Guardar cambios",
        buttonicon: "ui-icon-save",
        onClickButton: function () {
            guardarAPUEdit(ids);
        }
    });
    grid_ingreso_insumos.navButtonAdd('#page_insumos', {
        caption: "",
        title: "Eliminar fila(s) seleccionada(s)",
        buttonicon: "ui-icon-trash",
        onClickButton: function () {
            var datos = jQuery("#tbl_insumos").jqGrid('getGridParam', 'selarrrow')
                    , fila;
            var size = datos.length;
            for (var i = 0; i < size; i++) {
                fila = datos[i];

                jQuery("#tbl_insumos").jqGrid('delRowData', fila);
                i = i - 1;
                size = size - 1;

            }
        },
        position: "last"
    });
    //}

}

function guardarAPUEdit(ids) {
    var grid = jQuery("#tbl_insumos")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;
    var grupo_apu = $('#grupo_apu').val(), nomapu = $('#nomapu').val();
    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);
        if (data.descripcionins === '') {
            error = true;
            mensajesDelSistema('Escoja Un Insumo', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else {
            if (data.unidadmedida === '') {

                error = true;
                mensajesDelSistema('Escoja una Unidad de Medida', '300', 'auto', false);
                grid.jqGrid('editRow', filas[i], true, function () {
                    $("input, select", e.target).focus();
                });
                break;
            } else {
                if (data.cantidad === '') {

                    error = true;
                    mensajesDelSistema('Digite la Cantidad', '300', 'auto', false);
                    grid.jqGrid('editRow', filas[i], true, function () {
                        $("input, select", e.target).focus();
                    });
                    break;
                }
            }

        }

        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }

    if ($('#nomapu').val() === '') {
        error = true;
        mensajesDelSistema('Digite el Nombre del APU', '300', 'auto', false);
    }

    if (error)
        return;

    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 10, informacion: JSON.stringify({rows: filas}), grupo_apu: grupo_apu, nomapu: nomapu, ids: ids},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    CargarGruposAPU();
                    $('#nomapu').val('');
                    $('#tbl_insumos').jqGrid('GridUnload');
                    GridInsumos();

                }
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {

        }
    });
}

function numeros(e)
{
    var tecla = (document.all) ? e.keyCode : e.which;
    if (tecla === 8)
        return true;
    var patron = /\d/;
    var te = String.fromCharCode(tecla);
    return patron.test(te);
}

function crearUnidadMedida() {
    $('#nomunidad').val('');
    $('#div_unidad_medida').fadeIn('slow');
    AbrirDivCrearUnidadMedida();
}

function AbrirDivCrearUnidadMedida() {
    $("#div_unidad_medida").dialog({
        width: 500,
        height: 160,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR UNIDAD DE MEDIDA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarUnidadMedida();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarUnidadMedida() {
    var nomunidad = $('#nomunidad').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=APU';
    if (nomunidad !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 12,
                nombre: nomunidad
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        mensajesDelSistema("Se creo la Unidad de Medida...", '250', '150', true);
                        $('#nomunidad').val('');
                        var u = json.idCab;
                        $('.unidadm').append('<option value="' + u + '">' + nomunidad.toUpperCase() + '</option>');
                        cargarComboUnidadM([]);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear la unidad!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }
}

function decimales(e, thi) {
// Backspace = 8, Enter = 13, ?0? = 48, ?9? = 57, ?.? = 46
    var field = thi;
    key = e.keyCode ? e.keyCode : e.which;

    if (key === 8)
        return true;
    if (key > 47 && key < 58) {
        if (field.val() === "")
            return true;
        var existePto = (/[.]/).test(field.val());
        if (existePto === false) {
            regexp = /.[0-9]{10}$/;
        }
        else {
            regexp = /.[0-9]{2}$/;
        }

        return !(regexp.test(field.val()));
    }
    if (key === 46) {
        if (field.val() === "")
            return false;
        regexp = /^[0-9]+$/;
        return regexp.test(field.val());
    }
    return false;
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function mensajeConfirmAction(msj, width, height, okAction, id) {
    $("#msj2").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {                
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function cerrarDialogo(Nom_div){
    $("#"+Nom_div).dialog("destroy");    
}