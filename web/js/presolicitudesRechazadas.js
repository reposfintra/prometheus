let grid_tabla;
var solicitud;


$(document).ready(function () {
    cargaPresolicitudesMicrocredito();
});

function mensajesDelSistema(msj, width, height, swHideDialog, execFn) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }

    if (typeof execFn === 'function') {
        $("#info").dialog({
            width: width,
            height: height,
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closable: false,
            closeOnEscape: false,
            buttons: {
                "Aceptar": function () {
                    $(this).dialog("destroy");
                    execFn();
                },
                "Cancelar": function () {
                    $(this).dialog("destroy");
                }
            }
        });
    } else {
        $("#info").dialog({
            width: width,
            height: height,
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closable: false,
            closeOnEscape: false,
            buttons: {
                "Aceptar": function () {
                    $(this).dialog("destroy");
                }
            }
        });
    }
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargaPresolicitudesMicrocredito() {
    grid_tabla = $("#tabla_Presolicitudes");
    if ($("#gview_tabla_Presolicitudes").length) {
        reloadGridTabla(grid_tabla, 94);
    } else {
        grid_tabla.jqGrid({
            caption: "PRESOLICITUDES",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '550',
            width: 'auto',
            colModel: [
                {label: 'Unidad Negocio',name: 'entidad', index: 'entidad', width: 150, sortable: true, align: 'center', hidden: false},
                {label: 'Numero solicitud',name: 'numero_solicitud', index: 'numero_solicitud', width: 80, sortable: true, align: 'center', hidden: false, key: true},
                {label: 'Tipo Cr�dito',name: 'tipo_credito', index: 'tipo_credito', width: 120, sortable: true, align: 'center', hidden: false},
                {label: 'Identificacion',name: 'identificacion', index: 'identificacion', width: 90, sortable: true, align: 'center', hidden: false},
                {label: 'Cliente',name: 'cliente', index: 'nombre_cliente', width: 250, sortable: true, align: 'left', hidden: false},
                {label: 'Monto credito',name: 'monto_credito', index: 'monto_credito', sortable: true, width: 80, align: 'right', sorttype: 'currency', formatter: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}
                },
                {label: 'Valor cuota',name: 'valor_cuota', index: 'valor_cuota', sortable: true, width: 80, align: 'right', sorttype: 'currency', formatter: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}
                },
                {label: 'Plazo',name: 'numero_cuotas', index: 'numero_cuotas', width: 50, sortable: true, align: 'center', hidden: false},
                {label: 'Compra cartera',name: 'compra_cartera', index: 'compra_cartera', width: 80, sortable: true, align: 'center', hidden: false},
                {label: 'Fecha presolicitud',name: 'fecha_presolicitud', index: 'fecha_presolicitud', width: 100, sortable: true, align: 'center', hidden: false},
                {label: 'departamento', name: 'departamento', index: 'departamento', sortable: false, align: "center", width: 0, hidden: true},
                {label: 'ciudad', name: 'ciudad', index: 'ciudad', sortable: false, align: "center", width: 0, hidden: true},
                {label: 'fecha_pago', name: 'fecha_pago', index: 'fecha_pago', sortable: false, align: "center", width: 0, hidden: true},
                {label: 'tipo_identificacion', name: 'tipo_identificacion', index: 'tipo_identificacion', hidden: true},
                {label: 'fecha_expedicion', name: 'fecha_expedicion', index: 'fecha_expedicion', sortable: false, align: "center", width: 0, hidden: true},
                {label: 'fecha_nacimiento', name: 'fecha_nacimiento', index: 'fecha_nacimiento', sortable: false, align: "center", width: 0, hidden: true},
                {label: 'primer_apellido', name: 'primer_apellido', index: 'primer_apellido', sortable: false, align: "center", width: 0, hidden: true},
                {label: 'primer_nombre', name: 'primer_nombre', index: 'primer_nombre', sortable: false, align: "center", width: 0, hidden: true},
                {label: 'email', name: 'email', index: 'email', sortable: false, align: "center", width: 0, hidden: true},
                {label: 'telefono', name: 'telefono', index: 'telefono', sortable: false, align: "center", width: 0, hidden: true},
                {label: 'Acci�n', name: "acciones", width: 80, 
                    formatter: function(cellvalue, options, rowObject) {
                        return '<button id="reingresar" onclick="reingresarSolicitud(' + options.rowId 
                                + ')" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span class="ui-button-text">Reingresar</span></button>';
                    }
                }
            ],
            rowNum: 1000,
            rowTotal: 10000,
            loadonce: true,
            gridview: true,
            hidegrid: false,
            shrinkToFit: true,
            viewrecords: true,
            rownumbers: true,
            pager: '#pager',
            pgtext: "Page {0} of {1}",
            pgbuttons: true,
            sortname: 'invdate',            
            jsonReader: {
                repeatitems: false
            },
            ajaxGridOptions: {
                data: {
                    opcion: 103
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: true}, {});
    }
}

function reloadGridTabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function reingresarSolicitud(rowId) {
    solicitud = grid_tabla.getRowData(rowId);
    console.log("solicitud");
    console.log(solicitud);
    mensajesDelSistema("�Esta seguro que desea reingresar esta solicitud?", "auto", "auto", null, function () {
        window.open(window.location.pathname + "?estado=Menu&accion=Cargar&carpeta=/jsp/fenalco/avales&pagina=presolicitud.jsp", "Presolicitud");
    });
}