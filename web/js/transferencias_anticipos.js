/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $("#tabs").tabs({
        show: function (event, ui) {
            $("#container").html("");
        }

    });

    $("#buscar").click(function () {
        cargarPagina("jsp/logistica_transporte/administrativo/aprobar_anticipos.jsp");
    });

    $("#buscar2").click(function () {
        cargarPagina("jsp/logistica_transporte/administrativo/anular_anticipos.jsp");
    });

    buscarTransportadoras();
    buscarTCuetasTercero();

});

function HacerAlgo() {
    // hacer algo aqu�...
    cargarPagina("jsp/logistica_transporte/administrativo/aprobar_anticipos.jsp");
    //  setTimeout('HacerAlgo()',10000);
}


function buscarTransportadoras() {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        async: false,
        data: {
            opcion: 1
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                try {
                    $('#transportadora').empty();
                    $('#transportadora2').empty();

                    for (var key in json) {
                        $('#transportadora').append('<option value=' + json[key].id + '>' + json[key].razon_social + '</option>');
                        $('#transportadora2').append('<option value=' + json[key].id + '>' + json[key].razon_social + '</option>');
                   
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function buscarTCuetasTercero() {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        async: false,
        data: {
            opcion: 16
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                try {
                    $('#banco_tercero').empty();                 
                    $('#banco_tercero').append("<option value=''>Seleccione</option>");
                     console.log(json);
                    for (var key in json) {             
                        if(json[key].codigo==='BANCOLOMBIA PAB CTA CORRIENTE')
                         $('#banco_tercero').append('<option value=' + json[key].descripcion + '>' + json[key].codigo + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function cargarPagina(pagina) {
    $.ajax({
        type: 'POST',
        url: "" + pagina,
        dataType: 'html',
        success: function (html) {
            $("#container").html(html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}



function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}