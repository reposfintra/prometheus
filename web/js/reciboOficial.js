/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
init();

function init() {
   cargarFiltros();
   cargarTabla();
   cargarFiltrosDatacredito();
}

function cargarFiltros() {
    $.ajax({
        async:false,
        url:'./controller?estado=Recibo&accion=Oficial&opcion=2',
        datatype:'json',
        type:'get',
        success: function(json) {
            var elemento ;
            var opcion;
            if(json.mensaje) {
                alert(json.mensaje); return;
            }
            for (var id in json) {
                try {
                    elemento = $('#' + id);
                    for (var index in json[id]) {
                        try {
                            opcion = document.createElement('option');
                            opcion.id = index;
                            opcion.innerHTML = json[id][index];
                            elemento.append(opcion);
                        } catch (exception) {
                            alert('error :' + index + ' > ' + json[id][index]);
                            continue;
                        }
                    }
                } catch (exception) {
                    alert('error : ' + index + '>' + json[id][index]);
                    continue;
                }
            }
        },
        error: function(result) {
            alert(result);
        }
    });
}

function cargarTabla(cargar) {
    if (cargar) {
        var uneg = $('#uNeg').find('option:selected').attr('id');
        var periodo = $('#periodo').find('option:selected').attr('id') !== 'vacio' 
                      ? $('#periodo').find('option:selected').attr('id') : 'undefined';
        var documento = ($('#documento').val() === '') ? 'undefined' : $('#documento').val();
        var negocio = ($('#negocio').val() === '') ? 'undefined' : $('#negocio').val();
        var cedula = ($('#cedula').val() === '') ? 'undefined' : $('#cedula').val();
        var valor = ($('#valor').val() === '') ? 'undefined' : $('#valor').val();
        
        if (periodo === 'undefined' && documento === 'undefined' 
            && negocio === 'undefined' && cedula === 'undefined' && valor === 'undefined') {
            alert('Seleccione un filtro');
            return;
        }
        jQuery('#tabla').setGridParam({
            url: './controller?estado=Recibo&accion=Oficial&opcion=0'
                    + '&documento=' + documento + '&periodo=' + periodo + '&uneg=' + uneg
                    + '&cedula='+cedula+'&negocio='+negocio+'&valor='+valor,
            mtype: 'get',
            datatype: 'json',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            loadError: function(xhr, status, error) {
                alert(error);
            },
            subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id = subgrid_id+"_t"; 
                jQuery("#"+subgrid_id).html("<table id='"+subgrid_table_id+"'></table>");
                jQuery("#"+subgrid_table_id).jqGrid({
                    url: './controller?estado=Recibo&accion=Oficial&opcion=1'
                        + '&idRop='+row_id,
                    datatype: 'json',
                    colNames: ['ID', 'negocio', 'Fecha factura padre', 'Fecha vencimiento padre',
                             'Cuota', 'Concepto recaudo', 'Dias vencidos', 'Valor concepto',
                             'Interes por mora', 'Gastos de cobranza', 'Subtotal',
                             'Valor abono', 'Saldo','Tipo de pago'],
                    colModel: [
                        {name: 'id', index: 'id', hidden: true},
                        {name: 'negocio', index: 'negocio', sortable: true, align: 'center'},
                        {name: 'fecha_factura_padre', index: 'fecha_factura_padre', sortable: true, align: 'center'},
                        {name: 'fecha_vencimiento_padre', index: 'fecha_vencimiento_padre', sortable: true, align: 'center'},
                        {name: 'cuota', index: 'cuota', sortable: true, align: 'right'},
                        {name: 'concepto_recaudo', index: 'concepto_recaudo', sortable: true},
                        {name: 'dias_vencidos', index: 'dias_vencidos', sortable: true, align: 'right'},
                        {name: 'valor_concepto', index: 'valor_concepto', sortable: true, formatter: 'number', align: 'right'},
                        {name: 'interes_por_mora', index: 'interes_por_mora', sortable: true, formatter: 'number', align: 'right'},
                        {name: 'gastos_cobranza', index: 'gastos_cobranza', sortable: true, formatter: 'number', align: 'right'},
                        {name: 'subtotal', index: 'subtotal', sortable: true, formatter: 'number', align: 'right'},
                        {name: 'valor_abono', index: 'valor_abono', sortable: true, formatter: 'number', align: 'right'},
                        {name: 'total', index: 'total', sortable: true, formatter: 'number', align: 'right'},
                        {name: 'tipo_pago', index: 'tipo_pago', sortable: true, align: 'right'}
                    ],
                    rowNum:150,
                    height:'80%',
                    width:'90%',
                    jsonReader: {
                        root: 'rows',
                        cell:'',
                        repeatitems: false,
                        id: '0'
                    }, ondblClickRow: function (rowid, status, e) {
                         var fila = jQuery("#"+subgrid_table_id).getRowData(rowid);
                       
                        detallePagoReestructuracion(fila['negocio'],$("#documento").val());
                    },
                    
                    loadError: function(xhr, status, error) {
                        alert(error);
                    }
                });
            }
        }).trigger("reloadGrid");
    } else {
        var tabla = jQuery("#tabla");
        tabla.jqGrid({
            caption: '',
            datatype: 'local',
            data: {},
            width: 1750,
            height: 500,
            rowTotal: 500000,
            rowNum: 100,
            rowList: [25, 50, 75, 100, 125, 150, 175, 200, 500],
            pager: $('#page'),
            viewrecords: true,
            subGrid: true,
            gridview: true,
            hidegrid: true,
            shrinkToFit: true,
            loadonce: true,
            colNames: ['ID', 'No. extracto', 'Unidad de negocio',
                'Fecha ultimo pago', 'Negocio', 'Cedula', 'Nombre cliente',
                'Direccion', 'Capital', 'Intereses por mora', 
                'Gastos de cobranza', 'Descuentos','Total'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, key:true},
                {name: 'documento', index: 'documento', sortable: true},
                {name: 'unidad_negocio', index: 'unidad_negocio', sortable: true},
                {name: 'fecha_ultimo_pago', index: 'fecha_ultimo_pago', align: 'center', sorttype: 'date',
                    formatter: 'date', formatoptions: {newformat: 'Y-m-d'}, datefmt: 'Y-m-d',
                    searchoptions: {
                        sopt: ['eq', 'ne'],
                        dataInit: function(elem) {
                            setTimeout(function() {
                                $(elem).datepicker({
                                    dateFormat: 'yy-mm-dd',
                                    autoSize: true,
                                    changeYear: true,
                                    changeMonth: true,
                                    showButtonPanel: true,
                                    showWeek: true,
                                    onSelect: function() {
                                    }
                                });
                            }, 100);
                        }
                    }},
                {name: 'negocio', index: 'negocio', sortable: true},
                {name: 'cedula', index: 'cedula', sortable: true},
                {name: 'nombre_cliente', index: 'nombre_cliente', sortable: true},
                {name: 'direccion', index: 'direccion', sortable: true},
                {name: 'subtotal', index: 'subtotal', sortable: true, formatter: 'number', align: 'right'},
                {name: 'total_ixm', index: 'total_ixm', sortable: true, formatter: 'number', align: 'right'},
                {name: 'total_gac', index: 'total_gac', sortable: true, formatter: 'number', align: 'right'},
                {name: 'total_descuentos', index: 'total_descuentos', sortable: true, formatter: 'number', align: 'right'},
                {name: 'total', index: 'total', sortable: true, formatter: 'number', align: 'right'}
            ]
        });
        tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    multipleSearch: true
                });
        tabla.jqGrid('gridResize', {minWidth: 1750, minHeight: 500});
    }
}

function cargarHistoricoReportes(){
    var uneg = $('#unidNeg').find('option:selected').attr('id') !== 'vacio' ? $('#unidNeg').find('option:selected').attr('id') : 'undefined';
    var periodo = $('#periodoData').find('option:selected').attr('id') !== 'vacio' ? $('#periodoData').find('option:selected').attr('id') : 'undefined';
    var negocio = ($('#negocio').val() === '') ? 'undefined' : $('#negocio').val();
    var cedula = ($('#cedula').val() === '') ? 'undefined' : $('#cedula').val();
    var url = './controller?estado=Recibo&accion=Oficial&opcion=3&periodo=' + periodo + '&uneg=' + uneg + '&cedula='+cedula+'&negocio='+negocio;
    
    if (periodo === 'undefined' && uneg === 'undefined' && cedula === 'undefined' && negocio === 'undefined') {
        alert('Seleccione un filtro');
        return;
    }else{
        if ($("#gview_Historico").length) {
            refrescarGridHistorico(periodo, uneg, cedula,negocio);
        } else {
            jQuery("#Historico").jqGrid({
                caption: 'Reportados Centrales de Riesgo',
                url: url,
                datatype: 'json',
                height: 500,
                width: 3600,
                colNames: ['Und Negocio', 'Periodo', 'Tipo Id Cliente', 'Identificacion', 'Nombre Cliente','Situacion Titular','Cod Negocio', 'Fecha Apertura', 'Fecha Vencimiento', 'Fecha Corte Proceso', 'Dias Mora', 'Novedad', 'Min Dias Mora', 'Vr. Desembolso', 'Saldo Deuda','Saldo Mora','Cuota Mensual',
                'Numero Cuotas','Cuotas Canceladas','Cuotas en Mora','Fecha Limite Pago','Ultimo Pago','Ciudad Radicacion','Cod Dane Radicacion','Ciudad Residencia','Cod Dane Residencia','Dpto Residencia','Direccion Residencia','Telefono Residencia','Ciudad Laboral','Cod Dane Laboral','Departamento Laboral',
                'Direccion Laboral','Telefono Laboral','Ciudad Correspondencia','Cod Dane Correspondencia','Direccion Correspondencia','Correo Electronico','Celular Solicitante','Tipo','Accion'],
                colModel: [
                    {name: 'un_negocio', index: 'un_negocio', sortable: true, align: 'center', width: '900px'},
                    {name: 'periodo_lote', index: 'periodo_lote', sortable: true, align: 'center', width: '600px'},
                    {name: 'tipo_identificacion', index: 'tipo_identificacion', sortable: true, align: 'center', width: '600px'},
                    {name: 'identificacion', index: 'identificacion', sortable: true, align: 'center', width: '800px'},
                    {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '2000px'},
                    {name: 'situacion_titular', index: 'situacion_titular', sortable: true, align: 'center', width: '700px'},
                    {name: 'negocio', index: 'negocio', sortable: true, width: '600px', align: 'center', key: true},
                    {name: 'fecha_apertura', index: 'fecha_apertura', sortable: true, align: 'center', width: '700px'},
                    {name: 'fecha_vencimiento', index: 'fecha_vencimiento', sortable: true, width: '700px', align: 'center'},
                    {name: 'fecha_corte_proceso', index: 'fecha_corte_proceso', sortable: true, width: '700px', align: 'center'},
                    {name: 'dias_mora', index: 'dias_mora', sortable: true, width: '650px', align: 'center'},
                    {name: 'novedad', index: 'novedad', sortable: true, width: '650px', align: 'center'},
                    {name: 'min_dias_mora', index: 'min_dias_mora', sortable: true, width: '650px', align: 'center'},
                    {name: 'desembolso', index: 'desembolso', sortable: true, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                    {name: 'saldo_deuda', index: 'saldo_deuda', width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                    {name: 'saldo_en_mora', index: 'saldo_en_mora', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                    {name: 'cuota_mensual', index: 'cuota_mensual', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                    {name: 'numero_cuotas', index: 'numero_cuotas', width: '800px', align: 'center'},   
                    {name: 'cuotas_canceladas', index: 'cuotas_canceladas', sortable: false, width: '800px', align: 'center'},
                    {name: 'cuotas_en_mora', index: 'cuotas_en_mora', sortable: false, width: '600px', align: 'center'},
                    {name: 'fecha_limite_pago', index: 'fecha_limite_pago', width: '800px', align: 'center'},
                    {name: 'ultimo_pago', index: 'ultimo_pago', sortable: false, width: '800px', align: 'center'},
                    {name: 'ciudad_radicacion', index: 'ciudad_radicacion', sortable: false, width: '800px', align: 'center'},
                    {name: 'cod_dane_radicacion', index: 'cod_dane_radicacion', width: '650px', align: 'center'},
                    {name: 'ciudad_residencia', index: 'ciudad_residencia', width: '800px', align: 'center'},
                    {name: 'cod_dane_residencia', index: 'cod_dane_residencia', sortable: false, width: '650px', align: 'center'},
                    {name: 'departamento_residencia', index: 'departamento_residencia', sortable: false, width: '800px', align: 'center'},
                    {name: 'direccion_residencia', index: 'direccion_residencia', width: '1600px', align: 'center'},
                    {name: 'telefono_residencia', index: 'telefono_residencia', sortable: false, width: '650px', align: 'center'},
                    {name: 'ciudad_laboral', index: 'ciudad_laboral', width: '800px', align: 'center'},
                    {name: 'cod_dane_laboral', index: 'cod_dane_laboral', width: '600px', align: 'center'},
                    {name: 'departamento_laboral', index: 'departamento_laboral', sortable: false, width: '800px', align: 'center'},
                    {name: 'direccion_laboral', index: 'direccion_laboral', sortable: false, width: '1600px', align: 'center'},
                    {name: 'telefono_laboral', index: 'telefono_laboral', width: '600px', align: 'center'},
                    {name: 'ciudad_correspondencia', index: 'ciudad_correspondencia', sortable: false, width: '800px', align: 'center'},
                    {name: 'cod_dane_correspondencia', index: 'cod_dane_correspondencia', width: '600px', align: 'center'},
                    {name: 'direccion_correspondencia', index: 'direccion_correspondencia', width: '1600px', align: 'center'},
                    {name: 'correo_electronico', index: 'correo_electronico', sortable: false, width: '1600px', align: 'center'},
                    {name: 'celular_solicitante', index: 'celular_solicitante', sortable: false, width: '800px', align: 'center'},
                    {name: 'tipo', index: 'tipo', width: '600px', align: 'center'},
                    {name: 'eliminar', index: 'eliminar', width: '50px', align: 'center', fixed: true} 
                    
                ],
                rowNum: 1000,
                rowTotal: 500000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                pager: $('#page'),
                viewrecords: true,
                hidegrid: false,
                jsonReader: {
                        root: 'rows',
                        cell:'',
                        repeatitems: false,
                        id: '0'
                    },
                gridComplete: function() {
                        var ids = jQuery("#Historico").jqGrid('getDataIDs');
                        for (var i = 0; i < ids.length; i++) {
                            var cl = ids[i];
                            re = "<img src='/fintra/images/stop.png' align='absbottom'  name='eliminar' id='eliminar' width='20' height='20' title ='Eliminar' onclick=\"eliminarRegistro('"+cl+"','eliminarRegistro');\">";
                            jQuery("#Historico").jqGrid('setRowData', ids[i], {eliminar: re}); 
                            
                        }
                        
                    
                    },     
                loadError: function(xhr, status, error) {
                    alert(error);
                }
            });
            jQuery("#Historico").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    multipleSearch: true,
                    caseSensitive: false
            });
                
            jQuery("#Historico").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    }
}
    
}

function refrescarGridHistorico(periodo, uneg, cedula,negocio) {
    var url = './controller?estado=Recibo&accion=Oficial&opcion=3&periodo=' + periodo + '&uneg=' + uneg + '&cedula='+cedula+'&negocio='+negocio;
    
    jQuery("#Historico").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#Historico").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
                
    jQuery("#Historico").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    jQuery('#Historico').trigger("reloadGrid");
}

function ExportDataToExcel() {
    var uneg = $('#unidNeg').find('option:selected').attr('id');
    var periodo = $('#periodoData').find('option:selected').attr('id') !== 'vacio' ? $('#periodoData').find('option:selected').attr('id') : 'undefined';
    var negocio = ($('#negocio').val() === '') ? 'undefined' : $('#negocio').val();
    var cedula = ($('#cedula').val() === '') ? 'undefined' : $('#cedula').val();
    var url = './controller?estado=Recibo&accion=Oficial&opcion=4&periodo=' + periodo + '&uneg=' + uneg + '&cedula='+cedula+'&negocio='+negocio;
    var BASEURL = $("#BASEURL").val();
    if (periodo === 'undefined' && negocio === 'undefined' && cedula === 'undefined') {
        alert('Seleccione un filtro');
        return;
    }else{
    $("#divSalida").dialog("open");
    $("#divSalida").css('height', 'auto');
    $("#divSalida").dialog("option", "position", "center");

    $.ajax({
        dataType: 'html',
        url:url,
        method: 'post',
        success:  function (resp){
            var boton = "<div style='text-align:center'><img src='" + BASEURL +"/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\""+"#divSalida" +"\")' /></div>"
            document.getElementById('resp').innerHTML = resp+ "<br/><br/><br/><br/>"+boton
        },
        error: function(resp){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });  
}
}

function cerrarDiv(div)
{
    $(div).dialog('close');
}

function cargarPrevisualizacionReportados(){
    var uneg = $('#unidNeg').find('option:selected').attr('id');
    var nomNeg = $('#unidNeg').find('option:selected').val();
    var periodo = $('#periodoData').find('option:selected').attr('id');
    var fecha = $('#fecha').val();
    var rangoIni = $('#rangoIni').val();
    var rangoFin = $('#rangoFin').val();
    var url = './controller?estado=Recibo&accion=Oficial';
    
    if(uneg === '' || periodo === '' || fecha === '' || rangoIni === '' || rangoFin === ''){
        alert("Todos los campos son requeridos");
    }else{
        $("#botonAceptar").show();
       if ($("#gview_PrevisualizarReporte").length) {
            refrescarGridPrevisualizarReporte(periodo, uneg, fecha,rangoIni,rangoFin,nomNeg);
        } else {
            jQuery("#PrevisualizarReporte").jqGrid({
                caption: 'Previsualizar Reporte Datacredito',
                url: url,
                datatype: 'json',
                height: 500,
                width: 3600,
                colNames: ['Id','Und Negocio', 'Periodo Anterior','Periodo', 'Tipo Id Anterior','Tipo Id', 'Identificacion Anterior','Identificacion','Nombre Anterior', 'Nombre','Situacion Titular','Cod Negocio','Fecha Apertura Anterior', 'Fecha Apertura','Fecha Vencimiento Anterior', 'Fecha Vencimiento', 'Fecha Corte Proceso', 'Dias Mora','Novedad Anterior', 'Novedad', 'Min Dias Mora', 'Vr. Desembolso', 'Saldo Deuda','Saldo Mora Anterior','Saldo Mora','Cuota Mensual',
                'Numero Cuotas','Cuotas Canceladas','Cuotas en Mora','Fecha Limite Pago','Ultimo Pago','Ciudad Radicacion','Cod Dane Radicacion','Ciudad Residencia','Cod Dane Residencia','Dpto Residencia','Direccion Residencia','Telefono Residencia','Ciudad Laboral','Cod Dane Laboral','Departamento Laboral',
                'Direccion Laboral','Telefono Laboral','Ciudad Correspondencia','Cod Dane Correspondencia','Direccion Correspondencia','Correo Electronico','Celular Solicitante','Tipo'],
                colModel: [
                    {name: 'id', index: 'id', hidden: true, key: true },
                    {name: 'und_negocio', index: 'und_negocio', sortable: false, align: 'center', width: '1000px'},
                    {name: 'hr_periodo', index: 'hr_periodo', sortable: false, align: 'center', width: '960px'},
                    {name: 'periodo', index: 'periodo', sortable: false, align: 'center', width: '600px'},
                    {name: 'hr_tipo_id', index: 'hr_tipo_id', sortable: false, align: 'center', width: '700px'},
                    {name: 'tipo_identificacion', index: 'tipo_identificacion', sortable: false, align: 'center', width: '600px'},
                    {name: 'hr_identificacion', index: 'hr_identificacion', sortable: false, align: 'center', width: '900px'},
                    {name: 'identificacion', index: 'identificacion', sortable: false, align: 'center', width: '800px'},
                    {name: 'hr_nombre', index: 'hr_nombre', sortable: false, align: 'center', width: '2200px'},
                    {name: 'nombre', index: 'nombre', sortable: false, align: 'center', width: '2200px'},
                    {name: 'situacion_titular', index: 'situacion_titular', sortable: false, align: 'center', width: '800px'},
                    {name: 'negocio', index: 'negocio', sortable: false, width: '700px', align: 'center'},
                    {name: 'hr_fecha_apertura', index: 'hr_fecha_apertura', sortable: false, align: 'center', width: '900px'},
                    {name: 'fecha_apertura', index: 'fecha_apertura', sortable: false, align: 'center', width: '800px'},
                    {name: 'hr_fecha_ven', index: 'hr_fecha_ven', sortable: false, width: '900px', align: 'center'},
                    {name: 'fecha_vencimiento', index: 'fecha_vencimiento', sortable: false, width: '800px', align: 'center'},
                    {name: 'fecha_corte_proceso', index: 'fecha_corte_proceso', sortable: false, width: '800px', align: 'center'},
                    {name: 'dias_mora', index: 'dias_mora', sortable: false, width: '650px', align: 'center'},
                    {name: 'hr_novedad', index: 'hr_novedad', sortable: false, width: '800px', align: 'center'},
                    {name: 'novedad', index: 'novedad', sortable: false, width: '700px', align: 'center'},
                    {name: 'min_dias_mora', index: 'min_dias_mora', sortable: false, width: '800px', align: 'center'},
                    {name: 'desembolso', index: 'desembolso', sortable: false, width: '900px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                    {name: 'saldo_deuda', index: 'saldo_deuda', width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                    {name: 'hr_saldo_mora', index: 'hr_saldo_mora', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                    {name: 'saldo_mora', index: 'saldo_mora', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                    {name: 'cuota_mensual', index: 'cuota_mensual', sortable: false, width: '900px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                    {name: 'numero_cuotas', index: 'numero_cuotas', width: '900px', align: 'center'},   
                    {name: 'cuotas_canceladas', index: 'cuotas_canceladas', sortable: false, width: '900px', align: 'center'},
                    {name: 'cuotas_mora', index: 'cuotas_mora', sortable: false, width: '900px', align: 'center'},
                    {name: 'fecha_limite_pago', index: 'fecha_limite_pago', width: '800px', align: 'center'},
                    {name: 'ultimo_pago', index: 'ultimo_pago', sortable: false, width: '800px', align: 'center'},
                    {name: 'ciudad_radicacion', index: 'ciudad_radicacion', sortable: false, width: '900px', align: 'center'},
                    {name: 'cod_dane_radicacion', index: 'cod_dane_radicacion', width: '650px', align: 'center'},
                    {name: 'ciudad_residencia', index: 'ciudad_residencia', width: '900px', align: 'center'},
                    {name: 'departamento_residencia', index: 'departamento_residencia', sortable: false, width: '650px', align: 'center'},
                    {name: 'cod_dane_residencia', index: 'cod_dane_residencia', sortable: false, width: '900px', align: 'center'},
                    {name: 'direccion_residencia', index: 'direccion_residencia', width: '1800px', align: 'center'},
                    {name: 'telefono_residencia', index: 'telefono_residencia', sortable: false, width: '650px', align: 'center'},
                    {name: 'ciudad_laboral', index: 'ciudad_laboral', width: '900px', align: 'center'},
                    {name: 'cod_dane_laboral', index: 'cod_dane_laboral', width: '600px', align: 'center'},
                    {name: 'departamento_laboral', index: 'departamento_laboral', sortable: false, width: '800px', align: 'center'},
                    {name: 'direccion_laboral', index: 'direccion_laboral', sortable: false, width: '1800px', align: 'center'},
                    {name: 'telefono_laboral', index: 'telefono_laboral', width: '600px', align: 'center'},
                    {name: 'ciudad_correspondencia', index: 'ciudad_correspondencia', sortable: false, width: '800px', align: 'center'},
                    {name: 'cod_dane_correspondencia', index: 'cod_dane_correspondencia', width: '600px', align: 'center'},
                    {name: 'direccion_correspondencia', index: 'direccion_correspondencia', width: '1800px', align: 'center'},
                    {name: 'correo_electronico', index: 'correo_electronico', sortable: false, width: '1800px', align: 'center'},
                    {name: 'celular_solicitante', index: 'celular_solicitante', sortable: false, width: '800px', align: 'center'},
                    {name: 'tipo', index: 'tipo', width: '600px', align: 'center'} 
                    
                ],
                rowNum: 30000,
                rowTotal: 500000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                pager: $('#page'),
                viewrecords: true,
                hidegrid: false,
                multiselect: true,
                jsonReader: {
                        root: 'rows',
                        repeatitems: false,
                        id: '0'
                    },
                ajaxGridOptions: {
                    dataType: "json",
                    type: "POST",
                    async: false,
                    data: {
                        opcion: 5,
                        periodo: periodo,
                        uneg: uneg,
                        fecha: fecha,
                        rangoIni: rangoIni, 
                        rangoFin: rangoFin,
                        nomNeg: nomNeg
                    }
                }, 
                loadError: function(xhr, status, error) {
                    alert(error);
                }
            });
            jQuery("#PrevisualizarReporte").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    multipleSearch: true,
                    caseSensitive: false
            });
                
            jQuery("#PrevisualizarReporte").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    }
    }
    
    
}

function refrescarGridPrevisualizarReporte(periodo,uneg,fecha,rangoIni,rangoFin,nomNeg) {
    var url = './controller?estado=Recibo&accion=Oficial&opcion=5&periodo=' + periodo + '&uneg=' + uneg + '&fecha='+fecha+'&rangoIni='+rangoIni+'&rangoFin='+rangoFin+'&nomNeg='+nomNeg;
    
    jQuery("#PrevisualizarReporte").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#PrevisualizarReporte").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    multipleSearch: true,
                    //ignoreCase: true
                    caseSensitive: false
                });
                
    jQuery("#PrevisualizarReporte").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    jQuery('#PrevisualizarReporte').trigger("reloadGrid");
}


function exportarExcelPrevisualizacion() {
    var url = './controller?estado=Recibo&accion=Oficial';
    var BASEURL = $("#BASEURL").val();
    var fullData = jQuery("#PrevisualizarReporte").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);

    $("#divSalida").dialog("open");
    $("#divSalida").css('height', 'auto');
    $("#divSalida").dialog("option", "position", "center");
    $('#imgload').show();
    $('#resp').hide();
    $.ajax({
        dataType: 'html',
        url: url,
        type: "POST",
        data: {
            opcion: 6,
            listado: myJsonString
        },
        success: function (resp) {
            $('#imgload').hide();
            var boton = "<div style='text-align:center'><img src='" + BASEURL + "/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalida" + "\")' /></div>"
            document.getElementById('resp').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
            $('#resp').show();
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });
}

function guardarReportados() {
    var fecha = $('#fecha').val();
    var rangoIni = $('#rangoIni').val();
    var rangoFin = $('#rangoFin').val();
    var periodo = $('#periodoData').find('option:selected').attr('id');
    var uneg = $('#unidNeg').find('option:selected').attr('id');
    var nomNeg = $('#unidNeg').find('option:selected').val();
    var filasId = jQuery("#PrevisualizarReporte").jqGrid('getGridParam', 'selarrrow');
    var listado = "";
    var cedula = "";

   if (filasId != "") {
        var resp = confirm("Desea incluir los registros seleccionados al reporte?");
        if (resp) {
          for (var i = 0; i < filasId.length; i++) {
                var fila = jQuery("#PrevisualizarReporte").getRowData(filasId[i]);
                var codNeg = fila['negocio'];
                var cedula = fila['identificacion'];
                    listado += codNeg + "," + cedula + ";_;";
                
            } 

            var url = './controller?estado=Recibo&accion=Oficial';
            $.ajax({
                type: "POST",
                url: url,
                dataType: "text",
                data: {
                    opcion: 7,
                    periodo:periodo, 
                    uneg:uneg,
                    listado:listado
                   },
                success: function(data) {
                   if (data != "") {
                        alert("Registros Guardados con Exito");
                        refrescarGridPrevisualizarReporte(periodo, uneg, fecha, rangoIni, rangoFin, nomNeg);
                    }

                }

            });
        } 
   } else {
        alert("Debe seleccionar los registros a guardar");
    }
}

function limpiarReporteDatacredito() {
    var resp = confirm("Esta seguro que desea borrar los registros de este reporte?");
    if (resp == true) {
        var fecha = $('#fecha').val();
        var rangoIni = $('#rangoIni').val();
        var rangoFin = $('#rangoFin').val();
        var periodo = $('#periodoData').find('option:selected').attr('id');
        var uneg = $('#unidNeg').find('option:selected').attr('id');
        var nomNeg = $('#unidNeg').find('option:selected').val();

        var url = './controller?estado=Recibo&accion=Oficial&opcion=8&periodo=' + periodo + '&uneg=' + uneg;
        $.ajax({
            type: "POST",
            url: url,
            dataType: "text",
            success: function(data) {
                if (data != "") {
                    alert("Registros Limpiados");
                    refrescarGridPrevisualizarReporte(periodo, uneg, fecha, rangoIni, rangoFin, nomNeg);

                }

            }

        }); 

    }
}

function eliminarRegistro(cl){
    var uneg = $('#unidNeg').find('option:selected').attr('id') !== 'vacio' ? $('#unidNeg').find('option:selected').attr('id') : 'undefined';
    var periodo = $('#periodoData').find('option:selected').attr('id') !== 'vacio' ? $('#periodoData').find('option:selected').attr('id') : 'undefined';
    var negocio = ($('#negocio').val() === '') ? 'undefined' : $('#negocio').val();
    var cedula = ($('#cedula').val() === '') ? 'undefined' : $('#cedula').val();
    var resp = confirm("Esta seguro que desea eliminar este registro?");
    if (resp == true) {
        var registro = jQuery('#Historico').jqGrid('getRowData', cl); 
        var periodo_reg = registro.periodo_lote
        var url = './controller?estado=Recibo&accion=Oficial&opcion=9&periodo_reg=' + periodo_reg + '&negocio=' + cl;
        $.ajax({
            type: "POST",
            url: url,
            dataType: "text",
            success: function(data) {
                if(data != ""){
                    alert("Registro eliminado con �xito");
                    refrescarGridHistorico(periodo, uneg, cedula,negocio);
                }
            },
           error: function(error) {
                    alert("No se elimin� el registro");
                } 

        });
    }
}

function generarReporteDatacredito() {
    var fecha = $('#fecha').val();
    var rangoIni = $('#rangoIni').val();
    var rangoFin = $('#rangoFin').val();
    var periodo = $('#periodoData').find('option:selected').attr('id');
    var uneg = $('#unidNeg').find('option:selected').attr('id');
    
    if (uneg === '' || periodo === '' || fecha === '' || rangoIni === '' || rangoFin === '') {
        alert("Todos los campos son requeridos");
    } else {
        var resp = confirm("Esta seguro que desea generar el reporte?");

        if (resp == true) {
            var url = './controller?estado=Recibo&accion=Oficial&opcion=10&periodo=' + periodo + '&uneg=' + uneg + '&fecha=' + fecha + '&rangoIni=' + rangoIni + '&rangoFin=' + rangoFin;
            $('#div_espera').fadeIn('slow');
            $.ajax({
                type: "POST",
                url: url,
                dataType: "text",
                success: function(data) {
                    $('#div_espera').fadeOut('slow');
                    if (data != "") {
                        alert("Infome generado con �xito, puede descargarlo del Directorio de Archivos");
                    }
                },
                error: function(error) {
                    alert("No se gener� el informe");
                }

            });
        }
    }
}

function cargarFiltrosDatacredito() {
    $.ajax({
        url:'./controller?estado=Recibo&accion=Oficial&opcion=12',
        datatype:'json',
        type:'get',
       
        success: function(json) {
            var elemento;
            var opcion;
            if(json.mensaje) {
                alert(json.mensaje); return;
            }
            for (var id in json) {
                try {
                    elemento = $('#' + id);
                    for (var index in json[id]) {
                        try {
                            opcion = document.createElement('option');
                            opcion.id = index;
                            opcion.innerHTML = json[id][index];
                            elemento.append(opcion);
                        } catch (exception) {
                            alert('error :' + index + ' > ' + json[id][index]);
                            continue;
                        }
                    }
                } catch (exception) {
                    alert('error : ' + index + '>' + json[id][index]);
                    continue;
                }
            }
        },
        error: function(result) {
            alert(result);
        }
    });
}



function detallePagoReestructuracion(negocio, idRop) {
    var grid_detalle_rop = jQuery("#detallerop");
    if ($("#gview_detallerop").length) {
        reloadGridDetalleRop(grid_detalle_rop,negocio, idRop );
    } else {

        grid_detalle_rop.jqGrid({
            caption: "Negocios",
            url: "./controller?estado=Recibo&accion=Oficial&opcion=13&id_rop=" + idRop+"&negocio="+negocio,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: 'auto',
            colNames: ['Negocio', 'Documento', 'Cuota', 'Fecha Vencimiento', 'Periodo vct', 'Dias', 'Estado','Descripcion','Valor item','Accion','Valor accion','Valor llevar a'],
            colModel: [
                {name: 'valor_01', index: 'valor_01', width: 70, align: 'right'},
                {name: 'valor_02', index: 'valor_02', sortable: true, width: 80, align: 'center', key: true},
                {name: 'valor_03', index: 'valor_03', sortable: true, width: 100, align: 'center'},
                {name: 'valor_04', index: 'valor_04', sortable: true, width: 80, align: 'center'},
                {name: 'valor_05', index: 'valor_05', width: 100, align: 'center', hidden: false},
                {name: 'valor_06', index: 'valor_06', sortable: false, width: 90, align: 'center'},
                {name: 'valor_07', index: 'valor_07', width: 120, align: 'center', hidden: false},
                {name: 'valor_08', index: 'valor_08', width: 120, align: 'center', hidden: false},
                {name: 'valor_09', index: 'valor_09', width: 120, align: 'center', hidden: false},
                {name: 'valor_10', index: 'valor_10', width: 120, align: 'center', hidden: false},
                {name: 'valor_11', index: 'valor_11', width: 120, align: 'center', hidden: false},
                {name: 'valor_12', index: 'valor_12', width: 120, align: 'center', hidden: false}
                
                
            ],
            rowNum: 500,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            rownumbers: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            multiselect: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ondblClickRow: function (rowid, status, e) {
               var fila = jQuery("#detallerop").getRowData(rowid);
               cambiaFechaIF(fila);
            
            }, loadError: function (xhr, status, error) {
                alert(error + " " + xhr + " " + status);
                // mensajesDelSistema(error, 250, 200);
            }
        });
    }
    
    
      $("#ver_detalle_rop").dialog({
        width: 1400,
        height: 670,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n 
            "Salir": function() {
               $(this).dialog("close");
            }
        }
    });
}


function reloadGridDetalleRop(grid_detalle_rop,negocio, idRop ) {
    grid_detalle_rop.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Recibo&accion=Oficial&opcion=13&id_rop=" + idRop+"&negocio="+negocio
    });

    grid_detalle_rop.trigger("reloadGrid");
}


function cambiaFechaIF(fila) {

    $("#cod_neg").val(fila['valor_01']);
    $("#fecha_actual").val(fila['valor_04']);
    $("#valor_if").val(fila['valor_09']);
    $("#fechaif").val(fechaActual());
    
    if (fila['valor_08'] === 'INTERESES') {
        if (validate_fechaMayorQue(fechaActual(), fila['valor_04']) === 0) {

            $("#act_if").dialog({
                width: 260,
                height: 240,
                show: "scale",
                hide: "scale",
                resizable: false,
                position: "center",
                modal: true,
                closeOnEscape: false,
                buttons: {//crear bot�n 
                    "Aplicar": function () {
                        actualizarIF();
                    },
                    "Salir": function () {
                        $(this).dialog("close");
                    }
                }
            });

        } else {

            alert("La fecha actual es mayor a la de la factura.");
        }
    }else{
        alert("Solo se pueden modificar las consultas de intereses.");
    }

}

function fechaActual() {
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '-' +
            (month < 10 ? '0' : '') + month + '-' +
            (day < 10 ? '0' : '') + day;
    
    return output;

}

function validate_fechaMayorQue(fechaInicial, fechaFinal) {
    var valuesStart = fechaInicial.split("-");
    var valuesEnd = fechaFinal.split("-");
    // Verificamos que la fecha no sea posterior a la actual 
    var dateStart = new Date(valuesStart[0], (valuesStart[1] - 1), valuesStart[2]);
    var dateEnd = new Date(valuesEnd[0], (valuesEnd[1] - 1), valuesEnd[2]);
    if (dateStart >= dateEnd) {
        return 1;
    }
    return 0;
}


function actualizarIF() {

    var url = './controller?estado=Recibo&accion=Oficial';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 14,
            cod_neg: $("#cod_neg").val(),
            fecha_actual: $("#fecha_actual").val(),
            valor_if: $("#valor_if").val(),
            fechaif: $("#fechaif").val()
        },
        success: function (json) {
            if (json.respuesta === 'OK') {
                alert("Se ha realizado la actulizacion satisfactoriamente");
            } else {
                alert("Lo sentimos no se ha podido actualizar el documento");
            }
        }

    });

}

function previo_genArchivo() {
    $("#gen_arch").dialog({
        width: 230,
        height: 260,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n 
            "Generar Archivo Global": function () {
                generarArchivoGlobal();
            },
            "Generar Archivo Efecty Otros": function () {
                generarArchivoEfecty('OTROS');
            },
            "Generar Archivo Efecty Educativo": function () {
                generarArchivoEfecty('EDU');
            }
        }
    });
}

function generarArchivoGlobal() {
    $.ajax({
        type: "POST",
        url: './controller?estado=Recibo&accion=Oficial',
        dataType: "json",
        data: {
            opcion: 15,
            informacion: JSON.stringify({periodo:$("#arh_periodo").val(), ciclo:$("#arh_ciclo").val()})
        },
        success: function (json) {
            alert(json.mensaje);            
        }
    });
}

function generarArchivoEfecty(tipo) {
    $.ajax({
        type: "POST",
        url: './controller?estado=Recibo&accion=Oficial',
        dataType: "json",
        data: {
            opcion: 16,
            informacion: JSON.stringify({periodo:$("#arh_periodo").val(), ciclo:$("#arh_ciclo").val(),tipo:tipo})            
        },
        success: function (json) {
            alert(json.mensaje);            
        }
    });
}

