/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
            
    $('.solo-numero').keyup(function() {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });  
    
    $('.solo-numero').live('blur',function (event) {
           this.value = numberConComas(this.value);            
    }); 
    
    $("#fechainicio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    $("#fechafinal").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    var myDate = new Date();
    $("#fechainicio").datepicker("setDate", myDate);
    $("#fechafinal").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

  
    if($('#div_importar_anticipo').is(':visible')) {
      cargarProductosTransp(false);
      $('#format').val('s');
    }else{
        buscarTransportadoras();
        cargarProductosTransp(true);
    } 
    $("#clearanticipos").click(function () {      
        DeshabilitarControles(false);
        resetValues();
        $("#tabla_anticipos").jqGrid("clearGridData", true);
        $('#grid_anticipos').fadeOut("fast");
    }); 
    $("#buscaranticipos").click(function () {
        if ($('#transportadora').val()===''){
            mensajesDelSistema('Debe escoger la transportadora', '250', '150');
        }else if($("#fechainicio").val() > $("#fechafinal").val()){
            mensajesDelSistema('La fecha final no puede ser inferior a la inicial', '250', '175');
        }else{
              DeshabilitarControles(true);
              buscarAnticipos();
        }     
    });     
    
    $("#clearanticipos_anul").click(function () {      
        DeshabilitarControles(false);
        resetValues();
        $("#tabla_anticipos_anul").jqGrid("clearGridData", true);
        $('#grid_anticipos_anul').fadeOut("fast");
    }); 
    $("#buscaranticipos_anul").click(function () {
        if ($('#transportadora').val()===''){
            mensajesDelSistema('Debe escoger la transportadora', '280', '160');
        }else if($("#fechainicio").val() > $("#fechafinal").val()){
            mensajesDelSistema('La fecha final no puede ser inferior a la inicial', '280', '175');
        }else if ($('#planilla').val()===''){
           mensajesDelSistema('Debe ingresar el No de planilla', '280', '160');
        }else{       
            var json,gridTitle = '', root='';
            if($('#chk_reanticipo').is(":checked")){
                json = {
                    opcion:30,
                    id_transportadora: $('#transportadora').val(),                   
                    planilla: $('#planilla').val()
                };
                gridTitle = 'REANTICIPOS'; 
            }else{
                json = {
                       opcion:28,
                       id_transportadora: $('#transportadora').val(),
                       fecha_inicio:$('#fechainicio').val(),
                       fecha_fin:$('#fechafinal').val(),
                       planilla:$('#planilla').val(),
                       placa: $('#placa').val(),
                       producto:''
                };   
                gridTitle = 'ANTICIPOS'; 
                root = 'rows';
            }
            DeshabilitarControles(true);
            listarAnticipoReanticipos(json,gridTitle,root);
        }     
    });     

    $("#subirArchivo").click(function () {
       importarAnticipos();
    });

});

function importarAnticipos() {
    var nomArchivo = $("input[type='file']").val().split('/').pop().split('\\').pop();
    var extension = $('#examinar').val().split('.').pop().toLowerCase();  
    if (nomArchivo === "") {
        mensajesDelSistema("No ha seleccionado ning&uacute;n archivo. Por favor, seleccione uno!!", '250', '165');
        return;
    }else if (extension.toUpperCase() !== 'XLS'){
        $("#examinar").val("");
        mensajesDelSistema("La extensi&oacute;n del archivo no es v&aacute;lida. La extensi&oacute;n debe ser xls", '280', '170');
        return;
    }else{       
        subirArchivoAnticipos();     
    }
}

function subirArchivoAnticipos() {

    var fd = new FormData(document.getElementById('formulario'));
    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function(){
        $.ajax({
            async: false,
            url: "./controller?estado=Administracion&accion=Logistica&opcion=26&producto="+$('#productos').val()+"&formato="+$('#format').val(),
            type: 'POST',
            dataType: 'json',
            data: fd,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (json) {  
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        $("#examinar").val("");      
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        $("#examinar").val("");                                        
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Anticipos cargados satisfactoriamente!!", '280', '150');
                    } else {
                        $("#examinar").val("");      
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema(json.respuesta + ', VERIFIQUE EL LOG', '280', '165', false);
                        return;
                    }

                } else {
                    $("#examinar").val("");      
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci&oacute;n!!", '280', '170');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    },500);
}

function buscarTransportadoras() {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        async:false,
        data: {
            opcion: 34
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                try {
                    $('#transportadora').empty();
//                    $('#transportadora').append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#transportadora').append('<option value=' + json[key].id + '>' + json[key].razon_social + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                  mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function buscarAnticipos() {      
    $("#grid_anticipos").fadeIn();
    var grid_tbl_anticipos = jQuery("#tabla_anticipos");
     if ($("#gview_tabla_anticipos").length) {
        refrescarGridAnticipos();
     }else {
        grid_tbl_anticipos.jqGrid({
            caption: "LISTADO DE ANTICIPOS",
            url: "./controller?estado=Administracion&accion=Logistica",           	 
            datatype: "json",  
            height: '290',
            width: '1150',           
            colNames: ['Id','C&oacute;d. Agencia','C&oacute;d Transportadora', 'Nombre', 'Id Conductor', 'Conductor', 'Placa', 'Planilla',  'Vlr. Neto Ant.', 'Origen', 'Destino', 'C&oacute;d. Producto', 'Producto',  'Fecha Anticipo', 'Fecha Sistema'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'codigo_agencia', index: 'codigo_agencia', width: 90, align: 'center'},
                {name: 'codigo_empresa', index: 'codigo_empresa', width: 110, align: 'center'},         
                {name: 'nombre_empresa', index: 'nombre_empresa', width: 180, align: 'left'},               
                {name: 'cedula_conductor', index: 'cedula_conductor', width: 100, align: 'left'}, 
                {name: 'nombre_conductor', index: 'nombre_conductor', width: 180, align: 'left'},  
                {name: 'placa', index: 'placa', width: 90, align: 'center'}, 
                {name: 'planilla_interna', index: 'planilla_interna', width: 90, align: 'center'},
                {name: 'valor_reanticipo', index: 'valor_reanticipo', sortable: true, editable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'origen', index: 'origen', width: 120, align: 'center'},  
                {name: 'destino', index: 'destino', width: 120, align: 'center'},
                {name: 'codigo_producto', index: 'codigo_producto', width: 120, align: 'center'},  
                {name: 'nombre_producto', index: 'nombre_producto', width: 160, align: 'left'}, 
                {name: 'fecha_anticipo', index: 'fecha_anticipo', width: 90, align: 'center'},  
                {name: 'fecha_envio', index: 'fecha_envio', width: 120, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_anticipos'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,         
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion:28,
                       id_transportadora: $('#transportadora').val(),
                       fecha_inicio:$('#fechainicio').val(),
                       fecha_fin:$('#fechafinal').val(),
                       planilla:$('#planilla').val(),
                       placa:'',
                       producto:$('#productos').val()
                    }
            },   
            ondblClickRow: function(rowid) {                  
                abrirVentanaReanticipo(rowid);                                                                           
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_anticipos", {add: false, edit: false, del: false, search: false, refresh: false}, {});
    }  
       
}

function refrescarGridAnticipos(){   
    jQuery("#tabla_anticipos").setGridParam({
        url: "./controller?estado=Administracion&accion=Logistica",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: {
                opcion: 28,
                id_transportadora: $('#transportadora').val(),
                fecha_inicio: $('#fechainicio').val(),
                fecha_fin: $('#fechafinal').val(),
                planilla: $('#planilla').val(),
                placa: '',
                producto:$('#productos').val()
            }
        }       
    });
    
    jQuery('#tabla_anticipos').trigger("reloadGrid");
}

function abrirVentanaReanticipo(id){
    var cod_agencia = $("#tabla_anticipos").getRowData(id).codigo_agencia;
    var cod_transportadora = $("#tabla_anticipos").getRowData(id).codigo_empresa;
    var planilla = $("#tabla_anticipos").getRowData(id).planilla_interna;
    var origen = $("#tabla_anticipos").getRowData(id).origen;
    var destino = $("#tabla_anticipos").getRowData(id).destino;
    var fecha_sistema = $("#tabla_anticipos").getRowData(id).fecha_envio;  
    
    $('#codigo_empresa').val(cod_transportadora);
    $('#codigo_agencia').val(cod_agencia);
    $('#num_planilla').val(planilla);
    $('#fecha_reanticipo').val(fecha_sistema.toString("yyyy-mm-dd").substr(0,10));  
    $('#origen').val(origen);
    $('#destino').val(destino);
    $('#vlr_reanticipo').val('');
    $('#porc_comision').val(0);
    $('#vlr_comision').val(0);
    $('#vlr_desembolsar').val(0);
    $('#banco').val('');
    $('#sucursal').val('');
    $('#tipo_cuenta').val('');
    $('#cuenta').val('');
    $('#cedula_titular_cta').val('');
    $('#nombre_titular_cta').val(''); 
    ventanaEnvioReanticipo();
}

function ventanaEnvioReanticipo() {
    
    $("#dialogReanticipo").dialog({
        width: 570,
        height: 345,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: true, 
        buttons: {    
            "Enviar": function () {          
		enviarInfoReanticipo();            
            },
            "Salir": function () {          
		$(this).dialog("close");               
            }
        }
    });
}

function enviarInfoReanticipo(){
    
    var origen = $("#origen").val(); 
    var destino = $("#destino").val(); 
    var vlr_reanticipo = $("#vlr_reanticipo").val();  
    var banco = $("#banco").val();  
    var sucursal = $("#sucursal").val();  
    var tipo_cuenta = $("#tipo_cuenta").val();  
    var cuenta = $("#cuenta").val();  
    var cedula_titular_cta = $("#cedula_titular_cta").val();  
    var nombre_titular_cta = $("#nombre_titular_cta").val();   
    
    if(origen ===''){
       mensajesDelSistema("DEBE INGRESAR VALOR DEL CAMPO ORIGEN", '250', '165');
    }else if (destino === '') {    
        mensajesDelSistema("DEBE INGRESAR VALOR DEL CAMPO DESTINO", '250', '165');
    }else if (vlr_reanticipo === '' || parseFloat(vlr_reanticipo) <= 0) {   
        mensajesDelSistema("EL VALOR DEL REANTICIPO DEBE SER UN ENTERO MAYOR A CERO", '280', '175');
    }else if (banco ===''){
        mensajesDelSistema("DEBE INGRESAR VALOR DEL CAMPO BANCO", '280', '175');
    }else if (sucursal ===''){
        mensajesDelSistema("DEBE INGRESAR VALOR DEL CAMPO SUCURSAL", '280', '175');
    }else if (tipo_cuenta ===''){
        mensajesDelSistema("DEBE INGRESAR VALOR DEL CAMPO TIPO CUENTA", '280', '175');
    }else if (cuenta ===''){
        mensajesDelSistema("DEBE INGRESAR VALOR DEL CAMPO CUENTA", '280', '175');
    }else if (cedula_titular_cta ===''){
        mensajesDelSistema("DEBE INGRESAR VALOR DEL CAMPO CEDULA TITULAR CTA", '280', '175');
    }else if (nombre_titular_cta ===''){
        mensajesDelSistema("DEBE INGRESAR VALOR DEL CAMPO NOMBRE TITULAR CTA", '280', '175');        
    }else{
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Logistica",
            dataType: 'json',
            data: {
                opcion: 29,
                listadoReanticipos: tramaReanticipoToJson()
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    
                    if (json.respuesta === "OK") {
                        $("#dialogReanticipo").dialog('close');
                        mensajesDelSistema("Se genero el reanticipo satisfactoriamente!!", '280', '175', true);
                    } else {
                        mensajesDelSistema(json.respuesta, '250', '150');
                        return;
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci&oacute;n!!", '250', '150');
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
}
   


}

function tramaReanticipoToJson(){
        var sel_id = jQuery('#tabla_anticipos').jqGrid('getGridParam', 'selrow'); 
        var fecha_sistema = $("#tabla_anticipos").getRowData(sel_id).fecha_envio;
        var jsonObj = {"reanticipo": []};
        var item = {};
        item ["codigo_empresa"] =  $('#codigo_empresa').val();
        item ["codigo_agencia"] =  $('#codigo_agencia').val();
        item ["planilla"] =  $('#num_planilla').val();
        item ["fecha_reanticipo"] =  $('#fecha_reanticipo').val();
        item ["fecha_envio_fintra"] = fecha_sistema;
        item ["origen"] =   $('#origen').val();
        item ["destino"] =  $('#destino').val();
        item ["valor_reanticipo"] = parseFloat(numberSinComas($('#vlr_reanticipo').val()));
        item ["porc_comision_intermediario"] =  parseFloat($('#porc_comision').val());
        item ["valor_comision_intermediario"] =  parseFloat($('#vlr_comision').val());
        item ["valor_desembolsar"] = parseFloat($('#vlr_desembolsar').val());
        item ["banco"] = $('#banco').val();
        item ["sucursal"] =  $('#sucursal').val();
        item ["cedula_titular_cuenta"] =  $('#cedula_titular_cta').val();
        item ["nombre_titular_cuenta"] = $('#nombre_titular_cta').val(); 
        item ["tipo_cuenta"] = $('#tipo_cuenta').val();
        item ["no_cuenta"] =   $('#cuenta').val();
         
        jsonObj.reanticipo.push(item);           

        return JSON.stringify(jsonObj); 

}

function listarAnticipoReanticipos(jsonParams,title, root) {     
    $("#grid_anticipos_anul").fadeIn();
    var grid_tbl_anticipos = jQuery("#tabla_anticipos_anul");
     if ($("#gview_tabla_anticipos_anul").length) {
        refrescarGridAnticipoReanticipos(jsonParams);
     }else {
        grid_tbl_anticipos.jqGrid({
            caption: "LISTADO DE "+title,
            url: "./controller?estado=Administracion&accion=Logistica",           	 
            datatype: "json",  
            height: '290',
            width: '1280',           
            colNames: ['Id','C&oacute;d. Agencia','C&oacute;d Transportadora', 'Nombre', 'Id Conductor', 'Conductor', 'Placa', 'Planilla',  'Valor', 'Origen', 'Destino', 'C&oacute;d. Producto',  'Fecha', 'Secuencia', 'Accion'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'codigo_agencia', index: 'codigo_agencia', width: 90, align: 'center'},
                {name: 'codigo_empresa', index: 'codigo_empresa', width: 110, align: 'center'},         
                {name: 'nombre_empresa', index: 'nombre_empresa', width: 180, align: 'left'},               
                {name: 'cedula_conductor', index: 'cedula_conductor', width: 100, align: 'left'}, 
                {name: 'nombre_conductor', index: 'nombre_conductor', width: 180, align: 'left'},  
                {name: 'placa', index: 'placa', width: 90, align: 'center'}, 
                {name: 'planilla_interna', index: 'planilla_interna', width: 90, align: 'center'},
                {name: 'valor_reanticipo', index: 'valor_reanticipo', sortable: true, editable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'origen', index: 'origen', width: 120, align: 'center'},  
                {name: 'destino', index: 'destino', width: 120, align: 'center'},            
                {name: 'codigo_producto', index: 'codigo_producto', width: 120, align: 'center', hidden: true}, 
                {name: 'fecha_envio', index: 'fecha_envio', width: 90, align: 'center', hidden: true},
                {name: 'secuencia', index: 'secuencia', width: 90, align: 'center', hidden: true},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '60px'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_anticipos_anul'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,         
            jsonReader: {
                root: root,
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:jsonParams
            },
            gridComplete: function () {             
                var ids = jQuery("#tabla_anticipos_anul").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];   
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el registro seleccionado?','280','160',anularAnticipoReanticipo,'" + cl + "');\">";
                    jQuery("#tabla_anticipos_anul").jqGrid('setRowData', ids[i], {actions: an});

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_anticipos_anul", {add: false, edit: false, del: false, search: false, refresh: false}, {});
    } 
}

function refrescarGridAnticipoReanticipos(jsonParams){   
    jQuery("#tabla_anticipos_anul").setGridParam({
        url: "./controller?estado=Administracion&accion=Logistica",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data:jsonParams
        }      
    });
    
    jQuery('#tabla_anticipos_anul').trigger("reloadGrid");
}

function anularAnticipoReanticipo(id){
    if ($('#chk_reanticipo').is(":checked")) {
       anularReanticipo(id);
    } else {
        ventanaAgregarComentario(id);       
    }   
}

function ventanaAgregarComentario(rowid){
      $("#comment").val('');
      $("#dialogAddComent").dialog({
        width: 580,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ANULAR ANTICIPO',
        closeOnEscape: false,
        buttons: {  
            "Agregar": function () {
                if ($('#comment').val() === '') {
                    mensajesDelSistema('Por favor, ingrese la anotaci&oacute;n correspondiente', '280', '160');
                } else {
                    anularAnticipo(rowid);
                }               
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function anularAnticipo(rowid){
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        data: {
            opcion: 31,
            listadoNovedades: tramaAgregarNovedadToJson(rowid)
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }

                if (json.respuesta === "OK") {
                    var jsonParams = {
                        opcion: 28,
                        id_transportadora: $('#transportadora').val(),
                        fecha_inicio: $('#fechainicio').val(),
                        fecha_fin: $('#fechafinal').val(),
                        planilla: $('#planilla').val(),
                        placa: $('#placa').val(),
                        producto:''
                    };   
                    refrescarGridAnticipoReanticipos(jsonParams);
                    $("#dialogAddComent").dialog('close');
                    mensajesDelSistema("Se anulo el anticipo satisfactoriamente!!", '280', '175', true);
                } else {
                    mensajesDelSistema(json.respuesta + ', VERIFIQUE EL LOG', '280', '160');
                    return;
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci&oacute;n!!", '280', '160');
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function anularReanticipo(rowid){
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        data: {
            opcion: 32,
            listado: tramaAnularReanticipoToJson(rowid)
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }

                if (json.respuesta === "OK") {      
                    var jsonParams = {
                        opcion: 30,
                        id_transportadora: $('#transportadora').val(),
                        planilla: $('#planilla').val()
                    };
                    refrescarGridAnticipoReanticipos(jsonParams);
                    mensajesDelSistema("Se anulo el reanticipo satisfactoriamente!!", '280', '175', true);
                } else {
                    mensajesDelSistema(json.respuesta, '280', '160');
                    return;
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci&oacute;n!!", '280', '160');
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function tramaAgregarNovedadToJson(rowid){       
        var codigo_empresa = $("#tabla_anticipos_anul").getRowData(rowid).codigo_empresa;
        var codigo_agencia = $("#tabla_anticipos_anul").getRowData(rowid).codigo_agencia;
        var codigo_producto = $("#tabla_anticipos_anul").getRowData(rowid).codigo_producto;
        var planilla = $("#tabla_anticipos_anul").getRowData(rowid).planilla_interna;      
        var placa = $("#tabla_anticipos_anul").getRowData(rowid).placa;           
           
        var jsonObj = {"novedad": []};
        var item = {};
        item ["codigo_empresa"] = codigo_empresa;
        item ["codigo_agencia"] = codigo_agencia;
        item ["codigo_producto"] = codigo_producto;
        item ["tipo_novedad"] = '01';
        item ["codigo_novedad"] = '03';
        item ["planilla"] = planilla;
        item ["placa"] = placa;
        item ["descripcion"] = $('#comment').val();
                
        jsonObj.novedad.push(item);           

        return JSON.stringify(jsonObj); 

}

function tramaAnularReanticipoToJson(rowid){       
        var codigo_empresa = $("#tabla_anticipos_anul").getRowData(rowid).codigo_empresa;
        var codigo_agencia = $("#tabla_anticipos_anul").getRowData(rowid).codigo_agencia;
        var secuencia = $("#tabla_anticipos_anul").getRowData(rowid).secuencia;
        var planilla = $("#planilla").val(); 
        var planilla_interna = $("#tabla_anticipos_anul").getRowData(rowid).planilla_interna;      
        var placa = $("#tabla_anticipos_anul").getRowData(rowid).placa;           
           
        var jsonObj = {"reanticipo": []};
        var item = {};
        item ["codigo_empresa"] = codigo_empresa;
        item ["codigo_agencia"] = codigo_agencia;
        item ["id"] = rowid;
        item ["planilla"] = planilla;
        item ["planilla_interna"] = planilla_interna;
        item ["secuencia"] = secuencia;       
        item ["placa"] = placa;     
                
        jsonObj.reanticipo.push(item);           

        return JSON.stringify(jsonObj); 

}

 function cargarProductosTransp(showAll) {
    $('#productos').html('');
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        async:true,
        data: {
            opcion: 33
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    if(showAll) $('#productos').append("<option value=''>Todos</option>");
               
                    for (var key in json) {              
                       $('#productos').append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#productos').append("<option value=''>Todos</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function resetValues(){
    $('#transportadora').val('');  
    $('#fechainicio').datepicker("setDate", new Date()); 
    $('#fechafinal').datepicker("setDate", new Date());  
    $('#planilla').val('');
    $('#placa').val('');
    $('#productos').val('');  
    $('#chk_reanticipo').attr("checked", false);
}


function DeshabilitarControles(estado){
        $('#transportadora').attr({disabled: estado});       
        (estado) ? $('#fechainicio').attr({readonly: estado}).datepicker("destroy") : $('#fechainicio').attr({readonly: estado}).datepicker({dateFormat: 'yy-mm-dd'});
        (estado) ? $('#fechafinal').attr({readonly: estado}).datepicker("destroy") : $('#fechafinal').attr({readonly: estado}).datepicker({dateFormat: 'yy-mm-dd'});
        $('#planilla').attr({readonly: estado});
        $('#placa').attr({readonly: estado});
        $('#productos').attr({disabled: estado});       
        $('#chk_reanticipo').attr("disabled", estado);
} 

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajeConfirmAction(msj, width, height, okAction, id) { 
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: "Mensaje",
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("close");             
            }
        }
    });
}
