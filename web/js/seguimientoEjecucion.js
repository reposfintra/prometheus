/**
 * funciones para el programa de seguimiento a la ejecucion de los trabajos
 * Autor: darrieta-Geotech
 */

function agregarActividad(i) {
    var max = parseInt($F("maximo")) + 1;

    //Se hace una copia de la fila seleccionada
    var element = $("fila" + i);
    var clone = element.cloneNode(true);
    clone.id = "fila" + max;

    //Cambiar los IDs para que no se repitan
    clone.select("#add" + i)[0].id = "add" + max;
    clone.select("#delete" + i)[0].id = "delete" + max;
    var actividad = clone.select("#cmbActividad" + i)[0];
    actividad.id = "cmbActividad" + max;
    actividad.name = "cmbActividad" + max;
    var responsable = clone.select("#cmbResponsable" + i)[0];
    responsable.id = "cmbResponsable" + max;
    responsable.name = "cmbResponsable" + max;
    var peso = clone.select("#txtPeso" + i)[0];
    peso.id = "txtPeso" + max;
    peso.name = "txtPeso" + max;
    var fechaInicial = clone.select("#txtFechaInicial" + i)[0];
    fechaInicial.id = "txtFechaInicial" + max;
    fechaInicial.name = "txtFechaInicial" + max;
    
    //clone.select("#imgFechaInicial"+i)[0].id = "imgFechaInicial"+max;
    var fechafinal = clone.select("#txtFechaFinal" + i)[0];
    fechafinal.id = "txtFechaFinal" + max;
    fechafinal.name = "txtFechaFinal" + max;
    
    //clone.select("#imgFechaFinal"+i)[0].id = "imgFechaFinal"+max;
    var difFecha = clone.select("#txtDif" + i)[0];
    difFecha.id = "txtDif" + max;
    difFecha.name = "txtDif" + max;
    
    clone.select("script").each(function(item, index) {
        item.remove();
    });

    var html = "<tr id='fila" + max + "' class='fila'>" + clone.innerHTML + "</tr>"
    element.insert({after: html});

    //Agregar evento onclick a los botones
    $("add" + max).onclick = function() {
        agregarActividad(max);
    };
    $("delete" + max).onclick = function() {
        eliminarActividad(max);
    };
    $("cmbActividad" + max).onchange = function() {
        pesoPredeterminado(max);
    };

    $("txtFechaFinal" + max).onchange = function() {
        var ind = this.id.replace("txtFechaFinal","");
        calcDias('txtFechaInicial' + ind, 'txtFechaFinal' + ind, 'txtDif' + ind);
    };
    $("txtFechaInicial" + max).onchange = function() {
        var ind = this.id.replace("txtFechaInicial","");
        calcDias('txtFechaInicial' + ind, 'txtFechaFinal' + ind, 'txtDif' + ind);
    };
    $("txtDif" + max).onchange = function() {
        var ind = this.id.replace("txtDif","");
        calcDias('txtFechaInicial' + ind, 'txtFechaFinal' + ind, 'txtDif' + ind);
    };
    
    $("cont").value = parseInt($F("cont")) + 1;
    $("maximo").value = max;

    $("txtPeso" + max).value = "";
    $("cmbActividad" + max).options[0].selected = true;
    $("cmbResponsable" + max).options[0].selected = true;

    cargarCalendario(max);
    actualizarTotalProgramacion();
}

function eliminarActividad(i) {
    if (parseInt($F("cont")) > 1) {
        $("fila" + i).remove();
        $("cont").value = parseInt($F("cont")) - 1;
        actualizarTotalProgramacion();
    } else {
        alert("No se puede eliminar la ultima actividad");
    }
}

function cargarCalendarioInicial() {
    Calendar.setup({
        inputField: "txtFechaInicial1",
        trigger: "imgFechaInicial1",
        onSelect: function() {
            this.hide()
        }
    });

    Calendar.setup({
        inputField: "txtFechaFinal1",
        trigger: "imgFechaFinal1",
        onSelect: function() {
            this.hide()
        }
    });
}

function cargarCalendario(i) {
    Calendar.setup({
        inputField: "txtFechaInicial" + i,
        trigger: "imgFechaInicial" + i,
        onSelect: function() {
            this.hide()
        }
    });

    Calendar.setup({
        inputField: "txtFechaFinal" + i,
        trigger: "imgFechaFinal" + i,
        onSelect: function() {
            this.hide()
        }
    });
}

Array.prototype.indexAt = function(what) {
    var L = this.length;
    var i = 0;
    while (i < L) {
        if (this[i] === what)
            return i;
        ++i;
    }
    return -1;
}

function confirmarProgramacion() {
    var max = parseInt($F("maximo"));
    var sum = 0;
    var sw = false;
    var actividades = new Array();
    for (i = 1; i <= max; i++) {
        if ($("txtFechaInicial" + i) != null) {

            if ($F("cmbActividad" + i) != null && $F("cmbActividad" + i) != "-1" && $("cmbResponsable" + i) != "-1" && $F("txtFechaInicial" + i) != "" && $F("txtFechaFinal" + i) != "" && $F("txtPeso" + i) != "") {
                //var finicial = $F("txtFechaInicial"+i).replace(/-/gi, "/");
                //var ffinal = $F("txtFechaFinal"+i).replace(/-/gi, "/");
                try {
                    var dinicial = parseInt($F("txtFechaInicial" + i));//new Date(finicial);
                    var dfinal = parseInt($F("txtFechaFinal" + i));//new Date(ffinal);
                } catch (exception) {
                    alert('error valor dia en el item ' + i);
                    return;
                }
                if (dinicial <= dfinal) {
                    if (actividades.indexAt($F("cmbActividad" + i)) == -1) {
                        actividades.push($F("cmbActividad" + i));
                    } else {
                        sw = true;
                        alert("No se puede ingresar las actividades mas de una vez");
                        break;
                    }
                } else {
                    sw = true;
                    alert("La fecha final debe ser mayor que la de inicio");
                    break;
                }
                sum += parseInt($F("txtPeso" + i));
            } else {
                sw = true;
                alert("Debe seleccionar la actividad, responsable, peso, fecha de inicio y fin para todas las actividades");
                break;
            }
        }
    }
    if (!sw) {
        if (sum == 100) {
            if (confirm("Despues de confirmar no podr\xe1 modificar la programaci\xf3n. Esta seguro de que desea continuar?")) {
                document.forms[0].action += "&opcion=confirmar";
                document.forms[0].submit();
            }
        } else {
            alert("La sumatoria de los pesos de las actividades debe ser 100");
        }
    }
}

function actualizarTotalProgramacion() {
    var max = parseInt($F("maximo"));
    var sum = 0;
    for (i = 1; i <= max; i++) {
        if ($("txtFechaInicial" + i) != null) {
            if (!isNaN(parseInt($F("txtPeso" + i)))) {
                sum += parseInt($F("txtPeso" + i));
            }
        }
    }
    $("pTotal").innerHTML = sum + "%";
}

function guardarProgramacion() {
    var max = parseInt($F("maximo"));
    var sw = false;
    var actividades = new Array();
    for (i = 1; i <= max; i++) {
        if ($("cmbResponsable" + i) != null && $F("cmbActividad" + i) != null) {
            if ($F("cmbResponsable" + i) != "-1" && $F("cmbActividad" + i) != "-1") {
                if (actividades.indexAt($F("cmbActividad" + i)) == -1) {
                    actividades.push($F("cmbActividad" + i));
                } else {
                    sw = true;
                    alert("No se puede ingresar las actividades mas de una vez");
                    break;
                }
            } else {
                sw = true;
                alert("Debe seleccionar la actividad y un responsable");
                break;
            }
        }
    }
    if (!sw) {
        document.forms[0].action += "&opcion=guardar";
        document.forms[0].submit();
    }
}

function restablecerProgramacion(baseurl, idSolicitud, idAccion) {
    document.forms[0].action = baseurl + "/jsp/opav/programacionActividades.jsp?id_solicitud=" + idSolicitud + "&id_accion=" + idAccion;
    document.forms[0].submit();
}

function pesoPredeterminado(i) {
    if ($F("cmbActividad" + i) != "-1") {
        var peso = $F("cmbActividad" + i).split(";")[1];
        var responsable = $F("cmbActividad" + i).split(";")[2];
        $("txtPeso" + i).value = peso;
        $("cmbResponsable" + i).value = responsable;
    }
}

function confirmarAjustes() {
    var tam = parseInt($F("numActividades"));
    var sw = false;
    for (i = 0; i < tam; i++) {
        //var finicial = $F("txtFechaInicial"+i).replace(/-/gi, "/");
        //var ffinal = $F("txtFechaFinal"+i).replace(/-/gi, "/");
        try {
            var dinicial = parseInt($F("txtFechaInicial" + i));//new Date(finicial);
            var dfinal = parseInt($F("txtFechaFinal" + i));//new Date(ffinal);
        } catch (exception) {
            alert('error valor dia en el item ' + i);
            return;
        }
        if (dinicial > dfinal) {
            sw = true;
            alert("La fecha final debe ser mayor que la de inicio");
            break;
        }
    }
    if (!sw) {
        document.forms[0].submit();
    }

}

function validarSeguimiento() {
    var sw = false;
    var tam = parseInt($F("numActividades"));
    var guardado = $F("guardado");
    if (guardado == "false") {
        var fAnterior = $F("fechaConfirmado").replace(/-/gi, "/");
        var fecha = $F("txtFecha").replace(/-/gi, "/");
        var dfecha = new Date(fecha);
        var danterior = new Date(fAnterior);
        if (dfecha <= danterior) {
            sw = true;
            alert("La fecha seleccionada debe ser mayor que la fecha del anterior seguimiento");
        }
    }
    for (i = 0; i < tam; i++) {
        var avance = trim($F("txtAvance" + i));
        var avanceAnt = parseFloat($F("hidAvanceAnterior" + i));
        if (avance == "") {
            sw = true;
            alert("Debe ingresar un avance para cada actividad");
            break;
        } else if (isNaN(avance)) {
            sw = true;
            alert("Solo se permite ingresar caracteres numericos");
            break;
        } else if (avance > 100) {
            sw = true;
            alert("No puede ingresar un avance mayor a 100%");
            break;
        } else if (avanceAnt > avance) {
            if ($F("comentario" + i) == "") {
                sw = true;
                alert("Al ingresar un avance menor que el anterior debe colocar una observacion");
                break;
            }
        }
    }
    return sw;
}

function guardarSeguimiento() {
    if (!validarSeguimiento()) {
        document.forms[0].action += "&opcion=guardarSeguimiento";
        document.forms[0].submit();
    }
}

function confirmarSeguimiento() {
    if (!validarSeguimiento()) {
        if (confirm("Despues de confirmar no podr\xe1 modificar este seguimiento. Esta seguro de que desea continuar?")) {
            document.forms[0].action += "&opcion=confirmarSeguimiento";
            document.forms[0].submit();
        }
    }
}

function segEjecucionLoad() {
    $("totalAnt").innerHTML = $F("hidTotalAvanceAnt") + "%";
    $("totalEsp").innerHTML = $F("hidTotalAvanceEsp") + "%";
    $("total").innerHTML = $F("hidTotalAvance") + "%";
}

function roundNumber(num, dec) {
    var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    return result;
}

function calcularTotal() {
    var tam = parseInt($F("numActividades"));
    var total = 0;
    for (i = 0; i < tam; i++) {
        var avance = trim($F("txtAvance" + i));
        var pesoObra = parseFloat($F("hidPesoObra" + i));
        if (avance != "" && !isNaN(avance)) {
            total += (pesoObra * parseInt(avance)) / 100;
        }
    }
    $("total").innerHTML = roundNumber(total, 2);
    $("hidTotalAvance").value = roundNumber(total, 2);
}

function restablecerSeguimiento(baseurl, idSolicitud) {
    document.forms[0].action = baseurl + "/jsp/opav/seguimientoEjecucion.jsp?id_solicitud=" + idSolicitud + "&fecha=" + $F("txtFecha");
    document.forms[0].submit();
}

function agregarObservacion(i) {
    $("areaObservacion").readOnly = false;
    if (i != null) {
        var tam = $F("hidDescActividad" + i).length;
        $("areaObservacion").value += "\n" + $F("hidDescActividad" + i) + ": ";
    } else {
        $("areaObservacion").value += "\nGeneral: ";
    }
    $("areaObservacion").focus();
}

/**
 ** @param fila    '#' � '#:#'. 
 ** @param columna '#' � '#:#'.
 **/
function mostrarObservacion(fila, columna) {
    var iteraF = 0;
    var iteraC = 0;
    var colOri = 0;
    var comentario = '';
    var cmt = '';
    try {
        iteraF = (!fila.split(':')[1]) ? fila : fila.split(':')[1];
        fila = fila.split(':')[0];
    } catch (exception) {
        fila = fila;
        iteraF = fila;
    }
    try {
        iteraC = (!columna.split(':')[1]) ? columna : columna.split(':')[1];
        columna = columna.split(':')[0];
        colOri = columna;
    } catch (exception) {
        columna = columna;
        iteraC = columna;
        colOri = columna;
    } //alert(fila+":"+iteraF+" "+columna+":"+iteraC);
    while (fila <= iteraF) {
        while (columna <= iteraC) {
            try {
                cmt = $("P_" + fila + "_" + columna).title.replace(/^\s+|\s+$/g, '');
                if (cmt != '') {
                    comentario += "\b" + $("seg" + columna).innerHTML.replace(/^\s+|\s+$/g, '') + " "
                            + $("I_" + fila + "_1").innerHTML.replace(/^\s+|\s+$/g, '') + ":\b\n"
                            + cmt + "\n";
                }
            } catch (exception) {
                alert('exception');
                break;
            }
            columna++;
        }
        fila++;
        columna = colOri; //comentario += '\n'; 
    }
    if (comentario != '') {
        alert(comentario);
    }
}

function actualizarConsultaEjecucion(baseurl, idSolicitud) {
    location.href = baseurl + "/jsp/opav/consultaEjecucion.jsp?id_solicitud=" + idSolicitud + "&id_accion=" + $F("cmbAcciones");
}

function generarPDF() {
    document.forms[0].submit();
}

function calcDias(tfi, tff, tdd) {
    try {
        var di = document.getElementById(tfi);//'txtFechaInicial'
        var df = document.getElementById(tff);//'txtFechaFinal'
        var dd = document.getElementById(tdd);//'txtDif'

        var idi = parseInt((di.value === '') ? 0 : di.value);
        var idf = parseInt((df.value === '') ? 0 : df.value);
        var idd = parseInt((dd.value === '') ? 0 : dd.value);
        if (idi > idf && idf > 0) {
            //di.value = idf;
            //df.value = idi;
            //dd.value = idi - idf + 1;
            alert('La diferencia de dias no puede ser negativa');
        } else if (idi > 0 && idf > 0) {
            dd.value = idf - idi + 1;
        } else {
            df.value = idf == 0 || isNaN(idf) ? '' : idf;
            di.value = idi == 0 || isNaN(idi) ? '' : idi;
            dd.value = idd == 0 || isNaN(idd) ? '' : idd;
        }
    } catch (exception) {
        return;
    }

}

function AbrirVentana(ruta) {
    cronos = window.open(ruta + "/js/jquery/jQuery.Gantt/Gantt.html");
}
