var fechainical;
var fechafincal;

function today()
{
  var dt = new Date();
  var year  = dt.getFullYear();
  var month = dt.getMonth() + 1;
  var day   = dt.getDate();
  return (year + "-" + month + "-" + day);
}

function dateToMillisecs(tStamp)
{
  var dtFlds = tStamp.split("-");
  var Year = dtFlds[0];
  var Month = ((dtFlds[1].length == 1)? "0" + dtFlds[1] : dtFlds[1] );
  var Day = ((dtFlds[2].length == 1)? "0" + dtFlds[2] : dtFlds[2] );
  return Date.parse(Month + "/" + Day + "/" + Year);
}

function isFechaIniLeThanFechaFin()
{
  var fecini = dateToMillisecs(document.form1.fechaini.value);
  var fecfin = dateToMillisecs(document.form1.fechafin.value);
  return (fecini <= fecfin);
}

function clienteChange()
{
  var clSelect = document.form1.cliente;
  document.form1.nombreCliente.value = clSelect.options[clSelect.selectedIndex].label;
}

function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPaginaUbicacion(url, nombrePagina)
{
  var wdth = screen.width - screen.width * 0.15;
  var hght = screen.height - screen.height * 0.15;
  var lf = screen.height * 0.075;
  var tp = screen.height * 0.075;
  var options = "menubar=yes,scrollbars=no,resizable=yes,status=yes," +
                setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) ){
    hWnd.opener = document.window;
  }
}

function ConsultarBtnClickUbicacion(rootUrl)
{
 
  var frm = document.form1;
  var isDataRight = false;

  // Validar rango de fechas de remesas.
 
  if(frm.cliente.value ==""){
    alert("Debe seleccionar un cliente...");  
  }else{
      if ( frm.fechaini.value != "" || frm.fechafin.value != "" )
      {
        if( (frm.fechaini.value != "" && frm.fechafin.value == "") ||
            (frm.fechaini.value == "" && frm.fechafin.value != "") )
        {
          alert("El rango de fechas est� incompleto.");
        }else if( !isFechaIniLeThanFechaFin() ){
          alert("La fecha inicial debe ser MENOR O IGUAL que la final.");
        }else{
             isDataRight = true; 
        }
      }else{

        isDataRight = true;
      }
 }
  // Ver cuales de los valores del campo "Tipo de viaje" fueron
  // seleccionados, para armar con ellos una lista separada por comas.
  if( isDataRight )
  {
    var lst = "";
    if( frm.tipoViaje.options[0].selected )
    {
      // Escogerlas todas si fue seleccionada la opci�n que as� lo determina.
      lst = "NA,RM,RC,RE,DM,DC";
    }else{
      for( var idx = 1; idx < 5; idx++ )
      {
        if( frm.tipoViaje.options[idx].selected )
        {
          lst += frm.tipoViaje.options[idx].value + ",";
        }
      }
      lst = ((lst == "")? "" : lst.substring(0, lst.length - 1));
    }
    frm.listaTipoViaje.value = lst;
  }

  // Si los datos est�n correctos, env�elos para mostrar el reporte.
  if( isDataRight )
  {
    var toExcel = frm.displayInExcel.value;
    if( toExcel == "true" )
      window.status = "Generando archivo de reporte en MS-Excel. Por favor espere...";
    else
      window.status = "Generando reporte. Por favor espere...";
    var params = "&nombreCliente=" + frm.nombreCliente.value +
                 "&cliente=" + frm.cliente.value +
                 "&fechaini=" + frm.fechaini.value +
                 "&fechafin=" + frm.fechafin.value +
                 "&listaTipoViaje=" + frm.listaTipoViaje.value +
                 "&estadoViajes=" + frm.viajes.value +
                 "&displayInExcel=" + frm.displayInExcel.value;
    abrirPaginaUbicacion(rootUrl + params, "wubicveh");
  }
}
