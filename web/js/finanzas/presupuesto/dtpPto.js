    //////////////////////////////////////////////////////////////////////////////
    // CALENDARIO V 1.2 ADAPTACION TSP - PRESUPUESTO DE VENTAS (DIARIO) - HISTORIAL
    // MARIO FONTALVO SOLANO
    ///////////////////////////////////////////////////////////////////////////////

    Calendar.DaysOfWeek= ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'];
    Calendar.Months    = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    Calendar.DOMonths  = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    Calendar.bDOMonths = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    Calendar.styleH1   = "background-color:#ffa928; color:white; font-weight:bold; font-family:Verdana; font-size:14px;";
    Calendar.styleH2   = "background-color:#99ccff; color:black; font-weight:bold; font-family:Verdana; font-size:12px; width:40px;";
    Calendar.styleH3   = "background-color:#123456; color:white; font-weight:bold; font-family:Verdana; font-size:14px;";	
    Calendar.styleBd1  = "background-color:white;   color:gray;  font-family:Verdana; font-size:10px; text-align:center";
    Calendar.styleBd2  = "background-color:#D1DCEB; color:gray;  font-family:Verdana; font-size:10px; text-align:center";
    Calendar.styleTb   = "background-color:#D1DCEB; border:2 outset gray; padding:4";
    Calendar.styleTb1  = "background-color:#D1DCEB; padding:4";	
    Calendar.styleTxt  = "color:black;font-family:Arial; font-size:11px; width:100%; text-align:center;";
    Calendar.styleTxt2 = "color:red; font-family:Arial;   font-size:11px; width:100%; text-align:center; font-weight:bold;";
    Calendar.styleTxt3 = "color:navy;font-family:Arial;  font-size:11px; width:100%; text-align:center; font-weight:bold;";
    Calendar.styleBtn  = "background-color: #99ccff; font-size: 11px; font-weight: bold; color: #000000; border-color: #69F; width:110";
    Calendar.stylelink = "color:#003399; font-family:verdana; font-size:10px;";


    function Calendar(year, month){
      this.Year        = year;
      this.Month       = month;
      this.MonthMM     = (month<10?"0"+month:month);
      this.nameMonth   = this.nameMonth    (month);
      this.daysOfMonth = this.DOMonth      (month);
      this.initOfWeek  = this.getInitMonth (month-1);
      return this;
    }
    Calendar.prototype.nameMonth = function (month){
      return Calendar.Months [month-1];
    }
    Calendar.prototype.DOMonth = function (month){
      return (this.Year%4!=0)?Calendar.DOMonths [month-1]:Calendar.bDOMonths [month-1];
    }
    Calendar.prototype.getInitMonth = function (month){
      return (new Date(this.Year,month,1)).getDay();
    }
    Calendar.prototype.getHeader = function (){
      
      info= "<table align='center' width='100%'>" +
                "<tr class='fila'><td colspan='2'>"+
                "<table cellpadding='0' cellspacing='0' width='100%'>"+
                "  <tr class='fila'>"+
                "   <td width='65%' class='subtitulo1' >&nbsp;PRESUPUESTO DE VENTA DIARIO - ESTANDAR No ["+ StdJobNo +"]</td>"+
                "   <td width='35'  class='barratitulo'><img src='" + BASEURL + "/images/titulo.gif' width='32' height='20'></td>"+
                "  </tr>    "+
                "</table>   "+
                "</th></tr> "+ 
                "<tr class='fila'><td width='80'>&nbsp;Agencia    </td><td ><input type='text' value='"+ NombreAgencia  +"' name='NombreAgencia'  readonly style='width:90%'></td></tr>"+
                "<tr class='fila'><td width='80'>&nbsp;Cliente    </td><td ><input type='text' value='"+ NombreCliente  +"' name='NombreCliente'  readonly style='width:90%'></td></tr>"+
                "<tr class='fila'><td width='80'>&nbsp;Descripcion</td><td ><input type='text' value='"+ StdJobDesc     +"' name='StdJobDesc'     readonly style='width:90%'></td></tr>"+
                "<tr class='fila'><td width='80'>&nbsp;Ruta       </td><td ><input type='text' value='"+ NombreOrigen +" - "+ NombreDestino    +"' name='Ruta'   readonly style='width:90%'></td></tr>"+
                "</table>";

      header  = "<form action='"+ Action +"' method='post' name='formulario' onsubmit='jscript: return wsubmit(this);'>";
      header += "<br><table border='2'  width='70%' align='center'>"+
                "<tr ><td align='left' >"+ info +"</td></tr>"+
                "</table>";


      header += "<input type='hidden' value='"+ Distrito        +"' name='Distrito' >"+
                "<input type='hidden' value='"+ StdJobNo        +"' name='Stdjob'   >"+
                "<input type='hidden' value='"+ CodigoAgencia   +"' name='Agencia'  >"+
                "<input type='hidden' value='"+ CodigoCliente   +"' name='Cliente'  >"+
                "<input type='hidden' value='"+ this.Year       +"' name='Ano'      >"+
                "<input type='hidden' value='"+ this.MonthMM    +"' name='Mes'      >"+
                "<input type='hidden' value='"+ this.initOfWeek +"' name='Inicio'   >"+
	        "<input type='hidden' value='Guardar'               name='Opcion'>";
      header += "<br>";          
      header += "<table width='100%'  align='center' border = '0'><tr>";
      return header;
    }
    Calendar.prototype.getPresupuesto = function (){
      body  = "<td align='center' valign='top'>"; 
      body += "<table align='center' style='"+ Calendar.styleTb +"' width='350'>";
      body += "<tr>  <th colspan='7' >"+ generarCabecera(this.nameMonth + " del "+ this.Year) +"</th></tr><tr class='tblTitulo'>";
      for (i=0;i<7;i++) body += "<th class='bordereporte'>"+ Calendar.DaysOfWeek[i] +"</th>";  header += "</tr>";
      body += "<tr>";
      j = 0;
      for (i=0;i< this.initOfWeek ;i++, j++) body += "<td style='"+ (i%7==0?Calendar.styleBd2:Calendar.styleBd1) +"'>&nbsp; </td>";
      for (i=1;i<=this.daysOfMonth;i++, j++) body += "<td style='"+ (j%7==0?Calendar.styleBd2:Calendar.styleBd1) +"'>"+ i +"<br>"+
                                                     "    <input type='text'  day='"+ i +"' onfocus='this.select()' value='"+ (PtoViajes[i-1]!=0?PtoViajes[i-1]:"")   +"' style='"+ Calendar.styleTxt2 +"' name='Viajes' title='Doble clic para referencia el viaje' onDblClick=\"jscript:Ref_viaje(this);\" onkeyup=\"jscript: validar(this);\">"+
                                                     "    <input type='hidden' value='"+ PtoViajes[i-1] +"'  name='CViajes' > "+
                                                     "</td>" + ((j+1)%7==0?"</tr><tr>":"");
      for (i=j;(i%7!=0);i++)                 body += "<td style='"+ (i%7==0?Calendar.styleBd2:Calendar.styleBd1) +"'>&nbsp; </td>"  + ((i+1)%7==0?"</tr>":"");
      for (i=this.daysOfMonth ; i<(31+1);i++) body += "<input type='hidden' value='0' name='Viajes' ><input type='hidden' value='0' name='CViajes' >";
      body += "</table></td>";

      return body;
    }

    Calendar.prototype.getHistorial = function (){
      body  = "<td align='center' valign='top' class='tablaInferior'>"; 
      body += "<table align='center' style='"+ Calendar.styleTb +"' width='350'>";
      body += "<tr>  <th colspan='7' >"+ generarCabecera('Version Previa ' + this.nameMonth +' - '+ this.Year) +"</th></tr><tr class='tblTitulo'>";
      for (i=0;i<7;i++) body += "<th class='bordereporte'>"+ Calendar.DaysOfWeek[i] +"</th>";  header += "</tr>";
      body += "<tr>";
      j = 0;
      for (i=0;i< this.initOfWeek ;i++, j++) body += "<td style='"+ (i%7==0?Calendar.styleBd2:Calendar.styleBd1) +"'>&nbsp; </td>";
      for (i=1;i<=this.daysOfMonth;i++, j++) body += "<td style='"+ (j%7==0?Calendar.styleBd2:Calendar.styleBd1) +"'>"+ i +"<br>"+
                                                     "    <input type='text' onfocus='this.select()' value='"+ (HPtoViajes[i-1]!=0?HPtoViajes[i-1]:"") +"' style='"+ (HPtoViajes[i-1]!=PtoViajes[i-1]?Calendar.styleTxt3:Calendar.styleTxt2) +"' name='HViajes' readonly>"+
                                                     "</td>" + ((j+1)%7==0?"</tr><tr>":"");
      for (i=j;(i%7!=0);i++)                  body += "<td style='"+ (i%7==0?Calendar.styleBd2:Calendar.styleBd1) +"'>&nbsp; </td>"  + ((i+1)%7==0?"</tr>":"");
      body += "</table></td>";
      return body;
    }

    Calendar.prototype.getFooter = function (){
      footer = "</td><td align='center' valign='top'> " +
               "<table align='center' style='"+ Calendar.styleTb +"'>" +
               "<tr ><th colspan='2'>" + generarCabecera('RESUMEN') + "</th></tr>"+
               "<tr style='"+ Calendar.styleBd1 +"'><th width='100'>UNIDAD    </th><th width='100'><input type='text' value='"+ Unidad            +"' name='Unidad'    style='"+ Calendar.styleTxt  +"' readonly></th></tr>"+
               "<tr style='"+ Calendar.styleBd1 +"'><th width='100'>CANT. REQ </th><th width='100'><input type='text' value='"+ Cantidad          +"' name='Cantidad'  style='"+ Calendar.styleTxt  +"' readonly></th></tr>"+  
               "<tr style='"+ Calendar.styleBd1 +"'><th width='100'>TARIFA    </th><th width='100'><input type='text' value='"+ formato(Tarifa,2) +"' name='VTarifa'   style='"+ Calendar.styleTxt  +"' readonly><input type='hidden' value='"+ Tarifa +"' ></th></tr>"+
               "<tr style='"+ Calendar.styleBd1 +"'><th width='100'>MONEDA    </th><th width='100'><input type='text' value='"+ Moneda +"' name='Moneda' style='"+ Calendar.styleTxt + "' readonly></th></tr>"+
               "<tr style='"+ Calendar.styleBd1 +"'><th width='100'>TASA      </th><th width='100'><input type='text' value='"+ formato(Cambio,3) +"' name='VCambio'   style='"+ Calendar.styleTxt + "' readonly><input type='hidden' value='"+ Cambio +"' name='Cambio'></th></tr>"+
               "<tr style='"+ Calendar.styleBd1 +"'><th width='100'>NRO VIAJES</th><th width='100'><input type='text' value='"+ Total  +"' name='Total'  style='"+ Calendar.styleTxt  +"' readonly></th></tr> "+
               "<tr style='"+ Calendar.styleBd1 +"'><th width='100'>TOTAL PTO </th><th width='100'><input type='text' value='"+ (Tarifa*Total) +"' name='TotalPto' style='"+ Calendar.styleTxt +"' readonly></th> </tr> "+
               "</table><br>"+
               "<image src='"+ BASEURL +"/images/botones/aceptar.gif' style='cursor:hand;' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);' onclick='if (qsubmit(formulario)) { formulario.submit(); } '>&nbsp;"+
               "<image src='"+ BASEURL +"/images/botones/anular"+ (Total==0?"Disable":"")  +".gif'  style='cursor:hand;'  " + (Total!=0? "onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='window.location.href = ActionAnular;'":"") + "><br>"+
               "<image src='"+ BASEURL +"/images/botones/salir.gif'   style='cursor:hand;' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);' onclick=\"jscript: cerrar();\">"+
               "</td></tr></table>"+
               "<center>"+
               "  <br><a style='"+ Calendar.stylelink +"' href='javascript: void(0);' onclick='historial();'>Historial modo Normal</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a style='"+ Calendar.stylelink +"'  href='javascript: void(0);' onclick='historialedicion();'>Historial modo Edicion</a>"+
               "  <br><br><font class='informacion'>Indique en cada casilla los viajes presupuestado para ese dia.</font>"+
               "  <br><font class='informacion'><input type='checkbox' checked name='ActualizarListado'> Cuando cierre esta ventana, usted desea refrescar el listado principal?</font>";

       if (Mensaje!=''){
          footer +="<br><br><font class='informacion' style='color:red;'>"+ Mensaje +"</font>";    
       }
               
       footer+="</center>" +
  	       "<div id='capaRP' style='visibility:hidden; background-color:white; border:4 outset gray; position:absolute; top:80; left:280; height:440; width:550;'></div>" +			   
               "</form>"+
               "<script>calcular(formulario);</script>";
      return footer;
    }

    Calendar.prototype.ShowCalendar = function (){
      document.open();
      var header    = this.getHeader();
      var pto       = this.getPresupuesto();
      var historial = this.getHistorial();
      var footer    = this.getFooter();
      document.write( header + historial + pto +  footer );
      window.focus();
    }
    
    function generarCabecera(title){
         var cab =
          "<table border='0' width='100%' cellspacing='0'>"+
          "   <tr>"+
          "     <td class='subtitulo1'  width='80%'>&nbsp;"+ title +"</td>"+
          "     <td class='barratitulo' width='20'><img src='"+ BASEURL +"/images/titulo.gif' width='32' height='20'></td>"+
          "   </tr>"+
          "</table>";
         return cab;
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // FUNCIONES DE VALIDACION DE DATOS
        function formatoDia (dia){ 
           return (dia<10? '0' + dia: dia) ;
        }
        function validar(texto){
            if( texto.value=='' || /^\d+$/.test(texto.value) )
                calcular(texto.form)
            else {
                alert("Formato numerico no valido, se colocara en 0 para que vuelva intentarlo");
                // texto.value='0';
                 texto.value='';
            }
        }
        function calcular(tform){
            with(tform){
                var total    = 0;
                var totalpto = 0;
                var cambio   = parseFloat(Cambio.value);
                for (i=0;i<Viajes.length;i++)
                  if (Viajes[i].type=='text'){
                    if  (Viajes[i].value!='') {
                          total    += parseFloat(Viajes[i].value);
                          totalpto += parseFloat(Viajes[i].value) * ( cambio *  Tarifa ) ;
                    }
                  }
                Total.value    = total;
                TotalPto.value = formato(totalpto, 0);
            }
        }
        function psubmit(tform) {
            with(tform){
                var total    = 0;
                var totalpto = 0;
                var cambio   = Cambio.value;
                for (i=0;i<Viajes.length;i++) {
                        if( /^\d+$/.test( Viajes[i].value) ){
                           total    += parseFloat(Viajes[i].value);
                           totalpto += parseFloat(Viajes[i].value) * ( cambio *  parseFloat(Tarifa.value) );
                        }
                        else Viajes[i].value='';
                }
                Total.value    = total; 
                TotalPto.value = totalpto;
                if (total==0){
                   alert('Debe ingresar un valor por lo menos para poder grabarlo.');
                   return false;
                }
            }
            return true;
        }
        function qsubmit (tform){
            var cambios = detectarCambios(tform);
            if (cambios.length>0){
                generarCapaRP(cambios, tform);
		return false;
		// return wsubmit(tform);
            }
            else{
		alert ('No hay cambios para guardar.');
           	return false;
	    } 
        }
		
        function wsubmit (tform){
            for(i=0;i<tform.elements.length;i++){			  
                if (tform.elements[i].name=='RPLOV' && tform.elements[i].value==''){
                    alert ('Primero indique reprogramaciones a estas modificciones para poder continuar.');
                    return false;
                }
            }

            if (psubmit(tform))	{		
                with(tform){
                    for (i=0;i<Viajes.length;i++)
                        if(  Viajes[i].value=='' ) Viajes[i].value='0';                
                }
                return true;
            }
            return false;
        }
		
        function formato(numero, decimales){
           var tmp = parseInt(numero) ;
           var num = '';
           var pos = 0;
           while(tmp>0){
                  if (pos%3==0 && pos!=0) num = ',' + num;
                  res  = tmp % 10;
                  tmp  = parseInt(tmp / 10);
                  num  = res + num  ;
                  pos++;
           }
           var dec = 1;
           for (i=0;i<decimales;i++, dec*=10);
           dnum = parseInt ((parseFloat(numero) - parseInt(numero)) * dec);
                   tmpdec = '';
           for (i=0;i<decimales - (dnum==0?0:dnum.toString().length ) ;i++, tmpdec +='0');
           num += (decimales>0? '.' +  tmpdec + (dnum!=0?dnum:''): '');
           return num;
        }

        function cerrar(){
           if (formulario.ActualizarListado) {
                if (formulario.ActualizarListado.checked == true ) window.opener.location.href = ActionRefrescar ;
           }
           window.close();
        }
        function historial(){
              newWindow(ActionHistorial,'Historial');
        }
        function historialedicion(){
            newWindow(ActionHistorialE,'HistorialEdicion');
        }
        function newWindow(url, nombre){
              option="  width="+ (screen.width-30) +", height="+ (screen.height-80)  +",  scrollbars=no, statusbars=yes, resizable=yes, menubar=no ,top=10 , left=10 ";
              ventana=window.open('',nombre,option);
              ventana.location.href=url;
              ventana.focus();
        }





        /**
         * funcion para detectar si se hicieron cambios sobre el presupuesto real
         * en caso de ser positivo este procesará una ventana para calcular los
         * codigos de reprogramacion 
         * params tform ... formulario principal
         * return vd ...... array que contiene las posiciones que fueron modificadas
         **/
        function detectarCambios (tform){
            // variable para registrar
            // los viajes que han sido cambiados

            var vd = new Array();
            var cont = 0;

            with(tform){
              for (i=0;i<Viajes.length-1;i++){
				var aux = (Viajes[i].value==''?'0': Viajes[i].value) ;
                if( aux != CViajes[i].value )
                   vd[cont++] = i;
			  }
            }
            return vd;
        }  


        /**
         * procedimiento para generar la capa de reprogramaion
         */
         function generarCapaRP (cambios, formulario){
            var capa = document.getElementById('capaRP');            
            var cuerpo = "<table width='100%' align='center' style='"+ Calendar.styleTb1 +"'>" +
 						 "<tr style='cursor: move;' ondragstart='' onMouseDown=\"_initMove('capaRP');\"><th colspan='4'>"+ generarCabecera('Cambios Detectados') +"</th></tr>"+
                         "<tr class='tblTitulo'>"+
                         "<th class='bordereporte' width='98'>Fec Viaje</th>"+
                         "<th class='bordereporte' width='48'>Nue.</th>"+
                         "<th class='bordereporte' width='48'>Ant.</th>"+
                         "<th class='bordereporte' >Causa Reprogramacion</th>"+
                         "</tr></table>";

						 
	        cuerpo += "<div style='overflow:auto; height:315;'>" +
			          "<table width='100%' align='center' style='"+ Calendar.styleTb1 +"' >";
            for (i=0; i<cambios.length; i++){
              with (formulario) {
                cuerpo+= "<tr style='"+ Calendar.styleBd1 +"'>" + 
                         "<td width='100'>" + Ano.value + "-"  + Mes.value + "-" + formatoDia(parseFloat(cambios[i]+1)) + "</td>"+
                         "<td width='50' >" + Viajes [cambios[i]].value  + "</td>"+
                         "<td width='50' >" + CViajes[cambios[i]].value  + "</td>"+
                         "<td>"+
						 "  <input type='hidden' name='RPFEC' value='"+ Ano.value + "-"  + Mes.value + "-" + formatoDia(parseFloat(cambios[i]+1)) +"'>"+
						 "  <input type='hidden' name='RPVAN' value='"+ CViajes [cambios[i]].value +"'>"+
						 "  <input type='hidden' name='RPVNU' value='"+ (Viajes [cambios[i]].value==''?'0':Viajes [cambios[i]].value) +"'>"+
						 "  <select name='RPLOV' style='"+ Calendar.styleTxt +"'>"+ comboRP +"</select>"+
						 "</td>"+
                         "</tr>";
              }
			}
            cuerpo += "</table></div>"+
			          "<center><br>"+
			          "<image src='"+ BASEURL +"/images/botones/aceptar.gif'   style='cursor:hand;' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);' onclick=\" if (wsubmit(formulario)) { formulario.submit(); }  \">&nbsp;"+
  			          "<image src='"+ BASEURL +"/images/botones/cancelar.gif'   style='cursor:hand;' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick=\"capaRP.style.visibility='hidden';\">"+
			          "</center>";
			
			
            capa.innerHTML = cuerpo;
			capa.style.visibility = 'visible';
			capa.style.top = 40;
         }
    ////////////////////////////////////////////////////////////////////////////////////
    function Ref_viaje( viaje ) {  
   var viajes = parseFloat (viaje.value);
	if(viajes>0){
	 	window.open(ActionRef_viaje+"&Dia="+viaje.day+"&Viajes="+viaje.value,'R','status=yes,width=700,height=600,resizable=yes');
	}
	else {
 		alert("Debe digitar minimo 1 viaje");
	}
}

function validarRef() {
	var valor='';
	var con = 0;
	for(i=0;i< forma.Viajes.value;i++) {
		valor = document.getElementById("doc"+i).value;
		if( valor != ''){
			con++;
		}
    }
	if(con > forma.nrovj.value){
		alert("Los documentos referenciados deben ser menor o igual los viajes");
		return false;
	}
	else if( con == 0 ){
		alert("Referencie por lo menos un viaje");
		return false;
    }
	forma.submit();
}



