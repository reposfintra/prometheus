            function buscarValor(Ano, Mes, Moneda){
               var cambio = 0;
               for (m=0;m<12; m++){
                 var valor = tasa[m][0].split(separador);;
                 if (valor[0] == Ano && valor[1] == Mes){
                    if      (Moneda == 'PES') cambio = 1;
                    else if (Moneda == 'DOL') cambio = parseFloat(valor[2]);
                    else if (Moneda == 'BOL') cambio = parseFloat(valor[3]); 

                 }
               }
               return cambio;
            }
            function validar(texto){
                if( texto.value=='' || /^\d+$/.test(texto.value) )
                    calcular(texto.form);
                else {
                    alert("Formato numerico no valido, se colocara en vacio para que vuelva intentarlo");
                    texto.value='';
                }
            }
            function calcular(tform){
                with(tform){
                    var total    = 0;
                    var totalpto = 0;
                    var cambio   = 0; //buscarValor(Ano.value, Mes.value, Moneda.value);
                    var unidades = parseFloat(CantidadR.value);
                    for (i=0;i<Viajes.length;i++)
                      if (Viajes[i].type=='text'){
                        if  (Viajes[i].value!='') {
                              cambio   =  buscarValor(Ano.value, (Mes.value!='TD'? Mes.value:  ((i+1<10?'0':'')+(i+1))  ) , Moneda.value);
                              total    += parseFloat(Viajes[i].value);
                              totalpto += parseFloat(Viajes[i].value) * ( cambio *  parseFloat(Tarifa.value) );
                        }
                      }
                    Total.value    = total; 
                    TotalPto.value = formato1(totalpto * unidades, 0);
                }
            }
          function newWindow(url, nombre){
              option=" status=yes, width="+ (screen.width-30) +", height="+ (screen.height-80)  +",  scrollbars=yes, statusbars=yes, resizable=yes, menubar=no ,top=10 , left=10 ";
              ventana=window.open('',nombre,option);
              ventana.location.href=url;
              ventana.focus();
           }


        function psubmit(tform) {
            with(tform){
                var total    = 0;
                var totalpto = 0;
                var cambio   = 0
                var unidades = parseFloat(CantidadR.value);
                for (i=0;i<Viajes.length;i++) {
                        if( /^\d+$/.test( Viajes[i].value) ){
                           cambio   =  buscarValor(Ano.value, (Mes.value!='TD'? Mes.value :  (i+1<10?'0':'')+(i+1)  ) , Moneda.value);
                           total    += parseFloat(Viajes[i].value);
                           totalpto += parseFloat(Viajes[i].value) * ( cambio *  parseFloat(Tarifa.value) );
                        }
                        else Viajes[i].value='';
                }
                Total.value    = total; 
                TotalPto.value = totalpto + unidades;
                if (total==0){
                   alert('Debe ingresar un valor por lo menos para poder grabarlo.');
                   return false;
                }
            }
            return true;
        }
        function qsubmit (tform){
            if (psubmit(tform)){
                with(tform){
                    for (i=0;i<Viajes.length;i++)
                        if(  Viajes[i].value=='' ) Viajes[i].value='0';                
                }
                return true;
            }
            else 
                return false;
        } 
	function formato1(numero, decimales){
	   var tmp = parseInt(numero) ;
	   var num = '';
	   var pos = 0;
	   while(tmp>0){
		  if (pos%3==0 && pos!=0) num = ',' + num;
		  res  = tmp % 10;
		  tmp  = parseInt(tmp / 10);
		  num  = res + num  ;
		  pos++;
	   }
	   var dec = 1;
	   for (i=0;i<decimales;i++, dec*=10);
	   dnum = parseInt ((parseFloat(numero) - parseInt(numero)) * dec);
           tmpdec = '';
	   for (i=0;i<decimales - (dnum==0?0:dnum.toString().length ) ;i++, tmpdec +='0');
	   num += (decimales>0? '.' +  tmpdec + (dnum!=0?dnum:''): '');
	   return num;
	}
