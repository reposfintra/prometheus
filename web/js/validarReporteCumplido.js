/***************************************************************************
 * Nombre clase :                 ValidarReporteCumplido.js                *
 * Descripcion :                  js que maneja los eventos                *
 *                                relacionados con la validacion del       *
 *                                programa de generacion del Reporte de    *
 *                                cumplidos                                *
 * Autor :                        Ing. Juan Manuel Escandon Perez          *
 * Fecha :                        01 de febrero de 2006, 04:24 PM          *
 * Copyright :                    Fintravalores S.A.                  *
 ***************************************************************************/
function validar(form){
            if (form.Opcion.value=='Guardar' || form.Opcion.value=='Modificar'){
                if (form.codigo.value ==''){
                    alert('Defina el codigo para poder continuar...')
                    return false;
                }
                if (form.descripcion.value ==''){
                    alert('Defina el descripcion para poder continuar...')
                    return false;
                }
            }
            return true;
      }
    function SeleccionarAll(){
            for(i=0;i<forma.length;i++)
                    forma.elements[i].checked=forma.All.checked;
    }
    function ActivarAll(){
            forma.All.checked = true;
            for(i=1;i<forma.length;i++)	
                    if (!forma.elements[i].checked){
                            forma.All.checked = false;
                            break;
                    }
    }      
    
    function validarCumplido(form){
            for(i=1;i<forma.length;i++)	
                    if (forma.elements[i].checked)
                            return true;
            alert('Por favor seleccione un registro para poder continuar');
            return false;
    } 
	
	function Recorrido(){
            for(i=1;i<forma.length;i++)	
                   forma.elements[i].checked = true;
    } 
	
	function Show(){
		rev.style.visibility=''; 
		rev.style.left=150; 		
		rev.style.top=225;
	}
	