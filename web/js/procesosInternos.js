/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    cargarComboProcesosMeta();
    if($('#otro').val()=='SI'){
        cargarUsuarios();
    }else{
        cargarProcesosInternos();
    }
      
   $('#close').click(function () {
        $('#div_procesoInterno').fadeOut('slow');
        return false;
    });
    $('#closeEditarProInterno').click(function () {
        $('#div_editar_procesoInterno').fadeOut('slow');
        $('#div_undNegocios').fadeOut();       
        $('#bt_asociar_undPro').hide();
        $('#bt_desasociar_undPro').hide();        
        return false;
    });
    $('#closeAsocUserProInterno').click(function () {
        $('#div_asoc_user_prointerno').fadeOut('slow');
        $('#div_usuarios').fadeOut();       
        $('#bt_asociar_userPro').hide();
        $('#bt_desasociar_userPro').hide();
        return false;
    });
    
});

function crearProcesoInterno(){
     $('#idProMeta').val('');
     $('#nomProceso').val('');   
     $('#div_procesoInterno').fadeIn("slow");
     AbrirDivCrearProcesoInterno();
}

function AbrirDivCrearProcesoInterno(){
      $("#div_procesoInterno").dialog({
        width: 675,
        height: 165,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
              guardarProcesoInterno();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function cargarProcesosInternos(){
    var url = './controller?estado=Procesos&accion=Meta&opcion=4';
    if ($("#gview_procesosInternos").length) {
        refrescarGridProcesosInterno();
     }else {
        jQuery("#procesosInternos").jqGrid({
            caption: 'Procesos Internos',
            url: url,
            datatype: 'json',
            height: 500,
            width: 600,
            colNames: ['Id', 'Descripcion', 'Id Proceso Meta', 'Proceso Meta', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key:true},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'center', width: '600px'},
                {name: 'id_tabla_rel', index: 'id_tabla_rel', hidden:true},
                {name: 'descripcionTablaRel', index: 'descripcionTablaRel', sortable: true, align: 'center', width: '600px'},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '200px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager:'#page_tabla_procesos_internos',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            gridComplete: function() {
                    var ids = jQuery("#procesosInternos").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarProcesoInterno('" + cl + "');\">";
                        us = "<img src='/fintra/images/botones/iconos/users.gif' align='absbottom'  name='asociar' id='asociar' width='15' height='15' title ='Asociar usuarios'  onclick=\"asociarUsuarioProceso('" + cl + "');\">";
//                      an = "<img src='/fintra/images/botones/iconos/anular.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el proceso interno seleccionado?','250','150',existeUsuarioRelProcesoInterno,'" + cl + "');\">";
                        an = "<img src='/fintra/images/botones/iconos/anular.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Puede que existan usuarios asociados al proceso interno, desea continuar?','350','165','" + cl + "');\">";
                        jQuery("#procesosInternos").jqGrid('setRowData', ids[i], {actions:ed+'   '+us+'   '+an});

                    }
                },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_procesos_internos",{edit:false,add:false,del:false});
        jQuery("#procesosInternos").jqGrid("navButtonAdd", "#page_tabla_procesos_internos", {
            caption: "Nuevo", 
            title: "Agregar nuevo proceso",           
            onClickButton: function() {
               crearProcesoInterno(); 
            }
        });
     }
}

function AbrirDivProcesoInterno() {
     $("#div_procesoInterno").dialog({
        width: 900,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false
      
    });
}

function guardarProcesoInterno(){
    var idProMeta = $('#idProMeta').val();
    var nomProceso = $('#nomProceso').val();
    var url = './controller?estado=Procesos&accion=Meta';
    if(idProMeta!='' && nomProceso!=''){
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 5,
                    descripcion: nomProceso,
                    procesoMeta: idProMeta
                },
                success: function(json) {
                    
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }
                        
                        if (json.respuesta === "OK") {                            
                            mensajesDelSistema("Se cre� el proceso interno", '250', '150', true);
                            $('#idProMeta').val('');
                            $('#nomProceso').val(''); 
                            refrescarGridProcesosInterno();                            
                        }                       
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear el proceso interno", '250', '150');
                    }                         
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }      
            });
    }else{
         mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');      
    }
}

function refrescarGridProcesosInterno(){
    var url = './controller?estado=Procesos&accion=Meta&opcion=4';
    jQuery("#procesosInternos").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#procesosInternos').trigger("reloadGrid");
}

function cargarUnidadNegocios(idProceso){   
    var url = './controller?estado=Procesos&accion=Meta&opcion=6&idProceso='+idProceso;
    $('#div_unidadNegocio').fadeIn();
    $('#bt_asociar_und').show();
    jQuery("#unidadNegocio").jqGrid({
                caption: 'Unidades de Negocio',
                url: url,
                datatype: 'json',
                height: 'auto',
                width: 400,
                colNames: ['Id', 'Descripcion'],
                colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key:true},
                    {name: 'descripcion', index: 'descripcion', sortable: true, align: 'center', width: '700px'}
               ],
                rowNum: 1000,
                rowTotal: 50000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                multiselect: true,
                jsonReader: {
                    root: 'rows',
                    repeatitems: false,
                    id: '0'
                },
                loadError: function(xhr, status, error) {
                    mensajesDelSistema(error, 250, 150);
                }
            });    
}

function refrescarGridUnidadNegocios(id){
    var url = './controller?estado=Procesos&accion=Meta&opcion=6&idProceso='+id;
    jQuery("#unidadNegocio").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#unidadNegocio').trigger("reloadGrid");
}

function asociarProcesoInterno(){
    var idProMeta = $('#idProMeta').val();
    var nomProceso = $('#nomProceso').val();
    var listado = "";
    var filasId =jQuery('#unidadNegocio').jqGrid('getGridParam', 'selarrrow');
    if(filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        
        var url = './controller?estado=Procesos&accion=Meta';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 7,
                listado: listado,
                nomProceso: nomProceso,
                procesoMeta: idProMeta
            },
                success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        mensajesDelSistema("Se asociaron las unidades de Negocio al proceso interno", '250', '150', true);
                        $('#div_procesoInterno').fadeOut();
                        refrescarGridProcesosInterno();
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar las unidades de negocio al proceso interno!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            }            
        });
        
    }else{
         mensajesDelSistema("Debe seleccionar los convenios a asociar!!", '250', '150');      
    }
}

function editarProcesoInterno(id){
    var nomProinterno = jQuery('#procesosInternos').getCell(id, 'descripcion');
    var idMetaProceso = jQuery('#procesosInternos').getCell(id, 'id_tabla_rel');
    $('#nomProinterno').val(nomProinterno);
    $('#idProMetaEdit').val(idMetaProceso);
    $('#idProinterno').val(id);
    $('#div_editar_procesoInterno').fadeIn();
    $('#div_und_prointerno').fadeIn(); 
    listarUndNegocioPro(id);
    listarUndNegocio();
}

function listarUndNegocioPro(id){
    var url = './controller?estado=Procesos&accion=Meta&opcion=8&idProinterno='+id;
     if ($("#gview_UndNegocioPro").length) {
         refrescarGridUndNegocioPro(id);
     }else {
         jQuery("#UndNegocioPro").jqGrid({
            caption: 'Unidades de Negocio del Proceso Interno',
            url: url,           
            datatype: 'json',
            height: 400,
            width: 300,
            colNames: ['Id', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden:true, align: 'center', width: '100px', key:true},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'left', width: '600px'}/*,
                {name: 'eliminar', index: 'eliminar', align: 'center', width: '150px'}*/
           ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect:true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {
                 /* var ids = jQuery("#UndNegocioPro").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        el = "<img src='/fintra/images/stop.png' align='absbottom'  name='eliminar' id='eliminar' width='15' height='15' title ='Eliminar'  onclick=\"mensajeConfirmAction('Esta seguro que desea eliminar este convenio?','250','150',eliminarUndNegocioProinterno,'" + cl + "');\">";
                        jQuery("#UndNegocioPro").jqGrid('setRowData', ids[i], {eliminar: el});

                    }*/
                    $('#bt_asociar_undPro').show();
                },
            loadError: function(xhr, status, error) {
               mensajesDelSistema(error, 250, 150);
            }
        });
      
    }    
}

function refrescarGridUndNegocioPro(id){
    var url = './controller?estado=Procesos&accion=Meta&opcion=8&idProinterno='+id;
    jQuery("#UndNegocioPro").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#UndNegocioPro').trigger("reloadGrid");
}

function listarUndNegocio(){
    var idProInterno = $('#idProinterno').val();
    var url = './controller?estado=Procesos&accion=Meta&opcion=6&idProceso='+idProInterno;
    $('#div_undNegocios').fadeIn();
    $('#bt_listar_undPro').hide();
    $('#bt_asociar_undPro').show();
    if ($("#gview_UndadesNegocios").length) {
         refrescarGridUndNegocio(idProInterno);
    }else {
        jQuery("#UndadesNegocios").jqGrid({
                caption: 'Unidades de Negocio',
                url: url,
                datatype: 'json',
                height: 400,
                width: 300,
                colNames: ['Id', 'Descripcion'],
                colModel: [
                    {name: 'id', index: 'id', sortable: true, hidden:true, align: 'center', width: '100px', key:true},
                    {name: 'descripcion', index: 'descripcion', sortable: true, align: 'left', width: '600px'}
               ],
                rowNum: 1000,
                rowTotal: 50000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                multiselect:true,
                jsonReader: {
                    root: 'rows',
                    repeatitems: false,
                    id: '0'
                },
                gridComplete: function() {                 
                    $('#bt_desasociar_undPro').show();
                },
                loadError: function(xhr, status, error) {
                   mensajesDelSistema(error, 250, 150);
                }
            });           
    }
}

function refrescarGridUndNegocio(id){
    var url = './controller?estado=Procesos&accion=Meta&opcion=6&idProceso='+id;
    jQuery("#UndadesNegocios").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#UndadesNegocios').trigger("reloadGrid");
}

function actualizarProInterno(){
    var idProinterno = $('#idProinterno').val();
    var nomProinterno = $('#nomProinterno').val();
    var idProMeta = $('#idProMetaEdit').val();
    $.ajax({
        url: './controller?estado=Procesos&accion=Meta',
        datatype:'json',
        type:'post',
        data:{
            opcion: 9,
            idProinterno: idProinterno,
            nomProinterno: nomProinterno,
            idProMeta: idProMeta
        },                
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                     mensajesDelSistema("El proceso se actualiz� correctamente!!", '250', '150', true);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo actualizar el proceso interno!!", '250', '150');
            }
            refrescarGridProcesosInterno();       
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function desasociarUndProinterno(){
    
    var idProinterno = $('#idProinterno').val();
    var listado = "";
    var filasId =jQuery('#UndNegocioPro').jqGrid('getGridParam', 'selarrrow');
    if(filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        $.ajax({
            url: './controller?estado=Procesos&accion=Meta',
            datatype:'json',
            type:'post',
            data:{
                opcion: 10,
                listado: listado,
                idProinterno: idProinterno
            },       
            success: function(json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }

                        if (json.respuesta === "OK") {                         
                           refrescarGridUndNegocioPro(idProinterno);
                           refrescarGridUndNegocio(idProinterno);
                        }

                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo desasignar el convenio del proceso interno", '250', '150');
                    }

            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }else{
        if (jQuery("#UndNegocioPro").jqGrid('getGridParam', 'records')>0) {
           mensajesDelSistema("Debe seleccionar los convenios a desasignar", '250', '150');
        } else {
            mensajesDelSistema("No hay convenios por desasignar", '250', '150');
        }         
    }
}

function asociarUndProinterno(){
    var idProinterno = $('#idProinterno').val();
    var listado = "";
    var filasId =jQuery('#UndadesNegocios').jqGrid('getGridParam', 'selarrrow');
    if(filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        
        var url = './controller?estado=Procesos&accion=Meta';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 7,
                listado: listado,
                idProinterno: idProinterno
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {                       
                        $('#div_procesoInterno').fadeOut();
                        refrescarGridUndNegocioPro(idProinterno);
                        refrescarGridUndNegocio(idProinterno);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar las unidades de negocio al proceso interno!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            }      
        });
        
    }else{
         if (jQuery("#UndadesNegocios").jqGrid('getGridParam', 'records')>0) {
            mensajesDelSistema("Debe seleccionar los convenios a asociar", '250', '150');
        } else {
            mensajesDelSistema("No hay convenios por asociar", '250', '150');
        }         
    }
}

function cargarUsuarios(){
    var url = './controller?estado=Procesos&accion=Meta&opcion=11';
    if ($("#gview_listUsuarios").length) {
        reloadGridUsuarios();
    } else {
    jQuery("#listUsuarios").jqGrid({
            caption: 'Usuarios',
            url: url,
            datatype: 'json',
            height: 600,
            width: 700,
            colNames: ['Cod Usuario','Nombre Usuario', 'Usuario Login'],
            colModel: [
                {name: 'codUsuario', index: 'codUsuario', align: 'center', width: '100px', key:true},
                {name: 'nombre', index: 'nombre',  align: 'left', width: '400px'},
                {name: 'idusuario', index: 'idusuario', align: 'center', width: '200px'}
           ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },           
            ondblClickRow: function (codUsuario) {
                var nombre = jQuery("#listUsuarios").getRowData(codUsuario).nombre;
                var idusuario = jQuery("#listUsuarios").getRowData(codUsuario).idusuario;
                cargarDatosUsuario(codUsuario,nombre,idusuario);

                },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
        jQuery("#listUsuarios").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    }
}

function reloadGridUsuarios(){
    var url = './controller?estado=Procesos&accion=Meta&opcion=11';
    jQuery("#listUsuarios").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#listUsuarios").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    
    jQuery('#listUsuarios').trigger("reloadGrid");
}

function cargarDatosUsuario(codUsuario,nombre,idusuario) {
    AbrirDivAsociarUser(codUsuario,nombre,idusuario);
    var url = './controller?estado=Procesos&accion=Meta&opcion=12&codUsuario='+codUsuario;
    if ($("#gview_datosUserProInterno").length) {
        reloadGridDatosUserProInterno(codUsuario);
    } else {
        jQuery("#datosUserProInterno").jqGrid({
            caption: 'Usuarios',
            url: url,
            datatype: 'json',
            height: 150,
            width: 700,
            colNames: ['Id ProInterno', 'Id ProMeta', 'Tipo', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key: true},
                {name: 'id_tabla_rel', index: 'id_tabla_rel', sortable: true, align: 'center', width: '100px'},
                {name: 'tipo', index: 'tipo', sortable: true, align: 'center', width: '100px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'left', width: '400px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}

function reloadGridDatosUserProInterno(codUsuario){
    var url = './controller?estado=Procesos&accion=Meta&opcion=12&codUsuario='+codUsuario;
    jQuery("#datosUserProInterno").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#datosUserProInterno').trigger("reloadGrid");
}

function AbrirDivAsociarUser(codUsuario,nombre,idusuario) {
    $('#codUsuario').val(codUsuario);
    $('#nombre').val(nombre);
    $('#login').val(idusuario);
    $("#div_asoc_user_prointerno").dialog({
        width: 900,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Salir": function () {
                $(this).dialog("close");
                $("#datosUserProInterno").jqGrid("clearGridData", true).trigger("reloadGrid");
            }
        }
      
    });
}

function listarProInterno(){
    $('bt_asociar_ProInterno').show();
    var url = './controller?estado=Procesos&accion=Meta&opcion=13';
    if ($("#gview_cargarProInterno").length) {
        reloadGridProInterno();
    } else {
        jQuery("#cargarProInterno").jqGrid({
            caption: 'Procesos Internos',
            url: url,
            datatype: 'json',
            height: 350,
            width: 750,
            colNames: ['Id ProInterno', 'Id ProMeta', 'Tipo', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key: true},
                {name: 'id_tabla_rel', index: 'id_tabla_rel', sortable: true, align: 'center', width: '100px'},
                {name: 'tipo', index: 'tipo', sortable: true, align: 'center', width: '100px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'left', width: '400px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
             loadComplete: function () {
                $('div_show_proInterno').fadeIn();
                
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}

function reloadGridProInterno(){
    var url = './controller?estado=Procesos&accion=Meta&opcion=13';
    jQuery("#cargarProInterno").setGridParam({
        datatype: 'json',
        url: url,
    });
    
    jQuery('#cargarProInterno').trigger("reloadGrid");
}

function asociarProinterno(){
    var codUsuario = $('#codUsuario').val();
    var login = $('#login').val();
    var listado = "";
    var filasId =jQuery('#cargarProInterno').jqGrid('getGridParam', 'selarrrow');
    if (filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        
        var url = './controller?estado=Procesos&accion=Meta';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "text",
            data: {
                opcion: 14,
                listado: listado,
                codUsuario: codUsuario,
                login: login
            },
            success: function(data){
                var resp = data.trim();
                if (resp == "OK") {
                    alert("Se asociaron los procesos seleccionados al usuario");
                     reloadGridDatosUserProInterno(codUsuario);
                       setTimeout(function() {
                        reloadGridProInterno();
                       }, 1000);
                }
            }
        });
        
    }else{
        alert("Debe seleccionar los procesos internos a asociar");
    }
}

function anularProcesoInterno(cl){
    $.ajax({
        url: './controller?estado=Procesos&accion=Meta',
        datatype:'json',
        type:'get',
        data:{
            opcion: 16,
            idProinterno: cl           
        },          
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                     mensajesDelSistema("Se anul� el proceso interno", '250', '150', true);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular el proceso interno!!", '250', '150');
            }
            refrescarGridProcesosInterno();       
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }  
    });
}

 function cargarComboProcesosMeta() {
   
    $.ajax({
        type: 'POST',
        url: './controller?estado=Procesos&accion=Meta',
        dataType: 'json',
        data: {
            opcion: 17
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#idProMeta').append("<option value=''>Seleccione</option>");
                    $('#idProMetaEdit').append("<option value=''>Seleccione</option>");
               
                    for (var key in json) {
                        $('#idProMeta').append('<option value=' + key + '>' + json[key] + '</option>');   
                        $('#idProMetaEdit').append('<option value=' + key + '>' + json[key] + '</option>');   
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                  $('#idProMeta').append("<option value=''>Seleccione</option>");
                  $('#idProMetaEdit').append("<option value=''>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function asociarUsuarioProceso(id){    
       var nomProinterno = jQuery('#procesosInternos').getCell(id, 'descripcion');
       var metaProceso = jQuery('#procesosInternos').getCell(id, 'descripcionTablaRel');   
       $('#idProinternoAsoc').val(id);
       $('#nomProinternoAsoc').val(nomProinterno);
       $('#nommetaproceso').val(metaProceso);
       $('#div_asoc_user_prointerno').fadeIn("slow");
       $('#div_user_prointerno').fadeIn();
       listarUsuariosProInterno(id);
       listarUsuariosRelProInterno();

}

function listarUsuariosProInterno(idProceso) {
   
    if ($("#gview_userProInterno").length) {
        reloadGridUsersProInterno(idProceso);
    } else {
        $("#userProInterno").jqGrid({
            caption: 'Usuarios relacionados con el proceso',
            url: './controller?estado=Procesos&accion=Meta&opcion=18&idProceso='+idProceso,           
            datatype: 'json',
            height:  400,
            width: 'auto',
            colNames: ['C�digo', 'Id usuario', 'Nombre'],
            colModel: [             
                {name: 'codUsuario', index: 'codUsuario', hidden:true, sortable: true, align: 'center', width: '100px', key: true},
                {name: 'idusuario', index: 'idusuario', hidden:true, sortable: true, align: 'center', width: '100px'},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '300px'}/*,
                {name: 'eliminar', index: 'eliminar', align: 'center', width: '150px'}*/
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {
                   /*var ids = jQuery("#userProInterno").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        el = "<img src='/fintra/images/stop.png' align='absbottom'  name='eliminar' id='eliminar' width='15' height='15' title ='Eliminar'  onclick=\"mensajeConfirmAction('Esta seguro de desasignar este usuario del proceso?','250','150',eliminarRelUsuarioProinterno,'" + cl + "');\">";
                        jQuery("#userProInterno").jqGrid('setRowData', ids[i], {eliminar: el});
                    }*/                                    
                    $('#bt_asociar_userPro').show();      
         
                    
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
        
    }
}

function reloadGridUsersProInterno(idProceso){
    var url = './controller?estado=Procesos&accion=Meta&opcion=18&idProceso='+idProceso;
    jQuery("#userProInterno").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#userProInterno').trigger("reloadGrid");
}

function listarUsuariosRelProInterno(){
    var idProceso = $('#idProinternoAsoc').val();
    $('#div_usuarios').fadeIn();
    $('#bt_listar_userPro').hide();
    $('#bt_asociar_userPro').show();
    if ($("#gview_listUsuarios").length) {
        reloadGridUsuariosRelProInterno(idProceso);
    } else {
            var cbColModel;
            idsOfSelectedRows = [];
            jQuery("#listUsuarios").jqGrid({
                caption: 'Usuarios',
                url: './controller?estado=Procesos&accion=Meta&opcion=19&idProceso=' + idProceso,
                datatype: 'json',
                height: 384,
                width: 'auto',
                colNames: ['Cod Usuario', 'Nombre Usuario', 'Usuario Login'],
                colModel: [
                    {name: 'codUsuario', index: 'codUsuario', hidden:true, align: 'center', width: '100px', key: true},
                    {name: 'nombre', index: 'nombre', align: 'left', width: '300px'},
                    {name: 'idusuario', index: 'idusuario', hidden:true, align: 'center', width: '200px'}
                ],
                rowNum: 1000,
                rowTotal: 50000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                multiselect: true,
                jsonReader: {
                    root: 'rows',
                    repeatitems: false,
                    id: '0'
                },
                onSelectRow: function (id, isSelected) {
                    var p = this.p, item = p.data[p._index[id]], i = $.inArray(id, idsOfSelectedRows);
                    item.cb = isSelected;
                    if (!isSelected && i >= 0) {
                        idsOfSelectedRows.splice(i,1); // remove id from the list
                    } else if (i < 0) {
                        idsOfSelectedRows.push(id);
                    }                    
                },     
                loadComplete: function () {
                    var p = this.p, data = p.data, item, $this = $(this), index = p._index, rowid, i, selCount;
                    for (i = 0, selCount = idsOfSelectedRows.length; i < selCount; i++) {
                        rowid = idsOfSelectedRows[i];
                        item = data[index[rowid]];
                        if ('cb' in item && item.cb) {
                            $this.jqGrid('setSelection', rowid, false);
                        }
                    }
                },
                gridComplete: function() {                 
                    $('#bt_desasociar_userPro').show();
                },
                loadError: function(xhr, status, error) {
                    mensajesDelSistema(error, 250, 150);
                }
            });
            cbColModel = jQuery("#listUsuarios").jqGrid('getColProp', 'cb');
            cbColModel.sortable = true;
            cbColModel.sorttype = function (value, item) {
                return 'cb' in item && item.cb ? 1 : 0;
            };
            jQuery("#listUsuarios").jqGrid('filterToolbar',
                    {
                        autosearch: true,
                        searchOnEnter: true,
                        defaultSearch: "cn",
                        stringResult: true,
                        ignoreCase: true,
                        multipleSearch: true
                    });                    
       
    }
}

function reloadGridUsuariosRelProInterno(idProceso){
    var url = './controller?estado=Procesos&accion=Meta&opcion=19&idProceso='+idProceso;
    jQuery("#listUsuarios").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#listUsuarios").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    
    jQuery('#listUsuarios').trigger("reloadGrid");
}

function asociarUsuariosProinterno(){
    
    var idProinterno = $('#idProinternoAsoc').val();
    var jsonObj = [];
    var filasId =jQuery('#listUsuarios').jqGrid('getGridParam', 'selarrrow');
    if (filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            var id = filasId[i];
            var j = $.inArray(id, idsOfSelectedRows);
            idsOfSelectedRows.splice(j,1); // remove id from the list
            var idUsuario = jQuery("#listUsuarios").getRowData(id).idusuario;
            var item = {};
            item ["cod_usuario"] = id;
            item ["id_usuario"] = idUsuario;

            jsonObj.push(item);
        }
       var listUser = {};
       listUser ["usuarios"] = jsonObj;
      
        var url = './controller?estado=Procesos&accion=Meta';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 20,
                listado: JSON.stringify(listUser),
                idProInterno: idProinterno
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {                                           
                        reloadGridUsersProInterno(idProinterno);                       
                        reloadGridUsuariosRelProInterno(idProinterno);
                                          
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar los usuarios al proceso interno!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            } 
        });
        
    }else{
        if (jQuery("#listUsuarios").jqGrid('getGridParam', 'records')>0) {
             mensajesDelSistema("Debe seleccionar los usuarios a asociar al proceso interno!!", '250', '150');
        }else{
             mensajesDelSistema("No hay usuarios por asociar", '250', '150');
        }
             
    }
}



function desasociarUsuariosProinterno(){
    var idProinterno = $('#idProinternoAsoc').val();
    var listado = "";
    var filasId =jQuery('#userProInterno').jqGrid('getGridParam', 'selarrrow');
    if(filasId != ''){
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }   
        $.ajax({
            url: './controller?estado=Procesos&accion=Meta',
            datatype:'json',
            type:'post',
            data:{
                opcion: 21,
                listado: listado,
                idProinterno: idProinterno
            },      
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {                     
                        reloadGridUsersProInterno(idProinterno);
                        reloadGridUsuariosRelProInterno(idProinterno);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo desasignar los usuarios del proceso interno!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            }          
        });
    }else{      
        if (jQuery("#userProInterno").jqGrid('getGridParam', 'records')>0) {           
            mensajesDelSistema("Debe seleccionar los usuarios a desasignar", '250', '150');
        } else {
            mensajesDelSistema("No hay usuarios por desasignar", '250', '150');
        }        
    }
      
    
}

function existeUsuarioRelProcesoInterno(id){
    $.ajax({
        url: './controller?estado=Procesos&accion=Meta',
        datatype:'json',
        type:'post',
        data:{
            opcion: 22,
            tipo:"PINI",
            idProceso:id
        },          
        success: function(json) {
            if (!isEmptyJSON(json)) {
     
                if (json.respuesta === "SI") {
                    mensajeConfirmAnulacion('ALERTA!!! Existen usuarios asociados al proceso interno, desea continuar?','350','165', id);      
                }else{
                     anularProcesoInterno(id);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo realizar el proceso!!", '250', '150');
            }                 
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }  
    });
}

function mensajeConfirmAnulacion(msj, width, height, id) {
    
    mostrarContenido('dialogMsgPini');
    $("#msj").html("<span style='background: url(/fintra/images/warning03.png); height:32px; width:32px;float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsgPini").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog('destroy');               
                anularProcesoInterno(id);               
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajeConfirmAction(msj, width, height, okAction,  id) {
    mostrarContenido('dialogMsgPini');
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsgPini").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () { 
                $(this).dialog('destroy');
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    mostrarContenido('dialogMsgPini');
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsgPini").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("close");             
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

