



function ListarPerfiles(id) {

    var usuario = $("#"+id).val();
    $.ajax({
        type: 'POST',
      //url: "../controller?estado=Perfiles&accion=Riesgos",
        url: "controller?estado=Perfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos&pagina=ListarPerfilesRiesgos.jsp",
      
        dataType: 'json',
        data: {
            opcion: 1,
            usuario:usuario            
                },
        success: function(Resp) {
         ///////////////////////////////////////////////////////////////////////////////////////////
          var i=0;
        for(var i in Resp) {
        for(var j in Resp[i]) {
            

            if(j=='reg_status'){
             var accion = "";
             if(Resp[i]["reg_status"]==''){
                 accion='checked';
             }else{
                 accion='';
             }
             Resp[i][j]="<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='modificar' "+accion+" > <label onclick=\"ElimReg( "+Resp[i]["id"]+",'"+Resp[i]["reg_status"]+"')\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
             i=i+1;
           }   
            
        }
        }
            $("#TabPerfRies").jqGrid({
                
                data: Resp,       
                datatype: "local",
                height: 250,
		width: 1000,
                viewrecords: true, // show the current page, data rang and total records on the toolbar
                caption: "Lista de perfiles de riesgos",
                rowNum: 500,
                pager: "",
                
                colModel: [
                   {label: 'Id perfil', name: 'id', index: 'id', sortable: true, width: 50, align: 'right', search: false, key: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: ""}},

                    {label: 'Perfil', name: 'descripcion', index: 'descripcion', width: 120, sortable: true, align: 'left', hidden: false},
                   {label: 'Monto minimo', name: 'monto_minimo', index: 'monto_minimo', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                   {label: 'Monto maximo', name: 'monto_maximo', index: 'monto_maximo', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                   {label: 'L�mite de asignaciones por d�a', name: 'cant_max_cred', index: 'cant_max_cred', sortable: true, width: 120, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0}},
                  {label: 'Estado', name: 'reg_status', index: 'reg_status', width: 80, sortable: true, align: 'left', hidden: false},   {label: 'Modificar', name: 'modificar', index: 'modificar', width: 70, sortable: true, align: 'left', hidden: false}
                 

                ],
                gridComplete: function (id, rowid) {
                var cant = jQuery("#TabPerfRies").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var estado = $("#TabPerfRies").getRowData(cant[i]).reg_status;
                    var id = $("#TabPerfRies").getRowData(cant[i]).id;
                    var accion = "";
                    if(estado==''){accion='checked'}else{accion=''}
                    
                    var id = $("#TabPerfRies").getRowData(cant[i]).id;
                    var cl = cant[i];
                  //  CheckEstado = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='modificar' "+accion+" > <label onclick=\"ElimReg( "+id+",'"+estado+"')\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    colModificacion = "<a style=\"color:black;\" href='/fintra/controller?estado=Perfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos&pagina=ModificarPerfilesRiesgos.jsp&idperfil="+id+"')>Modificar</a>";
                    jQuery("#TabPerfRies").jqGrid('setRowData', cant[i], {modificar: colModificacion/*, reg_status: CheckEstado*/});
                }
            }

            });
            
        },
        error: function(xhr, ajaxOptions, thrownError) {
              mensajesDelSistema("Error al cargar los perfiles.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 

        }
    }); // fin $.ajax
     
     

}

function ElimReg(idperfil,estadoActual){

               $.ajax({
        type: 'POST',
        url: "controller?estado=Perfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos&pagina=ListarPerfilesRiesgos.jsp",
      
        dataType: 'json',
        data: {
            opcion: 2,
            idperfil:idperfil ,
            estadoActual:estadoActual            
                },
        success: function(Resp) {

        },
        error: function(xhr, ajaxOptions, thrownError) {
              mensajesDelSistema("Error al eliminar el perfil.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax         

}
                
function crearPerfil(){
    var monto_maximo = $("#monto_maximo").val().replace(/[.]/g, '');
    var monto_minimo = $("#monto_minimo").val().replace(/[.]/g, '');
    var cant_max_cred = $("#cant_max_cred").val().replace(/[.]/g, '');
    var descripcion = $("#descripcion").val();
    
    //guardar en una cadeana de texto los ids de la unidades marcadas
     var unidades_selecionadas = '';
        $("input:checkbox[id^='jqg_']").each(function(index,e){
            var $this = $(this);
            if($this.is(":checked")){
                var idCheck = $this.attr("id");
                var arrayIdCheck = idCheck.split('_');//Se divide el id del Checkbox
                var subIdCheck = arrayIdCheck[2];//Se guarda la parte que contiene el id de la unidad de negocio
                 unidades_selecionadas += subIdCheck +',';
            } 
        });
if(descripcion===""){
 mensajesDelSistema("Debe llenar la descripcion",'300', '150',  true); 
}else if(monto_minimo===""){
 mensajesDelSistema("Debe llenar el monto minimo",'300', '150',  true);
} else if(monto_maximo===""){
 mensajesDelSistema("Debe llenar el monto maximo",'300', '150',  true);
}else if(cant_max_cred===""){
 mensajesDelSistema("Debe llenar la cantidad maxima de creditos",'300', '150',  true);
}else if(unidades_selecionadas===""){
 mensajesDelSistema("Debe seleccionar al menos una unidad",'300', '150',  true);
}else{

   var msj="Desea Confirmar la creacion del perfil!";
   $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: 300,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Cancelar": function () {
                $(this).dialog("destroy");
                mensajesDelSistema("Creacion cancelada",'300', '150',  true); 
            }, "Aceptar": function () {
                
    $.ajax({
        type: 'POST',
        url: "../../controller?estado=Perfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos&pagina=ListarPerfilesRiesgos.jsp",
      
        dataType: 'json',
        data: {
            monto_maximo: monto_maximo,
            monto_minimo: monto_minimo,
            cant_max_cred: cant_max_cred,
            descripcion: descripcion,
            unidades_selecionadas:unidades_selecionadas,
             opcion: 3
                },
        success: function(Resp) {
            
            if(Resp=="ok"){
                alertaExitosa("Se ha creado correctamente el perfil "+descripcion, '300', '150','crear'); 

            }else{
               mensajesDelSistema(Resp,'350', '150',  true); 
            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
              mensajesDelSistema("Error al crear el perfil.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax   
                
              $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();

}

    
}

function retornarDatosPerfil(idperfil){

    
      $.ajax({
        type: 'POST',
        url: "controller?estado=Perfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos&pagina=ListarPerfilesRiesgos.jsp",
      
        dataType: 'json',
        data: {
            opcion: 4,
            idperfil:idperfil            
                },
        success: function(Resp) {
            
             var c_monto_maximo =  Resp[0]["monto_maximo"].toString();
              var c_monto_minimo =  Resp[0]["monto_minimo"].toString();
              var c_cant_max_cred =  Resp[0]["cant_max_cred"].toString();

              var v_monto_maximo ="0";
              var v_monto_minimo ="0";
              var v_cant_max_cred ="0";
             

            if(c_monto_maximo.length>3){
                  v_monto_maximo =  puntosDeMilreturn(c_monto_maximo,c_monto_maximo.charAt(c_monto_maximo.length-1));
            }  else{
                v_monto_maximo=c_monto_maximo;
            }
            if(c_monto_minimo.length>3){
                  v_monto_minimo =  puntosDeMilreturn(c_monto_minimo,c_monto_minimo.charAt(c_monto_minimo.length-1));
            
            }else{
                v_monto_minimo = c_monto_minimo;
            }
            if(c_cant_max_cred.length>3){
                   v_cant_max_cred =  puntosDeMilreturn(c_cant_max_cred,c_cant_max_cred.charAt(c_cant_max_cred.length-1));
 
            }else{
                v_cant_max_cred=c_cant_max_cred;
            }
           
             $("#descripcion").val(Resp[0]["descripcion"]);
             $("#monto_maximo").val(v_monto_maximo);
             $("#monto_minimo").val(v_monto_minimo);
             $("#cant_max_cred").val(v_cant_max_cred);
             
              var idperf=Resp[0]["id"];
             retornarUnidadNegocio(idperf);
           
            
        },
        error: function(xhr, ajaxOptions, thrownError) {
              mensajesDelSistema("Error al cargar la informacion del perfil.\n"+
                    "Favor actualizar la pagina", '370', '150',  true); 
        }
    }); // fin $.ajax         
}

function retornarUnidadNegocio(idperf){
    
      $.ajax({
        type: 'POST',
        url: "/fintra/controller?estado=Perfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos&pagina=ListarPerfilesRiesgos.jsp",
      
        dataType: 'json',
        data: {
            opcion: 6
            //id:5            
                },
        success: function(Resp) {
            var hjh='';
                 jQuery("#TabUniNeg").jqGrid({
	datatype: "local",
	height: 100,
	width: 450,
   	colNames:['Unidades De Necocio'],
   	colModel:[
   		{label:'Descripcion',name:'descripcion',index:'descripcion', width:100}	
   	],
   	multiselect: true,
   	//caption: "Manipulating Array Data"
        });
        
        for(var i=0;i<Resp.length;i++)
	jQuery("#TabUniNeg").jqGrid('addRowData',Resp[i]['id'],Resp[i]);
           
           //Esta funci�n se encarga de marcar los checks de las unidades que tenga asociadas el perfil.
           retornarUnidadesDelPerfil(idperf);
        },
        error: function(xhr, ajaxOptions, thrownError) {
              mensajesDelSistema("Error al cargar las unidades.\n"+
                    "Favor actualizar la pagina", '390', '150',  true); 
        }
    }); // fin $.ajax         
}

function retornarUnidadesDelPerfil(idperfil){
         $.ajax({
        type: 'POST',
      //url: "../controller?estado=Perfiles&accion=Riesgos",
        url: "/fintra/controller?estado=Perfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos&pagina=ListarPerfilesRiesgos.jsp",
      
        dataType: 'json',
        data: {
            idperfil:idperfil,
            opcion: 7
            //id:5            
                },
        success: function(Resp) {

             for(var i in Resp) {
          
        $("input:checkbox[id^='jqg_']").each(function(index,e){
            var $this = $(this);
            var idCheck = $this.attr("id");
            var arrayIdCheck = idCheck.split('_');//Se divide el id del Checkbox
            var subIdCheck = arrayIdCheck[2];//Se guarda la parte que contiene el id del perfil
            if(Resp[i]['id']==subIdCheck){
            $("#jqg_TabUniNeg_"+subIdCheck).attr('checked',true);
            }
        });

        
        }

        },
        error: function(xhr, ajaxOptions, thrownError) {
              mensajesDelSistema("Error al cargar las unidades del perfil.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax 
    
}


function ModificarPerfil(idperfil){
    var monto_maximo = $("#monto_maximo").val().replace(/[.]/g, '');
    var monto_minimo = $("#monto_minimo").val().replace(/[.]/g, '');
    var cant_max_cred = $("#cant_max_cred").val().replace(/[.]/g, '');
    var descripcion = $("#descripcion").val();
    //var dstrct = "Fintra";
        //guardar en una cadeana de texto los ids de la unidades marcadas
    var unidades_selecionadas = '';
        $("input:checkbox[id^='jqg_']").each(function(index,e){
            var $this = $(this);
            if($this.is(":checked")){
                var idCheck = $this.attr("id");
                var arrayIdCheck = idCheck.split('_');//Se divide el id del Checkbox
                var subIdCheck = arrayIdCheck[2];//Se guarda la parte que contiene el id del perfil
                 unidades_selecionadas += subIdCheck +',';
            } 
        });
        
if(descripcion===""){
 mensajesDelSistema("Debe llenar la descripcion",'300', '150',  true); 
}else if(monto_minimo===""){
 mensajesDelSistema("Debe llenar el monto minimo",'300', '150',  true);
} else if(monto_maximo===""){
 mensajesDelSistema("Debe llenar el monto maximo",'300', '150',  true);
}else if(cant_max_cred===""){
 mensajesDelSistema("Debe llenar la cantidad maxima de creditos",'300', '150',  true);
}else if(unidades_selecionadas===""){
 mensajesDelSistema("Debe seleccionar al menos una unidad",'300', '150',  true);
}else{
 
   var msj="Desea confirmar la modificacion del perfil!";
   $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: 300,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Cancelar": function () {
                $(this).dialog("destroy");
                mensajesDelSistema("Creacion cancelada",'300', '150',  true); 
            }, "Aceptar": function () { 
                
     $.ajax({
        type: 'POST',
      //url: "../controller?estado=Perfiles&accion=Riesgos",
        url: "controller?estado=Perfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos&pagina=ListarPerfilesRiesgos.jsp",
      
        dataType: 'json',
        data: {
            idperfil:idperfil,
            monto_maximo: monto_maximo,
            monto_minimo: monto_minimo,
            cant_max_cred: cant_max_cred,
            descripcion: descripcion,
            //dstrct: dstrct,
            unidades_selecionadas:unidades_selecionadas,
            /*libranza:libranza,
            consumo:consumo,
            educativo:educativo,
            microcredito:microcredito,*/
             opcion: 5
                },
        success: function(Resp) {

            if(Resp=="ok"){
                alertaExitosa("Se ha modificado correctamente el perfil"+" "+descripcion, '300', '150','modif'); 

            }else{
               mensajesDelSistema(Resp,'300', '150',  true); 
            }
            
            

        },
        error: function(xhr, ajaxOptions, thrownError) {
              mensajesDelSistema("Error al modificar el perfil.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax     
              $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();   
     
    
 }
    
}


function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}


function alertaExitosa(msj, width, height,ruta) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {
                $(this).dialog("destroy");
                if(ruta=='modif'){
                  window.location="controller?estado=Perfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos&pagina=ListarPerfilesRiesgos.jsp";
             }else if(ruta=='crear'){
              window.location="../../controller?estado=Perfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos&pagina=ListarPerfilesRiesgos.jsp";
              }
           }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

$(document).ready(function(){
    $('.solo_numero').keyup(function () {
       this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    
        $('.puntos_de_mil').keyup(function () {
        var donde=this;
        var  caracter = this.value.charAt(this.value.length-1);
       	pat = /[\*,\+,\(,\),\?,\,$,\[,\],\^]/
	valor = donde.value
	largo = valor.length
	crtr = true
	if(isNaN(caracter) || pat.test(caracter) == true){
		if (pat.test(caracter)==true){ 
			caracter = "\"" + caracter

		}
		carcter = new RegExp(caracter,"g")
		valor = valor.replace(carcter,"")
		donde.value = valor
		crtr = false
	}
	else{
		var nums = new Array()
		cont = 0
		for(m=0;m<largo;m++){
			if(valor.charAt(m) == "." || valor.charAt(m) == " ")
				{
                                    //continue;
                                }
			else{
				nums[cont] = valor.charAt(m)
				cont++
			}
		}
	}
	var cad1="",cad2="",tres=0
	if(largo > 3 && crtr == true){
		for (k=nums.length-1;k>=0;k--){
			cad1 = nums[k]
			cad2 = cad1 + cad2
			tres++
			if((tres%3) == 0){
				if(k!=0){
					cad2 = "." + cad2
				}
			}
		}
		donde.value = cad2
	}
    });
});


function puntosDeMilreturn(donde,caracter){
       	pat = /[\*,\+,\(,\),\?,\,$,\[,\],\^]/
	valor = donde
	largo = valor.length
	crtr = true
	if(isNaN(caracter) || pat.test(caracter) == true){
		if (pat.test(caracter)==true){ 
			caracter = "\"" + caracter

		}
		carcter = new RegExp(caracter,"g")
		valor = valor.replace(carcter,"")
		donde = valor
		crtr = false
	}
	else{
		var nums = new Array()
		cont = 0
		for(m=0;m<largo;m++){
			if(valor.charAt(m) == "." || valor.charAt(m) == " ")
				{
                                    //continue;
                                }
			else{
				nums[cont] = valor.charAt(m)
				cont++
			}
		}
	}
	var cad1="",cad2="",tres=0
	if(largo > 3 && crtr == true){
		for (k=nums.length-1;k>=0;k--){
			cad1 = nums[k]
			cad2 = cad1 + cad2
			tres++
			if((tres%3) == 0){
				if(k!=0){
					cad2 = "." + cad2
				}
			}
		}
		return cad2;    
	}
    
}