/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $("#fecha").val('');
    $("#fechafin").val('');
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    // $("#fecha").datepicker("setDate", myDate);
    //$("#fechafin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    cargarEstadoOfertas();
    $("#buscar").click(function () {
        var fechainicio = $("#fecha").val();
        var fechafin = $("#fechafin").val();
        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
            cargarOfertas();
        } else {
            mensajesDelSistema("Por favor revice el rango de fechas", '410', '150', false);
        }
    });
    $("#limpiar").click(function () {
        var fechainicio = $("#fecha").val("");
        var fechafin = $("#fechafin").val("");
    });

    $("#excluir").click(function () {
        ventanaExcluirNumOs();
    });

    $("#buscarinfo").click(function () {
        tipoOperacion();
    });

});
function cargarEstadoOfertas() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 13
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#estadoOfert').html('');
                $('#estadoOfert').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#estadoOfert').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarOfertas() {
    var grid_tabla = jQuery("#tabla_ofertas");
    if ($("#gview_tabla_ofertas").length) {
        reloadGridOfertas(grid_tabla, 14);
    } else {
        grid_tabla.jqGrid({
            caption: "Ofertas",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '530',
            width: '1650',
            colNames: ['Id solicitud', 'Nombre oferta','Alcance', 'Id cliente', 'Nombre cliente', 'Id cliente padre', 'Nombe cliente padre', 'Nic', 'Nombre responsable', 'Nombre interventor', 'Tipo solicitud', 'Fecha oferta', 'Fecha inicial',
                'Fecha aceptacion pagos','Multi-servicio', 'Fecha orden trabajo','Avance esperado', 'Avance registrado','Observaciones','Fecha ultimo seguimiento','Dias cronograma', 'Fecha finalizacion cronograma', 'Esquema', 'Valor oferta', 'Costo contratista', 'Materia', 'Mano de obra', 'Transporte', '% Administracion',
                'Administracion', '% Imprevisto', 'Imprevito', '% Utilidad', 'Utilidad', 'Bonificacion', 'Opav', 'Fintra', 'Interventoria', 'Provintegral', 'Eca', 'Base iva contratista', 'Iva contratista', 'Iva bonificacion', 'Iva opav', 
                'Iva fintra', 'Iva interventoria', 'Iva provintegral', 'Iva eca', 'Financiar sin iva', 'Iva', 'Financiar con iva'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', width: 60, sortable: true, align: 'center', hidden: false,key:true},
                {name: 'nombre_solicitud', index: 'nombre_solicitud', width: 180, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'alcalce', index: 'alcalce', width: 200, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_cliente', index: 'id_cliente', width: 80, sortable: true, align: 'center', hidden: true, search: true},
                {name: 'nomcli', index: 'nomcli', width: 180, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_cliente_padre', index: 'id_cliente_padre', width: 120, sortable: true, align: 'center', hidden: true, search: true},
                {name: 'nomcli_padre', index: 'nomcli_padre', width: 180, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nic', index: 'nic', width: 70, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_responsable', index: 'nombre_responsable', width: 170, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_interventor', index: 'nombre_interventor', width: 170, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'fecha_oferta', index: 'fecha_oferta', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_inicial', index: 'fecha_inicial', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_aceptacion_pagos1', index: 'fecha_aceptacion_pagos1', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'num_os', index: 'num_os', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'fecha_orden_trabajo1', index: 'fecha_orden_trabajo1', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'avance_esperado', index: 'avance_esperado', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'avance_registrado', index: 'avance_registrado', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'observaciones', index: 'observaciones', width: 280, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'fecha_ultima_ejecucion', index: 'fecha_ultima_ejecucion', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'finalizacion_cronograma', index: 'finalizacion_cronograma', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_finalizacion_cronograma', index: 'fecha_finalizacion_cronograma', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'esquema', index: 'esquema', width: 60, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'precio_total', index: 'precio_total', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'costo_contratista', index: 'costo_contratista', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'material', index: 'material', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'mano_obra', index: 'mano_obra', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'transporte', index: 'transporte', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'porc_administracion', index: 'porc_administracion', width: 110, sortable: true, align: 'right', hidden: false, search: true},
                {name: 'administracion', index: 'administracion', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'porc_imprevisto', index: 'porc_imprevisto', width: 110, sortable: true, align: 'right', hidden: false, search: true},
                {name: 'imprevisto', index: 'imprevisto', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'porc_utilidad', index: 'porc_utilidad', width: 95, sortable: true, align: 'right', hidden: true},
                {name: 'utilidad', index: 'utilidad', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'bonificacion', index: 'bonificacion', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'opav', index: 'opav', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'fintra', index: 'fintra', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interventoria', index: 'interventoria', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'provintegral', index: 'provintegral', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'eca', index: 'eca', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'base_iva_contratista', index: 'base_iva_contratista', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_contratista', index: 'iva_contratista', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_bonificacion', index: 'iva_bonificacion', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_opav', index: 'iva_opav', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_fintra', index: 'iva_fintra', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_interventoria', index: 'iva_interventoria', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_provintegral', index: 'iva_provintegral', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_eca', index: 'iva_eca', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'financiar_sin_iva', index: 'financiar_sin_iva', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva', index: 'iva', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'financiar_con_iva', index: 'financiar_con_iva', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            sortname: 'invdate',
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_ofertas"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id_solicitud = filas.id_solicitud;
                mostrarAcciones(id_solicitud);
            }, gridComplete: function () {
                var material = grid_tabla.jqGrid('getCol', 'material', false, 'sum');
                var mano_obra = grid_tabla.jqGrid('getCol', 'mano_obra', false, 'sum');
                var transporte = grid_tabla.jqGrid('getCol', 'transporte', false, 'sum');
                var administracion = grid_tabla.jqGrid('getCol', 'administracion', false, 'sum');
                var imprevisto = grid_tabla.jqGrid('getCol', 'imprevisto', false, 'sum');
                var utilidad = grid_tabla.jqGrid('getCol', 'utilidad', false, 'sum');
                var bonificacion = grid_tabla.jqGrid('getCol', 'bonificacion', false, 'sum');
                var opav = grid_tabla.jqGrid('getCol', 'opav', false, 'sum');
                var fintra = grid_tabla.jqGrid('getCol', 'fintra', false, 'sum');
                var interventoria = grid_tabla.jqGrid('getCol', 'interventoria', false, 'sum');
                var provintegral = grid_tabla.jqGrid('getCol', 'provintegral', false, 'sum');
                var eca = grid_tabla.jqGrid('getCol', 'eca', false, 'sum');
                var base_iva_contratista = grid_tabla.jqGrid('getCol', 'base_iva_contratista', false, 'sum');
                var iva_contratista = grid_tabla.jqGrid('getCol', 'iva_contratista', false, 'sum');
                var iva_bonificacion = grid_tabla.jqGrid('getCol', 'iva_bonificacion', false, 'sum');
                var iva_opav = grid_tabla.jqGrid('getCol', 'iva_opav', false, 'sum');
                var iva_fintra = grid_tabla.jqGrid('getCol', 'iva_fintra', false, 'sum');
                var iva_interventoria = grid_tabla.jqGrid('getCol', 'iva_interventoria', false, 'sum');
                var iva_provintegral = grid_tabla.jqGrid('getCol', 'iva_provintegral', false, 'sum');
                var iva_eca = grid_tabla.jqGrid('getCol', 'iva_eca', false, 'sum');
                var financiar_sin_iva = grid_tabla.jqGrid('getCol', 'financiar_sin_iva', false, 'sum');
                var iva = grid_tabla.jqGrid('getCol', 'iva', false, 'sum');
                var financiar_con_iva = grid_tabla.jqGrid('getCol', 'financiar_con_iva', false, 'sum');
                var precio_total = grid_tabla.jqGrid('getCol', 'precio_total', false, 'sum');
                var costo_contratista = grid_tabla.jqGrid('getCol', 'costo_contratista', false, 'sum');

                grid_tabla.jqGrid('footerData', 'set', {esquema: 'TOTAL', material: material, mano_obra: mano_obra, transporte: transporte, administracion: administracion, imprevisto: imprevisto,
                    utilidad: utilidad, bonificacion: bonificacion, opav: opav, fintra: fintra, interventoria: interventoria, provintegral: provintegral, eca: eca,
                    base_iva_contratista: base_iva_contratista, iva_contratista: iva_contratista, iva_bonificacion: iva_bonificacion, iva_opav: iva_opav, iva_fintra: iva_fintra,
                    iva_interventoria: iva_interventoria, iva_provintegral: iva_provintegral, iva_eca: iva_eca, financiar_sin_iva: financiar_sin_iva, iva: iva, financiar_con_iva: financiar_con_iva, precio_total: precio_total, costo_contratista: costo_contratista});
            },
            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                $("tr.jqgrow", this).contextMenu('menu', {
                    bindings: {
                        'observaciones': function (trigger) {
                            var filas = jQuery("#tabla_ofertas").getRowData(trigger.id);
                            var id_solicitud = filas.id_solicitud;
                            mostrarObservaciones(id_solicitud);
                        }
                    }, onContextMenu: function (event/*, menu*/) {
                        return true;
                    }
                });
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }

            },
            ajaxGridOptions: {
                data: {
                    opcion: 14,
                    estado_oferta: $("#estadoOfert").val(),
                    fechainicio: $("#fecha").val(),
                    fechafin: $("#fechafin").val(),
                    multiservicio: $("#multiservicio").val(),
                    cliente: $("#cliente").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
        $("#tabla_ofertas").navButtonAdd('#pager1', {
            caption: "Distribuciones",
            onClickButton: function () {
                mostrarDistribuciones();
            }
        });
        $("#tabla_ofertas").navButtonAdd('#pager1', {
            caption: "Exportar Excel",
            onClickButton: function () {
                var info = grid_tabla.getGridParam('records');
                if (info > 0) {
                    exportarExcelOfertas();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }

            }
        });
    }
}

function reloadGridOfertas(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                estado_oferta: $("#estadoOfert").val(),
                fechainicio: $("#fecha").val(),
                fechafin: $("#fechafin").val(),
                multiservicio: $("#multiservicio").val(),
                cliente: $("#cliente").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function  mostrarAcciones(id_solicitud) {
    cargarAcciones(id_solicitud);
    $("#dialogMsjAcciones").dialog({
        width: '1419',
        height: '500',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'DETALLE OFERTAS',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarAcciones(id_solicitud) {
    var grid_tabla = jQuery("#tabla_acciones");
    if ($("#gview_tabla_acciones").length) {
        reloadGridAcciones(grid_tabla, 15, id_solicitud);
    } else {
        grid_tabla.jqGrid({
            //     caption: "ACCIONES",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '1800',
            colNames: ['Contratista', 'Nombre contratista', 'Nombre Cliente', 'Id accion', 'estado', 'Descripcion estado', 'Costo contratista', 'Materia', 'Mano de obra', 'Transporte', '% Administracion',
                'Administracion', '% Imprevisto', 'Imprevito', '% Utilidad', 'Utilidad', 'Descripcion', 'Bonificacion', 'Opav', 'Fintra', 'Interventoria',
                'Provintegral', 'Eca', 'Base iva contratista', 'Iva contratista', 'Iva bonificacion', 'Iva opav', 'Iva fintra', 'Iva interventoria',
                'Iva provintegral', 'Iva eca', 'Financiar sin iva', 'Iva', 'Financiar con iva'],
            colModel: [
                {name: 'contratista', index: 'contratista', width: 60, sortable: true, align: 'center', hidden: false},
                {name: 'nombre_contratista', index: 'nombre_contratista', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'nomcli', index: 'nomcli', width: 180, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'id_accion', index: 'id_accion', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'estado', index: 'estado', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'descripcion_estado', index: 'descripcion_estado', width: 180, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'costo_contratista', index: 'costo_contratista', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'material', index: 'material', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'mano_obra', index: 'mano_obra', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'transporte', index: 'transporte', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'porc_administracion', index: 'porc_administracion', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'administracion', index: 'administracion', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'porc_imprevisto', index: 'porc_imprevisto', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'imprevisto', index: 'imprevisto', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'porc_utilidad', index: 'porc_utilidad', width: 95, sortable: true, align: 'center', hidden: true},
                {name: 'utilidad', index: 'utilidad', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'descripcion', index: 'descripcion', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'bonificacion', index: 'bonificacion', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'opav', index: 'opav', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'fintra', index: 'fintra', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interventoria', index: 'interventoria', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'provintegral', index: 'provintegral', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'eca', index: 'eca', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'base_iva_contratista', index: 'base_iva_contratista', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_contratista', index: 'iva_contratista', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_bonificacion', index: 'iva_bonificacion', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_opav', index: 'iva_opav', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_fintra', index: 'iva_fintra', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_interventoria', index: 'iva_interventoria', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_provintegral', index: 'iva_provintegral', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva_eca', index: 'iva_eca', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'financiar_sin_iva', index: 'financiar_sin_iva', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'iva', index: 'iva', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'financiar_con_iva', index: 'financiar_con_iva', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager2',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 15,
                    id_solicitud: id_solicitud
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager2", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_acciones").navButtonAdd('#pager2', {
            caption: "Exportar Excel",
            onClickButton: function () {
                var info = grid_tabla.getGridParam('records');
                if (info > 0) {
                    exportarExcelAcciones();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }

            }
        });
    }
}

function reloadGridAcciones(grid_tabla, op, id_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: id_solicitud
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function  mostrarDistribuciones() {
    tipoDistribuciones();
    $("#dialogMsjdistribuciones").dialog({
        width: '700',
        height: '480',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'DISTRIBUCIONES',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function tipoDistribuciones() {
    var grid_tabla = jQuery("#tabla_distribuciones");
    if ($("#gview_tabla_distribuciones").length) {
        reloadGridDistribuciones(grid_tabla, 16);
    } else {
        grid_tabla.jqGrid({
            //caption: "DISTRIBUCIONES",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '670',
            colNames: ['Distribucion', 'Tipo', '% Opav', '% Fintra', '% Interventoria', '% Provintegral', '% Eca', '% Iva', 'Valor agregado'],
            colModel: [
                {name: 'distribucion', index: 'distribucion', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'tipo', index: 'tipo', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_opav', index: 'porc_opav', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_fintra', index: 'porc_fintra', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_interventoria', index: 'porc_interventoria', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_provintegral', index: 'porc_provintegral', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_eca', index: 'porc_eca', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_iva', index: 'porc_iva', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'valor_agregado', index: 'valor_agregado', width: 60, sortable: true, align: 'center', hidden: false, search: true}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager3',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false

            },
            ajaxGridOptions: {
                data: {
                    opcion: 16
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager3", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridDistribuciones(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function  exportarExcelOfertas() {
    var fullData = jQuery("#tabla_ofertas").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            listado: myJsonString,
            opcion: 17
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function  exportarExcelAcciones() {
    var fullData = jQuery("#tabla_acciones").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx2").dialog(opt);
    $("#divSalidaEx2").dialog("open");
    $("#imgloadEx2").show();
    $("#msjEx2").show();
    $("#respEx2").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            listado: myJsonString,
            opcion: 18
        },
        success: function (resp) {
            $("#imgloadEx2").hide();
            $("#msjEx2").hide();
            $("#respEx2").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx2').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function   ventanaExcluirNumOs() {
    $("#dialogMsjexclusion").dialog({
        width: '613',
        height: '480',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'EXCLUSIONES',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Exlcuir/Incluir": function () {
                operacionExcluirIncluir();
            },
            "Salir": function () {
                jQuery("#tabla_busqueda").jqGrid("clearGridData", true);
                $(this).dialog("close");
                $("#numos").val("");
                $("#client").val("");
            }
        }
    });
}

function ExcluirNumOs(op) {
    var grid_tabla = jQuery("#tabla_busqueda");
    if ($("#gview_tabla_busqueda").length) {
        reloadGridExcluirNumOs(grid_tabla, op);
    } else {
        grid_tabla.jqGrid({
            //caption: "DISTRIBUCIONES",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '480',
            colNames: ['reg status', 'Codigo cliente', 'Nombre cliente', 'Id solicitud', 'Multiservicio'],
            colModel: [
                {name: 'estado', index: 'estado', width: 70, sortable: true, align: 'center', hidden: true, search: true},
                {name: 'id_cliente', index: 'id_cliente', width: 70, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'nomcli', index: 'nomcli', width: 190, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_solicitud', index: 'id_solicitud', width: 70, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'num_os', index: 'num_os', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager4',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                var ids = grid_tabla.jqGrid('getDataIDs');
                var allRowsInGrid = grid_tabla.jqGrid('getRowData');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                } else {
                    var excluidos = $("#excluidos").is(':checked');
                    if (excluidos === true) {
                        for (var i = 0; i < allRowsInGrid.length; i++) {
                            var cl = ids[i];
                            var estado = grid_tabla.getRowData(cl).estado;
                            if (estado === '') {
                                // $("#tabla_busqueda").jqGrid('setRowData', ids[i], false, {weightfont: 'bold', background: '#F5A9A9'});
                                grid_tabla.jqGrid('setCell', allRowsInGrid[i].num_os, "estado", "", {'background-color': '#F5A9A9', 'background-image': 'none'});
                                grid_tabla.jqGrid('setCell', allRowsInGrid[i].num_os, "id_cliente", "", {'background-color': '#F5A9A9', 'background-image': 'none'});
                                grid_tabla.jqGrid('setCell', allRowsInGrid[i].num_os, "nomcli", "", {'background-color': '#F5A9A9', 'background-image': 'none'});
                                grid_tabla.jqGrid('setCell', allRowsInGrid[i].num_os, "id_solicitud", "", {'background-color': '#F5A9A9', 'background-image': 'none'});
                                grid_tabla.jqGrid('setCell', allRowsInGrid[i].num_os, "num_os", "", {'background-color': '#F5A9A9', 'background-image': 'none'});
                            }
                        }
                    }
                }
            }, gridComplete: function (index) {
            },
            ajaxGridOptions: {
                data: {
                    opcion: op,
                    multiservicio: $("#numos").val(),
                    cliente: $("#client").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager4", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridExcluirNumOs(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                multiservicio: $("#numos").val(),
                cliente: $("#client").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function excluirIncluir(op) {
    var selRowIds = jQuery("#tabla_busqueda").jqGrid('getGridParam', 'selarrrow'), rowData;
    if (selRowIds.length > 0) {

        var arrayJson = new Array();
        for (var i = 0; i < selRowIds.length; i++) {
            rowData = $("#tabla_busqueda").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }

        console.log(JSON.stringify(arrayJson));

        $.ajax({
            async: false,
            url: "./controller?estado=Fintra&accion=Soporte",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: op,
                listJson: JSON.stringify(arrayJson)
            },
            success: function (json) {
                if (json.respuesta === 'GUARDADO') {
                    mensajesDelSistema("Exito al guardar ", '230', '150', true);
                    if (op === 24) {
                        ExcluirNumOs(23);
                    }
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("No hay datos seleccionados", '300', '150', true);
    }
}

function tipoOperacion() {
    var excluidos = $("#excluidos").is(':checked');
    if (excluidos === true) {
        ExcluirNumOs(23);
    } else {
        var multiservicio = $("#numos").val();
        var cliente = $("#client").val();
        if (multiservicio === "" && cliente === "") {
            mensajesDelSistema("Ingrese parametro de busqueda", '204', '140', false);
        } else {
            ExcluirNumOs(21);
        }
    }
}

function operacionExcluirIncluir() {
    var excluidos = $("#excluidos").is(':checked');
    if (excluidos === true) {
        excluirIncluir(24);
    } else {
        excluirIncluir(22);
    }
}

function  mostrarObservaciones(id_solicitud) {
    cargarObservaciones(id_solicitud);
    $("#dialogMsjObservacion").dialog({
        width: '1278',
        height: '470',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'DETALLE OFERTAS',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarObservaciones(id_solicitud) {
    var grid_tabla = jQuery("#tabla_observacion");
    if ($("#gview_tabla_observacion").length) {
        reloadGridObservaciones(grid_tabla, 79, id_solicitud);
    } else {
        grid_tabla.jqGrid({
            //     caption: "ACCIONES",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '1250',
            colNames: ['Id solicitud', 'Alcalce', 'Avance esperado','Avance registrado', 'Observaciones'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', width: 60, sortable: true, align: 'center', hidden: false},
                {name: 'alcalce', index: 'alcalce', width: 200, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'avance_esperado', index: 'avance_esperado', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'avance_registrado', index: 'avance_registrado', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'observaciones', index: 'observaciones', width: 700, sortable: true, align: 'left', hidden: false, search: true}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager5',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 79,
                    id_solicitud: id_solicitud
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager5", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridObservaciones(grid_tabla, op, id_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: id_solicitud
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}