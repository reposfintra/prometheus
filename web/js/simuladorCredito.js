/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
    
    $('.solo-numero').keyup(function() {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    
    $('#valor_negocio').live('blur',function (event) {
           this.value = numberConComas(this.value);            
    });   
    
    $("#fecha_liquidacion").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
//      minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
//      maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    var myDate = new Date();
    $("#fecha_liquidacion").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    cargarConveniosMicro();
    calcularFecha(myDate);
    
    $('#fecha_liquidacion').change(function(){        
        calcularFecha($('#fecha_liquidacion').val());
    });
    
      
    $("#btn_search").click(function() {      
        cargarInfoNegocioLiquidar();
    });  

    $("#btn_clear").click(function() {
       resetValues();
       DeshabilitarControles(false);       
       $("#tabla_simulador_Credito").jqGrid("clearGridData", true);
        $('#grid_liquidacion').fadeOut("fast");
    }); 
    
    $("#simular").click(function() {      
        calcularLiquidacion(1);
    });   
   
});

function cargarConveniosMicro() {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructuracion&accion=Negocios",
        dataType: 'json',
        data: {
            opcion: 16
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#convenio').append("<option value=''>Seleccione</option>");                  

                    for (var key in json) {
                        $('#convenio').append('<option value=' + key + '>' + json[key] + '</option>');                       
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function calcularFecha(fecha) {

    //var aux = replaceAll("2014-12-25", "-", "/");
    var date = new Date(fecha);
    var days = date.getDate();
    var fecha = "0099-01-01";
    var mes = date.getMonth() + 1;
    var anio = date.getFullYear();



    if (days >= 1 && days <= 2) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion3 = new Option(fecha, fecha);


        }

    }

    if (days > 2 && days <= 12) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-12";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion2 = new Option(fecha, fecha);
            if (mes === 11) {
                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion3 = new Option(fecha, fecha);
            } else {
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion3 = new Option(fecha, fecha);
            }

        }
    }

    if (days > 12 && days <= 17) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion3 = new Option(fecha, fecha);


        } else {
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion1 = new Option(fecha, fecha);

            if (mes === 11) {

                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion3 = new Option(fecha, fecha);

            } else {

                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion2 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
                opcion3 = new Option(fecha, fecha);
            }
        }

    }

    if (days > 17 && days <= 22) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-17";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion0 = new Option(fecha, fecha, "defauldSelected");

            if (mes === 11) {

                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion1 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-17";
                opcion3 = new Option(fecha, fecha);

            } else {

                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion1 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
                opcion2 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-17";
                opcion3 = new Option(fecha, fecha);
            }
        }

    }


    if (days > 22 && days <= 31) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-22";
            opcion3 = new Option(fecha, fecha);


        } else {

            if (mes === 11) {

                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion0 = new Option(fecha, fecha, "defauldSelected");
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion1 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-17";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-22";
                opcion3 = new Option(fecha, fecha);


            } else {

                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion0 = new Option(fecha, fecha, "defauldSelected");
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
                opcion1 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-17";
                opcion2 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-22";
                opcion3 = new Option(fecha, fecha);

            }

        }
    }


    document.formulario.primeracuota.options[0] = opcion0;
    document.formulario.primeracuota.options[1] = opcion1;
    document.formulario.primeracuota.options[2] = opcion2;
    document.formulario.primeracuota.options[3] = opcion3;


}


function calcularLiquidacion(op) {
  
    if ($("#tipo_cuota").val() !== '' && $("#cuota").val() !== ''
            && numberSinComas($("#valor_negocio").val()) !== '' && $("#fecha_liquidacion").val() !== '' && $("#primeracuota").val() !== '' 
            && $("#convenio").val() !== '') {

        $("#grid_liquidacion").show();

        if (numberSinComas($("#valor_negocio").val()) !== '0') {

           (op === 1) ? SimuladorCreditoMicro(): actualizarLiquidacionNegocio();
        } else {

            mensajesDelSistema("Valor Negocio debe ser mayor a cero.", 298, 150);
        }
    } else {
       
        mensajesDelSistema("Debe diligenciar todos los campos.", 298, 150);

    }

}

function SimuladorCreditoMicro() {
    if(($('#cod_negocio').is('[readonly]')===false)) {
        $('#cod_negocio').val('');
        $('#cod_negocio').attr({readonly: true});
    }
    var jsonParam;
    jsonParam = {opcion: 17, valor_negocio: numberSinComas($('#valor_negocio').val()), tipo_cuota:  $('#tipo_cuota').val(), num_cuotas: $('#cuota').val(), fecha_liquidacion:$('#fecha_liquidacion').val(), fecha_item: $('#primeracuota').val(), id_convenio:$('#convenio').val()};
    
    var grid_simulador_credito = jQuery("#tabla_simulador_Credito");

    if ($("#gview_tabla_simulador_Credito").length) {
        reloadGridSimuladorCredito(grid_simulador_credito, jsonParam);
    } else {

        grid_simulador_credito.jqGrid({
            caption: "Liquidacion Negocio",
            url: "./controller?estado=Reestructuracion&accion=Negocios",
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: '280',
            width: '995',
            colNames: ['Fecha', 'Cuota', 'Saldo Inicial', 'Valor Cuota', 'Capital', 'Interes', 'Capacitacion', 'Cat', 'Seguro', 'Saldo Final'],
            colModel: [
                {name: 'fecha', index: 'fecha', width: 80, align: 'center'},
                {name: 'item', index: 'item', sortable: true, width: 50, align: 'center', key: true},
                {name: 'saldo_inicial', index: 'saldo_inicial', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor', index: 'valor', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'capital', index: 'capital', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes', index: 'interes', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'capacitacion', index: 'capacitacion', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cat', index: 'cat', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'seguro', index: 'seguro', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'saldo_final', index: 'saldo_final', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager:'#page_simulador_Credito',
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                async:false,
                dataType: "json",
                type: "POST",
                data: jsonParam
            },
            loadComplete: function() {

                cacularTotalesSimuladorCredito(grid_simulador_credito);
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_simulador_Credito",{search:false,refresh:false,edit:false,add:false,del:false});
        grid_simulador_credito.jqGrid("navButtonAdd", "#page_simulador_Credito", {
            caption: "Exportar excel",         
            onClickButton: function() {
                var numRecords = jQuery('#tabla_simulador_Credito').getGridParam('records');
                if (numRecords > 0) {
                   exportarExcel(grid_simulador_credito);
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }
            }
        });
        
        grid_simulador_credito.jqGrid("navButtonAdd", "#page_simulador_Credito", {
            caption: "Aplicar Liquidacion",
            title: "Actualiza liquidacion  documentos operativos y facturas",
            onClickButton: function() {
                if ($('#cod_negocio').val() === '' || ($('#cod_negocio').is('[readonly]')===false)){
                    mensajesDelSistema("Por favor, ingrese el negocio a reliquidar", '350', '150');
                }else if ($('#aut_reliquidar_MC').val() === 'N'){
                    mensajesDelSistema("Usuario no autorizado para ejecutar la accion seleccionada", '250', '150');
                }else{
                    calcularLiquidacion(2);
                }
            }
        });          

    }


}

function  cacularTotalesSimuladorCredito(grid_simulador_credito) {

   
    var valor = grid_simulador_credito.jqGrid('getCol', 'valor', false, 'sum');
    var capital = grid_simulador_credito.jqGrid('getCol', 'capital', false, 'sum');
    var interes = grid_simulador_credito.jqGrid('getCol', 'interes', false, 'sum');
    var capacitacion = grid_simulador_credito.jqGrid('getCol', 'capacitacion', false, 'sum');
    var cat = grid_simulador_credito.jqGrid('getCol', 'cat', false, 'sum');
    var seguro = grid_simulador_credito.jqGrid('getCol', 'seguro', false, 'sum');
//    var saldo_final = grid_simulador_credito.jqGrid('getCol', 'saldo_final', false, 'sum');

    grid_simulador_credito.jqGrid('footerData', 'set', {
        cuota: 'Total:',
        valor: valor,
        capital: capital,
        interes: interes,
        capacitacion: capacitacion,
        cat: cat,
        seguro: seguro
    });

}


function  reloadGridSimuladorCredito(grid_simulador_credito, jsonParam) {
    grid_simulador_credito.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructuracion&accion=Negocios",
        datatype: 'json',
        ajaxGridOptions: {
            async: false,          
            type: "POST",
            data: jsonParam
        }
    });
    grid_simulador_credito.trigger("reloadGrid");
}

function exportarExcel(grid_simulador_credito) {
   
    var fullData = grid_simulador_credito.jqGrid('getRowData');
    var i;   
   
    if (fullData.length > 0) {
        
        var arrayJson = new Array();
        for (i = 0; i < fullData.length; i++) { 
           arrayJson.push(fullData[i]);
        }

        console.log(JSON.stringify(arrayJson));
        
        var opt = {
            autoOpen: false,
            modal: true,
            width: 220,
            height: 150,
            title: 'Descarga Reporte'
        };
  
        $("#divSalidaEx").dialog(opt);
        $("#divSalidaEx").dialog("open");
        $("#imgloadEx").show();
        $("#msjEx").show();
        $("#respEx").hide();
        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Reestructuracion&accion=Negocios",
                type: 'POST',
                dataType: "html",
                data: {
                    opcion: 18,
                    listado: JSON.stringify(arrayJson)
                },
                success: function (resp) {
                    $("#imgloadEx").hide();
                    $("#msjEx").hide();
                    $("#respEx").show();
                    var boton = "<div style='text-align:center'>\n\ </div>";                   
                    document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;


                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);

    }else{
        
        mensajesDelSistema("No hay datos para exportar", '300', '150', true);
    }
}

function cargarInfoNegocioLiquidar() {
    var negocio = $('#cod_negocio').val();  
    if (negocio === '') {
        mensajesDelSistema("Debe ingresar c�digo de negocio a liquidar", '300', '150', true);
    }else{
        resetValues();
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Reestructuracion&accion=Negocios",
            dataType: 'json',
            data: {
                opcion: 19,
                cod_neg: negocio.toUpperCase()
            },
            success: function(json) { 
                if (!isEmptyJSON(json)) {
                    $('#cod_negocio').val(negocio.toUpperCase());
                    $('#tipo_cuota').val(json.tipo_cuota);
                    $('#cuota').val(json.nro_docs);
                    $('#valor_negocio').val(numberConComas(json.vr_negocio));
                    $('#fecha_liquidacion').val(json.fecha_liquidacion);
                    $('#primeracuota').val(json.fecha_primera_cuota);
                    $('#convenio').val(json.id_convenio);
                    $('#aut_reliquidar_MC').val(json.aut_reliquidar);
                    DeshabilitarControles(true);
                }else{                 
                    mensajesDelSistema("Lo sentimos no se encontraron resultados para este c�digo de negocio!!", '300', '150');
                }    
            },
            error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }

}

function actualizarLiquidacionNegocio() {
    var url = './controller?estado=Reestructuracion&accion=Negocios';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        async:false,
        data: {
            opcion: 20,
            cod_neg:  $('#cod_negocio').val(), 
            tipo_cuota:  $('#tipo_cuota').val(), 
            num_cuotas: $('#cuota').val(),
            valor_negocio: numberSinComas($('#valor_negocio').val()),  
            fecha_liquidacion:$('#fecha_liquidacion').val(),
            fecha_item: $('#primeracuota').val(), 
            id_convenio:$('#convenio').val()
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }
                
                if (json.respuesta === "OK") {
                   mensajesDelSistema("Negocio Reliquidado satisfactoriamente", '250', '150', true); 
                }else{
                   mensajesDelSistema(json.respuesta, '350', '180'); 
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo actualizar liquidacion!!", '250', '150');
            }

        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function resetValues(){
    $('#cod_negocio').val('');
    $('#tipo_cuota').val('');
    $('#cuota').val('');
    $('#valor_negocio').val('0');
    $('#fecha_liquidacion').datepicker("setDate", new Date());; 
    $('#convenio').val('');
}


function DeshabilitarControles(estado){
        $('#cod_negocio').attr({readonly: estado});
        $('#tipo_cuota').attr({disabled: estado});
        $('#cuota').attr({readonly: estado});
        $('#valor_negocio').attr({readonly: estado});  
        (estado) ? $('#fecha_liquidacion').attr({readonly: estado}).datepicker("destroy") : $('#fecha_liquidacion').attr({readonly: estado}).datepicker();
        $('#primeracuota').attr({disabled: estado});
        $('#convenio').attr({disabled: estado});        
} 

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsg").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: "Mensaje",
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("close");             
            }
        }
    });
}