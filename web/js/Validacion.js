var nav4 = window.Event ? true : false;
function acceptNum(evt){	
   var key = nav4 ? evt.which : evt.keyCode;	
   return (key <= 13 || (key >= 48 && key <= 57));
}

function validar(){
   var d = document;
   var placa = d.frmplaca;
   var campos = new Array("placa", "marca",  "color", "modelo", "conductor", "propietario", "agencia");
   var sw = false;
   var i = 0;
   var campo = "";
     
   for (i = 0; i < campos.length; i++){
      campo = campos[i];
      if (placa.elements[campo].value == ""){
          alert("EL CAMPO " + placa.elements[campo].name + " ESTA VACIO!");
          placa.elements[campo].focus();
          sw = true;
          break;
      }
   }
   if (!sw)
      placa.submit();  
}

/*function validar(form){
            if (form.opcion.value=='llenarTabla'){
                if (form.id_usuario.value ==''){
                    alert('Defina el usuario para poder continuar...')
                    return false;
                }
            }
            return true;
      }*/
    function SelAll(){
            for(i=0;i<FormularioListado.length;i++)
                    FormularioListado.elements[i].checked=FormularioListado.All.checked;
    }
	
	function ActAll(){
            FormularioListado.All.checked = true;
            for(i=1;i<FormularioListado.length;i++)	
                    if (!FormularioListado.elements[i].checked){
                            FormularioListado.All.checked = false;
                            break;
                    }
    }
  function validarpais(){
	for (i = 0; i < formpais.elements.length; i++){
		if (formpais.elements[i].value == ""){
		  alert("NO SE PUEDE PROCESAR LA INFORMACION VERIFIQUE QUE TODOS LOS CAMPOS ESTEN LLENOS");
		  formpais.elements[i].focus();
		  return (false);
	  	}
	}
	return (true);
}

function validarestado(){
	for (i = 0; i < formestado.elements.length; i++){
		if (formestado.elements[i].value == ""){
		  alert("NO SE PUEDE PROCESAR LA INFORMACION VERIFIQUE QUE TODOS LOS CAMPOS ESTEN LLENOS");
		  formestado.elements[i].focus();
		  return (false);
	  	}
	}
	return (true);
}
function validarciudad(){
	for (i = 0; i < formciu.elements.length; i++){
		if (formciu.elements[i].value == ""){
		  alert("NO SE PUEDE PROCESAR LA INFORMACION VERIFIQUE QUE TODOS LOS CAMPOS ESTEN LLENOS");
		  formciu.elements[i].focus();
		  return (false);
	  	}
	}
	return (true);
}

function cargarestado( dir, selectObject ){
	var valor = selectObject.selectedIndex;
	window.location = dir + selectObject.options[valor].value;  	
}

function validarActividad(){
   var d = document; 
   var campos = new Array("codactividad", "descorta",  "deslarga");
   var checs = new Array("fecini", "fecfin", "duracion", "documento", "tiempodemora", "causademora", "resdemora", "observacion", "feccierre", "cantrealizada","cantplaneada","referencia1", "referencia2", "refnumerica1", "refnumerica2");
   var sw = false;
   var i = 0;
   var campo = "";
   var chec = "";
   var c = "corta";
   var l = "larga";
   var m = "m_";

   for (i = 0; i < campos.length; i++){
      campo = campos[i];
      if (forma.elements[campo].value == ""){
          alert("EL CAMPO " + forma.elements[campo].name + " ESTA VACIO!");
          forma.elements[campo].focus();
          sw = true;
          break;
      }
   }
   if(forma.linkimg.checked){
	   	forma.linkimg.value = 'S';
   }
   else{
   		forma.linkimg.value = 'N';
   }
  for (i = 0; i < checs.length; i++){
      chec = checs[i];
      if (forma.elements[chec].checked){
          forma.elements[chec].value = "S";
          if(forma.elements[m+chec].checked){
			forma.elements[m+chec].value = "S"; 
          } 
          if (forma.elements[c+chec].value == ""){
          	  alert("EL CAMPO NOMBRE ESTA VACIO!");
              forma.elements[c+chec].focus();
              sw = true;
              break;
          }
          if (forma.elements[l+chec].value == ""){
          	  alert("EL CAMPO DESCRIPCIÓN ESTA VACIO!");
              forma.elements[l+chec].focus();
              sw = true;
              break;
          }
      }
      else if (chec == "feccierre"){
          forma.elements[chec].value = "S";
	      if (forma.elements[c+chec].value == ""){
          	  alert("EL CAMPO NOMBRE ESTA VACIO!");
              forma.elements[c+chec].focus();
              sw = true;
              break;
          }
          if (forma.elements[l+chec].value == ""){
          	  alert("EL CAMPO DESCRIPCIÓN ESTA VACIO!");
              forma.elements[l+chec].focus();
              sw = true;
              break;
          }	
      }
      else {
		forma.elements[chec].value = "N";        
      } 
   }
   
   if (!sw)
      forma.submit();  
}


//Diogenes 17.11.05
function AdjuntarDocPlaca(CONTROLLER,estado,acti,tipo) {
    var pag="";	
	if(frmplaca.placa.value==""){
		alert("Debe Digitar la Placa");
		frmplaca.placa.focus();
	} else {
		window.open(CONTROLLER+'?estado=Imagen&accion=Control&documento='+frmplaca.placa.value.toUpperCase( )+'&actividad='+acti+'&tipoDocumento='+tipo, '','width=680,height=650,scrollbars=yes,resizable=yes,top=10,left=65,status=yes');
	}
}

function autorizaciones(CONTROLLER,BASEURL, tipo){
	if(!confirm ("Se necesita una autorizacion para hacer este cambio.\n Desea escribirla ahora?")){
		if(confirm ("Usted debe solicitar una clave de autorizacion.\n   Desea solicitarla ahora?")){
			if(tipo =='PLACA'){
			window.location= CONTROLLER+'?estado=Solicitar&accion=AutorizacionPlacaProp&placa='+frmplaca.placa.value+'&propietario='+frmplaca.propietario.value+'&vpropietario='+frmplaca.vprop.value+'&cmd='+tipo;
			}
			else{
				window.location= CONTROLLER+'?estado=Solicitar&accion=AutorizacionPlacaProp&placa='+frmplaca.placa.value+'&propietario='+frmplaca.propietario.value+'&cmd='+tipo;
				}
			//window.close();
		}
	}else{
		if(tipo =='PLACA'){
			frmplaca.action = BASEURL+"/placas/placaUpdate.jsp?clave=ok";
			frmplaca.submit();
		}
		else{
			frmplaca.action = BASEURL+"/placas/placaInsert.jsp?clave=ok";
			frmplaca.submit();
		}
		
	}
}

function validarReferencia(){
   var d = document;
   var refe = d.frmref;
   var numform = frmref.frm.value;
   var campos = '';
   if (numform==1)
       campos = new Array("nombre", "telefono",  "telciu", "contacto", "cargo");   
   else if (numform==2)
      campos = new Array("nombre", "telefono",  "telciu", "referencia", "relacion", "ciudaddir", "direccion");   
   else if (numform==3)
      campos = new Array("nombre", "telefono",  "telciu", "referencia", "relacion", "ciudaddir", "direccion", "nombreC", "telefonoC",  "telciuC", "referenciaC", "cargoC", "ciudaddirC", "direccionC");   
	   
   var i = 0;
   var campo = "";     
   for (i = 0; i < campos.length; i++){
      campo = campos[i];
	  if (refe.elements[campo].value == ""){
		  alert("EL CAMPO " + refe.elements[campo].name + " ESTA VACIO!");
		  refe.elements[campo].focus();       
		  return false;
		  break;
	  }
   }
   refe.submit();
}
//Modificado 28/01/06
function validarReferenciaUpdate(){
   var d = document;
   var refe = d.frmref;
   var numform = frmref.frm.value;
   var campos = '';
   if (numform==1)
       campos = new Array("nombreEA", "telefonoEA",  "telciuEA", "contactoEA", "cargoEA");   
   else if (numform==2)
      campos = new Array("nombreCO", "telefonoCO",  "telciuCO", "referenciaCO", "relacionCO", "ciudaddirCO", "direccionCO");   
   else if (numform==3)
      campos = new Array("nombrePR", "telefonoPR",  "telciuPR", "referenciaPR", "relacionPR", "ciudaddirPR", "direccionPR", "nombreCEC", "telefonoCEC",  "telciuCEC", "referenciaCEC", "cargoCEC", "ciudaddirCEC", "direccionCEC");   
	   
   var i = 0;
   var campo = "";     
   for (i = 0; i < campos.length; i++){
      campo = campos[i];
	  if (refe.elements[campo].value == ""){
		  alert("EL CAMPO " + refe.elements[campo].name + " ESTA VACIO!");
		  refe.elements[campo].focus();       
		  return false;
		  break;
	  }
   }
   refe.submit();
}
function validarPlacaEscolta(){
   var d = document; 
   var placa = d.frmplaca;
   var campos = new Array("placa", "marca",  "color", "modelo", "conductor", "propietario", "agencia");
   var sw = false;
   var i = 0;
   var campo = "";
     
   for (i = 0; i < campos.length; i++){
      campo = campos[i];
      if (placa.elements[campo].value == ""){
          alert("EL CAMPO " + placa.elements[campo].name + " ESTA VACIO!");
          placa.elements[campo].focus();
          sw = true;
          break;
      }
   }
   if (!sw)
      placa.submit();
}

function validarPlaca(CONTROLLER,BASEURL){
   var d = document;
   var placa = d.frmplaca;
   var fecha = placa.hoy1.value;
   var campos = new Array("placa", "marca",  "color", "modelo", "conductor", "propietario", "agencia","tenedor");
   var sw = false;
   var entrar = true;
   var i = 0;
   var campo = "";
   
//Enrique De Lavalle
   if(placa.uservetonit.value == 'S'){  
      //luigi......
	  if ((placa.veto.value == 'S') && (placa.causa.value == '')){
		alert("Debe seleccionar una Causa");
		sw = true;
      }
	  else if((placa.veto.value == 'S') && (placa.observacion.value == '')){
		alert("El campo Observacion Esta Vacio!");
		sw = true; 
	  }
   }

   for (i = 0; i < campos.length; i++){
      campo = campos[i];
      if (placa.elements[campo].value == ""){
          alert("EL CAMPO " + placa.elements[campo].name + " ESTA VACIO!");
          placa.elements[campo].focus();
          sw = true;
          break;
      }
   }	
  
  if((placa.venctarjetaoper.value!='') && (entrar)){
	 if(placa.venctarjetaoper.value <= fecha){
              alert("La fecha de vencimiento de la targeta de operacion debe ser mayor a la actual!");
              placa.venctarjetaoper.focus();
              sw = true;
              entrar = false;
	 }
   }
   if((placa.fecvenempresa.value!='')&& (entrar)){
             if(placa.fecvenempresa.value <= fecha){
                  alert("La fecha de vencimiento de la targeta empresarial debe ser mayor a la actual!");
                  placa.fecvenempresa.focus();
                  sw = true;
                  entrar = false;
             }
   }
   if((placa.fecvenreg.value!='')&& (entrar)){
               if(placa.fecvenreg.value <= fecha){
                      alert("La fecha de vencimiento del registro nacional de carga debe ser mayor a la actual!");
                      placa.fecvenreg.focus();
                      sw = true;
                      entrar = false;
               }
   }
   if((placa.fecvenandina.value!='')&& (entrar)){
               if(placa.fecvenandina.value <= fecha){
                          alert("La fecha de vencimiento de la poliza andina debe ser mayor a la actual!");
                          placa.fecvenandina.focus();
                          sw = true;
                          entrar = false;
               }
   }
   if((placa.fecvenhabil.value!='')&& (entrar)){
                         if(placa.fecvenhabil.value <= fecha){
                              alert("La fecha de vencimiento de la targeta de habilitacion debe ser mayor a la actual!");
                              placa.fecvenhabil.focus();
                              sw = true;
                              entrar = false;
                         }
   }
   if((placa.fecvenprop.value!='')&& (entrar)){
                             if(placa.fecvenprop.value <= fecha){
                                  alert("La fecha de vencimiento de la targeta de propiedad debe ser mayor a la actual!");
                                  placa.fecvenprop.focus();
                                  sw = true;
                                  entrar = false;
                             }
   }
   if((placa.venseguroobliga.value!='')&& (entrar)){
                                 if(placa.venseguroobliga.value <= fecha){
                                      alert("La fecha de vencimiento del seguro obligatorio debe ser mayor a la actual!");
                                      placa.venseguroobliga.focus();
                                      sw = true;
                                      entrar = false;
                                 }
   }
  if((placa.fecvengases.value!='')&& (entrar)){
                                     if(placa.fecvengases.value <= fecha){
                                          alert("La fecha de vencimiento del certificado de expedicion de gases debe ser mayor a la actual!");
                                          placa.fecvengases.focus();
                                          sw = true;
                                          entrar = false;
                                     }
  }

  if(!sw){
	placa.action = CONTROLLER+"?estado=Placa&accion=Update&cmd=show";
	placa.submit();
 	document.frmplaca.mod.src = BASEURL+"/images/botones/aceptarDisable.gif";
	document.mod.disabled = true;		
  }
  
}

//Modificado por DBAstidas 19.01.06
//luigi 16 marzo 2007
function validarPlacaInsert(CONTROLLER,BASEURL){
	var d = document; 
   var placa = d.frmplaca;
   var campos = new Array("placa", "nomotor", "modelo", "grupoid", "agencia", "propietario", "agencia", "tenedor", "conductor", "noChasis", "certgases" );
   var sw = false;
   var entrar = true;
   var i = 0;
   var campo = "";
   var fecha = placa.hoy.value;
   var year =  fecha.substring(0,4);
   
   //Enrique De Lavalle
   if(placa.uservetonit.value == 'S'){       
	  if ((placa.veto.value == 'S') && (placa.causa.value == '')){
		alert("Debe seleccionar una Causa");
		sw = true;
      }
	  else if((placa.veto.value == 'S') && (placa.observacion.value == '')){
		alert("El campo Observacion Esta Vacio!");
		sw = true; 
	  }
   }

   for (i = 0; i < campos.length; i++){
      campo = campos[i];
      if(campo == "certgases"){
           if ((year - placa.elements["modelo"].value)> 2){
               if ((placa.elements[campo].value == "")||(placa.elements["fecvengases"].value == "")){
                  alert("Debe digitar la Informacion correspondiente al certificado de Medio Ambiente.");
                  placa.elements[campo].focus();
                  sw = true;
                  break;
               }
          }
      }else{
          if (placa.elements[campo].value == ""){
              alert("EL CAMPO " + placa.elements[campo].name + " ESTA VACIO!");
              placa.elements[campo].focus();
              sw = true;
              break;
          }
     }     
   }

   if((placa.venctarjetaoper.value!='')&&(entrar)){
	 if(placa.venctarjetaoper.value <= fecha){
              alert("La fecha de vencimiento de la targeta de operacion debe ser mayor a la actual!");
              placa.venctarjetaoper.focus();
              sw = true;
              entrar = false;
	 }
   }
   if((placa.venempresa.value!='')&&(entrar)){
             if(placa.venempresa.value <= fecha){
                  alert("La fecha de vencimiento de la targeta empresarial debe ser mayor a la actual!");
                  placa.venempresa.focus();
                  sw = true;
                  entrar = false;
             }
   }
   if((placa.venregistro.value!='')&&(entrar)){
               if(placa.venregistro.value <= fecha){
                      alert("La fecha de vencimiento del registro nacional de carga debe ser mayor a la actual!");
                      placa.venregistro.focus();
                      sw = true;
                      entrar = false;
               }
   }
   if((placa.venpolizaandina.value!='')&&(entrar)){
               if(placa.venpolizaandina.value <= fecha){
                          alert("La fecha de vencimiento de la poliza andina debe ser mayor a la actual!");
                          placa.venpolizaandina.focus();
                          sw = true;
                          entrar = false;
               }
   }
   if((placa.venhabilitacion.value!='')&&(entrar)){
                         if(placa.venhabilitacion.value <= fecha){
                              alert("La fecha de vencimiento de la targeta de habilitacion debe ser mayor a la actual!");
                              placa.venhabilitacion.focus();
                              sw = true;
                              entrar = false;
                         }
   }
   if((placa.venpropiedad.value!='')&&(entrar)){
                             if(placa.venpropiedad.value <= fecha){
                                  alert("La fecha de vencimiento de la targeta de propiedad debe ser mayor a la actual!");
                                  placa.venpropiedad.focus();
                                  sw = true;
                                  entrar = false;
                             }
   }
   if((placa.venseguroobliga.value!='')&&(entrar)){
                                 if(placa.venseguroobliga.value <= fecha){
                                      alert("La fecha de vencimiento del seguro obligatorio debe ser mayor a la actual!");
                                      placa.venseguroobliga.focus();
                                      sw = true;
                                      entrar = false;
                                 }
   }
  if((placa.fecvengases.value!='')&&(entrar)){
                                     if(placa.fecvengases.value <= fecha){
                                          alert("La fecha de vencimiento del certificado de expedicion de gases debe ser mayor a la actual!");
                                          placa.fecvengases.focus();
                                          sw = true;
                                          entrar = false;
                                     }
  }

   if (!sw){
   	placa.submit();
  	document.frmplaca.mod.src = BASEURL+"/images/botones/aceptarDisable.gif";
	document.mod.disabled = true;
   }
   
   
} 

//Modificado por Jose de la rosa 16.02.06
function validarPlacaTrailerInsert(CONTROLLER,BASEURL){
   var d = document; 
   var placa = d.frmplaca;
   var campos = new Array("placa","tipo","clase","modelo","norin","tarpropiedad","propietario","capacidad","volumen","ancho","largo","alto","tara","piso","dimcarroceria","noejes");
   var sw = false;
   var i = 0;
   var campo = "";
     
   //Enrique De Lavalle
   if(placa.uservetonit.value == 'S'){        
	  if ((placa.veto.value == 'S') && (placa.causa.value == '')){
		alert("Debe seleccionar una Causa");
		sw = true;
      }
	  else if((placa.veto.value == 'S') && (placa.observacion.value == '')){
		alert("El campo Observacion Esta Vacio!");
		sw = true; 
	  }
   }
   
   
   for (i = 0; i < campos.length; i++){
      campo = campos[i];
      if (placa.elements[campo].value == ""){
          alert("EL CAMPO " + placa.elements[campo].name + " ESTA VACIO!");
          placa.elements[campo].focus();
          sw = true;
          break;
      }
   }
   if (!sw){
   	placa.submit();
	document.frmplaca.mod.src = BASEURL+"/images/botones/aceptarDisable.gif";
	document.mod.disabled = true;
   }
}
//Modificado por Jose de la rosa 16.02.06
function validarPlacaTrailerUpdate(CONTROLLER,BASEURL){
   var d = document; 
   var placa = d.frmplaca;
   var campos = new Array("placa","tipo","clase","modelo","numero_rin","tarprop","propietario","capacidad","volumen","ancho","largo","alto","tara","piso","dimcarroceria","noejes");
   var sw = false;
   var i = 0;
   var campo = "";
     //Enrique De Lavalle
   if(placa.uservetonit.value == 'S'){   	     	     
	  if ((placa.veto.value == 'S') && (placa.causa.value == '')){
		alert("Debe seleccionar una Causa");
		sw = true;
      }
	 else if((placa.veto.value == 'S') && (placa.observacion.value == '')){
		alert("El campo Observacion Esta Vacio!");
		sw = true; 
	  }
   }
   for (i = 0; i < campos.length; i++){
      campo = campos[i];
      if (placa.elements[campo].value == ""){
          alert("EL CAMPO " + placa.elements[campo].name + " ESTA VACIO!");
          placa.elements[campo].focus();
          sw = true;
          break;
      }
   }
   if(!sw){
	placa.action = CONTROLLER+"?estado=Placa&accion=Update&cmd=show";
	placa.submit();
 	document.frmplaca.mod.src = BASEURL+"/images/botones/aceptarDisable.gif";
	document.mod.disabled = true;

  }
}

//operez 05-01-2006


//Modificado por Jose de la rosa 16.02.06
//luigi 20 marzo 2006
function validarPlacaCabezoteInsert(CONTROLLER,BASEURL){
   var d = document; 
   var placa = d.frmplaca;
   var fecha = placa.hoy1.value;
   var year =  fecha.substring(0,4);
   var entrar = true;
   var campos = new Array("placa","tipo","noChasis","marca","norin","color","clase","modelo","nomotor","capacidad","grupoid","noejes","agencia","tarpropiedad","registro","soat","venseguroobliga","certgases","fecvengases","empafiliada","tenedor","conductor","propietario");
   var sw = false;
   var i = 0;
   var campo = "";
   
   //Enrique De Lavalle
   if(placa.uservetonit.value == 'S'){        
	  if ((placa.veto.value == 'S') && (placa.causa.value == '')){
		alert("Debe seleccionar una Causa");
		sw = true;
      }
	  else if((placa.veto.value == 'S') && (placa.observacion.value == '')){
		alert("El campo Observacion Esta Vacio!");
		sw = true; 
	  }
   }

   for (i = 0; i < campos.length; i++){
      campo = campos[i];
      if(campo == "certgases"){
           if ((year - placa.elements["modelo"].value)> 2){
               if ((placa.elements[campo].value == "")||(placa.elements["fecvengases"].value == "")){
                  alert("Debe digitar la Informacion correspondiente al certificado de Medio Ambiente.");
                  placa.elements[campo].focus();
                  sw = true;
                  break;
               }
          }
      }else{
          if (placa.elements[campo].value == ""){
              alert("EL CAMPO " + placa.elements[campo].name + " ESTA VACIO!");
              placa.elements[campo].focus();
              sw = true;
              break;
          }
      }
   }

   if( placa.gps.value == 'SI' ){
        if( placa.operador_gps.value == '' ){
            alert("Debe seleccionar un operador GPS");
            sw = true;
        }
        
        if( placa.clave_seg.value == '' || placa.conf_clave.value == '' ){
            alert("Ingrese la clave y la confirmacion");
            sw = true;
        }else if( placa.clave_seg.value != placa.conf_clave.value ){
            alert("Verifique las claves, deben ser iguales");
            sw = true;
        }        
        
   }
  
   if((placa.venhabilitacion.value!='')&&(entrar)){
	 if(placa.venhabilitacion.value <= fecha){
              alert("La fecha de vencimiento de la targeta de operacion debe ser mayor a la actual!");
              placa.venhabilitacion.focus();
              sw = true;
              entrar = false;
	 }
   }
   if((placa.venempresa.value!='')&&(entrar)){
             if(placa.venempresa.value <= fecha){
                  alert("La fecha de vencimiento de la targeta empresarial debe ser mayor a la actual!");
                  placa.venempresa.focus();
                  sw = true;
                  entrar = false;
             }
   }
   if((placa.venregistro.value!='')&&(entrar)){
               if(placa.venregistro.value <= fecha){
                      alert("La fecha de vencimiento del registro nacional de carga debe ser mayor a la actual!");
                      placa.venregistro.focus();
                      sw = true;
                      entrar = false;
               }
   }
   if((placa.venseguroobliga.value!='')&&(entrar)){
               if(placa.venseguroobliga.value <= fecha){
                          alert("La fecha de vencimiento del seguro obligatorio debe ser mayor a la actual!");
                          placa.venseguroobliga.focus();
                          sw = true;
                          entrar = false;
               }
   }
   if((placa.venpolizaandina.value!='')&&(entrar)){
                         if(placa.venpolizaandina.value <= fecha){
                              alert("La fecha de vencimiento de la targeta de la poliza andina debe ser mayor a la actual!");
                              placa.venpolizaandina.focus();
                              sw = true;
                              entrar = false;
                         }
   }
   if((placa.fecvengases.value!='')&&(entrar)){
                             if(placa.fecvengases.value <= fecha){
                                  alert("La fecha de vencimiento del certificado de emision de gases debe ser mayor a la actual!");
                                  placa.fecvengases.focus();
                                  sw = true;
                                  entrar = false;
                             }
   }
   if((placa.venrcivil.value!='')&&(entrar)){
                                 if(placa.venrcivil.value <= fecha){
                                      alert("La fecha de vencimiento de la poliza de responsabilidad civil debe ser mayor a la actual!");
                                      placa.venrcivil.focus();
                                      sw = true;
                                      entrar = false;
                                 }
   }

   
   if (!sw){
   	placa.submit();
  	document.frmplaca.mod.src = BASEURL+"/images/botones/aceptarDisable.gif";
	document.mod.disabled = true;
   }
}

//Modificado por Jose de la rosa 16.02.06
function validarPlacaCabezoteUpdate(CONTROLLER,BASEURL){
   var d = document; 
   var placa = d.frmplaca;
   var fecha = placa.hoy1.value;
   var campos = new Array("placa","tipo","nochasis","numero_rin","clase","marca","modelo","nomotor","capacidad","noejes","agencia","tarprop","reg_nal_carga","polizasoat","venseguroobliga","certgases","fecvengases","empresaafil","tenedor","conductor","propietario");
   var sw = false;
   var entrar = true;
   var i = 0;
   var campo = "";
   
   //Enrique De Lavalle
   if(placa.uservetonit.value == 'S'){  
      //luigi......
	  if ((placa.veto.value == 'S') && (placa.causa.value == '')){
		alert("Debe seleccionar una Causa");
		sw = true;
      }
	  else if((placa.veto.value == 'S') && (placa.observacion.value == '')){
		alert("El campo Observacion Esta Vacio!");
		sw = true; 
	  }
   }
   
   for (i = 0; i < campos.length; i++){
      campo = campos[i];
      if (placa.elements[campo].value == ""){
          alert("EL CAMPO " + placa.elements[campo].name + " ESTA VACIO!");
          placa.elements[campo].focus();
          sw = true;
          break;
		  
      }
   }

   if( placa.gps.value == 'SI' ){
        if( placa.operador_gps.value == '' ){
            alert("Debe seleccionar un operador GPS");
            sw = true;
        }
        if( placa.cambio_clave.value == 'S' ){
            if( placa.actual_passwd.value == '' && tenia_gps == 'SI'){
                alert("Ingrese la clave Actual");
                sw = true;
            }
            else if( placa.clave_seg.value == '' || placa.conf_clave.value == '' ){
                alert("Ingrese la clave Nueva y la confirmacion");
                sw = true;
            }else if( placa.clave_seg.value != placa.conf_clave.value ){
                alert("Verifique la clave nueva y la confirmacion, deben ser iguales");
                sw = true;
            }        
        }
   }

   if((placa.fecvenhabil.value!='')&&(entrar)){
	 if(placa.fecvenhabil.value <= fecha){
              alert("La fecha de vencimiento de la targeta de operacion debe ser mayor a la actual!");
              placa.fecvenhabil.focus();
              sw = true;
              entrar = false;
	 }
   }
   if((placa.fecvenempresa.value!='')&&(entrar)){
             if(placa.fecvenempresa.value <= fecha){
                  alert("La fecha de vencimiento de la targeta empresarial debe ser mayor a la actual!");
                  placa.fecvenempresa.focus();
                  sw = true;
                  entrar = false;
             }
   }
   if((placa.fecvenreg.value!='')&&(entrar)){
               if(placa.fecvenreg.value <= fecha){
                      alert("La fecha de vencimiento del registro nacional de carga debe ser mayor a la actual!");
                      placa.fecvenreg.focus();
                      sw = true;
                      entrar = false;
               }
   }
   if((placa.venseguroobliga.value!='')&&(entrar)){
               if(placa.venseguroobliga.value <= fecha){
                          alert("La fecha de vencimiento del seguro obligatorio debe ser mayor a la actual!");
                          placa.venseguroobliga.focus();
                          sw = true;
                          entrar = false;
               }
   }
   if((placa.fecvenandina.value!='')&&(entrar)){
                         if(placa.fecvenandina.value <= fecha){
                              alert("La fecha de vencimiento de la targeta de la poliza andina debe ser mayor a la actual!");
                              placa.fecvenandina.focus();
                              sw = true;
                              entrar = false;
                         }
   }
   if((placa.fecvengases.value!='')&&(entrar)){
                             if(placa.fecvengases.value <= fecha){
                                  alert("La fecha de vencimiento del certificado de emision de gases debe ser mayor a la actual!");
                                  placa.fecvengases.focus();
                                  sw = true;
                                  entrar = false;
                             }
   }
   if((placa.fecvresp_civil.value!='')&&(entrar)){
                                 if(placa.fecvresp_civil.value <= fecha){
                                      alert("La fecha de vencimiento de la poliza de responsabilidad civil debe ser mayor a la actual!");
                                      placa.fecvresp_civil.focus();
                                      sw = true;
                                      entrar = false;
                                 }
   }
   
   if(!sw){
	placa.action = CONTROLLER+"?estado=Placa&accion=Update&cmd=show";
        placa.cambio_clave.disabled = false;
	placa.submit();
  	document.frmplaca.mod.src = BASEURL+"/images/botones/aceptarDisable.gif";
	document.mod.disabled = true;
  }
}

function activarDatosGps( tiene ){
   var d = document; 
   var f = d.frmplaca;
   tenia_gps = tiene;      

   if( tenia_gps == 'SI' ){
       if( f.gps.value == 'SI' ){     
           f.cambio_clave.disabled = false;
           f.operador_gps.disabled = false;
           f.clave_seg.disabled = false;
           f.conf_clave.disabled = false;  
           f.actual_passwd.disabled = false;
       }else{
           f.cambio_clave.disabled = true;
           f.operador_gps.disabled = true;
           f.clave_seg.disabled = true;
           f.conf_clave.disabled = true;
           f.actual_passwd.disabled = true;
       }
   }else{   
       if( f.gps.value == 'SI' ){     
           f.cambio_clave.value = "S";
           f.cambio_clave.disabled = true;
           f.operador_gps.disabled = false;
           f.clave_seg.disabled = false;
           f.conf_clave.disabled = false;
           f.actual_passwd.disabled = true;
           document.getElementById("fila_claves").style.display = 'block';
           document.getElementById("actual_passwd").style.backgroundColor="#CCCCCC";
       }else{
           f.cambio_clave.disabled = true;
           f.operador_gps.disabled = true;
           f.clave_seg.disabled = true;
           f.conf_clave.disabled = true; 
           f.actual_passwd.disabled = true;
           document.getElementById("actual_passwd").style.backgroundColor="#CCCCCC";
           document.getElementById("fila_claves").style.display = 'none';
       }
   }
}

function activarDatosGpsInsert(){

   var d = document;
   var f = d.frmplaca;

       if( f.gps.value == 'SI' ){
           f.operador_gps.disabled = false;
           f.clave_seg.disabled = false;
           f.conf_clave.disabled = false;             
       }else{           
           f.operador_gps.disabled = true;
           f.clave_seg.disabled = true;
           f.conf_clave.disabled = true;           
       }   
}

function enableClaves(){
   var d = document; 
   var f = d.frmplaca;

   if( f.gps.value == 'SI' ){       
       if( f.cambio_clave.value == 'S' ){       
           document.getElementById("fila_claves").style.display = 'block';
           f.actual_passwd.disabled = false;
           f.clave_seg.disabled = false;
           f.conf_clave.disabled = false;       
       }else{
           document.getElementById("fila_claves").style.display = 'none';
           f.actual_passwd.disabled = true;
           f.clave_seg.disabled = true;
           f.conf_clave.disabled = true; 
       }
   }
}


function cargar_pagina( sel , cont ){    
    document.frmplaca.action = cont+'?estado=Placa&accion=Update&cmd=cargar_pagina';        
    document.frmplaca.submit();
}

function resetear(){   
    var theForm = document.frmplaca;
    for(var i=0;i<theForm.length;i++){
        var ele = theForm.elements[i];
        if( ele.type!='hidden' )
            ele.value='';
    }
}

function enableFila(){
   var d = document; 
   var f = d.forma;
     
       if( f.veto.value == 'S' ){       
           document.getElementById("fila_razon").style.display = 'block';
           f.causa.disabled = false;
           f.observacion.disabled = false;      
       }else{
           document.getElementById("fila_razon").style.display = 'none';
           f.causa.disabled = true;
           f.observacion.disabled = true; 
       }
   
}

function enableFila2(){
   var d = document; 
   var f = d.frmplaca;
     
       if( f.veto.value == 'S' ){       
           document.getElementById("fila_razon").style.display = 'block';
           document.getElementById("fila_razon1").style.display = 'block';
           f.causa.disabled = false;
           f.fuente.disabled = false;
           f.observacion.disabled = false;      
       }else{
           document.getElementById("fila_razon").style.display = 'none';
           document.getElementById("fila_razon1").style.display = 'none';
           f.causa.disabled = true;
           f.observacion.disabled = true;
           f.fuente.disabled = true;  
       }
   
}



