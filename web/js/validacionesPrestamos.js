  var BASEURL    = '';
  var CONTROLLER = '';

   function filtrarCombo(data, cmb){
       data = data.toLowerCase();
       for (i=0;i<cmb.length;i++){
          var cod = cmb[i].value;
          var txt = cmb[i].text.toLowerCase( );
          if( txt.indexOf(data)==0 ){
             cmb[i].selected = true;
             break;
          }
       }
    } 

	
    function encontrarProveedor (){
       var url = CONTROLLER + '?estado=Consulta&accion=Prestamos&Opcion=ConsultaProveedores';
       var win = open (url,'consultaProveedores',' top=100,left=100, width=700, height=500, scrollbar=no, status=yes  ');
       win.focus();
    } 
	
    function retornarProveedor (item) {
       var cprov = window.opener.document.getElementById ('Beneficiario');
       var nprov = window.opener.document.getElementById ('NombreBeneficiario');
       if (cprov) cprov.value = item.value;
       if (nprov) nprov.value = item.text;
       window.close();      
    }
	
     function retornar (combo){
	var item =  null;
	for (var i = 0; i<combo.length; i++){
	   if (combo[i].selected){
	     item = combo[i];
             retornarProveedor(item);
	     break;
	   }   
	}
      }
      


      function newWindow (url, name){
        var win = open (url,name,' top=5,left=5, width='+ (screen.width-25) +', height='+ (screen.height-100) +', scrollbar=no, status=yes, resizable  ');
        win.focus();
      }

      function viewAmortizaciones (index){
         var url = CONTROLLER + '?estado=Consulta&accion=Prestamos&Opcion=Amortizacion&Index=' + index;
         var win = newWindow(url,'consultaAmortizacion');
      }


       function validarListaSeleccion (form){
          for (var i=0;i<form.elements.length; i++){
              if (form.elements[i].type=='checkbox' && form.elements[i].checked && !form.elements[i].disabled){
                   return true;
              }
          }
          return false;
       }

       function descargar (tipo){
         var url = CONTROLLER + '?estado=Exportacion&accion=Prestamo&tipo=' + tipo;
         var win = open (url,'descarga',' top=100,left=50, width=800,height=300, scrollbar=no, status=yes, resizable ');
         win.focus();
       }


       function validarFormularioAmortizacion (form){
          var fechaAnterior = form.fechaED;
          with (form){
             if (cuotas.value==''){
                 alert ('Defina el numero de cuotas');
                 return false;
             }
             if (parseFloat(cuotas.value) <= parseFloat(cuotasPR.value) )  {
                 alert ('Numero no valido para redefinir el numero de cuotas, este debe ser mayor que el numero de cuotas procesadas');
                 return false;
             } 
             for (var i=0;i<elements.length; i++){
                if (elements[i].type == 'text' && elements[i].name == 'fechas'){
                   if (fechaAnterior!=null ){
                      var fecha1 = parseFloat(fechaAnterior.value.replace(/-|:| /g,''));
                      var fecha2 = parseFloat(elements[i].value.replace  (/-|:| /g,''));
                      var cuota  = parseFloat(elements[i].id.replace('fec',''));
                      if (fecha2<fecha1){
                         alert('La fecha de la cuota '+ (cuota+1) +' ser mayor que la fecha de ' + (cuota==0?'entrega de dinero':'la cuota ' + cuota));
                         elements[i].focus();
                         return false;
                      }
                   }
                   fechaAnterior = elements[i];
                }
             }
          }
          if (confirm('Esta seguro de que desea modificar las fechas de los prestamos?')) {
             form.submit();
          }
       }
       
       function focus(elemento){
         elemento.focus();
       }




