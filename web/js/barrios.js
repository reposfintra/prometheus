/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    
        cargarDepartamentos("CO", "departamento");
        $('#departamento').val('ATL');
        cargarCiudad('ATL', "ciudad");
        $('#ciudad').val('BQ');
        cargarBarrios();
      
        $("#departamento").change(function() {
            var op = $(this).find("option:selected").val();
            $("#div_barrios").fadeOut();          
            cargarCiudad(op, "ciudad");     
        });

        $("#ciudad").change(function() {
            var op = $(this).find("option:selected").val();
            if (op === ''){              
                $("#div_barrios").fadeOut();              
            }else{
                cargarBarrios();
            }           
        });
   
});

function cargarDepartamentos(codigo, combo) {
  if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "/fintra/controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 13,
                cod_pais: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
//                      $('#'+combo).append("<option value=''>Seleccione</option>");                 

                        for (var key in json) {
                            $('#'+combo).append('<option value=' + key + '>' + json[key] + '</option>');                      
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
  }
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function cargarBarrios(){   
     $("#div_barrios").fadeIn();      
    if ($("#gview_tabla_barrios").length) {
        reloadGridBarrios();
    } else {              
            jQuery("#tabla_barrios").jqGrid({
                caption: 'Listado de Barrios',
                url: './controller?estado=GestionSolicitud&accion=Aval',
                datatype: 'json',
                height: 415,
                width: 'auto',
                colNames: ['Id', 'Nombre Barrio', 'Estado', 'Activar/Inactivar'],
                colModel: [
                    {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '80px', key: true},
                    {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '330px'},
                    {name: 'reg_status', index: 'reg_status', width: 90, sortable: true, align: 'center', hidden: true},
                    {name: 'cambio', index: 'cambio', width: 100, sortable: true, align: 'left', hidden: false}
                ],
                rowNum: 1000,
                rowTotal: 50000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                viewrecords: true,
                hidegrid: false,
                ignoreCase: true,
                pgtext: null,
                pgbuttons: false,               
                pager:'#page_tabla_barrios',
                jsonReader: {
                    root: 'rows',
                    repeatitems: false,
                    id: '0'
                },             
                ajaxGridOptions: {
                    type: "POST",
                    data: {
                        opcion: 'cargarGridBarrios',
                        ciu: $('#ciudad').val()
                    }
                },
                gridComplete: function (index) {
                     var cant = jQuery("#tabla_barrios").jqGrid('getDataIDs');
                     for (var i = 0; i < cant.length; i++) {
                         var cambioEstado = $("#tabla_barrios").getRowData(cant[i]).cambio;
                         var cl = cant[i];
                         be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoBarrio('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "' style='padding-left:9px;'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                         jQuery("#tabla_barrios").jqGrid('setRowData', cant[i], {cambio: be});
                     }
                 },
                ondblClickRow: function (rowid, iRow, iCol, e) { 
                    var grid_tabla = jQuery("#tabla_barrios");                   
                    var estado = grid_tabla.getRowData(rowid).reg_status;   
                    if (estado === '') {                        
                        editarBarrio(rowid);
                    } else {
                        mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                    }
                },
                loadError: function(xhr, status, error) {
                    mensajesDelSistema(error, 250, 150);
                }
            }).navGrid("#page_tabla_barrios",{edit:false,add:false,del:false});;         
            jQuery("#tabla_barrios").jqGrid("navButtonAdd", "#page_tabla_barrios", {
                caption: "Nuevo",
                title: "Agregar nuevo barrio",
                onClickButton: function() {
                    crearBarrio();
                }
            });                
       
    }
}

function reloadGridBarrios(){
    var url = './controller?estado=GestionSolicitud&accion=Aval';
    jQuery("#tabla_barrios").setGridParam({
        datatype: 'json',
        url: url,
         ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 'cargarGridBarrios',
                ciu: $('#ciudad').val()
            }
        }
    });
    
    jQuery('#tabla_barrios').trigger("reloadGrid");
}


function crearBarrio(){   
     $('#nombre').val(''); 
     $('#div_barrio').fadeIn("slow");
     AbrirDivCrearBarrio();
}

function AbrirDivCrearBarrio(){
      $("#div_barrio").dialog({
        width: 590,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Adicionar": function() {
                guardarBarrio();
            },
            "Salir": function() {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarBarrio(){    
    var nombre = $('#nombre').val(); 
    var url = './controller?estado=GestionSolicitud&accion=Aval';
    if(nombre!==''){
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 'guardarBarrio',
                    ciu:  $('#ciudad').val(),
                    nombre: nombre
                },
                success: function(json) {
                    
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }
                        
                        if (json.respuesta === "OK") {                            
                            mensajesDelSistema("Se cre� el barrio", '250', '150', true);                            
                            $('#nombre').val('');                           
                            reloadGridBarrios();                          
                        }                       
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear el barrio", '250', '150');
                    }                         
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }      
            });
    }else{
         mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');      
    }
}

function editarBarrio(cl){        
    var fila = jQuery("#tabla_barrios").getRowData(cl);  
    var nombre = fila['nombre'];     
    $('#idBarrio').val(cl);
    $('#nombreEdit').val(nombre);   
    $('#div_editar_barrio').fadeIn("slow");
    AbrirDivEditarBarrio();
}

function AbrirDivEditarBarrio(){
      $("#div_editar_barrio").dialog({
        width: 590,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'EDITAR BARRIO',
        closeOnEscape: false,
        buttons: {    
            "Guardar": function() {
                actualizarBarrio();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarBarrio(){   
    var nombre = $('#nombreEdit').val();
    var idBarrio = $('#idBarrio').val();
    var url = './controller?estado=GestionSolicitud&accion=Aval';
    if(nombre!==''){
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 'guardarBarrio',
                nombre: nombre,
                idBarrio: idBarrio
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        reloadGridBarrios(); 
                        $("#div_editar_barrio").dialog('close');                          
                        
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar el nombre del barrio!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }else{
         mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');      
    }
    
}
function CambiarEstadoBarrio(rowid) {
    var grid_tabla = jQuery("#tabla_barrios");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=GestionSolicitud&accion=Aval",
        data: {
            opcion: 'cambiarEstadoBarrio',
            idBarrio: id
        },
        success: function (data) {
            reloadGridBarrios();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function AbrirDivCargarBarrios(){
      $("#dialogUploadFile").dialog({
        width: 590,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CARGAR BARRIOS',
        closeOnEscape: false,
        buttons: {    
            "Guardar": function() {
                alert('Cargar archivo');
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function loading(msj, width, height) {
    
    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {  
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsg").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}