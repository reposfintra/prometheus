var controler = '';
var timpuestoselec = '1';
function Opciones(base, agencias){
 timpuestoselec = formulario.impuesto.value;
 var elemento = agencias.split(";");
 var impuesto = formulario.impuesto.value;
 var tabla = document.getElementById("tabla");

 switch(impuesto){
 //IVA
 case 'IVA' :
	var lon = tabla.rows.length;
	InicializarTabla(lon);
	if( tabla.rows.length == 1 ){
		var imp = document.getElementById("impuesto");
		var tp = document.getElementById("Tipo");
		imp.value = 'IVA';
		tp.value = 'IVA';
		for( var i = 0 ; i < 8; i++){
			var filaNueva = tabla.insertRow();
			filaNueva.className = "fila" ;
			var titulo = "";
			var idinput = "";
			switch(i){
				case 0 :
					titulo = "Codigo impuesto "; idinput = "codimpuesto";
				break;
				case 1 :
					titulo = "Concepto "; idinput = "concepto";
				break;
    			case 2 :
					titulo = "Fecha de vigencia "; idinput = "fechavigencia";
				break;
    			case 3 :
					titulo = "Descripcion "; idinput = "descripcion";
				break;
				case 4 :
					titulo = "Porcentaje IVA " ; idinput = "porcentaje1";
				break;
    			case 5 :
					titulo = "Porcentaje RIVA "; idinput = "porcentaje2";
				break;
    			case 6 :
					titulo = "Indicador Signo "; idinput = "signo";
				break;
			case 7 :
					titulo = "Cuenta contable "; idinput = "cod_cuenta_contable";
				break;
			}

			for( var j=0; j<2; j++ ){
				var td = filaNueva.insertCell(-1);
				var text = document.createTextNode(titulo);
				var input = document.createElement("input");
				var imgo = document.createElement("img");
				var textarea = document.createElement("textarea");
				var imgo = document.createElement("img");
				imgo.name="obligatorio";
				imgo.align="absmiddle";
				imgo.src = base+"/images/botones/iconos/obligatorio.gif";
				imgo.width="10";
				imgo.height="10";
				imgo.border="0";
				textarea.cols = 60;
				textarea.rows = 5;
				textarea.className = "textbox";

			    input.className = "textbox";
				input.name = idinput;
				input.id = idinput;
				var signo = document.createElement("select");
				signo.name = idinput;
				signo.id = idinput;
				signo.className = "textbox";
				if( i == 0 ){
					input.onkeyup = new Function("procesar();");
					input.maxLength = "6";
				}

				if( j == 0 ){
					td.appendChild(text);
				}
				if( j == 1 ){
					if( i == 2 ){
						var fechaactual = new Date();
						input.value = fechaactual.getFullYear()+"-12-31";
                                               	input.id = 'fechavigencia';
						//input.onkeypress = new Function("FormatNumber(this);");
						input.readOnly = "true";
					}
					if( i == 1 ){
						//input.onkeypress = new Function("soloTexto(event);");
						input.setAttribute("maxLength",40);
						input.size = 45;
						var input0 = document.createElement("input");
						input0.setAttribute("maxLength",20);
						input0.setAttribute("type", "hidden");
						input0.setAttribute("value", "");
			    		input0.className = "textbox";
						input0.name = "cod_cuenta_contable";
						input0.id = "cod_cuenta_contable";
					}
					if( i == 3 ){
						textarea.id = idinput;
						textarea.name = idinput;
					}
					if (i == 4 || i == 5){
					    input.maxLength = 4 ;
						input.onkeypress = new Function("soloDigitos(event,'decOK')");
					}

					if( i == 3 )
						td.appendChild(textarea);
					else if( i == 6 ){
						signo[0] = new Option("Aumenta", "1");
						signo[1] = new Option("Disminuye", "-1");
						td.appendChild(signo);
						td.appendChild(imgo);
					} else
				    	td.appendChild(input);
					if( i==1 )
//						td.appendChild(input0);

					if( i == 2 ){
						var a = document.createElement("a");
						var img = document.createElement("img");
						img.name="popcal";
						img.align="absmiddle";
						img.src = base+"/js/Calendario/cal.gif";
						img.width="16";
						img.height="16";
						img.border="0";
						img.alt="";
						a.appendChild(img);

						a.setAttribute("href", "javascript:void(0)");
						a.hideFocus = "true";
						//a.onclick = new Function("show_calendar('fechavigencia')");
						a.onclick = new Function("if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechavigencia);return false;");
						td.appendChild(a);
					}

					if( i!= 1 )
						td.appendChild(imgo);

				}

			}//fin for j
		}//fin for i
	}

	 break;

 case 'RICA' :
  	var lon = tabla.rows.length;
	InicializarTabla(lon);
	if( tabla.rows.length == 1 ){
		var imp = document.getElementById("impuesto");
		var tp = document.getElementById("Tipo");
		imp.value = 'RICA';
		tp.value = 'RICA';
		for( var i = 0 ; i < 8; i++){
			var filaNueva = tabla.insertRow();
			filaNueva.className = "fila" ;
			var titulo = "";
			var idinput = "";
			switch(i){
				case 0 :
					titulo = "Agencia "; idinput = "agencia";
				break;
				case 1 :
					titulo = "Codigo impuesto "; idinput = "codimpuesto";
				break;
				case 2 :
					titulo = "Concepto "; idinput = "concepto";
				break;
    			case 3 :
					titulo = "Descripcion "; idinput = "descripcion";
				break;
				case 4 :
					titulo = "Fecha de vigencia "; idinput = "fechavigencia";

				break;
    			case 5 :
					titulo = "Porcentaje RICA " ; idinput = "porcentaje1";

				break;
				case 6 :
					titulo = "Cuenta contable "; idinput = "cod_cuenta_contable";
				break;
    			case 7 :
					titulo = "Indicador Signo "; idinput = "signo";
				break;

			}

			for( var j=0; j<2; j++ ){
				var td = filaNueva.insertCell(-1);
				var text = document.createTextNode(titulo);
				var input = document.createElement("input");
				var combo = document.createElement("select");
				var textarea = document.createElement("textarea");
				var imgo = document.createElement("img");
				imgo.name="obligatorio";
				imgo.align="absmiddle";
				imgo.src = base+"/images/botones/iconos/obligatorio.gif";
				imgo.width="10";
				imgo.height="10";
				imgo.border="0";
				textarea.cols = 60;
				textarea.rows = 5;
				textarea.className = "textbox";
				var signo = document.createElement("select");
				signo.name = idinput;
				signo.id = idinput;
				signo.className = "textbox";
				if( i == 1 ){
					input.onkeyup = new Function("procesar();");
					input.maxLength = "6";
				}
				if( i == 2 ){
					//input.onkeypress = new Function("soloTexto(event)");
					input.setAttribute("maxLength",40);
					input.size = 45;
				}
				if( i == 3 ){
					textarea.id = idinput;
					textarea.name = idinput;
				}
				if( i == 0 ){
					combo.id = idinput;
					combo.name = idinput;
				}
				if( i == 5 ){
					input.onkeypress = new Function("soloDigitos(event,'decOK')");
					input.maxLength  = "8";
				}
				if( i == 6 ){
					input.maxLength = "20";
				}
				combo.className = "textbox";
			    input.className = "textbox";
				input.name = idinput;
				input.id = idinput;



				if( j == 0 ){
					td.appendChild(text);
				}
				if( j == 1 ){
					if( i == 3 )
						td.appendChild(textarea);
					else if( i == 7 ){
						signo[0] = new Option("Aumenta", "1");
						signo[1] = new Option("Disminuye", "-1");
						td.appendChild(signo);
						td.appendChild(imgo);
					} else if( i == 0 ){
				    	for( var k=0; k<elemento.length; k++ ){
							var aux = elemento[k].split(":");
							combo[k] = new Option("["+aux[0]+"] "+aux[1],aux[0]);

						}
						td.appendChild(combo);
						td.appendChild(imgo);
					} else{
						if( i == 4 ){
							var fechaactual = new Date();
							input.value = fechaactual.getFullYear()+"-12-31";input.id = 'fechavigencia';
							//input.onkeypress = new Function("FormatNumber(this);");
							input.readOnly = "true";
						}
						if( i == 5 )
							input.onkeypress = new Function("soloDigitos(event,'decOK')");


						td.appendChild(input);

						if( i == 4 ){
							var a = document.createElement("a");
							var img = document.createElement("img");
							img.name="popcal";
							img.align="absmiddle";
							//img.src = base+"/js/Calendario/calbtn.gif";
							img.src = base+"/js/Calendario/cal.gif";
							img.width="16";
							img.height="16";
							img.border="0";
							img.alt="";
							a.appendChild(img);
							a.setAttribute("href", "javascript:void(0)");
						    a.hideFocus = "true";
							//a.onclick = new Function("show_calendar('fechavigencia')");
							a.onclick = new Function("if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechavigencia);return false;");
							td.appendChild(a);
						}
						td.appendChild(imgo);
					}//FIN ELSE
				}//FIN J == 1
			}//fin for j
		}//fin for i
	}

 break;

 case 'RFTE' :
  	var lon = tabla.rows.length;
	InicializarTabla(lon);
	if( tabla.rows.length == 1 ){
		var imp = document.getElementById("impuesto");
		var tp = document.getElementById("Tipo");
		imp.value = 'RFTE';
		tp.value = 'RFTE';
		for( var i = 0 ; i < 7; i++){
			var filaNueva = tabla.insertRow();
			filaNueva.className = "fila" ;
			var titulo = "";
			var idinput = "";
			switch(i){
				case 0 :
					titulo = "Codigo impuesto "; idinput = "codimpuesto";
				break;
				case 1 :
					titulo = "Concepto "; idinput = "concepto";
				break;
				case 2 :
					titulo = "Descripcion "; idinput = "descripcion";
				break;
    			case 3 :
					titulo = "Fecha de vigencia "; idinput = "fechavigencia";
				break;
				case 4 :
					titulo = "Porcentaje RFTE " ; idinput = "porcentaje1";
				break;
    			case 5 :
					titulo = "Cuenta contable "; idinput = "cod_cuenta_contable";
				break;
    			case 6 :
					titulo = "Indicador Signo "; idinput = "signo";
				break;

			}//FIN SWITCH

			for( var j=0; j<2; j++ ){
				var td = filaNueva.insertCell(-1);
				var text = document.createTextNode(titulo);
				var input = document.createElement("input");
				var textarea = document.createElement("textarea");
				var imgo = document.createElement("img");
				imgo.name="obligatorio";
				imgo.align="absmiddle";
				imgo.src = base+"/images/botones/iconos/obligatorio.gif";
				imgo.width="10";
				imgo.height="10";
				imgo.border="0";
				textarea.cols = 60;
				textarea.rows = 5;
				textarea.className = "textbox";
				var signo = document.createElement("select");
				signo.name = idinput;
				signo.id = idinput;
				textarea.className = "textbox";
			    if( i == 0 ){
					input.onkeyup = new Function("procesar();");
					input.maxLength = "6";
				}
				if( i == 1 ){
					//input.onkeypress = new Function("soloTexto(event)");
					input.setAttribute("maxLength",40);
					input.size = 45;
				}

				if( i == 2 ){
					textarea.id = idinput;
					textarea.name = idinput;
				}

				if( i == 5 ){
					input.maxLength = "20";
				}

				if( i == 8 ){
					input.maxLength = "8";
				}

				input.className = "textbox";
				input.id = idinput;
				input.name = idinput;


				if( j == 0 ){
					td.appendChild(text);
				}
				if( j == 1 ){
					if( i == 2 )
						td.appendChild(textarea);
					else if( i == 6 ){
							signo[0] = new Option("Aumenta", "1");
							signo[1] = new Option("Disminuye", "-1");
							td.appendChild(signo);
							td.appendChild(imgo);
					} else{
						if( i == 3 ){
							var fechaactual = new Date();
							input.value = fechaactual.getFullYear()+"-12-31";input.id = 'fechavigencia';
							//input.onkeypress = new Function("FormatNumber(this);");
							input.readOnly = "true";
						}

						if( i == 4 )
							input.onkeypress = new Function("soloDigitos(event,'decOK')");
						td.appendChild(input);
						if( i == 3 ){
							var a = document.createElement("a");
							var img = document.createElement("img");
							img.name="popcal";
							img.align="absmiddle";
							//img.src = base+"/js/Calendario/calbtn.gif";
							img.src = base+"/js/Calendario/cal.gif";
							img.width="16";
							img.height="16";
							img.border="0";
							img.alt="";
							a.appendChild(img);
							a.setAttribute("href", "javascript:void(0)");
							a.hideFocus = "true";
							//a.onclick = new Function("show_calendar('fechavigencia')");
							a.onclick = new Function("if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechavigencia);return false;");
							td.appendChild(a);
						}//FIN I == 3
						if( i!= 1 )
							td.appendChild(imgo);
					}//FIN ELSE
				}//FIN J == 1
			}//fin for j
		}//fin for i
	}
 break;

 //Modificado Ing. Andr�s Maturana D.

 case 'RIVA' :
	var lon = tabla.rows.length;
	InicializarTabla(lon);
	if( tabla.rows.length == 1 ){
		var imp = document.getElementById("impuesto");
		var tp = document.getElementById("Tipo");
		imp.value = 'RIVA';
		tp.value = 'RIVA';
		for( var i = 0 ; i < 6; i++){
			var filaNueva = tabla.insertRow();
			filaNueva.className = "fila" ;
			var titulo = "";
			var idinput = "";
			switch(i){
				case 0 :
					titulo = "Codigo impuesto "; idinput = "codimpuesto";
				break;
				case 1 :
					titulo = "Concepto "; idinput = "concepto";
				break;
    			case 2 :
					titulo = "Fecha de vigencia "; idinput = "fechavigencia";
				break;
				case 3 :
					titulo = "Porcentaje RIVA " ; idinput = "porcentaje1";
				break;
				case 4 :
					titulo = "Cuenta contable RIVA "; idinput = "cod_cuenta_contable";
				break;
    			case 5 :
					titulo = "Indicador Signo "; idinput = "signo";
				break;
			}

			for( var j=0; j<2; j++ ){
				var td = filaNueva.insertCell(-1);
				var text = document.createTextNode(titulo);
				var input = document.createElement("input");
				var imgo = document.createElement("img");
				imgo.name="obligatorio";
				imgo.align="absmiddle";
				imgo.src = base+"/images/botones/iconos/obligatorio.gif";
				imgo.width="10";
				imgo.height="10";
				imgo.border="0";

			    input.className = "textbox";
				input.name = idinput;
				input.id = idinput;
				var signo = document.createElement("select");
				signo.name = idinput;
				signo.id = idinput;
				signo.className = "textbox";
				if( i == 0 ){
					input.onkeyup = new Function("procesar();");
					input.maxLength = "6";
				}

				if( j == 0 ){
					td.appendChild(text);
				}
				if( j == 1 ){

					if( i == 2 ){
						var fechaactual = new Date();
						input.value = fechaactual.getFullYear()+"-12-31";input.id = 'fechavigencia';
						//input.onkeypress = new Function("FormatNumber(this);");
						input.readOnly = "true";
					}
					if( i == 1 ){
						//input.onkeypress = new Function("soloTexto(event);");
						input.setAttribute("maxLength",40);
						input.size = 45;
					}
					if (i == 3 ){
					    input.maxLength = 4 ;
						input.onkeypress = new Function("soloDigitos(event,'decOK')");
					}
					if( i == 4 ){
						input.setAttribute("maxLength",20);
					}

					if( i == 5 ){
						signo[0] = new Option("Aumenta", "1");
						signo[1] = new Option("Disminuye", "-1");
						td.appendChild(signo);
						td.appendChild(imgo);
					} else td.appendChild(input);

					if( i == 2 ){
						var a = document.createElement("a");
						var img = document.createElement("img");
						img.name="popcal";
						img.align="absmiddle";
						img.src = base+"/js/Calendario/cal.gif";
						img.width="16";
						img.height="16";
						img.border="0";
						img.alt="";
						a.appendChild(img);

						a.setAttribute("href", "javascript:void(0)");
						a.hideFocus = "true";
						//a.onclick = new Function("show_calendar('fechavigencia')");
						a.onclick = new Function("if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechavigencia);return false;");
						td.appendChild(a);
					}
					if( i!= 1 )
						td.appendChild(imgo);

				}

			}//fin for j
		}//fin for i
	}

 break;
 //AMATURANA 12.03.2007
 case 'RFTECL' :
  	var lon = tabla.rows.length;
	InicializarTabla(lon);
	if( tabla.rows.length == 1 ){
		var imp = document.getElementById("impuesto");
		var tp = document.getElementById("Tipo");
		imp.value = 'RFTECL';
		tp.value = 'RFTECL';
		for( var i = 0 ; i < 7; i++){
			var filaNueva = tabla.insertRow();
			filaNueva.className = "fila" ;
			var titulo = "";
			var idinput = "";
			switch(i){
				case 0 :
					titulo = "Codigo impuesto "; idinput = "codimpuesto";
				break;
				case 1 :
					titulo = "Concepto "; idinput = "concepto";
				break;
				case 2 :
					titulo = "Descripcion "; idinput = "descripcion";
				break;
    			case 3 :
					titulo = "Fecha de vigencia "; idinput = "fechavigencia";
				break;
				case 4 :
					titulo = "Porcentaje RFTECL " ; idinput = "porcentaje1";
				break;
    			case 5 :
					titulo = "Cuenta contable "; idinput = "cod_cuenta_contable";
				break;
    			case 6 :
					titulo = "Indicador Signo "; idinput = "signo";
				break;

			}//FIN SWITCH

			for( var j=0; j<2; j++ ){
				var td = filaNueva.insertCell(-1);
				var text = document.createTextNode(titulo);
				var input = document.createElement("input");
				var textarea = document.createElement("textarea");
				var imgo = document.createElement("img");
				imgo.name="obligatorio";
				imgo.align="absmiddle";
				imgo.src = base+"/images/botones/iconos/obligatorio.gif";
				imgo.width="10";
				imgo.height="10";
				imgo.border="0";
				textarea.cols = 60;
				textarea.rows = 5;
				textarea.className = "textbox";
				var signo = document.createElement("select");
				signo.name = idinput;
				signo.id = idinput;
				textarea.className = "textbox";
			    if( i == 0 ){
					input.onkeyup = new Function("procesar();");
					input.maxLength = "6";
				}
				if( i == 1 ){
					//input.onkeypress = new Function("soloTexto(event)");
					input.setAttribute("maxLength",40);
					input.size = 45;
				}

				if( i == 2 ){
					textarea.id = idinput;
					textarea.name = idinput;
				}

				if( i == 5 ){
					input.maxLength = "20";
				}

				if( i == 8 ){
					input.maxLength = "8";
				}

				input.className = "textbox";
				input.id = idinput;
				input.name = idinput;


				if( j == 0 ){
					td.appendChild(text);
				}
				if( j == 1 ){
					if( i == 2 )
						td.appendChild(textarea);
					else if( i == 6 ){
						signo[0] = new Option("Aumenta", "1");
						signo[1] = new Option("Disminuye", "-1");
						td.appendChild(signo);
						td.appendChild(imgo);
					} else{
						if( i == 3 ){
							var fechaactual = new Date();
							input.value = fechaactual.getFullYear()+"-12-31";input.id = 'fechavigencia';
							//input.onkeypress = new Function("FormatNumber(this);");
							input.readOnly = "true";
						}

						if( i == 4 )
							input.onkeypress = new Function("soloDigitos(event,'decOK')");

						td.appendChild(input);

						if( i == 3 ){
							var a = document.createElement("a");
							var img = document.createElement("img");
							img.name="popcal";
							img.align="absmiddle";
							//img.src = base+"/js/Calendario/calbtn.gif";
							img.src = base+"/js/Calendario/cal.gif";
							img.width="16";
							img.height="16";
							img.border="0";
							img.alt="";
							a.appendChild(img);
							a.setAttribute("href", "javascript:void(0)");
						    a.hideFocus = "true";
							//a.onclick = new Function("show_calendar('fechavigencia')");
							a.onclick = new Function("if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechavigencia);return false;");
							td.appendChild(a);
						}//FIN I == 3
						if( i!= 1 )
							td.appendChild(imgo);
					}//FIN ELSE
				}//FIN J == 1
			}//fin for j
		}//fin for i
	}
 break;

 case 'RICACL' :
  	var lon = tabla.rows.length;
	InicializarTabla(lon);
	if( tabla.rows.length == 1 ){
		var imp = document.getElementById("impuesto");
		var tp = document.getElementById("Tipo");
		imp.value = 'RICACL';
		tp.value = 'RICACL';
		for( var i = 0 ; i < 8; i++){
			var filaNueva = tabla.insertRow();
			filaNueva.className = "fila" ;
			var titulo = "";
			var idinput = "";
			switch(i){
				case 0 :
					titulo = "Agencia "; idinput = "agencia";
				break;
				case 1 :
					titulo = "Codigo impuesto "; idinput = "codimpuesto";
				break;
				case 2 :
					titulo = "Concepto "; idinput = "concepto";
				break;
    			case 3 :
					titulo = "Descripcion "; idinput = "descripcion";
				break;
				case 4 :
					titulo = "Fecha de vigencia "; idinput = "fechavigencia";

				break;
    			case 5 :
					titulo = "Porcentaje RICACL " ; idinput = "porcentaje1";

				break;
				case 6 :
					titulo = "Cuenta contable "; idinput = "cod_cuenta_contable";
				break;
    			case 7 :
					titulo = "Indicador Signo "; idinput = "signo";
				break;

			}

			for( var j=0; j<2; j++ ){
				var td = filaNueva.insertCell(-1);
				var text = document.createTextNode(titulo);
				var input = document.createElement("input");
				var combo = document.createElement("select");
				var textarea = document.createElement("textarea");
				var imgo = document.createElement("img");
				imgo.name="obligatorio";
				imgo.align="absmiddle";
				imgo.src = base+"/images/botones/iconos/obligatorio.gif";
				imgo.width="10";
				imgo.height="10";
				imgo.border="0";
				textarea.cols = 60;
				textarea.rows = 5;
				textarea.className = "textbox";
				var signo = document.createElement("select");
				signo.name = idinput;
				signo.id = idinput;
				textarea.className = "textbox";
				if( i == 1 ){
					input.onkeyup = new Function("procesar();");
					input.maxLength = "6";
				}
				if( i == 2 ){
					//input.onkeypress = new Function("soloTexto(event)");
					input.setAttribute("maxLength",40);
					input.size = 45;
				}
				if( i == 3 ){
					textarea.id = idinput;
					textarea.name = idinput;
				}
				if( i == 0 ){
					combo.id = idinput;
					combo.name = idinput;
				}
				if( i == 5 ){
					input.onkeypress = new Function("soloDigitos(event,'decOK')");
					input.maxLength  = "8";
				}
				if( i == 6 ){
					input.maxLength = "20";
				}
				combo.className = "textbox";
			    input.className = "textbox";
				input.name = idinput;
				input.id = idinput;



				if( j == 0 ){
					td.appendChild(text);
				}
				if( j == 1 ){
					if( i == 3 )
						td.appendChild(textarea);
					else if( i == 7 ){
						signo[0] = new Option("Aumenta", "1");
						signo[1] = new Option("Disminuye", "-1");
						td.appendChild(signo);
						td.appendChild(imgo);
					} else if( i == 0 ){
				    	for( var k=0; k<elemento.length; k++ ){
							var aux = elemento[k].split(":");
							combo[k] = new Option("["+aux[0]+"] "+aux[1],aux[0]);

						}
						td.appendChild(combo);
						td.appendChild(imgo);
					}
					else{
						if( i == 4 ){
							var fechaactual = new Date();
							input.value = fechaactual.getFullYear()+"-12-31";input.id = 'fechavigencia';
							//input.onkeypress = new Function("FormatNumber(this);");
							input.readOnly = "true";
						}
						if( i == 5 )
							input.onkeypress = new Function("soloDigitos(event,'decOK')");


						td.appendChild(input);

						if( i == 4 ){
							var a = document.createElement("a");
							var img = document.createElement("img");
							img.name="popcal";
							img.align="absmiddle";
							//img.src = base+"/js/Calendario/calbtn.gif";
							img.src = base+"/js/Calendario/cal.gif";
							img.width="16";
							img.height="16";
							img.border="0";
							img.alt="";
							a.appendChild(img);
							a.setAttribute("href", "javascript:void(0)");
						    a.hideFocus = "true";
							//a.onclick = new Function("show_calendar('fechavigencia')");
							a.onclick = new Function("if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechavigencia);return false;");
							td.appendChild(a);
						}
						if ( i!= 2 )
							td.appendChild(imgo);
					}//FIN ELSE
				}//FIN J == 1
			}//fin for j
		}//fin for i
	}

 break;

 }//fin switch
}


function Opciones2(base, agencias, impuesto, tipo, codigo,descripcion,concepto,fechav, porcentaje1, porcentaje2, cuenta, agencia, sign){
 var elemento = agencias.split(";");
 //var impuesto = formulario.impuesto.value;
 var tabla = document.getElementById("tabla");
 switch(tipo){
 //IVA
 case 'IVA' :
	var lon = tabla.rows.length;
	InicializarTabla(lon);
	if( tabla.rows.length == 1 ){
		var imp = document.getElementById("impuesto");
		var tp = document.getElementById("Tipo");
		imp.value = 'IVA';
		tp.value = 'IVA';
		for( var i = 0 ; i < 8; i++){
			var filaNueva = tabla.insertRow();
			filaNueva.className = "fila" ;
			var titulo = "";
			var idinput = "";
			switch(i){
				case 0 :
					titulo = "Codigo impuesto "; idinput = "codimpuesto";
				break;
				case 1 :
					titulo = "Concepto "; idinput = "concepto";
				break;
    			case 2 :
					titulo = "Fecha de vigencia "; idinput = "fechavigencia";
				break;
    			case 3 :
					titulo = "Descripcion "; idinput = "descripcion";
				break;
				case 4 :
					titulo = "Porcentaje IVA " ; idinput = "porcentaje1";
				break;
    			case 5 :
					titulo = "Porcentaje RIVA "; idinput = "porcentaje2";
				break;
    			case 6 :
					titulo = "Indicador Signo "; idinput = "signo";
				break;
			case 7 :
					titulo = "Cuenta contable "; idinput = "cod_cuenta_contable";
				break;
			}

			for( var j=0; j<2; j++ ){
				var td = filaNueva.insertCell(-1);
				var text = document.createTextNode(titulo);
				var input = document.createElement("input");
				var imgo = document.createElement("img");
				var textarea = document.createElement("textarea");
				var imgo = document.createElement("img");
				imgo.name="obligatorio";
				imgo.align="absmiddle";
				imgo.src = base+"/images/botones/iconos/obligatorio.gif";
				imgo.width="10";
				imgo.height="10";
				imgo.border="0";
				textarea.cols = 60;
				textarea.rows = 5;
				textarea.className = "textbox";
			        input.className = idinput;
				input.name = idinput;
				input.id = idinput;
				var signo = document.createElement("select");
				signo.name = idinput;
				signo.id = idinput;
				signo.className = "textbox";

				if( i == 0 ){
					input.onkeyup = new Function("procesar();");
					input.maxLength = "6";
				}
				if( i == 1 ){
					//input.onkeypress = new Function("soloTexto(event)");
					input.setAttribute("maxLength",40);
					input.size = 45;
					var input0 = document.createElement("input");
					input0.setAttribute("maxLength",20);
					input0.setAttribute("type", "hidden");
					input0.setAttribute("value", "");
					input0.className = "textbox";
					input0.name = "cod_cuenta_contable";
					input0.id = "cod_cuenta_contable";
				}
				if( i == 3 ){
					textarea.id = idinput;
					textarea.name = idinput;
				}
				if( i == 4 || i == 5){
					input.onkeypress = new Function("soloDigitos(event,'decOK')");
					input.maxLength = 4 ;
				}


				if( j == 0 ){
					td.appendChild(text);
				}
				if( j == 1 ){
					if( i == 2 ){
						var fechaactual = new Date();
						input.value = fechaactual.getFullYear()+"-12-31";input.id = 'fechavigencia';
						//input.onkeypress = new Function("FormatNumber(this);");
						input.readOnly = "true";
					}

					if( i == 3 )
						td.appendChild(textarea);
					else if( i == 6 ){
						signo[0] = new Option("Aumenta", "1");
						signo[1] = new Option("Disminuye", "-1");
						td.appendChild(signo);
						td.appendChild(imgo);
					} else
					    td.appendChild(input);
					if( i==1 )
//						td.appendChild(input0);

					if( i == 2 ){
						var a = document.createElement("a");
						var img = document.createElement("img");
						img.name="popcal";
						img.align="absmiddle";
						//img.src = base+"/js/Calendario/calbtn.gif";
						img.src = base+"/js/Calendario/cal.gif";
						img.width="16";
						img.height="16";
						img.border="0";
						img.alt="";
						a.appendChild(img);

						a.setAttribute("href", "javascript:void(0)");
						a.hideFocus = "true";
						//a.onclick = new Function("show_calendar('fechavigencia')");
						a.onclick = new Function("if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechavigencia);return false;");
						td.appendChild(a);
					}

					if( i!= 1 )
						td.appendChild(imgo);

				}

			}//fin for j
		}//fin for i
	}

	 break;

 case 'RICA' :
  	var lon = tabla.rows.length;
	InicializarTabla(lon);
	if( tabla.rows.length == 1 ){
		var imp = document.getElementById("impuesto");
		var tp = document.getElementById("Tipo");
		imp.value = 'RICA';
		tp.value = 'RICA';
		for( var i = 0 ; i < 8; i++){
			var filaNueva = tabla.insertRow();
			filaNueva.className = "fila" ;
			var titulo = "";
			var idinput = "";
			switch(i){
				case 0 :
					titulo = "Agencia "; idinput = "agencia";
				break;
				case 1 :
					titulo = "Codigo impuesto "; idinput = "codimpuesto";
				break;
				case 2 :
					titulo = "Concepto "; idinput = "concepto";
				break;
    			case 3 :
					titulo = "Descripcion "; idinput = "descripcion";
				break;
				case 4 :
					titulo = "Fecha de vigencia "; idinput = "fechavigencia";

				break;
    			case 5 :
					titulo = "Porcentaje RICA " ; idinput = "porcentaje1";

				break;
				case 6 :
					titulo = "Cuenta contable "; idinput = "cod_cuenta_contable";
				break;
    			case 7 :
					titulo = "Indicador Signo "; idinput = "signo";
				break;

			}

			for( var j=0; j<2; j++ ){
				var td = filaNueva.insertCell(-1);
				var text = document.createTextNode(titulo);
				var input = document.createElement("input");
				var combo = document.createElement("select");
				var textarea = document.createElement("textarea");
				var imgo = document.createElement("img");
				imgo.name="obligatorio";
				imgo.align="absmiddle";
				imgo.src = base+"/images/botones/iconos/obligatorio.gif";
				imgo.width="10";
				imgo.height="10";
				imgo.border="0";
				textarea.cols = 60;
				textarea.rows = 5;
				textarea.className = "textbox";
				var signo = document.createElement("select");
				signo.name = idinput;
				signo.id = idinput;
				signo.className = "textbox";
				if( i == 1 ){
					input.onkeyup = new Function("procesar();");
					input.maxLength = "6";
				}
				if( i == 2 ){
					//input.onkeypress = new Function("soloTexto(event)");
					input.setAttribute("maxLength",40);
					input.size = 45;
				}
				if( i == 3 ){
					textarea.id = idinput;
					textarea.name = idinput;
				}
				if( i == 0 ){
					combo.id = idinput;
					combo.name = idinput;
				}
				if( i == 5 ){
					input.maxLength = "8";
					input.onkeypress = new Function("soloDigitos(event,'decOK')");
				}
				if( i == 6 ){
					input.maxLength = "20";
				}
				combo.className = "textbox";
			    input.className = "textbox";
				input.name = idinput;
				input.id = idinput;



				if( j == 0 ){
					td.appendChild(text);
				}
				if( j == 1 ){
					if( i == 3 )
						td.appendChild(textarea);
					else if( i == 7 ){
						signo[0] = new Option("Aumenta", "1");
						signo[1] = new Option("Disminuye", "-1");
						td.appendChild(signo);
						td.appendChild(imgo);
					} else if( i == 0 ){
				    	for( var k=0; k<elemento.length; k++ ){
							var aux = elemento[k].split(":");
							combo[k] = new Option("["+aux[0]+"] "+aux[1],aux[0]);

						}
						td.appendChild(combo);
						td.appendChild(imgo);
					}
					else{
						if( i == 4 ){
							var fechaactual = new Date();
							input.value = fechaactual.getFullYear()+"-12-31";input.id = 'fechavigencia';
							//input.onkeypress = new Function("FormatNumber(this);");
							input.readOnly = "true";
						}
						if( i == 5 )
							input.onkeypress = new Function("soloDigitos(event,'decOK')");


						td.appendChild(input);
						if( i == 4 ){
							var a = document.createElement("a");
							var img = document.createElement("img");
							img.name="popcal";
							img.align="absmiddle";
							//img.src = base+"/js/Calendario/calbtn.gif";
						    img.src = base+"/js/Calendario/cal.gif";
							img.width="16";
							img.height="16";
							img.border="0";
							img.alt="";
							a.appendChild(img);
							a.setAttribute("href", "javascript:void(0)");
						    a.hideFocus = "true";
							//a.onclick = new Function("show_calendar('fechavigencia')");
							a.onclick = new Function("if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechavigencia);return false;");
							td.appendChild(a);
						}
						td.appendChild(imgo);
					}//FIN ELSE
				}//FIN J == 1
			}//fin for j
		}//fin for i
	}

 break;
 case 'RFTE' :
  	var lon = tabla.rows.length;
	InicializarTabla(lon);
	if( tabla.rows.length == 1 ){
		var imp = document.getElementById("impuesto");
		var tp = document.getElementById("Tipo");
		imp.value = 'RFTE';
		tp.value = 'RFTE';
		for( var i = 0 ; i < 7; i++){
			var filaNueva = tabla.insertRow();
			filaNueva.className = "fila" ;
			var titulo = "";
			var idinput = "";
			switch(i){
				case 0 :
					titulo = "Codigo impuesto "; idinput = "codimpuesto";
				break;
				case 1 :
					titulo = "Concepto "; idinput = "concepto";
				break;
				case 2 :
					titulo = "Descripcion "; idinput = "descripcion";
				break;
    			case 3 :
					titulo = "Fecha de vigencia "; idinput = "fechavigencia";
				break;
				case 4 :
					titulo = "Porcentaje RFTE " ; idinput = "porcentaje1";
				break;
    			case 5 :
					titulo = "Cuenta contable "; idinput = "cod_cuenta_contable";
				break;
    			case 6 :
					titulo = "Indicador Signo "; idinput = "signo";
				break;

			}//FIN SWITCH

			for( var j=0; j<2; j++ ){
				var td = filaNueva.insertCell(-1);
				var text = document.createTextNode(titulo);
				var input = document.createElement("input");
				var textarea = document.createElement("textarea");
				var imgo = document.createElement("img");
				imgo.name="obligatorio";
				imgo.align="absmiddle";
				imgo.src = base+"/images/botones/iconos/obligatorio.gif";
				imgo.width="10";
				imgo.height="10";
				imgo.border="0";
				textarea.className = "textbox";
				textarea.cols = 60;
				textarea.rows = 5;
				var signo = document.createElement("select");
				signo.name = idinput;
				signo.id = idinput;
				signo.className = "textbox";
			    if( i == 0 ){
					input.onkeyup = new Function("procesar();");
					input.maxLength = "6";
				}
				if( i == 1 ){
					//input.onkeypress = new Function("soloTexto(event)");
					input.setAttribute("maxLength",40);
					input.size = 45;
				}
				if( i == 2 ){
					textarea.id = idinput;
					textarea.name = idinput;
				}
				if( i == 4 ){
					input.maxLength = "8";
					input.onkeypress = new Function("soloDigitos(event,'decOK')");
				}
				if( i == 5 ){
					input.maxLength = "20";
				}
				input.className = "textbox";
				input.id = idinput;
				input.name = idinput;


				if( j == 0 ){
					td.appendChild(text);
				}
				if( j == 1 ){
					if( i == 2 )
						td.appendChild(textarea);
					else if( i == 6 ){
							signo[0] = new Option("Aumenta", "1");
							signo[1] = new Option("Disminuye", "-1");
							td.appendChild(signo);
							td.appendChild(imgo);
					} else{
						if( i == 3 ){
							var fechaactual = new Date();
							input.value = fechaactual.getFullYear()+"-12-31";input.id = 'fechavigencia';
							//input.onkeypress = new Function("FormatNumber(this);");
							input.readOnly = "true";
						}


						td.appendChild(input);
						if( i == 3 ){
							var a = document.createElement("a");
							var img = document.createElement("img");
							img.name="popcal";
							img.align="absmiddle";
							//img.src = base+"/js/Calendario/calbtn.gif";
  						    img.src = base+"/js/Calendario/cal.gif";
							img.width="16";
							img.height="16";
							img.border="0";
							img.alt="";
							a.appendChild(img);
							a.setAttribute("href", "javascript:void(0)");
						    a.hideFocus = "true";
							//a.onclick = new Function("show_calendar('fechavigencia')");
							a.onclick = new Function("if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechavigencia);return false;");
							td.appendChild(a);
						}//FIN I == 3
						if( i!= 1 )
							td.appendChild(imgo);
					}//FIN ELSE
				}//FIN J == 1
			}//fin for j
		}//fin for i
	}
 break;

 //Modificado Ing. Andr�s Maturana D.

 case 'RIVA' :
	var lon = tabla.rows.length;
	InicializarTabla(lon);
	if( tabla.rows.length == 1 ){
		var imp = document.getElementById("impuesto");
		var tp = document.getElementById("Tipo");
		imp.value = 'RIVA';
		tp.value = 'RIVA';
		for( var i = 0 ; i < 6; i++){
			var filaNueva = tabla.insertRow();
			filaNueva.className = "fila" ;
			var titulo = "";
			var idinput = "";
			switch(i){
				case 0 :
					titulo = "Codigo impuesto "; idinput = "codimpuesto";
				break;
				case 1 :
					titulo = "Concepto "; idinput = "concepto";
				break;
    			case 2 :
					titulo = "Fecha de vigencia "; idinput = "fechavigencia";
				break;
				case 3 :
					titulo = "Porcentaje RIVA " ; idinput = "porcentaje1";
				break;
    			case 4 :
					titulo = "Cuenta contable "; idinput = "cod_cuenta_contable";
				break;
    			case 5 :
					titulo = "Indicador Signo "; idinput = "signo";
				break;
			}

			for( var j=0; j<2; j++ ){
				var td = filaNueva.insertCell(-1);
				var text = document.createTextNode(titulo);
				var input = document.createElement("input");
				var imgo = document.createElement("img");
				imgo.name="obligatorio";
				imgo.align="absmiddle";
				imgo.src = base+"/images/botones/iconos/obligatorio.gif";
				imgo.width="10";
				imgo.height="10";
				imgo.border="0";
			    input.className = "textbox";
				input.name = idinput;
				input.id = idinput;
				var signo = document.createElement("select");
				signo.name = idinput;
				signo.id = idinput;
				signo.className = "textbox";
				if( i == 0 ){
					input.onkeyup = new Function("procesar();");
					input.maxLength = "6";
				}
				if( i == 1 ){
					//input.onkeypress = new Function("soloTexto(event)");
					input.setAttribute("maxLength",40);
					input.size = 45;
				}
				if( i == 3 ){
					input.onkeypress = new Function("soloDigitos(event,'decOK')");
					input.maxLength = 4 ;
				}
				if( i == 4 ){
					input.maxLength = 20 ;
				}


				if( j == 0 ){
					td.appendChild(text);
				}
				if( j == 1 ){
					if( i == 2 ){
						var fechaactual = new Date();
						input.value = fechaactual.getFullYear()+"-12-31";input.id = 'fechavigencia';
						//input.onkeypress = new Function("FormatNumber(this);");
						input.readOnly = "true";
					}

					if( i == 5 ){
						signo[0] = new Option("Aumenta", "1");
						signo[1] = new Option("Disminuye", "-1");
						td.appendChild(signo);
						td.appendChild(imgo);
					} else td.appendChild(input);
					if( i == 2 ){
						var a = document.createElement("a");
						var img = document.createElement("img");
						img.name="popcal";
						img.align="absmiddle";
						//img.src = base+"/js/Calendario/calbtn.gif";
						img.src = base+"/js/Calendario/cal.gif";
						img.width="16";
						img.height="16";
						img.border="0";
						img.alt="";
						a.appendChild(img);

						a.setAttribute("href", "javascript:void(0)");
						a.hideFocus = "true";
						//a.onclick = new Function("show_calendar('fechavigencia')");
						a.onclick = new Function("if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechavigencia);return false;");
						td.appendChild(a);
					}

					if( i!= 1 )
						td.appendChild(imgo);

				}

			}//fin for j
		}//fin for i
	}

 break;

 case 'RFTECL' :
  	var lon = tabla.rows.length;
	InicializarTabla(lon);
	if( tabla.rows.length == 1 ){
		var imp = document.getElementById("impuesto");
		var tp = document.getElementById("Tipo");
		imp.value = 'RFTE';
		tp.value = 'RFTE';
		for( var i = 0 ; i < 7; i++){
			var filaNueva = tabla.insertRow();
			filaNueva.className = "fila" ;
			var titulo = "";
			var idinput = "";
			switch(i){
				case 0 :
					titulo = "Codigo impuesto "; idinput = "codimpuesto";
				break;
				case 1 :
					titulo = "Concepto "; idinput = "concepto";
				break;
				case 2 :
					titulo = "Descripcion "; idinput = "descripcion";
				break;
    			case 3 :
					titulo = "Fecha de vigencia "; idinput = "fechavigencia";
				break;
				case 4 :
					titulo = "Porcentaje RFTECL " ; idinput = "porcentaje1";
				break;
    			case 5 :
					titulo = "Cuenta contable "; idinput = "cod_cuenta_contable";
				break;
    			case 6 :
					titulo = "Indicador Signo "; idinput = "signo";
				break;

			}//FIN SWITCH

			for( var j=0; j<2; j++ ){
				var td = filaNueva.insertCell(-1);
				var text = document.createTextNode(titulo);
				var input = document.createElement("input");
				var textarea = document.createElement("textarea");
				var imgo = document.createElement("img");
				imgo.name="obligatorio";
				imgo.align="absmiddle";
				imgo.src = base+"/images/botones/iconos/obligatorio.gif";
				imgo.width="10";
				imgo.height="10";
				imgo.border="0";
				textarea.className = "textbox";
				textarea.cols = 60;
				textarea.rows = 5;
				var signo = document.createElement("select");
				signo.name = idinput;
				signo.id = idinput;
				signo.className = "textbox";
			    if( i == 0 ){
					input.onkeyup = new Function("procesar();");
					input.maxLength = "6";
				}
				if( i == 1 ){
					//input.onkeypress = new Function("soloTexto(event)");
					input.setAttribute("maxLength",40);
					input.size = 45;
				}
				if( i == 2 ){
					textarea.id = idinput;
					textarea.name = idinput;
				}
				if( i == 4 ){
					input.maxLength = "8";
					input.onkeypress = new Function("soloDigitos(event,'decOK')");
				}
				if( i == 5 ){
					input.maxLength = "20";
				}
				input.className = "textbox";
				input.id = idinput;
				input.name = idinput;


				if( j == 0 ){
					td.appendChild(text);
				}
				if( j == 1 ){
					if( i == 2 )
						td.appendChild(textarea);
					else if( i == 6 ){
						signo[0] = new Option("Aumenta", "1");
						signo[1] = new Option("Disminuye", "-1");
						td.appendChild(signo);
						td.appendChild(imgo);
					} else{
						if( i == 3 ){
							var fechaactual = new Date();
							input.value = fechaactual.getFullYear()+"-12-31";input.id = 'fechavigencia';
							//input.onkeypress = new Function("FormatNumber(this);");
							input.readOnly = "true";
						}


						td.appendChild(input);
						if( i == 3 ){
							var a = document.createElement("a");
							var img = document.createElement("img");
							img.name="popcal";
							img.align="absmiddle";
							//img.src = base+"/js/Calendario/calbtn.gif";
  						    img.src = base+"/js/Calendario/cal.gif";
							img.width="16";
							img.height="16";
							img.border="0";
							img.alt="";
							a.appendChild(img);
							a.setAttribute("href", "javascript:void(0)");
						    a.hideFocus = "true";
							//a.onclick = new Function("show_calendar('fechavigencia')");
							a.onclick = new Function("if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechavigencia);return false;");
							td.appendChild(a);
						}//FIN I == 3
						if( i!= 1 )
							td.appendChild(imgo);
					}//FIN ELSE
				}//FIN J == 1
			}//fin for j
		}//fin for i
	}
 break;

 case 'RICACL' :
  	var lon = tabla.rows.length;
	InicializarTabla(lon);
	if( tabla.rows.length == 1 ){
		var imp = document.getElementById("impuesto");
		var tp = document.getElementById("Tipo");
		imp.value = 'RICACL';
		tp.value = 'RICACL';
		for( var i = 0 ; i < 8; i++){
			var filaNueva = tabla.insertRow();
			filaNueva.className = "fila" ;
			var titulo = "";
			var idinput = "";
			switch(i){
				case 0 :
					titulo = "Agencia "; idinput = "agencia";
				break;
				case 1 :
					titulo = "Codigo impuesto "; idinput = "codimpuesto";
				break;
				case 2 :
					titulo = "Concepto "; idinput = "concepto";
				break;
    			case 3 :
					titulo = "Descripcion "; idinput = "descripcion";
				break;
				case 4 :
					titulo = "Fecha de vigencia "; idinput = "fechavigencia";

				break;
    			case 5 :
					titulo = "Porcentaje RICACL " ; idinput = "porcentaje1";

				break;
				case 6 :
					titulo = "Cuenta contable "; idinput = "cod_cuenta_contable";
				break;
    			case 7 :
					titulo = "Indicador Signo "; idinput = "signo";
				break;

			}

			for( var j=0; j<2; j++ ){
				var td = filaNueva.insertCell(-1);
				var text = document.createTextNode(titulo);
				var input = document.createElement("input");
				var combo = document.createElement("select");
				var textarea = document.createElement("textarea");
				var imgo = document.createElement("img");
				imgo.name="obligatorio";
				imgo.align="absmiddle";
				imgo.src = base+"/images/botones/iconos/obligatorio.gif";
				imgo.width="10";
				imgo.height="10";
				imgo.border="0";
				textarea.cols = 60;
				textarea.rows = 5;
				textarea.className = "textbox";
				var signo = document.createElement("select");
				signo.name = idinput;
				signo.id = idinput;
				signo.className = "textbox";
				if( i == 1 ){
					input.onkeyup = new Function("procesar();");
					input.maxLength = "6";
				}
				if( i == 2 ){
					//input.onkeypress = new Function("soloTexto(event)");
					input.setAttribute("maxLength",40);
					input.size = 45;
				}
				if( i == 3 ){
					textarea.id = idinput;
					textarea.name = idinput;
				}
				if( i == 0 ){
					combo.id = idinput;
					combo.name = idinput;
				}
				if( i == 5 ){
					input.maxLength = "8";
					input.onkeypress = new Function("soloDigitos(event,'decOK')");
				}
				if( i == 6 ){
					input.maxLength = "20";
				}
				combo.className = "textbox";
			    input.className = "textbox";
				input.name = idinput;
				input.id = idinput;



				if( j == 0 ){
					td.appendChild(text);
				}
				if( j == 1 ){
					if( i == 3 )
						td.appendChild(textarea);
					else if( i == 7 ){
						signo[0] = new Option("Aumenta", "1");
						signo[1] = new Option("Disminuye", "-1");
						td.appendChild(signo);
						td.appendChild(imgo);
					} else if( i == 0 ){
				    	for( var k=0; k<elemento.length; k++ ){
							var aux = elemento[k].split(":");
							combo[k] = new Option("["+aux[0]+"] "+aux[1],aux[0]);

						}
						td.appendChild(combo);
						td.appendChild(imgo);
					}
					else{
						if( i == 4 ){
							var fechaactual = new Date();
							input.value = fechaactual.getFullYear()+"-12-31";input.id = 'fechavigencia';
							//input.onkeypress = new Function("FormatNumber(this);");
							input.readOnly = "true";
						}
						if( i == 5 )
							input.onkeypress = new Function("soloDigitos(event,'decOK')");


						td.appendChild(input);
						if( i == 4 ){
							var a = document.createElement("a");
							var img = document.createElement("img");
							img.name="popcal";
							img.align="absmiddle";
							//img.src = base+"/js/Calendario/calbtn.gif";
						    img.src = base+"/js/Calendario/cal.gif";
							img.width="16";
							img.height="16";
							img.border="0";
							img.alt="";
							a.appendChild(img);
							a.setAttribute("href", "javascript:void(0)");
						    a.hideFocus = "true";
							//a.onclick = new Function("show_calendar('fechavigencia')");
							a.onclick = new Function("if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechavigencia);return false;");
							td.appendChild(a);
						}
						td.appendChild(imgo);
					}//FIN ELSE
				}//FIN J == 1
			}//fin for j
		}//fin for i
	}

 break;

 }//fin switch
if ( tipo != '' ){
 	if( tipo != 'RIVA' )
 		formulario.descripcion.value = descripcion;
 	if( tipo == 'IVA' )
 		formulario.porcentaje2.value = porcentaje2;
 	if( tipo == 'RICA' || tipo == 'RICACL' )
 		formulario.agencia.value = agencia;
	formulario.cod_cuenta_contable.value = cuenta;
	formulario.impuesto.value = tipo;
 	formulario.codimpuesto.value = codigo;
 	formulario.concepto.value = concepto;
	formulario.fechavigencia.value = fechav;
 	formulario.porcentaje1.value = porcentaje1;
	formulario.signo.value = sign;
 }

 capaCentral.style.visibility = 'visible';
}




function InicializarTabla(lon){
	if( tabla.rows.length > 0 ){
		for( var i = 1 ; i < lon ; i++){
			tabla.deleteRow(1);
		}
	}
}

function ValidarImpuesto(form){//alert('impuesto: ' + form.impuesto.value);
	if( form.impuesto.value != '1' ){
		if (form.Opcion.value=='Guardar' || form.Opcion.value=='Modificar'){
			if ( form.fechavigencia.value == '' ){
        		alert('Seleccione la fecha de vigencia para poder continuar...')
            	return false;
            }
    		if (form.codimpuesto.value ==''){
        		alert('Defina el codigo para poder continuar...')
				form.codimpuesto.focus();
            	return false;
            }
    	    if (form.codimpuesto.value ==''){
        	    alert('Defina el codigo para poder continuar...')
				form.codimpuesto.focus();
            	return false;
            }
            /*if (form.concepto.value =='' && form.impuesto.value == 'RICA'){//AMATURANA 03.03.2007
            	alert('Defina el concepto para poder continuar...')
				form.concepto.focus();
                return false;
            }*/
			if (form.cod_cuenta_contable.value == '' && form.impuesto.value != 'IVA'){
                alert('Defina el codigo cuenta contable para poder continuar...')
				form.cod_cuenta_contable.focus();
                return false;
            }
			if (form.porcentaje1.value ==''){
                alert('Defina el porcentaje1 para poder continuar...')
				form.porcentaje1.focus();
                return false;
            }
			if (form.codimpuesto.value.length > 6 ){
               	alert('El codigo solo puede tener 6 caracteres')
				form.codimpuesto.focus();
               	return false;
            }
            if (form.concepto.value.length > 40 ){
                alert('El concepto solo puede tener 30 caracteres')
				form.concepto.focus();
                return false;
            }
			if (form.cod_cuenta_contable.value.length > 20){
                alert('El codigo de cuenta contable solo puede tener 20 caracteres')
				form.cod_cuenta_contable.focus();
                return false;
            }
			if( form.porcentaje1.value.length > 8 ){
				alert('El porcentaje solo puede tener 7  digitos')
				form.porcentaje1.focus();
                return false;
            }
			if( form.porcentaje1.value.indexOf(".") == -1 ){
				if (form.porcentaje1.value.length > 3 ){
                	alert('El porcentaje solo puede tener 3 digitos entero')
					form.porcentaje1.focus();
                	return false;
            	}
			} else {
				var entero = form.porcentaje1.value.split(".")[0];
				var decimal = form.porcentaje1.value.split(".")[1];
				if ( entero.length > 3 ){
					alert('El porcentaje solo puede tener 3 digitos entero')
					form.porcentaje1.focus();
                    return false;
				}
				if( decimal.length > 4 ){
					alert('El porcentaje solo puede tener 4 digitos decimales')
					form.porcentaje1.focus();
                    return false;
				}
			}

			if( form.impuesto.value ==  "IVA" ){
				if (form.porcentaje2.value ==''){
                	alert('Defina el porcentaje RIVA para poder continuar...')
					form.porcentaje2.focus();
                	return false;
               	}

				if( form.porcentaje2.value.length > 8 ){
					alert('El porcentaje RIVA solo puede tener 8  digitos')
					form.porcentaje2.focus();
                    return false;
                }

				if( form.porcentaje2.value.indexOf(".") == -1 ){
					if (form.porcentaje2.value.length > 3 ){
                    	alert('El porcentaje RIVA solo puede tener 3 digitos entero')
						form.porcentaje2.focus();
                    	return false;
                	}
				} else {
					var entero = form.porcentaje2.value.split(".")[0];
					var decimal = form.porcentaje2.value.split(".")[1];
					if ( entero.length > 3 ){
						alert('El porcentaje RIVA solo puede tener 3 digitos entero')
						form.porcentaje2.focus();
                    	return false;
					}
					if( decimal.length > 4 ){
						alert('El porcentaje RIVA solo puede tener 4 digitos decimales')
						form.porcentaje2.focus();
                    	return false;
					}
				}
			}
     	}
	} else if ( form.impuesto.value == '1' && ( form.Opcion.value=='Guardar' || form.Opcion.value=='Modificar' ) ) {
		alert('Seleccione el tipo de impuesto')
		form.impuesto.focus();
        return false;
	}
 	return true;
}

function solo_numeros (field, evt) {
  var keyCode =
    document.layers ? evt.which :
    document.all ? event.keyCode :
    document.getElementById ? evt.keyCode : 0;

  var r = '';
  if((keyCode>=96)&&(keyCode<=105)) return true;
  if((keyCode>=48)&&(keyCode<=57)) return true;
  if((keyCode==8)||(keyCode==37)||(keyCode==39)||(keyCode==46)) return true;
  else return false;
}

var isIE = document.all?true:false;
var isNS = document.layers?true:false;
function soloDigitos(e,decReq) {
    var key = (isIE) ? window.event.keyCode : e.which;
    var obj = (isIE) ? event.srcElement : e.target;
    var isNum = (key > 47 && key < 58) ? true:false;
    var dotOK =  (decReq=='decOK' && key ==46 && obj.value.indexOf('.')==-1) ? true:false;
    window.event.keyCode = (!isNum && !dotOK && isIE) ? 0:key;
    e.which = (!isNum && !dotOK && isNS) ? 0:key;
    return (isNum || dotOK );
}

function soloTexto(e) {
	var key = (isIE) ? window.event.keyCode : e.which;
	var obj = (isIE) ? event.srcElement : e.target;
	var isNum = ((key > 0 && key < 32) || (key > 32 && key < 65) || (key > 90 &&  key < 97) || (key > 122)) ? true:false;
	window.event.keyCode = (isNum && isIE) ? 0:key;
	e.which = (!isNum && isNS) ? 0:key;
	return (isNum);
}

function Validar(){
	if( FormularioListado.TipoI.value == "RICA" ||  FormularioListado.TipoI.value == "RICACL" )//AMATURANA 13.03.2007
		FormularioListado.select2.disabled = false;
	else
		FormularioListado.select2.disabled = true;
}



function procesar (){
  if (window.event.keyCode==13) buscarImpuesto();
}

function buscarImpuesto(){
  var url    = controler+"?estado=Tipo_impuesto&accion=Manager&Opcion=Seleccionar2&impuesto="+formulario.impuesto.value+"&codimpuesto="+formulario.codimpuesto.value;
  window.location.href = url;
}

function formatoFecha(input){
	var pattern =/[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}/;
	if(pattern.test(document.forms['formulario'].elements[input.name].value)==false){
		alert ("Formato de fecha invalido");
		document.forms['formulario'].elements[input.name].focus();
		return (false);
	}
}

function FormatNumber(num, format, shortformat)
{
 if(format==null)
 {
   // Choose the default format you prefer for the number.
  //format = "#-(###) ###-#### ";  // Telephone w/ LD Prefix and Area Code
  format = "####-##-##";   // FECHAS
  //format = "###-###-####";   // Telephone w/ Area Code (dash seperated)
  //format = "###-##-####";   //Social Security Number
 }
//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
 if(shortformat==null)
 {
  // Choose the short format (without area code) you prefer.
  //If you do not want multiple formats, leave it as "".

  //var shortformat = "###-#### ";
  var shortformat = "";
 }

//---------------------------------------------------------------------------------------------------------------------
//----------------------------------------This code can be used to format any number. ---------------------------------
//----------------------------------------Simply change the format to a number format ---------------------------------
//---------------------------------------- you prefer. It will ignore all characters  ---------------------------------
//----------------------------------------  except the #, where it will replace with  ---------------------------------
//----------------------------------------               user input.                  ---------------------------------
//---------------------------------------------------------------------------------------------------------------------

 var validchars = "0123456789";
 var tempstring = "";
 var returnstring = "";
 var extension = "";
 var tempstringpointer = 0;
 var returnstringpointer = 0;
 count = 0;

 // Get the length so we can go through and remove all non-numeric characters
 var length = num.value.length;


 // We are only concerned with the format of the phone number - extensions can be left alone.
 if (length > format.length)
 {
  length = format.length;
 };

 // scroll through what the user has typed
 for (var x=0; x<length; x++)
 {
  if (validchars.indexOf(num.value.charAt(x))!=-1)
  {
  tempstring = tempstring + num.value.charAt(x);
  };
 };
 // We should now have just the #'s - extract the extension if needed
 if (num.value.length > format.length)
 {
  length = format.length;
  extension = num.value.substr(format.length, (num.value.length-format.length));
 };

 // if we have fewer characters than our short format, we'll default to the short version.
 for (x=0; x<shortformat.length;x++)
 {
  if (shortformat.substr(x, 1)=="#")
  {
   count++;
  };
 }
 if (tempstring.length <= count)
 {
  format = shortformat;
 };


 //Loop through the format string and insert the numbers where we find a # sign
 for (x=0; x<format.length;x++)
 {
  if (tempstringpointer <= tempstring.length)
  {
   if (format.substr(x, 1)=="#")
   {
    returnstring = returnstring + tempstring.substr(tempstringpointer, 1);
    tempstringpointer++;
   }else{
    returnstring = returnstring + format.substr(x, 1);
   }
  }

 }

 // We have gone through the entire format, let's add the extension back on.
  returnstring = returnstring + extension;

 //we're done - let's return our value to the field.
 num.value = returnstring;
}

function resetDate(){
	var date = document.getElementById("fechavigencia");

	if( date ){
		var fechaactual = new Date();
		date.value = fechaactual.getFullYear()+"-12-31";
		//alert('Se le asigo a la fecha: ' + date.value);
	}
}
