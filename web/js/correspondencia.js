/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function initCorrespondencia() {
    listarCorrespondencia();

}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}



function listarCorrespondencia() {
    var grid_tbl_correspodencia = jQuery("#tabla_correspondencia");
    //alert('entra aqui');
    console.log('entra aca');
    if ($("#gview_tabla_correspondencia").length) {

        refrescarGridCorrespodencia();
    } else {
        grid_tbl_correspodencia.jqGrid({
            caption: "CORRESPONDENCIA",
            url: "./controlleropav?estado=Maestro&accion=Proyecto",
            datatype: "json",
            height: '520',
            width: '600',
            cellEdit: true,
            colNames: ['Id', 'Codigo', 'Descripcion', 'Id procedencia', 'Procedencia', 'Estado', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, align: 'left', key: true, hidden: true},
                {name: 'codparam', index: 'codparam', width: 80, align: 'left'},
                {name: 'descripcion', index: 'descripcion', width: 200, align: 'left'},
                {name: 'id_procedencia', index: 'id_procedencia', width: 150, align: 'left', hidden: true},
                {name: 'procedencia', index: 'procedencia', width: 150, align: 'left'},
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden: true},
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_correspondencia'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 52

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_correspondencia").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_correspondencia").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoCorrespondencia('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_correspondencia").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_correspondencia"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;

                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                    editarCorrespondencia(id);
                }

            }
        }).navGrid("#page_tabla_correspondencia", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_correspondencia").jqGrid("navButtonAdd", "#page_tabla_correspondencia", {
            caption: "Nuevo",
            onClickButton: function () {
                crearCorrespodencia();
            }
        });
    }

}

function refrescarGridCorrespodencia() {
    jQuery("#tabla_correspondencia").setGridParam({
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 52

            }
        }
    });

    jQuery('#tabla_correspondencia').trigger("reloadGrid");
}

function crearCorrespodencia() {
    $('#div_correspodencia').fadeIn('slow');
    $('#id').val('');
    $('#codparam').val('');
    $('#descripcion').val('');
    $("#id_procedencia").val('');
    AbrirDivCrearCorrespondencia();
}

function AbrirDivCrearCorrespondencia() {
    cargarEquivalencias();
    $("#div_correspodencia").dialog({
        width: 'auto',
        height: 200,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR O EDITAR CORRESPONDENCIA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarCorrespodencia();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_correspodencia").parent().find(".ui-dialog-titlebar-close").hide();
}

function guardarCorrespodencia() {
    var descripcion = $("#descripcion").val();
    var id_procedencia = $("#id_procedencia").val();
    if (descripcion !== '' && id_procedencia !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controlleropav?estado=Maestro&accion=Proyecto",
            data: {
                opcion: 55,
                descripcion: descripcion,
                id_procedencia:$('#id_procedencia').val()
                //id_procedencia: id_procedencia
                               
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                    $("#div_correspodencia").dialog('close');
                    //$("#codparam").val('');
                    $("#descripcion").val('');
                    $("#id_procedencia").val('');
                }
                listarCorrespondencia();
            }, error: function (result) {
                alert('ERROR');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function editarCorrespondencia(cl){
    $('#div_correspodencia').fadeIn('slow');
    var fila = jQuery("#tabla_correspondencia").getRowData(cl);    
    //var id = fila ['id'];
    var descripcion = fila['descripcion'];
    var id_procedencia = fila['id_procedencia'];
    $('#id').val(cl);
    $('#descripcion').val(descripcion);
    cargarEquivalencias("id_procedencia");
    $('#id_procedencia').val(id_procedencia);
    AbrirDivEditarCorrespondencia();
}

function AbrirDivEditarCorrespondencia(){
    //cargarEquivalencias();
    $("#div_correspodencia").dialog({
        width: 'auto',
        height: 200,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'EDITAR CORRESPONDENCIA',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              actualizarCorrespondencia();
              
            },
            "Salir": function () { 
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_correspodencia").parent().find(".ui-dialog-titlebar-close").hide();
}


function actualizarCorrespondencia() {
    var id = $("id").val();
    var descripcion = $("#descripcion").val();
    var id_procedencia = $("#id_procedencia").val();
    if (descripcion !== '' && id_procedencia !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controlleropav?estado=Maestro&accion=Proyecto",
            data: {
                opcion: 56,
                id: $('#id').val(),
                descripcion: descripcion,
                id_procedencia:$('#id_procedencia').val()
                
                               
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#div_correspodencia").dialog('close');
                    //$("#codparam").val('');
                    $("#descripcion").val('');
                    $("#id_procedencia").val('');
                }
                listarCorrespondencia();
            }, error: function (result) {
                alert('ERROR');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function cargarEquivalencias() {

    $.ajax({
        type: 'POST',
        async: false,
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        dataType: 'json',
        data: {
            opcion: 53
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    
                    $('#id_procedencia').html('');
                    $('#id_procedencia').append("<option value=''>Seleccione</option>");
                    for (var key in json.rows) {
                        //alert(key);
                        $('#id_procedencia').append('<option value=' + json.rows[key].id + '>' + json.rows[key].descripcion + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function CambiarEstadoCorrespondencia(rowid) {
    var grid_tabla = jQuery("#tabla_correspondencia");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        data: {
            opcion: 54,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridCorrespodencia();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar el estado!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

