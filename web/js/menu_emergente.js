var display_url  = true;


function highlight() {
   if (event.srcElement.className == "menuitem" ) {
       event.srcElement.style.backgroundColor = "highlight";
       event.srcElement.style.color = "white";
       if (display_url) window.status = event.srcElement.url;
   }
}

function lowlight() {
  if (event.srcElement.className == "menuitem"  ) {
      event.srcElement.style.backgroundColor = "";
      event.srcElement.style.color = "#003399";
      window.status = "";
  }
}


function showMenu() {
  if (menu){
	  var rightedge = document.body.clientWidth-event.clientX;
	  var bottomedge = document.body.clientHeight-event.clientY;
	  if (rightedge < menu.offsetWidth)
		 menu.style.left = document.body.scrollLeft + event.clientX - menu.offsetWidth;
	  else
		 menu.style.left = document.body.scrollLeft + event.clientX;
	
	  if (bottomedge < menu.offsetHeight)
		 menu.style.top = document.body.scrollTop + event.clientY - menu.offsetHeight;
	  else
		 menu.style.top = document.body.scrollTop + event.clientY;
	  menu.style.visibility = "visible";
  }
  return false;
}

function jumpto(numrem,base,controller) {
  if (event.srcElement.className == "menuitem") {
	// mover from campos
    if (event.srcElement.getAttribute("opcion")=='documentos')
            abrirPag(base+"/docremesas/documentos.jsp?numrem="+numrem);			
    if (event.srcElement.getAttribute("opcion")=='destinatarios')
			abrirPag(controller+"?estado=RemesaSin&accion=Doc&numrem="+numrem+"&Opcion=Destinatarios");

		
  }
  hideMenu ();
}

function hideMenu() {
  menu.style.visibility = "hidden";
}



function hideMenuPrincipal (){
	hideMenu();
    return true;
}


function setPageBounds(left, top, width, height){
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}
//FUNCIONES DE REMITENTES Y DESTINATARIOS
//ABRIR VENTANA (REMITENTE DESTINATARIO)
function abrirPag(url, nombrePagina){
  //alert (url);
  var wdth = screen.width - screen.width * 0.5;
  var hght = screen.height - screen.height * 0.5;
  var lf = screen.width * 0.2;
  var tp = screen.height * 0.25;
  var options = "status=yes,scrollbars=yes,resizable=yes," +
                setPageBounds(lf, tp, wdth, hght);
  	var hWnd = window.open(url, nombrePagina, options);
 	 if ( (document.window != null) && (!hWnd.opener) )
    	hWnd.opener = document.window;
}


