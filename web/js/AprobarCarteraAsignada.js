/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    cargarComboUnidNegocio();
    cargarCboTerceros();
});


function cargarComboUnidNegocio(){
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 27
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
                $('#id_unidad_negocio').html('');
                $('#id_unidad_negocio').append('<option value="" >...</option>');
                for (var datos in json) {
                    $('#id_unidad_negocio').append('<option value="' + datos + '" >' + json[datos] + '</option>');
                }
           

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    
}

function cargarCboTerceros(){
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 36
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
                $('#id_recaudador_tercero').html('');
                $('#id_recaudador_tercero').append('<option value="" >...</option>');
                for (var datos in json) {
                    $('#id_recaudador_tercero').append('<option value="' + datos + '" >' + json[datos] + '</option>');
                }

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    
}

function cargarCarteraPorAprobar() {
    if ($("#id_unidad_negocio").val()!="" && $("#id_recaudador_tercero").val()!=""){
       
    var grid_tabla = $("#tabla_negocios");
    if ($("#gview_tabla_negocios").length) {
        reloadGridTabla(grid_tabla);
    }else {
        
        grid_tabla.jqGrid({
            caption: "Reporte",
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '500',
            autowidth: true,            
            colNames: ['Cantidad','Empresa Responsable','Periodo carga','Estado','Unidad Negocio','Agencia','Altura Mora','Lote','Saldo Actual','Saldo vencido','Interes Mora',
                       'Gac','Total A Pagar'],
            colModel: [
                {name: 'cantidad',        index: 'cantidad',        width: 90,sortable: true, align: 'center', hidden: false},
                {name: 'empresa_responsable',index: 'empresa_responsable',width: 180, sortable: true, align: 'center',   hidden: false},
                {name: 'periodo_carga',index: 'periodo_carga',width: 90, sortable: true, align: 'center',   hidden: false},
                {name: 'estado',       index: 'estado',       width: 90, sortable: true, align: 'center',   hidden: false},
                {name: 'unidad_negocio',       index: 'unidad_negocio',       width: 120, sortable: true, align: 'center',   hidden: false},
                {name: 'agencia',       index: 'agencia',       width: 90, sortable: true, align: 'center',   hidden: false},                
                {name: 'altura_mora_actual',       index: 'altura_mora_actual',       width: 110, sortable: true, align: 'center',   hidden: false},
                {name: 'lote',       index: 'lote',       width: 90, sortable: true, align: 'center',   hidden: true},
                {name: 'saldo_actual', index: 'saldo_actual', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}}, 
                {name: 'valor_saldo_vencido', index: 'valor_saldo_vencido', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}}, 
                {name: 'interes_mora', index: 'interes_mora', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'gac', index: 'gac', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'total_a_pagar', index: 'total_a_pagar', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}}
            ],
            rowNum: 100000,
            rowTotal: 100000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#pager',
            pgtext: null,
            pgbuttons: false,
            multiselect:true,           
            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
                
            },
            loadComplete: function () {
            },
            ajaxGridOptions: {
                
                data: {
                    opcion: 37,
                    id_unidad_negocio: $("#id_unidad_negocio").val(),            
                    id_entidad_recaudo: $("#id_recaudador_tercero").val()            

                }                
                
            },
            gridComplete: function() { 
                
                var saldo_actual = $("#tabla_negocios").jqGrid('getCol', 'saldo_actual', false, 'sum'); 
                var valor_saldo_vencido = $("#tabla_negocios").jqGrid('getCol', 'valor_saldo_vencido', false, 'sum'); 
                var interes_mora = $("#tabla_negocios").jqGrid('getCol', 'interes_mora', false, 'sum'); 
                var gac = $("#tabla_negocios").jqGrid('getCol', 'gac', false, 'sum'); 
                var total_a_pagar = $("#tabla_negocios").jqGrid('getCol', 'total_a_pagar', false, 'sum'); 
                $("#tabla_negocios").jqGrid('footerData', 'set', {saldo_actual: saldo_actual,
                    valor_saldo_vencido:valor_saldo_vencido,interes_mora:interes_mora,gac:gac,total_a_pagar:total_a_pagar});

            }, 
            
           
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
                
            }
        }).navGrid('#pager', {add: false, edit: false, del: false, search: false, refresh: false});
           
    }
     }else {
            mensajesDelSistema("Por favor seleccione todas las opciones",'250', '150');
            }
    
}

function reloadGridTabla(grid_tabla) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",
       
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 37,
                id_unidad_negocio: $("#id_unidad_negocio").val(),            
                id_entidad_recaudo: $("#id_recaudador_tercero").val()    
            }
        }
    });
    grid_tabla.trigger("reloadGrid");    
     
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                
                $(this).dialog("close");
            }
        }
    });

}


function aprobarAsignarCarteraTercero() {
        var jsondocumento = [];
        var filasId = $('#tabla_negocios').jqGrid('getGridParam', 'selarrrow');
        if (filasId.length>0) {
            for (var i = 0; i < filasId.length; i++) {
                var altura_mora_actual = $("#tabla_negocios").getRowData(filasId[i]).altura_mora_actual;
                var lote = $("#tabla_negocios").getRowData(filasId[i]).lote;
                var json = {};
                json ["altura_mora_actual"] = altura_mora_actual;
                json ["lote"] = lote;
                jsondocumento.push(json);
            }
            var datos = {};
            datos ["data"] = jsondocumento;

            loading("Espere un momento por favor...", "270", "140");

            $.ajax({
                type: 'POST',
                url: "./controller?estado=Negocios&accion=Fintra",
                dataType: 'json',
                data: {
                    opcion: 38,
                    id_accion: $("#id_accion").val(),
                    data: JSON.stringify(datos)
                },
                success: function (json) {

                    if (json.respuesta === "OK") {
                        let msj;
                        $("#dialogLoading").dialog('close');
                        cargarCarteraPorAprobar();
                        if ($("#id_accion").val() === 'A') {
                            msj = 'Aprobacion exitosa';
                        } else {
                            msj = 'Se rechaz� la solicitud';
                        }
                        mensajesDelSistema(msj, '250', '150');
                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("No se asigno la cartera", '250', '150');
                    }
                },
                error: function (xhr) {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Error al refinanciar " + "\n" +
                            xhr.responseText, '650', '250', true);
                }
            });
        } else {
                mensajesDelSistema("Debe seleccionar el(los) registro(s) a procesar!!", '250', '150');
        }
    
}


function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}