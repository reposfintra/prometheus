/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 $(document).ready(function() {
      $('#btn_close').click(function() {    
          window.close();
      });
      $('#correspondencias').click(function(){
          $('#equiv_variables').fadeIn("slow");
          $('#equiv_variables').draggable();
      });
      $('#close').click(function(){
          $('#equiv_variables').fadeOut('slow');
      });
        var posicion = $("#equiv_variables").offset();
        var margenSuperior = 15;
        $(window).scroll(function() {
            if ($(window).scrollTop() > posicion.top) {
                $("#equiv_variables").stop().animate({
                    marginTop: $(window).scrollTop() - posicion.top + margenSuperior
                });
            } else {
                $("#equiv_variables").stop().animate({
                    marginTop: 0
                });
            }
        });
 });
 
 function initConfigDemandaDoc(){
   //ventanaEquivalencias();  
    cargarTablaEquivalencias();
    $('#div_demanda_docs').fadeIn("slow"); 
    $("#tabs").tabs({
    select: function(event, ui) {
        $('.nicEdit-panelContain').parent().width('100%');
        $('.nicEdit-panelContain').parent().next().width('100%');
        $('.nicEdit-main').width('99%');
      }
    });  
    $('.nicEdit-panelContain').parent().width('100%');   
    $('.nicEdit-panelContain').parent().next().width('100%');
    $('.nicEdit-main').width('99%');
    cargarConfigIniDocs();
  
     $('.nicEdit-panelContain').parent().width('100%');
                                $('.nicEdit-panelContain').parent().next().width('100%');
                                $('.nicEdit-main').width('99%'); 
    $('#mas_hechos').click(function() {
        var totItems = document.getElementsByName('descr_hechos').length;   
        var indice = totItems + 1;
        addElement('divHechos',"neo_"+indice,"hechos","hecho_neo_"+indice,"");                               
    });     
   /* $('#menos_hechos').click(function() {       
        var lastId = $("div#divHechos textarea.editor:last-child").attr('id');
        nicEditors.findEditor(lastId).remove();      
        $('#'+lastId).remove();   
    });   */ 
    $('#mas_pretensiones').click(function() {
        var totItems = document.getElementsByName('descr_pretensiones').length; 
        var indice = totItems + 1;
        addElement('divPretensiones',"neo_"+indice,"pretensiones","pretension_neo_"+indice,"");          
    });     
   /* $('#menos_pretensiones').click(function() {
        var lastId =  $("div#divPretensiones textarea.editor:last-child" ).attr('id');
       // nicEditors.findEditor(lastId).remove();      
        $('#'+lastId).remove();         
    });  */ 
    $('#mas_medidas').click(function() {
        var totItems = document.getElementsByName('descr_medidas').length; 
        var indice = totItems + 1;
        addElement('divMedidas',"neo_"+indice,"medidas","medida_neo_"+indice,"");          
    });     
  /*$('#menos_medidas').click(function() {
        var lastId =  $("div#divMedidas textarea.editor:last-child" ).attr('id');
      //  nicEditors.findEditor(lastId).remove();      
        $('#'+lastId).remove();         
    }); */ 
    
    $('#btn_save_config').click(function() {      
        guardarConfigDocs();
    }); 
 }
 
/*function initConfigMedidaDoc(){
    cargarConfigIniDocs(2);
    $('#btn_save_config').click(function() {
        var header = new nicEditors.findEditor('header').getContent();
        var intro = new nicEditors.findEditor('intro').getContent();
        var conclusion = new nicEditors.findEditor('conclusion').getContent();
        var footer = new nicEditors.findEditor('footer').getContent();
        var footer_page = new nicEditors.findEditor('footerpage').getContent();   
        var jsonParams = {
           opcion: 31,
           tipo_doc: 2,
           header: header,
           intro: intro,
           conclusion: conclusion,
           footer: footer,
           footer_page: footer_page,
           aux1: '',
           aux2: '',
           aux3: '',
           aux4: '',
           aux5: ''
        };
        guardarConfigDocs(jsonParams);
    }); 
 }
 
 function initConfigPoderDoc(){
    cargarConfigIniDocs(3);  
    $('#btn_save_config').click(function(){
        var header = new nicEditors.findEditor('header').getContent();
        var intro = new nicEditors.findEditor('intro').getContent();
        var conclusion = new nicEditors.findEditor('conclusion').getContent();
        var footer = new nicEditors.findEditor('footer').getContent();
        var footer_page = new nicEditors.findEditor('footerpage').getContent();   
        var jsonParams = {
           opcion: 31,
           tipo_doc: 3,
           header: header,
           intro: intro,
           conclusion: conclusion,
           footer: footer,
           footer_page: footer_page,
           aux1: '',
           aux2: '',
           aux3: '',
           aux4: '',
           aux5: ''
        };
        guardarConfigDocs(jsonParams);
    });
 }*/
 
 function cargarConfigIniDocs () {
                
    $.ajax({
            type: "POST",
            dataType: "json",          
            data: {opcion:30},
            async: false,
            url: './controller?estado=Proceso&accion=Ejecutivo',
            success: function(jsonData) {
                    if (!isEmptyJSON(jsonData)) {                 
                 
                        for (var i=0; i < jsonData.docs.length; i++){   
                           
                            if (jsonData.docs[i].tipo_doc == 1){  
                                
                                $("#header_dem").val(jsonData.docs[i].header_info);
                                $("#intro_dem").val(jsonData.docs[i].initial_info);                            
                                $("#footer_dem").val(jsonData.docs[i].signing_info);                              
                                $("#fundamentos").val(jsonData.docs[i].aux_1);
                                $("#competencia").val(jsonData.docs[i].aux_2);
                                $("#prueba").val(jsonData.docs[i].aux_3);
                                $("#anexo").val(jsonData.docs[i].aux_4);
                                $("#notificacion").val(jsonData.docs[i].aux_5);  
                                removeTextEditors('divHechos');                              
                                for (var j=0;j < jsonData.docs[i].hechos.length;j++){    
                                    addElement('divHechos',jsonData.docs[i].hechos[j].id_det_doc,jsonData.docs[i].hechos[j].titulo,jsonData.docs[i].hechos[j].nombre,jsonData.docs[i].hechos[j].descripcion);
                                }
                                removeTextEditors('divPretensiones');                                                       
                                for (var k=0;k < jsonData.docs[i].pretensiones.length;k++){
                                    addElement('divPretensiones',jsonData.docs[i].pretensiones[k].id_det_doc,jsonData.docs[i].pretensiones[k].titulo,jsonData.docs[i].pretensiones[k].nombre,jsonData.docs[i].pretensiones[k].descripcion);
                                }
                            }else if (jsonData.docs[i].tipo_doc == 2){                             
                                $("#header_med").val(jsonData.docs[i].header_info);
                                $("#intro_med").val(jsonData.docs[i].initial_info);                               
                                $("#footer_med").val(jsonData.docs[i].signing_info);                                                           
                               
                                removeTextEditors('divMedidas'); 
                                for (var m=0;m < jsonData.docs[i].medidas.length;m++){
                                    addElement('divMedidas',jsonData.docs[i].medidas[m].id_det_doc,jsonData.docs[i].medidas[m].titulo,jsonData.docs[i].medidas[m].nombre,jsonData.docs[i].medidas[m].descripcion);
                                }                                     
                             
                            }else if (jsonData.docs[i].tipo_doc == 3){                                    
                                $("#header_pod").val(jsonData.docs[i].header_info);
                                $("#intro_pod").val(jsonData.docs[i].initial_info);                              
                                $("#footer_pod").val(jsonData.docs[i].signing_info);                             
                            }                               
                               
                        }      
                               
                    }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
    });
       
 }
 
 function guardarConfigDocs (){ 
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion:31,                  
                    listadoDocs:guardarConfigDocsToJSON
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {              
                            cargarConfigIniDocs();
                            mensajesDelSistema("Configuración guardada exitosamente", '250', '150', true);                     
                        }
                        
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo guardar configuración del documento!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
 }
 
 function guardarConfigDocsToJSON(){
        var jsonDocs = { "docs":[                               
                                {                                                                 
                                 "tipo_doc": 1,
                                 "header_info": new nicEditors.findEditor('header_dem').getContent(),
                                 "initial_info": new nicEditors.findEditor('intro_dem').getContent(),
                                 "footer_info": "",
                                 "signing_info": new nicEditors.findEditor('footer_dem').getContent(),
                                 "footer_page": "",
                                 "aux_1": new nicEditors.findEditor('fundamentos').getContent(),
                                 "aux_2": new nicEditors.findEditor('competencia').getContent(),
                                 "aux_3": new nicEditors.findEditor('prueba').getContent(),
                                 "aux_4": new nicEditors.findEditor('anexo').getContent(),
                                 "aux_5": new nicEditors.findEditor('notificacion').getContent(),
                                 "hechos":[],
                                 "pretensiones":[]
                               },{                                                            
                                 "tipo_doc": 2,
                                 "header_info": new nicEditors.findEditor('header_med').getContent(),
                                 "initial_info": new nicEditors.findEditor('intro_med').getContent(),
                                 "footer_info": "",
                                 "signing_info": new nicEditors.findEditor('footer_med').getContent(),
                                 "footer_page": "",
                                 "aux_1": "",
                                 "aux_2": "",
                                 "aux_3": "",
                                 "aux_4": "",
                                 "aux_5": "",
                                 "medidas":[]
                               },{                                                             
                                 "tipo_doc": 3,
                                 "header_info": new nicEditors.findEditor('header_pod').getContent(),
                                 "initial_info": new nicEditors.findEditor('intro_pod').getContent(),
                                 "footer_info": "",
                                 "signing_info": new nicEditors.findEditor('footer_pod').getContent(),
                                 "footer_page": "",
                                 "aux_1": "",
                                 "aux_2": "",
                                 "aux_3": "",
                                 "aux_4": "",
                                 "aux_5": ""                                 
                               }
                        ]}; 
                   
                        var h = document.getElementsByName('descr_hechos');                      
                        for (var i = 0; i < h.length; i++) {
                            var id = h[i].id.replace("hechos_","");
                            var item = {};
                            item ["id_det_doc"] = id;
                            item ["tipo"] = "H";
                            item ["titulo"] = "HECHOS";
                            item ["nombre"] = "hecho_"+(i+1);
                            item ["descripcion"] = new nicEditors.findEditor(h[i].id).getContent();            
                            jsonDocs.docs[0].hechos.push(item);
                        }
             
                        var p = document.getElementsByName('descr_pretensiones');                      
                        for (var i = 0; i < p.length; i++) {
                            var id = p[i].id.replace("pretensiones_","");
                            var item = {};                        
                            item ["id_det_doc"] = id;
                            item ["tipo"] = "P";
                            item ["titulo"] = "PRETENSIONES";
                            item ["nombre"] = "pretension_"+(i+1);
                            item ["descripcion"] = new nicEditors.findEditor(p[i].id).getContent();            
                            jsonDocs.docs[0].pretensiones.push(item);
                        }
                        
                        var m = document.getElementsByName('descr_medidas');                      
                        for (var i = 0; i < m.length; i++) {
                            var id = m[i].id.replace("medidas_","");
                            var item = {};                        
                            item ["id_det_doc"] = id;
                            item ["tipo"] = "M";
                            item ["titulo"] = "MEDIDAS";
                            item ["nombre"] = "medida_"+(i+1);
                            item ["descripcion"] = new nicEditors.findEditor(m[i].id).getContent();            
                            jsonDocs.docs[1].medidas.push(item);
                        }
        
     return JSON.stringify(jsonDocs); 
     
}

function cargarTablaEquivalencias() {   
        $('#tbl_equiv_variables').html('');
        var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=44';
        $.ajax({
            type: "POST",
            dataType: "json",   
            async: false,
            url: url, 
            success: function(jsonData) {
                        var json = jsonData.rows;
                        var i = 1;                       
                        $('#tbl_equiv_variables').append('<tr><th>#</th> <th>Cód. Parametro</th> <th>Descripcion</th> </tr>');
                        for (var key in json) {
                            $('#tbl_equiv_variables').append('<tr>');
                            $('#tbl_equiv_variables tr:last').append('<td align="center">' + i + '</td>');
                            $('#tbl_equiv_variables tr:last').append('<td align="center">' + json[key].codparam + '</td>');
                            $('#tbl_equiv_variables tr:last').append('<td align="left">' + json[key].descripcion + '</td>');                         
                            $('#tbl_equiv_variables').append('</tr>');                           
                            i++;
                        }       
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        }); 
        
}


function ventanaEquivalencias() {
    
    $("#equiv_variables").dialog({
        width: 350,
        height: 375,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: false,
        closeOnEscape: true, 
        buttons: {          
            "Salir": function () {          
		$(this).dialog("close");               
            }
        }
    });
}
 
 function addElement(divId,indice,titulo,nombre,value) {     
    var ni = document.getElementById(divId); 
    var e,b1,b2;
     
    b1 = document.createElement('img');     
    b1.id = 'del_'+nombre;  
    b1.onclick = function() {        
        var id = b1.id.replace("del_","");  
        $('#'+id+'+ br').remove();
        nicEditors.findEditor(id).remove();      
        $('#'+id).remove();   
        $('#'+b1.id).remove();   
        $('#cond_'+id).remove();  
      
        eliminarCondicionesEspeciales(id);
    };
    b1.alt = 'menos';
    b1.title = 'Eliminar';
    b1.src = '/fintra/images/botones/iconos/close22.png';
    b1.style = 'margin-left:0px;height:100%;vertical-align: middle;float:right';
    ni.appendChild(b1); 
    
    b2 = document.createElement('img');   
    b2.id = 'cond_'+nombre; 
    b2.onclick = function() {       
        if (indice.toString().substr(0,4)!=="neo_"){     
            var id = b2.id.replace("cond_","");
            $('#div_asignar_cond_especial').fadeIn("slow");
            AbrirDivAsignarCondicionEspecial(id);
            listarCondicionesEspeciales(id); 
        }      
      };
    b2.alt = 'condicion';
    b2.title = 'Ir a configuracón condiciones especiales';
    b2.src = '/fintra/images/botones/iconos/special_condition.png';
    b2.style = 'margin-left:0px;height:24px;width:24px;vertical-align: middle;float:right';
    ni.appendChild(b2);      
    
    (indice.toString().substr(0,4)==="neo_") ? $('#'+b2.id).attr("disabled", "disabled").addClass('ui-state-disabled'):  $('#'+b2.id).removeAttr("disabled").removeClass('ui-state-disabled');
      
    e = document.createElement('textarea');   
    e.id = nombre; 
    e.name = 'descr_'+titulo.toLowerCase();
    e.setAttribute('class', 'editor');     
    ni.appendChild(e);     
    
    new nicEditor({maxHeight : 100, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance(e.id); 
    nicEditors.findEditor(e.id).setContent(value);
    ni.appendChild(document.createElement('br'));
}

function removeTextEditors(divId) {
    $('#' + divId).find('textarea.editor').each(function() {
        var id = $(this).attr('id');
        nicEditors.findEditor(id).remove();
        $(this).remove();
    });
    $('#' + divId).find('> img, br').each(function() {       
        $(this).remove();
    });   
}

function AbrirDivAsignarCondicionEspecial(idItem){
      $("#div_asignar_cond_especial").dialog({
        width: 410,
        height: 290,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CONFIGURACIÓN DE CONDICIONES ESPECIALES',
        closeOnEscape: false,
        buttons: { 
            "Actualizar": function () {
             asignarCondicionesEspeciales(idItem);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function listarCondicionesEspeciales(idItem) {
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=66';
    $.ajax({
        type: "POST",
        dataType: "json",
        async: false,
        url: url,
        data: {
            opcion:66,
            id_item: idItem
        },
        success: function(jsonData) {            
            $('#div_asignar_cond_especial').html('');     
            for (var i = 0; i < jsonData.length; i++) {
                $('#div_asignar_cond_especial').append("<input type='checkbox' name='conditions' value='" + jsonData[i].codigo + "' >" + jsonData[i].descripcion + "</><br>");
                (jsonData[i].aplica_condicion==="S") ? $("input[type=checkbox][value='"+ jsonData[i].codigo + "']").attr('checked', true): $("input[type=checkbox][value='"+ jsonData[i].codigo + "']").attr('checked', false);
            }   
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });         
}

function asignarCondicionesEspeciales(idItem){
    var listado = "";
    $('input[name="conditions"]:checked').each(function() {        
         listado += this.value + ",";
    });
 
        $.ajax({
            url: './controller?estado=Proceso&accion=Ejecutivo',
            datatype:'json',
            type:'post',
            data:{
                opcion: 67,
                listado: listado,
                id_item: idItem
            },      
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {          
                         $('#div_asignar_cond_especial').fadeOut("slow");
                         $('#div_asignar_cond_especial').dialog('close');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo cambiar configuración de condiciones especiales para el item dado!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            }          
        });
}

function eliminarCondicionesEspeciales(idItem) {  
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 68,
            id_item: idItem
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }                
            } else {
                mensajesDelSistema("Ocurrio un error al remover condiciones especiales!!", '250', '150');
            }

        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

 
function mensajesDelSistema(msj, width, height, swHideDialog) {   
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear botón cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}
 
function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}
