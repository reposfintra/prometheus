//confirma si el usuasario desea crear el plan de viaje manual
function ConfirmCreaPVjManual(){
   var frm = document.frmplamanual;
   var val = frm.confirm[0].checked;
   if (val){
      window.location = "controller?estado=PlanViajeManual&accion=GetInfo";
   }
   else{
      window.location = "controller?estado=PlanViaje&accion=QryAg&cmd=show";
   }
}

function validarManual(){
      if ( document.despmanual.planilla.value == "" )
      {
         alert("Digite n�mero de la planilla");
         document.despmanual.planilla.focus();
         return false;
      }
      if (document.despmanual.placa.value == "" )
      {
         alert("Digite placa del veh�culo");
         document.despmanual.placa.focus();
         return false;
      }
      if (document.despmanual.cedcon.value == "" )
      {
         alert("Digite la cedula del conductor");
         document.despmanual.cedcon.focus();
         return false;
      }
      return true;
}

function validarPlanviaje(){
      if ( document.frmplanvj.planilla.value == "" )
      {
         alert("Digite n�mero de la planilla");
         document.frmplanvj.planilla.focus();
         return false;
      }
      if (document.frmplanvj.placa.value == "" )
      {
         alert("Digite placa del veh�culo");
         document.frmplanvj.placa.focus();
         return false;
      }
      if (document.frmplanvj.producto.value == "" )
      {
         alert("Digite producto");
         document.frmplanvj.producto.focus();
         return false;
      }
      if (document.frmplanvj.fecha.value == "" )
      {
         alert("Seleccione fecha y hora de salida");
         document.frmplanvj.fecha.focus();
         return false;
      }
      if (document.frmplanvj.destinatario.value == "" )
      {
         alert("Digite destinatario");
         document.frmplanvj.destinatario.focus();
         return false;
      }
      if (document.frmplanvj.cedcon.value == "" )
      {
         alert("Digite c�dula del conductor");
         document.frmplanvj.cedcon.focus();
         return false;
      }
      if (document.frmplanvj.nomcon.value == "" )
      {
         alert("Digite nombre del conductor");
         document.frmplanvj.nomcon.focus();
         return false;
      }
      return true;
}