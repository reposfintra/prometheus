/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    //maximizarventana();
    jsonCategoriasAdmon = {};
    jsonUndsAdmon = listarComboGrid(10);
    cargarGridsCostos();   
    setTotalCostosAdmon();
    
    $("#btn_guardar").click(function () {     
        if (validateGridCostosAdmonAll()) {
            guardarCostosAdmonAll();
        }          
    });

    $("#btn_salir").click(function () {
        mensajeConfirmAction("Recuerde guardar los cambios. Esta seguro que desea salir ?", "230", "150", cerrarventana, "");
    });
});

function cargarGridsCostos() {

    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        dataType: 'json',
        async: false,
        data: {
            opcion: 57
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                jsonCategoriasAdmon = json;
                $('#div_costos_admon').html('');
                for (var i = 0; i < json.rows.length; i++) {
                    jsonItemsAdmon = listarItemsCategoria(json.rows[i].id);
                    addTableCostoAdmon(json.rows[i].id);
                    cargarCostosAdmon(json.rows[i].id,json.rows[i].nombre,jsonItemsAdmon);                   
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function addTableCostoAdmon(idCat) {       
    $('#div_costos_admon').append('<table id="tabla_costos_'+idCat+'"></table>'+
                '<div id="page_tabla_costos_'+idCat+'"></div>'+
                '<span style="margin-left:678px">Subtotal</span> <input type="text" id="subtotal_'+idCat+'" readonly value="0" style="color:#000;width:112px;text-align:right;margin-bottom:10px"><br>');
}

function cargarCostosAdmon(id_cat, descripcion, jsonItem) {     
     var grid_tbl_costos = jQuery("#tabla_costos_"+id_cat);
     if ($("#gview_tabla_costos_"+id_cat).length) {        
        refrescarGridCostos(id_cat);
     }else { 
        grid_tbl_costos.jqGrid({
            caption: descripcion,
            url: "./controlleropav?estado=Cotizacion&accion=Sl",           	 
            datatype: "json",  
            height: '150',
            width: '850', 
            colNames: ['Id', 'Id Categor�a', 'Id Item', 'Descripcion', 'Cantidad', 'Id Unidad', 'Unidad', 'Tiempo(Mes)', '% Aplicaci�n', 'Valor', 'Total'],
            colModel: [
                {name: 'id', index: 'id', hidden:true, width: 80, align: 'center', key: true},
                {name: 'id_categoria', index: 'id_categoria', hidden:true, width: 80, align: 'center'},
                {name: 'id_item', index: 'id_item', hidden:true, width: 80, align: 'center'},
                {name: 'descripcion', index: 'descripcion', editable:false, width: 260, align: 'left', edittype: 'select', 
                    editoptions: {
                        value: jsonItem,
                        defaultValue: 0,
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowId = e.target.id.replace("_descripcion", "");
                                        var id_item = e.target.value;
                                        var valor_item = obtenerValorItemDefault(id_item,id_cat);
                                        jQuery("#tabla_costos_" + id_cat).jqGrid('setCell', rowId, 'id_item', id_item);
                                        jQuery("#tabla_costos_" + id_cat).jqGrid('setCell', rowId, 'valor_item', valor_item);
                                    } catch (exc) {
                                    }
                                    return;
                                }}, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }}
                        ]}   
                },
                {name: 'cantidad', index: 'cantidad',editable:false, width: 80, align: 'center',
                        editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }}},
                {name: 'id_unidad_medicion', index: 'id_unidad_medicion', hidden:true, width: 80, align: 'center'},
                {name: 'unidad_medicion', index: 'unidad_medicion', editable:false, width: 90, resizable:false, sortable: true, align: 'center',edittype: 'select', 
                    editoptions: {
                        value: jsonUndsAdmon,
                        defaultValue: 1,
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowId = e.target.id.replace("_unidad_medicion", "");
                                        var id_und_medicion = e.target.value;
                                        jQuery("#tabla_costos_"+id_cat).jqGrid('setCell', rowId, 'id_unidad_medicion', id_und_medicion);                                        
                                    } catch (exc) {
                                    }
                                    return;
                                }}, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }}
                    ]}   
                },           
                {name: 'duracion', index: 'duracion',editable:true, width: 80, align: 'center',formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2},
                        editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {                           
                                if (e.which != 8 && e.which != 0  && e.which != 46 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }}
                },      
                {name: 'porc_aplicacion', index: 'porc_aplicacion', editable:true, width: 80, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 3}},
                {name: 'valor_item', index: 'valor_item', sortable: true, editable:true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {                             
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'valor_total', index: 'valor_total', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}} 
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_costos_'+id_cat),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: true,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pgtext: null,
            pgbuttons: false, 
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                    opcion: 9,                    
                    num_solicitud: $('#id_solicitud').val(),
                    id_categoria: id_cat
                }
            },   
            gridComplete: function() {             
                var colSumVlrItem = jQuery("#tabla_costos_" + id_cat).jqGrid('getCol', 'valor_item', false, 'sum');   
                var colSumTotal = jQuery("#tabla_costos_" + id_cat).jqGrid('getCol', 'valor_total', false, 'sum');  
                jQuery("#tabla_costos_" + id_cat).jqGrid('footerData', 'set', {valor_total: colSumTotal});     
                setSubTotalCostosAdmon(id_cat);
            },
            loadComplete: function (rowid, e, iRow, iCol) {
                    $("#tabla_costos_" + id_cat).contextMenu('myMenu', {
                    bindings: {
                        eliminar: function (rowid) {
                            var myGrid = jQuery("#tabla_costos_" + id_cat), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                            if(selRowIds===null){
                                 mensajesDelSistema('No hay registros seleccionados', '250', '150');
                                 return;
                            }
                            if (!selRowIds.startsWith("neo_")) {
                                eliminarCostoAdmon(selRowIds, id_cat);                               
                            } else {
                                $("#tabla_costos_" + id_cat).jqGrid('delRowData', selRowIds);
                            }
                            setTotalCostosAdmon();
                        }
                        }, onContexMenu: function (event/*, menu*/) {
                        }
                    });                
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {                
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_costos_"+id_cat, {add: false, edit: false, del: false, search: false, refresh: false}, {});  
    }
  }          


function refrescarGridCostos(id_cat){     
    jQuery("#tabla_costos_"+id_cat).setGridParam({
        url: "./controlleropav?estado=Cotizacion&accion=Sl",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            async:false,
            data: { 
                  opcion: 9,                    
                  num_solicitud: $('#id_solicitud').val(),
                  id_categoria: id_cat
            }
        }       
    });
    
    jQuery('#tabla_costos_'+id_cat).trigger("reloadGrid");
}

function editRowCostoAdmon(tableid, rowId){
    jQuery("#"+tableid).jqGrid('editRow', rowId, true, function () {
        //$("input, select", e.target).focus();
    }, null, null, {}, function (rowid) {
        var id_categoria = $("#"+tableid).getRowData(rowid).id_categoria;
        var cantidad = $("#"+tableid).getRowData(rowid).cantidad;
        var duracion = $("#"+tableid).getRowData(rowid).duracion;
        var porc_aplicacion = $("#"+tableid).getRowData(rowid).porc_aplicacion;
        var valor = $("#"+tableid).getRowData(rowid).valor_item;         
        jQuery("#"+tableid).jqGrid('setCell', rowid, 'valor_total', parseFloat((cantidad * duracion * valor * porc_aplicacion)/100));  
        var colSumTotal = jQuery("#" + tableid).jqGrid('getCol', 'valor_total', false, 'sum');  
        jQuery("#" + tableid).jqGrid('footerData', 'set', {valor_total: colSumTotal});   
        setSubTotalCostosAdmon(id_categoria);
        setTotalCostosAdmon();
    });
}

function listarComboGrid (op){
    var  Result={};
    $.ajax({
        type: 'GET',
        url: "./controlleropav?estado=Cotizacion&accion=Sl&opcion="+op,        
        dataType: 'json', 
        async:false,     
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    Result={};
                }else{
                    Result=json;
                }                
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return Result;
}

function listarItemsCategoria (id_cat){
    var  Result={};
    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Cotizacion&accion=Sl",        
        dataType: 'json', 
        async:false,
        data: {
            opcion: 11,           
            id_categoria: id_cat
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    Result={};
                }else{
                    Result=json;
                }                
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return Result;
}

function guardarCostosAdmon(tableid, id_cat){   
        
        var filasItems = jQuery("#"+tableid).jqGrid('getRowData');
        $.ajax({
                type: 'POST',
                url: './controlleropav?estado=Cotizacion&accion=Sl',
                dataType: 'json',
                async: false,
                data: {
                    opcion: 12,
                    num_solicitud: $('#id_solicitud').val(),   
                    id_categoria: id_cat,                                    
                    listadoitems: JSON.stringify({items: filasItems})
                          
                },
                success: function (json) {
                   
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        if (json.respuesta === "OK") {
                            mensajesDelSistema("INFORMACION GUARDADA", '250', '150', true);
                            refrescarGridCostos(id_cat);                   

                        }else{
                              mensajesDelSistema("Lo sentimos ocurri� un error al guardar informaci�n!!", '250', '150');
                        }

                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
}

function guardarCostosAdmonAll(){   
    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function () {
        $.ajax({
            type: 'POST',
            url: './controlleropav?estado=Cotizacion&accion=Sl',
            dataType: 'json',
            async: false,
            data: {
                opcion: 13,
                num_solicitud: $('#id_solicitud').val(),
                listadoitems: guardarCostosAdmonToJSON

            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    if (json.respuesta === "OK") {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("INFORMACION GUARDADA", '250', '150', true);
                        cargarGridsCostos();

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos ocurri� un error al guardar informaci�n!!", '250', '150');
                    }

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }, 500);
}

function guardarCostosAdmonToJSON(){
    var jsonItems = { "items":[]}; 
    for (var i = 0; i < jsonCategoriasAdmon.rows.length; i++) {      
        var id_cat = jsonCategoriasAdmon.rows[i].id;
        var filasItems = jQuery("#tabla_costos_"+id_cat).jqGrid('getRowData');        
        jsonItems.items.push(filasItems);
        /*console.log(jsonCategoriasAdmon.rows[i].id);
        console.log(jsonCategoriasAdmon.rows[i].nombre);*/
    }
    return JSON.stringify(jsonItems); 
}

function validateGridCostosAdmonAll(){
   var estado = true;
   for (var i = 0; i < jsonCategoriasAdmon.rows.length; i++) {      
        var id_cat = jsonCategoriasAdmon.rows[i].id;
        var descripcion = jsonCategoriasAdmon.rows[i].nombre;
        if(!validateGridCostosAdmon("tabla_costos_"+id_cat, descripcion)){
            estado=false;
            break;
        }       
    }
    return estado; 
}

function validateGridCostosAdmon(tableid, descripcion) {
    var estado = true;   
    var ids = jQuery("#" + tableid).jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id_item = jQuery("#" + tableid).getRowData(ids[i]).id_item;
        var cantidad = jQuery("#" + tableid).getRowData(ids[i]).cantidad;
        var duracion = jQuery("#" + tableid).getRowData(ids[i]).duracion;
        var porc_aplicacion = jQuery("#" + tableid).getRowData(ids[i]).porc_aplicacion;
        var valor_item = jQuery("#" + tableid).getRowData(ids[i]).valor_item;
        if (parseFloat(valor_item) === 0 || id_item === '0' || parseFloat(cantidad) === 0 || parseFloat(duracion) === 0 || parseFloat(porc_aplicacion) === 0 ) {
           estado = false;
           mensajesDelSistema('COSTOS ADMINISTRACI�N: ' + descripcion + ', INFORMACI�N INV�LIDA EN FILA ' + (i + 1), '320', '165');
           break;
        }
    }
    return estado;
}

function eliminarCostoAdmon(id_costo, id_cat){
       $.ajax({
        type: "POST",
        url: './controlleropav?estado=Cotizacion&accion=Sl',
        async: false,
        dataType: "json",
        data: {
            opcion: 14,
            id: id_costo
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridCostos(id_cat);;                              
                } else {
                    mensajesDelSistema(".::ERROR AL ELIMINAR COSTO::.", '250', '150');
                }

            }
        }
    });
}

function obtenerValorItemDefault(id_item, id_cat){
       var valor = 0;
       $.ajax({
        type: "POST",
        url: './controlleropav?estado=Cotizacion&accion=Sl',
        async: false,
        dataType: "json",
        data: {
            opcion: 27,
            id_cat: id_cat,
            id_item: id_item
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }
                valor = json.respuesta;

            }
        }
    });
    
    return valor;
}

function setSubTotalCostosAdmon(id_cat){   
    var total = jQuery("#tabla_costos_" + id_cat).jqGrid('getCol', 'valor_total', false, 'sum');      
    $('#subtotal_'+id_cat).val('$ '+numberConComas(total));
}


function setTotalCostosAdmon(){   
    var total = 0;
     for (var i = 0; i < jsonCategoriasAdmon.rows.length; i++) {      
        var id_cat = jsonCategoriasAdmon.rows[i].id;       
        var colSumTotal = jQuery("#tabla_costos_" + id_cat).jqGrid('getCol', 'valor_total', false, 'sum'); 
        total = total + parseFloat(colSumTotal);
    }
    $('#total_costos_admon').val('$ '+numberConComas(total));
}


function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}


function mensajeConfirmAction(msj, width, height, okAction, id) {  
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function cerrarventana(){
    window.close();
}