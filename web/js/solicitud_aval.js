var controller;

function varcontr(contr){
    controller=contr;
}

function dircobro(campo1, campo2){
    document.getElementById(campo2).value=document.getElementById(campo1).value;
}

function datsolic(){
    if(document.getElementById("parentesco").value=="HIJOS"){
        if(document.getElementById("tipo_p").value=="natural"){
            document.getElementById("dir_est").value=document.getElementById("dir_nat").value;
            document.getElementById("tel_est").value=document.getElementById("tel_nat").value;
        }
    }
}

function formato(obj, decimales){
    var numero = obj.value.replace( new RegExp(",","g"), "");
    var nums = ( new String (numero) ).split('.');
    var salida = new String();
    var TieneDec = numero.indexOf('.');
    var dato = new String();
    if( TieneDec !=-1  ){
        var deci = numero.split('.');
        var dec       = (deci[1].length >2)?deci[1].charAt(2):deci[1].substr(0,deci[1].length);


        if(dec > 5){
            dato =  (parseInt(deci[1].substr(0,2)) + 1);
            if(dato>100){
                nums[0] = new String (parseInt(nums[0])+1);
                obj.value = nums[0]+'';
            }else{
                for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
                obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>9)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '');

            }
        }else{

            for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
            obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '');

        }
    }else{

        for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
        obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '');

    }
}

function fhijos(pers){


/* Ivargas Se habilitara posteriormente cuando se solicite.
    *
    * var num = document.getElementById('nhijos_'+pers).value;
    var cont="<br><b>Informac&oacute;n Hijos</b>";
    cont+='<table id="thijos'+pers+'" style="border-collapse:collapse; width:100%" border="1">';

    for(i=0;i<num;i++){
        cont+='<tr class="filaazul">';
        cont+='<td colspan="2">Nombre&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp';
        cont+='&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ';
        cont+='&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ';
        cont+='&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ';
        cont+='Edad<br><input type="text" id="nom_hij_'+pers+i+'" name="nom_hij_'+pers+i+'" value="" size="50" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp';
        cont+='<input type="text" id="edad_hij_'+pers+i+'" name="edad_hij_'+pers+i+'" value="" size="5" > </td>';
        cont+='<td width="17%">Telefono<br><input type="text" id="tel_hij_'+pers+i+'" name="tel_hij_'+pers+i+'" value="" size="25" > </td>';
        cont+='<td width="17%">Direccion<br><input type="text" id="dir_hij_'+pers+i+'" name="dir_hij_'+pers+i+'" value="" size="30" > </td>';
        cont+='<td colspan="2">Email<br><input type="text" id="mail_hij_'+pers+i+'" name="mail_hij_'+pers+i+'" value="" size="60" > </td>';
        cont+=' </tr>';
    }
    cont+="</table>";
    if(num<1){
        cont="";
    }

    $('hijos'+pers).innerHTML = cont; */

}

function soloNumeros(id) {
    var valor = document.getElementById(id).value;
    valor =  valor.replace(/[^0-9^.]+/gi,"");
    document.getElementById(id).value = valor;
}
function soloAlfa(id) {
    var valor = document.getElementById(id).value;
    valor =  valor.replace(/[^A-Z,a-z^ ]+/gi,"");
    document.getElementById(id).value = valor;
}
function soloAlfa1(id) {
    var valor = document.getElementById(id).value;
    valor =  valor.replace(/[^A-Z,a-z]+/gi,"");
    document.getElementById(id).value = valor;
}

function colocarCod(indice, id_src, id_dest){
    var valor = $(id_src).options[indice].value;
    $(id_dest).value = valor;
}

function guardar(){
    var campos = "";
    var ncampos = "";
    var producto="";

    if(document.getElementById("tipoconv").value=="Microcredito"){
        producto=document.getElementById("producto").value;
    }

    if(document.getElementById("tipoconv").value=="Consumo"){

        campos = new Array("afiliado");
        ncampos = new Array("Afiliado ");

        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
        var indice = document.getElementById("producto").selectedIndex;
        producto=document.getElementById("producto").options[indice].value;
    }
    campos = new Array("convenio" );
    ncampos = new Array("Convenio");

    for (i = 0; i < campos.length; i++){
        campo = campos[i];
        if (document.getElementById("formulario").elements[campo].value == ''){
            alert("El campo "+ncampos[i]+" esta vacio.!");
            document.getElementById("formulario").elements[campo].focus();
            return (false);
            break;
        }
    
    }
    
    var est= producto=='01'?"S":"N";
    var cod= document.getElementById("codeu").checked?"S":"N";
    var cod2= (document.getElementById("codeu2")!=null && document.getElementById("codeu2").checked)?"S":"N";
    var accion = controller+"?estado=GestionSolicitud&accion=Aval";
    var valid = new Validation('formulario');
    if(valid.validate()){
        $('formulario').action = accion + '&opcion=ins_data&dato=B&est='+est+'&cod='+cod+'&cod2='+cod2+'&boton=guardar';
        //habilitamos el formulario
        hab();
        $('formulario').submit();
    }else{
        alert("una o mas fechas en el formulario son invalidas");
    }
}

function cargarCiudades(id_dept, id_ciud){
    var dept = $(id_dept).value;    
    var url = controller+"?estado=GestionSolicitud&accion=Aval";
    var p = "opcion=cargarciu&dept="+dept+"&ciu="+id_ciud;
    new Ajax.Request(url, {
        parameters: p,
        method: 'post',
        onComplete:  function (resp){
            document.getElementById("d_"+id_ciud).innerHTML = resp.responseText;            
        }
    });

}
function cargarCiudades(id_dept, id_ciud,valor){
    var dept = $(id_dept).value;
    var url = controller+"?estado=GestionSolicitud&accion=Aval";
    var p = "opcion=cargarciu&dept="+dept+"&ciu="+id_ciud;
    new Ajax.Request(url, {
        parameters: p,
        asynchronous: false,
        method: 'post',
        onComplete:  function (resp){
            document.getElementById("d_"+id_ciud).innerHTML = resp.responseText;
            if(valor!=null){
                document.getElementById(id_ciud).value=valor;
            }
        }
    });

}

function cargarOcupaciones(id_act, id_ocu){
    var act = $(id_act).value;
    habilitar_pensionado_hogar(true, act,id_act);
    var url = controller+"?estado=GestionSolicitud&accion=Aval";
    var p = "opcion=cargarocu&act="+act+"&ocu="+id_ocu;
    new Ajax.Request(url, {
        parameters: p,
        method: 'post',
        onComplete:  function (resp){
            document.getElementById("d_"+id_ocu).innerHTML = resp.responseText;
        }
    });

}
function habilitar_pensionado_hogar(disabled, act,id_act){
    //var tipo=$(id_act).attr("id");
    if((act=="PENSI" || act=="HOGAR")&& id_act=="act_econ_nat"){
       // $("nom_emp_nat").disabled=disabled;
        $("car_emp_nat").disabled=disabled;
        $("f_ing_nat").disabled=disabled;
        $("tipo_cont_nat").disabled=disabled;
        $("eps_nat").disabled=disabled;
        $("tip_afil_nat").disabled=disabled;
        $("tel_emp_nat").disabled=disabled;
        $("ext_emp_nat").disabled=disabled;
        $("cel_emp_nat").disabled=disabled;
        $("mail_empr_nat").disabled=disabled;
        $("dir_emp_nat").disabled=disabled;
        $("dep_emp_nat").disabled=disabled;
        $("ciu_emp_nat").disabled=disabled;
        $("nit_emp_nat").disabled=disabled;
        $("nit_emp_nat2").disabled=disabled;
       
    }else if((act=="PENSI" || act=="HOGAR")&&id_act=="act_econ_cod"){
        
        //$("nom_emp_cod").disabled=disabled;
        $("car_emp_cod").disabled=disabled;
        $("f_ing_cod").disabled=disabled;
        $("tipo_cont_cod").disabled=disabled;
        $("eps_cod").disabled=disabled;
        $("tip_afil_cod").disabled=disabled;
        $("tel_emp_cod").disabled=disabled;
        $("ext_emp_cod").disabled=disabled;
        $("cel_emp_cod").disabled=disabled;
        //$("mail_emp_cod").disabled=disabled;
        $("dir_emp_cod").disabled=disabled;
        $("dep_emp_cod").disabled=disabled;
        $("ciu_emp_cod").disabled=disabled;
        $("nit_emp_cod").disabled=disabled;
        $("nit_emp_cod2").disabled=disabled;
          
    }else if(id_act=="act_econ_nat"){
        
        $("nom_emp_nat").disabled=false;
        $("car_emp_nat").disabled=false;
        $("f_ing_nat").disabled=false;
        $("tipo_cont_nat").disabled=false;
        $("eps_nat").disabled=false;
        $("tip_afil_nat").disabled=false;
        $("tel_emp_nat").disabled=false;
        $("ext_emp_nat").disabled=false;
        $("cel_emp_nat").disabled=false;
        $("mail_empr_nat").disabled=false;
        $("dir_emp_nat").disabled=false;
        $("dep_emp_nat").disabled=false;
        $("ciu_emp_nat").disabled=false;
        $("nit_emp_nat").disabled=false;
        $("nit_emp_nat2").disabled=false;
        
    } else{
        
        $("nom_emp_cod").disabled=false;
        $("car_emp_cod").disabled=false;
        $("f_ing_cod").disabled=false;
        $("tipo_cont_cod").disabled=false;
        $("eps_cod").disabled=false;
        $("tip_afil_cod").disabled=false;
        $("tel_emp_cod").disabled=false;
        $("ext_emp_cod").disabled=false;
        $("cel_emp_cod").disabled=false;
        //$("mail_empr_cod").disabled=false;
        $("dir_emp_cod").disabled=false;
        $("dep_emp_cod").disabled=false;
        $("ciu_emp_cod").disabled=false;
        $("nit_emp_cod").disabled=false;
        $("nit_emp_cod2").disabled=false;
        
        
        
    }      
    
}

function busqueda(id_field){
    var dato = $(id_field).value;
    if(dato==''){
        alert('Para consultar debe escribir algo');
    }
    else{
        if(id_field=='id_est'){
            if(document.getElementById('id_nat').value==dato){
               
                document.getElementById('pr_apellido_est').value = document.getElementById('pr_apellido_nat').value;
                document.getElementById('seg_apellido_est').value = document.getElementById('seg_apellido_nat').value;
                document.getElementById('pr_nombre_est').value = document.getElementById('pr_nombre_nat').value;
                document.getElementById('seg_nombre_est').value = document.getElementById('seg_nombre_nat').value ;
                document.getElementById('genero_est').value = document.getElementById('genero_nat').value;
                document.getElementById('tipo_id_est').value = document.getElementById('tipo_id_nat').value ;
                document.getElementById('id_est').value = document.getElementById('id_est').value ;
                document.getElementById('dir_est').value = document.getElementById('dir_nat').value;
                document.getElementById('tel_est').value = document.getElementById('tel_nat').value;                 
                document.getElementById('f_exp_est').value = document.getElementById('f_exp_nat').value;                
                document.getElementById('dep_exp_est').value = document.getElementById('dep_exp_nat').value; 
                document.getElementById('f_nac_est').value =  document.getElementById('f_nac_nat').value;
                document.getElementById('dep_nac_est').value = document.getElementById('dep_nac_nat').value;
                document.getElementById('est_civil_est').value = document.getElementById('est_civil_nat').value;
                document.getElementById('pcargo_est').value = document.getElementById('pcargo_nat').value;
                document.getElementById('nhijos_est').value = document.getElementById('nhijos_nat').value;
                document.getElementById('ngrupo_est').value =  document.getElementById('ngrupo_nat').value;
                document.getElementById('dep_est').value = document.getElementById('dep_nat').value;
                document.getElementById('barrio_est').value = document.getElementById('barrio_nat').value;
                document.getElementById('estr_est').value = document.getElementById('estr_nat').value ;
                document.getElementById('tipo_viv_est').value = document.getElementById('tipo_viv_nat').value;
                document.getElementById('tres_est').value = document.getElementById('tres_nat').value;
                document.getElementById('cel_est').value = document.getElementById('cel_nat').value;
                document.getElementById('mail_est').value = document.getElementById('mail_nat').value;
                document.getElementById('tres_est1').value = document.getElementById('tres_nat1').value;
                cargarCiudades('dep_exp_est', 'ciu_exp_est',document.getElementById('ciu_exp_nat').value);
                cargarCiudades('dep_nac_est', 'ciu_nac_est',document.getElementById('ciu_nac_nat').value);
                cargarCiudades('dep_est', 'ciu_est',document.getElementById('ciu_nat').value);
                // INFORMACION LABORAL
                document.getElementById('trabaja').value = 'S';
                document.getElementById('emp_est').value = document.getElementById('nom_emp_nat').value;
                document.getElementById('dir_emp_est').value = document.getElementById('dir_emp_nat').value;
                document.getElementById('tel_emp_est').value = document.getElementById('tel_emp_nat').value;
                document.getElementById('sal_est').value = document.getElementById('sal_nat').value;

                return false;
            }
        }
        var url = controller+"?estado=GestionSolicitud&accion=Aval";
        var p = "opcion=buscarnit&dato="+dato+"&producto="+ document.getElementById('producto').value;
        new Ajax.Request(url, {
            parameters: p,
            method: 'post',
            onComplete:  function (resp){

                var texto = resp.responseText;
                var array = texto.split(";_;");
                if(array.length==1){
                    alert(array[0]);
                }
                if(array.length>9){
                    if(id_field=="id_nat"){
                        document.getElementById('pr_apellido_nat').value = array[0];
                        document.getElementById('seg_apellido_nat').value = array[1];
                        document.getElementById('pr_nombre_nat').value = array[2];
                        document.getElementById('seg_nombre_nat').value = array[3];
                        document.getElementById('genero_nat').value = array[5];
                        document.getElementById('tipo_id_nat').value = array[6];
                        document.getElementById('id_nat').value = array[7];
                        document.getElementById('dir_nat').value = array[8];
                        document.getElementById('tel_nat').value = array[9];
                        document.getElementById('cel_nat').value = array[10];

                    }
                    if(id_field=="nit"){
                        document.getElementById('razon_social').value = array[4];
                        document.getElementById('nit').value = array[7];
                        document.getElementById('dir_jur').value = array[8];
                        document.getElementById('telefono1_jur').value = array[9];
                        document.getElementById('cel_jur').value = array[10];
                    }
                    if(id_field=="id_cod"){
                        document.getElementById('pr_apellido_cod').value = array[0];
                        document.getElementById('seg_apellido_cod').value = array[1];
                        document.getElementById('pr_nombre_cod').value = array[2];
                        document.getElementById('seg_nombre_cod').value = array[3];
                        document.getElementById('genero_cod').value = array[5];
                        document.getElementById('tipo_id_cod').value = array[6];
                        document.getElementById('id_cod').value = array[7];
                        document.getElementById('dir_cod').value = array[8];
                        document.getElementById('tel_cod').value = array[9];
                        document.getElementById('cel_cod').value = array[10];

                    }
                    if(id_field=="id_cod_jur"){
                        document.getElementById('nombre_cod_jur').value = array[4];
                        document.getElementById('id_cod_jur').value = array[7];
                        document.getElementById('dir_cod_jur').value = array[8];
                        document.getElementById('tel_cod_jur').value = array[9];
                        document.getElementById('cel_cod_jur').value = array[28];
                        document.getElementById('dep_cod_jur').value = array[23];
                        cargarCiudades('dep_cod_jur', 'ciu_cod_jur',array[22] );
                        document.getElementById('f_exp_cod_jur').value = array[31];
                        document.getElementById('mail_cod_jur').value = array[29];
                        document.getElementById('barrio_cod_jur').value = array[24];
                        document.getElementById('rep_legal_cod_jur').value = array[32];
                        document.getElementById('gen_repr_cod_jur').value = array[33];
                        document.getElementById('caracter_cod_jur').value = array[41];
                        document.getElementById('tipo_id_repr_cod_jur').value = array[34];
                        document.getElementById('id_repr_cod_jur').value = array[35];
                    }
                    if(id_field=="id_est"){
                        document.getElementById('pr_apellido_est').value = array[0];
                        document.getElementById('seg_apellido_est').value = array[1];
                        document.getElementById('pr_nombre_est').value = array[2];
                        document.getElementById('seg_nombre_est').value = array[3];
                        document.getElementById('genero_est').value = array[5];
                        document.getElementById('tipo_id_est').value = array[6];
                        document.getElementById('id_est').value = array[7];
                        document.getElementById('dir_est').value = array[8];
                        document.getElementById('tel_est').value = array[9];
                        document.getElementById('cel_est').value = array[10];

                    }
                }
                if(array.length>29){
               
                    if(id_field=="id_nat"){
                        document.getElementById('f_nac_nat').value = array[13];
                        document.getElementById('dep_nat').value = array[23];
                        cargarCiudades('dep_nat', 'ciu_nat',array[22]);
                        document.getElementById('tipo_viv_nat').value = array[26];
                        //INFORMACION LABORAL
                        if(array.length>57){
                            document.getElementById('dir_emp_nat').value = array[62];
                            document.getElementById('dep_emp_nat').value = array[64];
                            cargarCiudades('dep_emp_nat', 'ciu_emp_nat',array[65] );
                            document.getElementById('tel_emp_nat').value = array[66];
                            document.getElementById('ext_emp_nat').value = array[67];
                            document.getElementById('car_emp_nat').value = array[68];
                            document.getElementById('f_ing_nat').value = array[69];
                            document.getElementById('sal_nat').value = array[71];
                            document.getElementById('otros_nat').value = array[72];

                            document.getElementById('act_econ_nat').value = array[57];
                            document.getElementById('ocup_nat').value = array[58];
                            document.getElementById('nom_emp_nat').value = array[59];
                            document.getElementById('nit_emp_nat').value = array[60];
                            document.getElementById('nit_emp_nat2').value = array[61];
//                            document.getElementById('dir_cob_nat').value = array[63];
                            document.getElementById('tipo_cont_nat').value = array[70];
                            document.getElementById('conc_otros_nat').value = array[73];
                            document.getElementById('manuten_nat').value = array[74];
                            document.getElementById('cred_nat').value = array[75];
                            document.getElementById('arr_nat').value = array[76];
                            document.getElementById('cel_emp_nat').value = array[77];
                            document.getElementById('mail_empr_nat').value = array[78];
                            document.getElementById('eps_nat').value = array[79];
                            document.getElementById('tip_afil_nat').value = array[80];
                        }

                        document.getElementById('f_exp_nat').value = array[10];                        
                        document.getElementById('dep_exp_nat').value = array[12];
                        cargarCiudades('dep_exp_nat', 'ciu_exp_nat',array[11]);                                                
                        document.getElementById('dep_nac_nat').value = array[15];
                        cargarCiudades('dep_nac_nat', 'ciu_nac_nat',array[14]);
                        document.getElementById('est_civil_nat').value = array[16];
                        document.getElementById('niv_est_nat').value = array[17];
                        document.getElementById('prof_nat').value = array[18];
                        document.getElementById('pcargo_nat').value = array[19];
                        document.getElementById('nhijos_nat').value = array[20];
                        document.getElementById('ngrupo_nat').value = array[21];                       
                        
                        document.getElementById('barrio_nat').value = array[24];
                        document.getElementById('estr_nat').value = array[25];
                        
                        document.getElementById('tres_nat').value = array[27];
                        document.getElementById('cel_nat').value = array[28];
                        document.getElementById('mail_nat').value = array[29];
                        document.getElementById('tres_nat1').value = array[43];
                        //DATOS CONYUGUE
                        document.getElementById('pr_apellido_con_nat').value = array[44];
                        document.getElementById('seg_apellido_con_nat').value = array[45];
                        document.getElementById('pr_nombre_con_nat').value = array[46];
                        document.getElementById('seg_nombre_con_nat').value = array[47];
                        document.getElementById('tipo_id_con_nat').value = array[48];
                        document.getElementById('id_con_nat').value = array[49];
                        document.getElementById('cel_con_nat').value = array[50];
                        document.getElementById('emp_con_nat').value = array[51];
                        document.getElementById('emp_dir_con_nat').value = array[52];
                        document.getElementById('emp_tel_con_nat').value = array[53];
                        document.getElementById('emp_car_con_nat').value = array[54];
                        document.getElementById('emp_sal_con_nat').value = array[55];
                        document.getElementById('mail_con_nat').value = array[56];
                        

                    }
                    if(id_field=="nit"){
                        
                        document.getElementById('dep_jur').value = array[23];
                        cargarCiudades('dep_jur', 'ciu_jur',array[22] );
                        document.getElementById('cel_jur').value = array[28];
                        document.getElementById('mail_jur').value = array[29];
                        document.getElementById('fax').value = array[30];
                        document.getElementById('f_const').value = array[31];
                        document.getElementById('rep_legal').value = array[32];
                        document.getElementById('gen_repr').value = array[33];
                        document.getElementById('tipo_id_repr').value = array[34];
                        document.getElementById('id_repr').value = array[35];
                        document.getElementById('autorizado').value = array[36];
                        document.getElementById('gen_autor').value = array[37];
                        document.getElementById('tipo_id_autor').value = array[38];
                        document.getElementById('id_autor').value = array[39];
                        document.getElementById('ciiu').value = array[40];
                        document.getElementById('caracter').value = array[41];
                        document.getElementById('telefono2_jur').value = array[42];
                    }
                    if(id_field=="id_cod"){
                        document.getElementById('f_nac_cod').value = array[13];
                        document.getElementById('dep_cod').value = array[23];
                        cargarCiudades('dep_cod', 'ciu_cod',array[22]);
                        document.getElementById('tipo_viv_cod').value = array[26];
                        //INFORMACION LABORAL
                        if(array.length>57){
                            document.getElementById('dir_emp_cod').value = array[62];
                            document.getElementById('dep_emp_cod').value = array[64];
                            cargarCiudades('dep_emp_cod', 'ciu_emp_cod',array[65] );
                            document.getElementById('tel_emp_cod').value = array[66];
                            document.getElementById('ext_emp_cod').value = array[67];
                            document.getElementById('car_emp_cod').value = array[68];
                            document.getElementById('f_ing_cod').value = array[69];
                            document.getElementById('sal_cod').value = array[71];
                            document.getElementById('otros_cod').value = array[72];

                            document.getElementById('act_econ_cod').value = array[57];
                            document.getElementById('ocup_cod').value = array[58];
                            document.getElementById('nom_emp_cod').value = array[59];
                            document.getElementById('nit_emp_cod').value = array[60];
                            document.getElementById('nit_emp_cod2').value = array[61];
                            document.getElementById('dir_cob_cod').value = array[63];
                            document.getElementById('tipo_cont_cod').value = array[70];
                            document.getElementById('conc_otros_cod').value = array[73];
                            document.getElementById('manuten_cod').value = array[74];
                            document.getElementById('cred_cod').value = array[75];
                            document.getElementById('arr_cod').value = array[76];
                            document.getElementById('cel_emp_cod').value = array[77];
                            document.getElementById('mail_emp_cod').value = array[78];
                            document.getElementById('eps_cod').value = array[79];
                            document.getElementById('tip_afil_cod').value = array[80];
                        }
                        document.getElementById('f_exp_cod').value = array[10];                        
                        document.getElementById('dep_exp_cod').value = array[12];
                        cargarCiudades('dep_exp_cod', 'ciu_exp_cod',array[11] );                                               
                        document.getElementById('dep_nac_cod').value = array[15];
                        cargarCiudades('dep_nac_cod', 'ciu_nac_cod',array[14]);
                        document.getElementById('est_civil_cod').value = array[16];
                        document.getElementById('niv_est_cod').value = array[17];
                        document.getElementById('prof_cod').value = array[18];
                        document.getElementById('pcargo_cod').value = array[19];
                        document.getElementById('nhijos_cod').value = array[20];
                        document.getElementById('ngrupo_cod').value = array[21];
                        document.getElementById('barrio_cod').value = array[24];
                        document.getElementById('estr_cod').value = array[25];
                        document.getElementById('tres_cod').value = array[27];
                        document.getElementById('cel_cod').value = array[28];
                        document.getElementById('mail_cod').value = array[29];
                        document.getElementById('tres_cod1').value = array[43];
                       
                    }

                    if(id_field=="id_est"){
                        document.getElementById('f_nac_est').value = array[13];
                        document.getElementById('dep_est').value = array[23];
                        cargarCiudades('dep_est', 'ciu_est',array[22]);
                        document.getElementById('tipo_viv_est').value = array[26];

                        document.getElementById('f_exp_est').value = array[10];                        
                        document.getElementById('dep_exp_est').value = array[12];
                        cargarCiudades('dep_exp_est', 'ciu_exp_est',array[11]);
                        document.getElementById('dep_nac_est').value = array[15];
                        cargarCiudades('dep_nac_est', 'ciu_nac_est',array[14]);
                        document.getElementById('est_civil_est').value = array[16];
                        document.getElementById('pcargo_est').value = array[19];
                        document.getElementById('nhijos_est').value = array[20];
                        document.getElementById('ngrupo_est').value = array[21];  
                        document.getElementById('barrio_est').value = array[24];
                        document.getElementById('estr_est').value = array[25];                        
                        document.getElementById('tres_est').value = array[27];
                        document.getElementById('cel_est').value = array[28];
                        document.getElementById('mail_est').value = array[29];
                        document.getElementById('tres_est1').value = array[43];

                    }
                }
            }
        } );
    }
}

function mantenerDatos(estado){
    var id_div = document.getElementById("tipo_p").value;
    var cedula= document.getElementById('id_nat').value
    var tipo_docu=document.getElementById('tipo_id_nat').value
    if(estado=='B' && id_div=="natural"  && cedula!="" && tipo_docu !=""){
        guardar();
    }
}

function limpiarcodj(estado){
    var mcheck =document.getElementById("codeu").checked;
    var mcheck2 =document.getElementById("codeu2").checked;
    var comp = ""
    
    var tco = document.getElementById('contenido').children['tcodeudor'];
    
    if(mcheck2 == true) {
      if(tco) {
          comp="codeu";
      } else {
          xpand_div1(); return;
      }
    } else if (mcheck == true) {
      comp="codeu2";
    } 
    
    if(comp != "") {  document.getElementById(comp).checked = false; }
    
    if(mcheck!=true && mcheck2!=true){
        if (!confirm("Est� seguro que no requiere la informaci�n del codeudor?")) {
            xpand_div1(); 
            return; 
        }
    }
    if(mcheck!=true) {
        //codeudor natural
        document.getElementById("pr_apellido_cod").value="";
        document.getElementById("seg_apellido_cod").value="";
        document.getElementById("pr_nombre_cod").value="";
        document.getElementById("seg_nombre_cod").value="";
        document.getElementById("genero_cod").value="";
        document.getElementById("tipo_id_cod").value="";
        document.getElementById("id_cod").value="";
        document.getElementById("f_exp_cod").value="";
        document.getElementById("ciu_exp_cod").value="";
        document.getElementById("dep_exp_cod").value="";
        document.getElementById("f_nac_cod").value="";
        document.getElementById("ciu_nac_cod").value="";
        document.getElementById("dep_nac_cod").value="";
        document.getElementById("est_civil_cod").value="";
        document.getElementById("niv_est_cod").value="";
        document.getElementById("prof_cod").value="";
        document.getElementById("pcargo_cod").value="";
        document.getElementById("nhijos_cod").value="";
        document.getElementById("ngrupo_cod").value="";
        document.getElementById("dir_cod").value="";
        document.getElementById("ciu_cod").value="";
        document.getElementById("dep_cod").value="";
        document.getElementById("barrio_cod").value="";
        document.getElementById("estr_cod").value="";
        document.getElementById("tel_cod").value="";
        document.getElementById("tipo_viv_cod").value="";
        //document.getElementById("tres_cod").value="";
        //document.getElementById("cel_cod").value="";
        //document.getElementById("mail_cod").value="";
        document.getElementById("pr_apellido_refp_cod").value="";
        document.getElementById("seg_apellido_refp_cod").value="";
        document.getElementById("pr_nombre_refp_cod").value="";
        document.getElementById("seg_nombre_refp_cod").value="";
        document.getElementById("tel1_refp_cod").value="";
        document.getElementById("tel2_refp_cod").value="";
        document.getElementById("ext_refp_cod").value="";
        document.getElementById("cel_refp_cod").value="";
        document.getElementById("ciu_refp_cod").value="";
        document.getElementById("dep_refp_cod").value="";
        document.getElementById("tconocido_refp_cod").value="";
        document.getElementById("ocup_cod").value="";
        document.getElementById("act_econ_cod").value="";
        document.getElementById("nom_emp_cod").value="";
        document.getElementById("nit_emp_cod").value="";
        document.getElementById("dir_emp_cod").value="";
        document.getElementById("ciu_emp_cod").value="";
        document.getElementById("dep_emp_cod").value="";
        document.getElementById("tel_emp_cod").value="";
        document.getElementById("ext_emp_cod").value="";
        document.getElementById("car_emp_cod").value="";
        document.getElementById("f_ing_cod").value="";
        document.getElementById("tipo_cont_cod").value="";
        document.getElementById("sal_cod").value="";
        document.getElementById("otros_cod").value="";
        document.getElementById("conc_otros_cod").value="";
        document.getElementById("manuten_cod").value="";
        document.getElementById("cred_cod").value="";
        document.getElementById("arr_cod").value="";
        document.getElementById("pr_apellido_refam_cod").value="";
        document.getElementById("seg_apellido_refam_cod").value="";
        document.getElementById("pr_nombre_refam_cod").value="";
        document.getElementById("seg_nombre_refam_cod").value="";
        document.getElementById("tel1_refam_cod").value="";
        document.getElementById("tel2_refam_cod").value="";
        document.getElementById("ext_refam_cod").value="";
        document.getElementById("cel_refam_cod").value="";
        document.getElementById("ciu_refam_cod").value="";
        document.getElementById("dep_refam_cod").value="";
        // document.getElementById("tconocido_refam_cod").value="";
    }
    if (mcheck2!=true) {
        //codeudor juridico
        document.getElementById('nombre_cod').value = "";
        document.getElementById('id_cod_jur').value = "";
        document.getElementById('dir_cod').value = "";
        document.getElementById('tel_cod').value = "";
        document.getElementById('cel_cod').value = "";
        document.getElementById('dep_cod').value = "";
        document.getElementById('ciu_cod').value = "";
        document.getElementById('f_exp_cod').value = "";
        document.getElementById('mail_cod').value = "";
        document.getElementById('barrio_cod').value = "";
        document.getElementById('rep_legal_cod').value = "";
        document.getElementById('gen_repr_cod').value = "";
        document.getElementById('caracter_cod').value = "";
        document.getElementById('tipo_id_repr_cod').value = "";
        document.getElementById('id_repr_cod').value = "";
        xpand_div1(); 
    } else {
        document.getElementById('contenido').children['tcodeudor'].innerHTML = document.getElementById('tcodeudor2').innerHTML;
    }
}

function limpiarcod(estado){
    var check =document.getElementById("codeu").checked;
    if(estado=='B'){
        guardar();
    }
    if(check!=true){
        if (confirm("Est� seguro que no requiere la informaci�n del codeudor?")) {
            document.getElementById("pr_apellido_cod").value="";
            document.getElementById("seg_apellido_cod").value="";
            document.getElementById("pr_nombre_cod").value="";
            document.getElementById("seg_nombre_cod").value="";
            document.getElementById("genero_cod").value="";
            document.getElementById("tipo_id_cod").value="";
            document.getElementById("id_cod").value="";
            document.getElementById("f_exp_cod").value="";
            document.getElementById("ciu_exp_cod").value="";
            document.getElementById("dep_exp_cod").value="";
            document.getElementById("f_nac_cod").value="";
            document.getElementById("ciu_nac_cod").value="";
            document.getElementById("dep_nac_cod").value="";
            document.getElementById("est_civil_cod").value="";
            document.getElementById("niv_est_cod").value="";
            document.getElementById("prof_cod").value="";
            document.getElementById("pcargo_cod").value="";
            document.getElementById("nhijos_cod").value="";
            document.getElementById("ngrupo_cod").value="";
            document.getElementById("dir_cod").value="";
            document.getElementById("ciu_cod").value="";
            document.getElementById("dep_cod").value="";
            document.getElementById("barrio_cod").value="";
            document.getElementById("estr_cod").value="";
            document.getElementById("tel_cod").value="";
            document.getElementById("tipo_viv_cod").value="";
            //document.getElementById("tres_cod").value="";
            //document.getElementById("cel_cod").value="";
            //document.getElementById("mail_cod").value="";
            document.getElementById("pr_apellido_refp_cod").value="";
            document.getElementById("seg_apellido_refp_cod").value="";
            document.getElementById("pr_nombre_refp_cod").value="";
            document.getElementById("seg_nombre_refp_cod").value="";
            document.getElementById("tel1_refp_cod").value="";
            document.getElementById("tel2_refp_cod").value="";
            document.getElementById("ext_refp_cod").value="";
            document.getElementById("cel_refp_cod").value="";
            document.getElementById("ciu_refp_cod").value="";
            document.getElementById("dep_refp_cod").value="";
            document.getElementById("tconocido_refp_cod").value="";
            document.getElementById("ocup_cod").value="";
            document.getElementById("act_econ_cod").value="";
            document.getElementById("nom_emp_cod").value="";
            document.getElementById("nit_emp_cod").value="";
            document.getElementById("dir_emp_cod").value="";
            document.getElementById("ciu_emp_cod").value="";
            document.getElementById("dep_emp_cod").value="";
            document.getElementById("tel_emp_cod").value="";
            document.getElementById("ext_emp_cod").value="";
            document.getElementById("car_emp_cod").value="";
            document.getElementById("f_ing_cod").value="";
            document.getElementById("tipo_cont_cod").value="";
            document.getElementById("sal_cod").value="";
            document.getElementById("otros_cod").value="";
            document.getElementById("conc_otros_cod").value="";
            document.getElementById("manuten_cod").value="";
            document.getElementById("cred_cod").value="";
            document.getElementById("arr_cod").value="";
            document.getElementById("pr_apellido_refam_cod").value="";
            document.getElementById("seg_apellido_refam_cod").value="";
            document.getElementById("pr_nombre_refam_cod").value="";
            document.getElementById("seg_nombre_refam_cod").value="";
            document.getElementById("tel1_refam_cod").value="";
            document.getElementById("tel2_refam_cod").value="";
            document.getElementById("ext_refam_cod").value="";
            document.getElementById("cel_refam_cod").value="";
            document.getElementById("ciu_refam_cod").value="";
            document.getElementById("dep_refam_cod").value="";
            // document.getElementById("tconocido_refam_cod").value="";
            xpand_div1();

        }else{
            document.getElementById("codeu").checked=true;
            document.getElementById("codeu2").checked=false;
        }
    }else{
        xpand_div1();
    }
}
function limpiarest(estado){
    /*if(estado=='B'){
            guardar();
        }*/
    var indice = document.getElementById("producto").selectedIndex;
    var producto=document.getElementById("producto").options[indice].value;

    if(producto!='01'){
        document.getElementById("pr_apellido_est").value="";
        document.getElementById("seg_apellido_est").value="";
        document.getElementById("pr_nombre_est").value="";
        document.getElementById("seg_nombre_est").value="";
        document.getElementById("genero_est") .value="";
        document.getElementById("tipo_id_est").value="";
        document.getElementById("id_est").value="";
        document.getElementById("f_nac_est").value="";
        document.getElementById("ciu_nac_est").value="";
        document.getElementById("dep_nac_est").value="";
        document.getElementById("dir_est").value="";
        document.getElementById("ciu_est").value="";
        document.getElementById("dep_est").value="";
        document.getElementById("tel_est").value="";
        document.getElementById("cel_est").value="";
        document.getElementById("mail_est").value="";
        document.getElementById("parentesco").value="";
        document.getElementById("nom_uni").value="";
        document.getElementById("nom_progr").value="";
        document.getElementById("f_ing_est").value="";
        document.getElementById("cod_est").value="";
        document.getElementById("sem_est").value="";
        document.getElementById("val_sem_est").value="";
        document.getElementById("tipo_carrera").value="";
        document.getElementById("trabaja").value="";
        document.getElementById("emp_est").value="";
        document.getElementById("dir_emp_est").value="";
        document.getElementById("tel_emp_est").value="";
        document.getElementById("sal_est").value="";
    }
    xpand_div1();
}

/****************************
 * autor :Iris vargas
 * parametros:
 * proposito: validar la Solicitud de Aval
 */
function validar_solicitud_aval(){

    var campos = "";
    var ncampos = "";
    //Datos de la Solicitud
    campos = new Array("fecha_cons","valor_solicitado","convenio","tipo_p" );
    ncampos = new Array("Fecha de consulta ","Valor solicitado","Convenio", "Tipo de Persona");

    for (i = 0; i < campos.length; i++){
        campo = campos[i];
        if (document.getElementById("formulario").elements[campo].value == ''){
            alert("El campo "+ncampos[i]+" esta vacio.!");
            document.getElementById("formulario").elements[campo].focus();
            return (false);
            break;
        }
    }
    if(document.getElementById("tipoconv").value=="Consumo"){
        var check =document.getElementById("aldia")!=null?document.getElementById("aldia").checked:false;
    

        if(check==true){
            if(document.getElementById("plazo").value!="1"||document.getElementById("forma_pago").value!="1"){
                alert("Para cheque al dia el plazo y plazo primera cuota deben se 1");
                return (false);
            }
        }

        if(document.getElementById("redescuento").value=="true" || document.getElementById("redescuento").value=="t"){
            if(document.getElementById("forma_pago").value=="1"){
                alert("El plazo primera cuota deben ser diferentes de 1");
                return (false);
            }
        }

        campos = new Array("afiliado","producto", "codigo2","codigo","codigo1", "valor_producto", "plazo", "cmbTituloValor", "forma_pago", "sector", "subsecttext" );
        ncampos = new Array("Afiliado","Producto","Codigo","Codigo","Codigo", "Valor Producto","Plazo", "Titulo Valor", "Plazo Primera Cuota", "Sector", "Subsector");

        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
    }
    if(document.getElementById("tipoconv").value=="Microcredito"){
        campos = new Array("asesor" );
        ncampos = new Array("Asesor");

        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
        var valorsol= $F("valor_solicitado").replace(/,/g, '');
        if(!(parseFloat(valorsol)>=(parseFloat($F("montomin"))*parseFloat($F("salariomin")))&&parseFloat(valorsol)<=(parseFloat($F("montomax"))*parseFloat($F("salariomin"))))){
            alert("El valor a financiar no se encuentra entre el monto minimo y maximo establecido en el convenio");           
            return (false);
        }
    }
    if(document.getElementById("producto").value=="02"){
        campos = new Array("servicio", "matricula");
        ncampos = new Array("Servicio ","Ciudad matricula");

        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
    }
    
    
   // Destino cr�dito
   if(document.getElementById("tipoconv").value=="Microcredito"){
       if (!document.getElementById("id_capital_trabajo").checked && !document.getElementById("id_activo_fijo").checked && !document.getElementById("compra_cartera").checked) {
       alert("Debe seleccionar una opci�n del destino cr�dito");
       return false;
   }
    }
   
    if(document.getElementById("tipo_p").value=="natural"){
        //Datos Persona Natural
        if(document.getElementById("producto").value=="01"){
            campos = new Array("pr_apellido_nat","pr_nombre_nat","genero_nat","est_civil_nat","tipo_id_nat","id_nat","f_exp_nat","dep_exp_nat","ciu_exp_nat","niv_est_nat","f_nac_nat",
                "dep_nac_nat","ciu_nac_nat","pcargo_nat","dir_nat","dep_nat","ciu_nat","tipo_viv_nat","tel_nat","mail_nat" );
            ncampos = new Array("Primer Apellido ","Primer Nombre","Genero","Estado Civil", "Tipo de Identificacion","Identificacion ","Fecha Expedicion","Departamento Expedicion","Ciudad Expedicion","Nivel de estudio", "Fecha de Nacimiento",
                "Departamento de Nacimiento","Ciudad de Nacimiento","Personas a cargo", "Direccion","Departamento ","Ciudad","Tipo de Vivienda", "Telefono","Email");

        }else{
            campos = new Array("pr_apellido_nat","pr_nombre_nat","genero_nat","est_civil_nat","tipo_id_nat","id_nat","f_exp_nat","dep_exp_nat","ciu_exp_nat","niv_est_nat","f_nac_nat",
                "dep_nac_nat","ciu_nac_nat","pcargo_nat","dir_nat","dep_nat","ciu_nat","barrio_nat","estr_nat","tipo_viv_nat","tres_nat","tel_nat","mail_nat");
            ncampos = new Array("Primer Apellido ","Primer Nombre","Genero","Estado Civil", "Tipo de Identificacion","Identificacion ","Fecha Expedicion","Departamento Expedicion","Ciudad Expedicion","Nivel de estudio", "Fecha de Nacimiento",
                "Departamento de Nacimiento","Ciudad de Nacimiento","Personas a cargo","Direccion","Departamento ","Ciudad","Barrio","Estrato","Tipo de Vivienda", "Tiempo de residencia", "Telefono","Email");
        }
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en los datos de Persona Natural, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
        if(validarEdad("f_nac_nat", 'Deudor')==false){
         
           return (false);
         
        }
        //Datos Persona Natural Informacion hijos
        /*if(document.getElementById("nhijos_nat").value>0){

            for( j=0;j<document.getElementById("nhijos_nat");j++){
                campos = new Array("nom_hij_nat"+j,"edad_hij_nat"+j,"dir_hij_nat"+j,"mail_hij_nat"+j, "tel_hij_nat"+j );
                ncampos = new Array("Nombre","Edad","Direccion","E-mail", "Telefono");

                for (i = 0; i < campos.length; i++){
                    campo = campos[i];
                    if (document.getElementById("formulario").elements[campo].value == ''){
                        alert("El campo "+ncampos[i]+" de la fila "+i+1+" en la informacion Hijos de Persona Natural, esta vacio.!");
                        document.getElementById("formulario").elements[campo].focus();
                        return (false);
                        break;
                    }
                }
            }
        }*/

        //Datos Persona Natural Informacion del conyuge
        if(document.getElementById("est_civil_nat").value=="C" || document.getElementById("est_civil_nat").value=="U"){

            campos = new Array("pr_apellido_con_nat","pr_nombre_con_nat","tipo_id_con_nat","id_con_nat" );
            ncampos = new Array("Primer Apellido","Primer Nombre","Tipo de Identificacion","Identificacion");
            for (i = 0; i < campos.length; i++){
                campo = campos[i];
                if (document.getElementById("formulario").elements[campo].value == ''){
                    alert("El campo "+ncampos[i]+" en los datos del conyuge, esta vacio.!");
                    document.getElementById("formulario").elements[campo].focus();
                    return (false);
                    break;
                }
            }
        }
        //Datos Persona Natural Informacion laboral
        
        if(document.getElementById("act_econ_nat").value!=''&&(document.getElementById("act_econ_nat").value=='PENSI'||document.getElementById("act_econ_nat").value=='HOGAR')){
            if(document.getElementById("producto").value=="01"){
                campos = new Array("sal_nat");
                ncampos = new Array( "Salario/Mesada/Ingreso mes");
            }else{
                campos = new Array("sal_nat","manuten_nat","cred_nat");
                ncampos = new Array( "Salario/Mesada/Ingreso mes",  "Gastos por manutension", "Gastos por credito");
            }
            for (i = 0; i < campos.length; i++){
                campo = campos[i];
                if (document.getElementById("formulario").elements[campo].value == ''){
                    alert("El campo "+ncampos[i]+" en la informacion laboral de persona natural, esta vacio.!");
                    document.getElementById("formulario").elements[campo].focus();
                    return (false);
                    break;
                }
            }
        }else{

            if(document.getElementById("act_econ_nat").value!=''&&(document.getElementById("act_econ_nat").value=='INDNFO'||document.getElementById("act_econ_nat").value=='PROIN')){
                if(document.getElementById("producto").value=="01"){
                    campos = new Array("act_econ_nat","ocup_nat","sal_nat");
                    ncampos = new Array("Actividad Economica","Ocupacion",   "Salario/Mesada/Ingreso mes");
                }else{
                    campos = new Array("act_econ_nat","ocup_nat","sal_nat","manuten_nat","cred_nat");
                    ncampos = new Array("Actividad Economica","Ocupacion",   "Salario/Mesada/Ingreso mes",  "Gastos por manutension", "Gastos por credito");
                   
                }
                for (i = 0; i < campos.length; i++){
                    campo = campos[i];
                    if (document.getElementById("formulario").elements[campo].value == ''){
                        alert("El campo "+ncampos[i]+" en la informacion laboral de persona natural, esta vacio.!");
                        document.getElementById("formulario").elements[campo].focus();
                        return (false);
                        break;
                    }
                }
            }else{

                if(document.getElementById("act_econ_nat").value!=''&&document.getElementById("act_econ_nat").value=='EPLDO'){
                    if(document.getElementById("producto").value=="01"){
                        campos = new Array("act_econ_nat","nom_emp_nat","dir_emp_nat","dep_emp_nat","ciu_emp_nat","tel_emp_nat","sal_nat");
                        ncampos = new Array("Actividad Economica","Nombre de la empresa","Direccion"," Departamento", "Ciudad", "Telefono",  "Salario/Mesada/Ingreso mes");
                    }else{
                        campos = new Array("act_econ_nat","nom_emp_nat","dir_emp_nat","dep_emp_nat","ciu_emp_nat","tel_emp_nat","sal_nat","manuten_nat","cred_nat");
                        ncampos = new Array("Actividad Economica","Nombre de la empresa","Direccion"," Departamento", "Ciudad", "Telefono", "Salario/Mesada/Ingreso mes",  "Gastos por manutension", "Gastos por credito");

                    }
                    for (i = 0; i < campos.length; i++){
                        campo = campos[i];
                        if (document.getElementById("formulario").elements[campo].value == ''){
                            alert("El campo "+ncampos[i]+" en la informacion laboral de persona natural, esta vacio.!");
                            document.getElementById("formulario").elements[campo].focus();
                            return (false);
                            break;
                        }
                    }
                }else{
                    if(document.getElementById("producto").value=="01"){
                        campos = new Array("act_econ_nat","ocup_nat","nom_emp_nat","dir_emp_nat","dep_emp_nat","ciu_emp_nat","tel_emp_nat","sal_nat");
                        ncampos = new Array("Actividad Economica","Ocupacion","Nombre de la empresa","Direccion"," Departamento", "Ciudad", "Telefono",  "Salario/Mesada/Ingreso mes");
                    }else{
                        campos = new Array("act_econ_nat","ocup_nat","nom_emp_nat","dir_emp_nat","dep_emp_nat","ciu_emp_nat","tel_emp_nat","sal_nat","manuten_nat","cred_nat");
                        ncampos = new Array("Actividad Economica","Ocupacion","Nombre de la empresa","Direccion"," Departamento", "Ciudad", "Telefono", "Salario/Mesada/Ingreso mes",  "Gastos por manutension", "Gastos por credito");

                    }
                    for (i = 0; i < campos.length; i++){
                        campo = campos[i];
                        if (document.getElementById("formulario").elements[campo].value == ''){
                            alert("El campo "+ncampos[i]+" en la informacion laboral de persona natural, esta vacio.!");
                            document.getElementById("formulario").elements[campo].focus();
                            return (false);
                            break;
                        }
                    }
                }
            }
        }
         if (document.getElementById("tipoconv").value == "Microcredito") {
            if (!document.getElementById("id_correo_electronico").checked && !document.getElementById("id_correspondencia").checked) {
                alert("Debe escoger una opci�n para el env�o de extracto de pago");
                return false;
            }

            let pasivos = document.getElementById("id_pasivos");
            let activos = document.getElementById("id_activos");

            if (pasivos.value === "" || parseInt(pasivos.value) < 0) {
                alert("El campo pasivos en la informacion laboral de persona natural, esta vacio o es menor a 0.!");
                return false;
            }

            if (activos.value === "" || parseInt(activos.value) < 0) {
                alert("El campo activos en la informacion laboral de persona natural, esta vacio o es menor a 0.!");
                return false;
            }

            if (document.getElementById("id_tipo_refc_de1").value === "") {
                alert("Debe escoger una opci�n en el campo Tipo referencia comercial.");
                document.getElementById("id_tipo_refc_de1").focus();
                return false;
            }

        }
        
        //Datos Persona Natural Referencias personales
        campos = new Array("pr_apellido_refp_nat1","pr_nombre_refp_nat1","tel1_refp_nat1", "cel_refp_nat1", "dep_refp_nat1","ciu_refp_nat1","dir_ref_nat1");
        ncampos = new Array("Primer Apellido ","Primer Nombre","Telefono", "Celular",  "Departamento","Ciudad", "Direcci�n");
        
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en la primera Referencia Personal de Persona Natural, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
        if(document.getElementById("producto").value==="04"){
            campos = new Array("pr_apellido_refp_nat2","pr_nombre_refp_nat2","tel1_refp_nat2", "cel_refp_nat2", "dep_refp_nat2","ciu_refp_nat2","dir_ref_nat2");
            ncampos = new Array("Primer Apellido ","Primer Nombre","Telefono", "Celular",  "Departamento","Ciudad", "Direcci�n");
            for (i = 0; i < campos.length; i++){
                campo = campos[i];
                if (document.getElementById("formulario").elements[campo].value == ''){
                    alert("El campo "+ncampos[i]+" en la segunda Referencia Personal de Persona Natural, esta vacio.!");
                    document.getElementById("formulario").elements[campo].focus();
                    return (false);
                    break;
                }
            }
        }
         //Datos Persona Natural Referencias Familiares
        campos = new Array("pr_apellido_refam_nat1","pr_nombre_refam_nat1","parent_refam_nat1","tel1_refam_nat1", "cel_refam_nat1","dep_refam_nat1","ciu_refam_nat1", "dir_refam_nat1");
        ncampos = new Array("Primer Apellido ","Primer Nombre","Parentesco","Telefono", "Celular", "Departamento","Ciudad" ,"Direcci�n");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value === ''){
                alert("El campo "+ncampos[i]+" en la primera Referencia Familiar de Persona Natural, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
        if(document.getElementById("producto").value==="04"){
            campos = new Array("pr_apellido_refam_nat2","pr_nombre_refam_nat2","parent_refam_nat2","tel1_refam_nat2", "cel_refam_nat2","dep_refam_nat2","ciu_refam_nat2", "dir_refam_nat2");
            ncampos = new Array("Primer Apellido ","Primer Nombre","tiempo de Conocido","Telefono", "Celular", "Departamento","Ciudad", "Direcci�n");
            for (i = 0; i < campos.length; i++){
                campo = campos[i];
                if (document.getElementById("formulario").elements[campo].value === ''){
                    alert("El campo "+ncampos[i]+" en la segunda Referencia Familiar de Persona Natural, esta vacio.!");
                    document.getElementById("formulario").elements[campo].focus();
                    return (false);
                    break;
                }
            }
        }
       
       // Datos del socio
       if (document.getElementById("id_tienesocios").value.toUpperCase() === "S") {
           campos = new Array("id_parcitipacion", "cedula_socio", "id_nomb_socio", "dir_neg_socio", "id_tel_socio");
           ncampos = new Array("% participaci�n", "C�dula socio", "Nombre socio", "Direcci�n socio", "Tel�fono socio");
           for (let i = 0; i < campos.length; i++) {
               campo = campos[i];               
               let element = document.getElementById(campo);
               
               if (element.value === "") {
                   alert("El campo " + ncampos[i] + " est� vac�o.");
                   element.focus();
                   return false;
               }
           }
       }
       
      
    }
    
    if(document.getElementById("tipo_p").value=="juridica"){
        //Datos Persona Juridica
        campos = new Array("razon_social","nit","ciiu","caracter","f_const","mail_jur","telefono1_jur","dir_jur","dep_jur","ciu_jur","cel_jur","rep_legal",
            "gen_repr","tipo_id_repr","id_repr","autorizado","gen_autor","tipo_id_autor","id_autor" );
        ncampos = new Array("Razon Social ","Nit","Ciiu","Caracter de la empresa","Fecha contitucion","E-mail", "Telefono","Direccion","Departamento","Ciudad","Celular", "Representante Legal",
            "Genero Rep. Legal","Tipo Identificacion Rep Legal","Identificacion Rep. Legal","Firmador de Cheque","Genero Firm. de Cheque", "Tipo Identificacion Firm. de Cheque","Indentificacion Firm. de Cheque ");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en los datos de Persona Juridica, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }

        //Datos Persona Juridica Referencia Comercial
        campos = new Array("raz_soc_ref1","dep_raz_soc_1","ciu_raz_soc_1","raz_soc_tel11");
        ncampos = new Array("Razon Social ","Departamento","Ciudad", "Telefono");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en la primera Referencia Comercial de Persona Juridica, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }

    }

    var check =document.getElementById("codeu").checked;
    
    if(check==true){
        //Datos del Codeudor
        campos = new Array("pr_apellido_cod","pr_nombre_cod","genero_cod","est_civil_cod","tipo_id_cod","id_cod","f_exp_cod","dep_exp_cod","ciu_exp_cod","niv_est_cod","f_nac_cod",
            "dep_nac_cod","ciu_nac_cod","pcargo_cod","dir_cod","dep_cod","ciu_cod","barrio_cod","estr_cod","tipo_viv_cod","tres_cod","tel_cod","mail_cod" );
        ncampos = new Array("Primer Apellido ","Primer Nombre","Genero","Estado Civil", "Tipo de Identificacion","Identificacion ","Fecha Expedicion","Departamento Expedicion","Ciudad Expedicion","Nivel de estudio", "Fecha de Nacimiento",
            "Departamento de Nacimiento","Ciudad de Nacimiento","Personas a cargo", "Direccion","Departamento ","Ciudad","Barrio","Estrato","Tipo de Vivienda", "Tiempo de residencia", "Telefono","Email deudor solidario");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];

            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en los datos del Codeudor, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
       
       if(validarEdad("f_nac_cod", 'Codeudor')==false){
           
           return (false);                     
       }

        //Datos del Codeudor Informacion hijos
        /*if(document.getElementById("nhijos_cod").value>0){

            for(var j=0;j<document.getElementById("nhijos_cod");j++){
                campos = new Array("nom_hij_cod"+j,"edad_hij_cod"+j,"dir_hij_cod"+j,"mail_hij_cod"+j, "tel_hij_cod"+j );
                ncampos = new Array("Nombre","Edad","Direccion","E-mail", "Telefono");

                for (i = 0; i < campos.length; i++){
                    campo = campos[i];
                    alert(campo);
                    if (document.getElementById("formulario").elements[campo].value == ''){
                        alert("El campo "+ncampos[i]+" de la fila "+i+1+" en la informacion Hijos del Codeudor, esta vacio.!");
                        document.getElementById("formulario").elements[campo].focus();
                        return (false);
                        break;
                    }
                }
            }
        }*/


        //Datos del Codeudor Informacion laboral        
        if(document.getElementById("act_econ_cod").value!=''&&(document.getElementById("act_econ_cod").value=='PENSI' ||document.getElementById("act_econ_cod").value=='HOGAR')){
            campos = new Array("sal_cod","manuten_cod","cred_cod");
            ncampos = new Array("Salario/Mesada/Ingreso mes",  "Gastos por manutension", "Gastos por credito");
            for (i = 0; i < campos.length; i++){
                campo = campos[i];
                if (document.getElementById("formulario").elements[campo].value == ''){
                    alert("El campo "+ncampos[i]+" en la informacion laboral del codeudor, esta vacio.!");
                    document.getElementById("formulario").elements[campo].focus();
                    return (false);
                    break;
                }
            }
        }else{

            if(document.getElementById("act_econ_cod").value!=''&&(document.getElementById("act_econ_cod").value=='INDNFO'||document.getElementById("act_econ_cod").value=='PROIN')){
                campos = new Array("act_econ_cod","ocup_cod","sal_cod","manuten_cod","cred_cod");
                ncampos = new Array("Actividad Economica","Ocupacion",   "Salario/Mesada/Ingreso mes",  "Gastos por manutension", "Gastos por credito");
                for (i = 0; i < campos.length; i++){
                    campo = campos[i];
                    if (document.getElementById("formulario").elements[campo].value == ''){
                        alert("El campo "+ncampos[i]+" en la informacion laboral del codeudor, esta vacio.!");
                        document.getElementById("formulario").elements[campo].focus();
                        return (false);
                        break;
                    }
                }
            }else{

                if(document.getElementById("act_econ_cod").value!=''&&document.getElementById("act_econ_cod").value=='EPLDO'){

                    campos = new Array("act_econ_cod","nom_emp_cod","dir_emp_cod","dep_emp_cod","ciu_emp_cod","tel_emp_cod","sal_cod","manuten_cod","cred_cod");
                    ncampos = new Array("Actividad Economica","Nombre de la empresa","Direccion"," Departamento", "Ciudad", "Telefono","Salario/Mesada/Ingreso mes",  "Gastos por manutension", "Gastos por credito");
                    for (i = 0; i < campos.length; i++){
                        campo = campos[i];
                        if (document.getElementById("formulario").elements[campo].value == ''){
                            alert("El campo "+ncampos[i]+" en la informacion laboral del codeudor, esta vacio.!");
                            document.getElementById("formulario").elements[campo].focus();
                            return (false);
                            break;
                        }
                    }
                }else{

                    campos = new Array("act_econ_cod","ocup_cod","nom_emp_cod","dir_emp_cod","dep_emp_cod","ciu_emp_cod","tel_emp_cod","sal_cod","manuten_cod","cred_cod");
                    ncampos = new Array("Actividad Economica","Ocupacion","Nombre de la empresa","Direccion"," Departamento", "Ciudad", "Telefono", "Salario/Mesada/Ingreso mes",  "Gastos por manutension", "Gastos por credito");
                    for (i = 0; i < campos.length; i++){
                        campo = campos[i];
                        if (document.getElementById("formulario").elements[campo].value == ''){
                            alert("El campo "+ncampos[i]+" en la informacion laboral del codeudor, esta vacio.!");
                            document.getElementById("formulario").elements[campo].focus();
                            return (false);
                            break;
                        }
                    }
                }
            }
        }
//        if(document.getElementById("otros_cod").value!=''&&document.getElementById("otros_cod").value>0&&document.getElementById("conc_otros_cod").value==''){
//            alert("El campo Concepto Otros ingresos en la informacion laboral del codeudor, esta vacio.!");
//        }
        if(document.getElementById("tipo_viv_cod").value=='02'&&(document.getElementById("arr_cod").value==''||parseInt(document.getElementById("arr_cod").value)<=0)){
            alert("El campo Gastos por arriendo en la informacion laboral del codeudor, esta vacio o es menor a 0.!");
        }
        //Datos del Codeudor Referencias personales
        campos = new Array("pr_apellido_refp_cod1","pr_nombre_refp_cod1","tel1_refp_cod1","dep_refp_cod1","ciu_refp_cod1");
        ncampos = new Array("Primer Apellido ","Primer Nombre","Telefono", "Departamento","Ciudad ");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en la primera Referencia Personal del Codeudor, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }

        //Datos del Codeudor Referencias Familiares
        campos = new Array("pr_apellido_refam_cod1","pr_nombre_refam_cod1","parent_refam_cod1","tel1_refam_cod1","dep_refam_cod1","ciu_refam_cod1");
        ncampos = new Array("Primer Apellido ","Primer Nombre","Parentesco","Telefono", "Departamento","Ciudad ");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en la primera Referencia Familiar del Codeudor, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
    }
    //Datos del Estudiante
    var producto="";
    if(document.getElementById("tipoconv").value=="Consumo"){    
        var indice = document.getElementById("producto").selectedIndex;
        producto=document.getElementById("producto").options[indice].value;
    }
    if(document.getElementById("tipoconv").value=="Microcredito"){
        producto=document.getElementById("producto").value;
    }
    if(producto=='01'){
        campos = new Array("pr_apellido_est","pr_nombre_est","genero_est","est_civil_est","tipo_id_est","id_est","f_exp_est","dep_exp_est","ciu_exp_est","f_nac_est",
            "dep_nac_est","ciu_nac_est","dir_est","dep_est","ciu_est","tel_est",
            "parentesco","nom_uni","nom_progr","cod_est","sem_est","tipo_carrera","trabaja");
        ncampos = new Array("Primer Apellido ","Primer Nombre","Genero","Estado Civil", "Tipo de Identificacion","Identificacion ","Fecha Expedicion","Departamento Expedicion","Ciudad Expedicion","Fecha de Nacimiento",
            "Departamento de Nacimiento","Ciudad de Nacimiento", "Direccion","Departamento ","Ciudad", "Telefono",
            "Parentesco con el Girador","Universidad","Programa","Codigo","Semestre", "Tipo de Carrera","Trabaja");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en los datos del Estudiante, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
        if(document.getElementById("trabaja").value=="S"){
            campos = new Array("dir_emp_est","tel_emp_est","sal_est" );
            ncampos = new Array("Direccion Empresa","telefono Empresa", "Salario/Mesada/Ingreso mes");
            for (i = 0; i < campos.length; i++){
                campo = campos[i];
                if (document.getElementById("formulario").elements[campo].value == ''){
                    alert("El campo "+ncampos[i]+" en los datos del Estudiante, esta vacio.!");
                    document.getElementById("formulario").elements[campo].focus();
                    return (false);
                    break;
                }
            }
        }

    //Datos de los hijos del  Estudiante
    /* if(document.getElementById("nhijos_est").value>0){

            for( j=0;j<document.getElementById("nhijos_est");j++){
                campos = new Array("nom_hij_est"+j,"edad_hij_est"+j,"dir_hij_est"+j,"mail_hij_est"+j, "tel_hij_est"+j );
                ncampos = new Array("Nombre","Edad","Direccion","E-mail", "Telefono");

                for (i = 0; i < campos.length; i++){
                    campo = campos[i];
                    if (document.getElementById("formulario").elements[campo].value == ''){
                        alert("El campo "+ncampos[i]+" de la fila "+j+1+" en la informacion Hijos del Estudiante, esta vacio.!");
                        document.getElementById("formulario").elements[campo].focus();
                        return (false);
                        break;
                    }
                }
            }
        }*/

    }
    

    //Datos de Cuenta
    var tituloValor=document.getElementById("cmbTituloValor");
    var selected = (tituloValor!=null) ? tituloValor.options[tituloValor.selectedIndex].text :"...";
    if(selected=="Cheque"){
    campos = new Array("tipo_cuenta1","banco1","cuenta1");
    ncampos = new Array("Tipo de Cuenta","Banco","Numero de Cuenta");

    for (i = 0; i < campos.length; i++){
        campo = campos[i];
        if (document.getElementById("formulario").elements[campo].value == ''){
            alert("El campo "+ncampos[i]+" en la fila 1 en la informacion de la cuenta, esta vacio.!");
            document.getElementById("formulario").elements[campo].focus();
            return (false);
            break;
        }
    }
    }

    if(document.getElementById("tipoconv").value=="Microcredito"){
        if(document.getElementById("tipo_p").value=="natural"&&document.getElementById("trans").value=="N"){
            campos = new Array("nombre_neg", "dir_neg","dep_neg", "ciu_neg", "barrio_neg", "telefono_neg", "id_tipo_actividad","id_cam_comercio","id_local","id_tipo_negocio");
            ncampos = new Array("Nombre","Direccion", "Departamento", "Ciudad", "Barrio", "Telefono", "Tipo actividad","Camara comercio","Local","Tipo negocio");

            for (i = 0; i < campos.length; i++){
                campo = campos[i];
                if (document.getElementById("formulario").elements[campo].value == ''){
                    alert("El campo "+ncampos[i]+" en los datos del Negocio,  esta vacio.!");
                    document.getElementById("formulario").elements[campo].focus();
                    return (false);
                    break;
                }
            }

                 //Datos Persona Natural Referencia Comercial Negocio
            campos = new Array("raz_soc_ref1","dep_raz_soc_1","ciu_raz_soc_1","raz_soc_tel11");
            ncampos = new Array("Razon Social ","Departamento","Ciudad", "Telefono");
            for (i = 0; i < campos.length; i++){
                campo = campos[i];
                if (document.getElementById("formulario").elements[campo].value == ''){
                    alert("El campo "+ncampos[i]+" en la primera Referencia Comercial del Negocio, esta vacio.!");
                    document.getElementById("formulario").elements[campo].focus();
                    return (false);
                    break;
                }
            }
        }

        if(document.getElementById("trans").value=="N"){
            campos = new Array("sector", "subsector","tiempo_local", "num_exp_negocio", "tiempo_microempresario", "num_trabajadores");
            ncampos = new Array("Sector","Subsector", "A�os local actual"," A�os exp. negocio actual", "A�os microempresario", "No. trabajadores");

            for (i = 0; i < campos.length; i++){
                campo = campos[i];
                if (document.getElementById("formulario").elements[campo].value == ''){
                    alert("El campo "+ncampos[i]+" en los datos del Negocio, esta vacio.!");
                    document.getElementById("formulario").elements[campo].focus();
                    return (false);
                    break;
                }
            }
        }
        
        //validacion formulario microcredito
        if (document.getElementById("compra_cartera").checked) {
            campos = new Array("entidad", "nit_prov", "tcuenta_prov", "cuenta_prov", "valor_recoger");
            var contador=0;
            var contador2=0;
            for (i = 1; i < 5; i++) {
                for (j = 0; j < campos.length; j++) {
                    campo = campos[j] + i;
                    if (document.getElementById("formulario").elements[campo].value !== '') {
                        contador++;
                    }
                }
                
            }            
            //validamos que el contador sea 5
            if (contador < 5 || (contador > 5 && contador < 10) || (contador > 10 && contador < 15) || (contador > 15 && contador < 20)) {
                alert("Debe diligenciar todos los campos de la entidad para la compra de cartera");
                return ;
            }
        }
    }
   

    var accion = controller+"?estado=GestionSolicitud&accion=Aval";
    //iris  agregar a la url &vista=1 para cuando sea modificacion
    var valid = new Validation('formulario');
    if(valid.validate()){
        $('formulario').action = accion + '&opcion=ins_data&dato=P';
        hab();
        if( document.getElementById("vista").value=="20")
            {
                alert(mensaje());
            }
        document.getElementById("formulario").submit();
    }else{
        alert("una o mas fechas en el formulario son invalidas");
    }
}

function hab() {
                    frm = document.forms['formulario'];
                    for(i=0; ele=frm.elements[i]; i++)
                        ele.disabled=false;
                }

function limpiarest2(estado){
    /*if(estado=='B'){
            guardar2();
        }*/
    var indice = document.getElementById("producto").selectedIndex;
    var producto=document.getElementById("producto").options[indice].value;

    if(producto!='01'){
        document.getElementById("pr_apellido_est").value="";
        document.getElementById("seg_apellido_est").value="";
        document.getElementById("pr_nombre_est").value="";
        document.getElementById("seg_nombre_est").value="";
        document.getElementById("genero_est") .value="";
        document.getElementById("tipo_id_est").value="";
        document.getElementById("id_est").value="";
        document.getElementById("f_nac_est").value="";
        document.getElementById("dir_est").value="";
        document.getElementById("ciu_est").value="";
        document.getElementById("dep_est").value="";
        document.getElementById("tel_est").value="";
        document.getElementById("cel_est").value="";
        document.getElementById("mail_est").value="";
        document.getElementById("nom_uni").value="";
        document.getElementById("nom_progr").value="";
        document.getElementById("cod_est").value="";
        document.getElementById("sem_est").value="";

    }
    xpand_div1();
}

function limpiarcod2(estado){
    var check =document.getElementById("codeu").checked;
    if(estado=='B'){
        guardar2();
    }
    if(check!=true){
        if (confirm("Est� seguro que no requiere la informaci�n del codeudor?")) {
            document.getElementById("pr_apellido_cod").value="";
            document.getElementById("seg_apellido_cod").value="";
            document.getElementById("pr_nombre_cod").value="";
            document.getElementById("seg_nombre_cod").value="";
            document.getElementById("genero_cod").value="";
            document.getElementById("tipo_id_cod").value="";
            document.getElementById("id_cod").value="";
            document.getElementById("f_nac_cod").value="";
            document.getElementById("dir_cod").value="";
            document.getElementById("ciu_cod").value="";
            document.getElementById("dep_cod").value="";
            document.getElementById("tel_cod").value="";
            document.getElementById("dir_emp_cod").value="";
            document.getElementById("ciu_emp_cod").value="";
            document.getElementById("dep_emp_cod").value="";
            document.getElementById("tel_emp_cod").value="";
            document.getElementById("ext_emp_cod").value="";
            xpand_div1();

        }else{
            document.getElementById("codeu").checked=true;
        }
    }else{
        xpand_div1();
    }
}

/****************************
 * autor :Iris vargas
 * parametros:
 * proposito: validar la Solicitud de Aval simple
 */
function validar_solicitud_aval2(){

    var campos = "";
    var ncampos = "";
    //Datos de la Solicitud
    campos = new Array("fecha_cons","valor_solicitado","afiliado","convenio","producto","codigo2","codigo","codigo1","tipo_p","valor_producto" , "plazo", "cmbTituloValor", "forma_pago", "sector", "subsecttext" );
    ncampos = new Array("Fecha de consulta ","Valor solicitado","Afiliado","Convenio","Producto","Codigo","Codigo","Codigo", "Tipo de Persona","Valor Producto","Plazo", "Titulo Valor", "Plazo Primera Cuota", "Sector", "Subsector");

    for (i = 0; i < campos.length; i++){
        campo = campos[i];
        if (document.getElementById("formulario").elements[campo].value == ''){
            alert("El campo "+ncampos[i]+" esta vacio.!");
            document.getElementById("formulario").elements[campo].focus();
            return (false);
            break;
        }
    }
        
    var check =document.getElementById("aldia")!=null?document.getElementById("aldia").checked:false;

    if(check==true){
        if(document.getElementById("plazo").value!="1"||document.getElementById("forma_pago").value!="1"){
            alert("Para cheque al dia el plazo y plazo primera cuota deben se 1");
            return (false);
        }
    }

     if(document.getElementById("redescuento").value=="true"|| document.getElementById("redescuento").value=="t"){
        if(document.getElementById("forma_pago").value=="1"){
             alert("El plazo primera cuota deben ser diferentes de 1");
            return (false);
        }
    }    
    
    if(document.getElementById("producto").value=="02"){
        campos = new Array("servicio", "matricula");
        ncampos = new Array("Servicio ","Ciudad matricula");

        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
    }
    if(document.getElementById("tipo_p").value=="natural"){
        //Datos Persona Natural
        campos = new Array("pr_apellido_nat","pr_nombre_nat","genero_nat","tipo_id_nat","id_nat","f_nac_nat", "tipo_viv_nat",
            "dir_nat","dep_nat","ciu_nat","tel_nat" );
        ncampos = new Array("Primer Apellido ","Primer Nombre","Genero","Tipo de Identificacion","Identificacion ", "Fecha de Nacimiento","Tipo de Vivienda",
            "Direccion","Departamento ","Ciudad","Telefono");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en los datos de Persona Natural, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }      

        //Datos Persona Natural Informacion laboral      

        campos = new Array("dir_emp_nat","dep_emp_nat","ciu_emp_nat","tel_emp_nat","sal_nat");
        ncampos = new Array("Direccion"," Departamento", "Ciudad", "Telefono", "Salario/Mesada/Ingreso mes");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en la informacion laboral de persona natural, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
    }
  
    if(document.getElementById("tipo_p").value=="juridica"){
        //Datos Persona Juridica
        campos = new Array("razon_social","nit","mail_jur","telefono1_jur","dir_jur","dep_jur","ciu_jur","cel_jur","rep_legal",
            "gen_repr","tipo_id_repr","id_repr" );
        ncampos = new Array("Razon Social ","Nit","E-mail", "Telefono","Direccion","Departamento","Ciudad","Celular", "Representante Legal",
            "Genero Rep. Legal","Tipo Identificacion Rep Legal","Identificacion Rep. Legal");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en los datos de Persona Juridica, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }      

    }

    var check =document.getElementById("codeu").checked;
    if(check==true){
        //Datos del Codeudor
        campos = new Array("pr_apellido_cod","pr_nombre_cod","genero_cod","tipo_id_cod","id_cod","f_nac_cod","tipo_viv_cod",
            "dir_cod","dep_cod","ciu_cod","tel_cod" );
        ncampos = new Array("Primer Apellido ","Primer Nombre","Genero", "Tipo de Identificacion","Identificacion ","Fecha de Nacimiento","Tipo de Vivienda",
            "Direccion","Departamento ","Ciudad","Telefono");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];

            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en los datos del Codeudor, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
       
        //Datos del Codeudor Informacion laboral
     

        campos = new Array("dir_emp_cod","dep_emp_cod","ciu_emp_cod","tel_emp_cod","sal_cod");
        ncampos = new Array("Direccion"," Departamento", "Ciudad", "Telefono", "Salario/Mesada/Ingreso mes");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en la informacion laboral del codeudor, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
            
        }
    }

    //Datos del Estudiante
    var indice = document.getElementById("producto").selectedIndex;
    var producto=document.getElementById("producto").options[indice].value;

    if(producto=='01'){
        campos = new Array("pr_apellido_est","pr_nombre_est","genero_est","tipo_id_est","id_est","f_nac_est","tipo_viv_est",
            "dir_est","dep_est","ciu_est","tel_est",
            "nom_uni","nom_progr","cod_est","sem_est");
        ncampos = new Array("Primer Apellido ","Primer Nombre","Genero","Tipo de Identificacion","Identificacion ","Fecha de Nacimiento","Tipo de vivienda",
            "Direccion","Departamento ","Ciudad", "Telefono",
            "Universidad","Programa","Codigo","Semestre");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en los datos del Estudiante, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
    }
    
    var accion = controller+"?estado=GestionSolicitud&accion=Aval";
    //iris  agregar a la url &vista=1 para cuando sea modificacion
    var valid = new Validation('formulario');
    if(valid.validate()){
        $('formulario').action = accion + '&opcion=ins_data2&dato=P';
        document.getElementById("formulario").submit();
    }else{
        alert("una o mas fechas en el formulario son invalidas");
    }
}

function maximizar(){
    //Maximiza la ventana
    window.moveTo(0,0);
    if (document.all) {
        top.window.resizeTo(screen.availWidth,screen.availHeight);
    }
    else if (document.layers||document.getElementById) {
        if(top.window.outerHeight<screen.availHeight||
            top.window.outerWidth<screen.availWidth){
            top.window.outerHeight = screen.availHeight;
            top.window.outerWidth = screen.availWidth;
        }
    }
}

function cargarValorProd(){
    var valor_sol=document.getElementById("valor_solicitado").value;
    var valor_prod=document.getElementById("valor_producto").value;
    document.getElementById("valor_producto").value=valor_sol;
    
}
function cargarValorSolicitado(){
    var valor_sol=document.getElementById("valor_solicitado").value;
    var valor_prod=document.getElementById("valor_producto").value;
    var producto=document.getElementById("producto").value;
   
    if(producto == "01"){
      document.getElementById("valor_solicitado").value=valor_prod;
    }
   
    
}

function cargarConvenios(ind){
    var afiliado = $('afiliado').options[ind].value;
    var url = controller+"?estado=GestionSolicitud&accion=Aval";
    var p = "opcion=cargarconvenios&afiliado="+afiliado;
    new Ajax.Request(url, {
        parameters: p,
        method: 'post',
        onComplete:  function (resp){            
            document.getElementById("convenios").innerHTML = resp.responseText;
            findcod(afiliado);
        }
    });

}



function cargarServicios(ind){
    var producto = $('producto').options[ind].value;
    if(producto!='02'){
        $('servicio').value="";
        $('matricula').value="";
        $('servicio').disabled=true;
        $('matricula').disabled=true;

    }else{
        $('servicio').disabled=false;
        $('matricula').disabled=false;
        var url = controller+"?estado=GestionSolicitud&accion=Aval";
        var p = "opcion=cargarservicios&producto="+producto;
        new Ajax.Request(url, {
            parameters: p,
            method: 'post',
            onComplete:  function (resp){
                document.getElementById("d_servicio").innerHTML = resp.responseText;
                cargarMatricula(producto);

            }
        });
    }
}

function cargarMontos(ind){
    var convenio = $('convenio').options[ind].value;
    var url = controller+"?estado=GestionSolicitud&accion=Aval";
    var p = "opcion=cargarmontos&convenio="+convenio;
    new Ajax.Request(url, {
        parameters: p,
        method: 'post',
        onComplete:  function (resp){
            var texto = resp.responseText;
            var array = texto.split(";_;");
            $('montomin').value = array[0];
            $('montomax').value = array[1];
            cargarasesores(convenio);
        }
    });
}

function cargarasesores(convenio) {
    var url = controller + "?estado=GestionSolicitud&accion=Aval";
    var p = "opcion=cargarAserores&convenio=" + convenio;
    new Ajax.Request(url, {
        parameters: p,
        method: 'post',
        dataType: 'json',
        onComplete: function (resp) {
            console.log(resp.responseText);
            document.getElementById("asesor").innerHTML = resp.responseText;
        }
    });
}

function cargarMatricula(producto){
    var url = controller+"?estado=GestionSolicitud&accion=Aval";
    var p = "opcion=cargarmatricula&producto="+producto;
    new Ajax.Request(url, {
        parameters: p,
        method: 'post',
        onComplete:  function (resp){
            document.getElementById("d_matricula").innerHTML = resp.responseText;

        }
    });
}

function findcod(afil){
    var url = controller+"?estado=GestionSolicitud&accion=Aval";
    var p =  "opcion=buscar_cod_afil&afil="+afil;
    new Ajax.Request(url, {
        parameters: p,
        method:'post',
        onComplete:finish
    });
}
function finish(resp){
    var texto = resp.responseText;
    var array = texto.split(";_;");
    $('codigo').value = array[0];
    $('codigo1').value = array[1];
    $('codigo2').value = array[2];
}

var win;
function referenciar(baseurl,negocio, form){
    win= new Window(
    {
        id: "diving",
        title: "Concepto",
        url:baseurl+'/jsp/fenalco/negocios/Conceptos.jsp?negocio='+negocio+"&form="+form+"&act=REF",
        showEffectOptions: {
            duration:1
        },
        hideEffectOptions: {
            duration:1
        },
        destroyOnClose: true,
        resizable: true,
        width:800,
        height:400
    });
    win.show(true);
    win.showCenter();
}

function closewin(){
    win.close();
}

function validarMax(id, max){
    var valor = document.getElementById(id).value;
    if(valor>max){
        alert("el numero digitado debe ser menor o igual a "+max);
        document.getElementById(id).value="";
    }
}

function buscarSubs(){

    var sel = document.getElementById("sector").selectedIndex;
    var valor = document.getElementById("sector").options[sel].value;
    var url = controller+"?estado=Proveedor&accion=Ingresar";
    var p =  'opcion=buscarsub&idsect='+valor;
    new Ajax.Request(
        url,
        {
            method: 'post',
            parameters: p,
            onComplete: function (resp){
                document.getElementById("subsectores").innerHTML = '<select name="subsector" id="subsector" style="width: 100%;">'+resp.responseText+'</select>';

            }
        }
        );
}

function validarEdad(id, nombre){
    var fechanac = document.getElementById(id).value;
    fecha = new Date(fechanac.replace(/-/ig, "/"));
    hoy = new Date();
    var diff = hoy.getTime() - fecha.getTime();
    ed = parseInt((diff)/365.2422/24/60/60/1000);
    if(!( ed >=18 && ed <75)){
        alert (nombre +" excede la edad para ser sujeto de credito");
        return false;
    }

}



function cargarCodigo(ind){
    var afiliado = $('afiliado').options[ind].value;
    findcod(afiliado);
}


function verConvsUsuario(controller){
    contr=controller;
    var selected = document.getElementById("afiliado").selectedIndex;
    var filtro = document.getElementById("afiliado").options[selected].value;
    var p, url = "";

    url = controller+"?estado=GestionSolicitud&accion=Aval";
    p =  'opcion=busConvUsuario&nitprov='+filtro;

    new Ajax.Request(
        url,
        {
            method: 'post',
            parameters: p,
            onComplete: openWin
        }
        );
}
function verConvs(controller){
    contr=controller;
    var selected = document.getElementById("afiliado").selectedIndex;
    var filtro = document.getElementById("afiliado").options[selected].value;
    var p, url = "";

    url = controller+"?estado=GestionSolicitud&accion=Aval";
    p =  'opcion=busConv&nitprov='+filtro;

    new Ajax.Request(
        url,
        {
            method: 'post',
            parameters: p,
            onComplete: openWin
        }
        );
}

var win2;
function openWin(response){
    $('contenido2').innerHTML=response.responseText;
    $('contenido2').style.display='block';
    win2= new Window(
    {
        id: "conv",
        title: "Convenios",
        showEffectOptions: {
            duration:1
        },
        hideEffectOptions: {
            duration:1
        },
        destroyOnClose: true,
        onClose:closewin,
        width:510,
        height:200,
        resizable: true
    });
    win2.show(true);
    win2.showCenter();
    win2.setContent('contenido2', true, false);
    $('contenido2').style.visibility='visible';
}

function colocar(convenio, sector, subsector, nomConvenio, nomSector, nomsubsector, facturaTercero, nitTercero, redescuento){
    document.getElementById("redescuento").value = redescuento;
    document.getElementById("convtext").value = nomConvenio;
    document.getElementById("convenio").value = convenio;
    document.getElementById("sector").value = nomSector;
    document.getElementById("sectid").value = sector;
    document.getElementById("subsecttext").value = nomsubsector;
    document.getElementById("subsectid").value = subsector;
    win2.close();
    cargarTitulosValor(convenio);
    validaraldia();
}


function closewin(){
    $('contenido2').style.display='none';
    $('contenido2').style.visibility='hidden';
    $('contenido2').innerHTML="";
}

function cerrarDiv(){
    win2.close();
}

function limpiarConv(){
    document.getElementById("redescuento").value ="";
    document.getElementById("convtext").value="";
    document.getElementById("convenio").value ="";
    document.getElementById("sector").value ="";
    document.getElementById("sectid").value ="";
    document.getElementById("subsecttext").value ="";
    document.getElementById("subsectid").value="";
    document.getElementById("cmbTituloValor").value="";
}

function cargarTitulosValor(convenio){
    var url =contr+"?estado=GestionSolicitud&accion=Aval";
    var p =  'opcion=buscarTitVlrConv&idConvenio='+convenio;
    new Ajax.Request(
        url,
        {
            method: 'post',
            parameters: p,
            onSuccess: function(transport){
                $("cmbTituloValor").update(transport.responseText);
            }
        }
        );
}

function validaraldia(){
    var selected = document.getElementById("cmbTituloValor").selectedIndex;
    var titval = document.getElementById("cmbTituloValor").options[selected].value;
    var check =document.getElementById("aldia").checked;
    var redesc=$('redescuento').value;
    if(titval=="01"&&(redesc=="false"||redesc=="f")){
        $('aldia').disabled=false;
        if(check==true){
            $('forma_pago').value="1";
            $('forma_pago').disabled=true;
            $('plazo').value="1";
            $('plazo').disabled=true;
        }else{
            $('forma_pago').disabled=false;
            $('plazo').disabled=false;
            if($('forma_pago').value=="1"){
                alert("La forma de pago debe ser diferente de 1 dia");
                $('forma_pago').value="";               
            }
             if($('plazo').value=="1"){
                alert("El plazo debe ser diferente de 1");
                $('plazo').value="";
                }
        }
    }else{
        if(check==true){
            $('forma_pago').value="";
            $('forma_pago').disabled=false;
            $('plazo').value="";
            $('plazo').disabled=false;
        }
        $('aldia').checked=false;
        $('aldia').disabled="disabled";
        if($('forma_pago').value=="1"){
                alert("La forma de pago debe ser diferente de 1 dia");
                $('forma_pago').value="";               
            }
         

    }
}

function valida_campos(op) {


    switch (op)
    {
     
        case 1:
            deshab();
            habilitar_solicitante(false);
            habilitar_estudiante(false)
            habilitar_codeudor(false);
            habilita_ref_p(false) ;
            habilita_ref_f(false) ;
            habilita_cuantas(false);
            break;
        case 2:
            deshab();
            habilita_cabecera(false);
            habilitar_codeudor(false);
            habilita_ref_p(false) ;
            habilita_ref_f(false) ;
            habilita_cuantas(false);
            break;       
    }
}

function habilitar_estudiante(disabled){
    document.getElementById("seg_apellido_est").disabled=disabled; 
    document.getElementById("seg_nombre_est").disabled=disabled; 
    
}

function habilitar_solicitante(disabled){
     document.getElementById("seg_apellido_nat").disabled=disabled; 
     document.getElementById("seg_nombre_nat").disabled=disabled; 
    
}

function habilitar_codeudor(disabled){
  var check =document.getElementById("codeu").checked;
  var estado=document.getElementById("pr_apellido_cod").value;
  
  if(check == true && estado == ""){
     
            
            document.getElementById("pr_apellido_cod").disabled=false;
            document.getElementById("seg_apellido_cod").disabled=false;
            document.getElementById("pr_nombre_cod").disabled=false;
            document.getElementById("seg_nombre_cod").disabled=false;
            document.getElementById("genero_cod").disabled=false;
            document.getElementById("tipo_id_cod").disabled=false;
            document.getElementById("id_cod").disabled=false;
            document.getElementById("f_exp_cod").disabled=false;
            document.getElementById("ciu_exp_cod").disabled=false;
            document.getElementById("dep_exp_cod").disabled=false;
            document.getElementById("f_nac_cod").disabled=false;
            document.getElementById("ciu_nac_cod").disabled=false;
            document.getElementById("dep_nac_cod").disabled=false;
            document.getElementById("est_civil_cod").disabled=false;
            document.getElementById("niv_est_cod").disabled=false;
            document.getElementById("prof_cod").disabled=false;
            document.getElementById("pcargo_cod").disabled=false;
            document.getElementById("nhijos_cod").disabled=false;
            document.getElementById("ngrupo_cod").disabled=false;
            document.getElementById("dir_cod").disabled=false;
            document.getElementById("ciu_cod").disabled=false;
            document.getElementById("dep_cod").disabled=false;
            document.getElementById("barrio_cod").disabled=false;
            document.getElementById("estr_cod").disabled=false;
            document.getElementById("tel_cod").disabled=false;
            document.getElementById("tipo_viv_cod").disabled=false;
            document.getElementById("tres_cod").disabled=false;
            document.getElementById("tres_cod1").disabled=false;
            document.getElementById("cel_cod").disabled=false;
            document.getElementById("mail_cod").disabled=false;
            document.getElementById("pr_apellido_refp_cod").disabled=false;
            document.getElementById("seg_apellido_refp_cod").disabled=false;
            document.getElementById("pr_nombre_refp_cod").disabled=false;
            document.getElementById("seg_nombre_refp_cod").disabled=false;
            document.getElementById("tel1_refp_cod").disabled=false;
            document.getElementById("tel2_refp_cod").disabled=false;
            document.getElementById("ext_refp_cod").disabled=false;
            document.getElementById("cel_refp_cod").disabled=false;
            document.getElementById("ciu_refp_cod").disabled=false;
            document.getElementById("dep_refp_cod").disabled=false;
            document.getElementById("tconocido_refp_cod").disabled=false;
            document.getElementById("ocup_cod").disabled=false;
            document.getElementById("act_econ_cod").disabled=false;
            document.getElementById("nom_emp_cod").disabled=false;
            document.getElementById("nit_emp_cod").disabled=false;
            document.getElementById("dir_emp_cod").disabled=false;
            document.getElementById("ciu_emp_cod").disabled=false;
            document.getElementById("dep_emp_cod").disabled=false;
            document.getElementById("tel_emp_cod").disabled=false;
            document.getElementById("ext_emp_cod").disabled=false;
            document.getElementById("car_emp_cod").disabled=false;
            document.getElementById("f_ing_cod").disabled=false;
            document.getElementById("tipo_cont_cod").disabled=false;
            document.getElementById("eps_cod").disabled=false; 
            document.getElementById("tip_afil_cod").disabled=false; 
            document.getElementById("cel_emp_cod").disabled=false; 
            document.getElementById("mail_emp_cod").disabled=false;  
            document.getElementById("dir_cob_cod").disabled=false;  
            document.getElementById("parent_refam_cod").disabled=false;   
            document.getElementById("mail_refp_cod").disabled=false;   
            document.getElementById("mail_refam_cod").disabled=false;
            document.getElementById("sal_cod").disabled=false;
            document.getElementById("otros_cod").disabled=false;
            document.getElementById("conc_otros_cod").disabled=false;
            document.getElementById("manuten_cod").disabled=false;
            document.getElementById("cred_cod").disabled=false;
            document.getElementById("arr_cod").disabled=false;
            document.getElementById("pr_apellido_refam_cod").disabled=false;
            document.getElementById("seg_apellido_refam_cod").disabled=false;
            document.getElementById("pr_nombre_refam_cod").disabled=false;
            document.getElementById("seg_nombre_refam_cod").disabled=false;
            document.getElementById("tel1_refam_cod").disabled=false;
            document.getElementById("tel2_refam_cod").disabled=false;
            document.getElementById("ext_refam_cod").disabled=false;
            document.getElementById("cel_refam_cod").disabled=false;
            document.getElementById("ciu_refam_cod").disabled=false;
            document.getElementById("dep_refam_cod").disabled=false;
      
      
  }else{
      //datos personales
        document.getElementById("seg_apellido_cod").disabled=false;
        document.getElementById("seg_nombre_cod").disabled=false;

      //referencias personales
        document.getElementById("pr_apellido_refp_cod").disabled=false;
        document.getElementById("seg_apellido_refp_cod").disabled=false;
        document.getElementById("pr_nombre_refp_cod").disabled=false;
        document.getElementById("seg_nombre_refp_cod").disabled=false;
        document.getElementById("tel1_refp_cod").disabled=false;
        document.getElementById("tel2_refp_cod").disabled=false;
        document.getElementById("ext_refp_cod").disabled=false;
        document.getElementById("cel_refp_cod").disabled=false;
        document.getElementById("ciu_refp_cod").disabled=false;
        document.getElementById("dep_refp_cod").disabled=false;
        document.getElementById("tconocido_refp_cod").disabled=false;
        
      //referencias familiares
        document.getElementById("pr_apellido_refam_cod").disabled=false;
        document.getElementById("seg_apellido_refam_cod").disabled=false;
        document.getElementById("pr_nombre_refam_cod").disabled=false;
        document.getElementById("seg_nombre_refam_cod").disabled=false;
        document.getElementById("tel1_refam_cod").disabled=false;
        document.getElementById("tel2_refam_cod").disabled=false;
        document.getElementById("ext_refam_cod").disabled=false;
        document.getElementById("cel_refam_cod").disabled=false;
        document.getElementById("ciu_refam_cod").disabled=false;
        document.getElementById("dep_refam_cod").disabled=false;

      
  }
  
  document.getElementById("codeu").disabled=disabled;    
}

function habilita_ref_p(disabled) {
    var i=1;   
    for(i; i<=2; i++)
    {      
        
         document.getElementById("pr_apellido_refp_nat"+i).disabled=disabled;
         document.getElementById("seg_apellido_refp_nat"+i).disabled=disabled;
         document.getElementById("pr_nombre_refp_nat"+i).disabled=disabled;
         document.getElementById("seg_nombre_refp_nat"+i).disabled=disabled;
         document.getElementById("tconocido_refp_nat"+i).disabled=disabled;
         document.getElementById("cel_refp_nat"+i).disabled=disabled;
         document.getElementById("tel1_refp_nat"+i).disabled=disabled;
         document.getElementById("tel2_refp_nat"+i).disabled=disabled;
         document.getElementById("ext_refp_nat"+i).disabled=disabled;
         document.getElementById("dep_refp_nat"+i).disabled=disabled;
         document.getElementById("ciu_refp_nat"+i).disabled=disabled;
         document.getElementById("pr_apellido_refp_nat"+i).disabled=disabled;
         document.getElementById("mail_refp_nat"+i).disabled=disabled;
    }

  }


  function habilita_ref_f(disabled) {
    var i=1;
    for(i; i<=2; i++)
    {
         document.getElementById("pr_apellido_refam_nat"+i).disabled=disabled;
         document.getElementById("seg_apellido_refam_nat"+i).disabled=disabled;
         document.getElementById("pr_nombre_refam_nat"+i).disabled=disabled;
         document.getElementById("seg_nombre_refam_nat"+i).disabled=disabled;
         document.getElementById("cel_refam_nat"+i).disabled=disabled;
         document.getElementById("tel1_refam_nat"+i).disabled=disabled;
         document.getElementById("tel2_refam_nat"+i).disabled=disabled;
         document.getElementById("ext_refam_nat"+i).disabled=disabled;
         document.getElementById("dep_refam_nat"+i).disabled=disabled;
         document.getElementById("ciu_refam_nat"+i).disabled=disabled;
         document.getElementById("mail_refam_nat"+i).disabled=disabled;
         document.getElementById("parent_refam_nat"+i).disabled=disabled;


    }

  }

  function habilita_cuantas(disabled) {
    var i=1;
    for(i; i<=3; i++)
    {
         document.getElementById("tipo_cuenta"+i).disabled=disabled;
         document.getElementById("banco"+i).disabled=disabled;
         document.getElementById("num_tarjeta"+i).disabled=disabled;
         document.getElementById("fecha_apertura"+i).disabled=disabled;
         document.getElementById("cuenta"+i).disabled=disabled;
    }


  }

  function habilita_cabecera(disabled) {

         document.getElementById("valor_solicitado").disabled=disabled;
         document.getElementById("plazo").disabled=disabled;
         //document.getElementById("cmbTituloValor").disabled=disabled;
         document.getElementById("codeu").disabled=disabled;
    

  }



  function mensaje()
  {
      var msj="";
      var solicitud=document.getElementById("num_solicitud").value;
      msj="Apreciado cliente su operaci�n ha sido aprobada con �xito, favor guardar  el numero del formulario ("+   solicitud  +") para legalizar el desembolso.\n\
           Comun�quese con Fintra al tel�fono 3679966 o ac�rquese a nuestras oficinas ubicadas en la Cra 53 No. 79 - 01 Of. 205";
      return msj;
  }

  function validarRenovacion(){

       var inpRenovacion=document.getElementById("inpRenovacion").value;
       var contenedor = document.getElementById("popup");
       if(inpRenovacion !=""){
        
        var url = controller+"?estado=Liquidador&accion=Negocios";
        var p =  'opcion=buscarRenovacion&identificacion='+inpRenovacion;
        new Ajax.Request(
            url,
            {
                method: 'post',
                parameters: p,
                cache : false,
                onSuccess: function(respue){
                    var out=respue.responseText.replace(/^\s+/g,'').replace(/\s+$/g,'');
                    //alert(out);
                    if(out=='N'){
                 
                        alert("El cliente no cumple con las politicas de renovacion");
                         //var contenedor = document.getElementById("popup");
                            contenedor.style.display = "none";
                            document.body.style.opacity = "1";
                            document.getElementById("inpRenovacion").value='';
                            document.getElementById("renovacion").checked=false;
                            return false;
                    
                    }else {
                        var array = out.split(";_;");
                        if(array[1]=='S'){
                           
                            document.getElementById("valor_solicitado").value =array[0].replace(".0", "");
                            document.getElementById("valor_producto").value =array[0].replace(".0", "");
                            document.getElementById("motonRenovacion").value =array[0].replace(".0", "");
                            document.getElementById("fecha_priemra_cuota").value=array[2];
                            document.getElementById("codNegocioRenovado").value=array[3]
                            //var contenedor = document.getElementById("popup");
                            contenedor.style.display = "none";
                            document.body.style.opacity = "1";
                            document.getElementById("inpRenovacion").value='';
                            return false;
                       }else {
                            alert("El cliente no cumple con las politicas de renovacion")
                            //var contenedor = document.getElementById("popup");
                            contenedor.style.display = "none";
                            document.body.style.opacity = "1";
                            document.getElementById("inpRenovacion").value='';
                            document.getElementById("renovacion").checked=false;
                            return false;
                            
                       }
                
                  }
            
                }
            }
            ); 
           
       }else{
           
           alert("esta vacio") 
       }
      
      
  }

//var i = 2;
var ns4 = (document.layers);
var ie4 = (document.all && !document.getElementById);
var ie5 = (document.all && document.getElementById);
var ns6 = (!document.all && document.getElementById);
var msg = new Array();

function Posicionar_div(id_objeto,e){
  //alert(e);
  //alert(window.event);
  obj = document.getElementById(id_objeto);
  var posx = 0;
  var posy = 0;
  if (!e) var e = window.event;
  if (e.pageX || e.pageY) {
   //alert('page');
   posx = e.pageX;
   posy = e.pageY;
  }
  else if (e.clientX || e.clientY) {
        //alert('client'); 
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
      }
      else
       alert('ninguna de las anteriores');
 //alert('posx='+posx + ' posy=' + posy);
 obj.style.left = posx+'px';
 obj.style.top = posy+'px';
 //obj.css("margin")="192px 51px 506px 83px";
 
 //alert('objleft='+posx + ' objtop=' + posy);
 //document.getElementById('posicion').innerHTML = 'scrollLeft='+document.body.scrollLeft+' scrollTop='+document.body.scrollTop+' cientX'+e.clientX +' clientY'+e.clientY;
}

function genDireccion(elemento,e) {
    //alert("Es: "+e);

    var contenedor = document.getElementById("direccion_dialogo")
      , res = document.getElementById("dir_resul");
    $j("#direccion_dialogo").draggable({ handle: "#drag_direcciones"});  
    Posicionar_div("direccion_dialogo",e);  //192px 51px 506px 83px
    contenedor.style.display = "block";
    
    res.name = elemento;    
    res.value = (elemento.value) ? elemento.value : '';/*
    $j("#direccion_dialogo").dialog({
        // open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
        width: "auto",
        height: "auto",
        show: "scale",
        hide: "scale",
        title: "Direccion",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {  
                $j('#'+elemento).val($j('#dir_resul').val());
                setDireccion(0);
             },
            "Salir": function () { 
                setDireccion(0);                
            }
        }
    });*/
}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3: 
            var res = document.getElementById('dir_resul')
              , des = document.getElementById(res.name);
              des.value = res.value;
              
            let cedula=document.getElementById('id_nat').value;
              if(res.name==="dir_nat"){
                validarDirCliente(des.value,cedula);
              }
              
        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");
            
            document.getElementById("direccion_dialogo").style.display = "none";
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
              , g = document.getElementById('via_genera_dir')
              , opcion;
              //alert('DireccionPrincipal: '+p);
            for (var i = 0; i < g.length; i++) {
                
                opcion = g[i];
                
                if (opcion.value === p) {
                    
                    opcion.style.display = 'none';
                    opcion.disabled = true;
                    
                } else {
                    
                    opcion.disabled = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if(!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                || !jQuery('#placa_dir').val() ) {
                  jQuery('#dir_resul').val("");
                  jQuery('#dir_resul').attr("class","validation-failed");
              } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                    jQuery('#via_princip_dir option:selected').text()
                   + ' '  + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                   + ' '  + jQuery('#via_genera_dir option:selected').text().trim()
                   + ' '  + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                   + ((jQuery('#via_genera_dir option:selected').text().toUpperCase()==='#' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='TRANSVERSAL') ?  '-':  ' ')
                   + jQuery('#placa_dir').val().trim().toUpperCase()
                   + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
              }
            break;
    }
}

function cargarCiudad(codigo, combo) {

        $j('#' + combo).empty();
        $j.ajax({
            async: false,
            type: 'POST',
            url: "/fintra/controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error, '250', '180');
                        return;
                    }
                    try {
                        $j('#' + combo).empty();
                        $j('#' + combo).append("<option value=''>...</option>");

                        for (var key in json) {
                            $j('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } 

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    
}


function cargarVias(codciu, combo) {
    //alert(codciu);

        $j('#' + combo).empty();
        $j.ajax({
            async: false,
            type: 'POST',
            url: "/fintra/controller?estado=GestionSolicitud&accion=Aval",
            dataType: 'json',
            data: {
                opcion: 'cargarvias',
                ciu: codciu
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error, '250', '180');
                        return;
                    }
                    try {
                        $j('#' + combo).empty();
                        $j('#' + combo).append("<option value=''></option>");

                        for (var key in json) {
                            $j('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } 
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}


function bloquearDiv() {
    var mcheck = document.getElementById("compra_cartera").checked;
  
    var nodes = document.getElementById("contenido_cc").getElementsByTagName('*');
    for (var i = 0; i < nodes.length; i++){
        if (mcheck) {
            nodes[i].disabled = false;            
        } else {
             nodes[i].disabled = true;
        }
        
        if (nodes[i].name !== undefined) {
            if (nodes[i].name.search('form.solicitante.obligaciones') !== -1) {
                nodes[i].value = '';
            }
        }
    }
}

function bloquearDivLoad() {
    var nodes = document.getElementById("contenido_cc").getElementsByTagName('*');
    var mcheck = document.getElementById("compra_cartera").checked;
    if (!mcheck) {
        for (var i = 0; i < nodes.length; i++)
        {
            nodes[i].disabled = true;
        }
    }
}


function buscarInformacionEntidad(index){
    
    var entidad=document.getElementById("entidad"+index).value;
    if(entidad !==""){        
         $j.ajax({
            async: false,
            type: 'POST',
            url: "/fintra/controller?estado=GestionSolicitud&accion=Aval",
            dataType: 'json',
            data: {
                opcion: 'BUSCAR_DATOS_PROVEEDOR',
                entidad: entidad
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error, '250', '180');
                        return;
                    }
                    document.getElementById("nit_prov"+index).value=json.nit;
                    document.getElementById("tcuenta_prov"+index).value=json.tipo_cuenta_prov;
                    document.getElementById("cuenta_prov"+index).value=json.cuenta_prov;   
                } 

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }else{
        document.getElementById("nit_prov" + index).value = '';
        document.getElementById("tcuenta_prov" + index).value = '';
        document.getElementById("cuenta_prov" + index).value = '';   
    }
  
    
}

function addObligs(nueva_fila, id_obj_obligacion,e) {
    e.preventDefault();
    var max_obligaciones = $j('#max_obligaciones');
    
    if(nueva_fila) {
         var fila = $j('#obligacion_' + (parseInt(max_obligaciones.val())+1));
        if (fila) {
            max_obligaciones.val(parseInt(max_obligaciones.val())+1);
            $j('#obligacion_' + max_obligaciones.val()).css("display","");
        }
    } else {
        var j = 0;
        for(var i = id_obj_obligacion; i < max_obligaciones.val(); i++) {
            j = i+1;
            $j('#nit_prov' + i).val($j('#nit_prov' + j).val());
            $j('#tcuenta_prov' + i).val($j('#tcuenta_prov' + j).val());
            $j('#cuenta_prov' + i).val($j('#cuenta_prov' + j).val());
            $j('#valor_recoger' + i).val($j('#valor_recoger' + j).val());
        }
        $j('#nit_prov' + max_obligaciones.val()).val("");
        $j('#tcuenta_prov' + max_obligaciones.val()).val("");
        $j('#cuenta_prov' + max_obligaciones.val()).val("");
        $j('#valor_recoger' + max_obligaciones.val()).val("0");
        
        $j('#obligacion_' + max_obligaciones.val()).css("display","none");
        max_obligaciones.val(parseInt(max_obligaciones.val())-1);
    }
}

function cargarCiudadesBarrio(id_dept, id_ciud, id_barrio){
    var dept = $(id_dept).value;    
    var url = controller+"?estado=GestionSolicitud&accion=Aval";
    var p = "opcion=cargarciubarrio&dept="+dept+"&ciu="+id_ciud+"&barrio="+id_barrio;
    new Ajax.Request(url, {
        parameters: p,
        method: 'post',
        onComplete:  function (resp){
            document.getElementById("d_"+id_ciud).innerHTML = resp.responseText;            
        }
    });

}

function cargarBarrios(id_ciudad, id_barrio){
    var codciu = $(id_ciudad).value;    
    var url = controller+"?estado=GestionSolicitud&accion=Aval";
    var p = "opcion=cargarbarrio&ciudad="+codciu+"&barrio="+id_barrio;
    new Ajax.Request(url, {
        parameters: p,
        method: 'post',
        onComplete:  function (resp){
            document.getElementById("d_"+id_barrio).innerHTML = resp.responseText;            
        }
    });

}

  function validarPreaprobado() {

    var inpPreaprobado = document.getElementById("inpPreaprobado").value;
    var contenedor = document.getElementById("popup2");
    if (inpPreaprobado !== "") {

        var url = controller + "?estado=Liquidador&accion=Negocios";
        var p = 'opcion=buscarPreaprobado&identificacion=' + inpPreaprobado;
        new Ajax.Request(
                url,
                {
                    method: 'post',
                    parameters: p,
                    cache: false,
                    onSuccess: function (respue) {
                        var out = respue.responseText.replace(/^\s+/g, '').replace(/\s+$/g, '');
                        //alert(out);
                        if (out === 'N') {
                            
                            alert("El cliente no se encuentra en la base de preaprobados");                          
                            contenedor.style.display = "none";
                            document.body.style.opacity = "1";
                            document.getElementById("inpPreaprobado").value = '';
                            document.getElementById("preAprobado").checked = false;
                            document.getElementById("valor_solicitado").value = '';
                            document.getElementById("valor_producto").value = '';
                            document.getElementById("motonRenovacion").value = '';

                            return false;

                        } else {
                            
                            var array = out.split(";_;");
                            
                            if(array[5]==='N'){
                                alert("El cliente no se encuentra al dia.")
                                return false;
                            }

                            document.getElementById("valor_solicitado").value = array[4].replace(".0", "");
                            document.getElementById("valor_producto").value = array[4].replace(".0", "");
                            document.getElementById("motonRenovacion").value = array[4].replace(".0", "");
                            document.getElementById("codNegocioRenovado").value = array[1];
                            //var contenedor = document.getElementById("popup");
                            contenedor.style.display = "none";
                            document.body.style.opacity = "1";
                            document.getElementById("inpPreaprobado").value = '';
                            return false;


                        }
                    }});


    } else {

        alert("esta vacio")
    }
      
      
  }

/**
 * Llena la informaci�n de la secci�n Negocio
 * a partir de la informaci�n econ�mica y laboral
 * @param {string} idCampo id del campo que tiene el dato
 */
function completarDatosNegocio(idCampo) {
    var nom = "nom_emp_nat", dir = "dir_emp_nat", dep = "dep_emp_nat", ciu = "ciu_emp_nat", cel = "cel_emp_nat";    
    
    switch (idCampo) {
        case nom:
            document.getElementById("nombre_neg").value = document.getElementById(nom).value;
            break;
        case dep:
            var d = document.getElementById("dep_neg"); 
            d.value = document.getElementById(dep).value;
            d.onchange();
            break;
        case ciu:
            var c = document.getElementById("ciu_neg"); 
            c.value = document.getElementById(ciu).value;
            c.onchange();
            break;
        case dir:
            document.getElementById("dir_neg").value = document.getElementById(dir).value;
            break;
        case cel:
            document.getElementById("telefono_neg").value = document.getElementById(cel).value; // en la seccion Negoci, el campo se llama telefono
            break;
    } 
}


/**
 * Activa/Inactiva campos del socio
 * @param {EventTarget} e evento disparado
 * @returns {undefined}
 */
function tieneSocio(e) {
    switch (e.target.value.toUpperCase()) {
        case "S":
            document.getElementById("id_parcitipacion").disabled = false;
            document.getElementById("id_nomb_socio").disabled = false;
            document.getElementById("img_dir_socio").disabled = false;
            document.getElementById("id_tel_socio").disabled = false;
            document.getElementById("cedula_socio").disabled = false;
            break;
        case "N":
        default:
            document.getElementById("id_parcitipacion").disabled = true;
            document.getElementById("id_nomb_socio").disabled = true;
            document.getElementById("img_dir_socio").disabled = true;
            document.getElementById("id_tel_socio").disabled = true;
            document.getElementById("cedula_socio").disabled = true;
            break;
    }
}



function validarDirCliente(direccion,cedula){
    
    $j.ajax({
        type: 'POST',
        url: "/fintra/controller?estado=GestionSolicitud&accion=Aval",
        dataType: 'json',
        data: {
            opcion: "ValidarDireccion",
            direccion: direccion,
            nit: cedula
        },
        success: function (json) {

            if (json.success === true) {
                mensajesDelSistema("SOLICITUD: "+json.numero_solicitud+" <br>"
                                   +"NOMBRE: "+json.nombre+" <br>"
                                   +"NIT: "+json.identificacion+" <br>"
                                   +"NEGOCIO: "+json.cod_neg+"<br>"
                                   );
            } 
        },
        error: function (xhr) {
            mensajesDelSistema("Error " + "\n" +
                    xhr.responseText);
        }
    });   
}

function mensajesDelSistema(msj){
    var contenedor = document.getElementById("popup3");
    document.getElementById("msj").innerHTML=msj;
        contenedor.style.display = "block";
        document.body.style.opacity = "0.5";
        return false;
} 


function cerrarVentanaNegocio(){
    var contenedor = document.getElementById("popup3");
        contenedor.style.display = "none";
       // contenedor.style.opacity = "0.5"
        document.body.style.opacity = "1";
        return false;
  
}