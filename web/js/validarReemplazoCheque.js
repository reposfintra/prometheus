
  var CONTROLLER = '';
  var BASEURL    = '';
  var separador  = '~';
  ////////////////////////////////////
  // desactivar ayudas
  function hiddenHelp () {
    return false ;
  }
  window.onhelp  = hiddenHelp;
  



  /////////////////////////////////////
  // utilidades de fecha
  function addFecha ( txtDate , tipo, inc ){
    var fecha = new String ( txtDate ).split(/-|:| /);
	//for (i=0;i<fecha.length;i++)
	// alert(fecha[i]);
	fecha[tipo] = parseFloat(fecha[tipo]) + inc;
	return new Date ( fecha[0] , parseFloat(fecha[1]) - 1  , fecha[2], fecha[3], fecha[4]  );
  }
  
  
  function getDate ( date, format ) {
     var str  = format;

	 str = str.replace('yyyy', completNumber(date.getFullYear(),4) );
	 str = str.replace('mm'  , completNumber(date.getMonth()+1 ,2) );	 
	 str = str.replace('dd'  , completNumber(date.getDate()    ,2) );	 

	 str = str.replace('hh'  , completNumber(date.getHours()   ,2) );	 
	 str = str.replace('mi'  , completNumber(date.getMinutes() ,2) );	 	 
	 return str;  
  }
  
  function completNumber (num, long) {
     var str =  new String ( num );
	 for ( i = str.length ; i < long ; str='0'+str, i++ );
	 return str;
  }  
  
  
  

  /////////////////////////////////////
  // funcionalidades de teclas
  var keyF1    = 112;
  var keyF2    = 113;
  var keyPlus  = 107;
  var keyMinus = 109;
  var keyPoint = 110;
  var keySpr   = 46;

  var textClipBoard = '';

  function _keys ( element ) {
     var key = ( event.which  ? event.which : event.keyCode );
     switch (key){
        case keyPoint :
             element.value = getDate (new Date, 'yyyy-mm-dd hh:mi');
        break;
        case keyPlus :
			if ( element.value != '' ) 
             element.value = getDate ( addFecha ( element.value , 2 , 1  ), 'yyyy-mm-dd hh:mi' );
        break;
        case keyMinus :
		    if ( element.value != '' ) 
             element.value = getDate ( addFecha ( element.value , 2 , -1 ), 'yyyy-mm-dd hh:mi' );
        break;
		case keySpr:
			element.value=''; 
		break;

     }
  }  

 
  ////////////////////////////////////////////////////////////////
    
    function addOption(Comb,valor,texto){
            var Ele = document.createElement("OPTION");
            Ele.value=valor;
            Ele.text=texto;
            Comb.add(Ele);
    }    
    
    function LoadBancos(CmbBanco, CmbSucursal){
       CmbBanco.length=0;
       var  aux='?';
       if (datos.length>0){
           for (i=0;i<datos.length;i++){
             var info=(new String(datos[i])).split(separador);
             if (aux!=info[0]){
                 addOption(CmbBanco,info[0],info[0]);
                 aux=info[0];
             }
           }
		   //addOption(CmbBanco,'ALL','Todos los Bancos');
        }else
           addOption(CmbBanco,'NINGUNO','NO SE ENCONTRARON BANCOS');
           
        LoadSucursal (CmbBanco, CmbSucursal);
    } 
   
    function LoadSucursal (CmbBanco, CmbSucursal){
       CmbSucursal.length=0;
       var  aux='?';
       if (datos.length>0){	  
           if (CmbBanco.value!='')
               for (i=0;i<datos.length;i++){
                 var info=(new String(datos[i])).split(separador);
                 if (CmbBanco.value==info[0])
                    if (aux!=info[1]){
                         addOption(CmbSucursal,info[1],info[1]);
                         aux=info[1];
                    }
               }
	   		   //addOption(CmbSucursal,'ALL','Todos las Sucursales');
        }else
           addOption(CmbSucursal,'NINGUNO','NO SE ENCONTRARON SUCURSALES'); 
    }
  
  
  
  function goFiltro (banco, sucursal, rangoini, rangofin){
    var url = CONTROLLER + '?estado=Reemplazo&accion=Cheques&Opcion=Filtrar&fltBanco=' + banco + '&fltSucursal='+ sucursal + '&fltRangoini=' + rangoini + '&fltRangofin=' + rangofin;
    window.location.href = url;
  }
  
  function _onsubmit (tform){ 
    for (i=0;i<tform.length;i++){
		if (tform.elements[i].type=='text' && tform.elements[i].value!='')
		   return true;
    }
    alert('Indique una fecha por lo menos para poder continuar.');
	return false;
  }


  function showMsg (message){

	  var obj = document.getElementById('msg');
	  if (obj){
		   obj.innerHTML  = message;
		   obj.style.visibility = 'visible';
		   obj.style.top  = event.y-100;
		   obj.style.left = event.x;
	  }
  }
  
  function hiddenMsg (){
	  var obj = document.getElementById('msg');
	  if (obj) obj.style.visibility = 'hidden';
  }
  
  function showInfoChq (bnc, suc, chq, fec, vlr, mon ,ben, fent, fenv, frec){
	  var tabla = "<br><table width='95%' align='center' border='0' cellpadding='2' cellspacing='1'>";
	  tabla += "<tr class='subtitulo1'><th colspan='2'>Detalle Registro de Fechas</th></tr>";
	  tabla += "<tr class='filagris'><td><b>Banco        </b></td><td>"+ bnc  +"</td></tr>";
	  tabla += "<tr class='filagris'><td><b>Sucursal     </b></td><td>"+ suc  +"</td></tr>";
	  tabla += "<tr class='filagris'><td><b>Cheque       </b></td><td>"+ chq  +"</td></tr>";
	  tabla += "<tr class='filagris'><td><b>Valor        </b></td><td>"+ vlr  +"</td></tr>";
	  tabla += "<tr class='filagris'><td><b>Moneda       </b></td><td>"+ mon  +"</td></tr>";
	  tabla += "<tr class='filagris'><td><b>Fec. Cheque  </b></td><td>"+ fec  +"</td></tr>";	  
	  tabla += "<tr class='filagris'><td><b>Beneficiario </b></td><td>"+ ben  +"</td></tr>";
	  tabla += "<tr class='filagris'><td><b>Fec. Entrega </b></td><td>"+ fent +"</td></tr>";
	  tabla += "<tr class='filagris'><td><b>Fec. Envio   </b></td><td>"+ fenv +"</td></tr>";
	  tabla += "<tr class='filagris'><td><b>Fec. Recibido</b></td><td>"+ frec +"</td></tr>";
	  tabla += "</table><br>&nbsp;";
	  showMsg(tabla);
  }

    function SelAll(){
        for(i=0;i<flist.length;i++)
            flist.elements[i].checked=flist.All.checked;
    }

    function ActAll(){
        flist.All.checked = true;
            for(i=1;i<flist.length;i++)	
                if (!flist.elements[i].checked){
                    flist.All.checked = false;
                    break;
                }
    }

    function validarlistado(){
            for(i=1;i<flist.length;i++)	
                    if (flist.elements[i].checked)
                            return true;
            alert('Por favor seleccione un registro para poder continuar');
            return false;
    }        


    var isIE = document.all?true:false;
    var isNS = document.layers?true:false;
    function soloDigitos(e,decReq) {
        var key = (isIE) ? window.event.keyCode : e.which;
        var obj = (isIE) ? event.srcElement : e.target;
        var isNum = (key > 47 && key < 58) ? true:false;
        var dotOK =  (decReq=='decOK' && key ==46 && obj.value.indexOf('.')==-1) ? true:false;
        window.event.keyCode = (!isNum && !dotOK && isIE) ? 0:key;
        e.which = (!isNum && !dotOK && isNS) ? 0:key;   
        return (isNum || dotOK );
    }

    function soloTexto(e) {
            var key = (isIE) ? window.event.keyCode : e.which;
            var obj = (isIE) ? event.srcElement : e.target;
            var isNum = ((key > 0 && key < 32) || (key > 32 && key < 65) || (key > 90 &&  key < 97) || (key > 122)) ? true:false;
            window.event.keyCode = (isNum && isIE) ? 0:key;
            e.which = (!isNum && isNS) ? 0:key;
            return (isNum);
    } 

    function soloAlfa(e) {
            var key = (isIE) ? window.event.keyCode : e.which;
            var obj = (isIE) ? event.srcElement : e.target;
            var isNum = ((key > 0 && key < 32) || (key > 32 && key < 48) || (key > 57 && key < 65) || (key > 90 &&  key < 97) || (key > 122) || (key == 13)) ? true:false;
            window.event.keyCode = (isNum && isIE) ? 0:key;
            e.which = (!isNum && isNS) ? 0:key;
            return (isNum);
    }
