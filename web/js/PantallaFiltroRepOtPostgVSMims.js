function enviar(url, nombrePagina){
  var form = document.forms[0];
  var fechaini = form.elements[0].value;
  var fechafin = form.elements[1].value;
  var contentType = form.elements[2].value;
  var params = "&fecini="+fechaini+"&fecfin="+fechafin+"&tipoPage="+contentType;
  var fecha1 = toDate(fechaini);
  var fecha2 = toDate(fechafin);
  
  if ((fechaini != "") && (fechafin != "")){
      if (fecha1.getTime() > fecha2.getTime())
         alert("LA FECHA INICIAL DE SER MENOR QUE LA FINAL");
      else{
         abrirPagina(url+params, nombrePagina);
      }
  }
  else
      alert("DEBE SELECCIONAR UN RANGO DE FECHAS");

}

function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPagina(url, nombrePagina)
{
  var wdth = screen.width - screen.width * 0.5;
  var hght = screen.height - screen.height * 0.5;
  var lf = screen.width * 0.2;
  var tp = screen.height * 0.25;
  var options = "status=yes,scrollbars=yes,resizable=yes,location=no,menubar=yes,toolbar=no," +
                setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) ){
     hWnd.opener = document.window;
  }
}

//FECHA EN FORMATO YYYYMMDD
function toDate(fecha){
   var fec = fecha;
   var ano = Number(fec.substr(0, 4));
   var mes = Number(fec.substr(4, 2)) - 1;
   var dia = Number(fec.substr(6, 2));

   newDate = new Date(ano, mes, dia, 0, 0);
   return newDate; 
}



