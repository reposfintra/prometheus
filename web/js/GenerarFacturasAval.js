var controller;
var baseurl;

function inizializarvar(contr, baseu){
    controller=contr;
    baseurl=baseu;
}

function actualizarCorte(){
    limpiartabla();
    var dia =parseInt($("dia").value);
    var mes =parseInt($("mes").value);
    var anio =parseInt($("anio").value);
    $("fechaInicial").value=anio+"-"+((mes<10)?"0"+mes:mes)+"-"+dia;
    if(mes==12){
        $("fechaFinal").value=(anio+1)+"-01-"+(dia-1);
    }else{
        $("fechaFinal").value=(anio)+"-"+(((mes+1)<10)?"0"+(mes+1):(mes+1))+"-"+(dia-1);
    }
}

function cargarConvenios(ind){
    limpiartabla();
    var vista=$("vista").value;
    var url = "";
    var p ="";
    if (vista=="1"){
        var anombre = $('anombre').options[ind].value;
        if(anombre==""){
            $('afiliado').innerHTML="";
            $('convenio').innerHTML="";
        }else{
            url = controller+"?estado=GenerarFacturas&accion=Aval";
            p = "opcion=CARGAR_CONVENIOS_ANOMBRE&anombre="+anombre;
            new Ajax.Request(url, {
                parameters: p,
                method: 'post',
                onComplete:  function (resp){
                    document.getElementById("d_convenio").innerHTML = resp.responseText;

                }
            });
        }
    }else if (vista=="2"){
        var avalista = $('avalista').options[ind].value;
        if(avalista==""){
            $('convenio').innerHTML="";
        }else{
            url = controller+"?estado=GenerarFacturas&accion=Aval";
            p = "opcion=CARGAR_CONVENIOS_AVALISTA&avalista="+avalista;
            new Ajax.Request(url, {
                parameters: p,
                method: 'post',
                onComplete:  function (resp){
                    document.getElementById("d_convenio").innerHTML = resp.responseText;

                }
            });
        }
    }
}

function cargarAfiliados(ind){
    limpiartabla();
    var id_convenio = $('convenio').options[ind].value;
    if(id_convenio==""){
        $('afiliado').innerHTML="";
    }else{
        var url = controller+"?estado=GenerarFacturas&accion=Aval";
        var p = "opcion=CARGAR_AFILIADOS&id_convenio="+id_convenio;
        new Ajax.Request(url, {
            parameters: p,
            method: 'post',
            onComplete:  function (resp){
                document.getElementById("d_afiliado").innerHTML = resp.responseText;
            }
        });
    }
}

function cargarNegocios(){
    var id_convenio = $('convenio').value;
    var fechaini= $("fechaInicial").value;
    var fechafin = $("fechaFinal").value;
    var vista = $("vista").value;
    var afiliado = "";
    var anombre = "";
    var avalista = "";
    if(!(fechaini!="" && fechafin!="")){
        alert("debe seleccionar un periodo");
        return false;
    }

    if(vista=="1"){
       afiliado = $('afiliado').value;
        anombre = $('anombre').value;

        if(anombre==""){
            alert("debe seleccionar si el aval es a nombre");
            return false;
        }
    }
    if(vista=="2"){
        avalista = $('avalista').value;
        if(avalista==""){
            alert("debe seleccionar un avalista");
            return false;
        }
    }
    if(id_convenio==""){
        alert("debe seleccionar un convenio");
        return false;
    }
   
    var url = controller+"?estado=GenerarFacturas&accion=Aval";
    var p = "opcion=CARGAR_NEGOCIOS&id_convenio="+id_convenio+"&afiliado="+afiliado+
    "&anombre="+anombre+"&fechaini="+fechaini+"&fechafin="+fechafin+"&vista="+vista;
    new Ajax.Request(url, {
        parameters: p,
        method: 'post',
        onLoading: function (){
            document.getElementById("d_contenido").innerHTML = '<img alt="cargando" src="'+baseurl+'/images/cargando.gif" name="imgload">';
        },
        onComplete:  function (resp){
            document.getElementById("d_contenido").innerHTML = resp.responseText;
        }
    });
    
}

function generar(){
    var id_convenio = $('convenio').value;
    var vista = $("vista").value;
    var url = controller+"?estado=GenerarFacturas&accion=Aval";
    var p = "";
     if(vista=="1"){
         p = "opcion=GENERAR_CXC&id_convenio="+id_convenio;        
     }else if(vista=="2"){
        p = "opcion=GENERAR_CXP&id_convenio="+id_convenio;        
     }

     new Ajax.Request(url, {
            parameters: p,
            method: 'post',
            onLoading: function (){
                document.getElementById("d_contenido").innerHTML = '<img alt="cargando" src="'+baseurl+'/images/cargando.gif" name="imgload">';
            },
            onComplete:  function (resp){
                document.getElementById("d_contenido").innerHTML = '<div id ="div_mensaje"><table width="700px"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">'+
                '<tr> <td width="550" align="center" class="mensajes">'+resp.responseText+'</td>'+
                ' <td width="29" background="'+baseurl+'/images/cuadronaranja.JPG">&nbsp;</td>'+
                '<td width="29" background="'+baseurl+'/images/cuadronaranja.JPG">&nbsp;</td>'+
                '</tr></table></div>';
            }
        });
}

function limpiartabla(){
    document.getElementById("d_contenido").innerHTML = "";
}


