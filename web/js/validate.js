/* Archivo con funciones �tiles a varios programas */

// Coloca el foco sobre el primer elemento del formulario
function doFieldFocus( form ) {
    if ( form == null ) {
        return;
    }
    var i;
    for ( i=0; i< form.elements.length; i++ ) {
        var el = form.elements[i];
        if ( el.type == "text" || el.type == "select-one" || el.type == "checkbox" ) {
            el.focus();
            return;
        }
    }
}

// Elimina los espacios innecesarios en una cadena
function trim(str) {
    return str.replace(/^\s+/,"").replace(/\s+$/,"");
}

// solo admite digitos y letras en una cadena
function onlyDigitsAndChars(str){
    var re = new RegExp("([A-Za-z0-9]+)");
    return (re.exec(str)!=null && RegExp.$1==str);
}

// solo admite d�gitos en una cadena
function onlydigits( str ) {
    var re = new RegExp("([0-9]+)");
    return (re.exec(str)!=null && RegExp.$1==str);
}

/**
 * Obtiene la fecha actual del cliente, y lo incrusta en la pagina actual
 */
function diaHoy() {
    var d, s;

    d = new Date();
    s = meses[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();

    document.write( s );
}

// Esta funcion retorna true si el Dato representa la cadena vacia
function estaVacio( dato ) {
    if ((dato == null) || (dato.length == 0)) {
        return true;
    }
    for ( var i = 0; i < Dato.length; i++ ) {
        if ( Dato.charAt( i ) != ' ' ) {
            return false;
        }
    }
    return true;
}

// Determina si un un valor val est� vacio
function isValEmpty( val ){
    if (val == null) {
        return true;
		}

    if ( val.length == 0 ) {
        return true;
		}
    re = / /gi;
    val = val.replace(re,'');
    return (val.length == 0)
}

// Cheque que el valor de un campo est� vacio
function isempty( fld1 ) {
    var val = fld1.value;
    return isValEmpty(val);
}

// Permite avanzar a el siguiente campo usando el ENTER
function keyDown( DnEvents ) { // handles keypress
    k =  window.event.keyCode;
    if (k == 13) { // enter key pressed
        if (nextfield == 'done') {
            return true; // submit, we finished all fields
        }
        else { // we're not done yet, send focus to next box
            eval(formaPrincipal + '.' + nextfield + '.focus()');
            return false;
        }
    }
}

// Retorna el valor de la fecha formateada
function getdatestring( d ) {
    if ( window.dateformat == "DD-Mon-YYYY" ) {
        var m = "EneFebMarAbrMayJunJulAgoSepOctNovDic";
        return d.getDate()+ "-" + m.substring(d.getMonth()*3,d.getMonth()*3+3) + "-" + d.getFullYear();
    }
    else if ( window.dateformat == "DD-MM-YYYY" )
        return d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
    else
        return  d.getDate() + "/" + ( d.getMonth() + 1 ) + "/" + d.getFullYear();
}

// Verifica la direccion de correo
function checkemailvalue( s_email, alrt ){
    if ((s_email.length < 6) ||
       (s_email.indexOf('@',0) < 1) ||
       (s_email.lastIndexOf('@') != s_email.indexOf('@',0)) ||
       (s_email.lastIndexOf('@') > (s_email.length - 5)) ||
       (s_email.lastIndexOf('.') > (s_email.length - 3)) ||
       (s_email.lastIndexOf('.') < (s_email.length - 5)) ||
       (s_email.indexOf('..',0) > -1) ||
       (s_email.indexOf('@.',0) > -1) ||
       (s_email.indexOf(' ',0) > -1) ||
       (s_email.indexOf('.@',0) > -1) ||
       (s_email.indexOf(',',0) > -1)) {
        if ( alrt ) {
            alert('Por favor, ingrese un valor de e-mail v�lido.');
        }
        return false;
    }
    return true;
}

function checkemail( fld1, emptyok, alrt ){
    fld1.value = trim(fld1.value);
    return checkemail2(fld1,fld1,emptyok,alrt);
}

function checkemail2( fld1, fld2, emptyok, alrt ) {
    var s_email = fld1.value;

    if ( s_email != fld2.value ){
        alert('E-mail addresses must match');
        return false;
    }
    if ( emptyok && s_email.length==0 ) {
        return true;
    }
    return checkemailvalue(s_email,alrt);
}



// Valida queun campo siga las reglas establecidas
function validate_field( field, type, doalert, autoplace, minval, maxval, mandatory ) {
    type = type.toLowerCase();
    if ( field.value == null || field.value.length == 0 ) {
        if (mandatory == true) {
		        if (doalert) alert("El campo debe tener un valor!");
            field.focus();
            field.select();
            window.isvalid = false;
            return false;
		    }
		    else {
		        window.isvalid = true;
		        return true;
		    }
    }
    var validflag = true;
    if ( type == "upper" ) {
        var val = field.value.toUpperCase();
        field.value = val;
    }
    else if ( type =="url" ) {
		    var val = field.value;
		    if ( val.indexOf('http://') == -1 && val.indexOf('https://') == -1 ) {
            if (doalert) alert("URL Inv�lido. Los Urls deben comenzar por http:// or https://");
            validflag = false;
		    }
	  }
    else if (type == "date") {
        validflag = true;
        var m=0,d=0,y=0,val=field.value;
        var fmterr;
        var year="";

        if( !window.dateformat ){
         window.dateformat = "DD-MM-YYYY";
		}

        if( window.dateformat == "DD-MM-YYYY" ) {
            if ( val.indexOf("-") != -1 ) {
                var c = val.split("-");
                if(onlydigits(c[0])) d = parseInt(c[0],10);
                if(onlydigits(c[1])) m = parseInt(c[1],10);
                if(onlydigits(c[2])) y = parseInt(c[2],10);
                year=c[2];
            }
            else {
                var l = val.length, str;
                str = val.substr(0,2-l%2); if(onlydigits(str)) d = parseInt(str,10);
                str = val.substr(2-l%2,2); if(onlydigits(str)) m = parseInt(str,10);
                str = val.substr(4-l%2);   if(onlydigits(str)) y = parseInt(str,10);
                year=parseInt(str,10);
            }
            fmterr = "DD-MM-YY, DD-MM-YYYY, DDMMYY o DDMMYYYY";
        }
        else if( window.dateformat == "DD/MM/YYYY" ) {
            if ( val.indexOf("/") != -1 ) {
                var c = val.split("/");
                if(onlydigits(c[0])) d = parseInt(c[0],10);
                if(onlydigits(c[1])) m = parseInt(c[1],10);
                if(onlydigits(c[2])) y = parseInt(c[2],10);
                year=c[2];
            }
            else {
                var l = val.length, str;
                str = val.substr(0,2-l%2); if(onlydigits(str)) d = parseInt(str,10);
                str = val.substr(2-l%2,2); if(onlydigits(str)) m = parseInt(str,10);
                str = val.substr(4-l%2);   if(onlydigits(str)) y = parseInt(str,10);
                year=str;
            }
            fmterr = "DD/MM/YY, DD/MM/YYYY, DDMMYY o DDMMYYYY";
        }
        else if( window.dateformat == "DD-Mon-YYYY" ) {
            var ms = "ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
            if ( val.indexOf( "-" ) != -1 ) {
                var c = val.split( "-" );
                if(onlydigits(c[0])) d = parseInt(c[0],10);
                m = (ms.indexOf(c[1].toUpperCase())+3)/3;
                if(onlydigits(c[2])) y = parseInt(c[2],10);
                year=c[2];
            }
            else {
                var l = val.length, str;
                str = val.substr(0,1+l%2); if(onlydigits(str)) d = parseInt(str,10);
                str = val.substr(1+l%2,3); if (ms.indexOf(str.toUpperCase()) >= 0) m = (ms.indexOf(str.toUpperCase())+3)/3;
                str = val.substr(4+l%2);   if(onlydigits(str)) y = parseInt(str,10);
                year=str;
            }
            fmterr = "DD-Mes-YY, DD-Mes-YYYY, DDMesYY o DDMesYYYY";
        }

        if(m==0 || d==0) {
            if ( doalert )
						    alert("Valor de la fecha invalida (Debe seguir el formato "+fmterr+")");
            validflag = false;
        }
        else {
            if ( y==0 && !onlydigits(year)) y = (new Date()).getYear();
            if( m < 1 )
						    m=1;
						else if( m > 12 )
						    m=12;
            if( d < 1 )
						    d=1;
						else if( d > 31 )
						    d=31;
            if( y < 100 )
						    y += ( ( y >= 70 ) ? 1900 : 2000 );
            if( y < 1000 )
						    y *= 10;
            if ( y > 9999 )
						    y = (new Date()).getYear();

            field.value = getdatestring(new Date(y, m-1, d));
        }
    }
    else if ( type == "integer" || type == "posinteger" || type == "float" || type == "posfloat" || type == "pct" ) {
        var numval;
        var custrange=false;
        if ((minval != null && maxval != null) || type == "pct") {
            custrange = true;
				}
        var minclip = minval == null ? (type == "pct" ? 0 : -Math.pow(2,32)) : minval;
        var maxclip = maxval == null ?(type == "pct" ? 100 : Math.pow(2,64)) : maxval;
        var val = field.value;
        val = val.replace(/,/g,"");
        val = val.replace(/%/g,"");

        if ( type == "integer" ) {
            numval = parseInt(val,10);
				}
        else if ( type == "posinteger" ) {
            numval = parseInt(val,10);
            minclip=0;
        }
        else if (type == "posfloat") {
            numval = parseFloat(val);
            minclip=0;
        }
        else {
            numval = parseFloat(val);
				}
        if (isNaN(numval) || (custrange && (numval > maxclip || numval < minclip)) || (!custrange && (numval > maxclip || numval < minclip))) {
            if (doalert) {
                if ( type == "pct" ) {
                    alert("Porcentaje inv�lido (debe estar entre 0 y 100)");
      	        }
                else if ( custrange == true ) {
                    if ( minval == null )
			                  alert("N�mero inv�lido (debe ser menor a "+maxclip+")");
                    else if ( maxval == null )
                        alert("N�mero inv�lido (debe ser mayor que "+minclip+")");
                    else
                        alert("N�mero inv�lido (debe estar entre "+minclip+" y "+maxclip+")");
                }
                else if ( type=="posinteger" || type=="posfloat" )
								    alert("N�mero inv�lido (debe ser positivo)");
                else if ( type=="integer" || type=="float" ) {
                    if ( isNaN(numval) )
										    alert('Solo se pueden ingresar valores num�ricos en este campo');
                    else
                        alert("N�mero incorrecto: " + numval);
                }
                else
                    alert("N�mero incorrecto");
            }
            validflag = false;
        }
        else {
            if ( type == "pct" ) {
                if ( numval = Math.floor(numval) )
								    field.value = numval + ".0%";
                else
                    field.value = numval + "%";
            }
            else
            	  field.value = numval;
            validflag = true;
        }
    }
    else if ( type == "time" || type == "timetrack" ) {
        var hours;
        var minutes;

        var re = /([0-9][0-9]?)?(:[0-5][0-9])?/
        var result = re.exec(field.value)
        if (result==null || result.index > 0 || result[0].length != field.value.length) {
            timeval = parseFloat(field.value);
            if (isNaN(timeval))
                hours = -1;
            else {
                hours = Math.floor(timeval);
                minutes = Math.floor((timeval-hours)*60+0.5);
            }
        }
        else {
            if ( RegExp.$1.length > 0 )
						    hours = parseInt(RegExp.$1,10);
            else
                hours = 0;
            if ( typeof(RegExp.$2) != "undefined" && RegExp.$2.length > 0 )
                minutes = parseInt(RegExp.$2.substr(1),10);
            else
                minutes = 0;
        }
        if ( hours >= 0 && minutes >= 0 && minutes < 60 ) {
            field.value = hours + ":" + (minutes < 10 ? "0" : "") + minutes;
            validflag = true;
        }
        else {
            if ( doalert ) alert("Hora inv�lida (Debe tener el formato HH:MI)");
            validflag = false;
        }
    }
    else if (type == "timeofday") {
        var hours;
        var minutes;
        var amorpm;

        var re;
        var re = /([0-9][0-9]?)(:[0-5][0-9])\s?([AaPp])?[Mm]?/
        var result = re.exec(field.value)
        if (result==null || result.index > 0 || result[0].length != field.value.length)
            hours = -1;
        else {
            if ( RegExp.$1.length > 0 )
                hours = parseInt(RegExp.$1,10);
            else
                hours = -1;
            if ( typeof(RegExp.$2) != "undefined" && RegExp.$2.length > 0 )
                minutes = parseInt(RegExp.$2.substr(1),10);
            else
                minutes = -1;
            amorpm = (RegExp.$3.length == 0 || RegExp.$3 == 'a' || RegExp.$3 == 'A') ? "" : "pm";
        }
        if ( hours > 0 && hours <=12 && minutes >= 0 && minutes < 60 ) {
			      if (amorpm == "") amorpm = "am";
            field.value = hours + ":" + (minutes < 10 ? "0" : "") + minutes + " " + amorpm;
            validflag = true;
        }
        else if ( hours > 12 && hours <= 25 && minutes >= 0 && minutes < 60 && amorpm == "" ) {
			      amorpm = "pm";
			      hours -= 12;
            field.value = hours + ":" + (minutes < 10 ? "0" : "") + minutes + " " + amorpm;
        }
        else {
            if (doalert) alert("Entre la hora del d�a como HH:MI o HH:MI AM/PM. Las horas deben estar entre 1 y 12 y los minutos de 0 a 59.");
            validflag = false;
        }
    }
    else if (type == "email"){
        if (checkemail(field, true, doalert))
            validflag = true;
        else
            validflag = false;
    }
    else if ( type == "color" ) {
        var val = field.value;
        if ( val.substring(0,1) == "#" ){
            val = val.substring(1);
				}

        var re = /^[0-9ABCDEFabcdef]{6,}$/;
        if ( val.length > 6 || !re.test(val) ){
            if (doalert) alert("Los colores deben tener el formato: #RRGGBB.  Ejemplo: #FF0000 es rojo.");
            validflag = false;
        }
        else {
            field.value = "#"+val;
				}
    }
    else if (type == "identifier") {
        var val = field.value;
        var re = /^[0-9A-Za-z_]+$/;
        if ( !re.test(val) ) {
            if (doalert) alert("Identificadores solo pueden contener letras o numeros, o \"_\" sin espacios");
            validflag = false;
		    }
	      else {
	          field.value = val.toLowerCase();
				}
    }
    if ( mandatory == true ){
        if ( field.value.length == 0 ) {
            if (doalert) alert("El campo debe contener un valor.");
            validflag = false;
		    }
	  }
    if ( !validflag ){
        field.focus();
        field.select();
    }
    window.isvalid = validflag;
    return validflag;
}

// La siguiente funcion permite terminar la aplicacion
function terminar() {
    var resp = confirm( "�Est� seguro que desea terminar la aplicaci�n?" );
		if ( resp ) {
		    window.navigate( "/ninfa/bye.tmpl?login=bye" );
		}
}

// El siguiente m�todo permite evitar que se examine el c�digo fuente de la aplicaci�n
function mouseManager() {
    var btn = window.event.button;

		if ( btn == 2 ) {
		    alert( "Operaci�n no soportada!" );
		}
}

