/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function initMenu(modulo){
     $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    
     $("#menu-pagina").delegate(".menu-item","click", function(){
        menuEvents(this);
    });
    obtenerMenu("#menu-pagina",modulo);

    $("#menu-pagina").delegate(".menu-item", "click", function() {
        cargarPagina($(this).attr("ruta-pagina"));
    });
    
} 

function obtenerMenu(elemento,modulo) {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Menu&accion=Bootstrap",
        dataType: 'json',
        data: {
            opcion: "0",
            modulo:modulo
            
        },
        success: function(json) {
            /*if (validarSesion(json)) {
                cargarPagina("presentacion.jsp");
                var menu = new Menu("Menu", json);
                $(elemento).html(menu.generarMenu());
                
                alertaNovedadesPorAprobar();
            }*/
            console.log(json);
            var menu = new Menu("Opciones", json);
            $(elemento).html(menu.generarMenu());
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarPagina(pagina) {
    $.ajax({
        type: 'POST',
        url: "" + pagina,
        dataType: 'html',
        success: function(html) {
            $("#container").empty();
            $("#container").html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
function cargarPaginaParametros(pagina, parametros) {
    $.ajax({
        type: 'POST',
        url: "" + pagina,
        data: parametros,
        dataType: 'html',
        success: function(html) {
            $("#container").html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensaje(msj, width, height) {
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Salir": function() {
                $(this).dialog("close");
            }
        }
    });
}

function menuEvents(element){

        var elemento = $(element);
        var icono = $(element).find("span");
        $(".selected").toggleClass("selected");
        elemento.toggleClass("selected");      

        if(typeof(elemento.attr("buscar-hijos")) === "undefined"){
            cargarPagina(elemento.attr("ruta-pagina"));
            if(elemento.attr("class").toLowerCase().indexOf("children") < 0){
                mostrando = "";
            }
            
        }else{
            mostrando = elemento.attr("buscar-hijos");
            iconoAnterior = icono;
            $("#"+elemento.attr("buscar-hijos")).toggleClass("hide-items");
            icono.toggleClass("glyphicon-menu-right");
            icono.toggleClass("glyphicon-menu-down");
        } 
        element.preventDefault();
        $(".selected").removeClass("selected");
        
}
