/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



function Menu(nombreMenu, opciones) {
    this.nombreMenu = nombreMenu;
    this.opciones = opciones;
    this.generarMenu = function () {
        var menu = this.cabecera(this.nombreMenu);
        var tienePadre = false;
        var grupo = '';
        var nivel = 0;
        for (i = 0; i < this.opciones.length; i++) {
            if (opciones[i].ruta === "") {
                if (opciones[i].padre === grupo) {

                } else {
                    if (tienePadre) {
                        for (var x = 0; x < (nivel - opciones[i].nivel); x++) {
                            menu += '</div>';
                        }

                    }
                }
                menu += this.itemOpcionPadre(opciones[i]);
                grupo = opciones[i].padre;
                tienePadre = true;
                if (i === (this.opciones.length - 1)) {
                    for (var x = 0; x < (nivel - opciones[i].nivel); x++) {
                        menu += '</div>';
                    }
                }
            } else {

                if (opciones[i].padre === 0) {
                    if (tienePadre) {
                        for (var x = 0; x < (nivel - opciones[i].nivel); x++) {
                            menu += '</div>';
                        }

                        tienePadre = false;
                    }

                    menu += this.itemOpcionHijoRaiz(opciones[i]);

                } else {
                    menu += this.itemOpcionHijo(opciones[i]);
                    if (i === (this.opciones.length - 1)) {
                        for (var x = 0; x < (nivel - opciones[i].nivel); x++) {
                            menu += '</div>';
                        }
                    }
                }
                grupo = opciones[i].padre;
            }
            nivel = opciones[i].nivel;
        }
        menu += this.cerrarSesion();
        return menu;
    };

    this.cabecera = function (nombre) {
        return  '<li class = "sidebar-brand">' +
                '<a>' + nombre + '</a>' +
                '</li>';
    };
    this.itemOpcionHijo = function (opcion) {
        return '<li>' +
                '<a class="menu-item children" ruta-pagina="' + opcion.ruta + '">' + opcion.descripcion + '</a>' +
                '</li>';
    };
    this.itemOpcionHijoRaiz = function (opcion) {
        return '<li>' +
                '<a class="menu-item padre" ruta-pagina="' + opcion.ruta + '">' + opcion.descripcion + '</a>' +
                '</li>';
    };
    this.itemOpcionPadre = function (opcion) {
        return '<li>' +
                '<a class="menu-item padre" buscar-hijos="' + opcion.descripcion.replace(/ /g, "") + '">' + opcion.descripcion + '<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>' +
                '</li>' +
                '<div id="' + opcion.descripcion.replace(/ /g, "") + '" class="hide-items otro">';
    };
    this.cerrarSesion = function () {
        return '<li>' +
                '<a class="padre" onclick="salir()" href="#">Salir</a>' +
                '</li>';
    };
}


function salir() {
    window.close();
}

