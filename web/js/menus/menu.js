/****************************************************************
Men� en arbol accesible. 28-Jul-07
Autor tunait http://javascript.tunait.com/
Script de libre uso mientras se mantengan intactos los cr�ditos de autor.
****************************************************************/

/*****************************************
men� �rbol. enero 2014
jpacosta

{
    contenedor:"",
    label:"",
    datos: [
        {id_opcion:
        ,nivel:
        ,id_padre:
        ,label:""
        ,title:""
        ,url:""
        ,orden:
        ,id_perfil:""},
        ...,
        ...
    ]
}
******************************************/
function menuJSon(json) {
   var contenedor = document.getElementById(json.contenedor);
   if (!contenedor) {alert('elemento no encontrado'); return;}
   contenedor.innerHTML = '';   
   var nodo = document.createElement('LABEL');
   nodo.appendChild(document.createTextNode(json.label));
   nodo.onclick = function () {
        var childrenNode = this.parentNode.getElementsByTagName('LI');
        for (var y in childrenNode) {
            try {
                if (childrenNode[y] && childrenNode[y].className === 'abierto') {
                    childrenNode[y].className = 'cerrado';
                }
            } catch (exception) { continue; }
        }
   }
   contenedor.appendChild(nodo);
   nodo = document.createElement('UL');
   nodo.className = 'abierto';
   contenedor.appendChild(nodo);
   contenedor = nodo;
   var parent = null;
    for (var x in json.datos) {
        if (json.datos[x].id_padre > 0) {
            parent = document.getElementById(json.datos[x].id_padre);
            if (parent) {
                parent.className = 'cerrado';
                if (parent.children[1]) {
                    parent = parent.children[1];
                } else {
                    ul = document.createElement('UL');
                    parent.appendChild(ul);
                    parent.children[0].onclick = function() {
                        parentNode = this.parentNode;
                        parentNode.className = parentNode.className == 'abierto' 
                                             ? 'cerrado' : 'abierto';
                    }
                    parent = ul;
                }

            } else {
                continue;
            }
        }
        nodo = document.createElement('LI');
        nodo.id = json.datos[x].id_opcion;
        label = document.createElement('LABEL');
        label.id = 'lbl' + json.datos[x].id_opcion;
        label.title = json.datos[x].title;
        label.url = json.datos[x].url;
        if (json.datos[x].url) {
            label.onclick = function() {
                window.open(this.url, '', 'width=940,height=600,scrollbars=yes,resizable=yes,top=0,left=50,status=yes');
            };
        }
        label.appendChild(document.createTextNode(json.datos[x].label));
        nodo.appendChild(label);
        nodo.className = (label.url) ? 'enlace' : 'cerrado';
        if (parent) {
            parent.appendChild(nodo);
        }
        else {
            contenedor.appendChild(nodo);
        }
    }
}