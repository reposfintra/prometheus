/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



$(document).ready(function () {
    cargarAgenciasT();

});

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function cargarAgenciasT() {
    var operacion;
    var grid_tabla = jQuery("#tabla_agencias_trans");
    if ($("#gview_tabla_agencias_trans").length) {
        reloadGridtabla(grid_tabla, 34);
    } else {

        grid_tabla.jqGrid({
            caption: "AGENCIAS",
            url: "./controller?estado=Administracion&accion=Etes",
            mtype: "POST",
            datatype: "json",
            height: '220',
            width: '1000',
            colNames: ['Id Trans', 'Cod Trans', 'Transportadora', 'Cod Agencia', 'Nombre Agencia', 'Ciudad', 'Direccion', 'Correo', 'Pais', 'Ciudad', 'Dpto', 'Estado'],
            colModel: [
                {name: 'transid', index: 'transid', width: 60, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'transcod', index: 'transcod', width: 60, sortable: true, align: 'left', hidden: true},
                {name: 'transrazonsocial', index: 'transrazonsocial', width: 170, sortable: true, align: 'left', hidden: false},
                {name: 'agcodagencia', index: 'agcodagencia', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'agnombre', index: 'agnombre', width: 170, sortable: true, align: 'left', hidden: false},
                {name: 'ciudad', index: 'ciudad', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'agdireccion', index: 'agdireccion', width: 240, sortable: true, align: 'left', hidden: false},
                {name: 'agcorreo', index: 'agcorreo', width: 170, sortable: true, align: 'left', hidden: false},
                {name: 'pais', index: 'pais', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'codciudad', index: 'codciudad', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'departamento', index: 'departamento', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'estado', index: 'estado', width: 100, sortable: true, align: 'left', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 34
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_agencias_trans").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 0;
                ventanaAgencia(operacion);
            }
        });
        $("#tabla_agencias_trans").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                operacion = 1;
                var myGrid = jQuery("#tabla_agencias_trans"), i, selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.agcodagencia;
                var idtrans = filas.transid;
                var nombreagencia = filas.agnombre;
                var direccion = filas.agdireccion;
                var pais = filas.pais;
                var codciudad = filas.codciudad;
                var departamento = filas.departamento;
                var correo = filas.agcorreo;
                var estado = filas.estado;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "Activo") {
                        ventanaAgencia(operacion, id, idtrans, nombreagencia, direccion, pais, codciudad, departamento, correo);
                        //ventanaEditarproducto(nombreprop, id);
                    } else {
                        mensajesDelSistema("PARA EDITAR , EL ESTADO DEBE ESTAR ACTIVO", '300', '150', false);
                    }
                }
            }
        });
        $("#tabla_agencias_trans").navButtonAdd('#pager', {
            caption: "Activar / Inactivar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_agencias_trans"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.agcodagencia;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    Confirmacion("�ESTA SEGURO QUE DESEA CAMBIAR EL ESTADO?", '300', '150', id);
                }
            }
        });
    }
}

function reloadGridtabla(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Etes",
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaAgencia(operacion, id, idtrans, nombreagencia, direccion, pais, codciudad, departamento, correo) {
    cargarPais();
    cargarDepartamento();
    cargarCiudad();
    cargarTransportadoras();
    if (operacion === 0) {
        $("#dialogMsjagencias").dialog({
            width: '419',
            height: '300',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'REGISTRO AGENCIAS TRANSPORTADORAS',
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    guardarAgencia();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });

    } else if (operacion === 1) {
        $("#idedit").val(id);
        $("#nombreagencia").val(nombreagencia);
        $("#transportadoras").val(idtrans);
        $("#correo").val(correo);
        $("#direccion").val(direccion);
        cargarPais();
        $("#pais").val(pais);
        cargarDepartamento();
        $("#departamento").val(departamento);
        cargarCiudad();
        $("#ciudad").val(codciudad);
        $("#dialogMsjagencias").dialog({
            width: '419',
            height: '300',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'ACTUALIZAR AGENCIAS TRANSPORTADORAS',
            buttons: {//crear bot�n cerrar
                "Actualizar": function () {
                    actualizarAgencia();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    }
}

function guardarAgencia() {
    var nombreagencia = $("#nombreagencia").val();
    var transportadoras = $("#transportadoras").val();
    var correo = $("#correo").val();
    var direccion = $("#direccion").val();
    var ciudad = $("#ciudad").val();
    if ((nombreagencia !== "") && (transportadoras !== "") && (correo !== "") && (direccion !== "")) {
        expr = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!expr.test(correo)) {
            mensajesDelSistema("DEBE CAMBIAR EL CORREO", '230', '150', false);
        } else {
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Administracion&accion=Etes",
                async: false,
                data: {
                    opcion: 35,
                    nombreagencia: nombreagencia,
                    transportadoras: transportadoras,
                    correo: correo,
                    direccion: direccion,
                    ciudad: ciudad,
                },
                success: function (data) {
                    $("#nombreagencia").val('');
                    $("#transportadoras").val('');
                    $("#correo").val('');
                    $("#direccion").val('');
                    cargarAgenciasT();
                    mensajesDelSistema("EXITO AL GUARDAR ", '230', '150', true);
                }, error: function (result) {
                    alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
                }
            });
        }
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function actualizarAgencia() {
    var id = $("#idedit").val();
    var nombreagencia = $("#nombreagencia").val();
    var transportadoras = $("#transportadoras").val();
    var correo = $("#correo").val();
    var direccion = $("#direccion").val();
    var ciudad = $("#ciudad").val();
    if ((nombreagencia !== "") && (transportadoras !== "") && (correo !== "") && (direccion !== "")) {
        expr = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!expr.test(correo)) {
            mensajesDelSistema("DEBE CAMBIAR EL CORREO", '230', '150', false);
        } else {
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Administracion&accion=Etes",
                async: false,
                data: {
                    opcion: 36,
                    nombreagencia: nombreagencia,
                    transportadoras: transportadoras,
                    correo: correo,
                    direccion: direccion,
                    ciudad: ciudad,
                    id: id
                },
                success: function (data) {
                    $("#nombreagencia").val('');
                    $("#transportadoras").val('');
                    $("#correo").val('');
                    $("#direccion").val('');
                    cargarAgenciasT();
                    mensajesDelSistema("Exito al actualizar ", '230', '150', true);
                }, error: function (result) {
                    alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
                }
            });
        }
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function cargarTransportadoras() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 9
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#transportadoras').html('');
                $('#transportadoras').append("<option value=''> Seleccione.. </option>");
                for (var datos in json) {
                    $('#transportadoras').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
function cargarPais() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 21
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#pais').html('');
                $('#pais').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#pais').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarDepartamento(id) {
    var pais = $("#pais").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 22,
            pais: pais
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#departamento').html('');
                $('#departamento').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#departamento').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCiudad() {
    var departamento = $("#departamento").val();
    //var departamentoedit = $("#departamentoedit").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 23,
            departamento: departamento
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#ciudad').html('');
                $('#ciudad').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#ciudad').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function validarEmail(email) {
    //expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    expr = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (!expr.test(email))
        alert("Error: La direcci�n de correo es incorrecta.");
}
function Confirmacion(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {
                cambiarEstadoAgencia(id);
                $(this).dialog("destroy");
            },
            "Cancelar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}
function cambiarEstadoAgencia(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        data: {
            opcion: 37,
            id: id
        },
        success: function (data) {
            cargarAgenciasT();
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}