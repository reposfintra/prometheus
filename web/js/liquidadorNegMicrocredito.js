
var contr;
var baseurl;

function inicializar(base, controller){
    contr=controller;
    baseurl=base;
}

function verForms(trans){
    var p, url = "";    
        url = contr+"?estado=LiquidadorNeg&accion=Microcredito";
        p =  'opcion=buscarForm&trans='+trans;
    new Ajax.Request(
        url,
        {
            method: 'post',
            parameters: p,
            onLoading: loading,
            onComplete: openWin2
        }
    );
}

 function cargarTitulosValor(convenio){
                var url = contr+"?estado=Liquidador&accion=Negocios";
                var p =  'opcion=buscarTitVlrConv&idConvenio='+convenio;
                new Ajax.Request(
                url,
                {
                    method: 'post',
                    parameters: p,
                    onLoading: loading,
                    onSuccess: function(transport){
                        $("cmbTituloValor").update(transport.responseText);
                        document.getElementById("spangen").innerHTML = "";
                    },
                    onFailure: errorEnvio
                }
            );
            }

            function loading(){
                document.getElementById("spangen").innerHTML = '<img alt="cargando" src="'+baseurl+'/images/cargando.gif" name="imgload">';
            }


var win;

function openWin2(response){    
    $("spangen").innerHTML = "";
    $('contenido2').innerHTML=response.responseText;
    $('contenido2').style.display='block';
    
    win= new Window(
    {id: "diving",
        title: "Formularios",
        showEffectOptions: {duration:1},
        hideEffectOptions: {duration:1},
        destroyOnClose: true,
        onClose:closewin2,
        resizable: true,
        width:400,
        height:300

    });
    win.show(true);
    win.showCenter();
    win.setContent('contenido2', true, false);
    $('contenido2').style.visibility='visible';
}

function closewin2(){
    $('contenido2').style.display='none';
    $('contenido2').style.visibility='hidden';
    $('contenido2').innerHTML="";
}

function datosform(numero_solicitud, identificacion, valor_solic, convenio, tasaconv, montomin, montomax, plazomax, renovacion,fecha_primera_cuota, fianza,pre_aprobado){

  
    valor_solic = ReplaceAll(valor_solic, ",", "");
    document.getElementById("numsolc").value = numero_solicitud;
    document.getElementById("nit").value = identificacion;
    document.getElementById("txtValor").value = valor_solic;
    $('convid').disabled = false;
    document.getElementById("convid").value = convenio;
    document.getElementById("tasaconv").value = tasaconv;
    document.getElementById("montomin").value = montomin;
    document.getElementById("montomax").value = montomax;
    document.getElementById("plazomax").value = plazomax;
    document.getElementById("renovacion").value = renovacion;
    document.getElementById("fianza").value = fianza;
    document.getElementById("pre_aprobado").value = pre_aprobado;
    cargarTitulosValor(convenio);
    if (renovacion === "S" || pre_aprobado==="S") {
        
        carcularFecha1(fecha_primera_cuota);
        
    }
    win.close();
}


function carcularFecha1(fecha_primera_cuota){
    // alert(document.getElementById("fechainicio").value);
    var fecha1=fecha_primera_cuota;
    
    
    var aux=replaceAll(fecha1,"-","/");
    var date= new Date(aux);
    var days=date.getDate();
    var fecha="0099-99-99";
    var mes = date.getMonth() + 1;
    var anio = date.getFullYear();
    
    
    if (days >= 1 && days <= 2) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion3 = new Option(fecha, fecha);


        }

    }

    if (days > 2 && days <= 12) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-12";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion2 = new Option(fecha, fecha);
             if (mes === 11) {
              fecha = (anio + 1) + "-" + "01" + "-02";
              opcion3 = new Option(fecha, fecha);
             }else{
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
            opcion3 = new Option(fecha, fecha);
             }

        }
    }

    if (days > 12 && days <= 17) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion3 = new Option(fecha, fecha);


        } else {
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion1 = new Option(fecha, fecha);
             
            if (mes === 11) {
                
                fecha = (anio + 1) + "-" + "01" + "-02"; 
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion3 = new Option(fecha, fecha);
                 
             }else{
            
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
            opcion2 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
            opcion3 = new Option(fecha, fecha);
            }
        }

    }

    if (days > 17 && days <= 22) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-17";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            
            if (mes === 11) {
                
                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion1 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-17";
                opcion3 = new Option(fecha, fecha);
                
            }else{
                
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
            opcion2 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-17";
            opcion3 = new Option(fecha, fecha);
            }
        }

    }


    if (days > 22 && days <= 31) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-22";
            opcion3 = new Option(fecha, fecha);


        } else {
            
            if(mes === 11){
                
                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion0 = new Option(fecha, fecha, "defauldSelected");
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion1 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-17";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-22";
                opcion3 = new Option(fecha, fecha);

                                
            }else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-22";
            opcion3 = new Option(fecha, fecha);
            
            }

        }
    }
    
    document.formulario.primeracuota.options[0]=opcion0;
    document.formulario.primeracuota.options[1]=opcion1;
    document.formulario.primeracuota.options[2]=opcion2; 
    document.formulario.primeracuota.options[3]=opcion3; 
   
    
}


function errorEnvio(){
    document.getElementById("spangen").innerHTML = "";
    Dialog.alert("Error enviando los datos al servidor. Verifique que la sesion no este cerrada",{
                width:250,
                height:100,
                windowParameters: {className: "alphacube"}
    });
}

function cerrarDiv(){
    win.close();
}

 

function enviarFormLiquidador(){
    var enviar = true;
    var renovacion=$F("renovacion");
    var pre_aprobado=$F("pre_aprobado");
    var mensaje= "Error: debe llenar todos los campos";
    if($F("numsolc")!="" && $F("convid")!="" && $F("nit")!="" && $F("txtNumCuotas")!="" && $F("txtValor")!=""
         && $F("fechainicio")!="" && $F("primeracuota")!="" && $F("cmbTituloValor")!="" && $F("tipo_cuota")!=""){
        if(parseInt($F("txtNumCuotas"))>parseInt($F("plazomax"))){
            enviar=false;
            mensaje="El plazo del negocio supera el maximo establecido para el convenio";
        }       
        if(!(parseFloat($F("txtValor"))>=(parseFloat($F("montomin"))*parseFloat($F("salariomin")))&&parseFloat($F("txtValor"))<=parseFloat($F("montomax"))*parseFloat($F("salariomin")))){
            enviar=false;
            mensaje="El valor a financiar no se encuentra entre el monto minimo y maximo establecido en el convenio";
        }
        var dias=DiferenciaFechas ($F("primeracuota"),$F("fechainicio"));        
        if(dias<1){
            enviar=false;
            mensaje="La fecha de la primera cuota debe ser mayor a la fecha del negocio";
        }
        if (renovacion !== 'S' && pre_aprobado !=='S') {
            if (dias > parseInt($F("diaslimit"))) {
                enviar = false;
                mensaje = "La fecha de la primera cuota supera el numero maximo de dias permitidos";
            }
        } else{
//           // fechaCalculadaPC
//            var diferenciaInterna=DiferenciaFechas ($F("primeracuota"),$F("fechaCalculadaPC")); 
//            if(diferenciaInterna>15){
//                enviar = false;
//                mensaje = "La fecha de la primera cuota supera el numero maximo de dias permitidos para la renovacion";
//                
//            }
                
        }
       if(dias < 30){
           enviar=false;
            mensaje="La fecha de la primera cuota debe ser mayor a 30 dias.";
       } 
       if(!validarConfiguracionFianza()){
            enviar=false;
            mensaje = "No existe valor de factor de fianza configurado para dicho plazo y linea de negocio";
       }
    }else{
        enviar=false;
    }

    if(enviar){       
            var solc=$F("numsolc");
            var tasaconv=$F("tasaconv");
            var p, url = "";

            url = contr+"?estado=Liquidador&accion=Negocios";
            p =  'opcion=validartasa&solc='+solc+'&tasaconv='+tasaconv+'&renovacion='+renovacion;

            new Ajax.Request(
                url,
                {
                    method: 'post',
                    parameters: p,
                    onComplete: function (resp){
                        var res= resp.responseText.replace(/^\s+/g,'').replace(/\s+$/g,'');                        
                        if(res=="E"){
                            Dialog.alert("Error: La tasa de inter�s ha cambiado, debe comunicarse con su asesor en Fintravalores",{
                                width:250,
                                height:100,
                                windowParameters: {
                                    className: "alphacube"
                                }
                            });
                        }else{
                            if(res=="N"){
                                document.getElementById("tasa").value="N";
                            }
                            habilitar();
                            document.forms[0].submit();
                        }
                    }
                }
                );
       
    }else{
        Dialog.alert(mensaje,{
            width:250,
            height:100,
                windowParameters: {className: "alphacube"}
        });
    }
}

function habilitar() {
    frm = document.forms['formulario'];
    for(i=0; ele=frm.elements[i]; i++)
        ele.disabled=false;
}

function soloNumeros(id) {
             var precio = document.getElementById(id).value;
             precio =  precio.replace(/[^0-9^.]+/gi,"");
             document.getElementById(id).value = precio;
        }

function DiferenciaFechas (CadenaFecha1, CadenaFecha2) {
   //Obtiene dia, mes y a�o
    var fecha1 = new fecha( CadenaFecha1 )
    var fecha2 = new fecha( CadenaFecha2 )

   //Obtiene objetos Date
    var miFecha1 = new Date( fecha1.anio, fecha1.mes -1, fecha1.dia )
    var miFecha2 = new Date( fecha2.anio, fecha2.mes -1, fecha2.dia )

   //Resta fechas y redondea
    var diferencia = miFecha1.getTime() - miFecha2.getTime();
    var dias = Math.floor(diferencia /(1000 * 60 * 60 * 24));
    return dias;
 }

 function fecha( cadena ) {

   //Separador para la introduccion de las fechas
    var separador = "-"

   //Separa por dia, mes y a�o
    if ( cadena.indexOf( separador ) != -1 ) {
         var posi1 = 0
         var posi2 = cadena.indexOf( separador, posi1 + 1 )
         var posi3 = cadena.indexOf( separador, posi2 + 1 )
         this.anio = cadena.substring( posi1, posi2 )
         this.mes = cadena.substring( posi2 + 1, posi3 )
         this.dia = cadena.substring( posi3 + 1, cadena.length )
    } else {
         this.dia = 0
         this.mes = 0
         this.anio = 0
    }

 }

 function datosConvenio(){
    var convenio=$F("convid");
    var url = contr+"?estado=LiquidadorNeg&accion=Microcredito";
    var p =  'opcion=datosConvenio&idConvenio='+convenio;
    new Ajax.Request(
        url,
        {
            method: 'post',
            parameters: p,
            onSuccess: function(transport){
                var texto=transport.responseText;
                var array = texto.split(";_;");
                document.getElementById("tasaconv").value = array[0];
                document.getElementById("montomin").value = array[1];
                document.getElementById("montomax").value = array[2];
                document.getElementById("plazomax").value = array[3];
            },
            onFailure: errorEnvio
        }
        );
}

function cargardatosform(numero_solicitud, identificacion, valor_solic, convenio, tasaconv, montomin, montomax, plazomax,titulo){

   alert("entro ");
   document.getElementById("numsolc").value = numero_solicitud;
    document.getElementById("nit").value = identificacion;
    document.getElementById("txtValor").value = valor_solic;
    document.getElementById("convid").value = convenio;
    document.getElementById("tasaconv").value = tasaconv;
    document.getElementById("montomin").value = montomin;
    document.getElementById("montomax").value = montomax;
    document.getElementById("plazomax").value = plazomax;
    cargarTitulosValor(convenio,titulo);

}

 function cargarTitulosValor(convenio,titulo,op){
                var url = contr+"?estado=Liquidador&accion=Negocios";
                var p =  'opcion=buscarTitVlrConv&idConvenio='+convenio;
                new Ajax.Request(
                url,
                {
                    method: 'post',
                    parameters: p,
                    onLoading: loading,
                    onSuccess: function(transport){
                        $("cmbTituloValor").update(transport.responseText);
                        document.getElementById("spangen").innerHTML = "";
                        document.getElementById("cmbTituloValor").value=titulo;
                        //$('cmbTituloValor').disabled=true;
                           
                    },
                    onFailure: errorEnvio
                }
            );
             datosConvenio();  
            }

            function loading(){
                document.getElementById("spangen").innerHTML = '<img alt="cargando" src="'+baseurl+'/images/cargando.gif" name="imgload">';
            }


function ReplaceAll(Source,stringToFind,stringToReplace){

    var temp = Source;

    var index = temp.indexOf(stringToFind);

    while(index != -1){

        temp = temp.replace(stringToFind,stringToReplace);

        index = temp.indexOf(stringToFind);

    }

    return temp;

}

 function validarConfiguracionFianza(){
    var convenio=$F("convid");
    var fianza=$F("fianza");
    var plazo=$F("txtNumCuotas");
    var continuar = true;  
    if(fianza ==='S'){
        var url = contr + "?estado=Liquidador&accion=Negocios";
        var p = 'opcion=existeConfigFianza&idConvenio=' + convenio + '&plazo=' + plazo;
        new Ajax.Request(
                url,
                {
                    method: 'post',
                    asynchronous: false,
                    parameters: p,
                    onLoading: loading,
                    onSuccess: function (transport) {
                        var existeConfig = transport.responseText; 
                        if (existeConfig.startsWith("NO")) {
                            continuar = false;                            
                        }
                    },
                    onFailure: errorEnvio
                }
        );
    }
   return continuar;          
}
