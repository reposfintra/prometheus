// JavaScript Document
function verificarInsert(){ 
    var formulario = document.form1;
  	if( formulario.codigo.value =='' || formulario.descripcion.value=='' || formulario.agencia.value=='' 
	   || formulario.unidades.value=='' || formulario.ajuste.value=='') {
    	alert('�Para poder guardar el registro no pueden quedar campos vacios!');
    	return false;
  	}
  	if( (formulario.cuenta.value!='' && formulario.elemento.value!='') 
	  	|| (formulario.cuenta.value=='' && formulario.elemento.value=='') ){
	  	  	alert('�Para poder guardar el registro se debe ingresar o la cuenta o el elemento!');
			return false;
  	}
  	return true;
}

function ValidarCodigo() { 
  var formulario = document.form1;
  if(formulario.codigo.value =='') {
    alert('�Para Consulta debe llenar el campo Codigo!');
    return false;
  }
  return true;
}

function VerificarModificacion(ruta) { 
    form1.action = ruta;
    if(verificarInsert())
      form1.submit();
}

function buscar() { 
    var formulario = document.form1;
  if(formulario.codigo.value =='') {
    alert('�Para Consulta debe llenar el campo Codigo!');
  }
  else {
    form1.submit();
  }
}