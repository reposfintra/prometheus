// Peticiones servidor
function init() {
}


function GuardarInicialHCG() {
    
    alert("COCO");
    console.log("PASA POR ACA");
    $.ajax({
        url: '/fintra/controller?estado=Factura&accion=Electronica',
        datatype: 'json',
        type: 'get',
        //data: {opcion: 2, form: JSON.stringify(formulario), listadodocs:jsonDocs},
        data: {opcion: 1},
        async: false,
        success: function (json) {
            try {
                
                if (json.error) {
                    
                    mensajesDelSistema("Guardar información", "Error Guardando informacion", "270");
                    console.log(json.error);
                    
                } else {
                    /*
                    $('#id_filtro').val(json.solicitud.id_filtro);
                    console.log("id_filtro: "+json.solicitud.id_filtro);
                    console.log("num_solicitud: "+json.solicitud.num_solicitud);
                    console.log("HDC respuesta: " + json.hdc.info.data.codigo_respuesta);
                    
                    if (json.solicitud.id_filtro && json.hdc.success && json.hdc.info.data.codigo_respuesta === "OK") {
                        mensajesDelSistema("Guardar información", "Se guardó la presolicitud y consulta a Datacrédito correctamente.", "270");
                    } else if (json.solicitud.id_filtro && json.hdc.success && json.hdc.info.data.codigo_respuesta !== "OK") {
                        mensajesDelSistema("Guardar información", "Se guardó la presolicitud pero hubo un error al consultar a Datacrédito. Error: " + json.hdc.info.data.codigo_respuesta, "270");
                    } else {
                        mensajesDelSistema("Guardar información", json.solicitud.mensaje, "270");
                    }*/
                    
                }
                
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}


function confirmacionGuardarSalvarTodo(nuevo) {
    var jsonDocs = [];
    $('input[name="chk_docs_req"]:checked').each(function () {
        jsonDocs.push(parseInt(this.value));       
    });
    console.log(JSON.stringify(jsonDocs));
    var accion;
    if(nuevo) {
        accion="INSERT_FILTRO";
    } else {
        accion="UPDATE_FILTRO";
    }
    var formulario = {
        accion:accion,
        id_filtro:$('#id_filtro').val(),
        identificacion: $('#id_sol').val(),
        fec_nac:$('#fec_nac').val(),
        est_civil:$('#est_civil').val(),
        pr_apel:$('#pr_apel').val(),
        sg_apel:$('#sg_apel').val(),
        pr_nom:$('#pr_nom').val(),
        sg_nom:$('#sg_nom').val(),
        tel:$('#tel').val(),
        cel:$('#cel').val(),
        id_ocupacion:$('#ocups').val(),
        fec_ocupa:$('#fec_ocu').val(),
        id_config:$('#convenios').val(),
        id_empresa_pagaduria:$('#empresas_pagaduria').val(),
        tasa:formato($('#tasa_anual').val()).numero,
        per_gracia:formato($('#per_gracia').val()).numero,
        salario:formato($('#salario').val()).numero,
        extraprima:formato($('#extraprima').val()).numero,
        otro_ingr:formato($('#otro_ingr').val()).numero,
        desc_ley:formato($('#desc_ley').val()).numero,
        fac_seg:formato($('#fac_seg').val()).numero,
        plazo:$('#plazo').val(),
        vlr_cuota:formato($('#total_cuota').html()).numero,
        valor_credito:formato($('#valor').val()).numero,
        viable:$('#viable').val()  
        //vlr_seguro:$('#vlr_seguro').html(),
        //vlr_extra:$('#vlr_extra').html(),
        //total_cuota:$('#total_cuota').html()
    };
    console.log(formulario);
    $.ajax({
        url: '/fintra/controller?estado=Factura&accion=Electronica',
        datatype: 'json',
        type: 'get',
        data: {opcion: 2, form: JSON.stringify(formulario), listadodocs:jsonDocs},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema("Guardar información", "Error Guardando informacion", "270");
                    console.log(json.error);
                } else {
                    $('#id_filtro').val(json.solicitud.id_filtro);
                    console.log("id_filtro: "+json.solicitud.id_filtro);
                    console.log("num_solicitud: "+json.solicitud.num_solicitud);
                    console.log("HDC respuesta: " + json.hdc.info.data.codigo_respuesta);
                    
                    if (json.solicitud.id_filtro && json.hdc.success && json.hdc.info.data.codigo_respuesta === "OK") {
                        mensajesDelSistema("Guardar información", "Se guardó la presolicitud y consulta a Datacrédito correctamente.", "270");
                    } else if (json.solicitud.id_filtro && json.hdc.success && json.hdc.info.data.codigo_respuesta !== "OK") {
                        mensajesDelSistema("Guardar información", "Se guardó la presolicitud pero hubo un error al consultar a Datacrédito. Error: " + json.hdc.info.data.codigo_respuesta, "270");
                    } else {
                        mensajesDelSistema("Guardar información", json.solicitud.mensaje, "270");
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

