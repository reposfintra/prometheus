/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
        $("#container").css({'width': '1460px'});
    buscarAnticiposPorAprobar($("#transportadora").val());

    $("#aceptar").click(function () {
        aprobarTransferencia() ;
    });
    
    $("#anular").click(function () {
        confirmarAliminarAnticipo();
    });
    // setInterval(HacerAlgo, 300000);
});


function buscarAnticiposPorAprobar(id_transportadora) {
    var grid_tbl_aprobar_aticipos = jQuery("#tbl_aprobar_anticipos");

    grid_tbl_aprobar_aticipos.jqGrid({
        caption: "Aprobar anticipos",
        url: "./controller?estado=Administracion&accion=Logistica",
        mtype: "POST",
        datatype: "json",
        height: '600',
        width: '1425',
        colNames: ['Transportadora', 'Id manifiesto', 'Agencia', 'Conductor', 'Propietario', 'Placa', 'Planilla', 'Fecha', 'Valor','Usuario','Subt.','Producto', 'Reanticipo'],
        colModel: [
            {name: 'transportadora', index: 'transportadora', width: 120, align: 'left'},
            {name: 'id_manifiesto', index: 'id_manifiesto', width: 100, align: 'left', hidden: true},
            {name: 'nombre_agencia', index: 'nombre_agencia', sortable: true, width: 80, align: 'left'},
            {name: 'conductor', index: 'conductor', sortable: true, width: 195, align: 'left'},
            {name: 'propietario', index: 'propietario', sortable: false, width: 195, align: 'left'},
            {name: 'placa', index: 'placa', width: 80, align: 'center'},
            {name: 'planilla', index: 'planilla', width: 105, align: 'left', key: true},
            {name: 'fecha_anticipo', index: 'fecha_anticipo', width: 120, align: 'left'},
            {name: 'valor_anticipo', index: 'valor_anticipo', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0}},            
            {name: 'usuario_creacion', index: 'usuario_creacion', width: 100, align: 'left'},
            {name: 'subt', index: '', width: 60, align: 'center', align: 'right', search: false, sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0}},
            {name: 'descripcion', index: 'descripcion', width: 120, align: 'left', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0} },
            {name: 'reanticipo', index: 'reanticipo', width: 80, align: 'center'}
            
        ],
        rowNum: 10000,
        rowTotal: 10000000,
        pager: ('#page_aprobar_anticipo'),
        loadonce: true,
        rownumWidth: 30,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: false,
        footerrow: true,
        rownumbers: true,
        multiselect: true,
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        }, ajaxGridOptions: {
            async: false,
            data: {
                opcion: 11,
                id_transportadora: id_transportadora
            }
        },
        loadComplete: function () {
            cacularTotalesAprobacion(grid_tbl_aprobar_aticipos);
            if (grid_tbl_aprobar_aticipos.jqGrid('getGridParam', 'records') <= 0) {
                // mensajesDelSistema("No se encontraron resultados para los parametros seleccionados.", "270", "170");
            }

        },
        onSelectRow: function(rowId) {
            var selectedRowsIds = $('#tbl_aprobar_anticipos').jqGrid('getGridParam', 'selarrrow');
            var totalSum = 0;
            
            $.each(selectedRowsIds, function(index, selectedRowId) {
                totalSum += parseFloat(($('#tbl_aprobar_anticipos').jqGrid('getCell', selectedRowId, 'valor_anticipo')));
            });
            
            $('#tbl_aprobar_anticipos').jqGrid('footerData', 'set', { 
                usuario_creacion: 'T. Aprobado',
                subt: totalSum });
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 200);
        }
    }).navGrid("#page_aprobar_anticipo", {add: false, edit: false, del: false, search: false, refresh: false}, {});



}

function reloadGridAnticiposPorAprobar() {
    jQuery("#tbl_aprobar_anticipos").setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Logistica",
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: 11,
                id_transportadora: $("#transportadora").val()
            }
        }
    });

     jQuery("#tbl_aprobar_anticipos").trigger("reloadGrid");
}

function cacularTotalesAprobacion(grid_tbl_aprobar_aticipos) {

    var valor_anticipo = grid_tbl_aprobar_aticipos.jqGrid('getCol', 'valor_anticipo', false, 'sum');

    grid_tbl_aprobar_aticipos.jqGrid('footerData', 'set', {
        fecha_anticipo: 'Totales',
        valor_anticipo: valor_anticipo
    });

}

function aprobarTransferencia() {
    var rowId = $("#tbl_aprobar_anticipos").jqGrid('getGridParam', 'selarrrow');
  if (rowId.length > 0) {
    var arrayJson = [];
    for (var i = 0; i < rowId.length; i++) {
        var rowData = jQuery("#tbl_aprobar_anticipos").getRowData(rowId[i]);
        arrayJson.push(rowData);
    }
    
    var myJsonString = JSON.stringify(arrayJson);
    console.log(myJsonString);
    
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        data: {
            opcion: 12,
            datajson:myJsonString
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                    if(json.error==='error'){
                          mensajesDelSistema('Lo sentimos no se pudieron aprobar los anticipos', '300', '180');                        
                    }
                    
                    if (json.respuesta === 'OK') {
                       reloadGridAnticiposPorAprobar();
                    }

            } else {

                  mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
     }else{
         mensajesDelSistema('Debe seleccionar al menos un anticipo.', '300', '150');
    }
}


function anularAnticipos() {
    var rowId = $("#tbl_aprobar_anticipos").jqGrid('getGridParam', 'selarrrow');
  if (rowId.length > 0) {
    var arrayJson = [];
    for (var i = 0; i < rowId.length; i++) {
        var rowData = jQuery("#tbl_aprobar_anticipos").getRowData(rowId[i]);
        arrayJson.push(rowData);
    }
    
    var myJsonString = JSON.stringify(arrayJson);
    console.log(myJsonString);
    
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        data: {
            opcion: 15,
            datajson:myJsonString
        },
        success: function (json) {
            $('#dialogo4').dialog("close");

            if (!isEmptyJSON(json)) {

                    if(json.error==='error'){
                          mensajesDelSistema('Lo sentimos no se pudieron anular los anticipos', '300', '180');                        
                    }
                    
                    if (json.respuesta === 'OK') {
                       reloadGridAnticiposPorAprobar();
                    }

            } else {
                  mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
     }else{
         mensajesDelSistema('Debe seleccionar al menos un anticipo.', '300', '150');
    }
}

function confirmarAliminarAnticipo() {
    var msj = "Esta seguro que desea Eliminar el Anticipo?";
    $("#msj4").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo4").dialog({
        width: 300,
        height: 150,
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Si": function () {
                anularAnticipos();
            }
            , "No": function () {
                $(this).dialog("close");
            }
        }
    });
}