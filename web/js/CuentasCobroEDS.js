/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#contenedor").hide();
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $("#fecha").datepicker("setDate", myDate);
    $("#fechafin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    cargarPropietario();
    cargarComboEds();

    $("#buscar").click(function () {
        var comboeds = $('#eds').val();
        if ((comboeds === '') || (comboeds === null)) {
            mensajesDelSistema("Por favor seleccione una estacion de servicio", '297', '140', false);
        } else {
            $("#contenedor").show();
            var url, op;
            url = "./controller?estado=Administracion&accion=Logistica";
            op = 23;
            cargarFacturasPendientes(url, op);
        }
    });

    $("#generar").click(function () {
        generarCuantasCobro();
    });

    $("#buscarcxp").click(function () {
        var url;
        var op;
        if (pagina === 'visualizacion') {
            var comboeds = $('#eds').val();
            if ((comboeds === '') || (comboeds === null)) {
                mensajesDelSistema("Por favor seleccione una estacion de servicio", '297', '140', false);
            } else {
                cargarCXPEDS();
                // alert(pagina);
                //url = "./controller?estado=Administracion&accion=Eds";
                // op = 43;
                // cargarFacturasPendientes(url, op);
            }
        } else {
            url = "./controller?estado=Administracion&accion=Logistica";
            op = 23;
            cargarFacturasPendientes(url, op);
        }
    });

    cargarFechas();

});

function  limpiar() {
    $("#contenedor").hide();
    $("#tabla_cuentas_cobro").jqGrid("clearGridData", true);
}
function cargarPropietario() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 9
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#propietario').html('');
                for (var datos in json) {
                    $('#propietario').append('<option value=' + json[datos].id + '>' + json[datos].razon_social + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarComboEds() {
    var idpropietario = $("#propietario").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 13,
            idpropietario: idpropietario
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                if (idpropietario !== '') {
                    $('#eds').html('');
                    if (Object.keys(json).length === 0) {
                        mensajesDelSistema("No tiene EDS registrada", '200', '150', false);
                        limpiar();
                    } else {
                        limpiar();
                        $('#eds').append("<option value=''> Seleccione.. </option>");
                        for (var datos in json) {
                            $('#eds').append('<option value=' + datos + '>' + json[datos] + '</option>');
                        }
                    }
                } else {
                    $('#eds').html('');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarFacturasPendientes(url, op) {
    var grid_tabla = jQuery("#tabla_cuentas_cobro");
    if ($("#gview_tabla_cuentas_cobro").length) {
        reloadGridFacturasPendientes(grid_tabla, url, op);
    } else {
        grid_tabla.jqGrid({
            caption: "FACTURAS PENDIENTES",
            url: url,
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '1360',
            colNames: ['# Venta', 'Fecha Venta', 'Planilla', 'Cedula', 'Coductor', 'Placa', 'Producto', '$ Producto',
                'Cant Suministrada', 'ud Medida', 'Subtotal', 'Comision', 'Total'],
            colModel: [
                {name: 'num_venta', index: 'num_venta', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'fecha_venta', index: 'fecha_venta', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'planilla', index: 'planilla', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'cedula', index: 'cedula', width: 70, sortable: true, align: 'left', hidden: false},
                {name: 'conductor', index: 'conductor', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'placa', index: 'placa', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'producto', index: 'producto', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'precio_producto', index: 'precio_producto', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                //  {name: 'cantidad_suministrada', index: 'cantidad_suministrada', width: 120, sortable: true, align: 'rigth', hidden: false},
                {name: 'cantidad_suministrada', index: 'cantidad_suministrada', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'unidad_medida', index: 'unidad_medida', width: 90, sortable: true, align: 'left', hidden: false},
                {name: 'subtotal', index: 'subtotal', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'comision', index: 'comision', sortable: true, width: 70, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'total', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                    limpiar();
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: op,
                    eds: $("#eds").val(),
                    fecha: $("#fecha").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, gridComplete: function () {
                var comision = grid_tabla.jqGrid('getCol', 'comision', false, 'sum');
                var SubTotal = grid_tabla.jqGrid('getCol', 'subtotal', false, 'sum');
                var Total = grid_tabla.jqGrid('getCol', 'total', false, 'sum');
                grid_tabla.jqGrid('footerData', 'set', {unidad_medida: 'TOTAL', total: Total, subtotal: SubTotal, comision: comision});
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_cuentas_cobro").navButtonAdd('#pager1', {
            caption: "Exportar Excel",
            onClickButton: function () {
                var info = grid_tabla.getGridParam('records');
                if (info > 0) {
                    exportarExcel();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }

            }
        });
    }
}


function cargarCXPEDS() {
    var grid_tabla = jQuery("#tabla_cuentas_cobro");
    if ($("#gview_tabla_cuentas_cobro").length) {
        reloadGridCXPEDS(grid_tabla, 38);
    } else {
        grid_tabla.jqGrid({
            caption: "VISUALIZACION CXP EDS",
            url: "./controller?estado=Administracion&accion=Eds",
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '1300',
            colNames: ['Estado', '# CXP', 'Identificacion', 'Nombre', 'ID Eds', 'Hc', 'Valor Neto', 'Valor Total Abonos', 'Valor Saldo', 'Documento de Ajuste', 'Fecha de Creacion', 'Usuario de Creacion', 'Imprimir'],
            colModel: [
                {name: 'cxp_estado', index: 'cxp_estado', width: 60, sortable: true, align: 'center', hidden: false},
                {name: 'cxp_documento', index: 'cxp_documento', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'cxp_proveedor', index: 'cxp_proveedor', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'edsnombre', index: 'edsnombre', width: 180, sortable: true, align: 'center', hidden: false},
                {name: 'edsid', index: 'edsid', width: 180, sortable: true, align: 'center', hidden: true},
                {name: 'hc', index: 'hc', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'cxp_vlr_neto', index: 'cxp_vlr_neto', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cxp_vlr_total_abonos', index: 'cxp_vlr_total_abonos', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cxp_vlr_saldo', index: 'cxp_vlr_saldo', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'documentoajuste', index: 'documentoajuste', width: 100, sortable: true, align: 'center', hidden: false, formatter: 'currencyFmatter'},
                {name: 'fecha_creacion', index: 'fecha_creacion', width: 95, sortable: true, align: 'left', hidden: false},
                {name: 'usuarios_creacion', index: 'usuarios_creacion', width: 95, sortable: true, align: 'left', hidden: false},
                {name: 'imprimir', index: 'imprimir', width: 100, align: 'center', sortable: false}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onCellSelect: function (rowid, index, contents, event) {
                var celda = grid_tabla.jqGrid('getGridParam', 'colModel');
//                var numventa = grid_tabla.getRowData(rowid).num_venta;
//                alert(numventa);
                if (celda[index].name === "documentoajuste") {
                    var cxp = grid_tabla.getRowData(rowid).cxp_documento;
                    var numero_nota = grid_tabla.getRowData(rowid).documentoajuste;
                    var creation_date=grid_tabla.getRowData(rowid).fecha_creacion;
                    var id_eds=grid_tabla.getRowData(rowid).edsid;
                    ventanaDocumentoAjuste(cxp, numero_nota,creation_date,id_eds);
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 1;
                var myGrid = jQuery("#tabla_cuentas_cobro"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                //var id = filas.idpropietario;
                var cxp = filas.cxp_documento;
                ventanacxpDetalle(cxp);
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                    limpiar();
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 38,
                    eds: $("#eds").val(),
                    fechainicio: $("#fecha").val(),
                    fechafin: $("#fechafin").val(),
                    cxp: $("#cxp").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, gridComplete: function () {
                var cant = jQuery("#tabla_cuentas_cobro").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cl = cant[i];
                    be = "<input style='height:20px;width:50px;color: #050505' type='button' value='imprimir' onclick=\"exportarpdf('" + cl + "');\" />";
                    jQuery("#tabla_cuentas_cobro").jqGrid('setRowData', cant[i], {imprimir: be});
                }
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_cuentas_cobro").navButtonAdd('#pager1', {
            caption: "Exportar Excel",
            onClickButton: function () {
                var info = grid_tabla.getGridParam('records');
                if (info > 0) {
                    exportarExcelCxp();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
//        $("#tabla_cuentas_cobro").navButtonAdd('#pager1', {
//            caption: "Pdf",
//            onClickButton: function () {
//                var myGrid = jQuery("#tabla_cuentas_cobro"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
//                filas = myGrid.jqGrid("getLocalRow", selRowIds);
//                //var id = filas.idpropietario;
//                var cxp = filas.cxp_documento;
//                var edsid = filas.edsid;
//                var total = filas.cxp_vlr_neto;
//                exportarpdf(cxp, total, edsid);
//            }
//        });
    }
}

function cargarCxpDetalle(cxp) {
    var grid_tabla = jQuery("#tabla_cuentas_cobro_detalle");
    if ($("#gview_tabla_cuentas_cobro_detalle").length) {
        reloadGridCxpDetalles(grid_tabla, 43, cxp);
    } else {
        grid_tabla.jqGrid({
            // caption: "DETALLE",
            url: "./controller?estado=Administracion&accion=Eds",
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '1400',
            colNames: ['# CXP', '# Venta', 'Fecha Venta', 'Planilla', 'Cedula', 'Coductor', 'Placa', 'Producto', '$ Producto',
                'Cant Suministrada', 'ud Medida', 'Subtotal', 'Comision', 'Total'],
            colModel: [
                {name: 'cxp_documento', index: 'cxp_documento', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'num_venta', index: 'num_venta', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'fecha_venta', index: 'fecha_venta', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'planilla', index: 'planilla', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'cedula', index: 'cedula', width: 70, sortable: true, align: 'left', hidden: false},
                {name: 'conductor', index: 'conductor', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'placa', index: 'placa', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'producto', index: 'producto', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'precio_producto', index: 'precio_producto', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                //  {name: 'cantidad_suministrada', index: 'cantidad_suministrada', width: 120, sortable: true, align: 'rigth', hidden: false},
                {name: 'cantidad_suministrada', index: 'cantidad_suministrada', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'unidad_medida', index: 'unidad_medida', width: 90, sortable: true, align: 'left', hidden: false},
                {name: 'subtotal', index: 'subtotal', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'comision', index: 'comision', sortable: true, width: 70, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'total', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager2',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                    limpiar();
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 43,
                    cxp: cxp
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, gridComplete: function () {
                var comision = grid_tabla.jqGrid('getCol', 'comision', false, 'sum');
                var SubTotal = grid_tabla.jqGrid('getCol', 'subtotal', false, 'sum');
                var Total = grid_tabla.jqGrid('getCol', 'total', false, 'sum');
                grid_tabla.jqGrid('footerData', 'set', {unidad_medida: 'TOTAL', total: Total, subtotal: SubTotal, comision: comision});
            }
        }).navGrid("#pager2", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_cuentas_cobro_detalle").navButtonAdd('#pager2', {
            caption: "Exportar Excel",
            onClickButton: function () {
                var info = grid_tabla.getGridParam('records');
                if (info > 0) {
                    exportarExcelDetalle();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }

            }
        });
    }
}

function reloadGridFacturasPendientes(grid_tabla, url, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: url,
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                eds: $("#eds").val(),
                fecha: $("#fecha").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function reloadGridCXPEDS(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Eds",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                eds: $("#eds").val(),
                fechainicio: $("#fecha").val(),
                fechafin: $("#fechafin").val(),
                cxp: $("#cxp").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function reloadGridCxpDetalles(grid_tabla, op, cxp) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Eds",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                cxp: cxp
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function generarCuantasCobro() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        data: {
            opcion: 24,
            eds: $("#eds").val(),
            fecha: $("#fecha").val()
        },
        success: function (data) {
            var res = data.respuesta;
            if (res === 'OK') {
                mensajesDelSistema("Exito al generar cuenta de cobro, Ver pdf en descargas", '363', '145', true);

                //cargarFacturasPendientes();
            } else if (res === 'PDFERROR') {
                mensajesDelSistema("Exito al generar cuentas de cobro \n Error al generar el pdf", '363', '140', false);
            } else if (res === 'BPERROR') {
                mensajesDelSistema("No se encontro el propietario de la EDS", '363', '140', false);
            } else {
                mensajesDelSistema("Error al generar la cuenta de cobro", '363', '140', false);
            }
            limpiar();
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarFechas() {
    var eds = $("#eds").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 37,
            eds: eds
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#fecha').html('');
                for (var datos in json) {
                    $('#fecha').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }

            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }
        , error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function exportarExcel() {
    var fullData = jQuery("#tabla_cuentas_cobro").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Administracion&accion=Eds",
        data: {
            listado: myJsonString,
            opcion: 39
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function exportarExcelDetalle() {
    var fullData = jQuery("#tabla_cuentas_cobro_detalle").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Administracion&accion=Eds",
        data: {
            listado: myJsonString,
            opcion: 45
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function exportarExcelCxp() {
    var fullData = jQuery("#tabla_cuentas_cobro").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaExcxp").dialog(opt);
    $("#divSalidaExcxp").dialog("open");
    $("#imgloadExcxp").show();
    $("#msjExcxp").show();
    $("#respExcxp").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Administracion&accion=Eds",
        data: {
            listado: myJsonString,
            opcion: 44
        },
        success: function (resp) {
            $("#imgloadExcxp").hide();
            $("#msjExcxp").hide();
            $("#respExcxp").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respExcxp').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function ventanacxpDetalle(cxp) {
    cargarCxpDetalle(cxp);
    $("#dialogMscxp").dialog({
        width: '1430',
        height: '430',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'DETALLE',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");

            }
        }
    });
}

function exportarpdf(rowid) {
    var grid_tabla = jQuery("#tabla_cuentas_cobro");
    var cxp = grid_tabla.getRowData(rowid).cxp_documento;
    var edsid = grid_tabla.getRowData(rowid).edsid;
    var total = grid_tabla.getRowData(rowid).cxp_vlr_neto;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Eds",
        data: {
            opcion: 50,
            cxp: cxp,
            total: total,
            edsid: edsid
        },
        success: function (json) {
            //alert (json.respuesta);
            if (json.respuesta === "GENERADO") {
                mensajesDelSistema("Exito en la generacion de pdf, por favor ver en el log", '335', '150', true);
            }
        }
    });
}
function ventanaDocumentoAjuste(cxp, numero_nota,creation_date,id_eds) {
    cargarDetDocumentoAjuste(cxp, numero_nota,creation_date,id_eds);
    $("#dialogMsNC").dialog({
        width: '1360',
        height: '400',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Nota de Ajuste',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}


function cargarDetDocumentoAjuste(cxp, numero_nota,creation_date,id_eds) {
    var grid_tabla = jQuery("#tabla_detalle_NC");
    if ($("#gview_tabla_detalle_NC").length) {
        reloadGridDetalleDocumentoAjuste(grid_tabla, 51, cxp, numero_nota,creation_date,id_eds);
    } else {
        grid_tabla.jqGrid({
            //caption: "VENTAS",
            url: "./controller?estado=Administracion&accion=Eds",
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '1335',
            colNames: ['Estado', '# NC', 'Tipo Documento', 'Identificacion', 'Nombre', 'Hc', 'Base (#Galones/Total Venta Gasolina)', 'Factor Descuento', 'Valor Neto', 'Valor Total Abonos', 'Valor Saldo', 'Fecha de Creacion', 'Usuario de Creacion'],
            colModel: [
                {name: 'nc_estado', index: 'nc_estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'nc_documento', index: 'nc_documento', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'tipo_documento_con', index: 'tipo_documento_con', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'nc_proveedor', index: 'nc_proveedor', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'edsnombre', index: 'edsnombre', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'hc', index: 'hc', width: 80, sortable: true, align: 'left', hidden: true},
                {name: 'base_numgalon_totalventa', index: 'base_numgalon_totalventa', sortable: true, width: 190, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                {name: 'factor_descuento', index: 'factor_descuento', width: 95, sortable: true, align: 'left', hidden: false},
                {name: 'nc_vlr_neto', index: 'nc_vlr_neto', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'nc_vlr_total_abonos', index: 'nc_vlr_total_abonos', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'nc_valor_saldo', index: 'nc_valor_saldo', sortable: true, width: 100, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'fecha_creacion', index: 'fecha_creacion', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'usuarios_creacion', index: 'usuarios_creacion', width: 95, sortable: true, align: 'left', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager3',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            ondblClickRow: function (rowid, iRow, iCol, e) {
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 51,
                    cxp: cxp,
                    numero_nota: numero_nota,
                    creation_date:creation_date,
                    id_estacion:id_eds
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, gridComplete: function () {
            }
        }).navGrid("#pager3", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}
function reloadGridDetalleDocumentoAjuste(grid_tabla, op, cxp,numero_nota,creation_date,id_eds) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Eds",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                cxp: cxp,
                numero_nota: numero_nota,
                creation_date:creation_date,
                id_estacion:id_eds
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}