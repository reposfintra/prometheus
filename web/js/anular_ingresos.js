let detalle;

window.onload = function () {
    document.getElementById("buscar").addEventListener("click", buscarIngresos);
    document.getElementById("salir").addEventListener("click", function () {
        window.close();
    });
};

function isEmptyJSON(obj) {
    for (let i in obj) {
        return false;
    }
    return true;
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html(msj);
    } else {
        $("#msj").html(msj);
    }
    $("#dialogMsg").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function loading(msj, width, height) {
    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });
    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function reloadGridMostrar(gridTable, opcion, numIngreso) {
    gridTable.setGridParam({
        datatype: "json",
        url: "./controller?estado=Contabilidad&accion=General",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                idIngreso: numIngreso
            }
        }
    });
    gridTable.trigger("reloadGrid");
}

function buscarIngresos() {
    let numIngreso = document.getElementById("numIngreso").value;
    if (numIngreso === "") {
        alert("Debe ingresar el n�mero del ingreso");
    } else {
        let gridTable = jQuery("#tablaIngresos");

        if ($("#gview_tablaIngresos").length) {
            reloadGridMostrar(gridTable, 18, numIngreso);
        } else {
            gridTable.jqGrid({
                caption: "Ingreso",
                url: "./controller?estado=Contabilidad&accion=General",
                mtype: "POST",
                datatype: "json",
                height: "300",
                width: document.getElementById("tablaIngresosWrapper").style.width,
                autowidth: true,
                colNames: ["Cliente", "N�mero ingreso", "Estado", "Tipo documento", "Sucursal", "Banco", "Descripci�n", "Valor", "Periodo", "Fecha ingreso", "Fecha consignaci�n", "Transacci�n", "Cuenta"],
                colModel: [
                    {name: "nitcli", index: "nitcli", width: 100, align: "center", sortable: false},
                    {name: "num_ingreso", index: "num_ingreso", width: 100, align: "center", sortable: false, key: true},
                    {name: "estado", index: "estado", width: 100, align: "center", sortable: false},
                    {name: "tipo_documento", index: "tipo_documento", width: 100, align: "center", sortable: false},
                    {name: "bank_account_no", index: "bank_account_no", width: 100, align: "center", sortable: false},
                    {name: "branch_code", index: "branch_code", width: 200, align: "center", sortable: false},
                    {name: "descripcion_ingreso", index: "descripcion_ingreso", width: 1000, align: "center", sortable: false},
                    {name: "vlr_ingreso", index: "vlr_ingreso", width: 100, align: "center", sortable: false,
                        formatter: 'currency', formatoptions: {decimalSeparator: ",", housandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                    {name: "periodo", index: "periodo", width: 80, align: "center", sortable: false},
                    {name: "fecha_ingreso", index: "fecha_ingreso", width: 100, align: "center", sortable: false},
                    {name: "fecha_consignacion", index: "fecha_consignacion", width: 110, align: "center", sortable: false},
                    {name: "transaccion", index: "transaccion", width: 100, align: "center", sortable: false},
                    {name: "cuenta", index: "cuenta", width: 100, align: "center", sortable: false}
                ],
                rowNum: 1,
                loadonce: false,
                viewrecords: true,
                gridview: true,
                shrinkToFit: false,
                pager: "#pager",
                hidegrid: false,
                pgbuttons: true,
                sortname: "num_ingreso",
                ajaxGridOptions: {
                    dataType: "json",
                    type: "POST",
                    data: {
                        opcion: 18,
                        idIngreso: numIngreso
                    }
                },
                jsonReader: {
                    repeatitems: false,
                    subgrid: {
                        root: "detalle",
                        repeatitems: true,
                        cell: "cell"
                    }
                },
                loadComplete: function (data) {
                    $("#dialogLoading").hide();
                },
                loadError: function (xhr, status, error) {
                    mensajesDelSistema("Error en la comunicacion con el servidor", 400, 150);
                },
                beforeProcessing: function (data) {
                    detalle = data[0].detalle;
                },
                subGrid: true,
                subGridRowExpanded: function (subgrid_id, row_id) {
                    // we pass two parameters
                    // subgrid_id is a id of the div tag created within a table
                    // the row_id is the id of the row
                    // If we want to pass additional parameters to the url we can use
                    // the method getRowData(row_id) - which returns associative array in type name-value
                    // here we can easy construct the following                    

                    let subgrid_table_id;
                    subgrid_table_id = subgrid_id + "_t";
                    jQuery("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table>");
                    jQuery("#" + subgrid_table_id).jqGrid({
                        datatype: "local",
                        colNames: ["Negocio", "N�mero ingreso", "item", "Documento", "Descripci�n", "Tipo documento", "Valor ingreso", "Saldo factura", "Cuenta", "Transacci�n"],
                        colModel: [
                            {name: "referencia_1", index: "referencia_1", width: 110, key: true, align: "center", sortable: false},
                            {name: "num_ingreso", index: "num_ingreso", width: 110, key: true, align: "center", sortable: false},
                            {name: "item", index: "item", width: 50, align: "center", sortable: false},
                            {name: "documento", index: "documento", width: 110, align: "center", sortable: false},
                            {name: "descripcion", index: "descripcion", width: 300, align: "center", sortable: false},
                            {name: "tipo_documento", index: "tipo_documento", width: 50, align: "center", sortable: false},
                            {name: "valor_ingreso", index: "valor_ingreso", width: 150, align: "center", sortable: false,
                                formatter: 'currency', formatoptions: {decimalSeparator: ",", housandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                            {name: "saldo_factura", index: "saldo_factura", width: 100, align: "center", sortable: false,
                                formatter: 'currency', formatoptions: {decimalSeparator: ",", housandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                            {name: "cuenta", index: "cuenta", width: 100, align: "center", sortable: false},
                            {name: "transaccion", index: "transaccion", width: 100, align: "center", sortable: false}
                        ],
                        height: '100%',
                        rowNum: 20,
                        sortname: 'item',
                        sortorder: "asc",
                        autoencode: true,
                        autowidth: true,
                        gridview: true,
                        data: detalle,
                        jsonReader: {repeatitems: false}
                    });
                }
            }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: true}).navButtonAdd("#pager", {
                caption: "Anular",
                title: "Anular",
                onClickButton: function () {
                    anularIngreso();
                }});
        }
    }
}

function anularIngreso() {
    let numIngreso = document.getElementById("numIngreso").value;
    let grid = $("#tablaIngresos");

    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                let json = JSON.parse(xhr.responseText);
                
                if (json.success) {
                    mensajesDelSistema("Ingreso anulado", 150, 150);
                } else {
                    mensajesDelSistema(json.error, 400, 150);
                }
            } else {
                mensajesDelSistema("Error en la comunicacion con el servidor", 400, 150);
            }
        }
    };
    xhr.open("POST", "./controller?estado=Contabilidad&accion=General&opcion=" + 19 + "&idIngreso=" + numIngreso, true);
    xhr.send();
}
