var indiceInicial;


function establecerIndiceInicial(formulario) {      
    indiceInicial = formulario.ubicacion.selectedIndex;    
    formulario.ubicacion.focus();            
}

function ubicacionSelect(formulario, controller, numpla){
    var indiceActual = formulario.ubicacion.selectedIndex;
    var vec = formulario.ubicacion.value.split("/");
    //if ( indiceActual < indiceInicial && ( vec[1] == 'PC' || ( ( vec[0] == formulario.ultpto.value && vec[1] || vec[0] == formulario.origen.value ) && vec[1] == 'CIU' ) ) ){
    if ( indiceActual < indiceInicial ){
        alert("Usted ha escogido una opción invalida");
        formulario.ubicacion.selectedIndex = indiceInicial;
    }
    else if ( indiceActual > indiceInicial ){        
	   forma.action = controller+"?estado=Insert&accion=RepMovTrafico&mensaje=Sancion&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico";
	   forma.submit();
    }
}

function isSancion(valor){
	var sw = 0;
	var vec = valor.split("/");
	//alert(vec+" Tamano"+vec.length);
	if(vec[0]=="EFR"){
		
		if(forma.frontera.value ==""){
			sw=1;
			alert("El despacho no es internacional no se puede generar tipo de reporte Entrega Frontera");
			forma.tiporep.selectedIndex=3;
		}
	}
	if(vec[1]=="BOD"){
		if(forma.frontera.value ==""){
			sw=1;
			alert("El despacho no es internacional no se puede generar tipo de reporte Entrega Frontera");
			forma.tiporep.selectedIndex=3;
		}
	}
        if(vec[1]=="L"){
          var vec = forma.ubicacion.value.split("/");
          if(forma.destino.value != vec[0]){
            sw=1;
            alert("El reporte de llegada solo se puede realizar en el destino");
            forma.tiporep.selectedIndex=3;
          }
         }
	if ( vec.length >1){
		if(vec[1]=="S"){
			window.open(forma.controller.value+"?estado=Informacion&accion=Planilla_sancion&numpla="+forma.numpla.value,'','status=yes,scrollbars=no,resizable=yes,width=875,height=575');
	   	}
		if(vec[1]=="O"){
			window.location=forma.controller.value+"?estado=Buscar&accion=NumPlanilla&pagina=ingresarObservacion.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla="+forma.numpla.value;
	   	}			
		if(vec[1]=="DE"){
			window.location=forma.controller.value+"?estado=Cambio_via&accion=RepMovTrafico&pagina=DetencionMovtraf.jsp&carpeta=jsp/trafico/Movimiento_trafico&opcion=4&rutas="+forma.rutas.value+"&numpla="+forma.numpla.value;
	   	}		
		if(vec[1]=="D"){
			window.location=forma.controller.value+"?estado=Informacion&accion=Planilla&cmd=show&numpla="+forma.numpla.value;
	   	}		
		if(vec[1]=="H"){
			window.open(forma.controller.value+"?estado=Informacion&accion=Planilla&cmd=show&numpla="+forma.numpla.value,'','status=yes,scrollbars=no,resizable=yes,width=875,height=575');
	   	}
		if(vec[1]=="E"){
			if(sw==0){
				window.open(forma.controller.value+"?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/Movimiento_trafico&pagina=RepEntregaParcial.jsp&marco=no",'','status=yes,scrollbars=no,resizable=yes,width=875,height=575');
			}
	   	}
		if(vec[1]=="R"){
			window.location=forma.controller.value+"?estado=Buscar&accion=NumPlanilla&pagina=ReinicioMovtraf.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla="+forma.numpla.value;
	   	}
    }
	
}

function salidaSeleccionable(formulario){
    var tiporeporte = formulario.tiporep.selectedIndex;
    if( tiporeporte == 3 ){
        alert("Esta planilla ya se encuentra en via");
        formulario.tiporep.selectedIndex = 0;
    }
}

function soloOrigen(formulario){
	//alert ("entro a soloorigen");
    var list = formulario.tiporep;
	var tiporeporte = list.options[list.selectedIndex].value;
	//alert("Tipo rep="+tiporeporte);
	if( tiporeporte == "SAL/"){
        var ubicacion = formulario.ubicacion.selectedIndex;
        if(ubicacion!=0){
            alert("Debe seleccionar el origen para este tipo de reporte.");
            formulario.ubicacion.selectedIndex = 0;
        }
    }
}

function seleccionarOrigen(formulario){
    var tiporeporte = formulario.tiporep.selectedIndex;
    if( tiporeporte == 3 ){
        formulario.ubicacion.selectedIndex = 0;
    }
}

function mostrarCaravana(url){
	window.open (url,'','status=yes,scrollbars=yes,resizable=yes,width=500,height=300');
	forma.enviar.value='SI';
}
function enviarReporte(url){
	if(forma.enviar.value==""){
		mostrarCaravana(url);
	}
	else{
		forma.mensaje.value='insert';
		forma.submit();
	}
}


function llenarObservacion(){
	var list =forma.tiporep;
	var tiporeporte = list.options[list.selectedIndex].text;
	
	if(list.options[list.selectedIndex].value!=""){
	//alert (list.options[list.selectedIndex].value ) ;
		if(list.options[list.selectedIndex].value == "ECL/" ||  list.options[list.selectedIndex].value == "EIN/" )	{
			tiporeporte="FIN DE VIAJE";
		}
		var list2 =forma.ubicacion;
		var pc = list2.options[list2.selectedIndex].text;

    	forma.obaut.value = tiporeporte+" "+pc+" "+forma.c_dia.value+" "+forma.c_hora.value;
	}
	else{
		forma.obaut.value = "";
		}
}

function llenarObservacionReinicio(){
    forma.obaut.value = "Reinicio "+forma.nomubic.value+" "+forma.c_dia.value+" "+forma.c_hora.value;
}

function llenarObservacion2(){
	var list2 =forma.ubicacion;
	var pc = list2.options[list2.selectedIndex].text;
    forma.obaut.value = "Observación "+pc+" "+forma.c_dia.value+" "+forma.c_hora.value;
}

//jose 2006-03-09
function enviaFormulario(BASEURL){
	var sw = 0, sw1 = 0, fe = 0;
	if(forma.c_hora.value.length>4 && forma.c_dia.value.length>9){
		var fecha = forma.c_dia.value+" "+forma.c_hora.value;
		var fecha_actu = getDate (new Date(), 'yyyy-mm-dd hh:mi');
		var mes  = forma.c_dia.value.substring(5,7);
		var ano  = forma.c_dia.value.substring(0,4);
		var dia  = forma.c_dia.value.substring(8,10);
		if( mes=="01" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "02" ){
			if( ano%4 == 0 ){
				if(dia>0 && dia <= 29){
					fe = 0;
				}
				else{
					fe = 1;
				}	
			}
			else{
				if(dia>0 && dia <= 28){
					fe = 0;
				}
				else{
					fe = 1;
				}
			}
		}
		else if( mes=="03" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "04" ){
			if(dia>0 && dia <= 30){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "05" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "06" ){
			if(dia>0 && dia <= 30){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "07" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "08" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "09" ){
			if(dia>0 && dia <= 30){
				fe = 0;
			}
			else{
				fe = 1;
			}
		}
		else if( mes == "10" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}
		}
		else if( mes == "11" ){
			if(dia>0 && dia <= 30){
				fe = 0;
			}
			else{
				fe = 1;
			}
		}
		else if( mes == "12" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}
		}
		else{
			fe = 1;
		}
		if( fecha <= fecha_actu && fe == 0){
			if(forma.c_dia.value.length>9){
				if(forma.c_dia.value.substring(5,7)>=0 && forma.c_dia.value.substring(5,7) <=12 ){
					if(forma.c_dia.value.substring(8,10)>=0 && forma.c_dia.value.substring(8,10) <=31 ){
						sw1 = 0;
					}
					else{
						sw1 = 1;
					}
				}
				else{
					sw1 = 1;			
				}	
			}
			else{
				sw1 = 1;
			}
			if(forma.c_hora.value.length>4){
				if(forma.c_hora.value.substr(0,2)>=0 && forma.c_hora.value.substr(0,2) <24 ){
					if(forma.c_hora.value.substr(3,5)>=0 && forma.c_hora.value.substr(3,5) <60 ){
						sw = 0;
					}
					else{
						sw = 1;
					}
				}
				else{
					sw = 1;
				}
			}
			else{
				sw = 1;
			}
			if(sw1 == 1 || sw == 1 ){
				if(sw==1){
					forma.c_hora.select();
				}
				else{
					forma.c_dia.select();
				}
				alert("Fecha mal ingresada");
			}
			else{
				if( forma.causas && forma.causas.value == "" ){
					alert("Debe seleccionar una causa");
				}
				else{
					if( forma.tiporep.value == "" ){
						alert("Debe seleccionar un tipo de reporte");
					}
					else if( forma.ubicacion.value == "" ){
						alert("Debe seleccionar una ubicación");
					}	
					else{
						forma.submit();
						document.forma.mod.src = BASEURL+"/images/botones/aceptarDisable.gif";
						document.mod.disabled = true;
					}		
				}
			}
		}
		else{
			if(fe == 1){
				alert("La fecha no existe");
			}
			else{
				alert("La fecha del reporte no debe ser mayor a la fecha actual");
			}
			forma.c_dia.select();
		}
	}
	else{
		alert("Debe llenar la fecha correctamente");
		if(forma.c_hora.value.length>4)
			forma.c_dia.select();
		else
			forma.c_hora.select();
	}
}

//jose 2006-04-11
function enviaFormularioReinicio(BASEURL){
	var sw = 0, sw1 = 0, fe = 0;
	if(forma.c_hora.value.length>4 && forma.c_dia.value.length>9){
		var fecha = forma.c_dia.value+" "+forma.c_hora.value;
		var mes  = forma.c_dia.value.substring(5,7);
		var ano  = forma.c_dia.value.substring(0,4);
		var dia  = forma.c_dia.value.substring(8,10);
		if( mes=="01" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "02" ){
			if( ano%4 == 0 ){
				if(dia>0 && dia <= 29){
					fe = 0;
				}
				else{
					fe = 1;
				}	
			}
			else{
				if(dia>0 && dia <= 28){
					fe = 0;
				}
				else{
					fe = 1;
				}
			}
		}
		else if( mes=="03" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "04" ){
			if(dia>0 && dia <= 30){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "05" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "06" ){
			if(dia>0 && dia <= 30){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "07" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "08" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}	
		}
		else if( mes == "09" ){
			if(dia>0 && dia <= 30){
				fe = 0;
			}
			else{
				fe = 1;
			}
		}
		else if( mes == "10" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}
		}
		else if( mes == "11" ){
			if(dia>0 && dia <= 30){
				fe = 0;
			}
			else{
				fe = 1;
			}
		}
		else if( mes == "12" ){
			if(dia>0 && dia <= 31){
				fe = 0;
			}
			else{
				fe = 1;
			}
		}
		else{
			fe = 1;
		}
		if( fe == 0){
			if(forma.c_dia.value.length>9){
				if(forma.c_dia.value.substring(5,7)>=0 && forma.c_dia.value.substring(5,7) <=12 ){
					if(forma.c_dia.value.substring(8,10)>=0 && forma.c_dia.value.substring(8,10) <=31 ){
						sw1 = 0;
					}
					else{
						sw1 = 1;
					}
				}
				else{
					sw1 = 1;			
				}	
			}
			else{
				sw1 = 1;
			}
			if(forma.c_hora.value.length>4){
				if(forma.c_hora.value.substr(0,2)>=0 && forma.c_hora.value.substr(0,2) <24 ){
					if(forma.c_hora.value.substr(3,5)>=0 && forma.c_hora.value.substr(3,5) <60 ){
						sw = 0;
					}
					else{
						sw = 1;
					}
				}
				else{
					sw = 1;
				}
			}
			else{
				sw = 1;
			}
			if(sw1 == 1 || sw == 1 ){
				if(sw==1){
					forma.c_hora.select();
				}
				else{
					forma.c_dia.select();
				}
				alert("Fecha mal ingresada");
			}
			else{
				if( forma.causas && forma.causas.value == "" ){
					alert("Debe seleccionar una causa");
				}
				else{
					if( forma.tiporep.value == "" ){
						alert("Debe seleccionar un tipo de reporte");
					}
					else if( forma.ubicacion.value == "" ){
						alert("Debe seleccionar una ubicación");
					}	
					else{
						forma.submit();
						document.forma.mod.src = BASEURL+"/images/botones/aceptarDisable.gif";
						document.mod.disabled = true;
					}		
				}
			}
		}
		else{
			if(fe == 1){
				alert("La fecha no existe");
			}
			else{
				alert("La fecha del reporte no debe ser manor a la fecha actual");
			}
			forma.c_dia.select();
		}
	}
	else{
		alert("Debe llenar la fecha correctamente");
		if(forma.c_hora.value.length>4)
			forma.c_dia.select();
		else
			forma.c_hora.select();
	}
}
function cargarCambioVia ( controller, numpla, ruta , origen , destino, dia, hora){
	//alert(controller+'?estado=Cambio_via&accion=RepMovTrafico&pagina=CambioViaMovtraf.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla='+numpla+'&opcion=0&ruta='+ruta+'&origen='+origen+'&destino='+destino+'&c_dia='+dia+'&c_hora='+hora)
	window.location= controller+'?estado=Cambio_via&accion=RepMovTrafico&pagina=CambioViaMovtraf.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla='+numpla+'&opcion=0&ruta='+ruta+'&origen='+origen+'&destino='+destino+'&c_dia='+dia+'&c_hora='+hora;
}
function cargarCambioRuta ( controller, numpla, ruta , origen , destino, dia, hora){
	window.location= controller+'?estado=Cambio_via&accion=RepMovTrafico&pagina=CambioRutaMovtraf.jsp&carpeta=jsp/trafico/Movimiento_trafico&paso=ok&numpla='+numpla+'&opcion=0&ruta='+ruta+'&origen='+origen+'&destino='+destino+'&c_dia='+dia+'&c_hora='+hora;
}
//jose 2006-03-28
function mostrarCiudadDinamica(lista,campo){
	var texto = campo.value.toLowerCase();
	for( var i=0; i<lista.length; i++ ){
		var v = lista[i].innerText.toLowerCase();
		if ( v.indexOf(texto) >= 0 ){
			lista.selectedIndex = i;
			break;
		}
	}
}

function ValidarFormularioCopia(){
	var fecha_actu = getDate (new Date(), 'yyyy-mm-dd hh:mi');
	var swP = 0;
	for (i = 0; i < forma.elements.length; i++){
		if (forma.elements[i].type == "checkbox" && forma.elements[i].checked){
			var numpla = forma.elements[i].id;
			var fecha_form = document.getElementById("c_dia"+numpla);
			var hora  = document.getElementById("c_hora"+numpla);
			var causales  = document.getElementById("causas"+numpla);
		  	var sw = 0, sw1 = 0, fe = 0;
			if(hora.value.length>4 && fecha_form.value.length>9){
				var fecha = fecha_form.value+" "+hora.value;
				var mes  = fecha_form.value.substring(5,7);
				var ano  = fecha_form.value.substring(0,4);
				var dia  = fecha_form.value.substring(8,10);
				if( mes=="01" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "02" ){
					if( ano%4 == 0 ){
						if(dia>0 && dia <= 29){
							fe = 0;
						}
						else{
							fe = 1;
						}	
					}
					else{
						if(dia>0 && dia <= 28){
							fe = 0;
						}
						else{
							fe = 1;
						}
					}
				}
				else if( mes=="03" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "04" ){
					if(dia>0 && dia <= 30){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "05" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "06" ){
					if(dia>0 && dia <= 30){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "07" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "08" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "09" ){
					if(dia>0 && dia <= 30){
						fe = 0;
					}
					else{
						fe = 1;
					}
				}
				else if( mes == "10" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}
				}
				else if( mes == "11" ){
					if(dia>0 && dia <= 30){
						fe = 0;
					}
					else{
						fe = 1;
					}
				}
				else if( mes == "12" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}
				}
				else{
					fe = 1;
				}		
				if( fecha <= fecha_actu && fe == 0 ){
					if(fecha_form.value.length>9){
						if(fecha_form.value.substring(5,7)>=0 && fecha_form.value.substring(5,7) <=12 ){
							if(fecha_form.value.substring(8,10)>=0 && fecha_form.value.substring(8,10) <=31 ){
								sw1 = 0;
							}
							else{
								sw1 = 1;
							}
						}
						else{
							sw1 = 1;			
						}	
					}
					else{
						sw1 = 1;
					}
					if(hora.value.length>4){
						if(hora.value.substr(0,2)>=0 && hora.value.substr(0,2) <24 ){
							if(hora.value.substr(3,5)>=0 && hora.value.substr(3,5) <60 ){
								sw = 0;
							}
							else{
								sw = 1;
							}
						}
						else{
							sw = 1;
						}
					}
					else{
						sw = 1;
					}
					if(sw1 == 1 || sw == 1 ){
						if(sw==1){
							hora.select();
						}
						else{
							fecha_form.select();
						}
						alert("Fecha mal ingresada");
						swP=1;
						break;
					}
					else{
						if( causales && causales.value == "" ){
							alert("Debe seleccionar una causa");
							causales.select();
							swP=1;
							break;
						}
					}
				}
				else{
					if(fe == 1){
						alert("La fecha no existe");
					}
					else{
						alert("La fecha del reporte no debe ser manor a la fecha actual");
					}
					fecha_form.select();
					swP=1;
					break;
				}
			}
			else{
				alert("Debe llenar la fecha correctamente");
				swP=1;
				if(hora.value.length>4)
					fecha_form.select();
				else
					hora.select();
			}
	  	}	 
	}
	if(swP==0){
		forma.submit();
		document.forma.mod.src = BASEURL+"/images/botones/aceptarDisable.gif";
		document.mod.disabled = true;	
	}
}

 function validacionUlt( tam ){
  var combo = document.getElementById('ubicacion');
  var tiporep = document.getElementById('tiporep');
  var pc = tam.split(",");
  var tipo = tiporep.value.split("/");
  var ult = pc.length - 1;
  
 
  if( combo.value.split("/")[0] == pc[ult] && (tipo[0]!='EIN') && (tipo[0]!='EFR') && (tipo[0]!='ECL') && (tipo[0]!='ENT') && (tipo[0]!='OBS') && (tipo[1]!='L') )  {
   	alert('No se puede realizar un reporte diferente a una entrega\n               en el final de la via');
   	return false;
  }
  else{
	  if(document.getElementById("eintermed").value.indexOf(document.getElementById("prox_rep").value)>=0 ){
		  	
			if(  tipo[0]!='ENT' ){
				alert ("Solo se permite reporte de Entrega Parcial o Detencion en una ciudad marcada como entregas intermedias.");
			 	return false;
		  	}
	  }
   
  }
  return true;
 }


function ValidarFormularioVariasCopia(BASEURL){
	var fecha_actu = getDate (new Date(), 'yyyy-mm-dd hh:mi');
	var swP = 0;
	for (i = 0; i < forma.elements.length; i++){
		if (forma.elements[i].type == "checkbox" && forma.elements[i].checked){
			var numpla = forma.elements[i].id;
			var fecha_form = document.getElementById("c_dia"+numpla);
			var hora  = document.getElementById("c_hora"+numpla);
		  	var sw = 0, sw1 = 0, fe = 0;
			if(hora.value.length>4 && fecha_form.value.length>9){
				var fecha = fecha_form.value+" "+hora.value;
				var mes  = fecha_form.value.substring(5,7);
				var ano  = fecha_form.value.substring(0,4);
				var dia  = fecha_form.value.substring(8,10);
				if( mes=="01" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "02" ){
					if( ano%4 == 0 ){
						if(dia>0 && dia <= 29){
							fe = 0;
						}
						else{
							fe = 1;
						}	
					}
					else{
						if(dia>0 && dia <= 28){
							fe = 0;
						}
						else{
							fe = 1;
						}
					}
				}
				else if( mes=="03" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "04" ){
					if(dia>0 && dia <= 30){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "05" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "06" ){
					if(dia>0 && dia <= 30){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "07" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "08" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}	
				}
				else if( mes == "09" ){
					if(dia>0 && dia <= 30){
						fe = 0;
					}
					else{
						fe = 1;
					}
				}
				else if( mes == "10" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}
				}
				else if( mes == "11" ){
					if(dia>0 && dia <= 30){
						fe = 0;
					}
					else{
						fe = 1;
					}
				}
				else if( mes == "12" ){
					if(dia>0 && dia <= 31){
						fe = 0;
					}
					else{
						fe = 1;
					}
				}
				else{
					fe = 1;
				}		
				if( fecha <= fecha_actu && fe == 0 ){
					if(fecha_form.value.length>9){
						if(fecha_form.value.substring(5,7)>=0 && fecha_form.value.substring(5,7) <=12 ){
							if(fecha_form.value.substring(8,10)>=0 && fecha_form.value.substring(8,10) <=31 ){
								sw1 = 0;
							}
							else{
								sw1 = 1;
							}
						}
						else{
							sw1 = 1;			
						}	
					}
					else{
						sw1 = 1;
					}
					if(hora.value.length>4){
						if(hora.value.substr(0,2)>=0 && hora.value.substr(0,2) <24 ){
							if(hora.value.substr(3,5)>=0 && hora.value.substr(3,5) <60 ){
								sw = 0;
							}
							else{
								sw = 1;
							}
						}
						else{
							sw = 1;
						}
					}
					else{
						sw = 1;
					}
					if(sw1 == 1 || sw == 1 ){
						if(sw==1){
							hora.select();
						}
						else{
							fecha_form.select();
						}
						alert("Fecha mal ingresada");
						swP=1;
						break;
					}
				}
				else{
					if(fe == 1){
						alert("La fecha no existe");
					}
					else{
						alert("La fecha del reporte no debe ser manor a la fecha actual");
					}
					fecha_form.select();
					swP=1;
					break;
				}
			}
			else{
				alert("Debe llenar la fecha correctamente");
				swP=1;
				if(hora.value.length>4)
					fecha_form.select();
				else
					hora.select();
			}
	  	}	 
	}
	if(swP==0){
		forma.submit();
		document.forma.mod.src = BASEURL+"/images/botones/aceptarDisable.gif";
		document.mod.disabled = true;	
	}
}

function ValidarFormularioHoras(){
    var fecha_actu = getDate (new Date(), 'yyyy-mm-dd hh:mi');    
    var swP = 0;    
    var fecha_form = document.getElementById( "c_dia" );
    var hora  = document.getElementById( "c_hora" );
    var sw = 0, sw1 = 0, fe = 0;    
    if(hora.value.length>4 && fecha_form.value.length>9){
        var fecha = fecha_form.value+" "+hora.value;                
        var mes  = fecha_form.value.substring(5,7);
        var ano  = fecha_form.value.substring(0,4);
        var dia  = fecha_form.value.substring(8,10);
        if( mes=="01" || mes=="03" || mes=="05" || mes=="07" || mes=="08" || mes=="10" || mes=="12" ){
            fe = (dia>0 && dia <= 31)? 0 : 1;
        }else if( mes == "02" ){
            fe = (ano%4 == 0)? ((dia>0 && dia <= 29)? 0 : 1) : ((dia>0 && dia <= 28)? 0 : 1);                        
        }            
        else if( mes == "04" || mes=="06" || mes=="09" || mes=="11" ){
            fe = (dia>0 && dia <= 30)? 0 : 1;
        }else{
            fe = 1;
        }
        if( fecha <= fecha_actu && fe == 0 ){
            if(fecha_form.value.length>9){
                if(fecha_form.value.substring(5,7)>=0 && fecha_form.value.substring(5,7) <=12 ){
                    sw1 = (fecha_form.value.substring(8,10)>=0 && fecha_form.value.substring(8,10) <=31 )? 0 : 1;
                }else{
                    sw1 = 1;
                }
            }else{
                sw1 = 1;
            }
            if(hora.value.length>4){
                if(hora.value.substr(0,2)>=0 && hora.value.substr(0,2) <24 ){
                    sw = (hora.value.substr(3,5)>=0 && hora.value.substr(3,5) <60 )? 0 : 1;                        
                }else{
                    sw = 1;
                }
            }else{
                sw = 1;
            }
            if(sw1 == 1 || sw == 1 ){
                if(sw==1){
                    hora.select();
                }else{
                    fecha_form.select();
                }
                alert("Fecha mal ingresada");
                swP=1;                
            }
        }else{
            if(fe == 1){
                alert("La fecha no existe");
            }else{
                alert("La fecha del reporte no debe ser manor a la fecha actual");
            }
            fecha_form.select();
            swP=1;            
        }
        
    }else{
        alert("Debe llenar la fecha correctamente");
        swP=1;
        if(hora.value.length>4) fecha_form.select();
        else hora.select();
    }    
    if(swP==0){                
            forma.submit();		
    }
}
