/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
   
    $('#generar').click(function () {
        CargarSolicitudesAsignar();    
        //GenerarDocumentos();    
    });
});



function CargarSolicitudesAsignar() {
    var grid_tabla = $("#tabla_info_negocio"); 
    
    if ($("#gview_tabla_info_negocio").length) {
        reloadGridTabla(grid_tabla, 20);
    } else {
        grid_tabla.jqGrid({
            caption: "Datos Negocio",
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '100',
            width: '1515',
            colNames: ['Convenio','Negocio','Num Solicitud','Cliente','Identificacion Cliente','Codeudor','Identificacion Codeudor','Valor Negocio','Fecha Negocio','Estado Negocio'],
            colModel: [
                {name: 'convenio', index: 'convenio', width: 160,sortable: true, align: 'center', hidden: false},
                {name: 'cod_neg', index: 'cod_neg', width: 90, sortable: true, align: 'center',   hidden: false},
                {name: 'numero_solicitud', index: 'numero_solicitud', width: 100, sortable: true, align: 'center',   hidden: false},
                {name: 'nombre_titular', index: 'nombre_titular', width: 250, sortable: true, align: 'center',   hidden: false}, 
                {name: 'identificacion_titular', index: 'identificacion_titular', width: 150, sortable: true, align: 'center',   hidden: false},
                {name: 'nombre_codeudor', index: 'nombre_titular', width: 250, sortable: true, align: 'center',   hidden: false}, 
                {name: 'identificacion_codeudor', index: 'identificacion_titular', width: 150, sortable: true, align: 'center',   hidden: false},
                {name: 'vr_negocio', index: 'vr_negocio', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'fecha_negocio', index: 'fecha_negocio', width: 90, sortable: true, align: 'center',   hidden: false},
                {name: 'estado_neg', index: 'estado_neg', width: 90, sortable: true, align: 'center',   hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#pager',
            pgtext: null,
            pgbuttons: false,
            multiselect:false,           
            
            jsonReader:{
                root: "rows",
                repeatitems: false,
                id: "0"
                
            },
            loadComplete: function () {
                
                if (grid_tabla.jqGrid('getGridParam', 'records') <= 0) {
                    mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            ajaxGridOptions: {
                
                data: {
                    opcion: 20, 
                    negocio: $("#negocio").val()
                }                
                
            },
            
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);                
            }
        }).navGrid('#pager', {add: false, edit: false, del: false, search: false, refresh: false});
        $("#tabla_info_negocio").navButtonAdd('#pager', {
            caption: "Generar Documentos",
            title: "Generar Documentos",
            onClickButton: function () {
                if (grid_tabla.jqGrid('getGridParam', 'records') > 0) {
                     GenerarDocumentos();
                }else {
                   mensajesDelSistema("Por favor no se encuentran negocios para generar documentos.", '250', '150', true);   
                }
               
            }
        });      
           
    }
    
}

function reloadGridTabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",
       
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op, 
                negocio: $("#negocio").val() 
            }
        }
    });
    grid_tabla.trigger("reloadGrid");    
     
}



function GenerarDocumentos(){ 
    var grid_tabla = $("#tabla_info_negocio");
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Negocios&accion=Fintra",
                dataType: 'json',
                data: {
                    opcion: 19,
                    negocio : grid_tabla.jqGrid('getRowData',1).cod_neg,
                     identificacion:grid_tabla.jqGrid('getRowData',1).identificacion_titular
                },
                success: function (json) {  
                    console.log(json);
                    if (!isEmptyJSON(json)) {
                       
                        if (json.respuesta == "OK") {
                            cerrarDiv("#divSalidaEx");
                            mensajesDelSistema("Exito al generar documentos, dir�jase al log de descarga", '250', '150', true);
                            
                           
                        } 
                        else  {
                          cerrarDiv("#divSalidaEx");
                             mensajesDelSistema("ERROR:" +json.respuesta , '450', '200', true);
                            
                        } 


                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos ocurri� un error al generar documentos!!", '250', '150');

                    }
                },
                error: function (xhr) {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Error al generar documentos: " + "\n" +
                             xhr.responseText, '650', '250', true);
                }
            });
    
}



function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                
                $(this).dialog("close");
            }
        }
    });

}


function cerrarDiv(div)
{
    $(div).dialog('close');
}

