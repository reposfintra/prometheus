var isIE = document.all?true:false;
var isNS = document.layers?true:false;
var request  = null;
var response = null;
var contadorPeticiones = 0;
var contadorPlanillasMalas = 0;
var beneficiario = null;  



  function beneficiarios(nit, nombre, tipo, banco, sucursal, cuenta){
     this.nit = nit;
     this.nombre = nombre;
     this.nit_name = '['+ nit +'] ' + nombre;
     this.tipo = tipo;
     this.tipo_name = (tipo=='EF'?'EFECTIVO': (tipo=='CA'?'CUENTA AHORROS': (tipo=='CC'?'CUENTA CORRIENTE':'') ) );
     this.banco = banco;
     this.sucursal = sucursal;
     this.cuenta = cuenta;
     return this;
  }


  function validarPlanillasPP(){
      if (contadorPlanillasMalas!=0){
          alert('Se encontraron ' + contadorPlanillasMalas + ' planilla(s) con errores, por favor retirelas para poder continuar');
          return false;
      }
      return true;
  }

  function newWindow(url, name){
	  /*option=" status=yes, width="+ (screen.width-30) +", height="+ (screen.height-80)  +",  scrollbars=yes, statusbars=yes, resizable=yes, menubar=no ,top=10 , left=10 ";
	  ventana=window.open('',nombre,option);
	  ventana.location.href=url;
	  ventana.focus();*/
	  option = "dialogWidth:800px;dialogHeight:600px;";
	  var wmodal = window.showModalDialog(url,name,option);
	  //alert(wmodal);
   }
   function closeModal (param){
  	  var MyArgs = new Array(param);
	  window.returnValue = MyArgs;
	  window.close();
   } 	
	function createRequest() {
	  try {
		request = new XMLHttpRequest();
	  } catch (trymicrosoft) {
		try {
		  request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (othermicrosoft) {
		  try {
			request = new ActiveXObject("Microsoft.XMLHTTP");
		  } catch (failed) {
			request = false;
		  }
		}
	  }
	
	  if (!request)
		alert("Error initializing XMLHttpRequest!");
		response = false;
	}
	
	
	function getParameter (index){
		var valor = "";
		if (response){
			var datos = response.split('|');
			valor = datos[index].replace(/\n/g,'');					
		}
		return valor;
	}	
	
	
	function buscarDatosProveedor(e, CONTROLLER, nit){
		var key = (isIE) ? window.event.keyCode : e.which;
		if(key==13){
			getDatosProveedor(CONTROLLER, nit);
		}
		return true;
	}
	
	function getDatosProveedor(CONTROLLER , nit) {
	  createRequest();
	  url = CONTROLLER + "?estado=Transferencias&accion=ProntoPago&Opcion=ConsultarProveedor&nit=" + nit;
	  request.open("GET", url, true);
	  request.onreadystatechange = updatePageByProveedor;
	  request.send(null);
	}

	function updatePageByProveedor() {
		if (request.readyState == 4){  
		   if (request.status == 200){
			
			 response  = request.responseText;
			 estado = getParameter(0);			 

			 if (estado == 'not found'){
				alert ('Proveedor no encontrado en la base de datos');
				dter.style.display = 'none';
				dnom.style.display = 'none';
				dban.style.display = 'none';
				dsuc.style.display = 'none';						
				forma.tercero.value  = '';
				forma.nombre.value   = '';				
				forma.banco.value    = '';
				forma.sucursal.value = '';
			 }	
			 else{			  
			    //forma.btercero.value ='';
				dter.style.display   = 'block';
				dnom.style.display   = 'block';				
				dban.style.display   = 'block';
				dsuc.style.display   = 'block';						
				forma.tercero.value  = getParameter(1);
				forma.nombre.value   = getParameter(2);
				forma.banco.value    = getParameter(4);
				forma.sucursal.value = getParameter(5);						
			 }				 									
		   }
		   else
			 alert("Error: status code is " + request.status);
	    }
        }

		
        function inicializarTercero(nit, nombre, banco, sucursal){
				//forma.btercero.value ='';
				dter.style.display   = 'block';
				dnom.style.display   = 'block';				
				dban.style.display   = 'block';
				dsuc.style.display   = 'block';						
				forma.tercero.value  = nit;
				forma.nombre.value   = nombre;
				forma.banco.value    = banco;
				forma.sucursal.value = sucursal;
	  }


           function validarTransferencia (){
		  if (forma.tercero.value==''){
			  alert('Su usuario no tiene asignado ningun proveedor de anticipo, no puede realizar esta operacion');
			  return false;			  
		  }

                  if (forma.B_trans.value==''){
			  alert('Su usuario no tiene asignado ningun Banco para la transaccion, no puede realizar esta operacion');
			  return false;			  
		  }

		  var valLiquidacion = parseFloat(sinformato(forma.vlrLiquidacion) );
		  var valReanticipo  = parseFloat(sinformato(forma.vlrReanticipo ) );  
	  	  if (valReanticipo > valLiquidacion ){
			  alert('El valor del reanticipo no puede ser mayor al valor de la Liquidacion');
			  return false;
		  }
	  	  if (valReanticipo < (valLiquidacion*0.1) ){
			  alert('El valor del reanticipo no puede ser menor al 10% del valor de la Liquidacion');
			  return false;
		  }
		  
                  
                  if (forma.t_nit.value == ''){
                       alert('Asigne primero un beneficiario del Pronto Pago.');
                       return false;
                  }


		  createRequest();
		  var params = 
		  '&nit='           + forma.nit.value        +
		  '&vlrReanticipo=' + sinformato(forma.vlrReanticipo) +
		  '&tercero='       + forma.tercero.value    +		  
		  '&banco='         + forma.banco.value      +
		  '&sucursal='      + forma.sucursal.value   +
                  '&t_nit='         + forma.t_nit.value      +
                  '&t_nombre='      + forma.t_nombre.value   +
                  '&t_tipo='        + forma.t_tipo.value     +
                  '&t_banco='       + forma.t_banco.value    +
                  '&t_sucursal='    + forma.t_sucursal.value +
                  '&t_cuenta='      + forma.t_cuenta.value   +
		  '&peticion='      + ((new Date()).getTime())+
                  '&B_trans='       + (forma.B_trans.value +'-'+forma.C_trans.value+'-'+ forma.Tc_trans.value);
		  url = forma.action + params;		  
		  request.open("GET", url, true);
		  request.onreadystatechange = updatePageSaveReanticipo;
		  request.send(null);		  
	  }
	  
	  function updatePageSaveReanticipo(){
		if (request.readyState == 4){
		   if (request.status == 200){			
			 response  = request.responseText;
			 estado = getParameter(0);			 
			 if (estado == 'error'){
				alert (getParameter(1));
			 }	
			 else{			  
				alert (getParameter(1));
                                window.open(BASEURL + '/jsp/cxpagar/liquidacion/impLiquidacionOP.jsp','','menubar=yes, resizable=yes, scrollbars=yes');
			 }				 									
		   }
		   else
			 alert("Error: status code is " + request.status);
		}		  
	  }
	  
	function soloDigitos(e,decReq) {
		var key = (isIE) ? window.event.keyCode : e.which;
		var obj = (isIE) ? event.srcElement : e.target;
		var isNum = (key > 47 && key < 58) ? true:false;
		var dotOK =  (decReq=='decOK' && key ==46 && obj.value.indexOf('.')==-1) ? true:false;
		window.event.keyCode = (!isNum && !dotOK && isIE) ? 0:key;
		e.which = (!isNum && !dotOK && isNS) ? 0:key;   
		return (isNum || dotOK );
	}	  
	

    function sinformato(element){
         return element.value.replace(new RegExp(",","g"), "");
    }

    function formatear(numero, decimales){
       var tmp   = parseInt(numero) ;
       var signo = (tmp<0?-1:1);
       tmp = tmp * signo;
       var num = '';
       var pos = 0;
       while(tmp>0){
        if (pos%3==0 && pos!=0) num = ',' + num;
        res  = tmp % 10;
        tmp  = parseInt(tmp / 10);
        num  = res + num  ;
        pos++;
       }
       if (num=='') num = '0';
       var dec = 1;
       for (var i=0;i<decimales;i++, dec*=10);
       dnum = parseInt ((parseFloat(numero) - parseInt(numero)) * dec) * signo;
         tmpdec = '';
       if(num!=''){
         for (i=0;i<decimales - (dnum==0?0:dnum.toString().length ) ;i++, tmpdec +='0');
         num += (decimales>0? '.' +  tmpdec + (dnum!=0?dnum:''): '');
       }
       if (signo==-1) num = '-'+num;
       return num;
    }


    function actualizarSaldo (){
       var liq        = parseFloat(forma.vlrLiquidacion.value.replace(new RegExp(",","g"), ""));
       var reanticipo = parseFloat(forma.vlrReanticipo.value.replace(new RegExp(",","g"), ""));
       var saldo      = formatear((liq-reanticipo)+'',2);
       forma.saldo.value = saldo;
    }

    function retornarParametrosPago(b){
        if (b==null){
            parent.opener.forma.t_nit.value       = '';
            parent.opener.forma.t_nombre.value    = '';
            parent.opener.forma.t_nit_name.value  = '';
            parent.opener.forma.t_tipo.value      = '';
            parent.opener.forma.t_tipo_name.value = '';
            parent.opener.forma.t_banco.value     = '';
            parent.opener.forma.t_sucursal.value  = '';
            parent.opener.forma.t_cuenta.value    = '';
        } else {
            parent.opener.forma.t_nit.value       = b.nit;
            parent.opener.forma.t_nombre.value    = b.nombre;
            parent.opener.forma.t_nit_name.value  = b.nit_name;
            parent.opener.forma.t_tipo.value      = b.tipo;
            parent.opener.forma.t_tipo_name.value = b.tipo_name;
            parent.opener.forma.t_banco.value     = b.banco;
            parent.opener.forma.t_sucursal.value  = b.sucursal;
            parent.opener.forma.t_cuenta.value    = b.cuenta;
        }
        parent.close();
    }

    function retornarParametrosPago2(b,c,d,e){
        if (b==null){
            parent.opener.forma.banco_T2.value       = '';
            parent.opener.forma.banco_T.value       = '';
            parent.opener.forma.B_trans.value       = '';
	    parent.opener.forma.C_trans.value       = '';
            parent.opener.forma.Tc_trans.value      = '';


        } else {
            parent.opener.forma.banco_T2.value      = c+' - '+d+' - '+e;
            parent.opener.forma.banco_T.value       = c;
            parent.opener.forma.B_trans.value       = b;
	    parent.opener.forma.C_trans.value       = d;
            parent.opener.forma.Tc_trans.value      = e;
        }
        parent.close();
    }
    
      function validarTransferencia (BASEURL){
		  if (forma.tercero.value==''){
			  alert('Su usuario no tiene asignado ningun proveedor de anticipo, no puede realizar esta operacion');
			  return false;			  
		  }

                  if (forma.B_trans.value==''){
			  alert('Su usuario no tiene asignado ningun Banco para la transaccion, no puede realizar esta operacion');
			  return false;			  
		  }

		  var valLiquidacion = parseFloat(sinformato(forma.vlrLiquidacion) );
		  var valReanticipo  = parseFloat(sinformato(forma.vlrReanticipo ) );  
	  	  if (valReanticipo > valLiquidacion ){
			  alert('El valor del reanticipo no puede ser mayor al valor de la Liquidacion');
			  return false;
		  }
	  	  if (valReanticipo < (valLiquidacion*0.1) ){
			  alert('El valor del reanticipo no puede ser menor al 10% del valor de la Liquidacion');
			  return false;
		  }
		  
                  
                  if (forma.t_nit.value == ''){
                       alert('Asigne primero un beneficiario del Pronto Pago.');
                       return false;
                  }

		  document.imgActualizar.src= BASEURL+'/images/botones/aceptarDisable.gif';
                  document.imgActualizar.onmouseover = new Function('');
                  document.imgActualizar.onmouseout  = new Function('');
                  document.imgActualizar.onclick     = new Function('');
		  createRequest();
		  var params = 
		  '&nit='           + forma.nit.value        +
		  '&vlrReanticipo=' + sinformato(forma.vlrReanticipo) +
		  '&tercero='       + forma.tercero.value    +		  
		  '&banco='         + forma.banco.value      +
		  '&sucursal='      + forma.sucursal.value   +
                  '&t_nit='         + forma.t_nit.value      +
                  '&t_nombre='      + forma.t_nombre.value   +
                  '&t_tipo='        + forma.t_tipo.value     +
                  '&t_banco='       + forma.t_banco.value    +
                  '&t_sucursal='    + forma.t_sucursal.value +
                  '&t_cuenta='      + forma.t_cuenta.value   +
		  '&peticion='      + ((new Date()).getTime())+
                  '&voajp='         + forma.voajp.value +
                  '&B_trans='       + (forma.B_trans.value +'-'+forma.C_trans.value+'-'+ forma.Tc_trans.value);
		  url = forma.action + params;		  
		  request.open("GET", url, true);
		  request.onreadystatechange = updatePageSaveReanticipo;
		  request.send(null);
	  }
	  function transferir (CONTROLLER, valor, cpro, npro,voajp){
            if (validarPlanillasPP()){
		var params =
			"valor="      + valor +
			"&cpro="      + cpro  +
			"&npro="      + npro  +
                        "&voajp="     + voajp ;
		 newWindow( CONTROLLER + '?estado=Transferencias&accion=ProntoPago&Opcion=ConsultarProveedorPorUsuario&' + params,'Transferencia');
            }
          }

