/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {  
   cargarConfiguracionLibranza();    
    cargarComboGenerico('ocupacion', 17);
    cargarComboGenerico('pagaduria', 36);
    cargarComboGenerico('convenio', 37);
    
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });   
    
    $('.solo-numeric').live('keypress', function(event) {
        return numbersonly(this, event);
    });
    
    $('.solo-numeric').live('blur',function (event) {  
           this.value = numberConComas(this.value);            
    });   
    
    $("#dia_ent_novedades").datepicker({
        dateFormat: 'dd',
        changeMonth: false,
        changeYear: false,
        minDate: new Date(new Date().getFullYear(), new Date().getMonth(), 01),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    
    $("#dia_pago").datepicker({
        dateFormat: 'dd',
        changeMonth: false,
        changeYear: false,        
        minDate: new Date(new Date().getFullYear(), new Date().getMonth(), 01),  
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    
    $('#tasa_mensual').blur(function () {       
       $('#tasa_anual').val((this.value!=='') ? calcularTasaAnual():0);
    });     

    var myDate = new Date();
    $("#dia_ent_novedades").datepicker("setDate", myDate);
    $("#dia_pago").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    
});

function cargarConfiguracionLibranza(){

    var grid_listar_config_libranza = $("#tabla_config_libranza");
    if ($("#gview_tabla_config_libranza").length) {
         refrescarGridConfigLibranza();
     }else {
         grid_listar_config_libranza.jqGrid({        
            caption:'CONFIGURACI�N DE LIBRANZA',
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",	 
            datatype: "json",           
            height: '380',
            width: '1350',          
            colNames: ['Id', 'Convenio Pagaduria', 'Id Convenio', 'Convenio', 'Id Pagaduria', 'Pagadur�a', 'Id Ocupacion', 'Ocupacion', 'Tasa Mes', 'Tasa Anual', 'Tasa Renov', 'Monto M�nimo', 'Monto M�ximo', 'Plazo M�nimo', 'Plazo M�ximo', 'Colch�n', '% Descuento','D�a Entrega Nov.', 'D�a Pago', 'Periodo Gracia', 'Anexo', 'Estado','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, resizable:false, sortable: true, align: 'center', key: true, hidden:true},
                {name: 'nombre_convenio_pagaduria', index: 'nombre_convenio_pagaduria', width: 150, resizable:false, sortable: true, align: 'left'},
                {name: 'id_convenio', index: 'id_convenio', resizable:false, sortable: true, width: 80, align: 'center', hidden:true},
                {name: 'convenio', index: 'convenio', resizable:false, sortable: true, width: 150, align: 'left'},
                {name: 'id_pagaduria', index: 'id_pagaduria', resizable:false, sortable: true, width: 150, align: 'center', hidden:true},
                {name: 'pagaduria', index: 'pagaduria', resizable:false, sortable: true, width: 150, align: 'left'},
                {name: 'id_ocupacion_laboral', index: 'id_ocupacion_laboral', resizable:false, sortable: true, width: 150, align: 'center', hidden:true},
                {name: 'ocupacion', index: 'ocupacion', resizable:false, sortable: true, width: 150, align: 'left'},
                {name: 'tasa_mensual', index: 'tasa_mensual', width: 80, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 3}},
                {name: 'tasa_anual', index: 'tasa_anual', width: 80, align: 'right', formatter: 'number', hidden:true, formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 3}},
                {name: 'tasa_renovacion', index: 'tasa_renovacion', width: 80, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 3}},
                {name: 'monto_minimo', index: 'monto_minimo', sortable: true, width: 120, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'monto_maximo', index: 'monto_maximo', sortable: true, width: 120, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'plazo_minimo', index: 'plazo_minimo', width: 80, align: 'center', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'plazo_maximo', index: 'plazo_maximo', width: 80, align: 'center', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'colchon', index: 'colchon', width: 70, align: 'center', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
//              {name: 'factor_seguro', index: 'factor_seguro', width: 80, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 5}},
                {name: 'porcentaje_descuento', index: 'porcentaje_descuento', width: 80, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                {name: 'dia_entrega_novedades', index: 'dia_entrega_novedades', width: 80, resizable:false, sortable: true, align: 'center'},  
                {name: 'dia_pago', index: 'dia_pago', width: 80, resizable:false, sortable: true, align: 'center'},  
                {name: 'periodo_gracia', index: 'periodo_gracia', width: 70, align: 'center', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'requiere_anexo', index: 'requiere_anexo', resizable:false, sortable: true, width: 70, align: 'center'},  
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden:true}, 
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'} 
              
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            subGrid: true,   
            subGridOptions: { "plusicon" : "ui-icon-triangle-1-e",
                      "minusicon" :"ui-icon-triangle-1-s",
                      "openicon" : "ui-icon-arrowreturn-1-e",
                      "reloadOnExpand" : true,
                      "selectOnExpand" : true },
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            pager:'#page_tabla_config_libranza',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {                
                dataType: "json",
                type: "POST",  
                async:false,
                data: {
                    opcion: 34
                }
            },    
            gridComplete: function() {
                var cant = jQuery("#tabla_config_libranza").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_config_libranza").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoConfLibranza('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_config_libranza").jqGrid('setRowData', cant[i], {cambio: be});

                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_config_libranza"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;

                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                    editarConfigLibranza(id);
                }

            },
            subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id = subgrid_id+"_t"; 
                jQuery("#"+subgrid_id).html("<table id='"+subgrid_table_id+"'></table>");
                jQuery("#"+subgrid_table_id).jqGrid({
                    url: './controller?estado=Maestro&accion=Libranza',                  
                    datatype: 'json',
                    colNames: ['Id', 'IdConfigLibranza','Nombre','Documento', 'Telefono', 'Correo', 'Estado','Activar/Inactivar'],
                    colModel: [
                        {name: 'id', index: 'id', sortable: true, width: 60, align: 'center', hidden:true, key:true},
                        {name: 'id_config_libranza', index: 'id_config_libranza', sortable: true, align: 'left',  width: 60, hidden:true},   
                        {name: 'nombre', index: 'nombre', sortable: true, align: 'left',  width: 303},                          
                        {name: 'documento', index: 'documento', width: 150, align: 'center'},  
                        {name: 'telefono', index: 'telefono', width: 150, align: 'center'},  
                        {name: 'correo', index: 'correo', width: 165, align: 'left'},  
                        {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden:true}, 
                        {name: 'cambio', index: 'cambio', width: 90, align: 'center'}         
                    ],
                    rowNum:150,
                    height:'80%',
                    width:'90%',
                    cellEdit:true,  
                    cellsubmit: "clientArray",
                    jsonReader: {
                        root: 'rows',
                        cell:'',
                        repeatitems: false,
                        id: '0'
                    },     
                    ajaxGridOptions: {
                        dataType: "json",
                        type: "POST",
                        data: {
                            opcion: 35,
                            id_config_libranza:row_id
                        }
                    },     
                    gridComplete: function () {
                        var cant = jQuery("#"+subgrid_table_id).jqGrid('getDataIDs');
                        for (var i = 0; i < cant.length; i++) {
                            var cambioEstado = $("#"+subgrid_table_id).getRowData(cant[i]).cambio;
                            var cl = cant[i];
                            be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoFirmaRegistrada('" + subgrid_table_id + "','" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                            jQuery("#"+subgrid_table_id).jqGrid('setRowData', cant[i], {cambio: be});

                        }
                    },
                    ondblClickRow: function (rowid, iRow, iCol, e) {
                        var myGrid = jQuery("#"+subgrid_table_id), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                        filas = myGrid.getRowData(selRowIds);  
                        var reg_status = filas.reg_status;                       
                        if (reg_status === 'A') {
                            mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                        } else {
                            editarFirmaRegistrada(subgrid_table_id,rowid);
                        }

                    },
                    loadError: function(xhr, status, error) {
                        alert(error);
                    }
                });               
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_config_libranza",{search:false,refresh:false,edit:false,add:false,del:false});      
        jQuery("#tabla_config_libranza").jqGrid("navButtonAdd", "#page_tabla_config_libranza", {
            caption: "Nuevo", 
            title: "Agregar nueva configuraci�n",           
            onClickButton: function() {
                crearConfigLibranza();           
            }
        });   
        jQuery("#tabla_config_libranza").jqGrid("navButtonAdd", "#page_tabla_config_libranza", {
            caption: "Firma Registrada", 
            title: "Agregar firma registrada a la configuraci�n",           
            onClickButton: function() {
               var myGrid = jQuery("#tabla_config_libranza"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
               filas = myGrid.jqGrid("getLocalRow", selRowIds);              
                if (selRowIds != null) {                   
                    var id_config = filas.id;                   
                    crearFirmaRegistrada(id_config);
                }else{
                    mensajesDelSistema("No hay ninguna configuraci�n seleccionada", '250', '150');
                }        
            }
        });   

     }
}


function refrescarGridConfigLibranza(){    
    jQuery("#tabla_config_libranza").setGridParam({
        url: "./controller?estado=Maestro&accion=Libranza",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async:false,
            data: {
                opcion: 34
            }
        }
    }).trigger("reloadGrid");
}


function cargarComboGenerico(idCombo, option) {
    $('#'+idCombo).html('');
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async:false,
        data: {
            opcion: option
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#'+idCombo).append("<option value=''>Seleccione</option>");
               
                    for (var key in json) {              
                       $('#'+idCombo).append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#'+idCombo).append("<option value=''>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function crearConfigLibranza(){   
    $('#div_config_libranza').fadeIn('slow');   
    resetearValoresConfigLibranza();
    AbrirDivCrearConfigLibranza();
}

function AbrirDivCrearConfigLibranza(){
      $("#div_config_libranza").dialog({
        width: 'auto',
        height: 320,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR CONFIGURACION LIBRANZA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {         
                guardarConfigLibranza(38,'insert');             
            },
            "Salir": function () {                  
                resetearValoresConfigLibranza();         
                $(this).dialog("destroy");
            }
        }
    });    
}

function editarConfigLibranza(cl){
    
    $('#div_config_libranza').fadeIn("slow");
    var fila = jQuery("#tabla_config_libranza").getRowData(cl);  
    var nom_convenio_pag = fila['nombre_convenio_pagaduria'];
    var convenio = fila['id_convenio'];
    var pagaduria = fila['id_pagaduria'];
    var ocupacion = fila['id_ocupacion_laboral'];
    var tasa_mensual = fila['tasa_mensual'];
    var tasa_anual = fila['tasa_anual'];
    var tasa_renov = fila['tasa_renovacion'];
    var monto_minimo = fila['monto_minimo'];
    var monto_maximo = fila['monto_maximo'];    
    var plazo_minimo = fila['plazo_minimo'];
    var plazo_maximo = fila['plazo_maximo'];
    var colchon = fila['colchon'];
//  var factor_seguro = fila['factor_seguro'];
    var porc_descuento = fila['porcentaje_descuento'];
    var dia_ent_novedades = fila['dia_entrega_novedades'];
    var dia_pago = fila['dia_pago'];
    var periodo_gracia = fila['periodo_gracia'];
    var requiere_anexo = fila['requiere_anexo'];  
    var reg_status = fila['reg_status'];    
  
    $('#id_config').val(cl);
    $('#nom_convenio_pag').val(nom_convenio_pag);
    $('#convenio').val(convenio);
    $('#pagaduria').val(pagaduria);
    $('#ocupacion').val(ocupacion);
    $('#tasa_mensual').val(numberConComas(tasa_mensual));
    $('#tasa_anual').val(numberConComas(tasa_anual));
    $('#tasa_renov').val(numberConComas(tasa_renov)); 
    $('#monto_minimo').val(numberConComas(monto_minimo));    
    $('#monto_maximo').val(numberConComas(monto_maximo));
    $('#plazo_minimo').val(numberConComas(plazo_minimo));
    $('#plazo_maximo').val(numberConComas(plazo_maximo));
    $('#colchon').val(numberConComas(colchon));      
    $('#porc_descuento').val(numberConComas(porc_descuento));
    $('#dia_ent_novedades').val(dia_ent_novedades);
    $('#dia_pago').val(dia_pago);
    $('#periodo_gracia').val(numberConComas(periodo_gracia));
    $('input[name=anexo]').val([requiere_anexo]);    
    AbrirDivEditarConfigLibranza();
}


function AbrirDivEditarConfigLibranza(){
      $("#div_config_libranza").dialog({
        width: 'auto',
        height: 320,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR CONFIGURACION LIBRANZA',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              guardarConfigLibranza(39,'update');
            },
            "Salir": function () {                
                resetearValoresConfigLibranza();              
                $(this).dialog("destroy");
            }
        }
    });       
}

function guardarConfigLibranza(option, action){  
    var id = $('#id_config').val();
    var nom_convenio_pag = $('#nom_convenio_pag').val();
    var convenio = $('#convenio').val();
    var pagaduria = $('#pagaduria').val();  
    var ocupacion = $('#ocupacion').val();
    var tasa_mensual = numberSinComas($('#tasa_mensual').val());
    var tasa_anual = numberSinComas($('#tasa_anual').val());
    var tasa_renov = numberSinComas($('#tasa_renov').val());
    var monto_minimo = numberSinComas($('#monto_minimo').val());
    var monto_maximo = numberSinComas($('#monto_maximo').val());
    var plazo_minimo = numberSinComas($('#plazo_minimo').val());
    var plazo_maximo = numberSinComas($('#plazo_maximo').val());    
    var colchon = numberSinComas($('#colchon').val());
//  var factor_seguro = numberSinComas($('#factor_seguro').val());
    var porc_descuento = numberSinComas($('#porc_descuento').val());
   // var fecha_ent_novedades = new Date($('#dia_ent_novedades').val());  
    var dia_ent_novedades = $('#dia_ent_novedades').val();
  //  var payDate = new Date($('#dia_pago').val());
    var dia_pago = $('#dia_pago').val();
    var periodo_gracia = numberSinComas($('#periodo_gracia').val());
    var requiere_anexo = $("input[name=anexo]:checked").val(); 
  
    var url = './controller?estado=Maestro&accion=Libranza';
   if(validaCamposConfigLibranza(nom_convenio_pag, convenio, pagaduria, ocupacion, tasa_mensual, tasa_renov, monto_minimo, monto_maximo, plazo_minimo, plazo_maximo, colchon, porc_descuento, dia_ent_novedades, dia_pago, periodo_gracia)){
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        opcion: option, 
                        id: id,
                        nom_conv_pagaduria: nom_convenio_pag,
                        id_convenio: convenio,
                        id_pagaduria: pagaduria,
                        id_ocupacion_laboral: ocupacion,
                        tasa_mensual: tasa_mensual,
                        tasa_anual: tasa_anual,
                        tasa_renovacion: tasa_renov,
                        monto_minimo: monto_minimo,
                        monto_maximo: monto_maximo,
                        plazo_minimo: plazo_minimo,
                        plazo_maximo: plazo_maximo,
                        colchon: colchon,                       
                        factor_seguro: 0,
                        porc_descuento: porc_descuento,
                        dia_ent_novedades: dia_ent_novedades,
                        dia_pago: dia_pago,
                        periodo_gracia: periodo_gracia,
                        requiere_anexo: requiere_anexo
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '270', '165');                          
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                resetearValoresConfigLibranza();
                                refrescarGridConfigLibranza();  
                                (action === 'update') ? $("#div_config_libranza").dialog('close'): mensajesDelSistema("Cambios guardados satisfactoriamente", '250', '150', true);

                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo guardar la configuraci�n!!", '250', '150');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                              "Message: " + xhr.statusText + "\n" +
                              "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                }); 
            },500);
    }

}

function CambiarEstadoConfLibranza(rowid){
    var grid_tabla = jQuery("#tabla_config_libranza");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 40,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarGridConfigLibranza();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado de la configuracion!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function validaCamposConfigLibranza(nom_convenio_pag, convenio, pagaduria, ocupacion, tasa, tasa_renov, monto_minimo, monto_maximo, plazo_minimo, plazo_maximo, colchon, porc_descuento, dia_ent_novedades, dia_pago, periodo_gracia){
    var sw = false;
    if (nom_convenio_pag === '' || convenio === '' || pagaduria === '' || ocupacion === '' || tasa === ''  || tasa_renov === '' || monto_minimo === '' || monto_maximo === '' || plazo_minimo === '' || plazo_maximo === '' || colchon === ''  || porc_descuento === '' || dia_ent_novedades === '' || dia_pago === '' || periodo_gracia === '') {
        mensajesDelSistema("Debe diligenciar todos los campos", '250', '150');
        return;
    }else if(parseFloat(monto_maximo) < parseInt(monto_minimo)){
        mensajesDelSistema('El monto m�ximo no puede ser inferior al monto m�nimo', '250', '150');
        return;
    }else if(parseFloat(plazo_maximo) < parseInt(plazo_minimo)){
        mensajesDelSistema('El plazo m�ximo no puede ser inferior al plazo m�nimo', '250', '150');
        return;
    }else if(parseInt(periodo_gracia)>3){
        mensajesDelSistema('El periodo de gracia debe ser un n�mero entero entre 0 y 3', '250', '150');
        return;
    }else{
         sw = true;
    }
    return sw;
}

function crearFirmaRegistrada(id_config){   
    $('#div_firmas_registradas').fadeIn('slow');
    $('#id_conf_libranza').val(id_config);   
    resetearValoresFirmaRegistrada();  
    AbrirDivCrearFirmaRegistrada();
}

function AbrirDivCrearFirmaRegistrada(){
      $("#div_firmas_registradas").dialog({
        width: 'auto',
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR FIRMA REGISTRADA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
                guardarFirmaRegistrada('insert');             
            },
            "Salir": function () {                   
                resetearValoresFirmaRegistrada();               
                $(this).dialog("destroy");
            }
        }
    });
    
}

function editarFirmaRegistrada(tableid, cl){
    
    $('#div_firmas_registradas').fadeIn("slow");
    var fila = jQuery("#"+tableid).getRowData(cl);  
    var id_conf_libranza = fila['id_config_libranza'];
    var nombre = fila['nombre'];
    var documento = fila['documento'];  
    var telefono = fila['telefono'];
    var email = fila['correo']; 
    var reg_status = fila['reg_status'];  
    $('#id').val(cl);
    $('#id_conf_libranza').val(id_conf_libranza);
    $('#nombre').val(nombre);
    $('#documento').val(documento);    
    $('#telefono').val(telefono);
    $('#correo').val(email);   
    AbrirDivEditarFirmaRegisrada();
}

function AbrirDivEditarFirmaRegisrada(){
      $("#div_firmas_registradas").dialog({
        width: 'auto',
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR FIRMA REGISTRADA',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              guardarFirmaRegistrada('update');
            },
            "Salir": function () {               
                resetearValoresFirmaRegistrada();               
                $(this).dialog("destroy");
            }
        }
    });
    
}


function guardarFirmaRegistrada(action){  
    var id = $('#id').val();
    var id_conf_libranza = $('#id_conf_libranza').val();
    var documento = $('#documento').val();  
    var nombre = $('#nombre').val();
    var telefono = $('#telefono').val();
    var email = $('#correo').val();
    var url = './controller?estado=Maestro&accion=Libranza';
    if(documento!=='' && nombre!=='' && telefono!=='' && email!==''){
        if(!validarTelefono(telefono))
        {
            mensajesDelSistema("Por favor, ingrese un numero de telefono valido", '250', '150'); 
        }else if(!validarEmail(email)){
            mensajesDelSistema("El email ingresado es incorrecto. Por favor, Verifique", '250', '150');       
        }else{
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        opcion: (action==='insert') ? 41: 42,     
                        id:id,
                        id_config_libranza:id_conf_libranza,
                        documento: documento,                     
                        nombre: nombre,                        
                        telefono: telefono,
                        email: email
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '270', '165');                          
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                resetearValoresFirmaRegistrada();
                                refrescarGridConfigLibranza();  
                                (action === 'update') ? $("#div_firmas_registradas").dialog('close'): mensajesDelSistema("Firma asignada satisfactoriamente a la configuraci�n", '250', '150', true);
                               
                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo crear la firma!!", '250', '150');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                              "Message: " + xhr.statusText + "\n" +
                              "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                }); 
            },500);
        }
    }else{
       mensajesDelSistema("FALTAN CAMPOS POR LLENAR", '250', '150');
    }
}

function CambiarEstadoFirmaRegistrada(tableid,rowid){
    var grid_tabla = jQuery("#"+tableid);
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 43,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarGridConfigLibranza();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado de la firma!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function resetearValoresConfigLibranza(){  
    $('#nom_convenio_pag').val('');
    $('#convenio').val('');
    $('#pagaduria').val('');
    $('#ocupacion').val('');
    $('#tasa_mensual').val('');
    $('#tasa_anual').val('');
    $('#tasa_renov').val('');
    $('#monto_minimo').val('');
    $('#monto_maximo').val('');
    $('#plazo_minimo').val('');
    $('#plazo_maximo').val('');
    $('#colchon').val('');
    $('#factor_seguro').val('');
    $('#porc_descuento').val('');
    $('#dia_ent_novedades').val('');  
    $('#dia_pago').val('');  
    $('#periodo_gracia').val('');
    $('input[name=anexo]').val(['N']);
}

function resetearValoresFirmaRegistrada(){  
    $('#nombre').val('');
    $('#documento').val('');   
    $('#telefono').val('');
    $('#correo').val('');  
}

function CalcularDv() {
    var vpri, x, y, z, i, nit1, dv1;
    nit1 = document.getElementById("documento").value;
    if (isNaN(nit1)) {
        //document.form1.dv.value="X";
        alert('El valor digitado no es un numero valido');
    } else {
        vpri = new Array(16);
        x = 0;
        y = 0;
        z = nit1.length;
        vpri[1] = 3;
        vpri[2] = 7;
        vpri[3] = 13;
        vpri[4] = 17;
        vpri[5] = 19;
        vpri[6] = 23;
        vpri[7] = 29;
        vpri[8] = 37;
        vpri[9] = 41;
        vpri[10] = 43;
        vpri[11] = 47;
        vpri[12] = 53;
        vpri[13] = 59;
        vpri[14] = 67;
        vpri[15] = 71;
        for (i = 0; i < z; i++) {
            y = (nit1.substr(i, 1));
            //document.write(y+"x"+ vpri[z-i] +":");
            x += (y * vpri[z - i]);
            //document.write(x+"<br>");     
        }
        y = x % 11;
        //document.write(y+"<br>");
        if (y > 1) {
            dv1 = 11 - y;
        } else {
            dv1 = y;
        }
        //document.form1.dv.value=dv1;
        document.getElementById("digito_verificacion").value = dv1;
    }
}


function validarTelefono(phone) {
    var filter = /^[0-9-()+]+$/;
    return filter.test(phone);
}

function validarEmail(email) {
  // var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  var emailReg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  return emailReg.test( email );
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function numbersonly(myfield, e, dec)
{
    var key;
    var keychar;
    
    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

// control keys
    if ((key == null) || (key == 0) || (key == 8) ||
            (key == 9) || (key == 13) || (key == 27))
        return true;

// numbers
    else if ((("0123456789.").indexOf(keychar) > -1))
        return true;

// decimal point jump
    else if (dec && (keychar == "."))
    {
        myfield.form.elements[dec].focus();
        return false;
    }
    else
        return false;
}

function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}


function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajeConfirmAction(msj, width, height, okAction, id) {  
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function calcularTasaAnual(){
        var tasa = 0;
        $.ajax({
            type: "POST",
            async:false,
            datatype: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            data: {
                opcion: 48,
                tasa_mensual: $('#tasa_mensual').val()
            },
            success: function (data, textStatus, jqXHR) {
               tasa = data.tasa;
            }, error: function (result) {
                alert('ERROR ');
            }
        });
        return tasa;
}