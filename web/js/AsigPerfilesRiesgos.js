

function ListarAnalistas(id) {

    var usuario = $("#"+id).val();
    $.ajax({
        type: 'POST',
      //url: "../controller?estado=Perfiles&accion=Riesgos",
        url: "controller?estado=AsigPerfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos_Asignacion&pagina=ListarAnalistas.jsp",
      
        dataType: 'json',
        data: {
            opcion: 1,
            usuario:usuario            
                },
        success: function(Resp) {
         ///////////////////////////////////////////////////////////////////////////////////////////
         for(var i in Resp) {
        for(var j in Resp[i]) {
         
        }
        }

        for(var i in Resp) {
        for(var j in Resp[i]) {
            

            if(j=='eliminar'){
             Resp[i][j]="<a style=\"color:black;\" href='#' id='" +Resp[i]["idusuario"]+"' name='" +Resp[i]["nombre"]+"' onclick=\"ElimReg('"+Resp[i]["idusuario"]+"','"+Resp[i]["id_perfil"]+"')\">"+ Resp[i][j]+"</a>";
            }   
            
            if(j=='modificar'){
            // Resp[i][j]="<a href='#'  onclick='ModifReg("+Resp[i]["id"]+")'>"+ Resp[i][j]+"</a>";
                Resp[i][j]="<a style=\"color:black;\" href='/fintra/controller?estado=Perfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos_Asignacion&pagina=ModificarAsignacion.jsp&nit="+Resp[i]["idusuario"]+"')>"+ Resp[i][j]+"</a>";
            }
        //console.log(j, '-->' ,Resp[i][j]);
        
        //Agregar estilo al campo estado
                 if(j=='estado'){
             var accion = "";
             if(Resp[i]["estado"]=='A'){
                 accion='checked';
             }else{
                 accion='';
             }
             Resp[i][j]="<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='modificar' "+accion+" > <label onclick=\"CambiarEstado( '"+Resp[i]["idusuario"]+"','"+Resp[i]["id_perfil"]+"','"+Resp[i]["estado"]+"')\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
            
           }  
        }
        }
        
            $("#TabPerfRies").jqGrid({
                
                data: Resp,       
                datatype: "local",
                height: 350,
		width: 1300,
                viewrecords: true, // show the current page, data rang and total records on the toolbar
                caption: "Lista de usuarios con perfiles asignados",
               // rowNum: 15,
                pager: "#CargarTablaPerfiles",
                
                colModel: [
                    { label: 'Usuario',      name: 'idusuario',       width: 70 },
                    { label: 'Nombre',   name: 'nombre',    width: 180, key:true },
                    { label: 'Perfil del sistema',   name: 'perfilAnalista',    width: 75, key:true },
                    { label: 'Rol',   name: 'rol',    width: 75, key:true },
                    { label: 'Perfil',   name: 'perfil',    width: 90, key:true },
                     {label: 'Monto Minimo Decisi�n', name: 'monto_minimo_decision', index: 'monto_minimo_decision', sortable: true, width: 80, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {label: 'Monto Maximo Decisi�n', name: 'monto_decision', index: 'monto_decision', sortable: true, width: 80, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    { label: 'Estado',   name: 'estado', width: 75 },
                    { label: 'Editar',   name: 'modificar', width: 50 },
                    { label: 'Eliminar', name: 'eliminar',  width: 50 }

                ],
             gridComplete: function (id, rowid) {
                var cant = jQuery("#TabPerfRies").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var estado = $("#TabPerfRies").getRowData(cant[i]).estado;
                    var id = $("#TabPerfRies").getRowData(cant[i]).id;
                    var accion = "";
                    if(estado==''){accion='checked'}else{accion=''}
                    var id = $("#TabPerfRies").getRowData(cant[i]).id;
                    var cl = cant[i];
                 }
            }

            });
        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al cargar la lista de los analistas.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 

        }
    }); // fin $.ajax

}


function CambiarEstado(idUsuario,idperfil,estado){

               $.ajax({
        type: 'POST',
        url: "/fintra/controller?estado=AsigPerfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos_Asignacion&pagina=ListarAnalistas.jsp",
      
        dataType: 'json',
        data: {
            opcion: 10,
            nit:idUsuario ,
            id_perfil:idperfil ,
            estado_asig:estado            
                },
        success: function(Resp) {

        },
        error: function(xhr, ajaxOptions, thrownError) {
              mensajesDelSistema("Error al eliminar el perfil.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax         

}

function ListarAnalistasFabrica(nit,form) {

  //  var usuario = $("#"+id).val();
    $.ajax({
        type: 'POST',
      //url: "../controller?estado=Perfiles&accion=Riesgos",
               url: "/fintra/controller?estado=AsigPerfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos_Asignacion&pagina=ListarAnalistas.jsp",
      
 
        dataType: 'json',
        data: {
            form:form,
            opcion: 2           
                },
        success: function(Resp) {
           var idperfil='';
        for(var i in Resp) {
            
           //Seleccionar por defecto el usuario elegido.
            if(nit == Resp[i]["idusuario"]){
                
              
             var c_monto_decision =  Resp[i]["monto_decision"].toString();
             var c_monto_minimo_decision =  Resp[i]["monto_minimo_decision"].toString();
             var v_monto_decision =  puntosDeMilreturn(c_monto_decision,c_monto_decision.charAt(c_monto_decision.length-1));
             var v_monto_minimo_decision =  puntosDeMilreturn(c_monto_minimo_decision,c_monto_minimo_decision.charAt(c_monto_minimo_decision.length-1));
             
               if(c_monto_decision.length>3){
                    v_monto_decision =  puntosDeMilreturn(c_monto_decision,c_monto_decision.charAt(c_monto_decision.length-1));
               }else{
                   v_monto_decision=c_monto_decision;
               } 
               
                if(c_monto_minimo_decision.length>3){
                    v_monto_minimo_decision =  puntosDeMilreturn(c_monto_minimo_decision,c_monto_minimo_decision.charAt(c_monto_minimo_decision.length-1));
               }else{
                   v_monto_minimo_decision=c_monto_minimo_decision;
               } 
               
                $("#monto_decision").val(v_monto_decision)
                $("#monto_minimo_decision").val(v_monto_minimo_decision)
             $("#usuarios").append('<option selected value="' + Resp[i]["idusuario"] + '">' + Resp[i]["nombre"] + '</option>');
                idperfil= Resp[i]["id_perfil"];
                
                if(Resp[i]["estado"]=='A'){
                 $("#estado").append('<option selected  value="A">ACTIVO</option>');
                 $("#estado").append('<option value="I">INACTIVO</option>');
                }else if(Resp[i]["estado"]=='I'){
                $("#estado").append('<option value="A">ACTIVO</option>');
                $("#estado").append('<option selected value="I">INACTIVO</option>');
                }
           
             }else{
            $("#usuarios").append('<option value="' + Resp[i]["idusuario"] + '">' + Resp[i]["nombre"] + '</option>');
             }
                 
          }
          ListaRoles(nit);
         // ListarPerfilesRiesgos(idperfil);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al cargar la lista de los analistas.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax
     
     

}



function ListaRoles(nit,idrol) {

   // var usuario = $("#"+id).val();
    $.ajax({
        type: 'POST',
               url: "/fintra/controller?estado=AsigPerfiles&accion=Riesgos",
   
        dataType: 'json',
        data: {
            opcion: 8          
                },
        success: function(Resp) {
            
        for(var i=0;i<Resp.length;i++) {
        // for(var j in Resp[i]) {
         if(idrol== Resp[i]["id"]){
          $("#rol").append('<option selected value="' + Resp[i]["id"] + '">' + Resp[i]["descripcion"] + '</option>');
             
         }else{
             $("#rol").append('<option value="' + Resp[i]["id"] + '">' + Resp[i]["descripcion"] + '</option>');
             }
        //  }
         } 
         retornarDatosTabPerfiles(nit);
        },
        error: function(xhr, ajaxOptions, thrownError) {
          mensajesDelSistema("Error al cargar la lista de roles.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax



}


function ElimReg(id,id_perfil){
 var nombreAnalista = $("#"+id).attr("name");

 var msj="Desea Confirmar la eliminacion!";
   $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: 300,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Cancelar": function () {
                $(this).dialog("destroy");
                mensajesDelSistema("Desasignacion cancelada", '300', '150',  true); 
            }, "Aceptar": function () {
                
                      $.ajax({
        type: 'POST',
      //url: "../controller?estado=Perfiles&accion=Riesgos",
        url: "/fintra/controller?estado=AsigPerfiles&accion=Riesgos",
      
        dataType: 'json',
        data: {
            opcion: 4,
            nit:id,
            id_perfil:id_perfil 
                },
        success: function(Resp) {

             if(Resp=="ok"){
                 alertaExitosa("Se ha eliminado correctamente la asignaci�n(es) al analista "+nombreAnalista, '350', '150'); 
            }else{
                  mensajesDelSistema(Resp, '300', '150',  true); 
            }
     
        },
        error: function(xhr, ajaxOptions, thrownError) {
             mensajesDelSistema("Error al eliminar la asignacion.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax   
                
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}


function AsignarPerfil(){
    
    // var nombrePerfil = $("#").attr("name");
    var nit = $("#usuarios").val();
    var perfil = $("#perfiles").val();
    var estado_asig = $("#estado").val();
    var idRol = $("#rol").val();
    var monto_decision = $("#monto_decision").val().replace(/[.]/g, '');
    var monto_minimo_decision = $("#monto_minimo_decision").val().replace(/[.]/g, '');
   // var creation_user = "RPARRA";
  //  var dstrct = "Fintra";
    
    
       
    //guardar en una cadeana de texto los ids de la unidades marcadas
     var perfiles_selecionados = '';
        $("input:checkbox[id^='jqg_']").each(function(index,e){
            var $this = $(this);
            if($this.is(":checked")){
                var idCheck = $this.attr("id");
                var arrayIdCheck = idCheck.split('_');//Se divide el id del Checkbox
                var subIdCheck = arrayIdCheck[2];//Se guarda la parte que contiene el id del perfil
                 perfiles_selecionados += subIdCheck +',';
            } 
        });
    

       if(monto_decision==""){
        mensajesDelSistema("Debe de ingresar el monto maximo de decisi�n", '350', '150',  true);
    } else  if(monto_minimo_decision==""){
        mensajesDelSistema("Debe de ingresar el monto minimo de decisi�n", '350', '150',  true);
    } else if(parseFloat(monto_minimo_decision)>=parseFloat(monto_decision)){
        mensajesDelSistema("El monto m�ximo no puede ser menor ni igual al monto m�nimo", '380', '150',  true);
    } else if(perfiles_selecionados==""){
        mensajesDelSistema("Debe de seleccionar al menos un perfil", '350', '150',  true); 
    }else{
  var msj="Desea confirmar la asignacion del perfil!";
   $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: 300,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Cancelar": function () {
                $(this).dialog("destroy");
                mensajesDelSistema("Asignacion cancelada", '300', '150',  true); 
            }, "Aceptar": function () {       
                      $.ajax({
        type: 'POST',
       url: "/fintra/controller?estado=AsigPerfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos_Asignacion&pagina=ListarAnalistas.jsp",
      
        dataType: 'json',
        data: {
            nit: nit,
            perfil: perfil,
            estado_asig: estado_asig,
            idRol:idRol,
            perfiles_selecionados:perfiles_selecionados,
            monto_decision:monto_decision,
            monto_minimo_decision:monto_minimo_decision,
            opcion: 5
                },
        success: function(Resp) {

             if(Resp=="ok"){
                 alertaExitosa("Se ha asignado correctamente al analista", '300', '150'); 
            }else{
                  mensajesDelSistema(Resp, '350', '150',  true); 
            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al asignar el analista.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax    
         $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
       
  }

}


function ModificarAsignacion(nit){
    
    // var nombrePerfil = $("#").attr("name");
    var nit = $("#usuarios").val();
    var perfil = $("#perfiles").val();
    var rol = $("#rol").val();
    var estado_asig = $("#estado").val();
    var user_update = "RPARRA";
    var dstrct = "Fintra";
    
    var monto_decision = $("#monto_decision").val().replace(/[.]/g, '');
    var monto_minimo_decision = $("#monto_minimo_decision").val().replace(/[.]/g, '');
    
    
    var perfiles_selecionados = '';
    $("input:checkbox[id^='jqg_']").each(function(index,e){
    var $this = $(this);
    if($this.is(":checked")){
          var idCheck = $this.attr("id");
          var arrayIdCheck = idCheck.split('_');//Se divide el id del Checkbox
          var subIdCheck = arrayIdCheck[2];//Se guarda la parte que contiene el id del perfil
          perfiles_selecionados += subIdCheck +',';
         } 
 });
    
    if(monto_decision==""){
        mensajesDelSistema("Debe de ingresar el monto maximo de decisi�n", '350', '150',  true);
    } else if(monto_minimo_decision==""){
        mensajesDelSistema("Debe de ingresar el monto minimo de decisi�n", '350', '150',  true);
    } else if(parseFloat(monto_minimo_decision)>=parseFloat(monto_decision)){
        mensajesDelSistema("El monto m�ximo no puede ser menor ni igual al monto m�nimo", '380', '150',  true);
    } else if(perfiles_selecionados==""){
        mensajesDelSistema("Debe de seleccionar al menos un perfil", '350', '150',  true);
    }else{


 var msj="Desea confirmar la modificacion del perfil!";
   $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: 300,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Cancelar": function () {
                $(this).dialog("destroy");
                mensajesDelSistema("Modificacion cancelada", '300', '150',  true); 
            }, "Aceptar": function () {  
                
     $.ajax({
        type: 'POST',
      //url: "../controller?estado=Perfiles&accion=Riesgos",
        url: "/fintra/controller?estado=AsigPerfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos_Asignacion&pagina=ListarAnalistas.jsp",
      
        dataType: 'json',
        data: {
             nit: nit,
            perfiles_selecionados: perfiles_selecionados,
            monto_decision:monto_decision,
            monto_minimo_decision:monto_minimo_decision,
            idRol: rol,

            opcion: 7
                },
        success: function(Resp) {

             if(Resp=="ok"){
                 alertaExitosa("Se ha modificado correctamente la asignacion", '300', '150'); 
            }else{
                  mensajesDelSistema(Resp, '350', '150',  true); 
            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al modificar la la asignacion.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax      
               $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();

    
    }
  
}



function retornarDatosTabPerfiles(nit){
 //var nombrePerfil = $("#"+id).attr("name");
    
      $.ajax({
        type: 'POST',
      //url: "../controller?estado=Perfiles&accion=Riesgos",
        url: "/fintra/controller?estado=AsigPerfiles&accion=Riesgos",
      
        dataType: 'json',
        data: {
            opcion: 3
            //id:5            
                },
        success: function(Resp) {
            var gridSelector = $("#Tabperfiles");
            var hjh='';
                 jQuery("#Tabperfiles").jqGrid({
	datatype: "local",
	height: 100,
	width: 450,
   	colNames:['Perfiles'],
   	colModel:[
   		//{name:'id',index:'id', width:60, sorttype:"int"},
   		//{name:'nombre',index:'nombre', width:90},
   		{name:'perfil',index:'perfil', width:100}	
   	],
   	multiselect: true,
 
beforeSelectRow: function(rowid, e)
{
    $(gridSelector).jqGrid('resetSelection');
    return (true);
},
beforeRequest : function() {
    $('input[id=cb_Tabperfiles]', 'div[id=jqgh_Tabperfiles_cb]').remove();
},
loadComplete : function () {
    $('input[id^="jqg_Tabperfiles_"]').attr("type", "radio");
}
        
    
   	//caption: "Manipulating Array Data"
        });
        for(var i=0;i<Resp.length;i++)
	jQuery("#Tabperfiles").jqGrid('addRowData',Resp[i]['id_perfil'],Resp[i]);
       

         retornarPerfilesDelAnalista(nit);
         // retornarUnidadesDelPerfil(idperf);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al retornar los datos de los perfiles.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax         
}



function retornarPerfilesDelAnalista(nit){
         $.ajax({
        type: 'POST',
      //url: "../controller?estado=Perfiles&accion=Riesgos",
        url: "/fintra/controller?estado=AsigPerfiles&accion=Riesgos",
        
        dataType: 'json',
        data: {
            nit:nit,
            opcion: 9
            //id:5            
                },
        success: function(Resp) {
             for(var i in Resp) {
        $("input:checkbox[id^='jqg_']").each(function(index,e){
            var $this = $(this);
            var idCheck = $this.attr("id");
            var arrayIdCheck = idCheck.split('_');//Se divide el id del Checkbox
            var subIdCheck = arrayIdCheck[2];//Se guarda la parte que contiene el id del perfil
            if(Resp[i]['id_perfil']==subIdCheck){
            $("#jqg_Tabperfiles_"+subIdCheck).attr('checked',true);
            }
        });
        
        }
         if(nit!="" && nit!= null){
             $('input[id^="jqg_Tabperfiles_"]').attr('onclick','desmarcarchks()');
  
             $('td[title!=""]').attr('onclick','desmarcarchks()');
        retornarDatosFormModiAnal(nit);
         }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al retornar los perfiles del analista.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax 
    
}

function desmarcarchks(){
    $('input[id^="jqg_Tabperfiles_"]').attr('checked',false);
}

function retornarDatosFormModiAnal(nit){
         $.ajax({
        type: 'POST',
      //url: "../controller?estado=Perfiles&accion=Riesgos",
        url: "/fintra/controller?estado=AsigPerfiles&accion=Riesgos",
        
        dataType: 'json',
        data: {
            nit:nit,
            opcion: 6
            //id:5            
                },
        success: function(Resp) {
       $("#rol option[value="+Resp[0]["idrol"]+"] ").attr("selected",true); 

        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al retornar  la informacion del formulario.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax 
    
}


function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}


function alertaExitosa(msj, width, height,ruta) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {
                $(this).dialog("destroy");
               window.location="/fintra/controller?estado=AsigPerfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos_Asignacion&pagina=ListarAnalistas.jsp";
               //location.reload();
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}



$(document).ready(function(){
    $('.solo_numero').keyup(function () {
        //Se valida que solo se permitan escribir numeros y puntos
       this.value = (this.value + '').replace(/[^0-9.]/g, '');
    });
    
    $('.puntos_de_mil').keyup(function () {
        var donde=this;
        var  caracter = this.value.charAt(this.value.length-1);
       	pat = /[\*,\+,\(,\),\?,\,$,\[,\],\^]/
	valor = donde.value
	largo = valor.length
	crtr = true
	if(isNaN(caracter) || pat.test(caracter) == true){
		if (pat.test(caracter)==true){ 
			caracter = "\"" + caracter

		}
		carcter = new RegExp(caracter,"g")
		valor = valor.replace(carcter,"")
		donde.value = valor
		crtr = false
	}
	else{
		var nums = new Array()
		cont = 0
		for(m=0;m<largo;m++){
			if(valor.charAt(m) == "." || valor.charAt(m) == " ")
				{
                                    //continue;
                                }
			else{
				nums[cont] = valor.charAt(m)
				cont++
			}
		}
	}
	var cad1="",cad2="",tres=0
	if(largo > 3 && crtr == true){
		for (k=nums.length-1;k>=0;k--){
			cad1 = nums[k]
			cad2 = cad1 + cad2
			tres++
			if((tres%3) == 0){
				if(k!=0){
					cad2 = "." + cad2
				}
			}
		}
		donde.value = cad2
	}
    });
    

});


function puntosDeMilreturn(donde,caracter){
       	pat = /[\*,\+,\(,\),\?,\,$,\[,\],\^]/
	valor = donde
	largo = valor.length
	crtr = true
	if(isNaN(caracter) || pat.test(caracter) == true){
		if (pat.test(caracter)==true){ 
			caracter = "\"" + caracter

		}
		carcter = new RegExp(caracter,"g")
		valor = valor.replace(carcter,"")
		donde = valor
		crtr = false
	}
	else{
		var nums = new Array()
		cont = 0
		for(m=0;m<largo;m++){
			if(valor.charAt(m) == "." || valor.charAt(m) == " ")
				{
                                    //continue;
                                }
			else{
				nums[cont] = valor.charAt(m)
				cont++
			}
		}
	}
	var cad1="",cad2="",tres=0
	if(largo > 3 && crtr == true){
		for (k=nums.length-1;k>=0;k--){
			cad1 = nums[k]
			cad2 = cad1 + cad2
			tres++
			if((tres%3) == 0){
				if(k!=0){
					cad2 = "." + cad2
				}
			}
		}
		return cad2;    
	}
    
}	
