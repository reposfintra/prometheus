/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    
    //$("#btn_endosar").attr('disabled','disabled');
    
    $('.solo-numero').keyup(function (){
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });    
    
    $('.boton').click(function () {        
        if($("#mas").html()==='Mas...'){
            $("#mas").html('Menos...');
        }else{
             $("#mas").html('Mas...');
        }
        $("#foto").val('');
        $("#busqueda_avanzada").slideToggle("fast");
        $("#page_facturas").find("table.navtable").hide();
    });

    $('#buscar').click(function () {
        buscarFacturasEndosar();
        $("#page_facturas").find("table.navtable").show();
    });
    
     $('#radio2').click(function () {
         
         $("#buscarAvanzada").hide();
         $("#busqueda_avanzada").hide();
    });
    
    $('#radio1').click(function () {
         
         $("#buscarAvanzada").show();
         $("#busqueda_avanzada").hide();
    });
    
    $('#ver_facturas').click(function () {
         exportarFacturasIndemnizadas();
    });

    $("#foto").blur(function () {
        var date = new Date();
        var mes = date.getMonth() + 1;
        var anio = date.getFullYear();
        if($("#foto").val()>(anio+""+mes)){
           mensajesDelSistema("El periodo no puede ser mayor al mes actual", '250', '150', true);
        }

    });
    
    //$("#busqueda_avanzada").css({display: 'none'});
    $("#fecha_inicio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
//    var myDate = new Date();
//    $("#fecha_inicio").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    
    $('#aceptar_endoso').click(function(){
         CargarAchivoEndoso();
    });

});

var valor_a_desistir = 0;

function FiltroInverso() {
    $("#unds_negocio").attr('disabled',false);
    $("#cartera_en").attr('disabled',false);    
    document.getElementById("cedula_cliente").value = "";
    document.getElementById("Negocio").value = "";
}

function onChangeLineasNegocio() {
    FiltroInverso();
    UnidadesNegocio();
}

function ValidarOnChange() {
    
    if ( document.getElementById("endosar_a").className != "combo_150px_disabled" ) {
        EndosarA();
    }
}

function CargarAchivoEndoso() {
    //alert(grid_facturas);
    
    let archivo = document.getElementById("archivo");
    /*let periodo = document.getElementById("periodo");
    let periodos = document.getElementById("periodos-checkbox");*/

    if (archivo.files.length === 0) {
        mensajesDelSistema("Falta informaci�n por digitar")
    } else {

    loading("Esta operaci�n puede tardar varios minutos, por favor espere.", 450);

    let xhr;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject();
    }

    let fd = new FormData();
    fd.append("archivo", archivo.files[0]);

    xhr.onreadystatechange = function () {
        if (xhr.status === 200) {
            if (xhr.readyState === 4) {
                $("#dialogLoading").dialog("close");
                mensajesDelSistema("Negocios Cargados, Por Favor Revisar...", "300", "150", true);
                /*let json = JSON.parse(xhr.responseText);
                if (json.error) {
                    mensajesDelSistema(json.error, '300', '150', true);
                } else {
                    mensajesDelSistema(json.respuesta, '300', '150', true);
                }*/
            }
        }
    }
    xhr.open("POST", "/fintra/controller?estado=Contabilidad&accion=General&opcion=20", true);
    xhr.send(fd);
    }

}

function GoEndosoFacturas() {
    
    if ($("#endosar_a").val() !== '' ) {
        endosarFacturas();
    }else{
        alert("Por favor selecciona a quien se le va a Endosar estas facturas");
    }
}    

function buscarFacturasEndosar() {
    
    //alert("Check es: "+$('input:checkbox[name=estado]:checked').val());

    if ( ($("#cedula_cliente").val() !== '' || $("#Negocio").val() !== '') && $("#cartera_en").val() == '' ) {
        
        mensajesDelSistema("Por favro seleccione En d�nde se encuentra la cartera.", "300", "150");
        
    }else if (  ($("#unidad_negocio").val() == '' && $("#unds_negocio").val() == null && $("#cartera_en").val() !== '' ) && (($("#cedula_cliente").val() == '' && $("#Negocio").val() == '') ) ) {
        
        mensajesDelSistema("Por favor debe digitar la c�dula o el negocio a buscar", "300", "150");
        
    }else if ( (($("#cedula_cliente").val() !== '' || $("#Negocio").val() !== '') && $("#cartera_en").val() !== '')   ||  ($("#unidad_negocio").val() !== '' && $("#unds_negocio").val() !== '' && $("#cartera_en").val() !== '')  ) {
        
        var caption = "Facturas a Endosar";
        var grid_facturas = $("#tbl_facturas");
        
        if ($("#gview_tbl_facturas").length) {
            
            reloadGridFacturasEndosar(grid_facturas);
            
        } else {

            grid_facturas.jqGrid({
                caption: caption,
                url: "./controller?estado=Contabilidad&accion=General",
                mtype: "POST",
                datatype: "json",
                height: '500',
                width: '1500',
                colNames: ['Negocio', 'Cartera En', 'IdUnidad Negocio', 'Unidad Negocio', 'Identificacion', 'Nombre', 'codigo cliente', 'Tipo Negocio', '# Factura', 'Cuota', 'Fecha vencimiento'
                            , 'Dias mora', 'Vlr factura', 'Vlr Abono','Vlr Saldo'],
                colModel: [
                    {name: 'negocio', index: 'negocio', width: 70, align: 'center'}, //, key: true
                    {name: 'cartera_en', index: 'cartera_en', width: 80, align: 'center'},
                    {name: 'id_uneg_negocio', index: 'id_uneg_negocio', width: 160, align: 'center',hidden:true},
                    {name: 'uneg_negocio', index: 'uneg_negocio', width: 160, align: 'center'},
                    {name: 'nit_cliente', index: 'nit_cliente', width: 80, sortable: true, align: 'left'}, //
                    {name: 'nombre_cliente', index: 'nombre_cliente', sortable: true, width: 220, align: 'left'},
                    {name: 'codcli', index: 'codcli', width: 80, sortable: true, align: 'center'},
                    {name: 'tipo_negocio', index: 'tipo_negocio', width: 100, align: 'center'},
                    {name: 'documento', index: 'documento', width: 70, align: 'left'},
                    {name: 'cuota', index: 'cuota', width: 50, align: 'center'},
                    {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 100, align: 'center'},
                    {name: 'dias_vencidos', index: 'dias_vencidos', width: 70, align: 'center'},
                    
                    {name: 'valor_factura', index: 'valor_factura', width: 100, align: 'right',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                    {name: 'valor_abono', index: 'valor_abono', width: 100, align: 'right',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                    {name: 'valor_saldo', index: 'valor_saldo', width: 100, align: 'right', editable: true,
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
                    
                ],
                rowNum: 6000,
                rowTotal: 1000000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                pager: '#page_facturas',
                viewrecords: true,
                hidegrid: false,
                shrinkToFit: false,
                footerrow: false,
                multiselect: true,
                cellsubmit: "clientArray",
                editurl: 'clientArray'
                ,ondblClickRow: function (rowid, iRow, iCol, e) {
                    if(iCol===12){
                        var rowsdata = $("#tbl_facturas").jqGrid("getLocalRow", rowid);
                        valor_a_desistir = rowsdata.valor_desistir;
                        grid_facturas.editRow(rowid, true); 
                    }
                    
                }, ajaxGridOptions: {
                    async: false,
                    data: {
                        opcion: 9,
                        linea_negocio: $("#unidad_negocio").val(),
                        unds_negocio: $("#unds_negocio").val(),
                        cartera_en: $("#cartera_en").val(),
                        endosar_a: $("#endosar_a").val(),
                        cedula_cliente: $("#cedula_cliente").val(),
                        negocio: $("#Negocio").val(),
                        checkSaldo: $('input:checkbox[name=estado]:checked').val()
                        
                    }
                    
                }, loadComplete: function () {
                    if (grid_facturas.jqGrid('getGridParam', 'records') > 0) {
                        //deshabilitarPanelBusqueda(true);
                        //EndosarA();
                    }else{
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
                },
                loadError: function (xhr, status, error) {
                    alert(error, 250, 150);
                },
                
                gridComplete: function () {
                    $("#endosar_a").removeClass('combo_150px_disabled');
                    $("#btn_endosar").attr('enabled',true);
                    $("#btn_endosar").addClass('ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only');
                    EndosarA();
                }             
                
                
            }).navGrid("#page_facturas", {add: false, edit: false, del: false, search: false, refresh: false}, {});
            
            //boton que sirve apra desbloquear el panel de buquedad
            $("#tbl_facturas").navButtonAdd('#page_facturas', {
                caption: "Limpiar",
                title: "Limpiar",
                onClickButton: function (e) {
                    //deshabilitarPanelBusqueda(false);
                    grid_facturas.jqGrid("clearGridData", true);
                }
            });
            
            $("#tbl_facturas").navButtonAdd('#page_facturas', {
                caption: "Exportar Excel",
                title: "Exportar Excel",
                onClickButton: function (e) {
                    exportarExcelEndoso(grid_facturas);
                }
            });

        }
    } else {
        mensajesDelSistema("El filtro tiene combinaciones que no son v�lidas", "300", "150");
    }

}

function reloadGridFacturasEndosar(grid_facturas) {
    //alert("torototo");
    //alert(grid_facturas);
    grid_facturas.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Contabilidad&accion=General",
        //autowidth: true,
        //width: 1700,
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: 9,
                linea_negocio: $("#unidad_negocio").val(),
                unds_negocio: $("#unds_negocio").val(),
                cartera_en: $("#cartera_en").val(),
                endosar_a: $("#endosar_a").val(),
                cedula_cliente: $("#cedula_cliente").val(),
                negocio: $("#Negocio").val(),
                checkSaldo: $('input:checkbox[name=estado]:checked').val()     
            }
        }
    });
    grid_facturas.trigger("reloadGrid");

}

function endosarFacturas(grid_facturas) {
    //alert(grid_facturas);
    var i, selRowIds = $("#tbl_facturas").jqGrid("getGridParam", "selarrrow"), rowData;
    if (selRowIds.length > 0) {
        
        loading("Espere un momento procesando facturas.", "270", "140");
        var arrayJson = new Array();
        for (i = 0; i < selRowIds.length; i++) {
            rowData = $("#tbl_facturas").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }

        console.log(JSON.stringify(arrayJson));

        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Contabilidad&accion=General",
                type: 'POST',
                dataType: 'json',
                data: {
                    opcion: 12,
                    linea_negocio: $("#unidad_negocio").val(),
                    unds_negocio: $("#unds_negocio").val(),
                    endosar_a: $("#endosar_a").val(),
                    cartera_en: $("#cartera_en").val(),
                    cedula_cliente: $("#cedula_cliente").val(),
                    negocio: $("#Negocio").val(),                    
                    checkSaldo: $('input:checkbox[name=estado]:checked').val(),                    
                    listJson: JSON.stringify(arrayJson)
                },
                success: function (json) {
                    //alert("Why to here?");
                    if (json.respuesta === 'TRUE') {
                        //alert("A");
                        //reloadGridFacturasEndosar(grid_facturas);
                        buscarFacturasEndosar();
                        $("#dialogLoading").dialog('close');
                    } else {
                        //alert("B");
                        //reloadGridFacturasEndosar(grid_facturas);
                        //buscarFacturasEndosar();
                        mensajesDelSistema(json.respuesta, '300', '150', true);
                        $("#dialogLoading").dialog('close');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                },
                
                complete: function (json) {
                        //alert("Deber�a");
                        //reloadGridFacturasEndosar(grid_facturas);
                }

            });

        }, 500);

    }else{
        
        mensajesDelSistema("No hay datos seleccionados para endosar", '300', '150', true);
    }
}

function UnidadesNegocio(){
    
    $.ajax({
        type: 'POST',
        url:"./controller?estado=Contabilidad&accion=General",
        dataType: 'json',
        data: {
            opcion: 10,
            linea:$("#unidad_negocio").val()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                try {
                    $('#unds_negocio').empty();
//                    $('#transportadora').append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#unds_negocio').append('<option value=' + json[key].codigo + '>' + json[key].nombre + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                  mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }
            CarteraEnCustodiaDe();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
}

function CarteraEnCustodiaDe(){
    
    $.ajax({
        type: 'POST',
        url:"./controller?estado=Contabilidad&accion=General",
        dataType: 'json',
        data: {
            opcion: 8,
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                try {
                    $('#cartera_en').empty();
//                    $('#transportadora').append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#cartera_en').append('<option value=' + json[key].codigo + '>' + json[key].nombre + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                  mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
}

function EndosarA(){
    
    $.ajax({
        type: 'POST',
        url:"./controller?estado=Contabilidad&accion=General",
        dataType: 'json',
        data: {
            opcion: 11,
            FiduciaActual:$("#cartera_en").val()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                try {
                    $('#endosar_a').empty();
//                    $('#transportadora').append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#endosar_a').append('<option value=' + json[key].codigo + '>' + json[key].nombre + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                  mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Errorpapu: " + xhr.status + "\n" +
                    "Messagepapu: " + xhr.statusText + "\n" +
                    "Responsepapu: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
}

function exportarExcelEndoso(grid_facturas) {
    alert("WTF!");
    var i, selRowIds = $("#tbl_facturas").jqGrid("getGridParam", "selarrrow"), rowData;
    if (selRowIds.length > 0) {
        
      //  loading("Espere un momento procesando excel.", "270", "140");
        var arrayJson = new Array();
        for (i = 0; i < selRowIds.length; i++) {
            rowData = $("#tbl_facturas").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }

        console.log(JSON.stringify(arrayJson));
        
        var opt = {
            autoOpen: false,
            modal: true,
            width: 220,
            height: 150,
            title: 'Descarga Reporte'
        };
        //  $("#divSalidaEx").dialog("open");
        $("#divSalidaEx").dialog(opt);
        $("#divSalidaEx").dialog("open");
        $("#imgloadEx").show();
        $("#msjEx").show();
        $("#respEx").hide();
        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Contabilidad&accion=General",
                type: 'POST',
                dataType: "html",
                data: {
                    opcion: 13,
                    listado: JSON.stringify(arrayJson),
                    tipo:$('input:radio[name=radios]:checked').val()
                },
                success: function (resp) {
                    $("#imgloadEx").hide();
                    $("#msjEx").hide();
                    $("#respEx").show();
                    var boton = "<div style='text-align:center'>\n\ </div>";                   
                    document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;


                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);

    }else{
        
        mensajesDelSistema("No hay datos seleccionados para exportar", '300', '150', true);
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}
function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function deshabilitarPanelBusqueda(status) {
    
    var nodes = document.getElementById("busqueda").getElementsByTagName('*');
    for (var i = 0; i < nodes.length; i++)
    {
        nodes[i].disabled = status;
    }

}

function BloquearBusqueda() {
    //alert("yega");
    
    var ivv = $('#cartera_en > option').length;
    //$('#cartera_en').remove()
    
    document.getElementById("unidad_negocio").innerHTML = "<select id='unidad_negocio' name='unidad_negocio' onchange='onChangeLineasNegocio()'><option value='' selected>-Seleccione-</option><option value='MIROCREDITO'>MICROCREDITO</option><option value='FENALCO_ATL'>FENALCO ATLANTICO</option><option value='FENALCO_BOL'>FENALCO BOLIVAR</option></select>"
    $("#unds_negocio").attr('disabled','disabled');
    //$("#cartera_en").attr('disabled','disabled');

    document.getElementById("unds_negocio").innerHTML = "<select id='unds_negocio' name='unds_negocio' disabled='disabled'></select>";
    //document.getElementById("cartera_en").innerHTML = "<select id='cartera_en' name='cartera_en' disabled='disabled'></select>";
    
    if ( ivv == 0){ CarteraEnCustodiaDe() }
    
}
