/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    cargarinfotablas();

});


function cargarinfotablas() {
    var grid_tabla = jQuery("#tabla_desbloquear_tablas");
    if ($("#gview_tabla_desbloquear_tablas").length) {
        reloadGridTabla(grid_tabla, 1);
    } else {
        grid_tabla.jqGrid({
            caption: "INFORMACION DE TABLAS",
            url: "./controller?estado=Desbloquear&accion=Tablas",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '990',
            colNames: ['Tabla', 'Campo', 'Last update', 'Fecha envio', 'LLave primaria', 'Fecha anulacion', 'Estado'],
            colModel: [
                {name: 'nombre_tabla', index: 'nombre_tabla', width: 290, sortable: true, align: 'left', hidden: false},
                {name: 'nombre_campo', index: 'nombre_campo', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'es_last_update', index: 'es_last_update', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'es_fecha_envio', index: 'es_fecha_envio', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'es_pk', index: 'es_pk', width: 90, sortable: true, align: 'center', hidden: false},
                {name: 'es_fecha_anulacion', index: 'es_fecha_anulacion', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'condicion', index: 'condicion', width: 120, sortable: true, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 1
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, gridComplete: function (index) {
                var ids = grid_tabla.jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var condicion = grid_tabla.getRowData(cl).condicion;
                    if (condicion !== 'Sin procesar.') {
                        $("#tabla_desbloquear_tablas").jqGrid('setRowData', ids[i], false, {weightfont: 'bold', background: '#F5A9A9'});
                    }

                }
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_desbloquear_tablas").navButtonAdd('#pager1', {
            caption: "Desbloquear tablas",
            onClickButton: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var con = 0;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var condicion = grid_tabla.getRowData(cl).condicion;
                    if (condicion !== 'Sin procesar.') {
                        con = con + 1;
                    }
                }
                if (con > 0) {
                    updateMasivo();
                } else {
                    mensajesDelSistema("No hay tablas bloqueadas", '230', '150', false);
                }
            }
        });
    }
}

function reloadGridTabla(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Desbloquear&accion=Tablas",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function updateMasivo() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Desbloquear&accion=Tablas",
        async: false,
        data: {
            opcion: 2
        },
        success: function (data) {
            if (data.respuesta === 'ERROR') {
                mensajesDelSistema("Error, no puedo realizar la actualizacion", '230', '150', true);
            } else {
                mensajesDelSistema("Exito en la actualizacion", '175', '150', true);
                cargarinfotablas();
            }

        }, error: function (result) {
            alert('No se pudo actualizar los datos ');
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}