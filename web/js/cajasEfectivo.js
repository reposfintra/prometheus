/**
 * Funciones para el programa de manejo de cajas de efectivo
 */

function cargarSucursalesIngreso(){
    var controller = document.getElementById("CONTROLLER").value;
    new Ajax.Updater({success: 'sucursal', failure:'divMensaje'}, controller+'?estado=Cajas&accion=Efectivo', {
        method: 'post',
        parameters: {opcion: 'CARGAR_SUCURSALES', banco: $F('banco')}
    });
}

function cargarSucursalesEngreso(){
    var controller = document.getElementById("CONTROLLER").value;
    new Ajax.Updater({success: 'sucursalDetalle', failure:'divMensaje'}, controller+'?estado=Cajas&accion=Efectivo', {
        method: 'post',
        parameters: {opcion: 'CARGAR_SUCURSALES', banco: $F('bancoDetalle')}
    });
}

function validarIngreso(){
    if($F("valor")!="" && $F("fecha")!="" && $F("descripcion")!="" && $F("banco")!="" && $F("sucursal")!="" ){
        var controller = document.getElementById("CONTROLLER").value;
         document.form1.action=controller+'?estado=Cajas&accion=Efectivo&opcion=GUARDAR_INGRESO'
         document.form1.submit();
    }else{
        alert("Debe ingresar todos los datos")
    }
}

function abrirDialogConfirmar(BASEURL, agencia){
    var options = "center:yes ;status:no; help:no; dialogWidth:500px; dialogHeight:300px;";
    window.showModalDialog(BASEURL+'/jsp/cajasEfectivo/confirmarIngresoCaja.jsp?agencia='+agencia, 'confirmar ingreso', options);
    //window.location.reload();
    window.location='controller?estado=Cajas&accion=Efectivo&agencia='+agencia;
}

function confirmarIngreso(){
    if(document.getElementById("ingreso").value!="" && document.getElementById("planilla").value!=""){
        document.form.submit();
    }else{
        alert("Debe seleccionar un ingreso y un archivo");
    }
}

function mostrarMensajeDialog(mensaje){
    if(mensaje!=null && mensaje!="" && mensaje!="null"){
        alert(mensaje);
        window.close();
    }
}

function mostrarMensajeDialogGuardar(mensaje){
    if(mensaje!=null && mensaje!="" && mensaje!="null"){
        alert(mensaje);
        window.close();
        window.opener.location.reload();
    }
}

function validarRangoFechas(){
    var hoy = new Date();
    var curr_date = hoy.getDate();
    var curr_month = hoy.getMonth() + 1; //months are zero based
    if(curr_month<10){
        curr_month = "0"+curr_month;
    }
    var curr_year = hoy.getFullYear();
    var strHoy = (curr_year+"-"+curr_month+"-"+curr_date);
    
    if(diferenciaFechas(strHoy, $F("fechaInicial"))>=0){
        if(diferenciaFechas($F("fechaFinal"), $F("fechaInicial"))>=0){
            document.form2.submit();
        }else{
            alert("La fecha inicial debe ser menor o igual a la fecha final");
        }
    }else{
        alert("La fecha inicial debe ser menor o igual al dia actual");
    }
}

function abrirDialogPago(BASEURL, agencia){
    var options = "center=yes,status=yes,scrollbars=no,width=650,height=500,resizable=yes";
    window.open(BASEURL+'/jsp/cajasEfectivo/guardarPagoCaja.jsp?agencia='+agencia, 'Guardar_pago', options);
}

function guardarPago(){
    if($F("transportadora")!="" && $F("fecha")!="" && $F("tipoDocumento")!="" && $F("documento")!="" && $F("provee")!="" &&  
        $F("banco")!="" && $F("valor")!="" && $F("comision")!="" && $F("valorComision")!="" && $F("valorEntregar")!="" && $F("tipo")!=""){
        document.form1.submit();
    }else{
        alert("debe llenar todos los campos");
    }
}

function calcularComision(){
    var valor = parseFloat($F("valor").replace(/,/ig, ""));
    var comision = parseFloat($F("comision").replace(/,/ig, ""));
    var comisionBancaria = parseFloat($F("comisionBancaria").replace(/,/ig, ""));
    var valorComision = valor * (comision/100);
    $("valorComision").value = Math.round(valorComision);
    $("valorEntregar").value = Math.round(valor - valorComision - comisionBancaria);
}

function cambiaComisionBancaria(){
  
  var valor= document.getElementById("tipoDocumento");
  var ele=valor.options[valor.selectedIndex].value
  if(ele=="T"){
      $("comisionBancaria").value='2,200';
  }else {
      
       $("comisionBancaria").value='0';
  }
  
}
