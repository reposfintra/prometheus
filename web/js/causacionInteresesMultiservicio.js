/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* global formulario */

formulario = new funcionFomurarios();
$(document).ready(function () {
    funcionFormatos = new formatos();
    cargarcmc();
    $('#buscar').click(function () {
        evento();
    });

});

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function evento() {
    //cambiamos el name para diferenciar los eventos que hara
    var field = document.getElementById("buscar");
    var name = document.getElementById("buscar").name;
    var requerido = formulario.requeridos();
    if (requerido == 0) {
        if (name == 'buscar') {
            cargando_toggle();
            field.setAttribute("name", "desbloquear");
            document.getElementById('buscar').innerHTML = 'desbloquear';
            document.getElementById("periodo").setAttribute("readonly", true);
            $('#cmc').attr('disabled', true);
            document.getElementById("cta_ingreso").setAttribute("readonly", true);
            $("#gbox_tabla_facturas").show();
            cargarInformacionFacturas();

        } else if (name == 'desbloquear') {
            field.setAttribute("name", "buscar");
            document.getElementById('buscar').innerHTML = 'buscar';
            document.getElementById("periodo").removeAttribute("readonly", true);
            $('#cmc').attr('disabled', false);
            document.getElementById("cta_ingreso").removeAttribute("readonly", true);
            $('#tabla_facturas').jqGrid('GridUnload');
            $("#gbox_tabla_facturas").hide();
        }
    } else {
        mensajesDelSistema("Falta llenar campos del filtro", '314', '140', false);
    }
}

function cargarInformacionFacturas() {
    var grid_tabla_ = jQuery("#tabla_facturas");
    if ($("#gview_tabla_facturas").length) {
        reloadGridMostrarInfo(grid_tabla_, 86);
    } else {
        grid_tabla_.jqGrid({
            caption: "Facturas por Causar",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '483',
            width: '1200',
            colNames: ['Factura', 'Cuenta', 'Cliente', 'Nombre Cliente', 'Estado', 'Tipo Cartera', 'Relacionado', 'Fecha Factura', 'Fecha Vencimiento', 'Valor Factura', 'Valor Saldo', 'Intereses', 'Multiservicio'],
            colModel: [
                {name: 'factura', index: 'factura', width: 110, align: 'center'},
                {name: 'codigo_cuenta_contable_db', index: 'codigo_cuenta_contable_db', width: 100, align: 'center', hidden: false},
                {name: 'nit_cliente', index: 'nit_cliente', width: 110, align: 'left', hidden: false},
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 200, align: 'left', hidden: false},
                {name: 'estado', index: 'estado', width: 100, align: 'center', hidden: false},
                {name: 'tipo_cartera', index: 'tipo_cartera', width: 100, align: 'center', hidden: false},
                {name: 'relacionado', index: 'relacionado', width: 100, align: 'center', hidden: true},
                {name: 'fecha_factura', index: 'fecha_factura', width: 100, align: 'center', hidden: false},
                {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 106, align: 'center', hidden: false},
                {name: 'valor_factura', index: 'valor_factura', width: 130, align: 'right', hidden: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_saldo', index: 'valor_saldo', width: 130, align: 'right', hidden: false, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'intereses', index: 'intereses', width: 130, align: 'right', hidden: false, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'multiservicio', index: 'multiservicio', width: 100, align: 'center', hidden: false}
            ],
            rownumbers: false,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: true,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 86,
                    periodo: $('#periodo').val(),
                    cmc: $('#cmc').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var valor_saldo = grid_tabla_.jqGrid('getCol', 'valor_saldo', false, 'sum');
                var interes = grid_tabla_.jqGrid('getCol', 'intereses', false, 'sum');
                grid_tabla_.jqGrid('footerData', 'set', {fecha_vencimiento: 'Total', valor_saldo: valor_saldo, intereses: interes});
                cargando_toggle();
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_facturas").navButtonAdd('#pager', {
            caption: "Causar Intereses",
            onClickButton: function () {
                causarIntereses();
            }
        });
    }
}

function reloadGridMostrarInfo(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                periodo: $('#periodo').val(),
                cmc: $('#cmc').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center", modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function causarIntereses() {
    cargando_toggle();
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: 'POST',
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        data: {
            opcion: 87,
            periodo: $('#periodo').val(),
            cmc: $('#cmc').val(),
            cta_ingreso: $('#cta_ingreso').val()
        },
        success: function (data) {
            cargando_toggle();
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = data + "<br/><br/><br/><br/>" + boton;
            //alert(JSON.stringify(data));
//            if (data[0].respuesta === 'OK') {
//                mensajesDelSistema("Exito al causar intereses", '314', '140', false);
//                cargando_toggle();
//                cargarInformacionFacturas();
//            } else {
//                mensajesDelSistema("Ocurrio un error", '314', '140', false);
//            }

        }, error: function (result) {
            alert('ERROR NO SE CAUSAR LOS INTERESES');
        }
    });
}

var jsonSelect = [];
var llaveSelect = {};
function cargarcmc() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 88
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#cmc').html('');
                $('#cmc').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#cmc').append('<option value=' + json[datos].cmc + '>' + json[datos].descripcion + '</option>');
                    llaveSelect[json[datos].cmc] = json[datos].cuenta;
                }
                jsonSelect.push(llaveSelect);
                console.log(jsonSelect);
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function asignacionCuenta(seleccion) {
    var valor = seleccion.value;
    var obj = JSON.stringify(llaveSelect);
    var json = $.parseJSON(obj);
    $('#cta_ingreso').val(json[valor]);
    //alert(json[valor]);
}