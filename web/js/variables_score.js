"use strict";

window.onload = () => {
    document.getElementById("button-buscar").addEventListener("click", () => {
        let modelo = document.getElementById("modelo").value;
        let unidad = document.getElementById("unidadNegocio").value;
        let id = document.getElementById("identificador").value;
        let op = document.getElementById("opcion").value;
        
        switch (modelo) {
            case "SCORECARD":
                buscarNegocios(unidad, op, id);
                break;
            case "FDURO":
                //$("#tVariables-score").jqGrid("GridUnload");
                buscarFiltroDuro(op, id);
                break;
        }
        
    });
}

const microColNames = ['N�mero solicitud', 'Identificacion', 'Exclusi�n', 'Puntuaci�n Base', 'D�as mora', 'Utilizaci�n cuenta cartera', 'Cr�ditos actuales neg', 'Cr�ditos cerrados', 'Max expmic',
    'Edad', 'Saldo total', 'Cuentas micro', 'Puntaje total','Zona Riesgos', 'Fecha'];
const microColModel = [
    {name: 's_numero_solicitud', index: 's_numero_solicitud', width: 80, sortable: true, align: 'center'},
    {name: 's_identificacion', index: 's_identificacion', width: 80, sortable: true, align: 'center'},
    {name: 's_exclusion', index: 's_exclusion', width: 80, sortable: true, align: 'center'},
    {name: 's_base_score', index: 's_base_score', width: 90, sortable: true, align: 'center'},
    {name: 's_diasmoramicro_cal', index: 's_diasmoramicro_cal', width: 80, sortable: true, align: 'center'},
    {name: 's_utilizacion_cuenta_cartera_a_fecha_cal', index: 's_utilizacion_cuenta_cartera_a_fecha_cal', width: 100, sortable: true, align: 'center'},
    {name: 's_creditosactulesnegativos_cal', index: 's_creditosactulesnegativos_cal', width: 100, sortable: true, align: 'center'},
    {name: 's_creditoscerrados_cal', index: 's_creditoscerrados_cal', width: 100, sortable: true, align: 'center'},
    {name: 's_maxexpmic_cal', index: 's_maxexpmic_cal', width: 80, sortable: true, align: 'center'},
    {name: 's_edad_cal', index: 's_edad_cal', width: 80, sortable: true, align: 'center'},
    {name: 's_saldototal_cal', index: 's_saldototal_cal', width: 80, sortable: true, align: 'center'},
    {name: 's_ncuentasmicro_cal', index: 's_ncuentasmicro_cal', width: 80, sortable: true, align: 'center'},
    {name: 's_score_total', index: 's_score_total', width: 80, sortable: true, align: 'center'},
    {name: 'zona_riesgo', index: 'zona_riesgo', width: 80, sortable: true, align: 'center'},
    {name: 's_creation_date', index: 's_creation_date', width: 80, sortable: true, align: 'center'}
];

const educativoColNames = ['N�mero solicitud', 'Identificacion', 'Puntaje M�ximo buro', 'Cuentas Ahorros abiertas', 'Tiempo primer producto', 'Tiempo �ltimo producto', 'Mora max semestre',
    'Mora max a�o', 'Mora max actual', 'Mora m�xima TDC', 'Cantidad carteras recuperadas', '% obligaciones al d�a', '�ltima peor calificaci�n', 'Antiguedad meses TDC', 'Total saldo mora', 'Mora 60 semestre',
    'Mora 90 semestre', 'Mora > 90 semestre', 'Mora 30 a�o', 'Mora > 30 a�o', 'Mora 30 60 a�o', 'Mora 60 90 a�o', 'Mora > 90 a�o', '% Uso TDC', 'Puntaje total', 'Fecha'];
const educativoColModel = [
    {name: 's_numero_solicitud', index: 's_numero_solicitud', width: 80, sortable: true, align: 'center'},
    {name: 's_identificacion', index: 's_identificacion', width: 80, sortable: true, align: 'center'},
    {name: 'puntaje_maximo_buro', index: 'puntaje_maximo_buro', width: 100, sortable: true, align: 'center'},
    {name: 's_cant_cta_ahorros_abiertas', index: 's_cant_cta_ahorros_abiertas', width: 110, sortable: true, align: 'center'},
    {name: 's_tiemp_primer_product_sect_financ', index: 's_tiemp_primer_product_sect_financ', width: 110, sortable: true, align: 'center'},
    {name: 's_tiemp_ultim_product_sect_financ', index: 's_tiemp_ultim_product_sect_financ', width: 110, sortable: true, align: 'center'},
    {name: 's_mora_max_semestre', index: 's_mora_max_semestre', width: 90, sortable: true, align: 'center'},
    {name: 's_mora_max_anio', index: 's_mora_max_anio', width: 80, sortable: true, align: 'center'},
    {name: 's_mora_max_actual', index: 's_mora_max_actual', width: 80, sortable: true, align: 'center'},
    {name: 's_mora_max_tdc', index: 's_mora_max_tdc', width: 90, sortable: true, align: 'center'},
    {name: 's_cant_carteras_recup', index: 's_cant_carteras_recup', width: 120, sortable: true, align: 'center'},
    {name: 's_porc_oblig_titular_aldia', index: 's_porc_oblig_titular_aldia', width: 100, sortable: true, align: 'center'},
    {name: 's_ultim_peor_calif', index: 's_ultim_peor_calif', width: 110, sortable: true, align: 'center'},
    {name: 's_antig_meses_tdc', index: 's_antig_meses_tdc', width: 110, sortable: true, align: 'center'},
    {name: 's_total_saldo_mora', index: 's_total_saldo_mora', width: 100, sortable: true, align: 'center'},
    {name: 's_cont_mora_sesenta_semestre_telcos', index: 's_cont_mora_sesenta_semestre_telcos', width: 100, sortable: true, align: 'center'},
    {name: 's_cont_mora_noventa_semestre_telcos', index: 's_cont_mora_noventa_semestre_telcos', width: 100, sortable: true, align: 'center'},
    {name: 's_cont_mora_mayor_noventa_semestre_telcos', index: 's_cont_mora_mayor_noventa_semestre_telcos', width: 100, sortable: true, align: 'center'},
    {name: 's_cont_mora_treinta_anio_telcos', index: 's_cont_mora_treinta_anio_telcos', width: 100, sortable: true, align: 'center'},
    {name: 's_cont_mora_mayor_treinta_anio_telcos', index: 's_cont_mora_mayor_treinta_anio_telcos', width: 100, sortable: true, align: 'center'},
    {name: 's_cont_mora_treinta_sesenta_anio', index: 's_cont_mora_treinta_sesenta_anio', width: 100, sortable: true, align: 'center'},
    {name: 's_cont_mora_sesenta_noventa_anio', index: 's_cont_mora_sesenta_noventa_anio', width: 100, sortable: true, align: 'center'},
    {name: 's_cont_mora_mayor_noventa_anio', index: 's_cont_mora_mayor_noventa_anio', width: 100, sortable: true, align: 'center'},
    {name: 's_porc_uso_tarjeta_credit', index: 's_porc_uso_tarjeta_credit', width: 100, sortable: true, align: 'center'},
    {name: 's_score_total', index: 's_score_total', width: 80, sortable: true, align: 'center'},
    {name: 'creation_date', index: 'creation_date', width: 80, sortable: true, align: 'center'}
];

let options = {
    caption: "VARIABLES DE PUNTAJE",
    url: "./controller?estado=Administracion&accion=Fintra",
    mtype: "GET",
    datatype: "json",
    height: '550',
    rowNum: 1000,
    rowTotal: 10000,
    gridview: true,
    hidegrid: false,
    viewrecords: true,
    rownumbers: true,
    loadonce: false,
    pager: '#pager',
    pgbuttons: true,
    sortname: 'invdate',
    jsonReader: {
        repeatitems: false
    },
    loadError: function (xhr, status, error) {
        mensajesDelSistema(error, 250, 150);
    }
};

let gridLoaded = false;

function buscarNegocios(unidad, op, id) {
    $("#tFiltros-duro").jqGrid("GridUnload");
    if (gridLoaded) {
        reloadGrid(unidad, op, id);
    } else {
        options.ajaxGridOptions = {
            data: {
                identificador: id,
                unidadNegocio: unidad,
                opcion: op
            }
        };

        switch (unidad) {
            case "MICRO":
                options.colModel = microColModel;
                options.colNames = microColNames;
                options.shrinkToFit = true;
                break;
            case "EDUCA":
                options.colModel = educativoColModel;
                options.colNames = educativoColNames;
                options.shrinkToFit = false;
                options.width = window.screen.width * 0.9;
                break;
        }
        $("#tVariables-score").jqGrid(options).navGrid("#pager", {search: true}, {});
        gridLoaded = true;
    }
}

function reloadGrid(unidad, op, id) {
//    $("#tVariables-score").clearBeforeUnload();
    $("#tVariables-score").jqGrid("GridUnload");

    switch (unidad) {
        case "MICRO":
            options.colModel = microColModel;
            options.colNames = microColNames;           
            options.shrinkToFit = true;
                break;
            case "EDUCA":
                options.colModel = educativoColModel;
                options.colNames = educativoColNames;
                options.shrinkToFit = false;
                options.width = window.screen.width * 0.9;
            break;
    }

    options.ajaxGridOptions = {
        data: {
            identificador: id,
            unidadNegocio: unidad,
            opcion: op
        }
    };
    $("#tVariables-score").jqGrid(options).navGrid("#pager", {search: true}, {});
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function buscarFiltroDuro(op, id){
    
    $("#tVariables-score").jqGrid("GridUnload");
    var grid_filtros_duro = $("#tFiltros-duro");
    if ($("#gview_tFiltros-duro").length) {
         refrescarFiltroDuro(op, id);
     }else {
         grid_filtros_duro.jqGrid({        
            caption:'Filtros Duros',
            url: "./controller?estado=Administracion&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '500',
            autowidth: true,
            colNames: ['N�mero solicitud', 'Identificacion', 'Filtro1', 'Filtro2', 'Filtro3', 'Filtro4', 'Filtro5', 'Filtro6', 'Filtro7', 'Decision'],
            colModel: [
                
                {name: 's_numero_solicitud', index: 's_numero_solicitud', width: 90, sortable: true, align: 'center'},
                {name: 's_identificacion', index: 's_identificacion', width: 80, sortable: true, align: 'center'},
                {name: 'filtro1', index: 'filtro1', width: 80, sortable: true, align: 'center'},
                {name: 'filtro2', index: 'filtro2', width: 80, sortable: true, align: 'center'},
                {name: 'filtro3', index: 'filtro3', width: 80, sortable: true, align: 'center'},
                {name: 'filtro4', index: 'filtro4', width: 80, sortable: true, align: 'center'},
                {name: 'filtro5', index: 'filtro5', width: 80, sortable: true, align: 'center'},
                {name: 'filtro6', index: 'filtro6', width: 80, sortable: true, align: 'center'},
                {name: 'filtro7', index: 'filtro7', width: 80, sortable: true, align: 'center'},
                {name: 'decision', index: 'decision', width: 80, sortable: true, align: 'center'}
              
            ],
            rowNum: 10000,
            rowTotal: 10000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#page_Fduro',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {                
                dataType: "json",
                type: "POST",  
                async:false,
                data: {
                    opcion_buscar: op,
                    identificador: id,
                    opcion: 12
                }
            },    
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_Fduro",{search:false,refresh:false,edit:false,add:false,del:false});      
        $("#tFiltros-duro").jqGrid("navButtonAdd", "#page_Fduro", {
            caption: "Descripcon Filtros", 
            title: "Descripcon Filtros",           
            onClickButton: function() {
                DescFiltrosDuros();           
            }
        });           

     }
}

function refrescarFiltroDuro(op, id){    
    $("#tVariables-score").jqGrid("GridUnload");
    $("#tFiltros-duro").setGridParam({
        url: "./controller?estado=Administracion&accion=Fintra",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async:false,
            data: {
                opcion_buscar: op,
                identificador: id,
                opcion: 12
            }
        }
    }).trigger("reloadGrid");
}

function DescFiltrosDuros(){   
    $('#div_desc_filtros_duros').fadeIn('slow');   
    AbrirDescFiltrosDuros();
}

function AbrirDescFiltrosDuros(){
      $("#div_desc_filtros_duros").dialog({
        width: 850,
        height: 400,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'DESCRIPCION FILTROS',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {         
                $(this).dialog("destroy");
            }
        }
    });    
}