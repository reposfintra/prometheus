function CalcularInteresesCausacion(rowid)
{

    var fila = jQuery("#Inversionistas").getRowData(rowid);
    var rxml;
    saveparameters = {
        "successfunc" : function(response){
            rxml=response.responseXML;
            refreshGridCausacion(rowid,response.responseXML);
           
        },
        "url" : "",
        "extraparam" : {
            op:20,
            no_transaccion:fila.no_transaccion,
            nit:fila.nit,
            subcuenta:fila.subcuenta
            },
        "aftersavefunc" :null,
        "errorfunc": null,
        "afterrestorefunc" : null,
        "restoreAfterError" : true,
        "mtype" : "POST"
    }
    jQuery("#Inversionistas").jqGrid('saveRow',rowid,saveparameters);

    $("#Inversionistas").jqGrid('setCell',rowid,'fecha_causacion', rxml.getElementsByTagName('fecha')[0].childNodes[0].data);
}




function refreshGridCausacion(rowid,respuesta) {

    var intereses=respuesta.getElementsByTagName('intereses')[0].childNodes[0].data
    var fecha=respuesta.getElementsByTagName('fecha')[0].childNodes[0].data
    $("#Inversionistas").jqGrid('setCell',rowid,'valor_intereses', intereses);
    $("#Inversionistas").jqGrid('setCell',rowid,'fecha', fecha);
}




function formatear(num,prefix){
    prefix = prefix || '';
    num += '';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
    splitRight = splitRight.substring(0,3);//090929
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
        splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
    }
    return prefix + splitLeft + splitRight;
}



function asignaDatosCuenta(rowid)
{    
    var fila = jQuery("#ListaCuentas").getRowData(rowid);
    $("#cuenta").val(fila.cuenta);
    $("#titular_cuenta").val(fila.titular_cuenta);
    $("#nit_cuenta").val(fila.nit_cuenta);
    $("#banco").val(fila.banco);
    $("#sw_datos_transferencia").val(false);
    var tp="";
   
    if(fila.tipo_cuenta=="Corriente"){
        tp="CC"
    }
    if(fila.tipo_cuenta=="Ahorro"){
        tp="CA"
    }
  
    $("#tipo_cuenta").val(tp);
    $('#VentanaCuentas').dialog('close')

    //desabilitar campos
    deshabilitarDatosCuenta(true);


}


function asignaDatosTransferencia(rowid,tipo)
{
    if(tipo=="C")
    {
        asignaDatosCheque(rowid);
    }

    if(tipo=="E")
    {
        asignaDatosCuenta(rowid);
    }
}




function asignaDatosCheque(rowid)
{
    var fila = jQuery("#ListaCuentas").getRowData(rowid);
    $("#nit_beneficiario").val(fila.nit_beneficiario);
    $("#nombre_beneficiario").val(fila.nombre_beneficiario);
    $("#cheque_cruzado").val(fila.cheque_cruzado);
    $("#cheque_primer_beneficiario").val(fila.cheque_primer_beneficiario);
    $("#sw_datos_transferencia").val(false);
    $('#VentanaCuentas').dialog('close')
    //desabilitar campos
    deshabilitarDatosCheque(true);


}


function deshabilitarDatosCuenta(ds)
{
    $("#cuenta").attr('disabled', ds);
    $("#banco").attr('disabled', ds);
    $("#nit_cuenta").attr('disabled', ds);
    $("#titular_cuenta").attr('disabled', ds);
    $("#tipo_cuenta").attr('disabled', ds);

}

function deshabilitarDatosCheque(ds)
{
    $("#nombre_beneficiario").attr('disabled', ds);
    $("#nit_beneficiario").attr('disabled', ds);
    $("#cheque_cruzado").attr('disabled', ds);
    $("#cheque_primer_beneficiario").attr('disabled', ds);


}


function refrescarFormsRetiros(ds,tipo)
{

    if(tipo=="C")
    {
   
        deshabilitarDatosCheque(ds);
        limpiarDatosTransferencia(ds,tipo);
    }

    if(tipo=="E")
    {
        deshabilitarDatosCuenta(ds);
        limpiarDatosTransferencia(ds,tipo);
    }
}


function validaFormConsignacion()
{
    sw=true;
    if($("#banco").val()==0)
    {
        alert("Seleccione el banco");
        sw=false;
        $("#banco").focus()
    }
    else
    {
        if($("#sucursal").val()==0)
        {
            alert("Seleccione la sucuersal del banco");
            sw=false;
            $("#sucursal").focus()

        }
        else
        {
            if($("#fecha").val()==0)
            {
                alert("Seleccione la fecha del ingreso");
                sw=false;
                $("#fecha").focus()
            }
            else{
               /* if($("#valor").val()==0)
                {
                    alert("Digite el valor del ingreso");
                    sw=false;
                    $("#valor").focus()
                }*/
            }
        }
    }
    return sw;
}

function validaFormRetiroElec()
{
    sw=true;
    if($("#titular_cuenta").val()==0)
    {

        alert("Digite el nombre del titular de la cuenta");
        sw=false;
        $("#titular_cuenta").focus()
    }
    else
    {
        if($("#nit_cuenta").val()==0)
        {
            alert("Digite el nit de la cuenta");
            sw=false;
            $("#nit_cuenta").focus()
        }
        else
        {
            if($("#banco").val()==0)
            {
                alert("Seleccione el banco");
                sw=false;
                $("#banco").focus()
            }
            else{
                if($("#cuenta").val()==0)
                {
                    alert("Digite  el numero de cuenta");
                    sw=false;
                    $("#cuenta").focus()
                }
                else{
                    if($("#valor").val()==0)
                    {
                        alert("Digite el valor a retirar");
                        sw=false;
                        $("#valor").focus()
                    }
                }
            }
        }
    }
    
    return sw;
}



function validaFormRetiroCheq()
{
    sw=true;
    if($("#nombre_beneficiario").val()==0)
    {

        alert("Digite el nombre del beneficiario");
        sw=false;
        $("#nombre_beneficiario").focus()
    }
    else
    {
        if($("#nit_beneficiario").val()==0)
        {
            alert("Digite el nit del beneficiario");
            sw=false;
            $("#nit_beneficiario").focus()
        }
        else
        {
            if($("#cheque_cruzado").val()==0)
            {
                alert("Seleccione el si es cheque cruzado");
                sw=false;
                $("#cheque_cruzado").focus()
            }
            else{
                if($("#cheque_cruzado").val!=0 && $("#cheque_primer_beneficiario").val()==0)
                {
                    alert("Seleccione  primer beneficiario");
                    sw=false;
                    $("#cheque_primer_beneficiario").focus()
                }
                else{
                    if($("#valor").val()==0)
                    {
                        alert("Digite el valor a retirar");
                        sw=false;
                        $("#valor").focus()
                    }
                }
            }
        }
    }

    return sw;
}


function validaFormRetiroInter()
{
    sw=true;
    if($("#nombre_beneficiario_capt").val()==0)
    {

        alert("Digite el nombre del beneficiario");
        sw=false;
        $("#nombre_beneficiario_capt").focus()
    }
    else
    {
        if($("#valor").val()==0)
        {
            alert("Digite el valor a retirar");
            sw=false;
            $("#valor").focus()
        }
    }
    return sw;
}




function validaTope()
{
    var   sw=true;

    var tope = parseFloat($('#tope').val())
    var cadena = ReplaceAll($('#valor').val(),",","");
    tope=tope.toFixed(2);
    var valor = parseFloat(cadena)
    // alert("valor : " +valor + " tope :"+tope);
    if( valor>tope)
    {
        alert("El valor maximo a retirar supera el valor permitido");
        sw=false;
    }
    return sw;
}



function limpiarDatosTransferencia(ds,tipo)
{

    if(ds==false)
    {
        if(tipo=="E")
        {
            $("#cuenta").val("");
            $("#banco").val("");
            $("#nit_cuenta").val("");
            $("#titular_cuenta").val("");
            $("#tipo_cuenta").val("");
        }              
        if(tipo=="C")
        {           
            $("#nombre_beneficiario").val("");
            $("#nit_beneficiario").val("");
            $("#cheque_cruzado").val("");
            $("#cheque_primer_veneficiario").val("");

        }

    }

}



function Confirmar()
{
    
    var controller =$("#CONTROLLER").val();
    var BASEURL =$("#BASEURL").val();
    var movimientos_chequeados = jQuery("#Retiros").jqGrid('getGridParam','selarrrow');
    //var retiros = movimientos_chequeados.toString().split(",");
    $("#divSalida").html("<div id='resp'></div>");
    $("#divSalida").dialog("open");
    $("#divSalida").css('height', 'auto');
    $("#divSalida").dialog("option", "position", "center");
    var url = controller+'?estado=Captacion&accion=Inversionista';
   
    $.ajax({
        dataType: 'html',
        url:url,
        data: {
            op : '9',
            retiros: ""+movimientos_chequeados
        },
        method: 'post',
        success:  function (resp){           
            respSalida(resp);

        },
        error: function(resp){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });      
}


function respSalida(resp)

{
    var BASEURL =$("#BASEURL").val();
    var boton = "<div style='text-align:center'><img src='" + BASEURL +"/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\""+"#divSalida" +"\");refreshGridConfirmacionMovimientos()' /></div>"
    $("#divSalida").html(resp+ "<br/><br/><br/><br/>"+boton);
}

function Aprobar()
{
    var controller =$("#CONTROLLER").val();
    var BASEURL =$("#BASEURL").val();
    var movimientos_chequeados = jQuery("#Retiros").jqGrid('getGridParam','selarrrow');
    $("#divSalida").dialog("open");
    $("#divSalida").css('height', 'auto');
    $("#divSalida").dialog("option", "position", "center");
    var url = controller+'?estado=Captacion&accion=Inversionista';
    $.ajax({
        dataType: 'html',
        url:url,
        data: {
            op : '11',
            retiros: ""+movimientos_chequeados
        },
        method: 'post',
        success:  function (resp){
            respSalida(resp);

        },
        error: function(resp){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });

}
function Transferir()
{
    var controller =$("#CONTROLLER").val();
    var BASEURL =$("#BASEURL").val();
    var movimientos_chequeados = jQuery("#Retiros").jqGrid('getGridParam','selarrrow');
    $("#divSalida").dialog("open");
    $("#divSalida").css('height', 'auto');
    $("#divSalida").dialog("option", "position", "center");
    var url = controller+'?estado=Captacion&accion=Inversionista';
    $.ajax({
        dataType: 'html',
        url:url,
        data: {
            op : '14',
            retiros: ""+movimientos_chequeados,
            banco : $("#banco").val()
        },
        method: 'post',
        success:  function (resp){
            respSalida(resp);
        },
        error: function(resp){
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}


function EliminarCuentasTransferencia()
{
    var controller =$("#CONTROLLER").val();
    var BASEURL =$("#BASEURL").val();
    var cuentas_chequeadas = jQuery("#ListaCuentas").jqGrid('getGridParam','selarrrow');
    var url = controller+'?estado=Captacion&accion=Inversionista';
    if (cuentas_chequeadas != "" ) {
        var cuentas  = new Array();
        cuentas = cuentas_chequeadas.toString().split(",");
        $.ajax({
            dataType: 'html',
            url: url,
            data: {
                op: '25',
                cuentas_chequeadas: "" + cuentas_chequeadas
            },
            method: 'post',
            success: function(resp) {
                for (i = 0; i < cuentas.length; i++) {
                    $('#ListaCuentas').jqGrid('delRowData', cuentas[i]);
                    jQuery("#ListaCuentas").trigger("reloadGrid");
                }
                alert("Se eliminaron los registros");
                
            },
            error: function(resp) {
                alert("Ocurrio un error enviando los datos al servidor");
            }

        });
    } else
    {
        alert("Debe seleccionar al menos una cuenta");
    }
    
}

function Causar()
{
    var controller =$("#CONTROLLER").val();
    var BASEURL =$("#BASEURL").val();
    var movimientos_chequeados = jQuery("#Inversionistas").jqGrid('getGridParam','selarrrow');
    $("#VentanaCausacion").dialog("open");
    $("#VentanaCausacion").css('height', 'auto');
    $("#VentanaCausacion").dialog("option", "position", "center");
    var url = controller+'?estado=Captacion&accion=Inversionista';
    $.ajax({
        dataType: 'html',
        url:url,
        data: {
            op : '21',
            movimientos: ""+movimientos_chequeados
        },
        method: 'post',
        success:  function (resp){
            var boton = "<div style='text-align:center'><img src='" + BASEURL +"/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\""+"#VentanaCausacion" +"\");refreshGridCausados();' /></div>"
            document.getElementById('resp').innerHTML = resp+ "<br/><br/><br/><br/>"+boton

        },
        error: function(){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });

}



function Exportar(formato)
{
    var controller =$("#CONTROLLER").val();
    var BASEURL =$("#BASEURL").val();
    $("#divSalida").dialog("open");
    $("#divSalida").css('height', 'auto');
    $("#divSalida").dialog("option", "position", "center");
    var url = controller+'?estado=Captacion&accion=Inversionista';
    $.ajax({
        dataType: 'html',
        url:url,
        data: {
            op : '18',
            subcuenta:$("#subcuenta").val() ,
            nit : $("#nit").val(),
            formato:formato,
            periodo:$('#ano').val()+$('#mes').val()
        },
        method: 'post',
        success:  function (resp){
            var boton = "<div style='text-align:center'><img src='" + BASEURL +"/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\""+"#divSalida" +"\");refreshGridConfirmacionMovimientos()' /></div>"
            document.getElementById('resp').innerHTML = resp+ "<br/><br/><br/><br/>"+boton
        },
        error: function(){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });

}



function ExportarInformeConsolidado(formato)
{
    var controller =$("#CONTROLLER").val();
    var BASEURL =$("#BASEURL").val();
    $("#divSalida").dialog("open");
    $("#divSalida").css('height', 'auto');
    $("#divSalida").dialog("option", "position", "center");
    var url = controller+'?estado=Captacion&accion=Inversionista';
    $.ajax({
        dataType: 'html',
        url:url,
        data: {
            op : '29',
            formato:formato
        },
        method: 'post',
        success:  function (resp){
            var boton = "<div style='text-align:center'><img src='" + BASEURL +"/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\""+"#divSalida" +"\");refreshGridConfirmacionMovimientos()' /></div>"
            document.getElementById('resp').innerHTML = resp+ "<br/><br/><br/><br/>"+boton
        },
        error: function(){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });

}

function ExportarInformePdf()
{
    var controller =$("#CONTROLLER").val();
    var BASEURL =$("#BASEURL").val();
    $("#divSalida").dialog("open");
    $("#divSalida").css('height', 'auto');
    $("#divSalida").dialog("option", "position", "center");
    var url = controller+'?estado=Captacion&accion=Inversionista';
    $.ajax({
        dataType: 'html',
        url:url,
        data: {
            op : '24',
            periodo:$('#ano').val()+$('#mes').val()
        },
        method: 'post',
        success:  function (resp){
            var boton = "<div style='text-align:center'><img src='" + BASEURL +"/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\""+"#divSalida" +"\");refreshGridConfirmacionMovimientos()' /></div>"
            document.getElementById('resp').innerHTML = resp+ "<br/><br/><br/><br/>"+boton

        },
        error: function(){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });

}






function ReplaceAll(Source,stringToFind,stringToReplace){

    var temp = Source;

    var index = temp.indexOf(stringToFind);

    while(index != -1){

        temp = temp.replace(stringToFind,stringToReplace);

        index = temp.indexOf(stringToFind);

    }

    return temp;

}




function ejecutarOperacion(op){
  
    switch (op)
    {
        case 1:
            var sw=true;


            if($('#tipo_movimiento').val()=="I"){
                sw=validaFormConsignacion();
            }
            else
            {


                if($('#tipo_transferencia').val()=="E")
                {
                    sw=  validaFormRetiroElec();
                }else
                {
                    if($('#tipo_transferencia').val()=="C")
                    {
                        sw=  validaFormRetiroCheq();
                    }
                    else
                    {
                        if($('#tipo_transferencia').val()=="I")
                        {
                            sw=  validaFormRetiroInter();
                        }
                    }
                }
                sw= validaTope();
            }
            if(sw){             
                guardarMovimiento();

            }

            break;

        case 2:
            op = 3;
            $('#divCrearMovimiento').dialog('close');
            tipoTrasferencia(op, $('input:radio[name="medio_retiro"]:checked').val());
            break;
        case 9:
            Confirmar();
            break;
        case 11:
            Aprobar();
            break;
    }


}


function registrarRetiro(op,tipo,fecha)
{   
    var fechaHoy = new Date();
    var anio = fechaHoy.getFullYear();
    var mes = fechaHoy.getMonth()+1;
    if(mes <= 9){
        mes ="0"+mes;
    }
    var dia = fechaHoy.getDate();
    if(dia <= 9){
        dia ="0"+dia;
    }
    var fechaCau = anio+'-'+mes+'-'+dia;
    
    if(fecha == "null"){
        var dia = fechaHoy.getDate();
        dia = dia-1;
        if (dia <= 9) {
            dia = "0" + dia;
        }
       fecha = anio+'-'+mes+'-'+dia;
        
    }
    
        if (fechaCau > fecha) {

            var url = $("#BASEURL").val() + "/jsp/inversionistas/registrarMovimiento.jsp?op=" + op + "&tipo_movimiento=" + tipo + "&nit=" + $('#nit').val() + "&subcuenta=" + $('#subcuenta').val();

            //$("#divCrearMovimiento").html("Cargarndo...");
            $('#divCrearMovimiento').dialog('close');
            $("#divCrearMovimiento").dialog("open");
            cargando("divCrearMovimiento", "Cargando");
            $("#divCrearMovimiento").load(url);
            $("#divCrearMovimiento").dialog("option", "position", "center");
        } else
        {
            alert("No puede realizar retiro hoy porque ya se realiz� el cierre del mes");
        }
   
    

}

function verReferenciaRetiro(rowid)
{
    var fila = jQuery("#Retiros").jqGrid('getRowData',rowid);
    var url = $("#BASEURL").val()+"/jsp/inversionistas/referenciaRetiro.jsp?no_transaccion="+fila.no_transaccion+"&opcion="+$('#opcion').val();
    cargando("divSalida","Cargando");
    $("#divSalida").load(url).dialog("open");

}
function verSaldos(rowid)
{
    var fila = jQuery("#Inversionistas").jqGrid('getRowData',rowid);



    var url = $("#BASEURL").val()+"/jsp/inversionistas/saldos.jsp?nit="+fila.nit+"&nombre="+ URLEncode(fila.nombre_subcuenta)+"&total="+ fila.saldo_total;
    $("#VentanaSaldos").html("");
    $("#VentanaSaldos").load(url).dialog("open");
}

function verCuentas()
{
    var url = $("#BASEURL").val()+"/jsp/inversionistas/referenciaCuentas.jsp?nit="+$("#nit").val()+"&tipo_transferencia="+$("#tipo_transferencia").val();
    $("#VentanaCuentas").html("");
    $("#VentanaCuentas").load(url).dialog("open");
}

function validarCheuqeCruzado()
{
    var d=true
    if($("#cheque_cruzado").val()=="S")
    {
        d=false;
    }

    $("#cheque_primer_beneficiario").attr('disabled', d);
}


function validaTipoInversionista()
{

    var d=true
   
    if(document.getElementById("tipo_inversionista").value=="Familiar Socio")
    {
        d=false;
    }
    if(d)
    {
        asignaIndex("nit_parentesco","");
        asignaIndex("tipo_parentesco","");
    }
    document.getElementById("nit_parentesco").disabled=d;
    document.getElementById("tipo_parentesco").disabled=d;
}



function crearMovimiento(op)
{
    $('#divCrearMovimiento').dialog('close');
   
    var url = $("#BASEURL").val()+"/jsp/inversionistas/registrarMovimiento.jsp?op="+op;
    $("#divCrearMovimiento").load(url).dialog("open");
    $("#divCrearMovimiento").dialog("option", "position", "center");
}


function tipoTrasferencia(op,tipo_tras)
{
    var url = $("#BASEURL").val()+"/jsp/inversionistas/registrarMovimiento.jsp?op="+op+"&tipo_transferencia="+tipo_tras+"&tipo_movimiento="+$("#tipo_movimiento").val()+"&subcuenta="+$("#subcuenta").val()+"&nit="+$("#nit").val();
    $("#divCrearMovimiento").load(url).dialog("open");
    $("#divCrearMovimiento").dialog("option", "position", "center");
}


function cerrarDiv(div)
{
    $(div).dialog('close')
}
function refreshGrid() {
    jQuery("#MovimientosCaptaciones").setGridParam({
        datatype: 'json'
    });
    jQuery("#MovimientosCaptaciones").trigger("reloadGrid");
}

function refrescarGridMovimientosMes()
{
    jQuery('#Movimientos').setGridParam({
        datatype: 'json',
        url: $('#CONTROLLER').val()+"?estado=Captacion&accion=Inversionista&op=17&nit="+$('#nit').val()+"&subcuenta="+$('#subcuenta').val()+"&periodo="+$('#ano').val()+$('#mes').val()

    });
    jQuery('#Movimientos').trigger("reloadGrid");
}

function refrescarGridInformeCierre()
{
       
    jQuery('#MovSocios').setGridParam({
        datatype: 'json',
        url: $('#CONTROLLER').val()+"?estado=Captacion&accion=Inversionista&op=22&periodo="+$('#ano').val()+$('#mes').val(),
        loadComplete: function(data) {
            refrescarGridInformeParticular();
           // verGrafico();
        }


    });
    jQuery('#MovSocios').trigger("reloadGrid");

  
}

function refrescarGridInformeParticular()
{
       
     jQuery('#MovParticular').setGridParam({
        datatype: 'json',
        url: $('#CONTROLLER').val()+"?estado=Captacion&accion=Inversionista&op=30&periodo="+$('#ano').val()+$('#mes').val(),
        loadComplete: function(data) {
         refrescarGridTotalInversionista();
            // verGrafico()
        }
        


    });
    jQuery('#MovParticular').trigger("reloadGrid");
  
}

function refrescarGridTotalInversionista(periodo)
{
     jQuery('#TotalInversionista').setGridParam({
        datatype: 'json',
        url: $('#CONTROLLER').val()+"?estado=Captacion&accion=Inversionista&op=32&periodo="+periodo
        
    });
    jQuery('#TotalInversionista').trigger("reloadGrid");
  
}

function refrescarGridDetalleInversionista(periodo,nit)
{
     jQuery('#DetalleInversionista').setGridParam({
        datatype: 'json',
        url: $('#CONTROLLER').val()+"?estado=Captacion&accion=Inversionista&op=31&periodo="+periodo+"&nit="+nit
        
    });
    jQuery('#DetalleInversionista').trigger("reloadGrid");
  
}


function verGrafico() {
    var BASEURL=$('#BASEURL').val()
    var nombre_grafico="informe"+$('#ano').val()+$('#mes').val()+".jpg";
    var imgGraf = "<div style='text-align:center'><img src='" + BASEURL +"/exportar/migracion/" + nombre_grafico +"'   /></div>";
    document.getElementById('imagen').innerHTML = imgGraf+"<br/>";
}

function insertarGrafico(periodo){
 
    $.ajax({
        dataType: 'html',
        url: $('#CONTROLLER').val()+"?estado=Captacion&accion=Inversionista&op=33&periodo="+periodo,
        success: function(resp){
           verGrafico();
            
        },
        error: function(){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });
   
}

function refreshGridConfirmacionMovimientos() {
    jQuery("#Retiros").setGridParam({
        datatype: 'json'
    });
    jQuery("#Retiros").trigger("reloadGrid");
}

function refreshGridCausados() {
    jQuery("#Inversionistas").setGridParam({
        datatype: 'json'
    });
    jQuery("#Inversionistas").trigger("reloadGrid");
}


function cargarSucursal(){
    var varbanco = document.getElementById('banco').value;
    // alert($("#CONTROLLER").val());
    // $("#sucursal").attr("disabled","disabled");;
    $.ajax({
        dataType: 'html',
        url: $("#CONTROLLER").val()+"?estado=Credito&accion=Bancario",
        data: "opcion=CARGAR_SUCURSALES&banco="+varbanco,
        success: function(resp){
            $("#sucursal").empty();        
            $("#sucursal").append(resp);
            
        },
        error: function(){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });
   
}
function guardarMovimiento(){
    var controller =$("#CONTROLLER").val();
    var BASEURL =$("#BASEURL").val();
    var varbanco = $("#banco").val();
    var valor = $("#valor").val();              
    var url = controller+'?estado=Captacion&accion=Inversionista';

    $.ajax({
        dataType: 'html',
        url:url,
        data: {
            op : '7',
            inversionista: $("#nit").val() ,
            valor:valor,
            banco :varbanco,
            sucursal:$("#sucursal").val(),
            subcuenta:$("#subcuenta").val(),
            tipo_movimiento:$("#tipo_movimiento").val(),
            fecha:$("#fecha").val(),
            titular_cuenta:$("#titular_cuenta").val(),
            nit_cuenta:$("#nit_cuenta").val(),
            cuenta:$("#cuenta").val(),
            tipo_cuenta:$("#tipo_cuenta").val(),
            nombre_beneficiario:$("#nombre_beneficiario").val(),
            nit_beneficiario:$("#nit_beneficiario").val(),
            cheque_cruzado:$("#cheque_cruzado").val(),
            cheque_primer_beneficiario:$("#cheque_primer_beneficiario").val(),
            nombre_beneficiario_capt:$("#nombre_beneficiario_capt").val(),
            tipo_transferencia:$("#tipo_transferencia").val(),
            sw_datos_transferencia:$("#sw_datos_transferencia").val(),
            concepto_transaccion:$("#conceptoTrans").val()
        },
        method: 'post',
        beforeSend: function(objeto){
            cargando("divCrearMovimiento","Guardando");
        },

        success:  function (resp){
            var boton = "<div style='text-align:center'><img src='" + BASEURL +"/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\""+"#divCrearMovimiento" +"\");refreshGrid()' /></div>"
            $("#divCrearMovimiento").html(resp+ "<br/><br/><br/><br/>"+boton);
        },
        error: function(){
            alert("Error");
        }
    });
    
}


function cargando(div,text)
{
    var BASEURL =$("#BASEURL").val();
    var loading ="";
    var salida = "<div style='text-align:center'><img src='" + BASEURL +"/images/loading.gif' align='absmiddle' />" + text +"</div>"
    $("#"+div).html(salida)
    return loading;
}








function verMovimiento(inversionista){
    var baseurl = $("#BASEURL").val();
    var nit = $("#inversionista").val();
    var subcuenta = $("#subcuenta").val();
    if(inversionista=="S")
    {
        location.href = baseurl+"/jsp/inversionistas/MovimientoInv.jsp?nit="+nit+"&subcuenta="+subcuenta;
    }else
    {
        location.href = baseurl+"/jsp/inversionistas/Movimiento.jsp?nit="+nit+"&subcuenta="+subcuenta;
    }

   
}






function calcularTasas(){

    var intereses = $("#intereses").val();
    var ano_base = $("#ano_base").val();
    var ano_base_td = $("#ano_base_td").val();
    var rendimiento =$("#rendimiento").val();
    if(parseInt(rendimiento)==0)
    {
   
        document.getElementById("rendimiento_dias").type="text";
        rendimiento=$("#rendimiento_dias").val();
    }
    else
    {
        document.getElementById("rendimiento_dias").type="hidden";
    }


    var t = (parseFloat($("#tasa").val())/100)

    // calcular tasa ea///
    var tea =(  Math.pow((1+t),ano_base/rendimiento))-1
    $("#ea").val((tea*100).toFixed(4) + "%");
    $("#tasa_ea").val(tea*100) ;


    ////tasa nominal
    var tn  = t *(ano_base/rendimiento)
    $("#nominal").val((tn*100).toFixed(5) + "%");
    $("#tasa_nominal").val(tn*100);


    ///tasa diaria
    if(intereses=="C")
    {
        var td=(  Math.pow((1+tea),1/ano_base))-1
    }
    else
    {
        var td=(tn/ano_base_td)
    }

    $("#diaria").val((td*100).toFixed(6) + "%");
    $("#tasa_diaria").val(td*100);




}

function consultarCedula(controller, BASEURL){
    new Ajax.Request(
        controller+'?estado=Captacion&accion=Inversionista',
        {
            method: 'post',
            parameters: {
                opcion: 'CONSULTAR_INVERSIONISTA',
                inversionista: $F('inversionista')
            },
            onSuccess: function (resp){
                var r=resp.responseText;
                if(r=="ok"){
                    //  alert(resp.responseText);
                    document.forms[0].action = BASEURL+"/jsp/inversionistas/crearInversionista.jsp";
                    document.forms[0].submit();
                }else{
                   
                    alert(resp.responseText);
                }
            }
        }
        );
}

function agregarSubcuenta(i, BASEURL) {
    var max = parseInt($F("max"))+1;
    var html = "<tr id='fila"+max+"' class='fila'>"+
    "<td colspan='2'>"+
    "<img alt='+' src='"+BASEURL+"/images/botones/iconos/mas.gif' onclick='agregarSubcuenta("+max+",\""+BASEURL+"\")' height='15' /> "+
    "<img alt='-' src='"+BASEURL+"/images/botones/iconos/menos1.gif' onclick='eliminarSubcuenta("+max+")' height='15' /> "+
    "<input type='text' id='nombre_subcuenta"+max+"' name='nombre_subcuenta"+max+"' style='width: 300px' />"+
    "</td>"+
    "</tr>";

    $("fila"+i).insert({
        after: html
    });
    $("count").value = parseInt($F("count")) + 1;
    $("max").value=max;
}

function eliminarSubcuenta(i){
    if( parseInt($F("count")) > 1){
        $("fila"+i).remove();
        $("count").value =parseInt($F("count")) - 1;
    }else{
        alert("No puede eliminar el ultimo item");
    }
}

function guardarInversionista(controller){
    var sw = true;
    for(var i=0;i<=$F("max") && sw;i++){
        if(document.getElementById("nombre_subcuenta"+i)!=null){
            if($F("nombre_subcuenta"+i) == ""){
                sw = false;
                alert("El campo del nombre de la subcuenta esta vacio");
            }
        }
    }
    if(sw){
        document.forms[0].action = controller+'?estado=Captacion&accion=Inversionista&opcion=GUARDAR_INVERSIONISTA';
        document.forms[0].submit();
    }
}
//// cargar subcuentas pendiente por modificar para jquery
function cargarSubcuentasd(){
    var controller = document.getElementById("CONTROLLER").value;
    new Ajax.Updater({
        success: 'subcuenta',
        failure:'divMensaje'
    }, controller+'?estado=Captacion&accion=Inversionista', {
        method: 'post',
        parameters: {
            opcion: 'CARGAR_SUBCUENTAS',
            inversionista: $F('inversionista')
        }
    });



}


function cargarSubcuentas(){
 
    var controller = document.getElementById("CONTROLLER").value;
    $.ajax({
        dataType: 'html',
        url: controller+'?estado=Captacion&accion=Inversionista',
        data: "opcion=CARGAR_SUBCUENTAS&inversionista="+$('#inversionista').val(),
        success: function(resp){
            
            $("#subcuenta").empty();
            $("#subcuenta").append(resp);
        },
        error: function(){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });

}


function cargarDatosSubcuenta(){
    //Hacer consulta por ajax que retorne un xml con los datos a mostrar
    var controller = document.getElementById("CONTROLLER").value;
    var opcion = "CARGAR_SUBCUENTA_INVERSIONISTA";
    var inversionista=$('#inversionista').val();
    var subcuenta =$('#subcuenta').val();

    if( window.XMLHttpRequest )
        ajax = new XMLHttpRequest(); // No Internet Explorer
    else
        ajax = new ActiveXObject("Microsoft.XMLHTTP"); // Internet Explorer
    ajax.onreadystatechange =RespuestaCargarDatosSubcuenta;
    ajax.open( "POST",controller+'?estado=Captacion&accion=Inversionista'+"&opcion="+opcion+"&inversionista="+inversionista+"&subcuenta="+subcuenta, true );
    ajax.send( "" );
}



function confirmarModificacionPagos()
{
    var BASEURL =$("#BASEURL").val();
    var boton_cencelar = "<img src='" + BASEURL +"/images/botones/cancelar.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\""+"#divSalida" +"\");' />"
    var boton_aceptar = "<img src='" + BASEURL +"/images/botones/aceptar.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='autorizarPagos();' />"
    
    var controller =$("#CONTROLLER").val();
    var url = controller+'?estado=Captacion&accion=Inversionista';
    var sw=$('input:checked[name="pagos_automaticos"]').is(':checked');
    // var sw_form = (($("#periodicidad_pago").val()!="0") && (sw));
    var sw_form = (parseInt($("#periodicidad_pago").val())!=0);
    
   
    if ((sw_form==true) && (sw==true))
    {
        $("#divSalida").html("<div id='resp'></div>");
        $('#divSalida').dialog('close');
        $("#divSalida").dialog("open");
        $.ajax({
            dataType: 'html',
            url:url,
            data: {
                op : '26',
                periodicidad_pago: $("#periodicidad_pago").val()
            },
            method: 'post',
            success:  function (resp){
                $("#divSalida").html("<p>Usted habilit&oacute; la opcion para pagos automaticos <b>"+""+"</b></p>"+
                    "<p> El proximo pago de intereses sera programado para el primer dia habil posterior a <b>"+resp+"</b>.</p>"+
                    "<p>&iquest;Desea guardar los cambios?</p> <br/><br/><div align='center'>"+boton_aceptar+"   " +boton_cencelar+"</div>");
                $("#periodicidad_pago").attr('disabled', false);

            },
            error: function(resp){
                alert("Ocurrio un error enviando los datos al servidor");
            }
        });
    }
    else
    {
        $("#periodicidad_pago").attr('disabled', false);

        if(!sw)
        {
            $("#divSalida").html("<div id='resp'></div>");
            $('#divSalida').dialog('close');
            $("#divSalida").dialog("open");
            $("#periodicidad_pago").attr('disabled', true);
            $("#divSalida").html("<p>Usted deshabilit&oacute; la opcion para pagos automaticos</p>"+
                "<p>No se generaran pagos automaticos a partir de de la fecha</b>.</p>"+
                "<p>&iquest;Desea guardar los cambios?</p> <br/><br/><div align='center'>"+boton_aceptar+"   " +boton_cencelar+"</div>");
            $("#periodicidad_pago").val(0);
        }

    }

}

function CancelarAutorizacionPagosAutomaticos(periodicidad)
{
    $("#divSalida").dialog('close')
}

function autorizarPagos()
{
    var BASEURL =$("#BASEURL").val();

    var controller =$("#CONTROLLER").val();
    var url = controller+'?estado=Captacion&accion=Inversionista';


    $.ajax({
        dataType: 'html',
        url:url,
        data: {
            op : '27',
            subcuenta:$("#subcuenta").val() ,
            nit : $("#nit").val(),
            periodicidad_pago: $("#periodicidad_pago").val(),
            pagos_automaticos:$("#pagos_automaticos").val()
        },
        method: 'post',
        success:  function (resp){
            $("#divSalida").html(resp);
       

        },
        error: function(resp){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });    
}

function RespuestaCargarDatosSubcuenta()
{
    if( ajax.readyState == 1 )
    {
    }
    if( ajax.readyState == 4 )
    {
        if( ajax.status == 200 )
        {
            // var codigo =document.getElementById('codigo')
            var respuesta=ajax.responseXML;
            $('#tasa').val(respuesta.getElementsByTagName('tasa')[0].childNodes[0].data)
            $('#rfte').val(respuesta.getElementsByTagName('retefuente')[0].childNodes[0].data)
            $('#rica').val(respuesta.getElementsByTagName('reteica')[0].childNodes[0].data)
            var ea=respuesta.getElementsByTagName('tasa_ea')[0].childNodes[0].data
            $('#ea').val(parseFloat(ea).toFixed(4) + "%");
            $('#tasa_ea').val(ea);

            var tn=respuesta.getElementsByTagName('tasa_nominal')[0].childNodes[0].data
            $('#nominal').val(parseFloat(tn).toFixed(4) + "%");
            $('#tasa_nominal').val(tn);
            var td=respuesta.getElementsByTagName('tasa_diaria')[0].childNodes[0].data
            $('#diaria').val(parseFloat(td).toFixed(4) + "%");
            $('#tasa_diaria').val(td);
            var ano_base =respuesta.getElementsByTagName('ano_base')[0].childNodes[0].data
            var ano_base_td =respuesta.getElementsByTagName('ano_base_td')[0].childNodes[0].data
            var rendimiento=respuesta.getElementsByTagName('rendimiento')[0].childNodes[0].data
            var tipo=respuesta.getElementsByTagName('tipo_interes')[0].childNodes[0].data
            var dc=respuesta.getElementsByTagName('dias_calendario')[0].childNodes[0].data
            var pago_automatico=respuesta.getElementsByTagName('pago_automatico')[0].childNodes[0].data
            var periodicidad_pago=respuesta.getElementsByTagName('periodicidad_pago')[0].childNodes[0].data
            var genera_documentos=respuesta.getElementsByTagName('genera_documentos')[0].childNodes[0].data

            genera_documentos=(/true/i.test(genera_documentos));
            $('input[name=genera_documentos]').attr('checked', genera_documentos);



            if((rendimiento!=30 && rendimiento!=60 && rendimiento!=90 && rendimiento!=180 && rendimiento!=360))
            {
                $('rendimiento_dias').value=rendimiento;
                $('rendimiento_dias').type="text";
                asignaIndex("rendimiento","0");
            }
            else
            {
                asignaIndex("rendimiento",rendimiento);
                $('rendimiento_dias').type="hidden";
            }
            asignaIndex("ano_base",ano_base);
            asignaIndex("ano_base_td",ano_base_td);
            asignaIndex("intereses",tipo);
            asignaIndex("dias_calendario",dc);
            asignaIndex("pago_automatico",pago_automatico);
            asignaIndex("periodicidad_pago",periodicidad_pago);

 
          

        }
    }
}




function asignaIndex(lista,value)
{
    var combo = document.getElementById(lista);
    for (i=0; i <= combo.length - 1; i++)
    {
        //si el codigo del elemento es igual al codigo que se escribio...
        if(combo.options[i].value ==value)
        {            //entonces: selecciona ese elemento
            combo.selectedIndex = i;
            return; //ya se encontro el elemento, por lo tanto sale de la funcion
        }
    }
}






function guardarLiquidacion(controller){
    if($("#inversionista").val()!="" && $("#subcuenta").val()!="" && $("#tasa").val()!="" && $("#ano_base").val()!="" && $("#rendimiento").val()!="" && $("#intereses").val()!="" && $("#rfte").val()!="" && $("#rica").val()!=""){

        document.forms[0].action = controller+'?estado=Captacion&accion=Inversionista&opcion=GUARDAR_DATOS_LIQUIDACION';
        document.forms[0].submit();
    }else{
        alert("Debe ingresar todos los datos");
    }
}




function cargarAnoMes(){
    var hoy = new Date();
    $("#ano").empty();
    for (var ano = 2012; ano <= hoy.getFullYear(); ano++){
        $("#ano").append("<option value='"+ ano +"' "+ (ano == hoy.getFullYear() ? 'selected': '') +">"+  ano +"</option>");
    }

    var meses = new Array ('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
    $("#mes").empty();
    for (var mes = 0; mes < meses.length; mes++){
        if ((mes+1)<10){
            $("#mes").append("<option value='"+ '0'+(mes+1) +"' "+ ((mes+1) == hoy.getMonth() + 1 ? 'selected': '') +">"+  meses[mes] +"</option>");
        }else{
            $("#mes").append("<option value='"+ (mes+1) +"' "+ ((mes+1) == hoy.getMonth() + 1 ? 'selected': '') +">"+  meses[mes] +"</option>");
        }
    }
}



function URLEncode(c){
    var o='';
    var x=0;
    c=c.toString();
    var r=/(^[a-zA-Z0-9_.]*)/;
    while(x<c.length){
        var m=r.exec(c.substr(x));
        if(m!=null && m.length>1 && m[1]!=''){
            o+=m[1];
            x+=m[1].length;
        }else{
            if(c[x]==' ')o+='+';
            else{
                var d=c.charCodeAt(x);
                var h=d.toString(16);
                o+='%'+(h.length<2?'0':'')+h.toUpperCase();
            }
            x++;
        }
    }
    return o;
}

function URLDecode(s){
    var o=s;
    var binVal,t;
    var r=/(%[^%]{2})/;
    while((m=r.exec(o))!=null && m.length>1 && m[1]!=''){
        b=parseInt(m[1].substr(1),16);
        t=String.fromCharCode(b);
        o=o.replace(m[1],t);
    }
    return o;
}





