/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $("#fechaini").val('');
    $("#fechafin").val('');
    $("#fechaini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $('#ui-datepicker-div').css('clip', 'auto');

    cargarAnticipoCajaMenor();

    $('#guardar').click(function () {
        var respuesta = validarGuardado();
        //alert(respuesta);
        if (respuesta === true) {
            guardarAnticipoCajaMenor();
            //guardarDocumentos();
        }
    });

    $('#buscarCuenta').click(function () {
        var tipo_busqueda = $("#tipo_busqueda").val();
        var informacion = $("#busqueda").val();
        if (informacion === '') {
            mensajesDelSistema("Digite la informacion a buscar", '314', '140', false);
        } else {
            buscarCuentas(tipo_busqueda, informacion);
        }
    });


    $('#buscar').click(function () {
        cargarAnticipoCajaMenor();
    });
});

function onKeyDecimal(e, num) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if (document.getElementById(num.id).value.indexOf('.') !== -1 && keynum === 46)
        return false;
    if ((keynum === 0 || keynum === 8 || keynum === 48 || keynum === 46)) {
        return true;
    }

    if (keynum >= 58) {
        return false;

    }

    return /\d/.test(String.fromCharCode(keynum));
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function validarGuardado() {
    var estado = true;
    var ids = jQuery("#tabla_anticipo").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var empleado = jQuery("#tabla_anticipo").getRowData(ids[i]).empleado;
        var identificacion = jQuery("#tabla_anticipo").getRowData(ids[i]).identificacion;
        var sucursal = jQuery("#tabla_anticipo").getRowData(ids[i]).sucursal;
        var Concepto = jQuery("#tabla_anticipo").getRowData(ids[i]).Concepto;
        var valor = jQuery("#tabla_anticipo").getRowData(ids[i]).descripcion;

        if (parseFloat(valor) === 0 || Concepto === '' || sucursal === '' || identificacion === '' || empleado === '') {
            estado = false;
            mensajesDelSistema('Falta ingresar datos en la cabecera', '250', '150');
        }

    }
    return estado;
}

function mensajesDelSistema(msj, width, height) {
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarFaturas(documento) {
    var grid_tabla = $("#tabla_factura");
    if ($("#gview_tabla_factura").length) {
        reloadGridtabla(grid_tabla, 18, documento);
    } else {
        grid_tabla.jqGrid({
            caption: "Documento cabecera",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '80',
            width: '1503',
            async: true,
            colNames: ['Id', 'Tipo Documento', 'Numero Documento', 'Empleado', 'Nit', 'Fecha documento', 'Autorizador', 'HC', 'Descripcion', 'Valor'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', hidden: true, key: true},
                {name: 'documento', index: 'documento', width: 90, align: 'left', hidden: true},
                {name: 'num_documento', index: 'num_documento', width: 120, align: 'center', hidden: false},
                {name: 'empleado', index: 'empleado', width: 270, align: 'left', hidden: false},
                {name: 'proveedor', index: 'proveedor', width: 120, align: 'center', hidden: false},
                //{name: 'buscar_proveedor', index: 'buscar_proveedor', width: 80, align: 'left', hidden: false},
                {name: 'fecha', index: 'fecha', width: 130, align: 'center', sortable: false, search: false, editable: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'autorizador', index: 'autorizador', width: 150, align: 'center', hidden: false, editable: true, edittype: 'select', editrules: {required: true},
                    editoptions: {
                        value: cargarAutorizador(),
                        style: "width: 140px !important",
                        dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        var rowid = e.target.id.replace("_autorizador", "");
//                                        jQuery("#tabla_factura").jqGrid('setCell', rowid, 'idautorizador', e.target.value);
                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }},
                {name: 'hc', index: 'hc', width: 105, align: 'center', hidden: false, editable: true, edittype: 'select', editrules: {required: true},
                    editoptions: {
                        value: cargarHC(),
                        style: "width: 140px",
                        dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        var rowid = e.target.id.replace("_hc", "");
                                        // jQuery("#tabla_factura").jqGrid('setCell', rowid, 'idautorizador', e.target.value);
                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }},
                {name: 'descripcion', index: 'descripcion', width: 457, align: 'left', hidden: false, editable: true, classes: ''},
                {name: 'valor', index: 'valor', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: false,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            editurl: 'clientArray',
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function (rowid) {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    var empleado = $('#empleado_').val();
                    var identificacion = $('#identificacion_').val();
                    var f = new Date();
                    var fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
                    var grid = $("#tabla_factura");
                    var rowid = 'neo_' + grid.getRowData().length;
                    var defaultData = {empleado: empleado, proveedor: identificacion, fecha: fecha};
                    grid.addRowData(rowid, defaultData);
                }

            },
            gridComplete: function (rowid) {
//                var grid_tabla_ = jQuery("#tabla_factura");
//                var ids = jQuery("#tabla_factura").jqGrid('getDataIDs');
//                for (var i = 0; i < ids.length; i++) {
//                    var cl = ids[i];
//                    be = "<img src='/fintra/images/botones/iconos/search.png' style='height:16px;width:21px;margin-left: 30px;' type='button' id='be" + cl + "' title ='Buscar proveedor' onclick=\"ventanaProveedor('" + cl + "','tabla_factura');\" />";
//                    jQuery("#tabla_factura").jqGrid('setRowData', ids[i], {buscar_proveedor: be});
//                }

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var valordoc = 'neo_' + ((grid_tabla.getRowData().length) - 1);
                var id = $("#tabla_factura").getRowData(rowid).id;
                // alert('valordoc: ' + valordoc + 'id: ' + id);
                if (id === valordoc) {
                    jQuery("#tabla_factura").jqGrid('editRow', rowid, true, function () {
                    }, null, null, {}, function (rowid) {
                        var valor = $("#tabla_factura").getRowData(rowid).valor;
                        var descripcion = $("#tabla_factura").getRowData(rowid).descripcion;
                        var proveedor = $("#tabla_factura").getRowData(rowid).proveedor;
                        var fecha = $("#tabla_factura").getRowData(rowid).fecha;
                        if (valor === 0 || descripcion === '' || proveedor === '' || fecha === '') {
                            mensajesDelSistema("Faltan ingresar datos en el documento cabecera", '314', '140', false);
                            jQuery("#tabla_factura").jqGrid('editRow', rowid, true, function () {
                            });
                        }
                    });
                    return;
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 18,
                    documento: documento
                }
            },
            loadError: function (xhr, status, error) {
                // mensajesDelSistema(error, 250, 150);
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
//        jQuery("#tabla_factura").navButtonAdd('#pager', {
//            caption: "Agregar",
//            title: "Agregar Facturacion",
//            onClickButton: function () {
//
//                var info = grid_tabla.getGridParam('records');
//                if (info === 0) {
//                    var f = new Date();
//                    var fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
//                    var grid = $("#tabla_factura");
//                    var rowid = 'neo_' + grid.getRowData().length;
//                    // alert(rowid);
//
//                    var defaultData = {valor: 0, fecha: fecha};
//                    grid.addRowData(rowid, defaultData);
//                    grid.jqGrid('editRow', rowid, true, function () {
//                    }, null, null, {}, function (rowid) {
//                        var valor = $("#tabla_factura").getRowData(rowid).valor;
//                        var descripcion = $("#tabla_factura").getRowData(rowid).descripcion;
//                        var proveedor = $("#tabla_factura").getRowData(rowid).proveedor;
//                        var fecha = $("#tabla_factura").getRowData(rowid).fecha;
//                        if (valor === 0 || descripcion === '' || proveedor === '' || fecha === '') {
//                            mensajesDelSistema("Faltan ingresar datos en el documento cabecera", '314', '140', false);
//                        }
//                    });
//                } else {
//                    mensajesDelSistema("Solo puede crear un documento cabecera", '314', '140', false);
//                }
//            }
//        });
    }
}

function reloadGridtabla(grid_tabla, op, documento) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                documento: documento
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function cargarBanco() {
    var resultado;
    $.ajax({
        type: 'get',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 29
        },
        success: function (json) {
            try {
                if (json.error) {
                    alert(json.error);
                    resultado = {};
                } else {
                    // console.log(json);

                    resultado = json;


                    console.log(resultado);

                }
            } catch (exception) {
                mensajesDelSistema('error : ', '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return resultado;
}

function cargarSucursal(nombre, banco) {
    var resultado;
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 30,
            banco: banco
        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#' + nombre + '_sucursal').html('');
                $('#' + nombre + '_sucursal').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#' + nombre + '_sucursal').append('<option value="' + datos + '">' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return resultado;
}

function cargarAutorizador() {
    var resultado;
    $.ajax({
        type: 'get',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 10
        },
        success: function (json) {
            try {
                if (json.error) {
                    alert(json.error);
                    resultado = {};
                } else {
                    console.log(json);
                    resultado = json;
                }
            } catch (exception) {
                mensajesDelSistema('error : ', '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return resultado;
}

function cargarHC() {
    var resultado;
    $.ajax({
        type: 'get',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 11
        },
        success: function (json) {
            try {
                if (json.error) {
                    alert(json.error);
                    resultado = {};
                } else {
                    console.log(json);
                    resultado = json;
                }
            } catch (exception) {
                mensajesDelSistema('error : ', '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return resultado;
}

function cargarFaturaDetalle(documento) {
//    documento = $('#documento').val();
    var grid_tabla = $("#tabla_factura_detalle");
    if ($("#gview_tabla_factura_detalle").length) {
        reloadGridtablaDet(grid_tabla, 19, documento);
    } else {
        grid_tabla.jqGrid({
            caption: "Documento detalle",
            url: "./controller?estado=Admin&accion=Fintra",
            //   editurl: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '200',
            width: '1503',
            colNames: ['Id', 'Item', 'Tipo Documento', 'Numero Documento', '#Factura', 'Proveedor', 'Nombre', 'Cuenta', 'Fecha Documento', 'Motivo', 'Descripcion', 'Valor', 'Iva',
                'Impuesto', 'Valor Iva', 'Concepto', '%', 'Valor Reteiva', 'Signo', 'Reteica', 'Impuesto', 'Valor Reteica', '%', 'Signo', 'Retefuente', 'Impuesto',
                'Valor Retefuente', '%', 'Signo', 'Total', 'Numos', 'Multiservicio'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'center', hidden: true, key: true},
                {name: 'item', index: 'item', width: 80, align: 'center', hidden: false},
                {name: 'tipo_documento', index: 'tipo_documento', width: 80, align: 'left', hidden: true},
                {name: 'documento', index: 'documento', width: 110, align: 'left', hidden: true},
                {name: 'referencia_1', index: 'referencia_1', width: 110, align: 'left', hidden: false, editable: true},
                {name: 'proveedor', index: 'proveedor', width: 90, align: 'center', hidden: false},
                {name: 'empleado', index: 'empleado', width: 200, align: 'center', hidden: false},
                {name: 'codigo_cuenta', index: 'codigo_cuenta', width: 110, align: 'center', hidden: false},
                //{name: 'buscar_cuenta', index: 'cuenta', width: 80, align: 'left', hidden: false},
                {name: 'creation_date', index: 'creation_date', width: 110, align: 'center', sortable: false, search: false, editable: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'motivo', index: 'motivo', width: 150, align: 'left', hidden: false, editable: true, edittype: 'select', editrules: {required: true},
                    editoptions: {
                        value: cargarMotivos(),
                        style: "width: 150px !important",
                        dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        var rowid = e.target.id.replace("_motivo", "");
                                        //  jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'idmotivo', e.target.value);
                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }},
                {name: 'descripcion', index: 'descripcion', width: 250, align: 'left', hidden: false, editable: true},
                {name: 'valor', index: 'valor', sortable: true, width: 110, align: 'right', search: true, editable: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;

                                }
                            });
                        }
                    }},
                {name: 'cod_impuesto_iva', index: 'cod_impuesto_iva', width: 80, align: 'center', hidden: false},
                {name: 'porc_iva', index: 'porc_iva', width: 80, align: 'center', hidden: false},
                {name: 'valor_iva', index: 'valor_iva', width: 80, align: 'center', hidden: false},
                {name: 'cod_impuesto_riva', index: 'cod_impuesto_riva', width: 80, align: 'center', hidden: false},
                {name: 'porc_riva', index: 'porc_riva', width: 80, align: 'center', hidden: false},
                {name: 'valor_riva', index: 'valor_riva', width: 80, align: 'center', hidden: false},
                {name: 'ivaSigno', index: 'ivaSigno', width: 80, align: 'center', hidden: true},
                //{name: 'buscar_iva', index: 'buscar_iva', width: 80, align: 'left', hidden: false},
                {name: 'cod_impuesto_rica', index: 'cod_impuesto_rica', width: 80, align: 'center', hidden: false},
                {name: 'porc_rica', index: 'porc_rica', width: 80, align: 'center', hidden: false},
                {name: 'valor_rica', index: 'valor_rica', width: 80, align: 'center', hidden: false},
                {name: 'porcrica', index: 'porcrica', width: 80, align: 'center', hidden: true},
                {name: 'ricaSigno', index: 'ricaSigno', width: 80, align: 'center', hidden: true},
                //{name: 'buscar_rica', index: 'buscar_rica', width: 80, align: 'left', hidden: false},
                {name: 'cod_impuesto_rtfuente', index: 'cod_impuesto_rtfuente', width: 80, align: 'center', hidden: false},
                {name: 'porc_rtfuente', index: 'porc_rtfuente', width: 80, align: 'center', hidden: false},
                {name: 'valor_rtfuente', index: 'valor_rtfuente', width: 80, align: 'center', hidden: false},
                {name: 'porcrtfuente', index: 'porcrtfuente', width: 80, align: 'center', hidden: true},
                {name: 'rfteSigno', index: 'rfteSigno', width: 80, align: 'center', hidden: true},
                //{name: 'buscar_rtfe', index: 'buscar_rtfe', width: 80, align: 'left', hidden: false},
                {name: 'vlr', index: 'vlr', sortable: true, width: 100, align: 'right', search: true, editable: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }},
                {name: 'numos', index: 'proyecto', width: 100, align: 'left', hidden: true},
                {name: 'multiservicio', index: 'multiservicio', width: 100, align: 'center', hidden: false}
                // {name: 'buscar_numos', index: 'buscar_numos', width: 100, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            editurl: 'clientArray',
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    //  mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }

            },
            gridComplete: function () {
                var grid_tabla_ = jQuery("#tabla_factura_detalle");
                var ids = jQuery("#tabla_factura_detalle").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var total = grid_tabla_.jqGrid('getCol', 'vlr', false, 'sum');
                    grid_tabla_.jqGrid('footerData', 'set', {vlr: total});
                }
            },
            onCellSelect: function (rowid, iCol, cellcontent, e) {
                var cm = jQuery('#tabla_factura_detalle').jqGrid("getGridParam", 'colModel');
                var colName = cm[iCol];
                //alert(colName.name);
                if (colName.name === 'proveedor') {
                    ventanaProveedor(rowid, 'tabla_factura_detalle');
                } else if (colName.name === 'cod_impuesto_iva') {
                    ventanaImpuesto(rowid, 'IVA');

                } else if (colName.name === 'cod_impuesto_riva') {
                    ventanaImpuesto(rowid, 'IVA');

                } else if (colName.name === 'cod_impuesto_rica') {
                    ventanaImpuesto(rowid, 'RICA');

                } else if (colName.name === 'cod_impuesto_rtfuente') {
                    ventanaImpuesto(rowid, 'RFTE');

                } else if (colName.name === 'codigo_cuenta') {
                    ventanaCuentas(rowid);

                } else if (colName.name === 'multiservicio') {
                    ventanaMultiservicio(rowid);
                }
                // now you can use rowData.name2, rowData.name3, rowData.name4 ,...
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

                jQuery("#tabla_factura_detalle").jqGrid('editRow', rowid, true, function () {
                }, null, null, {}, function (rowid) {
                    var myGrid = jQuery("#tabla_factura_detalle"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                    filas = myGrid.jqGrid("getLocalRow", selRowIds);
                    //filas = $("#tabla_factura_detalle").getRowData(rowid);
                    var iva = filas.cod_impuesto_iva;
                    var rica = filas.cod_impuesto_rica;
                    var rfte = filas.cod_impuesto_rtfuente;
                    var riva = filas.cod_impuesto_riva;
                    var valor = parseInt(filas.valor);
                    var ivaImp = parseFloat(filas.porc_iva);
                    var ivaPorct = parseFloat(filas.porc_riva);
                    var ricaImp = parseFloat(filas.porc_rica);
                    var rfteImp = parseFloat(filas.porc_rtfuente);

                    var valorCalculado = 0, valoriva = 0, valorriva = 0, valorRica = 0, valorRtfe = 0, valoriva_ = 0;
                    if (iva === '' && rica === '' && rfte === '') {
                        valorCalculado = valor;
                    }
                    if (iva !== '') {
                        valorCalculado = valor + ((valor * ivaImp) / 100);
                        valoriva = (valor * ivaImp) / 100;
                        jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_iva', valoriva);
                    } else {
                        jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_iva', 0);
                    }

                    if (riva !== '') {
                        valoriva = valor + ((valor * ivaImp) / 100);
                        valorriva = ((valor * ivaPorct) / 100);
                        valorCalculado = valoriva - valorriva;
                        jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_riva', valorriva);
                    } else {
                        jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_riva', 0);
                    }

                    if (rica !== '') {
                        if (iva === '') {
                            valorCalculado = valor - ((valor * ricaImp) / 100);
                            valorRica = (valor * ricaImp) / 100;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rica', valorRica);
                        } else {
                            valorCalculado = valorCalculado - ((valor * ricaImp) / 100);
                            valorRica = (valor * ricaImp) / 100;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rica', valorRica);
                        }
                    } else {
                        jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rica', 0);
                    }

                    if (rfte !== '') {
                        if (iva === '' && rica === '') {
                            valorCalculado = valor - ((valor * rfteImp) / 100);
                            valorRtfe = (valor * ricaImp) / 100;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rtfuente', valorRtfe);
                        } else {
                            valorCalculado = valorCalculado - ((valor * rfteImp) / 100);
                            valorRtfe = (valor * rfteImp) / 100;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rtfuente', valorRtfe);
                        }
                    } else {
                        jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rtfuente', 0);
                    }

                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'vlr', valorCalculado);
                    var total = jQuery("#tabla_factura_detalle").jqGrid('getCol', 'vlr', false, 'sum');
                    jQuery("#tabla_factura_detalle").jqGrid('footerData', 'set', {vlr: total});
                    calculaTotalCab(total);
                    validarDetalle(rowid);
                });
                return;

            },
            ajaxGridOptions: {
                async: false,
                data: {
                    opcion: 19,
                    documento: documento
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        jQuery("#tabla_factura_detalle").navButtonAdd('#pager1', {
            caption: "item",
            title: "Agregar ",
            onClickButton: function () {
                var f = new Date();
                var fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
                var grid = $("#tabla_factura_detalle");
                var rowid = 'neo_' + grid.getRowData().length;
                var item;
                if (((grid.getRowData().length) + 1) > 9) {
                    item = '0';
                } else {
                    item = '00';
                }
                var defaultData = {valor: 0, creation_date: fecha, tipo_documento: '', documento: '', item: item + ((grid.getRowData().length) + 1), vlr: 0, cod_impuesto_iva: '', cod_impuesto_riva: '', cod_impuesto_rica: '',
                    cod_impuesto_rtfuente: '', valor_iva: 0, valor_riva: 0, valor_rica: 0, valor_rtfuente: 0, porc_iva: 0, porc_riva: 0, porc_rica: 0, porc_rtfuente: 0};
                grid.addRowData(rowid, defaultData);
                grid.jqGrid('editRow', rowid, true, function () {
                }, null, null, {}, function (rowid) {
                    //asigna el valor total
                    var myGrid = jQuery("#tabla_factura_detalle"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                    filas = $("#tabla_factura_detalle").getRowData(rowid);
                    var iva = filas.cod_impuesto_iva;
                    var rica = filas.cod_impuesto_rica;
                    var rfte = filas.cod_impuesto_rtfuente;
                    var riva = filas.cod_impuesto_riva;
                    var valor = parseInt(filas.valor);
                    var ivaImp = parseFloat(filas.porc_iva);
                    var ivaPorct = parseFloat(filas.porc_riva);
                    var ricaImp = parseFloat(filas.porc_rica);
                    var rfteImp = parseFloat(filas.porc_rtfuente);
                    var valorCalculado = 0, valoriva = 0, valorriva = 0, valorRica = 0, valorRtfe = 0, valoriva_ = 0;
                    if (iva === '' && rica === '' && rfte === '') {
                        valorCalculado = valor;
                    }
                    if (iva !== '') {
                        valorCalculado = valor + ((valor * ivaImp) / 100);
                        valoriva = (valor * ivaImp) / 100;
                        jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_iva', valoriva);
                    } else {
                        jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_iva', 0);
                    }
                    if (riva !== '') {
                        valoriva = valor + ((valor * ivaImp) / 100);
                        valorriva = ((valor * ivaPorct) / 100);
                        valorCalculado = valoriva - valorriva;
                        jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_riva', valorriva);
                    } else {
                        jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_riva', 0);
                    }

                    if (rica !== '') {
                        if (iva === '') {
                            valorCalculado = valor - ((valor * ricaImp) / 100);
                            valorRica = (valor * ricaImp) / 100;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rica', valorRica);
                        } else {
                            valorCalculado = valorCalculado - ((valor * ricaImp) / 100);
                            valorRica = (valor * ricaImp) / 100;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rica', valorRica);
                        }
                    } else {
                        jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rica', 0);
                    }

                    if (rfte !== '') {
                        if (iva === '' && rica === '') {
                            valorCalculado = valor - ((valor * rfteImp) / 100);
                            valorRtfe = (valor * ricaImp) / 100;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rtfuente', valorRtfe);
                        } else {
                            valorCalculado = valorCalculado - ((valor * rfteImp) / 100);
                            valorRtfe = (valor * rfteImp) / 100;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rtfuente', valorRtfe);
                        }
                    } else {
                        jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rtfuente', 0);
                    }

                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'vlr', valorCalculado);
                    var total = jQuery("#tabla_factura_detalle").jqGrid('getCol', 'vlr', false, 'sum');
                    jQuery("#tabla_factura_detalle").jqGrid('footerData', 'set', {vlr: total});
                    calculaTotalCab(total);
                    validarDetalle(rowid);
                });
            }
        });
    }
}

function reloadGridtablaDet(grid_tabla, op, documento) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                documento: documento
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function calculaTotalCab(total) {
//asignar el total del detalle a la cabecera
    var grid = $("#tabla_factura");
    var rowid = 'neo_' + ((grid.getRowData().length) - 1);
    jQuery("#tabla_factura").jqGrid('setCell', rowid, 'valor', total);

    var valorCXC = numberSinComas($('#valor_factura').val());
    $('#valor_legalizar').val(numberConComas(total));
    var valorLegalizar = numberSinComas(total);
    var valor_total = parseFloat(valorCXC) - parseFloat(valorLegalizar);
    $('#total_').val(numberConComas(valor_total));
    if (valor_total < 0) {
        document.getElementById("total_").style.color = '#f60606';
    } else if (valor_total > 0) {
        document.getElementById("total_").style.color = '#050404';
    }

}

function validarDetalle(rowid) {
    var valor = $("#tabla_factura_detalle").getRowData(rowid).valor;
    var descripcion = $("#tabla_factura_detalle").getRowData(rowid).descripcion;
    var proveedor = $("#tabla_factura_detalle").getRowData(rowid).proveedor;
    var fecha = $("#tabla_factura_detalle").getRowData(rowid).creation_date;
    var codigo_cuenta = $("#tabla_factura_detalle").getRowData(rowid).codigo_cuenta;
    if (valor === 0 || descripcion === '' || proveedor === '' || fecha === '' || codigo_cuenta === '') {
        mensajesDelSistema("Faltan ingresar datos en el detalle", '314', '140', false);
    }
}

function ventanaProveedor(id, grilla) {

    autocompletarProveedor(id, grilla);
    $("#dialogMsjProveedor").dialog({
        width: '585',
        height: '200',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Buscar proveedor',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
//            "Asignar": function () {
//                jQuery("#" + grilla).jqGrid('setCell', id, 'proveedor', $("#proveedor").val());
//            },
            "Salir": function () {
                $('#proveedor').val('');
                $(this).dialog("close");
            }
        }
    });
}

function autocompletarProveedor(id, grilla) {
    $("#proveedor").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Admin&accion=Fintra",
                dataType: "json",
                data: {
                    informacion_: request.term,
                    opcion: 12,
                    busqueda: $('#tipo_busqueda_').val()
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.value,
                            mivar1: item.mivar1
                        };
                    }));
                }
            });
        },
        minLength: 3,
        delay: 500,
        disabled: false,
        select: function (event, ui) {
            if (grilla === 'tabla_anticipo') {
                jQuery("#" + grilla).jqGrid('setCell', id, 'identificacion', ui.item.value);
                jQuery("#" + grilla).jqGrid('setCell', id, 'empleado', ui.item.mivar1);
            } else {
                jQuery("#" + grilla).jqGrid('setCell', id, 'proveedor', ui.item.value);
                jQuery("#" + grilla).jqGrid('setCell', id, 'empleado', ui.item.mivar1);
            }

            console.log(ui.item ?
                    "Selected: " + ui.item.mivar1 :
                    "Nothing selected, input was " + ui.item.label);
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function ventanaImpuesto(id, impuesto) {
    buscarImpuesto(impuesto);
    $("#dialogMsjimpuesto").dialog({
        width: '819',
        height: '520',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Buscar impuesto',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function buscarImpuesto(impuesto) {
    var grid_tabla_ = jQuery("#tabla_impuesto");
    if ($("#gview_tabla_impuesto").length) {
        reloadGridMostrarImpuesto(grid_tabla_, 13, impuesto);
    } else {
        grid_tabla_.jqGrid({
            caption: "Impuesto",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '790',
            colNames: ['Agencia', 'Nombre', 'Tipo Impuesto', 'Codigo', 'Impuesto', 'Porcentaje', 'Concepto', 'Descripcion', 'Signo'],
            colModel: [
                {name: 'agencia', index: 'agencia', width: 90, sortable: true, align: 'left', hidden: true},
                {name: 'nombre', index: 'nombre', width: 120, sortable: true, align: 'center', hidden: true},
                {name: 'tipo_impuesto', index: 'tipo_impuesto', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'codigo', index: 'codigo', width: 120, sortable: true, align: 'center', hidden: false, key: true},
                {name: 'impuesto', index: 'impuesto', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'porcentaje', index: 'porcentaje', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'concepto', index: 'concepto', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 250, sortable: true, align: 'left', hidden: false},
                {name: 'signo', index: 'signo', width: 50, sortable: true, align: 'left', hidden: true}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager2',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 13,
                    impuesto: impuesto
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_impuesto"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var codigo = filas.codigo;
                var signo = filas.signo;
                var concepto = filas.concepto;
                var tipo_impuesto = filas.tipo_impuesto;
                var impuesto = filas.impuesto;
                var porcentaje = filas.porcentaje;
                var grid = $("#tabla_factura_detalle");
                var rowid = 'neo_' + ((grid.getRowData().length) - 1);

                var grid_tabla = jQuery("#tabla_factura_detalle");
                var valor = parseInt(grid_tabla.getRowData(rowid).valor);
                var valort = parseInt(grid_tabla.getRowData(rowid).vlr);
                var iva = grid_tabla.getRowData(rowid).cod_impuesto_iva;
                var rica = grid_tabla.getRowData(rowid).cod_impuesto_rica;
                // jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, tipo_impuesto.toLowerCase(), codigo);
                console.log(valor);
                var valorCalculado = 0, valoriva = 0, valorriva = 0, valorRica = 0, valorRtfe = 0, valoriva_;
                if (tipo_impuesto === 'IVA') {
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'cod_impuesto_iva', codigo);
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'porc_iva', impuesto);
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'porc_riva', porcentaje);
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'cod_impuesto_riva', concepto);
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'ivaSigno', signo);

                    if (isNaN(valor) !== true) {
                        //alert('entro');
                        if (codigo !== '') {
                            valorCalculado = valor + ((valor * impuesto) / 100);
                            valoriva = (valor * impuesto) / 100;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_iva', valoriva);
                            //alert('ivaImp:' + ivaImp + ' valorCalculado:' + valorCalculado + ' valoriva: ' + valoriva);
                        } else {
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_iva', 0);
                        }
                        if (concepto !== '') {
                            // alert(iva + ' ' + ivaPorct + ' ' + ivaPorct);
                            valoriva = valor + ((valor * impuesto) / 100);
                            valorriva = ((valor * porcentaje) / 100);
                            valorCalculado = valoriva - valorriva;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_riva', valorriva);
                            //alert('valoriva:' + valoriva + ' valorriva:' + valorriva + ' valorCalculado:' + valorCalculado);
                        } else {
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_riva', 0);
                        }
                    }
                }
                if (tipo_impuesto === 'RICA') {
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'cod_impuesto_rica', codigo);
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'porc_rica', impuesto);
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'porcrica', porcentaje);
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'ricaSigno', signo);

                    if (isNaN(valor) !== true) {
                        if (iva === '') {
                            valorCalculado = valor - ((valor * impuesto) / 100);
                            valorRica = (valor * impuesto) / 100;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rica', valorRica);
                            // alert('1 ricaImp:' + ricaImp + ' valorCalculado:' + valorCalculado);
                        } else {
                            valorCalculado = valort - ((valor * impuesto) / 100);
                            valorRica = (valor * impuesto) / 100;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rica', valorRica);
                            //alert('2 ricaImp:' + ricaImp + ' valorCalculado:' + valorCalculado);
                        }
                    }
                }
                if (tipo_impuesto === 'RFTE') {
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'cod_impuesto_rtfuente', codigo);
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'porc_rtfuente', impuesto);
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'porcrtfuente', porcentaje);
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'rfteSigno', signo);

                    if (isNaN(valor) !== true) {
                        if (iva === '' && rica === '') {
                            valorCalculado = valor - ((valor * impuesto) / 100);
                            valorRtfe = (valor * impuesto) / 100;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rtfuente', valorRtfe);
                            // alert('1 rfteImp:' + rfteImp + ' valorCalculado:' + valorCalculado);
                        } else {
                            valorCalculado = valort - ((valor * impuesto) / 100);
                            valorRtfe = (valor * impuesto) / 100;
                            jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'valor_rtfuente', valorRtfe);
                            //alert('2 rfteImp:' + rfteImp + ' valorCalculado:' + valorCalculado);
                        }
                    }
                }

                if (isNaN(valor) !== true) {
                    // var valordoc = 'neo_' + ((grid_tabla.getRowData().length) - 1);
                    jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'vlr', valorCalculado);

                    //calcular total detalle
                    var total = jQuery("#tabla_factura_detalle").jqGrid('getCol', 'vlr', false, 'sum');
                    jQuery("#tabla_factura_detalle").jqGrid('footerData', 'set', {vlr: total});
                    calculaTotalCab(total);
                    validarDetalle(rowid);
                }




                $("#dialogMsjimpuesto").dialog("close");
            }
        }).navGrid("#pager2", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridMostrarImpuesto(grid_tabla, opcion, impuesto) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "get",
            data: {
                opcion: opcion,
                impuesto: impuesto
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function guardarDocumentos() {
    var gridDet = jQuery("#tabla_factura_detalle"), filasd = gridDet.jqGrid('getDataIDs');
    var dataDet = jQuery("#tabla_factura_detalle").jqGrid('getRowData');
    for (var i = 0; i < filasd.length; i++) {
        console.log(filasd[i]);
        gridDet.saveRow(filasd[i]);
    }
    if (dataDet.length === 0) {
        mensajesDelSistema('Inserte infomacion', '300', '100', false);
    }

    var valorCXC = numberSinComas($('#valor_factura').val());
    var num_factura = $('#num_factura').val();
    var autorozador_ = $('#autorozador_').val();
    var valorLegalizar = numberSinComas($('#valor_legalizar').val());
    var anticipo = $('#anticipo_').val();
    var empleado_ = $('#identificacion_').val();


    $.ajax({
        async: false,
        url: "./controller?estado=Admin&accion=Fintra",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 14,
            informacion: JSON.stringify({detalle: dataDet, num_factura: num_factura, valorCXC: valorCXC, valorLegalizar: valorLegalizar, anticipo: anticipo, autorozador_: autorozador_, empleado_: empleado_})
        },
        success: function (json) {
//            if (json.respuesta === 'GUARDADO') {
//               mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);
//
//            } else {
//                mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
//            }
            if (json.respuesta === 'ERROR') {
                mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);

            } else {
                mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);
                $('.ui-dialog-buttonpane button:contains("Legalizar")').button().hide();
                cargarFaturas(json.respuesta);
                setTimeout(function () {
                    cargarFaturaDetalle(json.respuesta);
                }, 500);

            }


        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ventanaCuentas(id) {

    $("#dialogMsjcuenta").dialog({
        width: '630',
        height: '400',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Cuentas',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
//            "Asignar": function () {
//                jQuery("#tabla_factura").jqGrid('setCell', id, 'proveedor', $("#proveedor").val());
//            },
            "Salir": function () {
                $('#busqueda').val('');
                $(this).dialog("close");
            }
        }
    });
}

function buscarCuentas(busqueda, informacion) {
    var grid_tabla_ = jQuery("#tabla_cuentas");
    if ($("#gview_tabla_cuentas").length) {
        reloadGridMostrarcuentas(grid_tabla_, 15, busqueda, informacion);
    } else {
        grid_tabla_.jqGrid({
            caption: "Cuentas",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '150',
            width: '490',
            colNames: ['Cuenta', 'Nombre Cuenta'],
            colModel: [
                {name: 'cuenta', index: 'cuenta', width: 170, sortable: true, align: 'left', hidden: false},
                {name: 'nombre_largo', index: 'nombre_largo', width: 250, sortable: true, align: 'left', hidden: false}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager3',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 15,
                    busqueda: busqueda,
                    info: informacion
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_cuentas"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var cuenta = filas.cuenta;
                var grid = $("#tabla_factura_detalle");
                var rowid = 'neo_' + ((grid.getRowData().length) - 1);
                jQuery("#tabla_factura_detalle").jqGrid('setCell', rowid, 'codigo_cuenta', cuenta);
                $("#dialogMsjcuenta").dialog("close");
            }
        }).navGrid("#pager3", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridMostrarcuentas(grid_tabla, opcion, busqueda, informacion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "get",
            data: {
                opcion: opcion,
                busqueda: busqueda,
                info: informacion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function cargarMotivos() {
    var resultado;
    $.ajax({
        type: 'get',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 16
        },
        success: function (json) {
            try {
                if (json.error) {
                    alert(json.error);
                    resultado = {};
                } else {
                    console.log(json);
                    resultado = json;
                }
            } catch (exception) {
                mensajesDelSistema('error : ', '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return resultado;
}

function ventanaMultiservicio(id) {
    autocompletarMultiservicio(id);
    $("#dialogMsjNumos").dialog({
        width: '406',
        height: '200',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Buscar proveedor',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
//            "Asignar": function () {0
//                jQuery("#tabla_factura").jqGrid('setCell', id, 'proveedor', $("#proveedor").val());
//            },
            "Salir": function () {
                $('#proveedor').val('');
                $(this).dialog("close");
            }
        }
    });
}

function autocompletarMultiservicio(id) {
    $("#multiservicio").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Admin&accion=Fintra",
                dataType: "json",
                data: {
                    multiservicio: request.term,
                    opcion: 17
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.value,
                            mivar1: item.mivar1
                        };
                    }));
                }
            });
        },
        minLength: 3,
        delay: 500,
        disabled: false,
        select: function (event, ui) {
            jQuery("#tabla_factura_detalle").jqGrid('setCell', id, 'multiservicio', ui.item.value);
            jQuery("#tabla_factura_detalle").jqGrid('setCell', id, 'numos', 'NUMOS');
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar1 :
                    "Nothing selected, input was " + ui.item.label);
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarAnticipoCajaMenor() {
    var grid_tabla = $("#tabla_anticipo");
    if ($("#gview_tabla_anticipo").length) {
        reloadGridtablaAnticiposCajaMenor(grid_tabla, 25);
    } else {
        grid_tabla.jqGrid({
            caption: "Anticipos",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '270',
            width: '1503',
            async: true,
            colNames: ['Id', 'Anticipo', 'Tipo Anticipo', 'Identificacion', 'Empleado', '....', 'Fecha Documento', 'Banco', 'Sucursal', 'Autorizador', 'Concepto', 'Valor',
                '# CXC', 'Valor CXC', '# CXP', 'Valor CXP', 'Legalizado', 'Valor Legalizado', '# CXP Gasto', 'Valor Gastos', 'Legalizar', 'Anular'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', hidden: true, key: true},
                {name: 'cod_anticipo', index: 'cod_anticipo', width: 90, align: 'left', hidden: false},
                {name: 'tipo_anticipo', index: 'tipo_anticipo', width: 160, align: 'left', hidden: false, editable: true, edittype: 'select', editrules: {required: true},
                    editoptions: {
                        value: {'ANTICIPO GASTO VIAJE': 'ANTICIPO GASTO VIAJE', 'ANTICIPO OTROS': 'ANTICIPO OTROS'},
                        style: "width: 150px !important",
                        dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        //var rowid = e.target.id.replace("_sucursal", "");
                                        // jQuery("#tabla_factura").jqGrid('setCell', rowid, 'idsucursal', e.target.value);
                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }},
                {name: 'identificacion', index: 'identificacion', width: 90, align: 'center', hidden: false},
                {name: 'empleado', index: 'empleado', width: 200, align: 'center', hidden: false},
                {name: 'buscar_empleado', index: 'buscar_empleado', width: 80, align: 'left', hidden: false},
                {name: 'fecha', index: 'fecha', width: 110, align: 'center', sortable: false, search: false, editable: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'banco', index: 'banco', width: 150, align: 'center', hidden: false, editable: true, edittype: 'select', editrules: {required: true},
                    editoptions: {
                        value: cargarBanco(),
                        style: "width: 150px !important",
                        dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        var rowid = e.target.id.replace("_banco", "");
                                        // jQuery("#tabla_factura").jqGrid('setCell', rowid, 'idbanco', e.target.value);
                                        var grid = $("#tabla_anticipo");
                                        var rowid = 'neo_' + ((grid.getRowData().length) - 1);
                                        cargarSucursal(rowid, e.target.value);
                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }
                },
                {name: 'sucursal', index: 'sucursal', width: 150, align: 'center', hidden: false, editable: true, edittype: 'select', editrules: {required: true},
                    editoptions: {
                        value: function () {
                            //  alert("note diste cuena:" + banco_);
                        }, //cargarSucursal(banco_),
                        style: "width: 150px !important",
                        dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        var rowid = e.target.id.replace("_sucursal", "");
                                        // jQuery("#tabla_factura").jqGrid('setCell', rowid, 'idsucursal', e.target.value);
                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }},
                {name: 'autorizador', index: 'autorizador', width: 150, align: 'center', hidden: false, editable: true, edittype: 'select', editrules: {required: true},
                    editoptions: {
                        value: cargarAutorizador(),
                        style: "width: 140px !important",
                        dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        var rowid = e.target.id.replace("_autorizador", "");
                                        // jQuery("#tabla_factura").jqGrid('setCell', rowid, 'idautorizador', e.target.value);
                                    } catch (exc) {
                                    }
                                    return;
                                }
                            }, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }
                            }]
                    }},
                {name: 'concepto', index: 'concepto', width: 280, align: 'left', hidden: false, editable: true, classes: ''},
                {name: 'valor', index: 'valor', sortable: true, width: 110, align: 'right', search: true, editable: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'num_factura', index: 'num_factura', width: 80, align: 'center', hidden: false, editable: false, classes: ''},
                {name: 'valor_factura', index: 'valor_factura', sortable: true, width: 110, align: 'right', search: true, editable: false, hidden: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'num_cxp', index: 'num_cxp', width: 80, align: 'center', hidden: false, editable: false, classes: ''},
                {name: 'valor_cxp', index: 'valor_cxp', sortable: true, width: 110, align: 'right', search: true, editable: false, hidden: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'legalizado', index: 'legalizado', width: 80, align: 'center', hidden: false, editable: false, classes: ''},
                {name: 'valor_legalizado', index: 'valor_legalizado', sortable: true, width: 110, align: 'right', search: true, editable: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cxp_gasto', index: 'cxp_gasto', width: 80, align: 'center', hidden: false, editable: false, classes: ''},
                {name: 'valor_cxp_gasto', index: 'valor_cxp_gasto', sortable: true, width: 110, align: 'right', search: true, editable: false, hidden: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'legalizar', index: 'legalizar', width: 80, align: 'left', hidden: false},
                {name: 'cambio', index: 'cambio', width: 80, sortable: true, align: 'left', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: false,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager4',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            editurl: 'clientArray',
            subGrid: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {

            },
            gridComplete: function () {
                var ids = jQuery("#tabla_anticipo").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var legalizado = $("#tabla_anticipo").getRowData(cl).legalizado;
                    var num_cxp = $("#tabla_anticipo").getRowData(cl).num_cxp;
                    var cambioEstado = $("#tabla_anticipo").getRowData(cl).cambio;
                    if (legalizado === 'S' || num_cxp === '') {
                        le = "<img src='/fintra/images/botones/iconos/legalizar.png' style='height:20px;width:24px;margin-left: 30px;' type='button' id='le" + cl + "' title ='Legalizar' ";
                    }
                    else if (legalizado === 'N') {
                        le = "<img src='/fintra/images/botones/iconos/legalizado.png' style='height:20px;width:24px;margin-left: 30px;' type='button' id='le" + cl + "' title ='Legalizado' onclick=\"ventanaLegalizar('" + cl + "');\" />";
                        an = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"anularAnticipo('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                        jQuery("#tabla_anticipo").jqGrid('setRowData', ids[i], {cambio: an});
                    }
                    be = "<img src='/fintra/images/botones/iconos/search.png' style='height:16px;width:21px;margin-left: 30px;' type='button' id='be" + cl + "' title ='Buscar empleado' onclick=\"ventanaProveedor('" + cl + "','tabla_anticipo');\" />";
                    jQuery("#tabla_anticipo").jqGrid('setRowData', ids[i], {buscar_empleado: be});
                    jQuery("#tabla_anticipo").jqGrid('setRowData', ids[i], {legalizar: le});

                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var valordoc = 'neo_' + ((grid_tabla.getRowData().length) - 1);
                var id = $("#tabla_anticipo").getRowData(rowid).id;
                //alert('valordoc: ' + valordoc + 'id: ' + id);
                if (id === valordoc) {
                    jQuery("#tabla_anticipo").jqGrid('editRow', rowid, true, function () {
                    }, null, null, {}, function (rowid) {

                    });
                    return;
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 25,
                    fechaini: $('#fechaini').val(),
                    fechafin: $('#fechafin').val(),
                    empleado: $('#empleado').val(),
                    anticipo: $('#anticipo').val()
                }
            },
            loadError: function (xhr, status, error) {
                // mensajesDelSistema(error, 250, 150);
            }

        }).navGrid("#pager4", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });

        jQuery("#tabla_anticipo").navButtonAdd('#pager4', {
            caption: "Agregar",
            title: "Agregar Anticipo",
            onClickButton: function () {
                var info = grid_tabla.getGridParam('records');
                var f = new Date();
                var fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
                var grid = $("#tabla_anticipo");
                var rowid = 'neo_' + grid.getRowData().length;
                var defaultData = {valor: 0, fecha: fecha, legalizado: 'NO', num_factura: '', num_cxp: '', valor_legalizado: 0};
                grid.addRowData(rowid, defaultData);
                grid.jqGrid('editRow', rowid, true, function () {
                }, null, null, {}, function (rowid) {
                    var valor = $("#tabla_anticipo").getRowData(rowid).valor;

                });
            }
        });
    }
    if (true) {
        $("#tabla_anticipo").setGridParam({
            subGridRowExpanded: function (subgrid_id, row_id) {
                var pager_id;
                myrow_id = row_id;
                subgrid_table_id = subgrid_id + "_t";
                pager_id = "p_" + subgrid_table_id;
                $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                jQuery("#" + subgrid_table_id).jqGrid({
                    url: "./controller?estado=Admin&accion=Fintra&idanticipo=" + myrow_id,
                    datatype: "json",
                    colNames: ['Tipo Documento', 'Documento', 'Descripcion', 'HC', 'Valor', 'Abonos', 'Saldo'],
                    colModel: [
                        {name: "tipo_documento", index: "tipo_documento", width: 90, align: "center", sortable: false},
                        {name: "documento", index: "documento", width: 80, align: "center", sortable: false, key: true},
                        {name: "descripcion", index: "descripcion", width: 280, align: "left", sortable: false},
                        {name: "hc", index: "hc", width: 80, align: "center", sortable: false},
                        {name: 'vlr_neto', index: 'vlr_neto', sortable: true, width: 95, align: 'right', search: true, sorttype: 'currency',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                        {name: 'vlr_total_abonos', index: 'vlr_total_abonos', sortable: true, width: 95, align: 'right', search: true, sorttype: 'currency',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                        {name: 'vlr_saldo', index: 'vlr_saldo', sortable: true, width: 95, align: 'right', search: true, sorttype: 'currency',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}}

                    ],
                    rowNum: 28,
                    //pager: pager_id,
                    sortname: 'num',
                    width: '1000',
                    height: '150',
                    pgtext: null,
                    pgbuttons: false,
                    subGrid: true,
                    jsonReader: {
                        root: "rows",
                        repeatitems: false,
                        id: "0"
                    },
                    ajaxGridOptions: {
                        data: {
                            opcion: 28
                        }
                    }, gridComplete: function () {

                    }, subGridRowExpanded: function (subgrid_id, row_id) {
                        var pager_id;
                        myrow_id = row_id;
                        subgrid_table_id = subgrid_id + "_c";

                        var tablaSuggrid2 = (subgrid_table_id.replace('_' + row_id + '_c', ''));
                        var table2 = jQuery('#' + tablaSuggrid2);
                        var tipo_documento = table2.getRowData(row_id).tipo_documento;
                        var documento = table2.getRowData(row_id).documento;
                        if (tipo_documento === 'FAC') {
                            pager_id = "p_" + subgrid_table_id;
                            $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                            jQuery("#" + subgrid_table_id).jqGrid({
                                url: "./controller?estado=Admin&accion=Fintra&factura=" + myrow_id,
                                datatype: "json",
                                colNames: ['Tipo Documento', 'Documento', 'Descripcion', 'HC', 'Valor'],
                                colModel: [
                                    {name: "tipo_documento", index: "tipo_documento", width: 90, align: "center", sortable: false},
                                    {name: "documento", index: "documento", width: 80, align: "center", sortable: false, key: true},
                                    {name: "descripcion", index: "descripcion", width: 280, align: "left", sortable: false},
                                    {name: "hc", index: "hc", width: 80, align: "center", sortable: false},
                                    {name: 'vlr_neto', index: 'vlr_neto', sortable: true, width: 95, align: 'right', search: true, sorttype: 'currency',
                                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}}
                                ],
                                rowNum: 29,
                                //pager: pager_id,
                                sortname: 'num',
                                width: '950',
                                height: '80',
                                pgtext: null,
                                pgbuttons: false,
                                jsonReader: {
                                    root: "rows",
                                    repeatitems: false,
                                    id: "0"
                                },
                                ajaxGridOptions: {
                                    data: {
                                        opcion: 29
                                    }
                                }, gridComplete: function () {

                                }
                            });
                        } else if (tipo_documento === 'FAP' && documento.substring(0, 3) === 'GCM') {
                            pager_id = "p_" + subgrid_table_id;
                            $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                            jQuery("#" + subgrid_table_id).jqGrid({
                                url: "./controller?estado=Admin&accion=Fintra&cxp=" + myrow_id,
                                datatype: "json",
                                colNames: ['Tipo Documento', 'Documento', 'Descripcion', 'HC', 'Valor'],
                                colModel: [
                                    {name: "tipo_documento", index: "tipo_documento", width: 90, align: "center", sortable: false},
                                    {name: "documento", index: "documento", width: 80, align: "center", sortable: false, key: true},
                                    {name: "descripcion", index: "descripcion", width: 280, align: "left", sortable: false},
                                    {name: "hc", index: "hc", width: 80, align: "center", sortable: false},
                                    {name: 'vlr_neto', index: 'vlr_neto', sortable: true, width: 95, align: 'right', search: true, sorttype: 'currency',
                                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}}
                                ],
                                rowNum: 30,
                                //pager: pager_id,
                                sortname: 'num',
                                width: '950',
                                height: '80',
                                pgtext: null,
                                pgbuttons: false,
                                jsonReader: {
                                    root: "rows",
                                    repeatitems: false,
                                    id: "0"
                                },
                                ajaxGridOptions: {
                                    data: {
                                        opcion: 30
                                    }
                                }, gridComplete: function () {

                                }
                            });
                        }
                    }
                });
            }
        });
    }
}

function reloadGridtablaAnticiposCajaMenor(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                fechaini: $('#fechaini').val(),
                fechafin: $('#fechafin').val(),
                empleado: $('#empleado').val(),
                anticipo: $('#anticipo').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function guardarAnticipoCajaMenor() {
    var grid = jQuery("#tabla_anticipo"), filas = grid.jqGrid('getDataIDs');
    var data = jQuery("#tabla_anticipo").jqGrid('getRowData');
    for (var i = 0; i < filas.length; i++) {
        console.log(filas[i]);
        grid.saveRow(filas[i]);
    }
    if (data.length === 0) {
        mensajesDelSistema('Inserte infomacion', '300', '100', false);
    } else {
        $.ajax({
            async: false,
            url: "./controller?estado=Admin&accion=Fintra",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 24,
                informacion: JSON.stringify({anticipos: data})
            },
            success: function (json) {
                if (json.respuesta === 'SI') {
                    cargarAnticipoCajaMenor();
                    mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);

                } else {
                    mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function ventanaLegalizar(id) {
    $('.ui-dialog-buttonpane button:contains("Legalizar")').button().show();
    var empleado = $("#tabla_anticipo").getRowData(id).empleado;
    var identificacion = $("#tabla_anticipo").getRowData(id).identificacion;
    var cod_anticipo = $("#tabla_anticipo").getRowData(id).cod_anticipo;
    var num_factura = $("#tabla_anticipo").getRowData(id).num_factura;
    var valor_factura = $("#tabla_anticipo").getRowData(id).valor_factura;
    var autorizador = $("#tabla_anticipo").getRowData(id).autorizador;

    $('#autorozador_').val(autorizador);
    $('#empleado_').val(empleado);
    $('#identificacion_').val(identificacion);
    $('#anticipo_').val(cod_anticipo);
    $('#num_factura').val(num_factura);
    $('#valor_factura').val(numberConComas(valor_factura));
    //buscarCXPGastos(cod_anticipo);
    var documento = buscarCXPGastos(cod_anticipo);
    //cargarFaturas(documento);
    setTimeout(function () {
        cargarFaturaDetalle(documento);
    }, 500);
    $("#dialogMsjLegalizar").dialog({
        width: '1541',
        height: '760',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Legalizar',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Legalizar": function () {
                var valor_legalizar = $('#valor_legalizar').val();
                if (parseFloat(valor_legalizar) !== 0 && valor_legalizar !== '' && parseFloat(valor_legalizar) !== 00) {
                    ConfirmarLegalizacion('�Esta seguro de reallizar la legalizacion por el monto digitado?', '300', 'auto', false);
                } else {
                    mensajesDelSistema('Digitar valor a legalizar', '300', 'auto', false);
                }
            },
            "Salir": function () {
                $(this).dialog("close");
                cargarFaturaDetalle('');
                cargarAnticipoCajaMenor();
            }
        }
    });
}

function calcularTotal() {
    var valorCXP = numberSinComas($('#valor_cxp').val());
    var valorLegalizar = numberSinComas($('#valor_legalizar').val());
    var valor_total = parseFloat(valorCXP) - parseFloat(valorLegalizar);
    $('#total_').val(numberConComas(valor_total));
    if (valor_total < 0) {
        document.getElementById("total_").style.color = '#f60606';
    } else if (valor_total > 0) {
        document.getElementById("total_").style.color = '#050404';
    }
    //alert('valorLegalizar: ' + valorLegalizar + ' valorCXP: ' + valorCXP);
}

function ConfirmarLegalizacion(msj, width, height) {
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Si": function () {
                guardarDocumentos();
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });
}

function legalizarAnticipoCajaMenor() {
    var valorCXP = numberSinComas($('#valor_cxp').val());
    var valorLegalizar = numberSinComas($('#valor_legalizar').val());
    var anticipo = $('#anticipo_').val();

    $.ajax({
        async: false,
        url: "./controller?estado=Admin&accion=Fintra",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 26,
            valorCXP: valorCXP,
            valorLegalizar: valorLegalizar,
            anticipo: anticipo
        },
        success: function (json) {
            if (json.respuesta === 'SI') {
                cargarAnticipoCajaMenor();
                mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);
            } else {
                mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function buscarCXPGastos(cod_anticipo) {
    var cxpGasto;
    $.ajax({
        async: false,
        url: "./controller?estado=Admin&accion=Fintra",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 27,
            cod_anticipo: cod_anticipo
        },
        success: function (json) {
            cxpGasto = json[0].documento;
            console.log(json[0].documento);
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return cxpGasto;
}

function anularAnticipo(rowid) {
    var grid_tabla = jQuery("#tabla_anticipo");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        data: {
            opcion: 31,
            id: id
        },
        success: function (data) {
            cargarAnticipoCajaMenor();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}