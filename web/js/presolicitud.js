let applicationCompleted = false;
let fee;
let presolicitud;
let originalCreditType;
let isRetanqueo = false;
let negocioOrigen, montoRenovacion;

const API_URL = window.location.protocol + "//" + window.location.hostname + ":8084/fintracredit/webresources";

const numberWithCommas = (x) => {
    var parts = x.toString().split(",");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    return parts.join(",");
};

window.onload = () => {
    let fpp = document.getElementById("fpp");
    calcularFecha().forEach((e, i) => {
        fpp.options[i] = new Option(e, e);
    });
    loadDepartment();

    if (window.opener) {
        presolicitud = window.opener.solicitud;
    }
    if (presolicitud) {
        loadPre_Request();
    }
    document.getElementById("dpto").addEventListener("change", () => {
        loadCities(document.getElementById("dpto").value);
    });
    document.getElementById("valors").addEventListener("change", getFee);
    document.getElementById("plazo").addEventListener("change", getFee);
    document.getElementById("compra_cartera").addEventListener("change", getFee);
    document.getElementById("email").addEventListener("change", checkEmail);
    document.getElementById("siguiente").addEventListener("click", enableForm);
    document.getElementById("solicitar").addEventListener("click", checkCredit);
    document.getElementById("identificacion").addEventListener("change", getCreditType);
    document.getElementById("valors").addEventListener("change", getCreditType);
    document.getElementById("fnac").addEventListener("blur", (e) => {
        let birthday = document.getElementById("fnac");
        let r = checkFullAge(birthday.value, new Date(Date.now()).toLocaleDateString());
        if (!r) {
            birthday.value = "";
        }
    });
    document.getElementById("fexp").addEventListener("blur", (e) => {
        let end = document.getElementById("fexp");
        let birthday = document.getElementById("fnac");
        let r = checkFullAge(birthday.value, end.value);
        if (!r) {
            end.value = "";
        }
    });
};

function getRequest() {
    return new XMLHttpRequest();
}

function setDefaultsHeaders(xhr) {
    xhr.setRequestHeader("token", TOKEN);
    xhr.setRequestHeader("Cache-Control", "no-cache");
    xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
    xhr.setRequestHeader("Access-Control-Allow-Methods", "PUT,POST");
}

function loadDepartment() {
    let select = document.getElementById("dpto");
    let xhr = getRequest();
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                let json = JSON.parse(xhr.responseText);
                console.log(json);
                if (json.status === 200) {
                    json.info.data.forEach((e, i) => {
                        select.options[i] = new Option(e.departamento, e.codigo);
                    });
                    if (presolicitud) {
                        select.value = presolicitud.departamento;
                    }
                    loadCities(select.value);
                } else {
                    mensajesDelSistema("No se pudieron cargar los Departamentos");
                }
            } else {
                mensajesDelSistema("No se pudieron cargar los Departamentos");
            }
        }
    };
    xhr.overrideMimeType("application/json");
    xhr.open("post", API_URL + "/hdc/get_department", true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    setDefaultsHeaders(xhr);
    xhr.send();
}

function loadCities(dpto) {
    let select = document.getElementById("ciudad");
    let xhr = getRequest();
    let params = {
        departamento: dpto
    };
    let json = `data=${JSON.stringify(params)}`;

    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                let json = JSON.parse(xhr.responseText);
                console.log(json);
                if (json.status === 200) {
                    json.info.data.forEach((e, i) => {
                        select.options[i] = new Option(e.ciudad, e.codigo);
                    });
                    if (presolicitud) {
                        select.value = presolicitud.ciudad;
                    }
                } else {
                    mensajesDelSistema("No se pudieron cargar las ciudades");
                }
            } else {
                mensajesDelSistema("No se pudieron cargar las ciudades");
            }
        }
    };
    xhr.overrideMimeType("application/json");
    xhr.open("post", API_URL + "/hdc/get_cities", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    setDefaultsHeaders(xhr);
    xhr.send(json);
}

function getFee() {
    applicationCompleted = false;

    let params = {};
    let xhr = getRequest();
    let city = document.getElementById("ciudad").value;
    let dpto = document.getElementById("dpto").value;
    let fpp = document.getElementById("fpp").value;
    let amount = document.getElementById("valors").value;
    let term = document.getElementById("plazo").value;
    let cp = document.getElementById("compra_cartera").value;

    params.ciudad = city;
    params.departamento = dpto;
    params.fecha_pago = fpp;
    params.monto = amount;
    params.num_cuotas = term;
    params.compra_cartera = cp;

    let json = `data=${JSON.stringify(params)}`;

    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                let json = JSON.parse(xhr.responseText);

                if (json.status === 200) {
                    fee = json.info.data.valor_cuota;
                    displayFee();
                } else {
                    mensajesDelSistema("No se pudo cargar el valor de la cuota");
                }
            } else {
                mensajesDelSistema("No se pudo cargar el valor de la cuota");
            }
        }
    };
    xhr.overrideMimeType("application/json");
    xhr.open("post", API_URL + "/hdc/calculate_value", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    setDefaultsHeaders(xhr);
    xhr.send(json);
}

function calcularFecha() {

    //var aux = replaceAll("2014-12-25", "-", "/");
    var date = new Date();
    var days = date.getDate();
    var fecha = "0099-01-01";
    var mes = date.getMonth() + 1;
    var anio = date.getFullYear();
    let opcion0, opcion1, opcion2, opcion3;


    if (days >= 1 && days <= 2) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-02";
            opcion0 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
            opcion0 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion3 = new Option(fecha, fecha);


        }

    }

    if (days > 2 && days <= 12) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-12";
            opcion0 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
            opcion0 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion2 = new Option(fecha, fecha);
            if (mes === 11) {
                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion3 = new Option(fecha, fecha);
            } else {
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion3 = new Option(fecha, fecha);
            }

        }
    }

    if (days > 12 && days <= 17) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion0 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion3 = new Option(fecha, fecha);


        } else {
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion0 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion1 = new Option(fecha, fecha);

            if (mes === 11) {

                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion3 = new Option(fecha, fecha);

            } else {

                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion2 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
                opcion3 = new Option(fecha, fecha);
            }
        }

    }

    if (days > 17 && days <= 22) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-22";
            opcion0 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-17";
            opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            opcion0 = new Option(fecha, fecha);

            if (mes === 11) {

                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion1 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-17";
                opcion3 = new Option(fecha, fecha);

            } else {

                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion1 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
                opcion2 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-17";
                opcion3 = new Option(fecha, fecha);
            }
        }

    }


    if (days > 22 && days <= 31) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion0 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-17";
            opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-22";
            opcion3 = new Option(fecha, fecha);


        } else {

            if (mes === 11) {

                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion0 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion1 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-17";
                opcion2 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-22";
                opcion3 = new Option(fecha, fecha);


            } else {

                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
                opcion0 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
                opcion1 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-17";
                opcion2 = new Option(fecha, fecha);
                fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-22";
                opcion3 = new Option(fecha, fecha);

            }

        }
    }

    return [opcion0.value, opcion1.value, opcion2.value, opcion3.value];

    // $('#diadepago').append(opcion0);
    // $('#diadepago').append(opcion1);
    // $('#diadepago').append(opcion2);
    // $('#diadepago').append(opcion3);
    //alert(opcion0+' '+opcion1+' '+opcion2+' '+opcion3)


}

function enableForm() {
    if (applicationCompleted) {
        document.getElementById("nombre").disabled = "";
        document.getElementById("apellido").disabled = "";
        document.getElementById("ti").disabled = "";
        document.getElementById("identificacion").disabled = "";
        document.getElementById("fexp").disabled = "";
        document.getElementById("fnac").disabled = "";
        document.getElementById("email").disabled = "";
        document.getElementById("celular").disabled = "";
        document.getElementById("solicitar").disabled = "";
        document.getElementById("tipo_credito").disabled = "";
    } else {
        disabledForm();
        mensajesDelSistema("Revise los datos formulario");
    }
}

function disabledForm() {
    document.getElementById("nombre").disabled = "true";
    document.getElementById("apellido").disabled = "true";
    document.getElementById("ti").disabled = "true";
    document.getElementById("identificacion").disabled = "true";
    document.getElementById("fexp").disabled = "true";
    document.getElementById("fnac").disabled = "true";
    document.getElementById("email").disabled = "true";
    document.getElementById("celular").disabled = "true";
    document.getElementById("solicitar").disabled = "true";
    document.getElementById("tipo_credito").disabled = "true";

    applicationCompleted = false;
}

function checkEmptyInputs(elements) {
    return elements.some((e) => {
        console.log(e);
        return e === "" || e === undefined;
    });
}

function checkEmail() {
    let pattern = "[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}";

    if (!this.value.match(pattern)) {
        this.value = "";
        mensajesDelSistema("La direcci�n de correo ingresada no posee el formato correcto");
    }
}

function checkApplication() {
    let city = document.getElementById("ciudad").value;
    let dpto = document.getElementById("dpto").value;
    let fpp = document.getElementById("fpp").value;
    let amount = document.getElementById("valors").value;
    let term = document.getElementById("plazo").value;

    let name = document.getElementById("nombre").value;
    let lastName = document.getElementById("apellido").value;
    let ti = document.getElementById("ti").value;
    let id = document.getElementById("identificacion").value;
    let fexp = document.getElementById("fexp").value;
    let fnac = document.getElementById("fnac").value;
    let email = document.getElementById("email").value;
    let phone = document.getElementById("celular").value;
    let comprac = document.getElementById("compra_cartera").value;
    let tipoCredito = document.getElementById("tipo_credito").value;

    if (checkEmptyInputs([fee, city, dpto, fpp, amount, term, name, lastName, ti, id, fexp, fnac, email, phone, comprac])) {
        mensajesDelSistema("Faltan campos por llenar");
        return false;
    }

    let params = {
        "producto": "03",
        "entidad": "MICROCREDITO",
        "valor_cuota": Number.parseInt(fee),
        "monto": amount,
        "num_cuotas": Number.parseInt(term),
        "fecha_pago": fpp,
        "tipo_identificacion": ti,
        "identificacion": Number.parseInt(id),
        "fecha_expedicion": fexp,
        "primer_nombre": name,
        "primer_apellido": lastName,
        "fecha_nacimiento": fnac,
        "email": email,
        "empresa": "FINTRA",
        "asesor": asesor,
        "telefono": Number.parseInt(phone),
        "ciudad": city,
        "departamento": dpto,
        "nit_empresa": "8020220161",
        "lat": "11.016615",
        "lng": "-74.831120",
        "compra_cartera": comprac,
        "tipo_credito": tipoCredito
    };

    if (isRetanqueo) {
        params.negocio_origen = negocioOrigen;
        params.monto_renovacion = montoRenovacion;
    }
    sendApplication(params);
}

function sendApplication(params) {
    let json = `data=${JSON.stringify(params)}`;

    let xhr = getRequest();

    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                let jsonResponse = JSON.parse(xhr.responseText);
                console.log(jsonResponse);
                if (jsonResponse.status === 200) {
                    switch (jsonResponse.info.data.estado_sol) {
                        case "I":
                            mensajesDelSistema("Informaci�n: " + jsonResponse.info.data.comentario);
                            break;
                        case "Z":
                            mensajesDelSistema("Informaci�n: " + jsonResponse.info.data.comentario);
                            break;
                        case "P":
                            mensajesDelSistema("Perfecto: " + jsonResponse.info.data.comentario);
                            break;
                        case "R":
                            mensajesDelSistema("Pre-Solicitud no aceptada: " + jsonResponse.info.data.comentario);
                            break;
                    }
                } else {
                    mensajesDelSistema("No se pudo registrar la solicitud");
                }
            } else {
                mensajesDelSistema("No se pudo registrar la solicitud");
            }
        }
    };
    xhr.overrideMimeType("application/json");
    xhr.open("put", API_URL + "/hdc/credit_history_fintra2", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    setDefaultsHeaders(xhr);
    xhr.send(json);
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function getCreditType() {
    let id = document.getElementById("identificacion").value;
    let monto = document.getElementById("valors").value

    if (monto !== '' && id !== '') {
        let creditType = document.getElementById("tipo_credito");
        let params = {
            identificacion: id,
            monto: monto
        };
        let json = `data=${JSON.stringify(params)}`;
        let xhr = getRequest();

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                $("#dialogLoading").dialog("destroy");
                if (xhr.status === 200) {
                    let jsonResponse = JSON.parse(xhr.responseText);

                    if (jsonResponse.status === 200 && jsonResponse.success) {
                        creditType.value = jsonResponse.data.tipo_credito;
                        originalCreditType = jsonResponse.data.tipo_credito;
                    } else {
                        mensajesDelSistema("No se pudo validar al cliente en el sistema");
                    }
                } else {
                    mensajesDelSistema("Error estableciendo en la conexi�n con el servidor");
                }
            }
        };
        xhr.overrideMimeType("application/json");
        xhr.open("post", API_URL + "/hdc/micro_credit_type", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        setDefaultsHeaders(xhr);
        xhr.send(json);
    }
}

function checkCredit() {
    let tc = document.getElementById("tipo_credito").value;
    if (tc === "REN" && originalCreditType !== tc.value) {
        mensajesDelSistema("Cr�dito no es de tipo renovaci�n. Cliente se encuentra en base de preaprobados.", 'auto', 'auto', null);
    } else if (originalCreditType === "MOR") {
        mensajesDelSistema("No se puede ingresar la solicitud, el cliente se encuentra en mora.", 'auto', 'auto', null);
    } else {
        if (tc === "REN" || tc === "PRE")
            checkRetanqueo();
        else
            checkApplication();
    }
}

function loadPre_Request() {
    fee = presolicitud.valor_cuota;
    displayFee();
    document.getElementById("dpto").value = presolicitud.departamento;
    document.getElementById("valors").value = presolicitud.monto_credito;
    document.getElementById("plazo").value = presolicitud.numero_cuotas;
    document.getElementById("compra_cartera").value = 'S';
    document.getElementById("compra_cartera").disabled = true;

    document.getElementById("nombre").value = presolicitud.primer_nombre;
    document.getElementById("apellido").value = presolicitud.primer_apellido;
    document.getElementById("ti").value = presolicitud.tipo_identificacion;
    document.getElementById("identificacion").value = presolicitud.identificacion;
    document.getElementById("fexp").value = presolicitud.fecha_expedicion;
    document.getElementById("fnac").value = presolicitud.fecha_nacimiento;
    document.getElementById("email").value = presolicitud.email;
    document.getElementById("celular").value = presolicitud.telefono;
    document.getElementById("tipo_credito").value = 'CPD';

    enableForm();
}

function displayFee() {
    let output = document.getElementById("vcuota");
    let wrapper = document.getElementById("cuota-wrapper");
    wrapper.style.display = "block";
    wrapper.style.height = "60px";

    output.value = "$" + numberWithCommas(fee);
    if (Number.parseInt(fee) > 0) {
        applicationCompleted = true;
    } else {
        disabledForm();
        mensajesDelSistema("Valor solicitado es menor al SMMLV");
    }
}

function loading(msj, width, height) {
    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });
    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function checkFullAge(start, end) {
    let startDate = new Date(start);
    let endDate = new Date(end);
    let fullAgeDate = new Date(start);
    fullAgeDate.setFullYear(startDate.getFullYear() + 18);

    if (endDate < fullAgeDate) {
        mensajesDelSistema("No cumple con mayor�a de edad.", 'auto', 'auto', null);
        return false;
    }
    return true;
}

function createCellTableElement() {
    return document.createElement("td");
}

function checkRetanqueo() {
    let params = {
        identificacion: document.getElementById("identificacion").value
    };
    let json = `data=${JSON.stringify(params)}`;

    let xhr = getRequest();
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
            $("#dialogLoading").dialog("destroy");

            if (xhr.status === 200) {
                let jsonResponse = JSON.parse(xhr.responseText);

                if (jsonResponse.status === 200 && jsonResponse.success) {
                    let tBody = document.getElementById("tNegocios").lastElementChild;

                    if (tBody.innerHTML.length > 0) {
                        tBody.innerHTML = "";
                    }

                    let mora;

                    if (jsonResponse.data.length > 0) {
                        for (let i of jsonResponse.data) {
                            let row = document.createElement("tr");
                            let td = createCellTableElement();
                            td.innerHTML = i.negocio;
                            row.appendChild(td);

                            td = createCellTableElement();
                            td.innerHTML = i.nombre_cliente;
                            row.appendChild(td);

                            td = createCellTableElement();
                            td.innerHTML = i.valor_negocio;
                            row.appendChild(td);

                            td = createCellTableElement();
                            td.innerHTML = i.monto_retanqueo;
                            row.appendChild(td);

                            let moraNegocio = i.mora === "t" ? true : false;

                            if (moraNegocio)
                                mora = true;

                            td = createCellTableElement();
                            let btnCell = createCellTableElement();

                            switch (i.politica) {
                                case "R":
                                    if (moraNegocio) {
                                        td.innerHTML = "Mora";
                                        td.style.backgroundColor = "#eb4034";
                                        btnCell.innerHTML = "";
                                    } else {
                                        td.innerHTML = "Al d�a";
                                        td.style.backgroundColor = "#32a852";
                                        btnCell.innerHTML = `<button type="button" onclick="getSettlement('${i.negocio}')" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"`
                                                + `><span class="ui-button-text">Retanquear</span></button>`;
                                    }
                                    break;
                                case "X":
                                    td.innerHTML = "En proceso";
                                    td.style.backgroundColor = "#fcba03";
                                    btnCell.innerHTML = "";
                                    break;
                                default:
                                    if (moraNegocio) {
                                        td.innerHTML = "Mora";
                                        td.style.backgroundColor = "#eb4034";
                                        btnCell.innerHTML = "";
                                    } else {
                                        td.innerHTML = "Al d�a";
                                        td.style.backgroundColor = "#32a852";
                                        btnCell.innerHTML = "";
                                    }
                                    break;
                            }
                            row.appendChild(td);
                            row.appendChild(btnCell);
                            tBody.appendChild(row);
                        }

                        if (mora) {
                            tBody.childNodes.forEach(function (e) {
                                let btnCell = e.lastElementChild;
                                if (btnCell.children.length > 0) {
                                    btnCell.innerHTML = "";
                                }
                            });
                        }

                        document.getElementById("tLiquidacion").style.display = "none";
                        displayModalWindow("tNegocios");
                    } else {
                        checkApplication();
                    }
                } else {
                    mensajesDelSistema("Error: " + jsonResponse.data.msg);
                }
            } else {
                mensajesDelSistema("Error estableciendo en la conexi�n con el servidor");
            }
        } else {
            loading("Cargando informaci�n", "auto", "auto");
        }
    };
    xhr.overrideMimeType("application/json");
    xhr.open("post", API_URL + "/hdc/validate_customer", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    setDefaultsHeaders(xhr);
    xhr.send(json);
}

function getSettlement(id) {
    let params = {
        negocio: id,
        monto: document.getElementById("valors").value,
        num_cuotas: document.getElementById("plazo").value,
        fecha_pago: document.getElementById("fpp").value,
        departamento: document.getElementById("dpto").value
    };
    let json = `data=${JSON.stringify(params)}`;

    let xhr = getRequest();
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
            $("#dialogLoading").dialog("destroy");
            if (xhr.status === 200) {
                let jsonResponse = JSON.parse(xhr.responseText);

                if (jsonResponse.status === 200 && jsonResponse.success) {
                    isRetanqueo = true;
                    negocioOrigen = id;
                    montoRenovacion = jsonResponse.data.saldo_credito_actual;

                    document.getElementById("plazo-cell").innerHTML = jsonResponse.data.num_cuota;
                    document.getElementById("fpp-cell").innerHTML = jsonResponse.data.fecha_pago;
                    document.getElementById("valors-cell").innerHTML = "$" + numberWithCommas(jsonResponse.data.monto_solicitado);
                    document.getElementById("saldo-cell").innerHTML = "$" + numberWithCommas(montoRenovacion);
                    document.getElementById("dsto-aval-cell").innerHTML = "$" + numberWithCommas(jsonResponse.data.nuevo_valor_aval);
                    document.getElementById("dsto-estudio-cell").innerHTML = "$" + numberWithCommas(jsonResponse.data.monto_estudio_credito);
                    document.getElementById("dsto-comision-cell").innerHTML = "$" + numberWithCommas(jsonResponse.data.monto_comision_desembolso);
                    document.getElementById("desembolso-cell").innerHTML = "$" + numberWithCommas(jsonResponse.data.monto_desembolso);
                    document.getElementById("valor-cuota-cell").innerHTML = "$" + numberWithCommas(jsonResponse.data.nueva_cuota_aproximada);

                    document.getElementById("tNegocios").style.display = "none";
                    displayModalWindow("tLiquidacion");
                } else {
                    mensajesDelSistema("Error: " + jsonResponse.data.msg);
                }
            } else {
                mensajesDelSistema("Error estableciendo en la conexi�n con el servidor");
            }
        } else {
            loading("Cargando informaci�n", "auto", "auto");
        }
    };
    xhr.overrideMimeType("application/json");
    xhr.open("post", API_URL + "/hdc/settlement", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    setDefaultsHeaders(xhr);
    xhr.send(json);
}

function displayModalWindow(tableId) {
    document.getElementById(tableId).style.display = "table";

    let props = {
        width: "auto",
        height: "auto",
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Cancelar": function () {
                isRetanqueo = false;
                $(this).dialog("destroy");
            }
        }
    };

    if (tableId === "tLiquidacion") {
        props.buttons["Confirmar liquidaci�n"] = function () {
            $(this).dialog("destroy");
            return checkApplication();
        }
    }

    $("#tWrapper").dialog(props);
}