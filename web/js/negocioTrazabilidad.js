var controller;
var baseurl;

function inicializar(contr, baseu) {
    controller = contr;
    baseurl = baseu;
}

function guardar() {
    var negocio = $("negocio").value;
    var form = $("form").value;
    var act = $("act").value;
    var tipoconv = $("tipoconv").value;
    var coment = "";
    var coment_stby = "";
    var causa = "";
    var concep = "";
    var campos = "";
    var ncampos = "";
    var url = "";
    var p = "";
    var perfeccionar = "";

    if (act == "REF") {
        referenciar();
    }
    else
    {

        if (act == "PERF") {
            perferccionar();
        }
        else {


            if (act == "ANA") {
                coment = $("anacomentarios").value;
                coment_stby = $("anacomentarios_stby").value;
                causa = $("anacausal").value;
                concep = $("anaconcepto").value;                
                campos = new Array("anaconcepto", "anacomentarios");
                ncampos = new Array("Recomendacion", "Comentarios");
                if(concep === 'STANDBY'){                   
                    campos.push("anacomentarios_stby");
                    ncampos.push("Comentarios StandBy");
                }                      
            }
            if (act == "DEC") {
                coment = $("deccomentarios").value;
                coment_stby = $("deccomentarios_stby").value;
                causa = $("deccausal").value;
                concep = $("decconcepto").value;
                perfeccionar = $("perfeccionar").value;
                campos = new Array("decconcepto", "deccomentarios");
                ncampos = new Array("Decision", "Comentarios");
            }
            if (act == "RAD") {
                coment = $("radcomentarios").value;
                concep = $("radconcepto").value;
                campos = new Array("radconcepto", "radcomentarios");
                ncampos = new Array("Recomendacion", "Comentarios");              
            }
            if (act == "FOR") {
                coment = $("forcomentarios").value;
                coment_stby = $("forcomentarios_stby").value;
                causa = $("forcausal").value;
                concep = $("forconcepto").value;
                campos = new Array("forconcepto", "forcomentarios");
                ncampos = new Array("Concepto", "Comentarios");
                if(concep === 'STANDBY'){                   
                    campos.push("forcomentarios_stby");
                    ncampos.push("Comentarios StandBy");
                }
            }

            for (i = 0; i < campos.length; i++) {
                campo = campos[i];
                if (document.getElementById("formulario").elements[campo].value == '') {
                    alert("El campo " + ncampos[i] + " esta vacio.!");
                    document.getElementById("formulario").elements[campo].focus();
                    return (false);
                    break;
                }
            }
            if (act == "FOR" && concep == "FORMALIZ") {
                url = controller + "?estado=Negocios&accion=Update";

                p = "&negocio=" + negocio + "&op=2" + "&form=" + form + "&act=" + act + "&coment=" + coment + "&causa=" + causa + "&concep=" + concep + "&tipoconv=" + tipoconv;
               document.getElementById('imgaceptar').style.visibility = 'hidden';
                new Ajax.Request(url,
                        {
                            parameters: p,
                            method: 'post',
                            onComplete: function (resp) {

                                var txt = resp.responseText;

                                if (txt.charAt(0) == '/') {
                                    parent.closewin();
                                    parent.location.href = baseurl + txt;
                                }
                                else {
                                    $('busqueda').innerHTML = txt;
                                }

                            }
                        });
            } else if ((act === "DEC"  || act === "FOR") && concep==="RELIQUIDAR_FINTRA" ){
                url = $('formulario').action;
                p = "opcion=RELIQUIDAR_NEGOCIOS_FINTRA&negocio=" + negocio + "&form=" + form + "&act=" + act + "&coment=" + coment + "&causa=" + causa + "&concep=" + concep + "&perfeccionar=" + perfeccionar +"&coment_stby=" + coment_stby  ;
               
                new Ajax.Request(url,
                        {
                            parameters: p,
                            method: 'post',
                            onComplete: function (resp) {

                                var txt = resp.responseText;

                                if (txt.charAt(0) == '/') {
                                    parent.closewin();
                                    parent.location.href = baseurl + txt;
                                }
                                else {
                                    $('busqueda').innerHTML = txt;
                                }

                            }
                        });
            }else if (act === "FOR" && concep==="DEVOLVER_DECISION" ){
                url = $('formulario').action;
                p = "opcion=DEVOLVER_DECISION&negocio=" + negocio + "&form=" + form + "&act=" + act + "&coment=" + coment + "&causa=" + causa + "&concep=" + concep + "&perfeccionar=" + perfeccionar +"&coment_stby=" + coment_stby  ;
               
                new Ajax.Request(url,
                        {
                            parameters: p,
                            method: 'post',
                            onComplete: function (resp) {

                                var txt = resp.responseText;

                                if (txt.charAt(0) == '/') {
                                    parent.closewin();
                                    parent.location.href = baseurl + txt;
                                }
                                else {
                                    $('busqueda').innerHTML = txt;
                                }

                            }
                        });
            } 
            else {

                url = $('formulario').action;
                p = "opcion=GRABAR_TRAZA&negocio=" + negocio + "&form=" + form + "&act=" + act + "&coment=" + coment + "&causa=" + causa + "&concep=" + concep + "&perfeccionar=" + perfeccionar +"&coment_stby=" + coment_stby  ;
               
                new Ajax.Request(url,
                        {
                            parameters: p,
                            method: 'post',
                            onComplete: function (resp) {

                                var txt = resp.responseText;

                                if (txt.charAt(0) == '/') {
                                    parent.closewin();
                                    parent.location.href = baseurl + txt;
                                }
                                else {
                                    $('busqueda').innerHTML = txt;
                                }

                            }
                        });
            }
        }
    }


}

function referenciar()
{
    if (document.getElementById("refcomentarios").value != '')
    {
       if(document.getElementById("refconcepto").value !== 'STANDBY' || (document.getElementById("refconcepto").value === 'STANDBY' && document.getElementById("refcomentarios_stby").value !== '')){
            if (document.getElementById("archivo").value != '' && !validarArchivo(document.getElementById("archivo").value))
            {
                return false;
            }
            if (document.getElementById("archivo").value != '' || document.getElementById("sw").value != null) {
                document.getElementById("formulario").action = document.getElementById("formulario").action + "&opcion=GRABAR_REFERECIA";
                document.getElementById("formulario").submit();
            }
       }else{
            alert("El campo comentarios StandBy esta vacio.!")
       }        
    } else {
        alert("El campo comentarios esta vacio.!")
    }
}

function validarExtension(archivo) {
    var ext = /.EXE$/gi;
    if (ext.test(archivo.toUpperCase())) {
        alert('Extension no valida, para el archivo de importacion');
        return false;
    }
    return true;
}

function validarNombreArchivo(archivo) {
    var fname = archivo.substring(0, archivo.lastIndexOf('.'));
    if (!fname.match(/^[0-9a-zA-Z\_\-\(\)]*$/)) {
        alert('Nombre de archivo no v�lido. Caracteres permitidos( n�meros, letras, (, ), _, - ). Por favor, Verifique.');
        return false;
    }
    return true;
}

function validarArchivo(archivo) {
    var estado = false;
    var startIndex = (archivo.indexOf('\\') >= 0 ? archivo.lastIndexOf('\\') : archivo.lastIndexOf('/'));
    var filename = archivo.substring(startIndex);
    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
        filename = filename.substring(1);
    }
    if (validarNombreArchivo(filename)) {
        estado = validarExtension(filename);
    }
    if (estado == false)
        document.getElementById("archivo").value = '';
    return estado;
}

function redi(redi, act) {
    var op = "10";
    if (act == "PERF")
    {
        op = 17;
    }
    if (redi == "S")
    {

        parent.closewin();
        parent.location.href = baseurl + "/controller?estado=Negocios&accion=Ver&op=5&vista=" + op;
    }
}


function rediPerf() {
    parent.closewin();
    parent.location.href = baseurl + "/controller?estado=Negocios&accion=Ver&op=5&vista=17";
}

function radicar(baseurl, controller, negocio, form) {
    var p = "opcion=GRABAR_TRAZA&negocio=" + negocio + "&form=" + form + "&act=RAD";
    if ($("tipoconv").value == "Multiservicio") {
        p += "&tot=" + $("tot").value + "&id_solicitud=" + $("id_solicitud").value + "&tipoconv=Multiservicio";
        for (var i = 0; i < $("tot").value; i++) {
            if ($("formulario" + i).value != "" && $("garantia" + i).value != "") {
                p += "&equipo" + i + "=" + $("equipo" + i).value + "&formulario" + i + "=" + $("formulario" + i).value + "&garantia" + i + "=" + $("garantia" + i).value;
            } else {
                alert("debe digitar el formulario y/o garantia del item " + $("tot").value);
                return false;
            }
        }
    }
    var url = controller + "?estado=Negocio&accion=Trazabilidad";
    new Ajax.Request(url,
            {
                parameters: p,
                method: 'post',
                onComplete: function (resp) {

                    var txt = resp.responseText;

                    if (txt.charAt(0) == '/') {
                        ;
                        location.href = baseurl + txt;
                    }
                    else {
                        $('busqueda').innerHTML = txt;
                    }

                }
            });

}


var win, win2;
function verTraza(baseurl, form) {
    win = new Window(
            {id: "diving",
                title: "Trazabilidad del Negocio",
                url: baseurl + "/jsp/fenalco/negocios/NegocioTrazabilidad.jsp?form=" + form,
                showEffectOptions: {duration: 1},
                hideEffectOptions: {duration: 1},
                destroyOnClose: true,
                resizable: true,
                width: 1000,
                height: 600
            });
    win.show(true);
    win.showCenter();
}

function closewin() {
    win.close();
}

function concepto(baseurl, negocio, form, act, cuota, tipoconv) {
    win = new Window(
            {id: "diving",
                title: "Concepto",
                url: baseurl + '/jsp/fenalco/negocios/Conceptos.jsp?negocio=' + negocio + "&form=" + form + "&act=" + act + "&cuota=" + cuota + "&tipoconv=" + tipoconv,
                showEffectOptions: {duration: 1},
                hideEffectOptions: {duration: 1},
                destroyOnClose: true,
                resizable: true,
                width: 1000,
                height: 600
            });
    win.show(true);
    win.showCenter();
}

function xpand_div(act) {
    if (act != "") {
        if (act == "REF") {
            $('contenido').innerHTML = $("referenciar").innerHTML;
            if(document.getElementById('refconcepto_last_traza').value==='DEV_STANDBY') $('contenido').innerHTML += $("ref_standby").innerHTML;
            $('contenido').style.border = "0.1em solid black";
            $('contenido').style.padding = "0.3em";
            $('imgaceptar').style.visibility = "visible";

        }
        if (act == "RAD") {
            $('contenido').innerHTML = $("radicar").innerHTML;
            $('contenido').style.border = "0.1em solid black";
            $('contenido').style.padding = "0.3em";
            $('imgaceptar').style.visibility = "visible";

        }
        if (act == "ANA") {
            $('contenido').innerHTML = $("capacidad").innerHTML;
            $('contenido').innerHTML += $("tipocliente").innerHTML;
            $('contenido').innerHTML += $("score").innerHTML;
            $('contenido').innerHTML += $("info_cartera").innerHTML;
            $('contenido').innerHTML += $("liquidacion").innerHTML;
            $('contenido').innerHTML += $("referenciacion").innerHTML;  
            if(document.getElementById('concepto_last_traza').value==='DEV_STANDBY') $('contenido').innerHTML += $("standby").innerHTML;
            $('contenido').innerHTML += $("analizar").innerHTML;
            $('contenido').style.border = "0.1em solid black";
            $('contenido').style.padding = "0.3em";
            $('imgaceptar').style.visibility = "visible";

        }
        if (act == "DEC") {
            $('contenido').innerHTML = $("capacidad").innerHTML;
            $('contenido').innerHTML += $("tipocliente").innerHTML;
            $('contenido').innerHTML += $("score").innerHTML;
            $('contenido').innerHTML += $("info_cartera").innerHTML;
            $('contenido').innerHTML += $("referenciacion").innerHTML; 
            $('contenido').innerHTML += $("analisis").innerHTML;
            $('contenido').innerHTML += $("decidir").innerHTML;
            $('contenido').style.border = "0.1em solid black";
            $('contenido').style.padding = "0.3em";
            $('imgaceptar').style.visibility = "visible";

        }
        if (act == "FOR") {
            $('contenido').innerHTML += $("decision").innerHTML;
            $('contenido').innerHTML += $("formalizar").innerHTML;
            $('contenido').style.border = "0.1em solid black";
            $('contenido').style.padding = "0.3em";
            $('imgaceptar').style.visibility = "visible";

        }

        if (act == "PERF") {
            $('contenido').innerHTML = $("_perfeccionar").innerHTML;
            $('contenido').style.border = "0.1em solid black";
            $('contenido').style.padding = "0.3em";
            $('imgaceptar').style.visibility = "visible";

        }

    } else {
        $('contenido').innerHTML = '';
        $('contenido').style.border = "0em solid black";
        $('contenido').style.padding = "0em";
        $('imgaceptar').style.visibility = "hidden";

    }
}

function perferccionar()
{
    if (document.getElementById("perfcomentarios").value != '')
    {
        if ((document.getElementById("archivo").value != '' && !validarArchivo(document.getElementById("archivo").value)))
        {
            return false;
        }
        if (document.getElementById("archivo").value != '' || document.getElementById("sw").value != null) {
            document.getElementById("formulario").action = document.getElementById("formulario").action + "&opcion=PERFECCIONAR";
            document.getElementById("formulario").submit();
        }
    } else {
        alert("El campo comentarios esta vacio.!")
    }
}

function calculos(gfam) {
    var gastosfam = "";
    var salario = parseInt(($("salario").value).replace(/,/g, ''));
    var honorarios = parseInt(($("honorarios").value).replace(/,/g, ''));
    var otrosingresos = parseInt(($("otrosingresos").value).replace(/,/g, ''));
    var descuento = parseInt(($("descuento").value).replace(/,/g, ''));
    var porcgfam = parseFloat($("porcgfam").value);
    var gastosarr = parseInt(($("gastosarr").value).replace(/,/g, ''));
    var cuota = parseInt(($("cuota").value).replace(/,/g, ''));
    var otrosgastos = parseInt(($("otrosgastos").value).replace(/,/g, ''));
    var cuotaneg = parseInt(($("cuotaneg").value).replace(/,/g, ''));

    var totalIngreso = salario + honorarios + otrosingresos;
    if (gfam == "S") {
        gastosfam = parseInt(($("gastosfam").value).replace(/,/g, ''));
    } else {
        gastosfam = totalIngreso * porcgfam;
    }
    var ingresoneto = totalIngreso - descuento;
    var totalegreso = cuota + gastosfam + otrosgastos + gastosarr;
    var capacidadpago = (totalIngreso - totalegreso);
    capacidadpago = Math.round(capacidadpago);
    var disponible = (totalIngreso < 1) ? 100 : Math.round(((totalegreso + cuotaneg) / totalIngreso) * 100);

    if (disponible > 90) {
        $("disponible").setStyle({color: 'red'});
    } else {
        $("disponible").setStyle({color: '#003399'});
    }

    $("totalingreso").value = dar_formato(totalIngreso);
    $("totalegreso").value = dar_formato(totalegreso);
    $("ingresoneto").value = dar_formato(ingresoneto);
    $("capacidadpago").value = dar_formato(capacidadpago);
    $("disponible").value = dar_formato(disponible);
    $("salario").value = dar_formato(salario);
    $("honorarios").value = dar_formato(honorarios);
    $("otrosingresos").value = dar_formato(otrosingresos);
    $("descuento").value = dar_formato(descuento);
    $("gastosfam").value = dar_formato(gastosfam);
    $("gastosarr").value = dar_formato(gastosarr);
    $("otrosgastos").value = dar_formato(otrosgastos);
    $("cuota").value = dar_formato(cuota);
}

function calculos2(gfam) {
    var gastosfamcod = "";
    var salariocod = parseInt(($("salariocod").value).replace(/,/g, ''));
    var honorarioscod = parseInt(($("honorarioscod").value).replace(/,/g, ''));
    var otrosingresoscod = parseInt(($("otrosingresoscod").value).replace(/,/g, ''));
    var descuentocod = parseInt(($("descuentocod").value).replace(/,/g, ''));
    var porcgfamcod = parseFloat($("porcgfamcod").value);
    var gastosarrcod = parseInt(($("gastosarrcod").value).replace(/,/g, ''));
    var cuotacod = parseInt(($("cuotacod").value).replace(/,/g, ''));
    var otrosgastoscod = parseInt(($("otrosgastoscod").value).replace(/,/g, ''));
    var cuotanegcod = parseInt(($("cuotanegcod").value).replace(/,/g, ''));

    var totalIngresocod = salariocod + honorarioscod + otrosingresoscod;
    if (gfam == "S") {
        gastosfamcod = parseInt(($("gastosfamcod").value).replace(/,/g, ''));
    } else {
        gastosfamcod = totalIngresocod * porcgfamcod;
    }
    var ingresonetocod = totalIngresocod - descuentocod;


    var totalegresocod = cuotacod + gastosfamcod + otrosgastoscod + gastosarrcod;
    var capacidadpagocod = (totalIngresocod - totalegresocod);
    capacidadpagocod = Math.round(capacidadpagocod);
    var disponiblecod = (totalIngresocod < 1) ? 100 : Math.round(((totalegresocod + cuotanegcod) / totalIngresocod) * 100);
    if (disponiblecod > 90) {
        $("disponiblecod").setStyle({color: 'red'});
    } else {
        $("disponiblecod").setStyle({color: '#003399'});
    }
    $("totalingresocod").value = dar_formato(totalIngresocod);
    $("totalegresocod").value = dar_formato(totalegresocod);
    $("ingresonetocod").value = dar_formato(ingresonetocod);
    $("capacidadpagocod").value = dar_formato(capacidadpagocod);
    $("disponiblecod").value = dar_formato(disponiblecod);
    $("salariocod").value = dar_formato(salariocod);
    $("honorarioscod").value = dar_formato(honorarioscod);
    $("otrosingresoscod").value = dar_formato(otrosingresoscod);
    $("descuentocod").value = dar_formato(descuentocod);
    $("gastosfamcod").value = dar_formato(gastosfamcod);
    $("gastosarrcod").value = dar_formato(gastosarrcod);
    $("otrosgastoscod").value = dar_formato(otrosgastoscod);
    $("cuotacod").value = dar_formato(cuotacod);
}

function soloNumeros(id) {
    var valor = document.getElementById(id).value;
    valor = valor.replace(/[^0-9^.]+/gi, "");
    document.getElementById(id).value = valor;
}

function dar_formato(num) {
    var cadena = "";
    var aux;
    var cont = 1, m, k;
    if (num < 0)
        aux = 1;
    else
        aux = 0;
    num = num.toString();
    for (m = num.length - 1; m >= 0; m--) {
        cadena = num.charAt(m) + cadena;
        if (cont % 3 == 0 && m > aux)
            cadena = "," + cadena;
        else
            cadena = cadena;
        if (cont == 3)
            cont = 1;
        else
            cont++;
    }

    return cadena;

}
function cargarlista(id_valor, id, id_div, tabla) {
    var valor = $(id_valor).value;

    var url = controller + "?estado=Negocio&accion=Trazabilidad";
    var p = "opcion=CARGARLISTA&valor=" + valor + "&id=" + id + "&tabla=" + tabla;
    new Ajax.Request(url, {
        parameters: p,
        method: 'post',
        onComplete: function (resp) {
            document.getElementById(id_div).innerHTML = resp.responseText;
        }
    });


}


function verTrazaRechazado(baseurl, form) {
    win = new Window(
            {id: "diving",
                title: "Causales de Rechazo",
                url: baseurl + "/jsp/fenalco/negocios/causales.jsp?form=" + form,
                showEffectOptions: {duration: 1},
                hideEffectOptions: {duration: 1},
                destroyOnClose: true,
                resizable: true,
                width: 1000,
                height: 600
            });
    win.show(true);
    win.showCenter();
}

var win;
function detalle_mod_form(baseurl, neg) {
    win = new Window(
            {className: "dialog",
                width: 400,
                height: 300,
                zIndex: 100,
                resizable: true,
                title: "Detalles Modificaciones",
                showEffect: Effect.BlindDown,
                hideEffect: Effect.SwitchOff,
                draggable: true,
                wiredDrag: true,
                url: baseurl + "/jsp/fenalco/negocios/detalles_mod_form.jsp?negocio=" + neg,
                destroyOnClose: true
            });
    win.show(true);
    win.showCenter();
}


function asociar_Negocio(baseurl, id_valor, id_conv, nit_cliente) {
    var valor = $(id_valor).value;
    if (valor === 'FORMALIZ' && validar_convenio(id_conv)) {
        mensajeConfAsociac(baseurl, nit_cliente);
    }
}

/* Valida que el convenio que se envia como par�metro no corresponda a un
 *  microcr�dito o sea de tipo educativo*/
function validar_convenio(id_convenio) {
    var IsValidConv = true;
    if (id_convenio === 16 || id_convenio === 17 || id_convenio === 30 || id_convenio === 31 || id_convenio === 37) {
        IsValidConv = false;
    } else {
        if (id_convenio > 9 && id_convenio < 14) {
            IsValidConv = false;
        }
    }
    return IsValidConv;
}

function ventanaAsociacion(baseurl) {
    mostrarContenido('dialogo');
    $j("#dialogo").dialog({
        width: 785,
        height: 450,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear botones asociar y salir
            "Asociar": function () {
                actualizarNegocioRel(baseurl);
            },
            "Salir": function () {
                $j(this).dialog("close");
            }
        }
    });
}


function mensajeConfAsociac(baseurl, cliente) {
    mostrarContenido('dialogMsg');
    $j("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> Desea relacionar este negocio?");
    $j("#dialogMsg").dialog({
        width: 280,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Si": function () {
                ventanaAsociacion(baseurl);
                listarNegociosRel(baseurl, cliente);
                $j(this).dialog("close");
            },
            "No": function () {
                $j(this).dialog("close");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    mostrarContenido('dialogMsg');
    if (swHideDialog) {
        $j("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $j("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $j("#dialogMsg").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $j(this).dialog("close");
                if (typeof swHideDialog !== 'undefined') {
                    $j('#dialogo').dialog("close");
                }
            }
        }
    });
}


function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function listarNegociosRel(baseurl, cliente) {
    var grid_listar_negocios_rel = $j("#tabla_negocios_rel");
    var negocio = document.getElementById('negocio').value;
    if ($j("#gview_tabla_negocios_rel").length) {
        reloadGridListarNegociosRel(baseurl, grid_listar_negocios_rel, cliente, negocio);
    } else {

        grid_listar_negocios_rel.jqGrid({
            caption: "Lista de negocios para asociar",
            url: baseurl + "/controller?estado=Negocio&accion=Trazabilidad&opcion=BUSCAR_NEGOCIOS_REL&cliente=" + cliente + "&negocio=" + negocio,
            mtype: "POST",
            datatype: "json",
            height: $j("#gview_tabla_negocios_rel").height(),
            width: '750',
            colNames: ['Neg Asociado', 'Id Cliente', 'Fecha Negocio', 'Tipo Negocio', 'Valor Negocio', 'Valor Saldo', 'Estado'],
            colModel: [
                {name: 'negasoc', index: 'negasoc', width: 80, sortable: true, align: 'center', key: true},
                {name: 'cedula', index: 'cedula', width: 100, align: 'center'},
                {name: 'fecha_negocio', index: 'fecha_negocio', sortable: true, width: 120, align: 'center'},
                {name: 'tipo_neg', index: 'tipo_neg', width: 120, align: 'center'},
                {name: 'vr_negocio', index: 'vr_negocio', width: 90, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_saldo', index: 'valor_saldo', width: 90, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'estado', index: 'estado', sortable: true, width: 90, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            //pager: '#page_tabla_negocios_rel',
            multiselect: true,
            multiboxonly: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, onSelectRow: function (id) {
                var selectedrows = $j("#tabla_negocios_rel").jqGrid('getGridParam', 'selrow');
                if (selectedrows.length) {
                    var i;
                    for (i = 0; i < selectedrows.length; i++) {
                        if (id !== selectedrows[i]) {
                            $j("#tabla_negocios_rel").jqGrid('setSelection', selectedrows[i], false);
                        }
                    }
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
        //$j("#tabla_negocios_rel").jqGrid('navGrid','#page_tabla_negocios_rel',{add:false,del:false,edit:false,position:'top'});
        $j("#cb_" + grid_listar_negocios_rel[0].id).hide();

    }

}


function reloadGridListarNegociosRel(baseurl, grid_listar_negocios_rel, cliente, negocio) {
    grid_listar_negocios_rel.setGridParam({
        datatype: 'json',
        url: baseurl + "/controller?estado=Negocio&accion=Trazabilidad&opcion=BUSCAR_NEGOCIOS_REL&cliente=" + cliente + "&negocio=" + negocio
    });
    grid_listar_negocios_rel.trigger("reloadGrid");
}

function actualizarNegocioRel(baseurl) {
    var selectedrows = $j("#tabla_negocios_rel").jqGrid('getGridParam', 'selarrrow');
    if (selectedrows.length) {
        var cod_negocio = document.getElementById('negocio').value;
        var tipo = $j("input[name=tipo]:checked").val();
        var cod_neg_rel = selectedrows[0];
        $j.ajax({
            async: false,
            url: baseurl + "/controller?estado=Negocio&accion=Trazabilidad",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 'ACTUALIZAR_NEGOCIO_REL',
                cod_neg: cod_negocio,
                neg_rel: cod_neg_rel,
                neg_type: tipo
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        mensajesDelSistema("Se ha realizado la asociaci�n solicitada.", '250', '150', true);
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci�n!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema('No ha seleccionado ning�n negocio para asociar', 250, 150);
    }
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

//metodos para las garantias. 
function abrirGrarantias(codNegocio,identificacion, nombre) {
    limpiarFasecolda();
    j("#negocio").val(codNegocio);
    j("#identificacion_cliente").val(identificacion);
    j("#nombre_cliente").val(nombre);
    cargarComboxOption(5, "seguro", "seguro");
    cargarComboxOption(4, identificacion, identificacion);
    
    j("#divFormularioGarantias").dialog({
        width: '1100',
        height: '550',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Salir": function () {
                j(this).dialog("destroy");
            },
            "Ver garantias": function () {
               verGarantiasAutomotor(codNegocio);
            },
            "Agregar": function () {
               agregarGarantiasAutomotor();
            }

        }
    });

}

function cargarComboxOption(option, parametro1, parametro2) {

    var elemento;
    var marca_vehiculo;
    switch (option) {
        case 3:
            elemento = "clase";
            j("#referencia1").empty();
            j("#referencia2").empty();
            j("#referencia3").empty();
            jQuery("#infoFasecolda").jqGrid("clearGridData", true);
            break;
        case 4: 
            elemento="neg_poliza";
            break;
        case 5:
            elemento = "aseguradora";
            parametro1="aseguradora";
            parametro2="aseguradora";
            break;
        case 6:
            
            elemento = "referencia1";
            var clase = document.getElementById("clase");
            parametro1 = clase.options[clase.selectedIndex].value;
            parametro2 = clase.options[clase.selectedIndex].text;
            j("#referencia2").empty();
            j("#referencia3").empty();
            jQuery("#infoFasecolda").jqGrid("clearGridData", true);
            break;
        case 7:            
            elemento = "referencia2";
            var referencia1 = document.getElementById("referencia1");
            parametro1 = referencia1.options[referencia1.selectedIndex].value;
            parametro2 = j("#clase option:selected").text();
            marca_vehiculo= j("#marca").val();
            j("#referencia3").empty();
            jQuery("#infoFasecolda").jqGrid("clearGridData", true);
            break;
        case 8:
            elemento = "referencia3";
            var referencia2 = document.getElementById("referencia2");
            parametro1 = referencia2.options[referencia2.selectedIndex].value;
            var referencia1 = document.getElementById("referencia1");
            parametro2 = referencia1.options[referencia1.selectedIndex].value;
            jQuery("#infoFasecolda").jqGrid("clearGridData", true);
            break;
    }
   
    j("#codigo_fasecolda").val("");
    j("#valor_fasecolda").val("");
    j("#pais").val("");
    j("#servicio").val("");

    if ( parametro1 !== '' && parametro2 !== '') {
   
    j.ajax({
        async: false,
        url: "./controller?estado=Garantias&accion=Creditos",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 3,
            option: option,
            parametro1: parametro1,
            parametro2: parametro2,
            marca:marca_vehiculo
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {
                try {
                    j('#' + elemento).empty();

                    for (var key = 0; key <= json.length; key++) {
                        j('#' + elemento).append('<option value=' + json[key].id + '>' + json[key].value + '</option>');
                    }


                } catch (exception) {
                    mensajesDelSistemaFasecolda('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                mensajesDelSistemaFasecolda("Lo sentimos no se encontraron resultados!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    }
}


function listarInfoFasecolda() {

    //parametros... 
    var marca = document.getElementById("marca").value;

    var clase = document.getElementById("clase");
    var parametro1 = clase.options[clase.selectedIndex].text;

    var referencia1 = document.getElementById("referencia1");
    var parametro2 = referencia1.options[referencia1.selectedIndex].text;

    var referencia2 = document.getElementById("referencia2");
    var parametro3 = referencia2.options[referencia2.selectedIndex].text;

    var referencia3 = document.getElementById("referencia3");
    var parametro4 = referencia3.options[referencia3.selectedIndex].text;
    
//    var servicio = document.getElementById("servicio");
//    var parametro5 = servicio.options[servicio.selectedIndex].text;



    if (marca !== '' && clase !== 'Seleccionar' && parametro1 !== 'Seleccionar' && parametro2 !== 'Seleccionar' && parametro3 !== 'Seleccionar' && parametro4 !== 'Seleccionar') {
        var grid_fasecolda = j("#infoFasecolda");

        if (j("#gview_infoFasecolda").length) {
            reloadGridFasecolda(grid_fasecolda, marca, parametro1, parametro2, parametro3, parametro4);
        } else {

            grid_fasecolda.jqGrid({
                caption: "Informacion fasecolda",
                url: "./controller?estado=Garantias&accion=Creditos",
                mtype: "POST",
                datatype: "json",
                height: '135',
                width: '1080',
                colNames: ['Estado','Codigo Fasecolda', 'Marca', 'Clase', 'Servicio', 'Referencia 1', 'Referencia 2', 'Referencia 3',
                    '1970', '1971', '1972', '1973', '1974', '1975', '1976', '1977', '1978', '1979', '1980', '1981', '1982',
                    '1983', '1984', '1985', '1986', '1987', '1988', '1989', '1990', '1991', '1992', '1993', '1994', '1995',
                    '1996', '1997', '1998', '1999', '2000', '2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008',
                    '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020', '2021',
                    '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030', 'Bcpp'
                            , 'Nacionalidad', 'Potencia', 'Tipo caja', 'Cilindraje', 'Pasajeros', 'Cap.Carga', 'Puerta', 'Aire', 'Ejes',
                    'Combustible', 'Transmision'],
                colModel: [
                    {name: 'estado', index: 'estado', width: 100, sortable: true, align: 'center'},
                    {name: 'codigo', index: 'codigo', width: 100, sortable: true, align: 'center', key: true},
                    {name: 'marca', index: 'marca', width: 100, align: 'center'},
                    {name: 'clase', index: 'clase', sortable: true, width: 120, align: 'center'},
                    {name: 'servicio', index: 'servicio', width: 120, align: 'center'},
                    {name: 'ref1', index: 'ref1', width: 120, align: 'center'},
                    {name: 'ref2', index: 'ref2', width: 120, align: 'center'},
                    {name: 'ref3', index: 'ref3', width: 120, align: 'center'},
                    {name: 'periodo_1970', index: 'periodo_1970', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1971', index: 'periodo_1971', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1972', index: 'periodo_1972', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1973', index: 'periodo_1973', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1974', index: 'periodo_1974', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1975', index: 'periodo_1975', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1976', index: 'periodo_1976', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1977', index: 'periodo_1977', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1978', index: 'periodo_1978', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1979', index: 'periodo_1979', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1980', index: 'periodo_1980', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1981', index: 'periodo_1981', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1982', index: 'periodo_1982', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1983', index: 'periodo_1983', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1984', index: 'periodo_1984', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1985', index: 'periodo_1985', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1986', index: 'periodo_1986', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1987', index: 'periodo_1987', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1988', index: 'periodo_1988', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1989', index: 'periodo_1989', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1990', index: 'periodo_1990', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1991', index: 'periodo_1991', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1992', index: 'periodo_1992', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1993', index: 'periodo_1993', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1994', index: 'periodo_1994', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1995', index: 'periodo_1995', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1996', index: 'periodo_1996', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1997', index: 'periodo_1997', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1998', index: 'periodo_1998', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_1999', index: 'periodo_1999', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2000', index: 'periodo_2000', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2001', index: 'periodo_2001', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2002', index: 'periodo_2002', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2003', index: 'periodo_2003', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2004', index: 'periodo_2004', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2005', index: 'periodo_2005', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2006', index: 'periodo_2006', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2007', index: 'periodo_2007', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2008', index: 'periodo_2008', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2009', index: 'periodo_2009', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2010', index: 'periodo_2010', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2011', index: 'periodo_2011', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2012', index: 'periodo_2012', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2013', index: 'periodo_2013', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2014', index: 'periodo_2014', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2015', index: 'periodo_2015', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2016', index: 'periodo_2016', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2017', index: 'periodo_2017', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2018', index: 'periodo_2018', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2019', index: 'periodo_2019', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2020', index: 'periodo_2020', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2021', index: 'periodo_2021', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2022', index: 'periodo_2022', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2023', index: 'periodo_2023', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2024', index: 'periodo_2024', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2025', index: 'periodo_2025', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2026', index: 'periodo_2026', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2027', index: 'periodo_2027', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2028', index: 'periodo_2028', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2029', index: 'periodo_2029', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'periodo_2030', index: 'periodo_2030', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'bcpp', index: 'bcpp', width: 120, align: 'center'},
                    {name: 'nacionalidad', index: 'nacionalidad', width: 120, align: 'center'},
                    {name: 'potencia', index: 'potencia', width: 120, align: 'center'},
                    {name: 'tipo_caja', index: 'tipo_caja', width: 120, align: 'center'},
                    {name: 'cilinadraje', index: 'cilinadraje', width: 120, align: 'center'},
                    {name: 'pasajeros', index: 'pasajeros', width: 120, align: 'center'},
                    {name: 'carga', index: 'carga', width: 120, align: 'center'},
                    {name: 'puertas', index: 'puertas', width: 120, align: 'center'},
                    {name: 'aire', index: 'aire', width: 120, align: 'center'},
                    {name: 'ejes', index: 'ejes', width: 120, align: 'center'},
                    {name: 'combustible', index: 'combustible', width: 120, align: 'center'},
                    {name: 'transmision', index: 'transmision', width: 120, align: 'center'}
                ],
                rowNum: 50,
                rowTotal: 1000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                pager: '#page_infoFasecolda',
                viewrecords: true,
                hidegrid: false,
                shrinkToFit: false,
                footerrow: false,
                multiselect: false
                , onCellSelect: function (rowid, icol, cellcontent, e) {
              
                        var servicio = grid_fasecolda.jqGrid("getRowData", rowid).servicio;
                        var nacionalidad = grid_fasecolda.jqGrid("getRowData", rowid).nacionalidad;
                        var opcion=parseInt(j("#modelo").val()!==''?j("#modelo").val():0);
                        j("#valor_fasecolda").val(buscarPrecioFasecolda(opcion,grid_fasecolda,rowid));
                        j("#servicio").val(servicio);
                        j("#pais").val(nacionalidad);
                        j("#codigo_fasecolda").val(rowid)
                   
                }, loadComplete: function () {

                },
                ajaxGridOptions: {
                    async: false,
                    data: {
                        opcion: 4,
                        marca: marca,
                        clase: parametro1,
                        ref1: parametro2,
                        ref2: parametro3,
                        ref3: parametro4
                        //servicio:parametro5
                    }
                },
                loadError: function (xhr, status, error) {
                    alert(error, 250, 150);
                }
            }).navGrid("#page_infoFasecolda", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        }
    } else {

        mensajesDelSistemaFasecolda("Para cargar la informacion fasecolda debe seleccionar:<br><br><br>\n\
                                   <b><li>Clase<br>\n\
                                    <li>Servicio<br>\n\
                                    <li>referencia 1<br>\n\
                                    <li>referencia 2<br>\n\
                                    <li>referencia 3", '350', '220');
    }

}
function reloadGridFasecolda(grid_fasecolda, marca, clase, ref1, ref2, ref3) {
    grid_fasecolda.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Garantias&accion=Creditos"
        , ajaxGridOptions: {
            async: false,
            data: {
                opcion: 4,
                marca: marca,
                clase: clase,
                ref1: ref1,
                ref2: ref2,
                ref3: ref3
                //servicio:parametro5
            }
        }
    });
    grid_fasecolda.trigger("reloadGrid");
}



function mensajesDelSistemaFasecolda(msj, width, height, swHideDialog) {


    j("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);

    j("#dialogMsg").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                j(this).dialog("close");
            }
        }
    });
}


function limpiarFasecolda() {
    j("#nr_propiedad").val("");
    j("#chasis").val("");
    j("#motor").val("");
    j("#placa").val("");
    j("#marca").val("");
    j("#nro_poliza").val("");
    j("#fe_expedicion").val("");
    j("#fe_vencimiento").val("");
    j("#neg_poliza").empty();
    j("#clase").empty();
    j("#servicio").val("");
    j("#referencia1").empty();
    j("#referencia2").empty();
    j("#referencia3").empty();
    j("#pais").val("");
    j("#modelo").val("");
    j("#codigo_fasecolda").val("");
    j("#valor_fasecolda").val("");
    jQuery("#infoFasecolda").jqGrid("clearGridData", true);
}

function agregarGarantiasAutomotor(){
   var negocio= j("#negocio").val();
   var identificacion= j("#identificacion_cliente").val();
   var nombre= j("#nombre_cliente").val();
   var nro_propiedad= j("#nr_propiedad").val();
   var chasis= j("#chasis").val();
   var motor= j("#motor").val();
   var nr_poliza=j("#nro_poliza").val();
   var fecha_exp=j("#fe_expedicion").val();
   var fecha_ven=j("#fe_vencimiento").val();
   var negocio_poliza = j("#neg_poliza option:selected").text() !== 'Seleccione' ? j("#neg_poliza option:selected").text() : j("#negocio").val();
   var placa= j("#placa").val();
   var marca= j("#marca").val();
   var clase= j("#clase option:selected").text();
   var servicio= j("#servicio").val();
   var referencia1= j("#referencia1 option:selected").text();
   var referencia2= j("#referencia2 option:selected").text();
   var referencia3= j("#referencia3 option:selected").text();
   var pais= j("#pais").val();
   var modelo= j("#modelo").val();
   var aseguradora= j("#aseguradora option:selected").text();
   var codigo_fasecolda=j("#codigo_fasecolda").val();
   var valor_fasecolda=j("#valor_fasecolda").val().replace(/\,/g,'');
    
    if(negocio !=='' && identificacion!=='' && nombre!=='' /* && nro_propiedad !==''
            && chasis !=='' && motor !=='' */ && placa !=='' && clase !=='Seleccione' && servicio !=='' 
            && referencia1 !=='Seleccione' && referencia2 !=='Seleccione' && referencia3 !=='Seleccione' && pais !==''
            && modelo !=='' && aseguradora !=='Seleccione' && codigo_fasecolda !=='' && valor_fasecolda !==''
            && nr_poliza !=='' && fecha_exp !=='' && fecha_ven !==''){
        
        j.ajax({
            async: false,
            url: "./controller?estado=Garantias&accion=Creditos",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 5,
                negocio: negocio,
                identificacion: identificacion,
                nombre: nombre,
                nro_propiedad: nro_propiedad,
                chasis: chasis,
                motor: motor,
                nr_poliza:nr_poliza,
                fecha_exp:fecha_exp,
                fecha_ven:fecha_ven,
                negocio_poliza:negocio_poliza,
                placa: placa,
                marca: marca,
                clase: clase,
                servicio: servicio,
                referencia1: referencia1,
                referencia2: referencia2,
                referencia3: referencia3,
                pais: pais,
                modelo: modelo,
                aseguradora: aseguradora,
                codigo_fasecolda: codigo_fasecolda,
                valor_fasecolda: valor_fasecolda
                
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {
                    if(json.respuesta==='OK'){
                        mensajesDelSistemaFasecolda("Se ha guardado exitosamente la informacion.", '250', '150');
                        limpiarFasecolda();
                        
                    }else{                        
                       mensajesDelSistemaFasecolda("Lo sentimos no se pudieron guardar los datos.", '250', '150');
                    }

                } else {
                    mensajesDelSistemaFasecolda("El servidor no envio una respuesta validad.", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
        
        
    }else{
        
         mensajesDelSistemaFasecolda("Por favor debe diligenciar todos los campos marcados como obligatorios", '270', '160');
    }
}


function formato(input) {
    var num =input.value.replace(/\,/g,'');
     if (!isNaN(num)) {
          num = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");   
          input.value = num;
     }else{
         mensajesDelSistemaFasecolda('Solo se permiten numeros',250,150); 
         input.value =0;
     }
    
}

var lastsel;
function verGarantiasAutomotor(negocio){
    
     var editCell = false;
     var grid_fasecolda_lista = j("#lista_garantias_automotor");
     
        if (j("#gview_lista_garantias_automotor").length) {
            reloadGridFasecoldaLista(grid_fasecolda_lista,negocio);
        } else {

            grid_fasecolda_lista.jqGrid({
                caption: "Lista Garantias",
                url: "./controller?estado=Garantias&accion=Creditos",
                editurl :"./controller?estado=Garantias&accion=Creditos&opcion=7",
                mtype: "POST",
                datatype: "json",
                height: '200',
                width: '1300',
                colNames: ['Id','Estado','Negocio','Nit cliente','Nombre','Tarjeta propiedad','Nro chasis','Nro motor',
                            'Nro Poliza','Fecha expedicion','Fecha Vencimiento','Neg Poliza','Cod fasecolda',
                            'Modelo','Placa','Marca','Clase','Servicio','Referencia 1','Referencia 2','Referencia 3','Pais','Aseguradora','Valor Fasecolda'],
                colModel: [
                            {name: 'id', index: 'id', width: 100, sortable: true, align: 'center', key: true, hidden: true},
                            {name: 'reg_status', index: 'reg_status', width: 100, sortable: true, align: 'center',editable: true,edittype:"checkbox",editoptions: {value:"Activo:Inactivo"}},
                            {name: 'cod_neg', index: 'cod_neg', width: 100, sortable: true, align: 'center'},
                            {name: 'nit_cliente', index: 'nit_cliente', width: 100, align: 'center'},
                            {name: 'nombre_cliente', index: 'nombre_cliente', sortable: true, width: 120, align: 'center'},
                            {name: 'tarjeta_propiedad', index: 'tarjeta_propiedad', width: 120, align: 'center', editable: true},
                            {name: 'nro_chasis', index: 'nro_chasis', width: 120, align: 'center', editable: true},
                            {name: 'nro_motor', index: 'nro_motor', width: 120, align: 'center', editable: true},
                            {name: 'nro_poliza', index: 'nro_poliza', width: 120, align: 'center', editable: true},
                            {name: 'fecha_expedicion', index: 'fecha_expedicion', width: 120, align: 'center', editable: true},
                            {name: 'fecha_vencimineto', index: 'fecha_vencimineto', width: 120, align: 'center', editable: true},
                            {name: 'negocio_poliza', index: 'negocio_poliza', width: 120, align: 'center', editable: true},
                            {name: 'codigo_fasecolda', index: 'codigo_fasecolda', width: 120, align: 'center', editable: true},
                            {name: 'modelo', index: 'modelo', width: 120, align: 'center', editable: true},
                            {name: 'placa', index: 'placa', width: 120, align: 'center', editable: true},
                            {name: 'marca', index: 'marca', width: 120, align: 'center', editable: true},
                            {name: 'clase', index: 'clase', width: 120, align: 'center', editable: true},
                            {name: 'servicio', index: 'servicio', width: 120, align: 'center', editable: true},
                            {name: 'referencia1', index: 'referencia1', width: 120, align: 'center', editable: true},
                            {name: 'referencia2', index: 'referencia2', width: 120, align: 'center', editable: true},
                            {name: 'referencia3', index: 'referencia3', width: 120, align: 'center', editable: true},
                            {name: 'pais', index: 'pais', width: 120, align: 'center', editable: true},
                            {name: 'aseguradora', index: 'aseguradora', width: 138, align: 'center', editable: true, edittype: "select"},
                            {name: 'valor_fasecolda', index: 'valor_fasecolda', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 0, prefix: "$ "},editable: true}

                ],
                rowNum: 50,
                rowTotal: 1000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                pager: '#page_garantias_automotor',
                viewrecords: true,
                hidegrid: false,
                shrinkToFit: false,
                footerrow: false,
                multiselect: false
                ,loadComplete: function () {                    
                    if (grid_fasecolda_lista.jqGrid('getGridParam', 'records') > 0) {
                        cargarAseguradora(grid_fasecolda_lista);
                        j("#divListaGarantias").dialog({
                            width: '1350',
                            height: '380',
                            show: {effect: 'slide', duration: 500},
                            hide: {effect: 'explode', duration: 500},
                            resizable: false,
                            position: "center",
                            modal: true,
                            closeOnEscape: false,
                            buttons: {//crear bot�n de cerrar
                                "Salir": function () {
                                    j(this).dialog("close");
                                }
                            }
                        });

                    } else {
                        mensajesDelSistemaFasecolda("El negocio no tiene garantias para mostrar", '270', '160');
                    }

               },ondblClickRow: function (rowid, iRow, iCol, e) {

                            editCell = true;
                            grid_fasecolda_lista.jqGrid('restoreRow', lastsel);
                            var editparameters = getParametersFasecolda(grid_fasecolda_lista, rowid, clase);
                            grid_fasecolda_lista.jqGrid('editRow', rowid, editparameters);
                            lastsel = rowid;

                },ajaxGridOptions: {
                    async: false,
                    data: {
                        opcion:6,
                        negocio: negocio
                    }
                },
                loadError: function (xhr, status, error) {
                    alert(error, 250, 150);
                }
            }).navGrid("#page_garantias_automotor", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        }
    
    
    
}


function reloadGridFasecoldaLista(grid_fasecolda_lista, negocio) {
    grid_fasecolda_lista.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Garantias&accion=Creditos"
        , ajaxGridOptions: {
            async: false,
            data: {
                opcion:6,
                negocio: negocio
            }
        }
    });
    grid_fasecolda_lista.trigger("reloadGrid");
}

function getParametersFasecolda(grid, rowid, clase) {

    var parameters = {
        keys: true,
        mtype: "post",
        extraparam: {
            id: grid.getRowData(rowid).id,
            negocio: grid.getRowData(rowid).cod_neg
            
        },
        successfunc: function () {   
            return true;
        },
        aftersavefunc: function (rowid, json) {
            if(json.respuesta==='ERRORSQL'){
               mensajesDelSistemaFasecolda("Lo sentimos no se pudo actualizar la informacion.", '270', '160'); 
            }
        },
        restoreAfterError: true
    };

    return parameters;
}

function cargarAseguradora(grid) {
    j.ajax({
        type: "POST",
        url: "./controller?estado=Garantias&accion=Creditos",
        data :{
            opcion:8
        },
        async: true,
        dataType: "html",
        success: function(data) {
            if (data !== "") {
               grid.setColProp('aseguradora', {editoptions: {value:data+":"}});
              
            }
        }
    });   
  
}

function buscarPrecioFasecolda(opcion,grid_fasecolda,rowid){
    var valor_fasecolda;
    switch (opcion) {
        case 1970:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1970;
            break;
        case 1971:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1971;
            break;
        case 1972:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1972;
            break;
        case 1973:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1973;
            break;
        case 1974:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1974;
            break;
        case 1975:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1975;
            break;
        case 1976:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1976;
            break;
        case 1977:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1977;
            break;
        case 1978:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1978;
            break;
        case 1979:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1979;
            break;
        case 1980:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1980;
            break;
        case 1981:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1981;
            break;
        case 1982:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1982;
            break;
        case 1983:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1983;
            break;
        case 1984:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1984;
            break;
        case 1985:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1985
            break;
        case 1986:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1986;
            break;
        case 1987:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1987;
            break;
        case 1988:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1988;
            break;
        case 1989:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1989;
            break;
        case 1990:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1990;
            break;
        case 1991:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1991;
            break;
        case 1992:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1992;
            break;
        case 1993:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1993;
            break;
        case 1994:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1994;
            break;
        case 1995:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1995;
            break;
        case 1996:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1996;
            break;
        case 1997:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1997;
            break;
        case 1998:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1998;
            break;
        case 1999:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_1999;
            break;
        case 2000:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2000;
            break;
        case 2001:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2001;
            break;
        case 2002:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2002;
            break;
        case 2003:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2003;
            break;
        case 2004:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2004;
            break;
        case 2005:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2005;
            break;
        case 2006:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2006;
            break;
        case 2007:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2007;
            break;
        case 2008:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2008;
            break;
        case 2009:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2009;
            break;
        case 2010:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2010;
            break;
        case 2011:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2011;
            break;
        case 2012:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2012;
            break;
        case 2013:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2013;
            break;
        case 2014:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2014;
            break;
        case 2015:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2015;
            break;
        case 2016:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2016;
            break;
        case 2017:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2017;
            break;
        case 2018:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2018;
            break;
        case 2019:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2019;
            break;
        case 2020:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2020;
            break;
        case 2021:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2021;
            break;
        case 2022:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2022;
            break;
        case 2023:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2023;
            break;
        case 2024:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2024;
            break;
        case 2025:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2025;
            break;
        case 2026:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2026;
            break;
        case 2027:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2027;
            break;
        case 2028:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2028;
            break;
        case 2029:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2029;
            break;

        case 2030:
            valor_fasecolda=grid_fasecolda.jqGrid("getRowData", rowid).periodo_2030;
            break;
        default :
            valor_fasecolda=0;
    

    }

        return numberConComas(valor_fasecolda);
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function showHideCommentStby(id_valor, id_divComment, fieldId){
    var valor = $(id_valor).value;
    (valor==='STANDBY') ? $(id_divComment).style.display='':$(id_divComment).style.display='none';
    document.getElementById(fieldId).value = '';
}
