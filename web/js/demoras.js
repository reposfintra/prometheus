function validar1(){
        if(forma.fecha.value=='' && forma.duracion.value==''){
                alert('Ingrese la fecha inicial primero.');
                //forma.fecha.value.focus();
                return false;
        }
             
        else if(forma.fecha.value!='' && forma.duracion.value!=''){
                alert('No puede ingresar este campo.');
                //forma.fecha.value.focus();
                return false;
        }
        else
                return true;
}

function validar2(){
        if(forma.fecha.value!='' && forma.fecha2.value!=''){
                document.forma.duracion.readOnly = true;
        }                
}        

function validar3(){
        if(forma.fecha.value!='' && forma.fecha2.value!=''){
                document.forma.duracion.value = '';
                document.forma.duracion.readOnly = true;
                document.forma.duracion.className = 'textreadonly';
        }   
}

function validarFecha(){
        var fecha = parseFloat(forma.fecha.value.replace(/-/g,'').replace(/:/g,'').replace(/ /g,''));
        var fecha2 = parseFloat(forma.fecha2.value.replace(/-/g,'').replace(/:/g,'').replace(/ /g,''));

        if(fecha2 < fecha){
                return false;
        }
        else{
                return true;
        }
}

function validarFormulario(){
    for (i = 0; i < forma.elements.length; i++){
		if (forma.elements[i].value == "" && forma.elements[i].name != "observacion"){
		  alert("No se puede procesar la información. Verifique que todos lo campos esten llenos.");
		  forma.elements[i].focus();
		  return (false);
	  	}
	}
	return (true);
}

function reset(field){
        field.value = '';
}

function validarTCamposLlenos(){
	for (i = 0; i < forma.elements.length; i++){
		if (forma.elements[i].value == "" ){
		  alert("No se puede procesar la información. Verifique que todos lo campos esten llenos.");
		  forma.elements[i].focus();
		  return (false);
	  	}
	}
	return (true);
}