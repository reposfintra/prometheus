/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $('#buscar').click(function () {
        cargarAnalisisReporteProduccion();
    });

});

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function soloNumeros(e, num) {
    var keynum = window.event ? window.event.keyCode : e.which;
    //if (document.getElementById(num.id).value.indexOf('.') !== -1 && keynum === 46)
    //  return false;
    if ((keynum === 0 || keynum === 8 || keynum === 48 || keynum === 46)) {
        return true;
    }
    if (keynum >= 58) {
        return false;
    }
    return /\d/.test(String.fromCharCode(keynum));
}

function busqueda_filtro() {
    var filtro = $('#filtro_').val();
    if (filtro === 'otro') {
        $(".f_otro").css("display", "block");
        if ($('#anulado_').css('display') !== 'none') {
            $("#tablita").css("width", "914px");
            $("#tablainterna").css("width", "918px");
        } else {
            $("#tablita").css("width", "672px");
            $("#tablainterna").css("width", "677px");
        }
    } else {
        $(".f_otro").css("display", "none");
        $('#periodo_inicio').val('');
        $('#periodo_fin').val('');
        if ($('#anulado_').css('display') !== 'none') {
            $("#tablita").css("width", "566px");
            $("#tablainterna").css("width", "566px");
        } else {
            $("#tablita").css("width", "309px");
            $("#tablainterna").css("width", "274px");
        }
    }
}


function cargarAnalisisReporteProduccion() {
    //  $('#tabla_analisisReporteProduccion').jqGrid('GridUnload');
    var grid_tabla_ = jQuery("#tabla_analisisReporteProduccion");
    if ($("#gview_tabla_analisisReporteProduccion").length) {
        reloadGridMostrar(grid_tabla_, 35);
    } else {
        grid_tabla_.jqGrid({
            caption: "An�lisis Reporte Producci�n",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            width: '998',
            colNames: ['Periodo Anticipo', 'Descripcion', 'Valor', 'Valor Descuento', 'Valor Neto', 'Valor Combancaria', 'Valor Consignacion'],
            colModel: [
                {name: 'periodo_anticipo', index: 'periodo_anticipo', width: 100, align: 'center'},
                {name: 'descripcion', index: 'descripcion', width: 100, align: 'center', summaryType: function (value, name, record, newGroup) {
                        return "Total:";
                    }},
                {name: 'valor', index: 'valor', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_descuento', index: 'valor_descuento', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_neto', index: 'valor_neto', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_combancaria', index: 'valor_combancaria', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'vlr_consignacion', index: 'vlr_consignacion', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: true,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: true,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                data: {
                    opcion: 35,
                    filtro_: $('#filtro_').val(),
                    periodo_inicio: $('#periodo_inicio').val(),
                    periodo_fin: $('#periodo_fin').val(),
                    anulado: $('#anulado_').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {
                var valor = grid_tabla_.jqGrid('getCol', 'valor', false, 'sum');
                var valor_descuento = grid_tabla_.jqGrid('getCol', 'valor_descuento', false, 'sum');
                var valor_neto = grid_tabla_.jqGrid('getCol', 'valor_neto', false, 'sum');
                var valor_combancaria = grid_tabla_.jqGrid('getCol', 'valor_combancaria', false, 'sum');
                var vlr_consignacion = grid_tabla_.jqGrid('getCol', 'vlr_consignacion', false, 'sum');
                grid_tabla_.jqGrid('footerData', 'set', {descripcion: 'TOTAL', valor: valor, valor_descuento: valor_descuento, valor_neto: valor_neto, valor_combancaria: valor_combancaria, vlr_consignacion: vlr_consignacion});
            },
            gridComplete: function (index) {
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

            },groupingView: {
                groupField: ['periodo_anticipo'],
                groupSummary: [true],
                groupColumnShow: [true],
                groupCollapse: true,
                groupOrder: ['asc'],
                groupSummaryPos: ["footer"],
                groupText: ['<b>{0} </b> ']
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_analisisReporteProduccion").navButtonAdd('#pager', {
            caption: "Exportar Excel",
            onClickButton: function () {
                exportarExcel();
            }
        });
        $("#tabla_analisisReporteProduccion").navButtonAdd('#pager', {
            caption: "Ver Grafica",
            onClickButton: function () {
                mostrarGraficaAnalisisReporteProduccion();
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion, paso) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                filtro_: $('#filtro_').val(),
                periodo_inicio: $('#periodo_inicio').val(),
                periodo_fin: $('#periodo_fin').val(),
                anulado: $('#anulado_').val()
            }
        }
    });
    grid_tabla.jqGrid('groupingGroupBy', ['periodo_anticipo'], {
        groupSummary: [true],
        groupColumnShow: [true],
        groupCollapse: true,
        groupOrder: ['asc'],
        groupSummaryPos: ["footer"],
        groupText: ['<b>{0} </b> ']
    });
    grid_tabla.trigger("reloadGrid");
}

var aux = '';
arrtSetting = function (rowId, val, rawObject, cm) {
    //   var attr = rawObject.attr[cm.name], result;
    var result = '';
    if (typeof (rawObject) !== "undefined") {
        var attr = rawObject[cm.name];

        console.log('attr.rowspan: ' + attr.rowspan + ' attr.display: ' + attr.display);
        if (attr.rowspan) {
            result = ' rowspan=' + '"' + attr.rowspan + '"';
        } else if (attr.display) {
            result = ' style="display:' + attr.display + '"';
        } else {

            result = " rowspan='3'";
        }
        //empanada 
        if (aux == '') {
            aux = attr;
            result = " rowspan='3'";
        } else if (attr == aux) {
            result = "style='display:none'";
        } else {
            aux = attr;
            result = " rowspan='3'";
        }
    }
    console.log(result);
    return result;
};

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center", modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function  exportarExcel() {
    var fullData = $("#tabla_analisisReporteProduccion").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Admin&accion=Fintra",
        data: {
            listado: myJsonString,
            opcion: 36
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function  mostrarGraficaAnalisisReporteProduccion() {
    var grid = jQuery("#tabla_analisisReporteProduccion"), filas = grid.jqGrid('getDataIDs');
    //var data = jQuery("#tabla_analisisReporteProduccion").jqGrid('getRowData');
    var periodo_ = '';
    var periodos = [], valorGasolina = [], valorProntoPago = [], valorTransferencia = [],valorEfectivo = [];

    for (var i = 0; i < filas.length; i++) {
        var cl = filas[i];
        if (periodo_ !== grid.getRowData(cl).periodo_anticipo) {
            periodos.push((grid.getRowData(cl).periodo_anticipo) * 1);
            periodo_ = grid.getRowData(cl).periodo_anticipo;
        }

        if (grid.getRowData(cl).descripcion === 'GASOLINA') {
            /*
             * valores para la grafica de gasolina
             */
            valorGasolina.push(grid.getRowData(cl).valor * 1);
        }
        if (grid.getRowData(cl).descripcion === 'PRONTO PAGO') {
            /*
             * valores para la grafica de pronto pago
             */
            valorProntoPago.push(grid.getRowData(cl).valor * 1);
        }
        if (grid.getRowData(cl).descripcion === 'TRANSFERENCIA') {
            /*
             * valores para la grafica de transferencia
             */
            valorTransferencia.push(grid.getRowData(cl).valor * 1);
        }
        if (grid.getRowData(cl).descripcion === 'EFECTIVO') {
            /*
             * valores para la grafica de transferencia
             */
            valorEfectivo.push(grid.getRowData(cl).valor * 1);
        }
    }

    console.log(periodos);
    console.log(valorGasolina);

    verGraficaGasolina(periodos, valorGasolina);
    verGraficaProntoPago(periodos, valorProntoPago);
    verGraficaTransferencia(periodos, valorTransferencia);
    verGraficaEfectivo(periodos, valorEfectivo);
    $("#dialogMsjGraficaAnalisisReporteProduccion").dialog({
        width: '1309',
        height: '960',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function verGraficaGasolina(periodos, valorGasolina) {
    $('#containerGasolina').highcharts({
        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'Gasolina'
        },
        subtitle: {
           // text: 'Grafica Detallada Gasolina'
        },
        plotOptions: {
            column: {
                depth: 5
            }
        },
        xAxis: {
            title: {
                text: 'Periodo'
            },
            categories: periodos,
            labels: {
                formatter: function () {
                    return '<b>' + this.value + '</b>';
                }
            }
        },
        yAxis: {
            title: {
                text: 'Rango Valores'
            },
            labels: {
                formatter: function () {
                    return '<b> $' + numberConComas(this.value) + '</b>';
                }
            }
        },
        series: [
            {name: 'Valor', data: valorGasolina, drilldown: 'Opera'}
        ]
    });
}

function verGraficaProntoPago(periodos, valorProntoPago) {
    $('#containerProntoPago').highcharts({
        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'Pronto Pago'
        },
        subtitle: {
           // text: 'Grafica Detallada Pronto Pago'
        },
        plotOptions: {
            column: {
                depth: 250
            }
        },
        xAxis: {
            title: {
                text: 'Periodo'
            },
            categories: periodos,
            labels: {
                formatter: function () {
                    return '<b>' + this.value + '</b>';
                }
            }
        },
        yAxis: {
            title: {
                text: 'Rango Valores'
            },
            labels: {
                formatter: function () {
                    return '<b> $' + numberConComas(this.value) + '</b>';
                }
            }
        },
        series: [
            {name: 'Valor', data: valorProntoPago}
        ]
    });
}

function verGraficaTransferencia(periodos, valorTransferencia) {
    $('#containerTransferencia').highcharts({
        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'Transferencia'
        },
        subtitle: {
           // text: 'Grafica Detallada Transferencia'
        },
        plotOptions: {
            column: {
                depth: 250
            }
        },
        xAxis: {
            title: {
                text: 'Periodo'
            },
            categories: periodos,
            labels: {
                formatter: function () {
                    return '<b>' + this.value + '</b>';
                }
            }
        },
        yAxis: {
            title: {
                text: 'Rango Valores'
            },
            labels: {
                formatter: function () {
                    return '<b> $' + numberConComas(this.value) + '</b>';
                }
            }
        },
        series: [
            {name: 'Valor', data: valorTransferencia}
        ]
    });
}

function verGraficaEfectivo(periodos, valorEfectivo) {
    $('#containerEfectivo').highcharts({
        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'Efectivo'
        },
        subtitle: {
            //text: 'Grafica Detallada Efectivo'
        },
        plotOptions: {
            column: {
                depth: 250
            }
        },
        xAxis: {
            title: {
                text: 'Periodo'
            },
            categories: periodos,
            labels: {
                formatter: function () {
                    return '<b>' + this.value + '</b>';
                }
            }
        },
        yAxis: {
            title: {
                text: 'Rango Valores'
            },
            labels: {
                formatter: function () {
                    return '<b> $' + numberConComas(this.value) + '</b>';
                }
            }
        },
        series: [
            {name: 'Valor', data: valorEfectivo}
        ]
    });
}


function cargarGraficaGeneral() {
    var grid = jQuery("#tabla_analisisReporteProduccion"), filas = grid.jqGrid('getDataIDs');
    //var data = jQuery("#tabla_analisisReporteProduccion").jqGrid('getRowData');
    var periodo_ = '';
    var periodo_2 = '';
    var valorsum = 0;
    var valor_descuentosum = 0;
    var valor_netosum = 0;
    var valor_combancariasum = 0;
    var vlr_consignacionsum = 0;
    var vectorGenralvalor = [], periodos = [], valor = [], valor_descuento = [], valor_neto = [], valor_combancaria = [], vlr_consignacion = [];
    var vectorGenral = [], vectorGenral_ = [];
    var valorTotal, valor_descuentoTotal, valor_netoTotal, valor_combancariaTotal, vlr_consignacionTotal;
    var n = 0;

    for (var i = 0; i < filas.length; i++) {
        var cl = filas[i];
        if (periodo_ !== grid.getRowData(cl).periodo_anticipo) {
            periodos.push((grid.getRowData(cl).periodo_anticipo) * 1);
            periodo_ = grid.getRowData(cl).periodo_anticipo;

        }
        if (periodo_2 === grid.getRowData(cl).periodo_anticipo) {
            valorsum = parseFloat(valorsum) + (grid.getRowData(cl).valor) * 1;
            valor_descuentosum = parseFloat(valor_descuentosum) + (grid.getRowData(cl).valor_descuento) * 1;
            valor_netosum = parseFloat(valor_netosum) + (grid.getRowData(cl).valor_neto) * 1;
            valor_combancariasum = parseFloat(valor_combancariasum) + (grid.getRowData(cl).valor_combancaria) * 1;
            vlr_consignacionsum = parseFloat(vlr_consignacionsum) + (grid.getRowData(cl).vlr_consignacion) * 1;

        } else if (periodo_2 !== grid.getRowData(cl).periodo_anticipo) {
            periodo_2 = grid.getRowData(cl).periodo_anticipo;
            if (valorsum !== 0) {
                valor.push(valorsum);
                valor_descuento.push(valor_descuentosum);
                valor_neto.push(valor_netosum);
                valor_combancaria.push(valor_combancariasum);
                vlr_consignacion.push(vlr_consignacionsum);
            }
            valorsum = grid.getRowData(cl).valor;
            valor_descuentosum = grid.getRowData(cl).valor_descuento;
            valor_netosum = grid.getRowData(cl).valor_neto;
            valor_combancariasum = grid.getRowData(cl).valor_combancaria;
            vlr_consignacionsum = grid.getRowData(cl).vlr_consignacion;
        }
        valorTotal = grid.jqGrid('getCol', 'valor', false, 'sum');
        valor_descuentoTotal = grid.jqGrid('getCol', 'valor_descuento', false, 'sum');
        valor_netoTotal = grid.jqGrid('getCol', 'valor_neto', false, 'sum');
        valor_combancariaTotal = grid.jqGrid('getCol', 'valor_combancaria', false, 'sum');
        vlr_consignacionTotal = grid.jqGrid('getCol', 'vlr_consignacion', false, 'sum');

    }
    for (var i = 0; i < periodos.length; i++) {
        var vector = [];
        vector.push(periodos[n], valor[n]);
        vectorGenral.push(vector);
        n = n + 1;
    }

    console.log(vectorGenral);
    console.log(periodos);
    console.log(valor);
    console.log(valor_descuento);
    console.log(valor_neto);
    console.log(valor_combancaria);
    console.log(vlr_consignacion);
    //verGrafica(periodos, valor, valor_descuento, valor_neto, valor_combancaria, vlr_consignacion, valorTotal, valor_descuentoTotal, valor_netoTotal, valor_combancariaTotal, vlr_consignacionTotal);
    $("#dialogMsjGraficaGeneralAnalisisReporteProduccion").dialog({
        width: '1065',
        height: '600',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function verGraficaGeneral(periodos, valor, valor_descuento, valor_neto, valor_combancaria, vlr_consignacion, valorTotal, valor_descuentoTotal, valor_netoTotal, valor_combancariaTotal, vlr_consignacionTotal) {
    $('#container').highcharts({
        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'An�lisis Reporte Producci�n'
        },
        subtitle: {
            text: 'Grafica detallada (hacer clic en las columnas para ver detalle)'
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },
        xAxis: {
            title: {
                text: 'Periodo'
            },
            //categories: Highcharts.getOptions().lang.shortMonths
            categories: periodos
        },
        yAxis: {
            title: {
                text: 'Rango'
            }
//            categories: [1, 2, 3]
        },
        series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                        name: 'Valor',
                        y: valorTotal,
                        drilldown: 'Valor'
                    }, {
                        name: 'Valor Descuento',
                        y: valor_descuentoTotal,
                        drilldown: 'Valor Descuento'
                    }, {
                        name: 'Valor Neto',
                        y: valor_netoTotal,
                        drilldown: 'Valor Neto'
                    }, {
                        name: 'Valor Combancaria',
                        y: valor_combancariaTotal,
                        drilldown: 'Valor Combancaria'
                    }, {
                        name: 'Valor Consignacion',
                        y: vlr_consignacionTotal,
                        drilldown: 'Valor Consignacion'
                    }]
            }],
        drilldown: {
            series: [{
                    name: 'Valor',
                    id: 'Valor',
                    data: [
                        [
                            'v11.0',
                            24.13
                        ],
                        [
                            'v8.0',
                            17.2
                        ],
                        [
                            'v9.0',
                            8.11
                        ],
                        [
                            'v10.0',
                            5.33
                        ],
                        [
                            'v6.0',
                            1.06
                        ],
                        [
                            'v7.0',
                            0.5
                        ]
                    ]
                }]
        }
    });
}