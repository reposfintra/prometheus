/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){ 
    maximizarventana();
    cargarTablaEquivalencias();
    jsonDocsConfig = cargarConfigDocs();
    cargarTiposDocumentos();
    
    $('#correspondencias').click(function () {
        $('#equiv_variables').fadeIn("slow");
        $('#equiv_variables').draggable();
    });
    
    $('#close').click(function () {
        $('#equiv_variables').fadeOut('slow');
    });
    
});

function initializeEditor (editor){
     var ckEditor = CKEDITOR.replace(editor, {
        height: 530,
        language: 'es',
        skin: 'office2013',
        resize_enabled: false,
        // Define the toolbar groups as it is a more accessible solution.
        toolbarGroups: [
            {name: 'document', groups: ['document']},
            {name: 'clipboard', groups: ['clipboard', 'undo']},
            {name: 'editing', groups: ['find', 'selection'/*, 'spellchecker'*/]},
            {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align']},
            {name: 'links'},
            {name: 'insert'},
            {name: 'styles'},
            {name: 'colors'},
            {name: 'tools'},
            {name: 'others'}

        ],
        // Remove the redundant buttons from toolbar groups defined above.
        removeButtons: 'NewPage,Preview,Print,Maximize,Flash,Iframe,CreateDiv,ShowBlocks',
        removePlugins: 'elementspath'
    });      
         
    ckEditor.on("instanceReady", function () {

        // overwrite the default save function
        ckEditor.addCommand("save", {
            modes: {wysiwyg: 1, source: 1},
            exec: function () {

                // get the editor content
                var theData = ckEditor.getData(); 
                guardarConfigDocs(editor,theData);            
            }
        });
    });
}

 function cargarConfigDocs () {
    var result = "";
    $.ajax({
            type: "POST",
            dataType: "json",          
            data: {opcion:1},
            async: false,
            url: './controller?estado=Garantias&accion=Comunitarias',
            success: function(jsonData) {
                    if (!isEmptyJSON(jsonData)) {  
                        result = jsonData;          
                     }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
    });
    
    return result;
       
 }
 
function guardarConfigDocs (editor,data){ 
   var element = CKEDITOR.document.getById(editor);
   var tipo_doc = element.getId().replace('editor','');

    var url = './controller?estado=Garantias&accion=Comunitarias';
    $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion:2,
                    tipo_doc:tipo_doc,
                    content:data
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") { 
                            mensajesDelSistema("Configuraci&oacute;n guardada exitosamente", '250', '150', true);                     
                        }
                        
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo guardar configuración del documento!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
 }
 
 function cargarTablaEquivalencias() {   
        $('#tbl_equiv_variables').html('');
        var url = './controller?estado=Garantias&accion=Comunitarias';
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            dataType: "json",
            data: {
                opcion: 3
            },
            success: function(jsonData) {
                        var json = jsonData.rows;
                        var i = 1;                       
                        $('#tbl_equiv_variables').append('<tr><th>#</th> <th>C&oacute;d. Parametro</th> <th>Descripci&oacute;n</th> </tr>');
                        for (var key in json) {
                            $('#tbl_equiv_variables').append('<tr>');
                            $('#tbl_equiv_variables tr:last').append('<td align="center">' + i + '</td>');
                            $('#tbl_equiv_variables tr:last').append('<td align="center">' + json[key].codparam + '</td>');
                            $('#tbl_equiv_variables tr:last').append('<td align="left">' + json[key].descripcion + '</td>');
                            $('#tbl_equiv_variables').append('</tr>');                           
                            i++;
                        }       
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        }); 
        
}

 function cargarTiposDocumentos() {
    $.ajax({
            type: "POST",
            dataType: "json",          
            data: {opcion:0,
                   mostrarTodos:'N'},
            async: false,
            url: './controller?estado=Garantias&accion=Comunitarias',
            success: function(jsonData) {
                    if (!isEmptyJSON(jsonData)) {                       
                        for (var i = 0; i < jsonData.rows.length; i++) {                        
                            addTabDocument(jsonData.rows[i].id,jsonData.rows[i].nombre); 
                            initializeEditor('editor'+jsonData.rows[i].id);  
                            $('#editor'+jsonData.rows[i].id).val(jsonDocsConfig[jsonData.rows[i].id]);                            
                        }
                        $("#tabs_documentos").tabs();                     
                    }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
    });       
 }
 
function addTabDocument(id, nombre) {
    var ni = document.getElementById('tabs_documentos');
    var ul = document.getElementById('lst_documents');
    var e, li, b;
    li = document.createElement('li');
    e = document.createElement('a');
    e.setAttribute('href', '#tab_' + id);
    e.innerHTML = nombre;
    li.appendChild(e);
    ul.appendChild(li);
    e = document.createElement('div');
    e.id = 'tab_' + id;
    e.style.width = '98%';
    ni.appendChild(e);
    $('#tab_' + id).append('<form>'+
                                '<textarea cols="80" id="editor'+id+'" name="editor'+id+' " rows="20" ></textarea>'+  
                           '</form>');
}
 
 function mensajesDelSistema(msj, width, height, swHideDialog) {   
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear botón cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}
 
function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}


function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}


