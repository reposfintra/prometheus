/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    var examinar = $("#examinar").val();
    cargarDocumentos();
    $("#subir").click(function () {
        var examinar = $("#examinar").val();
        validarExtension(examinar);
    });

});

function validarExtension(examinar) {
   
    var ext = /.XLS$/gi;
    var nombre = $("#examinar").val().split('\\').pop();

    if (!ext.test(examinar.toUpperCase())) {
        mensajesDelSistema("la extension del archivo no es valida", '410', '150', false);
    } else {
        if (nombre !== 'ajuste_peso.xls') {
            mensajesDelSistema("archivo no valido", '410', '150', false);
        } else {
            // subirArchivo();
            var info = jQuery("#tabla_informacionfac").getGridParam('records');
            if (info > 0) {
                mensajesDelSistema("Hay documentos por procesar", '250', '150', false);
            } else {
                subirArchivo();
            }

        }

    }
}

function subirArchivo() {
    var fd = new FormData(document.getElementById('formulario'));
    $.ajax({
        async: false,
        url: "./controller?estado=Fintra&accion=Soporte&opcion=56",
        type: 'POST',
        dataType: 'json',
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function (json) {

            if (json.json === 'OK')
                cargarDocumentos();

        }, error: function (xhr, ajaxOptions, thrownError) {

            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarDocumentos() {
    var filaInvalida = 0;
    var grid_tabla_ = jQuery("#tabla_informacionfac");
    if ($("#gview_tabla_informacionfac").length) {
        reloadGridMostrar(grid_tabla_, 55);
    } else {
        grid_tabla_.jqGrid({
            caption: "Documentos",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '1170',
            colNames: ['Id', 'Factura', 'Codigo cliente', 'Descripcion', 'Concepto', 'Valor', 'Cuenta ajuste', 'Banco', 'Sucursal', 'Fecha consiganacion'],
            colModel: [
                {name: 'id', index: 'id', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'factura', index: 'factura', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'codcli', index: 'codcli', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'concepto', index: 'concepto', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'valor', index: 'valor', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'cuenta_ajuste', index: 'cuenta_ajuste', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'banco', index: 'banco', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'sucursal', index: 'sucursal', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'fecha_consignacion', index: 'fecha_consignacion', width: 120, sortable: true, align: 'left', hidden: false}
              
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 55
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {
                var ids = grid_tabla_.jqGrid('getDataIDs');
                var allRowsInGrid = grid_tabla_.jqGrid('getRowData');
                for (var i = 0; i < allRowsInGrid.length; i++) {
                    var cl = ids[i];
                    var factura = grid_tabla_.getRowData(cl).factura;
                    var codcli = grid_tabla_.getRowData(cl).codcli;
                    var descripcion = grid_tabla_.getRowData(cl).descripcion;
                    var concepto = grid_tabla_.getRowData(cl).concepto;
                    var valor = grid_tabla_.getRowData(cl).valor;
                    var cuenta_ajuste = grid_tabla_.getRowData(cl).cuenta_ajuste;
                    var fecha_consignacion = grid_tabla_.getRowData(cl).fecha_consignacion;

                    if (factura === '' || codcli === '' || descripcion === '' || concepto === '' || valor === '0.00' || cuenta_ajuste === '' || fecha_consignacion === '0099-01-01 00:00:00') {
                        filaInvalida = filaInvalida + 1;
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "factura", "", {'background-color': '#F32541', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "codcli", "", {'background-color': '#F32541', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "descripcion", "", {'background-color': '#F32541', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "concepto", "", {'background-color': '#F32541', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "valor", "", {'background-color': '#F32541', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cuenta_ajuste", "", {'background-color': '#F32541', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#F32541', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#F32541', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_consignacion", "", {'background-color': '#F32541', 'background-image': 'none'});
                    } else {
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "factura", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "codcli", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "descripcion", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "concepto", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "valor", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cuenta_ajuste", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_consignacion", "", {'background-color': '#90CC00', 'background-image': 'none'});

                    }

                }


            },
            gridComplete: function (index) {
              
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_informacionfac").navButtonAdd('#pager1', {
            caption: "Ajustar Peso",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    if (filaInvalida > 0) {
                        mensajesDelSistema("No se puede procesar los datos se encuentran valores no validos, por favor limpie y vuelva a cargar el documento ", '389', '150', false);
                    } else {
                        actualizarDocumentos();
                    }
                } else {
                    mensajesDelSistema("No hay informacion para Actualizar ", '250', '150', false);
                }
            }
        });
        $("#tabla_informacionfac").navButtonAdd('#pager1', {
            caption: "Limpiar",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    filaInvalida = 0;
                    limpiar();
                } else {
                    mensajesDelSistema("No hay informacion para eliminar ", '250', '150', false);
                }
            }
        });

    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        title: 'Mensaje',
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    //  $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function  limpiar() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 57
        },
        success: function (data) {
            if (data.rows === 'ELIMINADO') {
                jQuery("#tabla_documentos").jqGrid("clearGridData", true);
                mensajesDelSistema("Exito al eliminar datos  ", '230', '150', true);
                cargarDocumentos();
            }

        }, error: function (result) {
            alert('ERROR  VERIFICAR DATOS');
        }
    });
}

function  actualizarDocumentos() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 58
        },
        success: function (data) {
            if (data.rows === 'SI') {
                mensajesDelSistema("Exito al actualizar  ", '230', '150', true);
                cargarDocumentos();
            } else if (data.respuesta === 'ERROR') {
                mensajesDelSistema("Error al generar documentos", '230', '150', false);
            }
        }, error: function (result) {
            alert('ERROR  VERIFICAR DATOS');
        }
    });
}