/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#divSalida").dialog({
        autoOpen: false,
        height: "auto",
        width: "300px",
        modal: true,
        autoResize: true,
        resizable: false,
        position: "center",
        closeOnEscape: false
    });
     
    $("#botonAceptar").hide();


});

function cerrarDiv(div)
{
    $(div).dialog('close');
}

function mensaje(msj) {
    $("#msj1").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'>X</span> " + msj);
    $("#dialogo").dialog({
        width: 300,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}


function cargarAuditoriaIngresos() {
    var periodoIni = $('#periodoIni').val();
    var periodoFin = $('#periodoFin').val();
    
    var url = './controller?estado=Auditoria&accion=Ingresos&opcion=1&periodoIni='+periodoIni+'&periodoFin='+periodoFin;
    if (periodoIni === '' || periodoFin === '') {
        mensaje("Todos los campos son requeridos");
    } else {
        if (periodoIni > periodoFin) {
            mensaje("El periodo Inicial debe ser inferior al Final");
        } else {
            if ($("#gview_AuditoriaIngresos").length) {
                refrescarGridAuditoriaIngresos(periodoIni, periodoFin);
            } else {
                jQuery("#AuditoriaIngresos").jqGrid({
                    caption: 'Auditoria de Ingresos',
                    url: url,
                    datatype: 'json',
                    height: 500,
                    width: 2500,
                    colNames: ['Periodo','Negocio', 'Cedula', 'Nombre Cliente', 'Vencimiento Mayor', 'Fecha Pago Ingreso', 'Fecha Vencimiento Mayor', 'Diferencia Pago', 'Valor Saldo Foto','Interes Mora', 'Gasto Cobranza', 'Numero Ingreso', 
                        'Valor Ingreso', 'Vr. CxC Ingreso', 'G16252145', 'G94350302', 'GI010010014205', 'GI010130014205', 'I16252147', 'I94350301','II010010014170','II010130024170', 'I010140014170', 'I010140014205','I28150530','I28150531', 'Vr. IxM Ingreso', 'Vr. GxC Ingreso','Convenio'
                    ],
                    colModel: [
                        {name: 'periodo', index: 'periodo', width: '500px', align: 'center', sortable: false },
                        {name: 'negocio', index: 'negocio', width: '600px', align: 'center', sortable: false },
                        {name: 'cedula', index: 'cedula', width: '500px', align: 'center', sortable: false},
                        {name: 'nombre_cliente', index: 'nombre_cliente', width: '1300px', align: 'left', sortable: false},
                        {name: 'vencimiento_mayor', index: 'vencimiento_mayor', width: '800px', align: 'center', sortable: false},
                        {name: 'fecha_pago_ingreso', index: 'fecha_pago_ingreso', width: '400px', align: 'center', sortable: false},
                        {name: 'fecha_vencimiento_mayor', index: 'fecha_vencimiento_mayor', width: '400px', align: 'center', sortable: false},
                        {name: 'diferencia_pago', index: 'diferencia_pago', width: '500px', align: 'center', sortable: false},
                        {name: 'valor_saldo_foto', index: 'valor_saldo_foto', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'interes_mora', index: 'interes_mora', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'gasto_cobranza', index: 'gasto_cobranza', width: '500px', align: 'center', sorttype: 'currency' , sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'num_ingreso', index: 'num_ingreso', width: '400px', align: 'center', sortable: false},
                        {name: 'valor_ingreso', index: 'valor_ingreso', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_cxc_ingreso', index: 'valor_cxc_ingreso', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'G16252145', index: 'G16252145', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'G94350302', index: 'G94350302', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'GI010010014205', index: 'GI010010014205', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'GI010130014205', index: 'GI010130014205', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'I16252147', index: 'I16252147', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'I94350301', index: 'I94350301', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'II010010014170', index: 'II010010014170', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'II010130024170', index: 'II010130024170', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'I010140014170', index: 'I010140014170', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'I010140014205', index: 'I010140014205', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'I28150530', index: 'I28150530', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'I28150531', index: 'I28150531', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_ixm_ingreso', index: 'valor_ixm_ingreso', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_gac_ingreso', index: 'valor_gac_ingreso', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'convenio', index: 'convenio', width: '700px', align: 'center', sortable: false}
                    ],
                    rowNum: 10000,
                    rowTotal: 500000,
                    loadonce: true,
                    rownumWidth: 40,
                    gridview: true,
                    pager: $('#page'),
                    viewrecords: true,
                    hidegrid: false,
                    jsonReader: {
                        root: 'rows',
                        repeatitems: false,
                        id: '0'
                    },
                    loadError: function (xhr, status, error) {
                        alert(error);
                    }
                });
                jQuery("#AuditoriaIngresos").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
            }
        }
    }
}

function exportarAuditoriaIngresos(){
    var periodoIni = $('#periodoIni').val();
    var periodoFin = $('#periodoFin').val();
    if (periodoIni === '' || periodoFin === '') {
        mensaje('Seleccione todos los filtros');
        $('#dialogo').dialog("close");
    }else{
        $("#divSalida").dialog("open");
        $("#imgload1").show();
        $("#msj").show();
        $("#resp1").hide();
        $.ajax({
            type: "POST",
            url: "./controller?estado=Auditoria&accion=Ingresos&opcion=2&periodoIni="+periodoIni+"&periodoFin="+periodoFin,
            dataType: "html",
            success: function (resp) {
                $("#imgload1").hide();
                $("#msj").hide();
                $("#resp1").show();
                var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalida" + "\")' /></div>"
                document.getElementById('resp1').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
            },
            error: function (resp) {
                alert("Ocurrio un error enviando los datos al servidor");
            }
        });
    }  
}

function refrescarGridAuditoriaIngresos(periodoIni, periodoFin){
    var url = './controller?estado=Auditoria&accion=Ingresos&opcion=1&periodoIni='+periodoIni+'&periodoFin='+periodoFin;
    jQuery("#AuditoriaIngresos").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#AuditoriaIngresos").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    jQuery('#AuditoriaIngresos').trigger("reloadGrid");
}

function cargarReporteCosecha(){
    var periodoIni = $('#periodoIni').val();
    var periodoFin = $('#periodoFin').val();
    var unidad_negocio = $('#unidad_negocio').val();
    
    var url = './controller?estado=Auditoria&accion=Ingresos&opcion=3&periodoIni='+periodoIni+'&periodoFin='+periodoFin+'&unidad_negocio='+unidad_negocio;
    if (periodoIni === '' || periodoFin === '' || unidad_negocio === '') {
        mensaje("Todos los campos son requeridos");
    } else {
        if (periodoIni > periodoFin) {
            mensaje("El periodo Inicial debe ser inferior al Final");
        } else {
            if ($("#gview_Cosechas").length) {
                refrescarGridCosechas(periodoIni, periodoFin,unidad_negocio);
            } else {
                jQuery("#Cosechas").jqGrid({
                    caption: 'Reporte de Cosechas',
                    url: url,
                    datatype: 'json',
                    height: 'auto',
                    width: 700,
                    colNames: ['Vencimiento Mayor','Valor Colocaci�n','Cantidad','Valor Pagos', 'Valor Saldo','% Item','Detalle'],
                    colModel: [
                        {name: 'vencimiento_mayor', index: 'vencimiento_mayor', width: '250', align: 'center', sortable: false, key:true},
                        {name: 'valor_asignado', index: 'valor_asignado', width: '200', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'cantidad', index: 'cantidad', width: '100', align: 'center', sortable: false},
                        {name: 'valor_pagos', index: 'valor_pagos', width: '200', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_saldo_foto', index: 'valor_saldo_foto', width: '200', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'porc_item', index: 'porc_item', width: '100', align: 'center',formatter: 'currency', sortable: false, formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, suffix: "%"}},
                        {name: 'exportar', index: 'exportar', width: '60px', align: 'center', fixed: true}     
                    ],
                    rowNum: 10000,
                    rowTotal: 10000,
                    loadonce: true,
                    gridview: true,
                    viewrecords: true,
                    hidegrid: false,
                    footerrow: true,
                    jsonReader: {
                        root: 'rows',
                        repeatitems: false,
                        id: '0'
                    },
                    gridComplete: function() {
                        var ids = jQuery("#Cosechas").jqGrid('getDataIDs');
                        for (var i = 0; i < ids.length; i++) {
                            var cl = ids[i];
                            exp = "<img src='/fintra/images/Excel.JPG' align='absbottom'  name='devolver' id='export' width='15' height='15' title ='Exportar Detalle'  onclick=\"exportarDetalle('"+cl+"','exportarDetalle');\">";
                            jQuery("#Cosechas").jqGrid('setRowData', ids[i], {exportar: exp}); 
                            
                        }
                    }, 
                    loadComplete: function () {
                    cacularTotales(jQuery("#Cosechas"));
                    },
                    loadError: function (xhr, status, error) {
                        alert(error);
                    }
                });
               
            }
        }
    }
}

function refrescarGridCosechas(periodoIni, periodoFin,unidad_negocio){
    var url = './controller?estado=Auditoria&accion=Ingresos&opcion=3&periodoIni='+periodoIni+'&periodoFin='+periodoFin+'&unidad_negocio='+unidad_negocio;
    jQuery("#Cosechas").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#Cosechas').trigger("reloadGrid");
}

function  cacularTotales(grid_cosecha) {
    var cantidad=grid_cosecha.jqGrid('getCol', 'cantidad', false, 'sum');
    var valor_pagos= Math.round(grid_cosecha.jqGrid('getCol', 'valor_pagos', false, 'sum'));
    var valor_saldo = Math.round(grid_cosecha.jqGrid('getCol', 'valor_saldo_foto', false, 'sum'));
    var total_asignado = Math.round(grid_cosecha.jqGrid('getCol', 'valor_asignado', false, 'sum'));
    var total_porcentaje = Math.round(grid_cosecha.jqGrid('getCol', 'porc_item', false, 'sum'));
    var exp = "<img src='/fintra/images/Excel.JPG' align='absbottom'  name='devolver' id='export' width='15' height='15' title ='Exportar Detalle'  onclick=\"exportarDetalle('"+'TOTAL'+"','exportarDetalle');\">";
    grid_cosecha.jqGrid('footerData', 'set', {
        vencimiento_mayor: 'Totales:',
        valor_asignado: total_asignado,
        cantidad: cantidad,
        valor_pagos: valor_pagos,
        valor_saldo_foto:valor_saldo,
        porc_item: total_porcentaje,
        exportar:exp

    });

}

function exportarDetalle(id){
    var periodoIni = $('#periodoIni').val();
    var periodoFin = $('#periodoFin').val();
    var unidad_negocio = $('#unidad_negocio').val();
    if (periodoIni === '' || periodoFin === '' || unidad_negocio === '') {
        mensaje('Seleccione todos los filtros');
        $('#dialogo').dialog("close");
    }else{
        $("#divSalida").dialog("open");
        $("#imgload1").show();
        $("#msj").show();
        $("#resp1").hide();
        $.ajax({
            type: "POST",
            url: "./controller?estado=Auditoria&accion=Ingresos&opcion=4&periodoIni="+periodoIni+"&periodoFin="+periodoFin+"&unidad_negocio="+unidad_negocio+"&venc_mayor="+id,
            dataType: "html",
            success: function (resp) {
                $("#imgload1").hide();
                $("#msj").hide();
                $("#resp1").show();
                var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalida" + "\")' /></div>"
                document.getElementById('resp1').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
            },
            error: function (resp) {
                alert("Ocurrio un error enviando los datos al servidor");
            }
        });
    }  
}

function cargarCuentasCaidas(){
    var periodo = $('#periodo').val();
    var unidad_negocio = $('#unidad_negocio').val();
    
    var url = './controller?estado=Auditoria&accion=Ingresos&opcion=5&periodo='+periodo+'&unidad_negocio='+unidad_negocio;
    if (periodo === '' || unidad_negocio === '') {
        mensaje("Todos los campos son requeridos");
    } else {
            if ($("#gview_CuentasCaidas").length) {
                refrescarGridCuentasCaidas(url);
            } else {
                jQuery("#CuentasCaidas").jqGrid({
                    caption: 'Reporte de Cuentas Ca�das',
                    url: url,
                    datatype: 'json',
                    height: 'auto',
                    width: 650,
                    colNames: ['Rango de Mora','No. Cuentas Debido','No. Cuentas Caidas','% Cuentas Caidas','Detalle'],
                    colModel: [
                        {name: 'vencimiento_mayor', index: 'vencimiento_mayor', width: '200', align: 'center', sortable: false, key:true},
                        {name: 'cuentas_debido', index: 'cuentas_debido', width: '100', align: 'center', sortable: false},
                        {name: 'cuentas_caidas', index: 'cuentas_caidas', width: '100', align: 'center', sortable: false},
                        {name: 'porc_item', index: 'porc_item', width: '100', align: 'center',formatter: 'currency', sortable: false, formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, suffix: "%"}},
                        {name: 'exportar', index: 'exportar', width: '60px', align: 'center', fixed: true}    
                    ],
                    rowNum: 10000,
                    rowTotal: 10000,
                    loadonce: true,
                    gridview: true,
                    viewrecords: true,
                    hidegrid: false,
                    jsonReader: {
                        root: 'rows',
                        repeatitems: false,
                        id: '0'
                    },
                    gridComplete: function() {
                        var ids = jQuery("#CuentasCaidas").jqGrid('getDataIDs');
                        for (var i = 0; i < ids.length; i++) {
                            var cl = ids[i];
                            exp = "<img src='/fintra/images/buscar.jpg' align='absbottom'  name='devolver' id='export' width='15' height='15' title ='Ver Detalle'  onclick=\"abrirDivDetaCuentasCaidas('"+cl+"');\">";
                            jQuery("#CuentasCaidas").jqGrid('setRowData', ids[i], {exportar: exp}); 
                            
                        }
                    }, 
                    loadError: function (xhr, status, error) {
                        alert(error);
                    }
                });
               
            }
        }
    }
    
    function abrirDivDetaCuentasCaidas(id) {
    $("#div_deta_cuentas").dialog({
        width: 1450,
        height: 680,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Exportar": function () {
                exportarDetalleCuentasCaida();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
    detalleCuentasCaidas(id);
    }
    
    function detalleCuentasCaidas(id){
    var periodo = $('#periodo').val();
    var unidad_negocio = $('#unidad_negocio').val();
    
    var url = './controller?estado=Auditoria&accion=Ingresos&opcion=6&periodo='+periodo+'&unidad_negocio='+unidad_negocio+'&venc_mayor='+id;
    if (periodo === '' || unidad_negocio === '') {
        mensaje("Todos los campos son requeridos");
    } else {
            if ($("#gview_deta_cuentasCaidas").length) {
                refrescarGridDetalleCuentasCaidas(url);
            } else {
                jQuery("#deta_cuentasCaidas").jqGrid({
                    caption: 'Detalle Cuentas Ca�das',
                    url: url,
                    datatype: 'json',
                    height: 500,
                    width: 1400,
                    colNames: ['Cedula','Nombre','Negocio','Undidad de Negocio','Convenio','Cartera con Sancion','Valor Debido','Valor Cuota','Recaudo Aplicado al Debido','Valor Recaudo','Porcentaje'],
                    colModel: [
                        {name: 'cedula', index: 'cedula', width: '350', align: 'center', sortable: false, key:true},
                        {name: 'nombre_cliente', index: 'nombre_cliente', width: '1000', align: 'left', sortable: false},
                        {name: 'negocio', index: 'negocio', width: '250', align: 'center', sortable: false},
                        {name: 'unidad_negocio', index: 'unidad_negocio', width: '450', align: 'center',sortable: false},
                        {name: 'convenio', index: 'convenio', width: '70', align: 'center', fixed: true},
                        {name: 'cartera_con_sancion', index: 'cartera_con_sancion', width: '500', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_debido', index: 'valor_debido', width: '500', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_cuota', index: 'valor_cuota', width: '450px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_recaudo', index: 'valor_recaudo', width: '500px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_recaudo_debido', index: 'valor_recaudo_debido', width: '450px', align: 'center', sorttype: 'currency', sortable: false,
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'porc_item', index: 'porc_item', width: '200', align: 'center',formatter: 'currency', sortable: false, formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, suffix: "%"}},
                    ],
                    rowNum: 10000,
                    rowTotal: 10000,
                    loadonce: true,
                    gridview: true,
                    viewrecords: true,
                    hidegrid: false,
                    rownumbers: true,
                    jsonReader: {
                        root: 'rows',
                        repeatitems: false,
                        id: '0'
                    },
                    loadError: function (xhr, status, error) {
                        alert(error);
                    }
                });
               
            }
        }
    }
    
    function exportarDetalleCuentasCaida(){
        var fullData = jQuery("#deta_cuentasCaidas").jqGrid('getRowData');
        var myJsonString = JSON.stringify(fullData);

        $("#divSalida").dialog("open");
        $("#imgload").show();
        $("#msj").show();
        $("#resp1").hide();
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "./controller?estado=Auditoria&accion=Ingresos",
            data: {
                listado: myJsonString,
                opcion: 7
            },
            success: function (resp) {
                $("#imgloadEx").hide();
                $("#msj").hide();
                $("#resp1").show();
                var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalida" + "\")' /></div>";
                document.getElementById('resp1').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
            },
            error: function (resp) {
                alert("Ocurrio un error enviando los datos al servidor");
            }
        });
    }
    
    function refrescarGridDetalleCuentasCaidas(url){
        jQuery("#deta_cuentasCaidas").setGridParam({
            datatype: 'json',
            url: url
        });
        jQuery('#deta_cuentasCaidas').trigger("reloadGrid");
    }
    
     function refrescarGridCuentasCaidas(url){
        jQuery("#CuentasCaidas").setGridParam({
            datatype: 'json',
            url: url
        });
        jQuery('#CuentasCaidas').trigger("reloadGrid");
    }


