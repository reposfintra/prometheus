/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $('#buscar').click(function () {
        cargarPazYSalvo();
    });
});


function cargarPazYSalvo() {
    var grid_tabla_ = jQuery("#tabla_pazYsalvo");
    if ($("#gview_tabla_pazYsalvo").length) {
        reloadGridMostrar(grid_tabla_, 73);
    } else {
        grid_tabla_.jqGrid({
            caption: "Paz y Salvo",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            autowidth: false,
            colNames: ['Tipo Doc','Cedula', 'Nombre Cliente', 'Negocio', 'Genero', 'Nombre Codeudor', 'CC Cedula', 'Genero Codeudor','Fecha Negocio', 'Estado Negocio', 'Convenio', 'Id Convenio', 'Afiliado', 'Tipo Negocio', 'Cuotas', 'Valor Negocio', 'Saldo Cartera', 'Fianza', 'Generar Paz y Salvo', 'Visto Bueno','Poliza','Visto Bueno Poliza'],
            colModel: [
                {name: 'tipo_id', index: 'tipo_id', width: 100, align: 'center', hidden: true},
                {name: 'cod_cli', index: 'cod_cli', width: 100, align: 'center'},
                {name: 'cliente', index: 'cliente', width: 250, align: 'left', hidden: false},
                {name: 'negasoc', index: 'negasoc', width: 100, align: 'center', hidden: false},
                {name: 'genero', index: 'genero', width: 100, align: 'center', hidden: false},
                {name: 'codeudor', index: 'codeudor', width: 250, align: 'left', hidden: true},
                {name: 'cc_codeudor', index: 'cc_codeudor', width: 250, align: 'left', hidden: true},
                {name: 'genero_codeudor', index: 'genero_codeudor', width: 250, align: 'left', hidden: true},
                {name: 'fecha_negocio', index: 'fecha_negocio', width: 100, align: 'center', hidden: false},
                {name: 'estado_neg', index: 'estado_neg', width: 100, align: 'left', hidden: false},
                {name: 'convenio', index: 'convenio', width: 180, align: 'left', hidden: true},
                {name: 'id_convenio', index: 'id_convenio', width: 100, align: 'center', hidden: true},
                {name: 'afiliado', index: 'afiliado', width: 100, align: 'left', hidden: true},
                {name: 'tipo_negocio', index: 'tipo_negocio', width: 180, align: 'left', hidden: true},
                {name: 'nro_docs', index: 'nro_docs', width: 100, align: 'center', hidden: false},
                {name: 'vr_negocio', index: 'vr_negocio', width: 130, align: 'right', sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'saldo_cartera', index: 'saldo_cartera', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'fianza', index: 'fianza', width: 100, align: 'center', hidden: false},
                {name: 'generar', index: 'generar', width: 100, align: 'center', hidden: false},
                {name: 'visto_bueno_fianza', index: 'visto_bueno_fianza', width: 100, align: 'center', hidden: false},
                {name: 'poliza', index: 'poliza', width: 100, align: 'center', hidden: false},
                {name: 'visto_bueno_poliza', index: 'visto_bueno_poliza', width: 100, align: 'center', hidden: false}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                data: {
                    opcion: 73,
                    cliente: $('#cedula').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var ids = grid_tabla_.jqGrid('getDataIDs'), fila;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var saldo_cartera = grid_tabla_.getRowData(cl).saldo_cartera;
                    var fianza = grid_tabla_.getRowData(cl).fianza;
                    var visto_bueno_fianza = grid_tabla_.getRowData(cl).visto_bueno_fianza;
                    var poliza = grid_tabla_.getRowData(cl).poliza;
                    var visto_bueno_poliza = grid_tabla_.getRowData(cl).visto_bueno_poliza;
                    if (fianza == 'S' & saldo_cartera === '0.00' & visto_bueno_fianza === 'S') {
                        ed = "<img src='/fintra/images/descargarPdf.png' align='absbottom'  style='height:33px;width:35px;margin-left: 8px;' name='generar' id='generar'  width='19' height='19' title ='Generar Paz y Salvo'  onclick=\"exportarPdf('" + cl + "');\">";
                    } else if (saldo_cartera === '0.00' & poliza == 'S' & visto_bueno_poliza == 'S' ) {
                        ed = "<img src='/fintra/images/descargarPdf.png' align='absbottom'  style='height:33px;width:35px;margin-left: 8px;' name='generar' id='generar'  width='19' height='19' title ='Generar Paz y Salvo'  onclick=\"exportarPdf('" + cl + "');\">";
                    } else if (saldo_cartera === '0.00' & poliza == 'N' & fianza == 'N') {
                        ed = "<img src='/fintra/images/descargarPdf.png' align='absbottom'  style='height:33px;width:35px;margin-left: 8px;' name='generar' id='generar'  width='19' height='19' title ='Generar Paz y Salvo'  onclick=\"exportarPdf('" + cl + "');\">";
                    } else {
                        ed = "<img src='/fintra/images/descargarPdfI.png' align='absbottom'  style='height:25px;width:27px;margin-left: 12px;padding: 2px;' name='generar' id='generar'  width='19' height='19' title ='Generar Paz y Salvo' onclick=\"mensaje('" + cl + "');\">";
                    }
                    grid_tabla_.jqGrid('setRowData', ids[i], {generar: ed});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                cliente: $('#cedula').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center", modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function  exportarPdf(id) {
    var grid_tabla = jQuery("#tabla_pazYsalvo");
    var cedula = grid_tabla.getRowData(id).cod_cli;
    var cliente = grid_tabla.getRowData(id).cliente;
    console.log(cliente);
    var codeudor = grid_tabla.getRowData(id).codeudor;
    var cc_codeudor = grid_tabla.getRowData(id).cc_codeudor;
    var genero_codeudor= grid_tabla.getRowData(id).genero_codeudor;
    var negocio = grid_tabla.getRowData(id).negasoc;
    var genero = grid_tabla.getRowData(id).genero;
    var tipo_id = grid_tabla.getRowData(id).tipo_id;
    var json = [];
    var llave = {};
    llave['cedula'] = cedula;
    llave['cliente'] = cliente;
    llave['negocio'] = negocio;
    llave['genero'] = genero;
    llave['tipoid'] = tipo_id;
    llave['codeudor'] = codeudor;
    llave['cc_codeudor'] = cc_codeudor;
    llave['genero_codeudor'] = genero_codeudor;
    json.push(llave);
    $.ajax({
        type: "POST",
        dataType: "text",
        url: "./controller?estado=Admin&accion=Fintra",
        async: false,
        data: {
            opcion: 75,
            info: JSON.stringify({json: json})
        },
        success: function (resp) {
            if (resp.trim() === 'OK') {
                mensajesDelSistema('Exito al generar pdf', '300', 'auto', false);
            } else {
                mensajesDelSistema('Ocurrio un error', '300', 'auto', false);
            }
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function  mensaje(id) {
    funcionFormatos = new formatos();
    var grid_tabla = jQuery("#tabla_pazYsalvo");
    var negocio = grid_tabla.getRowData(id).negasoc;
    var saldo_cartera = grid_tabla.getRowData(id).saldo_cartera;
    var visto_bueno = grid_tabla.getRowData(id).visto_bueno_fianza;
    var mensaje;
    
    if (visto_bueno.toUpperCase() === "N") {
        mensaje = "No se puede generar el Paz y Salvo debido a que a�n no tiene visto bueno por parte de la entidad.";
    } else if (saldo_cartera > 0) {
        mensaje = 'No se puede generar el Paz y Salvo debido  a que el cliente presenta saldo en la obligacion No: ' + negocio + ' por un  valor de ' + funcionFormatos.numberConComas(saldo_cartera);
    }
    mensajesDelSistema(mensaje, '500', 'auto', false);
}