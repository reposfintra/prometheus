/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var funcionFormatos = new formatos();
$(document).ready(function () {

    $('#buscar').click(function () {
        cargarAnticipo();
    });
});


function cargarAnticipo() {
    var grid_tabla = jQuery("#tabla_anticipo");
    if ($("#gview_tabla_anticipo").length) {
        reloadGridAnticipo(grid_tabla, 96);
    } else {
        grid_tabla.jqGrid({
            caption: "Anticipos",
            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
            datatype: "json",
            height: '420',
            width: '1650',
            colNames: ['Id', 'Id solicitud', 'Multiservicio', 'Codigo Cliente', 'Nombre Cliente', 'Numero Anticipo', 'Valor anticipo', 'Fecha anticipo', '# CXC', 'Valor CXC', 'Saldo CXC', 'F. CXC', '# Ingreso', 'Valor Ingreso',
                'F. Consignacion', '# Nota Ajuste', 'Valor Nota Ajuste', '# CXP', 'Valor CXP', 'Saldo CXP', 'F. CXP'],
            colModel: [
                {name: 'id', index: 'id', width: 120, align: 'left', key: true, hidden: true, frozen: true},
                {name: 'id_solicitud', index: 'id_solicitud', width: 120, align: 'center', hidden: false, frozen: true},
                {name: 'multiservicio', index: 'multiservicio', width: 120, align: 'center', hidden: false, frozen: true},
                {name: 'codcli', index: 'codcli', width: 120, align: 'center', hidden: false, frozen: true},
                {name: 'nomcli', index: 'nomcli', width: 230, align: 'left', hidden: false, frozen: true},
                {name: 'cod_anticipo', index: 'cod_anticipo', width: 120, align: 'center', hidden: false, frozen: true},
                {name: 'valor_anticipo', index: 'valor_anticipo', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}, frozen: true},
                {name: 'fecha_anticipo', index: 'fecha_anticipo', width: 150, align: 'center', sortable: false, search: false, editable: false, editrules: {date: true}, frozen: true},
                {name: 'num_factura', index: 'num_factura', width: 120, align: 'center', hidden: false},
                {name: 'valor_factura', index: 'valor_factura', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'saldo_factura', index: 'saldo_factura', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'fecha_factura', index: 'fecha_factura', width: 110, align: 'center', sortable: false, search: false, editable: false, editrules: {date: true}},
                {name: 'num_ingreso', index: 'num_ingreso', width: 120, align: 'center', hidden: false},
                {name: 'valor_ingreso', index: 'valor_ingreso', sortable: true, editable: false, width: 110, align: 'right', hidden: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'fecha_consignacion', index: 'fecha_consignacion', width: 120, align: 'center', hidden: false},
                {name: 'nota_ajuste', index: 'nota_ajuste', width: 120, align: 'center', hidden: false},
                {name: 'valor_nota', index: 'valor_nota', sortable: true, editable: false, width: 110, align: 'right', search: false, hidden: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'num_cxp', index: 'num_cxp', width: 120, align: 'center', hidden: false},
                {name: 'valorcxp', index: 'valorcxp', sortable: true, editable: false, width: 110, align: 'right', search: false, hidden: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'saldoCxp', index: 'saldoCxp', sortable: true, editable: false, width: 110, align: 'right', search: false, hidden: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'fecha_cxp', index: 'fecha_cxp', width: 110, align: 'center', sortable: false, search: false, editable: false, hidden: false, editrules: {date: true}}
            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            pager: 'pager3',
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 96,
                    id_solicitud: $('#solicitud').val(),
                    foms: $('#foms').val(),
                    estado_: $('#estado').val(),
                    anticipo: $('#anticipo').val()
                }
            },
            loadComplete: function (rowid, e, iRow, iCol) {

            },
            gridComplete: function () {
                var valor_anticipo = jQuery("#tabla_anticipo").jqGrid('getCol', 'valor_anticipo', false, 'sum');
                var valor_factura = jQuery("#tabla_anticipo").jqGrid('getCol', 'valor_factura', false, 'sum');
                var saldo_factura = jQuery("#tabla_anticipo").jqGrid('getCol', 'saldo_factura', false, 'sum');
                var valor_ingreso = jQuery("#tabla_anticipo").jqGrid('getCol', 'valor_ingreso', false, 'sum');
                var valor_nota = jQuery("#tabla_anticipo").jqGrid('getCol', 'valor_nota', false, 'sum');
                var valorcxp = jQuery("#tabla_anticipo").jqGrid('getCol', 'valorcxp', false, 'sum');
                var saldoCxp = jQuery("#tabla_anticipo").jqGrid('getCol', 'saldoCxp', false, 'sum');
                jQuery("#tabla_anticipo").jqGrid('footerData', 'set', {valor_anticipo: valor_anticipo, valor_factura: valor_factura, saldo_factura: saldo_factura,
                    valor_ingreso: valor_ingreso, valor_nota: valor_nota, valorcxp: valorcxp, saldoCxp: saldoCxp});
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid('#pager3', {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        jQuery("#tabla_anticipo").navButtonAdd('#pager3', {
            caption: "Exportar",
            title: "Exportar a Excel",
            buttonicon: "ui-icon-triangle-1-s",
            onClickButton: function () {
                exportarExcelReporte();
            }
        });
        grid_tabla.jqGrid('setGroupHeaders', {
            useColSpanStyle: false,
            height: '2',
            groupHeaders: [
                {startColumnName: 'id_solicitud', numberOfColumns: 4, titleText: '<H3><em>OFERTA</em></H3>'},
                {startColumnName: 'cod_anticipo', numberOfColumns: 3, titleText: '<H3><em>ANTICIPO</em></H3>'},
                {startColumnName: 'num_factura', numberOfColumns: 4, titleText: '<H3><em>CXC</em></H3>'},
                {startColumnName: 'num_ingreso', numberOfColumns: 3, titleText: '<H3><em>INGRESO</em></H3>'},
                {startColumnName: 'nota_ajuste', numberOfColumns: 2, titleText: '<H3><em>NOTA AJUSTE</em></H3>'},
                {startColumnName: 'num_cxp', numberOfColumns: 4, titleText: '<H3><em>CXP</em></H3>'}
            ]
        });
        jQuery("#tabla_anticipo").jqGrid('setFrozenColumns');
    }
}

function sumaCantidadesAnticipo() {
    var valor_anticipo = jQuery("#tabla_anticipo").jqGrid('getCol', 'valor_anticipo', false, 'sum');
    $('.ui-jqgrid-sdiv td[aria-describedby="tabla_anticipo_valor_anticipo"]').text('$ ' + funcionFormatos.numberConComas(valor_anticipo));
}

function reloadGridAnticipo(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: $('#solicitud').val(),
                foms: $('#foms').val(),
                estado_: $('#estado').val(),
                anticipo: $('#anticipo').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
    sumaCantidadesAnticipo();
}

//$("#tabla_anticipo").jqGrid('getGridParam','colNames');

function  exportarExcelReporte() {
    var fullData = $("#tabla_anticipo").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var nameColum = $("#tabla_anticipo").jqGrid('getGridParam', 'colNames');
    nameColum.splice(0, 1);
//    var infotamano = $("#tabla_anticipo").jqGrid('getGridParam', 'colModel');
//    infotamano.splice(0, 1);
//    var tamano = [];
//    for (var i in infotamano) {
//        var valort = parseFloat(infotamano[i].width) * 100;
//        tamano.push(valort);
//    }
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        data: {
            listado: myJsonString,
            namecolum: nameColum.toString(),
            opcion: 97
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}