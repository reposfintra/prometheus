/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//eventos onclik de los botones en la vista.
$(document).ready(function() {

    cargarNegocios();

});

function cargarNegocios() {

    var grid_lista_negocios = jQuery("#tabla_negocios");

    if ($("#gview_tabla_negocios").length) {
        reloadGridListaNegocios(grid_lista_negocios);
    } else {

        grid_lista_negocios.jqGrid({
            caption: "Lista de negocios",
            url: "./controller?estado=Reestructuracion&accion=Negocios&opcion=9&act=ANA",
            mtype: "POST",
            datatype: "json",
            height: '530',
            width: '1210',
            colNames: ['Cod negocio', 'Cliente', 'Cedula', 'Valor negocio', 'Valor documentos', 'Fecha Negocio', 'Cuotas', 'Convenio', 'Tasa', 'Valor Seguro', 'CAT', 'Acciones'],
            colModel: [
                {name: 'cod_negocio', index: 'cod_negocio', width: 80, align: 'center', key: true},
                {name: 'nombreafil', index: 'nombreafil', sortable: true, width: 200, align: 'center'},
                {name: 'cod_cli', index: 'cod_cli', sortable: true, width: 80, align: 'center'},
                {name: 'vr_negocio', index: 'vr_negocio', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'totpagado', index: 'totpagado', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'fecha_neg', index: 'fecha_neg', sortable: true, width: 100, align: 'center'},
                {name: 'nodocs', index: 'nodocs', sortable: true, width: 50, align: 'center'},
                {name: 'id_convenio', index: 'id_convenio', sortable: true, width: 80, align: 'center'},
                {name: 'tasa', index: 'tasa', sortable: true, width: 80, align: 'center'},
                {name: 'valor_seguro', index: 'valor_seguro', sortable: true, width: 80, align: 'center', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'porcentaje_cat', index: 'porcentaje_cat', sortable: true, width: 80, align: 'center'},
                {name: 'acciones', index: 'acciones', sortable: false, width: 60, align: 'right'}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            rownumbers: true,
//            multiselect: true,
            footerrow: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function() {

                if (grid_lista_negocios.jqGrid('getGridParam', 'records') > 0) {

                    cacularTotalesNegocio(grid_lista_negocios);

                } else {
                    mensajesDelSistema("Lista negocios vacia.", "250", "150");
                    cacularTotalesNegocio(grid_lista_negocios);
                }


            }, gridComplete: function() {
                var ids = grid_lista_negocios.jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    be = "<input style='height:22px;width:20px;' type='button' value='A' onclick=\"ver_observacion('" + cl + "');\">";
                    se = "<input style='height:22px;width:20px;' type='button' value='L' onclick=\"ver_liquidacion('" + cl + "');\">";
                    ce = "<input style='height:22px;width:20px;' type='button' value='R' onclick=\"rechazar('" + cl + "');\">";
                    grid_lista_negocios.jqGrid('setRowData', ids[i], {acciones: be + se + ce});
                }
            }, loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });


        grid_lista_negocios.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    multipleSearch: true
                });


    }

}

function  reloadGridListaNegocios(grid_lista_negocios) {
    grid_lista_negocios.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructuracion&accion=Negocios&opcion=9&act=ANA"
    });
    grid_lista_negocios.trigger("reloadGrid");
}


function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function mensajesConfirmacion(msj, width, height, idRows) {

    $("#msj3").html("<b>" + msj + "</b>");
    $("#confirmacion").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Si": function() {
                rechazarNegocio(idRows);
                $(this).dialog("close");
            },
            "No": function() {
                $(this).dialog("close");
            }
        }
    });

}


function  cacularTotalesNegocio(grid_lista_negocios) {

    var vr_negocio = grid_lista_negocios.jqGrid('getCol', 'vr_negocio', false, 'sum');
    var totpagado = grid_lista_negocios.jqGrid('getCol', 'totpagado', false, 'sum');


    grid_lista_negocios.jqGrid('footerData', 'set', {
        totpagado: totpagado,
        vr_negocio: vr_negocio
    });

}

function ver_liquidacion(idRows) {
    var fila = jQuery("#tabla_negocios").getRowData(idRows);
    $("#interes").html(fila['tasa']);
    $("#fneg").html(fila['fecha_neg']);
    $("#cuota1").html(fila['nodocs']);
    $("#cat").html(fila['porcentaje_cat']);
    $("#vlr_seguro").html(numberConComas(fila['valor_seguro']));
    $("#vlr_negocio").html(numberConComas(fila['vr_negocio']));

    cargarLiquidacion(idRows);

    $("#ver_liquidacion").dialog({
        width: 1022,
        height: 535,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n 
            "Exportar": function() {
                exportData('10', 'xls');
            }
        }
    });
}

function rechazar(idRows) {
    mensajesConfirmacion("�Desea rechazar este negocio?", "230", "140", idRows);
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function ver_observacion(idRows) {

    buscarTrazabilida(idRows);
}

function cargarLiquidacion(negocio) {
    var grid_liquidador_credito = jQuery("#tabla_liquidador_neg");

    if ($("#gview_tabla_liquidador_neg").length) {
        reloadGridLiquidadorCredito(grid_liquidador_credito, negocio);
    } else {

        grid_liquidador_credito.jqGrid({
            caption: "Liquidacion Negocio",
            url: "./controller?estado=Reestructuracion&accion=Negocios&opcion=10&negocio=" + negocio,
            mtype: "POST",
            datatype: "json",
            height: '280',
            width: '995',
            colNames: ['Fecha', 'Cuota', 'Saldo Inicial', 'Valor Cuota', 'Capital', 'Interes', 'Capacitacion', 'Cat', 'Seguro', 'Saldo Final'],
            colModel: [
                {name: 'fecha', index: 'fecha', width: 80, align: 'center'},
                {name: 'item', index: 'item', sortable: true, width: 50, align: 'center', key: true},
                {name: 'saldo_inicial', index: 'saldo_inicial', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor', index: 'valor', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'capital', index: 'capital', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes', index: 'interes', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'capacitacion', index: 'capacitacion', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cat', index: 'cat', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'seguro', index: 'seguro', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'saldo_final', index: 'saldo_final', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function() {

                cacularTotalesLiquidadorCredito(grid_liquidador_credito);
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });




    }
}

function  reloadGridLiquidadorCredito(grid_liquidador_crediton, negocio) {
    grid_liquidador_crediton.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructuracion&accion=Negocios&opcion=10&negocio=" + negocio
    });
    grid_liquidador_crediton.trigger("reloadGrid");
}


function cacularTotalesLiquidadorCredito(grid_simulador_credito) {

   
    var valor = grid_simulador_credito.jqGrid('getCol', 'valor', false, 'sum');
    var capital = grid_simulador_credito.jqGrid('getCol', 'capital', false, 'sum');
    var interes = grid_simulador_credito.jqGrid('getCol', 'interes', false, 'sum');
    var capacitacion = grid_simulador_credito.jqGrid('getCol', 'capacitacion', false, 'sum');
    var cat = grid_simulador_credito.jqGrid('getCol', 'cat', false, 'sum');
    var seguro = grid_simulador_credito.jqGrid('getCol', 'seguro', false, 'sum');
    var saldo_final = grid_simulador_credito.jqGrid('getCol', 'saldo_final', false, 'sum');

    grid_simulador_credito.jqGrid('footerData', 'set', {
        cuota: 'Total:',
        valor: valor,
        capital: capital,
        interes: interes,
        capacitacion: capacitacion,
        cat: cat,
        seguro: seguro,
        saldo_final: saldo_final
    });
}

function rechazarNegocio(negocio) {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructuracion&accion=Negocios",
        dataType: 'json',
        data: {
            opcion: 11,
            negocio: negocio
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    if (json.respuesta === "OK") {
                        reloadGridListaNegocios(jQuery("#tabla_negocios"));
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + json + '>' + json, '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });


}

function buscarTrazabilida(negocio) {
    $.ajax({
        async: false,
        type: 'POST',
        url: "./controller?estado=Reestructuracion&accion=Negocios",
        dataType: 'json',
        data: {
            opcion: 12,
            negocio: negocio
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    var tds;
                    var item = 1;
                    var numero_solicitud;
                    var tabla = "<table id='traza' border='0' class='tablaTraza'>" +
                            "<thead>" +
                            "<tr>" +
                            "<th class='ui-state-default th'>Item</th>" +
                            "<th class='ui-state-default th'>Formulario</th>" +
                            "<th class='ui-state-default th'>Actividad</th>" +
                            "<th class='ui-state-default th'>Usuario</th>" +
                            "<th class='ui-state-default th'>Fecha</th>" +
                            "<th class='ui-state-default th'>Comentarios</th>" +
                            "</tr>" +
                            "</thead>" +
                            "<tbody>" +
                            "</tbody>" +
                            "</table>";

                    $("#div_trazabilidad").html(tabla);

                    for (var j in json) {
                        tds = "<tr>";
                        tds += "<td class='ui-widget-content first'>" + item + "</td>";
                        tds += "<td class='ui-widget-content first'>" + json[j].numeroSolicitud + "</td>";
                        tds += "<td class='ui-widget-content first'>" + json[j].actividad + "</td>";
                        tds += "<td class='ui-widget-content first'>" + json[j].usuario + "</td>";
                        tds += "<td class='ui-widget-content first'>" + json[j].fecha + "</td>";
                        tds += "<td class='ui-widget-content first'> <div id='comentarios" + item + "'>" + json[j].comentarios + "</div></td>";
                        tds += '</tr>';
                        $("#traza").append(tds);


                        numero_solicitud = json[j].numeroSolicitud;
                        $("#observaciones").append("<div id='con" + item + "' class='complete'></div>");
                        formatoComentario(item);
                        item++;

                    }


                    $("#ver_traza").dialog({
                        width: 650,
                        height: 324,
                        show: "scale",
                        hide: "scale",
                        resizable: false,
                        position: "center",
                        modal: true,
                        closeOnEscape: false,
                        dialogClass: 'no-close',
                        buttons: {//crear bot�n 
                            "Aprobar": function() {
                                aprobarNegocios(negocio, numero_solicitud);
                            },
                            "Salir": function() {
                                $(this).dialog("close");
                                $("#div_trazabilidad").html('');
                            }
                        }
                    });


                } catch (exception) {
                    mensajesDelSistema('error : ' + json + '>' + json[j], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });


}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}


function formatoComentario(item) {
    // Colocamos el contenido
    var caracteresAMostrar = 15;
    var contenido = $("#comentarios" + item).html();
    $("#con" + item).html(contenido);

    if (contenido.length > caracteresAMostrar) {
        var resumen = contenido.substr(0, caracteresAMostrar);
        var todo = contenido.substr(caracteresAMostrar, contenido.length - caracteresAMostrar);

        var nuevocontenido = resumen + '<span class="complete">' + todo + '</span><br><span ><a href="#" onclick="showObservacion(' + item + ')">Leer mas...</span>';
        $("#comentarios" + item).html(nuevocontenido);

    }
}

function showObservacion(item) {

    $("#obser").html($("#con" + item).html());
    $("#observaciones").dialog({
        width: '400',
        height: '220',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false

    });
}

function aprobarNegocios(negocio, formulario) {

    var observacion = $("#textarea").val();

    if (observacion !== "") {
        mensajesDelSistema2("Espere un momento por favor...", "270", "140");
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Reestructuracion&accion=Negocios",
            dataType: 'json',
            data: {
                opcion: 13,
                negocio: negocio,
                formulario: formulario,
                observacion: observacion
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        if (json.respuesta === "OK") {
                            

                            setTimeout(function() {
                               $("#dialogo2").dialog('close');
                                $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> El proceso ha sido exitoso !!!!" );
                                $("#dialogo").dialog({
                                    width: '250',
                                    height: '150',
                                    show: "scale",
                                    hide: "scale",
                                    resizable: true,
                                    position: "center",
                                    modal: true,
                                    closeOnEscape: false,
                                    buttons: {//crear bot�n de cerrar
                                        "Aceptar": function() {
                                            reloadGridListaNegocios(jQuery("#tabla_negocios"));
                                            $("#ver_traza").dialog("close");
                                            $(this).dialog("close");
                                        }
                                    }
                                });
                          
                              
                            }, 2000);

                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + json + '>' + json, '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {

        mensajesDelSistema("Lo sentimos debe ingresar una observacion", '250', '150');
    }

}


function exportData(cols, file) {
    var columns = cols.toString().split(",");
    var columnNms = $("#tabla_liquidador_neg").jqGrid('getGridParam', 'colNames');
    var theads = "";
    for (var cc = 0; cc < columnNms.length; cc++) {
        var isAdd = true;
        for (var p = 0; p < columns.length; p++) {
            if (cc == columns[p]) {
                isAdd = false;
                break;
            }
        }
        if (isAdd) {
            theads = theads + "<th>" + columnNms[cc] + "</th>"
        }
    }
    theads = "<tr>" + theads + "</tr>";
    var mya = new Array();
    mya = jQuery("#tabla_liquidador_neg").getDataIDs();  // Get All IDs
    var data = jQuery("#tabla_liquidador_neg").getRowData(mya[0]);     // Get First row to get the labels
    //alert(data['id']);
    var colNames = new Array();
    var ii = 0;
    for (var i in data) {
        colNames[ii++] = i;
    }

    var pageHead = "Liquidacion negocio";
    var html = "<html><head><style script='css/text'>table.tableList_1 th { text-align:center; vertical-align: middle; padding:1px; background:#e4fad0;}table.tableList_1 td {text-align: left; vertical-align: top; padding:1px;}</style></head><body><div class='pageHead_1'>" + pageHead + "</div><table border='" + (file == 'pdf' ? '0' : '0') + "' class='tableList_1 t_space' cellspacing='10' cellpadding='0'>" + theads;
    //alert('len'+mya.length);
    for (i = 0; i < mya.length; i++)
    {
        html = html + "<tr>";
        data = jQuery("#tabla_liquidador_neg").getRowData(mya[i]); // get each row
        for (j = 0; j < colNames.length; j++) {
            var isjAdd = true;
            for (var pj = 0; pj < columns.length; pj++) {
                if (j == columns[pj]) {
                    isjAdd = false;
                    break;
                }
            }
            if (isjAdd) {
                html = html + "<td>" + data[colNames[j]] + "</td>"; // output each column as tab delimited
            }
        }
        html = html + "</tr>";  // output each row with end of line

    }
    html = html + "</table></body></html>";  // end of line at the end
    //alert(html);
    document.form_expr.pdfBuffer.value = html;
    document.form_expr.fileType.value = file;
    document.form_expr.method = 'POST';
    document.form_expr.action = './controller?estado=Reestructuracion&accion=Negocios&opcion=14';  // send it to server which will open this contents in excel file
    document.form_expr.submit();
}

function mensajesDelSistema2(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();


}
