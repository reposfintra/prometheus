/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 $(document).ready(function() {
     
    $('.solo-numero').keyup(function() {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
      
   cargarUnidadesNegocio();
   cargarCarteraVencida();
    
    $('#btn_search_negocios').click(function() {
        cargarCarteraVencida();
    });
  
 });
 
 
  function cargarUnidadesNegocio() {
   
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Proceso&accion=Ejecutivo",
        dataType: 'json',
        async:false,
        data: {
            opcion: 20,
            ref_1: "und_proc_ejec"
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#unidad_negocio').append("<option value='0'>Todas</option>");
               
                    for (var key in json) {              
                       $('#unidad_negocio').append('<option value=' + json[key].id + '>' + json[key].descripcion + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                  $('#unidad_negocio').append("<option value='0'>Seleccione</option>");


            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
 
 function cargarCarteraVencida(){
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    var params = { 
        opcion:21,
        idetapa:"0",
        und_negocio:$('#unidad_negocio').val(),
        negocio: $('#negocio').val(),
        cedula: $('#cedula').val()
    };
    if ($("#gview_tabla_cartera").length) {
         refrescarGridCarteraVencida(params);
     }else {
        jQuery("#tabla_cartera").jqGrid({
            caption: 'Listado de Procesos',
            url: url,          
            datatype: 'json',
            height: 450,
            width: 1450,
            colNames: ['Negocio', 'Altura mora', 'F. marcaci�n', /*'Dias transcurridos',*/ 'Cedula','Nombre Cliente','Und Negocio', 'IdConvenio', 'Convenio', 'Pagar�', 'Id Demanda', 'Afiliado comercial', 'Valor negocio', 'Valor saldo', 'Estado cartera'],
            colModel: [
                {name: 'negocio', index: 'negocio', sortable: true, align: 'center', width: '110px', key:true}, 
                {name: 'mora', index: 'mora', sortable: true, align: 'center', width: '110px'},
                {name: 'fecha_marcacion', index: 'fecha_marcacion', sortable: true, align: 'center', width: '110px'},
               /*{name: 'dias_transcurridos', index: 'dias_transcurridos', sortable: true, align: 'center', width: '110px'},*/
                {name: 'cedula', index: 'cedula', sortable: true, align: 'center', width: '120px'},              
                {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '380px'},
                {name: 'und_negocio', index: 'und_negocio', sortable: true, align: 'center', width: '120px'},
                {name: 'id_convenio', index: 'id_convenio', sortable: true, hidden: true, align: 'center', width: '110px'},
                {name: 'convenio', index: 'convenio', sortable: true, align: 'left', width: '130px'}, 
                {name: 'num_pagare', index: 'num_pagare', sortable: true, align: 'center', width: '110px'}, 
                {name: 'id_demanda', index: 'id_demanda', sortable: true, hidden:true, align: 'center', width: '90px'}, 
                {name: 'niter', index: 'niter', sortable: true, align: 'left', width: '380px'},
                {name: 'vr_negocio', index: 'vr_negocio', sortable: true, align: 'right', width: '170px', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},          
                {name: 'valor_saldo', index: 'valor_saldo', sortable: true, align: 'right', width: '170px',sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'estado_cartera', index: 'estado_cartera', sortable: true, align: 'center', width: '120px'}
            ],
            rowNum: 2000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
            pager:'#page_tabla_cartera',
           /* pgtext: null,
            pgbuttons: false,*/
            multiselect:true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: { 
                async:false,              
                type: "POST",                
                data: params
            },
            loadError: function(xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_cartera",{search:false,refresh:false,edit:false,add:false,del:false});
        jQuery("#tabla_cartera").jqGrid("navButtonAdd", "#page_tabla_cartera", {
            caption: "Pasar a f�brica juridica",
            title: "Pasar negocios a la f�brica jur�dica",
            onClickButton: function() {
                pasarAreaJuridica();
            }
        });
        jQuery("#tabla_cartera").jqGrid("navButtonAdd", "#page_tabla_cartera", {
            caption: "Generar Acta",
            title: "Generar acta de entrega de pagar�s",
            onClickButton: function() {
                generarActaEntregaPagares();
            }
        });       
        jQuery("#tabla_cartera").jqGrid("navButtonAdd", "#page_tabla_cartera", {
            caption: "Exportar excel",
            onClickButton: function () {
                var info = jQuery('#tabla_cartera').getGridParam('records');
                if (info > 0) {
                    exportarExcel();
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }

            }

        });
  
     }
}


function refrescarGridCarteraVencida(params){    
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    jQuery("#tabla_cartera").setGridParam({
        datatype: 'json',
        url: url,
        ajaxGridOptions: {
            async:false,
            type: "POST",
            data: params
        }
    });
    
    jQuery('#tabla_cartera').trigger("reloadGrid");
}

function pasarAreaJuridica(){
    
    var listado = "";
    var filasId =jQuery('#tabla_cartera').jqGrid('getGridParam', 'selarrrow');
    if (filasId != ''){
        for (var i = 0; i < filasId.length; i++) {           
            listado += filasId[i] + ",";      
        }    
      
        var url = './controller?estado=Proceso&accion=Ejecutivo';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 22,
                idetapa: '0',
                idetapaNew: '1',
                comentario: '', 
                actualizar_fecha: 'S',
                listado: listado
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {                                           
                        refrescarGridCarteraVencida({
                            opcion:21,
                            und_negocio:$('#unidad_negocio').val()     
                        }); 
                        mensajesDelSistema("Proceso(s) transferido(s) a f�brica jur�dica", '250', '150', true);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo pasar los negocios a la f�brica juridica!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            } 
        });
        
    }else{
        if (jQuery("#tabla_cartera").jqGrid('getGridParam', 'records')>0) {
             mensajesDelSistema("Debe seleccionar los clientes que desea pasar a la f�brica juridica!!", '250', '150');
        }else{
             mensajesDelSistema("No hay clientes para pasar a la f�brica juridica", '250', '150');
        }
             
    }
}



function mensajesDelSistema(msj, width, height, swHideDialog) {   
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}


function  exportarExcel() {
    var fullData = jQuery("#tabla_cartera").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: './controller?estado=Proceso&accion=Ejecutivo',
        data: {
            listado: myJsonString,
            opcion: 58
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function generarActaEntregaPagares(){
    
    var jsonNeg = [];
    var filasId =jQuery('#tabla_cartera').jqGrid('getGridParam', 'selarrrow');
    if (filasId != ''){
        for (var i = 0; i < filasId.length; i++) {           
            var nitDemandado = jQuery('#tabla_cartera').getRowData(filasId[i]).cedula;  
            var nombre = jQuery('#tabla_cartera').getRowData(filasId[i]).nombre;           
            var pagare = jQuery('#tabla_cartera').getRowData(filasId[i]).num_pagare;  
            var valor_saldo = jQuery('#tabla_cartera').getRowData(filasId[i]).valor_saldo;         
            var negs = {};
            negs ["cod_neg"] = filasId[i];
            negs ["nit_demandado"] = nitDemandado;
            negs ["nombre"] = nombre;
            negs ["pagare"] = pagare;
            negs ["valor_saldo"] = valor_saldo;            
            jsonNeg.push(negs);   
        }    
        var listNegocios = {};
        listNegocios ["negocios"] = jsonNeg;
      
        var url = './controller?estado=Proceso&accion=Ejecutivo';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 65,
                listadoNegocios: JSON.stringify(listNegocios)
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {  
                        window.open('.'+json.Ruta);                               
                    }
                } else {
                    mensajesDelSistema("Lo sentimos ocurri� un error al generar el acta!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            } 
        });
        
    }else{
        if (jQuery('#tabla_cartera').jqGrid('getGridParam', 'records')>0) {
             mensajesDelSistema("Debe seleccionar los titulos a incluir en el acta!!", '250', '150');
        }else{
             mensajesDelSistema("No hay datos para generar el acta", '250', '150');
        }
             
    }
}
