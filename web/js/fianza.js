/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){   
   cargarComboGenerico('unidad_negocio', 5);
   cargarComboGenerico('empresa_fianza', 30);
   maximizarventana();
   
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
   
    $('#listarNegocios').click(function () {
       if($('#unidad_negocio').val()===''){
            mensajesDelSistema('Por favor, seleccione la unidad de negocio', '250', '150');
            return;
       }else if($('#agencia_unidad_negocio').val()==='' && ($('#unidad_negocio').val()==='1' || $('#unidad_negocio').val()==='22')){
            mensajesDelSistema('Por favor, seleccione una agencia para la unidad de negocio', '250', '150');
            return;
       }else if($('#empresa_fianza').val()===''){
            mensajesDelSistema('Por favor, seleccione la empresa', '250', '150');
            return;
       }else if($('#periodo_corte').val()===''){
            mensajesDelSistema('Por favor, ingrese el periodo', '250', '150');
            return;
       }else{
           cargarNegociosPendientesPorProcesar();
       }
    });
    
});

function cargarNegociosPendientesPorProcesar(){

    var grid_listar_deducciones_fianza = $("#tabla_deducciones_fianza");
    if ($("#gview_tabla_deducciones_fianza").length) {
         refrescarGridNegociosPendientesPorProcesar();
     }else {
         grid_listar_deducciones_fianza.jqGrid({        
            caption:'DEDUCCIONES FIANZA',
            url: "./controller?estado=Garantias&accion=Comunitarias",
            mtype: "POST",
            datatype: "json",
            height: '380',
            width: '1150',
            colNames: ['Id', 'Nit Cliente', 'Nombre', 'Doc. Relacionado', 'Negocio', 'No Cuotas', 'Convenio', 'Fecha Vencimiento', 'Valor Negocio', 'Valor Desembolso', 'Valor Fianza'],
            colModel: [
                {name: 'id', index: 'id', width: 80, resizable:false, sortable: true, align: 'center', key: true, hidden:true},              
                {name: 'nit_cliente', index: 'nit_cliente', resizable:false, sortable: true, width: 90, align: 'center'},
                {name: 'nombre_cliente', index: 'nombre_cliente', resizable:false, sortable: true, width: 180, align: 'left'},
                {name: 'documento_rel', index: 'documento_rel', resizable: false, sortable: true, width: 110, align: 'center'},
                {name: 'negocio', index: 'negocio', resizable: false, sortable: true, width: 110, align: 'center'},
                {name: 'num_cuotas', index: 'num_cuotas', resizable: false, sortable: true, width: 80, align: 'center'},
                {name: 'id_convenio', index: 'id_convenio', resizable: false, sortable: true, width: 90, align: 'left', hidden:true},      
                {name: 'fecha_vencimiento', index: 'fecha_vencimiento', resizable: false, sortable: true, width: 110, align: 'left'},                       
                {name: 'valor_negocio', index: 'valor_negocio', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_desembolsado', index: 'valor_desembolsado', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_fianza', index: 'valor_fianza', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}              
            ],
            rowNum: 2000,
            rowTotal: 2000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#page_tabla_deducciones_fianza',
            pgtext: null,
            pgbuttons: false,
            multiselect:true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {                
                dataType: "json",
                type: "POST",  
                async:false,
                data: {
                    opcion: 9,
                    id_unidad_negocio:$('#unidad_negocio').val(),
                    agencia:$('#agencia_unidad_negocio').val(),
                    empresa_fianza:$('#empresa_fianza').val(),
                    periodo_corte:$('#periodo_corte').val()
                }
            },  
            gridComplete: function() {             
                var colSumTotal = jQuery("#tabla_deducciones_fianza").jqGrid('getCol', 'valor_negocio', false, 'sum');
                var colSumDesem = jQuery("#tabla_deducciones_fianza").jqGrid('getCol', 'valor_desembolsado', false, 'sum');  
                var colSumFianza = jQuery("#tabla_deducciones_fianza").jqGrid('getCol', 'valor_fianza', false, 'sum');  
                jQuery("#tabla_deducciones_fianza").jqGrid('footerData', 'set', {valor_negocio: colSumTotal});
                jQuery("#tabla_deducciones_fianza").jqGrid('footerData', 'set', {valor_desembolsado: colSumDesem});                  
                jQuery("#tabla_deducciones_fianza").jqGrid('footerData', 'set', {valor_fianza: colSumFianza});                  
            },    
            loadComplete: function () {
                    if (grid_listar_deducciones_fianza.jqGrid('getGridParam', 'records') > 0) {
                        deshabilitarPanelBusqueda(true);
                    }else{
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_deducciones_fianza",{search:false,refresh:false,edit:false,add:false,del:false});      
        jQuery("#tabla_deducciones_fianza").jqGrid("navButtonAdd", "#page_tabla_deducciones_fianza", {
            caption: "Generar CxP", 
            title: "Generar CxP Negocios Seleccionados",           
            onClickButton: function() {  
                procesarNegocios();                     
            }
        });           
        
         //boton que sirve para desbloquear el panel de buqueda
        $("#tabla_deducciones_fianza").navButtonAdd('#page_tabla_deducciones_fianza', {
            caption: "Desbloquear",
            title: "desbloquear",
            onClickButton: function (e) {
                deshabilitarPanelBusqueda(false);
                grid_listar_deducciones_fianza.jqGrid("clearGridData", true);
            }
        });
        //boton que sirve para desbloquear el panel de buqueda
        $("#tabla_deducciones_fianza").navButtonAdd('#page_tabla_deducciones_fianza', {
            caption: "Exportar",
            title: "Exportar",
            onClickButton: function (e) {
                var info = jQuery('#tabla_deducciones_fianza').getGridParam('records');
                if (info > 0) {
                    ExportarCxpConsolidada();
                }else{
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }
                
            }
        });

     }
}


function refrescarGridNegociosPendientesPorProcesar(){    
    jQuery("#tabla_deducciones_fianza").setGridParam({
        url: "./controller?estado=Garantias&accion=Comunitarias",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async:false,
            data: {
                opcion: 9,
                id_unidad_negocio:$('#unidad_negocio').val(),
                agencia:$('#agencia_unidad_negocio').val(),
                empresa_fianza:$('#empresa_fianza').val(),
                periodo_corte:$('#periodo_corte').val()
            }
        }
    }).trigger("reloadGrid");
}

function cargarComboGenerico(idCombo, option) {
    $('#'+idCombo).html('');
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Garantias&accion=Comunitarias",
        dataType: 'json',
        async:false,
        data: {
            opcion: option
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#'+idCombo).append("<option value=''>Seleccione</option>");
                    for (var key in json) {              
                       $('#'+idCombo).append('<option value=' + key + '>' + json[key] + '</option>');                
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#'+idCombo).append("<option value=''>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function procesarNegocios(){
    var colSumFianza = jQuery("#tabla_deducciones_fianza").jqGrid('getCol', 'valor_fianza', false, 'sum'); 
    var jsonNegocios = [];
    var filasId =jQuery('#tabla_deducciones_fianza').jqGrid('getGridParam', 'selarrrow');
    if (filasId != ''){
        for (var i = 0; i < filasId.length; i++) {        
            var negocio = jQuery("#tabla_deducciones_fianza").getRowData(filasId[i]).negocio;
            var valor_fianza = jQuery("#tabla_deducciones_fianza").getRowData(filasId[i]).valor_fianza;      
            var negocios = {};
            negocios ["id"] = filasId[i];
            negocios ["negocio"] = negocio;
            negocios ["valor_fianza"] = valor_fianza;            
            jsonNegocios.push(negocios);   
        }    
        var listNegocios = {};
        listNegocios ["negocios"] = jsonNegocios;       
      
       loading("Espere un momento por favor...", "270", "140");
        setTimeout(function () {
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Garantias&accion=Comunitarias",
                dataType: 'json',
                data: {
                    opcion: 10,
                    id_unidad_negocio:$('#unidad_negocio').val(),
                    agencia:$('#agencia_unidad_negocio').val(),
                    periodo_corte:$('#periodo_corte').val(),
                    listadoNegocios: JSON.stringify(listNegocios),                    
                    valor_cxp_fianza:colSumFianza,
                    empresa_fianza:$('#empresa_fianza').val()
                },
                success: function (json) {             
                    if (!isEmptyJSON(json)) {
                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }

                        if (json.respuesta === "OK") {
                            refrescarGridNegociosPendientesPorProcesar();
                            $("#dialogLoading").dialog('close');
                            deshabilitarPanelBusqueda(false);
                            mensajesDelSistema("Exito al generar cuenta por pagar No:" +json.numCxP, '250', '150', true);
                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Error al generar la cuenta por pagar", '363', '140');
                        }

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos ocurri� un error al generar CXP!!", '250', '150');

                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }, 500);
        
    }else{
        if (jQuery("#tabla_deducciones_fianza").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar el(los) negocio(s) a procesar!!", '250', '150');
        } else {
            mensajesDelSistema("No hay negocios para procesar", '250', '150');
        }             
    } 
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajeConfirmAction(msj, width, height, okAction, id) {  
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function deshabilitarPanelBusqueda(status) {

    var nodes = document.getElementById("div_filtro_negocios").getElementsByTagName('*');
    for (var i = 0; i < nodes.length; i++)
    {
        nodes[i].disabled = status;
    }

}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}


function cargarComboGenerico2(idCombo, option) {
    $('#'+idCombo).html('');
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Garantias&accion=Comunitarias",
        dataType: 'json',
        async:false,
        data: {
            opcion: option,
            id_unidad_negocio:$('#unidad_negocio').val()
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#'+idCombo).append("<option value=''>Seleccione</option>");
                    for (var key in json) {              
                       $('#'+idCombo).append('<option value=' + key + '>' + json[key] + '</option>');                
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#'+idCombo).append("<option value=''>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}



function  ExportarCxpConsolidada() {
    var fullData = jQuery("#tabla_deducciones_fianza").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Garantias&accion=Comunitarias",
        data: {
            opcion: 44,
            listado: myJsonString
            
        },
        success: function () {
           
            cerrarDiv("#divSalidaEx");
             mensajesDelSistema ("Documento generado con exito, puede consultarlo en la carpeta descargas");
        },
        error: function () {
            mensajesDelSistema("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function cerrarDiv(div)
{
    $(div).dialog('close');
}