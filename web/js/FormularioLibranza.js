/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function init(es_nuevo) {
    //alert("es_nuevo: "+es_nuevo);   
    cargarDepartamentos('CO', 'dep_dir');
    $('#dep_dir').val('ATL');  
    cargarCiudad('ATL', "ciu_dir");
    $('#ciu_dir').val('BQ');
    cargarVias('BQ', "via_princip_dir");
    cargarVias('BQ', "via_genera_dir");
    //cargarAsesores();
    cargarBarrios('', '');
    $("#dep_dir").change(function () {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciu_dir");
    });
    $("#ciu_dir").change(function () {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarVias(op, "via_princip_dir");
        cargarVias(op, "via_genera_dir");
    });
    $("#via_princip_dir").change(function () {
        $("#via_genera_dir").val('');
    });
    
    $("#est_civil_nat").change(function () {
       (this.value==='C' || this.value==='U') ? changeStatusRequiredFieldsCony(true):changeStatusRequiredFieldsCony(false);         
    });    
    
    ValidarCompraCartera();
    cargarOcupaciones('ocupations', '');
    if(es_nuevo) {
        //getNegociosLegalizar(false);
    } else {
        if ($('#est_sol').val() !== 'B' && $('#est_sol').val() !== '') {
           $('#guardar_btn').hide();
        }
    }
    
    jQuery("#tabla_simulador_Credito").jqGrid({
        datatype: 'local',
        width: '895',
        height:'auto',
        colNames: ['Fecha','Dias', 'Cuota', 'Saldo Inicial', 'Capital', 'Interes', 'Seguro', 'Cuota Intermediacion','Aval', 'Valor Cuota', 'Saldo Final'],
        colModel: [
            {name: 'fecha', index: 'fecha', width: 80, align: 'center'},
            {name: 'dias', index: 'dias', width: 80, align: 'center',hidden: true},
            {name: 'item', index: 'item', sortable: true, width: 50, align: 'center', key: true},
            {name: 'saldo_inicial', index: 'saldo_inicial', width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
            {name: 'capital', index: 'capital', width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
            {name: 'interes', index: 'interes', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
            {name: 'seguro', index: 'seguro', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
            {name: 'cuota_manejo', index: 'cuota_manejo', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
             {name: 'aval', index: 'aval', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
            {name: 'valor', index: 'valor', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
            {name: 'saldo_final', index: 'saldo_final', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
        ],
        rowNum: 1000,
        rowTotal: 1000,
        loadonce: true,
        rownumWidth: 40,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: false,
        footerrow: true,
        userDataOnFooter: true,
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },loadComplete: function() {
            var tab = jQuery("#tabla_simulador_Credito");
            tab.jqGrid('footerData', 'set', {
                fecha: 'Total:',
                item: tab.jqGrid('getCol', 'item', false, 'max'),
                capital: tab.jqGrid('getCol', 'capital', false, 'sum'),
                interes: tab.jqGrid('getCol', 'interes', false, 'sum'),
                seguro: tab.jqGrid('getCol', 'seguro', false, 'sum'),       
                cuota_manejo: tab.jqGrid('getCol', 'cuota_manejo', false, 'sum'),       
                aval: tab.jqGrid('getCol', 'aval', false, 'sum'),
                valor: tab.jqGrid('getCol', 'valor', false, 'sum')
            });
        },
        loadError: function(xhr, status, error) {
            mensajesDelSistema(error, 250, 0);
        }
    });
    $("#tabla_simulador_Credito").parents('div.ui-jqgrid-bdiv').css("max-height","1800px");
}

function getNegociosLegalizar(todo) {
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 5},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "570",0);
                } else {
                    var ol = $('#lSolicitudes');
                    var li;
                    ol.empty();
                    for (var s in json.solicitudes) {
                        li = '<li>'
                                + '<label style="margin: 0.5em 2em;">' + json.solicitudes[s].numero_solicitud + ' / ' + json.solicitudes[s].cod_neg +'</label>'
                                + '<img alt="buscar" title="ir a formulario" src="/fintra/images/botones/iconos/lupa.gif" style="cursor:pointer;margin: 0em 0.5em;"'
                                + 'onclick="ir_a(' + json.solicitudes[s].numero_solicitud + ')" height="15" width="15">';
                        if (json.solicitudes[s].cod_neg === "") {
                            li += '<img alt="eliminar" title="rechazar solicitud" src="/fintra/images/botones/iconos/eliminar.gif" style="cursor:pointer;margin: 0em 0.5em;"'
                                    + 'onclick="rechazarSolicitud(' + json.solicitudes[s].numero_solicitud + ')" height="15" width="15">';
                        } else {
                            li += '<img alt="ver liquidacion" title="ver liquidacion" src="/fintra/images/botones/iconos/view-info.png" style="cursor:pointer;margin: 0em 0.5em;"'
                                    + 'onclick="cargarLiquidacion(\'' + json.solicitudes[s].cod_neg + '\')" height="15" width="15">';
                        }
                        li += '</li>';

                        ol.append(li);
                    }
                    if (todo) {
                        mensajesDelSistema(json.mensaje, "auto",1);
                    } else {
                        if (json.en_tramite >= 3) {
                            $('#aceptar_btn').hide();
                            $('#guardar_btn').hide();
                            mensajesDelSistema(json.mensaje, "auto",1);
                        } else {
                            $('#aceptar_btn').show();
                            $('#guardar_btn').show();
                        }
                    }
                    console.log(json);
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function cargarCiudades(id_dept, id_ciud, valor) {
    var ciudad = $('#' + id_ciud);
    if (!id_dept || id_dept === '' || id_dept === '...') {
        ciudad.empty();
        ciudad.append('<option value="">...</option>');
        return;
    }
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 2, dept: id_dept},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "270",0);
                } else {
                    ciudad.empty();
                    for (var i in json.ciudades) {
                        try {
                            ciudad.append(
                                    '<option value="' + json.ciudades[i].codigo + '"'
                                    + ((valor === json.ciudades[i].codigo) ? ' selected ' : ' ')
                                    + '>' + json.ciudades[i].valor + '</option>');
                        } catch (exce) {
                        }
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function cargarOcupaciones(id_ocup, valor) {
    var ocupacion = $('#' + id_ocup);
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 3},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "270",0);
                } else {
                    ocupacion.empty();
                    for (var i in json.ocupaciones) {
                         ocupacion.append(
                                '<option value="' + json.ocupaciones[i].codigo + '"'
                                + ((valor === json.ocupaciones[i].codigo) ? ' selected ' : ' ')
                                + '>' + json.ocupaciones[i].valor + '</option>');
                        
                        /*ocupacion.append(
                                '<option value="' + json.rows[i].id + '"'
                                + ((valor === json.rows[i].id) ? ' selected ' : ' ')
                                + '>' + json.rows[i].valor + '</option>');
                        */
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function cargarOcupaciones(id_acti, id_ocup, valor) {
    var ocupacion = $('#' + id_ocup);
    /*if (!id_acti || id_acti === '' || id_acti === '...') {
        ocupacion.empty();
        ocupacion.append('<option value="">...</option>');
        return;
    }*/
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 14, act:id_acti},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "270",0);
                } else {
                    ocupacion.empty();
                    for (var i in json.ocupaciones) {
                         ocupacion.append(
                                '<option value="' + json.ocupaciones[i].codigo + '"'
                                + ((valor === json.ocupaciones[i].codigo) ? ' selected ' : ' ')
                                + '>' + json.ocupaciones[i].valor + '</option>');
                        
                        /*ocupacion.append(
                                '<option value="' + json.rows[i].id + '"'
                                + ((valor === json.rows[i].id) ? ' selected ' : ' ')
                                + '>' + json.rows[i].valor + '</option>');
                        */
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}
/*
function cargarPuedeServir(id_entidad_obligacion, id_ocup, valor) {
    var ocupacion = $('#' + id_ocup);
    if (!id_entidad_obligacion || id_entidad_obligacion === '' || id_entidad_obligacion === '...') {
        ocupacion.empty();
        ocupacion.append('<option value="">...</option>');
        return;
    }
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 3, act: id_entidad_obligacion},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "270",0);
                } else {
                    ocupacion.empty();
                    for (var i in json.ocupaciones) {
                        
                        ocupacion.append(
                                '<option value="' + json.ocupaciones[i].codigo + '"'
                                + ((valor === json.ocupaciones[i].codigo) ? ' selected ' : ' ')
                                + '>' + json.ocupaciones[i].valor + '</option>');
                        
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}
*/
function cargarDetalleObligaciones(id_entidad_obligacion, id_obj_obligacion) {

    var nit_prov = $('#nit_prov' + id_obj_obligacion);
    var tcuenta_prov = $('#tcuenta_prov' + id_obj_obligacion);
    var cuenta_prov = $('#cuenta_prov' + id_obj_obligacion);
    if(!id_entidad_obligacion){
        nit_prov.val("");
        tcuenta_prov.val("");
        cuenta_prov.val("");
        return;
    }
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 11, entidad: id_entidad_obligacion},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "270",0);
                } else {
                    //ocupacion.empty();
                    for (var i in json.entidades_recoger_info) {
                        nit_prov.val(json.entidades_recoger_info[i].nit);
                        tcuenta_prov.val(json.entidades_recoger_info[i].tipo_cuenta);
                        cuenta_prov.val(json.entidades_recoger_info[i].no_cuenta);
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function cargarFechasPago(fecha_negocio, dias_plazo, valor) {
    var fecha_pago = $('#liq_fecha_pago');
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 10, fnegocio:fecha_negocio, plazo:dias_plazo},
        async: false,
        success: function (json) {
            try {
                fecha_pago.empty();
                if (json.error) {
                    mensajesDelSistema(json.error, "270",0);
                } else {
                    for (var i in json) {
                        fecha_pago.append(
                                '<option value="' + i + '"'
                                + ((valor === i) ? ' selected ' : ' ')
                                + '>' + json[i] + '</option>');
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function calcularFechaPago(fecha_negocio, pagaduria_form) {
    
    //alert("fecha_negocio: "+fecha_negocio+" "+"pagaduria_form: "+pagaduria_form);
    //$('#liq_fecha_pago').val("Ves que si1");
    //$('#liq_pl_cuota_1').val("Ves que si2");
    var fecha_pago = $('#liq_fecha_pago'), PlazoPriCta = $('#liq_pl_cuota_1');
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'POST',
        data: {opcion: 13, FechaNeg: fecha_negocio, Pagaduria: pagaduria_form},
        async: true,
        success: function (json) {
            //alert("Responde!");
            try {
                //fecha_pago.empty();
                //PlazoPriCta.empty();
                if (json.error) {
                    mensajesDelSistema(json.error, "270",0);
                } else {
                    console.log("Esta cag es: "+json);
                    //for (var i in json.rows) {
                   
                        fecha_pago.val(json[0].fch_pago);
                        PlazoPriCta.val(json[0].plz_1cta);
                        //$('#liq_fecha_pago').val(json[0].fch_Pago);
                        //$('#liq_pl_cuota_1').val(json[0].Plz_1Cta);
                        
                     
                       
                    //}
                    
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}



function setEmpresaSolicitante() {
    var pag = $('#pagaduria option:selected');
    $('#nit_emp_nat').val(pag.val());
    $('#nom_emp_nat').val(pag.text().trim());
}

function calcularTotales() {
    
    var total_ingresos = $('#ting_fin_nat')
            , total_egresos = $('#tegr_fin_nat')
            , total_patrimonio = $('#tpat_fin_nat');
            
    var salario = parseInt($('#sal_fin_nat').val().replace(/,/g,''))
            , honorarios = parseInt($('#hon_fin_nat').val().replace(/,/g,''))
            , otro_ing = parseInt($('#otro_fin_nat').val().replace(/,/g,''))
            , descuentos = parseInt($('#des_fin_nat').val().replace(/,/g,''))
            , arriendo = parseInt($('#hip_fin_nat').val().replace(/,/g,''))
            , prestamo = parseInt($('#pres_fin_nat').val().replace(/,/g,''))
            , otro_gas = parseInt($('#gas_fin_nat').val().replace(/,/g,''))
            , total_activo = parseInt($('#tac_fin_nat').val().replace(/,/g,''))
            , total_pasivo = parseInt($('#tpas_fin_nat').val().replace(/,/g,''));

    total_ingresos.val((salario + honorarios + otro_ing).toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&,'));
    total_egresos.val((descuentos + arriendo + prestamo + otro_gas).toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&,'));
    total_patrimonio.val((total_activo - total_pasivo).toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&,'));
}

function busquedaPersona(elemento) {
    
    var dato = $("#id_" + elemento).val().trim();
    if (!dato) {
        return;
    }
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 0, dato: dato},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "270",0);
                } else {
                    //BASICA
                    $("#f_nac_" + elemento).val(json.basica.fecha_nacimiento);
                    if (!validarEdad('f_nac_' + elemento, 'Persona'))
                        return;

                    $("#pr_apellido_" + elemento).val(json.basica.primer_apellido);
                    $("#seg_apellido_" + elemento).val(json.basica.segundo_apellido);
                    $("#pr_nombre_" + elemento).val(json.basica.primer_nombre);
                    $("#seg_nombre_" + elemento).val(json.basica.segundo_nombre);
                    $("#genero_" + elemento).val(json.basica.genero);
                    $("#tipo_id_" + elemento).val(json.basica.tipo_id);
                    $("#id_" + elemento).val(json.basica.identificacion);
                    $("#dir_" + elemento).val(json.basica.direccion);
                    $("#dep_" + elemento).val(json.basica.departamento);
                    cargarCiudades(json.basica.departamento, "ciu_" + elemento, json.basica.ciudad);
                    $("#barrio_" + elemento).val(json.basica.barrio);
                    $("#estr_" + elemento).val(json.basica.estrato);
                    $("#tipo_viv_" + elemento).val(json.basica.tipo_vivienda);
                    $("#tel_" + elemento).val(json.basica.telefono);
                    $("#cel_" + elemento).val(json.basica.celular);
                    $("#mail_" + elemento).val(json.basica.email);
                    $("#f_exp_" + elemento).val(json.basica.fecha_expedicion_id);
                    $("#dep_exp_" + elemento).val(json.basica.depto_expedicion_id);
                    cargarCiudades(json.basica.depto_expedicion_id, "ciu_exp_" + elemento, json.basica.ciudad_expedicion_id);
                    $("#dep_nac_" + elemento).val(json.basica.depto_nacimiento);
                    cargarCiudades(json.basica.depto_nacimiento, "ciu_nac_" + elemento, json.basica.ciudad_nacimiento);
                    $("#est_civil_" + elemento).val(json.basica.estado_civil);
                    $("#prof_" + elemento).val(json.basica.profesion);
                    $("#niv_est_" + elemento).val(json.basica.nivel_estudio);
                    $("#pcargo_" + elemento).val(json.basica.personas_a_cargo);
                    $("#nhijos_" + elemento).val(json.basica.num_hijos);
                    $("#ngrupo_" + elemento).val(json.basica.grupo_familia);
                    $("#an_res_" + elemento).val(json.basica.anos_residencia);
                    $("#mes_res_" + elemento).val(json.basica.meses_residencia);

                    //LABORAL
                    if (!$("#act_econ_" + elemento).is(':disabled')) {
                        $("#act_econ_" + elemento).val(json.laboral.actividad_economica);
                        cargarOcupaciones(json.laboral.actividad_economica, "ocup_" + elemento, json.laboral.ocupacion);
                    }
                    if (!$("#nit_emp_" + elemento).is(':disabled')) {
                        $("#nom_emp_" + elemento).val(json.laboral.nombre_empresa);
                        $("#nit_emp_" + elemento).val(json.laboral.nit);
                        $("#dig_nit_emp_" + elemento).val(json.laboral.digito_verificacion);
                    }
                    $("#f_ing_" + elemento).val(json.laboral.fecha_ingreso);
                    $("#tipo_cont_" + elemento).val(json.laboral.tipo_contrato);
                    $("#car_emp_" + elemento).val(json.laboral.cargo);
                    $("#dep_emp_" + elemento).val(json.laboral.departamento);
                    cargarCiudades(json.laboral.departamento, "ciu_emp_" + elemento, json.laboral.ciudad);
                    $("#dir_emp_" + elemento).val(json.laboral.direccion);
                    $("#cel_emp_" + elemento).val(json.laboral.celular);
                    $("#dir_cob_" + elemento).val(json.laboral.direccion_cobro);
                    $("#mail_emp_" + elemento).val(json.laboral.email);
                    $("#tel_emp_" + elemento).val(json.laboral.telefono);
                    $("#ext_emp_" + elemento).val(json.laboral.extension);

                    $("#eps_" + elemento).val(json.laboral.eps);
                    $("#tip_afil_" + elemento).val(json.laboral.tipo_afiliacion);

                    $("#sal_" + elemento).val(json.laboral.salario);
                    $("#otros_" + elemento).val(json.laboral.otros_ingresos);
                    $("#conc_otros_" + elemento).val(json.laboral.conceptos_otros_ingresos);
                    $("#manuten_" + elemento).val(json.laboral.gastos_manutencion);
                    $("#cred_" + elemento).val(json.laboral.gastos_creditos);
                    $("#arr_" + elemento).val(json.laboral.gastos_arriendo);
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {

        }
    });
}

function habilitarTransacciones() {
    
    var efectua = $('#trans_ext_nat')
      , pagos = $('#pag_ext_nat')
      , importa = $('#impor_ext_nat')
      , exporta = $('#expor_ext_nat')
      , invierte = $('#inver_ext_nat')
      , gira = $('#giro_ext_nat')
      , presta = $('#presta_ext_nat')
      , otro = $('#otro_ext_nat')
      , banco = $('#banco_ext_nat')
      , cuenta = $('#cuenta_ext_nat')
      , pais = $('#pais_ext_nat')
      , ciudad = $('#ciu_ext_nat')
      , moneda = $('#mon_ext_nat')
      , producto = $('#prod_ext_nat')
      , monto = $('#monto_ext_nat');
    
    if(efectua.is(":checked")) {
        
      importa.removeAttr("disabled");
      exporta.removeAttr("disabled");
      invierte.removeAttr("disabled");
      gira.removeAttr("disabled");
      presta.removeAttr("disabled");
      otro.removeAttr("disabled");
      
      pagos.removeAttr("disabled");
      pagos.prop('checked', true);
      
    } else {
        
      importa.attr("disabled","disabled");
      exporta.attr("disabled","disabled");
      invierte.attr("disabled","disabled");
      gira.attr("disabled","disabled");
      presta.attr("disabled","disabled");
      otro.attr("disabled","disabled");
      
      pagos.attr("disabled","disabled");
      pagos.prop('checked', false);
    }
    
    if(pagos.is(":checked")) {
        
      banco.removeAttr("disabled");
      cuenta.removeAttr("disabled");
      pais.removeAttr("disabled");
      ciudad.removeAttr("disabled");
      moneda.removeAttr("disabled");
      producto.removeAttr("disabled");
      monto.removeAttr("disabled");
      
    } else {
        
      banco.attr("disabled","disabled");
      cuenta.attr("disabled","disabled");
      pais.attr("disabled","disabled");
      ciudad.attr("disabled","disabled");
      moneda.attr("disabled","disabled");
      producto.attr("disabled","disabled");
      monto.attr("disabled","disabled");
   
    }
}

/*********************
 * Guardar formulario
 *********************/
function validar(estado) {
    //alert("estado: "+estado);
    var camposInvalidos = []
      , codeu1 = document.getElementById('codeu_1').checked
      , codeu2 = document.getElementById('codeu_2').checked;
    
    var info = form2object('formulario', '.', false, 
        function (node) {
            //alert("node: "+node.id);
            if(!node.id) return;
            
            if(!codeu1){
                if(node.id.match(/_cod_1$/)) return;
            } 
            if(!codeu2){
                if(node.id.match(/_cod_2$/)) return;
            }
            
            if (node.required && !node.validity.valid) {
                //console.log(node.validity.valid + ' :: ' +node.id);
                camposInvalidos.push(node.id);
                node.class = 'validation-failed';
            } else {
                node.class = '';
            }
        });
    console.log(info.form);
    //alert("Que trae el form"+info.form);
    //alert("Cantidad de items del form: "+camposInvalidos.length);
    if(camposInvalidos.length > 0) {
        mensajesDelSistema('Los campos resaltados en rojo, son obligatorios ' , "270", 0);
        return;
    } else {
        guardar(estado);
    }
}

function guardar(estado) {
    //alert("estado: "+estado);
    var info = form2object('formulario', '.', false, function (node) { if(!node.id) return; });
    console.log(info);
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'post',
        data: {opcion: 4, formulario: JSON.stringify(info.form)}, 
        async: true,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "570", 0);
                } else {
                    //actualiza formulario
                    $('#num_solicitud').val(json.numero_solicitud);
                    $('#actividad').val(json.actividad);
                    $('#est_sol').val(json.estado_solicitud);
                    
                    if(estado === 'P') {
                        if(json.codigo_negocio) {
                            cargarLiquidacion(json.codigo_negocio);
                        } else { 
                            mensajesDelSistema(json, "420", 2);
                        }
                    } else {
                        mensajesDelSistema('Informacion guardada, su numero de formulario es ' + json.numero_solicitud , "270", 0);
                    }
                }
            } catch (exc) {
                mensajesDelSistema(exc, "570",0);
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function rechazarSolicitud(num) {
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'post',
        data: {opcion: 6, numero_solicitud: num},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "570",0);
                } else {
                    getNegociosLegalizar(true);
                }
            } catch (exc) {
                mensajesDelSistema(exc, "570",0);
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function guardarLiquidacion() {
    //alert("Entra a guardarLiquidacion");
    if (!$('#liq_num_sol').val()) return;
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'post',
        data: {opcion: 8, info:JSON.stringify({numero_solicitud: $('#liq_num_sol').val(), fecha_primera_cuota:$('#liq_fecha_pago').val(), valor_fianza: $('#liq_valor_fianza').val()})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "570",0);
                } else {
                    $('#est_sol').val("P");
                    $('#negocio').val(json.codigo_negocio);
                    $('#guardar_btn').hide();
                    
                    mensajesDelSistema(json.mensaje, "270",0);
                }
            } catch (exc) {
                mensajesDelSistema(exc, "570",0);
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function win_concepto(form, negocio, actividad){
    //alert("form"+form+" Negocio: "+negocio+" actividad: "+actividad);
    //$('#concepto_dialogo').load("/fintra/jsp/fenalco/negocios/Conceptos.jsp?negocio="+negocio+"&form="+form+"&act="+actividad).dialog({modal:true}).id = 'concepto_dialogo';
    window.location.href = "/fintra/jsp/fenalco/negocios/Conceptos.jsp?negocio="+negocio+"&form="+form+"&act="+actividad;
}

/******************
 * Opciones varias
 ******************/
function ir_a(num_solicitud) {
    window.location.href = "/fintra/jsp/fenalco/avales/formulario_libranza.jsp?num_solicitud=" + num_solicitud;
}
function mensajesDelSistema(msj, width, tipo) {
    var botones, title;
    $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    switch (tipo) {
        case 0: //mensaje
            title = "mensaje";
            $('#msj').show();
            $('#lSolicitudes').hide();
            $('#liquidador').hide();
            botones = { "Aceptar": function () {  $(this).dialog("close");  } };
            break;
        case 1: //lista solicitudes
            title = "Lista de Solicitud";
            $('#msj').show();
            $('#lSolicitudes').show();
            $('#liquidador').hide();
            botones = { "Salir": function () {  $(this).dialog("close");  } };
            break;
        case 2: //liquidador
            title = "Liquidador";
            $('#msj').hide();
            $('#lSolicitudes').hide();
            $('#liquidador').show();
            
            //setea valores para la liquidacion
            if(msj) {
                $('#liq_num_sol').val(msj.numero_solicitud);
                $('#liq_convenio').val(msj.convenio);
                $('#liq_cod_neg').val(msj.codigo_negocio);
                $('#liq_cuotas').val(msj.numero_cuotas);
                $('#liq_valor_negocio').val(msj.valor_negocio);
                $('#liq_valor_fianza').val(msj.valor_fianza);
                $('#liq_fec_negocio').val(msj.fecha_negocio);
                //$('#liq_pl_cuota_1').val(msj.plazo_primera_cuota);
            
                //cargarFechasPago(msj.fecha_negocio, msj.plazo_primera_cuota);
                calcularFechaPago(msj.fecha_negocio, $('#pagaduria').val());
            }
            botones = { 
                        "Aceptar": function () {  cargarLiquidacion(); $(this).dialog("close");  },
                        "Salir": function () {  $(this).dialog("close");  }
                      };
            break;
    }
    $("#dialogo").dialog({
        open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
        width: width,
        height: "auto",
        show: "scale",
        hide: "scale",
        title: title,
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: botones //crear bot�n de cerrar
    });
}

function cargarLiquidacion(codigo_negocio) {
    /*alert("Entra a cargarLiquidacion");
    
    alert("valor_desembolso: "+$('#liq_valor_negocio').val());
    alert("num_cuotas: "+$('#liq_cuotas').val());
    alert("convenio: "+$('#liq_convenio').val());
    alert("tipo_cuota: "+$('#liq_tipo_cuota').val());
    alert("fecha_calculo: "+$('#liq_fec_negocio').val());
    alert("fecha_primera_cuota: "+$('#liq_fecha_pago').val());*/
    
    var grid_simulador_credito = jQuery("#tabla_simulador_Credito");
    var info, botones;
    if(!codigo_negocio) {
        info = "/fintra/controller?estado=Formulario&accion=Libranza&opcion=7&info="+JSON.stringify( {
                     valor_desembolso:$('#liq_valor_negocio').val()
                    ,num_cuotas:$('#liq_cuotas').val()
                    ,convenio:$('#liq_convenio').val()
                    ,tipo_cuota:$('#liq_tipo_cuota').val()
                    ,fecha_calculo:$('#liq_fec_negocio').val()
                    ,fecha_primera_cuota:$('#liq_fecha_pago').val()
                    ,num_solicitud : $('#liq_num_sol').val()
                });
        botones = {
            "Aceptar": function () {  guardarLiquidacion(); $(this).dialog("close"); },
            "Regresar": function () {  mensajesDelSistema(null, "400", 2); $(this).dialog("close"); },
            "Salir": function () {  $(this).dialog("close"); }
        };
    } else {
        info = "/fintra/controller?estado=Formulario&accion=Libranza&opcion=9&codigo_negocio="+codigo_negocio;
        botones = { "Salir": function () {  $(this).dialog("close"); } };
    }
    grid_simulador_credito.setGridParam({
        datatype: 'json',
        mtype: "GET",
        url: info
    });
    grid_simulador_credito.trigger("reloadGrid");

    $("#grid_liquidacion").dialog({
        open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
        width: "auto",
        height: "auto",
        show: "scale",
        hide: "scale",
        title: "Liquidacion Negocio",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: botones //crear bot�n de cerrar
    });
}

/*function genDireccion(elemento) {
    $('#dir_resul').val(elemento.value);
    $("#direccion_dialogo").dialog({
        // open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
        width: "auto",
        height: "auto",
        show: "scale",
        hide: "scale",
        title: "Direccion",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {  
                $('#'+elemento).val($('#dir_resul').val());
                setDireccion(0);
                $(this).dialog("close"); },
            "Salir": function () { 
                setDireccion(0);
                $(this).dialog("close");
            }
        }
    });
}

/***********
 * validaciones de textos digitados.
 ***********/
/*function setDireccion(orden) {
    switch (orden) {
        default:
        case 0:
            $('#via_princip_dir').val("");
            $('#nom_princip_dir').val("");
            $('#via_genera_dir').val("");
            $('#nom_genera_dir').val("");
            $('#placa_dir').val("");
            $('#cmpl_dir').val("");
            break;
        case 1: break;
        case 2:
            var p = $('#via_princip_dir').val()
              , g = document.getElementById('via_genera_dir')
              , opcion;
            
            for (var i = 0; i < g.length; i++) {
                opcion = g[i];
                if (opcion.value === p) {
                    opcion.style.display = 'none';
                } else {
                    opcion.style.display = 'block';
                }
            }
            break;
    }
    if(!$('#via_princip_dir').val() || !$('#nom_princip_dir').val()
      || !$('#via_genera_dir').val() || !$('#nom_genera_dir').val()
      || !$('#placa_dir').val() ) {
        $('#dir_resul').val("");
        $('#dir_resul').attr("class","validation-failed");
    } else {
        $('#dir_resul').removeAttr("class");
        $('#dir_resul').val(
              $('#via_princip_dir').val()
            + ' '  + $('#nom_princip_dir').val().trim().toUpperCase()
            + ' '  + $('#via_genera_dir').val().trim().toUpperCase()
            + ' '  + $('#nom_genera_dir').val().trim().toUpperCase()
            + ' '  + $('#placa_dir').val().trim().toUpperCase()
            + ', ' + $('#cmpl_dir').val().trim().toUpperCase());
    }
}*/

function validarEdad(campo_id, nombre) {
    var campo = document.getElementById(campo_id);
    var fecha = new Date(campo.value.replace(/-/ig, "/"))
            , hoy = new Date()
            , diff = hoy.getTime() - fecha.getTime()
            , ed = parseInt((diff) / 365.2422 / 24 / 60 / 60 / 1000);
    if (!(ed >= 18 && ed <= 75)) {
        mensajesDelSistema(nombre + " excede la edad para ser sujeto de credito", "270", 0);
        campo.value = '';
        return false;
    }
    return true;
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    } return true;
}

function soloNumeros(id) {
    var valor = document.getElementById(id).value;
    valor = valor.replace(/[^0-9^.]+/gi, "");
    document.getElementById(id).value = valor;
}

function soloLetras(e) {
    var key = e.keyCode || e.which
            , tecla = String.fromCharCode(key).toLowerCase()
            , letras = " �����abcdefghijklmn�opqrstuvwxyz"
            , especiales = [8, 9, 37, 39, 46];
    var tecla_especial = false;
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }
    if (letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function conMinusculas(field) {
    field.value = field.value.toLowerCase();
}

function formato(obj, decimales) {
    var numero = obj.value.replace(new RegExp(",", "g"), "");
    var nums = (new String(numero)).split('.');
    var salida = new String();
    var TieneDec = numero.indexOf('.');
    var dato = new String();
    if (TieneDec != -1) {
        var deci = numero.split('.');
        var dec = (deci[1].length > 2) ? deci[1].charAt(2) : deci[1].substr(0, deci[1].length);

        if (dec > 5) {
            dato = (parseInt(deci[1].substr(0, 2)) + 1);
            if (dato > 100) {
                nums[0] = new String(parseInt(nums[0]) + 1);
                obj.value = nums[0] + '';
            } else {
                for (var i = nums[0].length - 1, j = 0; i >= 0; salida = nums[0].charAt(i) + (j % 3 == 0 && j != 0? ',':'') + salida, i--, j++)
                    ;
                obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' + ((nums[1].length > 2) ? ((nums[1].charAt(2) > 9) ? (parseInt(nums[1].substr(0, 2)) + 1) : nums[1].substr(0, 2)) : (nums[1].length == 1) ? nums[1].substr(0, 1) + '0' : nums[1].substr(0, nums[1].length)) : '');
            }
        } else {
            for (var i = nums[0].length - 1, j = 0; i >= 0; salida = nums[0].charAt(i) + (j % 3 == 0 && j != 0? ',':'') + salida, i--, j++)
                ;
            obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' + ((nums[1].length > 2) ? ((nums[1].charAt(2) > 5) ? (parseInt(nums[1].substr(0, 2)) + 1) : nums[1].substr(0, 2)) : (nums[1].length == 1) ? nums[1].substr(0, 1) + '0' : nums[1].substr(0, nums[1].length)) : '');
        }
    } else {
        for (var i = nums[0].length - 1, j = 0; i >= 0; salida = nums[0].charAt(i) + (j % 3 == 0 && j != 0? ',':'') + salida, i--, j++)
            ;
        obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' + ((nums[1].length > 2) ? ((nums[1].charAt(2) > 5) ? (parseInt(nums[1].substr(0, 2)) + 1) : nums[1].substr(0, 2)) : (nums[1].length == 1) ? nums[1].substr(0, 1) + '0' : nums[1].substr(0, nums[1].length)) : '');
    }
}

function ValidarTipoDesembolso(IdBotton) {

    if ( IdBotton === 'TipoDesemCuenta' ){
        $("#EntidadDesembolso").attr('disabled',false);
    }else if ( IdBotton === 'TipoDesemCheque' ){
        $("#EntidadDesembolso").attr('disabled',true);
    }
    
}

function ValidarCompraCartera() {
    
    var max_obligaciones = $('#max_obligaciones');
    
    if (( $("#destino_credito").val() !== 'CC') && ( $("#destino_credito").val() !== 'SF')) { 
    
        var _entidad;
        var _nit_prov;
        var _tcuenta_prov;
        var _cuenta_prov;
        var _valor_recoger;
        $('#btn_new_oblig').hide();

        for (i = 1; i <= max_obligaciones.val(); i++) {

            _entidad = "entidad"+i;
            _nit_prov = "nit_prov"+i;
            _tcuenta_prov = "tcuenta_prov"+i;
            _cuenta_prov = "cuenta_prov"+i;
            _valor_recoger = "valor_recoger"+i;
            
            if($("#nuevo").val()==='S') $("#"+_valor_recoger).val('0');
            $("#"+_entidad).attr('disabled',true);
            $("#"+_nit_prov).attr('disabled',true);
            $("#"+_tcuenta_prov).attr('disabled',true);
            $("#"+_cuenta_prov).attr('disabled',true);
            $("#"+_valor_recoger).attr('disabled',true);
            

        }
        
    }else{
        
        $('#btn_new_oblig').show();
        for (i = 1; i <= max_obligaciones.val(); i++) {

            _entidad = "entidad"+i;
            _nit_prov = "nit_prov"+i;
            _tcuenta_prov = "tcuenta_prov"+i;
            _cuenta_prov = "cuenta_prov"+i;
            _valor_recoger = "valor_recoger"+i;

            $("#"+_entidad).attr('disabled',false);
            //$("#"+_nit_prov).attr('disabled',false);
            //$("#"+_tcuenta_prov).attr('disabled',false);
            //$("#"+_cuenta_prov).attr('disabled',false);
            $("#"+_valor_recoger).attr('disabled',false);

        }
    }
}

function busquedaFiltro(id_filtro) {
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=FiltroLibranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 9, id_filtro: id_filtro},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "270",0);
                } else {
                    //CREDITO
                    $("#valor_solicitado").val(json.valor_solicitado);
                    document.getElementById("valor_solicitado").value = formato(document.getElementById("valor_solicitado").value).moneda;
                    $("#plazo").val(json.plazo);
                    
                    //BASICA
                    $("#f_nac_nat").val(json.fecha_nacimiento);
                    //if (!validarEdad('f_nac_nat', 'Persona'))
                    //    return;

                    $("#pagaduria").val(json.doc_pagaduria);
                    $("#pr_apellido_nat").val(json.primer_apellido);
                    $("#seg_apellido_nat").val(json.segundo_apellido);
                    $("#pr_nombre_nat").val(json.primer_nombre);
                    $("#seg_nombre_nat").val(json.segundo_nombre);
                    $("#tipo_id_nat").val("CED");
                    $("#id_nat").val(json.identificacion);
                    $("#tel_nat").val(json.telefono);
                    $("#cel_nat").val(json.celular);
                    $("#est_civil_nat").val(json.estado_civil);

                    //$("#ocupations").val(json.actividad_economica);
                    $("#f_ing_nat").val(json.fecha_ocup_laboral);
                    
                    $("#sal_fin_nat").val(json.salario);
                    document.getElementById("sal_fin_nat").value = formato(document.getElementById("sal_fin_nat").value).moneda;
                    $("#otro_fin_nat").val(json.otros_ingresos);
                    document.getElementById("otro_fin_nat").value = formato(document.getElementById("otro_fin_nat").value).moneda;
                    $("#des_fin_nat").val(json.desc_ingreso);
                    document.getElementById("des_fin_nat").value = formato(document.getElementById("des_fin_nat").value).moneda;
                    calcularTotales();
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {

        }
    });
}

function formato(valor){
    var vaux;
    try {
        valor = valor.toString().replace(new RegExp(',','g'),'');
        var pattern =/\S+/;
        if (pattern.test( valor )) {
            pattern = /^-?(\d+\.?\d*)$|(\d*\.?\d+)$/;
            if(pattern.test( valor )) {
                vaux = parseFloat(valor);
            } else {
                vaux = 0;
            }
        } else {
            vaux = 0;
        }
    } catch(exc) {
        vaux = 0;
        console.log('error '+exc);
    }
    
    return {
        moneda:vaux.toString().replace(/(\d)(?=(\d{3})+(\.|$))/g, '$1,')
      , numero:vaux
      , porcentaje:vaux/100
    }
}

function val_obligaciones() {
    var tipo = $('input[name=form.cabecera.tipo_desembolso]:checked', '#formulario').val();
    var max_obligaciones = $('#max_obligaciones').val();
    var valor = formato($('#valor_solicitado').val()).numero;
    var ocupacion = $('#ocupations').val();
    var plazo = $('#plazo').val();
    var fianza = $('#fianza').val();
    if (!valor || valor === '0') {
        return;
    }
    var obligaciones = 0, obI = 0, CantidadObligaciones = 0;
    for (i = 1; i <= max_obligaciones; i++) {
        try {
            obI = formato($('#valor_recoger'+i).val()).numero;
            if ( $('#valor_recoger'+i).val() != 0 ){
                CantidadObligaciones++;
            }
        } catch (exception) {
            obI = 0;
        }
        obligaciones += obI;
    }
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 12, tipo: tipo, ocup:ocupacion, vlr_sol: valor, oblig: obligaciones, Cantoblig: CantidadObligaciones, plazo: plazo, fianza: fianza },
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "270",0);
                } else {
                    if(!json.valido) {
                        for (i = 1; i <= max_obligaciones; i++) { $('#valor_recoger'+i).val(0); }                        
                        mensajesDelSistema(json.mensaje, "270",0);
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });    
}


function cargarDepartamentos(codigo, combo) {
  if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "/fintra/controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 13,
                cod_pais: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#'+combo).append("<option value=''>Seleccione</option>");                 

                        for (var key in json) {
                            $('#'+combo).append('<option value=' + key + '>' + json[key] + '</option>');                      
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
  }
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "/fintra/controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function cargarVias(codciu, combo) {
    //alert(codciu);

        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "/fintra/controller?estado=GestionSolicitud&accion=Aval",
            dataType: 'json',
            data: {
                opcion: 'cargarvias',
                ciu: codciu
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''></option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } 
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

}

function Posicionar_div(id_objeto,e){
  obj = document.getElementById(id_objeto);
  var posx = 0;
  var posy = 0;
  if (!e) var e = window.event;
  if (e.pageX || e.pageY) {
   posx = e.pageX;
   posy = e.pageY;
  }
  else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
      }
      else
       alert('ninguna de las anteriores');

 obj.style.left = posx+'px';
 obj.style.top = posy+'px';
 obj.style.zIndex = 1300;

}

function genDireccion(elemento,e) {
  
    var contenedor = document.getElementById("direccion_dialogo")
      , res = document.getElementById("dir_resul");
     
    $("#direccion_dialogo").draggable({ handle: "#drag_direcciones"});
    Posicionar_div("direccion_dialogo",e); 
//    contenedor.style.left = 515 + 'px';  
    contenedor.style.display = "block";

    res.name = elemento;    
    res.value = (elemento.value) ? elemento.value : '';
    
}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3: 
            var res = document.getElementById('dir_resul')
              , des = document.getElementById(res.name);
              des.value = res.value;
        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");
            
            document.getElementById("direccion_dialogo").style.display = "none";           
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
              , g = document.getElementById('via_genera_dir')
              , opcion;
            for (var i = 0; i < g.length; i++) {
                
                opcion = g[i];
                
                if (opcion.value === p) {
                    
                    opcion.style.display = 'none';
                    opcion.disabled = true;
                    
                } else {
                    
                    opcion.disabled = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if(!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                || !jQuery('#placa_dir').val() ) {
                  jQuery('#dir_resul').val("");
                  jQuery('#dir_resul').attr("class","validation-failed");
              } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                    jQuery('#via_princip_dir option:selected').text()
                   + ' '  + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                   + ' '  + jQuery('#via_genera_dir option:selected').text().trim()
                   + ' '  + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                   + ((jQuery('#via_genera_dir option:selected').text().toUpperCase()==='#' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='TRANSVERSAL') ?  '-':  ' ')
                   + jQuery('#placa_dir').val().trim().toUpperCase()
                   + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
              }
            break;
    }
}

function validaFiltroSolicitud(elemento) {
    var dato = $("#id_" + elemento).val().trim();
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=FiltroLibranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 12, identificacion: dato},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "270",0);
                } else {
                    if (typeof json.id_filtro !== 'undefined') {                      
                        busquedaFiltro(json.id_filtro);
                    }else{
                        busquedaPersona(elemento);
                    }              
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {

        }
    });
}


function changeStatusRequiredFieldsCony(estado){
    $("#tipo_id_nat_conyuge").attr({required: estado});
    $("#id_nat_conyuge").attr({required: estado});
    $("#pr_apellido_nat_conyuge").attr({required: estado});
    $("#seg_apellido_nat_conyuge").attr({required: estado});
    $("#pr_nombre_nat_conyuge").attr({required: estado});
    $("#tel_nat_conyuge").attr({required: estado});
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function addObligs(nueva_fila, id_obj_obligacion) {
    var max_obligaciones = $('#max_obligaciones');
    
    if(nueva_fila) {
        var fila = $('#obligacion_' + (parseInt(max_obligaciones.val())+1));
        if (fila) {
            max_obligaciones.val(parseInt(max_obligaciones.val())+1);
            $('#obligacion_' + max_obligaciones.val()).css("display","");
        }
    } else {
        var j = 0;
        for(var i = id_obj_obligacion; i < max_obligaciones.val(); i++) {
            j = i+1;
            $('#nit_prov' + i).val($('#nit_prov' + j).val());
            $('#tcuenta_prov' + i).val($('#tcuenta_prov' + j).val());
            $('#cuenta_prov' + i).val($('#cuenta_prov' + j).val());
            $('#valor_recoger' + i).val($('#valor_recoger' + j).val());
        }
        $('#nit_prov' + max_obligaciones.val()).val("");
        $('#tcuenta_prov' + max_obligaciones.val()).val("");
        $('#cuenta_prov' + max_obligaciones.val()).val("");
        $('#valor_recoger' + max_obligaciones.val()).val("0");
        
        $('#obligacion_' + max_obligaciones.val()).css("display","none");
        max_obligaciones.val(parseInt(max_obligaciones.val())-1);
    }
}

function validarConfiguracionFianzaLibranza(){
    if($("#fianza").val() ==='S'){    
        $.ajax({
            async: false,
            type: 'POST',
            url: "/fintra/controller?estado=Liquidador&accion=Negocios",
            dataType: 'text',
            data: {
                opcion: 'existeConfigFianza',
                idConvenio: '38',
                plazo: $('#plazo').val()
            },
            success: function (response) {
                if(response.startsWith('NO')){
                   alert("No existe valor de factor de fianza configurado para dicho plazo y linea de negocio");
                    $('#plazo').val('');
                    $('#plazo').focus();
                }               
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }         
}

function cargarAsesores() {
    $.ajax({
        type: 'POST',
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        data: {
            opcion: 79,
            id_combo: 'asesores',
            producto:'5'
        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                   $('#asesores').html('');
                    $('#asesores').append('<option value="" ></option>');
                    for (var datos in json) {
                        $('#asesores').append('<option value="' + datos + '">' + json[datos] + '</option>');
                    }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarBarrios(id_ciudad, ciudad){
    console.log(ciudad);
    var codciu = $('#' + id_ciudad).val();
    $.ajax({
        type: 'POST',
        url: "/fintra/controller?estado=Admin&accion=Fintra",
        data: {
            opcion: 84,
            ciudad: codciu

        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#barrio_nat').html('');
                $('#barrio_nat').append('<option value="" ></option>');
                for (var datos in json) {
                    if(json[datos]==ciudad){
                         $('#barrio_nat').append('<option selected value="' + json[datos] + '">' + json[datos] + '</option>');
                    }else{
                         $('#barrio_nat').append('<option value="' + json[datos] + '">' + json[datos] + '</option>');
                    }
                   
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}