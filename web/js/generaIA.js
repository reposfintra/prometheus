/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
   

    $("#generaria").click(function () {
        alert("entro");
        generarIA();
    });


});


function generarIA() {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructurar&accion=Fenalco",
        dataType: 'json',
        data: {
            opcion: 18,
            idRop: $("#nro_extracto").val()
        },
        success: function (json) {

            if (json.respuesta === 'OK') {//todos
                mensajesDelSistema("Se ha generado la nota y la cartera exitozamente", '320', '170');
            } else {
                mensajesDelSistema("No se pudo generar la nota", '300', '180');
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}


function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}