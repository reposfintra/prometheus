/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* global pie_tabla_, tabla_reporte_produccion_ */

var tabla_reporte_produccion_;
var pie_tabla_;
$(document).ready(function () {
    $('#wast').mouseover(function () {
        var src = '/fintra/images/help_2.png';
        $(this).attr("src", src);
    }).mouseout(function () {
        var src = '/fintra/images/help_.png';
        $(this).attr("src", src);
    });
    $('#buscar').click(function () {
        var filtro_ = $('#filtro_').val();
        if (filtro_ !== 'otro') {
            $(".estado_").css("display", "none");
            $("#tablita").css("width", "300px");
            $("#tablainterna").css("width", "271px");
        } else {
            $(".estado_").css("display", "none");
            $("#tablita").css("width", "672px");
            $("#tablainterna").css("width", "677px");
        }
        var periodo_inicio = $('#periodo_inicio').val();
        var periodo_fin = $('#periodo_fin').val();
        var paso = saberPaso();
        if ((filtro_ !== 'otro' && (periodo_inicio === '' || periodo_fin === '')) || (filtro_ === 'otro' && (periodo_inicio !== '' && periodo_fin !== ''))) {
            cargando_toggle();
            switch (paso) {
                case 'paso1':
                    cargarProntoPagopaso1('paso1', 'tabla_reporteProduccion', 'pager4', 'anulado_1');
                    break;
                case  'paso2':
                    cargarProntoPagopaso2('paso2', 'tabla_reporteProduccionPaso2', 'pager5', 'anulado_2');
                    break;
                case 'paso3':
                    cargarProntoPagopaso3('paso3', 'tabla_reporteProduccionPaso3', 'pager6', 'anulado_3');
                    break;

            }
        } else {

            mensajesDelSistema('Debe digitar el rando de periodos', '300', 'auto', false);
        }
    });

    $('#atpaso1').click(function () {
        $(".estado_").css("display", "block");
        $("#tablita").css("width", "564px");
        $("#tablainterna").css("width", "566px");
        $('#paso_busqueda').val('atpaso1');
        tabla_reporte_produccion_ = 'tabla_reporteProduccion';
        pie_tabla_ = 'pager4';
    });
    $('#atpaso2').click(function () {
        $(".estado_").css("display", "block");
        $("#tablita").css("width", "564px");
        $("#tablainterna").css("width", "566px");
        $('#paso_busqueda').val('atpaso2');
        tabla_reporte_produccion_ = 'tabla_reporteProduccionPaso2';
        pie_tabla_ = 'pager5';
    });
    $('#atpaso3').click(function () {
        $(".estado_").css("display", "block");
        $("#tablita").css("width", "564px");
        $("#tablainterna").css("width", "566px");
        $('#paso_busqueda').val('atpaso3');
        tabla_reporte_produccion_ = 'tabla_reporteProduccionPaso3';
        pie_tabla_ = 'pager6';
    });

    $('#wast').click(function () {
        var paso_busqueda = $('#paso_busqueda').val();
        if (paso_busqueda.startsWith('atpaso')) {
            ventanaMostrarhelp();
        } else {
            mensajesDelSistema("Debe seleccionar una pesta�a", '250', '150', false);
        }

    });
});

function soloNumeros(e, num) {
    var keynum = window.event ? window.event.keyCode : e.which;
    //if (document.getElementById(num.id).value.indexOf('.') !== -1 && keynum === 46)
    //  return false;
    if ((keynum === 0 || keynum === 8 || keynum === 48 || keynum === 46)) {
        return true;
    }
    if (keynum >= 58) {
        return false;
    }
    return /\d/.test(String.fromCharCode(keynum));
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function openTAb(evt, idTab) {
    // Declare all variables
    var i, tabcontent, tablinks, tabla_filtro;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");

    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById("tablita").style.display = "block";
    document.getElementById(idTab).style.display = "block";

    evt.currentTarget.className += " active";
}

function cargarProntoPagopaso1(paso, tabla_reporte_produccion, pie_tabla, estadoReporte) {
    var estado = $('#anulado_').val();
    $('#tabla_prontoPago').jqGrid('GridUnload');
    var grid_tabla_ = jQuery("#tabla_prontoPago");
    if ($("#gview_tabla_prontoPago").length) {
        reloadGridMostrar(grid_tabla_, 32, paso);
    } else {
        grid_tabla_.jqGrid({
            caption: "Movimiento Auxiliar",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            autowidth: false,
            colNames: ['Periodo', 'Cuenta', 'Valor Debito', 'Valor Credito'],
            colModel: [
                {name: 'periodo', index: 'periodo', width: 100, align: 'center', cellattr: arrtSetting},
                {name: 'cuenta', index: 'cuenta', width: 100, align: 'left', hidden: false},
                {name: 'valor_debito', index: 'valor_debito', width: 130, align: 'right', sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_credito', index: 'valor_credito', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: true,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: true,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                data: {
                    opcion: 32,
                    paso: paso,
                    filtro_: $('#filtro_').val(),
                    periodo_inicio: $('#periodo_inicio').val(),
                    periodo_fin: $('#periodo_fin').val(),
                    tipo: 'EXT'
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {
                var valor_debito = grid_tabla_.jqGrid('getCol', 'valor_debito', false, 'sum');
                var valor_credito = grid_tabla_.jqGrid('getCol', 'valor_credito', false, 'sum');
                grid_tabla_.jqGrid('footerData', 'set', {cuenta: 'TOTAL', valor_debito: valor_debito, valor_credito: valor_credito});
            },
            gridComplete: function (index) {
                cargarreporteProduccion(paso, estado, tabla_reporte_produccion, pie_tabla, estadoReporte);
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

            },
            groupingView: {
                groupField: ['periodo'],
                groupSummary: [true],
                groupColumnShow: [true],
                groupText: ['<b>{0} Total Debito:</b> ${valor_debito} <b>Total Credito:</b> ${valor_credito}'],
                groupCollapse: true,
                groupOrder: ['asc']

            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_prontoPago").navButtonAdd('#pager', {
            caption: "Export Excel",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    exportarExcel('tabla_prontoPago', paso);
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
        $("#tabla_prontoPago").navButtonAdd('#pager', {
            caption: "Graph",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    mostrarGraficaProntoPago('tabla_prontoPago');
                } else {
                    mensajesDelSistema("No hay informacion para graficar ", '250', '150', false);
                }
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion, paso) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                paso: paso,
                filtro_: $('#filtro_').val(),
                periodo_inicio: $('#periodo_inicio').val(),
                periodo_fin: $('#periodo_fin').val(),
                tipo: 'EXT'
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center", modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

var aux = '';
arrtSetting = function (rowId, val, rawObject, cm) {
    //   var attr = rawObject.attr[cm.name], result;
    var result = '';
    if (typeof (rawObject) !== "undefined") {
        var attr = rawObject[cm.name];

        console.log('attr.rowspan: ' + attr.rowspan + ' attr.display: ' + attr.display);
        if (attr.rowspan) {
            result = ' rowspan=' + '"' + attr.rowspan + '"';
        } else if (attr.display) {
            result = ' style="display:' + attr.display + '"';
        } else {

            result = " rowspan='3'";
        }
        //empanada 
        if (aux == '') {
            aux = attr;
            result = " rowspan='3'";
        } else if (attr == aux) {
            result = "style='display:none'";
        } else {
            aux = attr;
            result = " rowspan='3'";
        }
    }
    console.log(result);
    return result;
};

function cargarProntoPagopaso2(paso, tabla_reporte_produccion, pie_tabla, estadoReporte) {
    var estado = $('#anulado_').val();
    $('#tabla_prontoPago2').jqGrid('GridUnload');
    var grid_tabla_ = jQuery("#tabla_prontoPago2");
    if ($("#gview_tabla_prontoPago2").length) {
        reloadGridMostrar(grid_tabla_, 32, paso);
    } else {
        grid_tabla_.jqGrid({
            caption: "Movimiento Auxiliar",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            autowidth: false,
            colNames: ['Periodo', 'Cuenta', 'Valor Debito', 'Valor Credito'],
            colModel: [
                {name: 'periodo', index: 'periodo', width: 100, align: 'center', cellattr: arrtSettingPaso2},
                {name: 'cuenta', index: 'cuenta', width: 100, align: 'left', hidden: false},
                {name: 'valor_debito', index: 'valor_debito', width: 130, align: 'right', sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_credito', index: 'valor_credito', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: true,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: true,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                data: {
                    opcion: 32,
                    paso: paso,
                    filtro_: $('#filtro_').val(),
                    periodo_inicio: $('#periodo_inicio').val(),
                    periodo_fin: $('#periodo_fin').val(),
                    tipo: 'EXT'
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {
                var valor_debito = grid_tabla_.jqGrid('getCol', 'valor_debito', false, 'sum');
                var valor_credito = grid_tabla_.jqGrid('getCol', 'valor_credito', false, 'sum');
                grid_tabla_.jqGrid('footerData', 'set', {cuenta: 'TOTAL', valor_debito: valor_debito, valor_credito: valor_credito});
            },
            gridComplete: function (index) {
                cargarreporteProduccion(paso, estado, tabla_reporte_produccion, pie_tabla, estadoReporte);
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

            }, groupingView: {
                groupField: ['periodo'],
                groupSummary: [true],
                groupColumnShow: [true],
                groupText: ['<b>{0} Total Debito:</b> ${valor_debito} <b>Total Credito:</b> ${valor_credito}'],
                groupCollapse: true,
                groupOrder: ['asc']

            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_prontoPago2").navButtonAdd('#pager1', {
            caption: "Export Excel",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    exportarExcel('tabla_prontoPago2', paso);
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
        $("#tabla_prontoPago2").navButtonAdd('#pager1', {
            caption: "Graph",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    mostrarGraficaProntoPago('tabla_prontoPago2');
                } else {
                    mensajesDelSistema("No hay informacion para graficar ", '250', '150', false);
                }
            }
        });
    }
}

var aux1 = '';
arrtSettingPaso2 = function (rowId, val, rawObject, cm) {
    //   var attr = rawObject.attr[cm.name], result;
    var result = '';
    if (typeof (rawObject) !== "undefined") {
        var attr = rawObject[cm.name];

        console.log('attr.rowspan: ' + attr.rowspan + ' attr.display: ' + attr.display);
        if (attr.rowspan) {
            result = ' rowspan=' + '"' + attr.rowspan + '"';
        } else if (attr.display) {
            result = ' style="display:' + attr.display + '"';
        } else {

            result = " rowspan='2'";
        }
        //empanada 
        if (aux1 == '') {
            aux1 = attr;
            result = " rowspan='2'";
        } else if (attr == aux1) {
            result = "style='display:none'";
        } else {
            aux1 = attr;
            result = " rowspan='2'";
        }
    }
    console.log(result);
    return result;
};

function cargarProntoPagopaso3(paso, tabla_reporte_produccion, pie_tabla, estadoReporte) {
    var estado = $('#anulado_').val();
    $('#tabla_prontoPago3').jqGrid('GridUnload');
    var grid_tabla_ = jQuery("#tabla_prontoPago3");
    if ($("#gview_tabla_prontoPago3").length) {
        reloadGridMostrar(grid_tabla_, 32, paso);
    } else {
        grid_tabla_.jqGrid({
            caption: "Movimiento Auxiliar",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            autowidth: false,
            colNames: ['Periodo', 'Cuenta', 'Valor Debito', 'Valor Credito'],
            colModel: [
                {name: 'periodo', index: 'periodo', width: 100, align: 'center', cellattr: arrtSettingPaso3},
                {name: 'cuenta', index: 'cuenta', width: 100, align: 'left', hidden: false},
                {name: 'valor_debito', index: 'valor_debito', width: 130, align: 'right', sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_credito', index: 'valor_credito', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: true,
            pager: '#pager2',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: true,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                data: {
                    opcion: 32,
                    paso: paso,
                    filtro_: $('#filtro_').val(),
                    periodo_inicio: $('#periodo_inicio').val(),
                    periodo_fin: $('#periodo_fin').val(),
                    tipo: 'EXT'
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {
                var valor_debito = grid_tabla_.jqGrid('getCol', 'valor_debito', false, 'sum');
                var valor_credito = grid_tabla_.jqGrid('getCol', 'valor_credito', false, 'sum');
                grid_tabla_.jqGrid('footerData', 'set', {cuenta: 'TOTAL', valor_debito: valor_debito, valor_credito: valor_credito});
            },
            gridComplete: function (index) {
                cargarreporteProduccion(paso, estado, tabla_reporte_produccion, pie_tabla, estadoReporte);
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

            }, groupingView: {
                groupField: ['periodo'],
                groupSummary: [true],
                groupColumnShow: [true],
                groupText: ['<b>{0} Total Debito:</b> ${valor_debito} <b>Total Credito:</b> ${valor_credito}'],
                groupCollapse: true,
                groupOrder: ['asc']

            }
        }).navGrid("#pager2", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_prontoPago3").navButtonAdd('#pager2', {
            caption: "Export Excel",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    exportarExcel('tabla_prontoPago3', paso);
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
        $("#tabla_prontoPago3").navButtonAdd('#pager2', {
            caption: "Graph",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    mostrarGraficaProntoPago('tabla_prontoPago3');
                } else {
                    mensajesDelSistema("No hay informacion para graficar ", '250', '150', false);
                }
            }
        });
    }
}

var aux2 = '';
arrtSettingPaso3 = function (rowId, val, rawObject, cm) {
    //   var attr = rawObject.attr[cm.name], result;
    var result = '';
    if (typeof (rawObject) !== "undefined") {
        var attr = rawObject[cm.name];

        console.log('attr.rowspan: ' + attr.rowspan + ' attr.display: ' + attr.display);
        if (attr.rowspan) {
            result = ' rowspan=' + '"' + attr.rowspan + '"';
        } else if (attr.display) {
            result = ' style="display:' + attr.display + '"';
        } else {

            result = " rowspan='4'";
        }
        //empanada 
        if (aux == '') {
            aux = attr;
            result = " rowspan='4'";
        } else if (attr == aux) {
            result = "style='display:none'";
        } else {
            aux = attr;
            result = " rowspan='4'";
        }
    }
    console.log(result);
    return result;
};

function busqueda_filtro() {
    var filtro = $('#filtro_').val();
    if (filtro === 'otro') {
        $(".f_otro").css("display", "block");
        if ($('#anulado_').css('display') !== 'none') {
            $("#tablita").css("width", "914px");
            $("#tablainterna").css("width", "918px");
        } else {
            $("#tablita").css("width", "672px");
            $("#tablainterna").css("width", "677px");
        }
    } else {
        $(".f_otro").css("display", "none");
        $('#periodo_inicio').val('');
        $('#periodo_fin').val('');
        if ($('#anulado_').css('display') !== 'none') {
            $("#tablita").css("width", "566px");
            $("#tablainterna").css("width", "566px");
        } else {
            $("#tablita").css("width", "309px");
            $("#tablainterna").css("width", "274px");
        }
    }
}

function cargarreporteProduccion(paso, estado, tabla_reporte_produccion, pie_tabla, estadoReporte) {
    $('#' + tabla_reporte_produccion).jqGrid('GridUnload');
    var grid_tabla_ = jQuery("#" + tabla_reporte_produccion);
    if ($("#gview_" + tabla_reporte_produccion).length) {
        reloadGridMostrar_(grid_tabla_, 33, paso, estado);
    } else {
        grid_tabla_.jqGrid({
            caption: "Reporte Produccion",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            width: '1000',
            colNames: ['Periodo Anticipo', 'Periodo Contabilizacion', 'Descripcion', 'Valor', 'Valor Descuento', 'Valor Neto', 'Valor Combancaria', 'Valor Consignacion', 'row_span', 'display'],
            colModel: [
                {name: 'periodo_anticipo', index: 'periodo_anticipo', width: 150, align: 'center', cellattr: arrtSettingPasoReporte},
                {name: 'periodo_contabilizacion', index: 'periodo_contabilizacion', width: 100, align: 'left', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 90, align: 'left', hidden: false},
                {name: 'valor', index: 'valor', width: 130, align: 'right', sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_descuento', index: 'valor_descuento', width: 150, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_neto', index: 'valor_neto', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_combancaria', index: 'valor_combancaria', width: 150, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'vlr_consignacion', index: 'vlr_consignacion', width: 150, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'row_span', index: 'row_span', width: 100, align: 'left', hidden: true},
                {name: 'display', index: 'display', width: 100, align: 'left', hidden: true}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: true,
            pager: '#' + pie_tabla,
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: true,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                data: {
                    opcion: 33,
                    paso: paso,
                    filtro_: $('#filtro_').val(),
                    periodo_inicio: $('#periodo_inicio').val(),
                    periodo_fin: $('#periodo_fin').val(),
                    concept_code: '50',
                    anulado: estado
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {
                var valor = grid_tabla_.jqGrid('getCol', 'valor', false, 'sum');
                var valor_descuento = grid_tabla_.jqGrid('getCol', 'valor_descuento', false, 'sum');
                var valor_neto = grid_tabla_.jqGrid('getCol', 'valor_neto', false, 'sum');
                var valor_combancaria = grid_tabla_.jqGrid('getCol', 'valor_combancaria', false, 'sum');
                var vlr_consignacion = grid_tabla_.jqGrid('getCol', 'vlr_consignacion', false, 'sum');
                grid_tabla_.jqGrid('footerData', 'set', {descripcion: 'TOTAL', valor: valor, valor_descuento: valor_descuento, valor_neto: valor_neto, valor_combancaria: valor_combancaria, vlr_consignacion: vlr_consignacion});
            },
            gridComplete: function (index) {
                cargando_toggle();
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

            },
            groupingView: {
                groupField: ['periodo_anticipo'],
                groupSummary: [true],
                groupColumnShow: [true],
                groupText: ['<b>{0} </b> '],
                groupCollapse: false,
                groupOrder: ['asc']

            }
        }).navGrid('#' + pie_tabla, {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#" + tabla_reporte_produccion).navButtonAdd('#' + pie_tabla, {
            caption: "Exportar Excel",
            onClickButton: function () {

            }
        });
        $("#" + pie_tabla + " table.navtable tbody tr").append(// here 'pager' part or #pager_left is the id of the pager
                '<td><div> \n\
                             <label style="margin-left: 80px;">Status</label>\n\
                            <select class="' + estadoReporte + '" id="' + estadoReporte + '" style="width: 133px;border: 1px solid #ccc;border: none;" onchange="buscarEstadoReporteProduccion(this.id)"> \n\
                            <option value="">Choose</option> \n\
                            <option value="0">All</option> \n\
                            <option value="1">Not Annulled</option>  \n\
                            <option value="2">Annulled</option> </select> </div></td>');
    }
}

function buscarEstadoReporteProduccion(id) {
    var paso_ = saberPaso();
    var estado = $('#' + id).val();
    cargarreporteProduccion(paso_, estado, tabla_reporte_produccion_, pie_tabla_, id);
    cargando_toggle();
}

function reloadGridMostrar_(grid_tabla, opcion, paso, periodo, estado) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                paso: paso,
                periodo: periodo,
                filtro_: $('#filtro_').val(),
                periodo_inicio: $('#periodo_inicio').val(),
                periodo_fin: $('#periodo_fin').val(),
                concept_code: '01',
                anulado: estado
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

arrtSettingPasoReporte = function (rowId, val, rawObject, cm, rdata) {
    //   var attr = rawObject.attr[cm.name], result;
    var result = '';
    if (typeof (rawObject) !== "undefined") {
        var attr = rawObject.attr[cm.name];
        console.log(attr);
        console.log('attr.display: ' + attr.valor_01 + ' attr.rowspan: ' + attr.valor_02);
        if (attr.valor_02 > 1) {
            result = ' rowspan=' + '"' + attr.valor_02 + '"';
        } else if (attr.valor_01) {
            result = ' style="display:' + attr.valor_01 + '"';
        }
    }

    return result;
};

function saberPaso() {
    var paso_busqueda = $('#paso_busqueda').val();
    var paso;
    switch (paso_busqueda) {
        case 'atpaso1':
            paso = 'paso1';
            break;
        case  'atpaso2':
            paso = 'paso2';
            break;
        case 'atpaso3':
            paso = 'paso3';
            break;
        case 'atpaso4':
            paso = 'paso4';
            break;
        case 'atpaso5':
            paso = 'paso5';
            break;
    }
    return paso;
}

function sabercontenedorGrafica(tabla) {
    var contenedor;
    switch (tabla) {
        case 'tabla_prontoPago':
            contenedor = 'containerProntoPagoPaso1';
            break;
        case  'tabla_prontoPago2':
            contenedor = 'containerProntoPagoPaso2';
            break;
        case 'tabla_prontoPago3':
            contenedor = 'containerProntoPagoPaso3';
            break;
    }
    return contenedor;
}

function mostrarGraficaProntoPago(tabla) {
    var grid = jQuery("#" + tabla), filas = grid.jqGrid('getDataIDs');
    var periodo_ = '';
    var periodos = [], valorDebito = [], valorCredito = [];
    var periodo_2 = grid.getRowData(1).periodo;
    var valorDebitosum = 0;
    var valorCreditosum = 0;
    var contenedorGrafica = sabercontenedorGrafica(tabla);
    for (var i = 0; i < filas.length; i++) {
        var cl = filas[i];
        if (periodo_ !== grid.getRowData(cl).periodo) {
            periodos.push((grid.getRowData(cl).periodo) * 1);
            periodo_ = grid.getRowData(cl).periodo;
        }
    }
    for (var i = 0; i < filas.length + 1; i++) {
        var cl = filas[i];
        if (periodo_2 === grid.getRowData(cl).periodo) {
            valorDebitosum = parseFloat(valorDebitosum) + ((grid.getRowData(cl).valor_debito) * 1);
            valorCreditosum = parseFloat(valorCreditosum) + ((grid.getRowData(cl).valor_credito) * 1);
        } else if (periodo_2 !== grid.getRowData(cl).periodo) {
            periodo_2 = grid.getRowData(cl).periodo;
            valorDebito.push(valorDebitosum * 1);
            valorCredito.push(valorCreditosum * 1);
            valorDebitosum = grid.getRowData(cl).valor_debito;
            valorCreditosum = grid.getRowData(cl).valor_credito;
        }
    }
    console.log(contenedorGrafica);
    console.log(periodos);
    console.log(valorDebito);
    console.log(valorCredito);
    verGrafica(periodos, valorDebito, valorCredito, contenedorGrafica);
    $("#dialogMsjGraficaProntoPago").dialog({
        width: '700',
        height: '500',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
                $('#' + contenedorGrafica).css("display", "none");
            }
        }
    });
}

function verGrafica(periodos, valorDebito, valorCredito, contenedorGrafica) {
    $('#' + contenedorGrafica).css("display", "block");
    $('#' + contenedorGrafica).highcharts({
        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'REPORTE PRONTO PAGO'
        },
        subtitle: {
            text: ''
        },
        plotOptions: {
            column: {
                depth: 5
            }
        },
        xAxis: {
            title: {
                text: 'Periodo'
            },
            categories: periodos,
            labels: {
                formatter: function () {
                    return '<b>' + this.value + '</b>';
                }
            }
        },
        yAxis: {
            title: {
                text: 'Rango Valores'
            },
            labels: {
                formatter: function () {
                    return '<b> $' + numberConComas(this.value) + '</b>';
                }
            }
        },
        series: [
            {name: 'Valor Debito', data: valorDebito},
            {name: 'Valor Credito', data: valorCredito, color: 'rgba(249, 135, 25, 0.9)'}
        ]
    });
}

function exportarExcel(tabla, paso) {
    var fullData = $("#" + tabla).jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Admin&accion=Fintra",
        data: {
            listado: myJsonString,
            opcion: 45,
            paso: paso
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function ventanaMostrarhelp() {
    var mensaje = mostrarHelp();
    var paso = saberPaso();
    var permiso = confirmarPermisoHelp();
    console.log(mensaje);
    if (permiso.startsWith('SI')) {
        /*usuarios con permiso para editar el texto de la ayuda*/
        if (mensaje === '') {
            mensajesDelSistema('No se ha realizado la configuraci�n de este paso', '370', 'auto', false);
        } else {
            swal({
                title: "Ayuda " + paso.toLowerCase(),
                text: '<p style = "text-align : justify;"> ' + mensaje + ' </p>',
                html: true,
                showCancelButton: true,
                confirmButtonColor: "#397EAD",
                confirmButtonText: "Aceptar",
                cancelButtonText: "Editar",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (!isConfirm) {
                    ventanaEditarHelp();
                }
            });
        }

    } else if (permiso.startsWith('NO')) {
        /*usuarios sin permiso para editar el texto de la ayuda*/
        swal({
            title: "Ayuda " + paso.toLowerCase(),
            text: '<p style = "text-align : justify;"> ' + mensaje + ' </p>',
            html: true,
            confirmButtonText: "Aceptar"
        });
    }
}

function mostrarHelp() {
    var paso = saberPaso();
    var help;
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'text',
        async: false,
        data: {
            opcion: 41,
            paso: paso,
            modulo: $('#modulo').val()
        },
        success: function (json) {
            console.log(json.length);
            if (json.length > 1) {
                help = json;
            } else {
                help = '';
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return help;
}

function confirmarPermisoHelp() {
    var permiso;
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'text',
        async: false,
        data: {
            opcion: 42
        },
        success: function (json) {
            permiso = json;
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return permiso;
}

function ventanaEditarHelp() {
    mostrarHelpDatos();
    $("#dialogMsjHelp").dialog({
        width: '556',
        height: '380',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Actualizar": function () {
                guardarCambioHelp();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function mostrarHelpDatos() {
    var paso = saberPaso();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 43,
            paso: paso,
            modulo: $('#modulo').val()
        },
        success: function (json) {
            $('#id').val(json[0].id);
            $('#modulo_').val(json[0].modulo);
            $('#paso_').val(json[0].paso);
            $('#textoArea').val(json[0].descripcion_ayuda);
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function guardarCambioHelp() {
    var id = $('#id').val();
    var textoAyuda = $('#textoArea').val();
    if (textoAyuda == '') {
        mensajesDelSistema('Debe ingresar la descripcion de la ayuda', '370', 'auto', false);
    } else {
        $.ajax({
            async: false,
            url: "./controller?estado=Admin&accion=Fintra",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 44,
                id: id,
                textoAyuda: textoAyuda
            },
            success: function (json) {
                console.log(json.respuesta);
                if (json.respuesta === 'Guardado') {
                    $("#dialogMsjHelp").dialog("close");
                    mensajesDelSistema('Exito al guardar', '300', 'auto', false);
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" + "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }

}