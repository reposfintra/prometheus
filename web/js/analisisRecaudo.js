/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#fecha_inicio").val('');
    $("#fecha_fin").val('');
    $("#fecha_inicio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date('2013/12/31'),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha_fin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date('2013/12/31'),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $('#ui-datepicker-div').css('clip', 'auto');

    $('#buscar').click(function () {
        var fecha_inicio = $("#fecha_inicio").val();
        var fecha_fin = $("#fecha_fin").val();
        if ((fecha_inicio === '' && fecha_fin === '') || (fecha_inicio !== '' && fecha_fin !== '')) {
            cargarRecaudos();
        }else{
             mensajesDelSistema('Verificar los datos de lso filtros', '300', 'auto', false);
        }

    });
});


function cargarRecaudos() {
    var grid_tabla_ = jQuery("#tabla_recaudos");
    if ($("#gview_tabla_recaudos").length) {
        reloadGridMostrar(grid_tabla_, 83);
    } else {
        grid_tabla_.jqGrid({
            caption: "Analisis de Recaudos",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            width: '1560',
            colNames: ['A�o', 'Periodo', 'A�o Consignacion', 'Periodo Consignacion', 'Banco', 'Sucursal', '#Ingreso', 'Tipo Documento', 'Nit Cliente', 'Nombre Cliente', 'Fecha Consignacion',
                'Cuenta', 'Documento', 'Tipo Documento', 'Valor Ingreso', 'Unidad Negocio', 'Negocio', 'Usuario Creacion'],
            colModel: [
                {name: 'anio', index: 'anio', width: 100, align: 'center', hidden: true},
                {name: 'periodo', index: 'periodo', width: 100, align: 'center', hidden: true},
                {name: 'anio_consignacion', index: 'anio_consignacion', width: 200, align: 'center', hidden: true},
                {name: 'periodo_consignacion', index: 'periodo_consignacion', width: 200, align: 'center', hidden: false},
                {name: 'banco', index: 'banco', width: 170, align: 'left', hidden: false},
                {name: 'sucursal', index: 'sucursal', width: 180, align: 'center'},
                {name: 'num_ingreso', index: 'num_ingreso', width: 150, align: 'center'},
                {name: 'tipo_documento', index: 'tipo_documento', width: 150, align: 'center'},
                {name: 'nitcli', index: 'nitcli', width: 170, align: 'left'},
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 290, align: 'left'},
                {name: 'fecha_consignacion', index: 'fecha_consignacion', width: 180, align: 'left'},
                {name: 'cuenta', index: 'cuenta', width: 170, align: 'left'},
                {name: 'documento', index: 'documento', width: 170, align: 'left'},
                {name: 'tipo_doc', index: 'tipo_doc', width: 170, align: 'left'},
                {name: 'valor_ingreso', index: 'valor_ingreso', width: 200, hidden: false, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'unidad_negocio', index: 'unidad_negocio', width: 200, align: 'left'},
                {name: 'negocio', index: 'negocio', width: 110, align: 'left'},
                {name: 'creation_user', index: 'creation_user', width: 180, align: 'left'}
            ],
            rownumbers: true,
            rownumWidth: 20,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            footerrow: false,
            pager: '#pager',
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: false,
            shrinkToFit: true,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 83,
                    banco: $('#banco').val(),
                    fecha_inicio: $('#fecha_inicio').val(),
                    fecha_fin: $('#fecha_fin').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function (id, rowid) {
            },
            gridComplete: function (index) {
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                banco: $('#banco').val(),
                fecha_inicio: $('#fecha_inicio').val(),
                fecha_fin: $('#fecha_fin').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center", modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}