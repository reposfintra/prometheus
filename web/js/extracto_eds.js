/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $("#fecha_inicio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    $("#fecha_fin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    var myDate = new Date();
    $("#fecha_inicio").datepicker("setDate", myDate);
    $("#fecha_fin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

    $("#buscar").click(function () {
        cargarExtracto();
    });
  
    $("#nombre").change(function() {
        var op = $(this).find("option:selected").val();
        $("#nit").val($('#'+op).attr('class'));
        //cargarCiudad(op, "ciudad_cg");
    });
    loadEds();


});

function cargarExtracto() {
    var grid_extracto_ventas = jQuery("#tbl_extracto_eds");

    if ($("#gview_tbl_extracto_eds").length) {
         reloadGridExtractoEds(grid_extracto_ventas);
    } else {

        grid_extracto_ventas.jqGrid({
            caption: "Movimientos Ventas",
            url: "./controller?estado=Administracion&accion=Logistica",
            mtype: "POST",
            datatype: "json",
            height: '600',
            width: '1790',
            colNames: ['Consecutivo Venta','Nombre eds', 'Fecha Venta', 'Hora', 'Nombre transportadora', 'Planilla', 'Placa',
                'Nombre conductor', 'Cedula', 'Producto', 'Precio unitario', 'Unidades', 'Valor venta', 'Descuento', 'Valor a Cobrar'],
            colModel: [
                {name: 'num_venta', index: 'num_venta', width: 120, align: 'center', key: true},
                {name: 'nombre_eds', index: 'nombre_eds', width: 150, align: 'center', key: true},
                {name: 'fecha_venta', index: 'fecha_venta', sortable: true, width: 100, align: 'center'},
                {name: 'hora', index: 'hora', sortable: true, width: 80, align: 'center'},
                {name: 'nombre_transportadora', index: 'nombre_transportadora', sortable: false, width: 140, align: 'center'},
                {name: 'planilla', index: 'planilla', width: 100, align: 'center'},
                {name: 'placa', index: 'placa', width: 105, align: 'center'},
                {name: 'nombre_conductor', index: 'nombre_conductor', width: 150, align: 'center'},
                {name: 'cedula_conductor', index: 'cedula_conductor', width: 100, align: 'center'},
                {name: 'nombre_producto', index: 'nombre_producto', width: 120, align: 'center'},
                {name: 'precio_xunidad', index: 'precio_xunidad', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 5, prefix: "$ "}},
                {name: 'unidades', index: 'unidades', sortable: false, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 5}},
                {name: 'valor_venta', index: 'valor_venta', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 5, prefix: "$ "}},
                {name: 'descuento', index: 'descuento', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 5, prefix: "$ "}},
                {name: 'valor_a_cobrar', index: 'valor_a_cobrar', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 5, prefix: "$ "}}
            ],
            rowNum: 10000,
            rowTotal: 10000000,
            pager: ('#page_extracto_eds'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                async: false,
                data: {
                    opcion: 5,
                    fecha_inicio: $("#fecha_inicio").val(),
                    fecha_fin: $("#fecha_fin").val(),
                    nombre: $("#nombre").val(),
                    nit: $("#nit").val()
                }
            },
            loadComplete: function () {
                cacularTotalesExtractoEds(grid_extracto_ventas);
                if (grid_extracto_ventas.jqGrid('getGridParam', 'records') <= 0) {
                     mensajesDelSistema("No se encontraron resultados para los parametros seleccionados.", "270", "170");
                } 
               
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_extracto_eds", {add: false, edit: false, del: false, search: false, refresh: false}, {});

        jQuery("#tbl_extracto_eds").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
                
         $("#tbl_extracto_eds").navButtonAdd('#page_extracto_eds', {
            caption: "Exportar Excel",
            onClickButton: function (e) {
                exportaExcel();
              //  exportData(14, "xls");
            }
        });
    }
}

function cacularTotalesExtractoEds(grid_extracto_ventas) {

    var valor_venta = grid_extracto_ventas.jqGrid('getCol', 'valor_venta', false, 'sum');
    var descuento = grid_extracto_ventas.jqGrid('getCol', 'descuento', false, 'sum');
    var valor_a_cobrar = grid_extracto_ventas.jqGrid('getCol', 'valor_a_cobrar', false, 'sum');

    grid_extracto_ventas.jqGrid('footerData', 'set', {
        nombre_producto: 'Totales',
        valor_venta: valor_venta,
        descuento: descuento,
        valor_a_cobrar: valor_a_cobrar
    });

}


function reloadGridExtractoEds(grid_extracto_ventas) {
    grid_extracto_ventas.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Logistica",
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: 5,
                fecha_inicio: $("#fecha_inicio").val(),
                fecha_fin: $("#fecha_fin").val(),
                nombre: $("#nombre").val(),
                nit: $("#nit").val()
            }
        }
    });
    grid_extracto_ventas.trigger("reloadGrid");
}

function loadEds() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        data: {
            opcion: 6
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#nombre').append('<option id='+ json[key].idusuario +' value='+ json[key].idusuario +' class= '+json[key].nit_estacion+'>' + json[key].nombre_eds + '</option>');
                    }
                    if(json.length===1){
                        $("#nit").val(json[0].nit_estacion);                       
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}


function exportaExcel(){
    
     $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: "html",
        data: {
            opcion: 25,
            fecha_inicio: $("#fecha_inicio").val(),
            fecha_fin: $("#fecha_fin").val(),
            nombre: $("#nombre").val(),
            nit: $("#nit").val()
        },
        success: function (data) {
            if (data !=='') {              
              mensajesDelSistema(data, '300', '180');            
             } else {
                mensajesDelSistema("Lo sentimos no se pudo generar el archivo.", '250', '150');
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
}


function exportData(cols, file) {
    var columns = cols.toString().split(",");
    var columnNms = $("#tbl_extracto_eds").jqGrid('getGridParam', 'colNames');
    var theads = "";
    for (var cc = 1; cc < columnNms.length; cc++) {
        var isAdd = true;
        for (var p = 0; p < columns.length; p++) {
            if (cc === columns[p]) {
                isAdd = false;
                break;
            }
        }
        if (isAdd) {
            theads = theads + "<th>" + columnNms[cc] + "</th>"
        }
    }
    theads = "<tr>" + theads + "</tr>";
    var mya = new Array();
    mya = jQuery("#tbl_extracto_eds").getDataIDs();  // Get All IDs
    var data = jQuery("#tbl_extracto_eds").getRowData(mya[0]);     // Get First row to get the labels
    //alert(data['id']);
    var colNames = new Array();
    var ii = 0;
    for (var i in data) {
        colNames[ii++] = i;
    }

    var pageHead = "Extracto eds";
    var html = "<html>\n\
                <head>\n\
                <style script='css/text'>\n\
                table.tableList_1 th{\n\
                                    text-align:center; \n\
                                    vertical-align: middle; \n\
                                    padding:1px; \n\
                                    background-color:blue;\n\
                }\n\
                \n\
                table.tableList_1 td {\n\
                                    text-align: left; \n\
                                    vertical-align: top; \n\
                                    padding:1px;\n\
                }\n\
                </style>\n\
                </head>\n\
                <body>\n\
                <div class='pageHead_1'>" + pageHead + "</div>\n\
                <table border='" + (file == 'pdf' ? '0' : '0') + "' class='tableList_1 t_space' cellspacing='3' cellpadding='0'>" + theads;
 //   alert(theads);
    for (i = 0; i < mya.length; i++)
    {
        html = html + "<tr>";
        data = jQuery("#tbl_extracto_eds").getRowData(mya[i]); // get each row
        for (j = 0; j < colNames.length; j++) {
            var isjAdd = true;
            for (var pj = 0; pj < columns.length; pj++) {
                if (j === columns[pj]) {
                    isjAdd = false;
                    break;
                }
            }
            if (isjAdd) {
                html = html + "<td>" + data[colNames[j].replace(/(<br>)*/g, '')] + "</td>"; // output each column as tab delimited
            }
        }
        html = html + "</tr>";  // output each row with end of line

    }
    
    html = html + "</table></body></html>";  // end of line at the end
    
   // alert(html+"saas");
    document.form_expr.pdfBuffer.value = html;
    document.form_expr.fileType.value = file;
    document.form_expr.method = 'POST';
    document.form_expr.action = './controller?estado=Administracion&accion=Logistica&opcion=4';//./controller?estado=Reestructuracion&accion=Negocios&opcion=14';  // send it to server which will open this contents in excel file
    document.form_expr.submit();
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}