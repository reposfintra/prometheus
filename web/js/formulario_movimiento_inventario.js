/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var array_de_proyectos = [];
var array_de_items = [];
var tabla_de_proyectos;
var tabla_de_items;
var items_seleccionados = [];
var sw = 0;



$( document ).ready(function() {
    cargando_toggle();
    cargarCombo('cbx_tipo_salida', 2);
    cargarCombo('cbx_tipo_movimiento', 3);
});

function cargando_toggle() {
    $('#loader-wrapper').toggle();
} 

function obtenerFila(e) {    
    var data = tabla_de_proyectos.row($(e).parents('tr')).data();
    if ($(e).attr("id") == 'img_origen') {
        //data[0] es el foms y data[1] es el id solicitud 
        escogerProyecto(data[0], data[1], "origen");              
    }else if($(e).attr("id") == 'img_destino'){
        escogerProyecto(data[0], data[1], "destino");
    }       
};



function obtenerItem(e) {    
    var data = tabla_de_items.row($(e).parents('tr')).data();
    $('#temp_tr').remove();
    if (items_seleccionados.includes(data[0])){
        toastr.error("Ya insertaste este item");
    }else{
    items_seleccionados.push(data[0])
    $('#tbl_solicitudes tbody').append(
            '<tr>\n\
                <td>'+data[0]+'</td>\n\
                <td>'+data[1]+'</td>\n\
                <td>'+Number(data[3])+'</td>\n\
                <td><input type="number" style="width: 100px;" value="'+data[3]+'"></td>\n\
                <td><select class="poner_unidad_de_medida" data-seleccionado="'+data[2]+'"></td>\n\
                <td><input type="text" style="width: 100px;"></td>\n\
                <td><textarea name="message" rows="2" cols="30" placeholder="Hay alguna observacion?"></textarea></td>\n\
                <td><input type="number" style="width: 100px;" value="0"></td>\n\
                <td><input type="button" value="Descartar" onclick="borrarFila(this)"></td>\n\
            </tr>');            
    };
    cargar_unidades_medida();
    if($('#cbx_tipo_movimiento').val() == 'traslado'){
        $('.unidad_de_medida_puesta').prop( "disabled", true );
    }
};

function cargar_unidades_medida() {

    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Modulo&accion=Planeacion",
        dataType: 'json',
        async: false,
        data: {
            opcion: 82
        },
    success: function(json) {
        total = Object.keys(json).length;
        for (var key in json) {
          $('.poner_unidad_de_medida').each(function(i, obj) {
              $(obj).append('<option value=' + key + '>' + json[key] + '</option>');
              
              if (Object.keys(json).indexOf(key)==total-1){
                $.each( $.find('.poner_unidad_de_medida'), function() {  
                    for (i in this.options){
                            void((this.options[i].innerHTML==$(this).attr("data-seleccionado")) && (this.value = this.options[i].value))	
                    }
                });
                
                
                 $(obj).toggleClass('poner_unidad_de_medida unidad_de_medida_puesta');
              }
              
          });                          
        }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
    });
}


function cargarItems(e) {
  cargando_toggle(); 
  array_de_items.length = 0;
  var tipo_movimiento =  $('#cbx_tipo_movimiento').val()
  var evento = e;
  
  var id_bodega = $("#cbx_lista_bodegas_origen").val();
  
  if (id_bodega == "0"){
    toastr.error("Primero debe seleccionar una bodega");
    return true;
  }

  if (tipo_movimiento == null){
    toastr.error("Primero debe seleccionar el tipo de movimiento");
    return true;      
  }
   var id_solicitud = $('#id_solicitud_origen').val();
  
  PosicionarDivLitleLeft('div_items', e, 500);
  //en la posicion 0 de array_de_items se guarda el ID de la bodega a la que pertenecen los items.

        $.ajax({
          type: 'POST',
          url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
          dataType: 'json',
          async: false,
          data: {
            opcion: 32,
            id_bodega: id_bodega,
            tipo_movimiento: tipo_movimiento,
            id_solicitud: id_solicitud
          },
          success: function(json) {
            if (json.error) {
              //  mensajesDelSistema(json.error, '250', '180');
              return;
            } 
              array_de_items[0] = id_bodega;
              for (var datos in json) {
                array_de_items.push(Object.values(json[datos]));
              }
          },
           complete: function(data) {
                cargando_toggle();
            },
          error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
              "Message: " + xhr.statusText + "\n" +
              "Response: " + xhr.responseText + "\n" + thrownError);
          }
        });

    
    if ( $.fn.DataTable.isDataTable('#show_items') ) {
      $('#show_items').DataTable().destroy();
    }

    $('#show_items tbody').empty();     
    tabla = $('#show_items').DataTable({
    destroy: true,
    processing: false,
    pageLength: 25,
    dom: '<"toolbar">frtip',
    data: array_de_items.slice(1),
    columns: [
        {title: "COD MATERIAL"},
        {title: "DESCRIPCION"},
        {title: "UNIDADES"},
        {title: "DISPONIBLE"},
        {title: "ACCION"}
    ],
    columnDefs: [
        {
          targets: -1,
          data: null,
          defaultContent: "<IMG SRC='./images/select_ad-x.png' WIDTH=21 HEIGHT=20 BORDER=2 ALT='Visualizar' onclick='obtenerItem(this)'> \n\ "
        },{
          targets: 1,
          class:'col-descripcion'          
        }
    ]
  });
  tabla_de_items = tabla;    
  $('#div_items').fadeIn('slow');
  
};

function CargarProyectos(e) {
  array_de_proyectos = [];
  var tipo_movimiento = $( "#cbx_tipo_movimiento" ).val(); 
  var opcion = 0;
  switch(tipo_movimiento) {
    case "entrada":
    case "salida":
        opcion = 31;
        break;
    case "traslado":
         opcion = 37;
        break;            
}
  cargando_toggle(); 
  var origen_o_destino;
  e.toElement.id == "seleccionar_origen" ? origen_o_destino = "origen" : origen_o_destino = "destino";
  PosicionarDivLitleLeft('div_proveedores', e, 500);
  
  
  function ajaxCargarProyectos() {
    // NOTA:  Esta funcion retorna el ajax que  
    //        carga todos los proyectos.
    return $.ajax({
      type: 'POST',
      url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
      dataType: 'json',
      async: true,
      data: {
        opcion: opcion
      },
      success: function(json) {       
        if (json.error) {
          //  mensajesDelSistema(json.error, '250', '180');
          return;
        }          
          for (var datos in json) {
            array_de_proyectos.push(Object.values(json[datos]));
          }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert("Error: " + xhr.status + "\n" +
          "Message: " + xhr.statusText + "\n" +
          "Response: " + xhr.responseText + "\n" + thrownError);
      }
    });
}
    $.when(ajaxCargarProyectos()).done(function(){
        if ( $.fn.DataTable.isDataTable('#show_proveedores') ) {
          $('#show_proveedores').DataTable().destroy();
        }

        $('#show_proveedores tbody').empty();    
        tabla = $('#show_proveedores').DataTable({
        destroy: false,
        processing: false,
        pageLength: 25,
        dom: '<"toolbar">frtip',
        data: array_de_proyectos,
        columns: [{
            title: "FOMS"
          }, {
            title: "ID SOLICITUD"
          }, {
            title: "DESCRIPCION PROYECTO"
          }, {
            title: "SELECCIONAR"
          }] ,
        columnDefs: [{
          targets: -1,
          data: null,
          defaultContent: "<IMG SRC='./images/select_ad-x.png' WIDTH=20 HEIGHT=20 BORDER=2 ALT='Visualizar' id='img_"+origen_o_destino+"' onclick='obtenerFila(this)'> \n\ "
        }]
      });
      tabla_de_proyectos = tabla;
      $('#div_proveedores').fadeIn('slow');
       cargando_toggle(); 
   });
   
};

function escogerProyecto(foms, id, origen_o_destino) {
  $('#foms_'+origen_o_destino).val(foms);
  $('#id_solicitud_'+origen_o_destino).val(id);
  $('#div_proveedores').fadeOut('slow');
  id_solicitud = $('#id_solicitud_'+origen_o_destino).val();
  cargarBodegasTraspaso(id_solicitud, '#cbx_lista_bodegas_'+origen_o_destino);
}

function cargarBodegasTraspaso(identificador_solicitud, cbx_lista_bodega) {
    $(cbx_lista_bodega).prop("disabled", false);
    $(cbx_lista_bodega).html('');
    $(cbx_lista_bodega).append('<option value="0">Selecciona bodega...</option>');
    //cargarBodegaPrincipalTraspaso(cbx_lista_bodega);

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Orden&accion=Compra',
        dataType: 'json',
        async: false,
        data: {
            id_solicitud: identificador_solicitud,
            opcion: 5
        },
        success: function (json) {                
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                for (var i in json) {
                    $(cbx_lista_bodega).append('<option value="'+json[i].id+'">' + json[i].direccion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
};

function cargarBodegaPrincipalTraspaso(cbx_lista_bodega) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Orden&accion=Compra',
        dataType: 'json',
        async: false,
        data: {
            opcion: 3
        },
        success: function (json) { 
            if (json.error) {                    
                return;
            }
            try {
                for (var i in json) {
                    $(cbx_lista_bodega).append('<option value="'+json[i].id+'">' + json[i].direccion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
};

function contarFilas(table) {
    return $('tr', $(table).find('tbody')).length;
};





$( "#cbx_tipo_movimiento" ).change(function() {
  cambio();
});
function cambio(){
    if($("#cbx_tipo_movimiento").val() == 'entrada'){
        $(".mov_entrada").show();
    }else{
        $(".mov_entrada").hide();
    }
    $('#div_items').fadeOut('fast')
    array_de_items = [];
    items_seleccionados = [];
    $("#tbl_solicitudes > tbody").html("<tr id='temp_tr'><td colspan='11'>INSERTE AL MENOS UN ITEM</td></tr>");
    var tipo_movimiento = $( "#cbx_tipo_movimiento" ).val();  
    $( "#titulo_tipo_movimiento" ).html(tipo_movimiento.toUpperCase());
    $.ajax({
      url: "jsp/solicitud_insumos/"+tipo_movimiento+"_form.jsp",
      type: 'GET',
      success: function(data){ 
        $( "#include_e_s_t" ).html( data );
        if (tipo_movimiento == 'entrada') {
          cargarCombo('cbx_tipo_entrada', 1);
        } else if (tipo_movimiento == 'salida') {
          cargarCombo('cbx_tipo_salida', 2);
        }
      },
      error: function(data) {

      }
    });  
}

$( "#cbx_lista_bodegas_origen" ).change(function() {
  $('#div_items').fadeOut('fast')
  array_de_items = [];
  items_seleccionados = [];
  
    if ( $.fn.DataTable.isDataTable('#show_items') ) {
      $('#show_items').DataTable().destroy();
    }

    $('#show_items tbody').empty();   
    array_de_items=[];
  $("#tbl_solicitudes > tbody").html("<tr id='temp_tr'><td colspan='11'>INSERTE AL MENOS UN ITEM</td></tr>");
});



function guardarEntradaSalida(tipo_movimiento) {
    fecha_transaccion = $("#fecha_transaccion").val();  
    responsable = $("#responsable").val();
    descripcion = $("#descripcion").val();
    id_solicitud_proyecto = $("#id_solicitud_origen").val();
    id_solicitud_destino = $("#id_solicitud_destino").val();
    id_bodega = $("#cbx_lista_bodegas_origen").val();
    id_bodega_destino = $("#cbx_lista_bodegas_destino").val();
      
    campos = [fecha_transaccion, responsable, descripcion, id_solicitud_proyecto, id_bodega];
    void(tipo_movimiento == 3 && campos.push(id_solicitud_destino) && campos.push(id_bodega_destino));
    if (campos.includes('')){
      toastr.error("Faltan datos en el formulario");
      return true;
    }
    if($('#cbx_lista_bodegas_destino').val()==0){
      toastr.error("Faltan datos en el formulario");
      return true;  
    }
    detalle = JSON.stringify(convertirTablaAJSON());
    if(solicitadoSuperaDisponible(detalle)){
      toastr.error("La cantidad solicitada no puede ser 0 ni superar el valor disponible");
      return true;
    }
  $.ajax({
    type: 'POST',
    url: "./controlleropav?estado=Compras&accion=Proceso",
    dataType: 'json',
    async: false,
    data: {
      opcion: 34,
      fecha_transaccion: fecha_transaccion,
      responsable: responsable,
      descripcion: descripcion,
      id_solicitud_proyecto: id_solicitud_proyecto,
      id_solicitud_destino: id_solicitud_destino,
      id_bodega_destino: id_bodega_destino,
      id_bodega: id_bodega,
      detalle: detalle,
      tipo_movimiento: tipo_movimiento
    },
    success: function(json) {
      void(json.error && toastr.error(json.error));
      if(json.status == '200'){         
          $('#div_nueva_ocs').fadeOut('slow');
          alert("Creado con exito");       
          location.reload();          
      }            
    }
    ,
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });    
};

function solicitadoSuperaDisponible(json){
    tipo_movimiento = $( "#cbx_tipo_movimiento" ).val(); 
    for (var i = 0; i < json.length; i++){
        var obj = json[i];
        if(obj["disponibles"]!=''){
                if ((Number(obj["solicitar"]) > Number(obj["disponibles"]))){
                    if (tipo_movimiento != "entrada"){
                        return true;
                    }
                }
                if (Number(obj["solicitar"])==0){
                    return true;
                }
                if ((Number(obj["solicitar"]) < 0)){                    
                    return true;                    
                }                
        }
    }
    return false;
}

$("#btn_guardar_movimiento").click(function(){
    if (confirm("Esta seguro que desea guardar el movimiento?")) {
    } else {
        return;
    }    
    if (array_de_items.length<1){        
        toastr.error("Inserte por lo menos un item");
        return;
    }
    var validar_cantidad = solicitadoSuperaDisponible(convertirTablaAJSON());
    if (validar_cantidad){
        toastr.error("Solicitado no puede ser mayor que disponible o cero");
        return;
    }
    
    tipo_movimiento = $("#cbx_tipo_movimiento").val();
    switch (tipo_movimiento){
        case "entrada":
            guardarEntradaSalida(1);
            break;
        case "salida":           
            guardarEntradaSalida(2);
            break;
        case "traslado":
            if (($("#cbx_lista_bodegas_destino").val()==0)||($("#id_solicitud_destino").val() == "")){
                toastr.error("Seleccione el proyecto y la bodega de destino!");
                return;                
            };
            guardarEntradaSalida(3);
            break;
    }
});

function convertirTablaAJSON(){
    var filas = [];
    $('#tbl_solicitudes tbody tr').each(function(i, n){
        var $fila = $(n);
        var json = {
            codigo_material:        $fila.find('td:eq(0)').text(),
            descripcion:            $fila.find('td:eq(1)').text(),            
            disponibles:            $fila.find('td:eq(2)').text(),
            solicitar:              $fila.find('td:eq(3)').children().val(),
            unidad_de_medida:       $fila.find('td:eq(4)').children().val(),
            referencia:             $fila.find('td:eq(5)').children().val(),
            observacion:            $fila.find('td:eq(6)').children().val()
        };
        if($("#cbx_tipo_movimiento").val() == 'entrada'){
            json.valor = $fila.find('td:eq(7)').children().val();
        }
        filas.push(json);
    });
    return filas;    
}

function borrarFila(r) {        
    var id =$(r).parents('tr').children()[0].innerHTML
    
    items_seleccionados = items_seleccionados.filter(item => item !== id)
    
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tbl_solicitudes").deleteRow(i);
    if (contarFilas("#tbl_solicitudes")==0){
        $("#tbl_solicitudes > tbody").html("<tr id='temp_tr'><td colspan='11'>INSERTE AL MENOS UN ITEM</td></tr>");
    }
}


function cargarCombo(id, op) {
    var elemento = $('#'+id);
    var json = '';
    $.ajax({
        url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
        datatype: 'json',
        type: 'GET',
        data: {opcion: 36, op: op},
        async: false,
        success: function (json) {
            console.log(json);
            json = json;
            elemento.append('<option value="0" selected disabled>SELECCIONE...</option>');  
            for (var e in json) {
                elemento.append('<option value="' + e + '">' + json[e] + '</option>');  
            }            
        }
    });
    return json;
}