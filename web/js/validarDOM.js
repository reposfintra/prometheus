/*JUAN MANUEL ESCANDON PEREZ*/
//Funcion que permite llenar un campo oculto con los datos correspondientes a los documentos de una remesa

function AlmacenarInfo(){
	var documentos = "";
	for (i = 1; i < formulario.elements.length; i++){
		if(formulario.elements[i].type == "select-one" ){
			if(formulario.elements[i+1].value == "" )
			formulario.elements[i+1].value = "XX";
			documentos += formulario.elements[i].value+":"+formulario.elements[i+1].value+";"
			formulario.elements[i].value = "001";
		}
		else if(formulario.elements[i].type == "text" ){
			formulario.elements[i].value = "";
		}
	}

	documentos = documentos.substr(0,documentos.length-1);
	formulario.documentosRem.value += ";"+documentos;
}

function Mas(){
	window.opener.form1.docinterno.value = formulario.documentosRem.value;
	window.close();
	//window.location.href=('controller?estado=remesa_docto&accion=Manager&documentos='+formulario.documentosRem.value +'&Opcion=Guardar');
}

function Mas2(numrem){
	
	window.location.href=('../controller?estado=remesa_docto&accion=Manager&documentos='+formulario.documentosRem.value +'&Opcion=Guardar&numrem='+numrem);
	//window.close();
}

function Guardar(numrem, controller){
	window.location.href=(controller+'?estado=remesa_docto&accion=Manager&documentos='+formulario.documentosRem.value +'&Opcion=GuardarDocumento&numrem='+numrem);
}

function agregarFilaDocumentos(cadena, valor){

	var elemento = cadena.split(";");	
	var tabla = document.getElementById("tablah"+valor);
	var numero =tabla.rows.length;
	var filaNueva = tabla.insertRow();
	filaNueva.align="center";
	for( var i=0; i<2; i++ ){
		var td = filaNueva.insertCell(-1);
		td.align="center";
		td.width="50%"
		
		if ( i == 0 ){
			var combo = document.createElement("select");
			for( var j=0; j<elemento.length; j++ ){
				var aux = elemento[j].split(":");
				combo[j] = new Option(aux[1],aux[0]);
			}
			
			var flag   = document.createElement("hidden");
			flag.id    = "flag"+valor+"_DocumentoRel"+numero;
			flag.name  = "flag"+valor+"_DocumentoRel"+numero;
			flag.value = "activo";
			td.appendChild(flag);
			
			var lov  = document.createElement("input");
			lov.type = "checkbox";
			lov.id   = "LOV"+valor+"_TipodocRel"+numero;
			lov.name = "LOV"+valor+"_TipodocRel"+numero;
			lov.onclick = "formulario.flag"+valor+"_DocumentoRel"+numero+".value='anular'";
			td.appendChild(lov);
			
			
			textNode = document.createTextNode(" ");
			td.appendChild(textNode);
			
			
			combo.className = "textbox";
			combo.id ="tp"+valor+"_TipodocRel"+numero;
			combo.name ="tp"+valor+"_TipodocRel"+numero;
			td.appendChild(combo);
		}		
		
		if( i == 1 ){
			var text = document.createElement("input");
			text.className = "textbox";
			text.id = "tp"+valor+"_DocumentoRel"+numero;
			text.name = "tp"+valor+"_DocumentoRel"+numero;
			td.appendChild(text);
		}
	}
}

/**/




function AgregarFilaDOCRELDocumentos(cadena, indice){
	var elemento = cadena.split(";");	
	var tabla = document.getElementById("tabla1");
	if( tabla == null ){
		tabla = document.createElement("table");
		tabla.id = "tabla1";
		tabla.align="center";
		tabla.border="0";
		tabla.width = "100%";
		tabla.className="fila";
		var panel = document.getElementById("panel");
	    	panel.appendChild(tabla);
	}

	var numero = tabla.rows.length;
	indice = numero;
	var filaNueva = tabla.insertRow();
	//index = indice;
	indice = indice + 1;
	
	filaNueva.align="center";
	filaNueva.className = "fila";
	for( var i=0; i<3; i++ ){
		var td = filaNueva.insertCell(-1);
		
		if ( i == 0 ){
			var combo = document.createElement("select");
			for( var j=0; j<elemento.length; j++ ){
				var aux = elemento[j].split(":");
				combo[j] = new Option(aux[1],aux[0]);
			}
			combo.className = "textbox";
			combo.id = "tp"+indice;
			combo.name ="tp"+indice;
			td.appendChild(combo);
		}		
		
		if( i == 1 ){
			var text = document.createElement("input");
			text.className = "textbox";
			text.id = "dp"+indice;
			text.name = "dp"+indice;
			td.appendChild(text);
			
			textNode = document.createTextNode("   ");
			td.appendChild(textNode);
			
			var boton = document.createElement("input");
			boton.type = "button";
			boton.className = "boton";
			boton.value = "+";
			boton.onclick = new Function("agregarFilaDocumentos('"+cadena+"','"+indice+"')")
			td.appendChild(boton);
			
		}
		
		if( i == 2 ){
						
			var tabla = document.createElement("table");
			tabla.id = "tablah"+indice;
			tabla.width="100%";		
			tabla.border="0";
			tabla.cellpadding="0";
			tabla.cellspacing="0";
		
			td.appendChild(tabla);

		}
	}
}








function AlmacenarInfoRel(destinatario){
	var documentos = destinatario+"�";
	//Recorre todas las tablas
	for( var tab = 1 ; tab <= 5 ; tab++){
		var padre = "";
     	var hijos = "";
		var tabla = document.getElementById("tabla"+tab);
		//alert(tabla.id);
		//Recorre las filas de  las tablas
		for(var i = 0; i < tabla.rows.length;i++){
			var fila = tabla.rows[i];
			//Recorre las celdas de una fila
			for( var j = 0; j < fila.cells.length;j++){
				var celda = fila.cells[j];
				var nodo = celda.firstChild;
				if( nodo != null ){
					if( nodo.id == "padre" ){
						if( nodo.value == ""){
							//padre += "XX"+":";
						}
						else{
							padre += nodo.value+":";
						}
						if( nodo.type == "text" ){
							nodo.value = "";
						}
					}//alert(nodo.value);
					else{	
				   		if( nodo.type == "select-one" ){
					  		hijos += nodo.value+":";
				   		}
				   		else if( nodo.type == "text" ){
							if( nodo.value == "" ){
								//hijos += "XX"+";";
								//nodo.value = "";
							}
							else{
								hijos += nodo.value+";";
								nodo.value = "";
							}
				   		}
					}
				}
			}//for celdas
		}//for filas
		
		padre = padre.substr(0,padre.length-1);
		hijos = hijos.substr(0,hijos.length-1);
	
		//tengo padres e hijos
		//alert("padre"+tab + " " +padre);
	//	alert("hijos"+tab + " " +hijos);
		
		var elementos = hijos.split(";");
		
		//recorre los hijos y arma el documentos
		for( var k = 0 ; k < elementos.length ; k++){
			documentos += padre+";"+elementos[k]+"/";
		}
		
		
	}//for tablas
	
	documentos = documentos.substr(0,documentos.length - 1);
	documentos  =documentos+"?"; 
//	if( formulario.documentosRem.value != documentos )
		formulario.documentosRem.value += "/"+documentos;
	
}

function MasRel(destinatario){
	window.opener.form2.elements["docui"+destinatario].value = formulario.documentosRem.value;
	var a =window.opener;
	window.opener.opener.form1.docudest.value = formulario.documentosRem.value;
	//alert(window.opener.opener.form1.docudest.value);
	window.close();
	//formulario.documentosRem.value = formulario.documentosRem.value.substr(1,formulario.documentosRem.length);
	//alert(" Valor Hidden " + document.formulario.documentosRem.value);
	//window.location.href=('controller?estado=remesa_docto&accion=Manager&documentos='+formulario.documentosRem.value +'&Opcion=GuardarRel');
}

function Rel(destinatario, numrem){
	/*window.opener.form2.elements["docui"+destinatario].value = formulario.documentosRem.value;
	var a =window.opener;
	window.opener.opener.form1.docudest.value =window.opener.opener.form1.docudest.value+ formulario.documentosRem.value;*/
	//window.location.href=('controller?estado=remesa_docto&accion=Manager&documentos='+formulario.documentosRem.value +'&Opcion=GuardarRel2&destinatario='+destinatario);
	window.location.href=('../controller?estado=remesa_docto&accion=Manager&documentos='+formulario.documentosRem.value +'&Opcion=GuardarRel2&destinatario='+destinatario+'&numrem='+numrem);
	window.close();
	//formulario.documentosRem.value = formulario.documentosRem.value.substr(1,formulario.documentosRem.length);
	//alert(" Valor Hidden " + document.formulario.documentosRem.value);
	
}

function MasRel2(){
	//formulario.documentosRem.value = formulario.documentosRem.value.substr(1,formulario.documentosRem.length);
	//alert(" Valor Hidden " + document.formulario.documentosRem.value);
	window.location.href=('controller?estado=remesa_docto&accion=Manager&documentos='+formulario.documentosRem2.value +'&Opcion=GuardarRel');
}

function Recorrido(){
	var documentos = "";
	var tabla = document.getElementById("tabla");
	var tam = tabla.rows.length - 2;
	for(var i = 0; i<tam ; i++){
		var original = document.getElementById("Original"+i);
		var tipodoc = document.getElementById("Tipodoc"+i);
		var documento = document.getElementById("Documento"+i);
		documentos += original.value+";"+tipodoc.value+":"+documento.value+"/";
	}
	documentos = documentos.substr(0,documentos.length-1);
	formulario.documentosRem.value = documentos;
	//alert(formulario.documentosRem.value);
	
}// fin funcion

function Recorrido2(){
	var documentos = "";
	var tabla = document.getElementById("tabla");
	var tam = tabla.rows.length - 4;
	var original = "";
	var tipodoc = "";
	var documento = "";
	for(var i = 2; i<tam+2 ; i++){
		var fila = tabla.rows[i];
		for( var j = 0; j < fila.cells.length;j++){
			var celda = fila.cells[j];
			for( var k = 0; k < celda.childNodes.length;k++){
				var nodo = celda.childNodes[k];
				if( nodo != null ){
					var id = nodo.id;
					//alert(nodo.type);
					if( nodo.type == "input"){ 
						original = nodo.value;
						//alert(" ENTRO INPUT "  + original);
					}
					/*if( nodo.id.substr(0,id.length-1) == "Tipodoc"){ 
						tipodoc = nodo.value;
					}
			    	if( nodo.id.substr(0,id.length-1) == "Documento"){ 
						documento = nodo.value;
					}	*/			
				}
			}
		}
		//documentos += original+";"+tipodoc+":"+documento+"/";
	}
	documentos = documentos.substr(0,documentos.length-1);
	formulario.documentosRem.value = documentos;
	//alert(formulario.documentosRem.value);
	
}// fin funcion


function TablaNueva(valorCombo, cadena){
	var elemento = valorCombo.split(";");	
	
var lista = cadena.split("/");
	for( var x=0; x < lista.length; x++ ){
	   
		/*CREO LA TABLA Y SUS PROPIEDADES*/	   
		var tablapapa = document.getElementById("tablefather");
		var ultima = document.getElementById("ultima");
		var tabla = document.createElement("table");
		tabla.id = "tabla"+x;
		tabla.width="80%";
		tabla.align="center"
		tabla.border="1";
		tabla.cellpadding="3";
		tabla.cellspacing="2";
		tabla.bordercolor="#CCCCCC";
		tabla.className = "fila";
		if (tablapapa.parentNode){
			if(tablapapa.nextSibling){
				tablapapa.parentNode.insertBefore(tabla,ultima);
			}
			else
				tablapapa.parentNode.appendChild(tabla);
		}
		
		
		/*RECORRRO PADRES E HIJOS*/
		var lista2 = lista[x].split("[");
		
		for( var y = 0 ; y < lista2.length; y++){
			
			var splitp = lista2[y].split(":");
			
			//Construyo la fila del Padre
			if( y == 0 ){
				var tipodocp = splitp[0];
				var documentop = splitp[1];
		
				var hijo = lista2[1];				
				hijo = hijo.substr(0, hijo.length-1);
				//PRIMER HIJO
				var primerhijo = hijo.split(";");
				
			/*CONSTRUYO LA FILA DEL PADRE*/
			/*INSERTO LA NUEVA FILA*/
				var filaNueva = tabla.insertRow();
				filaNueva.className="fila";
				filaNueva.align="center";
				/*INSERTO LAS COLUMNAS*/
				for( var i=0; i<4; i++ ){
					var td = filaNueva.insertCell(-1);
					td.width="20%";
			
					var text = document.createElement("input");
					var combo = document.createElement("select");
					var hiddenTipoDocOriginal = document.createElement("hidden");
					var hiddenDocOriginal = document.createElement("hidden");
					var hiddenTipoDocRelOriginal = document.createElement("hidden");
					var hiddenDocRelOriginal = document.createElement("hidden");
					
					if( i == 0 || i == 2 ){
						for( var j=0; j<elemento.length; j++ ){
							var aux = elemento[j].split(":");
							combo[j] = new Option("["+aux[0]+"] "+aux[1],aux[0]);
							
						}
						if( i == 0 ){
							combo.value = tipodocp;
							combo.id = "Tipodoc"+0;
							hiddenTipoDocOriginal.value = tipodocp
					        hiddenTipoDocOriginal.id = "OriginalTD"+0;
							td.appendChild(hiddenTipoDocOriginal);
							//alert('ORITP ' + hiddenTipoDocOriginal.value);
						}
						else{
							combo.value = primerhijo[0].split(":")[0];
							combo.id = "TipodocRel"+0;
							hiddenTipoDocRelOriginal.value = primerhijo[0].split(":")[0];
							hiddenTipoDocRelOriginal.id = "OriginalTDR"+0;
						    td.appendChild(hiddenTipoDocRelOriginal);
						}
						combo.className = "textbox";					
						td.appendChild(combo);
						

					}
					if( i == 1 ){
						text.className = "textbox";
						text.value = documentop;
						text.id = "Documento"+0;
						hiddenDocOriginal.value = documentop;
						hiddenDocOriginal.id = "OriginalDocumento"+0;
	
						td.appendChild(text);
						td.appendChild(hiddenDocOriginal);
					}
					if( i == 3 ){
						text.className = "textbox";
						text.id = "DocumentoRel"+0;
						text.value = primerhijo[0].split(":")[1];
	
						hiddenDocRelOriginal.value = primerhijo[0].split(":")[1];
						hiddenDocRelOriginal.id = "OriginalDocumentoR"+0;
						
						/*var boton = document.createElement("input");
						boton.type = "button";
						boton.className = "boton";
						boton.value = "+";
						

						boton.onclick = new Function("agregarFila('"+valorCombo+"','"+x+"')");*/
						
						td.align="left";
						td.appendChild(text);
						td.appendChild(hiddenDocRelOriginal);
						//td.appendChild(boton);
					}
				}
			}//FIN IF FILA PAPA
			
			/* PROCEDIMIENTO HIJOS*/
			else{
				lista2[y] = lista2[y].substr(0, lista2[y].length-1);
				
				var splitp = lista2[y].split(";");
				//alert(splitp);
				for( var h = 1 ; h < splitp.length ; h++){
					var hiddenTipoDocOH = document.createElement("hidden");
					var hiddenDocOH  = document.createElement("hidden");
					
					filaNueva = tabla.insertRow();
					filaNueva.className="fila";
					filaNueva.align="center";
					for( var t=0; t<4; t++ ){
						
						var aux2 = splitp[h].split(":");
						
						td = filaNueva.insertCell(-1);
						td.width="20%";
						var texth = document.createElement("input");
						var comboh = document.createElement("select");
						if( t == 2 ){
							for( var k = 0; k < elemento.length; k++ ){
								var aux = elemento[k].split(":");
								comboh[k] = new Option("["+aux[0]+"] "+aux[1],aux[0]);
							}
							comboh.className = "textbox";							
							comboh.value = aux2[0];
							combo.align="center";
							comboh.id = "TipodocRel"+h;
							hiddenTipoDocOH.value = aux2[0];
							hiddenTipoDocOH.id = "OriginalTDR"+h;
							td.appendChild(hiddenTipoDocOH);
							td.appendChild(comboh);
							
							
							
						}
						if( t == 3 ){
							texth.className = "textbox";
							texth.id = "DocumentoRel"+h;

							texth.value = splitp[h].split(":")[1];
							
							hiddenDocOH.value = aux2[1];
							hiddenDocOH.id = "OriginalDocumentoR"+h;
						    td.align="left";
							td.appendChild(texth);
							td.appendChild(hiddenDocOH);
							
							//alert("INSERTO DOCREL");
						}
					}// fin for
				}// fin for
			
			}//fin else hijos
		}
	}						
}// fin function

/*FUNCION RECORRIDO PARA CREAR CADENA PARA MODIFICACION*/
function RecorridoREL(cadena){

	var lista = "";
	
	//var cadena =  "003:333[005:333;008:333]/005:5632[002:2510;009:4567;008:9999]";
	var lon = cadena.split("/").length;

for( var l = 0 ; l < lon ; l++ ){
		var documentos = "";
	    var original = "";
		var tabla = document.getElementById("tabla"+l);
		
		//alert("NUMERO DE FILAS "+ tabla.rows.length);
		for(var i = 0; i<tabla.rows.length ; i++){
			var cont = i + 1;
			//alert("FILA # " +cont);
			var fila = tabla.rows[i];
			//alert(" CELDAS " + fila.cells.length );
			for( var j = 0; j < fila.cells.length;j++){
				var cont1 = j + 1;
				//alert( " CELDA # " + cont1);
				var celda = fila.cells[j];
				//hacer recorridos por todos los nodos.
				//var ListaNodos = celda.childNodes;
				
				//alert("NODOS " +  celda.childNodes.length);
			    for( var k = 0; k < celda.childNodes.length;k++){
					//var cont2 = k +1;
					//alert("NODO # " + cont2);
					
					var nodo = celda.childNodes[k];
					if( nodo != null ){
						var id = nodo.id;
						//alert("NODO ID "+ nodo.id);
						//alert("VALOR NODO # " + cont2 + " = " + nodo.value);
						if( nodo.id.substr(0,id.length-1) == "OriginalTD"){ 
							var originaltd = nodo.value;
						}
						if( nodo.id.substr(0,id.length-1) == "Tipodoc"){ 
							var tipodoc = nodo.value;
						}
			    		if( nodo.id.substr(0,id.length-1) == "OriginalDocumento"){ 
							var originaldocumento = nodo.value;
						}
						if( nodo.id.substr(0,id.length-1) == "Documento"){ 	
							var documento = nodo.value;
						}
						if( nodo.id.substr(0,id.length-1) == "OriginalTDR"){ 
							var originaltdr = nodo.value;
						}
						if( nodo.id.substr(0,id.length-1) == "TipodocRel"){ 
							var tipodocrel = nodo.value;
						}
			    		if( nodo.id.substr(0,id.length-1) == "OriginalDocumentoR"){ 
							var originaldocumentor = nodo.value;
						}
						if( nodo.id.substr(0,id.length-1) == "DocumentoRel"){ 		
							var documentorel = nodo.value;
						}
					}//FIN FIN
				}//FIN FOR NODOS
			}//FOR CELDAS
			
			if ( i == 0 ){				
				documentos += tipodoc+":"+documento+"�"+tipodocrel+":"+documentorel;
				original += originaltd+":"+originaldocumento+"�"+originaltdr+":"+originaldocumentor;											 											
			}
			else{
				documentos += ";"+tipodocrel+":"+documentorel;
				//alert('Otros hijo' + tipodocrel+":"+documentorel );
				original += ";"+originaltdr+":"+originaldocumentor;
			}				
		}//FIN FILAS
			
		documentos += "?";
    	original += "?";
    	lista += documentos+"%"+original+"/";

	//
	}//FIN TABLAS
	
	lista = lista.substr( 0, lista.length - 1);
	formulario.documentosRem.value = lista;
	alert(formulario.documentosRem.value);
		
}// fin funcion



function InfoRel(cadena){
	var documentos = "";
	var lon = cadena.split("/").length;

	//Recorre todas las tablas
	for( var l = 0 ; l < lon ; l++ ){

		var padre = "";
     	var hijos = "";
		var tabla = document.getElementById("tabla"+l);

		//Recorre las filas de  las tablas
		for(var i = 0; i < tabla.rows.length;i++){
			var fila = tabla.rows[i];
			//Recorre las celdas de una fila
			for( var j = 0; j < fila.cells.length;j++){
				var celda = fila.cells[j];
				
				for( var k = 0; k < celda.childNodes.length;k++){
					var nodo = celda.childNodes[k];
					if( nodo != null ){
						var id = nodo.id;
						if( nodo.id.substr(0,id.length-1) == "Tipodoc" ){
							padre += nodo.value+":";
						}
						if( nodo.id.substr(0,id.length-1) == "Documento" ){
							if( nodo.value == "" )
								padre += "XX";
							else
								padre += nodo.value;
						}
						if( nodo.id.substr(0,id.length-1) == "TipodocRel" ){
							hijos += nodo.value+":";
						}
						if( nodo.id.substr(0,id.length-1) == "DocumentoRel"){ 	
							if( nodo.value == "" )
								hijos += "XX"+";";
							else
								hijos += nodo.value+";";
						}
					}
				}
			}//for celdas
		}//for filas
		
		//padre = padre.substr(0,padre.length-1);
		hijos = hijos.substr(0,hijos.length-1);
	
		
		var elementos = hijos.split(";");
		//recorre los hijos y arma el documentos
		for( var k = 0 ; k < elementos.length ; k++){
			documentos += padre+";"+elementos[k]+"/";
		}
		
		
	}//for tablas
	
	documentos = documentos.substr(0,documentos.length - 1);
	formulario.documentosRem2.value = documentos;

	//alert(formulario.documentosRem2.value);
}


/*CARGAR AGREGAR DOCUMENTOS*/
function Cargar(valorCombo){
	var elemento = valorCombo.split(";");	
	var documentos = window.opener.form1.docinterno.value;
	documentos = documentos.substr(1, documentos.length );
	var cadena = "";
	if( documentos.length > 0 ){
		 cadena = documentos.split(";");
	
		//Recorrido cadena
		for( var i = 0; i < cadena.length ; i++ ){
			//Hago recorrido
			var j = i+1;
			if( j <= cadena.length ){
				if ( j <= 5 ){
					var combo = document.getElementById("tipodoc"+j);
					combo.value = cadena[i].split(":")[0];
					var text = document.getElementById("documento"+j);
					if( cadena[i].split(":")[1] != "XX"){
						text.value = cadena[i].split(":")[1];
					}
				}
				else{
					if( cadena[i].split(":")[1] != "XX"){
						var tabla = document.getElementById("tabla");
						//alert('Agregamos filas y columas');				
						var filaNueva = tabla.insertRow(j+1);
						for( var k=0; k<2; k++ ){
							var td = filaNueva.insertCell(-1);
							if ( k == 0 ){
								var combo = document.createElement("select");
								for( var l=0; l<elemento.length; l++ ){
									var aux = elemento[l].split(":");
									combo[l] = new Option(aux[1],aux[0]);
								}
								combo.className = "textbox";
								combo.id = "tipodoc"+j;
								combo.value = cadena[i].split(":")[0];
								td.appendChild(combo);
							}		
							if( k == 1 ){
								var text = document.createElement("input");
								text.className = "textbox";
								text.id = "DocumentoRel"+i
								text.size = 50;
								text.value = cadena[i].split(":")[1];
								td.appendChild(text);
							}					
						}
					}
				}
			}
		}
	}	
}


/*CARGAR DOCUMENTOS RELACIONADOS*/
/*function CargarRel2(valorCombo, destinatario){
	var e = "";
	var elemento = valorCombo.split(";");	
	var documentos = window.opener.opener.form1.docudest.value;
		
	if( documentos.length != "" ){
		
		destinatario = destinatario+"�";
		
		documentos = documentos.substr(1, documentos.length-2);	

        documentos = documentos.replace(new RegExp( destinatario, ['g']),'');
		documentos = documentos.replace(/008:XX;008:XX/g, '' );
	    documentos = documentos.replace(/009:XX;009:XX/g, '' );
		documentos = documentos.replace(/008:XX;009:XX/g, '' );
		documentos = documentos.replace(/009:XX;008:XX/g, '' );
		documentos = documentos.substr(0, documentos.length-1);	

		
		alert(documentos);		
		
		var cadena1 = documentos.split("/");		
		
		var padreanterior = "";
		alert(cadena1.length );
		for( var i = 0; i < cadena1.length ; i++ ){					
			//VALIDACION DE DESTINATARIOS
			alert("VALOR i " + i );
			alert( cadena1[i] );
			if( cadena1[i] != "" && cadena1[i] != "?" ){
				if( cadena1[i].indexOf("�") != -1){				
					var cadena2 = cadena1[i].split("�");
					var cadena = cadena2[1];
					e = cadena.split(";");
				}			
				else
					e = cadena1[i].split(";");
	
				padreactual = e[0].split(":")[0]+":"+e[0].split(":")[1];			
							
				//INSERTA EL PADRE
				if( padreanterior != padreactual ){
					var h = 1;
					var j =i+1;
					//if( j <= cadena1.length ){						
						var tabla = document.getElementById("tabla"+j)								
						for( var p = 0 ; p < tabla.rows.length ; p++ ){												
							var fila = tabla.rows[p];						
							//if ( j <= 5 ){		
								for( var l = 0; l < fila.cells.length ; l++){
									var celda = fila.cells[l];
				    				var nodo = celda.firstChild;						
									//alert("NODO ID" + nodo.id );
									//INSERTA VALORES EN LOS NODOS PADRES SI EL VALOR DEL DOCUMENTO ES DIFERENTE DE 'XX'
									if( e[0].split(":")[1] != "XX"){
									//COMBO PADRE
										if( nodo.id == "padre" && nodo.type == "select-one" ){
											nodo.value = e[0].split(":")[0];
						    				//alert('COMBO PADRE ' + nodo.value);
										}
										//TEXT PADRE
										if ( nodo.id == "padre" && nodo.type == "text" ){
											nodo.value = e[0].split(":")[1]; 
										//alert('TEXT PADRE' + nodo.value);
										}
									}						
									//INSERT VALORES+ EN EL NODOS HIJO SI EL VALOR DEL DOCUMENTO ES DIFERENTE DE 'XX'
									if( e[1].split(":")[1] != "XX"){
										//COMBO HIJO
										if( nodo.id == "tipodoc"+j ){
											nodo.value = e[1].split(":")[0];
										}
							
										//TEXT HIJO
										if( nodo.id == "documento"+j ){
											nodo.value = e[1].split(":")[1];
										}
									}//VALIDACION DOCUMENTO 'XX'
								}//ROWS
							}//FOR FILAS					
					//}//IF PADRE
					padreanterior = padreactual;
				}//IF PADRE
			// INSERTO LOS HIJOS
				else{
					if( e[1].split(":")[1] != "XX" && e[1].split(":")[1] != "XX?"  ){
				
						var tabla = document.getElementById("tabla"+j)
            			h++;
						filaNueva = tabla.insertRow();
						filaNueva.className="fila";
						filaNueva.align="center";
						for( var t=0; t<4; t++ ){		
							td = filaNueva.insertCell(-1);							
							var texth = document.createElement("input");
							var comboh = document.createElement("select");
				
						//INSERTAMOS EL COMBO
							if( t == 2 ){
								for( var k = 0; k < elemento.length; k++ ){
									var aux = elemento[k].split(":");
									comboh[k] = new Option("["+aux[0]+"] "+aux[1],aux[0]);
								}//FIN FOR K
								comboh.className = "textbox";							
								comboh.value = e[1].split(":")[0];
								comboh.align="center";
								comboh.id = "tipodoc"+h;
								td.appendChild(comboh);							
							}// FIN IF T == 2
							//INSETAMOS
							if( t == 3 ){
								texth.className = "textbox";
								texth.id = "documento"+h;
								texth.value = e[1].split(":")[1];
								td.align="left";
								td.appendChild(texth);																					
							}//FIN IF T ==3
						}//FIN FOR T					
					}//FIN IF
				}//fin else hijos													
			}//FOR CADENA1
		}//IF DOCUMENTO	
	//}
}//FIN FUNCION*/



/*FUNCION QUE PERMITE CARGAR LOS DOCUMENTOS MODIFICIDOS PARA */
function CARGARDOCMOD( valorCombo, cadena ){
	
	if( cadena.length > 0 ){
		
	cadena = cadena.substr(0, cadena.length - 1 );
	var elemento = valorCombo.split(";");	
	var tabla = document.getElementById("tabla");
    documentos = cadena.split(";");
	for( var i = 0; i < documentos.length ; i++ ){
		var filaNueva = tabla.insertRow();
		filaNueva.className = "fila";
		for( var k=0; k<2; k++ ){
			var td = filaNueva.insertCell(-1);						
			
				if ( k == 0 ){
					var combo = document.createElement("select");
						for( var l=0; l<elemento.length; l++ ){
							var aux = elemento[l].split(":");
							combo[l] = new Option(aux[1],aux[0]);
						}
												
						var lov  = document.createElement("input");
						lov.type = "checkbox";
						lov.id   = "LOV";
						lov.name = "LOV";
						td.appendChild(lov);
						
						combo.className = "textbox";
						combo.id = "Tipodoc"+i;
						combo.value = documentos[i].split(":")[0];
						td.appendChild(combo);
						
						var hidden = document.createElement("hidden");
						hidden.value = documentos[i].split(":")[0]+":"+documentos[i].split(":")[1];
						hidden.id = "Original"+i;
						td.appendChild(hidden);
				}		
				if( k == 1 ){
					var text = document.createElement("input");
					text.className = "textbox";
					text.id = "Documento"+i;
					text.size = 50;
					text.value = documentos[i].split(":")[1];
					
					text.onkeypress = new Function("soloAlfa(event)");
					
					td.appendChild(text);
				}
			}		
			
			var temp1 =  document.getElementById("Tipodoc"+i);
			var temp2 =  document.getElementById("Documento"+i);
			lov.value =  temp1.value+":"+temp2.value;
			
		}//Fin For
		
			
	}//fin cadena
}




/*CARGAR DOCUMENTOS RELACIONADOS*/
function CargarRel(valorCombo, destinatario){
	var e = "";
	var elemento = valorCombo.split(";");	
	var documentos = window.opener.opener.form1.docudest.value;
		
	if( documentos.length != "" ){
		
		destinatario = destinatario+"�";
		
		documentos = documentos.substr(1, documentos.length-2);	

        documentos = documentos.replace(new RegExp( destinatario, ['g']),'');
		documentos = documentos.replace(/008:XX;008:XX/g, '' );
	    documentos = documentos.replace(/009:XX;009:XX/g, '' );
		documentos = documentos.replace(/008:XX;009:XX/g, '' );
		documentos = documentos.replace(/009:XX;008:XX/g, '' );
		documentos = documentos.substr(0, documentos.length-1);	

		
		alert(documentos);		
		
		var cadena1 = documentos.split("/");		
		
		var padreanterior = "";
		alert(cadena1.length );
		for( var i = 0; i < cadena1.length ; i++ ){					
			//VALIDACION DE DESTINATARIOS			
	        alert("CADENA " + cadena1[i] );
			if( cadena1[i] != "" && cadena1[i] != "?"){				
				e = cadena1[i].split(";");
				
				padreactual = e[0].split(":")[0]+":"+e[0].split(":")[1];			
							
				//INSERTA EL PADRE
				if( padreanterior != padreactual ){
					var h = 1;
					var j = i+1;
					if( j <= cadena1.length ){												
						if( j <= 5 ){
							var tabla = document.getElementById("tabla"+j)													
							for( var p = 0 ; p < tabla.rows.length ; p++ ){												
								var fila = tabla.rows[p];						
									for( var l = 0; l < fila.cells.length ; l++){
										var celda = fila.cells[l];
				    					var nodo = celda.firstChild;						
										//INSERTA VALORES EN LOS NODOS PADRES SI EL VALOR DEL DOCUMENTO ES DIFERENTE DE 'XX'
										if( e[0].split(":")[1] != "XX"){
											//COMBO PADRE
											if( nodo.id == "padre" && nodo.type == "select-one" ){
												nodo.value = e[0].split(":")[0];
						    				//alert('COMBO PADRE ' + nodo.value);
											}
											//TEXT PADRE
											if ( nodo.id == "padre" && nodo.type == "text" ){
												nodo.value = e[0].split(":")[1]; 
											//alert('TEXT PADRE' + nodo.value);
											}
										}						
										//INSERT VALORES+ EN EL NODOS HIJO SI EL VALOR DEL DOCUMENTO ES DIFERENTE DE 'XX'
										if( e[1].split(":")[1] != "XX"){
											//COMBO HIJO
											if( nodo.id == "tipodoc"+j ){
												nodo.value = e[1].split(":")[0];
											}
							
											//TEXT HIJO
											if( nodo.id == "documento"+j ){
												nodo.value = e[1].split(":")[1];
											}
										}//VALIDACION DOCUMENTO 'XX'
									}//ROWS
								}//FOR FILAS
							}//J <= 5
							else{																								
								FilaPadre(j,e, valorCombo)								
							}
						}//IF J < LENGTH							
				padreanterior = padreactual;
				}//IF PADRE
			// INSERTO LOS HIJOS
			else{
				if( e[1].split(":")[1] != "XX" && e[1].split(":")[1] != "XX?"  ){
				
					var tabla = document.getElementById("tabla"+j)
            		h++;
					filaNueva = tabla.insertRow();
					filaNueva.className="fila";
					filaNueva.align="center";
					for( var t=0; t<4; t++ ){		
						td = filaNueva.insertCell(-1);
						//td.width="25%";
						var texth = document.createElement("input");
						var comboh = document.createElement("select");
				
						//INSERTAMOS EL COMBO
						if( t == 2 ){
							for( var k = 0; k < elemento.length; k++ ){
								var aux = elemento[k].split(":");
								comboh[k] = new Option("["+aux[0]+"] "+aux[1],aux[0]);
							}//FIN FOR K
							comboh.className = "textbox";							
							comboh.value = e[1].split(":")[0];
							comboh.align="center";
							comboh.id = "tipodoc"+h;
							td.appendChild(comboh);
							
						}// FIN IF T == 2
						//INSETAMOS
						if( t == 3 ){
							texth.className = "textbox";
							texth.id = "documento"+h;
							texth.value = e[1].split(":")[1];
							td.align="left";
							td.appendChild(texth);																					
						}//FIN IF T ==3
					}//FIN FOR T					
				}//FIN IF
			}//fin else hijos													
			}//FOR CADENA1
		}//FIN IF
	}//FIN DOCUMENTOS
}//FIN FUNCION



/*FUNCION CREAR FILA PADRE*/
function FilaPadre(x,lista, valorCombo){	
	var elemento = valorCombo.split(";");	
	var tablapapa = document.getElementById("tablefather");
	var ultima = document.getElementById("ultima");
	var tabla = document.createElement("table");
	tabla.id = "tabla"+x;
	tabla.width="100%";
	tabla.align="center"
	tabla.border="1";
	tabla.cellpadding="3";
	tabla.cellspacing="2";
	tabla.bordercolor="#CCCCCC";
	tabla.className = "fila";
	if (tablapapa.parentNode){
		if(tablapapa.nextSibling){
			tablapapa.parentNode.insertBefore(tabla,ultima);
		}
		else{
			tablapapa.parentNode.appendChild(tabla);
		}		
	}
	
	

	var filaNueva = tabla.insertRow();
	filaNueva.className="fila";
	filaNueva.align="center";
	/*INSERTO LAS COLUMNAS*/
	for( var i=0; i<4; i++ ){
		var td = filaNueva.insertCell(-1);
		td.width="20%";			
		var text = document.createElement("input");
		var combo = document.createElement("select");

		var papa = new String(lista).split(",")[0];
		var hijo = new String(lista).split(",")[1];	
		
		if( i == 0 || i == 2 ){
			for( var j=0; j<elemento.length; j++ ){
				var aux = elemento[j].split(":");
				combo[j] = new Option("["+aux[0]+"] "+aux[1],aux[0]);							
			}
			if( i == 0 ){				
				combo.id = "padre";		
				combo.value = papa.split(":")[0];						
			}
			else{				
				combo.id = "tipodoc"+x;		
				combo.value = hijo.split(":")[0];
			}
			combo.className = "textbox";					
			td.appendChild(combo);						
		}
		
		
		text.className = "textbox";		
		if( i == 1 ){
			text.id = "padre";
			text.value = papa.split(":")[1];
			td.appendChild(text);
		}
		if( i == 3 ){
			text.id = "documento"+x;
			text.value = hijo.split(":")[1];
			td.appendChild(text);				
			
			var boton = document.createElement("input");
			boton.type = "button";
			boton.className = "boton";
			boton.value = "+";
			boton.onclick = new Function("agregarFila('"+valorCombo+"','"+x+"')");
			td.appendChild(boton);				
		}
	}
	
}//fin function


function AlmacenarInfoRelK(destinatario){
	alert("Entre a la funcion");
	var documento = destinatario+"�";
	var numero =1;
	for (i=0; i < formulario.elements.length; i++){
		alert(formulario.elements[i].name);
		/*if(formulario.elements[i].id=="dp"+numero){
			alert("Elemento actual "+formulario.elements[i].id);
			var elemento = document.getElementByID(formulario.elements[i].id);
			if(elemento.value!=""){
				alert("valor del documento "+documento);
				documento = documento+document.getElementByID("tp"+numero).value+":"+formulario.elements[i].id.value;
				alert("valor del documento "+documento);
				alert("Voy a recorrer el formulario nuevamente");
				for (j=0; j < formulario.elements.length; j++){
					if(formulario.elements[j].id.indexOf("_")>0){
						var a = formulario.elements[j].id.split("_");
						if(a[0]==formulario.elements[j].id){
							alert(formulario.elements[j].type);
						}
					}
				
				}
			}
		}*/
		
	}

}

function soloNumText(e) {
	var isIE = document.all?true:false;
	var key = (isIE) ? window.event.keyCode : e.which;
	//alert (key);
	//var obj = (isIE) ? event.srcElement : e.target;
	var isNum = ( (key > 44 && key < 58) ||  ( key > 96) || (key==32) || ( key >64  &&  key < 95) )? true:false;
	//window.event.keyCode = (isNum && isIE) ? 0:key;
//	e.which = (!isNum && isNS) ? 0:key;
	return (isNum);
}

function agregarFila(cadena, valor){

	var elemento = cadena.split(";");	
	var tabla = document.getElementById("tablah"+valor);
	var numero =tabla.rows.length;
	var filaNueva = tabla.insertRow();
	filaNueva.align="center";
	for( var i=0; i<2; i++ ){
		var td = filaNueva.insertCell(-1);
		td.align="center";
		td.width="50%"
		
		if ( i == 0 ){
			var combo = document.createElement("select");
			for( var j=0; j<elemento.length; j++ ){
				var aux = elemento[j].split(":");
				combo[j] = new Option(aux[1],aux[0]);
			}
			combo.className = "textbox";
			combo.id ="tp"+valor+"_TipodocRel"+numero;
			combo.name ="tp"+valor+"_TipodocRel"+numero;
			td.appendChild(combo);
		}		
		
		if( i == 1 ){
			var text = document.createElement("input");
			text.className = "textbox";
			text.id = "tp"+valor+"_DocumentoRel"+numero;
			text.name = "tp"+valor+"_DocumentoRel"+numero;
			var elementAnt = document.getElementById("funcion");
			if(elementAnt!=null)			
				text.onkeypress=elementAnt.onkeypress;

			td.appendChild(text);
		}
	}
}
function AgregarFilaDOCREL(cadena, indice){


	var elemento = cadena.split(";");	
	var tabla = document.getElementById("tabla1");
	if( tabla == null ){
		tabla = document.createElement("table");
		tabla.id = "tabla1";
		tabla.align="center";
		tabla.border="0";
		tabla.width = "100%";
		tabla.className="fila";
		var panel = document.getElementById("panel");
	    	panel.appendChild(tabla);
	}

	var numero = tabla.rows.length;
	indice = numero;
	var filaNueva = tabla.insertRow();
	//index = indice;
	indice = indice + 1;
	
	filaNueva.align="center";
	filaNueva.className = "fila";
	for( var i=0; i<3; i++ ){
		var td = filaNueva.insertCell(-1);
		if ( i == 0 ){
			var combo = document.createElement("select");
			for( var j=0; j<elemento.length; j++ ){
				var aux = elemento[j].split(":");
				combo[j] = new Option(aux[1],aux[0]);
			}
			combo.className = "textbox";
			combo.id = "tp"+indice;
			combo.name ="tp"+indice;
			td.appendChild(combo);
		}		
		
		if( i == 1 ){
			

			//var funcion = textOld.onkeypress;
			var text = document.createElement("input");
			text.className = "textbox";
			text.id = "dp"+indice;
			text.name = "dp"+indice;
			
			var elementAnt = document.getElementById("funcion");
			if(elementAnt!=null)			
				text.onkeypress=elementAnt.onkeypress;
			//alert(text.onkeypress);
			td.appendChild(text);
			
			textNode = document.createTextNode("   ");
			td.appendChild(textNode);
			
			var boton = document.createElement("input");
			boton.type = "button";
			boton.className = "boton";
			boton.value = "+";
			boton.onclick = new Function("agregarFila('"+cadena+"','"+indice+"')")
			td.appendChild(boton);
			
		}
		
		if( i == 2 ){
						
			var tabla = document.createElement("table");
			tabla.id = "tablah"+indice;
			tabla.width="100%";		
			tabla.border="0";
			tabla.cellpadding="0";
			tabla.cellspacing="0";
		
			td.appendChild(tabla);
			
		}
	}
}
/*Enrique de la valle*/

//Funcion que valida que no se ingresen documentos relacionados repatidos a un despacho u orden de carga

function validarDocRelacionados( longitud ){
	var sw = true;			
	for (var i=0; i<longitud;i++){
		
		var tipodoc 	= document.getElementById('tipodoc'+i);
		var documento 	= document.getElementById('documento'+i);
		
		for (var j=i+1; j<longitud;j++){
			var tipodocaux 		= document.getElementById('tipodoc'+j);
			var documentoaux 	= document.getElementById('documento'+j);
					
			if( documento.value != '' ){
				if( tipodoc.value == tipodocaux.value && documento.value == documentoaux.value ){
					var aviso = "Documento Repetido!\n       Verifiquelos"
					alert(aviso);
					return (false);
					break;
				}
			}
		
		}// for j
		
	}//for i
	return sw;				
}

// --- kike the best --
//Funcion que valida que no se ingresen documentos relacionados repatidos a un despacho u orden de carga 

function validarDocRelacionados2(){
	var sw = true;
	var cantElements = document.formulario.length;
	var cont = 0, cont2 = 0, contLlaves = 0;
	var numDocReltp = [], llavesDocRel = [];
	//cuenta el numero de tp que hay en el formulario y sus documentso relacionados hijos
	for(var i=0; i<cantElements;i++){
		var tp = document.formulario.elements[i];
		if(tp.name.length==3 && tp.name.substring(0,2)=="tp" ){
			
			for(var j=i+3;j<cantElements;j++){
				var tpDocRel = document.formulario.elements[j];
				if(tpDocRel.name.length==15 && tpDocRel.name.substring(0,3)==tp.name){
					cont2=cont2+1;					 
				}
			}//fin for j
			numDocReltp[cont] = cont2;
			cont = cont + 1;
			cont2=0;
		}
	}//fin for i
	var llave = "", llaveAux = "";
	
	for(var k=0; k<cantElements;k++){
		var tp2 = document.formulario.elements[k];
		if(tp2.name.length==3 && tp2.name.substring(0,2)=="tp" ){
			llave = llave +' '+ tp2.value+' '+ document.formulario.elements[k+1].value;
			
			for(var l=k+3;l<cantElements;l++){
				var tpDocRel2 = document.formulario.elements[l];
				if (numDocReltp[cont2]!=0){
					
					if(document.formulario.elements[k+1].value==""){
						alert("Codigo de Documento relacionado vac�o\n Verifiquelo");						
						return(false);
					}
					
					
					if(tpDocRel2.name.length==15 && tpDocRel2.name.substring(0,3)==tp2.name){
						llaveAux = llave+' '+tpDocRel2.value+' '+ document.formulario.elements[l+1].value;
						llavesDocRel[contLlaves] = llaveAux;
						contLlaves = contLlaves+1;
						llaveAux = "";
					}
					
					
				}
						
			}	
			cont2 = cont2 + 1;
			llave = "";
		}
	}
	sw = encontrarRepetidos(llavesDocRel)
	
	return (sw);
		
					
}
//encuentra las llaves repetidas
function encontrarRepetidos(llaves){
	
	var sw = true;
	var long = llaves.length;
	
	for(var i=0; i<long; i++){
		
		for (var j=i+1; j<long;j++){
			if(llaves[i]==llaves[j]){
				var aviso = "Documento Repetido!\n       Verifiquelos"
				alert(aviso);
				return(false); 
				break;
			}
		}
	}
	
	return(sw); 
	
}

//Diogenes

function campoformulario (num) {
	var tip = document.getElementById("tipodoc"+num);


	var ffec = document.getElementById("ffecha"+num);
	var fcon = document.getElementById("fcon"+num);

    var infdato = tip.value.split("-");

    
    if ( infdato[0] == 'S' ){
		ffec.style.display = "block";        
	    fcon.style.display = "block";
	}else if ( infdato[0] == 'N' && infdato[1] == 'S') {
		ffec.style.display = "none";     
	    fcon.style.display = "block";
	}
	else {
		ffec.style.display = "none";     
	    fcon.style.display = "none";
	}
	
}

function validarDocumentos () {
	var num = formulario.items.value;
	var con = 0;
	for (i = 0; i < num; i++){    
		var tip = document.getElementById("tipodoc"+i).value; 
		var documen = document.getElementById("documento"+i).value;
		var des = document.getElementById("descripcion"+i).value;
		var fecsia = document.getElementById("fecha_sia"+i).value;
		var feceta = document.getElementById("fecha_eta"+i).value;
		var infdato = tip.split("-");
		if(documen != ''){
			if ( infdato[0] == 'S' ){
				if( fecsia=='' || feceta == ''){
					alert('Verifique que las 2 fechas del documento '+documen+' esten seleccionadas');
					return (false);
	                break;
				}
			}
			con++;
		}
	}
	if(con==0){
		alert('Se debe registrar por lo menos un documento');
		return (false);
	}
	formulario.submit();
}

function Borrar(){
	var num = formulario.items.value;
	var con = 0;
	for (i = 0; i < num; i++){ 
		 if( document.getElementById("pos"+i).checked )
			 con++;
	}
	if(con>0){
    	formulario.Opcion.value='AnularDocumento';
		formulario.submit(); 
	}else{
		alert('Seleccione un documento');
		return (false);
	}
}