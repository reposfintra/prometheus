/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechacxc").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $("#fecha").datepicker("setDate", myDate);
    $("#fechacxc").datepicker("setDate", myDate);
    $("#fechafin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    cargarTransportadoras();

    $("#buscar").click(function () {
        var tr = $('#transportadoras').val();
        var fech = $('#fechacxc').val();
        if (tr === "") {
            mensajesDelSistema("SELECIONE UNA TRANSPORTADORA", '200', '150', false);
        } else {
            if ((fech == null)) {
                mensajesDelSistema("NO SE ENCONTRARON REGISTROS ", '200', '150', false);
            } else {
                cargarCxcGeneradas();
            }
        }
    });
});
function cargarTransportadoras() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 9
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#transportadoras').html('');
                $('#transportadoras').append("<option value=''> Seleccione.. </option>");
                for (var datos in json) {
                    $('#transportadoras').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCxcGeneradas() {
    var grid_tabla = jQuery("#tabla_cargar_cxcGenerada");
    if ($("#gview_tabla_cargar_cxcGenerada").length) {
        reloadGridMostrar(grid_tabla, 29);
    } else {
        grid_tabla.jqGrid({
            caption: "CXC GENERADAS",
            url: "./controller?estado=Administracion&accion=Etes",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '1290',
            colNames: ['id transportadora', 'Nit', 'Nombre Cliente', 'Documento', 'Valor Factura', 'Valor Abono', 'Valor Saldo', 'Fecha vencimiento', 'Fecha Ultimo Pago', 'Usuario de Creacion', 'Fecha de Creacion', 'Fecha Corrida', '# Planillas'],
            colModel: [
                {name: 'transid', index: 'transid', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'nitcli', index: 'nitcli', width: 130, sortable: true, align: 'left', hidden: false},
                {name: 'nomcli', index: 'nomcli', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'facdocumento', index: 'facdocumento', width: 100, sortable: true, align: 'left', hidden: false, key: true},
                {name: 'facvlr', index: 'facvlr', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'facabono', index: 'facabono', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'facsaldo', index: 'facsaldo', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'facfechvencimiento', index: 'facfechvencimiento', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'facfechultimopago', index: 'facfechultimopago', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'usercreacion', index: 'usercreacion', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'faccreacion', index: 'faccreacion', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'fechacorrida', index: 'fechacorrida', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'numplanilla', index: 'numplanilla', width: 60, sortable: true, align: 'center', hidden: false}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_cargar_cxcGenerada"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var facdoc = filas.facdocumento;
                var fecha = filas.fechacorrida;
                var transportadora = filas.transid;
                ventanaCXC(facdoc, fecha, transportadora);
            }, loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("NO SE ENCONTRARON REGISTROS ", '200', '150', false);
                }
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "post",
                data: {
                    opcion: 29,
                    transportadora: $("#transportadoras").val(),
                    fecha: $("#fechacxc").val(),
                    fechafin: $("#fechafin").val()
                }
            }, loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, gridComplete: function () {
                var Totalabono = grid_tabla.jqGrid('getCol', 'facabono', false, 'sum');
                var Totalfac = grid_tabla.jqGrid('getCol', 'facvlr', false, 'sum');
                var Totalsaldo = grid_tabla.jqGrid('getCol', 'facsaldo', false, 'sum');
                grid_tabla.jqGrid('footerData', 'set', {facdocumento: 'TOTALES', facvlr: Totalfac, facabono: Totalabono, facsaldo: Totalsaldo});
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });

        $("#tabla_cargar_cxcGenerada").navButtonAdd('#pager', {
            caption: "Exportar Excel",
            onClickButton: function () {
                var info = grid_tabla.getGridParam('records');
                if (info > 0) {
                    exportarExcel();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }

            }
        });

    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Etes",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                transportadora: $("#transportadoras").val(),
                fecha: $("#fechacxc").val(),
                fechafin: $("#fechafin").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function reloadGridMostrarDetalle(grid_tabladet, opcion, facdoc, fecha, transportadora) {
    grid_tabladet.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Etes",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                transportadora: transportadora,
                fecha: fecha,
                cxc: facdoc
            }
        }
    });
    grid_tabladet.trigger("reloadGrid");
}
function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarFechasCXC() {
    var transportadora = $("#transportadoras").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 30,
            transportadora: transportadora
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#fechacxc').html('');
                $("#tabla_cargar_cxcGenerada").jqGrid("clearGridData", true);
                if (Object.keys(json).length === 0) {
                    $('#fechacxc').html('');
                    $('#fechacxc').val('');
                    mensajesDelSistema("NO SE ENCONTRARON REGISTROS ", '200', '150', false);
                } else {
                    $("#fechacxc").datepicker({
                        dateFormat: 'yy-mm-dd',
                        changeMonth: true,
                        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
                        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
                        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
                    });
                    var myDate = new Date();
                    $("#fechacxc").datepicker("setDate", myDate);
                    $('#ui-datepicker-div').css('clip', 'auto');
//                    // alert(Object.keys(json).length);
//                    $('#fechacxc').append("<option value=''> Todas </option>");
//                    for (var datos in json) {
//                        $('#fechacxc').append('<option value=' + datos + '>' + json[datos] + '</option>');
//                    }
                }

            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function exportarExcel() {
    var fullData = jQuery("#tabla_cargar_cxcGenerada").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga CXC Generadas'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Administracion&accion=Etes",
        data: {
            listado: myJsonString,
            opcion: 31
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function ventanaCXC(facdoc, fecha, transportadora) {
    cargarCxcDetalleGeneradas(facdoc, fecha, transportadora);
    $("#dialogCXC").dialog({
        width: '845',
        height: '570',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'REANTICIPOS GENERADOS',
        buttons: {
            "Exportar Excel": function () {
                exportarExcelCxcDetalle();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
    //$("#dialogCXC").siblings('div.ui-dialog-titlebar').remove();
    // $("#dialogCXC").siblings('div.ui-dialog-titlebar').append('<label>REANTICIPOS GENERADOS</label>');
}

function cargarCxcDetalleGeneradas(facdoc, fecha, transportadora) {
    var grid_tabladet = jQuery("#tabla_cargar_cxc_detalle");
    if ($("#gview_tabla_cargar_cxc_detalle").length) {
        reloadGridMostrarDetalle(grid_tabladet, 32, facdoc, fecha, transportadora);
    } else {
        grid_tabladet.jqGrid({
// caption: "CXC GENERADAS",
            url: "./controller?estado=Administracion&accion=Etes",
            mtype: "POST",
            datatype: "json",
            height: '400',
            width: '820',
            colNames: ['Cxc Corrida', 'Planilla', 'Reanticipo', 'Valor', 'Fecha de Creacion', 'Fecha Corrida', 'Fecha Vencimiento'],
            colModel: [
                {name: 'cxc_corrida', index: 'cxc_corrida', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'planilla', index: 'planilla', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'tipomanifiesto', index: 'tipomanifiesto', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'facvlr', index: 'facvlr', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'fechcreacion', index: 'fechcreacion', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'fechacorrida', index: 'fechacorrida', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'fechvencimiento', index: 'fechvencimiento', width: 120, sortable: true, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                data: {
                    opcion: 32,
                    transportadora: transportadora,
                    fecha: fecha,
                    cxc: facdoc
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, gridComplete: function () {
                var Totalfac = grid_tabladet.jqGrid('getCol', 'facvlr', false, 'sum');
                grid_tabladet.jqGrid('footerData', 'set', {tipomanifiesto: 'TOTAL', facvlr: Totalfac});
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
//        $("#tabla_cargar_cxc_detalle").navButtonAdd('#pager1', {
//            caption: "Exportar Excel",
//            onClickButton: function () {
//                exportarExcelCxcDetalle();
//            }
//        });
    }
}

function exportarExcelCxcDetalle() {
    var fullData = jQuery("#tabla_cargar_cxc_detalle").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descargar Detalles'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaExcxp").dialog(opt);
    $("#divSalidaExcxp").dialog("open");
    $("#imgloadExcxp").show();
    $("#msjExcxp").show();
    $("#respExcxp").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Administracion&accion=Etes",
        data: {
            listado: myJsonString,
            opcion: 33
        },
        success: function (resp) {
            $("#imgloadExcxp").hide();
            $("#msjExcxp").hide();
            $("#respExcxp").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respExcxp').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}