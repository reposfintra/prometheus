/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 $(document).ready(function() {
      cargarJuzgados();  
 });
 
 
 function cargarJuzgados(){
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=59';
    if ($("#gview_tabla_juzgados").length) {
         refrescarGridJuzgados();
     }else {
        jQuery("#tabla_juzgados").jqGrid({
            caption: 'Listado de Juzgados',
            url: url,
            datatype: 'json',
            height: 350,
            width: 520,
            colNames: ['Id', 'Nombre', 'Estado', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key:true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '700px'},         
                {name: 'reg_status', index: 'reg_status', hidden:true, sortable: true, align: 'center', width: '90px'},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '200px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            pager:'#page_tabla_juzgados',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {
                    var ids = jQuery("#tabla_juzgados").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        var estado = jQuery("#tabla_juzgados").getRowData(cl).reg_status;
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarJuzgado('" + cl + "');\">";
                        if (estado==='A'){
                            $("#tabla_juzgados").jqGrid('setRowData',ids[i],false, {weightfont:'bold',background:'#F6CECE'});          
                            ac = "<img src='/fintra/images/botones/iconos/check-blue.png' align='absbottom'  name='activar' id='activar' width='15' height='15' title ='Activar'  onclick=\"mensajeConfirmAction('Esta seguro de activar el juzgado seleccionado?','250','150',activarJuzgado,'" + cl + "');\">";
                        }else{
                            ac = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el juzgado seleccionado?','250','150',anularJuzgado,'" + cl + "');\">";
                        }                      
                        jQuery("#tabla_juzgados").jqGrid('setRowData', ids[i], {actions: ed+'  '+ac});
                       
                    }
                },
            loadError: function(xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_juzgados",{search:false,refresh:false,edit:false,add:false,del:false});
        jQuery("#tabla_juzgados").jqGrid("navButtonAdd", "#page_tabla_juzgados", {
            caption: "Nuevo", 
            title: "Agregar nuevo juzgado",           
            onClickButton: function() {
               crearJuzgado()
            }
        });
     }
}


function refrescarGridJuzgados(){    
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=59';
    jQuery("#tabla_juzgados").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#tabla_juzgados').trigger("reloadGrid");
}

function crearJuzgado(){
    $('#nomjuzgado').attr({readonly: false});      
    $('#nomjuzgado').val('');  
    $('#div_juzgados').fadeIn('slow');
    AbrirDivCrearJuzgados();
}

function AbrirDivCrearJuzgados(){
      $("#div_juzgados").dialog({
        width: 700,
        height: 180,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR JUZGADO',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
                guardarJuzgado();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarJuzgado(){    
    var nomJuzgado = $('#nomjuzgado').val();  
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if(nomJuzgado!==''){
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 60,
                    nombre: nomJuzgado
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {
                            refrescarGridJuzgados();                           
                            mensajesDelSistema("Se cre� el juzgado exitosamente", '250', '150', true); 
                            $('#nomjuzgado').val('');                          
                        }
                        
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear el nuevo juzgado!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
    }else{
       mensajesDelSistema("Debe ingresar el nombre del juzgado", '250', '150');
    }
}

function editarJuzgado(cl){
    
    $('#div_juzgados').fadeIn("slow");
    var fila = jQuery("#tabla_juzgados").getRowData(cl);
    var nombre = fila['nombre']; 
    var estado = fila['reg_status'];
     if (estado==="A"){
        $('#nomjuzgado').attr({readonly: true});     
    }else{
        $('#nomjuzgado').attr({readonly: false});      
    }
    $('#idjuzgado').val(cl);
    $('#nomjuzgado').val(nombre);  
    AbrirDivEditarJuzgados();
    
}

function AbrirDivEditarJuzgados(){
      $("#div_juzgados").dialog({
        width: 700,
        height: 180,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'EDITAR JUZGADOS',
        closeOnEscape: false,
        buttons: {   
            "Actualizar": function(){
               actualizarJuzgado();  
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarJuzgado(){
    var nombre = $('#nomjuzgado').val();   
    var idJuzgado = $('#idjuzgado').val();
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if(nombre!==''){
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 61,
                nombre: nombre,               
                idJuzgado: idJuzgado
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridJuzgados(); 
                        mensajesDelSistema("Se actualiz� el juzgado correctamente", '250', '150', true);     
                        $('#div_juzgados').dialog('close');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar el juzgado!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }else{
         mensajesDelSistema("Debe ingresar un nombre para el juzgado!!", '250', '150');      
    }
    
}

function anularJuzgado(cl){
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 62,            
            idJuzgado: cl,
            estadoJuzgado: "A"
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridJuzgados();                  
                    mensajesDelSistema("Se anul� el juzgado", '250', '150', true);                                   
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular el juzgado!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function activarJuzgado(cl){
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 62,            
            idJuzgado: cl,
            estadoJuzgado: ""
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridJuzgados();                  
                    mensajesDelSistema("Se activ� el juzgado", '250', '150', true);                               
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo activar el juzgado!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function mensajeConfirmAnulacion(msj, width, height, okAction, id) {   
    $("#msj").html("<span style='background: url(/fintra/images/warning03.png); height:32px; width:32px; float: left; margin: 0 7px 20px 0'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);               
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajeConfirmAction(msj, width, height, okAction, id) {
  
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}



function mensajesDelSistema(msj, width, height, swHideDialog) {   
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}
