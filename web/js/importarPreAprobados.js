/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 $(document).ready(function() {
    $('#unidad_negocio').attr({disabled: false});
    cargarUnidadesNegocio();   
    $("#subirArchivo").click(function () {       
       importarArchivoPreAprobados();
    });

 });

function importarArchivoPreAprobados() {
    var und_negocio = $('#unidad_negocio').val();   
    //var nomArchivo = $("#examinar").val();
    var nomArchivo = $("input[type='file']").val().split('/').pop().split('\\').pop();
    var extension = $('#examinar').val().split('.').pop().toLowerCase();  
    if (nomArchivo === "") {
        mensajesDelSistema("No ha seleccionado ning&uacute;n archivo. Por favor, seleccione uno!!", '250', '150');
        return;
    }else if (extension.toUpperCase() !== 'XLS'){
        $("#examinar").val("");
        mensajesDelSistema("La extension del archivo no es valida. La extensi�n debe ser xls", '250', '150');
        return;
    }else if (und_negocio === null || und_negocio === ''){       
        mensajesDelSistema("Debe seleccionar la unidad de negocio", '250', '150');
        return;
    }else{
        var numRecords = (parseInt($('#unidad_negocio').val())===12) ? jQuery("#tabla_preaprobados_univ").getGridParam('records') : jQuery("#tabla_preaprobados").getGridParam('records');
        if (numRecords > 0) {
            $("#examinar").val("");
            mensajesDelSistema("Hay registros por procesar", '250', '150', false);
            return;
        } else {
            subirArchivoImportacion();
        }       
    }
}

function subirArchivoImportacion() {

    var fd = new FormData(document.getElementById('formulario'));
    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function(){
        $.ajax({
            async: false,
            url: "./controller?estado=Fintra&accion=Soporte&opcion=11&und_negocio="+$('#unidad_negocio').val(),
            type: 'POST',
            dataType: 'json',
            data: fd,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (json) {

                if (json.success === true){
                    $("#examinar").val(""); 
                    $('#unidad_negocio').attr({disabled: true});
                    (parseInt($('#unidad_negocio').val())===12) ? cargarPreAprobadosUniv(json.rows) : cargarPreAprobados(json.rows); 
                    $("#dialogLoading").dialog('close');
                }else{
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema(json.error, '250', '150', false);
                    return;
                }


            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    },500);
}

function cargarPreAprobadosUniv(jsonData) {
    $('#div_preaprobados').fadeOut();
    $('#div_preaprobados_univ').fadeIn();  
    var grid_tabla_ = jQuery("#tabla_preaprobados_univ");
    if ($("#gview_tabla_preaprobados_univ").length) {
        refrescarGridPreAprobadosUniversidad(jsonData);
    } else {
        grid_tabla_.jqGrid({
            caption: "Listado de PreAprobados",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            cellsubmit: "clientArray",
            data: jsonData,         
            datatype: "local",
            height: '350',
            width: '845',
            colNames: ['Nit', 'Nombre Cliente', 'Und. Negocio', 'Negocio', 'Vr. Ultimo Cr�dito', 'Vr. Aprobado'],
            colModel: [
                {name: 'nit', index: 'nit', width: 120, sortable: true, align: 'left'},
                {name: 'nombre', index: 'nombre', width: 250, sortable: true, align: 'left'},
                {name: 'und_negocio', index: 'und_negocio', width: 100, sortable: true, align: 'center',editrules: {required: true}},
                {name: 'negocio', index: 'negocio', width: 90, sortable: true, align: 'center', key: true},
                {name: 'valor_ultimo_credito', index: 'valor_ultimo_credito', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_aprobado', index: 'valor_aprobado', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}              
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            pager: '#page_tabla_preaprobados_univ',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function() {
                calcularTotalesPreAprobados('tabla_preaprobados_univ');
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }
        }).navGrid("#page_tabla_preaprobados_univ", {search:false,refresh:false, edit: false, add: false, del: false});
        jQuery("#tabla_preaprobados_univ").jqGrid("navButtonAdd", "#page_tabla_preaprobados_univ", {
            caption: "Actualizar",
            title: "Actualizar base de datos de PreAprobados Universidad",
            onClickButton: function() {
                var numRecords = grid_tabla_.getGridParam('records');
                if (numRecords > 0) {                  
                    actualizarBasePreAprobados();                   
                } else {
                    mensajesDelSistema("No hay informacion para Actualizar ", '250', '150', false);
                }
            }
        });
        $("#tabla_preaprobados_univ").navButtonAdd('#page_tabla_preaprobados_univ', {
            caption: "Limpiar",
            onClickButton: function() {
                var numRecords = grid_tabla_.getGridParam('records');
                if (numRecords > 0) {                
                   $('#unidad_negocio').attr({disabled: false});
                   jQuery("#tabla_preaprobados_univ").jqGrid("clearGridData", true);
                   $('#div_preaprobados_univ').fadeOut();
                } else {
                    mensajesDelSistema("No hay informacion que limpiar", '250', '150', false);
                }
            }
        });
    }
}

function refrescarGridPreAprobadosUniversidad(data){   
    jQuery("#tabla_preaprobados_univ").setGridParam({
          data: data,
         datatype: "local"
    });
    jQuery('#tabla_preaprobados_univ').trigger("reloadGrid");
}


function cargarPreAprobados(jsonData) {
    $('#div_preaprobados_univ').fadeOut();
    $('#div_preaprobados').fadeIn();
    var grid_tabla_ = jQuery("#tabla_preaprobados");
    if ($("#gview_tabla_preaprobados").length) {
        refrescarGridPreAprobados(jsonData);
    } else {
        grid_tabla_.jqGrid({
            caption: "Listado de PreAprobados",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            cellsubmit: "clientArray",
            data: jsonData,         
            datatype: "local",
            height: '350',
            width: '1150',
            colNames: ['Nit', 'Nombre Cliente', 'Und. Negocio', 'Negocio', 'Vr. Ultimo Cr�dito', 'Vr. Aprobado', 'Incremento (%)', 'Afiliado', 'F. Desembolso', 'Periodo Desembolso', 'F. Vencimiento', 'Direccion', 'Telefono', 'Departamento', 'Ciudad', 'Barrio', 'Valor_saldo'],
            colModel: [
                {name: 'nit', index: 'nit', width: 100, sortable: true, align: 'left'},
                {name: 'nombre', index: 'nombre', width: 250, sortable: true, align: 'left'},
                {name: 'und_negocio', index: 'und_negocio', width: 110, sortable: true, align: 'center'},
                {name: 'negocio', index: 'negocio', width: 90, sortable: true, align: 'center', key: true},
                {name: 'valor_ultimo_credito', index: 'valor_ultimo_credito', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_aprobado', index: 'valor_aprobado', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'incremento', index: 'incremento', width: 90, sortable: true, align: 'center'},
                {name: 'afiliado', index: 'afiliado', width: 110, sortable: true, align: 'left'},
                {name: 'fecha_desembolso', index: 'fecha_desembolso', width: 110, sortable: true, align: 'center'},
                {name: 'periodo_desembolso', index: 'periodo_desembolso', width: 110, sortable: true, align: 'center'},
                {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 110, sortable: true, align: 'center'},
                {name: 'direccion', index: 'direccion', width: 110, sortable: true, align: 'left'},
                {name: 'telefono', index: 'telefono', width: 110, sortable: true, align: 'center'},
                {name: 'departamento', index: 'departamento', width: 110, sortable: true, align: 'left'},
                {name: 'ciudad', index: 'ciudad', width: 110, sortable: true, align: 'left'},
                {name: 'barrio', index: 'barrio', width: 110, sortable: true, align: 'left'},
                {name: 'valor_saldo', index: 'valor_saldo', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            pager: '#page_tabla_preaprobados',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function() {
                calcularTotalesPreAprobados('tabla_preaprobados');
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }
        }).navGrid("#page_tabla_preaprobados", {search:false,refresh:false, edit: false, add: false, del: false});
        jQuery("#tabla_preaprobados").jqGrid("navButtonAdd", "#page_tabla_preaprobados", {
            caption: "Actualizar",
            title: "Actualizar base de datos de PreAprobados",
            onClickButton: function() {
                var numRecords = grid_tabla_.getGridParam('records');
                if (numRecords > 0) {                  
                    actualizarBasePreAprobados();                   
                } else {
                    mensajesDelSistema("No hay informacion para Actualizar ", '250', '150', false);
                }
            }
        });
        $("#tabla_preaprobados").navButtonAdd('#page_tabla_preaprobados', {
            caption: "Limpiar",
            onClickButton: function() {
                var numRecords = grid_tabla_.getGridParam('records');
                if (numRecords > 0) {    
                   $('#unidad_negocio').attr({disabled: false});
                   jQuery("#tabla_preaprobados").jqGrid("clearGridData", true);
                   $('#div_preaprobados').fadeOut();
                } else {
                    mensajesDelSistema("No hay informacion que limpiar", '250', '150', false);
                }
            }
        });
    }
}

function refrescarGridPreAprobados(data){   
    jQuery("#tabla_preaprobados").setGridParam({
        data: data,
        datatype: "local"
    });
    
    jQuery('#tabla_preaprobados').trigger("reloadGrid");
}

function  calcularTotalesPreAprobados(tableid) {

   
    var valor_ultimo_credito = jQuery('#'+tableid).jqGrid('getCol', 'valor_ultimo_credito', false, 'sum');
    var valor_aprobado = jQuery('#'+tableid).jqGrid('getCol', 'valor_aprobado', false, 'sum');
    var valor_saldo = jQuery('#'+tableid).jqGrid('getCol', 'valor_saldo', false, 'sum');
   

    jQuery('#'+tableid).jqGrid('footerData', 'set', {
        negocio: 'Total:',
        valor_ultimo_credito: valor_ultimo_credito,
        valor_aprobado: valor_aprobado,
        valor_saldo: valor_saldo
    });

}

function  actualizarBasePreAprobados() {
    var id_und_negocio = parseInt($('#unidad_negocio').val());
    var tableid = (id_und_negocio===12) ? 'tabla_preaprobados_univ' : 'tabla_preaprobados';
    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function(){
        if(validateGridData(tableid)){
            $.ajax({
                async: false,
                url: "./controller?estado=Fintra&accion=Soporte",
                type: 'POST',
                data: {
                    opcion: (id_und_negocio === 12) ? 12 : 44,
                    unidad_negocio: $('#unidad_negocio').val(),
                    listadoDocs: (id_und_negocio === 12) ? guardarPreAprobadosUnivToJSON : guardarPreAprobadosToJSON()
                },
                success: function(json) {
                    if (json.respuesta === 'OK') {
                        if(id_und_negocio === 12) {                           
                            jQuery("#tabla_preaprobados_univ").jqGrid("clearGridData", true);
                            $('#div_preaprobados_univ').fadeOut();
                        }else{
                            jQuery("#tabla_preaprobados").jqGrid("clearGridData", true);
                            $('#div_preaprobados').fadeOut();
                        }
                        $('#unidad_negocio').attr({disabled: false});
                        $('#unidad_negocio').val(12);
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Exito al actualizar  ", '230', '150', true);
                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema(json.message, '250', '150', false);
                        return;
                    }
                }, error: function(result) {
                    $("#dialogLoading").dialog('close');
                    alert('Ocurri� un error al realizar la operaci�n. Por favor, intente m�s tarde');
                }
            });       
        }else{
            $("#dialogLoading").dialog('close');
            mensajesDelSistema('Informacion inv�lida. Corrija y vuelva a cargar el archivo', '250', '150', false);     
        }
    },500);
}

function guardarPreAprobadosUnivToJSON(){
       
       var jsonObj = {"preaprobados": []};
                  
        var filasId = jQuery('#tabla_preaprobados_univ').jqGrid('getDataIDs');       
        if (filasId != '') {
            for (var i = 0; i < filasId.length; i++) {                 
                    var negocio = filasId[i];
                    var nit = $("#tabla_preaprobados_univ").getRowData(filasId[i]).nit;
                    var valor_ult_credito = $("#tabla_preaprobados_univ").getRowData(filasId[i]).valor_ultimo_credito;
                    var valor_aprobado = $("#tabla_preaprobados_univ").getRowData(filasId[i]).valor_aprobado;                    
                    var item = {};
                    item ["negocio"] = negocio;
                    item ["nit"] = nit;
                    item ["valor_ult_credito"] = parseFloat(valor_ult_credito);
                    item ["valor_aprobado"] = parseFloat(valor_aprobado);  
                    jsonObj.preaprobados.push(item);                              
            }  
        }
        return JSON.stringify(jsonObj); 
     
}

function guardarPreAprobadosToJSON(){
       
       var jsonObj = {"preaprobados": []};
                  
        var filasId = jQuery('#tabla_preaprobados').jqGrid('getDataIDs');       
        if (filasId != '') {
            for (var i = 0; i < filasId.length; i++) {                 
                    var negocio = filasId[i];
                    var nit = $("#tabla_preaprobados").getRowData(filasId[i]).nit;
                    var valor_ult_credito = $("#tabla_preaprobados").getRowData(filasId[i]).valor_ultimo_credito;
                    var valor_aprobado = $("#tabla_preaprobados").getRowData(filasId[i]).valor_aprobado;  
                    var nombre = $("#tabla_preaprobados").getRowData(filasId[i]).nombre;
                    var incremento = $("#tabla_preaprobados").getRowData(filasId[i]).incremento;
                    var und_negocio = $("#tabla_preaprobados").getRowData(filasId[i]).und_negocio;
                    var afiliado = $("#tabla_preaprobados").getRowData(filasId[i]).afiliado;
                    var fecha_desembolso = $("#tabla_preaprobados").getRowData(filasId[i]).fecha_desembolso;
                    var periodo_desembolso = $("#tabla_preaprobados").getRowData(filasId[i]).periodo_desembolso;
                    var fecha_vencimiento = $("#tabla_preaprobados").getRowData(filasId[i]).fecha_vencimiento;
                    var direccion = $("#tabla_preaprobados").getRowData(filasId[i]).direccion;
                    var telefono = $("#tabla_preaprobados").getRowData(filasId[i]).telefono;
                    var departamento = $("#tabla_preaprobados").getRowData(filasId[i]).departamento;
                    var ciudad = $("#tabla_preaprobados").getRowData(filasId[i]).ciudad;
                    var barrio = $("#tabla_preaprobados").getRowData(filasId[i]).barrio;
                    var valor_saldo = $("#tabla_preaprobados").getRowData(filasId[i]).valor_saldo;
                      
                    var item = {};
                    item ["negocio"] = negocio;
                    item ["nit"] = nit;
                    item ["valor_ult_credito"] = parseFloat(valor_ult_credito);
                    item ["valor_aprobado"] = parseFloat(valor_aprobado); 
                    item ["nombre"] = nombre;
                    item ["incremento"] = incremento;
                    item ["und_negocio"] = und_negocio;
                    item ["afiliado"] = afiliado;
                    item ["fecha_desembolso"] = fecha_desembolso;
                    item ["periodo_desembolso"] = periodo_desembolso;
                    item ["fecha_vencimiento"] = fecha_vencimiento;
                    item ["direccion"] = direccion;
                    item ["telefono"] = telefono;
                    item ["departamento"] = departamento;
                    item ["ciudad"] = ciudad;
                    item ["barrio"] = barrio;
                    item ["valor_saldo"] = parseFloat(valor_saldo);                          
             
                    jsonObj.preaprobados.push(item);                              
            }  
        }
        return JSON.stringify(jsonObj); 
     
}

function cargarUnidadesNegocio() {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        data: {
            opcion: 25
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#unidad_negocio').append("<option value=''>Seleccione</option>");                  

                    for (var key in json) {
                        $('#unidad_negocio').append('<option value=' + key + '>' + json[key] + '</option>');                       
                    }
                    $('#unidad_negocio').val(12);
//                  $('#unidad_negocio').attr({disabled: true});

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function validateGridData(tableid){
    var estado = true;
    var ids = jQuery("#" + tableid).jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var cl = ids[i];
        var nit = jQuery("#" + tableid).getRowData(cl).nit;
        var nombre = jQuery("#" + tableid).getRowData(cl).nombre;
        var und_negocio = jQuery("#" + tableid).getRowData(cl).und_negocio;
        var negocio = jQuery("#" + tableid).getRowData(cl).negocio;
        var valor_ult_credito = jQuery("#" + tableid).getRowData(cl).valor_ultimo_credito;
        var valor_aprobado = jQuery("#" + tableid).getRowData(cl).valor_aprobado;       
      
        if (nit ==='' || nombre ==='' || und_negocio ==='' || negocio ==='' || isNaN(parseFloat(valor_ult_credito)) || isNaN(parseFloat(valor_aprobado))){
            estado = false;
            if (nit === '')
                jQuery("#" + tableid).jqGrid('setCell', cl, "nit", "", {'background-color': 'rgba(245, 140, 112, 0.53)', 'background-image': 'none'});
            if (nombre === '')
                jQuery("#" + tableid).jqGrid('setCell', cl, "nombre", "", {'background-color': 'rgba(245, 140, 112, 0.53)', 'background-image': 'none'});
            if (und_negocio === '')
                jQuery("#" + tableid).jqGrid('setCell', cl, "und_negocio", "", {'background-color': 'rgba(245, 140, 112, 0.53)', 'background-image': 'none'});
            if (negocio === '')
                jQuery("#" + tableid).jqGrid('setCell', cl, "negocio", "", {'background-color': 'rgba(245, 140, 112, 0.53)', 'background-image': 'none'});
            if(isNaN(parseFloat(valor_ult_credito)))
                jQuery("#" + tableid).jqGrid('setCell', cl, "valor_ultimo_credito", "", {'background-color': 'rgba(245, 140, 112, 0.53)', 'background-image': 'none'});
            if(isNaN(parseFloat(valor_aprobado)))
                jQuery("#" + tableid).jqGrid('setCell', cl, "valor_aprobado", "", {'background-color': 'rgba(245, 140, 112, 0.53)', 'background-image': 'none'});
        }else{
           if(parseInt($('#unidad_negocio').val())!==12){
                var incremento = jQuery("#" + tableid).getRowData(cl).incremento;
                var valor_saldo = jQuery("#" + tableid).getRowData(cl).valor_saldo;
                var fecha_desembolso = jQuery("#" + tableid).getRowData(cl).fecha_desembolso;
                var fecha_vencimiento = jQuery("#" + tableid).getRowData(cl).fecha_vencimiento;
                if (isNaN(incremento) || isNaN(valor_saldo) || isDate(fecha_desembolso) || isDate(fecha_vencimiento)){
                     estado = false;
                    if (isNaN(parseFloat(incremento)))
                        jQuery("#" + tableid).jqGrid('setCell', cl, "incremento", "", {'background-color': 'rgba(245, 140, 112, 0.53)', 'background-image': 'none'});
                    if (isNaN(parseFloat(valor_saldo)))
                        jQuery("#" + tableid).jqGrid('setCell', cl, "valor_saldo", "", {'background-color': 'rgba(245, 140, 112, 0.53)', 'background-image': 'none'});
                    if (isDate(fecha_desembolso))
                        jQuery("#" + tableid).jqGrid('setCell', cl, "fecha_desembolso", "", {'background-color': 'rgba(245, 140, 112, 0.53)', 'background-image': 'none'});
                    if (isDate(fecha_vencimiento))
                        jQuery("#" + tableid).jqGrid('setCell', cl, "fecha_vencimiento", "", {'background-color': 'rgba(245, 140, 112, 0.53)', 'background-image': 'none'});
                }
           }
        }      
    }
    
    return estado;
}

function isDate(val) {
    var d = new Date(val);
    return isNaN(d.valueOf());
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: "Mensaje",
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("close");             
            }
        }
    });
}
