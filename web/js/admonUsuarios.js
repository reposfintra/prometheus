/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function verifica_clave() {
    var clv = $('#clave')
      , clv2 = $('#clave_ver')
      , mensaje = "";
    if (clv.val() !== clv2.val()) {
        mensaje = 'Contrase�as no coinciden';
    }
    $('#mensaje').html(mensaje);
}
function verifica_usuario() {
    var id = $('#idusuario').val();
    $.ajax({
        url: '/fintra/controller?estado=GestionUsuarios&accion=AdmonUser',
        datatype: 'json',
        type: 'post',
        data: {opcion: 4, usuario:id},
        async: false,
        success: function (json) {
            if (json.mensaje) {
                if (json.mensaje !== 'OK') {
                    $('#mensaje').html(json.mensaje);
                } else {
                    $('#mensaje').html("");
                }
            }
        },
        error: function (error) {
            alert(error);
        }
    });
}
function agregarCompania(distrito, usuario, refrescar) {
    if (distrito) {
        $('#empresa').val(distrito);
        if(!refrescar) $('#empresa').attr('disabled', true);
    } else {
        $('#empresa').removeAttr('disabled');
        distrito = $('#empresa').val();
    }
    $.ajax({
        url: '/fintra/controller?estado=GestionUsuarios&accion=AdmonUser',
        datatype: 'json',
        type: 'post',
        data: {opcion: 3, distrito:distrito, usuario:usuario},
        async: false,
        success: function (json) {
            if(json.mensaje) {
                alert(json.mensaje); return;
            } var sel = $('#perfiles');
            sel.html('');
            for (var i in json.perfiles) {
                sel.append('<option value="'+json.perfiles[i].cod+'">'+json.perfiles[i].nombre+'</option>');
            }
            $("#dialogCompania").dialog({
                width: 300,
                height: 340,
                show: "scale",
                hide: "scale",
                resizable: true,
                position: "center",
                modal: true,
                closeOnEscape: false,
                buttons: {//crear bot�n 
                    "Aceptar": function () {
                        agregarPerfiles(usuario);
                        $(this).dialog("close");
                    },
                    "Salir": function () {
                        $(this).dialog("close");
                    }
                }
            })
        },
        error: function (error) {
            alert(error);
        }
    });
}
function agregarPerfiles(usuario) {
    var idEmp = $('#empresa').val();
    var lista = $('#'+idEmp);
    var origen = $('#perfiles');
    
    if(!lista.html()) {
        //alert('crear td');
        $('#tbl_empresas').append(
                '<td>'
                +'<span onclick=agregarCompania("'+idEmp+'","'+usuario+'")>'+$('#empresa')[0].selectedOptions[0].label+'</span>'
                +'<ol name="empresas" id="'+idEmp+'" title="N"></ol>'
                +'</td>');
        lista = $('#'+idEmp);
    }
    $("#perfiles :selected").each(function(){
        lista.append(
                '<li id="'+$(this).attr('value')+'" title="N">'
                +'<input type="checkbox" checked'
                +' id="'+idEmp+"_"+$(this).attr('value')+'">'
                +$(this).text()+'</li>');
    });
}
function locacion(opcion, valor) {
    var query = '', select;
    if(opcion === 'dptos'){
        query = 'GET_DEPARTAMENTOS';
        select = $('#depto');
    } else if (opcion === 'ciuds') {
        query = 'GET_CIUDADES';
        select = $('#ciudad');
    }
    $.ajax({
        url: '/fintra/controller?estado=GestionUsuarios&accion=AdmonUser',
        datatype: 'json',
        type: 'post',
        data: {opcion: 0, query:query, filtro:valor},
        async: false,
        success: function (json) {
            if(json.mensaje) {
                alert(json.mensaje); return;
            }
            select.html('');
            for(var i in json.rows) {
                select.append('<option value="'+json.rows[i].cod+'">'+json.rows[i].nombre+'</option>');
            }
        },
        error: function (error) {
            alert(error);
        }
    });
}
function crearUsuario(opc) {
    /******************** perfiles ******************************/
    if ($('#mensaje').html().length > 0) { return; }
    if (!$('#estado').val() || !$('#nombre').val() || !$('#direccion').val() || !$('#email').val()
        || !$('#telefono').val() || !$('#pais').val() || !$('#depto').val() || !$('#ciudad').val()
        || !$('#nit').val() || !$('#tipo').val() || !$('#agencia').val() || !$('#clave').val()
        || !$('#idusuario').val()) {
        alert('Informacion basica esta incompleta'); return;
    }
    var proyectos = [], perf, perfiles = [];
    var proy = document.getElementsByName('empresas');

    for (var i = 0; i < proy.length; i++) {
        perfiles = [];
        perf = proy[i].children;
        for (var j = 0; j < perf.length; j++) {
            perfiles.push( {
                estado: (perf[j].title) 
                        ? perf[j].title 
                        : ((document.getElementById(proy[i].id + '_' + perf[j].id).checked) ? '' : 'A'),
                codigo: perf[j].id
            });
        }
        proyectos.push( {
            estado: (proy[i].title) ? proy[i].title : '',
            distrito: proy[i].id,
            perfiles: perfiles
        });
    }
    if(!proyectos.length) { alert('No se le ha asignado un perfil'); return; }
    /******************** peticion ******************************/
    $.ajax({
        url: '/fintra/controller?estado=GestionUsuarios&accion=AdmonUser',
        datatype: 'json',
        type: 'post',
        data: {
            opcion: opc,
            informacion: JSON.stringify({
                basico: {
                    estado: $('#estado').val(),
                    nombre: $('#nombre').val(),
                    direccion: $('#direccion').val(),
                    email: $('#email').val(),
                    telefono: $('#telefono').val(),
                    codpais: $('#pais').val(),
                    depto: $('#depto').val(),
                    ciudad: $('#ciudad').val(),
                    nit: $('#nit').val(),
                    tipo: $('#tipo').val(),
                    id_agencia: $('#agencia').val(),
                    clave: $('#clave').val(),
                    nits_propietario: $('#npropietario').val(),
                    idusuario: $('#idusuario').val(),
                    base: 'COL',
                    cambio_clave: ($('#cambio_clave').is(':checked')),
                    app:($('#app').is(':checked')),
                    
                },
                proyectos: proyectos
            })
        },
        async: false,
        success: function (json) {
            try {
                if (json.mensaje) {
                    alert(json.mensaje);
                    if(json.mensaje === '�Creado con exito!' || json.mensaje === '�Usuario actualizado!') {
                        window.location = location.toString();
                    }
                    return;
                }
            } finally {
                
            }
        },
        error: function (error) {
            alert(error);
        }
    });
}

function agregarPerfilesFenalco(distrito, usuario, refrescar) {
    if (distrito) {
        $('#empresa').val(distrito);
        if(!refrescar) $('#empresa').attr('disabled', true);
    } else {
        $('#empresa').removeAttr('disabled');
        distrito = $('#empresa').val();
    }
    $.ajax({
        url: '/fintra/controller?estado=GestionUsuarios&accion=AdmonUser',
        datatype: 'json',
        type: 'post',
        data: {opcion: 5, distrito:distrito, usuario:usuario},
        async: false,
        success: function (json) {
            if(json.mensaje) {
                alert(json.mensaje); return;
            } var sel = $('#perfiles');
            sel.html('');
            for (var i in json.perfiles) {
                sel.append('<option value="'+json.perfiles[i].cod+'">'+json.perfiles[i].nombre+'</option>');
            }
            $("#dialogCompania").dialog({
                width: 300,
                height: 340,
                show: "scale",
                hide: "scale",
                resizable: true,
                position: "center",
                modal: true,
                closeOnEscape: false,
                buttons: {//crear bot�n 
                    "Aceptar": function () {
                        agregarPerfiles(usuario);
                        $(this).dialog("close");
                    },
                    "Salir": function () {
                        $(this).dialog("close");
                    }
                }
            })
        },
        error: function (error) {
            alert(error);
        }
    });
}

function verifica_usuarioApp() {
    var id = $('#idusuario').val();
    
     if ($('#app').is(':checked')) {
                    $.ajax({
                        url: '/fintra/controller?estado=GestionUsuarios&accion=AdmonUser',
                        datatype: 'json',
                        type: 'post',
                        data: {opcion: 6, usuario:id},
                        async: false,
                        success: function (json) {                            
//                            if (json.mensaje) {
                                if (json.mensaje !== 'Ok') {
                                    $('#mensaje').html(json.mensaje);
//                                    $('#idapp').attr('disabled',false);
//                                    $('#idapp').attr('placeholder','Ingresa Usuario diferente');
                                         document.getElementById("app").checked = false;
                                } else {
                                    $('#mensaje').html("");
                                     var idUsr = $('#idusuario').val();
                                        $('#idapp').val(idUsr); 
                                }
//                            }
                        },
                        error: function (error) {
                            alert(error);
                            console.log(error);
                        }
                    });
    
                     
                     
                } else {
                    $('#idapp').val('');
       }
}

