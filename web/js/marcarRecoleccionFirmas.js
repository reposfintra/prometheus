/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {


    $('#buscar').click(function () {
        mostrarPresolicitud();
    });

    $('#limpiar').click(function () {
        $("#identificacion").val('');
        $("#numero_solicitud").val('');

    });



});

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function mostrarPresolicitud() {
    console.log('Entra aqui');
    var numero_solicitud = $('#numero_solicitud').val();


    if (numero_solicitud !== '' ) {
        var grid_tabla = $("#tabla_marcarRecoleccion");
        if ($("#gview_tabla_marcarRecoleccion").length) {
            reloadGridMostrar(grid_tabla, 112, numero_solicitud);
        } else {

            grid_tabla.jqGrid({
                caption: "Recoleccion de firmas",
                url: "./controller?estado=Fintra&accion=Soporte",
                mtype: "POST",
                datatype: "json",
                height: '100',
                width: '880',
                colNames: ['Solicitud', 'Fecha', 'Monto', 'Identificacion', 'Nombre', 'Apellido', 'Acciones'],
                colModel: [
                    {name: 'numero_solicitud', index: 'numero_solicitud', width: 70, sortable: true, align: 'left', hidden: false, key: true},
                    {name: 'fecha_credito', index: 'fecha_credito', width: 70, sortable: true, align: 'left', hidden: false, search: true},
                    {name: 'monto_credito', index: 'monto_credito', width: 70, sortable: true, align: 'left', hidden: false, search: true},
                    {name: 'identificacion', index: 'identificacion', width: 70, sortable: true, align: 'left', hidden: false, search: true},
                    {name: 'primer_nombre', index: 'primer_nombre', width: 70, sortable: true, align: 'left', hidden: false, search: true},
                    {name: 'primer_apellido', index: 'primer_apellido', width: 70, sortable: true, align: 'left', hidden: false, search: true},
                    {name: 'acciones', index: 'acciones', width: 80, sortable: true, align: 'left', hidden: false, search: false}
                ],
                rownumbers: true,
                rownumWidth: 25,
                rowNum: 1000,
                rowTotal: 1000,
                loadonce: true,
                gridview: true,
                viewrecords: false,
                hidegrid: false,
                shrinkToFit: false,
                footerrow: false,
                pager: '#pager',
                multiselect: false,
                multiboxonly: false,
                pgtext: null,
                pgbuttons: false,
                jsonReader: {
                    root: "json",
                    repeatitems: false,
                    id: "0"
                }, ajaxGridOptions: {
                    dataType: "json",
                    type: "get",
                    data: {
                        opcion: 112
                        , numero_solicitud: numero_solicitud

                    }
                },
                loadError: function (xhr, status, error) {
                    mensajesDelSistema(error, 250, 150);

                }, loadComplete: function (id, rowid) {

                },
                gridComplete: function (index) {

                    var registros = $("#tabla_marcarRecoleccion").getGridParam('records');
                    var cant = jQuery("#tabla_marcarRecoleccion").jqGrid('getDataIDs');
                    if (registros == 0) {
                        mensajesDelSistema("No se encontraron negocios por programar", '230', '150', false);
                    }
                    else
                    {
                        for (var i = 0; i < cant.length; i++) {
                            var cl = cant[i];
                            //var reactivar = $("#tabla_marcarRecoleccion").getRowData(cant[i]).cambio;
                            be = '<input style="height:20px;width:68px;margin-left: 8px;margin-right: 8px;" type="button" title="Programar" value="Programar" onclick="programarRecoleccionFirmas(' + cl + ')" />';
                            jQuery("#tabla_marcarRecoleccion").jqGrid('setRowData', cant[i], {acciones: be});
                        }
                    }
                }


            }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
            });
//        $("#tabla_marcarRecoleccion").navButtonAdd('#pager', {
//            caption: "Todos",
//            onClickButton: function () {
//                operacion = 'Reactivar';
//                ventanaReactivar(operacion);
//            }
//        });
        }
    } else {
        mensajesDelSistema("Diligencie uno de los campos del filtro", '230', '150', false);
    }
}

function reloadGridMostrar(grid_tabla, op, numero_solicitud) {

    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            async: false,
            type: "POST",
            data: {
                opcion: 112
                , numero_solicitud: numero_solicitud
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function programarRecoleccionFirmas() {
    
    var numero_solicitud = document.getElementById('numero_solicitud').value;
    

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 113
            , numero_solicitud: numero_solicitud
            
            
        },
        success: function (data, textStatus, jqXHR) {
            if (data.respuesta === 'Guardado') {
                mensajesDelSistema2("Exito al programar", '230', '150', true);
            }
        }, error: function (result) {
            alert('No se puede programar, intente nuevamente');
        }
    });
    /*} else {
     mensajesDelSistema("Falta digitar informacion", '230', '150', false);
     }*/

}

function mensajesDelSistema2(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $("#programar").dialog('close');
                mostrarPresolicitud();
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}