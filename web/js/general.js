
var isIE = document.all?true:false;
var isNS = document.layers?true:false;
function soloDigitos(e,decReq) {
    var key = (isIE) ? window.event.keyCode : e.which;
    var obj = (isIE) ? event.srcElement : e.target;
    var isNum = (key > 47 && key < 58) ? true:false;
    var dotOK =  (decReq=='decOK' && key ==46 && obj.value.indexOf('.')==-1) ? true:false;
    window.event.keyCode = (!isNum && !dotOK && isIE) ? 0:key;
    e.which = (!isNum && !dotOK && isNS) ? 0:key;   
    return (isNum || dotOK );
}

function soloDigitos_signo(e,decReq) {
    var key = (isIE) ? window.event.keyCode : e.which;
    var obj = (isIE) ? event.srcElement : e.target;
    var isNum = (key > 47 && key < 58) ? true:false;
	var isSigno = (key == 45 ) ? true:false;
    var dotOK =  (decReq=='decOK' && key ==46 && obj.value.indexOf('.')==-1) ? true:false;
	window.event.keyCode = (!isNum && !dotOK && isIE && !isSigno) ? 0:key;
    e.which = (!isNum && !dotOK && isNS && !isSigno ) ? 0:key;   
    return (isSigno|| isNum || dotOK );
}

function TCamposLlenos(form){
 for (i = 0; i < form.elements.length; i++){
    if (form.elements[i].value == ""){
      alert("No se puede procesar la informacion. Verifique que todos lo campos esten llenos.");
      form.elements[i].focus();
      return (false);
    }
 }
 form.submit();
}

function soloAlfa(e) {
	var key = (isIE) ? window.event.keyCode : e.which;
	var obj = (isIE) ? event.srcElement : e.target;
	var isNum = ((key > 0 && key < 32) || (key > 32 && key < 48) || (key > 57 && key < 65) || (key > 90 &&  key < 97) || (key > 122)) ? true:false;
	window.event.keyCode = (isNum && isIE) ? 0:key;
	e.which = (!isNum && isNS) ? 0:key;
	return (isNum);
}
function openWindow(url){
    window.open(url,'NONE','status=yes,resizable=yes,width=700,height=590')
}
function getDate ( date, format ) {
  var str  = format;
  str = str.replace('yyyy', completNumber(date.getFullYear(),4) );
  str = str.replace('mm'  , completNumber(date.getMonth()+1 ,2) );  
  str = str.replace('dd'  , completNumber(date.getDate()    ,2) );
  str = str.replace('hh'  , completNumber(date.getHours()   ,2) );
  str = str.replace('mi'  , completNumber(date.getMinutes() ,2) );
  str = str.replace('ss'  , completNumber(date.getSeconds() ,2) );
  return str;  
}
  
function completNumber (num, long) {
  var str =  new String ( num );
  for ( i = str.length ; i < long ; str='0'+str, i++ );
  return str;
}
function soloAlfaNumEnterPuntoyComa(e) {
	var key = (isIE) ? window.event.keyCode : e.which;
	var obj = (isIE) ? event.srcElement : e.target;
	var isNum = ( (key > 0 && key < 13) || (key > 13 && key < 32) || (key > 32 && key < 44) || (key > 46 && key < 48) || (key > 59 && key < 65) || (key > 90 &&  key < 97) || (key > 122)) ? true:false;
	window.event.keyCode = (isNum && isIE) ? 0:key;
	e.which = (!isNum && isNS) ? 0:key;
	return (isNum);

}

function CamposLlenos(form){
 for (i = 0; i < form.elements.length; i++){
    if (form.elements[i].value == ""){
      alert("No se puede procesar la informacion. Verifique que todos lo campos esten llenos.");
      form.elements[i].focus();
      return false;
    }
 }
 return true;
}


/*Funcion que deshabilita el F5 y el boton derecho del mouse*/

var oLastBtn=0;
    bIsMenu = false;
    //No Right CLICK************************
    // ****************************
    if (window.Event) 
    document.captureEvents(Event.MOUSEUP); 
    function nocontextmenu()
    { 
    event.cancelBubble =true; 
    event.returnValue = false;
    alert("El clic derecho no puede ser usado en esta p�gina");
    return false; 
    } 
    function norightclick(e) 
    { 
    if (window.Event) 
    { 
		if (e.which !=1) 
		{
			
			return false; 
		}
    } 
    else 
		if (event.button !=1) 
		{ 
			event.cancelBubble = true; 
			event.returnValue = false; 
			return false; 
		} 
    } 
    document.oncontextmenu = nocontextmenu; 
    document.onmousedown = norightclick; 
    //**************************************
    // ****************************
    // Block backspace onKeyDown************
    // ***************************
     function onKeyDown() {
        if ( ((event.altKey) &&  (event.srcElement.type != "text" &&  event.srcElement.type != "textarea" &&  event.srcElement.type != "password")) || ((event.keyCode == 8) &&  (event.srcElement.type != "text" &&  event.srcElement.type != "textarea" &&  event.srcElement.type != "password")) || ((event.ctrlKey) && ((event.keyCode == 78) || (event.keyCode == 82)) ) || (event.keyCode == 116) || (event.keyCode == 122) )
        {
			alert("Esta tecla no puede ser usada en esta p�gina");
            event.keyCode = 0;
            event.returnValue = false;
        }
     }
     
 /*FIN DE LA Funcion que deshabilita el F5 y el boton derecho del mouse*/



 /*Funcion para validar la longitud de un campo con decimales*/
function ValidarCampo(campo,long,decimales){
     if(campo.value.indexOf('.')==-1){
	    if(campo.value.length > parseInt((long - decimales))){
		   alert('La longitud del campo debe ser menor o igual a '+ (long - decimales))
		   campo.focus();
		}
	 }else{
	     var str  = campo.value.split('.');
		 var num  = str[0];
		 var dec = str[1];
		 
		 if(num.length > parseInt((long - decimales))){
		    alert('La longitud del campo debe ser menor o igual a '+ (long - decimales))
			campo.focus();
		 }else if(dec.length > parseInt(decimales)){
		    alert('La longitud de los decimales debe ser menor o igual a  '+ decimales)
			campo.focus();
		 }
		 
	 }
  }
