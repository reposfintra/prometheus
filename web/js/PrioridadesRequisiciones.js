/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function init() {
    var FechaActual = new Date();   
    var anioActual = FechaActual.getFullYear();
    var mesActual = FechaActual.getMonth()+1;
    cargarAnios(anioActual);  
    $('#mes').val(mesActual);
    getFiltro("procesos");    
    $("#div_detalle_requisicion").draggable({ handle: "#drag_detalle_requisicion"});
    var tabla = jQuery("#tabla_requisiciones");
    tabla.tableDnD({onDrop: function() {
            //var tabla = $("#tabla_requisiciones");
            //var currRow = $('#tabla_requisiciones tbody #'+tabla.getGridParam('selrow')).children()[0].innerHTML;
            var i = 1; var t;
            var body = $('#tabla_requisiciones tbody').children();
            for(i = 1; i < body.length; i++) {
                t = body[i].id;
                $('#tabla_requisiciones tbody #'+t).children()[0].innerHTML = i;
            }
    }});
    tabla.jqGrid({
        datatype: 'local',
        width: 1850,
        height: 850,
        //userDataOnFooter: true,
        rowNum: 1000,
        pager: $('#page_requisiciones'),
        pgtext: null,
        pgbuttons: false,
        viewrecords: false,
        gridview: true,
        hidegrid: true,
        loadonce: true,
        rownumbers: true,
        cellsubmit: 'clientArray',
        editurl: 'clientArray',
        colNames: [ 'Id', 'Orden priorizacion', 'Tipo requisicion', 'Proceso interno'
                  , 'Radicado', 'Fecha radicacion', 'Usuario generador', 'Asunto'
                  , 'Descripcion', 'Prioridad', 'Solucionador responsable'
                  , 'Fecha inicio estimado', 'Fecha fin estimado', 'Horas trabajo'
                  , 'Fecha inicio actividades', 'Fecha cierre', 'Estado requisicion' ],
        colModel: [
            {name: 'id', index: 'id', hidden: true, width: '5px', key: true},
            {name: 'orden_prioridad', index: 'orden_prioridad', width: '55px', align:'right', classes: 'prioridad'},
            {name: 'tipo_requisicion', index: 'tipo_requisicion', width: '85px'},
            {name: 'proceso_interno', index: 'proceso_interno', width: '85px'},
            {name: 'radicado', index: 'radicado', width: '85px'},
            {name: 'fch_radicacion', index: 'fch_radicacion', width: '85px'},
            {name: 'usuario_generador', index: 'usuario_generador', width: '85px'},
            {name: 'asunto', index: 'asunto', width: '125px'},
            {name: 'descripcion', index: 'descripcion', width: '285px'},
            {name: 'prioridad', index: 'prioridad', width: '85px'},
            {name: 'solucionador_responsable', index: 'solucionador_responsable', width: '85px'},
            {name: 'fecha_inicio_estimado', index: 'fecha_inicio_estimado', width: '85px'},
            {name: 'fecha_fin_estimado', index: 'fecha_fin_estimado', width: '85px'},
            {name: 'horas_trabajo', index: 'horas_trabajo', width: '55px'},
            {name: 'fecha_inicio_actividades', index: 'fecha_inicio_actividades', width: '85px'},
            {name: 'fch_cierre', index: 'fch_cierre', width: '85px'},
            {name: 'estado_requisicion', index: 'estado_requisicion', width: '85px'}
        ],
        gridComplete: function() {
            $("#tabla_requisiciones").addClass("nodrag nodrop");
            $("#tabla_requisiciones").tableDnDUpdate();
        },
        ondblClickRow: function(rowid, iRow, iCol, e) {
           Cargar_detalle_requisicion('VISUALIZAR',rowid,e);
        }
    });
    tabla.navGrid("#page_requisiciones", {add: false, edit: false, del: false, search: false, refresh: true});
}
function getFiltro(nombre) {
    var elemento= $("#"+nombre);
    $.ajax({
        url: '/fintra/controller?estado=Prioridades&accion=Requisiciones',
        datatype: 'json',
        type: 'get',
        data: {opcion: 2, consulta: nombre},
        async: false,
        success: function (json) {
            console.log(json);
            try {
                if (json.error) {
                    alert(json.error);
                } else {
                    elemento.empty();
                    elemento.append("<option value=''>Seleccione...</option>");
                    for (var llave in json) {
                        elemento.append('<option value="'+llave+'">'+json[llave]+'</option>')
                    }
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                $("#lui_tabla_requisiciones,#load_tabla_requisiciones").hide();
            }
        },
        error: function () {
            $("#lui_tabla_requisiciones,#load_tabla_requisiciones").hide();
        }
    });
}
function buscarRequisiciones() {
    if ($('#procesos').val()==''){
        alert('Debe seleccionar un proceso');     
        return;
    }else{
        var tabla = jQuery("#tabla_requisiciones");
        $("#lui_tabla_requisiciones,#load_tabla_requisiciones").show();
        var a = $('#anho').val()
                , m = $('#mes').val()
                , p = $('#procesos').val();
        tabla.setGridParam({
            url: '/fintra/controller?estado=Prioridades&accion=Requisiciones&opcion=0'
                    + '&informacion=' + JSON.stringify({anho: a, mes: m, proceso: p}),
            datatype: 'json',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            loadComplete: function() {
                $("#lui_tabla_requisiciones,#load_tabla_requisiciones").hide();
            },
            loadError: function(xhr, status, error) {
                alert(error);
                $("#lui_tabla_requisiciones,#load_tabla_requisiciones").hide();
            }
        });
        tabla.trigger("reloadGrid");
    }
}

function guardarRequisiciones() {
    $("#lui_tabla_requisiciones,#load_tabla_requisiciones").show();
    var filas = jQuery("#tabla_requisiciones").jqGrid('getRowData');
    $.ajax({
        url: '/fintra/controller?estado=Prioridades&accion=Requisiciones',
        datatype: 'json',
        type: 'post',
        data: {opcion: 1, informacion: JSON.stringify({rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    alert(json.error);
                } else {
                    alert(json.mensaje);
                    buscarRequisiciones();
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                $("#lui_tabla_requisiciones,#load_tabla_requisiciones").hide();
            }
        },
        error: function () {
            $("#lui_tabla_requisiciones,#load_tabla_requisiciones").hide();
        }
    });
}

    function PosicionarDivLitleLeft(id_objeto, e, resta) {

        obj = document.getElementById(id_objeto);

        var posx = 0;
        var posy = 0;

        if (!e)
            var e = window.event;

        if (e.pageX || e.pageY) {
            //alert('page');
            posx = e.pageX;
            posy = e.pageY;
        } else if (e.clientX || e.clientY) {
            //alert('client'); 
            posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        } else
            alert('ninguna de las anteriores');

    
        obj.style.left = posx - resta;
        obj.style.top = posy;
  
    }     

    function Cargar_detalle_requisicion(accion,item,e){

             PosicionarDivLitleLeft('div_detalle_requisicion',e,600);
             $('#div_espera').fadeIn('slow');

             $.ajax({
                 type: "POST",
                 url : "/fintra/jsp/requisiciones/requisicion_detalle.jsp",
                 async:true,
                 dataType: "html",

                 data:{
                     estado:"",
                     mes:$('#mes').val(),
                     ano:$('#anho').val(),
                     proceso:$('#proceso_filtro').val(),
                     selec_prioridades:"",
                     tipo_tarearq:"",
                     ACCIONE:accion,
                     ITEM:item,
                     StatusTask:"",
                     UserTask:"",
                     ItemTask:""
                 },

                 success:function (data){
                     if (data!=""){
                         $('#div_detalle_requisicion').html(data);
                         $('#div_detalle_requisicion').fadeIn('slow');
                         $('#div_espera').fadeOut('slow');
                     }
                 }
             });

             $('#div_espera').fadeOut('slow');

    }

    function cargarAnios(anioActual) {

        var cad = "";
        for (var j = 2015; j <= 2025; j++) {
            var anio = j;
            if (anio == anioActual) {
                cad = "selected";
            } else {
                cad = "";
            }
            $('#anho').append('<option value=' + anio + ' ' + cad + '>' + anio + '</option>');
        }

    }
    
    function consultarNomarchivo(idReq,nomarchivo,containerId){          
            $.ajax({
                type: "POST",
                url : "/fintra/controller?estado=Requisicion&accion=Eventos&evento=CONSULTAR_ARCHIVO",
                async:true,
                dataType: "html",
                data:{                   
                    IdReq: idReq,
                    nomarchivo:nomarchivo
                },
                success:function (archivo){
                    //alert(data);
                    if (archivo!=""){
                       // alert(archivo); 
                       
                            var dataFile = {   
                                estado:"",
                                mes:$('#mes').val(),
                                ano:$('#anho').val(),
                                proceso:$('#proceso_filtro').val(),
                                selec_prioridades:"",
                                tipo_tarearq:"",
                                ACCIONE:'VISUALIZAR',
                                ITEM:idReq,
                                StatusTask:"",
                                UserTask:"",
                                ItemTask:"",
                                archivolisto: nomarchivo
                            };                          
                       cargarpagina('/fintra/jsp/requisiciones/requisicion_detalle.jsp',dataFile,containerId);
                }else{
                        alert(".::ERROR AL RECUPERAR ARCHIVO.");
                    }    
                }
            });
        } 
        
        function cargarpagina(url, data, divId){
                   $.ajax({
                       type: "POST",
                       url : url,
                       async:true,
                       dataType: "html",
                       data: data,
                       success:function (data){
                           if (data!=""){
                               $("#"+divId).html(data);
                               $('#'+divId).fadeIn('slow');                       
                           }
                       }
                   });
        }
       
    