/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    obtenerMenu("#menu-pagina");

    $("#menu-pagina").delegate(".menu-item", "click", function () {
        cargarPagina($(this).attr("ruta-pagina"));
    });
});

$("#salir").click(function () {
    alert("dssd");

});


function obtenerMenu(elemento) {
//    var text = '[{"id": 1,"descripcion": "Cargar Archivo","ruta": "jsp/cxcobrar/recaudos/cargarArchivoAsobancaria.jsp", "orden": 1},\n\
//                 {"id": 2,"descripcion": "Ver Recaudos","ruta": "jsp/cxcobrar/recaudos/verRecaudoAsobancariaInfo.jsp", "orden": 2}]';
//    var json = JSON.parse(text);
//    cargarPagina("presentacion.jsp");
//    var menu = new Menu("Opciones", json);
//    $(elemento).html(menu.generarMenu());
    
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        data: {
            opcion: 101
        },
        success: function(json) {

                var menu = new Menu("Opciones", json);
                $(elemento).html(menu.generarMenu());

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
                
}


function cargarPagina(pagina) {
    $.ajax({
        type: 'POST',
        url: "" + pagina,
        dataType: 'html',
        success: function(html) {
            $("#container").html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
function cargarPaginaParametros(pagina, parametros) {
    $.ajax({
        type: 'POST',
        url: "" + pagina,
        data: parametros,
        dataType: 'html',
        success: function(html) {
            $("#container").html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}