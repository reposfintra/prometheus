/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    fechas();

    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $('.solo-numeric').live('blur', function (event) {
        this.value = numberConComas(this.value);
    });
    
    $('#buscar').click(function () {
        cargarEmpleados();
    });
    
    $('#limpiar').click(function () {
        $("#identificacion2").val('');

    });


});

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function numberSinPuntos(x) {
    return x.toString().replace(/\./g, "");
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}


function format(input) {
    var num = input.value.replace(/\./g, '');
    if (!isNaN(num)) {
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/, '');
        input.value = num;
    }
    else {
        input.value = input.value.replace(/[^\d\.]*/g, '');
    }
}

function validarEmail(email) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    //expr = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (!expr.test(email)) {
        //alert("Error: La direcci�n de correo es incorrecta.");
    }
}

function activarCampo() {
    if (tipo_contrato.value !== "2" && tipo_contrato.value !== "4") {
        duracion.disabled = true;
        $('#duracion').val('0');
    } else {
        duracion.disabled = false;
        $('#duracion').val('');
    }
}

function activarCampo1() {
    if (sexo.value !== "M") {
        libreta_militar.disabled = true;
        $('#libreta_militar').val('N/A');
    } else {
        libreta_militar.disabled = false;
        $('#libreta_militar').val('');
    }
}

function fechas() {

    $(".fechas").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });




    var myDate = new Date();
    $("#fecha_expedicion").datepicker("setDate", myDate);
    $("#fecha_nacimiento").datepicker("setDate", myDate);
    $("#fecha_ingreso").datepicker("setDate", myDate);
    $("#fecha_retiro").datepicker("setDate", '0099-01-01');
    $('#ui-datepicker-div').css('clip', 'auto');
    
    $("#fecha_retiro").val('');
}


function onKeyDecimal(e, num) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if (document.getElementById(num.id).value.indexOf('.') !== -1 && keynum === 46)
        return false;
    if ((keynum === 0 || keynum === 8 || keynum === 48 || keynum === 46)) {
        return true;
    }

    if (keynum >= 58) {
        return false;

    }

    return /\d/.test(String.fromCharCode(keynum));
}



function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}


function cargarEmpleados() {
    //
    //alert('Entra aqui');
    var operacion;
    var grid_tabla_ = jQuery("#tabla_Empleados");
    if ($("#gview_tabla_Empleados").length) {
        reloadGridMostrar(grid_tabla_, 106);
    } else {
        //alert('Entra aqui');    
        grid_tabla_.jqGrid({
            caption: "Empleados",
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            mtype: "POST",
            datatype: "json",
            height: '700',
            width: '1500',
            colNames: ['Id', 'Estado','Fecha Actualizacion','Tipo Doc', 'Id Tipo Doc', 'No. Doc','Fecha Expedicion', 'Dpto Expedicion','Id Dpto Expedicion','Ciudad Expedici�n', 'Id Ciudad Expedici�n','Nombre Completo', 
                'Profesi�n', 'Id Profesion', 'Cargo', 'Id Cargo','Macroproceso','Id Macroproceso','Proceso','Id Proceso','Linea Negocio','Id Linea Negocio','Producto','Id Producto','Fecha Ingreso',
                'Salario', 'Nivel Jerarquico', 'Id Nivel Jerarquico','Tipo Contrato', 'Id Tipo Contrato','Duracion Contrato', 'Libreta Militar',  'Banco', 'Id Banco','Tipo Cuenta',
                'No. Cuenta', 'Riesgo','Id Riesgo', 'EPS','Id EPS', 'ARL','Id ARL', 'AFP','Id AFP', 'Cesant�as', 'Id Cesantias','CCF','Id CCF','Dpto Domicilio','Id Dpto Domicilio', 
                'Ciudad Domicilio','Id Ciudad Domicilio', 'Direcci�n', 'Telefono', 'Celular', 'Email', 'Fecha Nacimiento', 'Dpto Nacimiento', 'Id Dpto Nacimiento','Ciudad Nacimiento', 
                'Id Ciudad Nacimiento','Sexo', 'Estado Civil', 'Id Estado Civil','Nivel Estudio','Id Nivel Estudio',  'Personas a Cargo', 'Num hijos','Grupo Familiar', 'Fecha Retiro', 
                'Observaciones', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'fecha_actualizacion', index: 'fecha_actualizacion', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'tipo_doc', index: 'tipo_doc', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'id_tipo_doc', index: 'id_tipo_doc', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'identificacion', index: 'identificacion', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'fecha_expedicion', index: 'fecha_expedicion', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'dpto_expedicion', index: 'dpto_expedicion', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'id_dpto_expedicion', index: 'id_dpto_expedicion', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'ciudad_expedicion', index: 'ciudad_expedicion', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'id_ciudad_expedicion', index: 'id_ciudad_expedicion', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'nombre_completo', index: 'nombre_completo', width: 250, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'profesion', index: 'profesion', width: 200, sortable: true, align: 'center', hidden: false},
                {name: 'id_profesion', index: 'id_profesion', width: 100, sortable: true, align: 'center', hidden: true},
                {name: 'cargo', index: 'cargo', width: 150, sortable: true, align: 'center', hidden: false},
                {name: 'id_cargo', index: 'id_cargo', width: 150, sortable: true, align: 'left', hidden: true},
                {name: 'macroproceso', index: 'macroproceso', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_macroproceso', index: 'id_macroproceso', width: 120, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'proceso', index: 'proceso', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_proceso', index: 'id_proceso', width: 120, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'linea_negocio', index: 'linea_negocio', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_linea_negocio', index: 'id_linea_negocio', width: 120, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'producto', index: 'producto', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_producto', index: 'id_producto', width: 120, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'fecha_ingreso', index: 'fecha_ingreso', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'salario', index: 'salario', sortable: true, width: 120, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'nivel_jerarquico', index: 'nivel_jerarquico', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_nivel_jerarquico', index: 'id_nivel_jerarquico', width: 120, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'tipo_contrato', index: 'tipo_contrato', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'id_tipo_contrato', index: 'id_tipo_contrato', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'duracion', index: 'duracion', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'libreta_militar', index: 'libreta_militar', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'banco', index: 'banco', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'id_banco', index: 'id_banco', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'tipo_cuenta', index: 'tipo_cuenta', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'no_cuenta', index: 'no_cuenta', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'riesgo', index: 'riesgo', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'id_riesgo', index: 'id_riesgo', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'eps', index: 'eps', width: 250, sortable: true, align: 'left', hidden: false},
                {name: 'id_eps', index: 'id_eps', width: 230, sortable: true, align: 'left', hidden: true},
                {name: 'arl', index: 'arl', width: 250, sortable: true, align: 'left', hidden: false},
                {name: 'id_arl', index: 'id_arl', width: 230, sortable: true, align: 'left', hidden: true},
                {name: 'afp', index: 'afp', width: 250, sortable: true, align: 'left', hidden: false},
                {name: 'id_afp', index: 'id_afp', width: 230, sortable: true, align: 'left', hidden: true},
                {name: 'cesantias', index: 'cesantias', width: 250, sortable: true, align: 'left', hidden: false},
                {name: 'id_cesantias', index: 'id_cesantias', width: 230, sortable: true, align: 'left', hidden: true},
                {name: 'ccf', index: 'ccf', width: 250, sortable: true, align: 'left', hidden: false},
                {name: 'id_ccf', index: 'id_ccf', width: 230, sortable: true, align: 'left', hidden: true},
                {name: 'dpto', index: 'dpto', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'id_dpto', index: 'id_dpto', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'ciudad', index: 'ciudad', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'id_ciudad', index: 'id_ciudad', width: 120, sortable: true, align: 'center', hidden: true},
                {name: 'direccion', index: 'direccion', width: 180, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'telefono', index: 'telefono', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'celular', index: 'celular', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'email', index: 'email', width: 180, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'fecha_nacimiento', index: 'fecha_nacimiento', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'dpto_nacimiento', index: 'dpto_nacimiento', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'id_dpto_nacimiento', index: 'id_dpto_nacimiento', width: 120, sortable: true, align: 'center', hidden: true},
                {name: 'ciudad_nacimiento', index: 'ciudad_nacimiento', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'id_ciudad_nacimiento', index: 'id_ciudad_nacimiento', width: 120, sortable: true, align: 'center', hidden: true},
                {name: 'sexo', index: 'sexo', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'estado_civil', index: 'estado_civil', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'id_estado_civil', index: 'id_estado_civil', width: 100, sortable: true, align: 'center', hidden: true},
                {name: 'nivel_estudio', index: 'nivel_estudio', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'id_nivel_estudio', index: 'id_nivel_estudio', width: 100, sortable: true, align: 'center', hidden: true},
                {name: 'personas_a_cargo', index: 'personas_a_cargo', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'num_de_hijos', index: 'num_de_hijos', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'total_grupo_familiar', index: 'total_grupo_familiar', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'fecha_retiro', index: 'fecha_retiro', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'observaciones', index: 'observaciones', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'cambio', index: 'cambio', width: 100, sortable: true, align: 'left', hidden: true}


            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 106,
                    identificacion2: $("#identificacion2").val(),
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_Empleados").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_Empleados").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoEmpleados('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_Empleados").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_Empleados"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var nivel_jerarquico = filas.nivel_jerarquico;
                var tipo_contrato = filas.tipo_contrato;
                var duracion = filas.duracion;
                var nombre_completo = filas.nombre_completo;
                var tipo_doc = filas.tipo_doc;
                var identificacion = filas.identificacion;
                var fecha_expedicion = filas.fecha_expedicion;
                var dpto_expedicion = filas.dpto_expedicion;
                var ciudad_expedicion = filas.ciudad_expedicion;
                var libreta_militar = filas.libreta_militar;
                var salario = filas.salario;
                var fecha_ingreso = filas.fecha_ingreso;
                var banco = filas.banco;
                var tipo_cuenta = filas.tipo_cuenta;
                var no_cuenta = filas.no_cuenta;
                var cargo = filas.cargo;
                var riesgo = filas.riesgo;
                var eps = filas.eps;
                var afp = filas.afp;
                var arl = filas.arl;
                var cesantias = filas.cesantias;
                var ccf = filas.ccf;
                var dpto = filas.dpto;
                var ciudad = filas.ciudad;
                var direccion = filas.direccion;
                var telefono = filas.telefono;
                var celular = filas.celular;
                var email = filas.email;
                var fecha_nacimiento = filas.fecha_nacimiento;
                var dpto_nacimiento = filas.dpto_nacimiento;
                var ciudad_nacimiento = filas.ciudad_nacimiento;
                var sexo = filas.sexo;
                var estado_civil = filas.estado_civil;
                var nivel_estudio = filas.nivel_estudio;
                var profesion = filas.profesion;
                var personas_a_cargo = filas.personas_a_cargo;
                var num_de_hijos = filas.num_de_hijos;
                var total_grupo_familiar = filas.total_grupo_familiar;
                var fecha_retiro = filas.fecha_retiro;
                var observaciones = filas.observaciones;
                var id_tipo_doc = filas.id_tipo_doc;
                var id_nivel_jerarquico = filas.id_nivel_jerarquico;
                var id_tipo_contrato = filas.id_tipo_contrato;
                var id_cargo = filas.id_cargo;
                var id_riesgo = filas.id_riesgo;
                var id_eps = filas.id_eps;
                var id_arl = filas.id_arl;
                var id_afp = filas.id_afp;
                var id_cesantias = filas.id_cesantias;
                var id_ccf = filas.id_ccf;
                var id_estado_civil = filas.id_estado_civil;
                var id_nivel_estudio = filas.id_nivel_estudio;
                var id_profesion = filas.id_profesion;
                var id_banco = filas.id_banco;
                var id_dpto_expedicion = filas.id_dpto_expedicion;
                var id_ciudad_expedicion = filas.id_ciudad_expedicion;
                var id_dpto_nacimiento = filas.id_dpto_nacimiento;
                var id_ciudad_nacimiento = filas.id_ciudad_nacimiento;
                var id_dpto = filas.id_dpto;
                var id_ciudad = filas.id_ciudad;
                var macroproceso=filas.macroproceso;
                var id_macroproceso=filas.id_macroproceso;
                var proceso=filas.proceso;
                var id_proceso=filas.id_proceso;
                var linea_negocio=filas.linea_negocio;
                var id_linea_negocio=filas.id_linea_negocio;
                var producto=filas.producto;
                var id_producto=filas.id_producto;
                
                
                    ventanaEmpleados(operacion, id, nivel_jerarquico, tipo_contrato, duracion, nombre_completo, tipo_doc, identificacion, fecha_expedicion, dpto_expedicion,
                            ciudad_expedicion, libreta_militar, salario, fecha_ingreso, banco, tipo_cuenta, no_cuenta, cargo, riesgo, eps, afp, arl, cesantias, ccf, dpto, ciudad, direccion,
                            telefono, celular, email, fecha_nacimiento, dpto_nacimiento, ciudad_nacimiento, sexo, estado_civil, nivel_estudio, profesion, personas_a_cargo, num_de_hijos,
                            total_grupo_familiar, fecha_retiro, observaciones, id_tipo_doc, id_nivel_jerarquico, id_tipo_contrato, id_cargo, id_riesgo, id_eps, id_arl, id_afp, id_cesantias, 
                            id_ccf, id_estado_civil,id_nivel_estudio, id_profesion, id_banco,id_dpto_expedicion,id_ciudad_expedicion,id_dpto_nacimiento,id_dpto_nacimiento,id_ciudad_nacimiento,
                            id_dpto,id_ciudad,macroproceso, id_macroproceso,proceso,id_proceso,linea_negocio,id_linea_negocio,producto,id_producto);
                
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla_.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
//        $("#tabla_Empleados").navButtonAdd('#pager', {
//            caption: "Exportar",
//            onClickButton: function () {
//                exportarExcel();
//            }
//        });

    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                identificacion2: $("#identificacion2").val(),
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaEmpleados(operacion, id, nivel_jerarquico, tipo_contrato, duracion, nombre_completo, tipo_doc, identificacion, fecha_expedicion, dpto_expedicion,
                            ciudad_expedicion, libreta_militar, salario, fecha_ingreso, banco, tipo_cuenta, no_cuenta, cargo, riesgo, eps, afp, arl, cesantias, ccf, dpto, ciudad, direccion,
                            telefono, celular, email, fecha_nacimiento, dpto_nacimiento, ciudad_nacimiento, sexo, estado_civil, nivel_estudio, profesion, personas_a_cargo, num_de_hijos,
                            total_grupo_familiar, fecha_retiro, observaciones, id_tipo_doc, id_nivel_jerarquico, id_tipo_contrato, id_cargo, id_riesgo, id_eps, id_arl, id_afp, id_cesantias, 
                            id_ccf, id_estado_civil,id_nivel_estudio, id_profesion, id_banco,id_dpto_expedicion,id_ciudad_expedicion,id_dpto_nacimiento,id_dpto_nacimiento,id_ciudad_nacimiento,
                            id_dpto,id_ciudad,macroproceso, id_macroproceso,proceso,id_proceso,linea_negocio,id_linea_negocio,producto,id_producto) {
     if (operacion === 'Editar') {
        $("#id").val(id);
        cargarMacroprocesos();
        $("#macroproceso").val(id_macroproceso);
        cargarProcesos(id_macroproceso);
        $("#proceso").val(id_proceso);
        cargarLineasNegocio(id_proceso);
        $("#linea_negocio").val(id_linea_negocio);
        cargarProductos(id_linea_negocio);
        $("#producto").val(id_producto);
        cargarNivelesJerarquicos();
        $("#nivel_jerarquico").val(id_nivel_jerarquico);
        cargarTiposContrato();
        $("#tipo_contrato").val(id_tipo_contrato);
        $("#duracion").val(duracion);
        $("#nombre_completo").val(nombre_completo);
        cargarTipoDoc();
        $("#tipo_doc").val(id_tipo_doc);
        $("#identificacion").val(identificacion);
        $("#fecha_expedicion").val(fecha_expedicion);
        dptoExpedicion();
        $("#dpto_expedicion").val(id_dpto_expedicion);
        ciudadExpedicion(id_dpto_expedicion);
        $("#ciudad_expedicion").val(id_ciudad_expedicion);
        $("#libreta_militar").val(libreta_militar);
        $("#salario").val(salario);
        $("#fecha_ingreso").val(fecha_ingreso);
        cargarBancos();
        $("#banco").val(id_banco);
        $("#tipo_cuenta").val(tipo_cuenta);
        $("#no_cuenta").val(no_cuenta);
        cargarCargos();
        $("#cargo").val(id_cargo);
        cargarRiesgos();        
        $("#riesgo").val(id_riesgo);
        cargarEPS();
        $("#eps").val(id_eps);
        cargarAFP();
        $("#afp").val(id_afp);
        cargarARL();
        $("#arl").val(id_arl);
        cargarCesantias();
        $("#cesantias").val(id_cesantias);
        cargarCCF();
        $("#ccf").val(id_ccf);
        $("#direccion").val(direccion);
        $("#telefono").val(telefono);
        $("#celular").val(celular);
        $("#email").val(email);
        $("#fecha_nacimiento").val(fecha_nacimiento);
        dptoNacimiento();
        $("#dpto_nacimiento").val(id_dpto_nacimiento);
        ciudadNacimiento(id_dpto_nacimiento);
        $("#ciudad_nacimiento").val(id_ciudad_nacimiento);
        dptoResidencia();
        $("#dpto").val(id_dpto);
        ciudadResidencia(id_dpto);
        $("#ciudad").val(id_ciudad);
        $("#sexo").val(sexo);
        cargarEstadoCivil();
        $("#estado_civil").val(id_estado_civil);
        cargarNivelEstudio();
        $("#nivel_estudio").val(id_nivel_estudio);
        cargarProfesiones();
        $("#profesion").val(id_profesion);
        $("#personas_a_cargo").val(personas_a_cargo);
        $("#num_de_hijos").val(num_de_hijos);
        $("#total_grupo_familiar").val(total_grupo_familiar);
        $("#fecha_retiro").val(fecha_retiro);
        $("#observaciones").val(observaciones);
        $("#dialogEmpleados").dialog({
            width: '800',
            height: '800',
            show: "scale",
            hide: "scale",
            resizable: true,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'CREACION DE EMPLEADOS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {

                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#nivel_jerarquico").val('');
                    $("#tipo_contrato").val('');
                    $("#duracion").val('');
                    $("#nombre_completo").val('');
                    $("#tipo_doc").val('');
                    $("#identificacion").val('');
                    $("#fecha_expedicion").val('');
                    $("#dpto_expedicion").val('');
                    $("#ciudad_expedicion").val('');
                    $("#libreta_militar").val('');
                    $("#salario").val('');
                    $("#fecha_ingreso").val('');
                    $("#banco").val('');
                    $("#tipo_cuenta").val('');
                    $("#no_cuenta").val('');
                    $("#cargo").val('');
                    $("#riesgo").val('');
                    $("#eps").val('');
                    $("#afp").val('');
                    $("#arl").val('');
                    $("#cesantias").val('');
                    $("#ccf").val('');
                    $("#dpto").val('');
                    $("#ciudad").val('');
                    $("#direccion").val('');
                    $("#telefono").val('');
                    $("#celular").val('');
                    $("#email").val('');
                    $("#fecha_nacimiento").val('');
                    $("#dpto_nacimiento").val('');
                    $("#ciudad_nacimiento").val('');
                    $("#sexo").val('');
                    $("#estado_civil").val('');
                    $("#nivel_estudio").val('');
                    $("#profesion").val('');
                    $("#personas_a_cargo").val('');
                    $("#num_de_hijos").val('');
                    $("#total_grupo_familiar").val('');
                    $("#fecha_retiro").val('');
                    $("#observaciones").val('');
                    $("#macroproceso").val('');
                    $("#proceso").val('');
                    $("#linea_negocio").val('');
                    $("#producto").val('');
                }
            }
        });
    }
}

function cargarTipoDoc() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 41
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#tipo_doc').html('');
                $('#tipo_doc').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#tipo_doc').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarEstadoCivil() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 42
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#estado_civil').html('');
                $('#estado_civil').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#estado_civil').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function dptoExpedicion() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 43
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#dpto_expedicion').html('');
                $('#dpto_expedicion').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#dpto_expedicion').append('<option value=' + json[i].department_code + '>' + json[i].department_name + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function ciudadExpedicion(department_code) {
    //var dpto_expedicion = $("#" + department_code).val();
    //var dpto_expedicion = $('#dpto_expedicion').val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 44,
            dpto_expedicion: department_code

        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#ciudad_expedicion').html('');
                $('#ciudad_expedicion').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#ciudad_expedicion').append('<option value=' + json[i].codciu + '>' + json[i].nomciu + '</option>');

                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}




function cargarNivelEstudio() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 45
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#nivel_estudio').html('');
                $('#nivel_estudio').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#nivel_estudio').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function dptoNacimiento() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 46
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#dpto_nacimiento').html('');
                $('#dpto_nacimiento').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#dpto_nacimiento').append('<option value=' + json[i].department_code + '>' + json[i].department_name + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ciudadNacimiento(department_code) {
    //var dpto_nacimiento = $("#" + department_code).val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 47,
            dpto_nacimiento: department_code

        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#ciudad_nacimiento').html('');
                $('#ciudad_nacimiento').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#ciudad_nacimiento').append('<option value=' + json[i].codciu + '>' + json[i].nomciu + '</option>');

                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTiposContrato() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 29
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#tipo_contrato').html('');
                $('#tipo_contrato').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#tipo_contrato').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarDependencias() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 21
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#dependencia').html('');
                $('#dependencia').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#dependencia').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCargos() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 25
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#cargo').html('');
                $('#cargo').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#cargo').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarRiesgos() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 33
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#riesgo').html('');
                $('#riesgo').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#riesgo').append('<option value=' + json[i].id + '>' + json[i].codigo + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarEPS() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 5
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#eps').html('');
                $('#eps').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#eps').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarARL() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 9
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#arl').html('');
                $('#arl').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#arl').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarAFP() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 13
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#afp').html('');
                $('#afp').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#afp').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCesantias() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 13
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#cesantias').html('');
                $('#cesantias').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#cesantias').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCCF() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 17
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#ccf').html('');
                $('#ccf').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#ccf').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function dptoResidencia() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 56
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#dpto').html('');
                $('#dpto').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#dpto').append('<option value=' + json[i].department_code + '>' + json[i].department_name + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ciudadResidencia(department_code) {
    //var dpto = $("#" + department_code).val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 57,
            dpto: department_code

        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#ciudad').html('');
                $('#ciudad').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#ciudad').append('<option value=' + json[i].codciu + '>' + json[i].nomciu + '</option>');

                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarNivelesJerarquicos() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 48
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#nivel_jerarquico').html('');
                $('#nivel_jerarquico').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#nivel_jerarquico').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarProfesiones() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 52
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#profesion').html('');
                $('#profesion').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#profesion').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}



function cargarBancos() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 58
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#banco').html('');
                $('#banco').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#banco').append('<option value=' + json[i].codigo + '>' + json[i].nombre + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}



function cargarMacroprocesos() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 62
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#macroproceso').html('');
                $('#macroproceso').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#macroproceso').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarProcesos(id) {
console.log(id);
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 63,
            macroproceso: id

        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#proceso').html('');
                $('#proceso').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#proceso').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');

                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarLineasNegocio(id) {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 64,
            proceso: id

        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#linea_negocio').append('<option value=' + json[i].id + '>' + json[i].linea_negocio + '</option>');

                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarProductos(id) {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 65,
            linea_negocio: id

        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#producto').html('');
                $('#producto').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#producto').append('<option value=' + json[i].id + '>' + json[i].producto + '</option>');

                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function fechaRetiro(){
    var f = $("#fecha_ingreso").val();
    var d = $("#duracion").val();
    console.log(d);
    vec = f.split('-'); // Parsea y pasa a un vector
    var fecha = new Date(vec[0], vec[1], vec[2]); // crea el Date
    fecha.setDate(fecha.getDate()+parseInt(d)); // Hace el c�lculo
    res = fecha.getFullYear()+'-'+fecha.getMonth()+'-'+fecha.getDate(); // carga el resultado
    $("#fecha_retiro").datepicker("setDate",res)
    console.log(res);
    
}

function  exportarExcel() {
//    var fullData = $("#tabla_Empleados").jqGrid('getRowData');
//    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 240,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        data: {
//            listado: myJsonString,
            opcion: 67
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}