/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//}
$(document).ready(function () {

    fechas();
    horas ();
    restarHoras();
    //calcularDias();

    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $('.solo-numeric').live('blur', function (event) {
        this.value = numberConComas(this.value);
    });


});

function inicio1() {
    cargarHorasExtras();
}


function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function numberSinPuntos(x) {
    return x.toString().replace(/\./g, "");
}

function fechas() {

    $(".fechas").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    var myDate = new Date();
    $("#fecha_solicitud").datepicker("setDate", myDate);
    $("#fecha_ini").datepicker("setDate", myDate);
    $("#fecha_fin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    $("#duracion_dias").val('0');
    $("#dias_a_pagar").val('0');
    $('#dias_disfrute').val('0');
    
}

function calcularDias(){
    
    var fechaInicial=document.getElementById("fecha_ini").value;
    var fechaFinal=document.getElementById("fecha_fin").value;
    var resultado="";
    
    inicial=fechaInicial.split("-");
    final=fechaFinal.split("-");
    
    var dateStart=new Date(inicial[0],(inicial[1]),inicial[2]);
    var dateEnd=new Date(final[0],(final[1]),final[2]);
    
    //alert (fechaInicial);
    //alert (fechaFinal);
    if(dateStart<dateEnd){                   
        //resultado="La diferencia es de "+(((dateEnd-dateStart)/86400)/1000)+" d�as";
        res= ((dateEnd-dateStart)/86400)/1000;
        //$("#duracion_dias").val(res);
        $("#dias_a_pagar").val(res);
    }else{ 
        resultado="La fecha inicial es posterior a la fecha final";
        alert (resultado);
        }
        //alert (resultado);
    }


                
	

function horas () {

    $(".horas").timepicker({
//        showPeriod: true,
//        showLeadingZero: true

    });
    
    //$("#hora_ini").timepicker('setTime', new Date());
    //$("#hora_fin").timepicker('setTime', new Date());
    $("#hora_ini").timepicker('setTime', '00:00');
    $("#hora_fin").timepicker('setTime', '00:00');
    //$("#duracion_horas").val('0');
}

function restarHoras() {
  var hora_ini = $("#hora_ini").val();
  var hora_fin = $("#hora_fin").val();
  
  
  inicioMinutos = parseInt(hora_ini.substr(3,2));
  inicioHoras = parseInt(hora_ini.substr(0,2));
  
  finMinutos = parseInt(hora_fin.substr(3,2));
  finHoras = parseInt(hora_fin.substr(0,2));

  transcurridoMinutos = finMinutos - inicioMinutos;
  transcurridoHoras = finHoras - inicioHoras;
  
  if (transcurridoMinutos < 0) {
    transcurridoHoras--;
    transcurridoMinutos = 60 + transcurridoMinutos;
  }
  
  horas = transcurridoHoras.toString();
  minutos = transcurridoMinutos.toString();
  
  if (horas.length < 2) {
    horas = "0"+horas;
  }
  
  if (horas.length < 2) {
    horas = "0"+horas;
  }
  
  //document.getElementById("duracion_horas").value = horas+":"+minutos;
  //$("#duracion_horas").val(horas+":"+minutos);
  $("#duracion_horas").val(horas);
  
}

function format(input) {
    var num = input.value.replace(/\./g, '');
    if (!isNaN(num)) {
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/, '');
        input.value = num;
    }
    else {
        //alert('Solo se permiten numeros');
        input.value = input.value.replace(/[^\d\.]*/g, '');
    }
}



function limpiar(){
    $("#tipo_hora").val('');
    $("#razon").val('');     
    $("#identificacion").val(''); 
    $("#nombre_completo").val('');
    $("#fecha_solicitud").val('');
    $("#fecha_trabajo").val('');  
    $("#hora_ini").val('');     
    $("#hora_fin").val('');    
    $("#duracion_horas").val('');    
     $("#jefe_directo").val('');     
    $("#descripcion").val('');     
    
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarTipoHoras() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 87
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {        
                console.log(json.length);
                $('#tipo_hora').html('');
                $('#tipo_hora').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#tipo_hora').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarHorasExtras() {
    //
    //alert('Entra aqui');
    var operacion;
    var grid_tabla_ = jQuery("#tabla_HorasExtras");
    if ($("#gview_tabla_HorasExtras").length) {
        reloadGridMostrar(grid_tabla_, 92);
    } else {
    //alert('Entra aqui');    
        grid_tabla_.jqGrid({
            
            caption: "GESTION SOLICITUD DE HORAS EXTRAS",
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            mtype: "POST",
            datatype: "json",
            height: '750',
            width: '1500',
            colNames: ['Id', 'Estado','Tipo Hora','Id Tipo Hora','Fecha Solicitud','Identificacion','Nombre','Fecha Trabajo','Hora Inicial','Horas Final','No. Horas', 
                'Descripcion','Aprobado','Comentario','Fecha Aprobacion','Id Jefe Aprueba','Jefe Aprueba','Remunerado','Fecha Visto Bueno','Usuario Visto Bueno',
                'Observaciones','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'tipo_hora', index: 'tipo_hora', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'id_tipo_hora', index: 'id_tipo_hora', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'fecha_solicitud', index: 'fecha_solicitud', width: 80, sortable: true, align: 'left', hidden: false},
                {name: 'identificacion', index: 'identificacion', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'nombre_completo', index: 'nombre_completo', width: 220, sortable: true, align: 'left', hidden: false},
                {name: 'fecha_trabajo', index: 'fecha_trabajo', width: 80, sortable: true, align: 'left', hidden: false},
                {name: 'hora_ini', index: 'hora_ini', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'hora_fin', index: 'hora_fin', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'duracion_horas', index: 'duracion_horas', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 250, sortable: true, align: 'left', hidden: false},
                {name: 'aprobado', index: 'aprobado', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'comentario', index: 'comentario', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'fecha_autoriza', index: 'fecha_autoriza', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'id_jefe_directo', index: 'id_jefe_directo', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'jefe_directo', index: 'jefe_directo', width: 200, sortable: true, align: 'left', hidden: false},
                {name: 'remunerada', index: 'remunerada', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'fecha_visto_bueno', index: 'fecha_visto_bueno', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'user_visto_bueno', index: 'user_visto_bueno', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'observaciones', index: 'observaciones', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'cambio', index: 'cambio', width: 100, sortable: true, align: 'left', hidden: true}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 92
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_HorasExtras").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_HorasExtras").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoHorasExtras('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_HorasExtras").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_HorasExtras"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var tipo_hora = filas.tipo_hora;
                var id_tipo_hora = filas.id_tipo_hora;
                var fecha_solicitud = filas.fecha_solicitud;
                var identificacion = filas.identificacion;
                var nombre_completo = filas.nombre_completo;
                var fecha_trabajo = filas.fecha_trabajo;
                var hora_ini = filas.hora_ini;
                var duracion_horas = filas.duracion_horas;
                var hora_fin = filas.hora_fin; 
                var descripcion = filas.descripcion;
                var id_jefe_directo = filas.id_jefe_directo;
                var jefe_directo = filas.jefe_directo;
                var aprobado = filas.aprobado;
                var comentario = filas.comentario;
                var remunerada =filas.remunerada;
                
                
                if (remunerada ==='') {
                    ventanaHorasExtras(operacion, id, estado, tipo_hora, id_tipo_hora, fecha_solicitud, identificacion, nombre_completo, fecha_trabajo, hora_ini, duracion_horas, hora_fin,
                    descripcion,id_jefe_directo, jefe_directo,aprobado, comentario);
                } else {
                    mensajesDelSistema("No debe tener visto bueno a�n", '250', '180', false);
                }

            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
//        $("#tabla_HorasExtras").navButtonAdd('#pager', {
//            caption: "Nuevo",
//            onClickButton: function () {
//                operacion = 'Nuevo';
//                ventanaHorasExtras(operacion);
//            }
//        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaHorasExtras(operacion, id, estado, tipo_hora, id_tipo_hora, fecha_solicitud, identificacion, nombre_completo, fecha_trabajo, hora_ini, duracion_horas, hora_fin,
                    descripcion, id_jefe_directo,jefe_directo, aprobado, comentario) {
    if (operacion === 'Editar') {
        $("#id").val(id);
        cargarTipoHoras();
        $("#tipo_hora").val(id_tipo_hora);
        $("#identificacion").val(identificacion);
        cargarNombre(identificacion);
        $("#nombre_completo").val(nombre_completo);
        $("#fecha_solicitud").val(fecha_solicitud);
        $("#fecha_trabajo").val(fecha_trabajo);
        $("#hora_ini").val(hora_ini);
        $("#hora_fin").val(hora_fin);
        $("#duracion_horas").val(duracion_horas);
        cargarJefeDirecto();
        $("#jefe_directo").val(id_jefe_directo);
        $("#descripcion").val(descripcion);
        $("#aprobado").val(aprobado);
        $("#comentario").val(comentario);
        $("#dialogHorasExtras").dialog({
            width: '750',
            height: '400',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'GESTION DE HORAS EXTRAS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    aprobarSolicitudHorasExtras();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    limpiar();
                }
            }
        });
    }
}
function aprobarSolicitudHorasExtras() {

    var aprobado= $("#aprobado").val();
    var comentario= $("#comentario").val();
    var identificacion= $("#identificacion").val();
    var tipo_hora= $("#tipo_hora").val();
    var jefe_directo= $("#jefe_directo").val();
    
    if (
//     tipo_novedad!== '' && razon!== '' && identificacion!== '' && fecha_solicitud!== '' && fecha_ini!== '' && fecha_fin!== '' && duracion_dias!== '' && hora_ini!== '' && hora_fin!== '' && duracion_horas!== ''    
//     && cod_enfermedad!== '' && jefe_directo!== '' && descripcion!== '' && dias_disfrute!== '' && dias_a_pagar!== ''
    aprobado !== '' && comentario !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            data: {
                opcion: 93,
                id: $("#id").val(),
                aprobado: $("#aprobado").val(),
                comentario: $("#comentario").val(),
                identificacion: $("#identificacion").val(),
                tipo_hora: $("#tipo_hora").val(),
                jefe_directo: $("#jefe_directo").val()

            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al aprobar", '230', '150', true);
                    $("#dialogHorasExtras").dialog('close');
                    $("#id").val('');
                    $("#tipo_hora").val('');
                    $("#identificacion").val(''); 
                    $("#fecha_solicitud").val('');
                    $("#fecha_trabajo").val('');  
                    $("#hora_ini").val('');     
                    $("#hora_fin").val('');    
                    $("#duracion_horas").val('');
                    $("#jefe_directo").val('');     
                    $("#descripcion").val('');     
                    $("#aprobado").val('');  
                    $("#comentario").val('');  
                    cargarHorasExtras();
                }else {
                    mensajesDelSistema('Lo sentimos no se pudo aprobar la solicitud', '300', 'auto', false);
                }
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function cargarNombre(identificacion) {
    
    identificacion = $('#identificacion').val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'text',
        async: false,
        data: {
            opcion: 72,
            identificacion: numberSinPuntos($("#identificacion").val()),
        },
        success: function (json) {
            console.log(json);
            if (json.error) {
                console.log(json.error);
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {        
                //alert(identificacion);
                console.log(json);
                if(json.trim() ==''){
                    alert('El documento no es valido o est� vac�o. Favor verificar');
            }
                $('#nombre_completo').val(json);
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarJefeDirecto() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 73
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {        
                console.log(json.length);
                $('#jefe_directo').html('');
                $('#jefe_directo').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#jefe_directo').append('<option value=' + json[i].id + '>' + json[i].login + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}



function calcularDiasF() {
    var fecha_ini = $("#fecha_ini").val();
    //var fecha_fin = $("#fecha_fin").val();
    var duracion_dias = $("#duracion_dias").val();
     $.ajax({
            type: "POST",
            dataType: "json",
            data: {opcion:85,
                fecha_ini: fecha_ini,
                duracion_dias: duracion_dias},
            async: false,
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            success: function (jsonData) {
                console.log(jsonData.respuesta);
            try {        

                $('#fecha_fin').val(jsonData.respuesta);
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }

                
		
                