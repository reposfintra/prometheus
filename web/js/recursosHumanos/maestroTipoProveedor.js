/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function inicio1() {
    cargarTipo_proveedor();
}


function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarTipo_proveedor() {
    console.log('Entra aqui');
    var operacion;
    var grid_tabla_ = jQuery("#tabla_Tipo_proveedor");
    if ($("#gview_tabla_Tipo_proveedor").length) {
        reloadGridMostrar(grid_tabla_, 1);
    } else {
    //alert('Entra aqui');    
        grid_tabla_.jqGrid({
            
            caption: "Tipos de Proveedor",
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '600',
            colNames: ['Id', 'Estado','Codigo', 'Descripcion','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'codigo', index: 'codigo', width: 80, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'descripcion', index: 'descripcion', width: 285, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cambio', index: 'cambio', width: 100, sortable: true, align: 'left', hidden: false}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 1
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_Tipo_proveedor").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_Tipo_proveedor").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoTipo_proveedor('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_Tipo_proveedor").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_Tipo_proveedor"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var codigo = filas.codigo;
                var descripcion = filas.descripcion;
                if (estado === 'Activo') {
                    ventanaTipo_proveedor(operacion, id, codigo, descripcion);
                } else if (estado === 'Inactivo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_Tipo_proveedor").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaTipo_proveedor(operacion);
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaTipo_proveedor(operacion, id, codigo, descripcion) {
    if (operacion === 'Nuevo') {
        $("#dialogTipo_proveedor").dialog({
            width: '650',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'TIPOS DE PROVEEDOR',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarTipo_proveedor();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#codigo").val('');
                    $("#descripcion").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        $("#nombre").val(codigo);
        $("#descripcion").val(descripcion);
        

        $("#dialogTipo_proveedor").dialog({
            width: '650',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'TIPOS DE PROVEEDOR',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarTipo_proveedor();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#codigo").val('');
                    $("#descripcion").val('');
                }
            }
        });
    }
}

function guardarTipo_proveedor() {
    var codigo = $("#codigo").val();
    var desc = $("#descripcion").val();
    //alert(desc);
    if (codigo !== '' && desc !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            data: {
                opcion: 2,
                nombre: $("#nombre").val(),
                codigo: $("#codigo").val(),
                descripcion: $("#descripcion").val(),
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                    $("#dialogTipo_proveedor").dialog('close');
                    $("#codigo").val('');
                    $("#descripcion").val('');
                }
                cargarTipo_proveedor();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function ActualizarTipo_proveedor() {
    var codigo = $("#codigo").val();
    var desc = $("#descripcion").val();
    if (codigo !== '' && desc !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            data: {
                opcion: 3,
                id: $("#id").val(),
                codigo: $("#codigo").val(),
                desc: $("#descripcion").val(),
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#dialogTipo_proveedor").dialog('close');
                    $("#codigo").val('');
                    $("#descripcion").val('');
                }
                cargarTipo_proveedor();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function CambiarEstadoTipo_proveedor(rowid) {
    var grid_tabla = jQuery("#tabla_Tipo_proveedor");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        data: {
            opcion: 4,
            id: id
        },
        success: function (data) {
            cargarTipo_proveedor();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

