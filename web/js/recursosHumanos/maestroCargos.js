/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function inicio1() {
    cargarCargos();
}


function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarCargos() {
    //
    //alert('Entra aqui');
    var operacion;
    var grid_tabla_ = jQuery("#tabla_Cargos");
    if ($("#gview_tabla_Cargos").length) {
        reloadGridMostrar(grid_tabla_, 25);
    } else {
    //alert('Entra aqui');    
        grid_tabla_.jqGrid({
            
            caption: "Cargos",
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            mtype: "POST",
            datatype: "json",
            height: '400',
            width: '650',
            colNames: ['Id', 'Estado', 'Descripcion','Codigo','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 320, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'codigo', index: 'codigo', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'cambio', index: 'cambio', width: 100, sortable: true, align: 'left', hidden: false}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 25
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_Cargos").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_Cargos").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoCargos('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_Cargos").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_Cargos"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var descripcion = filas.descripcion;
                var codigo = filas.codigo;
                if (estado === 'Activo') {
                    ventanaCargos(operacion, id, descripcion, codigo);
                } else if (estado === 'Inactivo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_Cargos").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaCargos(operacion);
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaCargos(operacion, id, descripcion, codigo) {
    if (operacion === 'Nuevo') {
        $("#dialogCargos").dialog({
            width: '430',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'CARGOS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarCargos();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#descripcion").val('');
                    $("#codigo").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        $("#descripcion").val(descripcion);
        $("#codigo").val(codigo);
        $("#dialogCargos").dialog({
            width: '430',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'CARGOS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarCargos();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#descripcion").val('');
                }
            }
        });
    }
}

function guardarCargos() {
    var desc = $("#descripcion").val();
    var cod = $("#codigo").val();
    //alert(desc);
    if (desc !== '' && cod!=='') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            data: {
                opcion: 26,
                descripcion: $("#descripcion").val(),
                codigo: $("#codigo").val()
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                    $("#dialogCargos").dialog('close');
                    $("#descripcion").val('');
                    $("#codigo").val('');
                }
                cargarCargos();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function ActualizarCargos() {
    var desc = $("#descripcion").val();
    var cod = $("#codigo").val();
    if (desc !== '' && cod!=='') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            data: {
                opcion: 27,
                id: $("#id").val(),
                descripcion: $("#descripcion").val(),
                codigo: $("#codigo").val()
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#dialogCargos").dialog('close');
                    $("#descripcion").val('');
                    $("#codigo").val('');
                }
                cargarCargos();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function CambiarEstadoCargos(rowid) {
    var grid_tabla = jQuery("#tabla_Cargos");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        data: {
            opcion: 28,
            id: id
        },
        success: function (data) {
            cargarCargos();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}



