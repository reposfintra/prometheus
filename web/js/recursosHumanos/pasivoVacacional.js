/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//}
$(document).ready(function () {
    
    fechas();
//    horas();
//    restarHoras();
    //calcularDias();

    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $('.solo-numeric').live('blur', function (event) {
        this.value = numberConComas(this.value);
    });


//    $("#fechaini").val('');
//    $("#fechafin").val('');
    $("#periodo_ini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())

    });
    $("#periodo_fin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $("#periodo_ini").datepicker("setDate", myDate);
    $("#periodo_fin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
   
    

    $('#buscar').click(function () {
       
        cargarPasivo();
        
    });
    $('#limpiar').click(function () {

        $("#identificacion2").val('');

    });


});




function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function numberSinPuntos(x) {
    return x.toString().replace(/\./g, "");
}

function fechas() {

    $("#fecha_fin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha_solicitud").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha_ini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var f = new Date();
    var myDate = new Date();

    $("#fecha_solicitud").datepicker("setDate", myDate);
    $("#fecha_ini").datepicker("setDate", myDate);
    $("#fecha_fin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    $("#duracion_dias").val('0');
    $("#dias_a_pagar").val('0');
    $('#dias_disfrute').val('0');
    if ((f.getMonth() + 1) < 10) {
        $('#fechaSolicitud').val(f.getFullYear() + '-' + '0' + (f.getMonth() + 1) + '-' + f.getDate());
        if ((f.getDate()) < 10) {
            $('#fechaSolicitud').val(f.getFullYear() + '-' + '0' + (f.getMonth() + 1) + '-' + '0' + f.getDate());
        }
        
        
    } else {
        $('#fechaSolicitud').val(f.getFullYear() + '-' + (f.getMonth() + 1) + '-' + f.getDate() );
    }

}



function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}


function cargarPasivo() {
    //
    //alert('Entra aqui');
    //var operacion;
    var grid_tabla_ = jQuery("#tabla_PasivoVacacional");
    if ($("#gview_tabla_PasivoVacacional").length) {
        reloadGridMostrar(grid_tabla_, 112);
    } else {
        //alert('Entra aqui');    
        grid_tabla_.jqGrid({
            caption: "PASIVO VACACIONAL",
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            mtype: "POST",
            datatype: "json",
            height: '650',
            width: '1220',
            colNames: ['Fecha Ingreso', 'Identificacion', 'Nombre', 'Periodo Inicial', 'Periodo Final', 'Saldo Inicial', 'No. Dias', 'Dias Causados', 'Dias Disfrutados','Dias Compensados', 'Saldo Final'],
            colModel: [
                //{name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'fecha_ingreso', index: 'fecha_ingreso', width: 80, sortable: true, align: 'left', hidden: false},
                {name: 'identificacion', index: 'identificacion', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'nombre_completo', index: 'nombre_completo', width: 240, sortable: true, align: 'left', hidden: false},
                {name: 'periodo_ini', index: 'periodo_ini', width: 80, sortable: true, align: 'left', hidden: false},
                {name: 'periodo_fin', index: 'periodo_fin', width: 80, sortable: true, align: 'left', hidden: false},
                {name: 'saldo_inicial', index: 'saldo_inicial', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'dias', index: 'dias', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'dias_causados', index: 'dias_causados', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 2}},
                {name: 'dias_disfrutados', index: 'dias_disfrutados', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'dias_compensados', index: 'dias_compensados', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'saldo_final', index: 'saldo_final', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 2}}
                
                

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 112,
                    identificacion2: $("#identificacion2").val(),

                }
            },
            gridComplete: function () {
                var saldo_final = jQuery("#tabla_PasivoVacacional").jqGrid('getCol', 'saldo_final', false, 'sum');
                jQuery("#tabla_PasivoVacacional").jqGrid('footerData', 'set', {saldo_final: saldo_final});

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_PasivoVacacional").navButtonAdd('#pager', {
            caption: "Exportar",
            onClickButton: function () {
                exportarExcel();
            }
        });
                $("#tabla_PasivoVacacional").navButtonAdd('#pager', {
            caption: "Exportar Acumulado",
            onClickButton: function () {
                exportarExcelAcumulado();
            }
        });

    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
//                fecha_inicio: $("#fechaini").val(),
//                fecha_fin: $("#fechafin").val(),
//                tipo_novedad1: $("#tipo_novedad1").val(),
                identificacion2: $("#identificacion2").val(),
                //status: $("#status").val()

            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}


function  exportarExcel() {
    var fullData = $("#tabla_PasivoVacacional").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 240,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        data: {
            listado: myJsonString,
            opcion: 121
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
    
    }

function  exportarExcelAcumulado() {
//    var fullData = $("#tabla_Empleados").jqGrid('getRowData');
//    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 240,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        data: {
//            listado: myJsonString,
            opcion: 122,
            identificacion: $("#identificacion2").val(),
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

//
//
//function actualizarPeriodoActual(identificacion) {
//    //alert(identificacion);
//    identificacion = $('#identificacion').val();
//    $.ajax({
//        type: 'POST',
//        url: "./controller?estado=AdmonRecursos&accion=Humanos",
//        dataType: 'text',
//        async: false,
//        data: {
//            opcion: 117,
//            identificacion2: $("#identificacion2").val()
//            
//        },
//        success: function (json) {
//            console.log(json);
//            if (json.error) {
//                console.log(json.error);
//                return;
//            }
//            try {
//                console.log(json);
//                //$('#saldo').val(json);
//            } catch (exception) {
//                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
//            }
//            
//            
//
//        }, error: function (xhr, ajaxOptions, thrownError) {
//            alert("Error: " + xhr.status + "\n" +
//                    "Message: " + xhr.statusText + "\n" +
//                    "Response: " + xhr.responseText + "\n" + thrownError);
//        }
//    });
//}