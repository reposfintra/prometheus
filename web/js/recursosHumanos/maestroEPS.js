/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function inicio1() {
    cargarEPS();
}


function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarEPS() {
    //
    //alert('Entra aqui');
    var operacion;
    var grid_tabla_ = jQuery("#tabla_EPS");
    if ($("#gview_tabla_EPS").length) {
        reloadGridMostrar(grid_tabla_, 5);
    } else {
    //alert('Entra aqui');    
        grid_tabla_.jqGrid({
            
            caption: "Entidades Promotoras de Salud",
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '720',
            colNames: ['Id', 'Estado','Nit','Digito', 'Descripcion','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'nit', index: 'nit', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'digito_verificacion', index: 'digito_verificacion', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 320, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cambio', index: 'cambio', width: 100, sortable: true, align: 'left', hidden: false}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 5
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_EPS").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_EPS").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoEPS('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_EPS").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
//            ondblClickRow: function (rowid, iRow, iCol, e) {
//                operacion = 'Editar';
//                var myGrid = jQuery("#tabla_EPS"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
//                filas = myGrid.jqGrid("getLocalRow", selRowIds);
//                var id = filas.id;
//                var estado = filas.estado;
//                var nit = filas.nit;
//                var digito_verificacion = filas.digito_verificacion;
//                var descripcion = filas.descripcion;
//                if (estado === 'Activo') {
//                    ventanaEPS(operacion, id, nit, digito_verificacion, descripcion);
//                } else if (estado === 'Inactivo') {
//                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
//                }
//            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        /*$("#tabla_EPS").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaEPS(operacion);
            }
        });*/
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaEPS(operacion, id, nit, digito_verificacion, descripcion) {
    if (operacion === 'Nuevo') {
        $("#dialogEPS").dialog({
            width: '550',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'ENTIDADES PROMOTORAS DE SALUD',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarEPS();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#nit").val('');
                    $("#digito_verificacion").val('');
                    $("#descripcion").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        $("#nit").val(nit);
        $("#digito_verificacion").val(digito_verificacion);
        $("#descripcion").val(descripcion);
        $("#dialogEPS").dialog({
            width: '420',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'ENTIDADES PROMOTORAS DE SALUD',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarEPS();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#nit").val('');
                    $("#digito_verificacion").val('');
                    $("#descripcion").val('');
                }
            }
        });
    }
}

function guardarEPS() {
    var nit  = $("#nit").val();
    var digito = $("#digito_verificacion").val();
    var desc = $("#descripcion").val();
    //alert(desc);
    if (nit !== '' && digito !== ''  && desc !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            data: {
                opcion: 6,
                nombre: $("#nombre").val(),
                desc: $("#descripcion").val(),
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                    $("#dialogEPS").dialog('close');
                    $("#nit").val('');
                    $("#digito_verificacion").val('');
                    $("#descripcion").val('');
                }
                cargarEPS();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function ActualizarEPS() {
    
    var nit  = $("#nit").val();
    var digito = $("#digito_verificacion").val();
    var desc = $("#descripcion").val();
    if (nit !== '' && digito !== ''  && desc !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            data: {
                opcion: 7,
                id: $("#id").val(),
                nit: $("#nit").val(),
                digito: $("#digito_verificacion").val(),
                desc: $("#descripcion").val(),
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#dialogEPS").dialog('close');
                    $("#nit").val('');
                    $("#digito_verificacion").val('');
                    $("#descripcion").val('');
                }
                cargarEPS();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function CambiarEstadoEPS(rowid) {
    var grid_tabla = jQuery("#tabla_EPS");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        data: {
            opcion: 8,
            id: id
        },
        success: function (data) {
            cargarEPS();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}
