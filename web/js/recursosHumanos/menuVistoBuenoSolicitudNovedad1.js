/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function Menu(nombreMenu, opciones){
    this.nombreMenu = nombreMenu;
    this.opciones = opciones;
    this.generarMenu = function(){
        var menu = this.cabecera(this.nombreMenu);
        for(i = 0; i<this.opciones.length;i++){
            menu += this.itemOpcion(opciones[i]);
        }
        menu += this.cerrarSesion();
        return menu;
    };
    this.cabecera = function(nombre){
        return  '<li class = "sidebar-brand" style="cursor:pointer;">'+
                    '<a>'+ nombre +'</a>'+
                '</li>';
    };
    this.itemOpcion = function(opcion){
        return '<li style="cursor:pointer;">'+
                    '<a class="menu-item" ruta-pagina="'+opcion.ruta+'">'+opcion.descripcion+'</a>'+
               '</li>';
    };
    this.cerrarSesion = function(){
        return '<li style="cursor:pointer;">'+
                    '<a href="#" onclick=" window.close(); ">Salir</a>'+
               '</li>';
    };
}


