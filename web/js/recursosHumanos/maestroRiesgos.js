/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function inicio1() {
    cargarRiesgos();
}


function onKeyDecimal(e,thix) {
        var keynum = window.event ? window.event.keyCode : e.which;
        if (document.getElementById(thix.id).value.indexOf('.') != -1 && keynum == 46)
            return false;
        if ((keynum == 8 || keynum == 48 || keynum == 46))
            return true;
        if (keynum <= 47 || keynum >= 58) return false;
        return /\d/.test(String.fromCharCode(keynum));
    }

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarRiesgos() {
    //
    //alert('Entra aqui');
    var operacion;
    var grid_tabla_ = jQuery("#tabla_Riesgos");
    if ($("#gview_tabla_Riesgos").length) {
        reloadGridMostrar(grid_tabla_, 33);
    } else {
    //alert('Entra aqui');    
        grid_tabla_.jqGrid({
            
            caption: "Riesgos",
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '1500',
            colNames: ['Id', 'Estado', 'Intensidad','Codigo','Porcentaje','Actividades','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'intensidad', index: 'intensidad', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'codigo', index: 'codigo', width: 80, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'porcentaje', index: 'porcentaje', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'actividades', index: 'actividades', width: 1015, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cambio', index: 'cambio', width: 100, sortable: true, align: 'left', hidden: false}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 33
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_Riesgos").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_Riesgos").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoRiesgos('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_Riesgos").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_Riesgos"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var intensidad = filas.intensidad;
                var codigo = filas.codigo;
                var porcentaje = filas.porcentaje;
                var actividades = filas.actividades;
                if (estado === 'Activo') {
                    ventanaRiesgos(operacion, id, intensidad, codigo, porcentaje, actividades);
                } else if (estado === 'Inactivo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_Riesgos").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaRiesgos(operacion);
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaRiesgos(operacion, id, intensidad, codigo, porcentaje, actividades) {
    if (operacion === 'Nuevo') {
        $("#dialogRiesgos").dialog({
            width: '570',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'TIPOS DE RIESGOS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarRiesgos();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#intensidad").val('');
                    $("#codigo").val('');
                    $("#porcentaje").val('');
                    $("#actividades").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        $("#intensidad").val(intensidad);
        $("#codigo").val(codigo);
        $("#porcentaje").val(porcentaje);
        $("#actividades").val(actividades);
        $("#dialogRiesgos").dialog({
            width: '570',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'TIPOS DE RIESGOS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarRiesgos();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#intensidad").val('');
                    $("#codigo").val('');
                    $("#porcentaje").val('');
                    $("#actividades").val('');
                }
            }
        });
    }
}

function guardarRiesgos() {
    var intensidad = $("#intensidad").val();
    var porcentaje = $("#porcentaje").val();
    var actividades = $("#actividades").val();
    //alert(desc);
    if (intensidad !== '' && porcentaje !== '' && actividades !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            data: {
                opcion: 34,
                intensidad: $("#intensidad").val(),
                porcentaje: $("#porcentaje").val(),
                actividades: $("#actividades").val(),
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                    $("#dialogRiesgos").dialog('close');
                    $("#descripcion").val('');
                }
                cargarRiesgos();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function ActualizarRiesgos() {
    var intensidad = $("#intensidad").val();
    var porcentaje = $("#porcentaje").val();
    var actividades = $("#actividades").val();
    if (intensidad !== '' && porcentaje !== '' && actividades !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            data: {
                opcion: 35,
                id: $("#id").val(),
                intensidad: $("#intensidad").val(),
                porcentaje: $("#porcentaje").val(),
                actividades: $("#actividades").val(),
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#dialogRiesgos").dialog('close');
                    $("#descripcion").val('');
                }
                cargarRiesgos();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function CambiarEstadoRiesgos(rowid) {
    var grid_tabla = jQuery("#tabla_Riesgos");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        data: {
            opcion: 36,
            id: id
        },
        success: function (data) {
            cargarRiesgos();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}





