/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//}
$(document).ready(function () {
    
    fechas();
//    horas();
//    restarHoras();
    //calcularDias();

    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $('.solo-numeric').live('blur', function (event) {
        this.value = numberConComas(this.value);
    });


//    $("#fechaini").val('');
//    $("#fechafin").val('');
    $("#fechaini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())

    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $("#fechaini").datepicker("setDate", myDate);
    $("#fechafin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    cargarTipoIncapacidad();
    
    var operacion = 'Nuevo';
    $('#nuevo').click(function () {
        ventanaNovedades(operacion);
    });
    $('#buscar').click(function () {
        cargarNovedades();
    });
    $('#exportar').click(function () {
        exportarExcelAuditoria();
    });

    $('#limpiar').click(function () {
        $("#fechaini").val('');
        $("#fechafin").val('');
        $("#tipo_novedad1").val('');
        $("#identificacion1").val('');

    });


});

function inicio1() {
    cargarNovedades();
}


function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function numberSinPuntos(x) {
    return x.toString().replace(/\./g, "");
}

function fechas() {

    $("#fecha_fin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha_solicitud").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha_ini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var f = new Date();
    var myDate = new Date();
    $("#fecha_solicitud").datepicker("setDate", myDate);
    $("#fecha_ini").datepicker("setDate", myDate);
    $("#fecha_fin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    $("#duracion_dias").val('0');
    $("#dias_a_pagar").val('0');
    $('#dias_disfrute').val('0');
    if ((f.getMonth() + 1) < 10) {
        $('#fechaSolicitud').val(f.getFullYear() + '-' + '0' + (f.getMonth() + 1) + '-' + f.getDate());
    } else {
        $('#fechaSolicitud').val(f.getFullYear() + '-' + (f.getMonth() + 1) + '-' + f.getDate());
    }

}


function calcularDias() {

    var fechaInicial = document.getElementById("fecha_ini").value;
    var fechaFinal = document.getElementById("fecha_fin").value;
    var resultado = "";

    inicial = fechaInicial.split("-");
    final = fechaFinal.split("-");

    var dateStart = new Date(inicial[0], (inicial[1]), inicial[2]);
    var dateEnd = new Date(final[0], (final[1]), final[2]);

    //alert (fechaInicial);
    //alert (fechaFinal);
    if (dateStart < dateEnd) {
        //resultado="La diferencia es de "+(((dateEnd-dateStart)/86400)/1000)+" d�as";
        res = ((dateEnd - dateStart) / 86400) / 1000;
        //$("#duracion_dias").val(res);
        $("#dias_a_pagar").val(res);
    } else {
        resultado = "La fecha inicial es posterior a la fecha final";
        alert(resultado);
    }
    //alert (resultado);
}

function onKeyDecimal(e, num) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if (document.getElementById(num.id).value.indexOf('.') !== -1 && keynum === 46)
        return false;
    if ((keynum === 0 || keynum === 8 || keynum === 48 || keynum === 46)) {
        return true;
    }

    if (keynum >= 58) {
        return false;

    }

    return /\d/.test(String.fromCharCode(keynum));
}





function format(input) {
    var num = input.value.replace(/\./g, '');
    if (!isNaN(num)) {
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/, '');
        input.value = num;
    }
    else {
        //alert('Solo se permiten numeros');
        input.value = input.value.replace(/[^\d\.]*/g, '');
    }
}

function activarCampos() {
    if (tipo_novedad.value === "4") {
        dias_disfrute.disabled = false;
        dias_a_pagar.disabled = false;
        $('#dias_disfrute').val('15');
    } else {
        dias_disfrute.disabled = true;
        dias_a_pagar.disabled = true;
    }
    

}


function limpiar() {
    $("#tipo_novedad").val('');
    $("#razon").val('');
    $("#identificacion").val('');
    $("#nombre_completo").val('');
    $("#fecha_solicitud").val('');
    $("#fecha_ini").val('');
    $("#fecha_fin").val('');
    $("#duracion_dias").val('');
    $("#duracion_horas").val('');
    $("#cod_enfermedad").val('');
    $("#enfermedad").val('');
    $("#jefe_directo").val('');
    $("#descripcion").val('');
    $("#dias_disfrute").val('');
    $("#dias_a_pagar").val('');

}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarTipoIncapacidad() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 97
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#tipo_novedad').html('');
                //$('#tipo_novedad').append("<option value=''>...</option>");
                //$('#tipo_novedad').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                for (var i = 0; i < json.length; i++) {
                    $('#tipo_novedad').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }

            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarNovedades() {
    //
    //alert('Entra aqui');
    var operacion;
    var grid_tabla_ = jQuery("#tabla_Novedades");
    if ($("#gview_tabla_Novedades").length) {
        reloadGridMostrar(grid_tabla_, 107);
    } else {
        //alert('Entra aqui');    
        grid_tabla_.jqGrid({
            caption: "SOLICITUD DE NOVEDADES PENDIENTES POR VISTO BUENO",
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            mtype: "POST",
            datatype: "json",
            height: '650',
            width: '1800',
            colNames: ['Estado','Id','Numero Solicitud', 'Estado','Origen','Razon', 'Id Razon','Tipo Novedad', 'Id Tipo Novedad', 'Fecha Solicitud', 'Identificacion', 'Nombre', 'Fecha Inicial', 'Fecha Final', 'No. Dias', 'Hora Inicial', 'Horas Final', 'No. Horas', 
                'Id Cod Enfermedad', 'Codigo Enfermedad', 'Enfermedad', 'Descripcion', 'Aprobado', 'Comentario', 'Fecha Aprobacion', 'Id Jefe Aprueba', 'Jefe Directo', 'Remunerado', 'Fecha Visto Bueno', 'Usuario Visto Bueno',
                'Observaciones', 'Dias a Disfrutar', 'Dias a Pagar', 'Aplica Recobro', 'Recobro ARL','Recobro EPS','Tramitada','Acciones', 'EPS', 'ARL','NIT EPS', 'NIT ARL','Activar/Inactivar'],
            colModel: [
                {name: 'status', index: 'status', width: 40, sortable: true, align: 'left', hidden: false},
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'numsolicitud', index: 'numsolicitud', width: 80, sortable: true, align: 'left', hidden: false},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: true},
                {name: 'origen', index: 'origen', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'razon', index: 'razon', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'id_razon', index: 'id_razon', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'tipo_novedad', index: 'tipo_novedad', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'id_tipo_novedad', index: 'id_tipo_novedad', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'fecha_solicitud', index: 'fecha_solicitud', width: 80, sortable: true, align: 'left', hidden: false},
                {name: 'identificacion', index: 'identificacion', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'nombre_completo', index: 'nombre_completo', width: 200, sortable: true, align: 'left', hidden: false},
                {name: 'fecha_ini', index: 'fecha_ini', width: 80, sortable: true, align: 'left', hidden: false},
                {name: 'fecha_fin', index: 'fecha_fin', width: 80, sortable: true, align: 'left', hidden: false},
                {name: 'duracion_dias', index: 'duracion_dias', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'hora_ini', index: 'hora_ini', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'hora_fin', index: 'hora_fin', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'duracion_horas', index: 'duracion_horas', width: 100, sortable: true, align: 'left', hidden: true},
                
                {name: 'id_cod_enfermedad', index: 'id_cod_enfermedad', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'cod_enfermedad', index: 'cod_enfermedad', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'enfermedad', index: 'enfermedad', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 300, sortable: true, align: 'left', hidden: false},
                {name: 'aprobado', index: 'aprobado', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'comentario', index: 'comentario', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'fecha_autoriza', index: 'fecha_autoriza', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'id_jefe_directo', index: 'id_jefe_directo', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'jefe_directo', index: 'jefe_directo', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'remunerada', index: 'remunerada', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'fecha_visto_bueno', index: 'fecha_visto_bueno', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'user_visto_bueno', index: 'user_visto_bueno', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'observaciones', index: 'observaciones', width: 300, sortable: true, align: 'left', hidden: false},
                {name: 'dias_disfrute', index: 'dias_disfrute', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'dias_a_pagar', index: 'dias_a_pagar', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'recobro', index: 'recobro', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'valor_recobro_arl', index: 'valor_recobro_arl', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_recobro_eps', index: 'valor_recobro_eps', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 2, prefix: "$ "}},
                {name: 'tramitada', index: 'tramitada', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'listo', index: 'listo', width: 50, sortable: true, align: 'center', hidden: true},
                {name: 'eps', index: 'eps', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'arl', index: 'arl', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'nit_eps', index: 'nit_eps', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'nit_arl', index: 'nit_arl', width: 100, sortable: true, align: 'left', hidden: true},
                {name: 'cambio', index: 'cambio', width: 100, sortable: true, align: 'left', hidden: true}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 107,
                    fecha_inicio: $("#fechaini").val(),
                    fecha_fin: $("#fechafin").val(),
                    tipo_novedad1: $("#tipo_novedad1").val(),
                    identificacion2: $("#identificacion2").val(),
                    remunerada1: $("#remunerada1").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_Novedades").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cl = cant[i];
                    var recobrotramitado = grid_tabla_.getRowData(cl).tramitada;
                    var remunerado = grid_tabla_.getRowData(cl).remunerada;
                    var observaciones = grid_tabla_.getRowData(cl).observaciones;
                    /*if (recobrotramitado === '' && remunerado !== 'P' ) {
                       be = "<img src = '/fintra/images/check.png' type='button' style = 'horizontal-align: middle; height: 24px; width: 24px;vertical-align: middle;' title='Click si fue tramitada' onclick=\"actualizaEstadoTramitada('" + cl + "');\" />";
                       be1 = "<img src = '/fintra/images/no.png' type='button' style = 'horizontal-align: middle; height: 20px; width: 20px;vertical-align: middle;' title='Click si no requiere tr�mite' onclick=\"actualizaEstadoNoTramite('" + cl + "');\" />";
                        jQuery("#tabla_Novedades").jqGrid('setRowData', cant[i], {listo: be + be1});
                    }*/
//                    if (remunerado === 'N') {
//                        be1 = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: 10px;horizontal-align: middle; height: 20px; width: 20px;vertical-align: middle;"onclick = "" title="Rechazado">';
//                        jQuery("#tabla_Novedades").jqGrid('setRowData', cant[i], {status: be1});
//                    } 
                    if (remunerado !== 'P' ) {
                        be1 = '<img src = "/fintra/images/flag_green.gif"style = "margin-left: 10px;horizontal-align: middle; height: 20px; width: 20px;vertical-align: middle;"onclick = "" title="Con Visto Bueno">';
                        jQuery("#tabla_Novedades").jqGrid('setRowData', cant[i], {status: be1});
                    }
                    if (remunerado === 'P' ) {
                        be1 = '<img src = "/fintra/images/flag_orange.gif"style = "margin-left: 10px;horizontal-align: middle; height: 20px; width: 20px;vertical-align: middle;"onclick = "" title="Pendiente Visto Bueno">';
                        jQuery("#tabla_Novedades").jqGrid('setRowData', cant[i], {status: be1});
                    }
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_Novedades"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var numsolicitud = filas.numsolicitud;
                var estado = filas.estado;
                var tipo_novedad = filas.tipo_novedad;
                var id_tipo_novedad = filas.id_tipo_novedad;
                var fecha_solicitud = filas.fecha_solicitud;
                var identificacion = filas.identificacion;
                var nombre_completo = filas.nombre_completo;
                var fecha_ini = filas.fecha_ini;
                var duracion_dias = filas.duracion_dias;
                var fecha_fin = filas.fecha_fin;
                var hora_ini = filas.hora_ini;
                var duracion_horas = filas.duracion_horas;
                var hora_fin = filas.hora_fin;
                var razon = filas.razon;
                var id_razon = filas.id_razon;
                var id_cod_enfermedad = filas.id_cod_enfermedad;
                var cod_enfermedad = filas.cod_enfermedad;
                var enfermedad = filas.enfermedad;
                var descripcion = filas.descripcion;
                var id_jefe_directo = filas.id_jefe_directo;
                var jefe_directo = filas.jefe_directo;
                var dias_disfrute = filas.dias_disfrute;
                var dias_a_pagar = filas.dias_a_pagar;
                var aprobado = filas.aprobado;
                var comentario = filas.comentario;
                var remunerada = filas.remunerada;
                var observaciones = filas.observaciones;
                var recobro = filas.recobro;
                var id_proceso = filas.id_proceso;
                var proceso_actual = filas.proceso_actual;
                var origen = filas.origen;
                var eps = filas.eps;
                var arl = filas.arl;
                var valor_recobro_eps = filas.valor_recobro_eps;
                var valor_recobro_arl = filas.valor_recobro_arl;
                var nit_eps = filas.nit_eps;
                var nit_arl = filas.nit_arl;


              if (remunerada == 'P') {
                ventanaNovedades(operacion, id, estado, tipo_novedad, id_tipo_novedad, fecha_solicitud, identificacion, nombre_completo, fecha_ini, duracion_dias, fecha_fin, hora_ini, duracion_horas, hora_fin,
                        razon, id_razon, id_cod_enfermedad, cod_enfermedad, enfermedad, descripcion, id_jefe_directo, jefe_directo, dias_disfrute, dias_a_pagar, aprobado, comentario, remunerada, 
                        observaciones, recobro,numsolicitud,id_proceso,proceso_actual,origen, eps, arl, valor_recobro_eps, valor_recobro_arl, nit_eps, nit_arl);
                } else  {
                    ventanaNovedadesSinEditar(operacion, id, estado, tipo_novedad, id_tipo_novedad, fecha_solicitud, identificacion, nombre_completo, fecha_ini, duracion_dias, fecha_fin, hora_ini, duracion_horas, hora_fin,
                        razon, id_razon, id_cod_enfermedad, cod_enfermedad, enfermedad, descripcion, id_jefe_directo, jefe_directo, dias_disfrute, dias_a_pagar, aprobado, comentario, remunerada, 
                        observaciones, recobro,numsolicitud,id_proceso,proceso_actual,origen, eps, arl, valor_recobro_eps, valor_recobro_arl, nit_eps, nit_arl);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
//        $("#tabla_Novedades").navButtonAdd('#pager', {
//            caption: "Nuevo",
//            onClickButton: function () {
//                operacion = 'Nuevo';
//                ventanaNovedades(operacion);
//            }
//        });
        $("#tabla_Novedades").navButtonAdd('#pager', {
            caption: "Exportar",
            onClickButton: function () {
                exportarExcel();
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                fecha_inicio: $("#fechaini").val(),
                fecha_fin: $("#fechafin").val(),
                tipo_novedad1: $("#tipo_novedad1").val(),
                identificacion: $("#identificacion").val() 
                //remunerada1: $("#remunerada1").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaNovedades(operacion, id, estado, tipo_novedad, id_tipo_novedad, fecha_solicitud, identificacion, nombre_completo, fecha_ini, duracion_dias, fecha_fin, hora_ini, duracion_horas, hora_fin,
        razon, id_razon, id_cod_enfermedad, cod_enfermedad, enfermedad, descripcion, id_jefe_directo, jefe_directo, dias_disfrute, dias_a_pagar, aprobado, comentario, remunerada, observaciones, 
        recobro,numsolicitud,id_proceso,proceso_actual,origen, eps, arl, valor_recobro_eps, valor_recobro_arl, nit_eps, nit_arl) {
    if (operacion === 'Editar') {
        $("#id").val(id);
        cargarTipoIncapacidad();
        $("#tipo_novedad").val(id_tipo_novedad);
        $("#origen").val(origen);
        cargarRazon(id_tipo_novedad);
        $("#razon").val(id_razon);
        $("#identificacion").val(identificacion);
        cargarNombre(identificacion);
        $("#nombre_completo").val(nombre_completo);
        $("#fechaSolicitud").val(fecha_solicitud);
        $("#fecha_ini").val(fecha_ini);
        $("#fecha_fin").val(fecha_fin);
        $("#duracion_dias").val(duracion_dias);
        $("#hora_ini").val(hora_ini);
        $("#hora_fin").val(hora_fin);
        $("#duracion_horas").val(duracion_horas);
        $("#cod_enfermedad").val(cod_enfermedad);
        cargarEnfermedades(cod_enfermedad);
        $("#enfermedad").val(enfermedad);
        cargarJefeDirecto();
        $("#jefe_directo").val(id_jefe_directo);
        $("#descripcion").val(descripcion);
        $("#dias_disfrute").val(dias_disfrute);
        $("#dias_a_pagar").val(dias_a_pagar);
        $("#aprobado").val(aprobado);
        $("#comentario").val(comentario);
        $("#remunerada").val(remunerada);
        $("#observaciones").val(observaciones);
        $("#recobro").val(recobro);
        $("#numsolicitud").val(numsolicitud);
        //procesoActual();
        //cargarProcesosActuales();
        $("#eps").val(eps);
        $("#arl").val(arl);
        $("#valor_recobro_eps").val(valor_recobro_eps);
        $("#valor_recobro_arl").val(valor_recobro_arl);
        $("#nit_eps").val(nit_eps);
        $("#nit_arl").val(nit_arl);
        $("#dialogNovedades").dialog({
            width: '970',
            height: '700',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'VISTO BUENO DE NOVEDADES',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Ver Archivos": function () {
                    ventanaVerArchivo(identificacion);
                },
                "Aceptar": function () {
                    vistoBuenoSolicitudNovedad();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    limpiar();
                }
            }
        });
    }
}

function ventanaNovedadesSinEditar(operacion, id, estado, tipo_novedad, id_tipo_novedad, fecha_solicitud, identificacion, nombre_completo, fecha_ini, duracion_dias, fecha_fin, hora_ini, duracion_horas, hora_fin,
        razon, id_razon, id_cod_enfermedad, cod_enfermedad, enfermedad, descripcion, id_jefe_directo, jefe_directo, dias_disfrute, dias_a_pagar, aprobado, comentario, remunerada, observaciones, 
        recobro,numsolicitud,id_proceso,proceso_actual,origen, eps, arl, valor_recobro_eps, valor_recobro_arl, nit_eps, nit_arl) {
    if (operacion === 'Editar') {
        $("#id").val(id);
        cargarTipoIncapacidad();
        $("#tipo_novedad").val(id_tipo_novedad);
        $("#origen").val(origen);
        cargarRazon(id_tipo_novedad);
        $("#razon").val(id_razon);
        $("#identificacion").val(identificacion);
        cargarNombre(identificacion);
        $("#nombre_completo").val(nombre_completo);
        $("#fechaSolicitud").val(fecha_solicitud);
        $("#fecha_ini").val(fecha_ini);
        $("#fecha_fin").val(fecha_fin);
        $("#duracion_dias").val(duracion_dias);
        $("#hora_ini").val(hora_ini);
        $("#hora_fin").val(hora_fin);
        $("#duracion_horas").val(duracion_horas);
        $("#cod_enfermedad").val(cod_enfermedad);
        cargarEnfermedades(cod_enfermedad);
        $("#enfermedad").val(enfermedad);
        cargarJefeDirecto();
        $("#jefe_directo").val(id_jefe_directo);
        $("#descripcion").val(descripcion);
        $("#dias_disfrute").val(dias_disfrute);
        $("#dias_a_pagar").val(dias_a_pagar);
        $("#aprobado").val(aprobado);
        $("#comentario").val(comentario);
        $("#remunerada").val(remunerada);
        $("#observaciones").val(observaciones);
        $("#recobro").val(recobro);
        $("#numsolicitud").val(numsolicitud);
        //procesoActual();
        //cargarProcesosActuales();
        $("#eps").val(eps);
        $("#arl").val(arl);
        $("#valor_recobro_eps").val(valor_recobro_eps);
        $("#valor_recobro_arl").val(valor_recobro_arl);
        $("#nit_eps").val(nit_eps);
        $("#nit_arl").val(nit_arl);
        $("#dialogNovedades").dialog({
            width: '970',
            height: '700',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'VISTO BUENO DE NOVEDADES',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Ver Archivos": function () {
                    ventanaVerArchivo(identificacion);
                },
                "Salir": function () {
                    $(this).dialog("close");
                    limpiar();
                }
            }
        });
    }
}

function vistoBuenoSolicitudNovedad() {

    var remunerada = $("#remunerada").val();
    var observaciones = $("#observaciones").val();
    var valor_recobro_eps = $("#valor_recobro_eps").val();
    var valor_recobro_arl = $("#valor_recobro_arl").val();
    var nit_eps = $("#nit_eps").val();
    var nit_arl = $("#nit_arl").val();
    var identificacion = $("#identificacion").val();
    var numsolicitud = $("#numsolicitud").val();
    //var identificacion= $("#identificacion").val();
    //var tipo_novedad= $("#tipo_novedad").val();
    //var jefe_directo= $("#jefe_directo").val();

    if (
//     tipo_novedad!== '' && razon!== '' && identificacion!== '' && fecha_solicitud!== '' && fecha_ini!== '' && fecha_fin!== '' && duracion_dias!== '' && hora_ini!== '' && hora_fin!== '' && duracion_horas!== ''    
//     && cod_enfermedad!== '' && jefe_directo!== '' && descripcion!== '' && dias_disfrute!== '' && dias_a_pagar!== ''
            remunerada !== '' && observaciones !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            data: {
                opcion: 80,
                id: $("#id").val(),
                remunerada: $("#remunerada").val(),
                observaciones: $("#observaciones").val(),
                valor_recobro_eps : $("#valor_recobro_eps").val(),
                valor_recobro_arl : $("#valor_recobro_arl").val(),
                nit_eps : $("#nit_eps").val(),
                nit_arl : $("#nit_arl").val(),
                identificacion : $("#identificacion").val(),
                numsolicitud: $("#numsolicitud").val()
                //identificacion: $("#identificacion").val(),
                //tipo_novedad: $("#tipo_novedad").val(),
                //jefe_directo: $("#jefe_directo").val()
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#dialogNovedades").dialog('close');
                    $("#id").val('');
                    $("#tipo_novedad").val('');
                    $("#razon").val('');
                    $("#identificacion").val('');
                    $("#fecha_solicitud").val('');
                    $("#fecha_ini").val('');
                    $("#fecha_fin").val('');
                    $("#duracion_dias").val('');
                    $("#hora_ini").val('');
                    $("#hora_fin").val('');
                    $("#duracion_horas").val('');
                    $("#cod_enfermedad").val('');
                    $("#jefe_directo").val('');
                    $("#descripcion").val('');
                    $("#dias_disfrute").val('');
                    $("#dias_a_pagar").val('');
                    $("#aprobado").val('');
                    $("#comentario").val('');
                    $("#remunerada").val('');
                    $("#observaciones").val('');
                    cargarNovedades();
                } else {
                    mensajesDelSistema('Lo sentimos no se pudo visar la solicitud', '300', 'auto', false);
                }
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function CambiarEstadoNovedades(rowid) {
    var grid_tabla = jQuery("#tabla_Novedades");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        data: {
            opcion: 76,
            id: id
        },
        success: function (data) {
            cargarNovedades();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}


function cargarEnfermedades(codigo) {

    codigo = $('#cod_enfermedad').val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'text',
        async: false,
        data: {
            opcion: 70,
            cod_enfermedad: codigo
        },
        success: function (json) {
            console.log(json);
            if (json.error) {
                console.log(json.error);
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                //alert(json)
                console.log(json);
                if (json.trim() == '') {
                    //  alert('El codigo no es valido o est� vac�o. Si no aplica digitar codigo: 0');
                }
                $('#enfermedad').val(json);
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

//function cargarAviso(codigo){
//    codigo = $('#cod_enfermedad').val();
//    if (codigo == ''){
//        alert('Digite el codigo de la enfermedad. Si no aplica digitar codigo: 0')
//        
//    }
//    
//}

function cargarRazon(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 71,
            tipo_novedad: id

        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#razon').html('');
                $('#razon').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#razon').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');

                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarNombre(identificacion) {

    identificacion = $('#identificacion').val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'text',
        async: false,
        data: {
            opcion: 72,
            identificacion: numberSinPuntos($("#identificacion").val()),
        },
        success: function (json) {
            console.log(json);
            if (json.error) {
                console.log(json.error);
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                //alert(identificacion);
                console.log(json);
                if (json.trim() == '') {
                    alert('El documento no es valido o est� vac�o. Favor verificar');
                }
                $('#nombre_completo').val(json);
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarJefeDirecto() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 73
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#jefe_directo').html('');
                $('#jefe_directo').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#jefe_directo').append('<option value=' + json[i].idusuario + '>' + json[i].idusuario + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ventanaVerArchivo(identificacion) {
    $("#identificacion").val(identificacion);
    obtenerArchivoCargado();
    $("#verarchivo").dialog({
        width: 500,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: true,
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function obtenerArchivoCargado() {
    limpiarArchivos();
    var identificacion = $('#identificacion').val();

    $.ajax({
        type: "POST",
        dataType: "json",
        data: {opcion: 82,
            identificacion1: identificacion},
        async: false,
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        success: function (jsonData) {
            if (!isEmptyJSON(jsonData)) {
                $('#tbl_archivos_cargados').html('');
                for (var i = 0; i < jsonData.length; i++) {
                    var nomarchivo = jsonData[i];
                    $('#tbl_archivos_cargados').append("<tr class='fila'><td><a target='_blank' onClick=\"consultarNomarchivo('" + identificacion + "','" + nomarchivo + "');\n\
                       \" style='cursor:hand' ><strong>" + nomarchivo + "</strong></a> &nbsp;&nbsp;&nbsp;<a id='view_file' target='_blank' href='#' style='display:none'>Ver</a></td></tr>");
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function consultarNomarchivo(identificacion, nomarchivo) {
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {opcion: 83,
            identificacion1: identificacion,
            nomarchivo: nomarchivo},
        async: false,
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        success: function (jsonData) {
            if (!isEmptyJSON(jsonData)) {

                if (jsonData.error) {
                    mensajesDelSistema(jsonData.error, '270', '165');
                    return;
                }
                if (jsonData.respuesta === "SI") {
                    $('#view_file').attr("href", "/fintra/images/multiservicios/" + jsonData.login + "/" + nomarchivo);
                    $('#view_file').fadeIn();
                } else {
                    mensajesDelSistema(".::ERROR AL OBTENER ARCHIVO::.", '250', '150');
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}


//function generarPDFrecobro(rowid) {
//    var grid_tabla = jQuery("#tabla_Novedades");
//    var identificacion = grid_tabla.getRowData(rowid).identificacion;
//    var valor_recobro =  grid_tabla.getRowData(rowid).valor_recobro_arl;
//
//    $.ajax({
//        type: "POST",
//        datatype: 'json',
//        url: "./controller?estado=AdmonRecursos&accion=Humanos",
//        data: {
//            opcion: 84,
//            identificacion: identificacion,
//            valor_recobro: valor_recobro
//        },
//        success: function (json) {
//            //alert (json.respuesta);
//            if (json.respuesta === "GENERADO") {
//                mensajesDelSistema("Exito en la generacion de pdf, por favor ver en el log", '335', '150', true);
//            }
//        }
//    });
//}


function actualizaEstadoTramitada(rowid) {
    var grid_tabla = jQuery("#tabla_Novedades");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        data: {
            opcion: 84,
            id: id
            
        },
        success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#id").val('');
                    cargarNovedades();
                } else {
                    mensajesDelSistema('Lo sentimos no se pudo dar tr�mite', '300', 'auto', false);
                }
                
            }, error: function (result) {
                alert('ERROR ');
            }
    });
}


function calcularDiasF() {
    var fecha_ini = $("#fecha_ini").val();
    //var fecha_fin = $("#fecha_fin").val();
    var duracion_dias = $("#duracion_dias").val();
     $.ajax({
            type: "POST",
            dataType: "json",
            data: {opcion:85,
                fecha_ini: fecha_ini,
                duracion_dias: duracion_dias},
            async: false,
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            success: function (jsonData) {
                console.log(jsonData.respuesta);
            try {        

                $('#fecha_fin').val(jsonData.respuesta);
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }

function actualizaEstadoNoTramite(rowid) {
    var grid_tabla = jQuery("#tabla_Novedades");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        data: {
            opcion: 86,
            id: id
            
        },
        success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#id").val('');
                    cargarNovedades();
                } else {
                    mensajesDelSistema('Lo sentimos no se pudo dar tr�mite', '300', 'auto', false);
                }
                
            }, error: function (result) {
                alert('ERROR ');
            }
    });
}


function procesoActual() {
   
    $.ajax({
        type: "POST",
        dataType: "json",
        async: false,
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        data: {
            opcion: 104,
            identificacion: $("#identificacion").val(),
        },
        success: function (jsonData) {
            console.log(jsonData.respuesta);
            try {
                $('#proceso_actual').val(jsonData[0].descripcion);
                $('#id_proceso_actual').val(jsonData[0].id_proceso_interno);
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarProcesosActuales() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 105
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                console.log(json.length);
                $('#proceso_nuevo').html('');
                $('#proceso_nuevo').append("<option value=''>...</option>");
                for (var i = 0; i < json.length; i++) {
                    $('#proceso_nuevo').append('<option value=' + json[i].id + '>' + json[i].descripcion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function  exportarExcel() {
//    var fullData = $("#tabla_Empleados").jqGrid('getRowData');
//    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 240,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        data: {
//            listado: myJsonString,
            fecha_inicio: $("#fechaini").val(),
            fecha_fin: $("#fechafin").val(),
            tipo_novedad1: $("#tipo_novedad1").val(),
            identificacion2: $("#identificacion2").val(),
            opcion: 149
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function limpiarArchivos() {
    var d = document.getElementById("tbl_archivos_cargados");
    while (d.hasChildNodes())
    d.removeChild(d.firstChild);
}
