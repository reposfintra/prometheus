/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function inicio1() {
    cargarNivelesJerarquicos();
}


function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarNivelesJerarquicos() {
    var operacion;
    var grid_tabla_ = jQuery("#tabla_NivelesJerarquicos");
    if ($("#gview_tabla_NivelesJerarquicos").length) {
        reloadGridMostrar(grid_tabla_, 48);
    } else {
        grid_tabla_.jqGrid({
            
            caption: "Niveles Jerarquicos",
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '550',
            colNames: ['Id', 'Estado', 'Descripcion','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'descripcion', index: 'descripcion', width: 320, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'cambio', index: 'cambio', width: 100, sortable: true, align: 'left', hidden: false}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 48
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_NivelesJerarquicos").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_NivelesJerarquicos").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoNivelesJerarquicos('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_NivelesJerarquicos").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_NivelesJerarquicos"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var descripcion = filas.descripcion;
                if (estado === 'Activo') {
                    ventanaNivelesJerarquicos(operacion, id, descripcion);
                } else if (estado === 'Inactivo') {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_NivelesJerarquicos").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaNivelesJerarquicos(operacion);
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function ventanaNivelesJerarquicos(operacion, id, descripcion) {
    if (operacion === 'Nuevo') {
        $("#dialogNivelesJerarquicos").dialog({
            width: '430',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'NIVELES JERARQUICOS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarNivelesJerarquicos();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#descripcion").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        $("#descripcion").val(descripcion);
        $("#dialogNivelesJerarquicos").dialog({
            width: '430',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'NIVELES JERARQUICOS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarNivelesJerarquicos();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#descripcion").val('');
                }
            }
        });
    }
}

function guardarNivelesJerarquicos() {
    var desc = $("#descripcion").val();
    //alert(desc);
    if (desc !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            data: {
                opcion: 47,
                descripcion: $("#descripcion").val(),
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                    $("#dialogNivelesJerarquicos").dialog('close');
                    $("#descripcion").val('');
                }
                cargarNivelesJerarquicos();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function ActualizarNivelesJerarquicos() {
    var desc = $("#descripcion").val();
    if (desc !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=AdmonRecursos&accion=Humanos",
            data: {
                opcion: 48,
                id: $("#id").val(),
                descripcion: $("#descripcion").val(),
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#dialogNivelesJerarquicos").dialog('close');
                    $("#descripcion").val('');
                }
                cargarNivelesJerarquicos();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function CambiarEstadoNivelesJerarquicos(rowid) {
    var grid_tabla = jQuery("#tabla_NivelesJerarquicos");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=AdmonRecursos&accion=Humanos",
        data: {
            opcion: 49,
            id: id
        },
        success: function (data) {
            cargarNivelesJerarquicos();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}