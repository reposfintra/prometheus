
var id_ciudad;
var id_tasa;
var cuota_inicio;
var id_compra_cartera;
var id_saldo_vencido;
var id_porcentaje;
var aplica_incial;
var negocio;
var dias_mora;
var total_a_pagar;
var periodo_pago_inicial;
$(document).ready(function () {
    $('#buscar').click(function () {
        buscar_negocio();
    });

    comboEstrategiaCartera();

    let combo = document.getElementById("cuota");
    combo.options.add(new Option('...', ''));
    for (let i = 1; i <= 60; i++) {
        combo.options.add(new Option(i, i));
    }

    let fecha = new Date();
    let fullfecha;
    fecha.setDate(fecha.getDate());
    fullfecha = fecha.getFullYear() + "-" + ('0' + (fecha.getMonth() + 1)).slice(-2) + "-" + ('0' + fecha.getDate()).slice(-2);
    $('#select_fecha').val(fullfecha);
    
    $('.input-number').keyup(function (){
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    
});

function buscar_negocio() {
    $("#div_detalle_cartera").hide();
    var grid_tabla = $("#tabla_saldo_financiar");
    if ($("#gview_tabla_saldo_financiar").length) {
        reloadGridTabla(grid_tabla, 19);
    } else {

        grid_tabla.jqGrid({

            caption: "Negocios Refinanciar",
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: 'auto',
            width: '840',
            colNames: ['Identificacion', 'Nombre', 'Negocio', 'Vr Negocio', 'Cuotas', 'Agencia', 'Valor cuota', 'Dias mora', 'Periodicidad','Aplica Inicial',"Aplica Proyeccion","Fecha Proyeccion", 'Seleccionar'],
            colModel: [
                {name: 'indentificacion', index: 'indentificacion', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'nombre', index: 'nombre', width: 220, sortable: true, align: 'center', hidden: false},
                {name: 'negocio', index: 'negocio', width: 65, sortable: true, align: 'center', hidden: false},
                {name: 'valor_negocio', index: 'valor_negocio', width: 70, sortable: true, align: 'center', hidden: false,
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total_cuotas', index: 'total_cuotas', width: 40, sortable: true, align: 'center', hidden: false},
                {name: 'agencia', index: 'agencia', width: 50, sortable: true, align: 'center', hidden: false},
                {name: 'valor_cuota', index: 'valor_cuota', width: 70, sortable: true, align: 'center', hidden: false,
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'dias_mora', index: 'dias_mora', width: 50, sortable: true, align: 'center', hidden: false},
                {name: 'periodicidad', index: 'periodicidad', width: 90, sortable: true, align: 'center', hidden: true},
                {name: 'aplica_inicial', index: 'aplica_inicial', width: 90, sortable: true, align: 'center', hidden: true},
                {name: 'aplica_proyeccion', index: 'aplica_proyeccion', width: 90, sortable: true, align: 'center', hidden: true},
                {name: 'fecha_proyeccion', index: 'fecha_proyeccion', width: 90, sortable: true, align: 'center', hidden: true},
                {name: 'select', index: 'select', width: 100, sortable: true, align: 'center', hidden: false, search: false}
            ],
            rowNum: 10000,
            rowTotal: 10000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            restoreAfterError: true,
            pager: '#pager',
            pgtext: null,
            pgbuttons: false,
            multiselect: false,

            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "2"

            },
            loadComplete: function () {

                if (grid_tabla.jqGrid('getGridParam', 'records') <= 0) {

                    mensajesDelSistema("No se encontraron resultados. Por favor verifique la estrategia seleccionada", '250', '150');
                }
            },
            ajaxGridOptions: {

                data: {
                    opcion: 19,
                    tipo_busqueda: $("#tipo_busqueda").val(),
                    documento: $("#documento").val(),
                    tipo_refi: $('#estrategia_cartera').val()

                }

            },
            gridComplete: function () {
                var colSumTotal = jQuery("#tabla_saldo_financiar").jqGrid('getCol', 'vlr_neto', false, 'sum');
                var colSumTotalcapital = jQuery("#tabla_saldo_financiar").jqGrid('getCol', 'capital', false, 'sum');
                $("#tabla_saldo_financiar").jqGrid('footerData', 'set', {valor_negocio: colSumTotal, saldo_cartera: colSumTotalcapital});

                var cant = $("#tabla_saldo_financiar").jqGrid('getDataIDs');

                for (let i = 0; i < cant.length; i++) {
                    let negocios = $("#tabla_saldo_financiar").getRowData(cant[i]).negocio;
                    let periodicidad = $("#tabla_saldo_financiar").getRowData(cant[i]).periodicidad;
                    let diasMora = $("#tabla_saldo_financiar").getRowData(cant[i]).dias_mora;
                    let aplica_inicial = $("#tabla_saldo_financiar").getRowData(cant[i]).aplica_inicial;
                    let aplica_proyeccion = $("#tabla_saldo_financiar").getRowData(cant[i]).aplica_proyeccion;
                    let fecha_proyeccion = $("#tabla_saldo_financiar").getRowData(cant[i]).fecha_proyeccion;
                    let valor_cuota = $("#tabla_saldo_financiar").getRowData(cant[i]).valor_cuota;

                    if (periodicidad === "true") {
                        negocio=negocios;
                        dias_mora=diasMora;
                        let btn = "<button class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' style='font-size: 14px; padding: 5px; margin: 4px;' onclick=\"Modalfecha('" + negocios + "','" + diasMora + "'," + aplica_inicial + "," + aplica_proyeccion + ",'" + fecha_proyeccion + "','" + valor_cuota + "');\">Seleccionar</button>"
                        $("#tabla_saldo_financiar").jqGrid('setRowData', cant[i], {select: btn});
                    }
                }

            },

            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }
        });

    }

}


function reloadGridTabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",

        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                tipo_busqueda: $("#tipo_busqueda").val(),
                documento: $("#documento").val(),
                tipo_refi: $('#estrategia_cartera').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");

}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {

                $(this).dialog("close");
            }
        }
    });

}

function ver_detalle_refinanciacion_cartera() {
    let negocio = $("#cod_negocio").val();
    $('#cuota')[0].selectedIndex = 0;
    buscar_fecha_pago(negocio, cargar_cartera);
}


function cargar_cartera(error, msj) {

    if (error) {
        return mensajesDelSistema(msj, 250, 150);
    }

    getTotalPagar();

    $("#div_detalle_refinanciacion_tb").dialog({
        modal: true,
        width: '850',
        height: '600',
        show: "scale",
        hide: "scale",
        resizable: true,
        title: 'Refinanciar',
        closable: false,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {//crear bot�n de cerrar
            "Solicitar Aprobaci�n": function () {
                agregarObservacion();
            },
            "Salir": function () {
                cargarDetalleCartera(negocio, dias_mora); 
                $("#tabla_detalle_refinanciacion").jqGrid("clearGridData", true).trigger("reloadGrid");
                $(this).dialog("destroy");
            }
        }
    });

}

function cargarDetalleCartera(negocio, diasMora) {

    $("#cod_negocio").val(negocio);
    $("#tipo_refi").val($('#estrategia_cartera').val());
    $('#div_detalle_cartera').fadeIn();
    var gripdetalle = $("#tabla_detalle_cartera");

    if ($("#gview_tabla_detalle_cartera").length) {
        reloadtabla_saldo_financiar(gripdetalle, negocio, 20, $("#tipo_refi").val(), $("#select_fecha").val());
    } else {
        gripdetalle.jqGrid({
            caption: 'Detalle Cartera',
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '920',
            colNames: ['Documento', 'Cuota', 'Vencimiento', 'mora', 'Esquema', 'Estado', 'Vlr capital', 'Vlr interes',
                'Vlr CAT', 'Cuota admon', 'Valor seguro', 'Vr aval', 'Vr int aval', 'Valor cuota', 'IxM', 'GaC', 'Total saldos'],
            colModel: [

                {name: 'documento', index: 'documento', width: 65, resizable: false, sortable: true, align: 'center'},
                {name: 'cuota', index: 'cuota', width: 30, resizable: false, sortable: true, align: 'center'},
                {name: 'fecha_vencimiento', index: 'fecha_vencimiento', resizable: false, sortable: true, width: 70, align: 'center'},
                {name: 'dias_mora', index: 'dias_mora', resizable: false, sortable: true, width: 30, align: 'center'},
                {name: 'esquema', index: 'esquema', resizable: false, sortable: true, width: 80, align: 'center', hidden: true},
                {name: 'estado', index: 'estado', resizable: false, sortable: true, width: 60, align: 'center'},
                {name: 'valor_saldo_capital', index: 'valor_saldo_capital', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_saldo_mi', index: 'valor_saldo_mi', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_saldo_ca', index: 'valor_saldo_ca', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_cm', index: 'valor_cm', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_seguro', index: 'valor_seguro', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_capital_aval', index: 'valor_capital_aval', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',hidden :true,
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_interes_aval', index: 'valor_interes_aval', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',hidden :true,
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_saldo_cuota', index: 'valor_saldo_cuota', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'IxM', index: 'IxM', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'GaC', index: 'GaC', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'suma_saldos', index: 'suma_saldos', summaryType: 'sum', sortable: true, width: 70, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            restoreAfterError: true,
            pager: '#page_tabla_saldo_financiar',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            gridComplete: function () {
                var rows = $("#tabla_detalle_cartera").getDataIDs();
                let valor_saldo_ca_vencido = 0;
                let valor = 0;
                for (var i = 0; i < rows.length; i++)
                {
                   $("#tabla_detalle_cartera").setCell (rows[i],"valor_saldo_cuota",'',{ 'background-color':'#fff6cd'});
                   $("#tabla_detalle_cartera").setCell (rows[i],"suma_saldos",'',{ 'background-color':'#fff6cd'});
                   var status = $("#tabla_detalle_cartera").getCell(rows[i], "estado");
                    if (status === "VENCIDO" || status === "CORRIENTE")
                    {
                        valor = $("#tabla_detalle_cartera").getCell(rows[i], "valor_saldo_ca");
                        valor_saldo_ca_vencido = (parseInt(valor_saldo_ca_vencido) + parseInt(valor));
                    }
                }
                
                let valor_saldo_capital = jQuery("#tabla_detalle_cartera").jqGrid('getCol', 'valor_saldo_capital', false, 'sum');
                let valor_saldo_mi = jQuery("#tabla_detalle_cartera").jqGrid('getCol', 'valor_saldo_mi', false, 'sum');
                let valor_saldo_ca = jQuery("#tabla_detalle_cartera").jqGrid('getCol', 'valor_saldo_ca', false, 'sum');
                let valor_cm = jQuery("#tabla_detalle_cartera").jqGrid('getCol', 'valor_cm', false, 'sum');
                let valor_seguro = jQuery("#tabla_detalle_cartera").jqGrid('getCol', 'valor_seguro', false, 'sum');
                let valor_capital_aval = jQuery("#tabla_detalle_cartera").jqGrid('getCol', 'valor_capital_aval', false, 'sum');
                let valor_interes_aval = jQuery("#tabla_detalle_cartera").jqGrid('getCol', 'valor_interes_aval', false, 'sum');
                let valor_saldo_cuota = jQuery("#tabla_detalle_cartera").jqGrid('getCol', 'valor_saldo_cuota', false, 'sum');
                let valor_saldo_ixm = jQuery("#tabla_detalle_cartera").jqGrid('getCol', 'IxM', false, 'sum');
                let valor_saldo_gac = jQuery("#tabla_detalle_cartera").jqGrid('getCol', 'GaC', false, 'sum');
                let total_saldo = jQuery("#tabla_detalle_cartera").jqGrid('getCol', 'suma_saldos', false, 'sum');
               
                
                $("#tabla_detalle_cartera").jqGrid('footerData', 'set', 
                {
                 valor_saldo_capital: valor_saldo_capital,
                 valor_saldo_mi:valor_saldo_mi,
                 valor_saldo_ca:valor_saldo_ca,
                 valor_cm:valor_cm,
                 valor_seguro:valor_seguro,
                 valor_capital_aval:valor_capital_aval,
                 valor_interes_aval:valor_interes_aval,
                 IxM:valor_saldo_ixm,
                 GaC:valor_saldo_gac,                 
                 suma_saldos:total_saldo                 
                });

                //refinanciacion.
                $("#dias_vencidos").val(diasMora);
                $("#id_capital_tb").text(numberConComas(valor_saldo_capital));               
                $("#id_interes_tb").text(numberConComas(valor_saldo_mi));
                $("#id_cat_tb").text(numberConComas(valor_saldo_ca));
                $("#id_cat_vencido_tb").text(numberConComas(valor_saldo_ca_vencido));
                $("#id_cuota_admon_tb").text(numberConComas(valor_cm));
                $("#id_capital_aval_tb").text(numberConComas(valor_capital_aval));
                $("#id_int_aval_tb").text(numberConComas(valor_interes_aval));
                $("#id_gac_tb").text(numberConComas(valor_saldo_gac));
                $("#id_ixm_tb").text(numberConComas(valor_saldo_ixm)); 
                
            },
                
            ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 20,
                    negocio: negocio,
                    tipo_refi: $("#tipo_refi").val(),
                    fecha: $("#select_fecha").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_saldo_financiar", {search: false, refresh: false, edit: false, add: false, del: false});
//       

    }

}

function ver_cartera_refinanciar() {

    var gripDetalle = $("#tabla_detalle_refinanciacion");

    if ($("#gview_tabla_detalle_refinanciacion").length) {
        reloadtabla_detalle_refinanciacion(gripDetalle);
    } else {
        gripDetalle.jqGrid({
            caption: 'Detalle Cartera',
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '260',
            width: '760',
            colNames: ['Cuota', 'Vencimiento', 'Saldo Inicial', 'Capital', 'Interes', 'Cat', 'Cuota Admin','Seguro','K Aval', 'I Aval', 'Vlr cuota','Saldo Final', 'Dias'],
            colModel: [

                {name: 'item', index: 'item', width: 30, resizable: false, sortable: true, align: 'center'},
                {name: 'fecha', index: 'fecha', resizable: false, sortable: true, width: 80, align: 'center'},
                {name: 'saldo_inicial', index: 'saldo_inicial', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'capital', index: 'capital', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes', index: 'interes', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cat', index: 'cat', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cuota_manejo', index: 'cuota_manejo', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'seguro', index: 'seguro', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'capital_aval', index: 'valor_aval', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes_aval', index: 'valor_aval', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                 {name: 'valor_cuota', index: 'valor_cuota', summaryType: 'sum', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'saldo_final', index: 'saldo_final', sortable: true, width: 65, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},               
                {name: 'dias_vencidos', index: 'dias_vencidos', width: 30, resizable: false, sortable: true, align: 'center', hidden: true}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            restoreAfterError: true,
            pager: '#page_tabla_detalle_refinanciacion',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            gridComplete: function () {
                let capital = $("#tabla_detalle_refinanciacion").jqGrid('getCol', 'capital', false, 'sum');
                let interes = $("#tabla_detalle_refinanciacion").jqGrid('getCol', 'interes', false, 'sum');
                let cat = $("#tabla_detalle_refinanciacion").jqGrid('getCol', 'cat', false, 'sum');
                let seguro = $("#tabla_detalle_refinanciacion").jqGrid('getCol', 'seguro', false, 'sum');
                let cuota_manejo = $("#tabla_detalle_refinanciacion").jqGrid('getCol', 'cuota_manejo', false, 'sum');
                let capital_aval = $("#tabla_detalle_refinanciacion").jqGrid('getCol', 'capital_aval', false, 'sum');
                let interes_aval = $("#tabla_detalle_refinanciacion").jqGrid('getCol', 'interes_aval', false, 'sum');
                let valor_cuota = $("#tabla_detalle_refinanciacion").jqGrid('getCol', 'valor_cuota', false, 'sum');
                
                var rows = $("#tabla_detalle_refinanciacion").getDataIDs();
                for (var i = 0; i < rows.length; i++)
                {
                  $("#tabla_detalle_refinanciacion").setCell (rows[i],"valor_cuota",'',{ 'background-color':'#fff6cd'});
                }   
                
                $("#tabla_detalle_refinanciacion").jqGrid('footerData', 'set', 
                {
                 capital: capital,
                 interes:interes,
                 cat:cat,               
                 cuota_manejo:cuota_manejo,     
                 seguro:seguro,
                 capital_aval:capital_aval,                
                 interes_aval:interes_aval,               
                 valor_cuota:valor_cuota                
                });

            },

            ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 21,
                    saldo_capital: $("#id_capital_tb").text().replace(/[$,]/g, ""),
                    cuota: $("#cuota").val(),
                    fecha: $("#id_fecha").val(),
                    ciudad: id_ciudad,
                    tasa: id_tasa,
                    saldo_cat: $("#id_cat_tb").text().replace(/[$,]/g, ""),
                    tipo_refi: $("#tipo_refi").val(),
                    cuota_inicio: cuota_inicio,
                    compra_cartera:id_compra_cartera
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_detalle_refinanciacion", {search: false, refresh: false, edit: false, add: false, del: false});

    }

}


function reloadtabla_saldo_financiar(grid_tabla, negocio, op, tipo, fecha) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",

        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                negocio: negocio,
                tipo_refi: tipo,
                fecha: fecha
            }
        }
    });
    grid_tabla.trigger("reloadGrid");

}

function reloadtabla_detalle_refinanciacion(grid_tabla) {

    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",
        height: '350',
        width: '920',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 21,
                    saldo_capital: $("#id_capital_tb").text().replace(/[$,]/g, ""),
                    cuota: $("#cuota").val(),
                    fecha: $("#id_fecha").val(),
                    ciudad: id_ciudad,
                    tasa: id_tasa,
                    saldo_cat: $("#id_cat_tb").text().replace(/[$,]/g, ""),
                    tipo_refi: $("#tipo_refi").val(),
                    cuota_inicio: cuota_inicio,
                    compra_cartera: id_compra_cartera
            }
        }
    });
    grid_tabla.trigger("reloadGrid");

}

function refinanciar() {
    
    if($("#id_observaciones").val()===''){
        mensajesDelSistema("La Observaci�n de la negociaci�n es obligatoria.", '250', '150');
        return;
    }
    if($("#celular").val()===''){
         mensajesDelSistema("El nuemero de celular del cliente es obligatorio.", '250', '150');
         return;
    }else if($("#celular").val().length !==10){
         mensajesDelSistema("El nuemero de celular no es valido", '250', '150');
         return;
    }
    
    loading("Espere un momento por favor...", "270", "140");
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        data: {
            opcion: 22,
            negocio: $("#cod_negocio").val(),
            tipo_refi: $("#tipo_refi").val(),
            fecha_primera_cuota: $("#id_fecha").val() === null ? "" : $("#id_fecha").val(),
            cuota: $("#cuota").val(),
            fecha_proyeccion: $("#select_fecha").val(),
            cuota_inicio: cuota_inicio,
            ciudad: id_ciudad,
            compra_cartera: id_compra_cartera,
            porcentaje: id_porcentaje,
            ispago: true,
            interes: $("#id_interes_tb").text().replace(/[$,]/g, ""),
            cat_vencido:$("#id_cat_vencido_tb").text().replace(/[$,]/g, ""),
            cuota_admin:$("#id_cuota_admon_tb").text().replace(/[$,]/g, ""),
            intxmora: $("#id_ixm_tb").text().replace(/[$,]/g, ""),
            gasto_cobranza: $("#id_gac_tb").text().replace(/[$,]/g, ""),
            pago_inicial:$("#total_pagar").text().replace(/[$,]/g, ""),
            capital_refin: $("#id_capital_tb").text().replace(/[$,]/g, ""),
            saldo_cat:$("#id_cat_tb").text().replace(/[$,]/g, ""),
            tot_a_pagar:total_a_pagar,
            periodo_pago:periodo_pago_inicial,
            observacion:$("#id_observaciones").val(),
            celular: $("#celular").val()
        },
        success: function (json) {

            if (json.success === true) {
                $("#dialogLoading").dialog('close');
                mensajesDelSistema(json.data, '250', '150');              
            } else {
                $("#dialogLoading").dialog('close');
                mensajesDelSistema(json.data, '320', '170');                
            }
            $("#div_detalle_refinanciacion_tb").dialog("destroy");
            $("#div_obsevaciones").dialog("destroy");
            $("#tabla_detalle_refinanciacion").jqGrid("clearGridData", true).trigger("reloadGrid");

        },
        error: function (xhr) {
            $("#dialogLoading").dialog('close');
            mensajesDelSistema("Error al refinanciar " + "\n" +
                    xhr.responseText, '650', '250', true);
        }
    });


}
function numberConComas(x) {
    return "$" + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}
function buscar_fecha_pago(negocio, callback) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        data: {
            opcion: 23,
            negocio: negocio,
            tipo_refi: $("#tipo_refi").val(),
            fecha: $("#select_fecha").val()
        },
        success: function (json) {

            if (json.success) {
                id_tasa=json.tasa;
                cuota_inicio=json.cuota_inicio;
                id_ciudad=json.ciudad;
                id_compra_cartera=json.compra_cartera;
                id_saldo_vencido=numberConComas(redondeo(json.saldo_vencido, 0));
                carcularFecha(json.fecha_pago);
                callback(false);
            } else {
                mensajesDelSistema(json.data, '250', '150');
                callback(true, "Error")
            }
        },
        error: function (xhr) {
            mensajesDelSistema("Error al refinanciar " + "\n" +
                    xhr.responseText, '650', '250', true);
        }
    });

}
function redondeo(num, dec) {
    return parseFloat(num).toFixed(dec);
}

function Modalfecha(negocio, diasMora, aplica_inicial,aplica_proyeccion,fecha_proyeccion,valor_cuota) {

    let fecha = new Date();
    let fullfecha;
    fecha.setDate(fecha.getDate());
    fullfecha = fecha.getFullYear() + "-" + ('0' + (fecha.getMonth() + 1)).slice(-2) + "-" + ('0' + fecha.getDate()).slice(-2);
    
    if (aplica_proyeccion){
        $('#select_fecha').val(fecha_proyeccion);
    }else{
        $('#select_fecha').val(fullfecha);
    }
    $("#id_vlr_cuota_actual").text(numberConComas(valor_cuota));
    if (aplica_inicial) {
        $("#div_fecha").dialog({
            modal: true,
            width: '360',
            height: '180',
            show: "scale",
            hide: "scale",
            resizable: false,
            title: 'Fecha',
            closable: true,
            closeOnEscape: true,
            buttons: {//crear bot�n de cerrar
                "Aceptar": function () {
                    if ($('#select_fecha').val() < fullfecha) {
                        mensajesDelSistema("La fecha seleccionada debe ser mayor o igual a: " + fullfecha, '250', '150');
                    } else {
                        cargarDetalleCartera(negocio, diasMora);
                        $(this).dialog("destroy");
                    }
                },
                "Salir": function () {

                    $(this).dialog("destroy");

                }

            }
        });
    }else{
        cargarDetalleCartera(negocio, diasMora);        
    }

}

function carcularFecha(inputDate) {

    $('#id_fecha').empty();

    var aux = replaceAll(inputDate, "-", "/");
    var date = new Date(aux);
    date.setDate(date.getDate());

    var days = date.getDate();
    var fecha = "0099-01-01";
    var mes = date.getMonth() + 1;
    var anio = date.getFullYear();

    if (days >= 1 && days <= 2) {
                    
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-02";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-12";
                    opcion1 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-17";
                    opcion2 = new Option(fecha, fecha);
                    //fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-22";
                    //opcion3 = new Option(fecha, fecha);
          
                 }
              
                if (days > 2 && days <= 12) {

                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-12";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-17";
                    opcion1 = new Option(fecha, fecha);
                    //fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-22";
                    //opcion2 = new Option(fecha, fecha);
                    
                    if(mes === 12){
                        
                        fecha = (anio +1) + "-" +"01" + "-02";
                        opcion2 = new Option(fecha, fecha);
                        
                    }else{
                        
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
                    opcion2 = new Option(fecha, fecha);
                   
                    }
           
                }

                if (days > 12 && days <= 17) {

                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-17";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    //fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-22";
                    //opcion1 = new Option(fecha, fecha);
                    
                     if(mes === 12){
                        
                        fecha = (anio +1) + "-" +"01" + "-02";
                        opcion1 = new Option(fecha, fecha);
                        fecha = (anio +1) + "-" +"01" + "-12";
                        opcion2 = new Option(fecha, fecha);
                        
                    }else{
                    
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
                    opcion1 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
                    opcion2 = new Option(fecha, fecha);
                    
                    }

                }

                if (days > 17 && days <= 22) {

                    //fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-22";
                    //opcion0 = new Option(fecha, fecha, "defauldSelected");
                    
                    if(mes === 12){
                        
                    fecha = (anio +1) + "-" + "01" + "-02";
                    opcion0 = new Option(fecha, fecha);
                    fecha = (anio +1) + "-" +  "01" + "-12";
                    opcion1 = new Option(fecha, fecha);
                    fecha = (anio +1) + "-" +  "01" + "-17";
                    opcion2 = new Option(fecha, fecha);
                    
                    }else{
                    
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
                    opcion0 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
                    opcion1 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
                    opcion2 = new Option(fecha, fecha);
                    
                    }



                }


                if (days > 22 && days <= 31) {
                    
                    
                   if(mes === 12){
                       
                    fecha = (anio +1) + "-" + "01" + "-02";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    fecha = (anio +1) + "-" + "01"  + "-12";
                    opcion1 = new Option(fecha, fecha);
                    fecha = (anio +1) + "-" + "01"  + "-17";
                    opcion2 = new Option(fecha, fecha);
                    //fecha = (anio +1) + "-" + "01"  + "-22";
                    //opcion3 = new Option(fecha, fecha);
                           
                           
                   }else{

                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
                    opcion1 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
                    opcion2 = new Option(fecha, fecha);
                    //fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
                    //opcion3 = new Option(fecha, fecha);

                   }
       
                }

    var combo = document.getElementById("id_fecha");
    combo.options.add(opcion0);
    combo.options.add(opcion1);
    combo.options.add(opcion2);
//    combo.options.add(opcion3);
}
function replaceAll(text, busca, reemplaza) {
    while (text.toString().indexOf(busca) !== - 1)
        text = text.toString().replace(busca, reemplaza);
    return text;
}
function getTotalPagar() {
    let myoption;

    myoption = {
        opcion: 47,
        id_tipo_refi: $('#tipo_refi').val(),
        id_dias_mora: $("#dias_vencidos").val(),
        id_saldo_capital: $("#id_capital_tb").text().replace(/[$,]/g, ""),
        id_saldo_capital_aval: $("#id_capital_aval_tb").text().replace(/[$,]/g, ""),
        id_saldo_interes: $("#id_interes_tb").text().replace(/[$,]/g, ""),
        id_saldo_cat: $("#id_cat_tb").text().replace(/[$,]/g, ""),
        id_saldo_cat_vencido: $("#id_cat_vencido_tb").text().replace(/[$,]/g, ""),
        id_cuota_admin: $("#id_cuota_admon_tb").text().replace(/[$,]/g, ""),
        id_saldo_interes_aval: $("#id_int_aval_tb").text().replace(/[$,]/g, ""),
        id_saldo_intxmora: $("#id_ixm_tb").text().replace(/[$,]/g, ""),
        id_saldo_gac: $("#id_gac_tb").text().replace(/[$,]/g, "")
    };

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        data: myoption,
        success: function (json) {
            if (json.success) {
                if(json.hidden_cat){
                   $("#id_cat_vencido").hide();
               }else{
                   $("#id_cat_vencido").show();
               }
                $("#total_pagar").text(numberConComas(json.valor_pagar));
                id_porcentaje=json.porcentaje;
                $("#id_capital_tb").text(numberConComas(json.capital));
                $("#id_interes_tb").text(numberConComas(json.interes));
                $("#id_cat_tb").text(numberConComas(json.cat));
                $("#id_cat_vencido_tb").text(numberConComas(json.cat_vencido));
                $("#id_cuota_admon_tb").text(numberConComas(json.cuota_admon));
                $("#total_pagar").text(numberConComas(json.valor_pagar));
                $("#dto_gac").text(numberConComas(json.dto_gac));
                $("#dto_ixm").text(numberConComas(json.dto_ixm));
                $("#peridos").text(json.periodos_cuota);
                total_a_pagar=json.valor_pagar_total;
                periodo_pago_inicial =json.periodos_cuota;
                
            } else {
                mensajesDelSistema(json.error, '250', '150');
            }
        },
        error: function (xhr) {
            mensajesDelSistema("Error al refinanciar " + "\n" +
                    xhr.responseText, '650', '250', true);
        }
    });

}
function comboEstrategiaCartera() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 49
        },
        success: function (json) {
            if (json.error) {
                return;
            }
            $('#estrategia_cartera').html('');
            $('#estrategia_cartera').append('<option value="" >...</option>');
            for (var datos in json) {
                $('#estrategia_cartera').append('<option value="' + datos + '" >' + json[datos] + '</option>');
            }

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarObservacion() {
    $("#id_observaciones").val("");    
    $("#div_obsevaciones").dialog({
        modal: true,
        width: '390',
        height: '300',
        show: "scale",
        hide: "scale",
        resizable: true,
        title: 'Observaciones',
        closable: false,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {//crear bot�n de cerrar
            "Enviar Negociaci�n": function () {
             refinanciar();
            },
            "Salir": function () {
                $(this).dialog("destroy");
                $("#id_observaciones").val("");
                $("#celular").val("");
            }
        }
    });
   
    
}