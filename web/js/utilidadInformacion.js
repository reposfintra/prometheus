/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var informacionDatos;
$(document).ready(function () {
    funcionFormatos = new formatos();
});

function formatos() {
    return {
        numberConComas: function (x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        numberSinComas: function (x) {
            return x.toString().replace(/,/g, "");
        },
        numberConComasKeyup: function (x) {
            var valor = x.value;
            valor = valor.toString().replace(/,/g, "");
            valor = valor.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $('#' + x.id).val(valor);
        },
        numerSinPunto: function (x) {
            x.toString().replace(/\./g, "");
        },
        soloNumero: function (x) {
            var valor = x.value;
            valor = valor.toString().replace(/[^0-9]/g, '');
            $('#' + x.id).val(valor);
        },
        textMayusKeyup: function (x) {
            var text = x.value;
            text = text.toUpperCase();
            $('#' + x.id).val(text);
        }
    };
}

function funcionFomurarios() {
    return {
        requeridos: function () {
            var vacios = 0;
            $(".requerido").each(function () {
                if (this.value === '') {
                    vacios = vacios + 1;
                }
            });
            return vacios;
        },
        informacion: function (idContenedor, on, off) {
            var json = [];
            var llave = {};
            var check = on;
            var uncheck = off;
            var vacios = this.requeridos();
            if (vacios === 0) {
                $("#" + idContenedor).find(':input').each(function () {
                    var elemento = this;
                    if (elemento.type !== 'button') {
                        if (elemento.type === 'checkbox') {
                            if (on === '' && off === '') {
                                llave[elemento.id] = elemento.value;
                            } else {
                                if ($("#" + elemento.id).is(':checked')) {
                                    llave[elemento.id] = check;
                                } else {
                                    llave[elemento.id] = uncheck;
                                }
                            }
                        }
                        if (elemento.type === 'radio') {
                            var valorRadio = $('input:radio[name=' + elemento.name + ']:checked').val();
                            if (typeof (valorRadio) !== "undefined") {
                                llave[elemento.id] = valorRadio;
                            } else {
                                llave[elemento.id] = '';
                            }
                        } else {
                            llave[elemento.id] = elemento.value;
                        }
                    }
                });
                json.push(llave);
            }
            //console.log(json);
            return  json;
            //$('input:radio[name=edad]:checked').val()
        },
        limpiarCampos: function (contenedor) {
            $("#" + contenedor).find(':input').each(function () {
                var elemento = this;
                if (elemento.type !== 'button') {
                    $('#' + elemento.id).val('');
                }
            });
            $("#" + contenedor).find(':checkbox').each(function () {
                var elemento = this;
                $('#' + elemento.id).attr('checked', false);
            });
        },
        camposRequeridos: function () {
            var campos = [];
            $(".requerido").each(function () {
                console.log(this.id);
                if (this.value === '') {
                    campos.push(this.id);
                }
            });
            return campos;
        },
        mensajeDelSistema: function (idElemento1, idElemento2, msj, width, height, swHideDialog) {
            if (swHideDialog) {
                $("#" + idElemento1).html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
            } else {
                $("#" + idElemento1).html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
            }
            $("#" + idElemento2).dialog({
                width: width,
                height: height,
                show: "scale",
                hide: "scale",
                resizable: false,
                position: "center", modal: true,
                closable: false,
                closeOnEscape: false,
                buttons: {//crear bot�n cerrar
                    "Aceptar": function () {
                        $(this).dialog("destroy");
                    }
                }
            });
            $("#" + idElemento2).siblings('div.ui-dialog-titlebar').remove();
        }
    };
}

function grilla() {
    return {
        //Esta funcion tiene como finalidad devolver un json con las filas seleccionadas en una grilla
        selecciondados: function (idTabla) {
            var i, selRowIds = $("#" + idTabla).jqGrid("getGridParam", "selarrrow"), rowData;
            if (selRowIds.length > 0) {
                var json = new Array();
                for (i = 0; i < selRowIds.length; i++) {
                    rowData = $("#" + idTabla).jqGrid("getLocalRow", selRowIds[i]);
                    json.push(rowData);
                }
                return json;
            } else {
                json = '';
                return json;
            }
        },
        // Esta funcion tiene como finalidad totalizar las columnas de una grilla
        // Ejemplo suma.sumFooter('tabla',[10,7]); 
        // si solo mandas el nombre de la tabla te suma todas las columnas si le mandas el numero de la columna te suma esas en especifico 
        sumFooter: function (idTabla, numColums) {
            var datoColModel = $("#" + idTabla).jqGrid('getGridParam', 'colModel');//obtiene todos los datos de las columnas
            //console.log(numColums);
            if (typeof (numColums) !== 'undefined') {
                for (var i = 0; i < numColums.length; i++) {
                    var posicion = numColums[i];
                    var valorSum = jQuery("#" + idTabla).jqGrid('getCol', datoColModel[posicion].name, false, 'sum');
                    if (datoColModel[posicion].frozen == false || typeof (datoColModel[posicion].frozen) == 'undefined') {
                        jQuery("#" + idTabla).jqGrid('footerData', 'set', jsonValor(datoColModel[posicion].name, valorSum));
                    } else {
                        jQuery("#" + idTabla).jqGrid('footerData', 'set', jsonValor(datoColModel[posicion].name, valorSum));
                        $('.ui-jqgrid-sdiv td[aria-describedby="' + idTabla + '_' + datoColModel[posicion].name + '"]').text('$ ' + funcionFormatos.numberConComas(valorSum));
                    }
                }
            } else {
                for (var i in datoColModel) {
                    if (datoColModel[i].sorttype == 'number') {
                        var valorSum = jQuery("#" + idTabla).jqGrid('getCol', datoColModel[i].name, false, 'sum');
                        if (datoColModel[i].frozen == false || typeof (datoColModel[i].frozen) == 'undefined') {
                            jQuery("#" + idTabla).jqGrid('footerData', 'set', jsonValor(datoColModel[i].name, valorSum));
                        } else {
                            jQuery("#" + idTabla).jqGrid('footerData', 'set', jsonValor(datoColModel[i].name, valorSum));
                            $('.ui-jqgrid-sdiv td[aria-describedby="' + idTabla + '_' + datoColModel[i].name + '"]').text('$ ' + funcionFormatos.numberConComas(valorSum));
                        }
                    }
                }
            }
        }
    };
}

//Devuelve el JSON para asignar los valores en la grilla
function jsonValor(name, valorSum) {
    var valor = {};
    valor[name] = valorSum;
    //console.log(valor);
    return valor;
}

function cargardosList() {
    return {
        //Cargar los combos solo debe de mandar la url, datos (json), el id del html
        //cargarCombos.cargarCombo(url, datos,'tipo_cuota');
        cargarCombo: function (url, datos, idElemento) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                async: false,
                data: datos,
                success: function (json) {
                    try {
                        $('#' + idElemento).html('');
                        for (var datos in json) {
                            $('#' + idElemento).append('<option value=' + datos + '>' + json[datos] + '</option>');
                        }
                    } catch (exception) {
                        alert('error : ' + datos + '>' + json[datos][datos], '250', '180');
                    }
                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        },
//function autocompletarProveedor(id, grilla) {
//    var autocompletar = new cargardosList();
//    var url = "./controller?estado=Admin&accion=Fintra";
//    var datos = {opcion: 12, busqueda: $('#tipo_busqueda_').val()};
//    autocompletar.autocomplete('proveedor', url, datos, function () {
//        jQuery("#" + grilla).jqGrid('setCell', id, 'identificacion', informacionDatos.value);
//        jQuery("#" + grilla).jqGrid('setCell', id, 'empleado', informacionDatos.mivar1);
//    });
//}
        autocomplete: function (idElemento, url, data, callback, callbackOpen, callbackClose) {
            $("#" + idElemento).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        dataType: "json",
                        data: datosEnvio(data, request.term),
                        success: function (data) {
                            response($.map(data, function (item) {
                                console.log(item);
                                return datosRecibido(data);
                            }));
                        }
                    });
                },
                minLength: 3,
                delay: 500,
                disabled: false,
                select: function (event, ui) {
                    callback();
                },
                open: function () {
                    callbackOpen();
                    //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
                },
                close: function () {
                    callbackClose();
                    // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
                }
            });
        }
    };
}

function datosEnvio(data, infoBusqueda) {
    var datos = {};
    var jsonData = [];
    jsonData.push(data);
    datos['_informacion'] = infoBusqueda;
    for (var i in jsonData) {
        var val = jsonData[i];
        for (var j in val) {
            var sub_key = j;
            var sub_val = val[j];
            datos[sub_key] = sub_val;
        }
    }
    return datos;
}

function datosRecibido(data) {
    var datos = {};
    for (var i in data) {
        var val = data[i];
        for (var j in val) {
            var sub_key = j;
            var sub_val = val[j];
            datos[sub_key] = sub_val;
        }
    }
    informacionDatos = datos;
    return datos;
}

//    var objeto = new cargardosList();
//    var objeto2 = new objeto.primer();
//    objeto2.select(function () {
//        console.log('mariana');
//    });

//c=new Canvas()
//c.example_father(function(data){alert('hola mari'+data)},'otra cosa')
//
//example_father(callback,params){
//    	callback(params)
//    }


function CapturarInputsDataTable() {
    return {
        
        requeridos: function () {
            var vacios = 0;
            $(".requerido").each(function () {
                if (this.value === '') {
                    vacios = vacios + 1;
                }
            });
            return vacios;
        },

        informacion: function (contenedor) {

            var json = [];
            //var llave = {};

            var vacios = this.requeridos();

            if (vacios === 0) {

                $('#tbl_solicitudes tbody tr').each(function () {

                    /*
                    var siete_id = $(this).find("td")[7].children[0].id;
                    var siete = $(this).find("td")[7].children[0].value;
                    var ocho_id = $(this).find("td")[8].children[0].value;
                    var ocho = $(this).find("td")[8].children[0].id;
                    var nueve_id = $(this).find("td")[9].children[0].id;
                    var nueve = $(this).find("td")[9].children[0].value;
                    console.log( siete_id + ' - ' + siete + ' - ' + ocho_id + ' - ' + ocho + ' - ' + nueve_id + ' - ' + nueve  );
                    
                    toton = $(this).find("td")[7].children[0].value;
                    console.log('toton es: ');
                    console.log(toton);
                    
                    $(this).children('td').each(function(indiceColumna) {

                        console.log(indiceColumna);
                        console.log($(this).text());
                        if ( indiceColumna == 6 ){//Indice de la columna Office

                            //console.log('CASTIGO: '+$(this).text());
                            console.log("ESTO ES: "+$(this).text() + " - " + toton);

                            if ( $(this).text() == toton ) {
                                console.log($(this).text() + " - " + $(this).find("td")[7].children[0].value);
                            }
                        }
                    });                   
                    */
                   
                   //console.log("xxx: "+ $(this).find("td")[7].children[0].value);
                    var llave = {};

                    llave[$(this).find("td")[8].children[0].id] = $(this).find("td")[8].children[0].value;
                    llave[$(this).find("td")[9].children[0].id] = $(this).find("td")[9].children[0].value;
                    llave[$(this).find("td")[10].children[0].id] = $(this).find("td")[10].children[0].value;
                    llave[$(this).find("td")[11].children[0].id] = $(this).find("td")[11].children[0].value;
                    llave[$(this).find("td")[12].children[0].id] = $(this).find("td")[12].children[0].value;
                    
                    $(this).children('td').each(function(indiceColumna) {
                        //console.log(indiceColumna);
                        //console.log($(this).text());
                        if ( indiceColumna == 0 ){//Indice de la columna Office
                            //console.log("ESTO ES: "+$(this).text());
                            llave['insumoadicional'] = $(this).text();
                        }
                        if ( indiceColumna == 4 ){//Indice de la columna Office
                            //console.log("ESTO ES: "+$(this).text());
                            llave['unidadmedida'] = $(this).text();
                        }
                        
                    });                     
                    
                    json.push(llave);

                });

            }

            console.log(json);

            return  json;

        }

    };
}

function CapturarInputsOrdenCS() {
    return {
        
        requeridos: function () {
            var vacios = 0;
            $(".requerido").each(function () {
                if (this.value === '') {
                    vacios = vacios + 1;
                }
            });
            return vacios;
        },

        informacion: function (contenedor) {

            var json = [];
            var llave = {};

            var toton;
            var validarCantidad;
            
            var vacios = this.requeridos();

            if (vacios === 0) {

                $('#tbl_insumos_ocs tbody tr').each(function () {

                    /*
                     var siete_id = $(this).find("td")[8].children[0].id;
                     var siete = $(this).find("td")[8].children[0].value;
                     var ocho = $(this).find("td")[9].children[0].value;
                     var nueve = $(this).find("td")[10].children[0].value;
                     console.log( siete_id + ' - ' + siete + ' - ' + ocho + ' - ' + nueve  );
                     */

                    toton = $(this).find("td")[10].children[0].value;
                    validarCantidad = $(this).find("td")[11].children[0].value;
                    console.log(toton);
                    console.log(validarCantidad);
                    /*
                    $(this).children('td').each(function(indiceColumna) {

                        //console.log(indiceColumna);
                        //console.log($(this).text());
                        if ( indiceColumna == 6 ){//Indice de la columna Office

                            //console.log('CASTIGO: '+$(this).text());
                            //console.log("ESTO ES: "+$(this).text() + " - " + toton);

                            if ( $(this).text() == toton ) {
                                console.log($(this).text() + " - " + $(this).find("td")[8].children[0].value);
                            }
                        }
                    });*/

                    //$(this).find("td")[8].children[0].value = 500;
                    var llave = {};

                    if ( validarCantidad > 0 ) {
                        
                        llave[ $(this).find("td")[10].children[0].id ] = $(this).find("td")[10].children[0].value;
                        llave[ $(this).find("td")[11].children[0].id ] = $(this).find("td")[11].children[0].value;
                        llave[ $(this).find("td")[12].children[0].id ] = $(this).find("td")[12].children[0].value;
                        llave[ $(this).find("td")[13].children[0].id ] = $(this).find("td")[13].children[0].value;

                        $(this).children('td').each(function(indiceColumna) {
                            //console.log(indiceColumna);
                            //console.log($(this).text());
                            if ( indiceColumna == 0 ){//Indice de la columna Office
                                //console.log("ESTO ES: "+$(this).text());
                                llave['insumoadicional'] = $(this).text();
                            }
                            if ( indiceColumna == 4 ){//Indice de la columna Office
                                //console.log("ESTO ES: "+$(this).text());
                                llave['unidadmedida'] = $(this).text();
                            }

                        }); 

                        json.push(llave);
                        
                    }    

                });

            }

            console.log(json);

            return  json;

        },
        
        InfoDespacho: function (contenedor) {

            var json = [];
            var llave = {};

            var toton;
            var validarCantidad;
            
            var vacios = this.requeridos();

            if (vacios === 0) {

                $('#tbl_despacho tbody tr').each(function () {

                    /*
                     var siete_id = $(this).find("td")[8].children[0].id;
                     var siete = $(this).find("td")[8].children[0].value;
                     var ocho = $(this).find("td")[9].children[0].value;
                     var nueve = $(this).find("td")[10].children[0].value;
                     console.log( siete_id + ' - ' + siete + ' - ' + ocho + ' - ' + nueve  );
                     */
                    
                    toton = $(this).find("td")[9].children[0].value;
                    validarCantidad = replaceAll($(this).find("td")[10].children[0].value, ",", "" );
                    console.log("toton: "+toton);
                    console.log("validarCantidad: "+validarCantidad);

                    var llave = {};
                    
                    if ( validarCantidad > 0 ) {
                        
                        llave[ $(this).find("td")[8].children[0].id ] = replaceAll($(this).find("td")[8].children[0].value, ",", "" );
                        llave[ $(this).find("td")[9].children[0].id ] = replaceAll($(this).find("td")[9].children[0].value, ",", "" );
                        llave[ $(this).find("td")[10].children[0].id ] = replaceAll($(this).find("td")[10].children[0].value, ",", "" );
                        llave[ $(this).find("td")[11].children[0].id ] = replaceAll($(this).find("td")[11].children[0].value, ",", "" );

                        $(this).children('td').each(function(indiceColumna) {
                            console.log("indiceColumna: "+indiceColumna);
                            console.log($(this).text());
                            if ( indiceColumna == 0 ){//Indice de la columna Office
                                //console.log("ESTO ES: "+$(this).text());
                                llave['insumoadicional'] = $(this).text();
                            }
                            if ( indiceColumna == 3 ){//Indice de la columna Office
                                //console.log("ESTO ES: "+$(this).text());
                                llave['unidadmedida'] = $(this).text();
                            }

                        }); 

                        json.push(llave);
                        
                    }    

                });

            }

            console.log(json);

            return  json;

        }

    };
}

function exportarDatos() {
    return {
//             < div id = "divSalidaEx" title = "Exportacion" style = " display: block" >
//            < p  id = "msjEx" style = " display:  none" > Espere un momento por favor... < /p>
//            < center >
//            < img id = "imgloadEx" style = "position: relative;  top: 7px; display: none " src = "./images/cargandoCM.gif" / >
//            < /center>
//            < div id = "respEx" style = " display: none" > < /div>
//            < /div> 
//            EJEMPLO: exportar.exportarExcelJqgrid('tabla_liquidacion',1,'prueba');

        exportarExcelJqgrid: function (nombre_tabla, num_espacios, nombre_reporte, columnasExcluidas) {
            if (typeof (columnasExcluidas) !== 'undefined') {
                var jsonData = $("#" + nombre_tabla).jqGrid('getRowData');
                var datomodel = $("#" + nombre_tabla).jqGrid('getGridParam', 'colModel');
                datomodel.splice(0, num_espacios);
                
                myJsonString = replacer(jsonData, columnasExcluidas, datomodel);
            } else {
                var fullData = $("#" + nombre_tabla).jqGrid('getRowData');
                var myJsonString = JSON.stringify(fullData);
                var nameColum = $("#" + nombre_tabla).jqGrid('getGridParam', 'colNames');
                nameColum.splice(0, num_espacios);
            }
 
            var opt = {
                autoOpen: false,
                modal: true,
                width: 200,
                height: 150,
                title: 'Descarga'
            };
            $("#divSalidaEx").dialog(opt);
            $("#divSalidaEx").dialog("open");
            $("#imgloadEx").show();
            $("#msjEx").show();
            $("#respEx").hide();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "./controller?estado=Admin&accion=Fintra",
                data: {
                    listado: myJsonString,
                    namecolum: nameColum.toString(),
                    namereporte: nombre_reporte,
                    opcion: 92
                },
                success: function (resp) {
                    $("#imgloadEx").hide();
                    $("#msjEx").hide();
                    $("#respEx").show();
                    var boton = "<div style='text-align:center'>\n\ </div>";
                    document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
                },
                error: function (resp) {
                    alert("Ocurrio un error enviando los datos al servidor");
                }
            });
        }
    };
}

function replacer(jsonData, numcolumnas, datomodel) {
    var datos = {};
    for (var i in jsonData) {
        var val = jsonData[i];
        for (var j in val) {
            var sub_key = j;
            var sub_val = val[j];
//                console.log(sub_key);
            var diferencias = 0;
            for (var i = 0; i < numcolumnas.length; i++) {
                var posicion = numcolumnas[i];
//                    console.log(' datomodel[posicion].name: ' + datomodel[posicion].name);
                if (sub_key !== datomodel[posicion].name) {
                    diferencias = diferencias + 1;
                    if (diferencias === 2) {
                        datos[sub_key] = sub_val;
                    }
                    //i= numcolumnas.length;
                    //break;
                }
            }
        }
    }
    return datos;
}

function prueba() {
    var numcolumnas = [0, 2];
    var nameColum = $("#tabla_estadoCuenta").jqGrid('getGridParam', 'colNames');
    nameColum.splice(0, 2);
    console.log(nameColum);
    function replacer(nameColum) {
        for (var i = 0; i < numcolumnas.length; i++) {
            var posicion = numcolumnas[i];
            var resul = nameColum.splice(0, posicion);
            console.log(resul);
        }

    }
    replacer(nameColum);
}