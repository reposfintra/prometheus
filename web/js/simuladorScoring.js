/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    
    getNextSimuladorNumber();
    cargarUnidadesNegocio();
    
    $('.solo-numeric').live('keypress', function (event) {
        return numbersonly(this, event);
    });
        
    $("#btn_show_simulador").click(function () {
        $('#id_und_negocio').attr({disabled: true});
        cargarSimuladorScoring();
        cargarSelectsValoresPredeterminados();
    });
    $("#btn_clear_simulador").click(function () {
        getNextSimuladorNumber();
        $('#id_und_negocio').attr({disabled: false});
        $('#id_und_negocio').val('');
        $('#min_value').val('');
        $('#max_value').val('');
        jQuery("#tabla_simulador_scoring").jqGrid("clearGridData", true);
        $('#div_simulador_scoring').fadeOut();
    });
});

function cargarUnidadesNegocio() {
    $('#id_und_negocio').html('');
  
    $.ajax({
        type: 'POST',
        url: './controller?estado=Auto_&accion=Scoring',
        dataType: 'json',
        data: {
            opcion: 17
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#id_und_negocio').append("<option value=''>Seleccione</option>");                   
                    for (var key in json) {
                        $('#id_und_negocio').append('<option value=' + key + '>' + json[key] + '</option>');                     
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                $('#id_und_negocio').append("<option value=''>Seleccione</option>");              
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarSimuladorScoring() {     
    if ($('#id_und_negocio').val() === '') {
        mensajesDelSistema("DEBE SELECCIONAR LA UNIDAD DE NEGOCIO", '250', '165');
    } else {  
         $('#div_simulador_scoring').fadeIn();
           var grid_tbl_simulador_scoring = jQuery("#tabla_simulador_scoring");
     if ($("#gview_tabla_simulador_scoring").length) {
        refrescarGridSimuladorScoring();
     }else {         
        grid_tbl_simulador_scoring.jqGrid({
            caption: "Listado de Variables de Mercado",
            url: "./controller?estado=Auto_&accion=Scoring",           	 
            datatype: "json",  
            height: '450',
            width: '550',
            cellEdit: true,
            colNames: ['Id', 'VARIABLE MERCADO', 'TIPO VARIABLE', 'VALOR'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'variable_mercado', index: 'variable_mercado', width: 250, align: 'left'},
                {name: 'tipo_variable', index: 'tipo_variable', width: 110, align: 'center', hidden:true},
                {name: 'valor', index: 'valor', resizable:false, align: 'center', width: '180px'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_simulador_scoring'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,  
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                    opcion: 59,
                    id_unidad_negocio: $('#id_und_negocio').val()
                }
            },   
            gridComplete: function () {
                var ids = jQuery("#tabla_simulador_scoring").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var tipo_variable = jQuery("#tabla_simulador_scoring").getRowData(cl).tipo_variable;
                  
                    if (tipo_variable === 'DIGITACION') {                       
                        el = '<input name="valor_param" type="text" class="solo-numeric" value="" id=' + 'valor_' + cl+ '> ';
                    } else {                          
                        el = ' <select value="" id=' + 'valor_' + cl+ ' style="width:170px"></select>';
                    }

                    jQuery("#tabla_simulador_scoring").jqGrid('setRowData', ids[i], {valor: el});

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_simulador_scoring", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_simulador_scoring").jqGrid("navButtonAdd", "#page_tabla_simulador_scoring", {
            caption: "Simular",
            title: "Simular calculo de scoring",
            onClickButton: function () {
                var info = jQuery('#tabla_simulador_scoring').getGridParam('records');
                if (info > 0) {
                   actualizaValoresVariablesMercado();
                } else {
                    mensajesDelSistema("No hay informacion para la simulaci�n", '250', '150');
                }              
            }
        });
    }
  }          
}

function refrescarGridSimuladorScoring(){   
    jQuery("#tabla_simulador_scoring").setGridParam({
        url: "./controller?estado=Auto_&accion=Scoring",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            async:false,
            data: { 
                    opcion: 59,
                    id_unidad_negocio: $('#id_und_negocio').val()
            }
        }       
    });
    
    jQuery('#tabla_simulador_scoring').trigger("reloadGrid");
}

function cargarSelectsValoresPredeterminados(){
    $.ajax({
        type: 'POST',
        url: './controller?estado=Auto_&accion=Scoring',
        dataType: 'json',
        data: {
            opcion: 60,
            id_unidad_negocio:  $('#id_und_negocio').val()
        },
        async: false,
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
      
                for (var key in json) {                  
                    $('#valor_'+json[key].id_variable_mercado).append('<option value=' + json[key].id + '>' + json[key].descripcion + '</option>');                 
                }

            } catch (exception) {
                mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
            }           
          
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });        
}

function actualizaValoresVariablesMercado(){
    var jsonVariables = [];
    var filasId = jQuery("#tabla_simulador_scoring").jqGrid('getRowData');  
    for (var i = 0; i < filasId.length; i++) {
       var id = filasId[i].id;   
       var variable_mercado = jQuery("#tabla_simulador_scoring").getRowData(id).variable_mercado;
       var tipo_variable = jQuery("#tabla_simulador_scoring").getRowData(id).tipo_variable;
       var valor = $('#valor_'+id).val();      
       var errorMessage =  (tipo_variable==='DIGITACION') ? validData(valor) : "";
       if(errorMessage!==''){
           break;
       }  
       var variables = {};
        variables ["id_variable_mercado"] = id;
        variables ["variable_mercado"] = variable_mercado;
        variables ["valor"] = valor;
        jsonVariables.push(variables);
    }
    var listVariables = {};
    listVariables ["variables_mercado"] = jsonVariables;
    if (errorMessage!==''){
         mensajesDelSistema(errorMessage.toString(), '250', '150');
    }else{
        loading("Espere un momento por favor...", "270", "140"); 
        var url = './controller?estado=Auto_&accion=Scoring';
        $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
        opcion: 61,
        id_unidad_negocio:  $('#id_und_negocio').val(),
        num_solicitud: $('#id_solicitud').val(),
        listadoVariables: JSON.stringify(listVariables)
        },
        success: function (json) {
        if (!isEmptyJSON(json)) {

        if (json.error) {
            $("#dialogLoading").dialog('close');
            mensajesDelSistema(json.error, '250', '150');
            return;
        }

        if (json.respuesta === "OK") {               
           $("#dialogLoading").dialog('close');
           $('#min_value').val(json.minimo);
           $('#max_value').val(json.maximo);
           getNextSimuladorNumber();         

        }

        } else {
            $("#dialogLoading").dialog('close');
            mensajesDelSistema("Lo sentimos no se pudo actualizar los valores!!", '250', '150');
        }

        }, error: function (xhr, ajaxOptions, thrownError) {
        alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
        }
        });
    }
}

function getNextSimuladorNumber() { 
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Auto_&accion=Scoring",
            dataType: 'json',
            data: {
                opcion: 62
            },
            success: function(json) {
              
                if (!isEmptyJSON(json)) {
                               
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }
                    
                    if (json.respuesta){   
                        $('#id_solicitud').val(json.respuesta); 
                    }
        
                }else{
                    alert("Lo sentimos no se encontraron resultados!!");
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
}

function validData(valor){  
     var errorMessage = "";
           
        if (valor===''){
           errorMessage = "Faltan valores por llenar";         
        }else if(!isNumber(valor)){
            errorMessage = "Algunos puntajes son inv�lidos. Por favor, verifique";          
        }      
      
    return errorMessage;
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: "Mensaje",
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("close");             
            }
        }
    });
}

function numbersonly(myfield, e, dec)
{
    var key;
    var keychar;
    
    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

// control keys
    if ((key == null) || (key == 0) || (key == 8) ||
            (key == 9) || (key == 13) || (key == 27))
        return true;

// numbers
    else if ((("0123456789-").indexOf(keychar) > -1))
        return true;

// decimal point jump
    else if (dec && (keychar == "."))
    {
        myfield.form.elements[dec].focus();
        return false;
    }
    else
        return false;
}


