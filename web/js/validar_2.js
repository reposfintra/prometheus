// JavaScript Document

function verificar() { 
  if(formulario.Serie_Inicial.value=='' || formulario.Serie_Final.value=='' || formulario.Prefijo.value=='' || formulario.Ultimo_Numero.value=='') {
    alert('�Para poder guardar el registro no pueden quedar campos vacios!');
    return false;
  }

  if(formulario.Serie_Final.value < formulario.Serie_Inicial.value) {
    alert('�La Serie Final debe ser mayo a la Serie Inicial!');
    return false;
  }
  
  if((formulario.Ultimo_Numero.value > formulario.Serie_Final.value) || (formulario.Ultimo_Numero.value < formulario.Serie_Inicial.value)) {
    alert('�El Ultimo Numero debe estar en un rango entre la Serie Inicla y la Final!');
    return false;
  }
  //alert('hola');
  return true;
}

function solo_numeros (field, evt) {
  var keyCode = 
    document.layers ? evt.which :
    document.all ? event.keyCode :
    document.getElementById ? evt.keyCode : 0;
  
  var r = '';
  if((keyCode>=96)&&(keyCode<=105)) return true;
  if((keyCode>=48)&&(keyCode<=57)) return true;
  if((keyCode==8)||(keyCode==37)||(keyCode==39)||(keyCode==46)) return true;
  else return false;
}

function verificar_checkbox()
{    
    for(i=0;i<=formulario.length-1;i++)    
        if(formulario.elements[i].type=='checkbox' && formulario.elements[i].checked)        
            return true;
    alert('Debe seleccionar por lo menos un item...')                                    
    return false;
}

function NuevaVentana(nombre,url,largo,hancho,x,y){
      option="width="+largo+", height="+hancho+" scrollbars=yes top="+x+" left="+y;
      ventana=window.open(url,nombre,option);
      return ventana;
   }

function cambiarFormulario2(dir)
{
	form1.action=dir;
	form1.submit();	
}

function cambiarFormulario(dir)
{
	form1.action=dir;
}

function cambiarForm(dir)
{
	form2.action=dir;
}
function enviarForm(dir)
{
	form2.action=dir;
	if(ValidarFormularioCompleto(form2))
		form2.submit();	
} 


function validarSalida(dir, no)
{
	form2.action=dir;
	if(ValidarFormulario2(form2,no))						 
		form2.submit();	
	
}

function validarSalidaNo(no){
	
	form2.action=form2.action+"&num="+no;
	
	if(ValidarFormulario2(form2,no))						 
		form2.submit();	
	
}
 
function ValidarFormulario2(theForm, num)
{	  
  //alert("El numero de elementos es: "+theForm.elements.length);
	for (i = 0; i < theForm.elements.length; i++){
	  
		var campo = new String(theForm.elements[i].name);
		var no;
	  		
		if(num>9)
			no=campo.substring(campo.length-2,campo.length);
	  
		else if(num<=9){
			if(campo.substr(campo.length-2,1)!="1" && campo.substr(campo.length-2,1)!="2" && campo.substr(campo.length-2,1)!="3" )
				no=campo.substr(campo.length-1,1);
		}
		
		if(no==num){
			 
			if (theForm.elements[i].value ==""){
				alert("NO SE PUEDE PROCESAR LA INFORMACION VERIFIQUE QUE NINGUN CAMPO ESTE VACIO!");
				theForm.elements[i].focus();
				return (false);
			}
			var campos = new Array("pesov"+no, "anticipo"+no, "gacpm"+no, "peajea"+no, "peajeb"+no, "peajec"+no);
			for (j = 0; j < campos.length; j++){
				var c = campos[j];
				if(isNaN(theForm.elements[c].value)){
					alert("Algun campo numerico tiene un valor no valido.");
					theForm.elements[c].focus();
					return (false);
				}
			}
		}
	}

  return (true);
}

function ValidarFormularioCompleto(theForm)
{
var sw=0;
	
 
 for (i = 0; i < theForm.elements.length; i++){
	  
	  var campos = new Array("pesov"+i, "anticipo"+i, "gacpm"+i, "peajea"+i, "peajeb"+i, "peajec"+i);
	  
	  if (theForm.elements[i].value == ""){
		  alert("NO SE PUEDE PROCESAR LA INFORMACION VERIFIQUE QUE TODOS LOS CAMPOS ESTEN LLENOS");
		  theForm.elements[i].focus();
		  return (false);
	  }
	   for (j = 0; j < campos.length; j++){
		campo = campos[j];
		if(isNaN(theForm.elements[campo].value)){
			alert("Algun campo numerico tiene un valor no valido.");
			theForm.elements[campo].focus();
			return (false);
		}
		
		  
	  }
	 
 }

  return (true);
}

function ValidarFormularioACPM(theForm)
{
	var sw=0;
	var campos = new Array("acpm", "emax");
 
 	for (i = 0; i < theForm.elements.length; i++){
		if (theForm.elements[i].value == ""){
		  alert("NO SE PUEDE PROCESAR LA INFORMACION VERIFIQUE QUE TODOS LOS CAMPOS ESTEN LLENOS");
		  theForm.elements[i].focus();
		  return (false);
	  	}
	   	for (j = 0; j < campos.length; j++){
			campo = campos[j];
			if(isNaN(theForm.elements[campo].value)){
				alert("Algun campo numerico tiene un valor no valido.");
				theForm.elements[campo].focus();
				return (false);
			}
		}
	}
	if(!theForm.tipoe.checked &&  !theForm.tipoa.checked){
		alert("Escoja el tipo de servicio");
		return (false);
	}

  	return (true);
}

function ValidarFormularioACPMUpdate(theForm)
{
	var sw=0;
	var campos = new Array("acpm", "emax");

 	for (i = 0; i < theForm.elements.length; i++){
		if (theForm.elements[i].value == ""){
		  alert("NO SE PUEDE PROCESAR LA INFORMACION VERIFIQUE QUE TODOS LOS CAMPOS ESTEN LLENOS");
		  theForm.elements[i].focus();
		  return (false);
	  	}
	   	for (j = 0; j < campos.length; j++){
			campo = campos[j];
			if(isNaN(theForm.elements[campo].value)){
				alert("Algun campo numerico tiene un valor no valido.");
				theForm.elements[campo].focus();
				return (false);
			}
		}
	}
	

  	return (true);
}

function ValidarFormulario(theForm)
{


  if (theForm.standard.value =="0")
  {
	alert("Elija el standard a utilizar.");
	theForm.standard.focus();
	return (false);
  }
  
  
  for (i = 0; i < theForm.elements.length; i++){
	  if (theForm.elements[i].value == ""){
		  alert("NO SE PUEDE PROCESAR LA INFORMACION VERIFIQUE QUE NINGUN CAMPO ESTE VACIO!");
		  theForm.elements[i].focus();
		  return (false);
	  }
   }
   if(isNaN(theForm.orden.value)){
	   alert("El campo orden debe ser numerico");
	   theForm.orden.focus();
		return (false);
	}
	if(isNaN(theForm.peso.value)){
	   alert("El campo peso debe ser numerico");
	   theForm.peso.focus();
		return (false);
	}
 
  return (true);
}


function ValidarFormModificar(theForm)
{

  for (i = 0; i < theForm.elements.length; i++){
	  if (theForm.elements[i].value == ""){
		  alert("NO SE PUEDE PROCESAR LA INFORMACION VERIFIQUE QUE NINGUN CAMPO ESTE VACIO!");
		  theForm.elements[i].focus();
		  return (false);
	  }
   }
   if(isNaN(theForm.orden.value)){
	   alert("El campo orden debe ser numerico");
	   theForm.orden.focus();
		return (false);
	}
	if(isNaN(theForm.peso.value)){
	   alert("El campo peso debe ser numerico");
	   theForm.peso.focus();
		return (false);
	}
	if(isNaN(theForm.pesov.value)){
	   alert("El campo peso vacio debe ser numerico");
	   theForm.pesov.focus();
		return (false);
	}
	if(isNaN(theForm.anticipo.value)){
	   alert("El campo anticipo debe ser numerico");
	   theForm.anticipo.focus();
		return (false);
	}
	if(isNaN(theForm.acpm.value)){
	   alert("El campo acpm debe ser numerico");
	   theForm.acpm.focus();
		return (false);
	}
	if(isNaN(theForm.peajea.value)){
	   alert("El campo peaje a debe ser numerico");
	   theForm.peajea.focus();
		return (false);
	}
	if(isNaN(theForm.peajeb.value)){
	   alert("El campo acpm debe ser numerico");
	   theForm.peajeb.focus();
		return (false);
	}
	if(isNaN(theForm.peajec.value)){
	   alert("El campo acpm debe ser numerico");
	   theForm.peajec.focus();
		return (false);
	}
 
  return (true);
}

function llenarInfo()
{
	if(!form1.tipoe.checked && form1.tipoa.checked){
		form1.emax.value=0;
	}
	else if(form1.tipoe.checked && !form1.tipoa.checked){
		form1.acpm.value=0;
	}
	else if(form1.tipoe.checked && form1.tipoa.checked){
		form1.acpm.value="";
		form1.emax.value="";
		
	}
}
function ValidarFormAnticipo(theForm)
{

  for (i = 0; i < theForm.elements.length; i++){
	  if (theForm.elements[i].value == ""){
		  alert("NO SE PUEDE PROCESAR LA INFORMACION VERIFIQUE QUE NINGUN CAMPO ESTE VACIO!");
		  theForm.elements[i].focus();
		  return (false);
	  }
   }
}
function ValidarFormTiquetes(theForm)
{
 if(theForm.valor.value==""){
	   alert("El campo valor debe ser numerico");
	   theForm.valor.focus();
		return (false);
	}
  if(isNaN(theForm.valor.value)){
	   alert("El campo valor debe ser numerico");
	   theForm.valor.focus();
		return (false);
	}
}
function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPagina(url, nombrePagina)
{
  var wdth = screen.width - screen.width * 0.7;
  var hght = screen.height - screen.height * 1.0;
  var lf = screen.width * 0.2;
  var tp = screen.height * 0.25;
  var options = "status=yes,scrollbars=yes,resizable=yes," +
                setPageBounds(lf, tp, wdth, hght);
  if(form1.standard.value==""){
	alert ('Elija un standard');  
  }else{
  	var hWnd = window.open(url+form1.standard.value, nombrePagina, options);
 	 if ( (document.window != null) && (!hWnd.opener) )
    	hWnd.opener = document.window;
  }
}
function cerrar()
{
  window.opener.document.form1.destinatarios.value="";	
  for (i = 0; i < form2.elements.length; i++){
	  if(form2.elements[i].name.substring(0,5)=="check"){
		 if(form2.elements[i].checked){
	  	window.opener.document.form1.destinatarios.value+=form2.elements[i].value+",";	
		 }
	  }
  }
  close();
}

function iniciar()
{
  destinatarios = window.opener.document.form1.destinatarios.value.split(",");
 for (j =0 ; j <destinatarios.length; j++){
	
	for (i = 0; i < form2.elements.length; i++){
		if(form2.elements[i].value==destinatarios[j]){
			if(form2.elements[i].type=="checkbox"){
				form2.elements[i].checked=true;
			}
		}  
  	}	
  }
 

}
function iniciarR()
{
  destinatarios = window.opener.document.form1.remitentes.value.split(",");
 for (j =0 ; j <destinatarios.length; j++){
	
	for (i = 0; i < form2.elements.length; i++){
		if(form2.elements[i].value==destinatarios[j]){
			if(form2.elements[i].type=="checkbox"){
				form2.elements[i].checked=true;
			}
		}  
  	}	
  }
 

}
function cerrarR()
{
  window.opener.document.form1.remitentes.value="";	
  for (i = 0; i < form2.elements.length; i++){
	  if(form2.elements[i].name.substring(0,5)=="check"){
		 if(form2.elements[i].checked){
	  		window.opener.document.form1.remitentes.value+=form2.elements[i].value+",";	
			
		 }
	  }
  }
  close();
}
function ValidarColpapel(theForm)
{
	//alert("Esta validando");
	var campos = new Array("toneladas", "anticipo");
	if(theForm.remitentes.value=="a" || theForm.remitentes.value=="" ){
		alert("Ingrese los remitentes");
		theForm.Submit.focus();
		return (false);
	}
	
	if(theForm.destinatarios.value=="a" ||  theForm.destinatarios.value==""){
		alert("Ingrese los destinatarios");
		theForm.Submit2.focus();
		return (false);
	}
	
 	for (i = 0; i < theForm.elements.length; i++){
		if (theForm.elements[i].value == ""){
		  alert("NO SE PUEDE PROCESAR LA INFORMACION VERIFIQUE QUE TODOS LOS CAMPOS ESTEN LLENOS");
		  theForm.elements[i].focus();
		  return (false);
	  	}
	   	for (j = 0; j < campos.length; j++){
			campo = campos[j];
			if(isNaN(theForm.elements[campo].value)){
				alert("Algun campo numerico tiene un valor no valido.");
				theForm.elements[campo].focus();
				return (false);
			}
		}
	}
	

  	return true;
}

function ValidarCumplido(theForm)
{
	//alert("Esta validando");
	var campos = new Array("cantidad");
	
	   	for (j = 0; j < campos.length; j++){
			campo = campos[j];
			if(isNaN(theForm.elements[campo].value)){
				alert("Algun campo numerico tiene un valor no valido.");
				theForm.elements[campo].focus();
				return (false);
			}
		}
	if(theForm.feccum.value=""){
		alert('El campo fecha debe estar lleno');
		return (false);
	}
	if(theForm.cantidad.value=""){
		alert('El campo fecha debe estar lleno');
		return (false);
	}
	if(theForm.observacion.value=""){
		alert('El campo fecha debe estar lleno');
		return (false);
	}
	

  	return true;
}


function ValidarColpapelR(theForm)
{
	//alert("Esta validando");
	if(theForm.remitentes.value=="a" || theForm.remitentes.value=="" ){
		alert("Ingrese los remitentes");
		theForm.aremit.focus();
		return (false);
	}
	
	if(theForm.destinatarios.value=="a" ||  theForm.destinatarios.value==""){
		alert("Ingrese los destinatarios");
		theForm.adest.focus();
		return (false);
	}
	
 	for (i = 0; i < theForm.elements.length; i++){
		if (theForm.elements[i].value == ""){
		  alert("NO SE PUEDE PROCESAR LA INFORMACION VERIFIQUE QUE TODOS LOS CAMPOS ESTEN LLENOS");
		  theForm.elements[i].focus();
		  return (false);
	  	}
	   	
	}
	

  	return true;
}

function limpiarColpapel()

{	
	if(form1.ruta.value=="")
		form1.valorpla.length=0;
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
function openPage(url, nombrePagina)
{
  var wdth = screen.width*0.8;
  var hght = screen.height*0.8;
  var lf = 0;
  var tp = 0;
  var options = "status=yes,scrollbars=yes,resizable=yes," +
                setPageBounds(lf, tp, wdth, hght);
  if(form1.ventana.value=="a"){
	alert ('No puede abrir una segunda ventana');  
  }else{
	 form1.ventana.value=="a";
  	var hWnd = window.open(url, nombrePagina, options);
 	 if ( (document.window != null) && (!hWnd.opener) )
    	hWnd.opener = document.window;
  }
}

function frameBuster(){
	if(window!=top)
    	top.location.href = "/fintravalores/index.jsp";
          
}       


function validarReAnticipo(theForm){
	if(theForm.reant.value == ""){
		alert ('El campo de reanticipo debe estar lleno!.');
		return (false);
	}
	if(isNaN(theForm.reant.value)){
		alert ('El campo de reanticipo debe ser numerico!.');
		return (false);
	}
	
	return (true);
}

function validarConsulta(theForm){
	if(theForm.fechaini.value == ""){
		alert ('El campo fecha de inicio no debe ser vacio!.');
		return (false);
	}
	if(theForm.fechafin.value == ""){
		alert ('El campo fecha fin no debe ser vacio!.');
		return (false);
	}
	if(theForm.fechafin.value<theForm.fechaini.value){
		alert ('La fecha fin no puede ser menor que la fecha inicio.');
		return (false);
	}
	
	return (true);
}

/**************************************JUANM*****************************************/

function CheckRemesas(){
	for(i=0;i<document.forms.length;i++) {
		var theForm = document.forms[i];		
		for(j=0;j<theForm.length;j++) 
			theForm.elements[j].checked = true;		
	}		
}

function MensajeImpresion()
{  
    var comilla='"';     
    var i;
    var Registros = "";       
    var ventana; 


    var BaseLeft = 45;
    var BaseTop  = 2;
    var sw = true;
    var cabecera = "<html>   "+
                   "<head>   "+
                   "<style>  "+
                   "   div            {font-family:'Courier New', Courier, mono; font-size: 12px; font-style: normal; color: #000000; } "+
                   "   .saltodepagina {page-break-after: always } "+
                   "</style> "+
                   "</head>  "+
                   "<body>   ";
     var pie     = "</body></html>";
     var mensaje = "";
     var NumeroRemesas = "";
     var Remesas = "";

     var verificacion = true;
     if (document.getElementById('Verificacion')) verificacion = document.getElementById('Verificacion').checked;
	

	
	for(i=0;i<document.forms.length;i++) {
        if(document.forms[i].Impresa.checked) {  
 		

             var agenciaUsuario  =  document.forms[i].agenciaUsuario.value;
             BaseTop         =  (agenciaUsuario=='BQ')?-15:2;
             var posTop          =  (agenciaUsuario=='BQ')?18:BaseTop;
             

	    	
			if((verificacion && confirm("MENSAJE DE IMPRESION \n\n"+
                                        "Se imprimir� la Remision No "+ document.forms[i].Numrem.value + " y los datos adicionales son: \n"+
                                        "\n\n"+
                                        "OT            :"+ document.forms[i].Numrem.value  +"\n"+
                                        "FECHA      :"+ document.forms[i].Fecha.value   +"\n"+
                                        "CLIENTE  :"+ document.forms[i].Cliente.value +"\n"+
                                        "ORIGEN   :"+ document.forms[i].Origen.value  +"\n"+
                                        "DESTINO :"+ document.forms[i].Destino.value +"\n"+                                              
                                        "\n"+
                                        "NOTA: Asegurese que la impresora est� lista para \nllevar a cabo la impresi�n."))
                || (!verificacion)
               ) {


                              Registros    +=document.forms[i].Numrem.value+"~"+document.forms[i].Oc.value+",";
	                      NumeroRemesas+=document.forms[i].Numrem.value+"~";
			      Remesas      +=document.forms[i].Numrem.value+",";

                              if (mensaje != '')  {
                                mensaje += "<div class='saltodepagina'></div>\n\n";
                              }


                              mensaje+="<DIV style='position:relative;' >";

                              //Comentario1 + Codigo de Remesa
                              mensaje+="<div style=' border:0 outset red; overflow:hidden; position:absolute; left:"+(0+BaseLeft)+"px;  top:"+(0+posTop)+"px;  width:300px '>Remesa del Sistema: " + document.forms[i].Numrem.value + "</DIV>";
                 

                              var margenK = 15;

                              //Agencia de Origen
                              mensaje+="<div style=' border:0 outset blue; overflow:hidden; position:absolute; left:"+(10+BaseLeft) +"px;   top:"+(73+BaseTop + margenK) +"px;  width:124px '> " + document.forms[i].AgenciaOri.value + "iiiiiiiiiiiiiiiiiii</DIV>";
                              //Fecha
                              mensaje+="<div style=' border:0 outset green; overflow:hidden; position:absolute; left:"+(135+BaseLeft)+"px;  top:"+(73+BaseTop + margenK) +"px;  width:100px '> " + document.forms[i].Fecha.value.substr(8,2) + "&nbsp;&nbsp;&nbsp;" + document.forms[i].Fecha.value.substr(5,2) + "&nbsp;&nbsp;&nbsp;" + document.forms[i].Fecha.value.substr(2,2) + "</DIV>";
                              //Origen de Carga
                              mensaje+="<div style=' border:0 outset yellow; overflow:hidden; position:absolute; left:"+(10 +BaseLeft) +"px; top:"+(100+BaseTop + margenK)+"px; width:124px '> " + document.forms[i].Origen.value + "</DIV>";
                              //Remitente
                              mensaje+="<div style=' border:0 outset red; overflow:hidden; position:absolute;    left:"+(135+BaseLeft)+"px;  top:"+(100+BaseTop + margenK)+"px;  width:555px '> " + document.forms[i].Remitente.value + "</DIV>";
                              //Destinatario
                              mensaje+="<div style=' border:0 outset green; overflow:hidden; position:absolute; left:"+(10+BaseLeft) +"px;  top:"+(130+BaseTop + margenK + 5 )+"px;  width:329px'> " + document.forms[i].Destinatario.value + "</DIV>";
                              //Direccion Destinatario
                              mensaje+="<div style=' border:0 outset red; overflow:hidden; position:absolute; left:"+(340+BaseLeft)+"px;  top:"+(130+BaseTop + margenK + 5)+"px;  width:239px '  > " + document.forms[i].DireccionDes.value + "</DIV>";
                              //Ciudad Destinatario
                              mensaje+="<div style=' border:0 outset blue; overflow:hidden; position:absolute; left:"+(580+BaseLeft)+"px;  top:"+(130+BaseTop + margenK + 5)+"px;  width:110px '> " + document.forms[i].CiudadDes.value + "</DIV>";
                              //Planilla
                              mensaje+="<div style=' border:0 outset red; overflow:hidden; position:absolute; left:"+(10+BaseLeft) +"px;  top:"+(160+BaseTop + margenK)+"px;  width:124px '> " + document.forms[i].Oc.value + "</DIV>";
                              //Placa
                              mensaje+="<div style=' border:0 outset red; overflow:hidden; position:absolute; left:"+(135+BaseLeft)+"px;  top:"+(160+BaseTop + margenK)+"px;  width:174px '> " + document.forms[i].Placa.value + "</DIV>";
                              //Conductor
                              mensaje+="<div style=' border:0 outset red; overflow:hidden; position:absolute; left:"+(310+BaseLeft)+"px;  top:"+(160+BaseTop + margenK)+"px;  width:380px '> " + document.forms[i].Conductor.value + "</DIV>";



                                // SECCION DE DOCUMENTO INTERNO
                                var docs = document.forms[i].DocInterno.value.split(',');
                                var doc_interno = '';
                                for (x=0;x<docs.length;x++)
                                    doc_interno += docs[x].split(':')[0] + ','; 
                                doc_interno = doc_interno.substr(0,doc_interno.length-1);
                                // FIN SECCION DE DOCUMENTO INTERNO


                              mensaje+="<div style="+ comilla +" border:0 outset red; overflow:hidden; position:absolute; left:"+(10+BaseLeft) +"px;  top:"+(210+BaseTop + margenK)+"px;  width:680 "+ comilla +">SEG�N DOCUMENTO(S) INTERNO(S) DEL CLIENTE: <br>"+ doc_interno +"</DIV>";
                              //Observacion
                              //mensaje+="<div style="+ comilla +"position:absolute; left:"+(10+BaseLeft)+"px;  top:"+(255+BaseTop)+"px;  width:100% "+ comilla +">Observacion: " + document.forms[i].Observacion.value + "</DIV>";
                               
							  //MFontalvo 18.01.06
                              //Precintos y Fiduciaria
                              var texto = '';
                              //Contenedores , Precintos y Fiduciaria
                              if(document.forms[i].Contenedor.value!='') texto = 'Contenedor(es): '+ document.forms[i].Contenedor.value + '<br>';
                              if(document.forms[i].Precinto.value!='')   texto += 'SELLOS : '       + document.forms[i].Precinto.value + '<br>';
                              if(document.forms[i].Fiduciaria.value!='') texto += document.forms[i].Fiduciaria.value;
                              mensaje+="<div style="+ comilla +" border:0 outset red; overflow:hidden;  position:absolute; left:"+(10+BaseLeft)+"px;  top:"+(280+BaseTop)+"px;  width:680 "+ comilla +">"+ texto +"</DIV>";


                              //mensaje+="<div style="+ comilla +" border:1 outset red;position:absolute; left:"+(10+BaseLeft)+"px;  top:"+(310+BaseTop)+"px;  width:620; height:300px;"+ comilla +">"+ document.forms[i].TextoExt.value  +"</DIV>";
                              //Usuario
                              mensaje+="<div style="+ comilla +" border:0 outset red; overflow:hidden; position:absolute; left:"+(10+BaseLeft)+"px;  top:"+(435+BaseTop + 20)+"px;  width:250 "+ comilla +">" + document.forms[i].Usuario.value + "</DIV>";

                              mensaje+="</div>";
            }
        }
    }

    Remesas = Remesas.substr(0, Remesas.length-1);
    if( confirm ("\nMENSAJE DE IMPRESION "+
                 "\n\n� Desea imprimir las planillas asociadas a esta remesa(s)? \n"+
                 "\nREMESA(S)\t"+ Remesas   +
                 "\n\nNOTA: \nAsegurese que la impresora est� lista para llevar a cabo \t\nla impresi�n")){
	sw = true;
    }
    else{
        sw = false;
    }
	


    if (mensaje!=''){
        ventana = NuevaVentana('vent','',850,700,3000,3000);
        ventana.document.write(cabecera + mensaje + pie);
        ventana.document.close();
        ventana.print();
        //ventana.close();
    }

    if(Registros!=''){
        if(sw){
            //window.open('controller?estado=Planillas&accion=Remesa&Impresa='+ NumeroRemesas,'','status=yes, width=0, height=0, screenx=50000,screeny=500000');
        }
        //window.location.href=('controller?estado=Remesa&accion=Actualizar&Registro='+ Registros);
    }

  }
  
  
  /*MENSAJE DE IMPRESION SIN PREGUNTAR POR LAS PLANILLAS*/
function Impresion()
{
    var comilla='"';
    var i;
    var Registros = "";       
    var ventana;


    var BaseLeft = 45;
    var BaseTop  = 2;
    var sw = true;
    var cabecera = "<html>   "+
                   "<head>   "+
                   "<style>  "+
                   "   div            {font-family:'Courier New', Courier, mono; font-size: 12px; font-style: normal; color: #000000; } "+
                   "   .saltodepagina {page-break-after: always } "+
                   "</style> "+
                   "</head>  "+
                   "<body>   ";
     var pie     = "</body></html>";
     var mensaje = "";
     var NumeroRemesas = "";
                   


    var verificacion = true;
    if (document.getElementById('Verificacion')) verificacion = document.getElementById('Verificacion').checked;

    for(i=0;i<document.forms.length;i++) {
        if(document.forms[i].Impresa.checked) {  

             var agenciaUsuario  =  document.forms[i].agenciaUsuario.value;
             BaseTop         =  (agenciaUsuario=='BQ')?-15:2;
             var posTop          =  (agenciaUsuario=='BQ')?18:BaseTop;

	     if((verificacion && confirm("MENSAJE DE IMPRESION \n\n"+
                                        "Se imprimir� la Remision No "+ document.forms[i].Numrem.value + " y los datos adicionales son: \n"+
                                        "\n\n"+
                                        "OT            :"+ document.forms[i].Numrem.value  +"\n"+
                                        "FECHA      :"+ document.forms[i].Fecha.value   +"\n"+
                                        "CLIENTE  :"+ document.forms[i].Cliente.value +"\n"+
                                        "ORIGEN   :"+ document.forms[i].Origen.value  +"\n"+
                                        "DESTINO :"+ document.forms[i].Destino.value +"\n"+                                              
                                        "\n"+
                                        "NOTA: Asegurese que la impresora est� lista para \nllevar a cabo la impresi�n."))
                || (!verificacion)
               ) {

                              Registros+=document.forms[i].Numrem.value+"~"+document.forms[i].Oc.value+",";
                              if (mensaje != '')  {
                                mensaje += "<div class='saltodepagina'></div>\n\n";
                              }


                              mensaje+="<DIV style='position:relative;' >";

                              //Comentario1 + Codigo de Remesa
                              mensaje+="<div style=' border:0 outset red; overflow:hidden; position:absolute; left:"+(0+BaseLeft)+"px;  top:"+(0+posTop)+"px;  width:300px '>Remesa del Sistema: " + document.forms[i].Numrem.value + "</DIV>";
                 

                              var margenK = 15;

                              //Agencia de Origen
                              mensaje+="<div style=' border:0 outset blue; overflow:hidden; position:absolute; left:"+(10+BaseLeft) +"px;   top:"+(73+BaseTop + margenK) +"px;  width:124px '> " + document.forms[i].AgenciaOri.value + "iiiiiiiiiiiiiiiiiii</DIV>";
                              //Fecha
                              mensaje+="<div style=' border:0 outset green; overflow:hidden; position:absolute; left:"+(135+BaseLeft)+"px;  top:"+(73+BaseTop + margenK) +"px;  width:100px '> " + document.forms[i].Fecha.value.substr(8,2) + "&nbsp;&nbsp;&nbsp;" + document.forms[i].Fecha.value.substr(5,2) + "&nbsp;&nbsp;&nbsp;" + document.forms[i].Fecha.value.substr(2,2) + "</DIV>";
                              //Origen de Carga
                              mensaje+="<div style=' border:0 outset yellow; overflow:hidden; position:absolute; left:"+(10 +BaseLeft) +"px; top:"+(100+BaseTop + margenK)+"px; width:124px '> " + document.forms[i].Origen.value + "</DIV>";
                              //Remitente
                              mensaje+="<div style=' border:0 outset red; overflow:hidden; position:absolute;    left:"+(135+BaseLeft)+"px;  top:"+(100+BaseTop + margenK)+"px;  width:555px '> " + document.forms[i].Remitente.value + "</DIV>";
                              //Destinatario
                              mensaje+="<div style=' border:0 outset green; overflow:hidden; position:absolute; left:"+(10+BaseLeft) +"px;  top:"+(130+BaseTop + margenK + 5 )+"px;  width:329px'> " + document.forms[i].Destinatario.value + "</DIV>";
                              //Direccion Destinatario
                              mensaje+="<div style=' border:0 outset red; overflow:hidden; position:absolute; left:"+(340+BaseLeft)+"px;  top:"+(130+BaseTop + margenK + 5)+"px;  width:239px '  > " + document.forms[i].DireccionDes.value + "</DIV>";
                              //Ciudad Destinatario
                              mensaje+="<div style=' border:0 outset blue; overflow:hidden; position:absolute; left:"+(580+BaseLeft)+"px;  top:"+(130+BaseTop + margenK + 5)+"px;  width:110px '> " + document.forms[i].CiudadDes.value + "</DIV>";
                              //Planilla
                              mensaje+="<div style=' border:0 outset red; overflow:hidden; position:absolute; left:"+(10+BaseLeft) +"px;  top:"+(160+BaseTop + margenK)+"px;  width:124px '> " + document.forms[i].Oc.value + "</DIV>";
                              //Placa
                              mensaje+="<div style=' border:0 outset red; overflow:hidden; position:absolute; left:"+(135+BaseLeft)+"px;  top:"+(160+BaseTop + margenK)+"px;  width:174px '> " + document.forms[i].Placa.value + "</DIV>";
                              //Conductor
                              mensaje+="<div style=' border:0 outset red; overflow:hidden; position:absolute; left:"+(310+BaseLeft)+"px;  top:"+(160+BaseTop + margenK)+"px;  width:380px '> " + document.forms[i].Conductor.value + "</DIV>";



                                // SECCION DE DOCUMENTO INTERNO
                                var docs = document.forms[i].DocInterno.value.split(',');
                                var doc_interno = '';
                                for (x=0;x<docs.length;x++)
                                    doc_interno += docs[x].split(':')[0] + ','; 
                                doc_interno = doc_interno.substr(0,doc_interno.length-1);
                                // FIN SECCION DE DOCUMENTO INTERNO


                              mensaje+="<div style="+ comilla +" border:0 outset red; overflow:hidden; position:absolute; left:"+(10+BaseLeft) +"px;  top:"+(210+BaseTop + margenK)+"px;  width:680 "+ comilla +">SEG�N DOCUMENTO(S) INTERNO(S) DEL CLIENTE: <br>"+ doc_interno +"</DIV>";
                              //Observacion
                              //mensaje+="<div style="+ comilla +"position:absolute; left:"+(10+BaseLeft)+"px;  top:"+(255+BaseTop)+"px;  width:100% "+ comilla +">Observacion: " + document.forms[i].Observacion.value + "</DIV>";
                               
			      //MFontalvo 18.01.06
                              //Precintos y Fiduciaria
                              var texto = '';
                              //Contenedores , Precintos y Fiduciaria
                              if(document.forms[i].Contenedor.value!='') texto = 'Contenedor(es): '+ document.forms[i].Contenedor.value + '<br>';
                              if(document.forms[i].Precinto.value!='')   texto += 'SELLOS : '       + document.forms[i].Precinto.value + '<br>';
                              if(document.forms[i].Fiduciaria.value!='') texto += document.forms[i].Fiduciaria.value;
                              mensaje+="<div style="+ comilla +" border:0 outset red; overflow:hidden;  position:absolute; left:"+(10+BaseLeft)+"px;  top:"+(280+BaseTop)+"px;  width:680 "+ comilla +">"+ texto +"</DIV>";


                              //mensaje+="<div style="+ comilla +" border:1 outset red;position:absolute; left:"+(10+BaseLeft)+"px;  top:"+(310+BaseTop)+"px;  width:620; height:300px;"+ comilla +">"+ document.forms[i].TextoExt.value  +"</DIV>";
                              //Usuario
                              mensaje+="<div style="+ comilla +" border:0 outset red; overflow:hidden; position:absolute; left:"+(10+BaseLeft)+"px;  top:"+(435+BaseTop + 20)+"px;  width:250 "+ comilla +">" + document.forms[i].Usuario.value + "</DIV>";

                              mensaje+="</div>";
            }
        }
    }

    if (mensaje!=''){
        ventana = NuevaVentana('vent','',850,700,3000,3000);
        ventana.document.write(cabecera + mensaje + pie);
        ventana.document.close();
        ventana.print();
        ventana.close();
    }

    if(Registros!=""){
        window.location.href('controller?estado=Remesa&accion=Actualizar&Registro='+ Registros);
    }
  }

  function warningRemesa (){
     alert ('Esta remesa no se puede imprimir debido a que tiene asociada una planilla que \nrequiere una autorizacion de la Administracion y Prevencion de Riesgos,\nconsulte el correo para su autorizacion');
  }
