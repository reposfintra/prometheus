/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



$(document).ready(function () {

    loading("Verificando si existen archivos sin procesar...", "270", "140");
    $("#ctrl_carga").val("");
    setTimeout(function () {
       cargarGridCartera();
    }, 500);
        
    $(".boton").click(function () {
        $("#desplegable").slideToggle("slow");
    });
    $("#desplegable").css({display: 'none'});

    $('#cargarArchivo').click(function () {
        if ($("#archivo_credito").val() !== '' &&  $("#archivo_persona").val()!=='' ) {
            if ($("#ctrl_carga").val() === '') {
                cargarArchivoCartera();
            } else {
                mensajesDelSistema("Ya existe un archivo precargado en el sistema.", '250', '150');
            }
         }else{
           mensajesDelSistema("El cargue de solicitud Credito y Persona son Obligatorias", '300', '150');
        }
    });

});

function cargarArchivoCartera() {

    var fd = new FormData(document.getElementById('formulario'));


    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function () {
        $.ajax({
            async: false,
            url: "./controller?estado=Compra&accion=Cartera&opcion=1",
            type: 'POST',
            dataType: 'json',
            data: fd,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (json) {
                $("#archivo").val("");
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        $("#dialogLoading").dialog('close');
                        $("#internoNoty").append("<div class='errorClass'><label>Error:</label><br><span>"+json.exception+"</span> </div>");
                        mensajesDelSistema(json.error, '300', '175');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        $("#dialogLoading").dialog('close');
                         $("#internoNoty").append("<div class='errorClass'><label>Log:</label><br><span> Archivo cargado exitosamente.</span> </div>");
                       // mensajesDelSistema("Archivo cargado exitosamente", '250', '150', true);
                        limpiarEntrada();
                        cargarGridCartera();
                      //  mostrarArchivoPrecarga();
                        return;
                    }

                } else {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci&oacute;n!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }, 500);
}


function cargarGridCartera(){
    
    var grid_cartera = $("#tbl_cartera_nueva");
    if ($("#gview_tbl_cartera_nueva").length) {
            reloadGridCartera(grid_cartera);
        } else {

            grid_cartera.jqGrid({
                caption: "Solicitud cartera",
                url: "./controller?estado=Compra&accion=Cartera",
                mtype: "POST",
                datatype: "json",
                height: '500',
                width: '1800',
                colNames: ['# Solicitud', 'Fecha consulta', 'Valor solicitado', 
                            'Estado', 'Tipo persona', 'Valor aprobado', 'Tipo Negocio', 'Afiliado', 
                            'Convenio', 'Producto', 'Valor producto', 'Sector', 'Subsector', 
                            'Plazo', 'Plazo pr_cuota', 'Primera cuota', 'Ciclo', 'Tasa'],
      
                colModel: [
                    {name: 'numero_solicitud', index: 'numero_solicitud', width: 80, sortable: true, align: 'center', key: true},
                    {name: 'fecha_consulta', index: 'fecha_consulta', width: 90, sortable: true, align: 'center'},
                    {name: 'valor_solicitado', index: 'valor_solicitado', width: 100, align: 'right', 
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'estado_sol', index: 'estado_sol', sortable: true, width: 80, align: 'center'},
                    {name: 'tipo_persona', index: 'tipo_persona', width: 90, align: 'center'},
                    {name: 'valor_aprobado', index: 'valor_aprobado', width: 120, align: 'right', 
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'tipo_negocio', index: 'tipo_negocio', width: 100, align: 'center'},
                    {name: 'afiliado', index: 'afiliado', width: 100, align: 'center'},
                    {name: 'id_convenio', index: 'id_convenio', width: 100, align: 'center'},
                    {name: 'producto', index: 'producto', width: 100, align: 'center'},
                    {name: 'valor_producto', index: 'valor_producto', width: 100, align: 'right',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'cod_sector', index: 'cod_sector', width: 80, align: 'center'},
                    {name: 'cod_subsector', index: 'cod_subsector', width: 80, align: 'center'},
                    {name: 'plazo', index: 'plazo', width: 80, align: 'center'},
                    {name: 'plazo_pr_cuota', index: 'plazo_pr_cuota', width: 100, align: 'center'},
                    {name: 'fecha_primera_cuota', index: 'fecha_primera_cuota', width: 120, align: 'center'},
                    {name: 'ciclo', index: 'ciclo', width: 70, align: 'center'},
                    {name: 'tasa', index: 'tasa', width: 70, align: 'center'}
                    
                ],
                rowNum:1000,
                rowTotal: 200000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                pager: '#page_cartera_nueva',
                viewrecords: true,
                hidegrid: false,
                shrinkToFit: false,
                footerrow: false,
                multiselect: false,
                subGrid: true,
                subGridRowExpanded: function (subgrid_id, row_id) {
                  
                    var subgrid_table_id = subgrid_id + "_t";
                    jQuery("#" + subgrid_id).html("<table id='" + subgrid_table_id + "'></table>");
                    jQuery("#" + subgrid_table_id).jqGrid({
                        url: './controller?estado=Compra&accion=Cartera',
                        datatype: 'json',
                        colNames: ['Tipo', 'Nombre', 'Genero', 'Estado civil','Direccion',
                                   'Departamento', 'Barrio', 'Tipo Identificacion','Identificacion',
                                   'Fecha Nacimiento', 'Telefono','Celular'],
                        colModel: [
                                    {name: 'tipo', index: 'tipo', sortable: true, align: 'center',width: 78},
                                    {name: 'nombre', index: 'nombre', sortable: true, align: 'center',width: 195},
                                    {name: 'genero', index: 'genero', sortable: true, align: 'center',width: 80},
                                    {name: 'estado_civil', index: 'estado_civil', sortable: true, align: 'center',width: 90},
                                    {name: 'direccion', index: 'direccion', sortable: true,width: 120,align: 'left'},
                                    {name: 'departamento', index: 'departamento', sortable: true, align: 'center',width: 100 },
                                    {name: 'barrio', index: 'barrio', sortable: true, align: 'center',width: 100},
                                    {name: 'tipo_id', index: 'tipo_id', sortable: true, align: 'center',width: 100},
                                    {name: 'identificacion', index: 'identificacion', sortable: true,align: 'right',width: 100},
                                    {name: 'fecha_nacimiento', index: 'fecha_nacimiento', sortable: true,  align: 'center',width: 100},
                                    {name: 'telefono', index: 'telefono', sortable: true, align: 'center',width: 80},
                                    {name: 'celular', index: 'celular', sortable: true, align: 'center',width: 80}
                        ],
                        rowNum: 10000,
                        rowTotal: 200000,
                        height: '80%',
                        width: '90%',
                        jsonReader: {
                            root: 'rows',
                            cell: '',
                            repeatitems: false,
                            id: '0'
                        },ajaxGridOptions: {
                            async: false,
                                data: {
                                    opcion: 2,
                                    parametro:row_id
                                }
                            },
                        loadError: function (xhr, status, error) {
                            alert(error);
                        }
                    });
                    
                 },ajaxGridOptions: {
                    async: false,
                    data: {
                        opcion: 2,
                        parametro:""
                    }
                },loadComplete: function () {     
                    $("#dialogLoading").dialog('close');
                    if (grid_cartera.jqGrid('getGridParam', 'records') > 0) {
                        $("#contenedor").show();
                        $("#ctrl_carga").val("Existe");
                    }else{
                        mostrarNegociosCartera();
                    }

                },
                loadError: function (xhr, status, error) {
                    alert(error, 250, 150);
                }
            }).navGrid("#page_cartera_nueva", {add: false, edit: false, del: false, search: false, refresh: false}, {});
            
            $("#tbl_cartera_nueva").navButtonAdd('#page_cartera_nueva', {
                caption: "Crear Solicitud",
                onClickButton: function (e) {
                   generarNegocioCartera();
                }
            });
            
              $("#tbl_cartera_nueva").navButtonAdd('#page_cartera_nueva', {
                caption: "Limpiar",
                onClickButton: function (e) {
                  eliminarArchivoCartera(); 
                }
            });
        }
}
function reloadGridCartera(grid_cartera) {
    grid_cartera.setGridParam({
        datatype: 'json',
        url:"./controller?estado=Compra&accion=Cartera"
        , ajaxGridOptions: {
            async: false,
            data: {
                opcion: 2,
                parametro: ""
            }
        }
    });
    grid_cartera.trigger("reloadGrid");
}

function  generarNegocioCartera() {
    loading("Espere un momento procesando informacion..", "270", "140");
    setTimeout(function () {
        $.ajax({
            async: false,
            url: "./controller?estado=Compra&accion=Cartera&opcion=3",
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $("#dialogLoading").dialog('close');
                if (json.error) {
                    $("#dialogLoading").dialog('close');
                    $("#internoNoty").append("<div class='errorClass'><label>Error:</label><br><span>" + json.exception + "</span> </div>");
                    mensajesDelSistema("Lo sentimos algo salio mal al procesar la informacion", '300', '175');
                    return;
                }

                if (json.respuesta === 'OK') {
                    mensajesDelSistema("Negocios creados exitosamente", '250', '150', true);
                    $("#contenedor").hide();
                    mostrarNegociosCartera();
                    jQuery("#tbl_cartera_nueva").jqGrid("clearGridData", true);
                }
                
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    }, 500);

}

function  eliminarArchivoCartera() {
    loading("Espere un momento procesando informacion..", "270", "140");
    setTimeout(function () {
        $.ajax({
            async: false,
            url: "./controller?estado=Compra&accion=Cartera&opcion=6",
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $("#dialogLoading").dialog('close');
                if (json.error) {
                    $("#dialogLoading").dialog('close');
                    $("#internoNoty").append("<div class='errorClass'><label>Error:</label><br><span>" + json.exception + "</span> </div>");
                    mensajesDelSistema("Lo sentimos algo salio mal al procesar la informacion", '300', '175');
                    return;
                }

                if (json.respuesta === 'OK') {
                    $("#contenedor").hide();
                    $("#contenedor_1").hide();
                    $("#ctrl_carga").val("");
                 }
                
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    }, 500);

}

function mostrarNegociosCartera(){
    var grid_negocios_cartera = $("#tbl_negocios_cartera_nueva");
    if ($("#gview_tbl_negocios_cartera_nueva").length) {
            reloadGridCarteraNegocios(grid_negocios_cartera);
        } else {

            grid_negocios_cartera.jqGrid({
                caption: "Negocios Cartera",
                url: "./controller?estado=Compra&accion=Cartera",
                mtype: "POST",
                datatype: "json",
                height: '500',
                width: '1180',
                colNames: ['# Solicitud',"Fecha consulta", "Valor Solicitado", "Valor aprobado", 
                            "Negocio","Convenio", "Plazo", "Plazo_pr_cuota", 'Primera cuota', 'Ciclo', 'Tasa','Referencia'],      
      
                colModel: [
                    {name: 'numero_solicitud', index: 'numero_solicitud', width: 80, sortable: true, align: 'center', key: true},
                    {name: 'fecha_consulta', index: 'fecha_consulta', width: 90, sortable: true, align: 'center'},
                    {name: 'valor_solicitado', index: 'valor_solicitado', width: 100, align: 'right', 
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'valor_aprobado', index: 'valor_aprobado', width: 120, align: 'right', 
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {name: 'cod_neg', index: 'cod_neg', width: 100, align: 'center'},                    
                    {name: 'id_convenio', index: 'id_convenio', width: 100, align: 'center'},                                 
                    {name: 'plazo', index: 'plazo', width: 80, align: 'center'},
                    {name: 'plazo_pr_cuota', index: 'plazo_pr_cuota', width: 100, align: 'center'},
                    {name: 'fecha_primera_cuota', index: 'fecha_primera_cuota', width: 120, align: 'center'},
                    {name: 'ciclo', index: 'ciclo', width: 70, align: 'center'},
                    {name: 'tasa', index: 'tasa', width: 70, align: 'center'},
                    {name: 'referencia', index: 'referencia', width: 70, align: 'center'}
                    
                ],
                rowNum:1000,
                rowTotal: 200000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                pager: '#page_negocios_cartera_nueva',
                viewrecords: true,
                hidegrid: false,
                shrinkToFit: false,
                footerrow: false,
                multiselect: false,
                ajaxGridOptions: {
                    async: false,
                    data: {
                        opcion: 4
                    }
                },loadComplete: function () {     
                
                    if (grid_negocios_cartera.jqGrid('getGridParam', 'records') > 0) {
                         $("#ctrl_carga").val("Existe");
                        $("#contenedor_1").show();                        
                    }

                },
                loadError: function (xhr, status, error) {
                    alert(error, 250, 150);
                }
            }).navGrid("#page_negocios_cartera_nueva", {add: false, edit: false, del: false, search: false, refresh: false}, {});
            
            $("#tbl_negocios_cartera_nueva").navButtonAdd('#page_negocios_cartera_nueva', {
                caption: "Crear Cartera",
                onClickButton: function (e) {
                 crearDocumentosCartera();
                }
            });
            
            $("#tbl_negocios_cartera_nueva").navButtonAdd('#page_negocios_cartera_nueva', {
                caption: "Descartar",
                onClickButton: function (e) {
                   descartarNegociosCartera();
                }
            });
            
            
        }
    
}

function  descartarNegociosCartera() {
    loading("Espere un momento procesando informacion..", "270", "140");
    setTimeout(function () {
        $.ajax({
            async: false,
            url: "./controller?estado=Compra&accion=Cartera&opcion=5",
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $("#dialogLoading").dialog('close');
                if (json.error) {
                    $("#dialogLoading").dialog('close');
                    $("#internoNoty").append("<div class='errorClass'><label>Error:</label><br><span>" + json.exception + "</span> </div>");
                    mensajesDelSistema("Lo sentimos algo salio mal al procesar la informacion", '300', '175');
                    return;
                }

                if (json.respuesta === 'OK') {
                    $("#contenedor").hide();
                    $("#contenedor_1").hide();
                     $("#ctrl_carga").val("");
                 }
                
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    }, 500);

}



function  crearDocumentosCartera() {
    loading("Espere un momento procesando informacion..", "270", "140");
    setTimeout(function () {
        $.ajax({
            async: false,
            url: "./controller?estado=Compra&accion=Cartera&opcion=7",
            type: 'POST',
            dataType: 'json',
            success: function (json) {
                $("#dialogLoading").dialog('close');
                if (json.error) {
                    $("#dialogLoading").dialog('close');
                    $("#internoNoty").append("<div class='errorClass'><label>Error:</label><br><span>" + json.exception + "</span> </div>");
                    mensajesDelSistema("Lo sentimos algo salio mal al procesar la informacion", '300', '175');
                    return;
                }

                if (json.respuesta === 'OK') {
                    mensajesDelSistema("La cartera se ha generado exitosamente.", '300', '175');
                    $("#contenedor").hide();
                    $("#contenedor_1").hide();
                    $("#ctrl_carga").val("");
                 }
                
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    }, 500);

}


function reloadGridCarteraNegocios(grid_negocios_cartera) {
    grid_negocios_cartera.setGridParam({
        datatype: 'json',
        url:"./controller?estado=Compra&accion=Cartera"
        , ajaxGridOptions: {
            async: false,
            data: {
                opcion: 4
            }
        }
    });
    grid_negocios_cartera.trigger("reloadGrid");
}


function limpiarEntrada(){
    $("#archivo_credito").val("") ;
    $("#archivo_persona").val("");
    $("#archivo_estudiante").val("");
    
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}
function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}