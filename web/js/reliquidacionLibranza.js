/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* global formato */
var formato = new formatos();
$(document).ready(function () {

    $("#fecha_negocio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        // maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
//    $("#Fecha_primera_cuota").datepicker({
//        dateFormat: 'yy-mm-dd',
//        changeMonth: true,
//        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
//        // maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
//        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
//    });

    var myDate = new Date();
    $("#fecha_negocio").datepicker("setDate", myDate);
    $("#Fecha_primera_cuota").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

    cargarConvenios();
    cargarTipoCuota();
    $('#buscar_form').click(function () {
        var info = $('#dato').val();
        if (info !== '') {
            cargarTituloValor();
            ventanaInfoForm();
        } else {
            mensajesDelSistema('Digitar informacion de busqueda', '250', '180');
        }

    });
    $('#aceptar').click(function () {
        validar_obligaciones();
//        var ultilidad = funcionFomurarios();
//        var validacion = ultilidad.requeridos();
//        if (validacion == 0) {
//            ventanaInfoRelq();
//        } else {
//            mensajesDelSistema('Falta campos por llenar', '250', '180');
//        }
    });

});

function cargarConvenios() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 76
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {

//                $('#convenio').html('');
//                $('#convenio').append("<option value=''>  </option>");
                for (var datos in json) {
//                    $('#convenio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                    $('#convenio').val(json[datos]);
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ventanaInfoForm() {
    cargarInfo();
    $("#dialogMsjDetalle").dialog({
        width: '850',
        height: '400',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarInfo() {
    var grid_tabla_ = jQuery("#tabla_infoLibranza");
    if ($("#gview_tabla_infoLibranza").length) {
        reloadGridMostrarInfo(grid_tabla_, 77);
    } else {
        grid_tabla_.jqGrid({
            //caption: "Formularios",
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",
            datatype: "json",
            height: '283',
            width: '800',
            colNames: ['Cedula', 'Nombre', 'No Formulario', 'Negocio', 'Valor Solicitado', 'Valor Negocio', 'Valor Compra','Cant Obligaciones', 'Ocupacion','Tipo Desembolso','Fianza','Tasa Convenio','Tasa Negocio'],
            colModel: [
                {name: 'identificacion', index: 'identificacion', width: 100, align: 'center'},
                {name: 'nombre', index: 'nombre', width: 250, align: 'left', hidden: false},
                {name: 'numero_solicitud', index: 'numero_solicitud', width: 100, align: 'center', hidden: false},
                {name: 'cod_neg', index: 'cod_neg', width: 100, align: 'center', hidden: false},
                {name: 'valor_solicitado', index: 'valor_solicitado', width: 130, align: 'right', hidden: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'vr_negocio', index: 'vr_negocio', width: 130, align: 'right', hidden: false, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_compra', index: 'valor_compra', width: 130, align: 'right', hidden: false, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'cant_obligaciones', index: 'cant_obligaciones', width: 100, align: 'center', hidden: true},
                {name: 'ocupacion', index: 'ocupacion', width: 100, align: 'center', hidden: true},
                {name: 'tipo', index: 'tipo', width: 100, align: 'center', hidden: true},
                {name: 'fianza', index: 'fianza', width: 100, align: 'center', hidden: true},
                {name: 'tasa_interes', index: 'tasa_interes', width: 100, align: 'center', hidden: true},
                {name: 'tasa_neg', index: 'tasa_neg', width: 100, align: 'center', hidden: true},
                
            ],
            rownumbers: false,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            //pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 77,
                    busqueda: $('#busqueda').val(),
                    dato: $('#dato').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_infoLibranza"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var identificacion = filas.identificacion;
                var valor_solicitado = filas.vr_negocio;
                var numero_solicitud = filas.numero_solicitud;
                var cod_neg = filas.cod_neg;
                var valor_compra = filas.valor_compra;
                var cant_obligaciones = filas.cant_obligaciones;
                var ocupacion = filas.ocupacion;
                var tipo = filas.tipo;
                var fianza = filas.fianza;
                var tasa_convenio = parseFloat(filas.tasa_interes);
                var tasa_neg = parseFloat(filas.tasa_neg);
                console.log('tasa_convenio:' +tasa_convenio + ' tasa_neg: '+tasa_neg);
                
                if (tasa_neg > tasa_convenio) {
                    actualizarTasaNegocio(cod_neg,tasa_convenio);
                }
                $('#cliente').val(identificacion);
                $('#valor_financiar').val(formato.numberConComas(valor_solicitado));
                $('#formalario').val(numero_solicitud);
                $('#dato').val(cod_neg);
                $('#valor_compra').val(valor_compra);
                $('#cant_obligaciones').val(cant_obligaciones);
                $('#ocupacion').val(ocupacion);
                $('#tipo').val(tipo);
                $('#fianza').val(fianza);
                $('#tasa_convenio').val(tasa_convenio);
                
                $("#dialogMsjDetalle").dialog("close");
                cargarConveniosLibranza();

            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridMostrarInfo(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                busqueda: $('#busqueda').val(),
                dato: $('#dato').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center", modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarConveniosLibranza() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 78,
            cliente: $('#cliente').val()
        },
        success: function (json) {
            try {

                $('#convenio_l').html('');
                for (var datos in json) {
                    $('#convenio_l').append('<option value=' + datos + '>' + json[datos] + '</option>');

                }
                cargarEmpresasLibranza();
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarEmpresasLibranza() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 79,
            id_pagaduria: $('#convenio_l').val()
        },
        success: function (json) {
            try {

                $('#empresas').html('');
                for (var datos in json) {
                    $('#empresas').append('<option value=' + datos + '>' + json[datos] + '</option>');

                }
                calcularPrimeraCuota();
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTipoCuota() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 80
        },
        success: function (json) {
            try {

                $('#tipo_cuota').html('');
                for (var datos in json) {
                    $('#tipo_cuota').append('<option value=' + datos + '>' + json[datos] + '</option>');

                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTituloValor() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Maestro&accion=Libranza",
        dataType: 'json',
        async: false,
        data: {
            opcion: 81
        },
        success: function (json) {
            try {

                $('#tipo_titulo_valor').html('');
                $('#tipo_titulo_valor').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#tipo_titulo_valor').append('<option value=' + datos + '>' + json[datos] + '</option>');

                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ventanaInfoRelq() {
    reliquidar();
    $("#dialogMsjVentanaReliq").dialog({
        width: '923',
        height: '600',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Aceptar": function () {
                reliquidarNegocio();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function  reliquidar() {
    var ultilidad = funcionFomurarios();
    var data_inicial = ultilidad.informacion('tablita');
    var informacionNegocio = [];
    var llave = {};
    for (var i in data_inicial) {
        var val = data_inicial[i];
        for (var j in val) {
            var sub_key = j;
            var sub_val = formato.numberSinComas(val[j]);
            llave[sub_key] = sub_val;
        }
    }
    informacionNegocio.push(llave);
    console.log(informacionNegocio);
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "./controller?estado=Maestro&accion=Libranza",
        async: false,
        data: {
            opcion: 82,
            info: JSON.stringify({json: informacionNegocio})
        },
        success: function (json) {
            if (json[1].repuesta == 'OK') {
                casteo(json);
                cargarInfoReliq(json);
            } else {
                mensajesDelSistema('Ha ocurrido un error', '250', '180');
            }
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function casteo(json) {
    var longitud = json.length;
    console.log(json);
    console.log(longitud);
    var ncuotas = $('#num_cuotas').val();
    var fnegocio = $('#fecha_negocio').val();
    var fliquidacion = $('#num_cuotas').val();
    $('#val_neg').text(formato.numberConComas($('#valor_financiar').val()));
    $('#tasa_interes').text(json[0].tasa);
    $('#no_cuotas').text(ncuotas);
    //$('#porc_comision_ley').text(json[0].porcentaje_cat);
    $('#fecha_neg').text(fnegocio);
    $('#vlr_fianza').text(formato.numberConComas(json[1].vlr_fianza));
}

// _valor_desembolso , _numero_cuotas , _tipo_cuota , _fecha_calculo , _fecha_primera_cuota 
function cargarInfoReliq() {
    var grid_tabla_ = jQuery("#tabla_reliquidacion");
    if ($("#gview_tabla_reliquidacion").length) {
        reloadGridMostrarInfoReliq(grid_tabla_, 83);
    } else {
        grid_tabla_.jqGrid({
            //caption: "Formularios",
            url: "./controller?estado=Maestro&accion=Libranza",
            mtype: "POST",
            datatype: "json",
            height: '283',
            width: '870',
            colNames: ['Fecha', 'Cuota', 'Saldo Inicial', 'Capital', 'Interes', 'Capacitacion', 'Seguro', 'Valor Cuota', 'Saldo Final'],
            colModel: [
                {name: 'fecha', index: 'fecha', width: 170, align: 'center'},
                {name: 'cuota', index: 'cuota', width: 100, align: 'center', hidden: false},
                {name: 'saldo_inicial', index: 'saldo_inicial', width: 200, align: 'right', hidden: false, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'capital', index: 'capital', width: 200, align: 'right', hidden: false, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'interes', index: 'interes', width: 200, align: 'right', hidden: false, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'capacitacion', index: 'capacitacion', width: 200, align: 'right', hidden: false, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'seguro', index: 'seguro', width: 200, align: 'right', hidden: false, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor', index: 'valor', width: 200, align: 'right', hidden: false, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'saldo_final', index: 'saldo_final', width: 200, align: 'right', hidden: false, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
            ],
            rownumbers: false,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: true,
            //pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 83,
                    valor_desembolso: formato.numberSinComas($('#valor_financiar').val()),
                    numero_cuotas: $('#num_cuotas').val(),
                    tipo_cuota: $('#tipo_cuota').val(),
                    fecha_calculo: $('#fecha_negocio').val(),
                    fecha_primera_cuota: $('#Fecha_primera_cuota').val(),
                    formulario: $('#formalario').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var valor = jQuery("#tabla_reliquidacion").jqGrid('getCol', 'valor', false, 'sum');
                var capital = jQuery("#tabla_reliquidacion").jqGrid('getCol', 'capital', false, 'sum');
                var interes = jQuery("#tabla_reliquidacion").jqGrid('getCol', 'interes', false, 'sum');
                var capacitacion = jQuery("#tabla_reliquidacion").jqGrid('getCol', 'capacitacion', false, 'sum');
                var seguro = jQuery("#tabla_reliquidacion").jqGrid('getCol', 'seguro', false, 'sum');
                jQuery("#tabla_reliquidacion").jqGrid('footerData', 'set', {valor: valor, capital: capital, interes: interes, capacitacion: capacitacion, seguro: seguro});
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            }
        }).navGrid("#pager2", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridMostrarInfoReliq(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                valor_desembolso: formato.numberSinComas($('#valor_financiar').val()),
                numero_cuotas: $('#num_cuotas').val(),
                tipo_cuota: $('#tipo_cuota').val(),
                fecha_calculo: $('#fecha_negocio').val(),
                fecha_primera_cuota: $('#Fecha_primera_cuota').val(),
                formulario: $('#formalario').val(),
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function  reliquidarNegocio() {
    var ultilidad = funcionFomurarios();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "./controller?estado=Maestro&accion=Libranza",
        async: false,
        data: {
            opcion: 84,
            valor_desembolso: formato.numberSinComas($('#valor_financiar').val()),
            numero_cuotas: $('#num_cuotas').val(),
            tipo_cuota: $('#tipo_cuota').val(),
            fecha_calculo: $('#fecha_negocio').val(),
            fecha_primera_cuota: $('#Fecha_primera_cuota').val(),
            formulario: $('#formalario').val(),
            valorFianza: formato.numberSinComas($('#vlr_fianza').text())
        },
        success: function (json) {
            if (json[0].respuesta == 't') {
                mensajesDelSistema('Exito al reliquidar', '250', '180');
                ultilidad.limpiarCampos('tablita');
                $("#dialogMsjVentanaReliq").dialog("close");
            }
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function  calcularPrimeraCuota() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "./controller?estado=Maestro&accion=Libranza",
        async: false,
        data: {
            opcion: 85,
            formulario: $('#formalario').val()
        },
        success: function (json) {
            // alert(json[0].fch_Pago);
            if (json[0].fch_Pago === 'NO') {
                mensajesDelSistema('El formulario no tiene pagaduria', '250', '180');
                $('#Fecha_primera_cuota').val('')
            } else {
                $('#Fecha_primera_cuota').val(json[0].fch_pago);
            }

        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function validar_obligaciones() {
    var tipo = $('#tipo').val();
    var ocupacion = $('#ocupacion').val();
    var valor = numberSinComas($("#valor_financiar").val());
    var obligaciones = $('#valor_compra').val();
    var CantidadObligaciones = $('#cant_obligaciones').val();
    var plazo = $('#num_cuotas').val();
    var fianza = $('#fianza').val();
    
   
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=Libranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 12, tipo: tipo, ocup:ocupacion, vlr_sol: valor, oblig: obligaciones, Cantoblig: CantidadObligaciones, plazo: plazo, fianza: fianza },
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, "270",0);
                } else {
                    if(!json.valido) {
                        mensajesDelSistema(json.mensaje, '300', '150');
                        //alert(json.mensaje);
                    }
                    if(json.valido) {
                        var ultilidad = funcionFomurarios();
                        var validacion = ultilidad.requeridos();
                        if (validacion == 0) {
                            ventanaInfoRelq();
                        } else {
                            mensajesDelSistema('Falta campos por llenar', '250', '180');
                        }
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });    
}


function actualizarTasaNegocio(cod_neg,tasa_convenio) {

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Maestro&accion=Libranza",
            async: false,
            data: {
                opcion: 86,
                cod_neg: cod_neg,
                tasa_convenio: tasa_convenio
                
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    /*mensajesDelSistema("Exito al actualizar", '230', '150', true);*/
                }
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } 


