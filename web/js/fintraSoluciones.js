/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $('#wast').mouseover(function () {
        var src = '/fintra/images/help_2.png';
        $(this).attr("src", src);
    }).mouseout(function () {
        var src = '/fintra/images/help_.png';
        $(this).attr("src", src);
    });

    $('#buscar').click(function () {
        var filtro_ = $('#filtro_').val();
        var periodo_inicio = $('#periodo_inicio').val();
        var periodo_fin = $('#periodo_fin').val();
        if ((filtro_ !== 'otro' && (periodo_inicio === '' || periodo_fin === '')) || (filtro_ === 'otro' && (periodo_inicio !== '' && periodo_fin !== ''))) {
            cargando_toggle();
            tiempoEspera('paso1');
        } else {
            mensajesDelSistema('Debe digitar el rando de periodos', '300', 'auto', false);
        }
    });
    $('#atpaso1').click(function () {
        $('#paso_busqueda').val('atpaso1');
    });
    $('#wast').click(function () {
        var paso_busqueda = $('#paso_busqueda').val();
        if (paso_busqueda.startsWith('atpaso')) {
            ventanaMostrarhelp();
        } else {
            mensajesDelSistema("Debe seleccionar una pesta�a", '250', '150', false);
        }

    });
});

function tiempoEspera() {
    var paso = saberPaso();
    var grillas = new grande();
    setTimeout(function () {
        switch (paso) {
            case 'paso1':
                grillas.cargarFintraSolucionesPaso1('paso1');
        }
    }, 500);
}

function openTAb(evt, idTab) {
// Declare all variables
    var i, tabcontent, tablinks, tabla_filtro;
    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

// Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

// Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById("tablita").style.display = "block";
    document.getElementById(idTab).style.display = "block";
    evt.currentTarget.className += " active";
}

function saberPaso() {
    var paso_busqueda = $('#paso_busqueda').val();
    var paso;
    switch (paso_busqueda) {
        case 'atpaso1':
            paso = 'paso1';
            break;
        case  'atpaso2':
            paso = 'paso2';
            break;
        case 'atpaso3':
            paso = 'paso3';
            break;
        case 'atpaso4':
            paso = 'paso4';
            break;
        case 'atpaso5':
            paso = 'paso5';
            break;
    }
    return paso;
}

function busqueda_filtro() {
    var filtro = $('#filtro_').val();
    if (filtro === 'otro') {
        $(".f_otro").css("display", "block");
        $("#tablita").css("width", "682px");
        $("#tablainterna").css("width", "680px");
    } else {
        $(".f_otro").css("display", "none");
        $('#periodo_inicio').val('');
        $('#periodo_fin').val('');
        $("#tablita").css("width", "290px");
        $("#tablainterna").css("width", "272px");
    }
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function ventanaMostrarhelp() {
    var mensaje = mostrarHelp();
    var paso = saberPaso();
    var permiso = confirmarPermisoHelp();
    console.log(mensaje);
    if (permiso.startsWith('SI')) {
        /*usuarios con permiso para editar el texto de la ayuda*/
        if (mensaje === '') {
            mensajesDelSistema('No se ha realizado la configuraci�n de este paso', '370', 'auto', false);
        } else {
            swal({
                title: "Ayuda " + paso.toLowerCase(),
                text: '<p style = "text-align : justify;"> ' + mensaje + ' </p>',
                html: true,
                showCancelButton: true,
                confirmButtonColor: "#397EAD",
                confirmButtonText: "Aceptar",
                cancelButtonText: "Editar",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (!isConfirm) {
                    ventanaEditarHelp();
                }
            });
        }

    } else if (permiso.startsWith('NO')) {
        /*usuarios sin permiso para editar el texto de la ayuda*/
        swal({
            title: "Ayuda " + paso.toLowerCase(),
            text: '<p style = "text-align : justify;"> ' + mensaje + ' </p>',
            html: true,
            confirmButtonText: "Aceptar"
        });
    }
}

function ventanaEditarHelp() {
    mostrarHelpDatos();
    $("#dialogMsjHelp").dialog({
        width: '556',
        height: '380',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Actualizar": function () {
                guardarCambioHelp();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function confirmarPermisoHelp() {
    var permiso;
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'text',
        async: false,
        data: {
            opcion: 42
        },
        success: function (json) {
            permiso = json;
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return permiso;
}

function mostrarHelpDatos() {
    var paso = saberPaso();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 43,
            paso: paso,
            modulo: $('#modulo').val()
        },
        success: function (json) {
            $('#id').val(json[0].id);
            $('#modulo_').val(json[0].modulo);
            $('#paso_').val(json[0].paso);
            $('#textoArea').val(json[0].descripcion_ayuda);
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mostrarHelp() {
    var paso = saberPaso();
    var help;
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'text',
        async: false,
        data: {
            opcion: 41,
            paso: paso,
            modulo: $('#modulo').val()
        },
        success: function (json) {
            console.log(json.length);
            if (json.length > 1) {
                help = json;
            } else {
                help = '';
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return help;
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center", modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function grande() {
    return {
        cargarFintraSolucionesPaso1: function (paso) {
            $('#tabla_fintra_soluciones').jqGrid('GridUnload');
            var grid_tabla_ = jQuery("#tabla_fintra_soluciones");
            if ($("#gview_tabla_fintra_soluciones").length) {
                reloadGridMostrar(grid_tabla_, 70, paso);
            } else {
                grid_tabla_.jqGrid({
                    caption: "Movimiento Auxiliar",
                    url: "./controller?estado=Admin&accion=Fintra",
                    mtype: "POST",
                    datatype: "json",
                    height: '495',
                    autowidth: false,
                    colNames: ['Periodo', 'Cuenta', 'Nombre Cuenta', 'Valor Debito', 'Valor Credito'],
                    colModel: [
                        {name: 'periodo', index: 'periodo', width: 100, align: 'center', cellattr: arrtSetting},
                        {name: 'cuenta', index: 'cuenta', width: 100, align: 'left', hidden: false, summaryType: function (value, name, record, newGroup) {
                                return "Total:";
                            }},
                        {name: 'nombre_cuenta', index: 'nombre_cuenta', width: 300, align: 'left', hidden: false},
                        {name: 'valor_debito', index: 'valor_debito', width: 130, align: 'right', search: true, sorttype: 'currency',
                            formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                        {name: 'valor_credito', index: 'valor_credito', width: 130, align: 'right', search: true, sorttype: 'currency',
                            formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
                    ],
                    rownumbers: true,
                    rownumWidth: 25,
                    rowNum: 1000,
                    rowTotal: 1000,
                    loadonce: true,
                    gridview: true,
                    viewrecords: false,
                    hidegrid: false,
                    shrinkToFit: true,
                    footerrow: true,
                    pager: '#pager',
                    multiselect: false,
                    multiboxonly: false,
                    pgtext: null,
                    pgbuttons: false,
                    grouping: true,
                    cmTemplate: {sortable: false},
                    jsonReader: {
                        root: "json",
                        repeatitems: false,
                        id: "0"
                    }, ajaxGridOptions: {
                        dataType: "json",
                        type: "POST",
                        async: false,
                        data: {
                            opcion: 70,
                            paso: paso,
                            filtro_: $('#filtro_').val(),
                            periodo_inicio: $('#periodo_inicio').val(),
                            periodo_fin: $('#periodo_fin').val()
                        }
                    },
                    loadError: function (xhr, status, error) {
                        mensajesDelSistema(error, 250, 150);
                    }, loadComplete: function (id, rowid) {
                        var valor_debito = grid_tabla_.jqGrid('getCol', 'valor_debito', false, 'sum');
                        var valor_credito = grid_tabla_.jqGrid('getCol', 'valor_credito', false, 'sum');
                        grid_tabla_.jqGrid('footerData', 'set', {cuenta: 'TOTAL', valor_debito: valor_debito, valor_credito: valor_credito});
                        cargando_toggle();
                    },
                    gridComplete: function (index) {

                    },
                    ondblClickRow: function (rowid, iRow, iCol, e) {

                    },
                    groupingView: {
                        groupField: ['periodo'],
                        groupSummary: [true],
                        groupColumnShow: [true],
                        groupText: ['<b>{0} Total Debito:</b> ${valor_debito}  <b>Total Credito:</b> ${valor_credito}'],
                        groupCollapse: true,
                        groupOrder: ['asc'],
                        groupSummaryPos: ["footer"]
                    }
                }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
                });
                $("#tabla_fintra_soluciones").navButtonAdd('#pager', {
                    caption: "Export Excel",
                    onClickButton: function () {
                        var info = grid_tabla_.getGridParam('records');
                        if (info > 0) {
                            exportarExcel('tabla_fintra_soluciones', paso);
                        } else {
                            mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                        }
                    }
                });
            }
        }
    };
}


function reloadGridMostrar(grid_tabla, opcion, paso) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                paso: paso,
                filtro_: $('#filtro_').val(),
                periodo_inicio: $('#periodo_inicio').val(),
                periodo_fin: $('#periodo_fin').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

var aux = '';
arrtSetting = function (rowId, val, rawObject, cm) {
    //   var attr = rawObject.attr[cm.name], result;
    var result = '';
    if (typeof (rawObject) !== "undefined") {
        var attr = rawObject[cm.name];
        // console.log('attr.rowspan: ' + attr.rowspan + ' attr.display: ' + attr.display);
        if (attr.rowspan) {
            result = ' rowspan=' + '"' + attr.rowspan + '"';
        } else if (attr.display) {
            result = ' style="display:' + attr.display + '"';
        } else {

            result = " rowspan='18'";
        }
        //empanada 
        if (aux == '') {
            aux = attr;
            result = " rowspan='18'";
        } else if (attr == aux) {
            result = "style='display:none'";
        } else {
            aux = attr;
            result = " rowspan='18'";
        }
    }
    // console.log(result);
    return result;
};

function exportarExcel(tabla, paso) {
    var fullData = $("#" + tabla).jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Admin&accion=Fintra",
        data: {
            listado: myJsonString,
            opcion: 71,
            paso: paso
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}