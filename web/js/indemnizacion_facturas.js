/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {



    $('.boton').click(function () {        
        if($("#mas").html()==='Mas...'){
            $("#mas").html('Menos...');
        }else{
             $("#mas").html('Mas...');
        }
        $("#foto").val('');
        $("#busqueda_avanzada").slideToggle("fast");
        $("#page_facturas").find("table.navtable").hide();
    });

    $('#buscar').click(function () {
        buscarFacturas();
        $("#page_facturas").find("table.navtable").show();
    });
    
     $('#radio2').click(function () {
         
         $("#buscarAvanzada").hide();
         $("#busqueda_avanzada").hide();
    });
    
    $('#radio1').click(function () {
         
         $("#buscarAvanzada").show();
         $("#busqueda_avanzada").hide();
    });
    
    $('#ver_facturas').click(function () {
         exportarFacturasIndemnizadas();
    });


    $("#foto").blur(function () {
        var date = new Date();
        var mes = date.getMonth() + 1;
        var anio = date.getFullYear();
        if($("#foto").val()>(anio+""+mes)){
           mensajesDelSistema("El periodo no puede ser mayor al mes actual", '250', '150', true);
        }

    });
    
    buscarTipoCartera();
    //$("#busqueda_avanzada").css({display: 'none'});
     $("#fecha_inicio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
//    var myDate = new Date();
//    $("#fecha_inicio").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

});

var valor_a_desistir=0;
function buscarFacturas() {
//    var editcelda=false;
//    var hiddencell=true;
    if ($("#unidad_negocio").val() !== '' && $("#cartera_en").val() !== '' && $("#fecha_inicio").val() !=='') {
        
        var caption = "Facturas";
        var grid_facturas = $("#tbl_facturas");
        if ($("#gview_tbl_facturas").length) {
            reloadGridFacturas(grid_facturas);
        } else {

            grid_facturas.jqGrid({
                caption: caption,
                url: "./controller?estado=Contabilidad&accion=General",
                mtype: "POST",
                datatype: "json",
                height: '500',
                width: 'auto',
                colNames: ['Cod cliente', 'Identificacion', 'Nombre', 'Foto', 'Negocio', '# Factura', 'Cuota', 'Fecha vencimiento'
                            , 'Dias mora', 'Vlr factura', 'Vlr indemnizado','Vlr a desistir', 'Fecha Indemnizacion', '# Aval', 'Convenio', 'Cuenta', 'Linea Neg','Cartera en'],
                colModel: [
                    {name: 'codcli', index: 'codcli', width: 80, sortable: true, align: 'center',hidden:true},
                    {name: 'nit_cliente', index: 'nit_cliente', width: 80, sortable: true, align: 'left'},
                    {name: 'nombre_cliente', index: 'nombre_cliente', sortable: true, width: 210, align: 'left'},
                    {name: 'periodo_foto', index: 'periodo_foto', width: 70, align: 'center'},
                    {name: 'negocio', index: 'negocio', width: 70, align: 'center'},
                    {name: 'documento', index: 'documento', width: 70, align: 'left', key: true},
                    {name: 'cuota', index: 'cuota', width: 50, align: 'center'},
                    {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 100, align: 'center'},
                    {name: 'dias_vencidos', index: 'dias_vencidos', width: 70, align: 'center'},
                    {name: 'valor_factura', index: 'valor_factura', width: 100, align: 'right',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                    {name: 'valor_indemnizado', index: 'valor_indemnizado', width: 100, align: 'right',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                    {name: 'valor_desistir', index: 'valor_desistir', width: 100, align: 'right', editable: true,
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                        editoptions: {dataEvents: [{type: 'keydown', fn: function (e) {
                                        if (e.keyCode === 13) {
                                            var rowid = e.target.id.replace("_valor_desistir", "");
                                            if (parseInt(valor_a_desistir) < parseInt(e.target.value)) {
                                                   mensajesDelSistema("El valor a desistir no puede ser mayor a :"+valor_a_desistir, "320", "150"); 
                                                     $("#" + rowid + '_valor_desistir').val(valor_a_desistir);
                                            }
                                        }

                                return;
                            }}]}
                    
                    
                    },
                    {name: 'fecha_indemnizacion', index: 'fecha_indemnizacion', width: 100, align: 'center'},
                    {name: 'numero_aval', index: 'numero_aval', width: 70, align: 'center'},
                    {name: 'convenio', index: 'convenio', width: 60, align: 'center'},
                    {name: 'cuenta', index: 'cuenta', width: 70, align: 'center'},
                    {name: 'ref_4', index: 'ref_4', width: 80, align: 'center',hidden:true},
                    {name: 'cartera_en', index: 'cartera_en', width: 80, align: 'center',hidden:true}
                ],
                rowNum: 6000,
                rowTotal: 1000000,
                loadonce: true,
                rownumWidth: 40,
                gridview: true,
                pager: '#page_facturas',
                viewrecords: true,
                hidegrid: false,
                shrinkToFit: false,
                footerrow: false,
                multiselect: true,
                cellsubmit: "clientArray",
                editurl: 'clientArray'
                ,ondblClickRow: function (rowid, iRow, iCol, e) {
                    if(iCol===12){
                        var rowsdata = $("#tbl_facturas").jqGrid("getLocalRow", rowid);
                        valor_a_desistir = rowsdata.valor_desistir;
                        grid_facturas.editRow(rowid, true); 
                    }
                    
                }, ajaxGridOptions: {
                    async: false,
                    data: {
                        opcion: $('input:radio[name=radios]:checked').val(),
                        mora: $("#mora").val(),
                        linea_negocio: $("#unidad_negocio").val(),
                        foto: $("#foto").val(),
                        cartera_en: $("#cartera_en").val(),
                        fecha_corte: $("#fecha_inicio").val()
                    }
                }, loadComplete: function () {
                    if (grid_facturas.jqGrid('getGridParam', 'records') > 0) {
                        deshabilitarPanelBusqueda(true);
                    }else{
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
                },
                loadError: function (xhr, status, error) {
                    alert(error, 250, 150);
                }
            }).navGrid("#page_facturas", {add: false, edit: false, del: false, search: false, refresh: false}, {});
            
//            jQuery("#tbl_facturas").jqGrid('filterToolbar',
//                    {
//                        autosearch: true,
//                        searchOnEnter: true,
//                        defaultSearch: "cn",
//                        stringResult: true,
//                        ignoreCase: true,
//                        multipleSearch: true
//                    });


            //boton que sirve apra desbloquear el panel de buquedad
            $("#tbl_facturas").navButtonAdd('#page_facturas', {
                caption: "Desbloquear",
                title: "desbloquear",
                onClickButton: function (e) {
                    deshabilitarPanelBusqueda(false);
                    grid_facturas.jqGrid("clearGridData", true);
                }
            });
            
            $("#tbl_facturas").navButtonAdd('#page_facturas', {
                caption: "Exportar Excel",
                title: "Exportar Excel",
                onClickButton: function (e) {
                    exportarExcel(grid_facturas);
                }
            });


            if ($('input:radio[name=radios]:checked').val() === '1') {

                $("#tbl_facturas").navButtonAdd('#page_facturas', {
                    caption: "Indemnizar",
                    title: "indemnizar",
                    onClickButton: function (e) {
                        indemnizaFacturas(grid_facturas);
                    }
                });
                $("#tbl_facturas").hideCol("valor_desistir");
                $("#tbl_facturas").jqGrid('setGridWidth','1338');
            }

            if ($('input:radio[name=radios]:checked').val() === '2') {

                $("#tbl_facturas").navButtonAdd('#page_facturas', {
                    caption: "Desistir",
                    title: "desistir",
                    onClickButton: function (e) {
                        desistirFacturas(grid_facturas);
                    }
                });
                
                $("#tbl_facturas").jqGrid('setGridWidth','1435');
            }


        }
    } else {
        mensajesDelSistema("Debe seleccionar la linea de negocio, el tipo de cartera y la fecha de corte", "300", "150");
    }

}

function reloadGridFacturas(grid_facturas) {

    grid_facturas.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Contabilidad&accion=General",
        autowidth: true,
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: $('input:radio[name=radios]:checked').val(),
                mora: $("#mora").val(),
                linea_negocio: $("#unidad_negocio").val(),
                foto: $("#foto").val(),
                cartera_en:$("#cartera_en").val(),
                fecha_corte: $("#fecha_inicio").val()
            }
        }
    });
    grid_facturas.trigger("reloadGrid");

    if ($('input:radio[name=radios]:checked').val() === '1') {

        var $td = $(grid_facturas[0].p.pager + '_left ' + 'td[title="indemnizar"]');
        $td.remove();
        $td = $(grid_facturas[0].p.pager + '_left ' + 'td[title="desistir"]');
        $td.remove();
         
        $("#tbl_facturas").navButtonAdd('#page_facturas', {
            caption: "Indemnizar",
            title: "indemnizar",
            onClickButton: function (e) {
                indemnizaFacturas(grid_facturas);
            }
        });
        $("#tbl_facturas").hideCol("valor_desistir");
        $("#tbl_facturas").jqGrid('setGridWidth','1338');
    }

    if ($('input:radio[name=radios]:checked').val() === '2') {
        var $td = $(grid_facturas[0].p.pager + '_left ' + 'td[title="desistir"]');
        $td.remove();
        $td = $(grid_facturas[0].p.pager + '_left ' + 'td[title="indemnizar"]');
        $td.remove();
       

        $("#tbl_facturas").navButtonAdd('#page_facturas', {
            caption: "Desistir",
            title: "desistir",
            onClickButton: function (e) {
                desistirFacturas(grid_facturas);
            }
        });
         $("#tbl_facturas").showCol("valor_desistir");
         $("#tbl_facturas").jqGrid('setGridWidth','1435');
    }

}

function indemnizaFacturas(grid_facturas) {
    
    var i, selRowIds = $("#tbl_facturas").jqGrid("getGridParam", "selarrrow"), rowData;
    if (selRowIds.length > 0) {
        
        loading("Espere un momento procesando facturas.", "270", "140");
        var arrayJson = new Array();
        for (i = 0; i < selRowIds.length; i++) {
            rowData = $("#tbl_facturas").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }

        console.log(JSON.stringify(arrayJson));

        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Contabilidad&accion=General",
                type: 'POST',
                dataType: 'json',
                data: {
                    opcion: 3,
                    listJson: JSON.stringify(arrayJson)
                },
                success: function (json) {

                    if (json.respuesta === 'TRUE') {
                        reloadGridFacturas(grid_facturas);
                        $("#dialogLoading").dialog('close');
                    } else {
                        mensajesDelSistema(json.respuesta, '300', '150', true);
                        $("#dialogLoading").dialog('close');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);

    }else{
        
        mensajesDelSistema("No hay datos seleccionados para indemnizar", '300', '150', true);
    }
}

function desistirFacturas(grid_facturas) {

    var i, selRowIds = $("#tbl_facturas").jqGrid("getGridParam", "selarrrow"), rowData;
    
    if (selRowIds.length > 0) {
      
        loading("Espere un momento procesando facturas.", "270", "140");
        
        var arrayJson = new Array();
        for (i = 0; i < selRowIds.length; i++) {
            rowData = $("#tbl_facturas").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }

        console.log(JSON.stringify(arrayJson));

        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Contabilidad&accion=General",
                type: 'POST',
                dataType: 'json',
                data: {
                    opcion: 5,
                    listJson: JSON.stringify(arrayJson)
                },
                success: function (json) {

                    if (json.respuesta === 'TRUE') {
                        reloadGridFacturas(grid_facturas);
                        $("#dialogLoading").dialog('close');
                    } else {
                        mensajesDelSistema(json.respuesta, '300', '150', true);
                        $("#dialogLoading").dialog('close');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);
    } else {
        mensajesDelSistema("No hay datos seleccionados para desistir", '300', '150', true);
    }

}


function buscarTipoCartera(){
    $.ajax({
        type: 'POST',
        url:"./controller?estado=Contabilidad&accion=General",
        dataType: 'json',
        data: {
            opcion: 4
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                try {
                    $('#cartera_en').empty();
//                    $('#transportadora').append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#cartera_en').append('<option value=' + json[key].codigo + '>' + json[key].nombre + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                  mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    
}

function exportarExcel(grid_facturas) {
    
    var i, selRowIds = $("#tbl_facturas").jqGrid("getGridParam", "selarrrow"), rowData;
    if (selRowIds.length > 0) {
        
      //  loading("Espere un momento procesando excel.", "270", "140");
        var arrayJson = new Array();
        for (i = 0; i < selRowIds.length; i++) {
            rowData = $("#tbl_facturas").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }

        console.log(JSON.stringify(arrayJson));
        
        var opt = {
            autoOpen: false,
            modal: true,
            width: 220,
            height: 150,
            title: 'Descarga Reporte'
        };
        //  $("#divSalidaEx").dialog("open");
        $("#divSalidaEx").dialog(opt);
        $("#divSalidaEx").dialog("open");
        $("#imgloadEx").show();
        $("#msjEx").show();
        $("#respEx").hide();
        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Contabilidad&accion=General",
                type: 'POST',
                dataType: "html",
                data: {
                    opcion: 6,
                    listado: JSON.stringify(arrayJson),
                    tipo:$('input:radio[name=radios]:checked').val()
                },
                success: function (resp) {
                    $("#imgloadEx").hide();
                    $("#msjEx").hide();
                    $("#respEx").show();
                    var boton = "<div style='text-align:center'>\n\ </div>";                   
                    document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;


                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);

    }else{
        
        mensajesDelSistema("No hay datos seleccionados para exportar", '300', '150', true);
    }
}

function exportarFacturasIndemnizadas() {    
  
        
        var opt = {
            autoOpen: false,
            modal: true,
            width: 250,
            height: 150,
            title: 'Descarga Reporte'
        };
        //  $("#divSalidaEx").dialog("open");
        $("#divSalidaEx").dialog(opt);
        $("#divSalidaEx").dialog("open");
        $("#imgloadEx").show();
        $("#msjEx").show();
        $("#respEx").hide();
        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Contabilidad&accion=General",
                type: 'POST',
                dataType: "html",
                data: {
                    opcion: 7                   
                },
                success: function (resp) {
                    $("#imgloadEx").hide();
                    $("#msjEx").hide();
                    $("#respEx").show();
                    var boton = "<div style='text-align:center'>\n\ </div>";                   
                    document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;


                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);

  
}


function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}
function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function deshabilitarPanelBusqueda(status) {

    var nodes = document.getElementById("busqueda").getElementsByTagName('*');
    for (var i = 0; i < nodes.length; i++)
    {
        nodes[i].disabled = status;
    }

}