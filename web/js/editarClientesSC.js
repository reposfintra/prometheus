/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {

    cargarDepartamentos('cooddpto');
    cargarDepartamentos('dep_dir');
    $('#dep_dir').val('ATL');
    cargarCiudad('ATL', "ciu_dir");
    $('#ciu_dir').val('BQ');
    cargarVias('BQ', "via_princip_dir");
    cargarVias('BQ', "via_genera_dir");

    //agregar eventos onchange
    $("#cooddpto").change(function() {
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "codciu");
    });
    $("#dep_dir").change(function() {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciu_dir");
    });
    $("#ciu_dir").change(function() {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarVias(op, "via_princip_dir");
        cargarVias(op, "via_genera_dir");
    });
    $("#via_princip_dir").change(function() {
          $("#via_genera_dir").val('');
    });
});

    function buscarCliente() {
        limpiar();
        if (document.getElementById("nit").value != '') {
            var url = "./controller?estado=Clientefen&accion=Update&op=3";
            var p = "nit=" + document.getElementById("nit").value;
            new Ajax.Request(url, {
                parameters: p,
                asynchronous: false,
                method: 'post',
                onComplete: function(resp) {
                    var datos = resp.responseText;

                    if (trim(datos) != 'N') {
                        var array = datos.split(";");
                        cargarCiudad(array[8], "codciu");
                        document.getElementById("codigo").value = array[0];
                        document.getElementById("name").value = array[1];
                        document.getElementById("direccion").value = array[3];
                        document.getElementById("telefono").value = array[4];
                        document.getElementById("celular").value = array[5];
                        document.getElementById("barrio").value = array[6];
                        document.getElementById("codciu").value = array[7];
                        document.getElementById("cooddpto").value = array[8];

                    } else {

                        alert("No se encontraron resultados.")
                    }

                }
            });
        } else {

            alert("No se ha digitado un documento para consultar")
        }
    }
    
    function trim(cadena) {
        cadena = cadena.replace(/^\s+/, '').replace(/\s+$/, '');
        return(cadena);
    }
    
    function limpiar() {
        document.getElementById("codigo").value = "";
        document.getElementById("name").value = "";
        document.getElementById("direccion").value = "";
        document.getElementById("telefono").value = "";
        document.getElementById("celular").value = "";
        document.getElementById("barrio").value = "";
        document.getElementById("codciu").value = "";
        document.getElementById("cooddpto").value = "";

    }
            
 function guardar() {

    if (document.getElementById("nit").value !== '') {

        var nit = document.getElementById("nit").value;
        var codigo = document.getElementById("codigo").value;
        var name = document.getElementById("name").value;
        var direccion = document.getElementById("direccion").value;
        var telefono = document.getElementById("telefono").value;
        var celular = document.getElementById("celular").value;
        var barrio = document.getElementById("barrio").value;
        var codciu = document.getElementById("codciu").value;
        var cooddpto = document.getElementById("cooddpto").value;

        if (nit !== '' && codigo !== '' && name !== '' && direccion !== '' && telefono !== '' &&
            barrio !=='' && codciu !=='' && cooddpto !=='') {
        
            //mensajesDelSistema2("Espere un momento por favor...", "270", "140");
            $('#div_espera').fadeIn('slow');
            $.ajax({
                url: './controller?estado=Clientefen&accion=Update&op=4',
                data: {
                    nit: nit,
                    codigo: codigo,
                    name: name.toUpperCase(),
                    direccion: direccion.toUpperCase(),
                    telefono: telefono.toUpperCase(),
                    celular : celular.toUpperCase(),
                    barrio : barrio.toUpperCase(),
                    codciu: codciu,
                    cooddpto:cooddpto
                    
                },
                method: 'post',
                success: function(resp) {
                    var datos = resp;

                    if (trim(datos) !== 'N') {
                        
                         setTimeout(function() {
                           // $("#dialogo2").dialog("close"); 
                            $('#div_espera').fadeOut('slow');
                            limpiar();
                            document.getElementById("nit").value = "";
                            mensajesDelSistema("El cliente ha sido actualizado.","250","150");
                            $('#div_actualizar_datos').fadeOut('slow');
                         }, 1000);

                    } else {

                        mensajesDelSistema("No se pudo actualizar el cliente.","250","150");
                    }

                }
            });
        } else {

            mensajesDelSistema("Para modificar debes llenar todos los campos.","300","150");
        }

    } else {

        mensajesDelSistema("En este momento este boton no esta disponible.","300","150");

    }
    
}

          
function cargarDepartamentos(combo) {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructuracion&accion=Negocios",
        dataType: 'json',
        async:false,
        data: {
            opcion: 5
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#'+combo).append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#'+combo).append('<option value=' + key + '>' + json[key] + '</option>');

                    }

                } catch (exception) {
                    alert('error : ' + key + '>' + json[key][key]);
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!","300","150");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Reestructuracion&accion=Negocios",
            dataType: 'json',
            data: {
                opcion: 6,
                cod_ciudad: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '450', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function genDireccion(elemento,e) {
  
    var contenedor = document.getElementById("direccion_dialogo")
      , res = document.getElementById("dir_resul");
    $("#direccion_dialogo").draggable({ handle: "#drag_direcciones"});  
    document.getElementById('direccion_dialogo').style.left = '250px';
    document.getElementById('direccion_dialogo').style.top = '150px';
   // Posicionar_div("direccion_dialogo",e);  
    contenedor.style.display = "block";
    
    res.name = elemento;    
    res.value = (elemento.value) ? elemento.value : '';
    
}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3: 
            var res = document.getElementById('dir_resul')
              , des = document.getElementById(res.name);
              des.value = res.value;
        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");
            
            document.getElementById("direccion_dialogo").style.display = "none";
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
              , g = document.getElementById('via_genera_dir')
              , opcion;           
            for (var i = 0; i < g.length; i++) {
                
                opcion = g[i];
                
                if (opcion.value === p) {
                    
                    opcion.style.display = 'none';
                    opcion.disabled = true;
                    
                } else {
                    
                    opcion.disabled = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if(!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                || !jQuery('#placa_dir').val() ) {
                  jQuery('#dir_resul').val("");
                  jQuery('#dir_resul').attr("class","validation-failed");
              } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                    jQuery('#via_princip_dir option:selected').text()
                   + ' '  + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                   + ' '  + jQuery('#via_genera_dir option:selected').text().trim()
                   + ' '  + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                   + ((jQuery('#via_genera_dir option:selected').text().toUpperCase()==='#' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='TRANSVERSAL') ?  '-':  ' ')
                   + jQuery('#placa_dir').val().trim().toUpperCase()
                   + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
              }
            break;
    }
}


function cargarVias(codciu, combo) {
    //alert(codciu);

        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "/fintra/controller?estado=GestionSolicitud&accion=Aval",
            dataType: 'json',
            data: {
                opcion: 'cargarvias',
                ciu: codciu
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''></option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } 
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

}

 function resetAddressValues(){
       $("#dir_resul").val('');
       $("#nom_princip_dir").val('');
       $("#nom_genera_dir").val('');
       $("#placa_dir").val('');
       $("#cmpl_dir").val('');
       $("#via_princip_dir").val('');
       $("#via_genera_dir").val(''); 
 }

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function mensajesDelSistema2(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();
}

