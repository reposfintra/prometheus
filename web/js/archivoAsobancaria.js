/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 $(document).ready(function() {
    
         
    if ($('#div_qryrecaudo').is(':visible')) {    
        $("#startDatePicker").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
            maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
            defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
        });
        $("#endDatePicker").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
            maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
            defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
        });

        var myDate = new Date();
        $("#startDatePicker").datepicker("setDate", myDate);
        $("#endDatePicker").datepicker("setDate", myDate);
        $('#ui-datepicker-div').css('clip', 'auto');

        cargarEntidadesRecaudadoras();
    }
    

    $('#cargarArchivo').click(function(){
         cargarArchivoRecaudoAsobancaria();
    });
    
    $('#listarRecaudos').click(function() {
        if ($("#startDatePicker").val() <= $("#endDatePicker").val()) {
            listarRecaudoResumen();
        } else {             
            mensajesDelSistema('La fecha final no puede ser inferior a la inicial', '250', '175');
            $("#endDatePicker").datepicker("setDate", myDate);
        }
    });
    
    $('#archivo').change(function(){ 
       if($("#gview_tabla_cabecera_recaudo").length)  $('#tabla_cabecera_recaudo').jqGrid('GridUnload');;
       if($("#gview_tabla_detalle_recaudo").length)  $('#tabla_detalle_recaudo').jqGrid('GridUnload');
    });
    
 });
 
 function cargarEntidadesRecaudadoras() {
   
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Archivo&accion=Asobancaria",
        dataType: 'json',
        data: {
            opcion: 4
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#entidad_recaudo').append("<option value=''>Seleccione</option>");
               
                    for (var key in json) {
                        $('#entidad_recaudo').append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                  $('#entidad_recaudo').append("<option value=''>Seleccione</option>");
//                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
   
function cargarArchivoRecaudoAsobancaria() {
        
//      var extension = $('#archivo').val().split('.').pop().toLowerCase();
//      var filetype = document.getElementById('archivo').files[0].type;
        var fd = new FormData(document.getElementById('formulario'));
        var archivo = document.getElementById('archivo').value;
      
        if (archivo === "") {
            mensajesDelSistema("No ha seleccionado ning&uacute;n archivo. Por favor, seleccione uno!!", '250', '150');
            return;
        }
        var fsize=$('#archivo')[0].files[0].size;
        if (fsize > 4096 * 1024){
            $("#archivo").val("");
            mensajesDelSistema("Archivo no cargado. El tama&ntilde;o del archivo no puede ser superior a los 4MB", '250', '175');
            return;
        }
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function(){
        $.ajax({
            async: false,
            url: "./controller?estado=Archivo&accion=Asobancaria&opcion=1",
            type: 'POST',
            dataType: 'json',
            data: fd,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(json) {
                $("#archivo").val("");
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        if (json.idRecaudo > 0) {
                            listarRecaudoResumen(json.idRecaudo);
                            listarRecaudoDetalle(json.idRecaudo, json.tipo);
                        }
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema(json.error, '250', '175');
                        return;
                    }

                    if (json.respuesta === "OK") {                       
                        listarRecaudoResumen(json.idRecaudo);
                        listarRecaudoDetalle(json.idRecaudo, json.tipo);
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Archivo cargado exitosamente", '250', '150', true);                       
                        return;
                    }

                } else {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci&oacute;n!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
        },500);
}

function ventanaRecaudoDetalle() {
    
    $("#dialogRecaudoDet").dialog({
        width: 785,
        height: 600,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: true, 
        buttons: {          
            "Salir": function () {          
		$(this).dialog("close");               
            }
        }
    });
}

function verInformacionPago(cl){
    
    $('#div_recaudo_info_proc').fadeIn("slow");  
    listarInfoPagoExtracto(cl);
    ventanaVerInformacionPago();
   
}

function ventanaVerInformacionPago() { 
    $("#div_recaudo_info_proc").dialog({        
        width: 910,
        height: 640,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:"Informaci&oacute;n de pago extracto",
        closeOnEscape: true,       
        buttons: {          
            "Salir": function () {          
		$(this).dialog("close");               
            }
        }
    });
}

function listarRecaudoResumen(idRecaudo) {
    var jsonParam;
    if (typeof idRecaudo !== 'undefined') {
        jsonParam = {opcion: 5, idRecaudo: idRecaudo};
    } else {
        var fechaini = $("#startDatePicker").val();
        var fechafin = $("#endDatePicker").val();
        var entidadRecaudo = $("#entidad_recaudo").val();
        var referencia = $("#referencia_factura").val();
        jsonParam = {opcion: 2, fechaini: fechaini, fechafin: fechafin, entidad_recaudadora: entidadRecaudo, referencia: referencia};
    }
    
    var grid_listar_cabecera_recaudo = $("#tabla_cabecera_recaudo");
  
    if ($("#gview_tabla_cabecera_recaudo").length) {
        reloadGridListarRecaudoResumen(grid_listar_cabecera_recaudo,jsonParam);
    } else {

        grid_listar_cabecera_recaudo.jqGrid({
            caption: "Informaci&oacute;n de recaudo",
            url: "./controller?estado=Archivo&accion=Asobancaria",
            mtype: "POST",	 
            datatype: "json",           
            height: $("#gview_tabla_cabecera_recaudo").height(),
            width: '1120',          
            colNames: ['Id', 'Nit facturadora', 'Entidad Recaudadora', 'Fecha Cargue', 'Fecha Recaudo', '# Registros', '# Encontrados', '# Procesados', 'Total Aplicado', 'Total Recaudo', 'Num lotes'],
            colModel: [
                {name: 'id', index: 'id', width: 60, resizable:false, sortable: true, align: 'center', key: true},
                {name: 'facturadora_nit', index: 'facturadora_nit', resizable:false, width: 100, align: 'center'},
                {name: 'entidad_recaudo', index: 'entidad_recaudo', resizable:false, sortable: true, width:'310', align:'center'},
                {name: 'fecha_creacion', index: 'fecha_creacion', resizable:false, sortable: true,  width: 100, align: 'center'},
                {name: 'fecha_recaudo', index: 'fecha_recaudo', resizable:false, sortable: true,  width: 100, align: 'center'},  
                {name: 'total_registros', index: 'total_registros', resizable:false, sortable: true, width: 80, align: 'center'},
                {name: 'total_encontrados', index: 'total_encontrados', resizable:false, sortable: true, width: 85, align: 'center'},
                {name: 'total_procesados', index: 'total_procesados', resizable:false, sortable: true, width: 85, align: 'center'},
                {name: 'valor_aplicado', index: 'valor_aplicado', resizable:false, sortable: true, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_total', index: 'valor_total', resizable:false, sortable: true, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'num_lotes', index: 'num_lotes', hidden:true,  width: 50}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,           
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: { 
                async:false,
                dataType: "json",
                type: "POST",                
                data: jsonParam
            }
            ,ondblClickRow: function(rowid) {  
                   if ($('#div_qryrecaudo').is(':visible')) {    
                        var sel_id = grid_listar_cabecera_recaudo.jqGrid('getGridParam', 'selrow'); 
                        var numLotes = $("#tabla_cabecera_recaudo").getRowData(sel_id).num_lotes;
                        var tipo = (Number(numLotes) === 0) ? "ASO98" : "ASO2001-09";
                        ventanaRecaudoDetalle();
                        listarRecaudoDetalle(sel_id, tipo); 
                   }                              
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });

    }

}

function reloadGridListarRecaudoResumen(grid_listar_cabecera_recaudo, params) {
    grid_listar_cabecera_recaudo.setGridParam({
        url: "./controller?estado=Archivo&accion=Asobancaria",
        datatype: 'json',
        ajaxGridOptions: {
            async:false,
            type: "POST",
            data: params
        }
    });

    grid_listar_cabecera_recaudo.trigger("reloadGrid");
}

function listarRecaudoDetalle(idRecaudo, tipo) {    
 
    var grid_listar_detalle_recaudo = $("#tabla_detalle_recaudo");
   
    if ($("#gview_tabla_detalle_recaudo").length) {    
          reloadGridListarRecaudoDetalle(grid_listar_detalle_recaudo,idRecaudo,tipo);
    } else {      
        grid_listar_detalle_recaudo.jqGrid({
            url: "./controller?estado=Archivo&accion=Asobancaria&opcion=3&id="+idRecaudo+"&tipoasobanc="+tipo,
            type: "POST",
            datatype: "json",
            async: false,           
            height: $("#gview_tabla_detalle_recaudo").height(),   
            width: '540',
            scrollOffset:30,
            colNames: ['Id', 'Referencia', 'Negocio', 'Procedencia pago', 'Medio pago', 'Valor recaudado', 'Encontrado', 'Proc. cartera', 'Causal dev'],
            colModel: [
                {name: 'id', index: 'id', width: 80, hidden:true, sortable: true, align: 'center', key: true},
                {name: 'referencia_factura', index: 'referencia_factura', width: 110, align: 'center', searchoptions: {clearSearch: false }},
                {name: 'negocio', index: 'negocio', width: 90, align: 'center', searchoptions: {clearSearch: false }},
                {name: 'procedencia_pago', index: 'procedencia_pago', sortable: true, width: 180, align: 'center',searchoptions: {clearSearch: false }},
                {name: 'medio_pago', index: 'medio_pago', hidden: true, sortable: true, width: 100, align: 'center'},
                {name: 'valor_recaudado', index: 'valor_recaudado', sortable: true, width: 100, align: 'right', sorttype: 'currency', searchoptions: {clearSearch: false },
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, searchoptions: {clearSearch: false }, prefix: "$ "}},
                {name: 'encontrado', index: 'encontrado', resizable:false, sortable: true, width: 80, align: 'center', searchoptions: {clearSearch: false }},
                {name: 'procesado_cartera', index: 'procesado_cartera', resizable:false, sortable: true, width: 80, align: 'center', searchoptions: {clearSearch: false }},
                {name: 'causal_devolucion_proc', index: 'causal_devolucion_proc', resizable:false, hidden: true, width: 80, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            ignoreCase: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function() {
                var numrecords = $("#tabla_detalle_recaudo").getGridParam('records');
                if (numrecords > 20)
                {
                    $('#tabla_detalle_recaudo').setGridWidth($('#tabla_detalle_recaudo').width() + 15);
                }
                else
                {
                    $('#tabla_detalle_recaudo').setGridWidth($('#tabla_detalle_recaudo').width());
                }    
               
                $('#tabla_detalle_recaudo').contextMenu('myMenu1', {
                    bindings: {
                        ver_detalle_pago: function(trigger, currentTarget) {
                           var rowKey = $('#tabla_detalle_recaudo').getGridParam("selrow");
                           var fila = jQuery("#tabla_detalle_recaudo").getRowData(rowKey);
                           var procesado = fila['procesado_cartera'];
                           if (procesado === "Si") {
                                verInformacionPago(rowKey);
                           }                          
                        },
                        ver_causal_dev: function(trigger, currentTarget) {                             
                           var rowKey = $('#tabla_detalle_recaudo').getGridParam("selrow");
                           var fila = jQuery("#tabla_detalle_recaudo").getRowData(rowKey);
                           var encontrado = fila['encontrado'];
                           var procesado = fila['procesado_cartera'];
                           var causal = fila['causal_devolucion_proc'];
                                if (encontrado === "Si" && procesado === "No") {                                    
                                    if (causal !== "") {
                                        obtenerCausalDevolucion(causal);
                                    }
                                }
                        }
                    },
                    onContextMenu: function(event/*, menu*/) {                   
                        var rowId = $(event.target).closest("tr.jqgrow").attr("id");      
                        var encontrado = $("#tabla_detalle_recaudo").getRowData(rowId).encontrado;       
                        var procesado = $("#tabla_detalle_recaudo").getRowData(rowId).procesado_cartera;                      
                        // se deshabilita opcion menu para registros no procesados
                        $('#ver_detalle_pago').attr("disabled", procesado === "No");
                        if (procesado === "No") {
                            (encontrado==="Si") ? $('#ver_causal_dev').removeAttr("disabled").removeClass('ui-state-disabled') : $('#ver_causal_dev').attr("disabled", "disabled").addClass('ui-state-disabled');
                            $('#ver_detalle_pago').attr("disabled", "disabled").addClass('ui-state-disabled');
                        } else {        
                            $('#ver_causal_dev').attr("disabled", "disabled").addClass('ui-state-disabled');
                            $('#ver_detalle_pago').removeAttr("disabled").removeClass('ui-state-disabled');
                        }
                        return true;
                    }
                });
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
          jQuery("#tabla_detalle_recaudo").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,                   
                    multipleSearch: true
                });                    
     
        $("#tabla_detalle_recaudo").parents('div.ui-jqgrid-bdiv').css("max-height","430px");

    }

}

function reloadGridListarRecaudoDetalle(grid_listar_detalle_recaudo, idRecaudo, tipo) {
    grid_listar_detalle_recaudo.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Archivo&accion=Asobancaria&opcion=3&id=" + idRecaudo +"&tipoasobanc="+tipo
    });
    grid_listar_detalle_recaudo.trigger("reloadGrid");
}

function listarInfoPagoExtracto(idDetalleRecaudo) {
    
    var jsonParam = {opcion: 6, idDetRecaudo: idDetalleRecaudo};
        
    var grid_listar_info_pago = $("#tabla_info_pago_extracto");
  
    if ($("#gview_tabla_info_pago_extracto").length) {
        reloadGridListarInfoPagoExtracto(grid_listar_info_pago,jsonParam);
    } else {

        grid_listar_info_pago.jqGrid({        
            url: "./controller?estado=Archivo&accion=Asobancaria",
            mtype: "POST",	 
            datatype: "json",           
            height: $("#gview_tabla_info_pago_extracto").height(),
            width: '858',          
            colNames: ['Tipo', 'No ingreso', 'Fecha', 'Nit', 'Nombre', 'Cuenta', 'Vlr ingreso', 'Usuario crea'],
            colModel: [
                {name: 'valor_01', index: 'valor_01', width: 80, resizable:false, sortable: true, align: 'center'},
                {name: 'valor_02', index: 'valor_02', width: 80, resizable:false, sortable: true, align: 'center', key: true},
                {name: 'valor_03', index: 'valor_03', resizable:false, sortable: true, width: 90, align: 'center'},
                {name: 'valor_04', index: 'valor_04', resizable:false, sortable: true, width:90, align:'center'},
                {name: 'valor_05', index: 'valor_05', resizable:false, sortable: true,  width: 200},  
                {name: 'valor_06', index: 'valor_06', resizable:false, sortable: true,  width: 90},    
                {name: 'valor_07', index: 'valor_07', resizable:false, sortable: true, width: 100, align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'creation_user', index: 'creation_user', resizable:false, width: 90, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            subGrid: true,            
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {                
                dataType: "json",
                type: "POST",                
                data: jsonParam
            },           
            subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id = subgrid_id+"_t"; 
                jQuery("#"+subgrid_id).html("<table id='"+subgrid_table_id+"'></table>");
                jQuery("#"+subgrid_table_id).jqGrid({
                    url: './controller?estado=Archivo&accion=Asobancaria&opcion=7'
                        + '&num_ingreso='+row_id,
                    datatype: 'json',
                    colNames: ['Tipo', 'Documento', 'Doc. Afectado', 'Fecha Vence', 'Cuenta', 'Valor'],
                    colModel: [
                        {name: 'valor_01', index: 'valor_01', sortable: true, width: 60, align: 'center'},
                        {name: 'valor_02', index: 'valor_02', sortable: true, hidden: true, align: 'center'},
                        {name: 'valor_03', index: 'valor_03', sortable: true, align: 'center', key:true},
                        {name: 'valor_04', index: 'valor_04', sortable: true, width: 90, align: 'center'},
                        {name: 'valor_05', index: 'valor_05', sortable: true, width:100, align:'center'},
                        {name: 'valor_06', index: 'valor_06', sortable: true, width:100, align: 'right', sorttype: 'currency',
                         formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
                    ],
                    rowNum:150,
                    height:'80%',
                    width:'90%',
                    jsonReader: {
                        root: 'rows',
                        cell:'',
                        repeatitems: false,
                        id: '0'
                    },                    
                    loadError: function(xhr, status, error) {
                        alert(error);
                    }
                });               
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
        
         $("#tabla_info_pago_extracto").parents('div.ui-jqgrid-bdiv').css("max-height","480px");

    }

}

function reloadGridListarInfoPagoExtracto(grid_listar_info_pago, params) {
   grid_listar_info_pago.setGridParam({
            url: "./controller?estado=Archivo&accion=Asobancaria",       
            datatype: 'json',
            ajaxGridOptions: {
                type: "POST",
                data: params
            }            
        }).trigger("reloadGrid");

}

function obtenerCausalDevolucion(idCausal) {
   
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Archivo&accion=Asobancaria",
        dataType: 'json',
        data: {
            opcion: 8,
            idCausal:idCausal
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
               
                mostrarTexto(json.Causal,'Causal Devoluci&oacute;n','250', '180'); 

            } else {                
                
              mensajesDelSistema("Lo sentimos no se pudo obtener la causal!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
 
function mostrarTexto(texto, title, width, height) {   
    $("#msj").html(texto);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: title,
        closeOnEscape: false       
    });
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: "Mensaje",
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("close");             
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}