
function ReferenciaConductor(CONTROLLER, sw) {
	if(sw == 1){
		window.open(CONTROLLER+"?estado=Referencia&accion=Insert&tiporeferencia=CO&opcion=LANZAR&documento="+formulario.identificacion.value,'res','width=1000,height=800,scrollbars=yes,resizable=yes,top=10,left=65');	
    } else {
        window.open(CONTROLLER+"?estado=Referencia&accion=Search&tipo=CO&documento="+formulario.identificacion.value,'res','width=1000,height=800,scrollbars=yes,resizable=yes,top=10,left=65');
	}
}

function ReferenciaPropietario(CONTROLLER, sw) {
	if(sw == 1){
		window.open(CONTROLLER+"?estado=Referencia&accion=Insert&tiporeferencia=PR&opcion=LANZAR&documento="+forma.c_nit.value,'res','width=1000,height=800,scrollbars=yes,resizable=yes,top=10,left=65');	
    } else {
        window.open(CONTROLLER+"?estado=Referencia&accion=Search&tipo=PR&documento="+forma.c_nit.value,'res','width=1000,height=800,scrollbars=yes,resizable=yes,top=10,left=65');
	}
}

function ReferenciaPlaca(CONTROLLER, sw) {
	if(frmplaca.placa.value==""){
		alert("Debe digitar una placa");
		frmplaca.placa.focus();
	} else {
		if (sw==1){
			window.open(CONTROLLER+"?estado=Referencia&accion=Insert&tiporeferencia=EA&opcion=LANZAR&documento="+frmplaca.placa.value,'res','width=1000,height=800,scrollbars=yes,resizable=yes,top=10,left=65');
		}
		else {
	        window.open(CONTROLLER+"?estado=Referencia&accion=Search&tipo=EA&documento="+frmplaca.placa.value,'res','width=1000,height=800,scrollbars=yes,resizable=yes,top=10,left=65');
		}
	}
}

function agregarReferencia(CONTROLLER) {	
     window.open(CONTROLLER+"?estado=Referencia&accion=Insert&tiporeferencia="+frmref.tipo.value+"&opcion=LANZAR&documento="+frmref.documento.value,'referencia','width=1000,height=800,scrollbars=yes,resizable=yes,top=10,left=65');	
}

function agregarReferenciaMod(CONTROLLER) {	
     window.open(CONTROLLER+"?estado=Referencia&accion=Insert&tiporeferencia="+frmref.tipo.value+"&opcion=LANZAR&documento="+frmref.documento.value+"&recargar=ok",'referencia','width=1000,height=800,scrollbars=yes,resizable=yes,top=10,left=65');	
}
function Otros(){
	 if(frmref.relacion.value == 'OTRO'){
		 frmref.otra_rel.style.visibility="visible";
		 frmref.img_otro.style.visibility="visible";
	 }
	 else{
  	   frmref.otra_rel.style.visibility="hidden";
	   frmref.img_otro.style.visibility="hidden";
	   frmref.otra_rel.value="";
	 }
}

function OtrosUPdate(campo,tipo){
	 var rel="relacion"+tipo+campo;
	 var otro="otra_rel"+tipo+campo;
     var imgotro="img_otro"+tipo+campo;	
	 
	 var objrel  = document.getElementById(rel);
	 var objotro = document.getElementById(otro);
	 var objimgotro = document.getElementById(imgotro);
	 
	 if(objrel.value == 'OTRO'){
   	     objotro.style.visibility="visible";		 
		 objimgotro.style.visibility="visible";
		 objotro.value="";
	 }
	 else{
  	     objotro.style.visibility="hidden";
		 objimgotro.style.visibility="hidden";
		 objotro.value=" ";
	 }
}

function validarReferencia(){
   var d = document;
   var refe = d.frmref;
   var numform = frmref.frm.value;
   var campos = '';
   var ncampos = '';
   if (numform==1){
       campos = new Array("nombre", "telefono",  "telciu", "contacto", "cargo", "referencia");   
       ncampos = new Array("nombre", "telefono",  "ciudad", "contacto", "cargo", "referencia");   
   }
   else if (numform==2){
	  if (frmref.nombreEC.value == "" &&  frmref.telefonoEC.value == "" && frmref.direccionEC.value == ""  && frmref.contactoEC.value == "" && frmref.cargoEC.value == "" && frmref.referenciaEC.value == ""){ 
	     if(frmref.relacion.value=="OTRO"){
            campos = new Array("nombre", "telefono",  "telciu", "relacion","otra_rel",  "referencia"); 
	        ncampos = new Array("nombre", "telefono",  "ciudad telefono", "relacion","otros",  "referencia");
		 }
		 else{
 		    campos = new Array("nombre", "telefono",  "telciu", "relacion",  "referencia");
            ncampos = new Array("nombre", "telefono",  "Ciudad telefono", "Relacion",   "Referencia");
		 }
	  }
	  else{
		  if(frmref.relacion.value=="OTRO"){
		  	campos = new Array("nombre", "telefono",  "telciu", "relacion","otra_rel",   "referencia", "nombreEC", "telefonoEC",  "telciuEC", "direccionEC", "ciudaddirEC", "contactoEC", "cargoEC", "referenciaEC");
            ncampos = new Array("nombre", "telefono",  "ciudad telefono",  "relacion","otros",   "referencia", "nombre empresa cargue", "telefono empresa cargue",  "ciudad empresa cargue", "direccion empresa cargue", "ciudad direccion empresa cargue", "contacto empresa cargue", "cargo empresa cargue", "referencia empresa cargue");
		  }
		  else{
			 campos = new Array("nombre", "telefono",  "telciu", "referencia", "relacion",  "nombreEC", "telefonoEC",  "telciuEC", "direccionEC", "ciudaddirEC", "contactoEC", "cargoEC", "referenciaEC");
             ncampos = new Array("nombre", "telefono",  "ciudad telefono", "referencia", "relacion", "nombre empresa cargue", "telefono empresa cargue",  "ciudad empresa cargue", "direccion empresa cargue", "ciudad direccion empresa cargue", "contacto empresa cargue", "cargo empresa cargue", "referencia empresa cargue");
		  }
          frmref.emp.value='OK'
	  }
   }
   else if (numform==3){
	  if(frmref.relacion.value=="OTRO"){
      	 campos = new Array("nombre", "telefono",  "telciu", "relacion","otra_rel",  "referencia");
		 ncampos = new Array("nombre", "telefono",  "ciudad telefono", "relacion","otros",  "referencia");
	  }
	  else{
		  campos = new Array("nombre", "telefono",  "telciu",  "relacion",  "referencia");   
		  ncampos = new Array("nombre", "telefono",  "ciudad telefono",  "relacion",  "referencia");   
	  }
   }
	   
   var i = 0;
   var campo = "";
   var  nom = ""
   for (i = 0; i < campos.length; i++){
      campo = campos[i];
      nom = ncampos[i];  
	  if (refe.elements[campo].value == ""){
		  alert("El campo " + nom + " esta vacio!");
		  refe.elements[campo].focus();       
		  return false;
		  break;
	  }
   }
   refe.submit();

}


/*******************/
function modReferenciaPlaca(CONTROLLER) {
    window.open(CONTROLLER+"?estado=Referencia&accion=Search&tipo=EA&documento="+frmplaca.placa.value,'refplaca','width=1000,height=600,scrollbars=yes,resizable=yes,top=10,left=65');
}
function modReferenciaConductor(CONTROLLER) {
	window.open(CONTROLLER+"?estado=Referencia&accion=Search&tipo=CO&documento="+formulario.identificacion.value,'refCond','width=1000,height=800,scrollbars=yes,resizable=yes,top=10,left=65');
}
function modReferenciaPropietario(CONTROLLER) {
	window.open(CONTROLLER+"?estado=Referencia&accion=Search&tipo=PR&documento="+frmprop.c_nit.value,'refPro','width=1000,height=600,scrollbars=yes,resizable=yes,top=10,left=65');
}
function abrirVent(url){
    window.open(url);
}


function TCamposReferencia(form){
 for (i = 0; i < form.elements.length; i++){
    if (form.elements[i].saltar!='yes' && form.elements[i].value == ""){
      alert("No se puede procesar la informacion. Verifique que todos lo campos esten llenos.");
      form.elements[i].focus();
      return (false);
    }
 }
 form.submit();
}
