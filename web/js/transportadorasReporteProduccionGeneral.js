/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $("#fechainicio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafinal").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $("#fechainicio").datepicker("setDate", myDate);
    $("#fechafinal").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    cargarTransportadora();

    $("#buscarfiltro").click(function () {
        // alert("hola");
        cargarTransproduccion();                            
    });
});



function cargarTransportadora() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            //  if (!isEmptyJSON(json)) {
            if (json.error) {
                return;
            }
            try {
                $('#transportadora').empty();
                for (var key in json) {
                    $('#transportadora').append('<option value=' + json[key].id + '>' + json[key].razon_social + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
            //  }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTransproduccion() {
    var grid_tabla_transportadora = jQuery("#tabla_reporte_transportadoras");
    if ($("#tabla_reporte_transportadoras tbody").children().length) {
        reloadGridtransportadora(grid_tabla_transportadora, 18);
    } else {
        grid_tabla_transportadora.jqGrid({
            caption: "REPORTE PRODUCCION TRANSPORTADORAS",
            url: "./controller?estado=Administracion&accion=Logistica",
            mtype: "POST",
            datatype: "json",
            height: '520',
            width: '1425',
            colNames: ['Id', 'Estado', 'Observaci�n Anulaci�n', 'Producto', 'Nombre Agencia', 'Nit Condutor', 'Nombre Conductor', 'Veto', 'Veto Casual', 'Nit Propietario',
                'Nombre Propietario', 'Veto Propietario', 'Veto Casual Propietario', 'Placa', 'Planilla', 'Fecha Anticipo', 'Fecha Env�o', 'Fecha Creaci�n de Fintra', 'Reanticipo', 'Aprobado',
                'Usuario Creaci�n', 'Transferido', 'Banco Transferencia', 'Cuenta Transferencia', 'Tipo Cuenta Transferencia', 'Banco', 'Cuenta', 'Tipo Cuenta', 'Nombre Cuenta', 'Nit Cuenta',
                'Valor Manifiesto', 'Valor Anticipo', 'Total Descuento', 'Valor Anticipo con Descuento', 'Valor Comisi�n', 'Valor Consignado', 'Fecha Transferencia',
                'N�mero Egreso', 'Valor Egreso', 'Transportadora', 'Origen', 'Destino', 'Fecha Pago Fintra', '# Corrida'],
            colModel: [
                {name: 'id', index: 'id', width: 30, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estado', index: 'estado', width: 90, sortable: true, align: 'left'},
                {name: 'obs_anulacion', index: 'obs_anulacion', width: 300, sortable: true, align: 'left'},
                {name: 'producto', index: 'producto', width: 200, sortable: true, align: 'left'},
                {name: 'nombre_agencia', index: 'nombre_agencia', width: 150, sortable: true, align: 'left'},
                {name: 'nit_conductor', index: 'nit_conductor', width: 100, sortable: true, align: 'left'},
                {name: 'nombre_conductor', index: 'nombre_conductor', width: 200, sortable: true, align: 'left'},
                {name: 'veto', index: 'veto', width: 100, sortable: true, align: 'left'},
                {name: 'veto_causal', index: 'veto_causal', width: 100, sortable: true, align: 'left'},
                {name: 'nit_propietario', index: 'nit_propietario', width: 100, sortable: true, align: 'left'},
                {name: 'nombre_propietario', index: 'nombre_propietario', width: 200, sortable: true, align: 'left'},
                {name: 'veto_propietario', index: 'veto_propietario', width: 100, sortable: true, align: 'left'},
                {name: 'veto_causal_propietario', index: 'veto_causal_propietario', width: 100, sortable: true, align: 'left'},
                {name: 'placa', index: 'placa', width: 100, sortable: true, align: 'left'},
                {name: 'planilla', index: 'planilla', width: 100, sortable: true, align: 'left'},
                {name: 'fecha_anticipo', index: 'fecha_anticipo', width: 200, sortable: true, align: 'left'},
                {name: 'fecha_envio', index: 'fecha_envio', width: 200, sortable: true, align: 'left'},
                {name: 'fecha_creacion_fintra', index: 'fecha_creacion_fintra', width: 200, sortable: true, align: 'left'},
                {name: 'reanticipo', index: 'reanticipo', width: 100, sortable: true, align: 'left'},
                {name: 'aprobado', index: 'reanticipo', width: 100, sortable: true, align: 'left'},
                {name: 'usuario_creacion', index: 'usuario_creacion', width: 100, sortable: true, align: 'left'},
                {name: 'transferido', index: 'transferido', width: 100, sortable: true, align: 'left'},
                {name: 'banco_transferencia', index: 'banco_transferencia', width: 150, sortable: true, align: 'left'},
                {name: 'cuenta_transferencia', index: 'cuenta_transferencia', width: 150, sortable: true, align: 'left'},
                {name: 'tipo_cuenta_transferencia', index: 'tipo_cuenta_transferencia', width: 100, sortable: true, align: 'left'},
                {name: 'banco', index: 'banco', width: 150, sortable: true, align: 'left'},
                {name: 'cuenta', index: 'cuenta', width: 150, sortable: true, align: 'left'},
                {name: 'tipo_cuenta', index: 'tipo_cuenta', width: 100, sortable: true, align: 'left'},
                {name: 'nombre_cuenta', index: 'nombre_cuenta', width: 100, sortable: true, align: 'left'},
                {name: 'nit_cuenta', index: 'nit_cuenta', width: 100, sortable: true, align: 'left'},
                {name: 'valor_manifiesto', index: 'valor_planilla', width: 100, sortable: true, align: 'left'},
                {name: 'valor_anticipo', index: 'valor_neto_anticipo', width: 120, sortable: true, align: 'left'},
                {name: 'total_dsct', index: 'total_dsct', width: 100, sortable: true, align: 'left'},
                {name: 'valor_anticipo_con_descuento', index: 'valor_neto_con_descuento', width: 150, sortable: true, align: 'left'},
                {name: 'valor_comision', index: 'valor_comision', width: 100, sortable: true, align: 'left'},
                {name: 'valor_consignado', index: 'valor_consignado', width: 100, sortable: true, align: 'left'},
                {name: 'fecha_transferencia', index: 'fecha_transferencia', width: 200, sortable: true, align: 'left'},
                {name: 'numero_egreso', index: 'numero_egreso', width: 100, sortable: true, align: 'left'},
                {name: 'valor_egreso', index: 'valor_egreso', width: 100, sortable: true, align: 'left'},
                {name: 'transportadora', index: 'transportadora', width: 200, sortable: true, align: 'left'},
                {name: 'origen', index: 'origen', width: 200, sortable: true, align: 'left'},
                {name: 'destino', index: 'destino', width: 200, sortable: true, align: 'left'},
                {name: 'fecha_pago_fintra', index: 'fecha_pago_fintra', width: 150, sortable: true, align: 'left'},
                {name: 'nro_corrida', index: 'nro_corrida', width: 150, sortable: true, align: 'center'}
            ],
            rowNum: 10000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 27,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                data: {
                    opcion: 18,
                    transportadora: $("#transportadora").val(),
                    fecha_inicio: $("#fechainicio").val(),
                    fecha_fin: $("#fechafinal").val()
                }
            },
            loadError: function (xhr, status, error) {
                alert(error);
                //  mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_reporte_transportadoras").navButtonAdd('#pager', {
            caption: "Exportar Excel",
            onClickButton: function () {
                exportarExcel();
            }
        });
    }
}

function reloadGridtransportadora(grid_tabla_transportadora, opcion) {
    grid_tabla_transportadora.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Logistica",
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: opcion,
                transportadora: $("#transportadora").val(),
                fecha_inicio: $("#fechainicio").val(),
                fecha_fin: $("#fechafinal").val()
            }
        }
    });
    grid_tabla_transportadora.trigger("reloadGrid");
}

function exportarExcel() {
    var fullData = jQuery("#tabla_reporte_transportadoras").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 220,
        height: 150,
        title: 'Descarga Reporte Produccion'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Administracion&accion=Logistica",
        data: {
            listado: myJsonString,
            opcion: 19
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}