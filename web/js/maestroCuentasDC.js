/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    //autocompletarCuenta();
    cargarModulos_1();
    cargarCuentas();

    $('#buscar_2').click(function () {
        cargarInfoCuenta();
    });
    $('#buscar_').click(function () {
        cargarCuentas();
    });
});

function autocompletarCuenta() {
    $("#cuenta").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Admin&accion=Fintra",
                dataType: "json",
                data: {
                    informacion_: request.term,
                    opcion: 53
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.value
//                            mivar1: item.mivar1
                        };
                    }));
                }
            });
        },
        minLength: 3,
        delay: 500,
        disabled: false,
        select: function (event, ui) {
            $("#cuenta").val(ui.item.value);
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar1 :
                    "Nothing selected, input was " + ui.item.label);
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}



function cargarCuentas() {
    $('#tabla_cuentas').jqGrid('GridUnload');
    var grid_tabla_ = jQuery("#tabla_cuentas");
    if ($("#gview_tabla_cuentas").length) {
        reloadGridMostrar(grid_tabla_, 54);
    } else {
        grid_tabla_.jqGrid({
            caption: "Cuentas",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            width: '970',
            colNames: ['Id', 'Estado', 'Cuenta', 'Nombre Cuenta', 'Codigo', 'Modulo', 'Paso', 'Clasificacion','Linea Negocio', 'Eliminar'],
            colModel: [
                {name: 'id', index: 'id', width: 100, align: 'center', hidden: true, sortable: true},
                {name: 'estado', index: 'estado', width: 100, align: 'center', hidden: true, sortable: true},
                {name: 'cuenta', index: 'cuenta', width: 200, align: 'left', search: true, sortable: true},
                {name: 'nombre_cuenta', index: 'nombre_cuenta', width: 300, align: 'left', hidden: false, search: true, sortable: true},
                {name: 'codigo', index: 'codigo', width: 100, align: 'left', hidden: true, sortable: true},
                {name: 'modulo', index: 'modulo', width: 290, align: 'left', search: true, sortable: true},
                {name: 'paso', index: 'paso', width: 100, align: 'left', search: true, sortable: true},
                {name: 'clasificacion', index: 'clasificacion', width: 340, align: 'left', search: true, sortable: true},
                {name: 'linea_negocio', index: 'linea_negocio', width: 300, align: 'left', search: true, sortable: true},
                {name: 'accion', index: 'accion', width: 130, align: 'left',hidden: true,sortable: false}
            ],
            rownumbers: true,
            rownumWidth: 20,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            pager: '#pager',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            sortname: 'id',
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 54,
                    modulo: $('#modulo').val(),
                    cuenta: $('#cuenta').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function (id, rowid) {
            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_cuentas").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_cuentas").getRowData(cant[i]).accion;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='accion' " + cambioEstado + " > <label onclick=\"CambiarEstado('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_cuentas").jqGrid('setRowData', cant[i], {accion: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_cuentas"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var cuenta = filas.cuenta;
                var nombre_cuenta = filas.nombre_cuenta;
                var codigo = filas.codigo;
                var paso = filas.paso;
                var clasificacion = filas.clasificacion;
                ventanaGuardar('EDITAR', id, cuenta, nombre_cuenta, codigo, paso, clasificacion);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        grid_tabla_.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
        $("#tabla_cuentas").navButtonAdd('#pager', {
            caption: "Nuevo",
            id: 'nuevo',
            onClickButton: function () {
                ventanaGuardar('GUARDAR');
            }
        });
        $("#tabla_cuentas").navButtonAdd('#pager', {
            caption: "Eliminar",
            id: 'eliminar',
            onClickButton: function () {
                var infoid = jQuery('#tabla_cuentas').jqGrid('getGridParam', 'selarrrow');
                if (infoid.length > 0) {
                    eliminarMultiple();
                } else {
                    mensajesDelSistema("Para eliminar debe seleccionar fila", '314', '140', false);
                }
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                modulo: $('#modulo').val(),
                cuenta: $('#cuenta').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center", modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear botón cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function CambiarEstado(rowid) {
    var grid_tabla = jQuery("#tabla_cuentas");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        data: {
            opcion: 55,
            id: id,
            accion_:'uno_uno'
        },
        success: function (json) {
            console.log(json.respuesta);
            if (json.respuesta === 'Guardado') {
                cargarCuentas();
            }
        }
    });
}

function ventanaGuardar(operacion, id, cuenta, nombre_cuenta, codigo, paso, clasificacion) {
    s = funcionValidacionJson();
    if (operacion === 'GUARDAR') {
        cargarModulos_2();
        $('#buscar_2').show();
        $('#mostrarlabel').show();
        $('#mostrar').show();
        $('#cuenta_').attr('readonly', false);
        $("#dialogMsguardar").dialog({
            width: '667',
            height: '340',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'Guardar Cuenta',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarCuenta();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    s.limpiarCampos();
                }
            }
        });
    } else if (operacion === 'EDITAR') {
        $('#buscar_2').hide();
        $('#mostrarlabel').hide();
        $('#mostrar').hide();
        $('#id_').val(id);
        $('#cuenta_').val(cuenta);
        $('#cuenta_').attr('readonly', true);
        $('#nombre_cuenta_').val(nombre_cuenta);
        cargarModulos_2();
        $('#modulo_').val(codigo);
        $('#paso_').val(paso);
        $('#clasificacion_').val(clasificacion);
        $("#dialogMsguardar").dialog({
            width: '669',
            height: '340',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'Actualizar Cuenta',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    acualizarCuenta();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    s.limpiarCampos();
                }
            }
        });
    }

}

function cargarInfoCuenta() {
    $.ajax({
        async: false,
        url: "./controller?estado=Admin&accion=Fintra",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 56,
            cuenta: $('#cuenta_').val()
        },
        success: function (json) {
            $('#cuenta_').val(json[0].cuenta);
            $('#nombre_cuenta_').val(json[0].nombre_largo);
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function funcionValidacionJson() {
    return {
        requeridos: function () {
            var vacios = 0;
            $(".requerido").each(function () {
                if (this.value === '') {
                    vacios = vacios + 1;
                }
            });
            return vacios;
        },
        informacion: function () {
            var json = [];
            var llave = {};
            var vacios = this.requeridos();
            if (vacios === 0) {
                $("#dialogMsguardar").find(':input').each(function () {
                    var elemento = this;
                    if (elemento.type !== 'button') {
                        if (elemento.type === 'checkbox') {
                            if ($("#" + elemento.id).is(':checked')) {
                                llave[elemento.id] = 'S';
                            } else {
                                llave[elemento.id] = 'N';
                            }
                        } else {
                            llave[elemento.id] = elemento.value;
                        }
                    }
                });
                json.push(llave);
            }
            return  json;
        },
        limpiarCampos: function () {
            $("#dialogMsguardar").find(':input').each(function () {
                var elemento = this;
                if (elemento.id !== 'buscar_2') {
                    $('#' + elemento.id).val('');
                }
            });
            $("#dialogMsguardar").find(':checkbox').each(function () {
                var elemento = this;
                $('#' + elemento.id).attr('checked', false);
            });
        }
    };
}

function  guardarCuenta() {
    s = funcionValidacionJson();
    var json = s.informacion();
    console.log(json);
    if (json.length === 0) {
        mensajesDelSistema('Falta información por ingresar', '370', 'auto', false);
    } else {
        $.ajax({
            async: false,
            url: "./controller?estado=Admin&accion=Fintra",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 57,
                informacion: JSON.stringify({json: json})
            },
            success: function (json) {
                if (json.respuesta === 'Guardado') {
                    s.limpiarCampos();
                    $('#dialogMsguardar').dialog("close");
                    mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);
                    cargarCuentas();
                } else {
                    mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function  acualizarCuenta() {
    s = funcionValidacionJson();
    var json = s.informacion();
    //console.log(json);
    if (json.length === 0) {
        mensajesDelSistema('Falta información por ingresar', '370', 'auto', false);
    } else {
        $.ajax({
            async: false,
            url: "./controller?estado=Admin&accion=Fintra",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 65,
                informacion: JSON.stringify({json: json})
            },
            success: function (json) {
                if (json.respuesta === 'Guardado') {
                    s.limpiarCampos();
                    $('#dialogMsguardar').dialog("close");
                    cargarCuentas();
                } else {
                    mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function cargarModulos_1() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 72
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#modulo').html('');
                $('#modulo').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#modulo').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarModulos_2() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 72
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#modulo_').html('');
                $('#modulo_').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#modulo_').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function eliminarMultiple() {
    var infoid = jQuery('#tabla_cuentas').jqGrid('getGridParam', 'selarrrow');
    var json = [];
    for (var i = 0; i < infoid.length; i++) {
        var llave = {};
        var id = jQuery("#tabla_cuentas").getRowData(infoid[i]).id;
        llave['id'] = id;
        json.push(llave);
    }
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        async: false,
        data: {
            opcion: 55,
            accion_:'multiple',
            info: JSON.stringify({json: json})
        },
        success: function (json) {
            console.log(json.respuesta);
            if (json.respuesta === 'Guardado') {
                cargarCuentas();
            }

        }
    });
}



