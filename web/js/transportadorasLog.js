/* 
 * transportadorasLog
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//$(document).ready(function () {
var usus;
var validar;
function inicio1() {
    $("#fechainicio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafinal").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#buscarlog").click(function () {
        cargarLog();
    });
    $("#salirtrans").click(function () {
        window.close();
    });
    var myDate = new Date();
    $("#fechainicio").datepicker("setDate", myDate);
    $("#fechafinal").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    cargarTipo();
    // var perfilusu = obtenerPerfil();
    cargarEmpresas();
}
//});
function inicio2() {
    cargarProductosTrans();
}

function inicio3() {
    cargarTransportadoras();
    $("#cargar").click(function () {
        tablaRelacionProTran();
    });
    tablaRelacionProTran();
    $("#guardar").click(function () {
        guardarProducto();
    });
}
function inicio4() {
    cargarTipoDescuento();
}

function inicio5() {
    cargarGrillaTransportadoras();
}

function cargarHC() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 28
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#hc').html('');
                for (var datos in json) {
                    $('#hc').append('<option value="' + datos + '">' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTipo() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 0
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    //  mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#tipo').append("<option value=''>  </option>"); //primer elemento

                    for (var datos in json) {
                        $('#tipo').append('<option value=' + datos + '>' + json[datos] + '</option>');
                    }
                } catch (exception) {
                    //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
                }
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarEmpresas() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 3
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    //  mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    // if (perfil!=='TRANSPORTAD')  $('#empresa').append("<option value=''>  </option>");//primer elemento

                    for (var datos1 in json) {
                        $('#empresa').append('<option value=' + datos1 + '>' + json[datos1] + '</option>');
                    }
                } catch (exception) {
                    //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
                }
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mostrar(id) {
    ventana();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'text',
        data: {
            opcion: 2,
            id: id
        },
        success: function (json) {
            $('#texto').text(json);
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarLog() {
    var grid_mostrar_log = jQuery("#tabla_cargar_log");
    if ($("#gview_tabla_cargar_log").length) {
        reloadGridMostrarLog(grid_mostrar_log);
    } else {

        grid_mostrar_log.jqGrid({
            caption: "Mostrar Log",
            url: "./controller?estado=Administracion&accion=Etes",
            mtype: "POST",
            datatype: "json",
            height: '400',
            width: '1420',
            colNames: ['Id proceso', 'Empresa', 'Estado', 'Tipo', 'Excepcion', 'Observacion', 'Fecha Inicio', 'Fecha Final', 'Tiempo', 'Ver'],
            colModel: [
                {name: 'idproceso', index: 'idproceso', width: 60, sortable: true, align: 'center', key: true},
                {name: 'empresa', index: 'empresa', width: 100, align: 'center'},
                {name: 'estado', index: 'estado', width: 70, align: 'center'},
                {name: 'tipo', index: 'tipo', sortable: true, width: 90, align: 'left'},
                {name: 'excepcion', index: 'excepcion', width: 310, align: 'left'},
                {name: 'observacion', index: 'observacion', width: 310, align: 'left'},
                {name: 'fechainicio', index: 'fechainicio', sortable: true, width: 125, align: 'center'},
                {name: 'fechafin', index: 'fechafin', sortable: true, width: 125, align: 'center'},
                {name: 'tiempo', index: 'tiempo', sortable: true, width: 70, align: 'center'},
                {name: 'ver', index: 'ver', width: 100, align: 'center', sortable: false}
            ],
            gridComplete: function (id) {
                var cant = jQuery("#tabla_cargar_log").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cl = cant[i];
                    be = "<input style='height:20px;width:50px;' type='button' value='ver' onclick=\"mostrar('" + cl + "');\" />";
                    jQuery("#tabla_cargar_log").jqGrid('setRowData', cant[i], {ver: be});
                }
            },
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            //pager: '#page_tabla_negocios_rel',
            multiselect: false,
            multiboxonly: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                data: {
                    opcion: 1,
                    fechainicio: $("#fechainicio").val(),
                    fechafinal: $("#fechafinal").val(),
                    tipo: $("#tipo").val(),
                    empresa: $("#empresa").val(),
                    estadoT: $("#estadoT").val(),
                    planillaP: $("#planillaP").val()

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
        //$j("#tabla_negocios_rel").jqGrid('navGrid','#page_tabla_negocios_rel',{add:false,del:false,edit:false,position:'top'});
        // $("#cb_" + grid_listar_negocios_rel[0].id).hide();
    }
}

function descargarArchivo(contenidoT, nombreArchivo) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var save = document.createElement('a');
        save.href = event.target.result;
        save.target = '_blank';
        save.download = nombreArchivo || 'trama.dat';
        var clicEvent = new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': true
        });
        save.dispatchEvent(clicEvent);
        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    };
    reader.readAsDataURL(contenidoT);
}

function generarTexto(datos) {
    var texto = [];
    texto.push('Informacion Trama:\n');
    texto.push(datos);
    texto.push('\n');
    return new Blob(texto, {
        type: 'text/plain'
    });
}

function reloadGridMostrarLog(grid_mostrar_log) {
    grid_mostrar_log.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Etes",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 1,
                fechainicio: $("#fechainicio").val(),
                fechafinal: $("#fechafinal").val(),
                tipo: $("#tipo").val(),
                empresa: $("#empresa").val(),
                estadoT: $("#estadoT").val(),
                planillaP: $("#planillaP").val()
            }
        }
    });
    grid_mostrar_log.trigger("reloadGrid");
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function ventana() {

    $("#dialogMsj").dialog({
        width: '800',
        height: '600',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Descargar": function () {
                var datos = $('#texto').val();
                descargarArchivo(generarTexto(datos), 'archivo.txt');
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function obtenerPerfil() {
    var perfil = "";
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 4
        },
        success: function (json) {
            perfil = json.Perfil;
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return perfil.trim();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function mensajesDelSistema2(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function guardarProductoTrans() {
    var nomproducto = $("#nompro").val();
    if ((nomproducto != "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Etes",
            async: false,
            data: {
                opcion: 6,
                nomproducto: nomproducto
            },
            success: function (data) {
                cargarProductosTrans();
                $("#nompro").val("");
                mensajesDelSistema("EXITO AL GUARDAR PRODUCTO DE TRANSPORTADORA ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function cambiarEstadoPropTrans(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        data: {
            opcion: 7,
            id: id
        },
        success: function (data) {
            cargarProductosTrans();
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}

function cargarProductosTrans() {
    var grid_tabla_productos = jQuery("#tabla_productos");
    if ($("#gview_tabla_productos").length) {
        reloadGridMostrarProd(grid_tabla_productos, 5);
    } else {
        grid_tabla_productos.jqGrid({
            caption: "PRODUCTOS DE TRANSPORTADORAS",
            url: "./controller?estado=Administracion&accion=Etes",
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '605',
            colNames: ['id Producto', 'Cod Producto', 'Producto', 'Estado'],
            colModel: [
                {name: 'proId', index: 'proId', width: 120, sortable: true, align: 'left', hidden: false, key: true},
                {name: 'proCodProducto', index: 'proCodProducto', width: 130, sortable: true, align: 'left', hidden: false},
                {name: 'proDescripcion', index: 'proDescripcion', width: 200, sortable: true, align: 'left', hidden: false},
                {name: 'proEstado', index: 'proEstado', width: 120, sortable: true, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 5
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_productos").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                ventanaProductoTrans();
            }
        });
        $("#tabla_productos").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_productos"), i, selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.proId;
                var nombreprop = filas.proDescripcion;
                var estado = filas.proEstado;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "Activo") {
                        ventanaEditarproducto(nombreprop, id);
                    } else {
                        mensajesDelSistema("PARA EDITAR, EL ESTADO DEBE ESTAR ACTIVO", '300', '150', false);
                    }
                }
            }
        });
        $("#tabla_productos").navButtonAdd('#pager', {
            caption: "Activar / Inactivar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_productos"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.proId;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    ConfirmacionPro("�ESTA SEGURO QUE DESEA CAMBIAR EL ESTADO?", '300', '150', id);
                }
            }
        });
    }
}

function ventanaEditarproducto(nombreprop, id) {
    $("#nombreprop").val(nombreprop);
    $("#idpro").val(id);
    $("#dialogTransEditarProp").dialog({
        width: '560',
        height: '217',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                actualizarProductoTrans();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
    $("#dialogTransEditarProp").siblings('div.ui-dialog-titlebar').remove();
}


function ventanaProductoTrans() {

    $("#dialogMsjproducto").dialog({
        width: '460',
        height: '230',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Guardar": function () {
                guardarProductoTrans();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
    $("#dialogMsjproducto").siblings('div.ui-dialog-titlebar').remove();
}

function reloadGridMostrarProd(grid_tabla_productos, opcion) {
    grid_tabla_productos.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Etes",
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla_productos.trigger("reloadGrid");
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function ConfirmacionPro(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {
                cambiarEstadoPropTrans(id);
                $(this).dialog("destroy");
            },
            "Cancelar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function actualizarProductoTrans() {
    var nombreproducto = $("#nombreprop").val();
    var id = $("#idpro").val();
    if ((nombreproducto !== "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Etes",
            data: {
                opcion: 8,
                nombreproducto: nombreproducto,
                id: id
            },
            success: function (data) {
                cargarProductosTrans();
                $("#nombreprop").val("");
                mensajesDelSistema("EXITO AL ACTUALIZAR PRODUCTO ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function cargarTransportadoras() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 9
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    return;
                }
                try {
                    $('#transportadoras').append("<option value=''>  </option>");
                    for (var datos1 in json) {
                        $('#transportadoras').append('<option value=' + datos1 + '>' + json[datos1] + '</option>');
                    }
                } catch (exception) {
                }
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTipoDescuento() {
    var operacion;
    var grid_tabla_productos = jQuery("#tabla_tipo_decuento");
    if ($("#gview_tabla_tipo_decuento").length) {
        reloadGridMostrarProd(grid_tabla_productos, 13);
    } else {

        grid_tabla_productos.jqGrid({
            caption: "TIPO DESCUENTOS",
            url: "./controller?estado=Administracion&accion=Etes",
            mtype: "POST",
            datatype: "json",
            height: '220',
            width: '425',
            colNames: ['Id Descuento', 'Descuento', 'Estado'],
            colModel: [
                {name: 'tdid', index: 'tdid', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'tddescripcion', index: 'tddescripcion', width: 300, sortable: true, align: 'left', hidden: false},
                {name: 'tdestado', index: 'tdestado', width: 100, sortable: true, align: 'left', hidden: false}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 13
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_tipo_decuento").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 0;
                ventanaTipoDescuento(operacion);
            }
        });
        $("#tabla_tipo_decuento").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                operacion = 1;
                var myGrid = jQuery("#tabla_tipo_decuento"), i, selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.tdid;
                var nombretipodes = filas.tddescripcion;
                var estado = filas.tdestado;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "Activo") {
                        ventanaTipoDescuento(operacion, id, nombretipodes);
                        //ventanaEditarproducto(nombreprop, id);
                    } else {
                        mensajesDelSistema("PARA EDITAR , EL ESTADO DEBE ESTAR ACTIVO", '300', '150', false);
                    }
                }
            }
        });
        $("#tabla_tipo_decuento").navButtonAdd('#pager', {
            caption: "A / I",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_tipo_decuento"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.tdid;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    ConfirmacionTipoDes("�ESTA SEGURO QUE DESEA CAMBIAR EL ESTADO?", '300', '150', id);
                }
            }
        });
    }
}

function tablaRelacionProTran() {
    var grid_relacion_ProTrans = jQuery("#tabla_relacion");
    if ($("#gview_tabla_relacion").length) {
        grid_relacion_ProTrans.setGridParam({
            datatype: 'json',
            url: "./controller?estado=Administracion&accion=Etes&opcion=12&transid=" + $("#transportadoras").val(),
            ajaxGridOptions: {
                type: "GET",
                dataType: "json",
                async: false,
                data: {
                    opcion: 12,
                    transid: $("#transportadoras").val()
                }
            }, jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }
        });
        grid_relacion_ProTrans.trigger("reloadGrid");
        //reloadGridMostrarProd(grid_relacion_ProTrans, 12);
    } else {
        grid_relacion_ProTrans.jqGrid({
            caption: "Relacion Producto-Transportadora",
            url: "./controller?estado=Administracion&accion=Etes",
            mtype: "POST",
            datatype: "json",
            height: '400',
            width: '1345',
            colNames: ['Id', 'Id Producto', 'Producto', 'Id Descuento', 'Tipo Descuento', 'Descripcion', 'Descripcion Reporte Produccion', 'Porcentaje Descuento', 'Valor Descuento', 'Estado'],
            colModel: [
                {name: 'cpdid', index: 'cpdid', width: 100, editable: false, align: 'center', hidden: true},
                {name: 'proId', index: 'proId', width: 100, editable: false, align: 'center', hidden: true},
                {name: 'proDescripcion', index: 'proDescripcion', width: 200, sortable: true, editable: true,
                    edittype: 'select',
                    editoptions: {
                        value: ajaxSelect,
                        dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        var rowid = e.target.id.replace("_proDescripcion", "");
                                        jQuery("#tabla_relacion").jqGrid('setCell', rowid, 'proId', e.target.value);
                                    } catch (exc) {
                                    }
                                    return;
                                }}]}, align: 'center'},
                {name: 'tdid', index: 'tdid', width: 100, editable: false, align: 'center', hidden: true},
                {name: 'tddescripcion', index: 'tddescripcion', width: 200, editable: true, edittype: 'select',
                    editoptions: {
                        value: ajaxSelectTip,
                        dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        var rowid = e.target.id.replace("_tddescripcion", "");
                                        jQuery("#tabla_relacion").jqGrid('setCell', rowid, 'tdid', e.target.value);
                                    } catch (exc) {
                                    }
                                    return;
                                }}]}, align: 'center'},
                {name: 'cpddescripcion', index: 'cpddescripcion', width: 250, editable: true, align: 'center'},
                {name: 'cpddescripcioncorta', index: 'cpddescripcioncorta', sortable: true, editable: true, width: 250, align: 'left',
                    editoptions: {dataEvents: [{type: 'keyup', fn: function (e) { //console.log(e);
                                    try {
                                        var rowid = $("#" + e.target.id);
                                        rowid.val(rowid.val().replace(' ', ''));
                                    } catch (exc) {
                                    }
                                    return;
                                }}]}
                },
                {name: 'cpdporcent', index: 'cpdporcent', width: 150, editable: true, align: 'right', formatter: 'number',
                    editoptions: {dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        var rowid = e.target.id.replace("_cpdporcent", "");
                                        if (parseInt(e.target.value) === 0) {
                                            $("#" + rowid + '_cpdvalor').removeAttr('readonly');
                                        } else {
                                            $("#" + rowid + '_cpdvalor').attr('readonly', true);
                                            $("#" + rowid + '_cpdvalor').val('0.00');
                                        }
                                    } catch (exc) {
                                    }
                                    return;
                                }}]}},
                {name: 'cpdvalor', index: 'cpdvalor', width: 100, editable: true, align: 'right', formatter: 'number',
                    editoptions: {dataEvents: [{type: 'change', fn: function (e) { //console.log(e);
                                    try {
                                        var rowid = e.target.id.replace("_cpdvalor", "");
                                        if (parseInt(e.target.value) === 0) {
                                            $("#" + rowid + '_cpdporcent').removeAttr('readonly');
                                        } else {
                                            $("#" + rowid + '_cpdporcent').attr('readonly', true);
                                            $("#" + rowid + '_cpdporcent').val('0.00');
                                        }
                                    } catch (exc) {
                                    }
                                    return;
                                }}]}},
                {name: 'cpdestado', index: 'cpdestado', width: 100, editable: false, align: 'center'}
            ],
            cellEdit: false,
            rowNum: 1000,
            //rowTotal: 1000,
            loadonce: true,
            rownumWidth: 35,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            restoreAfterError: true,
            userDataOnFooter: true,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            loadComplete: function (rowid, e, iRow, iCol) {
                $("#tabla_relacion").contextMenu('menu', {
                    bindings: {
                        eliminar: function (rowid) {
                            var myGrid = jQuery("#tabla_relacion"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                            //$('#tabla_relacion tr:eq(' + selRowIds + ')').remove();
                            $('#tabla_relacion').jqGrid('delRowData', selRowIds);
                        }
                    }, onContexMenu: function (event/*, menu*/) {

                    }

                });
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_relacion"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var estado = filas.cpdestado;
                if (estado === "INACTIVO") {
                    mensajesDelSistema("El estado debe estar activo", '300', '150', false);
                } else {
                    var grid = jQuery("#tabla_relacion");
                    grid.jqGrid('editRow', rowid, true, function () {
                        $("input, select", e.target).focus();
                        var cpdvalor = filas.cpdvalor;
                        var cpdporcent = filas.cpdporcent;
                        if (cpdporcent !== '0.00') {
                            $("#" + rowid + '_cpdvalor').attr('readonly', true);
                            $("#" + rowid + '_cpdporcent').attr('readonly', false);
                        } else if (cpdvalor !== '0.00') {
                            $("#" + rowid + '_cpdporcent').attr('readonly', true);
                            $("#" + rowid + '_cpdvalor').attr('readonly', false);
                        }
                    });
                }
                return;
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_relacion").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                //  jQuery("#tabla_relacion").addRow(0, {});
                var transportadora = $('#transportadoras').val();
                if (transportadora === '') {
                    mensajesDelSistema("Debe seleccionar la transportadora", '268', '150', false);
                } else {
                    var grid = $("#tabla_relacion"), rowid = 'neo_' + grid.getRowData().length;
                    ;//rowid = 'neo_' + grid.getRowData().length;
                    grid.addRowData(rowid, {});
                    grid.jqGrid('editRow', rowid, true, function () {
                        // $("input, select", e.target).focus();
                    }, null, null, {}, null, null, function (id) {
                        var g = $('#tabla_relacion'), r = g.jqGrid('getLocalRow', id);
                        if (!r.id) {
                            g.jqGrid('delRowData', id);
                        }
                    });
                }
            }
        });
        $("#tabla_relacion").navButtonAdd('#pager', {
            caption: "Guardar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_relacion"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
                filas = myGrid.jqGrid("getLocalRow", selRowIds);

                var ids = grid_relacion_ProTrans.jqGrid('getDataIDs'), cant = 0;

                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var cpdvalor = grid_relacion_ProTrans.getRowData(cl).cpdvalor;
                    var cpdporcent = grid_relacion_ProTrans.getRowData(cl).cpdporcent;
                    if (((cpdvalor === '0.00') && (cpdporcent === '0.00')) || ((cpdvalor === '0') || (cpdporcent === '0')) || ((cpdvalor === cpdporcent))) {
                        cant = cant + 1;
                    }
                }
                if (cant > 0) {
                    //  alert(cant);
                    mensajesDelSistema("porcentaje descuento � valor descuento debe ser diferente de cero", '200', '150', false);
                } else {
                    guardarProducto();
                }
            }
        });
        $("#tabla_relacion").navButtonAdd('#pager', {
            caption: "Activar / Inactivar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_relacion"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.cpdid;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    ConfirmacionCambio("�Esta seguro que desea cambiar el estado?", '300', '150', id);
                }
            }
        });
    }

}

function guardarProducto() {
    var grid = jQuery("#tabla_relacion"), filas = grid.jqGrid('getDataIDs');
    for (var i = 0; i < filas.length; i++) {
        console.log(filas[i]);
        grid.saveRow(filas[i]);
    }
    var trans = $('#transportadoras').val();
    var data = jQuery("#tabla_relacion").jqGrid('getRowData');
    $.ajax({
        url: "./controller?estado=Administracion&accion=Etes",
        datatype: 'json',
        type: 'get',
        data: {
            opcion: 10,
            informacion: JSON.stringify({transportadora: trans, datos: data})
        },
        async: false,
        success: function (data) {
            if (data.respuesta === 'ERROR') {
                mensajesDelSistema("Error, no puede registrar productos ya guardados ", '230', '150', true);
                tablaRelacionProTran();
            } else {
                tablaRelacionProTran();
                mensajesDelSistema("Exito al guardar", '175', '150', true);
            }
        }
    });
}


function ajaxSelect() {
//var sql = 'PRODUCTOS_ES';
    var resultado;
    $("#lui_tabla_relacion,#load_tabla_relacion").show();
    $.ajax({
        url: "./controller?estado=Administracion&accion=Etes",
        datatype: 'json',
        type: 'get',
        data: {
            opcion: 11
                    //, informacion:JSON.stringify({query:sql, filtros:[]})
        },
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    alert(json.error);
                    resultado = {};
                } else {
                    // console.log(json);
                    resultado = json;
                }
            } catch (exc) {
                //console.error(exc);
            } finally {
                $("#lui_tabla_relacion,#load_tabla_relacion").hide();
            }
        },
        error: function () {
            alert('error');
            $("#lui_tabla_relacion,#load_tabla_relacion").hide();
        }
    });
    return resultado;
}

function ajaxSelectTip() {
//var sql = 'PRODUCTOS_ES';
    var resultado;
    $("#lui_tabla_relacion,#load_tabla_relacion").show();
    $.ajax({
        url: "./controller?estado=Administracion&accion=Etes",
        datatype: 'json',
        type: 'get',
        data: {
            opcion: 14
        },
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    alert(json.error);
                    resultado = {};
                } else {
                    resultado = json;
                }
            } catch (exc) {
            } finally {
                $("#lui_tabla_relacion,#load_tabla_relacion").hide();
            }
        },
        error: function () {
            alert('error');
            $("#lui_tabla_relacion,#load_tabla_relacion").hide();
        }
    });
    return resultado;
}

function ConfirmacionCambio(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {
                cambiarEstadoRelTransPro(id);
                $(this).dialog("destroy");
            },
            "Cancelar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cambiarEstadoRelTransPro(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        data: {
            opcion: 15,
            id: id
        },
        success: function (data) {
            tablaRelacionProTran();
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}

function ventanaTipoDescuento(operacion, id, nombretipodes) {
    if (operacion == 0) {
        $("#dialogMsjtipodesc").dialog({
            width: '375',
            height: '230',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    guardarTipoDescuento();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    } else {
        if (operacion == 1) {
            $("#idtipdes").val(id);
            $("#nomtipdes").val(nombretipodes);
            $("#dialogMsjtipodesc").dialog({
                width: '375',
                height: '230',
                show: "scale",
                hide: "scale",
                resizable: false,
                position: "center",
                modal: true,
                closeOnEscape: false,
                buttons: {//crear bot�n cerrar
                    "Guardar": function () {
                        actualizarTipoDescuento();
                    },
                    "Salir": function () {
                        $(this).dialog("close");
                        $("#idtipdes").val("");
                        $("#nomtipdes").val("");
                    }
                }
            });
        }

    }

    $("#dialogMsjtipodesc").siblings('div.ui-dialog-titlebar').remove();
}

function guardarTipoDescuento() {
    var nomtipdes = $("#nomtipdes").val();
    if ((nomtipdes != "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Etes",
            async: false,
            data: {
                opcion: 16,
                nomtipdes: nomtipdes
            },
            success: function (data) {
                $("#nomtipdes").val("");
                cargarTipoDescuento();
                mensajesDelSistema("EXITO AL GUARDAR TIPO DE DESCUENTO", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function actualizarTipoDescuento() {
    var nombretipodes = $("#nomtipdes").val();
    var id = $("#idtipdes").val();
    if ((nombretipodes !== "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Etes",
            data: {
                opcion: 17,
                nombretipodes: nombretipodes,
                id: id
            },
            success: function (data) {
                cargarTipoDescuento();
                $("#nomtipdes").val("");
                mensajesDelSistema("EXITO AL ACTUALIZAR EL TIPO DE DESCUENTO ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function ConfirmacionTipoDes(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {
                cambiarEstadoTipoDes(id);
                $(this).dialog("destroy");
            },
            "Cancelar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cambiarEstadoTipoDes(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        data: {
            opcion: 18,
            id: id
        },
        success: function (data) {
            cargarTipoDescuento();
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}

function cargarGrillaTransportadoras() {
    var operacion;
    var grid_tabla_productos = jQuery("#tabla_transportadoras");
    if ($("#gview_tabla_transportadoras").length) {
        reloadGridMostrarProd(grid_tabla_productos, 19);
    } else {
        grid_tabla_productos.jqGrid({
            caption: "TRANSPORTADORAS",
            url: "./controller?estado=Administracion&accion=Etes",
            mtype: "POST",
            datatype: "json",
            height: '400',
            width: '1480',
            colNames: ['Id Trans', 'Cod Trans', 'Razon Social', 'Nit', 'Direccion', 'Correo', 'Id Periodicidad', 'Periodicidad', 'Cupo Rotativo', 'Clave', 'cod pais', 'cod departamento',
                'cod ciudad', 'Representante Legal', 'Documento Representante', 'Usuario', 'Estado', 'Hc', 'Tipo Documento', 'Cod Cliente', 'Calendario', 'Autorizar ventas', 'estado usuario', 'Autorizar Venta'],
            colModel: [
                {name: 'transid', index: 'transid', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'transcod', index: 'transcod', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'tranrazonsocial', index: 'tranrazonsocial', width: 200, sortable: true, align: 'left', hidden: false},
                {name: 'transnit', index: 'transnit', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'trandireccion', index: 'trandireccion', width: 200, sortable: true, align: 'left', hidden: false},
                {name: 'trancorreo', index: 'trancorreo', width: 200, sortable: true, align: 'left', hidden: false},
                {name: 'perid', index: 'perid', width: 200, sortable: true, align: 'left', hidden: true},
                {name: 'perdescripcion', index: 'perdescripcion', width: 150, sortable: true, align: 'left', hidden: true},
                {name: 'cupo_rotativo', index: 'cupo_rotativo', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'clave', index: 'clave', width: 200, sortable: true, align: 'left', hidden: true},
                {name: 'pais', index: 'pais', width: 200, sortable: true, align: 'left', hidden: true},
                {name: 'departamento', index: 'departamento', width: 200, sortable: true, align: 'left', hidden: true},
                {name: 'ciudad', index: 'ciudad', width: 200, sortable: true, align: 'left', hidden: true},
                {name: 'tranreplegal', index: 'tranreplegal', width: 200, sortable: true, align: 'left', hidden: false},
                {name: 'trandoc', index: 'trandoc', width: 150, sortable: true, align: 'left', hidden: true},
                {name: 'tranusuario', index: 'tranusuario', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'tranestado', index: 'tranestado', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'hc', index: 'hc', width: 120, sortable: true, align: 'center', hidden: true},
                {name: 'tipodoc', index: 'tipodoc', width: 120, sortable: true, align: 'center', hidden: true},
                {name: 'codcli', index: 'codcli', width: 120, sortable: true, align: 'center', hidden: true},
                {name: 'calendario', index: 'calendario', width: 90, sortable: true, align: 'center', hidden: false},
                {name: 'autoriza_ventas', index: 'autoriza_ventas', width: 90, sortable: true, align: 'center', hidden: true},
                {name: 'estado_usuario', index: 'estado_usuario', width: 90, sortable: true, align: 'center', hidden: true},
                {name: 'autoriza_venta', index: 'autoriza_venta', width: 90, sortable: true, align: 'center', hidden: false}


            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            gridComplete: function (id, rowid) {
                var cant = jQuery("#tabla_transportadoras").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var autoven = $("#tabla_transportadoras").getRowData(cant[i]).autoriza_venta;
                    //alert(autoven);
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='autoriza_venta' " + autoven + " > <label onclick=\"AutorizarVenta('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    cal = "<input style='height:20px;width:70px;' type='button' id='calendario_" + cl + "' value='calendario' onclick=\"jj('" + cl + "');\" />";
                    jQuery("#tabla_transportadoras").jqGrid('setRowData', cant[i], {calendario: cal, autoriza_venta: be});
                }
            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 19
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_transportadoras").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 0;
                ventanaTransportadora(operacion);
            }
        });
        $("#tabla_transportadoras").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                operacion = 1;
                var myGrid = jQuery("#tabla_transportadoras"), i, selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.transid;
                var nombretran = filas.tranrazonsocial;
                var nittran = filas.transnit;
                var direcciontran = filas.trandireccion;
                var correo = filas.trancorreo;
                var nombreencargado = filas.tranreplegal;
                var identificacion = filas.trandoc;
                var estado = filas.tranestado;
                var idusu = filas.tranusuario;
                var pas1 = filas.clave;
                var pais = filas.pais;
                var ciudad = filas.ciudad;
                var departamento = filas.departamento;
                var periodicidad = filas.perid;
                var hc = filas.hc;
                var tipodoc = filas.tipodoc;
                var codcli = filas.codcli;
                var cuporot = filas.cupo_rotativo;

                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "Activo") {
                        ventanaTransportadora(operacion, id, nombretran, nittran, direcciontran, correo, nombreencargado, identificacion, idusu, pas1, pais, ciudad, departamento, periodicidad, hc, tipodoc, codcli, cuporot);
                    } else {
                        mensajesDelSistema("PARA EDITAR LA EDS, EL ESTADO DEBE SER ACTIVO", '300', '150', false);
                    }
                }
            }
        });
        $("#tabla_transportadoras").navButtonAdd('#pager', {
            caption: "Activar / Inactivar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_transportadoras"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.transid;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    ConfirmacionTransportadora("�ESTA SEGURO QUE DESEA CAMBIAR EL ESTADO?", '300', '150', id);
                }
            }
        });
    }
}

function ventanaTransportadora(operacion, id, nombretran, nittran, direcciontran, correo, nombreencargado, identificacion, idusu, pas1, pais, ciudad, departamento, periodicidad, hc, tipodoc, codcli, cuporot) {

    if (operacion === 0) {
        cargarPais();
        cargarPeriodicidad();
        cargarHC();
        $("#dialogMsjtransportadora").dialog({
            width: '570',
            height: '580',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    guardarTransportadora();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    } else {
        if (operacion === 1) {
            $("#nittran").attr('readOnly', true);
            $("#idusu").attr('readOnly', true);
            $("#tipodoc").attr('disabled', true);
            $("#codcli").val(codcli);
            $("#iditusu").val(idusu);
            $("#idtran").val(id);
            $("#nombretran").val(nombretran);
            $("#nittran").val(nittran);
            $("#direcciontran").val(direcciontran);
            $("#correo").val(correo);
            $("#nombreencargado").val(nombreencargado);
            $("#identificacion").val(identificacion);
            $("#idusu").val(idusu);
            $("#pas1").val(pas1);
            $("#pas2").val(pas1);
            cargarPais();
            $("#pais").val(pais);
            cargarDepartamento();
            $("#departamento").val(departamento);
            cargarCiudad();
            $("#ciudad").val(ciudad);
            cargarPeriodicidad();
            $("#periodicidad").val(periodicidad);
            cargarHC();
            $("#hc").val(hc);
            $("#tipodoc").val(tipodoc);
            $("#cuporot").val(cuporot);
            $("#dialogMsjtransportadora").dialog({
                width: '570',
                height: '580',
                show: "scale",
                hide: "scale",
                resizable: false,
                position: "center",
                modal: true,
                closeOnEscape: false,
                buttons: {//crear bot�n cerrar
                    "Guardar": function () {
                        ActualizarTransportadora();
                    },
                    "Salir": function () {
                        $("#nittran").attr('readOnly', false);
                        $("#idusu").attr('readOnly', false);
                        $("#tipodoc").attr('disabled', false);
                        $(this).dialog("close");
                        $("#idtran").val("");
                        $("#nombretran").val("");
                        $("#nittran").val("");
                        $("#direcciontran").val("");
                        $("#correo").val("");
                        $("#nombreencargado").val("");
                        $("#identificacion").val("");
                        $("#idusu").val("");
                        $("#pas1").val("");
                        $("#pas2").val("");
                        $("#iditusu").val("");
                        $("#cuporot").val("");
                    }
                }
            });
        }
    }
    $("#dialogMsjtransportadora").siblings('div.ui-dialog-titlebar').remove();
}

function guardarTransportadora() {
    var nombretran = $("#nombretran").val();
    var nittran = $("#nittran").val();
    var direcciontran = $("#direcciontran").val();
    var correo = $("#correo").val();
    var nombreencargado = $("#nombreencargado").val();
    var identificacion = $("#identificacion").val();
    var idusu = $("#idusu").val();
    var pas1 = $("#pas1").val();
    var pas2 = $("#pas2").val();
    var pais = $("#pais").val();
    var departamento = $("#departamento").val();
    var ciudad = $("#ciudad").val();
    var cambioclav = $("#actdes").val();
    var periodicidad = $("#periodicidad").val();
    var hc = $("#hc").val();
    var tipodoc = $("#tipodoc").val();
    var cuporot = $("#cuporot").val();
    if ((nombretran !== "") && (nittran !== "") && (direcciontran !== "") && (correo !== "") && (nombreencargado !== "") && (identificacion !== "") && (idusu !== "") && (pas1 !== "") && (pas2 !== "") && (periodicidad !== "") && (cuporot !== "")) {
        expr = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!expr.test(correo)) {
            mensajesDelSistema("DEBE CAMBIAR EL CORREO", '230', '150', false);
        } else if (usus.trim() === "OK") {
            if ($("#dialogMsjSMS").html() === "") {
                $.ajax({
                    type: 'POST',
                    url: "./controller?estado=Administracion&accion=Etes",
                    data: {
                        opcion: 20,
                        nombretran: nombretran,
                        nittran: nittran,
                        direcciontran: direcciontran,
                        correo: correo,
                        nombreencargado: nombreencargado,
                        identificacion: identificacion,
                        idusu: idusu,
                        pas1: pas1,
                        pais: pais,
                        departamento: departamento,
                        ciudad: ciudad,
                        cambioclav: cambioclav,
                        periodicidad: periodicidad,
                        hc: hc,
                        tipodoc: tipodoc,
                        cuporot: cuporot
                    },
                    success: function (data) {
                        cargarGrillaTransportadoras();
                        $("#nombretran").val("");
                        $("#nittran").val("");
                        $("#direcciontran").val("");
                        $("#correo").val("");
                        $("#nombreencargado").val("");
                        $("#identificacion").val("");
                        $("#idusu").val("");
                        $("#pas1").val("");
                        $("#pas2").val("");
                        $("#cuporot").val("");
                        $("#cuporot").val("");
                        mensajesDelSistema("EXITO AL GUARDAR TRANSPORTADORA ", '230', '150', true);
                    }, error: function (result) {
                        alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
                    }
                });
            } else {
                mensajesDelSistema("LAS CONTRASE�AS SON DIFERENTES", '300', '150', true);
            }
        } else {
            mensajesDelSistema(" YA EXISTE", '230', '150', false);
        }
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function cargarPais() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 21
        },
        success: function (json) {
            if (json.error) {
                //mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#pais').html('');
                $('#pais').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#pais').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarDepartamento(id) {
    var pais = $("#pais").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 22,
            pais: pais
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#departamento').html('');
                $('#departamento').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#departamento').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarCiudad() {
    var departamento = $("#departamento").val();
    //var departamentoedit = $("#departamentoedit").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 23,
            departamento: departamento
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#ciudad').html('');
                $('#ciudad').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#ciudad').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function validarpass() {
    var clave = $("#pas1").val();
    var clave1 = $('#pas2').val();
    if (clave !== clave1) {
        $("#dialogMsjSMS").html("La contrase�a es diferente");
        return false;
    }

    $("#dialogMsjSMS").html("");
    return true;
}

function verificarUsuario() {
    var idusuario = $("#idusu").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        datatype: 'text',
        data: {
            opcion: 24,
            idusuario: idusuario
        },
        success: function (data) {
            if (data.trim() !== "OK") {
                $("#sms2").html(data);
                $("#sms2").html("");
            } else {
                $("#sms2").html("");

            }
            if (validar === 0) {
                usus === 'OK';
                $("#sms2").html("");
            } else {
                usus = data;
                $("#sms2").html(data);
            }
        }
    });
}

function verificarTrasportadora() {
    var nittrans = $("#nittran").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        datatype: 'text',
        data: {
            opcion: 38,
            nittrans: nittrans
        },
        success: function (data) {
            if (data.length > 1) {
                $("#idusu").val(data);
                $("#sms2").html("");
            } else {
                $("#sms2").html(data);
                $("#sms2").html("");
            }
        }
    });
}

function cargarPeriodicidad() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        dataType: 'json',
        async: false,
        data: {
            opcion: 25
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#periodicidad').html('');
                $('#periodicidad').append("<option value=''>  </option>");
                for (var datos in json) {
                    $('#periodicidad').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ConfirmacionTransportadora(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                cambiarEstadoTransportadoras(id);
                $(this).dialog("destroy");
            },
            "Cancelar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cambiarEstadoTransportadoras(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Etes",
        data: {
            opcion: 27,
            id: id
        },
        success: function (data) {
            cargarGrillaTransportadoras();
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}

function ActualizarTransportadora() {
    $("#nittran").attr('readOnly', true);
    $("#idusu").attr('readOnly', true);
    $("#tipodoc").attr('disabled', true);
    var id = $("#idtran").val();
    var codcli = $("#codcli").val();
    var iditusu = $("#iditusu").val();
    var nombretran = $("#nombretran").val();
    var nittran = $("#nittran").val();
    var direcciontran = $("#direcciontran").val();
    var correo = $("#correo").val();
    var nombreencargado = $("#nombreencargado").val();
    var identificacion = $("#identificacion").val();
    var idusu = $("#idusu").val();
    var pas1 = $("#pas1").val();
    var pas2 = $("#pas2").val();
    var pais = $("#pais").val();
    var departamento = $("#departamento").val();
    var ciudad = $("#ciudad").val();
    var cambioclav = $("#actdes").val();
    var periodicidad = $("#periodicidad").val();
    var hc = $("#hc").val();
    var tipodoc = $("#tipodoc").val();
    var cuporot = $("#cuporot").val();
    if ((nombretran !== "") && (nittran !== "") && (direcciontran !== "") && (correo !== "") && (nombreencargado !== "") && (identificacion !== "") && (idusu !== "") && (pas1 !== "") && (pas2 !== "") && (periodicidad !== "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Administracion&accion=Etes",
            data: {
                opcion: 26,
                nombretran: nombretran,
                nittran: nittran,
                direcciontran: direcciontran,
                correo: correo,
                nombreencargado: nombreencargado,
                identificacion: identificacion,
                idusu: idusu,
                pas1: pas1,
                pais: pais,
                departamento: departamento,
                ciudad: ciudad,
                cambioclav: cambioclav,
                periodicidad: periodicidad,
                iditusu: iditusu,
                hc: hc,
                tipodoc: tipodoc,
                codcli: codcli,
                id: id,
                cuporot: cuporot
            },
            success: function (data) {
                $("#nittran").attr('readOnly', false);
                $("#idusu").attr('readOnly', false);
                $("#tipodoc").attr('disabled', false);
                cargarGrillaTransportadoras();
                $("#nombretran").val("");
                $("#nittran").val("");
                $("#direcciontran").val("");
                $("#correo").val("");
                $("#nombreencargado").val("");
                $("#identificacion").val("");
                $("#idusu").val("");
                $("#pas1").val("");
                $("#pas2").val("");
                $("#cuporot").val("");
                mensajesDelSistema("EXITO ALACTUALIZAR TRANSPORTADORA ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function fechapago() {
    alert("en construccion ");
}

function AutorizarVenta(rowid) {
    var grid_tabla = jQuery("#tabla_transportadoras");
    var transid = grid_tabla.getRowData(rowid).transid;
    var idusuario = grid_tabla.getRowData(rowid).tranusuario;
    var autorizar_venta = grid_tabla.getRowData(rowid).autoriza_ventas;
    var estadousuario;
    if (autorizar_venta === 'N') {
        estadousuario = 'A';
    } else {
        estadousuario = 'I';
    }

    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Etes",
        data: {
            opcion: 39,
            transid: transid,
            idusuario: idusuario,
            estadousuario: estadousuario

        },
        success: function (data) {
            cargarGrillaTransportadoras();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

