/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 $(document).ready(function() {  
    deshabilitarPanelBusqueda(false);
    var fechaHoy = new Date(); 
    anioActual = fechaHoy.getFullYear();
    mesActual = (parseInt(fechaHoy.getMonth()+1)<10)?'0'+(fechaHoy.getMonth()+1).toString():fechaHoy.getMonth()+1;   
    $('#periodo').val(anioActual.toString()+mesActual.toString());
    cargarUnidadesNegocio();     
    
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    
    $("#consultar_clientes").click(function () {
        if ($('#unidad_negocio').val() === null || $('#unidad_negocio').val() === '') {
            mensajesDelSistema("Debe seleccionar la unidad de negocio", '250', '150');
            return;
        } else if ($('#periodo').val() === '') {
            mensajesDelSistema("Debe ingresar el periodo", '250', '150');
            return;
        } else {
            cargarClasificacion();
        }
    });
 });


function cargarUnidadesNegocio() {   
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Fintra&accion=Soporte",
                dataType: 'json',
                async:false,
                data: {
                    opcion: 59
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }
                        try {
                            $('#unidad_negocio').append("<option value=''>Seleccione</option>");                  

                            for (var key in json) {
                                $('#unidad_negocio').append('<option value=' + key + '>' + json[key] + '</option>');                       
                            }

                        } catch (exception) {
                            mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                        }

                    } else {

                        mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                    }

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });    
}

function cargarClasificacion() {    
    $('#div_clasificacion_clientes').fadeIn();
    var grid_tabla_ = jQuery("#tabla_clasificacion");
    if ($("#gview_tabla_clasificacion").length) {
        refrescarGridClasificacion();
    } else {
        grid_tabla_.jqGrid({
            caption: "Listado de Clientes",       
            url: "./controller?estado=Fintra&accion=Soporte",     
            datatype: "json",
            height: '550',
            width: '1350',
            colNames: ['Id Unidad', 'Periodo', 'Negocio', 'Cedula Deudor', 'Nombre Deudor', 'Telefono', 'Celular', 'Direccion', 'Barrio',
                       'Ciudad', 'Email', 'Cedula Codeudor', 'Nombre Codeudor', 'Telefono Codeudor', 'Celular Codeudor', 'Id Convenio',
                       'Afiliado', 'Tipo', 'F. Desembolso', 'Clasificaci�n', 'F. ult. Pago', 'Dia ult. pago', 'Altura Mora M�x.', 'Altura Mora Actual',
                       '# Cuotas', 'Cuotas Pagadas', 'Cuotas x Pagar', 'Cuotas Restantes', 'Vr. Negocio', 'Vr. Factura', 'Vr. Saldo', '% Cumpl.', 'Vr. PreAprobado'],
            colModel: [
                {name: 'id_unidad_negocio', index: 'id_unidad_negocio', width: 90, sortable: true, hidden:true, align: 'left'},
                {name: 'periodo', index: 'periodo', width: 90, sortable: true, align: 'center'},
                {name: 'negasoc', index: 'negasoc', width: 90, sortable: true, align: 'center', key: true},
                {name: 'cedula_deudor', index: 'cedula_deudor', width: 110, sortable: true, align: 'center'},
                {name: 'nombre_deudor', index: 'nombre_deudor', width: 180, sortable: true, align: 'left'},               
                {name: 'telefono', index: 'telefono', width: 100, sortable: true, align: 'center'},
                {name: 'celular', index: 'celular', width: 100, sortable: true, align: 'center'},
                {name: 'direccion', index: 'direccion', width: 130, sortable: true, align: 'left'},
                {name: 'barrio', index: 'barrio', width: 110, sortable: true, align: 'left'},
                {name: 'ciudad', index: 'ciudad', width: 110, sortable: true, align: 'left'},
                {name: 'email', index: 'email', width: 110, sortable: true, align: 'left'},
                {name: 'cedula_codeudor', index: 'cedula_codeudor', width: 110, sortable: true, align: 'center'},
                {name: 'nombre_codeudor', index: 'nombre_codeudor', width: 180, sortable: true, align: 'left'},
                {name: 'telefono_codeudor', index: 'telefono_codeudor', width: 100, sortable: true, align: 'center'},
                {name: 'celular_codeudor', index: 'celular_codeudor', width: 100, sortable: true, align: 'center'},                
                {name: 'id_convenio', index: 'id_convenio', width: 80, sortable: true, align: 'center'},
                {name: 'afiliado', index: 'afiliado', width: 160, sortable: true, align: 'left'},
                {name: 'tipo', index: 'tipo', width: 120, sortable: true, align: 'left'},
                {name: 'fecha_desembolso', index: 'fecha_desembolso', width: 100, sortable: true, align: 'center'},
                {name: 'clasificacion', index: 'clasificacion', width: 90, sortable: true, align: 'center'},
                {name: 'fecha_ult_pago', index: 'fecha_ult_pago', width: 100, sortable: true, align: 'center'},
                {name: 'dias_ultimo_pago', index: 'dias_ultimo_pago', width: 90, sortable: true, align: 'center'},
                {name: 'altura_mora_maxima', index: 'altura_mora_maxima', width: 110, sortable: true, align: 'center'},
                {name: 'altura_mora_actual', index: 'altura_mora_actual', width: 110, sortable: true, align: 'center'},
                {name: 'numero_cuotas', index: 'numero_cuotas', width: 80, sortable: true, align: 'center'},
                {name: 'cuotas_pagadas', index: 'cuotas_pagadas', width: 80, sortable: true, align: 'center'},
                {name: 'cuotas_xpagar', index: 'cuotas_xpagar', width: 80, sortable: true, align: 'center'},
                {name: 'cuotas_restantes_xpagar', index: 'cuotas_xpagar', width: 80, sortable: true, align: 'center'},
                {name: 'vr_negocio', index: 'vr_negocio', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_factura', index: 'valor_factura', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_saldo', index: 'valor_saldo', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},                   
                {name: 'porcentaje_cump', index: 'porcentaje_cump', sortable: true, width: 90, align: 'center', sorttype: 'currency',
                        formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, suffix: "% "}},
                {name: 'valor_preaprobado', index: 'valor_preaprobado', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 5000,
            rowTotal: 5000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#page_tabla_clasificacion',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 106,
                    unidad_negocio: $('#unidad_negocio').val(),
                    periodo: $('#periodo').val()
                }
            },   
            loadComplete: function () {               
                if (grid_tabla_.jqGrid('getGridParam', 'records') > 0) {
                    deshabilitarPanelBusqueda(true);
                } else {
                    mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true);
                }
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }
        }).navGrid("#page_tabla_clasificacion", {search:false,refresh:false, edit: false, add: false, del: false});
        $("#tabla_clasificacion").navButtonAdd('#page_tabla_clasificacion', {
            caption: "Limpiar",
            onClickButton: function() {
                var numRecords = grid_tabla_.getGridParam('records');
                if (numRecords > 0) {    
                   deshabilitarPanelBusqueda(false);
                   $('#unidad_negocio').val('');
                   $('#periodo').val(anioActual.toString()+mesActual.toString());                   
                   jQuery("#tabla_clasificacion").jqGrid("clearGridData", true);
                   $('#div_clasificacion_clientes').fadeOut();
                   $("#tabla_clasificacion").jqGrid('GridUnload');
                } else {
                    mensajesDelSistema("No hay informacion que limpiar", '250', '150', false);
                }
            }
        });
        $("#tabla_clasificacion").jqGrid("navButtonAdd", "#page_tabla_clasificacion", {
            caption: "Exportar excel",
            onClickButton: function () {
                var info = jQuery('#tabla_clasificacion').getGridParam('records');
                if (info > 0) {
                    exportarExcelClasificacion();
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }
            }
        });
    }
}

function refrescarGridClasificacion(){   
    jQuery("#tabla_clasificacion").setGridParam({
        url: "./controller?estado=Fintra&accion=Soporte",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 106,
                unidad_negocio: $('#unidad_negocio').val(),
                periodo: $('#periodo').val()
            }
        }       
    });
    
    jQuery('#tabla_clasificacion').trigger("reloadGrid");
}

function  exportarExcelClasificacion() {
    var fullData = jQuery("#tabla_clasificacion").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
   
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: './controller?estado=Fintra&accion=Soporte',
        data: {
            listado: myJsonString,
            opcion: 107
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function deshabilitarPanelBusqueda(status) {

    var nodes = document.getElementById("div_filtro_clientes").getElementsByTagName('*');
    for (var i = 0; i < nodes.length; i++)
    {
        nodes[i].disabled = status;
    }

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: "Mensaje",
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("close");             
            }
        }
    });
}

