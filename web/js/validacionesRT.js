    function cambiar(Opcion,Boton){
            Opcion.value=Boton.value;
    }
    function validar_form_busqueda(formulario){
        if(formulario.Opcion.value=='Buscar'){
              if (formulario.CodigoPlanilla.value==''){
                alert('Debe definir el codigo de la planilla para poder continuar');
                return false;
              }
        }
        return true;
    }
    function fdatetime(strDT){
	if (strDT.indexOf('am')!=-1 || strDT.indexOf('pm')!=-1){
		var dt=strDT.split(' ');
		var t = dt[1].split(':');
		h = t[0];
		m = t[1].substring(0,2);
		p = t[1].substring(2,4);
		h = (p=='am')?((h==12)?'00':h):( (h=='12')?h:  parseFloat(h)+12 );
		return dt[0]+' '+h+':'+m+':00'
	}else return strDT;
   }
   function validar_form_RT(form){
	if (form.DTT.value==''){
		alert ('Debe definir la fecha y hora para poder continuar');
		return false;
	}
        if (form.Demora.value==''){
		alert ('Debe definir el tiempo de demora para poder continuar');
		return false;
	}else if (!isDouble(form.Demora.value)){
            alert ('Formato numerico no valido. Formato valido[#.#]');
            return false;
        }

	if (form.D_Code.selectedIndex==-1){
		alert('Debe seleccionar un delay para poder continuar');
		return false;
	}

	form.DTT.value=fdatetime(form.DTT.value);
	return true;
   }

    function validar_form_reporteRT(formulario){
       
        if(  (formulario.FInicial.value=="") || (formulario.FFinal.value=="")  ){
            alert('Debera seleccionar el rango de Fecha para la Busqueda ');
            return false;
        }     
	var ano_ini=parseFloat(formulario.FInicial.value.substr(0,4));
        var mes_ini=parseFloat(formulario.FInicial.value.substr(5,2));
        var dia_ini=parseFloat(formulario.FInicial.value.substr(8,2));
	var ano_fin=parseFloat(formulario.FFinal.value.substr(0,4));          
	var mes_fin=parseFloat(formulario.FFinal.value.substr(5,2));
	var dia_fin=parseFloat(formulario.FFinal.value.substr(8,2));
        if(ano_fin<ano_ini){
                alert('El A�o Final deber� ser Mayor o Igual  que el A�o Inicial');
                formulario.FFinal.value='';
                formulario.FFinal.focus();
                return false;
        }

        if(ano_fin==ano_ini && mes_fin<mes_ini){
                alert('El Mes Final deber� ser Mayor  o Igual que el Mes Inicial');
                formulario.FFinal.value='';
                formulario.FFinal.focus();
                return false;
        }

        if(ano_fin==ano_ini &&  mes_fin==mes_ini && dia_fin<dia_ini){
                alert('El Dia de la Fecha Final deber� ser Mayor o Igual que el dia de la Fecha  Inicial');
                formulario.FFinal.value='';
                formulario.FFinal.focus();
                return false;
        }
        return true;
        
     }

     // devuelve -1: Time1 > Time2 ; 1 lo contrario
    function compareTime(Time1,Time2) {
            fh1 = datetime(fdatetime(Time1));
            fh2 = datetime(fdatetime(Time2));
            if (parseFloat(fh1[0])>parseFloat(fh2[0]))
                    return -1;
            else if (parseFloat(fh1[0]) == parseFloat(fh2[0])){
                    if (parseFloat(fh1[1])>=parseFloat(fh2[1]))
                            return -1;
                    else
                        return 1;
            }
            else
                return 1;
    }

     // yyyy-mm-dd hh:mm:ss devuelve yyyymmdd hhmmss
    function datetime(fechahora){
            return fechahora.replace(/-|:|\//g,'').split(' ');
    }
    // rellena una cadena de ceros al principio
    function fill(cadena,longitud){
            cadena = cadena + '';
            for (i=cadena.length;i<longitud;i++)
                    cadena = '0'+cadena;
            return cadena;	
    }
    // devuelve la fecha actual
    function toDay(){
        var fd = new Date();
        return  (fill(fd.getFullYear(),4)+'-'+fill(fd.getMonth()+1,2)+'-'+fill(fd.getDate(),2)+' '+fill(fd.getHours(),2)+':'+fill(fd.getMinutes(),2)+':'+fill(fd.getSeconds(),2));
    }


    function validarTiempos(){
        if (compareTime(document.forms[1].DTT.value,toDay())==-1)
            dtt[0].className='novalido';

        for (i=2;i<document.forms.length;i++){
            if (compareTime(document.forms[i-1].DTT.value,document.forms[i].DTT.value)==-1)
              dtt[i-1].className='novalido';
        }
     }

     
    function isDouble(cadena){
        return /^\d+(.\d+)?$/.test(cadena);
    }


    
