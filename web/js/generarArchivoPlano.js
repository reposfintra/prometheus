
// 
// // Documento JavaScript
// Esta funci�n cargar� las paginas
function llamarasincrono(url, id_contenedor) {

    //validamos si el div tine contenido...
    //var emptyTest = $('#tabla').is(':empty');
    var check = $("input[name='radio']:checked").val();
    ;

    if (check == "1") {
        //esta vacio....    
        var pagina_requerida = false
        if (window.XMLHttpRequest) {// Si es Mozilla, Safari etc
            pagina_requerida = new XMLHttpRequest()
        } else if (window.ActiveXObject) { // pero si es IE
            try {
                pagina_requerida = new ActiveXObject("Msxml2.XMLHTTP")
            }
            catch (e) { // en caso que sea una versi�n antigua
                try {
                    pagina_requerida = new ActiveXObject("Microsoft.XMLHTTP")
                }
                catch (e) {
                }
            }
        }
        else
            return false
        pagina_requerida.onreadystatechange = function () { // funci�n de respuesta
            cargarpagina(pagina_requerida, id_contenedor)
        }
        pagina_requerida.open('GET', url, true) // asignamos los m�todos open y send
        pagina_requerida.send(null)

    }
    //alert("fdsdsd"+emptyTest);
}

function llamarasincrono_v2(url, id_contenedor) {


    //esta vacio....    
    var pagina_requerida = false
    if (window.XMLHttpRequest) {// Si es Mozilla, Safari etc
        pagina_requerida = new XMLHttpRequest()
    } else if (window.ActiveXObject) { // pero si es IE
        try {
            pagina_requerida = new ActiveXObject("Msxml2.XMLHTTP")
        }
        catch (e) { // en caso que sea una versi�n antigua
            try {
                pagina_requerida = new ActiveXObject("Microsoft.XMLHTTP")
            }
            catch (e) {
            }
        }
    }
    else
        return false
    pagina_requerida.onreadystatechange = function () { // funci�n de respuesta
        cargarpagina(pagina_requerida, id_contenedor)
    }
    pagina_requerida.open('GET', url, true) // asignamos los m�todos open y send
    pagina_requerida.send(null)


}

$(document).ready(function () {
    $("#VentanaCuentas").dialog({
        autoOpen: false,
        height: "auto",
        width: '530PX',
        modal: true,
        autoResize: false,
        resizable: false,
        position: "center"
    });


    $("#divSalidaEx").dialog({
        autoOpen: false,
        height: "auto",
        width: "300px",
        modal: true,
        autoResize: true,
        resizable: false,
        position: "center",
        closeOnEscape: false
    });
});

function cerrarDiv(div)
{
    $(div).dialog('close');
}

// todo es correcto y ha llegado el momento de poner la informaci�n requerida
// en su sitio en la pagina xhtml


function cargarpagina(pagina_requerida, id_contenedor) {
    if (pagina_requerida.readyState == 1)
    {
        document.getElementById("imgworking").style.visibility = "visible";
    }
    if (pagina_requerida.readyState == 4 && (pagina_requerida.status == 200 || window.location.href.indexOf("http") == -1))
    {
        document.getElementById(id_contenedor).innerHTML = pagina_requerida.responseText;
        document.getElementById("imgworking").style.visibility = "hidden";
    }
}

function Sell_all(theForm, ele) {
    if (ele.id == 'All') {
        for (i = 0; i < theForm.length; i++)
            if (theForm.elements[i].type == 'checkbox' && theForm.elements[i].disabled == false)
                theForm.elements[i].checked = theForm.All.checked;
    } else {
        theForm.All.checked = true;
        for (i = 0; i < theForm.length; i++)
            if (theForm.elements[i].type == 'checkbox' && !theForm.elements[i].checked)
                theForm.All.checked = false;
    }

}
function send(theForm) {
    var con = 0;

    for (var i = 0; i < theForm.length; i++)
    {
        var ele = theForm.elements[i];
        if (ele.type == 'checkbox' && ele.id != 'All' && ele.checked)
            con++;
    }
    if (con == 0)
        alert('Deber� seleccionar por lo menos un negocio');
    else
        theForm.submit();
}


var lastsel;
var condicion = false;
function buscarNegocios() {

    var ng = document.getElementById("unidad_negocio").value;
    var bco = $("#bco").val();
    if (ng != "") {
        if (!document.getElementById("prov").checked) {
            var controller = $("#CONTROLLER").val();
            var url = controller + '?estado=Negocios&accion=Transf&evento=19&negocio=' + ng + '&banco=' + bco +'&fecha' ;
            var ediurl = controller + '?estado=Negocios&accion=Transf&evento=8';
            if ($("#gview_Negocios").length) {
                refrescarGridNegocios(ng, bco);
            } else {
                jQuery("#Negocios").jqGrid({
                    caption: 'Detalle Negocios a Transferir',
                    url: url,
                    editurl: ediurl,
                    datatype: "json",
                    height: 500,
                    width: 1800,
                    colNames: ['CXP', 'Cod. Negocio', 'Id Cliente', 'Nombre',  'Comision', 'Fecha Negocio', 'Banco Tercero', 'Tipo CTA', 'Titular CTA', 'CC o Nit', 'No CTA'],
                    colModel: [
                        {name: 'valor_13', index: 'valor_13', sortable: true, align: 'center', width: '600px'},
                        {name: 'valor_10', index: 'valor_10', sortable: true, align: 'center', width: '600px', key: true},
                        {name: 'valor_01', index: 'valor_01', sortable: true, align: 'center', width: '600px'},
                        {name: 'valor_02', index: 'valor_02', sortable: true, align: 'center', width: '1400px'},
//                        {name: 'valor_03', index: 'valor_03', sortable: true, width: '600px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                        {name: 'valor_15', index: 'valor_15', sortable: true, align: 'center', width: '600px', editable: true, edittype: "select"},
                        {name: 'valor_11', index: 'valor_11', sortable: true, align: 'center', width: '600px'},
                        {name: 'valor_09', index: 'valor_09', sortable: true, width: '800px', align: 'center'},
                        {name: 'valor_07', index: 'valor_07', sortable: true, width: '600px', align: 'center'},
                        {name: 'valor_05', index: 'valor_05', sortable: true, width: '1400px', align: 'center'},
                        {name: 'valor_06', index: 'valor_06', sortable: true, width: '600px', align: 'center'},
                        {name: 'valor_08', index: 'valor_08', sortable: true, width: '800px', align: 'center'}
//                        {name: 'valor_04', index: 'valor_04', sortable: true, width: '600px', align: 'center'}
//                        {name: 'valor_16', index: 'valor_16', sortable: true, width: '600px', align: 'center'},
//                        {name: 'valor_17', index: 'valor_17', sortable: true, width: '600px', align: 'center'}
                      //  {name: 'accion', index: 'accion', width: '110px', align: 'center', fixed: true}

                    ],
                    rowNum: 1000,
                    rowTotal: 500000,
                    loadonce: true,
                    rownumWidth: 40,
                    gridview: true,
                    viewrecords: true,
                    hidegrid: false,
                    multiselect: true,
                    footerrow: true,
                    userDataOnFooter: true,
                    subGrid: true,
                    onSelectRow: function (valor_10) {
                        handleSelectedRow(valor_10);
                    },
                    onSelectAll: function (valor_10) {
                        handleSelectedRowAll(valor_10);
                       // jQuery("#Negocios").jqGrid('resetSelection'); 
                        var grid_tabla_ = jQuery("#Negocios");
                        var ids = grid_tabla_.jqGrid('getDataIDs');
                        var allRowsInGrid = jQuery("#Negocios").jqGrid('getGridParam', 'selarrrow');
                        var allRowsId = jQuery("#Negocios").jqGrid('getRowData');
                        for (var i = 0; i < allRowsInGrid.length; i++) {
                            var cl = ids[i];
                            var generado = grid_tabla_.getRowData(cl).valor_17;
                            var comprcar = grid_tabla_.getRowData(cl).valor_16;
                            if (generado === '' && comprcar === 'S') {
                                console.log("monda: " + cl);
                                jQuery("#Negocios").jqGrid('setSelection', cl);
                            }
                        }
                      // console.log(jQuery("#cb_Negocios").attr('checked'));
                        if (jQuery("#cb_Negocios").attr('checked') || condicion === true) {
                            $("#cb_Negocios").attr("checked", false);
                            for (var i = 0; i < allRowsId.length; i++) {
                                var cl = ids[i];
                                var generado = grid_tabla_.getRowData(cl).valor_17;
                                var comprcar = grid_tabla_.getRowData(cl).valor_16;
                                if (generado !== '' && comprcar !== 'S') {
                                    jQuery("#Negocios").jqGrid('setSelection', cl);
                                }
                            }
                            condicion = false;
                        } else {
                            $("#cb_Negocios").attr("checked", true);
                            condicion = true;
                        }

                    },
                    ondblClickRow: function (valor_10) {
                        jQuery('#Negocios').jqGrid('restoreRow', lastsel);
                        jQuery('#Negocios').jqGrid('editRow', valor_10, true);
                        lastsel = valor_10;
                    },
                    loadComplete: function () {
                        var grid_tabla_ = jQuery("#Negocios");
                        var cant = jQuery("#Negocios").jqGrid('getDataIDs');
                        for (var i = 0; i < cant.length; i++) {
                            var cl = cant[i];
                            var generado = grid_tabla_.getRowData(cl).valor_17;
                            var comprcar = grid_tabla_.getRowData(cl).valor_16;
                            if (generado === '' && comprcar === 'S') {
                                $('#jqg_Negocios_' + cl).attr("disabled", true);
                            } else {
                                $('#jqg_Negocios_' + cl).attr("disabled", false);
                            }
                        }
                        cargarComision();
                    },
                    gridComplete: function () {
                        var grid_tabla_ = jQuery("#Negocios");
                        var ids = jQuery("#Negocios").jqGrid('getDataIDs');
                        for (var i = 0; i < ids.length; i++) {
                            var cl = ids[i];
                            var generado = grid_tabla_.getRowData(cl).valor_17;
                            var comprcar = grid_tabla_.getRowData(cl).valor_16;
                            if (generado === '' && comprcar === 'S') {
                                //be = " <span id ='" + cl + "' class='form-submit-button form-submit-button-simple_green_apple ' style='font-size: 11px; padding: 3px;border: 1px solid #234684 none;' value='accion' onclick=\"preguntaPrefeccionar('" + cl + "');\" >Des</span> ";
                                be = "<img src='/fintra/images/payment_card_credit-128.png' align='absbottom'  name ='" + cl + "' id ='" + cl + "' width='25' height='25' title ='Desembolsar Cheque'  onclick=\"preguntaPrefeccionar('" + cl + "');\">";
                                dev = "<img src='/fintra/images/revert.png' align='absbottom'  name='devolver' id='devolver' width='20' height='20' title ='Devolver'  onclick=\"devolucion('" + cl + "','devolver');\">";
                                jQuery("#Negocios").jqGrid('setRowData', ids[i], {accion: be + dev});
                            } else {
                                dev = "<img src='/fintra/images/revert.png' align='absbottom'  name='devolver' id='devolver' width='20' height='20' title ='Devolver'  onclick=\"devolucion('" + cl + "','devolver');\">";
                                jQuery("#Negocios").jqGrid('setRowData', ids[i], {accion: dev});
                            }
                        }
                    },
                    jsonReader: {
                        root: "rows",
                        repeatitems: false,
                        id: "0"
                    },
                    loadError: function (xhr, status, error) {
                        alert(error);
                    }
                });

                jQuery("#Negocios").jqGrid('filterToolbar',
                        {
                            autosearch: true,
                            searchOnEnter: true,
                            defaultSearch: "cn",
                            stringResult: true,
                            ignoreCase: true,
                            multipleSearch: true
                        });

                jQuery("#Negocios").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
            }
            if (true) {
                $("#Negocios").setGridParam({
                    subGridRowExpanded: function (subgrid_id, row_id) {
                        var grid_tabla = jQuery("#Negocios");
                        var valor_16 = grid_tabla.getRowData(row_id).valor_16;
                        if (valor_16 == 'S') {
                            var subgrid_grid_tabla_, pager_id;
                            subgrid_grid_tabla_ = subgrid_id + "_t";
                            pager_id = "p_" + subgrid_grid_tabla_;
                            $("#" + subgrid_id).html("<table id='" + subgrid_grid_tabla_ + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                            jQuery("#" + subgrid_grid_tabla_).jqGrid({
                                url: "./controller?estado=Negocios&accion=Transf&doc_rel=" + row_id,
                                datatype: "json",
                                colNames: ['Documento', 'Nit', 'Proveedor', 'Descripcion', 'Valor a transferir', 'Referencia 2', 'Tipo referencia 2', 'generado', 'Generado'],
                                colModel: [
                                    {name: "valor_16", index: "valor_16", width: 70, align: "center", sortable: false},
                                    {name: "valor_17", index: "valor_17", width: 65, sortable: false},
                                    {name: "valor_24", index: "valor_24", width: 200, align: "left", sortable: false, hidden: false},
                                    {name: "valor_18", index: "valor_18", width: 200, align: "left", sortable: false},
                                    {name: 'valor_19', index: 'valor_19', sortable: true, width: 120, align: 'right', search: true, sorttype: 'currency',
                                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                                    {name: "valor_20", index: "valor_20", width: 45, align: "left", sortable: false, hidden: true},
                                    {name: "valor_21", index: "valor_21", width: 45, align: "left", sortable: false, hidden: true},
                                    {name: "valor_23", index: "valor_23", width: 45, align: "left", sortable: false, hidden: true},
                                    {name: 'generado', index: 'generado', width: '80px', align: 'center', fixed: true}
                                ],
                                rowNum: 100,
                                //pager: pager_id,
                                sortname: 'num',
                                width: '944',
                                height: '119',
                                pgtext: null,
                                pgbuttons: false,
                                jsonReader: {
                                    root: "rows",
                                    repeatitems: false,
                                    id: "0"
                                },
                                ajaxGridOptions: {
                                    dataType: "json",
                                    type: "get",
                                    async: true,
                                    data: {
                                        evento: 15
                                    }
                                }, gridComplete: function (index, row_id) {
                                    // gbox_Negocios1_LB00025_t
                                    var cant = jQuery("#" + subgrid_id + "_t").jqGrid('getDataIDs');
                                    for (var i = 0; i < cant.length; i++) {
                                        var cl = cant[i];
                                        var generado = jQuery("#" + subgrid_id + "_t").getRowData(cl).valor_23;
                                        console.log(generado);
                                        if (generado === '') {
                                            be = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                                        } else {
                                            be = '<img src = "/fintra/images/flag_green.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                                        }
                                        jQuery("#" + subgrid_id + "_t").jqGrid('setRowData', cant[i], {generado: be});
                                    }
                                }
                            });
                        }
                       
                    }
                });
            }
        } else {
            var url = $("#BASEURL").val() + "/jsp/fenalco/negocios/listaProveedores.jsp";
            $("#VentanaCuentas").html("");
            $("#VentanaCuentas").load(url).dialog("open");

        }
    } else
        alert("Debe seleccionar una unidad de negocio");
}

function refrescarGridNegocios(ng, bco) {
    var controller = $("#CONTROLLER").val();
    var url = controller + '?estado=Negocios&accion=Transf&evento=19&negocio=' + ng + '&banco=' + bco;
    totTransf = 0;
    totalValor = 0;
    totComision = 0;
    jQuery("#Negocios").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#Negocios").jqGrid('filterToolbar',
            {
                autosearch: true,
                searchOnEnter: true,
                defaultSearch: "cn",
                stringResult: true,
                ignoreCase: true,
                multipleSearch: true
            });

    jQuery("#Negocios").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    jQuery('#Negocios').trigger("reloadGrid");
}



function cargarProveedores() {
    var ng = document.getElementById("unidad_negocio").value;
    var bco = $("#bco").val();
    var controller = $("#CONTROLLER").val();
    jQuery("#Lista").jqGrid({
        caption: 'Seleccione Proveedor',
        url: controller + '?estado=Negocios&accion=Transf&evento=5&negocio=' + ng + '&banco=' + bco,
        datatype: "json",
        height: 250,
        width: 'auto',
        colNames: ['Cedula Cuenta', 'Nombre '],
        colModel: [
            {name: 'cedula_cuenta', index: 'cedula_cuenta', key: true, width: 100},
            {name: 'nombre_cuenta', index: 'nombre_cuenta', sortable: true, align: 'center', width: 350}
        ],
        rowNum: -1,
        rowTotal: 500000,
        loadonce: true,
        rownumWidth: 40,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        multiselect: true,
        jsonReader: {
            root: "rows",
            repeatitems: false,
            id: "0"
        },
        loadError: function (xhr, status, error) {
            alert(error);
        }
    });
}


function cargarComision() {
    var controller = $("#CONTROLLER").val();
    var url = controller + '?estado=Negocios&accion=Transf&evento=9';

    $.ajax({
        type: "POST",
        url: url,
        async: false,
        dataType: "html",
        success: function (data) {
            if (data != "") {
                jQuery("#Negocios").setColProp('valor_15', {editoptions: {value: data + "sin comision:sin comision"}});
                $("#div_botones").show();
            }
        }
    });

}

function transferir() {

    var filasId = jQuery("#Negocios").jqGrid('getGridParam', 'selarrrow');
    var listado = "";
    var selcom = "";

    if (($("#bco").val() != "...") && (filasId != "")) {
        $("#div_botones").hide();
        for (var i = 0; i < filasId.length; i++) {
            var fila = jQuery("#Negocios").getRowData(filasId[i]);
            var comision = fila['valor_15'];
            var cxp = fila['valor_13'];
            var negocio = fila['valor_10'];
            listado += negocio + "," + i + "," + cxp + ";_;";
            selcom += comision + ";_;";
        }
        var bco = $("#bco").val();
        var controller = $("#CONTROLLER").val();
        var url = controller + '?estado=Negocios&accion=Transf&evento=20&option=0205';
        $('#div_espera').fadeIn('slow');
        $.ajax({
            type: "POST",
            url: url,
            data: {
                listado: listado,
                selcom: selcom,
                bco: bco
            },
            success: function (data) {
                $('#div_espera').fadeOut('slow');
                refrescarGridNegocios($("#unidad_negocio").val(), $("#bco").val());
            }

        });
    } else {

        alert("Debe seleccionar un negocio y el banco a transferir");
    }
    
}


var totalValor = 0;
var totComision = 0;
var totTransf = 0;
function handleSelectedRow(id) {

    var jqgcell = jQuery('#Negocios').getCell(id, 'valor_10');
    var valor = jQuery('#Negocios').getCell(id, 'valor_03');
    var comision = jQuery('#Negocios').getCell(id, 'valor_15');
    if (comision == 'sin comision') {
        comision = 0;
    }
    var cbIsChecked = (jQuery("#jqg_Negocios_" + jqgcell).attr('checked'));

    if (cbIsChecked == true) {

        if (valor != null) {
            totalValor = parseInt(totalValor) + parseInt(valor);
            totComision = parseInt(totComision) + parseInt(comision);
        }
    } else {
        if (valor != null) {
            totalValor = parseInt(totalValor) - parseInt(valor);
            totComision = parseInt(totComision) - parseInt(comision);
        }
    }
    totTransf = totalValor - totComision;
    jQuery('#Negocios').jqGrid('footerData', 'set', {valor_02: '<span style=" font-size: 15px">Total Transferencia: </span>', valor_03: totTransf});


}


function handleSelectedRowAll() {
    totTransf = 0;
    totalValor = 0;
    totComision = 0;
    var filasId = jQuery("#Negocios").jqGrid('getGridParam', 'selarrrow');

    if (filasId.length > 0) {

        for (var i = 0; i < filasId.length; i++) {
            var jqgcell = jQuery('#Negocios').getCell(filasId[i], 'valor_10');
            var valor = jQuery('#Negocios').getCell(filasId[i], 'valor_03');
            var comision = jQuery('#Negocios').getCell(filasId[i], 'valor_15');
            if (comision == 'sin comision') {
                comision = 0;
            }
            var cbIsChecked = (jQuery("#jqg_Negocios_" + jqgcell).attr('checked'));

            if (cbIsChecked == true) {

                if (valor != null) {
                    totalValor = parseInt(totalValor) + parseInt(valor);
                    totComision = parseInt(totComision) + parseInt(comision);
                }
            } else {
                if (valor != null) {
                    totalValor = parseInt(totalValor) - parseInt(valor);
                    totComision = parseInt(totComision) - parseInt(comision);
                }
            }
        }
        totTransf = totalValor - totComision;
    } else {
        totTransf = 0;
        totalValor = 0;
        totComision = 0;
    }

    jQuery('#Negocios').jqGrid('footerData', 'set', {valor_02: '<span style=" font-size: 15px">Total Transferencia: </span>', valor_03: totTransf});

}

function devolucion(cl, ac) {
    var resp = confirm("Esta seguro que quiere " + ac + " este negocio?");
    if (resp == true) {
        var ng = document.getElementById("unidad_negocio").value;
        var bco = $("#bco").val();
        var controller = $("#CONTROLLER").val();
        var url = controller + '?estado=Negocios&accion=Transf&evento=11&option=0205';
        $.ajax({
            type: "POST",
            url: url,
            data: {
                negocio: cl,
                ac: ac
            },
            success: function (data) {
                refrescarGridNegocios(ng, bco);
            }

        });
    }
}

function exportarExcel() {
    var cols = '13';
    var file = 'xls';
    var columns = cols.toString().split(",");
    var columnNms = $("#Negocios").jqGrid('getGridParam', 'colNames');
    var theads = "";
    for (var cc = 1; cc < columnNms.length; cc++) {
        var isAdd = true;
        for (var p = 0; p < columns.length; p++) {
            if (cc == columns[p]) {
                isAdd = false;
                break;
            }
        }
        if (isAdd) {
            theads = theads + "<th>" + columnNms[cc] + "</th>";
        }
    }
    theads = "<tr>" + theads + "</tr>";
    var mya = new Array();
    mya = jQuery("#Negocios").getDataIDs();  // Get All IDs
    var data = jQuery("#Negocios").getRowData(mya[0]);     // Get First row to get the labels
    //alert(data['id']);
    var colNames = new Array();
    var ii = 0;
    for (var i in data) {
        colNames[ii++] = i;
    }

    var pageHead = "Transferencia de Negocios";
    var html = "<html><head><style script='css/text'>table.tableList_1 th { text-align:center; vertical-align: middle; padding:1px; background:#e4fad0;}table.tableList_1 td {text-align: left; vertical-align: top; padding:1px;}</style></head><body><div class='pageHead_1'>" + pageHead + "</div><table border='" + (file == 'pdf' ? '0' : '0') + "' class='tableList_1 t_space' cellspacing='10' cellpadding='0'>" + theads;
    //alert('len'+mya.length);
    for (i = 0; i < mya.length; i++)
    {
        html = html + "<tr>";
        data = jQuery("#Negocios").getRowData(mya[i]); // get each row
        for (var j = 0; j < colNames.length; j++) {
            var isjAdd = true;
            for (var pj = 0; pj < columns.length; pj++) {
                if (j == columns[pj]) {
                    isjAdd = false;
                    break;
                }
            }
            if (isjAdd) {
                html = html + "<td>" + data[colNames[j]] + "</td>"; // output each column as tab delimited
            }
        }
        html = html + "</tr>";  // output each row with end of line

    }
    html = html + "</table></body></html>";  // end of line at the end
    document.form2.pdfBuffer.value = html;
    document.form2.fileType.value = file;
    document.form2.method = 'POST';
    document.form2.action = './controller?estado=Negocios&accion=Transf&evento=12';  // send it to server which will open this contents in excel file
    document.form2.submit();
}

function exportarTablaExcel() {
    var fullData = jQuery("#Negocios").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);

    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Negocios&accion=Transf&evento=13",
        data: {
            listado: myJsonString
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            cerrarDiv("#divSalidaEx");
            alert("El archivo se encuentra en el log de archivos");
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}


//function buscarNegociosLibranza() {
//    var ng = document.getElementById("unidad_negocio").value;
//    var grid_tabla_ = jQuery("#Negocios1");
//    if ($("#gview_Negocios1").length) {
//        reloadGridMostrar(grid_tabla_, 14);
//    } else {
//        grid_tabla_.jqGrid({
//            caption: "Detalle Negocios a Transferir",
//            url: "./controller?estado=Negocios&accion=Transf",
//            mtype: "POST",
//            datatype: "json",
//            height: 500,
//            width: 1800,
//            colNames: ['CXP', 'Cod. Negocio', 'Id Cliente', 'Nombre', 'Vr. Desembolso', 'Comision', 'Fecha Negocio', 'Banco Tercero', 'Tipo CTA', 'Titular CTA', 'CC o Nit', 'No CTA', 'Observaciones', 'generado', 'Acciones'],
//            colModel: [
//                {name: 'valor_13', index: 'valor_13', sortable: true, align: 'center', width: '600px'},
//                {name: 'valor_10', index: 'valor_10', sortable: true, align: 'center', width: '600px', key: true},
//                {name: 'valor_01', index: 'valor_01', sortable: true, align: 'center', width: '600px'},
//                {name: 'valor_02', index: 'valor_02', sortable: true, align: 'center', width: '1400px'},
//                {name: 'valor_03', index: 'valor_03', sortable: true, width: '600px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
//                {name: 'valor_15', index: 'valor_15', sortable: true, align: 'center', width: '600px', editable: true, edittype: "select"},
//                {name: 'valor_11', index: 'valor_11', sortable: true, align: 'center', width: '600px'},
//                {name: 'valor_09', index: 'valor_09', sortable: true, width: '800px', align: 'center'},
//                {name: 'valor_07', index: 'valor_07', sortable: true, width: '600px', align: 'center'},
//                {name: 'valor_05', index: 'valor_05', sortable: true, width: '1400px', align: 'center'},
//                {name: 'valor_06', index: 'valor_06', sortable: true, width: '600px', align: 'center'},
//                {name: 'valor_08', index: 'valor_08', sortable: true, width: '800px', align: 'center'},
//                {name: 'valor_04', index: 'valor_04', sortable: true, width: '600px', align: 'center'},
//                {name: 'valor_22', index: 'valor_22', sortable: true, width: '600px', align: 'center'},
//                {name: 'accion', index: 'accion', width: '80px', align: 'center', fixed: true}
//            ],
//            subGrid: true,
//            rowNum: 1000,
//            rowTotal: 500000,
//            loadonce: true,
//            rownumWidth: 40,
//            gridview: true,
//            viewrecords: true,
//            hidegrid: false,
//            multiselect: true,
//            footerrow: true,
//            userDataOnFooter: true,
//            jsonReader: {
//                root: "rows",
//                repeatitems: false,
//                id: "0"
//            }, ajaxGridOptions: {
//                dataType: "json",
//                type: "get",
//                data: {
//                    evento: 14,
//                    negocio: ng,
//                    banco: $("#bco").val()
//                }
//            },
//            loadError: function (xhr, status, error) {
//                mensajesDelSistema(error, 250, 150);
//
//            }, loadComplete: function () {
//                $('#cb_Negocios1').attr("disabled", true);
//                $('#cb_Negocios1').attr("hidden", true);
//
//                var cant = jQuery("#Negocios1").jqGrid('getDataIDs');
//                for (var i = 0; i < cant.length; i++) {
//                    var cl = cant[i];
//                    var generado = grid_tabla_.getRowData(cl).valor_22;
//                    if (generado === '') {
//                        $('#jqg_Negocios1_' + cl).attr("disabled", true);
//                    } else {
//                        $('#jqg_Negocios1_' + cl).attr("disabled", false);
//                    }
//                }
//                cargarComision();
//            },
//            gridComplete: function (index) {
//                var cant = jQuery("#Negocios1").jqGrid('getDataIDs');
//                for (var i = 0; i < cant.length; i++) {
//                    var cl = cant[i];
//                    var generado = grid_tabla_.getRowData(cl).valor_22;
//                    if (generado === '') {
//                        be = " <span id ='" + cl + "' class='form-submit-button form-submit-button-simple_green_apple ' style='font-size: 11px; padding: 3px;border: 1px solid #234684 none;' value='accion' onclick=\"preguntaPrefeccionar('" + cl + "');\" >Desembolso</span> ";
//                        jQuery("#Negocios1").jqGrid('setRowData', cant[i], {accion: be});
//                    }
//                }
//            },
//            ondblClickRow: function (rowid, iRow, iCol, e) {
//            }
//        });
//    }
//    if (true) {
//        $("#Negocios1").setGridParam({
//            subGridRowExpanded: function (subgrid_id, row_id) {
//                var subgrid_grid_tabla_, pager_id;
//                subgrid_grid_tabla_ = subgrid_id + "_t";
//                pager_id = "p_" + subgrid_grid_tabla_;
//                $("#" + subgrid_id).html("<table id='" + subgrid_grid_tabla_ + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
//                jQuery("#" + subgrid_grid_tabla_).jqGrid({
//                    url: "./controller?estado=Negocios&accion=Transf&doc_rel=" + row_id,
//                    datatype: "json",
//                    colNames: ['Documento', 'Nit','Proveedor', 'Descripcion', 'Valor a transferir', 'Referencia 2', 'Tipo referencia 2', 'generado', 'Generado'],
//                    colModel: [
//                        {name: "valor_16", index: "valor_16", width: 70, align: "center", sortable: false},
//                        {name: "valor_17", index: "valor_17", width: 65, sortable: false},
//                        {name: "valor_24", index: "valor_24", width: 200, align: "left", sortable: false, hidden: false},
//                        {name: "valor_18", index: "valor_18", width: 200, align: "left", sortable: false},
//                        {name: 'valor_19', index: 'valor_19', sortable: true, width: 120, align: 'right', search: true, sorttype: 'currency',
//                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
//                        {name: "valor_20", index: "valor_20", width: 45, align: "left", sortable: false, hidden: true},
//                        {name: "valor_21", index: "valor_21", width: 45, align: "left", sortable: false, hidden: true},
//                        {name: "valor_23", index: "valor_23", width: 45, align: "left", sortable: false, hidden: true},
//                        {name: 'generado', index: 'generado', width: '80px', align: 'center', fixed: true}
//                    ],
//                    rowNum: 100,
//                    //pager: pager_id,
//                    sortname: 'num',
//                    width: '944',
//                    height: '119',
//                    pgtext: null,
//                    pgbuttons: false,
//                    jsonReader: {
//                        root: "rows",
//                        repeatitems: false,
//                        id: "0"
//                    },
//                    ajaxGridOptions: {
//                        dataType: "json",
//                        type: "get",
//                        data: {
//                            evento: 15
//                        }
//                    }, gridComplete: function (index, row_id) {
//                        // gbox_Negocios1_LB00025_t
//                        var cant = jQuery("#" + subgrid_id + "_t").jqGrid('getDataIDs');
//                        for (var i = 0; i < cant.length; i++) {
//                            var cl = cant[i];
//                            var generado = jQuery("#" + subgrid_id + "_t").getRowData(cl).valor_23;
//                            if (generado === '') {
//                                be = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
//                            } else {
//                                be = '<img src = "/fintra/images/flag_green.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
//                            }
//                            jQuery("#" + subgrid_id + "_t").jqGrid('setRowData', cant[i], {generado: be});
//                        }
//                    }
//                });
//            }
//        });
//    }
//}

function reloadGridMostrar(grid_tabla) {
    var ng = document.getElementById("unidad_negocio").value;
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Transf",
        ajaxGridOptions: {
            type: "POST",
            data: {
                evento: 14,
                negocio: ng,
                banco: $("#bco").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function perfeccionar(negocio) {
    var banco_transferencia = $("#bco").val();
    if (banco_transferencia !== '...') {
        var grid_tabla = jQuery("#Negocios");
        var cxp = grid_tabla.getRowData(negocio).valor_13;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Negocios&accion=Transf",
            async: false,
            data: {
                evento: 16,
                negocio: negocio,
                cxp: cxp
            },
            success: function (data) {
                var repuest = data.respuesta;                
                if (repuest === 'OK') {
                    //mensajedelsistema("Los cambios fueron realizados satisfactoriamente", '400', '150', true);
                    exportarPdfCheque(negocio, cxp);
                   
                } else {
                    mensajedelsistema("Error durante los cambios", '400', '150', true);
                }
            }
        });
    }else{
         mensajedelsistema("Escoger banco de tranferencia", '400', '150', true);
    }
   
}

function preguntaPrefeccionar(negocio) {
    mensajes("�Desea desembolsar los cheques?", '400', '150', true, negocio);
}

function mensajedelsistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function mensajes(msj, width, height, swHideDialog, negocio) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                perfeccionar(negocio);
            },
            "No": function () {
                var generado = jQuery("#Negocios").getRowData(negocio).valor_22;
                if (generado === '') {
                    jQuery("#Negocios").jqGrid('setSelection', negocio);
                }
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function exportarPdfCheque(negocio, cxp) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controller?estado=Negocios&accion=Transf",
        async: false,
        data: {
            evento: 17,
            negocio: negocio,
            cxp: cxp,
            banco_transferencia: $("#bco").val()
        },
        success: function (data) {
            var resp = data.respuesta;
            if (resp === 'ERROR') {
                mensajedelsistema("Ocurrio un error durante el proceso", '335', '150', false);
            } else {
                mensajedelsistema("Los cambios fueron realizados satisfactoriamente, ver pdf en en log", '335', '150', true);
            }
            buscarNegocios();
        },error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function IdentificarUnidadNegocio() {
    
    var UnidNegocio = $("#unidad_negocio").val();
    var controller = $("#CONTROLLER").val();
    var url = controller + '?estado=Negocios&accion=Transf&evento=18&unidad_negocio='+UnidNegocio;

    $.ajax({
        type: "POST",
        url: url,
        async: false,
        dataType: "html",
        success: function (data) {
            
            if (data.trim() == 'COMPRA_OBLIGACIONES') {
                buscarNegociosLibranza();
            }else{
                buscarNegocios();
            }
                
        }
    });     
    
}
