// Elimina los espacios innecesarios en una cadena
function trim(str) {
    return str.replace(/^\s+/,"").replace(/\s+$/,"");
}

//funcion que permite enviar una url de una pagina
function enviarPagina( url ){
    location.replace( url+"/" );
}
function validarEnter( e, forma ) {
    var key = (isIE) ? window.event.keyCode : e.which;
    var isNum = (key==13) ? false:true;
    if(!isNum){
        document.getElementById(forma).submit();
    }    
}
function enviarCliente(codigo){ 
 opener.form1.cliente.value=codigo;
 opener.document.getElementById("form1").submit();
 window.self.close();
}

function validarF( ruta ){
        if(form1.tipoQuery1.checked){
            if(form1.mesini.value>form1.mesfin.value){
                if(form1.anioini.value>=form1.aniofin.value){
                document.getElementById("errorFecha").style.visibility="visible";
                return false;
            }                        
        }
        if(form1.anioini.value>form1.aniofin.value){            
                document.getElementById("errorFecha").style.visibility="visible";
                return false;
            }
    }             
    document.getElementById("baceptar").src = ruta + "/images/botones/aceptarDisable.gif"    
    document.getElementById("baceptar").onclick = "";
    document.getElementById("baceptar").onmouseover = "";
    document.getElementById("baceptar").onmouseout = "";
    form1.submit();
}


//funcion que retorna el valor del querystring de un forma
function ParametrosForm( forma ){
    var i;
    var formulario = document.getElementById( forma );
    var queryString = "";
    for (i=0;i< formulario.elements.length;i++) {
        var objel = formulario.elements[i];        
        var temp = objel.name + "="+ objel.value;
        queryString += "&"+temp;
    }
    return queryString;
}
//funcion que valida que todos los campos requeridos se validen o contengan informaci�n
function validarForm( forma ){
    var i;    
    var formulario = document.getElementById( forma );
    for (i=0;i< formulario.elements.length;i++) {
        var objel = formulario.elements[i];        
        if ( objel.tagName == "SELECT" ) {            
            if( objel.selectedIndex == "-1" ){
                alert("Debe seleccionar una opci�n de las listas requeridas");
                objel.focus();
                return false;
            }
        }
        if ( objel.tagName == "INPUT" ) {
            if( trim( objel.value ) == "" ){                
                alert("Debe llenar los campos requeridos");
                objel.focus();
                return false;
            }
        }
    }
    return true;
}

//M�todo que recibe un formulario y resetea todos sus datos
//la forma de poseer un id y un name para que funcione correctamente
//resetea principalmente SELECT, INPUT...
function resetearForm( forma ){        
    var i;    
    var formulario = document.getElementById( forma );
    for (i=0;i< formulario.elements.length;i++) {
        var objel = formulario.elements[i];        
        if ( objel.tagName == "SELECT" ) {            
            if( objel.selectedIndex > 0){
                objel.selectedIndex = 0;
            }
        }
        if ( objel.tagName == "INPUT" ) {
            objel.value = "";
        }
    }
}

//m�todo que envia un dato a un input dado desde otra ventana
function enviarCuenta( dato ){    
    opener.form1.cuenta.value = dato;    
    window.self.close();
}

/////////////////////////////////////////////
//inicio funciones con AJAX
////////////////////////////////////////////
function getAjax(){
    var ajax = false;
    try {
        ajax = new ActiveXObject( "Msxml2.XMLHTTP" );
    } catch ( e ) {
        try {
            ajax = new ActiveXObject( "Microsoft.XMLHTTP" );
        } catch ( E ) {
           ajax = false;
        }
    }
    if ( !ajax && typeof XMLHttpRequest!='undefined' ) {
        ajax = new XMLHttpRequest();
    }
    return ajax;
}
function managerInterfaceContable( capa, target, forma ){
    var enviar = validarForm( forma );
    if(enviar){
        var ajax = getAjax();
        var placeContent = document.getElementById( capa );    
        var parametros = ParametrosForm( forma );
        var imagen = "<img src='" + document.getElementById("baseurl").value + "/images/cargando.gif'" + " border='0'>";
        ajax.open ('GET', target+parametros, true );
        ajax.onreadystatechange = function() {
            if (ajax.readyState==1) {
                document.getElementById("working").style.visibility = "visible";
             }else if (ajax.readyState==4){
                document.getElementById("working").style.visibility = "hidden";
                if(ajax.status==200){                
                    placeContent.innerHTML=ajax.responseText;
                    if( document.getElementById( "reset" ).value == "si" ){
                        resetearForm( forma );
                    }
                    if( document.getElementById( "reset" ).value == "ref" ){
                        document.getElementById("cmcv").value = document.getElementById("cmc").value;
                        document.getElementById("tipodocv").value = document.getElementById("tipodoc").value;
                        opener.location.reload();
                    }
                }else if(ajax.status==404){
                    placeContent.innerHTML="otro error"; 
                }
             }
        }
        ajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        ajax.send( null );    
    }
}

//funcion que permite enviar una url y concatena los parametros del formulario
function enviarPaginaForma( url, forma ){
    var parametros = ParametrosForm( forma );
    location.href = url + parametros;
}

function disableCombos(val){
    if(val==1){
        form1.mesini.disabled=true;
        form1.anioini.disabled=true;
        form1.mesfin.disabled=true;
        form1.aniofin.disabled=true;
    }
    else{
        form1.mesini.disabled=false;
        form1.anioini.disabled=false;
        form1.mesfin.disabled=false;
        form1.aniofin.disabled=false;
    }
}

function mostrarVias( capa, target, forma ){
    var ajax = getAjax();
    var placeContent = document.getElementById( capa );    
    var parametros = ParametrosForm( forma );        
    ajax.open ( 'GET', target+parametros, true );
    ajax.onreadystatechange = function() {
        if ( ajax.readyState == 1 ) {
            document.getElementById("working").style.visibility = "visible";
         }else if ( ajax.readyState == 4 ){
            document.getElementById("working").style.visibility = "hidden";
            if( ajax.status == 200 ){                
                placeContent.innerHTML = ajax.responseText;                
            }else if(ajax.status==404){
                placeContent.innerHTML="otro error"; 
            }
         }
    }
    ajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    ajax.send( null );    
}
function validandoEnvio( capa, target, forma ){        
    var ajax = getAjax();
    var placeContent = document.getElementById( capa );
    var parametros = ParametrosCheck( forma );
    ajax.open ( 'GET', target+parametros, true );    
    ajax.onreadystatechange = function() {
        if ( ajax.readyState == 1 ) {            
            document.getElementById("validate").style.visibility = "visible";            
         }else if ( ajax.readyState == 4 ){
            document.getElementById("validate").style.visibility = "hidden";
            if( ajax.status == 200 ){
                eval( ajax.responseText );
            }
         }
    }
    ajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    ajax.send( null );
}
//funcion que retorna el valor del querystring de un forma
function ParametrosCheck( forma ){
    var i;
    var formulario = document.getElementById( forma );
    var queryString = "";
    for (i=0;i< formulario.elements.length;i++) {
        var objel = formulario.elements[i];
        if ( objel.tagName == "INPUT" ) {
            if( objel.checked ){
                var temp = objel.name + "="+ objel.value;
                queryString += "&"+temp;
            }
        }
    }
    return queryString;
}

//Solo numero
$(function () {
$('.solo-numero').keypress(function(event) {
        var charCode = (event.which) ? event.which : event.keyCode

        if (
          (charCode != 45 || $(this).val().indexOf('-') != -1) && // ?-? CHECK MINUS, AND ONLY ONE.
          (charCode != 46 || $(this).val().indexOf('.') != -1) && // ?.? CHECK DOT, AND ONLY ONE.
          (charCode < 48 || charCode > 57))
          return false;

        return true;

      });
  })