/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* global funct, opcion, valorAnticipoCon */
var variableAdministracionAnt;
var administracion_ret;
var imprevisto_ant;
var imprevisto_rete;
var utilidad_ant;
var utilidad_rete;
var antes_iva_ant;
var antes_iva_ret;

$(document).ready(function () {
    funciones = new formatos();
    funcionesformulario = new funcionFomurarios();
    //imagenes();
    //clicImagnes();
    maximizarventana();
    cargarLineasNegocio();
    cargarCondicionesComerciales();
    $('#listarSolicitudes').click(function () {
        //DeshabilitarFiltroSearchSol(true);      
        var fechainicio = $("#fecha_ini_approv_solicitud").val();
        var fechafin = $("#fecha_fin_approv_solicitud").val();
        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
            if (fechainicio > fechafin) {
                mensajesDelSistema("La fecha final no puede ser inferior a la inicial", '350', '150', false);
            } else {
                $('#divObservaciones').fadeIn();
                cargarSolicitudesPendientes();
            }
        } else {
            mensajesDelSistema("Por favor revise el rango de fecha ingresado", '350', '150', false);
        }
    });

    $('#prepararAccion').click(function () {
        var id_solicitud = $('#id_solicitudSelec').val();
        abrirPrepararFacturacion(id_solicitud);
    });

    $("#clearSolicitudes").click(function () {
        DeshabilitarFiltroSearchSol(false);
        resetSearchValuesSol();
        $("#tabla_solicitudes_pendientes").jqGrid('GridUnload');
        $('#divObservaciones').fadeOut();
    });

    $("#nit_empresa, #nit_contratista").blur(function () {
        var dvId = (this.id === 'nit_empresa') ? 'digito_verif_empresa' : 'digito_verif_contratista';
        CalcularDv(this.id, dvId);
    });

    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $('.solo-numeric').live('keypress', function (event) {
        return numbersonly(this, event);
    });

    $('.solo-numero').live('blur', function (event) {
        this.value = numberConComas(this.value);
    });

    $('.solo-numeric').live('blur', function (event) {
        this.value = numberConComas(this.value);
    });

    $("input").keypress(function (e) {
        var k = e.keyCode || e.which;
        if (k == 13) {
            e.preventDefault();
        }
    });

    $('#porc_anticipo').keyup(function () {
        //var subtotal = $('#vlr_antes_iva').val();
        var subtotal = $('#base_calculo_an').val();
        $('#valor_anticipo').val(numberConComas(parseFloat(numberSinComas(subtotal) * this.value / 100).toFixed(2)));
    });

    $('#valor_anticipo').keyup(function () {
        //var subtotal = $('#vlr_antes_iva').val();
        var subtotal = $('#base_calculo_an').val();
        $('#porc_anticipo').val(numberConComas(parseFloat(this.value / numberSinComas(subtotal) * 100).toFixed(2)));
    });

    $('#porc_retegarantia').keyup(function () {
        //var subtotal = $('#vlr_antes_iva').val();
        var subtotal = $('#base_calculo_ret').val();
        $('#valor_retegarantia').val(numberConComas(parseFloat(numberSinComas(subtotal) * this.value / 100).toFixed(2)));
    });

    $('#valor_retegarantia').keyup(function () {
        //var subtotal = $('#vlr_antes_iva').val();
        var subtotal = $('#base_calculo_ret').val();
        $('#porc_retegarantia').val(numberConComas(parseFloat(this.value / numberSinComas(subtotal) * 100).toFixed(2)));
    });

    $("#fecha_ini_approv_solicitud").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    $("#fecha_fin_approv_solicitud").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    var myDate = new Date();
    $('#ui-datepicker-div').css('clip', 'auto');
    autocompletar("txt_nom_cliente", 1);
    autocompletar("txt_nom_proyecto", 2);


 $("#dep_dir").change(function () {
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciu_dir");
    });
    
    // direcciones 
    cargarDepartamentos('CO', 'dep_dir');
    $('#dep_dir').val('ATL');
    cargarCiudad('ATL', "ciu_dir");
    $('#ciu_dir').val('BQ');
    cargarVias('BQ', "via_princip_dir");
    cargarVias('BQ', "via_genera_dir");
});

function validarGuardado() {
    var estado = true;
    var ids = jQuery("#tabla_anticipo").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = jQuery("#tabla_anticipo").getRowData(ids[i]).id;
        //alert(id);
        if (id !== '') {
            estado = false;
            mensajesDelSistema('El registro ya ha sido guardado', '250', '150');
        } else {
            estado = true;
        }
    }
    return estado;
}

function numberConComas2() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo'];

    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).val(numberConComas($(campos[i]).val()));
    }
}

function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function imagenes() {
    $('.anticipo').mouseover(function () {
        var src = '/fintra/images/boton_ant2.png';
        $(this).attr("src", src);
    }).mouseout(function () {
        var src = '/fintra/images/boton_ant.png';
        $(this).attr("src", src);
    });
    $('.retegarantia').mouseover(function () {
        var src = '/fintra/images/boton_ret2.png';
        $(this).attr("src", src);
    }).mouseout(function () {
        var src = '/fintra/images/boton_rete.png';
        $(this).attr("src", src);
    });
}

function guardarCondicionesComerciales() {
    var json = [];
    var llave = {};
    var aplica;
    $(".anticipo").each(function () {
        var seleccionado = (this.src).split('');
        var dato = '';
        for (var i = seleccionado.length - 5; i > 0; i--) {
            dato = seleccionado[i] + dato;
            if (seleccionado[i] === 'b')
                break
        }
        if (dato === 'boton_ant2') {
            aplica = 'S';
        } else if (dato === 'boton_ant') {
            aplica = 'N';
        }
        llave[this.id] = aplica;
    });

    $(".retegarantia").each(function () {
        var seleccionado = (this.src).split('');
        var dato = '';
        for (i = seleccionado.length - 5; i > 0; i--) {
            dato = seleccionado[i] + dato;
            if (seleccionado[i] === 'b')
                break
        }
        if (dato === 'boton_ret2') {
            aplica = 'S';
        } else if (dato === 'boton_rete') {
            aplica = 'N';
        }
        llave[this.id] = aplica;
    });
    json.push(llave);
    console.log(json);
    return json;
}


function calcularClicImagen(imagen) {
    switch (imagen) {
        case 'antes_iva_ant':
            var valor_iva = numberSinComas($('#valor_iva').val());
            var vlr_base = numberSinComas($('#base_calculo_an').val());
            var porcAnticipo = $('#porc_anticipo').val();
            if (antes_iva_ant === 1) {
                var imagen1 = new Image;
                imagen1.src = '/fintra/images/boton_ant2.png';
                document.images['antes_iva_ant'].src = imagen1.src;

                var base_calculo_an = parseFloat(vlr_base) + parseFloat(valor_iva);
                $('#base_calculo_an').val(numberConComas((base_calculo_an)));
                var valorTolal = (parseFloat(base_calculo_an) * parseFloat(porcAnticipo)) / 100;
                $('#valor_anticipo').val(numberConComas(valorTolal));
                antes_iva_ant = 0;
            } else {
                var mi_imagen2 = new Image;
                mi_imagen2.src = '/fintra/images/boton_ant.png';
                document.images['antes_iva_ant'].src = mi_imagen2.src;

                var base_calculo_an = parseFloat(vlr_base) - parseFloat(valor_iva);
                $('#base_calculo_an').val(numberConComas(base_calculo_an));
                var valorTolal = (parseFloat(base_calculo_an) * parseFloat(porcAnticipo)) / 100;
                $('#valor_anticipo').val(numberConComas(valorTolal));
                antes_iva_ant = 1;
            }
            break;
        case 'antes_iva_ret':
            var valor_iva = numberSinComas($('#valor_iva').val());
            var vlr_base = numberSinComas($('#base_calculo_ret').val());
            var porcRetegarantia = $('#porc_retegarantia').val();
            if (antes_iva_ret === 1) {
                var imagen1 = new Image;
                imagen1.src = '/fintra/images/boton_ret2.png';
                document.images['antes_iva_ret'].src = imagen1.src;

                var base_calculo_ret = parseFloat(vlr_base) + parseFloat(valor_iva);
                $('#base_calculo_ret').val(numberConComas((base_calculo_ret)));
                var valorTolal = (parseFloat(base_calculo_ret) * parseFloat(porcRetegarantia)) / 100;
                $('#valor_retegarantia').val(numberConComas(valorTolal));
                antes_iva_ret = 0;
            } else {
                var mi_imagen2 = new Image;
                mi_imagen2.src = '/fintra/images/boton_rete.png';
                document.images['antes_iva_ret'].src = mi_imagen2.src;

                var base_calculo_ret = parseFloat(vlr_base) - parseFloat(valor_iva);
                $('#base_calculo_ret').val(numberConComas(base_calculo_ret));
                var valorTolal = (parseFloat(base_calculo_ret) * parseFloat(porcRetegarantia)) / 100;
                $('#valor_retegarantia').val(numberConComas(valorTolal));
                antes_iva_ret = 1;
            }
            break;
        case 'administracion_ant':
            var valorAdministracion = numberSinComas($('#vlr_admon').val());
            var vlr_base = numberSinComas($('#base_calculo_an').val());
            var porcAnticipo = $('#porc_anticipo').val();

            if (variableAdministracionAnt === 1) {
                var imagen1 = new Image;
                imagen1.src = '/fintra/images/boton_ant2.png';
                document.images['administracion_ant'].src = imagen1.src;

                var base_calculo_an = parseFloat(vlr_base) + parseFloat(valorAdministracion);
                $('#base_calculo_an').val(numberConComas((base_calculo_an)));
                var valorTolal = (parseFloat(base_calculo_an) * parseFloat(porcAnticipo)) / 100;
                $('#valor_anticipo').val(numberConComas(valorTolal));
                variableAdministracionAnt = 0;
            } else {
                var mi_imagen2 = new Image;
                mi_imagen2.src = '/fintra/images/boton_ant.png';
                document.images['administracion_ant'].src = mi_imagen2.src;

                var base_calculo_an = parseFloat(vlr_base) - parseFloat(valorAdministracion);
                $('#base_calculo_an').val(numberConComas(base_calculo_an));
                var valorTolal = (parseFloat(base_calculo_an) * parseFloat(porcAnticipo)) / 100;
                $('#valor_anticipo').val(numberConComas(valorTolal));
                variableAdministracionAnt = 1;
            }
            break;
        case 'imprevisto_ant':
            var valorImprevisto = numberSinComas($('#vlr_imprevisto').val());
            var vlr_base = numberSinComas($('#base_calculo_an').val());
            var porcAnticipo = $('#porc_anticipo').val();

            if (imprevisto_ant === 1) {
                var imagen1 = new Image;
                imagen1.src = '/fintra/images/boton_ant2.png';
                document.images['imprevisto_ant'].src = imagen1.src;

                var base_calculo_an = parseFloat(vlr_base) + parseFloat(valorImprevisto);
                $('#base_calculo_an').val(numberConComas((base_calculo_an)));
                var valorTolal = (parseFloat(base_calculo_an) * parseFloat(porcAnticipo)) / 100;
                $('#valor_anticipo').val(numberConComas(valorTolal));
                imprevisto_ant = 0;
            } else {
                var mi_imagen2 = new Image;
                mi_imagen2.src = '/fintra/images/boton_ant.png';
                document.images['imprevisto_ant'].src = mi_imagen2.src;

                var base_calculo_an = parseFloat(vlr_base) - parseFloat(valorImprevisto);
                $('#base_calculo_an').val(numberConComas(base_calculo_an));
                var valorTolal = (parseFloat(base_calculo_an) * parseFloat(porcAnticipo)) / 100;
                $('#valor_anticipo').val(numberConComas(valorTolal));
                imprevisto_ant = 1;
            }
            break;
        case 'utilidad_ant':
            var valorUtilidad = numberSinComas($('#vlr_utilidad').val());
            var vlr_base = numberSinComas($('#base_calculo_an').val());
            var porcAnticipo = $('#porc_anticipo').val();

            if (utilidad_ant === 1) {
                var imagen1 = new Image;
                imagen1.src = '/fintra/images/boton_ant2.png';
                document.images['utilidad_ant'].src = imagen1.src;

                var base_calculo_an = parseFloat(vlr_base) + parseFloat(valorUtilidad);
                $('#base_calculo_an').val(numberConComas((base_calculo_an)));
                var valorTolal = (parseFloat(base_calculo_an) * parseFloat(porcAnticipo)) / 100;
                $('#valor_anticipo').val(numberConComas(valorTolal));
                utilidad_ant = 0;
            } else {
                var mi_imagen2 = new Image;
                mi_imagen2.src = '/fintra/images/boton_ant.png';
                document.images['utilidad_ant'].src = mi_imagen2.src;

                var base_calculo_an = parseFloat(vlr_base) - parseFloat(valorUtilidad);
                $('#base_calculo_an').val(numberConComas(base_calculo_an));
                var valorTolal = (parseFloat(base_calculo_an) * parseFloat(porcAnticipo)) / 100;
                $('#valor_anticipo').val(numberConComas(valorTolal));
                utilidad_ant = 1;
            }
            break;
        case 'administracion_ret':
            var valorAdministracion = numberSinComas($('#vlr_admon').val());
            var vlr_base = numberSinComas($('#base_calculo_ret').val());
            var porcRetefuente = $('#porc_retegarantia').val();

            if (administracion_ret === 1) {
                var imagen1 = new Image;
                imagen1.src = '/fintra/images/boton_ret2.png';
                document.images['administracion_ret'].src = imagen1.src;

                var base_calculo_ret = parseFloat(vlr_base) + parseFloat(valorAdministracion);
                $('#base_calculo_ret').val(numberConComas((base_calculo_ret)));
                var valorTolal = (parseFloat(base_calculo_ret) * parseFloat(porcRetefuente)) / 100;
                $('#valor_retegarantia').val(numberConComas(valorTolal));
                administracion_ret = 0;
            } else {
                var mi_imagen2 = new Image;
                mi_imagen2.src = '/fintra/images/boton_rete.png';
                document.images['administracion_ret'].src = mi_imagen2.src;

                var base_calculo_ret = parseFloat(vlr_base) - parseFloat(valorAdministracion);
                $('#base_calculo_ret').val(numberConComas(base_calculo_ret));
                var valorTolal = (parseFloat(base_calculo_ret) * parseFloat(porcRetefuente)) / 100;
                $('#valor_retegarantia').val(numberConComas(valorTolal));
                administracion_ret = 1;
            }
            break;
        case 'imprevisto_rete':
            var valorImprevisto = numberSinComas($('#vlr_imprevisto').val());
            var vlr_base = numberSinComas($('#base_calculo_ret').val());
            var porcRetefuente = $('#porc_retegarantia').val();

            if (imprevisto_rete === 1) {
                var imagen1 = new Image;
                imagen1.src = '/fintra/images/boton_ret2.png';
                document.images['imprevisto_rete'].src = imagen1.src;

                var base_calculo_ret = parseFloat(vlr_base) + parseFloat(valorImprevisto);
                $('#base_calculo_ret').val(numberConComas((base_calculo_ret)));
                var valorTolal = (parseFloat(base_calculo_ret) * parseFloat(porcRetefuente)) / 100;
                $('#valor_retegarantia').val(numberConComas(valorTolal));
                imprevisto_rete = 0;
            } else {
                var mi_imagen2 = new Image;
                mi_imagen2.src = '/fintra/images/boton_rete.png';
                document.images['imprevisto_rete'].src = mi_imagen2.src;

                var base_calculo_ret = parseFloat(vlr_base) - parseFloat(valorImprevisto);
                $('#base_calculo_ret').val(numberConComas(base_calculo_ret));
                var valorTolal = (parseFloat(base_calculo_ret) * parseFloat(porcRetefuente)) / 100;
                $('#valor_retegarantia').val(numberConComas(valorTolal));
                imprevisto_rete = 1;
            }
            break;
        case 'utilidad_rete':
            var valorUtilidad = numberSinComas($('#vlr_utilidad').val());
            var vlr_base = numberSinComas($('#base_calculo_ret').val());
            var porcRetefuente = $('#porc_retegarantia').val();

            if (utilidad_rete === 1) {
                var imagen1 = new Image;
                imagen1.src = '/fintra/images/boton_ret2.png';
                document.images['utilidad_rete'].src = imagen1.src;

                var base_calculo_ret = parseFloat(vlr_base) + parseFloat(valorUtilidad);
                $('#base_calculo_ret').val(numberConComas((base_calculo_ret)));
                var valorTolal = (parseFloat(base_calculo_ret) * parseFloat(porcRetefuente)) / 100;
                $('#valor_retegarantia').val(numberConComas(valorTolal));
                utilidad_rete = 0;
            } else {
                var mi_imagen2 = new Image;
                mi_imagen2.src = '/fintra/images/boton_rete.png';
                document.images['utilidad_rete'].src = mi_imagen2.src;

                var base_calculo_ret = parseFloat(vlr_base) - parseFloat(valorUtilidad);
                $('#base_calculo_ret').val(numberConComas(base_calculo_ret));
                var valorTolal = (parseFloat(base_calculo_ret) * parseFloat(porcRetefuente)) / 100;
                $('#valor_retegarantia').val(numberConComas(valorTolal));
                utilidad_rete = 1;
            }
            break;
    }
}

function initContrato() {

    cargarCboClasificacionPersona();
    cargarDepartamentos('CO', 'dep_dir');
    $('#dep_dir').val('ATL');
    cargarCiudad('ATL', "ciu_dir");
    $('#ciu_dir').val('BQ');
    cargarVias('BQ', "via_princip_dir");
    cargarVias('BQ', "via_genera_dir");
    cargarInfoSolicitud($('#num_solicitud').val());
    jsonDiasPago = listarComboGrid(9);
    jsonPolizas = listarComboGrid(10);
    jsonBeneficiarios = listarComboGrid(41, 1);
    jsonCausales = listarComboGrid(42);
    jsonTipoEntrada = {};
    obtenerArchivosCargados();
    cargarGarantias();
    if (jQuery('#tabla_garantias').jqGrid('getGridParam', 'records') > 0)
        cargarOtroSi();
    //cargarGarantiasOtroSi();
    cargarGarantiasExtraContractuales();

    $("#dep_dir").change(function () {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciu_dir");
    });
    $("#ciu_dir").change(function () {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarVias(op, "via_princip_dir");
        cargarVias(op, "via_genera_dir");
    });

    $("#via_princip_dir").change(function () {
        $("#via_genera_dir").val('');
    });

    $("#fecha_inicio_proyecto").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    $("#fecha_fin_proyecto").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    $("#fecha_expedicion").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });


    var myDate = new Date();
    $("#fecha_inicio_proyecto").datepicker("setDate", myDate);
    $("#fecha_fin_proyecto").datepicker("setDate", myDate);
    $("#fecha_expedicion").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

    $('#fecha_expedicion').datepicker("destroy");

    $("#subirArchivo").click(function (e) {
        e.preventDefault();
        cargarArchivos();
    });

    $("#btn_guardar").click(function () {
        if ($('#tbl_archivos_cargados tr').length === 0) {
            mensajesDelSistema('Por favor, Debe cargar al menos el documento del contrato!!!', '250', '150');
            return;
        } else if ($('#clasificacion').val() === '') {
            mensajesDelSistema('Por favor, seleccione una clasificacion!!!', '250', '150');
            return;
        } else if ($('#cedula_rep_empresa').val() === '') {
            mensajesDelSistema('Por favor, ingrese la identificaci�n del representante legal!!!', '250', '150');
            return;
        } else if ($('#anticipo').val() === '1' && (parseFloat(numberSinComas($('#porc_anticipo').val())) === 0 || parseFloat(numberSinComas($('#valor_anticipo').val())) === 0)) {
            mensajesDelSistema('Por favor, Ingrese valor de anticipo!!!', '250', '150');
            return;
        } else if ($('#retegarantia').val() === '1' && (parseFloat(numberSinComas($('#porc_retegarantia').val())) === 0 || parseFloat(numberSinComas($('#valor_retegarantia').val())) === 0)) {
            mensajesDelSistema('Por favor, Ingrese valor de la retegarant�a!!!', '250', '150');
            return;
        } else if (jQuery("#tabla_garantias").jqGrid('getGridParam', 'records') === 0) {
            mensajesGuardarGarantia("�Esta seguro de guardar esta solictud sin garantias?", '250', '150');
        } else if (validateGridGarantias('tabla_garantias') && validateGridGarantias('tabla_garantias_extra')) {
            guardarCondicionesComercialesImagenes();
            guardarContrato();
        }
    });

    $("#btn_salir").click(function () {
        mensajeConfirmAction("Recuerde guardar los cambios. Esta seguro que desea salir ?", "230", "150", cerrarventana, "");
    });

}

function cargarSolicitudesPendientes() {

    $('#div_solicitudes_pendientes').fadeIn();
    var grid_tbl_solicitudes_pendientes = jQuery("#tabla_solicitudes_pendientes");
    if ($("#gview_tabla_solicitudes_pendientes").length) {
        refrescarGridSolicitudesPendientes();
    } else {
        grid_tbl_solicitudes_pendientes.jqGrid({
            caption: "Listado de Solicitudes Pendientes por Contrato",
            url: "./controlleropav?estado=Minutas&accion=Contratacion",
            datatype: "json",
            height: '450',
            width: '1650',
            colNames: ['IdSolicitud', 'Fecha Creacion', 'Foms', 'Nombre Cliente', 'Nombre Proyecto', 'Costo Contratista', 'Valor Cotizacion', '% Anticipo', '% Retegarantia', 'Cantidad Facturada', 'Valor Facturado', 'Etapa', 'Nombre estado',
                'Id Cliente', 'Tipo Solicitud', 'Estado Cartera', 'F Validacion Cartera', 'Responsable', 'Interventor', 'IdTrazabilidad', 'Id estado',
                'Modalidad Comercial', 'No Contrato', 'Tipo Contrato', 'Accion'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, align: 'center', key: true},
                {name: 'creation_date', index: 'creation_date', width: 130, align: 'center', hidden: true},
                {name: 'num_os', index: 'num_os', width: 110, align: 'center'},
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 230, align: 'left'},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 230, align: 'left'},
                {name: 'valor_cotizacion', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'perc_anticipo', index: 'perc_anticipo', width: 120, align: 'center'},
                {name: 'perc_retegarantia', index: 'perc_retegarantia', width: 120, align: 'center'},
                {name: 'cant_facturada', index: 'cant_facturada', width: 120, align: 'center'},
                {name: 'valor_faturado', index: 'valor_faturado', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'etapa_trazabilidad', index: 'etapa_trazabilidad', width: 125, align: 'left'},
                {name: 'nombre_estado', index: 'nombre_estado', width: 125, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'id_cliente', index: 'id_cliente', width: 110, align: 'left', hidden: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 110, align: 'left', hidden: true},
                {name: 'estado_cartera', index: 'estado_cartera', width: 110, align: 'left', hidden: true},
                {name: 'fecha_validacion_cartera', index: 'fecha_validacion_cartera', width: 110, align: 'center', hidden: true},
                {name: 'Responsable', index: 'Responsable', width: 130, align: 'left', hidden: true},
                {name: 'Interventor', index: 'Interventor', width: 130, align: 'left', hidden: true},
                {name: 'id_trazabilidad', index: 'id_trazabilidad', width: 110, align: 'center', hidden: true},
                {name: 'id_estado', index: 'id_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'modalidad_comercial', index: 'modalidad_comercial', width: 80, align: 'center', hidden: true},
                {name: 'num_contrato', index: 'num_contrato', width: 110, align: 'center', hidden: true},
                {name: 'tipo_contrato', index: 'tipo_contrato', width: 110, align: 'center', hidden: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '175px', search: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_solicitudes_pendientes'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            multiselect: false,
            subGrid: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 0,
                    fecha_inicial: $('#fecha_ini_approv_solicitud').val(),
                    fecha_final: $('#fecha_fin_approv_solicitud').val(),
                    num_solicitud: $('#solicitud').val(),
                    lineaNegocio: $("#linea_negocio").val(),
                    responsable: $("#responsable").val(),
                    trazabilidad: '4',
                    etapaActual: $('#etapaActual').val(),
                    id_cliente: $('#id_cliente').val(),
                    nom_proyecto: $('#txt_nom_proyecto').val(),
                    foms: $('#txt_foms').val(),
                    tipo_proyecto: $('#tipo_proyecto').val()
                }
            },
            gridComplete: function () {

                var ids = grid_tbl_solicitudes_pendientes.jqGrid('getDataIDs');
                var fila;
                var tipo_contrato, num_contrato, generar_ot = '', accion = '', AYF = '', genDoc= '', fac='', contrato='', Minuta='';
                for (var i = 0; i < ids.length; i++) {

                    var cl = ids[i];
                    fila = grid_tbl_solicitudes_pendientes.jqGrid("getLocalRow", cl);
                    tipo_contrato = fila.tipo_contrato;
                    num_contrato = fila.num_contrato;


                    var bgcolor = '#FFFFFF none repeat scroll 0 0;';
                    if ((fila.id_trazabilidad === '4') || (fila.id_trazabilidad === '6') || (fila.id_trazabilidad === '5')) {

                        if (num_contrato === '') {
                            bgcolor = 'rgba(245, 140, 112, 0.53)';
                            genDoc = "<img src='/fintra/images/opav/Edit-contrato_grey.png' align='absbottom'  name='genera_docs' id='genera_docs' width='23' height='23' title ='Editar Contrato' );\">";
                            contrato = "<img src='/fintra/images/opav/Contrato_grey.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='26' height='26' title ='Contrato' );\">";

                        } else {

                            genDoc = "<img src='/fintra/images/opav/Edit-contrato.png' align='absbottom'  name='genera_docs' id='genera_docs' width='23' height='23' title ='Editar Contrato'  onclick=\"AbrirDivGeneraContratoDocs('" + tipo_contrato + "','" + num_contrato + "');\">";
                            contrato = "<img src='/fintra/images/opav/Contrato.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='26' height='26' title ='Contrato'  onclick=\"verContrato('" + cl + "','" + tipo_contrato + "','" + num_contrato + "');\">";

                        }

                        Minuta = "<img src='/fintra/images/opav/admin_garantias.png' align='absbottom'  name='generar' id='generar' width='26' height='26' title ='Abrir ventana de contrato'  onclick=\"Minutas('" + cl + "');\">";

//                        if ((fila.id_estado === '430') || (fila.id_estado === '410') ||(fila.id_trazabilidad === '6') || (fila.id_trazabilidad === '5')) {
                        if ((fila.id_estado === '410')|| (fila.id_estado === '610')) {
                            accion = "<img src='/fintra/images/opav/Transicion.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n'  onclick=\"mostrarVentanaAccion('" + cl + "');\">";
                        } else {
                            accion = "<img src='/fintra/images/opav/Transicion_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n' >";
                        }

                        if ((fila.id_estado === '420') || (fila.id_estado === '430') || (fila.id_estado === '510')) {

                            AYF = "<img src='/fintra/images/opav/Facturacion.png' align='absbottom' name='AYF' id='AYF'value='Anticipos'  width='26' height='26' title ='Anticipo'  onclick=\"abrir_Anticipo_Facturacion('" + cl + "');\">";

                        } else {
                            AYF = "<img src='/fintra/images/opav/Facturacion_grey.png' align='absbottom' name='AYF' id='AYF'value='Anticipos Y Facturaci�n'  width='26' height='26' title ='Anticipo '>";
                            fac = "<img src='/fintra/images/opav/facturacionGris.png' align='absbottom' name='AYF' id='fac'value='Facturaci�n'  width='26' height='26' title ='Facturaci�n'>";
                        }

                        if (fila.num_os === '' && fila.id_estado === '430') {
                            generar_ot = "<img src='/fintra/images/opav/OT.png' align='absbottom'  style='height:25px;width:26px;margin-left: 8px;border-right: 1px solid;padding-right: 2px;' name='Generar_OT' id='Generar_OT'value=''  width='19' height='19' title ='Generar OT'  onclick=\"mostrarVentanaAccion3('" + cl + "');\">";
                        } else {
                            generar_ot = "<img src='/fintra/images/opav/OT_grey.png' align='absbottom'  style='height:25px;width:26px;margin-left: 8px;border-right: 1px solid;padding-right: 2px;' name='Generar_OT' id='Generar_OT'value=''  width='19' height='19' title ='Generar OT'  >";
                        }
                        if ((fila.id_trazabilidad === '6') || (fila.id_trazabilidad === '5')) {
                            fac = "<img src='/fintra/images/opav/facturacion.png' align='absbottom' name='AYF' id='fac'value='Facturaci�n'  width='26' height='26' title ='Facturaci�n'  onclick=\"abrir_Facturacion('" + cl + "');\">";
                        }


                    } else {

                        genDoc = "<img src='/fintra/images/opav/Edit-contrato_grey.png' align='absbottom'  name='genera_docs' id='genera_docs' width='23' height='23' title ='Editar Contrato' );\">";
                        contrato = "<img src='/fintra/images/opav/Contrato_grey.png' align='absbottom' name='contrato' id='contrato'value='Contrato'  width='26' height='26' title ='Contrato' );\">";

                        accion = "<img src='/fintra/images/opav/Transicion_grey.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Acci�n' >";
                        Minuta = "<img src='/fintra/images/opav/admin_garantias_grey.png' align='absbottom'  name='generar' id='generar' width='26' height='26' title ='Abrir ventana de contrato'>";
                        AYF = "<img src='/fintra/images/opav/Facturacion.png' align='absbottom' name='AYF' id='AYF'value='Anticipos'  width='26' height='26' title ='Anticipo'  onclick=\"abrir_Anticipo_Facturacion('" + cl + "');\">";
                        fac = "<img src='/fintra/images/opav/facturacion.png' align='absbottom' name='AYF' id='fac'value='Facturaci�n'  width='26' height='26' title ='Facturaci�n'  onclick=\"abrir_Facturacion('" + cl + "');\">";
                        generar_ot = "<img src='/fintra/images/opav/OT_grey.png' align='absbottom'  style='height:25px;width:26px;margin-left: 8px;border-right: 1px solid;padding-right: 2px;' name='Generar_OT' id='Generar_OT'value=''  width='19' height='19' title ='Generar OT'  >";

                    }

                    changeBackgroundRow(cl, bgcolor);

                    var x =  accion + ' ' + Minuta + ' ' + AYF + fac + generar_ot + ' ' + genDoc + ' ' + contrato + ' ';

                    grid_tbl_solicitudes_pendientes.jqGrid('setRowData', ids[i], {actions: accion + ' ' + Minuta + ' ' + AYF + ' ' + fac + ' ' + generar_ot + ' ' + genDoc + ' ' + contrato + ' '});

                }

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_solicitudes_pendientes", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_solicitudes_pendientes").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: false,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
        jQuery("#tabla_solicitudes_pendientes").jqGrid("navButtonAdd", "#page_tabla_solicitudes_pendientes", {
            caption: "Generar Contrato",
            title: "Generar documentos contrato",
            onClickButton: function () {
                generarContrato();
            }
        });
    }
    if (true) {
        $("#tabla_solicitudes_pendientes").setGridParam({
            subGridRowExpanded: function (subgrid_id, row_id) {
                var pager_id, subgrid_table_id;
                subgrid_table_id = subgrid_id + "_t";
                pager_id = "p_" + subgrid_table_id;
                $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");

                jQuery("#" + subgrid_table_id).jqGrid({
                    url: "./controlleropav?estado=Procesos&accion=Cliente&id_solicitud=" + row_id,
//                    caption: "Anticipo",
                    datatype: "json",
                    colNames: ['Id', 'Numero_anticipo', 'Tolal Anticipo', 'Facha Anticipo', 'Numero Factura', 'Valor Factura', 'Fecha Factura', 'Numero CXP', 'Valor CXP', 'Fecha CXP'],
                    colModel: [
                        {name: "id", index: "id", width: 90, align: "center", sortable: false, key: true, hidden: true},
                        {name: "cod_anticipo", index: "cod_anticipo", width: 90, align: "center", sortable: false},
                        {name: 'total', index: 'total', sortable: true, width: 95, align: 'right', search: true, sorttype: 'currency',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                        {name: "fecha_anticipo", index: "fecha_anticipo", width: 80, align: "center", sortable: false},
                        {name: "num_factura", index: "num_factura", width: 80, align: "center", sortable: false},
                        {name: 'valor', index: 'valor', sortable: true, width: 98, align: 'right', search: true, sorttype: 'currency',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                        {name: "fecha_factura", index: "fecha_factura", width: 80, align: "center", sortable: false},
                        {name: "num_cxp", index: "num_cxp", width: 80, align: "center", sortable: false},
                        {name: 'valorCxp', index: 'valorCxp', sortable: true, width: 95, align: 'right', search: true, sorttype: 'currency',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}},
                        {name: "fecha_cxp", index: "fecha_cxp", width: 80, align: "center", sortable: false}
                    ],
                    rowNum: 29,
                    //pager: pager_id,
                    //sortname: 'num',
                    width: '980',
                    height: '80',
                    pgtext: null,
                    loadonce: true,
                    pgbuttons: false,
                    gridview: true,
                    viewrecords: true,
                    rowTotal: 1000,
                    ajaxGridOptions: {
                        dataType: "json",
                        type: "post",
                        data: {
                            opcion: 62
                        }

                    }, jsonReader: {
                        root: "rows",
                        repeatitems: false,
                        id: "0"
                    }, gridComplete: function () {

                    }
                });

            }

        });
    }
}

function refrescarGridSolicitudesPendientes() {
    jQuery("#tabla_solicitudes_pendientes").setGridParam({
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 0,
                fecha_inicial: $('#fecha_ini_approv_solicitud').val(),
                fecha_final: $('#fecha_fin_approv_solicitud').val(),
                num_solicitud: $('#solicitud').val(),
                lineaNegocio: $("#linea_negocio").val(),
                responsable: $("#responsable").val(),
                trazabilidad: '4',
                etapaActual: $('#etapaActual').val(),
                id_cliente: $('#id_cliente').val(),
                nom_proyecto: $('#txt_nom_proyecto').val(),
                foms: $('#txt_foms').val(),
                tipo_proyecto: $('#tipo_proyecto').val()
            }
        }
    });

    jQuery('#tabla_solicitudes_pendientes').trigger("reloadGrid");
}

function cargarDepartamentos(codigo, combo) {
    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            type: 'POST',
            async: false,
            url: "/fintra/controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 13,
                cod_pais: codigo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "/fintra/controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function CalcularDv(id, dvId) {
    var vpri, x, y, z, i, nit1, dv1;
    nit1 = document.getElementById(id).value;
    if (isNaN(nit1)) {
        //document.form1.dv.value="X";
        alert('El valor digitado no es un numero valido');
    } else {
        vpri = new Array(16);
        x = 0;
        y = 0;
        z = nit1.length;
        vpri[1] = 3;
        vpri[2] = 7;
        vpri[3] = 13;
        vpri[4] = 17;
        vpri[5] = 19;
        vpri[6] = 23;
        vpri[7] = 29;
        vpri[8] = 37;
        vpri[9] = 41;
        vpri[10] = 43;
        vpri[11] = 47;
        vpri[12] = 53;
        vpri[13] = 59;
        vpri[14] = 67;
        vpri[15] = 71;
        for (i = 0; i < z; i++) {
            y = (nit1.substr(i, 1));
            //document.write(y+"x"+ vpri[z-i] +":");
            x += (y * vpri[z - i]);
            //document.write(x+"<br>");     
        }
        y = x % 11;
        //document.write(y+"<br>");
        if (y > 1) {
            dv1 = 11 - y;
        } else {
            dv1 = y;
        }
        //document.form1.dv.value=dv1;
        document.getElementById(dvId).value = dv1;
    }
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function Posicionar_div(id_objeto, e) {
    obj = document.getElementById(id_objeto);
    var posx = 0;
    var posy = 0;
    if (!e)
        var e = window.event;
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    }
    else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    else
        alert('ninguna de las anteriores');

    obj.style.left = posx + 'px';
    obj.style.top = posy - 95 + 'px';
    obj.style.zIndex = 1300;

}

function genDireccion(elemento, e) {

    var contenedor = document.getElementById("direccion_dialogo")
            , res = document.getElementById("dir_resul");

    //$("#direccion_dialogo").draggable({ handle: "#drag_direcciones"});
    Posicionar_div("direccion_dialogo", e);
    // contenedor.style.left = 515 + 'px';  
    contenedor.style.display = "block";

    res.name = elemento;
    res.value = (elemento.value) ? elemento.value : '';

}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3:
            var res = document.getElementById('dir_resul')
                    , des = document.getElementById(res.name);
            des.value = res.value;
        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");

            document.getElementById("direccion_dialogo").style.display = "none";
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
                    , g = document.getElementById('via_genera_dir')
                    , opcion;
            for (var i = 0; i < g.length; i++) {

                opcion = g[i];

                if (opcion.value === p) {

                    opcion.style.display = 'none';
                    opcion.disabled = true;

                } else {

                    opcion.disabled = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if (!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                    || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                    || !jQuery('#placa_dir').val()) {
                jQuery('#dir_resul').val("");
                jQuery('#dir_resul').attr("class", "validation-failed");
            } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                        jQuery('#via_princip_dir option:selected').text()
                        + ' ' + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                        + ' ' + jQuery('#via_genera_dir option:selected').text().trim()
                        + ' ' + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                        + ((jQuery('#via_genera_dir option:selected').text().toUpperCase() === '#' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase() === 'TRANSVERSAL') ? '-' : ' ')
                        + jQuery('#placa_dir').val().trim().toUpperCase()
                        + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
            }
            break;
    }
}

function cargarVias(codciu, combo) {
    //alert(codciu);

    $('#' + combo).empty();
    $.ajax({
        async: false,
        type: 'POST',
        url: "/fintra/controller?estado=GestionSolicitud&accion=Aval",
        dataType: 'json',
        data: {
            opcion: 'cargarvias',
            ciu: codciu
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    alert(json.error, '250', '180');
                    return;
                }
                try {
                    $('#' + combo).empty();
                    $('#' + combo).append("<option value=''></option>");

                    for (var key in json) {
                        $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    alert('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function cargarInfoSolicitud(num_solicitud) {
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {opcion: 1,
            id_solicitud: num_solicitud},
        async: false,
        url: '/fintra/controlleropav?estado=Minutas&accion=Contratacion',
        success: function (jsonData) {
            if (!isEmptyJSON(jsonData)) {
                $('#razon_social_empresa').val(jsonData.razon_social_empresa);
                $('#nit_empresa').val(jsonData.nit_empresa);
                $('#direccion_empresa').val(jsonData.direccion_empresa);
                $('#representante_empresa').val(jsonData.nombre_representante);
                $('#cedula_rep_empresa').val(jsonData.cedula_representante);
                CalcularDv('nit_empresa', 'digito_verif_empresa');
                $('#clasificacion').val(jsonData.clasificacion);
                $('#regimen').val(jsonData.regimen);
                $('#gran_contribuyente').val(jsonData.gran_contribuyente);
                $('#agente_retenedor').val(jsonData.agente_retenedor);
                $('#autoretenedor_rfte').val(jsonData.autoret_rfte);
                $('#autoretenedor_iva').val(jsonData.autoret_iva);
                $('#autoretenedor_ica').val(jsonData.autoret_ica);
                $('#porc_admon').val(jsonData.perc_administracion);
                $('#vlr_admon').val(numberConComas(jsonData.administracion));
                $('#porc_imprevisto').val(jsonData.perc_imprevisto);
                $('#vlr_imprevisto').val(numberConComas(jsonData.imprevisto));
                $('#porc_utilidad').val(jsonData.perc_utilidad);
                $('#vlr_utilidad').val(numberConComas(jsonData.utilidad));
                $('#porc_aiu').val(jsonData.perc_aiu);
                $('#valor_aiu').val(numberConComas(jsonData.valor_aiu));
                $('#vlr_antes_iva').val(numberConComas(jsonData.subtotal));
                $('#base_calculo_an').val(numberConComas(jsonData.subtotal));
                $('#base_calculo_ret').val(numberConComas(jsonData.subtotal));
                $('#porc_iva').val(jsonData.perc_iva);
                $('#valor_iva').val(numberConComas(jsonData.valor_iva));
                //($("#tbl_cond_comerciales tr.valores_iva").is(':visible'))?alert("Total IVA"):alert("Total aiu");
                ($("#tbl_cond_comerciales tr.valores_iva").is(':visible')) ? $('#valor_total_iva').val(numberConComas(jsonData.total)) : $('#valor_total_aiu').val(numberConComas(jsonData.total));
                $('#anticipo').val(jsonData.anticipo);
                habdesAnt(false);
                $('#porc_anticipo').val(jsonData.perc_anticipo);
                $('#valor_anticipo').val(numberConComas(jsonData.valor_anticipo));
                $('#retegarantia').val(jsonData.retegarantia);
                habdesRet(false);
                $('#porc_retegarantia').val(jsonData.perc_retegarantia);
                $('#valor_retegarantia').val(numberConComas(jsonData.valor_retegarantia));

                $('#num_contrato').val(jsonData.num_contrato);
                $('input[name=tipo_contrato]').val([jsonData.tipo_contrato]);
                $('#asignado_broker').val(jsonData.asignado_broker);
                //  $('.titulotablita').html("<b>"+jsonData.nombre_proyecto+"</b>");              

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function validaCamposFormContrato() {
    var sw = false;
    var razon_social_empresa = $('#razon_social_empresa').val();
    var nit_empresa = $('#nit_empresa').val();
    var direccion_empresa = $('#direccion_empresa').val();
    var representante_empresa = $('#representante_empresa').val();
    var cedula_rep_empresa = $('#cedula_rep_empresa').val();

    var razon_social_contratista = $('#razon_social_contratista').val();
    var nit_contratista = $('#nit_contratista').val();
    var direccion_contratista = $('#direccion_contratista').val();
    var representante_contratista = $('#representante_contratista').val();
    var cedula_rep_contratista = $('#cedula_rep_contratista').val();

    var nombre_proyecto = $('#nombre_proyecto').val();
    var alcance_proyecto = $('#alcance_proyecto').val();
    var fecha_inicio_proyecto = $('#fecha_inicio_proyecto').val();
    var fecha_fin_proyecto = $('#fecha_fin_proyecto').val();

    var vlr_antes_iva = $('#vlr_antes_iva').val();
    var porc_admon = $('#porc_admon').val();
    var porc_aiu = $('#porc_aiu').val();
    var valor_aiu = $('#valor_aiu').val();
    var porc_imprevisto = $('#porc_imprevisto').val();
    var porc_utilidad = $('#porc_utilidad').val();
    var porc_iva = $('#porc_iva').val();
    var valor_iva = $('#valor_iva').val();

    var porc_anticipo = $('#porc_anticipo').val();
    var fact_parciales = $('#fact_parciales').val();

    if (razon_social_empresa === '' || nit_empresa === '' || direccion_empresa === '' || representante_empresa === '' || cedula_rep_empresa === '' || razon_social_contratista === '' || nit_contratista === '' || direccion_contratista === '' || representante_contratista === '' || cedula_rep_contratista === '' || nombre_proyecto === '' || alcance_proyecto === '' || fecha_inicio_proyecto === '' || fecha_fin_proyecto === ''
            || vlr_antes_iva === '' || porc_admon === '' || porc_aiu === '' || valor_aiu === '' || porc_imprevisto === '' || porc_utilidad === '' || porc_iva === '' || valor_iva === '' || porc_anticipo === '' || fact_parciales === '') {
        mensajesDelSistema("Por favor, diligencie todos los campos obligatorios", '250', '150');
        return;
    } else if (fecha_fin_proyecto < fecha_inicio_proyecto) {
        mensajesDelSistema('La fecha de finalizaci�n del proyecto no puede ser inferior a la fecha de inicio', '250', '150');
        return;
    } else if (!isNumber(vlr_antes_iva) || !isNumber(porc_admon) || !isNumber(porc_aiu) || !isNumber(valor_aiu) || !isNumber(porc_imprevisto) || !isNumber(porc_utilidad) || !isNumber(porc_iva) || !isNumber(valor_iva) || !isNumber(porc_anticipo)) {
        mensajesDelSistema('Algunos valores son inv�lidos. Por favor, verifique', '250', '150');
        return;
    } else {
        sw = true;
    }
    return sw;
}

function cargarArchivos() {

    //grab all form data  
    var fd = new FormData(document.getElementById('form_generar_contrato'));
    var archivo = document.getElementById('examinar').value;

    if (archivo === "") {
        mensajesDelSistema("No ha seleccionado ning&uacute;n archivo. Por favor, seleccione uno!!", '250', '150');
        return;
    }
    if (!validarNombresArchivos()) {
        return;
    }

    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function () {

        $.ajax({
            url: '/fintra/controlleropav?estado=Minutas&accion=Contratacion&opcion=2',
            type: 'POST',
            data: fd,
            dataType: 'json',
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            success: function (data, textStatus, jqXHR)
            {
                $("#examinar").val("");
                if (data.respuesta === "OK") {
                    $("#dialogLoading").dialog('close');
                    obtenerArchivosCargados();
                } else {
                    $("#dialogLoading").dialog('close');
                    alert(".::ERROR AL CARGAR ARCHIVO::.");
                }
            }
        });

    }, 500);

}

function obtenerArchivosCargados() {
    $('#tbl_archivos_cargados').html('');
    var num_sol = $('#num_solicitud').val();
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {opcion: 3,
            id_solicitud: num_sol},
        async: false,
        url: '/fintra/controlleropav?estado=Minutas&accion=Contratacion',
        success: function (jsonData) {
            if (!isEmptyJSON(jsonData)) {
                for (var i = 0; i < jsonData.length; i++) {
                    var nomarchivo = jsonData[i];
//                       $('#tbl_archivos_cargados').append('<tr class="fila"><td><a target="_blank" onClick="consultarNomarchivo('+num_sol+',"'+nomarchivo+'");" style="cursor:hand" ><strong>'+nomarchivo+'</strong></a>\n\
//                                                           &nbsp;&nbsp;&nbsp;<a href="#" onCLick="EliminarArchivo('+num_sol+','+nomarchivo+');">Eliminar</a></td></tr>');
                    $('#tbl_archivos_cargados').append("<tr class='fila'><td><a target='_blank' onClick=\"consultarNomarchivo(" + num_sol + ",'" + nomarchivo + "'," + i + ");\" style='cursor:hand' ><strong>" + nomarchivo + "</strong></a> &nbsp;&nbsp;&nbsp;<a id='view_file" + i + "' target='_blank' href='#' style='display:none'>Ver</a>\n\
                       &nbsp;&nbsp;&nbsp;<a href='#' onCLick=\"EliminarArchivo(" + num_sol + ",'" + nomarchivo + "');\">Eliminar</a></td></tr>");
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function consultarNomarchivo(num_sol, nomarchivo, index) {
    $.ajax({
        type: "POST",
        url: '/fintra/controlleropav?estado=Minutas&accion=Contratacion',
        async: false,
        dataType: "json",
        data: {
            opcion: 4,
            num_solicitud: num_sol,
            nomarchivo: nomarchivo
        },
        success: function (json) {
            //alert(data);
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "SI") {
                    $('#view_file' + index).attr("href", "/fintra/images/multiservicios/" + json.login + "/" + nomarchivo);
                    $('#view_file' + index).fadeIn();
                } else {
                    mensajesDelSistema(".::ERROR AL OBTENER ARCHIVO::.", '250', '150');
                }

            }
        }
    });
}

function EliminarArchivo(num_sol, nomarchivo) {
    $.ajax({
        type: "POST",
        url: '/fintra/controlleropav?estado=Minutas&accion=Contratacion',
        async: false,
        dataType: "json",
        data: {
            opcion: 5,
            num_solicitud: num_sol,
            nomarchivo: nomarchivo
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {
                    obtenerArchivosCargados();
                } else {
                    mensajesDelSistema(".::ERROR AL ELIMINAR ARCHIVO::.", '250', '150');
                }

            }
        }
    });
}

function validarNombresArchivos() {
    var archivo = document.getElementById('examinar');
    for (var i = 0; i < archivo.files.length; i++) {
        var file = archivo.files[i].name;
        var fname = file.substring(0, file.lastIndexOf('.'));
        if (!fname.match(/^[0-9a-zA-Z\_\-\ ]*$/)) {
            alert('Nombre de archivo no v�lido. por favor, Verifique. Archivo: ' + file);
            return false;
        }
    }
    return true;
}

function guardarContrato() {

    var filasGarantias = jQuery("#tabla_garantias").jqGrid('getRowData');
    var filasGarantiasExtra = jQuery("#tabla_garantias_extra").jqGrid('getRowData');

    loading("Espere un momento por favor...", "270", "140");
    setTimeout(function () {

        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Minutas&accion=Contratacion',
            dataType: 'json',
            async: false,
            data: {
                opcion: 6,
                num_solicitud: $('#num_solicitud').val(),
                tipo_contrato: $("input[name=tipo_contrato]:checked").val(),
                num_contrato: $('#num_contrato').val(),
                vlr_antes_iva: numberSinComas($('#vlr_antes_iva').val()),
                porc_admon: numberSinComas($('#porc_admon').val()),
                valor_admon: numberSinComas($('#vlr_admon').val()),
                porc_imprevisto: numberSinComas($('#porc_imprevisto').val()),
                valor_imprevisto: numberSinComas($('#vlr_imprevisto').val()),
                porc_utilidad: numberSinComas($('#porc_utilidad').val()),
                valor_utilidad: numberSinComas($('#vlr_utilidad').val()),
                porc_aiu: numberSinComas($('#porc_aiu').val()),
                valor_aiu: numberSinComas($('#valor_aiu').val()),
                porc_iva: numberSinComas($('#porc_iva').val()),
                valor_iva: numberSinComas($('#valor_iva').val()),
                valor_total: numberSinComas(($("#tbl_cond_comerciales tr.valores_iva").is(':visible')) ? $('#valor_total_iva').val() : $('#valor_total_aiu').val()),
                porc_anticipo: numberSinComas($('#porc_anticipo').val()),
                nit_empresa: $('#nit_empresa').val(),
                cedula_rep_empresa: $('#cedula_rep_empresa').val(),
                listadogarantias: JSON.stringify({garantias: filasGarantias}),
                listadogarantias_extra: JSON.stringify({garantiasExtra: filasGarantiasExtra}),
                infoProveedor: infoProveedorToJSON(),
                infoFormasPago: infoFormasPagoToJSON()

            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    if (json.respuesta === "OK") {
                        cargarInfoSolicitud($('#num_solicitud').val());
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("CONTRATO GUARDADO", '250', '150', true);
                        cargarGarantias();
                        cargarOtroSi();
                        cargarGarantiasExtraContractuales();

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se pudo guardar el contrato!!", '250', '150');
                    }

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

    }, 500);

}

function cargarFacturacionParcial() {

    var grid_tbl_facturacion = jQuery("#tabla_facturacion");
    if ($("#gview_tabla_facturacion").length) {
        refrescarGridFacturacionParcial();
    } else {
        grid_tbl_facturacion.jqGrid({
            caption: "Facturas Parciales",
            url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
            datatype: "json",
            height: '150',
            width: '600',
            colNames: ['Id', 'Valor', 'Dias Ejecucion', 'F. Facturacion', 'Dia Pago', 'Amortizaci�n', 'Retegarantia'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'valor', index: 'valor', sortable: true, editable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'dias_ejecucion', index: 'dias_ejecucion', editable: true, width: 80, align: 'center',
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'fecha_facturacion', index: 'fecha_facturacion', editable: false, width: 80, align: 'left'},
                {name: 'dia_pago', index: 'dia_pago', editable: true, width: 70, resizable: false, sortable: true, align: 'center', edittype: 'select',
                    editoptions: {
                        value: jsonDiasPago,
                        defaultValue: 1
                    }
                },
                {name: 'amortizacion', index: 'amortizacion', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'retegarantia', index: 'retegarantia', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_facturacion'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 7,
                    num_solicitud: $('#num_solicitud').val()
                }
            },
            gridComplete: function () {
                var colSumValor = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum');
                var colSumAmort = jQuery("#tabla_facturacion").jqGrid('getCol', 'amortizacion', false, 'sum');
                var colSumRet = jQuery("#tabla_facturacion").jqGrid('getCol', 'retegarantia', false, 'sum');
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor: colSumValor});
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {amortizacion: colSumAmort});
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {retegarantia: colSumRet});
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                jQuery("#tabla_facturacion").jqGrid('editRow', rowid, true, function () {
                    //$("input, select", e.target).focus();
                }, null, null, {}, function (rowid) {
                    var valor = $("#tabla_facturacion").getRowData(rowid).valor;
                    var dias_ejecucion = $("#tabla_facturacion").getRowData(rowid).dias_ejecucion;
                    var fecha_facturacion = sumaFecha(dias_ejecucion);
                    jQuery("#tabla_facturacion").jqGrid('setCell', rowid, 'fecha_facturacion', fecha_facturacion);
                    jQuery("#tabla_facturacion").jqGrid('setCell', rowid, 'fecha_facturacion', fecha_facturacion);
                    var sumValor = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum');
                    var total = numberSinComas(($("#tbl_cond_comerciales tr.valores_iva").is(':visible')) ? $('#valor_total_iva').val() : $('#valor_total_aiu').val());

                    if (sumValor > total) {
                        setTimeout(function () {
                            mensajesDelSistema("EL VALOR DE LAS CUOTAS EXCEDE EL TOTAL", '320', '165');
                        }, 500);
                        $("#tabla_facturacion").jqGrid('delRowData', rowid);
                        calculateAmortizacion();
                        return;
                    }
                    calculateAmortizacion();
                    jQuery("#tabla_facturacion").jqGrid('setCell', rowid, 'retegarantia', parseFloat((valor * $('#porc_retegarantia').val()) / 100));
                    var sumAmort = jQuery("#tabla_facturacion").jqGrid('getCol', 'amortizacion', false, 'sum');
                    var sumumRet = jQuery("#tabla_facturacion").jqGrid('getCol', 'retegarantia', false, 'sum');
                    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor: sumValor});
                    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {amortizacion: sumAmort});
                    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {retegarantia: sumumRet});

                    //alert(rowid);
                });
                return;
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_facturacion", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_facturacion").jqGrid("navButtonAdd", "#page_tabla_facturacion", {
            caption: "Agregar",
            title: "Agregar Facturacion",
            onClickButton: function () {
                var grid = $("#tabla_facturacion")
                        , rowid = 'neo_' + grid.getRowData().length;
                var defaultData = {valor: 0, dias_ejecucion: 0, dia_pago: 0, amortizacion: 0, retegarantia: 0};
                grid.addRowData(rowid, defaultData);
                grid.jqGrid('editRow', rowid, true, function () {
                    //  $("input, select", e.target).focus();
                }, null, null, {}, function (rowid) {
                    var valor = $("#tabla_facturacion").getRowData(rowid).valor;
                    var dias_ejecucion = $("#tabla_facturacion").getRowData(rowid).dias_ejecucion;
                    var fecha_facturacion = sumaFecha(dias_ejecucion);
                    jQuery("#tabla_facturacion").jqGrid('setCell', rowid, 'fecha_facturacion', fecha_facturacion);
                    var sumValor = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum');
                    var total = numberSinComas(($("#tbl_cond_comerciales tr.valores_iva").is(':visible')) ? $('#valor_total_iva').val() : $('#valor_total_aiu').val());

                    if (sumValor > total) {
                        setTimeout(function () {
                            mensajesDelSistema("EL VALOR DE LAS CUOTAS EXCEDE EL TOTAL", '320', '165');
                        }, 500);
                        $("#tabla_facturacion").jqGrid('delRowData', rowid);
                        calculateAmortizacion();
                        return;
                    }

                    calculateAmortizacion();
                    jQuery("#tabla_facturacion").jqGrid('setCell', rowid, 'retegarantia', parseFloat((valor * $('#porc_retegarantia').val()) / 100));
                    var sumAmort = jQuery("#tabla_facturacion").jqGrid('getCol', 'amortizacion', false, 'sum');
                    var sumumRet = jQuery("#tabla_facturacion").jqGrid('getCol', 'retegarantia', false, 'sum');
                    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor: sumValor});
                    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {amortizacion: sumAmort});
                    jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {retegarantia: sumumRet});
                    //alert(rowid);                   
                });
                //alert('Ha pulsado sobre el boton Agregar');
            }
        });

    }
}


function refrescarGridFacturacionParcial() {
    jQuery("#tabla_facturacion").setGridParam({
        url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 7,
                num_solicitud: $('#num_solicitud').val()
            }
        }
    });

    jQuery('#tabla_facturacion').trigger("reloadGrid");
}

function cargarGarantias() {

    var grid_tbl_garantias = jQuery("#tabla_garantias");
    if ($("#gview_tabla_garantias").length) {
        refrescarGridGarantias();
    } else {
        grid_tbl_garantias.jqGrid({
            caption: "Garant�as",
            url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
            datatype: "json",
            height: '150',
            width: '1180',
            colNames: ['Id', 'Cot. Aceptada', '', 'Id Poliza', 'Poliza', 'Id Beneficiario', 'IdTipoEntrada', 'Tipo Entrada', 'Valor Base', '% Poliza', 'Valor Asegurar', 'Vigencia', 'Id Causal', 'Causal', 'Valor Prima', 'CxP Generada'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'cotizacion_aceptada', index: 'cotizacion_aceptada', width: '80px', align: 'center', hidden: true},
                {name: 'flag_aceptado', index: 'flag_aceptado', width: '40px', align: 'center', fixed: true},
                {name: 'id_poliza', index: 'id_poliza', width: 80, align: 'left', hidden: true},
                {name: 'poliza', index: 'poliza', editable: true, width: 330, align: 'left', edittype: 'select',
                    editoptions: {
                        value: jsonPolizas,
                        defaultValue: 0,
                        dataInit: function (elem) {
                            if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
                                // we are NOT in the searching bar
                                $(elem).find("option[value=\"\"]").remove();
                                setTimeout(function () {
                                    $(elem).trigger('change');
                                }, 500);
                            }
                        },
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowId = e.target.id.replace("_poliza", "");
                                        var id_poliza = e.target.value;
                                        jQuery("#tabla_garantias").jqGrid('setCell', rowId, 'id_poliza', id_poliza);
                                        var jsonTipoEnt = (id_poliza === '15') ? {0: 'Manual', 1: 'Valor Contrato Antes IVA', 2: 'Valor Contrato IVA Incluido', 3: 'Valor Anticipo Antes IVA', 4: 'Valor Anticipo IVA Incluido'} : {0: 'Manual', 1: 'Valor Contrato Antes IVA', 2: 'Valor Contrato IVA Incluido'};
                                        cargaListaDependentSelect(rowId + "_tipo_entrada", jsonTipoEnt);
                                        var id_tipo_entrada = jQuery("#tabla_garantias").getRowData(rowId).id_tipo_entrada;
                                        $("select#" + rowId + "_tipo_entrada").val(id_tipo_entrada);
                                    } catch (exc) {
                                    }
                                    return;
                                }}, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }}
                        ]}
                },
                {name: 'id_beneficiario', index: 'id_beneficiario', width: 80, align: 'left', hidden: true},
                {name: 'id_tipo_entrada', index: 'id_tipo_entrada', width: 80, align: 'left', hidden: true},
                {name: 'tipo_entrada', index: 'tipo_entrada', editable: true, width: 140, align: 'left', edittype: 'select',
                    editoptions: {
                        value: jsonTipoEntrada,
                        defaultValue: 0,
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowId = e.target.id.replace("_tipo_entrada", "");
                                        var id_tipo_entrada = e.target.value;
                                        jQuery("#tabla_garantias").jqGrid('setCell', rowId, 'id_tipo_entrada', id_tipo_entrada);
                                        if (id_tipo_entrada === '1') {
                                            jQuery("#tabla_garantias").jqGrid('setCell', rowId, 'valor_base', numberSinComas($('#vlr_antes_iva').val()));
                                        } else if (id_tipo_entrada === '2') {
                                            jQuery("#tabla_garantias").jqGrid('setCell', rowId, 'valor_base', numberSinComas(($("#tbl_cond_comerciales tr.valores_iva").is(':visible')) ? $('#valor_total_iva').val() : $('#valor_total_aiu').val()));
                                        } else if (id_tipo_entrada === '3') {
                                            jQuery("#tabla_garantias").jqGrid('setCell', rowId, 'valor_base', numberSinComas($('#vlr_antes_iva').val()) * $('#porc_anticipo').val() / 100);
                                        } else if (id_tipo_entrada === '4') {
                                            jQuery("#tabla_garantias").jqGrid('setCell', rowId, 'valor_base', numberSinComas(($("#tbl_cond_comerciales tr.valores_iva").is(':visible')) ? $('#valor_total_iva').val() : $('#valor_total_aiu').val()) * $('#porc_anticipo').val() / 100);
                                        } else {
                                            jQuery("#tabla_garantias").jqGrid('setCell', rowId, 'valor_base', 0);
                                        }
                                        jQuery("#tabla_garantias").saveRow(rowId);
                                        editRowGarantia('tabla_garantias', rowId);
                                    } catch (exc) {
                                    }
                                    return;
                                }}, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }}
                        ], style: "width: 150px"}
                },
                {name: 'valor_base', index: 'valor_base', sortable: true, editable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                var rowId = e.target.id.replace("_valor_base", "");
                                var id_tipo_entrada = $("#tabla_garantias").getRowData(rowId).id_tipo_entrada;
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) || id_tipo_entrada !== '0') {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'porcentaje_poliza', index: 'porcentaje_poliza', editable: true, width: 60, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                {name: 'valor_poliza', index: 'valor_poliza', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'vigencia_poliza', index: 'vigencia_poliza', editable: true, width: 70, align: 'center',
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'id_causal', index: 'id_causal', width: 80, align: 'left', hidden: true},
                {name: 'causal', index: 'causal', width: 100, align: 'left'},
                {name: 'valor_asegurado', index: 'valor_asegurado', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cxp_aseguradora', index: 'cxp_aseguradora', width: 80, align: 'left', hidden: false}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_garantias'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: true,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 8,
                    num_solicitud: $('#num_solicitud').val(),
                    extracontractual: 'N',
                    otro_si: 'N'
                }
            },
            gridComplete: function () {
                var ids = grid_tbl_garantias.jqGrid('getDataIDs');
                var fila;
                var cotizacion_aceptada;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tbl_garantias.jqGrid("getLocalRow", cl);
                    cotizacion_aceptada = fila.cotizacion_aceptada;
                    if (cotizacion_aceptada === 'N') {
                        be = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                    } else {
                        be = '<img src = "/fintra/images/flag_green.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                    }
                    grid_tbl_garantias.jqGrid('setRowData', ids[i], {flag_aceptado: be});

                }
                var colSumPol = jQuery("#tabla_garantias").jqGrid('getCol', 'valor_poliza', false, 'sum');
                var colSumPrima = jQuery("#tabla_garantias").jqGrid('getCol', 'valor_asegurado', false, 'sum');
                jQuery("#tabla_garantias").jqGrid('footerData', 'set', {valor_poliza: colSumPol});
                jQuery("#tabla_garantias").jqGrid('footerData', 'set', {valor_asegurado: colSumPrima});
            },
            loadComplete: function (rowid, e, iRow, iCol) {
                if ($('#asignado_broker').val() === 'N') {
                    $("#tabla_garantias").contextMenu('myMenu', {
                        bindings: {
                            eliminar: function (rowid) {
                                var myGrid = jQuery("#tabla_garantias"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                                if (selRowIds === null) {
                                    mensajesDelSistema('No hay registros seleccionados', '250', '150');
                                    return;
                                }
                                if (!selRowIds.startsWith("neo_")) {
                                    if ($('#asignado_broker').val() === 'N') {
                                        eliminarGarantiaMinuta(selRowIds, 'tabla_garantias');
                                    } else {
                                        mensajesDelSistema('No es posible eliminar el registro. Ya ha sido asignado', '250', '150');
                                    }
                                } else {
                                    $('#tabla_garantias').jqGrid('delRowData', selRowIds);
                                }
                            }
                        }, onContexMenu: function (event/*, menu*/) {
                        }
                    });
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                if ($('#asignado_broker').val() === 'N')
                    editRowGarantia('tabla_garantias', rowid);
                return;
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_garantias", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        if ($('#asignado_broker').val() === 'N') {
            jQuery("#tabla_garantias").jqGrid("navButtonAdd", "#page_tabla_garantias", {
                caption: "Agregar",
                title: "Agregar",
                onClickButton: function () {
                    var grid = $("#tabla_garantias")
                            , rowid = 'neo_' + grid.getRowData().length;
                    var defaultData = {valor_base: 0, porcentaje_poliza: 0, id_beneficiario: 1, id_tipo_entrada: 0, tipo_entrada: 'Manual', valor_poliza: 0, id_poliza: 0, vigencia_poliza: 0, id_causal: 1, causal: 'NO APLICA', valor_asegurado: 0, cotizacion_aceptada: 'N'};
                    grid.addRowData(rowid, defaultData);
                    editRowGarantia('tabla_garantias', rowid);
                }
            });
        }
    }
}


function refrescarGridGarantias() {
    jQuery("#tabla_garantias").setGridParam({
        url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 8,
                num_solicitud: $('#num_solicitud').val(),
                extracontractual: 'N',
                otro_si: 'N'
            }
        }
    });

    jQuery('#tabla_garantias').trigger("reloadGrid");
}

function editRowGarantia(tableid, rowId) {
    jQuery("#" + tableid).jqGrid('editRow', rowId, true, function () {
        //$("input, select", e.target).focus();
    }, null, null, {}, function (rowid) {
        var valor_base = $("#" + tableid).getRowData(rowid).valor_base;
        var porc_poliza = $("#" + tableid).getRowData(rowid).porcentaje_poliza;
        jQuery("#" + tableid).jqGrid('setCell', rowid, 'valor_poliza', parseFloat((valor_base * porc_poliza) / 100));
        var sumPoliza = jQuery("#" + tableid).jqGrid('getCol', 'valor_poliza', false, 'sum');
        var sumPrima = jQuery("#" + tableid).jqGrid('getCol', 'valor_asegurado', false, 'sum');
        jQuery("#" + tableid).jqGrid('footerData', 'set', {valor_poliza: sumPoliza});
        jQuery("#" + tableid).jqGrid('footerData', 'set', {valor_asegurado: sumPrima});
    });
}

function cargarOtroSi() {

    var grid_tbl_otro_si = jQuery("#tabla_otro_si");
    if ($("#gview_tabla_otro_si").length) {
        refrescarGridOtroSi();
    } else {
        grid_tbl_otro_si.jqGrid({
            caption: "Garant�as Otro Si",
            url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
            datatype: "json",
            height: '150',
            width: '1180',
            colNames: ['Id', 'Descripcion', 'Id Causal', 'Causal', 'Fecha Creacion', 'Usuario Crea', 'Valor'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'descripcion', index: 'descripcion', width: 400, align: 'left'},
                {name: 'id_causal', index: 'id_causal', width: 80, align: 'left', hidden: true},
                {name: 'causal', index: 'causal', editable: false, width: 150, resizable: false, sortable: true, align: 'left', edittype: 'select',
                    editoptions: {
                        value: jsonCausales,
                        defaultValue: 1,
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowId = e.target.id.replace("_causal", "");
                                        var id_causal = e.target.value;
                                        jQuery("#tabla_otro_si").jqGrid('setCell', rowId, 'id_causal', id_causal);
                                    } catch (exc) {
                                    }
                                    return;
                                }}, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }}
                        ]}
                },
                {name: 'fecha_creacion', index: 'fecha_creacion', width: 110, align: 'left'},
                {name: 'usuario_crea', index: 'usuario_crea', width: 65, align: 'left'},
                {name: 'valor_otro_si', index: 'valor_otro_si', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_otro_si'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: true,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 47,
                    num_contrato: $('#num_contrato').val()
                }
            },
            gridComplete: function () {
                var colSumTotal = jQuery("#tabla_otro_si").jqGrid('getCol', 'valor_otro_si', false, 'sum');
                jQuery("#tabla_otro_si").jqGrid('footerData', 'set', {valor_otro_si: colSumTotal});
            },
            loadComplete: function (rowid, e, iRow, iCol) {
                $("#tabla_otro_si").contextMenu('myMenu', {
                    bindings: {
                        eliminar: function (rowid) {
                            var myGrid = jQuery("#tabla_otro_si"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                            if (selRowIds === null) {
                                mensajesDelSistema('No hay registros seleccionados', '250', '150');
                                return;
                            }
                            if (!selRowIds.startsWith("neo_")) {
                                eliminarOtroSi(selRowIds);
                            } else {
                                $('#tabla_otro_si').jqGrid('delRowData', selRowIds);
                            }
                        }
                    }, onContexMenu: function (event/*, menu*/) {
                    }
                });
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                cargarGarantiasOtroSi(rowid);
                AbrirDivDetalleOtroSi(rowid);
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_otro_si", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_otro_si").jqGrid("navButtonAdd", "#page_tabla_otro_si", {
            caption: "Agregar",
            title: "Agregar Otro Si",
            onClickButton: function () {
                insertarOtroSi();
            }
        });
    }
}


function refrescarGridOtroSi() {
    jQuery("#tabla_otro_si").setGridParam({
        url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 47,
                num_contrato: $('#num_contrato').val()
            }
        }
    });

    jQuery('#tabla_otro_si').trigger("reloadGrid");
}


function cargarGarantiasOtroSi(secuencia) {

    var grid_tbl_garantias_otro_si = jQuery("#tabla_garantias_otro_si");
    if ($("#gview_tabla_garantias_otro_si").length) {
        refrescarGridGarantiasOtroSi(secuencia);
    } else {
        grid_tbl_garantias_otro_si.jqGrid({
            caption: "Garantias",
            url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
            datatype: "json",
            height: '175',
            width: '950',
            colNames: ['Id', 'Cot. Aceptada', '', 'Id Poliza', 'Poliza', 'Id Beneficiario', 'IdTipoEntrada', 'Tipo Entrada', 'Valor Base', '% Poliza', 'Valor Asegurar', 'Vigencia', 'Id Causal', 'Causal', 'Valor Prima', 'CxP Generada', 'Secuencia'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'cotizacion_aceptada', index: 'cotizacion_aceptada', width: '80px', align: 'center', hidden: true},
                {name: 'flag_aceptado', index: 'flag_aceptado', width: '40px', align: 'center', fixed: true},
                {name: 'id_poliza', index: 'id_poliza', width: 80, align: 'left', hidden: true},
                {name: 'poliza', index: 'poliza', editable: true, width: 190, align: 'left', edittype: 'select',
                    editoptions: {
                        value: jsonPolizas,
                        defaultValue: 0,
                        dataInit: function (elem) {
                            if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
                                // we are NOT in the searching bar
                                $(elem).find("option[value=\"\"]").remove();
                                setTimeout(function () {
                                    $(elem).trigger('change');
                                }, 500);
                            }
                        },
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowId = e.target.id.replace("_poliza", "");
                                        var id_poliza = e.target.value;
                                        jQuery("#tabla_garantias_otro_si").jqGrid('setCell', rowId, 'id_poliza', id_poliza);
                                        var jsonTipoEnt = (id_poliza === '15') ? {0: 'Manual', 1: 'Valor Contrato Antes IVA', 2: 'Valor Contrato IVA Incluido', 3: 'Valor Anticipo Antes IVA', 4: 'Valor Anticipo IVA Incluido'} : {0: 'Manual', 1: 'Valor Contrato Antes IVA', 2: 'Valor Contrato IVA Incluido'};
                                        cargaListaDependentSelect(rowId + "_tipo_entrada", jsonTipoEnt);
                                        var id_tipo_entrada = jQuery("#tabla_garantias_otro_si").getRowData(rowId).id_tipo_entrada;
                                        $("select#" + rowId + "_tipo_entrada").val(id_tipo_entrada);
                                    } catch (exc) {
                                    }
                                    return;
                                }}, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }}
                        ]}
                },
                {name: 'id_beneficiario', index: 'id_beneficiario', width: 80, align: 'left', hidden: true},
                {name: 'id_tipo_entrada', index: 'id_tipo_entrada', width: 80, align: 'left', hidden: true},
                {name: 'tipo_entrada', index: 'tipo_entrada', editable: true, width: 150, align: 'left', edittype: 'select',
                    editoptions: {
                        value: jsonTipoEntrada,
                        defaultValue: 0,
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowId = e.target.id.replace("_tipo_entrada", "");
                                        var id_tipo_entrada = e.target.value;
                                        jQuery("#tabla_garantias_otro_si").jqGrid('setCell', rowId, 'id_tipo_entrada', id_tipo_entrada);
                                        if (id_tipo_entrada === '1') {
                                            jQuery("#tabla_garantias_otro_si").jqGrid('setCell', rowId, 'valor_base', numberSinComas($('#vlr_antes_iva').val()));
                                        } else if (id_tipo_entrada === '2') {
                                            jQuery("#tabla_garantias_otro_si").jqGrid('setCell', rowId, 'valor_base', numberSinComas(($("#tbl_cond_comerciales tr.valores_iva").is(':visible')) ? $('#valor_total_iva').val() : $('#valor_total_aiu').val()));
                                        } else if (id_tipo_entrada === '3') {
                                            jQuery("#tabla_garantias_otro_si").jqGrid('setCell', rowId, 'valor_base', numberSinComas($('#vlr_antes_iva').val()) * $('#porc_anticipo').val() / 100);
                                        } else if (id_tipo_entrada === '4') {
                                            jQuery("#tabla_garantias_otro_si").jqGrid('setCell', rowId, 'valor_base', numberSinComas(($("#tbl_cond_comerciales tr.valores_iva").is(':visible')) ? $('#valor_total_iva').val() : $('#valor_total_aiu').val()) * $('#porc_anticipo').val() / 100);
                                        } else {
                                            jQuery("#tabla_garantias_otro_si").jqGrid('setCell', rowId, 'valor_base', 0);
                                        }
                                        jQuery("#tabla_garantias_otro_si").saveRow(rowId);
                                        editRowGarantia('tabla_garantias_otro_si', rowId);
                                    } catch (exc) {
                                    }
                                    return;
                                }}, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }}
                        ], style: "width: 150px"}
                },
                {name: 'valor_base', index: 'valor_base', sortable: true, editable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                var rowId = e.target.id.replace("_valor_base", "");
                                var id_tipo_entrada = $("#tabla_garantias_otro_si").getRowData(rowId).id_tipo_entrada;
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) || id_tipo_entrada !== '0') {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'porcentaje_poliza', index: 'porcentaje_poliza', editable: true, width: 60, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                {name: 'valor_poliza', index: 'valor_poliza', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'vigencia_poliza', index: 'vigencia_poliza', editable: true, width: 70, align: 'center',
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'id_causal', index: 'id_causal', width: 80, align: 'left', hidden: true},
                {name: 'causal', index: 'causal', editable: true, width: 150, resizable: false, sortable: true, hidden: true, align: 'left', edittype: 'select',
                    editoptions: {
                        value: jsonCausales,
                        defaultValue: 1,
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowId = e.target.id.replace("_causal", "");
                                        var id_causal = e.target.value;
                                        jQuery("#tabla_garantias_otro_si").jqGrid('setCell', rowId, 'id_causal', id_causal);
                                    } catch (exc) {
                                    }
                                    return;
                                }}, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }}
                        ]}
                },
                {name: 'valor_asegurado', index: 'valor_asegurado', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cxp_aseguradora', index: 'cxp_aseguradora', width: 80, align: 'left', hidden: false},
                {name: 'secuencia', index: 'secuencia', width: 80, align: 'left', hidden: true}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_garantias_otro_si'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 8,
                    num_solicitud: $('#num_solicitud').val(),
                    extracontractual: 'S',
                    otro_si: 'S',
                    secuencia: secuencia
                }
            },
            gridComplete: function () {
                var ids = grid_tbl_garantias_otro_si.jqGrid('getDataIDs');
                var fila;
                var cotizacion_aceptada;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tbl_garantias_otro_si.jqGrid("getLocalRow", cl);
                    cotizacion_aceptada = fila.cotizacion_aceptada;
                    if (cotizacion_aceptada === 'N') {
                        be = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                    } else {
                        be = '<img src = "/fintra/images/flag_green.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                    }
                    grid_tbl_garantias_otro_si.jqGrid('setRowData', ids[i], {flag_aceptado: be});

                }
                var colSumPol = jQuery("#tabla_garantias_otro_si").jqGrid('getCol', 'valor_poliza', false, 'sum');
                var colSumPrima = jQuery("#tabla_garantias_otro_si").jqGrid('getCol', 'valor_asegurado', false, 'sum');
                jQuery("#tabla_garantias_otro_si").jqGrid('footerData', 'set', {valor_poliza: colSumPol});
                jQuery("#tabla_garantias_otro_si").jqGrid('footerData', 'set', {valor_asegurado: colSumPrima});
            },
            loadComplete: function (rowid, e, iRow, iCol) {
                $("#tabla_garantias_otro_si").contextMenu('myMenu', {
                    bindings: {
                        eliminar: function (rowid) {
                            var myGrid = jQuery("#tabla_garantias_otro_si"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                            if (selRowIds === null) {
                                mensajesDelSistema('No hay registros seleccionados', '250', '150');
                                return;
                            }
                            if (!selRowIds.startsWith("neo_")) {
                                eliminarGarantiaMinuta(selRowIds, 'tabla_garantias_otro_si', secuencia);
                            } else {
                                $('#tabla_garantias_otro_si').jqGrid('delRowData', selRowIds);
                            }
                        }
                    }, onContexMenu: function (event/*, menu*/) {
                    }
                });
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                editRowGarantia('tabla_garantias_otro_si', rowid);
                return;
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_garantias_otro_si", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_garantias_otro_si").jqGrid("navButtonAdd", "#page_tabla_garantias_otro_si", {
            caption: "Agregar",
            title: "Agregar",
            onClickButton: function () {
                var grid = $("#tabla_garantias_otro_si")
                        , rowid = 'neo_' + grid.getRowData().length;
                var defaultData = {valor_base: 0, porcentaje_poliza: 0, id_tipo_entrada: 0, tipo_entrada: 'Manual', valor_poliza: 0, id_poliza: 0, vigencia_poliza: 0, id_beneficiario: 1, id_causal: 1, secuencia: secuencia, valor_asegurado: 0, cotizacion_aceptada: 'N'};
                grid.addRowData(rowid, defaultData);
                editRowGarantia('tabla_garantias_otro_si', rowid);
            }
        });
    }
}


function refrescarGridGarantiasOtroSi(secuencia) {
    jQuery("#tabla_garantias_otro_si").setGridParam({
        url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 8,
                num_solicitud: $('#num_solicitud').val(),
                extracontractual: 'S',
                otro_si: 'S',
                secuencia: secuencia
            }
        }
    });

    jQuery('#tabla_garantias_otro_si').trigger("reloadGrid");
}


function cargarGarantiasExtraContractuales() {

    var grid_tbl_garantias_extra = jQuery("#tabla_garantias_extra");
    if ($("#gview_tabla_garantias_extra").length) {
        refrescarGridGarantiasExtraContractuales();
    } else {
        grid_tbl_garantias_extra.jqGrid({
            caption: "Garant�as ExtraContractuales",
            url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
            datatype: "json",
            height: '150',
            width: '1180',
            colNames: ['Id', 'Cot. Aceptada', '', 'Id Poliza', 'Poliza', 'Id Beneficiario', 'Beneficiario', 'IdTipoEntrada', 'Tipo Entrada', 'Valor Base', '% Poliza', 'Valor Asegurar', 'Vigencia', 'Id Causal', 'Causal', 'Valor Prima', 'CxP Generada'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'cotizacion_aceptada', index: 'cotizacion_aceptada', width: '80px', align: 'center', hidden: true},
                {name: 'flag_aceptado', index: 'flag_aceptado', width: '40px', align: 'center', fixed: true},
                {name: 'id_poliza', index: 'id_poliza', width: 80, align: 'left', hidden: true},
                {name: 'poliza', index: 'poliza', editable: true, width: 190, align: 'left', edittype: 'select',
                    editoptions: {
                        value: jsonPolizas,
                        defaultValue: 0,
                        dataInit: function (elem) {
                            if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
                                // we are NOT in the searching bar
                                $(elem).find("option[value=\"\"]").remove();
                                setTimeout(function () {
                                    $(elem).trigger('change');
                                }, 500);
                            }
                        },
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowId = e.target.id.replace("_poliza", "");
                                        var id_poliza = e.target.value;
                                        jQuery("#tabla_garantias_extra").jqGrid('setCell', rowId, 'id_poliza', id_poliza);
                                        var jsonTipoEnt = (id_poliza === '15') ? {0: 'Manual', 1: 'Valor Contrato Antes IVA', 2: 'Valor Contrato IVA Incluido', 3: 'Valor Anticipo Antes IVA', 4: 'Valor Anticipo IVA Incluido'} : {0: 'Manual', 1: 'Valor Contrato Antes IVA', 2: 'Valor Contrato IVA Incluido'};
                                        cargaListaDependentSelect(rowId + "_tipo_entrada", jsonTipoEnt);
                                        var id_tipo_entrada = jQuery("#tabla_garantias_extra").getRowData(rowId).id_tipo_entrada;
                                        $("select#" + rowId + "_tipo_entrada").val(id_tipo_entrada);
                                    } catch (exc) {
                                    }
                                    return;
                                }}, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }}
                        ]}
                },
                {name: 'id_beneficiario', index: 'id_beneficiario', width: 80, align: 'left', hidden: true},
                {name: 'beneficiario', index: 'beneficiario', editable: true, width: 130, resizable: false, sortable: true, align: 'left', edittype: 'select',
                    editoptions: {
                        value: jsonBeneficiarios,
                        defaultValue: 1,
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowId = e.target.id.replace("_beneficiario", "");
                                        var id_beneficiario = e.target.value;
                                        jQuery("#tabla_garantias_extra").jqGrid('setCell', rowId, 'id_beneficiario', id_beneficiario);
                                    } catch (exc) {
                                    }
                                    return;
                                }}, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }}
                        ]}
                },
                {name: 'id_tipo_entrada', index: 'id_tipo_entrada', width: 80, align: 'left', hidden: true},
                {name: 'tipo_entrada', index: 'tipo_entrada', editable: true, width: 130, align: 'left', edittype: 'select',
                    editoptions: {
                        value: jsonTipoEntrada,
                        defaultValue: 0,
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowId = e.target.id.replace("_tipo_entrada", "");
                                        var id_tipo_entrada = e.target.value;
                                        jQuery("#tabla_garantias_extra").jqGrid('setCell', rowId, 'id_tipo_entrada', id_tipo_entrada);
                                        if (id_tipo_entrada === '1') {
                                            jQuery("#tabla_garantias_extra").jqGrid('setCell', rowId, 'valor_base', numberSinComas($('#vlr_antes_iva').val()));
                                        } else if (id_tipo_entrada === '2') {
                                            jQuery("#tabla_garantias_extra").jqGrid('setCell', rowId, 'valor_base', numberSinComas(($("#tbl_cond_comerciales tr.valores_iva").is(':visible')) ? $('#valor_total_iva').val() : $('#valor_total_aiu').val()));
                                        } else if (id_tipo_entrada === '3') {
                                            jQuery("#tabla_garantias_extra").jqGrid('setCell', rowId, 'valor_base', numberSinComas($('#vlr_antes_iva').val()) * $('#porc_anticipo').val() / 100);
                                        } else if (id_tipo_entrada === '4') {
                                            jQuery("#tabla_garantias_extra").jqGrid('setCell', rowId, 'valor_base', numberSinComas(($("#tbl_cond_comerciales tr.valores_iva").is(':visible')) ? $('#valor_total_iva').val() : $('#valor_total_aiu').val()) * $('#porc_anticipo').val() / 100);
                                        } else {
                                            jQuery("#tabla_garantias_extra").jqGrid('setCell', rowId, 'valor_base', 0);
                                        }
                                        jQuery("#tabla_garantias_extra").saveRow(rowId);
                                        editRowGarantia('tabla_garantias_extra', rowId);
                                    } catch (exc) {
                                    }
                                    return;
                                }}, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }}
                        ], style: "width: 150px"}
                },
                {name: 'valor_base', index: 'valor_base', sortable: true, editable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                var rowId = e.target.id.replace("_valor_base", "");
                                var id_tipo_entrada = $("#tabla_garantias_extra").getRowData(rowId).id_tipo_entrada;
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) || id_tipo_entrada !== '0') {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'porcentaje_poliza', index: 'porcentaje_poliza', editable: true, width: 50, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                {name: 'valor_poliza', index: 'valor_poliza', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'vigencia_poliza', index: 'vigencia_poliza', editable: true, width: 50, align: 'center',
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'id_causal', index: 'id_causal', width: 80, align: 'left', hidden: true},
                {name: 'causal', index: 'causal', editable: true, width: 150, resizable: false, sortable: true, align: 'left', edittype: 'select',
                    editoptions: {
                        value: jsonCausales,
                        defaultValue: 1,
                        dataEvents: [{type: 'change', fn: function (e) {
                                    try {
                                        var rowId = e.target.id.replace("_causal", "");
                                        var id_causal = e.target.value;
                                        jQuery("#tabla_garantias_extra").jqGrid('setCell', rowId, 'id_causal', id_causal);
                                    } catch (exc) {
                                    }
                                    return;
                                }}, {type: "keyup", fn: function (e) {
                                    $(e.target).trigger("change");
                                }}
                        ]}
                },
                {name: 'valor_asegurado', index: 'valor_asegurado', sortable: true, width: 100, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cxp_aseguradora', index: 'cxp_aseguradora', width: 80, align: 'left', hidden: false}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_garantias_extra'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: true,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 8,
                    num_solicitud: $('#num_solicitud').val(),
                    extracontractual: 'S',
                    otro_si: 'N'
                }
            },
            gridComplete: function () {
                var ids = grid_tbl_garantias_extra.jqGrid('getDataIDs');
                var fila;
                var cotizacion_aceptada;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tbl_garantias_extra.jqGrid("getLocalRow", cl);
                    cotizacion_aceptada = fila.cotizacion_aceptada;
                    if (cotizacion_aceptada === 'N') {
                        be = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                    } else {
                        be = '<img src = "/fintra/images/flag_green.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                    }
                    grid_tbl_garantias_extra.jqGrid('setRowData', ids[i], {flag_aceptado: be});

                }
                var colSumPol = jQuery("#tabla_garantias_extra").jqGrid('getCol', 'valor_poliza', false, 'sum');
                var colSumPrima = jQuery("#tabla_garantias_extra").jqGrid('getCol', 'valor_asegurado', false, 'sum');
                jQuery("#tabla_garantias_extra").jqGrid('footerData', 'set', {valor_poliza: colSumPol});
                jQuery("#tabla_garantias_extra").jqGrid('footerData', 'set', {valor_asegurado: colSumPrima});
            },
            loadComplete: function (rowid, e, iRow, iCol) {
                $("#tabla_garantias_extra").contextMenu('myMenu', {
                    bindings: {
                        eliminar: function (rowid) {
                            var myGrid = jQuery("#tabla_garantias_extra"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                            if (selRowIds === null) {
                                mensajesDelSistema('No hay registros seleccionados', '250', '150');
                                return;
                            }
                            if (!selRowIds.startsWith("neo_")) {
                                eliminarGarantiaMinuta(selRowIds, 'tabla_garantias_extra');
                            } else {
                                $('#tabla_garantias_extra').jqGrid('delRowData', selRowIds);
                            }
                        }
                    }, onContexMenu: function (event/*, menu*/) {
                    }
                });
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                editRowGarantia('tabla_garantias_extra', rowid);
                return;
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_garantias_extra", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_garantias_extra").jqGrid("navButtonAdd", "#page_tabla_garantias_extra", {
            caption: "Agregar",
            title: "Agregar",
            onClickButton: function () {
                var grid = $("#tabla_garantias_extra")
                        , rowid = 'neo_' + grid.getRowData().length;
                var defaultData = {valor_base: 0, porcentaje_poliza: 0, id_tipo_entrada: 0, tipo_entrada: 'Manual', valor_poliza: 0, id_poliza: 0, vigencia_poliza: 0, id_beneficiario: 0, id_causal: 1, valor_asegurado: 0, cotizacion_aceptada: 'N'};
                grid.addRowData(rowid, defaultData);
                editRowGarantia('tabla_garantias_extra', rowid);
            }
        });
    }
}


function refrescarGridGarantiasExtraContractuales() {
    jQuery("#tabla_garantias_extra").setGridParam({
        url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 8,
                num_solicitud: $('#num_solicitud').val(),
                extracontractual: 'S',
                otro_si: 'N'
            }
        }
    });

    jQuery('#tabla_garantias_extra').trigger("reloadGrid");
}

function AbrirDivDetalleOtroSi(rowid) {
    $("#div_detalle_otro_si").dialog({
        width: 975,
        height: 410,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'DETALLE OTRO SI ' + rowid,
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                if (validateGridGarantias('tabla_garantias_otro_si')) {
                    guardarDetalleOtroSi(rowid);
                }
            },
            "Salir": function () {
                mensajeConfirmAction("Recuerde guardar los cambios. Esta seguro que desea salir ?", "230", "150", cerrarDialogo, "div_detalle_otro_si");
            }
        }
    });

    $(".ui-dialog-titlebar-close").hide();
}

function insertarOtroSi() {

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Minutas&accion=Contratacion',
        dataType: 'json',
        async: false,
        data: {
            opcion: 48,
            num_contrato: $('#num_contrato').val()
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (json.respuesta === "OK") {
                    refrescarGridOtroSi();
                } else {
                    mensajesDelSistema("Lo sentimos no se pudo insertar el otro si!!", '250', '150');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function guardarDetalleOtroSi(secuencia) {

    var filasGarantiasOtroSi = jQuery("#tabla_garantias_otro_si").jqGrid('getRowData');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Minutas&accion=Contratacion',
        dataType: 'json',
        async: false,
        data: {
            opcion: 49,
            num_contrato: $('#num_contrato').val(),
            listadogarantias_otro_si: JSON.stringify({garantiasOtroSi: filasGarantiasOtroSi})
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (json.respuesta === "OK") {
                    mensajesDelSistema("Informaci�n guardada exitosamente", '250', '150', true);
                    refrescarGridGarantiasOtroSi(secuencia);
                    refrescarGridOtroSi();
                } else {
                    mensajesDelSistema("Lo sentimos no se pudo guardar las garantias!!", '250', '150');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function eliminarOtroSi(secuencia) {
    $.ajax({
        type: "POST",
        url: '/fintra/controlleropav?estado=Minutas&accion=Contratacion',
        async: false,
        dataType: "json",
        data: {
            opcion: 50,
            num_contrato: $('#num_contrato').val(),
            secuencia: secuencia
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridOtroSi();
                } else {
                    mensajesDelSistema(".::ERROR AL ANULAR OTRO SI::.", '250', '150');
                }

            }
        }
    });
}


function cargaListaDependentSelect(rowId, json) {

    var newOptions = '';
    var select = $("select#" + rowId);
    select.empty();
    for (var jsonId in json) {
        newOptions += '<option role="option" value="' + jsonId + '">' + json[jsonId] + '</option>';
    }
    select.html(newOptions);

}

function listarComboGrid(op, param) {
    var Result = {};
    $.ajax({
        type: 'GET',
        url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion&opcion=" + op + "&param=" + param,
        dataType: 'json',
        async: false,
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    Result = {};
                } else {
                    Result = json;
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return Result;
}

function cargarCboClasificacionPersona() {
    $.ajax({
        type: 'GET',
        url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion&opcion=14",
        dataType: 'json',
        async: false,
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }
                try {
                    $('#clasificacion').append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#clasificacion').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function numbersonly(myfield, e, dec)
{
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

// control keys
    if ((key == null) || (key == 0) || (key == 8) ||
            (key == 9) || (key == 13) || (key == 27))
        return true;

// numbers
    else if ((("0123456789.").indexOf(keychar) > -1))
        return true;

// decimal point jump
    else if (dec && (keychar == "."))
    {
        myfield.form.elements[dec].focus();
        return false;
    }
    else
        return false;
}

function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}


function sumaFecha(d, fecha)
{
    var Fecha = new Date();
    var sFecha = fecha || (Fecha.getDate() + "-" + (Fecha.getMonth() + 1) + "-" + Fecha.getFullYear());
    var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
    var aFecha = sFecha.split(sep);
    var fecha = aFecha[2] + '/' + aFecha[1] + '/' + aFecha[0];
    fecha = new Date(fecha);
    fecha.setDate(fecha.getDate() + parseInt(d));
    var anno = fecha.getFullYear();
    var mes = fecha.getMonth() + 1;
    var dia = fecha.getDate();
    mes = (mes < 10) ? ("0" + mes) : mes;
    dia = (dia < 10) ? ("0" + dia) : dia;
    var fechaFinal = anno + sep + mes + sep + dia;
    return (fechaFinal);
}

function calculateAmortizacion() {
    var num_records = jQuery("#tabla_facturacion").jqGrid('getGridParam', 'records');
    var valor_amort = ((parseInt(num_records) > 0) ? (parseFloat(numberSinComas($('#valor_anticipo').val())) / parseInt(num_records)) : 0);
    var ids = jQuery("#tabla_facturacion").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        jQuery("#tabla_facturacion").jqGrid('setCell', ids[i], 'amortizacion', valor_amort);
    }
}

function validateGridFacturacion() {
    var estado = true;
    var ids = jQuery("#tabla_facturacion").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var valor = jQuery("#tabla_facturacion").getRowData(ids[i]).valor;
        if (parseFloat(valor) === 0) {
            estado = false;
            mensajesDelSistema('Facturas Parciales: Informacion invalida en fila ' + (i + 1), '250', '150');
        }
    }
    return estado;
}

function validateGridGarantias(tableid) {
    var estado = true;
    var ids = jQuery("#" + tableid).jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id_poliza = jQuery("#" + tableid).getRowData(ids[i]).id_poliza;
        var id_beneficiario = jQuery("#" + tableid).getRowData(ids[i]).id_beneficiario;
        var id_causal = jQuery("#" + tableid).getRowData(ids[i]).id_causal;
        var valor_base = jQuery("#" + tableid).getRowData(ids[i]).valor_base;
        if (parseFloat(valor_base) === 0 || id_poliza === '0' || id_beneficiario === '0' || id_causal === '0') {
            estado = false;
            var tipo_polizas = (tableid === 'tabla_garantias_otro_si') ? 'Otro Si' : (tableid === 'tabla_garantias_extra') ? 'Extracontractuales' : '';
            mensajesDelSistema('Garant�as ' + tipo_polizas + ': Informaci�n inv�lida en fila ' + (i + 1), '250', '150');
        }
    }
    return estado;
}

function infoProveedorToJSON() {
    return JSON.stringify({
        "nit_empresa": $('#nit_empresa').val(),
        "razon_social": $('#razon_social_empresa').val(),
        "digito_verificacion": $('#digito_verif_empresa').val(),
        "clasificacion": $("#clasificacion").val(),
        "regimen": $("#regimen").val(),
        "gran_contribuyente": $("#gran_contribuyente").val(),
        "agente_retenedor": $('#agente_retenedor').val(),
        "autoretenedor_ica": $('#autoretenedor_ica').val(),
        "autoretenedor_iva": $('#autoretenedor_iva').val(),
        "autoretenedor_rfte": $('#autoretenedor_rfte').val()
    });
}

function eliminarGarantiaMinuta(id_garantia, tableid, param) {
    $.ajax({
        type: "POST",
        url: '/fintra/controlleropav?estado=Minutas&accion=Contratacion',
        async: false,
        dataType: "json",
        data: {
            opcion: 36,
            id: id_garantia
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {
                    (tableid === 'tabla_garantias_extra') ? refrescarGridGarantiasExtraContractuales() : (tableid === 'tabla_garantias_otro_si') ? refrescarGridGarantiasOtroSi(param) : refrescarGridGarantias();
                } else {
                    mensajesDelSistema(".::ERROR AL ELIMINAR GARANTIA::.", '250', '150');
                }

            }
        }
    });
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajeConfirmAction(msj, width, height, okAction, id) {
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function mensajesGuardarGarantia(msj, width, height) {
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                guardarCondicionesComercialesImagenes();
                guardarContrato();
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function resetAddressValues() {
    $("#dir_resul").val('');
    $("#nom_princip_dir").val('');
    $("#nom_genera_dir").val('');
    $("#placa_dir").val('');
    $("#cmpl_dir").val('');
    $("#via_princip_dir").val('');
    $("#via_genera_dir").val('');
    $("#desc_bodega").val('');
    $('#dep_dir').val('ATL');
    $('#ciu_dir').val('BQ');
}


function resetSearchValuesSol() {
    $('#fecha_ini_approv_solicitud').val('');
    $('#fecha_fin_approv_solicitud').val('');
    $('#num_solicitud').val('');
    $('#linea_negocio').val('');
    $('#responsable').val('');
    $('#etapaActual').val('');
    $('#id_cliente').val('');
    $('#txt_nom_proyecto').val('');
    $('#txt_foms').val('');
    $('#tipo_proyecto').val('');
}


function DeshabilitarFiltroSearchSol(estado) {
    (estado) ? $('#fecha_ini_approv_solicitud').datepicker("destroy") : $('#fecha_ini_approv_solicitud').datepicker({dateFormat: 'yy-mm-dd'});
    (estado) ? $('#fecha_fin_approv_solicitud').datepicker("destroy") : $('#fecha_fin_approv_solicitud').datepicker({dateFormat: 'yy-mm-dd'});
    $('#num_solicitud').attr({readonly: estado});
}


function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function Minutas(id_solicitud) {
    var filas = jQuery("#tabla_solicitudes_pendientes").getRowData(id_solicitud);
    var nombre_proyecto = filas.nombre_proyecto;
    var aiu = (filas.modalidad_comercial === '1') ? 'S' : 'N';
    window.open("/fintra/jsp/opav/minutas/generarContrato.jsp?num_solicitud=" + id_solicitud + "&proyecto=" + nombre_proyecto + "&aiu=" + aiu, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
}


//////MMEDINA
function  mostrarVentanaAccion(id_solicitud) {
    var grid_tabla = jQuery("#tabla_solicitudes_pendientes");
    var trazabilidad = grid_tabla.getRowData(id_solicitud).id_trazabilidad;
    var id_estado = grid_tabla.getRowData(id_solicitud).id_estado;
    var nombre_estado = grid_tabla.getRowData(id_solicitud).nombre_estado;
    $('#idsolicitud').val(id_solicitud);
    $('#estadoActual').val(nombre_estado);
    $('#idestadoActual').val(id_estado);
    $('#descripcion_causal').val('');
    cargarEtapas(trazabilidad);
    cargarTrazabilidadOferta(id_solicitud);
    cargarEstadoEtapas(id_estado);
    $("#cambioEtapa").dialog({
        width: '800',
        height: '475',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Cambio de etapa',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Actualizar": function () {
                cambiarEstado();
            },
            "Salir": function () {
                $(this).dialog("close");

            }
        }
    });
}

function cargarEtapas(trazabilidad) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 40,
            etapa: trazabilidad
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#etapa').val('');
                $('#idetapa').val('');
                for (var datos in json) {
                    $('#etapa').val(json[datos]);
                    $('#idetapa').val(datos);
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarEstadoEtapas(id_estado) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 41,
            etapa: 4,
            id_estado: id_estado
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#estado').html('');
                $('#estado').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#estado').append('<option value=' + json[datos].id_estado_destino + '>' + json[datos].nombre_estado + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cambiarEstado() {

    var estado = $("#estado").val();

    if ((estado !== '') && ($("#descripcion_causal").val().length > 10)) {

        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            dataType: 'json',
            async: false,
            data: {
                opcion: 42,
                etapa: $("#idetapa").val(),
                estado_actual: $("#idestadoActual").val(),
                estados: $("#estado").val(),
                causal: $('#causal').val(),
                observacion: $('#descripcion_causal').val(),
                idsolicitud: $('#idsolicitud').val()
            },
            success: function (json) {
                console.log(json.respuesta);
                var resp = json.respuesta;
                if (resp === 'Guardado') {
                    toastr.success('Cambio satisfactorio', 'Enviado');
                    $("#cambioEtapa").dialog("close");
                    cargarInfoSolicitudes();
                } else {
                    mensajesDelSistema("Error", '204', '140', false);
                }
            }
        });
    } else {
        toastr.error('Por favor llene todos los campos', 'Error');
    }
}

function generarContrato() {

    var jsonContrato = [];
    var filasId = jQuery('#tabla_solicitudes_pendientes').jqGrid('getGridParam', 'selarrrow');
    if (filasId != '') {
        for (var i = 0; i < filasId.length; i++) {
            var num_contrato = jQuery("#tabla_solicitudes_pendientes").getRowData(filasId[i]).num_contrato;
            var tipo_contrato = jQuery("#tabla_solicitudes_pendientes").getRowData(filasId[i]).tipo_contrato;
            var contratos = {};
            if (num_contrato !== '') {
                contratos ["num_solicitud"] = filasId[i];
                contratos ["num_contrato"] = num_contrato;
                contratos ["tipo_contrato"] = tipo_contrato;
                jsonContrato.push(contratos);
            }

        }
        var listContratos = {};
        listContratos ["contratos"] = jsonContrato;

        var url = './controlleropav?estado=Minutas&accion=Contratacion';
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function () {
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 21,
                    listadoContratos: JSON.stringify(listContratos)
                },
                success: function (json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }

                        if (json.respuesta === "OK") {
                            refrescarGridSolicitudesPendientes();
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Se generaron los documentos base para las solicitudes seleccionadas!!", '280', '170', true);
                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.respuesta, '250', '150');
                            return;
                        }

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se pudo generar los contratos!!", '250', '165');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }, 500);

    } else {
        if (jQuery('#tabla_solicitudes_pendientes').jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las solicitudes para las que desea generar contrato!!", '250', '150');
        } else {
            mensajesDelSistema("No hay solicitudes pendientes por generar documentos", '250', '150');
        }

    }
}

function existeDocumentoContrato(tipo_contrato, num_contrato) {
    var estado = false;
    $.ajax({
        url: './controlleropav?estado=Minutas&accion=Contratacion',
        datatype: 'json',
        type: 'post',
        async: false,
        data: {
            opcion: 22,
            tipo_contrato: tipo_contrato,
            num_contrato: num_contrato
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.respuesta === "SI") {
                    estado = true;
                }

            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return estado;
}

function cargarDocumentosContrato(tipo_contrato, num_contrato) {
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {opcion: 23,
            tipo_contrato: tipo_contrato,
            num_contrato: num_contrato
        },
        async: false,
        url: './controlleropav?estado=Minutas&accion=Contratacion',
        success: function (jsonData) {
            if (!isEmptyJSON(jsonData)) {
                initializeEditor('editor1');
                $('#editor1').val(jsonData.document_info);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function AbrirDivGeneraContratoDocs(tipo_contrato, num_contrato) {
    if (num_contrato == "") {
        mensajesDelSistema('Alerta!!! Debe llenar primero la informacion asociada al contrato', '350', '165');
    } else if (!existeDocumentoContrato(tipo_contrato, num_contrato)) {
        mensajesDelSistema('Alerta!!! Debe generar la preforma del contrato', '350', '165');
    } else {

        $('#div_docs_contrato').fadeIn("slow");
        $('#tipo_doc').val(tipo_contrato);
        $('#numero_contrato').val(num_contrato);
        cargarDocumentosContrato(tipo_contrato, num_contrato);
        ventanaGenerarContrato();
    }
}

function ventanaGenerarContrato() {
    $("#div_docs_contrato").dialog({
        width: 980,
        height: 750,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'GENERACION DOCUMENTOS',
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                mensajeConfirmAction("Recuerde guardar los cambios. Esta seguro que desea salir ?", "230", "150", cerrarDialogo, "div_docs_contrato");
            }
        }
    });
}

function initializeEditor(editor) {
    var instance = CKEDITOR.instances[editor];
    if (instance)
    {
        instance.destroy(true);
    }
    var ckEditor = CKEDITOR.replace(editor, {
        height: 530,
        language: 'es',
        skin: 'office2013',
        resize_enabled: false,
        // Define the toolbar groups as it is a more accessible solution.
        toolbarGroups: [
            {name: 'document', groups: ['document']},
            {name: 'clipboard', groups: ['clipboard', 'undo']},
            {name: 'editing', groups: ['find', 'selection'/*, 'spellchecker'*/]},
            {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align']},
            {name: 'links'},
            {name: 'insert'},
            {name: 'styles'},
            {name: 'colors'},
            {name: 'tools'},
            {name: 'others'}

        ],
        // Remove the redundant buttons from toolbar groups defined above.
        removeButtons: 'NewPage,Preview,Print,Maximize,Flash,Iframe,CreateDiv,ShowBlocks',
        removePlugins: 'elementspath'
    });

    ckEditor.on("instanceReady", function () {

        // overwrite the default save function
        ckEditor.addCommand("save", {
            modes: {wysiwyg: 1, source: 1},
            exec: function () {

                // get the editor content
                var theData = ckEditor.getData();
                guardarContratoDocs(theData);
            }
        });
    });
}

function guardarContratoDocs(data) {

    var url = './controlleropav?estado=Minutas&accion=Contratacion';
    loading("Espere un momento por favor...", "270", "140");
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 24,
            tipo_contrato: $('#tipo_doc').val(),
            num_contrato: $('#numero_contrato').val(),
            content: data
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    cargarDocumentosContrato($('#tipo_doc').val(), $('#numero_contrato').val());
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Documento guardado exitosamente", '250', '150', true);
                }

            } else {
                $("#dialogLoading").dialog('close');
                mensajesDelSistema("Lo sentimos no se pudo guardar el documento!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function verContrato(id_solicitud, tipo_contrato, num_contrato) {
    if (num_contrato == "") {
        mensajesDelSistema('Alerta!!! Debe llenar primero la informacion asociada al contrato', '350', '165');
    } else if (!existeDocumentoContrato(tipo_contrato, num_contrato)) {
        mensajesDelSistema('Alerta!!! Debe generar primero el contrato', '350', '165');
    } else {

        var url = './controlleropav?estado=Minutas&accion=Contratacion';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 16,
                num_solicitud: id_solicitud
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        window.open('.' + json.Ruta);
                    }
                } else {
                    mensajesDelSistema("Lo sentimos ocurri� un error al generar contrato!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function changeBackgroundRow(cl, bgcolor) {

    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "id_solicitud", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "nombre_cliente", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "nombre_proyecto", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "creation_date", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "tipo_solicitud", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "fecha_validacion_cartera", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "Responsable", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "Interventor", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "etapa_trazabilidad", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "num_os", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "valor_cotizacion", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "nombre_estado", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "total", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "perc_anticipo", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "perc_retegarantia", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "cant_facturada", "", {'background-color': bgcolor, 'background-image': 'none'});
    jQuery("#tabla_solicitudes_pendientes").jqGrid('setCell', cl, "valor_faturado", "", {'background-color': bgcolor, 'background-image': 'none'});

}
function cerrarDialogo(Nom_div) {
    $("#" + Nom_div).dialog("destroy");
}

function cerrarventana() {
    window.close();
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function habdesAnt(swReset) {
    if (swReset) {
        $('#porc_anticipo').val(0);
        $('#valor_anticipo').val(0);
    }
    if ($('#anticipo').val() === '1') {
        $('#porc_anticipo').attr({readonly: false});
        $('#valor_anticipo').attr({readonly: false});
    } else {
        $('#porc_anticipo').attr({readonly: true});
        $('#valor_anticipo').attr({readonly: true});
    }
}

function habdesRet(swReset) {
    if (swReset) {
        $('#porc_retegarantia').val(0);
        $('#valor_retegarantia').val(0);
    }
    if ($('#retegarantia').val() === '1') {
        $('#porc_retegarantia').attr({readonly: false});
        $('#valor_retegarantia').attr({readonly: false});
    } else {
        $('#porc_retegarantia').attr({readonly: true});
        $('#valor_retegarantia').attr({readonly: true});
    }

}

function infoFormasPagoToJSON() {
    return JSON.stringify({
        anticipo: $('#anticipo').val(),
        porc_anticipo: numberSinComas($('#porc_anticipo').val()),
        valor_anticipo: numberSinComas($('#valor_anticipo').val()),
        retegarantia: $('#retegarantia').val(),
        porc_retegarantia: numberSinComas($('#porc_retegarantia').val()),
        valor_retegarantia: numberSinComas($('#valor_retegarantia').val())
    });
}

function cargarTrazabilidadOferta(id_solicitud) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 81,
            idsolicitud: id_solicitud
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#trazabilidad').val(json.respuesta);
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}




function  mostrarVentanaAccion3(id_solicitud) {


    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + "Esta seguro que desea Generar OT? Recuerde que antes necesita crear la bodega del proyecto que se encuentra a continuacion.");
    $("#dialogMsj").dialog({
        width: 250,
        height: 200,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                AgregarBodega(id_solicitud);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
    
}


function AgregarBodega(id_solicitud) {
  $('#id_solicitud').val(id_solicitud);
  $("#div_tabla_bodegas").dialog({
    title: 'Bodegas de solicitud : ' + id_solicitud,
    width: 500,
    height: 600,
    hide: "scale",
    position: "center",
    modal: true,
    closeOnEscape: false,
    resizable: false,
    show: {
      effect: "blind",
      duration: 500
    },
    buttons: {
      "Aceptar": function() {
        campos = [$('#desc_bodega').val(), $('#dir_resul').val(),
          $('#nombre_contacto').val(), $('#cargo_contacto').val(),
          $('#telefono1_contacto').val(), $('#telefono2_contacto').val(),
          $('#ciu_dir').val()
        ];
        campos = campos.map(function(x) {
          return x.toUpperCase();
        });
        if (campos.includes('')) {
          toastr.error("Faltan datos en el formulario");
          return false;
        };
        $.ajax({
          type: 'POST',
          url: '/fintra/controlleropav?estado=Modulo&accion=Bodegas',
          dataType: 'json',
          async: false,
          data: {
            opcion: 1,
            informacion: JSON.stringify({
              id_solicitud: $('#id_solicitud').val(),
              desc_bodega: campos[0],
              ciu_dir: campos[6],
              dir_resul: campos[1],
              nombre_contacto: campos[2],
              cargo_contacto: campos[3],
              telefono1_contacto: campos[4],
              telefono2_contacto: campos[5]
            })
          },
          success: function(json) {
            if (!isEmptyJSON(json)) {
              if (json.error) {
                toastr.error(json.error, "Error");
                return;
              }
              if (json.respuesta === "OK") {
                toastr.success("Bodega generada correctamente", "�xito");
                Generar_OT(id_solicitud);
              } else {
                toastr.error("Ha ocurrido un error", "Error");
              }
            }
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
              "Message: " + xhr.statusText + "\n" +
              "Response: " + xhr.responseText + "\n" + thrownError);
          }
        });
        resetAddressValues();
        $(this).dialog("close");
      },
      "Salir": function() {
        resetAddressValues();
        $(this).dialog("close");
      }
    }
  });
}

function  Generar_OT(id_solicitud) {


    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 80,
            id_solicitud: id_solicitud

        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                if (json.respuesta === "OK") {
                    mensajesDelSistema("Se Genero de forma Correcta la OT.", '250', '150', true);
                    generar_centro_costos(id_solicitud);
                    cargarSolicitudesPendientes();
                } else {
                    mensajesDelSistema("Ocurrio un problema al generar la OT.", '250', '150', true);

                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function generar_centro_costos(id_solicitud) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 98,
            id_solicitud: id_solicitud,
            id: 1
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema("Ocurrio un problema al generar los centros de costos.", '250', '150', true);
                    return;
                }
                mensajesDelSistema("Se generaron los centros de costos.", '250', '150', true);

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

//function abrir_Anticipo_Facturacion(id_solicitud) {
//    
//    $('#idsolicitud').val(id_solicitud);
//
//    $("#ventana_Anticipo_Facturacion").dialog({
//        width: '450',
//        height: '170',
//        show: "scale",
//        hide: "scale",
//        resizable: false,
//        position: "center",
//        modal: true,
//        closeOnEscape: false,
//        title: 'Anticipo Y Facturaci�n',
//        open: function (event, ui) {
//            $(".ui-dialog-titlebar-close").hide();
//        },
//        buttons: {
//            "Aceptar": function () {
//                //cambiarEstado();
//                window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/OportunidadNegocio/&pagina=Ejecucion.jsp?num_solicitud=" + id_solicitud + "&opcion_=" + $('#select_AYF').val(), '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
//            },
//            "Salir": function () {
//                $(this).dialog("close");
//            }
//        }
//    });
//    $("#ventana_Anticipo_Facturacion").css('overflow', 'hidden');

//}

valorAnticipoCon;
function abrir_Anticipo_Facturacion(id_solicitud) {
    cargarInfoAnticipo(id_solicitud);
    valorAnticipoCon =  $('#val_anticipo_').val();
    document.getElementById("val_anticipo_").disabled = true;
    cargarCasosAnticipos();
    cargarAnticipo(id_solicitud);
    $("#dialogAnticipo").dialog({
        width: '1330',
        height: '450',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        //title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarCasosAnticipos(){
    listas = new cargardosList();
    var url =  "/fintra/controlleropav?estado=Procesos&accion=Cliente";
    datos ={opcion: 99};
    listas.cargarCombo(url,datos,'caso_anticipo_');
}


function valorAticipo() {
    var seleccionado = $('#caso_anticipo_ option:selected').text();
    if (seleccionado !== 'CONTRACTUAL') {
        $('#val_anticipo_').val('');
        document.getElementById("val_anticipo_").disabled = false;
    } else {
        $('#val_anticipo_').val(valorAnticipoCon);
        document.getElementById("val_anticipo_").disabled = true;        
    }
}

function cargarInfoAnticipo(id_solicitud) {
    $.ajax({
        async: false,
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 51,
            id_solicitud: id_solicitud
        },
        success: function (json) {
            $('#codcli_').val(json[0].cod_cli);
            $('#codcot_').val(json[0].no_cotizacion);
            $('#nomcli_').val(json[0].nonmbre_cliente);
            $('#nombre_proyecto_').val(json[0].nombre_proyecto);
            if (json[0].anticipo === "1") {
                $('#perc_anticipo_').val((json[0].perc_anticipo));
                $('#val_anticipo_').val(numberConComas(json[0].valor_anticipo));
                document.getElementById("perc_anticipo_").disabled = false;
            } else {
                $('#perc_anticipo_').val(0);
                $('#valor_anticipo_').val(0);
                document.getElementById("perc_anticipo_").disabled = true;
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarAnticipo(id_solicitud) {
    var grid_tabla = jQuery("#tabla_anticipo");
    if ($("#gview_tabla_anticipo").length) {
        reloadGridAnticipo(grid_tabla, 62, id_solicitud);
    } else {
        grid_tabla.jqGrid({
            caption: "Anticipos",
            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
            datatype: "json",
            height: '150',
            width: '1283',
            colNames: ['Id', 'Id solicitud', 'Numero Anticpo','Id Tipo Anticipo','Tipo Anticipo', 'Fecha Anticipo', '# CXC', 'Valor CXC', 'F. CXC', '# CXP', 'Valor CXP', 'F. CXP', 'Valor anticpo', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 120, align: 'left', key: true, hidden: true},
                {name: 'id_solicitud', index: 'id_solicitud', width: 120, align: 'center', hidden: false},
                {name: 'cod_anticipo', index: 'cod_anticipo', width: 120, align: 'center', hidden: false},
                {name: 'id_tipo_anticipo', index: 'id_tipo_anticipo', width: 120, align: 'center', hidden: true},
                {name: 'tipo_anticipo', index: 'tipo_anticipo', width: 123, align: 'center', hidden: false},
                {name: 'fecha_anticipo', index: 'fecha_anticipo', width: 110, align: 'center', sortable: false, search: false, editable: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'num_factura', index: 'num_factura', width: 120, align: 'center', hidden: false},
                {name: 'valor', index: 'valor', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'fecha_factura', index: 'fecha_factura', width: 110, align: 'center', sortable: false, search: false, editable: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
//                {name: 'fecha_vencimiento', index: 'dia_pago', width: 110, align: 'center', sortable: false, search: false, editable: true, editrules: {date: true},
//                    editoptions: {dataInit: function (elem) {
//                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
//                        }}},
                {name: 'num_cxp', index: 'num_cxp', width: 120, align: 'center', hidden: false},
                {name: 'valorcxp', index: 'valorcxp', sortable: true, editable: false, width: 110, align: 'right', search: false, hidden: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'fecha_cxp', index: 'fecha_cxp', width: 110, align: 'center', sortable: false, search: false, editable: false, hidden: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'total', index: 'total', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'acciones', index: 'acciones', width: 100, align: 'left', hidden: true}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            pager: 'pager3',
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 62,
                    id_solicitud: id_solicitud
                }
            },
            loadComplete: function (rowid, e, iRow, iCol) {

            },
            gridComplete: function () {
                var grid_tabla_ = jQuery("#tabla_anticipo");
                var ids = jQuery("#tabla_anticipo").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    be = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: 38px; height: 19px; vertical-align: middle;"onclick = "">';
                    jQuery("#tabla_facturacion").jqGrid('setRowData', ids[i], {mandar_cliente: be});
                }
                var colSumValor = jQuery("#tabla_anticipo").jqGrid('getCol', 'valor', false, 'sum');
                var total = jQuery("#tabla_anticipo").jqGrid('getCol', 'total', false, 'sum');
                jQuery("#tabla_anticipo").jqGrid('footerData', 'set', {valor: colSumValor});
                jQuery("#tabla_anticipo").jqGrid('footerData', 'set', {total: total});
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

                jQuery("#tabla_anticipo").jqGrid('editRow', rowid, true, function () {
                }, null, null, {}, function (rowid) {

                    calculateFooterAnticipo();
                });
                return;
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid('#pager3', {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        jQuery("#tabla_anticipo").navButtonAdd('#pager3', {
            caption: "Agregar",
            title: "Agregar Anticipo",
            onClickButton: function () {
                var info = grid_tabla.getGridParam('records');
//                if (info === 0) {
                    var f = new Date();
                    var fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
                    var grid = $("#tabla_anticipo");
                    var rowid = 'neo_' + grid.getRowData().length;
                    var valor_anticipo = numberSinComas($('#val_anticipo_').val());
                    var id_tipo_anticipo = $('#caso_anticipo_').val();
                    var tipo_anticipo = $('#caso_anticipo_ option:selected').text();
                    if (valor_anticipo === '') {
                        mensajesDelSistema("Esta solicitud no tiene Anticipo registrado", '314', '140', false);
                    } else {
                    var defaultData = {valor: valor_anticipo, id_solicitud: id_solicitud, fecha_factura: fecha, fecha_vencimiento: fecha, mandado_cliente: '', total: valor_anticipo, fecha_anticipo: fecha, id_tipo_anticipo: id_tipo_anticipo, tipo_anticipo: tipo_anticipo};
                    grid.addRowData(rowid, defaultData);
                        grid.jqGrid('editRow', rowid, true, function () {
                        }, null, null, {}, function (rowid) {
                            calculateFooterAnticipo();
                        });
                    }
//                } else {
//                    mensajesDelSistema("Solo se permite agregar una linea", '314', '140', false);
//                }
            }
        });
        jQuery("#tabla_anticipo").navButtonAdd('#pager3', {
            caption: "Guardar",
            title: "Guardar Anticipos",
            onClickButton: function () {
                var respuesta = validarGuardado();
                //if (respuesta === true) {
                    guardarAnticipo(id_solicitud);
                    //generarCuantasCobro();
                    // ventanaEmpresa();
                //}
            }
        });
    }
}

function reloadGridAnticipo(grid_tabla, op, id_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: id_solicitud
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function calculateFooterAnticipo() {
    var sumValor = jQuery("#tabla_anticipo").jqGrid('getCol', 'valor', false, 'sum');
    var total = jQuery("#tabla_anticipo").jqGrid('getCol', 'total', false, 'sum');
    jQuery("#tabla_anticipo").jqGrid('footerData', 'set', {valor: sumValor});
    jQuery("#tabla_anticipo").jqGrid('footerData', 'set', {total: total});
}


function guardarAnticipo(id_solicitud) {
    var cod_cli = $('#codcli_').val();
    var nomcli = $('#nomcli_').val();
    var codcot = $('#codcot_').val();
    var perc_anticipo = $('#perc_anticipo_').val();
    var val_anticipo = $('#val_anticipo_').val();
    var grid = jQuery("#tabla_anticipo"), filas = grid.jqGrid('getDataIDs');
    var data = jQuery("#tabla_anticipo").jqGrid('getRowData');
    for (var i = 0; i < filas.length; i++) {
        console.log(filas[i]);
        grid.saveRow(filas[i]);
    }
    if (data.length === 0) {
        mensajesDelSistema('Inserte al menos una fatura', '300', 'auto', false);
    } else {
        console.log('guardo');
        $.ajax({
            async: false,
            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 63,
                informacion: JSON.stringify({json: data, cod_cli: cod_cli, nomcli: nomcli, codcot: codcot, perc_anticipo: perc_anticipo, val_anticipo: numberSinComas(val_anticipo)})
            },
            success: function (json) {
                if (json.respuesta === 'SI') {
                    mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);
                    cargarAnticipo(id_solicitud);
                    cargarInfoAnticipo(id_solicitud);
                    ventanaEmpresa();
                } else {
                    mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }

}

function ventanaEmpresa() {
    $("#dialogEmpresa").dialog({
        width: '513',
        height: '200',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        //title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
            $('.ui-dialog-titlebar').hide();
        },
        buttons: {
            "Generar pdf": function () {
                generarCuantasCobro();
                $(this).dialog("close");
            }
        }
    });
}

function generarCuantasCobro() {
    var cod_cli = $('#codcli_').val();
    var nomcli = $('#nomcli_').val();
    var codcot = $('#codcot_').val();
    var empresa = $('#empresa').val();
    var firma = $('#firma').val();
    var cuenta = $('#cuenta').val();
    var nombre_proyecto = $('#nombre_proyecto_').val();
    var perc_anticipo = $('#perc_anticipo_').val();
    var val_anticipo = $('#val_anticipo_').val();
    var grid = jQuery("#tabla_anticipo"), filas = grid.jqGrid('getDataIDs');
    var data = jQuery("#tabla_anticipo").jqGrid('getRowData');
    for (var i = 0; i < filas.length; i++) {
        console.log(filas[i]);
        grid.saveRow(filas[i]);
    }
    if (data.length === 0) {
        mensajesDelSistema('Inserte al menos una fatura', '300', 'auto', false);
    }
    $.ajax({
        type: 'POST',
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        dataType: 'json',
        data: {
            opcion: 64,
            informacion: JSON.stringify({json: data, cod_cli: cod_cli, nomcli: nomcli, codcot: codcot, perc_anticipo: perc_anticipo, val_anticipo: numberSinComas(val_anticipo), nombre_proyecto: nombre_proyecto, empresa: empresa, firma: firma, cuenta: cuenta})
        },
        success: function (data) {
            var res = data.respuesta;
            console.log(res);
            if (res === 'OK') {
                mensajesDelSistema("Exito al generar cuenta de cobro, Ver pdf en descargas", '400', '120', true);
            } else {

                mensajesDelSistema("Error al generar la cuenta de cobro", '363', '140', false);
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function guardarCondicionesComercialesImagenes() {
    var info = guardarCondicionesComerciales();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        data: {
            opcion: 91,
            informacion: JSON.stringify({info: info, id_solicitud: $('#num_solicitud').val()})
        },
        success: function (json) {
            console.log(json);
            var repuesta = json.respuesta;
            if (repuesta === 'Guardado') {
                mensajesDelSistema("Exito al guardar los cambios", '420', '150', true);
                cargarCondicionesComerciales();
            } else {
                mensajesDelSistema("ha ocuriido un error", '400', '120', true);
            }
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function cargarCondicionesComerciales() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        async: false,
        data: {
            opcion: 92,
            id_solicitud: $('#num_solicitud').val()
        },
        success: function (json) {
            console.log(json);
            if (json.length == 0) {
                antes_iva_ant = 1;
                antes_iva_ret = 1;
                variableAdministracionAnt = 1;
                administracion_ret = 1;
                imprevisto_ant = 1;
                imprevisto_rete = 1;
                utilidad_ant = 1;
                utilidad_rete = 1;
            } else {
                var imagen1Ant = new Image; //no
                var imagen2Ant = new Image; //si
                var imagen1Ret = new Image; //no
                var imagen2Ret = new Image; //si
                imagen1Ant.src = '/fintra/images/boton_ant.png';
                imagen2Ant.src = '/fintra/images/boton_ant2.png';
                imagen1Ret.src = '/fintra/images/boton_rete.png';
                imagen2Ret.src = '/fintra/images/boton_ret2.png';

                if (json[0].iva_anticipo === 'N') {
                    document.images['antes_iva_ant'].src = imagen1Ant.src;
                    antes_iva_ant = 1;
                } else if (json[0].iva_anticipo === 'S') {
                    document.images['antes_iva_ant'].src = imagen2Ant.src;
                    antes_iva_ant = 0;
                }
                if (json[0].iva_retegarantia === 'N') {
                    document.images['antes_iva_ret'].src = imagen1Ret.src;
                    antes_iva_ret = 1;
                } else if (json[0].iva_retegarantia === 'S') {
                    document.images['antes_iva_ret'].src = imagen2Ret.src;
                    antes_iva_ret = 0;
                }
                if (json[0].administracion_anticipo === 'N') {
                    document.images['administracion_ant'].src = imagen1Ant.src;
                    variableAdministracionAnt = 1;
                } else if (json[0].administracion_anticipo === 'S') {
                    document.images['administracion_ant'].src = imagen2Ant.src;
                    variableAdministracionAnt = 0;
                }
                if (json[0].administracion_retegarantia === 'N') {
                    document.images['administracion_ret'].src = imagen1Ret.src;
                    administracion_ret = 1;
                } else if (json[0].administracion_retegarantia === 'S') {
                    document.images['administracion_ret'].src = imagen2Ret.src;
                    administracion_ret = 0;
                }
                if (json[0].imprevisto_anticipo === 'N') {
                    document.images['imprevisto_ant'].src = imagen1Ant.src;
                    imprevisto_ant = 1;
                } else if (json[0].imprevisto_anticipo === 'S') {
                    document.images['imprevisto_ant'].src = imagen2Ant.src;
                    imprevisto_ant = 0;
                }
                if (json[0].imprevisto_retegarantia === 'N') {
                    document.images['imprevisto_rete'].src = imagen1Ret.src;
                    imprevisto_rete = 1;
                } else if (json[0].imprevisto_retegarantia === 'S') {
                    document.images['imprevisto_rete'].src = imagen2Ret.src;
                    imprevisto_rete = 0;
                }
                if (json[0].utilidad_anticipo === 'N') {
                    document.images['utilidad_ant'].src = imagen1Ant.src;
                    utilidad_ant = 1;
                } else if (json[0].utilidad_anticipo === 'S') {
                    document.images['utilidad_ant'].src = imagen2Ant.src;
                    utilidad_ant = 0;
                }
                if (json[0].utilidad_retegarantia === 'N') {
                    document.images['utilidad_rete'].src = imagen1Ret.src;
                    utilidad_rete = 1;
                } else if (json[0].utilidad_retegarantia === 'S') {
                    document.images['utilidad_rete'].src = imagen2Ret.src;
                    utilidad_rete = 0;
                }
            }


        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function abrir_Facturacion(id_solicitud) {
    $('#id_solicitudSelec').val(id_solicitud);
    cargarFacturacion(id_solicitud);
    cargarCotizacion(id_solicitud);
    cargarAccionPreparada(id_solicitud);
    $("#dialogMsjFacturacion").dialog({
        width: '1210',
        height: '715',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        //title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarCotizacion(id_solicitud) {
    $.ajax({
        async: false,
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 51,
            id_solicitud: id_solicitud
        },
        success: function (json) {
            document.getElementById('lb_facturacion').innerHTML = ('FACTURACION PROYECTO ' + json[0].nombre_proyecto + ' (' + json[0].num_os + ')');
            $('#val_total').val(numberConComas(json[0].total));
            if (json[0].anticipo === "1") {
                console.log('entro');
                $('#val_anticipo').val(numberConComas(json[0].valor_anticipo));
            } else {
                $('#val_anticipo').val('0');
            }

            if (json[0].retegarantia === "1") {
                var porcretegarantia = json[0].perc_retegarantia;
                var valor_retegarantia = (json[0].total * porcretegarantia) / 100;
                $('#val_retegarantia').val(numberConComas(valor_retegarantia));
            } else {
                $('#val_retegarantia').val('0');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarFacturacion(id_solicitud) {
    var grid_tabla = jQuery("#tabla_facturacion");
    if ($("#gview_tabla_facturacion").length) {
        reloadGridFactuacion(grid_tabla, 50, id_solicitud);
    } else {
        grid_tabla.jqGrid({
            caption: "Facturas Creadas",
            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
            datatype: "json",
            height: '250',
            width: '1057',
            colNames: ['Id solicitud', 'Foms Padre', 'Foms Corte', '# Factura', 'F. Facturacion', 'F. Vencimiento', 'Descripcion', 'Valor', 'Valor pago', 'Valor saldo'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', width: 120, align: 'left', hidden: false},
                {name: 'foms_padre', index: 'foms_padre', width: 120, align: 'left', hidden: true},
                {name: 'foms_corte', index: 'foms_corte', width: 120, align: 'left', hidden: true},
                {name: 'num_factura', index: 'num_factura', width: 120, align: 'left', hidden: false},
                {name: 'fecha_facturacion', index: 'fecha_facturacion', width: 110, align: 'center', sortable: false, search: false, editable: true, hidden: false, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'fecha_vencimiento', index: 'dia_pago', width: 110, align: 'center', sortable: false, search: false, editable: true, editrules: {date: true},
                    editoptions: {dataInit: function (elem) {
                            $(elem).datepicker({dateFormat: "yy-mm-dd"});
                        }}},
                {name: 'descripcion', index: 'descripcion', width: 220, align: 'left', hidden: false},
                {name: 'valor', index: 'valor', sortable: true, editable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'valor_pago', index: 'valor_pago', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                },
                {name: 'valor_saldo', index: 'valor_saldo', sortable: true, editable: false, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "},
                    editoptions: {dataInit: function (elem) {
                            $(elem).keypress(function (e) {
                                if (e.which === 8 && e.which === 0 && (e.which === 48 || e.which === 46)) {
                                    return true;
                                }
                                if (e.which >= 58) {
                                    return false;
                                }
                            });
                        }
                    }
                }
            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            pager: 'pager2',
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: true,
            //hiddengrid: true,
            setGridState: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            subgrid: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 50,
                    id_solicitud: id_solicitud
                }
            },
            loadComplete: function (rowid, e, iRow, iCol) {
                $("#tabla_facturacion").contextMenu('menu2', {
                    bindings: {
                        'eliminar': function (rowid) {
                            var myGrid = jQuery("#tabla_facturacion"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                            var mandado_cliente = $("#tabla_facturacion").getRowData(selRowIds).mandado_cliente;
                            if (mandado_cliente === '') {
                                $('#tabla_facturacion').jqGrid('delRowData', selRowIds);
                                $('#eliminar').removeAttr("disabled").removeClass('ui-state-disabled');
                            }
                        }
                    }, onContexMenu: function (event/*, menu*/) {

                    }
                });
            },
            gridComplete: function () {
                var grid_tabla_ = jQuery("#tabla_facturacion");
                var ids = jQuery("#tabla_facturacion").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
//                    var mandado_cliente = grid_tabla_.getRowData(cl).mandado_cliente;
                    var valor = grid_tabla_.getRowData(cl).valor;
                    var valor_pago = grid_tabla_.getRowData(cl).valor_pago;
                    var valor_saldo = valor - valor_pago;
                    jQuery("#tabla_facturacion").jqGrid('setCell', ids[i], 'valor_saldo', valor_saldo);
                }
                var colSumValor = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum');
                // var colSumAmort = jQuery("#tabla_facturacion").jqGrid('getCol', 'amortizacion', false, 'sum');
                var colSumValorPagado = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor_pago', false, 'sum');
                var colSumValorSaldo = jQuery("#tabla_facturacion").jqGrid('getCol', 'valor_saldo', false, 'sum');
                //var colSumRet = jQuery("#tabla_facturacion").jqGrid('getCol', 'retegarantia', false, 'sum');
                // var total = jQuery("#tabla_facturacion").jqGrid('getCol', 'total', false, 'sum');
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor: colSumValor});
                // jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {amortizacion: colSumAmort});
                //jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {retegarantia: colSumRet});
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor_pago: colSumValorPagado});
                jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {valor_saldo: colSumValorSaldo});
                // jQuery("#tabla_facturacion").jqGrid('footerData', 'set', {total: total});
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid('#pager2', {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridFactuacion(grid_tabla, op, id_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: id_solicitud
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function preguntaMandarCliente(id) {
    mensajes("�Desea enviar al cliente?", '400', '150', true, id);
}

function mensajes(msj, width, height, swHideDialog, id) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                EnviarFacturaCliente(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function preguntaEliminarFactura(id) {
    mensajeELiminacion("�Desea eliminar la factura?", '400', '150', true, id);
}

function mensajeELiminacion(msj, width, height, swHideDialog, id) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                eliminarFacturaCliente(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function guardarFacturacion(id_solicitud) {
    var data_inicial = funcionesformulario.informacion('dialogMsjPrepararFacturacion');
    var data = [];
    var llave = {};
    for (var i in data_inicial) {
        var val = data_inicial[i];
        for (var j in val) {
            var sub_key = j;
            var sub_val = funciones.numberSinComas(val[j]);
            llave[sub_key] = sub_val;
        }
    }
    data.push(llave);
    console.log(data);

    // console.log(JSON.stringify(data));
    $.ajax({
        async: false,
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 52,
            informacion: JSON.stringify({json: data})
        },
        success: function (json) {
            console.log(json.respuesta);
            if (json.respuesta === 'OK') {
                mensajesDelSistema('Exito al preparar la facturacion', '300', 'auto', false);
                cargarCotizacion(id_solicitud);
                cargarFacturacion(id_solicitud);
                cargarAccionPreparada(id_solicitud);
            } else {
                mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
//}

}

function EnviarFacturaCliente(id) {
    var num_factura = $("#tabla_facturacion").getRowData(id).num_factura;
    var valor = $("#tabla_facturacion").getRowData(id).valor;
    var amortizacion = $("#tabla_facturacion").getRowData(id).amortizacion;
    var retegarantia = $("#tabla_facturacion").getRowData(id).retegarantia;
    var id_solicitud = $("#tabla_facturacion").getRowData(id).id_solicitud;
    $.ajax({
        async: false,
        url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 53,
            id: id,
            num_factura: num_factura,
            valor: valor,
            amortizacion: amortizacion,
            retegarantia: retegarantia
        },
        success: function (json) {
            if (json.respuesta === 'OK') {
                cargarCotizacion(id_solicitud);
                cargarFacturacion(id_solicitud);
                mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);
            } else {
                mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function abrirPrepararFacturacion(id_solicitud) {
    var valorPreparado = 0;
    var ids = jQuery("#tabla_accionPreparada").jqGrid('getDataIDs');
    var allRowsInGrid = jQuery("#tabla_accionPreparada").jqGrid('getRowData');
    for (var i = 0; i < allRowsInGrid.length; i++) {
        var cl = ids[i];
        var liquidado = jQuery("#tabla_accionPreparada").getRowData(cl).liquidado;
        // if (liquidado == 'NO') {
        var valor_facturar = parseFloat(jQuery("#tabla_accionPreparada").getRowData(cl).valor_facturar);
        valorPreparado = parseFloat(valorPreparado) + valor_facturar;
        // }
    }
    // var colSumValor = parseFloat(jQuery("#tabla_facturacion").jqGrid('getCol', 'valor', false, 'sum'));
    var precioVenta = parseFloat(funciones.numberSinComas($('#val_total').val()));
    //var valorXfacturar = precioVenta - (colSumValor + valorPreparado);
    var valorXfacturar = precioVenta - (valorPreparado);
    $('#porFacturar').val(funciones.numberConComas(valorXfacturar.toFixed(2)));
    $('#aFacturar').val(funciones.numberConComas(valorXfacturar.toFixed(2)));
    var diferencia = (funciones.numberSinComas($('#porFacturar').val()) - funciones.numberSinComas($('#aFacturar').val())).toFixed(2);
    $('#valorPendiente').val(funciones.numberConComas(diferencia));
    $('#valorMaterial').val('0,00');
    $('#_id_solicitud').val(id_solicitud);

    $("#dialogMsjPrepararFacturacion").dialog({
        width: '653',
        height: '460',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Preparar Facturacion',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Aceptar": function () {
                mensajeConfirmacion("Desea preparar la facturacion por: $" + $('#aFacturar').val(), '350', '150', true, id_solicitud);
            },
            "Salir": function () {
                $(this).dialog("close");
                funcionesformulario.limpiarCampos('dialogMsjPrepararFacturacion');
            }
        }
    });
}

function calcularValorxFatura() {
    var porFacturar = funciones.numberSinComas($('#porFacturar').val());
    var aFacturar = funciones.numberSinComas($('#aFacturar').val());
    var diferencia = (porFacturar - aFacturar).toFixed(2);
    if (diferencia < 0) {
        mensajesDelSistema("El valor a facturar no puede ser mayor ", '350', '150', false);
    } else {
        $('#valorPendiente').val(funciones.numberConComas(diferencia));
    }
    // console.log(diferencia);
}

function ValidarValorMaterial() {
    var valorMaterial = parseFloat(funciones.numberSinComas($('#valorMaterial').val()));
    var aFacturar = parseFloat(funciones.numberSinComas($('#aFacturar').val()));
    if (aFacturar < valorMaterial) {
        mensajesDelSistema("El valor material no puede ser mayor a valor a facturar ", '350', '150', false);
        $('#valorMaterial').val('0.00');
        console.log('valorMaterial: ' + valorMaterial + ' aFacturar: ' + aFacturar);
    }
}

function mensajeConfirmacion(msj, width, height, swHideDialog, id_solicitud) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                guardarFacturacion(id_solicitud);
                $(this).dialog("destroy");
                $("#dialogMsjPrepararFacturacion").dialog("destroy");
                funcionesformulario.limpiarCampos('dialogMsjPrepararFacturacion');
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}


function cargarAccionPreparada(id_solicitud) {
    var grid_tabla = jQuery("#tabla_accionPreparada");
    if ($("#gview_tabla_accionPreparada").length) {
        reloadGridAccionPreparada(grid_tabla, 93, id_solicitud);
    } else {
        grid_tabla.jqGrid({
            caption: "Acciones Preparada",
            url: "/fintra/controlleropav?estado=Procesos&accion=Cliente",
            datatype: "json",
            height: '250',
            width: '1157',
            colNames: ['Id Accion', 'Id Solicitud', 'Estado', 'Estado', 'Contratista', 'Material', 'Mano de Obra', 'Valor Facturar', 'Descripcion', 'Tipo de Trabajo', 'Liquidado'],
            colModel: [
                {name: 'id_accion', index: 'id_accion', width: 120, align: 'center', hidden: false},
                {name: 'id_solicitud', index: 'id_solicitud', width: 120, align: 'center', hidden: true},
                {name: 'estado', index: 'estado', width: 50, align: 'left', hidden: true},
                {name: 'estado_accion', index: 'estado_accion', width: 260, align: 'left', hidden: false},
                {name: 'contratista', index: 'contratista', width: 90, align: 'center', hidden: false},
                {name: 'material', index: 'material', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'mano_obra', index: 'mano_obra', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_facturar', index: 'valor_facturar', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', hidden: true, formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'descripcion', index: 'descripcion', width: 240, align: 'left', hidden: false},
                {name: 'tipo_trabajo', index: 'tipo_trabajo', width: 100, align: 'left', hidden: false},
                {name: 'liquidado', index: 'liquidado', width: 100, align: 'left', hidden: false}
            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            pager: 'pager4',
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: true,
            setGridState: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 93,
                    id_solicitud: id_solicitud
                }
            },
            loadComplete: function (rowid, e, iRow, iCol) {

            },
            gridComplete: function () {
                var material = jQuery("#tabla_accionPreparada").jqGrid('getCol', 'material', false, 'sum');
                var mano_obra = jQuery("#tabla_accionPreparada").jqGrid('getCol', 'mano_obra', false, 'sum');
                var valor_facturar = jQuery("#tabla_accionPreparada").jqGrid('getCol', 'valor_facturar', false, 'sum');
                jQuery("#tabla_accionPreparada").jqGrid('footerData', 'set', {material: material, mano_obra: mano_obra, valor_facturar: valor_facturar});
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid('#pager4', {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridAccionPreparada(grid_tabla, op, id_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                id_solicitud: id_solicitud
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}



//ANTICIPO CONTRACTUAL
//ANTICIPO CORTE DE OBRA
//ANTICIPO OTROS SIES

