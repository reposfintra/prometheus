/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $("#fecha_inicio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    $("#fecha_fin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    var myDate = new Date();
    $("#fecha_inicio").datepicker("setDate", myDate);
    $("#fecha_fin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

    $("#buscar").click(function () {   
        $("#rowsdelegate").html("");
        cargarExtractoPropietario();
        $("#rowsdelegate tr:even").addClass("even");
        $("#rowsdelegate tr:odd").addClass("odd");
        $("#tbl_report_owner").css({'display':'inline-table'});
    });
    
    $("#divSalidaEx").dialog({
        autoOpen: false,
        height: "auto",
        width: "300px",
        modal: true,
        autoResize: true,
        resizable: false,
        position: "center",
        closeOnEscape: false
    });
    
    $("#exportar").click(function () {   
      exportarExcelPropietario();
    });

    $("#rowsdelegate").delegate(".header", "click", function () {

        $(this).find('span').text(function (_, value) {
            var padre = $(this).parent();
            if (value === '+') {
                padre.parent().css({'color': 'rgba(235, 242, 226, 0.6)'});
              // ajustarTabla();
             } else {
                padre.parent().css({'color': 'black'}); 
             //  ajustarTabla();
             }

            return value === '-' ? '+' : '-';
        });


        $(this).nextUntil('tr.header').css('display', function (i, v) {
            return this.style.display === 'table-row' ? 'none' : 'table-row';
           
        });

    });
    
    $("#propietario").change(function () {
        var op = $(this).find("option:selected").val();        
        loadPlaca(op);
    });
		 
    $(function(){
             $("#rowsdelegate tr").hover(
              function()
              {
                   $(this).addClass("highlight");
              },
              function()
              {
                   $(this).removeClass("highlight");
              }
             );
      }
     );
    
    loadOwner();

});

function cargarExtractoPropietario() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        async: false,
        data: {
            opcion: 7,
            propietario: $("#propietario").val(),
            placa: $("#placa").val(),
            planilla: $("#planilla").val(),
            fecha_inicio: $("#fecha_inicio").val(),
            fecha_fin: $("#fecha_fin").val()
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                try {

                    //var valores = JSON.parse(json);

                    for (var lbl in json) {
                        console.log(lbl + '  --> ' + json[lbl].planilla);
                        var tr = "<tr class='header'  style='display: table-row; background-color:rgba(167, 228, 245, 0.2)' id='" + json[lbl].planilla + "'  >" +
                                "<td class='ui-state-default widhRezisable'><span>+</span></td>" +
                                "<td class='colPlanilla'>" + json[lbl].planilla + "</td>" +
                                "<td class='colPlaca'>" + json[lbl].placa + "</td>" +
                                "<td class='colNombreConductor'>" + json[lbl].nombre_conductor + "</td>" +
                                "<td class='colccConductor' >" + json[lbl].cedula_conductor + "</td>" +
                                "<td class='colVaorAnticipo'> " + numberConComas(json[lbl].valor_anticipo.replace(".00", "")) + "</td>" +
                                "<td class='colDescuento'> " + numberConComas(json[lbl].descuento.replace(".00", "")) + "</td>" +
                                "<td class='colValorPlanilla'> " + numberConComas(json[lbl].valor_planilla.replace(".00", "")) + "</td>" +
                                "<td class='colConsecutivoVenta'></td>" +
                                "<td class='colFechaVenta'></td>" +
                                "<td class='colHora'></td>" +
                                "<td class='colBandera'></td>" +
                                "<td class='colNombreEds'></td>" +
                                "<td class='colKilometraje'></td>" +
                                "<td class='colProducto'></td>" +
                                "<td class='colPrecioUnitario'></td>" +
                                "<td class='colUnidades'></td>" +
                                "<td class='colVelorVenta'>" + numberConComas(json[lbl].total_venta) + "</td>" +
                                "<td class='colDisponible'></td>" +
                                "</tr>";

                        //cargamos el detalle 
                        var contador = 0;
                        for (var key in json[lbl].detalle) {

                            tr += "<tr style='display:none;' >" ;
                            if (contador === 0) {
                                tr +=  "<td class='widhRezisable'rowspan='" + json[lbl].rowspan + "' ></td>"+
                                        "<td class='colPlanilla' rowspan='" + json[lbl].rowspan + "'>" + json[lbl].detalle[key].planilla + "</td>" +
                                        "<td class='colPlaca' rowspan='" + json[lbl].rowspan + "'>" + json[lbl].detalle[key].placa + "</td>" +
                                        "<td class='colNombreConductor' rowspan='" + json[lbl].rowspan + "'>" + json[lbl].detalle[key].nombre_conductor + "</td>" +
                                        "<td class='colccConductor' rowspan='" + json[lbl].rowspan + "'>" + json[lbl].detalle[key].cedula_conductor + "</td>" +
                                        "<td class='colVaorAnticipo' rowspan='" + json[lbl].rowspan + "'>" + numberConComas(json[lbl].detalle[key].valor_anticipo.replace(".00", "")) + "</td>" +
                                        "<td class='colDescuento' rowspan='" + json[lbl].rowspan + "'>" + numberConComas(json[lbl].detalle[key].descuento.replace(".00", "")) + "</td>" +
                                        "<td class='colValorPlanilla' rowspan='" + json[lbl].rowspan + "'>" + numberConComas(json[lbl].detalle[key].valor_planilla.replace(".00", "")) + "</td>";
                            }
                            tr += "<td class='colConsecutivoVenta'>" + json[lbl].detalle[key].num_venta + "</td>" +
                                    "<td class='colFechaVenta'>" + json[lbl].detalle[key].fecha_venta + "</td>" +
                                    "<td class='colHora'>" + json[lbl].detalle[key].hora + "</td>" +
                                    "<td class='colBandera'>" + json[lbl].detalle[key].bandera.toLowerCase() + "</td>" +
                                    "<td class='colNombreEds' >" + json[lbl].detalle[key].nombre_eds.toLowerCase()  + "</td>" +
                                    "<td class='colKilometraje'>" + numberConComas(json[lbl].detalle[key].kilometraje) + "</td>" +
                                    "<td class='colProducto'>" + json[lbl].detalle[key].nombre_producto.toLowerCase() + "</td>" +
                                    "<td class='colPrecioUnitario'>" + numberConComas(json[lbl].detalle[key].precio_xunidad.replace(".00", "")) + "</td>" +
                                    "<td class='colUnidades'>" + numberConComas(json[lbl].detalle[key].unidades.replace(".00", "")) + "</td>" +
                                    "<td class='colVelorVenta'>" + numberConComas(json[lbl].detalle[key].valor_venta.replace(".00", "")) + "</td>" +
                                    "<td class='colDisponible'>" + numberConComas(json[lbl].detalle[key].disponible.replace(".00", "")) + "</td>" +
                                    "</tr>";
                            contador++;
                        }

                        tr += "<tr style='display: none;  background-color:rgba(254, 249, 203, 0.8);'  >" +
                                "<td class='widhRezisable'></td>" +
                                "<td class='colPlanilla'>" + json[lbl].planilla + "</td>" +
                                "<td class='colPlaca' >" + json[lbl].placa + "</td>" +
                                "<td class='colNombreConductor' >" + json[lbl].nombre_conductor + "</td>" +
                                "<td class='colccConductor'>" + json[lbl].cedula_conductor + "</td>" +
                                "<td class='colVaorAnticipo'> " + numberConComas(json[lbl].valor_anticipo.replace(".00", "")) + "</td>" +
                                "<td class='colDescuento'> " + numberConComas(json[lbl].descuento.replace(".00", "")) + "</td>" +
                                "<td class='colValorPlanilla'> " + numberConComas(json[lbl].valor_planilla.replace(".00", "")) + "</td>" +
                                "<td class='colConsecutivoVenta'></td>" +
                                "<td class='colFechaVenta'></td>" +
                                "<td class='colHora'></td>" +
                                "<td class='colBandera'></td>" +
                                "<td class='colNombreEds' ></td>" +
                                "<td class='colKilometraje'></td>" +
                                "<td class='colProducto'></td>" +
                                "<td class='colPrecioUnitario'></td>" +
                                "<td class='colUnidades'></td>" +
                                "<td class='colVelorVenta'>" + numberConComas(json[lbl].total_venta) + "</td>" +
                                "<td class='colDisponible'></td>" +
                                "</tr>";


                        $("#rowsdelegate").append(tr);

                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}


//function cargarExtracto() {
//    var grid_extracto_ventas = jQuery("#tbl_extracto_propietario");
//
//    if ($("#gview_tbl_extracto_propietario").length) {
//        reloadGridExtractoPropietario(grid_extracto_ventas);
//    } else {
//
//        grid_extracto_ventas.jqGrid({
//            caption: "Movimientos Eds",
//            url: "./controller?estado=Administracion&accion=Logistica",
//            mtype: "POST",
//            datatype: "json",
//            height: '600',
//            width: 'auto',
//            colNames: ['id','Consecutivo Venta', 'Planilla', 'Placa', 'Nombre conductor', 'Cedula', 'Valor anticipo',
//                'Descuento', 'Valor planilla', 'Fecha Venta', 'Hora', 'Bandera', 'Nombre eds', 'Kilometraje', 'Producto',
//                'Precio unitario', 'Unidades', 'Valor venta', 'Disponible'],
//            colModel: [
//                {name: 'id', index: 'id', width: 120, align: 'center',hidden: true, key: true },
//                {name: 'num_venta', index: 'num_venta', width: 120, align: 'center'},
//                {name: 'planilla', index: 'planilla', sortable: true, width: 100, align: 'center'},
//                {name: 'placa', index: 'placa', sortable: true, width: 80, align: 'center'},
//                {name: 'nombre_conductor', index: 'nombre_conductor', sortable: false, width: 140, align: 'center'},
//                {name: 'cedula_conductor', index: 'cedula_conductor', width: 100, align: 'center'},
//                {name: 'valor_anticipo', index: 'valor_anticipo', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
//                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
//                {name: 'descuento', index: 'descuento', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
//                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
//                {name: 'valor_planilla', index: 'valor_planilla', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
//                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
//                {name: 'fecha_venta', index: 'fecha_venta', width: 105, align: 'center'},
//                {name: 'hora', index: 'hora', width: 80, align: 'center'},
//                {name: 'bandera', index: 'bandera', width: 130, align: 'center'},
//                {name: 'nombre_eds', index: 'nombre_eds', width: 120, align: 'center'},
//                {name: 'kilometraje', index: 'kilometraje', width: 120, align: 'center'},
//                {name: 'nombre_producto', index: 'nombre_producto', width: 120, align: 'center'},
//                {name: 'precio_xunidad', index: 'precio_xunidad', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
//                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
//                {name: 'unidades', index: 'unidades', sortable: false, width: 100, align: 'right', search: false, sorttype: 'currency',
//                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0}},
//                {name: 'valor_venta', index: 'valor_venta', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
//                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}
//                 , summaryType: sumTotalItems},
//                {name: 'disponible', index: 'disponible', sortable: true, width: 100, align: 'right', search: false, sorttype: 'currency',
//                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
//            ],
//            rowNum: 10000,
//            rowTotal: 10000000,
//            pager: ('#page_extracto_propietario'),
//            loadonce: true,
//            rownumWidth: 30,
//            gridview: true,
//            viewrecords: true,
//            hidegrid: false,
//            shrinkToFit: false,
//            footerrow: true,
//            rowTotals: true,
//            colTotals: true,
//            grouping: true,
//            rownumbers: true,
//            jsonReader: {
//                root: "rows",
//                repeatitems: false,
//                id: "0"
//            }, ajaxGridOptions: {
//                async: false,
//                data: {
//                    opcion: 7,
//                    propietario: $("#propietario").val(),
//                    placa: $("#placa").val(),
//                    planilla: $("#planilla").val(),
//                    fecha_inicio: $("#fecha_inicio").val(),
//                    fecha_fin: $("#fecha_fin").val()
//                }
//            },
//            groupingView: {
//                groupField: ['planilla'],
//                groupSummary: [true],
//                groupColumnShow: [true],
//                groupText: ['<b> # planilla: {0},  Total venta: {valor_venta}</b>'],
//                groupCollapse: false,            
//                groupDataSorted: false
//
//            },
//            loadComplete: function () {
//                if (grid_extracto_ventas.jqGrid('getGridParam', 'records') <= 0) {
//                     mensajesDelSistema("No se encontraron resultados para los parametros seleccionados.", "270", "170");
//                } 
//            },
//            loadError: function (xhr, status, error) {
//                mensajesDelSistema(error, 250, 200);
//            }
//        }).navGrid("#page_extracto_propietario", {add: false, edit: false, del: false, search: false, refresh: false}, {});
//
//          
//        $("#tbl_extracto_propietario").navButtonAdd('#page_extracto_propietario', {
//            caption: "Exportar Excel",
//            onClickButton: function (e) {
//                exportData(19, "xls");
//            }
//        });
//    }
//}

function sumTotalItems(val, name, record) {  
        var valor = parseFloat(val || 0) + parseFloat((record[name] || 0));
        return valor;  
}

function reloadGridExtractoPropietario(grid_extracto_ventas) {
  
    grid_extracto_ventas.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Logistica",
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: 7,
                propietario: $("#propietario").val(),
                placa: $("#placa").val(),
                planilla: $("#planilla").val(),
                fecha_inicio: $("#fecha_inicio").val(),
                fecha_fin: $("#fecha_fin").val()
            }
        }
    });
    grid_extracto_ventas.jqGrid('groupingGroupBy', ['planilla'], {
       groupSummary: [true],
        groupColumnShow: [true],
        groupText: ['<b> # planilla: {0},  Total venta: {valor_venta}</b>'],
        groupCollapse: false,    
        groupDataSorted: false
    });

    grid_extracto_ventas.trigger("reloadGrid");
}

function exportData(cols, file) {
    var columns = cols.toString().split(",");
    var columnNms = $("#tbl_extracto_propietario").jqGrid('getGridParam', 'colNames');
    var theads = "";
    for (var cc = 0; cc < columnNms.length; cc++) {
        var isAdd = true;
        for (var p = 0; p < columns.length; p++) {
            if (cc === columns[p]) {
                isAdd = false;
                break;
            }
        }
        if (isAdd) {
            theads = theads + "<th>" + columnNms[cc] + "</th>";
        }
    }
    theads = "<tr>" + theads + "</tr>";
    var mya = new Array();
    mya = jQuery("#tbl_extracto_propietario").getDataIDs();  // Get All IDs
    var data = jQuery("#tbl_extracto_propietario").getRowData(mya[0]);     // Get First row to get the labels
    //alert(data['id']);
    var colNames = new Array();
    var ii = 0;
    for (var i in data) {
        colNames[ii++] = i;
    }

    var pageHead = "Extracto Propietario";
    var html = "<html>\n\
                <head>\n\
                <style script='css/text'>\n\
                table.tableList_1 th{\n\
                                    text-align:center; \n\
                                    vertical-align: middle; \n\
                                    padding:1px; \n\
                                    background-color:blue;\n\
                }\n\
                \n\
                table.tableList_1 td {\n\
                                    text-align: left; \n\
                                    vertical-align: top; \n\
                                    padding:1px;\n\
                }\n\
                </style>\n\
                </head>\n\
                <body>\n\
                <div class='pageHead_1'>" + pageHead + "</div>\n\
                <table border='" + (file == 'pdf' ? '0' : '0') + "' class='tableList_1 t_space' cellspacing='3' cellpadding='0'>" + theads;
 //   alert(theads);
    for (i = 0; i < mya.length; i++)
    {
        html = html + "<tr>";
        data = jQuery("#tbl_extracto_propietario").getRowData(mya[i]); // get each row
        for (j = 0; j < colNames.length; j++) {
            var isjAdd = true;
            for (var pj = 0; pj < columns.length; pj++) {
                if (j === columns[pj]) {
                    isjAdd = false;
                    break;
                }
            }
            if (isjAdd) {
                html = html + "<td>" + data[colNames[j].replace(/(<br>)*/g, '')] + "</td>"; // output each column as tab delimited
            }
        }
        html = html + "</tr>";  // output each row with end of line

    }
    
    html = html + "</table></body></html>";  // end of line at the end
    
   // alert(html+"saas");
    document.form_expr.pdfBuffer.value = html;
    document.form_expr.fileType.value = file;
    document.form_expr.method = 'POST';
    document.form_expr.action = './controller?estado=Administracion&accion=Logistica&opcion=4';//./controller?estado=Reestructuracion&accion=Negocios&opcion=14';  // send it to server which will open this contents in excel file
    document.form_expr.submit();
}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}


function loadOwner() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        data: {
            opcion: 8
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#propietario').append('<option value='+json[key].id+'>' + json[key].nombre + '</option>');
                    }
                    if(json.length===1){
                        loadPlaca(json[0].id);           
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function exportarExcelPropietario(){
        $("#divSalidaEx").dialog("open");
        $("#imgloadEx").show();
        $("#msjEx").show();
        $("#respEx").hide();
        $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'html',
        data: {
            opcion: 10,
            propietario: $("#propietario").val(),
            placa: $("#placa").val(),
            planilla: $("#planilla").val(),
            fecha_inicio: $("#fecha_inicio").val(),
            fecha_fin: $("#fecha_fin").val()
        },
        success: function (resp) {
            //alert(resp);
                $("#imgloadEx").hide();
                $("#msjEx").hide();
                $("#respEx").show();
               // var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>"
                document.getElementById('respEx').innerHTML = resp ;

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    
    
}

function loadPlaca(id_propietario) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        data: {
            opcion: 9,
            id_propietario:id_propietario
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#placa').empty();
                    for (var key in json) {
                        $('#placa').append('<option value='+json[key].placa+'>' + json[key].placa + '</option>');
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}


//function ajustarTabla(){
//    
//    // Change the selector if needed
//var $table = $('table.scroll'),
//    $bodyCells = $table.find('tbody tr:first').children(),
//    colWidth;
//
//// Adjust the width of thead cells when window resizes
//$(window).resize(function() {
//    // Get the tbody columns width array
//    colWidth = $bodyCells.map(function() {
//        return $(this).width();
//    }).get();
//    
//    // Set the width of thead columns
//    $table.find('table.scroll2 thead tr').children().each(function(i, v) {
//        $(v).width(colWidth[i]);
//    });    
//}).resize(); // Trigger resize handler
//    
//}
