// JavaScript Document
/****************************
 * autor :Diogenes Bastidas
 * parametros:  direccion
 * proposito: llamar a un action para cargal los selects|
 */
function cargarSelects ( dir ){
	form1.action = dir;
	form1.submit();	
}
/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller
 * proposito: buscar los clientes
 */ 
function buscarCliente(CONTROLLER){
	if(form1.cliente.value.length<6){
		var tamano = 6-form1.cliente.value.length;
		var ceros='';
		i =1;
		//while(i<=tamano){
			//ceros = ceros + '0'; //090707
			///i++;  //090707
		//}
       form1.cliente.value = ceros +form1.cliente.value;
	}
    form1.action = CONTROLLER+'?estado=Ingreso&accion=Cargar&carpeta=jsp/cxcobrar/ingreso&pagina=ingreso.jsp&evento=cliente&cliente='+form1.cliente.value+'&moneda='+form1.moneda.value+'&concepto='+form1.concepto.value+'&valor='+sinformato(form1.valor.value)+'&descripcion='+form1.descripcion.value;
    form1.submit();
} 

/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller
 * proposito: validar los ingresos cabecera
 */ 
function validarIngreso(BASEURL){
   if(form1.tipodoc.value == 'ING'){
   	   var campos = new Array("cliente","nombre","banco","sucursal","fecha","concepto","valor","moneda" );
       var ncampos = new Array("cliente","nombre cliente","banco","sucursal","fecha","concepto","valor","moneda" );
   }else{
       var campos = new Array("cliente","nombre","concepto","valor","moneda" );
       var ncampos = new Array("cliente","nombre cliente","concepto","valor","moneda" );
   }
   var campo = "";
   var ncampo = "";
   for (i = 0; i < campos.length; i++){
    	campo = campos[i];
        ncampo = ncampos[i];
		alert(form1.elements[campo].value);
    	if (form1.elements[campo].value == ''){
	    	alert("El campo "+ncampos[i]+" esta vacio.!");				
            form1.elements[campo].focus();
            return (false);
            break;
        }
   }
   /*if( form1.valor.value <= 0){
		alert("El ingreso debe ser mayor que cero");
 		return (false);
   }*/
   form1.valor.value = sinformato(form1.valor.value);
   document.form1.imgaceptar.src = BASEURL+"/images/botones/aceptarDisable.gif";
   document.imgaceptar.disabled = true;
   form1.submit();
}

 var isIE = document.all?true:false;
  var isNS = document.layers?true:false;
function Digitos(e,decReq) {
      var key = (isIE) ? window.event.keyCode : e.which;
      var obj = (isIE) ? event.srcElement : e.target;
      var isNum =  (key > 47 && key < 58 )? true:false;
      var dotOK =  (decReq=='decOK' && key ==46 && obj.value.indexOf('.')==-1) ? true:false;
	 
	  if (key == 45 ){
		 obj.value =  (obj.value.charAt(0) == '-')? obj.value = obj.value.replace ('-',''):  '';
		 if (obj.value=='')
		 	return true;
	  }
      window.event.keyCode = (!isNum && !dotOK && isIE) ? 0:key;
      e.which = (!isNum && !dotOK && isNS) ? 0:key;
      return (isNum || dotOK );
}

function Buscar(e,decReq,CONTROLLER){
  if (form1.cliente.value!=''){ 
 	buscarCliente(CONTROLLER);	
  }
  else
	form1.nombre.value='';

  soloDigitos(e,decReq);
 }

/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller
 * proposito: buscar identificacion
 */ 
function BuscarId(e,decReq,CONTROLLER){
  if (window.event.keyCode==13 && form1.identificacion.value!=''){ 
 	buscarIdentificacion(CONTROLLER);	
  }
  soloDigitos(e,decReq);
 }

/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller
 * proposito: buscar identificacion
 */ 
function BuscarIden(CONTROLLER){
  if (form1.identificacion.value!=''){ 
 	buscarIdentificacion(CONTROLLER);	
  }
}
/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller
 * proposito: el formulario de busaqueda d elos ingresos
 */ 

function validarBusqueda(){
	if(form1.numero.value=='' && form1.factura.value==''){
		if( form1.fechaInicio.value == '' || form1.fechaFinal.value == ''){
			alert("Debe Ingresar un rango de fechas");
			return (false);
        }
 		else{
			if(form1.fechaInicio.value > form1.fechaFinal.value ){
				alert("La fecha final debe ser mayor que la inicial");
				return (false);
	        }
        }
	}
	if(form1.numero.value!=''){
		form1.evento.value = '1';
	}
	else{
		if(form1.factura.value!=''){
			form1.evento.value = '2';
		}
	}
	form1.submit();
}
/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller
 * proposito: complertar con ceros los codigos del cliente
 */ 
function completarCodigo(){
	if(form1.cliente.value.length<6){
		var tamano = 6-form1.cliente.value.length;
		var ceros='';
		i =1;
		while(i<=tamano){
			ceros = ceros + '0';
			i++;
		}
       form1.cliente.value = ceros +form1.cliente.value;
	}
} 

function sinformato(element){
	return element.replace( new RegExp(",","g") ,'');
}


function buscarIdentificacion(CONTROLLER){
    form1.action =CONTROLLER+'?estado=Ingreso&accion=Cargar&carpeta=jsp/cxcobrar/ingresoMiscelaneo&pagina=ingresoMiscelaneo.jsp&evento=Identificacion&identificacion='+form1.identificacion.value;
    form1.submit(); 
} 

/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller
 * proposito: validar los ingresos miscelaneos cabecera
 */ 
function validarIngresoMiscelaneo(BASEURL){
   if(form1.tipodoc.value == 'ING'){
	   var campos = new Array("identificacion","nombre","banco","sucursal","valor","moneda","concepto" );
	   var ncampos = new Array("identificacion","nombre de la Identificacion","banco","sucursal","valor","moneda","concepto" );
   }else{
   	   var campos = new Array("identificacion","nombre","valor","moneda","concepto" );
	   var ncampos = new Array("identificacion","nombre de la Identificacion","valor","moneda","concepto" );
       if( form1.banco.value!='' && form1.sucursal.value==''  ){
			alert("Seleccione la sucursal");
            form1.sucursal.focus();
 			return (false);
	   }
	   else if( form1.banco.value=='' && form1.sucursal.value!=''  ){
			alert("Seleccione el banco");
			form1.banco.focus();
 			return (false);
	   }
   }
   var campo = "";
   var ncampo = "";
   for (i = 0; i < campos.length; i++){
    	campo = campos[i];
        ncampo = ncampos[i];
    	if (form1.elements[campo].value == ''){
	    	alert("El campo "+ncampos[i]+" esta vacio.!");				
            form1.elements[campo].focus();
            return (false);
            break;
        }
   }
   /* if( form1.valor.value <= 0){
		alert("El ingreso debe ser mayor que cero");
 		return (false);
	}*/

   form1.valor.value = sinformato(form1.valor.value);
   document.form1.imgaceptar.src = BASEURL+"/images/botones/aceptarDisable.gif";
   document.imgaceptar.disabled = true;
   form1.submit();
}

function validarIngresoMiscelaneoM(BASEURL){
   if(form1.tipodoc.value == 'ING'){
	   var campos = new Array("identificacion","nombre","banco","sucursal","valor","moneda","concepto" );
	   var ncampos = new Array("identificacion","nombre de la Identificacion","banco","sucursal","valor","moneda","concepto" );
   }else{
   	   var campos = new Array("identificacion","nombre","valor","moneda","concepto" );
	   var ncampos = new Array("identificacion","nombre de la Identificacion","valor","moneda","concepto" );
	   if( form1.banco.value!='' && form1.sucursal.value==''  ){
			alert("Seleccione la sucursal");
            form1.sucursal.focus();
 			return (false);
	   }
	    else if( form1.banco.value=='' && form1.sucursal.value!=''  ){
			alert("Seleccione el banco");
			form1.banco.focus();			
 			return (false);
	   }
   }
   var campo = "";
   var ncampo = "";
   for (i = 0; i < campos.length; i++){
    	campo = campos[i];
        ncampo = ncampos[i];
    	if (form1.elements[campo].value == ''){
	    	alert("El campo "+ncampos[i]+" esta vacio.!");				
            form1.elements[campo].focus();
            return (false);
            break;
        }
   }
   /*if( form1.valor.value <= 0){
       alert("El ingreso debe ser mayor que cero");
       return (false);
   }*/

   form1.valor.value = sinformato(form1.valor.value);
   form1.submit();
}


function formato(n, decimales){
   var valor = ""+n;
                  var res = "";
                  for( var i=valor.length-1,j = 0; i >= 0; i--,j++ ){
                       if ( j % 3 == 0 && j != 0 ){
                          res += ",";
						 
                       }
                       res += valor.charAt(i);
					    
                  }
                  //ahora nos toca invertir el numero;
                  var aux = "";
                  for( var i=res.length-1; i >= 0; i-- ){

                       aux += res.charAt(i);
					   if(res.charAt(i) == '-' && res.charAt(i-1) ==',' )i--;
					   
                  }
                  return aux;
}


function buscarTipoImpuesto(tipo, BASEURL, x, valor){
	var campo = document.getElementById("valor"+x);
	if(valor!=''){ 
		var dir =BASEURL+"/jsp/cxcobrar/ItemsIngreso/Buscar_impuesto.jsp?tipo="+tipo+"&id="+x+"&valor="+valor;
		window.open(dir,'Impuesto','width=700,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
	}else{
		alert("Debe digitar el valor");
		campo.select();
    }

}
       
function VerificarCuenta(id){
	var cuenta = document.getElementById("cuenta"+id); 
	var tipo = document.getElementById("tipo"+id); 
	if(!cuenta.value == ''){
      enviar('?estado=IngresoMiscelaneoDetalle&accion=Buscar&evento=Cuenta&cuenta=' + cuenta.value+'&campo='+id+'&tipo='+tipo.value );
   	}
}  
function VerificarDocumento(id){
	var campo = document.getElementById("doc"+id);
	var tipo = document.getElementById("tipodoc"+id); 
	if(tipo.value == '' && campo.value != ''){
		alert("Seleccione el tipo de documento");
	    tipo.focus();
	}
	else if(tipo.value != '' && campo.value == ''){
		alert("digite el nro documento");
	}
    else{
		if(campo.value != '' && tipo.value != ''){
    		enviar('?estado=IngresoMiscelaneoDetalle&accion=Buscar&evento=Documento&documento=' + campo.value+'&campo='+id+'&tipo='+tipo.value );
   		}
    }
}  

function VerificarTipoDocumento(id){
	var campo = document.getElementById("doc"+id);
	var tipo = document.getElementById("tipodoc"+id); 
	if(tipo.value != ''){
		if(!campo.value == ''){
    		enviar('?estado=IngresoMiscelaneoDetalle&accion=Buscar&evento=Documento&documento=' + campo.value+'&campo='+id+'&tipo='+tipo.value );
   		}
	}
}  

function buscarAuxiliar(BASEURL, id, cuenta, tipo){
    var  cuen = document.getElementById("cuenta"+id);
	var tip = document.getElementById("tipo"+id); 
    if(cuen.value == '' || tip.value =='' ){
		alert("Verifique que la cuenta y el tipo tenga datos");
    }
	else{
		var dir =BASEURL+"/jsp/finanzas/contab/subledger/consultasIdSubledger.jsp?idcampo="+id+"&cuenta="+cuenta+"&tipo="+tipo+"&reset=ok";
		window.open(dir,'Auxiliar','width=700,height=520,scrollbars=yes,resizable=yes,top=10,left=65,status=yes');
	}
}
 
function validarBusqMiscelaneo(){
	var eventos = document.getElementsByName("evento");
	var evento = false;
    var valor = 0;
	for( var i=0; i<eventos.length; i++ ){
		if ( eventos[i].checked ){
	   		 evento = true;
             valor = eventos[i].value;
	  	 	 break;
	  	}
	}
    if (!evento){
     	alert("Seleccione uno de los filtros de busqueda");
     	return (false);
    }
	if(valor == 1){
		if(form1.numero.value==''){
			alert("Digite el numero de ingreso");
	     	return (false);
        }
	}
   	else if(valor==2){
		if(form1.identificacion.value==''){
			alert("Digite el codigo del cliente");
	     	return (false);
        }
        if( form1.fechaInicio.value == '' || form1.fechaFinal.value == ''){
			alert("Debe Ingresar un rango de fechas");
			return (false);
        }
 		else{
			if(form1.fechaInicio.value > form1.fechaFinal.value ){
				alert("La fecha final debe ser mayor que la inicial");
	        }
        }
	}
    else  if(valor==3){
		if( form1.fechaInicio.value == '' || form1.fechaFinal.value == ''){
			alert("Debe Ingresar un rango de fechas");
			return (false);
        }
 		else{
			if(form1.fechaInicio.value > form1.fechaFinal.value ){
				alert("La fecha final debe ser mayor que la inicial");
	        }
        }
    }
    else{
		if(form1.tipodoc.value==''){
			alert("Seleccione el tipo");
	     	return (false);
        }
        if( form1.fechaInicio.value == '' || form1.fechaFinal.value == ''){
			alert("Debe Ingresar un rango de fechas");
			return (false);
        }
 		else{
			if(form1.fechaInicio.value > form1.fechaFinal.value ){
				alert("La fecha final debe ser mayor que la inicial");
	        }
        }
	}



   form1.submit();
}


function SumarValores(){
    var total = 0;
	var valor_total = document.getElementById("total");
	var items = document.getElementById("items");
	var acum = document.getElementById("acumdetalle");
	var mon = document.getElementById("mon_ingreso");
	for (j=1; j<=items.value; j++){
		var vlrneto = document.getElementById("valor"+j);
        if(vlrneto!=null){
			if( vlrneto.value != '' && vlrneto.value.length>1 )
				var vlr_total = sinformato(vlrneto.value);
			else
				var vlr_total = 0;
			total = parseFloat(total) + parseFloat(vlr_total);
        }
	}

	if(mon.value == 'DOL'){
		document.getElementById("vlracum").innerText = Fdolar(total,2);
	}
	else{
		document.getElementById("vlracum").innerText = formato(total);
	}
	acum.value = total;
}
/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller
 * proposito: actualizar los valores de los items de ingreso detalle
 *            calcula valoresy realiza sumatoria de acumulado
 */ 
function AcualizarVlr(idcampo){
	var mon = document.getElementById("mon_ingreso");
    var valor = document.getElementById("valor"+idcampo);
    /*var vlr_neto = document.getElementById("vlrneto"+idcampo);
    var val_rfte = document.getElementById("val_rfte"+idcampo);
    var val_rica = document.getElementById("val_rica"+idcampo);
    var rfte = 0; 
    var rica = 0; */
    var vlr = sinformato(valor.value);
    if(vlr==''){
       vlr=0;
    }

    /*if( val_rfte.value != ''){
			document.getElementById("c_valor_rfte"+idcampo).value = formato(vlr * (val_rfte.value/100), 2 );
            rfte = vlr * (val_rfte.value/100);     
	}
    else{
		rfte = 0;
    }
	if( val_rica.value != '' ){
			document.getElementById("c_valor_rica"+idcampo).value = formato( vlr * (val_rica.value/100), 2 );
			rica = vlr * (val_rica.value/100);
	}
    else{
		rica =0;
	}*/

	if(mon.value == 'DOL'){
    	valor.value = Fdolar(vlr,2);
		//vlr_neto.value =  Fdolar( vlr -  ( parseFloat(rfte) + parseFloat(rica) ),2  );	
	}else{
    	valor.value = formato(vlr);
		//vlr_neto.value =  formato( vlr -  ( parseFloat(rfte) + parseFloat(rica) ) );	
	}
	SumarValores();
}

/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller
 * proposito: validar el id subledeger de la cuenta
 */ 
function validarAuxiliar(BASEURL, id){
    var  cuen = document.getElementById("cuenta"+id);
	var tip = document.getElementById("tipo"+id);
    var aux = document.getElementById("auxiliar"+id);
    if(aux.value != ''){ 
    	if(cuen.value == '' || tip.value =='' ){
			alert("Verifique que la cuenta y el tipo tenga datos");
	    }
		else{
			enviar('?estado=IngresoMiscelaneoDetalle&accion=Buscar&evento=Auxiliar&cuenta=' + cuen.value+'&tipo='+tip.value+'&auxiliar='+aux.value+'&campo='+id )
		}
	}
}

/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller
 * proposito: actualizar los valores de los items de ingreso detalle
 *            calcula valoresy realiza sumatoria de acumulado
 */ 
function AcualizarVlrImpuesto(idcampo){
	var mon = document.getElementById("mon_ingreso");
    var valor = document.getElementById("valor"+idcampo);
    /*var vlr_neto = document.getElementById("vlrneto"+idcampo);
    var val_rfte = document.getElementById("c_valor_rfte"+idcampo);
    var val_rica = document.getElementById("c_valor_rica"+idcampo);
    var rfte = 0;
    var rica = 0;*/

    var vlr = sinformato(valor.value);
	if(vlr==''){
       vlr=0;
    }
    /*if( val_rfte.value != ''){
            rfte = val_rfte.value;     
	}
	if( val_rica.value != '' ){
			rica =val_rica.value;
	}*/


	if(mon.value == 'DOL'){
    	valor.value = Fdolar(vlr,2);
		//vlr_neto.value =  Fdolar( (vlr -   ( parseFloat(rfte) + parseFloat(rica) ) ),2  );	
	}
	else{
		valor.value = formato(vlr);
		//vlr_neto.value =  formato( vlr -  ( parseFloat(rfte) + parseFloat(rica) ) );	
	}

	SumarValores();

}


/**validacion de los items*/
function validarItems(){
    var total = 0;
	var acum = parseFloat(document.getElementById("acumdetalle").value);
  	var val_ing = parseFloat(document.getElementById("val_ing").value);
	var items = document.getElementById("items");
    var sw = false;

	for (j=1; j<=items.value; j++){
		var cuenta = document.getElementById("cuenta"+j);
		var tipo = document.getElementById("tipo"+j);
		var aux = document.getElementById("auxiliar"+j);
		var valor = document.getElementById("valor"+j);
		var tipo_doc = document.getElementById("tipodoc"+j);
	    var doc = document.getElementById("doc"+j);
		if(valor){
			if(cuenta.value != ''){
				if(tipo.value =='' && aux.value == ''){
            		sw=true;
				}
				else{
					if(tipo.value !='' && aux.value != ''){
			    		sw=true;
					}else if (tipo.value ==''){
						alert("El tipo esta vacio");
						tipo.focus();
						return false;
					}else if (aux.value ==''){
						alert("El auxiliar esta vacio");
						aux.focus();						
						return false;
					}
		   		}
			}else{
				alert("El nro de la cuenta esta vacio");
				cuenta.focus();
				return false;
        	}
			// valido los tipos documentos
			if (tipo_doc.value != ''){
	    		if(doc.value == ''){
		    		alert("El campo documento esta vacio");
					doc.focus();	
					return false;
				}
			}else{
				if(doc.value != ''){
		    		alert("El campo tipo documento esta vacio");
					tipo_doc.focus();	
					return false;
				}
			}

			//valido el valor
			if(sw){
	    		if(valor.value == ''){
					alert("El valor esta vacio");
					valor.focus();
					return false;
				}
			}
			
		}
	}
	if(acum != val_ing){
		alert("La sumatoria de los items debe ser Igual al ingreso");
		return false;
	}  
	form1.submit();
	return true;
}

function cambiar(dato){
  var valor =  document.getElementById("valor");
  var tasa =  document.getElementById("tasa");
  var vlNet = valor.value.replace( new RegExp(",","g"), "");
  if(dato == 'DOL'){
     valor.onkeypress = new Function (" soloDigitos(event, 'decOK'); ");  
	 formatoDolar( valor,2 );
	 tasa.readOnly = false;
  }else{
	  if(dato == 'BOL'){
	  	tasa.readOnly = false;
	  }
	  else{
		  tasa.value='';
		  tasa.readOnly = true;
	  }
     valor.onkeypress = new Function (" soloDigitos(event, 'decOK'); ");  
	 valor.value=Math.round( parseFloat( sinformato( valor.value ) ) );
	 valor.value     = formato(Math.round(vlNet));
  }
}


function Fdolar (valor, decimales){
		
    valor = new String (valor); 
    var nums =  valor.split('.');
	var salida = new String();
	
	var TieneDec = valor.indexOf('.');
	var dato = new String();
	if( TieneDec !=-1  ){
	   var deci = valor.split('.');
	   var dec       = (deci[1].length >2)?deci[1].charAt(2):deci[1].substr(0,deci[1].length);
	   
	   if(dec > 5){
	       dato =  (parseInt(deci[1].substr(0,2)) + 1);
		   if(dato>99){
		     nums[0] = new String (parseInt(nums[0])+1);
			 return nums[0]+'.00';
		   }
	   }
    }
	var signo = (parseFloat(nums[0])<0?-1:1);
	   nums[0] = new String(parseFloat(nums[0])*signo);
  	   for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	   var sa = salida + (nums.length > 1 && decimales > 0 ? '.'+((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
	   if (signo==-1) sa = "-" + sa;
	   
	   return sa
	
	

 }

function formatear(objeto){
  var numero = objeto.value.replace( new RegExp(",","g"), "");
  var n= parseFloat(numero);
   	 if(isNaN(n)){
		 n=0;
	}
	objeto.value=formatearNumero(n); 
}

function formatearNumero(n){
                  // para estar seguros que vamos a trabajar con un string
                  var valor = ""+n;
                  var res = "";
                  for( var i=valor.length-1,j = 0; i >= 0; i--,j++ ){
                       if ( j % 3 == 0 && j != 0 ){
                          res += ",";
                       }
                       res += valor.charAt(i);
                  }
                  //ahora nos toca invertir el numero;
                  var aux = "";
                  for( var i=res.length-1; i >= 0; i-- ){
                       aux += res.charAt(i);
                  }
                  return aux;
    }

/**Escribir archivo temporal**/
function escribirArchivo(CONTROLLER){
	document.form1.action = CONTROLLER+"?estado=IngresoDetalle&accion=Temporal"; 
	document.form1.submit(); 
}

function Restablecer(CONTROLLER){		
		document.form1.action = CONTROLLER+"?estado=IngresoMiscelaneo&accion=Buscar&evento=Borrar"; 
		document.form1.submit();
    }

function formatoDolar (obj, decimales){
    var numero = obj.value.replace( new RegExp(",","g"), "");
    var nums = ( new String (numero) ).split('.');
	var salida = new String();
	var TieneDec = numero.indexOf('.');
	var dato = new String();
	if( TieneDec !=-1  ){
	   var deci = numero.split('.');
	   var dec       = (deci[1].length >2)?deci[1].charAt(2):deci[1].substr(0,deci[1].length);
	   
	   if(dec > 5){
	       dato =  (parseInt(deci[1].substr(0,2)) + 1);
		   if(dato>99){
		     nums[0] = new String (parseInt(nums[0])+1);
			 obj.value = nums[0]+'.00';
		   }else{
		     for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	         obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
		   
		   }
	   }else{
	       for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	       obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
	   } 
    }else{
  	   for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	   obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
	}
 }
/*
function tipoIngreso(){
	var tipo = document.getElementById('tipodoc').value;
	if(tipo=='ICR'){
		cuenta.style.display="block";
		banco.style.display="none";
		val.innerHTML = 'Valor';
		TDabc.innerHTML = 'ABC';
		document.getElementById('abc').style.display="block";
		document.getElementById('fecha').style.display="none";
		document.getElementById('calen').style.display="none";
		document.getElementById('imagen').style.display="block";
		document.getElementById('TDabc').height=10;
	}else{
		cuenta.style.display="none";
		banco.style.display="block";
		val.innerHTML = 'Valor Consignación';
		TDabc.innerHTML = 'Fecha Consignación';
		document.getElementById('abc').value = '';
		document.getElementById('abc').style.display="none";
		document.getElementById('imagen').style.display="none";
		
		document.getElementById('fecha').style.display="block";
		document.getElementById('fecha').size=12;
		document.getElementById('calen').style.display="block";
		document.getElementById('TDabc').height=10;
	}

}*/


function VerificarCuentaContable(id,modulo){
	var cuenta = document.getElementById("cuenta"+id); 
	var tipo = document.getElementById("tipo"+id); 
	if(!cuenta.value == ''){
      enviar('?estado=CuentaContable&accion=Verificar&evento=Cuenta&cuenta=' + cuenta.value+'&campo='+id+'&tipo='+tipo.value+'&modulo='+modulo );
   	}
}

/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller
 * proposito: validar los ingresos cabecera
 */ 
function validarIngresoCliente(BASEURL){
   if(form1.tipodoc.value == 'ING'){
   	   var campos = new Array("cliente","nombre","banco","sucursal","fecha","concepto","valor","moneda" );
       var ncampos = new Array("cliente","nombre cliente","banco","sucursal","fecha","concepto","valor","moneda" );
   }else if(form1.tipodoc.value == 'ICR'){
       var campos = new Array("cliente","nombre","cuenta1","concepto","valor","moneda" );
       var ncampos = new Array("cliente","nombre cliente","cuenta","concepto","valor","moneda" );
   }else if(form1.tipodoc.value == 'ICA'){
       var campos = new Array("cliente","nombre","cuenta1","concepto","valor","moneda" );
       var ncampos = new Array("cliente","nombre cliente","cuenta","concepto","valor","moneda" );
   }
   var campo = "";
   var ncampo = "";
   for (i = 0; i < campos.length; i++){
    	campo = campos[i];
        ncampo = ncampos[i];
    	if (form1.elements[campo].value == ''){
	    	alert("El campo "+ncampos[i]+" esta vacio.!");				
            form1.elements[campo].focus();
            return (false);
            break;
        }
   }
   if( form1.cuenta1.value != '' && form1.tipo1.value != '' && form1.auxiliar1.value == '' ){
	   alert("Debe llenar el auxiliar");
	   form1.auxiliar1.focus();
 		return (false);
   }
   if( form1.tipo1.value == ''){
	   form1.auxiliar1.value = '';
   }
//   if( form1.nro_extracto.value === ''){
//	   form1.nro_extracto.focus();
//            alert("Debe llenar el numero de extracto!");
//            return (false);
//   }
   /*if( form1.valor.value <= 0){
		alert("El ingreso debe ser mayor que cero");
 		return (false);
   }*/
   form1.valor.value = sinformato(form1.valor.value);
   document.form1.imgaceptar.src = BASEURL+"/images/botones/aceptarDisable.gif";
   document.imgaceptar.disabled = true;
   form1.submit();
}

function soloNumeros(id) {
    var valor = document.getElementById(id).value;
    valor =  valor.replace(/[^0-9^.]+/gi,"");
    document.getElementById(id).value = valor;
}