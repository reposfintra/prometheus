/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function iniciar1() {
    cargarPrioridadRequisicion();
}
function iniciar2() {
    cargarEstadoRequisicion();
}

function iniciar3() {
    cargarTipoTarea();
}

function iniciar4() {
    cargarEstadoTipoTarea();
}

function iniciar5() {
    cargarTipoRequisicion();
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function cargarPrioridadRequisicion() {
    var operacion;
    var grid_tablas = jQuery("#tabla_prioridad_requisicion");
    if ($("#gview_tabla_prioridad_requisicion").length) {
        reloadGridGrillas(grid_tablas, "GRILLAPRIORIDADREQUISICION");
    } else {
        grid_tablas.jqGrid({
            caption: "PRIORIDAD REQUISICION",
            url: "./controller?estado=Requisicion&accion=Eventos",
            mtype: "POST",
            datatype: "json",
            height: '210',
            width: '480',
            colNames: ['id', 'Descripcion', 'Color', 'Estado'],
            colModel: [
                {name: 'prireid', index: 'prireid', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'priredecripcion', index: 'priredecripcion', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'prirecolor', index: 'prirecolor', width: 180, sortable: true, align: 'left', hidden: false},
                {name: 'prireestado', index: 'prireestado', width: 120, sortable: true, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                data: {
                    evento: "GRILLAPRIORIDADREQUISICION"
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_prioridad_requisicion").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 0;
                ventanaPrioridadRequisicion(operacion);
            }
        });
        $("#tabla_prioridad_requisicion").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_prioridad_requisicion"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                operacion = 1;
                var id = filas.prireid;
                var descripcion = filas.priredecripcion;
                var color = filas.prirecolor;
                var estado = filas.prireestado;

                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "Activo") {
                        ventanaPrioridadRequisicion(operacion, descripcion, id, color);
                    } else {
                        mensajesDelSistema("PARA EDITAR LA , EL ESTADO DEBE SER ACTIVO", '300', '150', false);
                    }
                }
            }
        });
        $("#tabla_prioridad_requisicion").navButtonAdd('#pager', {
            caption: "Activar / Inactivar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_prioridad_requisicion"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.prireid;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    Confirmacion("�ESTA SEGURO QUE DESEA CAMBIAR EL ESTADO?", '300', '150', id);
                }
            }
        });
    }
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function reloadGridGrillas(grid_tablas, evento) {
    grid_tablas.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Requisicion&accion=Eventos",
        ajaxGridOptions: {
            type: "POST",
            data: {
                evento: evento
            }
        }
    });
    grid_tablas.trigger("reloadGrid");
}

function ventanaPrioridadRequisicion(operacion, descripcion, id, color) {
    if (operacion === 0) {
        $("#dialogMsjPrioridadRequisicion").dialog({
            width: '470',
            height: '230',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    guardarPrioridadRequisicion();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    } else if (operacion === 1) {
        $("#idprireq").val(id);
        $("#descripcion").val(descripcion);
        $("#color").val(color);

        $("#dialogMsjPrioridadRequisicion").dialog({
            width: '470',
            height: '230',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    updatePrioridadRequisicion();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    }

    $("#dialogMsjPrioridadRequisicion").siblings('div.ui-dialog-titlebar').remove();
}

function guardarPrioridadRequisicion() {
    var descripcion = $("#descripcion").val();
    var color = $("#color").val();
    if ((descripcion != "") && (color != "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Requisicion&accion=Eventos",
            data: {
                evento: "GUERDARPRIORIDADREQUISICION",
                descripcion: descripcion,
                color: color
            },
            success: function (data) {
                cargarPrioridadRequisicion();
                $("#descripcion").val("");
                $("#color").val("");
                mensajesDelSistema("EXITO AL GUARDAR ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function updatePrioridadRequisicion() {
    var descripcion = $("#descripcion").val();
    var color = $("#color").val();
    var id = $("#idprireq").val();
    if ((descripcion != "") && (color != "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Requisicion&accion=Eventos",
            data: {
                evento: "UPDATEPRIORIDADREQUISICION",
                descripcion: descripcion,
                color: color,
                id: id
            },
            success: function (data) {
                cargarPrioridadRequisicion();
                $("#descripcion").val("");
                $("#color").val("");
                $("#idprireq").val("");
                mensajesDelSistema("EXITO AL GUARDAR ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function Confirmacion(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Si": function () {
                cambiarEstadoPrIpreq(id);
                $(this).dialog("destroy");
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cambiarEstadoPrIpreq(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Requisicion&accion=Eventos",
        data: {
            evento: "CAMBIARESTADOPRIOREQ",
            id: id
        },
        success: function (data) {
            cargarPrioridadRequisicion();
            //mensajesDelSistema("EXITO EN EL CAMBIO DE ESTADO EDS", '230', '150', true);
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}

function cargarEstadoRequisicion() {
    var operacion;
    var grid_tablas = jQuery("#tabla_estado_requisicion");
    if ($("#gview_tabla_estado_requisicion").length) {
        reloadGridGrillas(grid_tablas, "GRILLAESTADOREQUISICION");
    } else {
        grid_tablas.jqGrid({
            caption: "ESTADO REQUISICION",
            url: "./controller?estado=Requisicion&accion=Eventos",
            mtype: "POST",
            datatype: "json",
            height: '150',
            width: '350',
            colNames: ['id', 'Descripcion', 'Estado'],
            colModel: [
                {name: 'estreqid', index: 'estreqid', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'estreqdescripcion', index: 'estreqdescripcion', width: 200, sortable: true, align: 'left'},
                {name: 'estreqestado', index: 'estreqestado', width: 120, sortable: true, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                data: {
                    evento: "GRILLAESTADOREQUISICION"
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_estado_requisicion").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 0;
                ventanaEstadoRequisicion(operacion);
            }
        });
        $("#tabla_estado_requisicion").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_estado_requisicion"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                operacion = 1;
                var id = filas.estreqid;
                var descripcion = filas.estreqdescripcion;
                var estado = filas.estreqestado;

                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "Activo") {
                        ventanaEstadoRequisicion(operacion, descripcion, id);
                    } else {
                        mensajesDelSistema("PARA EDITAR , EL ESTADO DEBE SER ACTIVO", '300', '150', false);
                    }
                }
            }
        });
        $("#tabla_estado_requisicion").navButtonAdd('#pager', {
            caption: "A / I",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_estado_requisicion"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.estreqid;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    ConfirmacionEstadoReq("�ESTA SEGURO QUE DESEA CAMBIAR EL ESTADO?", '300', '150', id);
                }
            }
        });
    }
}

function ventanaEstadoRequisicion(operacion, descripcion, id) {
    if (operacion === 0) {
        $("#dialogMsjestadoRequisicion").dialog({
            width: '470',
            height: '230',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    guardarEstadoRequisicion();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    } else if (operacion === 1) {
        $("#idestadoreq").val(id);
        $("#descestadoreq").val(descripcion);

        $("#dialogMsjestadoRequisicion").dialog({
            width: '470',
            height: '230',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    updateEstadoRequisicion();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    $("#dialogMsjestadoRequisicion").siblings('div.ui-dialog-titlebar').remove();
}

function guardarEstadoRequisicion() {
    var descripcion = $("#descestadoreq").val();
    if ((descripcion !== "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Requisicion&accion=Eventos",
            data: {
                evento: "GUERDARESTADOREQUISICION",
                descripcion: descripcion
            },
            success: function (data) {
                cargarEstadoRequisicion();
                $("#descripcion").val("");
                mensajesDelSistema("EXITO AL GUARDAR ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function updateEstadoRequisicion() {
    var descripcion = $("#descestadoreq").val();
    var id = $("#idestadoreq").val();
    if ((descripcion !== "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Requisicion&accion=Eventos",
            data: {
                evento: "EDITARESTADOREQUISICION",
                descripcion: descripcion,
                id: id
            },
            success: function (data) {
                cargarEstadoRequisicion();
                $("#descestadoreq").val("");
                $("#idestadoreq").val("");
                mensajesDelSistema("EXITO AL GUARDAR ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function ConfirmacionEstadoReq(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Si": function () {
                cambiarEstadoReq(id);
                $(this).dialog("destroy");
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cambiarEstadoReq(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Requisicion&accion=Eventos",
        data: {
            evento: "CAMBIARESTADOREQ",
            id: id
        },
        success: function (data) {
            cargarEstadoRequisicion();
            //mensajesDelSistema("EXITO EN EL CAMBIO DE ESTADO EDS", '230', '150', true);
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}

function cargarTipoTarea() {
    var operacion;
    var grid_tablas = jQuery("#tabla_tipo_tarea");
    if ($("#gview_tabla_tipo_tarea").length) {
        reloadGridGrillas(grid_tablas, "GRILLATIPOTAREA");
    } else {
        grid_tablas.jqGrid({
            caption: "TIPO TAREA",
            url: "./controller?estado=Requisicion&accion=Eventos",
            mtype: "POST",
            datatype: "json",
            height: '150',
            width: '350',
            colNames: ['id', 'Tipo Tarea', 'Estado'],
            colModel: [
                {name: 'tiptareaid', index: 'tiptareaid', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'tiptareadesc', index: 'tiptareadesc', width: 200, sortable: true, align: 'left'},
                {name: 'tiptareaestado', index: 'tiptareaestado', width: 120, sortable: true, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                data: {
                    evento: "GRILLATIPOTAREA"
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_tipo_tarea").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 0;
                ventanaTipoTarea(operacion);
            }
        });
        $("#tabla_tipo_tarea").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_tipo_tarea"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                operacion = 1;
                var id = filas.tiptareaid;
                var descripcion = filas.tiptareadesc;
                var estado = filas.tiptareaestado;

                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "Activo") {
                        ventanaTipoTarea(operacion, descripcion, id);
                    } else {
                        mensajesDelSistema("PARA EDITAR , EL ESTADO DEBE SER ACTIVO", '300', '150', false);
                    }
                }
            }
        });
        $("#tabla_tipo_tarea").navButtonAdd('#pager', {
            caption: "A / I",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_tipo_tarea"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.tiptareaid;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    ConfirmacionEstadoTipoTarea("�ESTA SEGURO QUE DESEA CAMBIAR EL ESTADO?", '300', '150', id);
                }
            }
        });
    }
}

function ventanaTipoTarea(operacion, descripcion, id) {
    if (operacion === 0) {
        $("#dialogMsjTipoTarea").dialog({
            width: '460',
            height: '230',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    guardarTipoTarea();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    } else if (operacion === 1) {
        $("#idtiptarea").val(id);
        $("#desctiptarea").val(descripcion);

        $("#dialogMsjTipoTarea").dialog({
            width: '460',
            height: '230',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    updateTipoTarea();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    $("#dialogMsjTipoTarea").siblings('div.ui-dialog-titlebar').remove();
}

function ConfirmacionEstadoTipoTarea(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Si": function () {
                cambiarEstadoTipoTarea(id);
                $(this).dialog("destroy");
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cambiarEstadoTipoTarea(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Requisicion&accion=Eventos",
        data: {
            evento: "CAMBIARESTADOTIPOTAREA",
            id: id
        },
        success: function (data) {
            cargarTipoTarea();
            //mensajesDelSistema("EXITO EN EL CAMBIO DE ESTADO EDS", '230', '150', true);
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}

function guardarTipoTarea() {
    var descripcion = $("#desctiptarea").val();
    if ((descripcion !== "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Requisicion&accion=Eventos",
            data: {
                evento: "GUERDARTIPOTAREA",
                descripcion: descripcion
            },
            success: function (data) {
                cargarTipoTarea();
                $("#desctiptarea").val("");
                mensajesDelSistema("EXITO AL GUARDAR ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function updateTipoTarea() {
    var descripcion = $("#desctiptarea").val();
    var id = $("#idtiptarea").val();
    if ((descripcion !== "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Requisicion&accion=Eventos",
            data: {
                evento: "EDITARTIPOTAREA",
                descripcion: descripcion,
                id: id
            },
            success: function (data) {
                cargarTipoTarea();
                $("#desctiptarea").val("");
                $("#idtiptarea").val("");
                mensajesDelSistema("EXITO AL GUARDAR ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function cargarEstadoTipoTarea() {
    var operacion;
    var grid_tablas = jQuery("#tabla_estado_tipo_tarea");
    if ($("#gview_tabla_estado_tipo_tarea").length) {
        reloadGridGrillas(grid_tablas, "GRILLAESTADOTIPOTAREA");
    } else {
        grid_tablas.jqGrid({
            caption: "ESTADO TIPO TAREA",
            url: "./controller?estado=Requisicion&accion=Eventos",
            mtype: "POST",
            datatype: "json",
            height: '150',
            width: '350',
            colNames: ['id', 'Descripcion', 'Estado'],
            colModel: [
                {name: 'esttiptareaid', index: 'esttiptareaid', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'esttiptareadesc', index: 'esttiptareadesc', width: 200, sortable: true, align: 'left'},
                {name: 'esttiptareaestado', index: 'esttiptareaestado', width: 120, sortable: true, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                data: {
                    evento: "GRILLAESTADOTIPOTAREA"
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_estado_tipo_tarea").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 0;
                ventanaEstadoTipoTarea(operacion);
            }
        });
        $("#tabla_estado_tipo_tarea").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_estado_tipo_tarea"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                operacion = 1;
                var id = filas.esttiptareaid;
                var descripcion = filas.esttiptareadesc;
                var estado = filas.esttiptareaestado;

                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "Activo") {
                        ventanaEstadoTipoTarea(operacion, descripcion, id);
                    } else {
                        mensajesDelSistema("PARA EDITAR , EL ESTADO DEBE SER ACTIVO", '300', '150', false);
                    }
                }
            }
        });
        $("#tabla_estado_tipo_tarea").navButtonAdd('#pager', {
            caption: "A / I",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_estado_tipo_tarea"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.esttiptareaid;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    ConfirmacionCambioEstTipoTarea("�ESTA SEGURO QUE DESEA CAMBIAR EL ESTADO?", '300', '150', id);
                }
            }
        });
    }
}

function ventanaEstadoTipoTarea(operacion, descripcion, id) {
    if (operacion === 0) {
        $("#dialogMsjEstadoTipoTarea").dialog({
            width: '460',
            height: '230',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    guardarEstadoTipoTarea();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    } else if (operacion === 1) {
        $("#idestiptarea").val(id);
        $("#descestiptarea").val(descripcion);

        $("#dialogMsjEstadoTipoTarea").dialog({
            width: '460',
            height: '230',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    updateEstadoTipoTarea()();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    $("#dialogMsjEstadoTipoTarea").siblings('div.ui-dialog-titlebar').remove();
}

function guardarEstadoTipoTarea() {
    var descripcion = $("#descestiptarea").val();
    if ((descripcion !== "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Requisicion&accion=Eventos",
            data: {
                evento: "GUERDARESTADOTIPOTAREA",
                descripcion: descripcion
            },
            success: function (data) {
                cargarEstadoTipoTarea();
                $("#descestiptarea").val("");
                mensajesDelSistema("EXITO AL GUARDAR ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function updateEstadoTipoTarea() {
    var descripcion = $("#descestiptarea").val();
    var id = $("#idestiptarea").val();
    if ((descripcion !== "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Requisicion&accion=Eventos",
            data: {
                evento: "EDITARESTADOTIPOTAREA",
                descripcion: descripcion,
                id: id
            },
            success: function (data) {
                cargarEstadoTipoTarea();
                $("#descestiptarea").val("");
                $("#idestiptarea").val("");
                mensajesDelSistema("EXITO AL GUARDAR ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function  ConfirmacionCambioEstTipoTarea(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Si": function () {
                cambiarEstTipoTarea(id);
                $(this).dialog("destroy");
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cambiarEstTipoTarea(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Requisicion&accion=Eventos",
        data: {
            evento: "CAMBIARESTTIPOTAREA",
            id: id
        },
        success: function (data) {
            cargarEstadoTipoTarea();
            //mensajesDelSistema("EXITO EN EL CAMBIO DE ESTADO EDS", '230', '150', true);
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}

function cargarTipoRequisicion() {
    var operacion;
    var grid_tablas = jQuery("#tabla_tipo_requisicion");
    if ($("#gview_tabla_tipo_requisicion").length) {
        reloadGridGrillas(grid_tablas, "GRILLATIPOREQUISICION");
    } else {
        grid_tablas.jqGrid({
            caption: "TIPO REQUISICION",
            url: "./controller?estado=Requisicion&accion=Eventos",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '750',
            colNames: ['id', 'Descripcion', 'Meta Eficacia', 'Meta Eficiencia', 'Estado'],
            colModel: [
                {name: 'tiporeqid', index: 'tiporeqid', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'tiporeqdescripcion', index: 'tiporeqdescripcion', width: 200, sortable: true, align: 'left'},
                {name: 'tiporeqeficacia', index: 'tiporeqeficacia', width: 200, sortable: true, align: 'left'},
                {name: 'tiporeqeficiencia', index: 'tiporeqeficiencia', width: 200, sortable: true, align: 'left'},
                {name: 'tiporeqestado', index: 'tiporeqestado', width: 120, sortable: true, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                data: {
                    evento: "GRILLATIPOREQUISICION"
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_tipo_requisicion").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 0;
                ventanaTipoRequisicion(operacion);
            }
        });
        $("#tabla_tipo_requisicion").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_tipo_requisicion"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                operacion = 1;
                var id = filas.tiporeqid;
                var descripcion = filas.tiporeqdescripcion;
                var estado = filas.tiporeqestado;
                var eficacia = filas.tiporeqeficacia;
                var eficiencia = filas.tiporeqeficiencia;


                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "Activo") {
                        ventanaTipoRequisicion(operacion, descripcion, id, eficacia, eficiencia);
                    } else {
                        mensajesDelSistema("PARA EDITAR , EL ESTADO DEBE SER ACTIVO", '300', '150', false);
                    }
                }
            }
        });
        $("#tabla_tipo_requisicion").navButtonAdd('#pager', {
            caption: "A / I",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_tipo_requisicion"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.tiporeqid;
                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    ConfirmacionTipoReq("�ESTA SEGURO QUE DESEA CAMBIAR EL ESTADO?", '300', '150', id);
                }
            }
        });
    }
}

function ventanaTipoRequisicion(operacion, descripcion, id, eficacia, eficiencia) {
    if (operacion === 0) {
        $("#dialogMsjTipoRequisicion").dialog({
            width: '470',
            height: '230',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    guardarTipoRequisicion();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    } else if (operacion === 1) {
        $("#nomtiporeq").val(descripcion);
        $("#idtipreq").val(id);
        $("#eficacia").val(eficacia);
        $("#eficiencia").val(eficiencia);
        $("#dialogMsjTipoRequisicion").dialog({
            width: '470',
            height: '230',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            buttons: {//crear bot�n cerrar
                "Guardar": function () {
                    updateTipoRequisicion();
                },
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    $("#dialogMsjTipoRequisicion").siblings('div.ui-dialog-titlebar').remove();
}

function guardarTipoRequisicion() {
    var nomtiporeq = $("#nomtiporeq").val();
    var eficacia = $("#eficacia").val();
    var eficiencia = $("#eficiencia").val();

    if ((nomtiporeq !== "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Requisicion&accion=Eventos",
            data: {
                evento: "GUERDARTIPOREQUISICION",
                nomtiporeq: nomtiporeq,
                eficacia: eficacia,
                eficiencia: eficiencia

            },
            success: function (data) {
                cargarTipoRequisicion();
                $("#nomtiporeq").val("");
                $("#eficacia").val("");
                $("#eficiencia").val("");

                mensajesDelSistema("EXITO AL GUARDAR ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function updateTipoRequisicion() {
    var descripcion = $("#nomtiporeq").val();
    var id = $("#idtipreq").val();
    var eficacia = $("#eficacia").val();
    var eficaciencia = $("#eficiencia").val();
    if ((descripcion !== "")) {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Requisicion&accion=Eventos",
            data: {
                evento: "EDITARTIPOREQUISICION",
                descripcion: descripcion,
                id: id,
                eficacia: eficacia,
                eficaciencia: eficaciencia
            },
            success: function (data) {
                cargarTipoRequisicion();
                $("#nomtiporeq").val("");
                $("#idtipreq").val("");
                $("#eficacia").val("");
                $("#eficiencia").val("");
                mensajesDelSistema("EXITO AL GUARDAR ", '230', '150', true);
            }, error: function (result) {
                alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
            }
        });
    } else {
        mensajesDelSistema("FALTA LLENAR CAMPOS", '230', '150', false);
    }
}

function ConfirmacionTipoReq(msj, width, height, id) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Si": function () {
                cambiarTipoReq(id);
                $(this).dialog("destroy");
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cambiarTipoReq(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Requisicion&accion=Eventos",
        data: {
            evento: "CAMBIARESTADOTIPOREQ",
            id: id
        },
        success: function (data) {
            cargarTipoRequisicion();
            //mensajesDelSistema("EXITO EN EL CAMBIO DE ESTADO EDS", '230', '150', true);
        }, error: function (result) {
            alert('ERROR NO SE PUEDO GUARDAR VERIFICAR DATOS');
        }
    });
}