
/*
  AUTOR :  FERNEL VILLACOB
  FECHA :  07/10/2005
*/



/*_____________________________________________________________________________
   NUEVAS VENTANAS
  ___________________________________________________________________________ */



        function NuevaVentana(url,largo,ancho,x,y){
              var option =" status=yes, resizable=yes, top="+ x +",left="+ y +",toolbar=no,location=no,status=yes, menubar=no,scrollbars=yes, resizable=yes,width=" + largo + ",height=" + ancho;
              ventana=window.open(url,"ventana",option);
              return ventana;
        }

        function NuevaVentana(url,name,largo,ancho,x,y){
              var option ="status=yes, resizable=yes, scrollbars=yes, status=yes, top="+ x +",left="+ y +",width=" + largo + ",height=" + ancho;
              ventana=window.open(url,name,option);
              return ventana;
         }
   

/* _____________________________________________________________________________
   SELECCION DE CHECKED
   _____________________________________________________________________________ */

  function selectChecked(theForm,ele){
      if (ele.id=='All'){
         for (i=0;i<theForm.length;i++)
               if (theForm.elements[i].type=='checkbox')
		  theForm.elements[i].checked=theForm.All.checked;
      }else
          theForm.All.checked=true;
 
     for (i=0;i<theForm.length;i++)
          if (theForm.elements[i].type=='checkbox' && !theForm.elements[i].checked)
             theForm.All.checked=false;
   }

   

/*_____________________________________________________________________________
   CONTROL DE BOTONES
  ___________________________________________________________________________ */



          function anularBtn(btn){ 
              btn.style.visibility='hidden';
          }
              
          function activarBtn(btn){
             btn.style.visibility='';            
          }



/* _____________________________________________________________________________
    BUSQUEDA DE PALABRAS EN COMBOS
   _____________________________________________________________________________ */


     function searchCombo(data, cmb){
           data = data.toLowerCase();
           for (i=0;i<cmb.length;i++){
              cmb[i].selected = false; 
           }           
           for (i=0;i<cmb.length;i++){                
                var txt = cmb[i].text.toLowerCase( );
                if( txt.indexOf(data)==0 ){
                    cmb[i].selected = true;
                    break;
               }
               else
                  cmb[i].selected = false;
            }
      }
          



/*_____________________________________________________________________________
   REMPLAZAR ALL : CARACTERES DENTRO DE UNA CADENA
  ___________________________________________________________________________ */


  function  replaceALL(cad,caracter,newCaracter){
     var n = "";
     for (i=0;i<cad.length;i++){
         var letra = cad.charAt(i);
         var nuevo = (letra==caracter)?newCaracter:letra;
         n+=nuevo;
     }
     return n;
  }




/*_____________________________________________________________________________
   CANTIDAD DE CARACTER IGUAL EN UNA CADENA
  ___________________________________________________________________________ */


  function  countCaracter(cad,caracter){
     var n = 0;
     for (i=0;i<cad.length;i++){
         var letra = cad.charAt(i);
         if(letra==caracter)
            n++;
     }
     return n;
  }






/*_____________________________________________________________________________
   SEPARADORES DE MILES
  ___________________________________________________________________________ */



function formatear(objeto){
    var n     =   replaceALL(objeto.value,',','');
    objeto.value=formatearNumero(n);
}



function formatearNumero(n){
 // para estar seguros que vamos a trabajar con un string
    
    var valor = ""+n;
    var aux   = "";
    if(valor!='' ){
       if( isNumerico(valor) ){
            var cont     = 1;
            var res      = '';

         // Anteponemos cero al punto decimal
            var pr = valor.charAt(0);
            if(pr=='.')
                valor = '0' + valor;

            for( var i=valor.length-1; i >= 0; i-- ){
                 var caracter = valor.charAt(i);
                 res         += caracter;
                 if(caracter=='.'){
                     res  = replaceALL(res,',','');
                     cont = 1;
                 }
                 else{
                    
                     if(cont==3 && i>0){
                        res += ",";
                        cont   =0;
                     }
                     cont++;
                 }
            }


            for( var i=res.length-1; i >= 0; i-- ){                  
                   aux += res.charAt(i);
            }


       }
    }
    return aux;
}


/*_____________________________________________________________________________
  NUMERICO
  _____________________________________________________________________________ */


  function isNumerico(cad){
       car     = "0123456789.-";
       sw      = 0;
       swPunto = 0;
       cad     = replaceALL(cad,'+','');

       if (cad.length<1) sw=1;
       else{
      
       // Punto decimal
           var cantPuntos = countCaracter(cad,'.');
           if( cantPuntos > 1 ){
               alert('Deber� tener un solo punto decimal');
               return false;
           }

       // Signos negativos
           var cantMenos = countCaracter(cad,'-');
           if( cantMenos > 1 ){
                alert('Deber� tener un solo signo negativo');
                return false;
           }
           else if(  cantMenos == 1   ){           
               var pr = cad.charAt(0);
               if( pr!='-' ){
                   alert('El signo negativo deber� ir al principio del n�mero');
                   return false;
               }
           }



          for (i=0;i<cad.length;i++){
               var caracter = cad.charAt(i);
               if (car.indexOf( caracter )==-1){
                   alert('Deber� digitar solo n�meros');
                   return false;
               }
           }

       }

       if(sw==0) return true;
    
  }


/*_____________________________________________________________________________
 COMBOS MULTIPLES
  ___________________________________________________________________________ */


       function move(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
            }
         /*  order(cmbD); */
           deleteRepeat(cmbD);
        }
        
        function addOption(Comb,valor,texto){
           if(valor!='' && texto!=''){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
          }
        }  
        
        function order(cmb){
           for (i=0; i<cmb.length;i++)
             for (j=i+1; j<cmb.length;j++)
                if (cmb[i].text > cmb[j].text){
                    var temp = document.createElement("OPTION");
                    temp.value = cmb[i].value;
                    temp.text  = cmb[i].text;
                    cmb[i].value = cmb[j].value;
                    cmb[i].text  = cmb[j].text;
                    cmb[j].value = temp.value;
                    cmb[j].text  = temp.text;
                }
        }
        
        /*function deleteRepeat(cmb){
          var ant='';
          for (i=0;i<cmb.length;i++){
             if(ant== cmb[i].value){
                cmb.remove(i);
                i--;
             }
             ant = cmb[i].value;
          }
        } */
         function deleteRepeat(cmb){ 
              var total = cmb.length;   
              for (i=0; i<total;i++)
                   for (j=i+1; j<total;j++){
                       if (cmb[i].value == cmb[j].value){
                           cmb.remove(j);
                           total--;
                       } 
                   }
        }
        
       function selectAll(cmb){
            for (i=0;i<cmb.length;i++)
              cmb[i].selected = true;
       }



      function desabledALL(cmb){
              for (i=0;i<cmb.length;i++)
                  cmb[i].selected = false;
      }


     
     function deleteAll(cmb){
         for (i=cmb.length-1; i>=0 ;i--)
               cmb.remove(i);
     }

/*_____________________________________________________________________________
  IMAGENES
  ___________________________________________________________________________ */



  function viewImagen(url, tipoAct, tipoDoc, documento ){

     var contenido  = " <center> ";
         contenido += " <table>  ";
         contenido += "   <tr><td> <img src='"+ url +"'></td></tr> ";
         contenido += " </table> ";
         contenido += " <br>     ";
         if(tipoAct != '' && tipoDoc != '' && documento != '' ){ 
         	contenido += " <a href='imagen/Manejo.jsp?actividad="+ tipoAct +"&tipoDocumento="+ tipoDoc +"&documento="+ documento +"'>Nueva Imagen</a> ";
         }
     var ventana = window.open('','Imagen',' top=100,left=150, width=600, height=500, scrollbars=yes, status=yes, resizable=yes  ');

     ventana.document.write(contenido);
     ventana.document.close(); 
  }



/*_____________________________________________________________________________
  PLAN DE VIAJES
  ___________________________________________________________________________ */


        function planViaje_validarPlanilla(theForm){
              var sw = 0;
              if(theForm.txtPlanilla.value==''){
                 sw = 1;
                 alert('Deber� digitar el numero de la planilla');
                 theForm.txtPlanilla.focus();
              }
             
              var url = theForm.action;
              for (i=0;i<formulario.length;i++){                 
                  if( theForm.elements[i].type =='radio' ){
                      if( theForm.elements[i].checked==true  )
                         url =  url +"&"+ formulario.elements[i].name + "=" + formulario.elements[i].value;
                  }  
                  else
                      url =  url +"&"+ formulario.elements[i].name + "=" + formulario.elements[i].value;
              }

              if(sw==0)
                 var win = window.open(url,'planViaje',' top=100,left=100, width=700, height=600, scrollbars=yes, status=yes, resizable=yes  ');
        }




      function validarFormPlanViaje(theForm, opcion){

            selectAll( theForm.alimentacionPlan );
            selectAll( theForm.pernotacionPlan );
            selectAll( theForm.parqueaderoPlan );
            selectAll( theForm.tanqueoPlan );

             if( theForm.cbRetorno.checked==false)
                 theForm.hRetorno.value='N';
             else
                 theForm.hRetorno.value='S';

             if(opcion=='Crear')
                 theForm.Opcion.value='creado';
             if(opcion=='Modificar')
                 theForm.Opcion.value='modificado';
             if(opcion=='Eliminar')
                 theForm.Opcion.value='eliminado';

             theForm.submit();
      }
      /*script que valida que una fecha inicial sea menor que la final*/
      function validarFechaIF(form){
  	var fecha1 = form.fechaInicio.value.replace('-','').replace('-','');
   	var fecha2 = form.fechaFinal.value.replace('-','').replace('-','');	
	if(fecha1 > fecha2) {     
   		alert('La FECHA INICIAL debe ser igual o menor que la FECHA FINAL');
		return (false);
         }else {
		return (true);
	}
     }
