/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var $j = jQuery.noConflict();
$j(document).ready(function() {
    var FechaActual = new Date();   
    var anio = FechaActual.getFullYear();
    var mes = FechaActual.getMonth()+1;
    cargarMeses(mes);
    cargarAnios(anio);
    getFiltro("procesos");
    if ($('#div_actividades_usuario').is(':visible')) { 
        getUsuariosProceso();       
        $("#procesos").change(function() {        
            getUsuariosProceso();           
        });     
        
        $j('#buscar_acts').click(function() {
            document.getElementById('calcontainer').style.display = "block";
            cargarActividadesUsuario(FechaActual, anio, mes);
        });
    }else{        
        $j('#buscar').click(function() {
            document.getElementById('calcontainer').style.display = "block";
            cargarTareas(FechaActual);
        });
    }

});

function cargarAnios(anioActual){
    
    var cad="";
    for(var j=2009; j<=2020; j++){
        var anio = j;
        if ( anio == anioActual ) {
            cad = "selected";
        }else{ 
            cad = "";
        }
        $j('#anho').append('<option value=' + anio + ' ' + cad + '>' + anio + '</option>');       
    }

}

function cargarMeses(mesActual){
      
      $j('#mes').append("<option value='0'>Todos</option>");
            var meses = "";
            var cadi = "";
            for (var i = 1; i <= 12; i++) {
                var value = i;                 
                switch (value) {
                    case 1:
                        meses='ENE';
                        break
                    case 2:
                        meses='FEB';
                        break
                    case 3:
                        meses = "MAR";
                        break;
                    case 4:
                        meses = "ABR";
                        break;
                    case 5:
                        meses = "MAY";
                        break;
                    case 6:
                        meses = "JUN";
                        break;
                    case 7:
                        meses = "JUL";
                        break;
                    case 8:
                        meses = "AGO";
                        break;
                    case 9:
                        meses = "SEP";
                        break;
                    case 10:
                        meses = "OCT";
                        break;
                    case 11:
                        meses = "NOV";
                        break;
                    case 12:
                        meses = "DIC";
                        break;                  
                }
                if (value == mesActual) {
                    cadi = "selected";
                } else {
                    cadi = "";
                }
                $j('#mes').append('<option value=' + value + ' ' + cadi + '>' + meses + '</option>');
            }
}

function getFiltro(nombre) {
    var elemento= $j("#"+nombre);
    $j.ajax({
        url: '/fintra/controller?estado=Prioridades&accion=Requisiciones',
        datatype: 'json',
        type: 'get',
        data: {opcion: 2, consulta: "nombre"},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    alert(json.error);
                } else {
                    elemento.empty();
                    for (var llave in json) {
                        elemento.append('<option value="'+llave+'">'+json[llave]+'</option>');
                    }
                }
            } catch (exc) {
                console.error(exc);
            } 
        },
        error: function (xhr, ajaxOptions, thrownError) {
              alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTareas(fecha){ 
    var anio = $j('#anho').val();
    var mes = $j('#mes').val();  
    fecha.setFullYear(anio);
    if(mes > 0) fecha.setMonth(mes-1);   
    var idProceso = $j('#procesos').val();
    var DATA_FEED_URL = "/fintra/controller?estado=Requisicion&accion=Eventos";
    $j('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        height: 675,
        axisFormat:'h(:mm) a',
        defaultDate: fecha,
        lang: 'es',
        editable: false,
        eventLimit: true,
        eventClick: function(event, jsEvent, view) {       
            $j("#eventInfo").html(event.title);  
            ventanaEventosDetalle();
        }
    });
    var events = {
        url: DATA_FEED_URL + "&evento=VERCRONO_PROCESO",
        type: 'POST',
        data: {
            idproceso: idProceso,
            anio: anio,
            mes: mes
        }
    };
    
    $j('#calendar').fullCalendar('removeEventSource', events);
    if($j('#calendar').fullCalendar('clientEvents') == ""){
        $j('#calendar').fullCalendar('addEventSource', events);  
    }     
    $j('#calendar').fullCalendar('rerenderEvents'); 
    $j('#calendar').fullCalendar('gotoDate', fecha);  
    $j('#calendar').fullCalendar('changeView','month');
   
}

function getUsuariosProceso() {
    var elemento= $j("#usuarios");
    $j.ajax({
        url: '/fintra/controller?estado=Prioridades&accion=Requisiciones',
        datatype: 'json',
        type: 'get',
        data: {opcion: 3, idProceso: $('#procesos').val()},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    alert(json.error);
                } else {
                    elemento.empty();
                    for (var llave in json) {
                        elemento.append('<option value="'+llave+'">'+json[llave]+'</option>');
                    }
                }
            } catch (exc) {
                console.error(exc);
            } 
        },
        error: function (xhr, ajaxOptions, thrownError) {
              alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarActividadesUsuario(fechaActual, anioActual, mesActual){ 
    
    var fecha = new Date();
    var anio = $j('#anho').val();
    var mes = $j('#mes').val();  
   
    fecha.setFullYear(anio);
    if(mes > 0) fecha.setMonth(mes-1);   
   

    var d = new Date(anio, mes, 0);
    var lastDayOfMonth = d.getDate();
    
    (parseInt(anioActual) !== parseInt(anio) || parseInt(mesActual) !== parseInt(mes)) ? fecha.setDate(lastDayOfMonth) : fecha.setDate(fechaActual.getDate()) ;  
  
    var idProceso = $j('#procesos').val();
    var idUsuario = $j('#usuarios').val();
   
    var DATA_FEED_URL = "/fintra/controller?estado=Requisicion&accion=Eventos";
    $j('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'agendaWeek, agendaDay'
        },
        height: 675,
        axisFormat:'h(:mm) a',
        defaultDate: fecha,
        defaultView: 'agendaDay',
        lang: 'es',
        editable: false,
        hiddenDays: [ 0 ],
        eventLimit: true,
        eventLimitText: 'm�s',
        eventClick: function(event, jsEvent, view) {
            //set the values and open the modal 
            $j("#eventInfo").html(event.title);  
            ventanaEventosDetalle();
        }
    });
  
    var events = {
        url: DATA_FEED_URL + "&evento=VER_ACTIVIDADES_USUARIO",
        type: 'POST',
        async: false,
        data: {
            idproceso: idProceso,
            idusuario: idUsuario,
            anio: anio,
            mes: mes
        }
    };
    
    $j('#calendar').fullCalendar('removeEventSource', events);
    if($j('#calendar').fullCalendar('clientEvents') == ""){
        $j('#calendar').fullCalendar('addEventSource', events);  
    }     
    $j('#calendar').fullCalendar('rerenderEvents'); 
    $j('#calendar').fullCalendar('gotoDate', fecha);  
    $j('#calendar').fullCalendar('changeView','agendaDay');
   
}

function ventanaEventosDetalle() {
    
    $("#eventContent").dialog({
        width: 450,
        height: 250,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: true, 
        buttons: {          
            "Salir": function () {          
		$(this).dialog("close");               
            }
        }
    });
}

/*function cargarPagina(pagina) {
    $j.ajax({
        type: 'POST',
        url: "" + pagina,
        dataType: 'html',
        success: function(html) {       
          $j("#calendar").html(html);       
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function verCronograma() {
   
    $j("#dialogCronograma").dialog({
        width:1000,
        height:730,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: true, 
        buttons: {          
            "Salir": function () {    
              $j(this).dialog("close");  
            }
        }
    });
}*/
