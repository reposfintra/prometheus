/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


 $(document).ready(function() {
     
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $("#menu-pagina").delegate(".menu-item", "click", function() {
        cargarPagina($(this).attr("ruta-pagina"));
    }); 
     
    $('#loginEds').click(function(){
        iniciarSesionEDS();
    });

 });



 function iniciarSesionEDS() {
    var idusuario = $("#idusuario").val();
    var passwd = $("#password").val();
    if(idusuario ==''){
       mensajesDelSistema("INGRESE EL NOMBRE DE USUARIO", '250', '165');
    }else if(passwd==''){
       mensajesDelSistema("INGRESE LA CONTRASEŅA", '250', '165');
    }else{
        $.ajax({
            type: "POST",
            crossDomain: true,
            dataType: "json",
            contentType: "application/x-www-form-urlencoded",
            data: { 'data':loginformToJSON() },
            async: false,
            url: 'http://prometheus.fintra.co:8094/ApiServerEds/webresources/login/eds', 
            success: function(jsonData) {
                    if(jsonData.success===true ){
                        var json = jsonData.info; //JSON.parse(jsonData.info);                     
                        localStorage.setItem("token", json.token);      
                        ventanaConfigIniProd();
                        listarConfigProd('tbl_config_ini_prices',"S");
                    } else {
                         mensajesDelSistema(jsonData.message.error, '320', '165');
                    }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

  function loginformToJSON() {
    return JSON.stringify({
        "usuario": $("#idusuario").val().toUpperCase(),
        "clave": $("#password").val()
    });
} 

function obtenerMenu(elemento) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Ventas&accion=Eds",
        dataType: 'json',
        data: {
            opcion: 0
        },
        success: function(json) {

                var menu = new Menu("Opciones", json);               
                $(elemento).html(menu.generarMenu());              
                $('#div_inicioSesion').replaceWith($('#wrapper'));
                $('#wrapper').fadeIn();

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
                
}


function cargarPagina(pagina) {
    $.ajax({
        type: 'POST',
        url: "" + pagina,
        dataType: 'html',
        success: function(html) {
            $("#container").html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
function cargarPaginaParametros(pagina, parametros) {
    $.ajax({
        type: 'POST',
        url: "" + pagina,
        data: parametros,
        dataType: 'html',
        success: function(html) {
            $("#container").html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
