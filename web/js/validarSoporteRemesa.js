
      function enabledBoton (idform){
          var form  = eval('form' + idform);       
          var boton = document.getElementById('boton'+ idform);
          with (form){
             if (Tipo.value=='LOGICA' && FechaEnvioLogico.value!='' && FechaRecibidoLogico.value!='')
                 boton.style.display='none';
             else if  (Tipo.value=='FISICA' && FechaEnvioFisico.value!='' && FechaRecibidoFisico.value!='')
                 boton.style.display='none';
             else if  (Tipo.value=='TODAS' && FechaEnvioLogico.value!='' && FechaRecibidoLogico.value!='' && FechaEnvioFisico.value!='' && FechaRecibidoFisico.value!='')
                 boton.style.display='none';                 
          }
       }
    
       function initFecha (idform, fecha, campo, calendario){
           var field = null; 
           try{   field = eval ('form'+ idform +'.'+ campo );  } 
           catch (error) { }
           
           if (field){
             if (fecha!='0099-01-01 00:00'){  
                field.value = fecha;
                var cal = document.getElementById(calendario+idform);
                if (cal)
                   cal.style.visibility='hidden';
                   //cal.style.display='none';                
                              
                // si se puede editar el objeto
                try{   
                  var edit = eval ('form'+ idform +'.edit'+ calendario );  
                  edit.value = 'false';
                } catch (error) { }    
            

             }
             else
                field.onkeyup = new Function (' _keys(this); ');
           }
       }
       
       function _onsubmit (form){
       
           //var msg ='';
           //for (var i=0;i<form.elements.length; i++)
           //   msg += '\n' + form.elements[i].name +' : '+ form.elements[i].value ;
           //alert (msg);
         
          with (form){
             if ( AgenciaEnvio.value=='NADA'){
                  alert ('Debe poder asignar una agencia de envio para poder continuar');
                  return false;             
             }
          
             
             if (Tipo.value == 'LOGICA'){
                
                 if (editFEL.value=='true'){
                    if (FechaEnvioLogico.value==''){
                        alert ('Para poder modificar indique fecha de envio logico');
                        return false;
                    }
                    if(FechaRecibido.value=='') return true;
                 }
                 if (editFRL.value=='true'){
                    if (FechaRecibidoLogico.value==''){
                        alert ('Defina la fecha recibido logico para poder hacer la modificacion.');
                        return false;
                    }
                    if (compare(FechaEnvioLogico.value, FechaRecibidoLogico.value)!=-1){
                       alert ('La fecha recibido logico debe ser mayor que la fecha de envio logico.');
                       return false;
                    }
                 }                 
             
             } // end if LOGICA
             
             else if (Tipo.value == 'FISICA') {
             
                 if (editFEF.value=='true'){
                    if (FechaEnvioFisico.value==''){
                        alert ('Para poder modificar indique fecha de envio fisico');
                        return false;
                    }
                    if(FechaRecibidoFisico.value=='') return true;
                 }
                 if (editFRF.value=='true'){
                    if (FechaRecibidoFisico.value==''){
                        alert ('Defina la fecha recibido fisico para poder hacer la modificacion.');
                        return false;
                    }
                    if (compare(FechaEnvioFisico.value, FechaRecibidoFisico.value)!=-1){
                       alert ('La fecha recibido fisico debe ser mayor que la fecha de envio fisico.');
                       return false;
                    }
                 }
                 
             } // end else if FISICO

             else if ( Tipo.value == 'TODAS' ){
             
                  var sw = true;
                  if ( ((editFEF.value=='true' && FechaEnvioFisico.value   =='') || editFEF.value=='false') && 
                       ((editFRF.value=='true' && FechaRecibidoFisico.value=='') || editFRF.value=='false') && 
                       ((editFEL.value=='true' && FechaEnvioLogico.value   =='') || editFEL.value=='false') && 
                       ((editFRL.value=='true' && FechaRecibidoLogico.value=='') || editFRL.value=='false')) {
                       alert ('Debe indicar algun cambio en las fechas para poder continuar');
                       return false;
                  }
                  
                  
                  if (editFRL.value=='true' && FechaRecibidoLogico.value!='' && FechaEnvioLogico.value==''){
                          alert ('Debe defina la fecha envio logico para poder hacer la modificacion.');
                          return false;                  
                  }                  
                  else if ( (editFEL.value=='true'  && FechaEnvioLogico!='' && editFRL.value=='true' && FechaRecibidoLogico.value!='') || 
                       (editFEL.value=='false' && editFRL.value=='true' && FechaRecibidoLogico.value!='') ) {
                       if (compare(FechaEnvioLogico.value, FechaRecibidoLogico.value)!=-1){
                          alert ('La fecha recibido logico debe ser mayor que la fecha de envio logico.');
                          return false;
                       }
                  }  
                  
                  if (editFRF.value=='true' && FechaRecibidoFisico.value!='' && FechaEnvioFisico.value==''){
                          alert ('Debe defina la fecha envio fisico para poder hacer la modificacion.');
                          return false;                  
                  } 
                  else if ( (editFEF.value=='true'  && FechaEnvioFisico!='' && editFRF.value=='true' && FechaRecibidoFisico.value!='') || 
                       (editFEF.value=='false' && editFRF.value=='true' && FechaRecibidoFisico.value!='') ) {
                       if (compare(FechaEnvioFisico.value, FechaRecibidoFisico.value)!=-1){
                          alert ('La fecha recibido fisico debe ser mayor que la fecha de envio fisico.');
                          return false;
                       }
                  }  
             } // end if TODAS

          }//end WITH
          return true;
       }
       
       
       function compare (f1, f2){
         var fi = parseFloat( f1.replace(/:|\/|-| /g,'') );
         var ff = parseFloat( f2.replace(/:|\/|-| /g,'') );
         if   (fi>ff) return 1;
         else if (fi<ff) return -1;
         else return 0;
       }

       function validarFormuarioFiltro(form){
           var remesa = form.Remesa.value;
           var fecIni = form.fecIni.value;
           var fecFin = form.fecFin.value;


           if ((fecIni=='' && fecFin=='' && remesa!='') || (fecIni!='' && fecFin!='') ){
              return true;
           }

           if (fecIni=='' && fecFin=='' && remesa==''){
              alert('Debe indicar la remesa o la fecha de cumplido para poder continuar');
              return false;
           }

           if ((fecIni=='' && fecFin!='') || (fecIni!='' && fecFin=='')){
              alert('Debe indicar ambas fechas si quiere que aplique este filtro, sino por favor quitelo');
              return false;
           }

           if (compare(fecIni,fecFin)!=-1 ){
              alert('La fecha inicio debe ser menor a la fecha final. rectifiquelo para poder continuar');
              return false;
           }
           return true;
 
       }

