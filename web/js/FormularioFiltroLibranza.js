// Peticiones servidor
function init() {
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=FiltroLibranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 5},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema("info ocupaciones", "Error obteniendo lista de ocupaciones", "270")
                    console.log(json.error);
                } else {
                    var ocupacion = $('#ocups');
                    ocupacion.empty();
                    ocupacion.append('<option value="">...</option>');
                    for (var i in json.rows) {
                        ocupacion.append('<option value="' + json.rows[i].id + '">' + json.rows[i].valor + '</option>');
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=FiltroLibranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 8},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema("info Estado civil", "Error obteniendo lista de estado civil", "270")
                    console.log(json.error);
                } else {
                    var ocupacion = $('#est_civil');
                    ocupacion.empty();
                    ocupacion.append('<option value="">...</option>');
                    for (var i in json.rows) {
                        ocupacion.append('<option value="' + json.rows[i].id + '">' + json.rows[i].valor + '</option>');
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    document.getElementById("btn_hdc").addEventListener("click", verHdc);
}

function cal_edad(){
    var fecha = document.getElementById("fec_nac").value;
    if(fecha === "0099-01-01") {
        fecha = "";
        document.getElementById('edad').value = "";
        document.getElementById('extraprima').value = "0";
    }
    if(fecha !== "") {
        fecha = new Date(fecha);
        var hoy = new Date();
        document.getElementById('edad').value = parseInt((hoy - fecha) / 365 / 24 / 60 / 60 / 1000);
    
        cal_extraprima();
    }
}

function cal_ingresos() {
    if ($("#salario").val() === '') {
        document.getElementById("otro_ingr").value = "";
        document.getElementById("tot_ingr").value = "";
        return;
    }
    var salario = formato($("#salario").val()).numero
      , oIngr = formato($("#otro_ingr").val()).numero;
    
    document.getElementById("tot_ingr").value = formato(salario + oIngr).moneda;
}

function cal_descuentos() {
    var ocups, salario;
    if ($("#ocups").val() === '' || $("#salario").val() === '') {
        document.getElementById("desc_ley").value = "";
        document.getElementById("tot_desc").value = "";
        return;
    }
    try {
        ocups = $("#ocups").val();
        salario = formato($("#salario").val()).numero;
    } catch (exception) {
        document.getElementById("desc_ley").value = "";
        document.getElementById("tot_desc").value = "";
        return;
    }
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=FiltroLibranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 6, ocupacion:ocups, salario: salario},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //mensajesDelSistema("calculo descuento", "Se interrumpio el calculo del descuento", "270");
                    document.getElementById("desc_ley").value = formato("0").moneda;
                    document.getElementById("tot_desc").value = formato("0").moneda;
                    console.log(json.error);
                } else {
                    document.getElementById("desc_ley").value = formato(json.porc_desc).moneda;
                    document.getElementById("tot_desc").value = formato(json.total).moneda;
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function cal_extraprima() {
    var ocups, edad;
    if ($("#ocups").val() === '' || $("#edad").val() === '') {
        document.getElementById("extraprima").value = "";
        return;
    }
    try {
        ocups = $("#ocups").val();
        edad = parseInt($("#edad").val());
    } catch (exception) {
        document.getElementById("extraprima").value = "";
        return;
    }
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=FiltroLibranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 7, ocupacion:ocups, edad: edad},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //mensajesDelSistema("calculo Extraprima", "Se interrumpio el calculo de la extraprima", "270");
                    document.getElementById("extraprima").value = "0";
                    console.log(json.error);
                } else {
                    document.getElementById("extraprima").value = formato(json.extraprima).moneda;
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });      
}

// Calculos locales
function capacidad_pago() {
    var salario, desc, colchon, porcentaje; 
    
    salario = formato($("#salario").val()).numero;
    desc = formato($("#tot_desc").val()).numero;
    colchon = formato($("#colchon").val()).numero;
    porcentaje = formato($("#porc_desc").val()).porcentaje;       // porcentaje de descuento 
    
    console.log(salario+' '+desc+' '+colchon+' '+porcentaje); 
    document.getElementById("vlr_cons").value = formato((salario - desc)*porcentaje - colchon).moneda;
}

function vlrCuota(plazo) {
    plazo = formato(plazo).numero;
    var valor = formato($("#vlr_sol").val()).numero;
    var tasa_anual = formato($("#tasa_anual").val()).porcentaje;
    var extra = formato($("#extraprima").val()).porcentaje;
    var per_gracia = formato($("#per_gracia").val()).numero;
    var fac_seg = formato($("#fac_seg").val()).numero;
    var v_cuota = 0, v_seg = 0, v_ext = 0
      , tasa = Math.pow((1+tasa_anual),(1/12))-1;
    /*
    console.log(
            {valor:valor,plazo:plazo,tasa_anual:tasa_anual,tasa:tasa
            ,extra:extra,per_gracia:per_gracia,fac_seg:fac_seg});
    */
    v_cuota = (valor+(valor*tasa*(per_gracia+1))+(valor*fac_seg*(per_gracia+1)))*(tasa/(1-Math.pow((1+tasa),-plazo)));
    v_seg = valor * fac_seg;
    v_ext = v_seg * extra;
    
    return {  cuota: v_cuota.toFixed(0)
            , seguro: v_seg.toFixed(0)
            , extraprima: v_ext.toFixed(0)
            , total: parseFloat(v_cuota+v_seg+v_ext).toFixed(0)
           };
}

function resumenCuota(plazo) {
    //Este metodo llena El resumen
    if(plazo) {
        document.getElementById("plazo").value = formato(plazo).numero;

        var vlr = vlrCuota(plazo);

        document.getElementById("valor").value = formato(document.getElementById("vlr_sol").value).moneda;
        document.getElementById("vlr_cuota").innerHTML = formato(vlr.cuota).moneda;
        document.getElementById("vlr_seguro").innerHTML = formato(vlr.seguro).moneda;
        document.getElementById("vlr_extra").innerHTML = formato(vlr.extraprima).moneda;
        document.getElementById("total_cuota").innerHTML = formato(vlr.total).moneda;
    } else {
        document.getElementById("plazo").value = "";
        document.getElementById("valor").value = "";
        document.getElementById("vlr_cuota").innerHTML = "";
        document.getElementById("vlr_seguro").innerHTML = "";
        document.getElementById("vlr_extra").innerHTML = "";
        document.getElementById("total_cuota").innerHTML = "";
    }
}

function showCuotas() {
    // este metodo debe llenar la tabla de cuotas
    // s� el valor monto consumible es menor a la cuota, no permitir que se seleccione la cuota.
    var lista = $('#lisCuotas');
    var valor = formato($("#vlr_sol").val()).numero;
    var tasa_anual = formato($("#tasa_anual").val()).porcentaje;
    var extra = formato($("#extraprima").val()).porcentaje;
    var per_gracia = formato($("#per_gracia").val()).numero;
    var fac_seg = formato($("#fac_seg").val()).numero;
    var vlr_consumible = formato($("#vlr_cons").val()).numero;
    var v_fijo = 0, v_seg = 0, v_ext = 0;
    
    lista.empty();
    if(!valor) {
        return;
    }
    
    var tasa = Math.pow((1+tasa_anual),(1/12))-1;
    var v_fijo = (valor+(valor*tasa*(per_gracia+1))+(valor*fac_seg*(per_gracia+1)));
    v_seg = valor * fac_seg;
    v_ext = v_seg * extra;

    var cuota1=0, habil1=true, cuota2=0, habil2=true, cuota3=0, habil3=true;
    
    for(i = 1; i <= 24; i++) {
        cuota1=(v_fijo*(tasa/(1-Math.pow((1+tasa),-i))))+v_seg+v_ext;
        cuota2=(v_fijo*(tasa/(1-Math.pow((1+tasa),-(i+24)))))+v_seg+v_ext;
        cuota3=(v_fijo*(tasa/(1-Math.pow((1+tasa),-(i+48)))))+v_seg+v_ext;
        
        habil1 = (cuota1 <= vlr_consumible && i > 6); 
        habil2 = (cuota2 <= vlr_consumible);
        habil3 = (cuota3 <= vlr_consumible);
       
        lista.append("<tr>"
            +"<td "+(habil1?"onclick='resumenCuota("+i+")'":"style='color:red;'")+">"+i+"</td>"
            +"<td "+(habil1?"onclick='resumenCuota("+i+")'":"style='color:red;'")+">$ "
                   +formato(cuota1.toFixed(0)).moneda+"</td>"
            +"<td>&nbsp;</td>"
            +"<td "+(habil2?"onclick='resumenCuota("+(i+24)+")'":"style='color:red;'")+">"+(i+24)+"</td>"
            +"<td "+(habil2?"onclick='resumenCuota("+(i+24)+")'":"style='color:red;'")+">$ "
                   +formato(cuota2.toFixed(0)).moneda+"</td>"
            +"<td>&nbsp;</td>"
            +"<td "+(habil3?"onclick='resumenCuota("+(i+48)+")'":"style='color:red;'")+">"+(i+48)+"</td>"
            +"<td "+(habil3?"onclick='resumenCuota("+(i+48)+")'":"style='color:red;'")+">$ "
                   +formato(cuota3.toFixed(0)).moneda+"</td>"    
            +"</tr>");
    }
}

function buscarPersona(ident) {
    if(!ident) {
        ident = $('#id_sol').val();
    }
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=FiltroLibranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 1, identificacion: ident},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //mensajesDelSistema("Buscar Persona", "No existe Cliente con esta identificacion", "270")
                    console.log(json.error);
                } else {
                    $('#id_filtro').val(json.id_filtro_busqueda);
                    $('#fec_nac').val(json.fecha_nacimiento);
                    $('#est_civil').val(json.estado_civil);
                    $('#pr_apel').val(json.primer_apellido);
                    $('#sg_apel').val(json.segundo_apellido);
                    $('#pr_nom').val(json.primer_nombre);
                    $('#sg_nom').val(json.segundo_nombre);
                    $('#tel').val(json.telefono);
                    $('#cel').val(json.celular);
                    
                    $('#ocups').val(json.id_ocupacion_laboral);
                    $('#fec_ocu').val(json.fecha_ocup_laboral);
                    get_convenios();
                    $('#convenios').val(json.id_configuracion_libranza);
                    get_empresas_pagaduria();
                    $('#empresas_pagaduria').val(json.id_empresa_pagaduria);
                    info_convenio();
                    $('#extraprima').val(formato(json.extraprima).moneda);
                    $('#fac_seg').val(formato(json.factor_seguro).moneda);
                    $('#salario').val(formato(json.salario).moneda);
                    $('#desc_ley').val(formato(json.descuento_ley).moneda);
                    $('#otro_ingr').val(formato(json.otros_ingresos).moneda);
                    $('#vlr_sol').val(formato(json.valor_solicitado).moneda);
                    $('#valor').val(formato(json.valor_solicitado).moneda);
                    $('#plazo').val(json.plazo);
                    
                    if(parseFloat(json.salario)>0){
                        cal_descuentos();
                        cal_ingresos();
                        capacidad_pago();
                    }
         
                    if(parseInt(json.plazo)>0){
                        showCuotas();
                        resumenCuota(json.plazo);
                    }
                    $('#viable').val(json.viable);
                    
                    cal_edad();
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function get_convenios() {
    var ocupacion = $('#ocups').val();
    var conv = $('#convenios');
    if (ocupacion === '') {
        conv.empty();
        return;
    }
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=FiltroLibranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 4, ocuLab:ocupacion},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema("info Convenios", "Error obteniendo lista de convenios", "270")
                    console.log(json.error);
                } else {
                    conv.empty();
                    conv.append('<option value="">...</option>');
                    for (var i in json.rows) {
                        conv.append('<option value="' + json.rows[i].id + '">' + json.rows[i].valor + '</option>');
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function info_convenio() {
    var id_cnv = $('#convenios').val();
    if (!id_cnv) {
        $('#tasa_anual').val("");
        $('#fac_seg').val("");
        $('#per_gracia').val("");
        $('#colchon').val("");
        $('#porc_desc').val("");
    }
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=FiltroLibranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 2, convenio: id_cnv},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema("info convenio", "Error obteniendo informacion del convenio", "270");
                    $('#tasa_anual').val("");
                    $('#fac_seg').val("");
                    $('#per_gracia').val("");
                    $('#colchon').val("");
                    $('#porc_desc').val("");
                    console.log(json.error);
                } else {
                    $('#tasa_anual').val(formato(json.tasa).numero);
                    $('#fac_seg').val(formato(json.fac_seg).numero);
                    $('#per_gracia').val(formato(json.per_gracia).numero);
                    $('#colchon').val(formato(json.colchon).moneda);
                    $('#porc_desc').val(formato(json.porc_desc).numero);
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function guardar_filtro(nuevo) {  
    var jsonDocs = [];
    $('input[name="chk_docs_req"]:checked').each(function () {
        jsonDocs.push(parseInt(this.value));       
    });
    console.log(JSON.stringify(jsonDocs));
    var accion;
    if(nuevo) {
        accion="INSERT_FILTRO";
    } else {
        accion="UPDATE_FILTRO";
    }
    var formulario = {
        accion:accion,
        id_filtro:$('#id_filtro').val(),
        identificacion: $('#id_sol').val(),
        fec_nac:$('#fec_nac').val(),
        est_civil:$('#est_civil').val(),
        pr_apel:$('#pr_apel').val(),
        sg_apel:$('#sg_apel').val(),
        pr_nom:$('#pr_nom').val(),
        sg_nom:$('#sg_nom').val(),
        tel:$('#tel').val(),
        cel:$('#cel').val(),
        id_ocupacion:$('#ocups').val(),
        fec_ocupa:$('#fec_ocu').val(),
        id_config:$('#convenios').val(),
        id_empresa_pagaduria:$('#empresas_pagaduria').val(),
        tasa:formato($('#tasa_anual').val()).numero,
        per_gracia:formato($('#per_gracia').val()).numero,
        salario:formato($('#salario').val()).numero,
        extraprima:formato($('#extraprima').val()).numero,
        otro_ingr:formato($('#otro_ingr').val()).numero,
        desc_ley:formato($('#desc_ley').val()).numero,
        fac_seg:formato($('#fac_seg').val()).numero,
        plazo:$('#plazo').val(),
        vlr_cuota:formato($('#total_cuota').html()).numero,
        valor_credito:formato($('#valor').val()).numero,
        viable:$('#viable').val()  
        //vlr_seguro:$('#vlr_seguro').html(),
        //vlr_extra:$('#vlr_extra').html(),
        //total_cuota:$('#total_cuota').html()
    };
    console.log(formulario);
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=FiltroLibranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 3, form: JSON.stringify(formulario), listadodocs:jsonDocs},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema("Guardar informaci�n", "Error Guardando informacion", "270");
                    console.log(json.error);
                } else {
                    $('#id_filtro').val(json.solicitud.id_filtro);
                    console.log("id_filtro: "+json.solicitud.id_filtro);
                    console.log("num_solicitud: "+json.solicitud.num_solicitud);
                    console.log("HDC respuesta: " + json.hdc.info.data.codigo_respuesta);
                    
                    if (json.solicitud.id_filtro && json.hdc.success && json.hdc.info.data.codigo_respuesta === "OK") {
                        mensajesDelSistema("Guardar informaci�n", "Se guard� la presolicitud y consulta a Datacr�dito correctamente.", "270");
                    } else if (json.solicitud.id_filtro && json.hdc.success && json.hdc.info.data.codigo_respuesta !== "OK") {
                        mensajesDelSistema("Guardar informaci�n", "Se guard� la presolicitud pero hubo un error al consultar a Datacr�dito. Error: " + json.hdc.info.data.codigo_respuesta, "270");
                    } else {
                        mensajesDelSistema("Guardar informaci�n", json.solicitud.mensaje, "270");
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function confirmacionGuardar() {
    
    /*
    alert("id_filtro: "+$('#id_filtro').val());
    alert("pr_apel: "+$('#pr_apel').val());
    alert("pr_nom: "+$('#pr_nom').val());
    alert("cel: "+$('#cel').val());
    alert("plazo: "+$('#plazo').val());
    alert("valor: "+$('#valor').val());
     */
          
    //$('#id_filtro').val()==='' ||
    if ($('#id_sol').val()==='' || $('#fec_nac').val()==='' || $('#est_civil').val()==='' || $('#pr_apel').val()==='' || $('#pr_nom').val()==='' || $('#cel').val()==='' || $('#ocups').val()==='' ||  $('#fec_ocu').val()==='' 
       ||  $('#fec_ocu').val()==='0099-01-01' ||  $('#convenios').val()==='' ||  $('#empresas_pagaduria').val()==='' ||  $('#fac_seg').val()==='' ||  $('#salario').val()==='' ||  $('#vlr_sol').val()===''   || $('#plazo').val()===''  || $('#valor').val()==='' ) {
        mensajesDelSistema("guardar info", "Rellene toda la informacion requerida", "270");
        return;
    }   
    var msg = "�Desea actualizar o crear una nueva presolicitud?";    
    var botones;
    
    $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> "+msg);
    
    botones = { "Nuevo": function () { $(this).dialog("close"); guardar_filtro(true); }
               ,"Actualizar": function () { $(this).dialog("close"); guardar_filtro(false); },
           "Salir": function () { $(this).dialog("close")}};
           
    $("#dialogo").dialog({
        open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
        width: "auto",
        height: "auto",
        show: "scale",
        hide: "scale",
        title: "Confirmacion",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: botones //crear bot�n de cerrar
    });
    
}

function mensajesDelSistema(titulo, msj, width) {
    
    var botones, title;
    $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    title = titulo;
    botones = { "Aceptar": function () {  $(this).dialog("close");  } };
    $("#dialogo").dialog({
        open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
        width: width,
        height: "auto",
        show: "scale",
        hide: "scale",
        title: title,
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: botones
    });
}

function formato(valor){
    var vaux;
    try {
        valor = valor.toString().replace(new RegExp(',','g'),'');
        var pattern =/\S+/;
        if (pattern.test( valor )) {
            pattern = /^-?(\d+\.?\d*)$|(\d*\.?\d+)$/;
            if(pattern.test( valor )) {
                vaux = parseFloat(valor);
            } else {
                vaux = 0;
            }
        } else {
            vaux = 0;
        }
    } catch(exc) {
        vaux = 0;
        console.log('error '+exc);
    }
    
    return {
        moneda:vaux.toString().replace(/(\d)(?=(\d{3})+(\.|$))/g, '$1,')
      , numero:vaux
      , porcentaje:vaux/100
    }
}

function refrescarVista(elemento) {
    switch (elemento) {
        case 'OCUPACION':
            cal_edad();
            cal_descuentos();
            get_convenios();
            break;
        case 'CONVENIO':
            get_empresas_pagaduria();
            info_convenio();
        case 'SEGURO':
        case 'SALARIO':
            cal_descuentos();
            cal_ingresos();
            capacidad_pago();
        case 'VALOR':
            showCuotas();
            resumenCuota();
            break;
        default:
            break;
    }
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function get_empresas_pagaduria() {
    var id_convenio = $('#convenios').val();
    var emp = $('#empresas_pagaduria');
    if (id_convenio === '') {
        emp.empty();
        return;
    }
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=FiltroLibranza',
        datatype: 'json',
        type: 'get',
        data: {opcion: 13, id_conf_libranza:id_convenio},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema("info Empresas", "Error obteniendo lista de empresas", "270")
                    console.log(json.error);
                } else {
                    emp.empty();
                    emp.append('<option value="">...</option>');
                    for (var i in json.rows) {
                        emp.append('<option value="' + json.rows[i].id + '">' + json.rows[i].valor + '</option>');
                    }
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function AbrirDivDocumentosRequeridos(){
    verDocsRequeridos();
    $("#dialogo_docs_requeridos").dialog({
        width: 'auto',
        height: 250,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'DOCUMENTOS REQUERIDOS',
        closeOnEscape: false,
        buttons: {            
            "Salir": function () {               
                $(this).dialog("close");
            }
        }
    });    
}


function verDocsRequeridos() {
    $.ajax({
        url: '/fintra/controller?estado=Formulario&accion=FiltroLibranza',
        datatype: 'json',
        type: 'post',
        data: {opcion: 14,
               id_filtro_libranza:($('#id_filtro').val())?$('#id_filtro').val():$('#id_filtro_busqueda').val()},
        async: false,
        success: function (json) {
            try {
                if (json.error) {                 
                    //mensajesDelSistema("Buscar Persona", "No existe Cliente con esta identificacion", "270")
                    console.log(json.error);
                } else {
                    console.log(json);
                    $('#dialogo_docs_requeridos').empty();
                    for (var i in json.rows) {
                        $('#dialogo_docs_requeridos').append('<input type="checkbox" name="chk_docs_req" value="' + json.rows[i].id + '"' + json.rows[i].estado + '/>' + json.rows[i].valor + '<br/>');        
                    }
                                 
                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function verHdc(){
    let cedula =  document.getElementById("id_sol").value;

    if (cedula !== undefined && cedula !== "") {
        let hdcWindow = window.open("controller?estado=Historia&accion=Credito&opcion=GENERAR_PDF&vista=5&tipoIdentificacion=1&identificacion=" + cedula, "hdcWindow");
    } else {
        mensajesDelSistema("Informaci�n","Debe ingresar el n�mero de c�dula");
    }
}