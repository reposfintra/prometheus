/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {   
    maximizarventana();
    cargarLineasNegocio();
    $('#listarSolicitudes').click(function(){      
        var fechainicio = $("#fecha_ini_contrato").val();
        var fechafin = $("#fecha_fin_contrato").val();
        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
            if (fechainicio > fechafin) {
               mensajesDelSistema("La fecha final no puede ser inferior a la inicial", '350', '150', false);
            } else {
               cargarSolicitudesAsignadasBroker();
            }                
        } else {
            mensajesDelSistema("Por favor revise el rango de fecha ingresado", '350', '150', false);
        } 
    });   
    
    $("#clearSolicitudes").click(function () {
        DeshabilitarFiltroSearchSol(false);
        resetSearchValuesSol();
        $("#tabla_solicitudes_broker").jqGrid('GridUnload');      
    });
    
    $("#aseguradora, #beneficiario, #otro_si").change(function() {
        var op = $(this).find("option:selected").val();
        if($('#aseguradora').val()!=='0' && $('#beneficiario').val()!=='0') { 
            var num_contrato = $("#tabla_solicitudes_broker").getRowData($('#id_solicitud').val()).num_contrato;  
            cargarOtrosCostosAseguradora(num_contrato,$('#aseguradora').val(),$('#beneficiario').val(),$('#otro_si').val());
            cargarCotizacionAseguradora($('#id_solicitud').val());         
        }else{        
            $('#valor_gastos').val('0');
            $('#valor_iva').val('0');
            $('#valor_total').val('0');
            $("#tabla_cotizacion_aseguradora").jqGrid("clearGridData", true);         
        }
    });
    
    $("#beneficiario").change(function () {  
        var num_contrato = $("#tabla_solicitudes_broker").getRowData($('#id_solicitud').val()).num_contrato;  
        var op = $(this).find("option:selected").val();
        cargarOtrosSiContrato(num_contrato,op);
    });
    
    $('.solo-numero').keyup(function () {
        this.value = numberConComas((this.value + '').replace(/[^0-9]/g, ''));
    });
    
    $('.solo-numeric').live('keypress', function (event) {
        return numbersonly(this, event);
    });   
    
    $('.solo-numeric').live('blur', function (event) {
        this.value = numberConComas(this.value);
    });  
    
    $('#valor_gastos').live('blur', function (event) {
        if($(this).val()==='') this.value = 0;
        calcularTotalesCotizacion();
    });   
    
          
    $("#fecha_ini_contrato").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    $("#fecha_fin_contrato").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });    
        
    var myDate = new Date();   
    $('#ui-datepicker-div').css('clip', 'auto');
    
    
});


function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarSolicitudesAsignadasBroker() {     
   
    $('#div_filtro_solicitudes').fadeIn();
    var grid_tbl_solicitudes_broker = jQuery("#tabla_solicitudes_broker");
     if ($("#gview_tabla_solicitudes_broker").length) {        
        refrescarGridSolicitudesAsignadasBroker();
     }else { 
        grid_tbl_solicitudes_broker.jqGrid({
            caption: "Listado de Solicitudes Asignadas a broker",
            url: "./controlleropav?estado=Minutas&accion=Contratacion",           	 
            datatype: "json",  
            height: '450',
            width: '1450',          
            colNames: ['No Solicitud',  'Id Cliente', 'Nombre', 'No Oferta', 'Nombre Proyecto', 'F. Creacion', 'Tipo Solicitud', 
                'F Contrato',  'Responsable', 'Interventor','IdTrazabilidad', 'Etapa','Id estado','Nombre estado', 'No Contrato',
                'Tipo Contrato', 'Id Broker', 'Broker', 'Enviado Broker', 'Enviado', 'Total Cotizaciones', 'Cot. Aceptada', 'Accion'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', width: 80, align: 'left', key: true},
                {name: 'id_cliente', index: 'id_cliente', width: 110, align: 'left', hidden:true},
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 250, align: 'left'},
                {name: 'num_os', index: 'num_os', width: 110, align: 'center', hidden:true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 160, align: 'left'},
                {name: 'creation_date', index: 'creation_date', width: 110, align: 'center'},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 110, align: 'left'},             
                {name: 'fecha_contrato', index: 'fecha_contrato', width: 110, align: 'center'},
                {name: 'Responsable', index: 'Responsable', width: 130, align: 'left'},
                {name: 'Interventor', index: 'Interventor', width: 130, align: 'left'},
                {name: 'id_trazabilidad', index: 'id_trazabilidad', width: 110, align: 'center', hidden:true},
                {name: 'etapa_trazabilidad', index: 'etapa_trazabilidad', width: 110, align: 'left', hidden:true},
                {name: 'id_estado', index: 'id_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'nombre_estado', index: 'nombre_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},                
                {name: 'num_contrato', index: 'num_contrato', width: 110, align: 'center', hidden: true},
                {name: 'tipo_contrato', index: 'tipo_contrato', width: 110, align: 'center', hidden: true},
                {name: 'id_broker', index: 'id_broker', width: 160, align: 'left', hidden:true},
                {name: 'broker', index: 'broker', width: 160, align: 'left',hidden:true},
                {name: 'cotizado_broker', index: 'cotizado_broker', width: '80px', align: 'center', hidden: true},
                {name: 'enviado', index: 'enviado', width: '80px', align: 'center', fixed: true},
                {name: 'total_cotizaciones', index: 'total_cotizaciones', width: '80px', align: 'center', hidden: true},
                {name: 'cotizacion_aceptada', index: 'cotizacion_aceptada', width: '80px', align: 'center', hidden: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '130px', search: false}               
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_solicitudes_broker'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            subGrid: true,   
            subGridOptions: { "plusicon" : "ui-icon-triangle-1-e",
                      "minusicon" :"ui-icon-triangle-1-s",
                      "openicon" : "ui-icon-arrowreturn-1-e",
                      "reloadOnExpand" : true,
                      "selectOnExpand" : true },
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,  
            multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                    opcion: 28,
                    fecha_inicial:$('#fecha_ini_contrato').val(),
                    fecha_final:$('#fecha_fin_contrato').val(),
                    num_solicitud: $('#num_solicitud').val(),
                    nombre_cliente: $('#nombre_cliente').val(),
                    lineaNegocio: $("#linea_negocio").val(),
                    responsable: $("#responsable").val(),
                    trazabilidad: '4'
                }
            },   
            gridComplete: function () {
                var ids = grid_tbl_solicitudes_broker.jqGrid('getDataIDs');
                var fila;
                var tipo_contrato,num_contrato,pend_cot_broker,total_cotizaciones,cotizacion_aceptada;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tbl_solicitudes_broker.jqGrid("getLocalRow", cl);                
                    tipo_contrato = fila.tipo_contrato;
                    num_contrato = fila.num_contrato;
                    pend_cot_broker = pendienteCotizarBroker(num_contrato);
                    total_cotizaciones = fila.total_cotizaciones;
                    cotizacion_aceptada = fila.cotizacion_aceptada;
                    if (pend_cot_broker) {
                        be = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                    } else {
                        be = '<img src = "/fintra/images/flag_green.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                    }
                      
                    ca = '<img src = "/fintra/images/botones/iconos/obligatorio.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;" title ="Cotizaci�n aceptada" onclick = "">';
                    if(cotizacion_aceptada === 'S'){
                        grid_tbl_solicitudes_broker.jqGrid('setRowData', ids[i], {enviado: be+'    '+ca});
                    }else{
                         grid_tbl_solicitudes_broker.jqGrid('setRowData', ids[i], {enviado: be});
                    }
                  
                                   
                    broker = "<input style='height:26px;width:57px;' type='button' name='cotizar' id='cotizar'value='Cotizar'  width='19' height='19' title ='Abrir ventana de cotizaci�n precios aseguradora'  onclick=\"AbrirDivCotizacionAseguradora('" + cl + "');\">";
                    ce = "<img src='/fintra/images/botones/iconos/cambiar1.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='editar' id='editar'value='Cambiar'  width='19' height='19' title ='Enviar cotizaci�n'  onclick=\"mensajeConfirmAction('Esta seguro de enviar la cotizaci�n del proyecto seleccionado?','250','150',enviarCotizacionBroker,'" + num_contrato + "');\">";
                    if (pend_cot_broker) {
                        (total_cotizaciones>0)? grid_tbl_solicitudes_broker.jqGrid('setRowData', ids[i], {actions: broker+ '  '+ce}):grid_tbl_solicitudes_broker.jqGrid('setRowData', ids[i], {actions: broker});
                    }
                }
            },
            subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id = subgrid_id+"_t"; 
                var num_contrato = $("#tabla_solicitudes_broker").getRowData(row_id).num_contrato;              
                jQuery("#"+subgrid_id).html("<table id='"+subgrid_table_id+"'></table>");
                jQuery("#"+subgrid_table_id).jqGrid({
                    url: './controlleropav?estado=Minutas&accion=Contratacion', 
                    datatype: 'json',
                    colNames: ['Id','Id Aseguradora','Aseguradora','Id Beneficiario','Beneficiario', 'Secuencia', 'Otro Si', 'Valor Prima', 'Otros gastos', 'Valor IVA', 'Total', 'Cotiz. Broker','Aceptada'],
                    colModel: [
                        {name: 'id', index: 'id', sortable: true, width: 60, align: 'center', hidden:true, key:true},
                        {name: 'id_aseguradora', index: 'id_aseguradora', sortable: true, width: 60, align: 'center', hidden:true},
                        {name: 'nombre_seguro', index: 'nombre_seguro', sortable: true, align: 'left',  width: 335},
                        {name: 'id_beneficiario', index: 'id_beneficiario', sortable: true, width: 60, align: 'center', hidden:true, key:true},
                        {name: 'beneficiario', index: 'beneficiario', sortable: true, align: 'left',  width: 270},
                        {name: 'secuencia', index: 'secuencia', sortable: true, width: 60, align: 'center', hidden:true, key:true},
                        {name: 'otro_si', index: 'otro_si', sortable: true, align: 'left',  width: 115},
                        {name: 'valor_prima', index: 'valor_prima', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_otros_gastos', index: 'valor_otros_gastos', sortable: true, width: 120, align: 'right', search: false, sorttype: 'number',
                                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_iva', index: 'valor_iva', sortable: true, width: 120, align: 'right', search: false, sorttype: 'number',
                                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_total', index: 'valor_total', sortable: true, width: 130, align: 'right', search: false, sorttype: 'number',
                                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'cotiz_broker_aceptada', index: 'cotiz_broker_aceptada', width: '80px', align: 'center', hidden: true},
                        {name: 'cotizacion_aceptada', index: 'cotizacion_aceptada', width: '80px', align: 'center', fixed: true}
                    ],
                    rowNum:150,
                    height:'80%',
                    width:'90%',   
                    jsonReader: {
                        root: 'rows',
                        cell:'',
                        repeatitems: false,
                        id: '0'
                    },  
                    ajaxGridOptions: {
                        type: "POST",
                        async: false,
                        data: {
                            opcion: 32,
                            num_contrato: num_contrato,
                            cotizado_broker: 'N'
                        }
                    },   
                    gridComplete: function () {
                        var ids = jQuery("#"+subgrid_table_id).jqGrid('getDataIDs');
                        var fila;
                        var cotizacion_aceptada;
                        for (var i = 0; i < ids.length; i++) {
                            var cl = ids[i];
                            fila = jQuery("#"+subgrid_table_id).jqGrid("getLocalRow", cl);
                            cotizacion_aceptada = $("#"+subgrid_table_id).getRowData(cl).cotiz_broker_aceptada;                            
                            if (cotizacion_aceptada === 'N') {
                                be = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                            } else {
                                be = '<img src = "/fintra/images/flag_green.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                            }
                            jQuery("#"+subgrid_table_id).jqGrid('setRowData', ids[i], {cotizacion_aceptada: be});
                        }
                    },
                    ondblClickRow: function (rowid, iRow, iCol, e) {
                        var myGrid = jQuery("#tabla_solicitudes_broker"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                        filas = myGrid.jqGrid("getLocalRow", selRowIds);
                        var num_contrato = filas.num_contrato;  
                        var id_aseguradora = $("#"+subgrid_table_id).getRowData(rowid).id_aseguradora;                        
                        var nombre_aseguradora = $("#"+subgrid_table_id).getRowData(rowid).nombre_seguro; 
                        var id_beneficiario = $("#"+subgrid_table_id).getRowData(rowid).id_beneficiario; 
                        var beneficiario = $("#"+subgrid_table_id).getRowData(rowid).beneficiario; 
                        var secuencia = $("#"+subgrid_table_id).getRowData(rowid).secuencia; 
                        var otro_si = $("#"+subgrid_table_id).getRowData(rowid).otro_si; 
                        var valor_otros_gastos = $("#" + subgrid_table_id).getRowData(rowid).valor_otros_gastos;
                        var valor_iva = $("#" + subgrid_table_id).getRowData(rowid).valor_iva;
                        var valor_total = $("#" + subgrid_table_id).getRowData(rowid).valor_total; 
                        cargarDetalleAseguradora(num_contrato,id_aseguradora,id_beneficiario,secuencia);
                        $('#nombre_aseguradora').val(nombre_aseguradora);
                        $('#total_gastos').val(numberConComas(valor_otros_gastos));
                        $('#total_iva').val(numberConComas(valor_iva));
                        $('#total').val(numberConComas(valor_total));                            
                        AbrirDivDetalleAseguradora(beneficiario+' '+otro_si);

                    },
                    loadError: function(xhr, status, error) {
                        alert(error);
                    }
                });               
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_solicitudes_broker"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);              
                cargarGarantias(rowid);
                AbrirDivPolizasProyecto();
                

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_solicitudes_broker", {add: false, edit: false, del: false, search: false, refresh: false}, {});   
        jQuery("#tabla_solicitudes_broker").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: false,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
        });
       
    }
  }          


function refrescarGridSolicitudesAsignadasBroker(){     
    jQuery("#tabla_solicitudes_broker").setGridParam({
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 28,
                fecha_inicial: $('#fecha_ini_contrato').val(),
                fecha_final: $('#fecha_fin_contrato').val(),
                num_solicitud: $('#num_solicitud').val(),
                nombre_cliente: $('#nombre_cliente').val(),
                lineaNegocio: $("#linea_negocio").val(),
                responsable: $("#responsable").val(),
                trazabilidad: '4'
            }
        }       
    });
    
    jQuery('#tabla_solicitudes_broker').trigger("reloadGrid");
}

function AbrirDivPolizasProyecto() {
    $("#div_polizas_proyecto").dialog({
        width: 850,
        height: 430,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'P�lizas requeridas para el proyecto',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $(".ui-dialog-titlebar-close").hide();
}

function cargarGarantias(id_solicitud) {     
      
    var grid_tbl_garantias = jQuery("#tabla_garantias");
     if ($("#gview_tabla_garantias").length) {        
        refrescarGridGarantias(id_solicitud);
     }else { 
        grid_tbl_garantias.jqGrid({
            caption: "Garantias",
            url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",           	 
            datatype: "json",  
            height: '150',
            width: '750', 
            colNames: ['Id', 'Id Poliza', 'Poliza','Valor Base', '% Poliza', 'Valor Poliza', 'Vigencia'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden:true},
                {name: 'id_poliza', index: 'id_poliza', width: 80, align: 'left', hidden:true},
                {name: 'poliza', index: 'poliza', width: 260, align: 'left'},
                {name: 'valor_base', index: 'valor_base', sortable: true, width: 120, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},              
                {name: 'porcentaje_poliza', index: 'porcentaje_poliza', editable:true, width: 80, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                {name: 'valor_poliza', index: 'valor_poliza', sortable: true, width: 120, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'vigencia_poliza', index: 'vigencia_poliza', width: 80, align: 'center'}              
                
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_garantias'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pgtext: null,
            pgbuttons: false, 
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                    opcion: 8,                    
                    num_solicitud: id_solicitud
                }
            },   
            gridComplete: function() {             
                var colSumBase = jQuery("#tabla_garantias").jqGrid('getCol', 'valor_base', false, 'sum');
                var colSumPol = jQuery("#tabla_garantias").jqGrid('getCol', 'valor_poliza', false, 'sum');  
                jQuery("#tabla_garantias").jqGrid('footerData', 'set', {valor_base: colSumBase});
                jQuery("#tabla_garantias").jqGrid('footerData', 'set', {valor_poliza: colSumPol});                  
            },          
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_garantias", {add: false, edit: false, del: false, search: false, refresh: false}, {});  
    }
  }          


function refrescarGridGarantias(id_solicitud){     
    jQuery("#tabla_garantias").setGridParam({
        url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            async:false,
            data: { 
                  opcion: 8,                    
                  num_solicitud: id_solicitud
            }
        }       
    });
    
    jQuery('#tabla_garantias').trigger("reloadGrid");
}

function AbrirDivCotizacionAseguradora(id_solicitud){ 
        var num_contrato = $("#tabla_solicitudes_broker").getRowData(id_solicitud).num_contrato;
        $('#id_solicitud').val(id_solicitud);
        $('#valor_gastos').val('0');
        $('#valor_iva').val('0');
        $('#valor_total').val('0');
        cargarAseguradoras();  
        cargarBeneficiariosAsignados(num_contrato);
        cargarOtrosSiContrato(num_contrato,$('#beneficiario').val());
        cargarCotizacionAseguradora("");
        ventanaCotizacionAseguradoras(id_solicitud); 
}

 
  function cargarAseguradoras() {
    $('#aseguradora').html('');
    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        dataType: 'json',
        async:false,
        data: {
            opcion: 29
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#aseguradora').append("<option value='0'>Seleccione</option>");
               
                    for (var key in json) {              
                       $('#aseguradora').append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#aseguradora').append("<option value='0'>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarBeneficiariosAsignados(num_contrato) {    
    $('#beneficiario').html('');
    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        dataType: 'json',
        async:false,
        data: {
            opcion: 45,
            num_contrato:num_contrato
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                   
                    for (var key in json) {              
                       $('#beneficiario').append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#beneficiario').append("<option value='0'>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarOtrosSiContrato(num_contrato,id_beneficiario) {    
    $('#otro_si').html('');
    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        dataType: 'json',
        async:false,
        data: {
            opcion: 51,
            num_contrato:num_contrato,
            id_beneficiario:id_beneficiario
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#otro_si').append("<option value='0'>...</option>");
                    for (var key in json) {              
                       $('#otro_si').append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#otro_si').append("<option value='0'>...</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function pendienteCotizarBroker(num_contrato){
    var estado = false;
    $.ajax({
        url: './controlleropav?estado=Minutas&accion=Contratacion',
        datatype:'json',
        type:'post',
        async:false,
        data:{
            opcion: 46,
            num_contrato: num_contrato
        },          
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.respuesta === "SI") {               
                   estado = true;                  
                }
                
            }              
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }  
    });
    return estado;
}

function cargarCotizacionAseguradora(id_solicitud) {  
    $("#tabla_cotizacion_aseguradora").jqGrid('GridUnload');
    var grid_tbl_cotizacion_aseguradora = jQuery("#tabla_cotizacion_aseguradora");
     if ($("#gview_tabla_cotizacion_aseguradora").length) {        
        refrescarGridCotizacionAseguradora();
     }else { 
        grid_tbl_cotizacion_aseguradora.jqGrid({
            caption: "Cotizaci�n Aseguradora",
            url: "./controlleropav?estado=Minutas&accion=Contratacion",           	 
            datatype: "json",  
            height: '165',
            width: '810', 
            colNames: ['Id', 'Id Garant�a', 'Poliza','Valor Base', '% Base', 'Valor Asegurar', 'Vigencia', '% Prima', 'Valor Prima'],
            colModel: [
                {name: 'id_cot_aseguradora', index: 'id_cot_aseguradora', width: 80, align: 'left', hidden:true},
                {name: 'id_garantia', index: 'id_garantia', width: 80, align: 'left', hidden:true, key: true},
                {name: 'poliza', index: 'poliza', width: 260, align: 'left'},
                {name: 'valor_base', index: 'valor_base', sortable: true, width: 120, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},              
                {name: 'porcentaje_poliza', index: 'porcentaje_poliza', width: 80, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                {name: 'valor_poliza', index: 'valor_poliza', sortable: true, width: 120, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'vigencia_poliza', index: 'vigencia_poliza', width: 80, align: 'center', hidden:true},
                {name: 'porcentaje_aseguradora', index: 'porcentaje_aseguradora', editable:false, width: 80, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 3}},
                {name: 'valor_aseguradora', index: 'valor_aseguradora', sortable: true,  editable:true, width: 120, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
                
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pgtext: null,
            pgbuttons: false, 
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                    opcion: 30,                    
                    num_solicitud: id_solicitud,
                    id_aseguradora: $('#aseguradora').val(),
                    id_beneficiario: $('#beneficiario').val(),
                    secuencia: $('#otro_si').val()
                }
            },   
            gridComplete: function() {             
                var colSumBase = jQuery("#tabla_cotizacion_aseguradora").jqGrid('getCol', 'valor_base', false, 'sum');
                var colSumPol = jQuery("#tabla_cotizacion_aseguradora").jqGrid('getCol', 'valor_poliza', false, 'sum');
                var colSumAseg = jQuery("#tabla_cotizacion_aseguradora").jqGrid('getCol', 'valor_aseguradora', false, 'sum');  
                jQuery("#tabla_cotizacion_aseguradora").jqGrid('footerData', 'set', {valor_base: colSumBase});
                jQuery("#tabla_cotizacion_aseguradora").jqGrid('footerData', 'set', {valor_poliza: colSumPol}); 
                jQuery("#tabla_cotizacion_aseguradora").jqGrid('footerData', 'set', {valor_aseguradora: colSumAseg}); 
                calcularTotalesCotizacion();
            },  
            ondblClickRow: function (rowid, iRow, iCol, e) {
                editRowValoresAseguradora(rowid);       
                return;
            },            
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });  
    }
  }          


function refrescarGridCotizacionAseguradora(){     
    jQuery("#tabla_cotizacion_aseguradora").setGridParam({
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            async:false,
            data: { 
                  opcion: 30,                    
                  num_solicitud:  $('#id_solicitud').val(),
                  id_aseguradora: $('#aseguradora').val(),
                  id_beneficiario: $('#beneficiario').val(),
                  secuencia: $('#otro_si').val()
            }
        }       
    });
    
    jQuery('#tabla_cotizacion_aseguradora').trigger("reloadGrid");
}


function ventanaCotizacionAseguradoras(id_solicitud){
    
      var num_contrato = $("#tabla_solicitudes_broker").getRowData(id_solicitud).num_contrato;
      $("#dialogCotizacionAseguradora").dialog({
        width: 850,
        height: 485,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'COTIZACI�N DE ASEGURADORAS',
        closeOnEscape: false,
        buttons: {  
            "Guardar": function () {
                if ($('#aseguradora').val() === '0') {
                    mensajesDelSistema('Por favor, seleccione la aseguradora!!!', '250', '150');
                    return;
                }else if (jQuery("#tabla_cotizacion_aseguradora").jqGrid('getGridParam', 'records') === 0) {
                    mensajesDelSistema("No hay valores de aseguradora para ingresar!!", '250', '150');
                }else if ($('#valor_gastos').val()==='' || $('#valor_iva').val()===''){
                   mensajesDelSistema("Hay valores pendientes por ingresar. Favor, verifique!!", '250', '150');
                } else if (validateGridCotizAseguradora()) {
                     guardarCotizacionAseguradora(num_contrato);
                }   
            },
            "Salir": function () {
                mensajeConfirmAction("Recuerde guardar los cambios. Esta seguro que desea salir ?","230","150",cerrarDialogo,"dialogCotizacionAseguradora");
            }
        }
    });
}

 function guardarCotizacionAseguradora(num_contrato){   
        var filasCotizacion = jQuery("#tabla_cotizacion_aseguradora").jqGrid('getRowData');
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function(){
            $.ajax({
                    type: 'POST',
                    url: './controlleropav?estado=Minutas&accion=Contratacion',
                    dataType: 'json',
                    async: false,
                    data: {
                        opcion: 31,                  
                        num_contrato: num_contrato,
                        id_aseguradora: $('#aseguradora').val(),
                        id_beneficiario: $('#beneficiario').val(),
                        secuencia: $('#otro_si').val(),
                        listadoaseguradora: JSON.stringify({cotizaciones: filasCotizacion}),
                        otros_gastos: numberSinComas($('#valor_gastos').val()),
                        porc_iva:'19',
                        valor_iva: numberSinComas($('#valor_iva').val())
                    },
                    success: function (json) {

                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '250', '180');
                                return;
                            }
                            if (json.respuesta === "OK") {       
                                refrescarGridSolicitudesAsignadasBroker();
                                refrescarGridCotizacionAseguradora();      
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema("COTIZACION GUARDADA", '250', '150', true);                                             

                            }else{
                                  $("#dialogLoading").dialog('close');
                                  mensajesDelSistema("Lo sentimos no se pudo guardar la cotizaci�n!!", '250', '150');
                            }
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                                "Message: " + xhr.statusText + "\n" +
                                "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                });
        },500);
}


function editRowValoresAseguradora(rowId){
    jQuery("#tabla_cotizacion_aseguradora").jqGrid('editRow', rowId, true, function () {
        //$("input, select", e.target).focus();
    }, null, null, {}, function (rowid) {
        var valor_poliza = $("#tabla_cotizacion_aseguradora").getRowData(rowid).valor_poliza;
        var porc_aseguradora = $("#tabla_cotizacion_aseguradora").getRowData(rowid).porcentaje_aseguradora;
        var valor_asegurado = $("#tabla_cotizacion_aseguradora").getRowData(rowid).valor_aseguradora;        
        jQuery("#tabla_cotizacion_aseguradora").jqGrid('setCell', rowid, 'porcentaje_aseguradora', parseFloat((valor_asegurado / valor_poliza) * 100));
        var sumBase = jQuery("#tabla_cotizacion_aseguradora").jqGrid('getCol', 'valor_base', false, 'sum');
        var sumPoliza = jQuery("#tabla_cotizacion_aseguradora").jqGrid('getCol', 'valor_poliza', false, 'sum');
        var sumAseg = jQuery("#tabla_cotizacion_aseguradora").jqGrid('getCol', 'valor_aseguradora', false, 'sum');       
        jQuery("#tabla_cotizacion_aseguradora").jqGrid('footerData', 'set', {valor_base: sumBase});
        jQuery("#tabla_cotizacion_aseguradora").jqGrid('footerData', 'set', {valor_poliza: sumPoliza});
        jQuery("#tabla_cotizacion_aseguradora").jqGrid('footerData', 'set', {valor_aseguradora: sumAseg});
        calcularTotalesCotizacion();
    });
}

function cargarDetalleAseguradora(num_contrato, id_aseguradora, id_beneficiario, secuencia) {     
      
    var grid_tbl_garantias = jQuery("#tabla_detalle_aseguradora");
     if ($("#gview_tabla_detalle_aseguradora").length) {        
        refrescarGridDetalleAseguradora(num_contrato, id_aseguradora, id_beneficiario, secuencia);
     }else { 
        grid_tbl_garantias.jqGrid({
            caption: "Detalle Aseguradora",
            url: "./controlleropav?estado=Minutas&accion=Contratacion",           	 
            datatype: "json",  
            height: '150',
            width: '726', 
            colNames: ['Id', 'P�liza', '% Base', 'Valor Asegurar', '% Prima', 'Valor Prima'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden:true}, 
                {name: 'nombre_poliza', index: 'nombre_poliza', width: 260, align: 'left'},
                {name: 'porcentaje_poliza', index: 'porcentaje_poliza', editable:true, width: 90, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                {name: 'valor_poliza', index: 'valor_poliza', sortable: true, width: 130, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'porcentaje_aseguradora', index: 'porcentaje_aseguradora', editable:true, width: 90, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 3}},
                {name: 'valor_aseguradora', index: 'valor_aseguradora', sortable: true, width: 130, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}  
                
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pgtext: null,
            pgbuttons: false, 
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                    opcion: 33,                    
                    num_contrato: num_contrato,
                    id_aseguradora: id_aseguradora,
                    id_beneficiario: id_beneficiario,
                    secuencia: secuencia
                }
            },   
            gridComplete: function() {             
                var colSumPol = jQuery("#tabla_detalle_aseguradora").jqGrid('getCol', 'valor_poliza', false, 'sum'); 
                var colSumAseg = jQuery("#tabla_detalle_aseguradora").jqGrid('getCol', 'valor_aseguradora', false, 'sum');               
                jQuery("#tabla_detalle_aseguradora").jqGrid('footerData', 'set', {valor_poliza: colSumPol});       
                jQuery("#tabla_detalle_aseguradora").jqGrid('footerData', 'set', {valor_aseguradora: colSumAseg});               
            },          
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });  
    }
  }          


function refrescarGridDetalleAseguradora(num_contrato, id_aseguradora, id_beneficiario, secuencia){     
    jQuery("#tabla_detalle_aseguradora").setGridParam({
        url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            async:false,
            data:{
                    opcion: 33,                    
                    num_contrato: num_contrato,
                    id_aseguradora: id_aseguradora,
                    id_beneficiario: id_beneficiario,
                    secuencia:secuencia
            }
        }       
    });
    
    jQuery('#tabla_detalle_aseguradora').trigger("reloadGrid");
}


function AbrirDivDetalleAseguradora(tipo_beneficiario) {
    $("#div_detalle_cot_aseguradora").dialog({
        width: 790,
        height: 450,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'DETALLE COTIZACION - '+tipo_beneficiario,
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $(".ui-dialog-titlebar-close").hide();
}

function enviarCotizacionBroker(num_contrato){    
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        data: {
            opcion: 34,
            num_contrato: num_contrato
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarGridSolicitudesAsignadasBroker();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo enviar cotizaci�n!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

  function validateGridCotizAseguradora(){
    var estado = true;  
    if($('#otro_si').val()==='0'){
        var ids = jQuery("#tabla_cotizacion_aseguradora").jqGrid('getDataIDs');
        for (var i = 0; i < ids.length; i++) {       
        var valor_aseguradora = jQuery("#tabla_cotizacion_aseguradora").getRowData(ids[i]).valor_aseguradora; 
        if(parseFloat(valor_aseguradora)===0){
            estado= false;
            mensajesDelSistema('Informacion invalida en fila '+(i+1) +'. No hay valor de aseguradora', '250', '150');
        }
      }
    }
   
    return estado;
 }
 
function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function resetSearchValuesSol(){     
    $('#fecha_ini_contrato').val(''); 
    $('#fecha_fin_contrato').val(''); 
    $('#num_solicitud').val('');
    $('#nombre_cliente').val('');
    $('#linea_negocio').val('');
    $('#responsable').val('');
}


function DeshabilitarFiltroSearchSol(estado){          
    (estado) ? $('#fecha_ini_contrato').datepicker("destroy") : $('#fecha_ini_contrato').datepicker({dateFormat: 'yy-mm-dd'});
    (estado) ? $('#fecha_fin_contrato').datepicker("destroy") : $('#fecha_fin_contrato').datepicker({dateFormat: 'yy-mm-dd'});        
    $('#num_solicitud').attr({readonly: estado});
} 

function mensajeConfirmAction(msj, width, height, okAction, id) {
    
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");
            }
        }
    });

}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function cerrarDialogo(Nom_div){
    $("#"+Nom_div).dialog("destroy");    
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function calcularTotalesCotizacion(){   
    var colSumAseg = jQuery("#tabla_cotizacion_aseguradora").jqGrid('getCol', 'valor_aseguradora', false, 'sum');   
    var valor_gastos = parseFloat(numberSinComas($('#valor_gastos').val()));
    var valor_iva = parseFloat(((parseFloat(colSumAseg) + valor_gastos) * 19) / 100);
    var total = parseFloat(colSumAseg) + parseFloat(valor_iva) + parseFloat(valor_gastos);
    setTimeout(function () {
      $('#valor_iva').val(numberConComas(valor_iva));     
      $('#valor_total').val(numberConComas(total));
    }, 500);    
}

function cargarOtrosCostosAseguradora(num_contrato, id_aseguradora, id_beneficiario, secuencia) {
    var colSumAseg = jQuery("#tabla_cotizacion_aseguradora").jqGrid('getCol', 'valor_aseguradora', false, 'sum'); 
    $('#valor_gastos').val('0');
    $('#valor_iva').val('0');
    $('#valor_total').val('0');
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {opcion: 37,
               id_contrato: num_contrato,
               id_aseguradora:id_aseguradora,
               id_beneficiario:id_beneficiario,
               secuencia:secuencia
              },
        async: false,
        url: './controlleropav?estado=Minutas&accion=Contratacion',
        success: function (jsonData) {
            if (!isEmptyJSON(jsonData)) {
                var total = parseFloat(colSumAseg)+parseFloat(jsonData.valor_gastos)+parseFloat(jsonData.valor_iva);
                $('#valor_gastos').val(numberConComas(jsonData.valor_otros_gastos));
                $('#valor_iva').val(numberConComas(jsonData.valor_iva));
                $('#valor_total').val(numberConComas(total)); 
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}
