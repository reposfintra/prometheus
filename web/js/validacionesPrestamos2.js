  var BASEURL    = '';
  var CONTROLLER = '';

   function filtrarCombo(data, cmb){
       data = data.toLowerCase();
       for (i=0;i<cmb.length;i++){
          var cod = cmb[i].value;
          var txt = cmb[i].text.toLowerCase( );
          if( txt.indexOf(data)==0 ){
             cmb[i].selected = true;
             break;
          }
       }
    } 
	
	
    function encontrarProveedor (){
       var url = CONTROLLER + '?estado=Consulta&accion=Prestamos&Opcion=ConsultaProveedores';
       var win = open (url,'consultaProveedores',' top=100,left=100, width=700, height=500, scrollbar=no, status=yes  ');
       win.focus();
    } 
	
    function retornarProveedor (item) {
       var cprov = window.opener.document.getElementById ('Beneficiario');
       var nprov = window.opener.document.getElementById ('NombreBeneficiario');
       if (cprov) cprov.value = item.value;
       if (nprov) nprov.value = item.text;
       window.close();      
    }
	
     function retornar (combo){
	var item =  null;
	for (var i = 0; i<combo.length; i++){
	   if (combo[i].selected){
	     item = combo[i];
             retornarProveedor(item);
	     break;
	   }   
	}
      }
      


      function newWindow (url, name){
        var win = open (url,name,' top=5,left=5, width='+ (screen.width-25) +', height='+ (screen.height-100) +', scrollbar=no, status=yes, resizable  ');
        win.focus();
      }

      function viewAmortizaciones (index){
         var url = CONTROLLER + '?estado=Consulta&accion=Prestamos&Opcion=Amortizacion&Index=' + index;
         var win = newWindow(url,'consultaAmortizacion');
      }


       function validarListaSeleccion (form){
          for (var i=0;i<form.elements.length; i++){
              if (form.elements[i].type=='checkbox' && form.elements[i].checked && !form.elements[i].disabled){
                   return true;
              }
          }
          return false;
       }

       function descargar (tipo){
         var url = CONTROLLER + '?estado=Exportacion&accion=Prestamo&tipo=' + tipo;
         var win = open (url,'descarga',' top=100,left=50, width=800,height=300, scrollbar=no, status=yes, resizable ');
         win.focus();
       }


       function validarFormularioAmortizacion (form){
          var fechaAnterior = form.fechaED;
          with (form){
             if (cuotas.value==''){
                 alert ('Defina el numero de cuotas');
                 return false;
             }
             if (parseFloat(cuotas.value) <= parseFloat(cuotasPR.value) )  {
                 alert ('Numero no valido para redefinir el numero de cuotas, este debe ser mayor que el numero de cuotas procesadas');
                 return false;
             }
             for (var i=0;i<elements.length; i++){
                if (elements[i].type == 'text' && elements[i].name == 'fechas'){
                   if (fechaAnterior!=null ){
                      var fecha1 = parseFloat(fechaAnterior.value.replace(/-|:| /g,''));
                      var fecha2 = parseFloat(elements[i].value.replace  (/-|:| /g,''));
                      var cuota  = parseFloat(elements[i].id.replace('fec',''));
                      if (fecha2<fecha1){
                         alert('La fecha de la cuota '+ (cuota+1) +' ser mayor que la fecha de ' + (cuota==0?'entrega de dinero':'la cuota ' + cuota));
                         elements[i].focus();
                         return false;
                      }
                   }
                   fechaAnterior = elements[i];
                }
             }
          }
          if (confirm('Esta seguro de que desea modificar las fechas de los prestamos?')) {
             form.submit();
          }
       }
	
	function loadDatosEquipo( form ){		
		with( form ){
			if ( tipoPrestamo.value == 'EQ'){
				var datos = tipoEquipo.options[ tipoEquipo.selectedIndex ].datos.split(',');
				
				monto.value           = parseFloat(datos[0]) - parseFloat(datos[1]) ;  formatear(monto);
				cuota_inicial.value   = parseFloat(datos[1]);  formatear(cuota_inicial);
				cuota_monitoreo.value = parseFloat(datos[2]);  formatear(cuota_monitoreo);
				cuotas.value          = datos[3]; 
				periodo.value         = datos[4];  
				tasa.value            = datos[5];  formatear(tasa);
				concepto.value        = datos[6];  
				
			}
		}		
	}
	
	function seleccionarPrestamoEquipo (form){
		
		with (form){
			if (tipoPrestamo.value == 'EQ'){
				fila_equipo.style.display   = 'block';
				fila_equipo_1.style.display = fila_equipo.style.display; 
				fila_equipo_2.style.display = fila_equipo.style.display; 
				fila_equipo_3.style.display = fila_equipo.style.display; 
				
				celda_tasa0.style.display = 'none'; 
				celda_tasa1.style.display = celda_tasa0.style.display; 
				celda_tasa2.style.display = celda_tasa0.style.display; 
				celda_tasa3.style.display = celda_tasa0.style.display; 
				celda_tipoPrestamo.style.width = '85%';
				celda_entrega.innerHTML = "&nbsp;Entrega Equipo";
			} else if (tipoPrestamo.value == 'AP') {	
			    cuotas.value = 0;
				fila_equipo.style.display   = 'none';
				fila_equipo_1.style.display = fila_equipo.style.display; 
				fila_equipo_2.style.display = fila_equipo.style.display; 
				fila_equipo_3.style.display = fila_equipo.style.display; 

				celda_tasa0.style.display = 'block'; 
				celda_tasa1.style.display = celda_tasa0.style.display; 
				celda_tasa2.style.display = celda_tasa0.style.display; 
				celda_tasa3.style.display = celda_tasa0.style.display; 
				celda_tipoPrestamo.style.width = '31%';
				celda_entrega.innerHTML = "&nbsp;Entrega Dinero";
			} else {
				fila_equipo.style.display   = 'none';
				fila_equipo_1.style.display = fila_equipo.style.display; 
				fila_equipo_2.style.display = fila_equipo.style.display; 
				fila_equipo_3.style.display = fila_equipo.style.display; 

				celda_tasa0.style.display = 'block'; 
				celda_tasa1.style.display = celda_tasa0.style.display; 
				celda_tasa2.style.display = celda_tasa0.style.display; 
				celda_tasa3.style.display = celda_tasa0.style.display; 
				celda_tipoPrestamo.style.width = '31%';
				celda_entrega.innerHTML = "&nbsp;Entrega Dinero";
			}
			tipoEquipo.value = '';
		}
		
	}
	
	function verFacturasCXP( nit,  factura ){
		var url = CONTROLLER + '?estado=Reporte&accion=Facturas&factura='+ factura +'&tipo_doc=010&proveedor='+nit+'&planilla=&agencia=&banco=&sucursal=&FechaI=&FechaF=&pagada=&transaccion=&transaccion_anualada=&ret_pago=&placa=';
		var win = open (url,'facturas',' top=20,left=20, width=800, height=700, scrollbar=yes, status=yes, resizable=yes  ');
        win.focus();
	}
	
	function verFacturasRXP( nit,  factura ){
		var url = CONTROLLER + '?estado=Reporte&accion=FacturasRecurrentes&factura='+ factura +'&tipo_doc=010&proveedor='+nit+'&planilla=&agencia=&banco=&sucursal=&FechaI=&FechaF=&pagada=&transaccion=&transaccion_anualada=&ret_pago=&placa=&anuladas=all&tipo_factura=';
		var win = open (url,'facturas',' top=20,left=20, width=800, height=700, scrollbar=yes, status=yes, resizable=yes  ');
        win.focus();
	}



