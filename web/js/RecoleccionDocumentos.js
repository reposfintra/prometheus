/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var visualizar;
$(document).ready(function () {
    visualizar = document.getElementById("visualizar").checked;
    $("#fecha").val('');
    $("#fechafin").val('');
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $('#ui-datepicker-div').css('clip', 'auto');

    cargarEmpresaEnvioFiltro();
    cargarUnidadNegocio();

    $('#visualizar').click(function () {
        $('#gbox_tabla_recoleccionDocumentos').hide();
        validarcheck();
    });

    $('#limpiar').click(function () {
        $("#fecha").val('');
        $("#fechafin").val('');
    });

    $('#buscar').click(function () {
        $('#gbox_tabla_recoleccionDocumentos').show();
        var consulta;
        if (visualizar !== false) {
            consulta = 'visualizar';
        } else {
            consulta = 'buscar';
        }
        cargarRecoleccionDocumentos(consulta);
    });


    if (visualizar !== false) {
        $('#empresa_envio').attr('hidden', false);
        $('#emp').attr('hidden', false);
        $('#exportar').hide();
        $('#guardar').hide();
        $('#recibir').show();
        $('#reasignar').show();
    } else {
        $('#empresa_envio').attr('hidden', true);
        $('#emp').attr('hidden', true);
        $('#exportar').show();
        $('#guardar').show();
        $('#recibir').hide();
        $('#reasignar').hide();
    }
});


function validarcheck() {
    visualizar = document.getElementById("visualizar").checked;
    if (visualizar !== false) {
        $('#empresa_envio').attr('hidden', false);
        $('#emp').attr('hidden', false);
        $('#exportar').hide();
        $('#guardar').hide();
        $('#recibir').show();
        $('#reasignar').show();
    } else {
        $('#empresa_envio').attr('hidden', true);
        $('#emp').attr('hidden', true);
        $('#exportar').show();
        $('#guardar').show();
        $('#recibir').hide();
        $('#reasignar').hide();
    }
}

function cargarRecoleccionDocumentos(consulta) {
    var grid_tabla = $("#tabla_recoleccionDocumentos");
    if ($("#gview_tabla_recoleccionDocumentos").length) {
        reloadGridTabla(grid_tabla, 4, consulta);
    } else {
        grid_tabla.jqGrid({
            caption: "Recoleccion de documentos",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '490',
            width: '1650',
            colNames: ['Linea Negocio', 'Numero solicitud', 'Estado negocio', 'Estado solicitud', 'tipo', 'Nombre', 'Identificacion', 'Direccion', 'Ciudad', 'Municipio', 'Telefono', 'Celular', 'Direccion Laboral', 'tipo', 'Nombre', 'Identificacion', 'Direccion', 'Ciudad', 'Municipio',
                'Telefono', 'Celular', 'Direccion Laboral', 'Negocio', 'Fecha solcitud', 'Fecha entrega documento', 'Fecha recibido', 'Entregado', 'Empresa', 'Acciones'],
            colModel: [
                {name: 'unidad_negocio', index: 'unidad_negocio', width: 80, sortable: true, align: 'center', hidden: true},
                {name: 'numero_solicitud', index: 'numero_solicitud', width: 80, sortable: true, align: 'center', hidden: false, key: true},
                {name: 'estado_neg', index: 'estado_neg', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'estado_sol', index: 'estado_sol', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'tipo', index: 'tipo', width: 150, sortable: true, align: 'left', hidden: true},
                {name: 'nombre', index: 'nombre', width: 200, sortable: true, align: 'left', hidden: false},
                {name: 'identificacion', index: 'identificacion', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'direccion', index: 'direccion', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'nomciu', index: 'nomciu', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'barrio', index: 'barrio', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'telefono', index: 'telefono', width: 110, sortable: true, align: 'left', hidden: false},
                {name: 'celular', index: 'celular', width: 110, sortable: true, align: 'center', hidden: false},
                {name: 'direccion_lab', index: 'direccion_lab', width: 110, sortable: true, align: 'left', hidden: false},
                {name: 'tipo_cod', index: 'tipo_cod', width: 110, sortable: true, align: 'center', hidden: true},
                {name: 'nombre_cod', index: 'nombre_cod', width: 200, sortable: true, align: 'left', hidden: false},
                {name: 'identificacion_cod', index: 'identificacion_cod', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'direccion_cod', index: 'direccion_cod', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'ciudadcod', index: 'ciudadcod', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'barriocod', index: 'barriocod', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'telefono_cod', index: 'telefono_cod', width: 110, sortable: true, align: 'left', hidden: false},
                {name: 'celular_cod', index: 'celular_cod', width: 110, sortable: true, align: 'center', hidden: false},
                {name: 'direccion_lad_cod', index: 'direccion_lad_cod', width: 110, sortable: true, align: 'center', hidden: false},
                {name: 'cod_neg', index: 'cod_neg', width: 90, sortable: true, align: 'center', hidden: false},
                {name: 'fecha_solcitud', index: 'fecha_solcitud', width: 130, sortable: true, align: 'center', hidden: false},
                {name: 'fecha_entrega_documento', index: 'fecha_entrega_documento', width: 130, sortable: true, align: 'center', hidden: false},
                {name: 'fecha_recibido', index: 'fecha_recibido', width: 130, sortable: true, align: 'center', hidden: false},
                {name: 'entregado', index: 'entregado', width: 80, sortable: true, align: 'left', hidden: true},
                {name: 'empresa', index: 'empresa', width: 80, sortable: true, align: 'left', hidden: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 90}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pager: '#pager',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            sortname: 'invdate',
            ondblClickRow: function (rowid, iRow, iCol, e) {
//                
            }, gridComplete: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila;
                visualizar = document.getElementById("visualizar").checked;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];

                    if (visualizar === true) {
                        docreq = '<img src = "/fintra/images/botones/iconos/notas.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;" name="docs_req" id="docs_req" title ="Abrir ventana Docs. Requeridos"  onclick=\"AbrirDivDocsRequeridos(' + cl + ');">';
                        nota = '<img src = "/fintra/images/note.png"style = "margin-left: 10px; height: 20px; vertical-align: middle;" name="note" id="note" title ="Agregar observacion"  onclick=\"mostrarVentanaObeservacion(' + cl + ');">';
                        grid_tabla.jqGrid('setRowData', ids[i], {actions: docreq + nota});
                    } else {
                        docreq = '<img src = "/fintra/images/botones/iconos/notas.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;" name="docs_req" id="docs_req" title ="Abrir ventana Docs. Requeridos"  onclick=\"AbrirDivDocsRequeridos(' + cl + ');">';
                        grid_tabla.jqGrid('setRowData', ids[i], {actions: docreq });
                    }


                }
            },
            onSelectAll: function (rowid, e, aRowid) {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var allRowsInGrid = jQuery("#tabla_recoleccionDocumentos").jqGrid('getGridParam', 'selarrrow');
                var allRowsId = jQuery("#tabla_recoleccionDocumentos").jqGrid('getRowData');

            },
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
            },
            ajaxGridOptions: {
                data: {
                    opcion: 4,
                    fecha_inicio: $("#fecha").val(),
                    fecha_fin: $("#fechafin").val(),
                    linea_negocio: $("#linea_negocio").val(),
                    empresa: $("#empresa_envio").val(),
                    consulta: consulta
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
//        if (visualizar === false) {
        var Exportar;
        $("#tabla_recoleccionDocumentos").navButtonAdd('#pager', {
            caption: "Guardar",
            id: 'guardar',
            buttonicon: "ui-icon-del",
            //disabled: "disabled",
            onClickButton: function () {
                Exportar = 'N';
                mostrarEmpresas(Exportar);
            }
        });
        $("#tabla_recoleccionDocumentos").navButtonAdd('#pager', {
            caption: "Guardar/Exportar",
            id: 'exportar',
            buttonicon: "ui-icon-del",
            onClickButton: function () {
                var myGrid = $("#tabla_recoleccionDocumentos"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var entregado = filas.entregado;
                Exportar = 'S';
                if (entregado === 'N') {
                    guardarRecoleccionDocumentos(Exportar, entregado);
                } else {
                    mostrarEmpresas(Exportar);
                }
            }
        });
        $("#tabla_recoleccionDocumentos").navButtonAdd('#pager', {
            caption: "Recibir",
            id: 'recibir',
            buttonicon: "ui-icon-del",
            //disabled: "disabled",
            onClickButton: function () {
                Exportar = 'N';
                guardarRecoleccionDocumentos(Exportar, 'S', 'recibir');
            }
        });
        $("#tabla_recoleccionDocumentos").navButtonAdd('#pager', {
            caption: "Reasignar",
            id: 'reasignar',
            buttonicon: "ui-icon-del",
            onClickButton: function () {
                var myGrid = $("#tabla_recoleccionDocumentos"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var empresa = filas.empresa;
                Exportar = 'N';
                mostrarEmpresas(Exportar, 'S', 'reasignar', empresa);
                // guardarRecoleccionDocumentos(Exportar, 'S', 'reasignar');
            }
        });
        validarcheck();
//        }
        grid_tabla.jqGrid('setGroupHeaders', {
            useColSpanStyle: false,
            height: '5',
            groupHeaders: [
                {startColumnName: 'tipo', numberOfColumns: 9, titleText: '<H3><em>SOLICITANTE</em></H3>'},
                {startColumnName: 'tipo_cod', numberOfColumns: 9, titleText: '<H3><em>CODEUDOR</em></H3>'}
            ]
        });
    }
}

function reloadGridTabla(grid_tabla, op, consulta) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                fecha_inicio: $("#fecha").val(),
                fecha_fin: $("#fechafin").val(),
                linea_negocio: $("#linea_negocio").val(),
                empresa: $("#empresa_envio").val(),
                consulta: consulta
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function guardarRecoleccionDocumentos(Exportar, entregado, otro) {

    var selRowIds = $("#tabla_recoleccionDocumentos").jqGrid('getGridParam', 'selarrrow'), rowData;
    if (selRowIds.length > 0) {
        var arrayJson = new Array();
        for (var i = 0; i < selRowIds.length; i++) {
            rowData = $("#tabla_recoleccionDocumentos").jqGrid("getLocalRow", selRowIds[i]);
            //alert(rowData);
            arrayJson.push(rowData);
        }
    }

    if (selRowIds.length === 0) {
        mensajesDelSistema('No hay filas seleccionadas', '300', 'auto', false);
    } else {
        $.ajax({
            async: false,
            url: "./controller?estado=Admin&accion=Fintra",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 5,
                informacion: JSON.stringify({json: arrayJson, operacion: otro}),
                empresa_envio: $('#empresa_envio_2').val(),
                exportar: Exportar
            },
            success: function (json) {
                if (json.respuesta === 'GUARDADO') {
                    if (Exportar === 'S') {
                        if (entregado === 'N') {
                            exportarExcelRecoleccionDocumentos();
                        } else {
                            $("#dialogMsjEmpresas").dialog("close");
                            exportarExcelRecoleccionDocumentos();
                        }
                    } else {
                        if (otro === 'recibir') {
                            cargarRecoleccionDocumentos('visualizar');
                            mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);
                        } else if (otro === 'reasignar') {
                            $("#dialogMsjEmpresas").dialog("close");
                            cargarRecoleccionDocumentos('visualizar');
                            mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);
                        } else {
                            $("#dialogMsjEmpresas").dialog("close");
                            cargarRecoleccionDocumentos('buscar');
                            mensajesDelSistema('Exito al guardar los registros', '300', 'auto', false);
                        }
                    }
                } else {
                    mensajesDelSistema('vaya ha ocurrido un error', '300', 'auto', false);
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }

}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function  mostrarEmpresas(Exportar, entregado, otro, empresa) {
    cargarEmpresaEnvio();
    $('#empresa_envio_2').val(empresa);
    $("#dialogMsjEmpresas").dialog({
        width: '361',
        height: '200',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Guardar": function () {
                guardarRecoleccionDocumentos(Exportar, entregado, otro);
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }

    });
    $("#dialogMsjEmpresas").siblings('div.ui-dialog-titlebar').remove();
}

function cargarUnidadNegocio() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 8
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarEmpresaEnvioFiltro() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 6
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#empresa_envio').html('');
                $('#empresa_envio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#empresa_envio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarEmpresaEnvio() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 6
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#empresa_envio_2').html('');
                $('#empresa_envio_2').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#empresa_envio_2').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function exportarExcelRecoleccionDocumentos() {
    var selRowIds = $("#tabla_recoleccionDocumentos").jqGrid('getGridParam', 'selarrrow'), rowData;
    if (selRowIds.length > 0) {
        var arrayJson = new Array();
        for (var i = 0; i < selRowIds.length; i++) {
            rowData = $("#tabla_recoleccionDocumentos").jqGrid("getLocalRow", selRowIds[i]);
            //alert (rowData);
            arrayJson.push(rowData);
        }
    } else {
        mensajesDelSistema('No hay filas seleccionadas', '300', 'auto', false);
    }
    // alert(JSON.stringify({json: arrayJson}));
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        async: false,
        data: {
            opcion: 7,
            info: JSON.stringify(arrayJson)
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
            cargarRecoleccionDocumentos('buscar');
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function AbrirDivDocsRequeridos(id_solicitud) {
    verDocsRequeridos(id_solicitud);
    $("#dialogo_docs_requeridos").dialog({
        width: 'auto',
        height: 250,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'DOCUMENTOS REQUERIDOS',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function verDocsRequeridos(id_solicitud) {
    var id_unidad_negocio = $("#tabla_recoleccionDocumentos").getRowData(id_solicitud).unidad_negocio;

    $.ajax({
        url: './controller?estado=Admin&accion=Fintra',
        datatype: 'json',
        type: 'post',
        data: {opcion: 66,
            id_unidad_negocio: id_unidad_negocio,
            id_solicitud: id_solicitud
        },
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    console.log(json.error);
                } else {
                    $('#dialogo_docs_requeridos').empty();
                    for (var i in json.rows) {
                        var flag = "";
                        if (json.rows[i].estado === 'required') {
                            flag = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;" title ="Pendiente" >';
                        } else {
                            flag = '<img src = "/fintra/images/flag_green.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;" title ="Listo" >'
                        }
                        $('#dialogo_docs_requeridos').append(flag + json.rows[i].valor + '<br/>');
                    }

                }
            } catch (exc) {
                console.error(exc);
            }
        },
        error: function () {
        }
    });
}

function  mostrarVentanaObeservacion(id) {
    $('#id_solicitud').val(id);
    cargarObservaciones();
    $("#dialogMsjObservaciones").dialog({
        width: '650',
        height: '300',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Guardar": function () {
                guardarObservaciones();
            },
            "Salir": function () {
                $(this).dialog("close");
                $("#observaciones").val('');
            }
        }
    });
    $("#dialogMsjObservaciones").siblings('div.ui-dialog-titlebar').remove();
}

function cargarObservaciones() {
    $.ajax({
        async: false,
        url: "./controller?estado=Admin&accion=Fintra",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 81,
            numero_solicitud: $("#id_solicitud").val()
        },
        success: function (json) {
            $("#observaciones").val(json[0].observaciones);
            console.log(json);

        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function guardarObservaciones() {
    $.ajax({
        async: false,
        url: "./controller?estado=Admin&accion=Fintra",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 82,
            numero_solicitud: $("#id_solicitud").val(),
            observaciones: $("#observaciones").val()
        },
        success: function (json) {
            console.log(json);
            if (json.respuesta === 'Guardado') {
                mensajesDelSistema('Exito al guardar', '300', 'auto', false);
            } else {
                mensajesDelSistema('ha ocurrido un error', '300', 'auto', false);
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}