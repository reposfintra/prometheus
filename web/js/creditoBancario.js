
function cargarSucursalesEgreso(){
    var controller = document.getElementById("CONTROLLER").value;
    new Ajax.Updater({success: 'sucursalEgreso', failure:'divMensaje'}, controller+'?estado=Credito&accion=Bancario', {
        method: 'post',
        parameters: {opcion: 'CARGAR_SUCURSALES', banco: $F('bancoEgreso')}
    });
}

function cargarSucursalesIngreso(){
    var controller = document.getElementById("CONTROLLER").value;
    new Ajax.Updater({success: 'sucursalIngreso', failure:'divMensaje'}, controller+'?estado=Credito&accion=Bancario', {
        method: 'post',
        parameters: {opcion: 'CARGAR_SUCURSALES', banco: $F('bancoIngreso')}
    });
}

function cargarDefaults(){
    var referencia = $("referencia");
    var id = referencia.options[referencia.selectedIndex].id;
    if(id!=""){
        var vec = id.split("_");
        $("hc").value = vec[1];
        if(vec[2]=="UNICO"){
            $("unico").show();
            $("periodicidad").hide();
            $("periodicidad").value="";
            $("tasaEA").value="";
            $("tasaNominal").value="";
            $("tasaVencida").value="";
            $("tasaDiaria").value="";
            $("trTasaEA").hide();
            $("trTasaNominal").hide();
            $("trTasaVencida").hide();
            $("trTasaDiaria").hide();
            $("trTasaPactada").show();
            $("trTasaCobrada").show();
            $("trVlrIntereses").show();
            $("trVlrFactura").show();
            $("dtfFactoring").show();
            $("tipoDTF").hide();
            $("tipoDTF").value = "EA";
        }else{
            $("unico").hide();
            $("periodicidad").show();
            $("trTasaEA").show();
            $("trTasaNominal").show();
            $("trTasaVencida").show();
            $("trTasaDiaria").show();
            $("trTasaPactada").hide();
            $("trTasaCobrada").hide();
            $("trVlrIntereses").hide();
            $("trVlrFactura").hide();
            $("dtfFactoring").hide();
            $("tipoDTF").show();
        }
        calcularDTF();
    }
}

function cargarPuntosBasicos(){
    var controller = document.getElementById("CONTROLLER").value;
    if($F("banco")!="" && $F("linea")!=""){
        var banco = $("banco");
        var idBanco = (banco.options[banco.selectedIndex].id).split("_");
        var codigoBanco = idBanco[0];
        var linea = $F("linea");
        var lin = linea.split(";");
  
        new Ajax.Request(
            controller+'?estado=Credito&accion=Bancario',
            {
                method: 'post',
                parameters: {opcion: 'CARGAR_PUNTOS_BASICOS', banco: codigoBanco, linea: lin[1]},
                onSuccess: function (resp){
                    $('ptoBasicos').value = resp.responseText;
                    recalcularTasas();
                }
            }
        );
    }
}

function recalcularTasas(){

    if($F("dtf")!="" && $F("ptoBasicos")!=""){
        var linea = $F("linea");
        var lin = linea.split(";");
        if(linea!=""){
            var dtf = parseFloat($F("dtf"))/100;
            var puntosBasicos = parseFloat($F("ptoBasicos"))/100;
            if(lin[1] == "FACTORING"){
                //CREDITOS FACTORING
                calcularTasaFactoring(dtf, puntosBasicos);
            }else{
                if($F("periodicidad")!=""){
                    //CREDITOS ORDINARIOS Y DE TESORERIA
                    var banco = $("banco");
                    var idBanco = (banco.options[banco.selectedIndex].id).split("_");
                    var baseBanco = idBanco[1];
                    var p = parseInt($F("periodicidad")); //numero de cuotas al a�o

                    calcularTasaOrdinario(baseBanco, dtf, puntosBasicos, p);
                }
            }
        }
    }
}

function calcularTasaFactoring(dtf, puntosBasicos){
    var dtfTA = (parseFloat($F("dtfTA"))/100)+puntosBasicos;
    $("tasaPactada").value = ((Math.pow(((dtfTA/4)/(1-(dtfTA/4))+1),4) -1)*100).toFixed(3) + "%";
    if($F("fechaInicial")!="" && $F("vlrCredito")!="" && $F("vlrIntereses")!="" && $F("fechaVencimiento")!=""){
        var vlrDesembolso = parseFloat($F("vlrCredito").replace(/,/ig, ""));
        var vlrIntereses = parseFloat($F("vlrIntereses").replace(/,/ig, ""));
        var vlrFactura = vlrDesembolso + vlrIntereses;
        $("vlrFactura").value = formatNumber(vlrFactura);
        try{
            var fDesembolso = new Date($F("fechaInicial").replace(/-/ig, "/"));
            var diff = diferenciaFechas($F("fechaVencimiento"),dateToString(fDesembolso));
            var tasaCobrada = Math.pow((vlrFactura/vlrDesembolso),(1/(diff/360)))-1;
            $("tasaCobrada2").value = (tasaCobrada*100).toFixed(3) + "%";
            $("tasaCobrada").value = tasaCobrada*100;
        }catch(e){}
    }
}

function calcularTasaOrdinario(baseBanco, dtf, puntosBasicos, p){

   
        if($("tipoDTF").value=="EA")
        {
          
            
            var dtfTA = (parseFloat($F("dtfTA"))/100);
             if($F("dtfTA")!=0)
            {
            
            dtf=(Math.pow(( (dtfTA/4)/(1-(dtfTA/4))+1),4))-1;
            var tea = ((dtf+puntosBasicos));
            }
            else
            {                     
                var tea = ((dtf+puntosBasicos));
                 
            }


            //var tea =  ttv.toFixed(5) + "%";
        }
        else
            {    //Tasa trimestre vencido
                 
            
                var ttv = ((dtf+puntosBasicos)/4)/(1-((dtf+puntosBasicos)/4));
                var tea = Math.pow(ttv+1, 4) - 1;
                
           }




    //Tasa EA   

     if($F("dtfTA").value=0)
     {
       tea=(tea*100).toFixed(5) + "%";
     }
    $("tasaEA").value = tea;
    





    //Tasa nominal
    var tn = (Math.pow(tea+1, 1/p) - 1) * p;
    $("tasaNominal").value = (tn*100).toFixed(5) + "%";

    if(baseBanco==360){
        //Tasa vencida
        var tv = tn / p;
        $("tasaVencida").value = (tv*100).toFixed(5) + "%";

        //Tasa diaria
        var dp = 360/p; //Numero de d�as que tiene el periodo
        var td = tv / dp;
        $("tasaDiaria").value = (td*100).toFixed(5) + "%";
    }else if(baseBanco==365){
        //Tasa vencida
        $("tasaVencida").value = " ";

        //Tasa diaria
        td = tn / 365;
        $("tasaDiaria").value = (td*100).toFixed(5) + "%";
    }
    if($("tasa")!=null){
        $("tasa").value = td;
    }

     ///alert("--->"+tea);
}

/*function validarCredito(){

    if($F("referencia")!="" && $F("banco")!="" && $F("linea")!="" && $F("documento")!="" && $F("dtf")!="" && $F("ptoBasicos")!=""
        && $F("vlrCredito")!="" && $F("fechaInicial")!="" && $F("fechaVencimiento")!="" && $F("cupo")!="" && $F("bancoEgreso")!="" && $F("sucursalEgreso")!=""
        && $F("autorizador")!="" && $F("hc")!="" && $F("descripcion")!="" && $F("bancoIngreso")!="" && $F("sucursalIngreso")!=""){
            if(diferenciaFechas($F("fechaVencimiento"), $F("fechaInicial"))>=0){
                var ptoBasico = parseFloat($F("ptoBasicos"));
                if(ptoBasico>0){
                    var referencia = $("referencia");
                    var id = referencia.options[referencia.selectedIndex].id;
                    if(id!=""){
                        var vec = id.split("_");
                        if((vec[2]!="UNICO" && $F("periodicidad")!="" && $F("tasaEA")!="" && $F("tasaNominal")!="" && $F("tasaVencida")!="" && $F("tasaDiaria")!="")
                            || (vec[2]=="UNICO" && $F("vlrIntereses")!="" && $F("tasaCobrada")!="" )){
                            document.form1.submit();
                        }else{
                            alert("Debe llenar todos los campos: periodicidad");
                        }
                    }else{
                        alert("Ha ocurrido un error al cargar los datos de la referencia. Refresque la pagina.")
                    }
                }else{
                    alert("Los puntos basicos no pueden ser cero")
                }
            }else{
                alert("La fecha incial debe ser menor que la fecha de vencimiento");
            }
    }else{
        alert("Debe llenar todos los campos..");
    }

}*/



function validarCredito(){

    if($F("banco")!="" && $F("linea")!="" && $F("documento")!="" && $F("dtf")!="" && $F("ptoBasicos")!=""
        && $F("vlrCredito")!="" && $F("fechaInicial")!="" && $F("fechaVencimiento")!="" && $F("cupo")!="" && $F("bancoEgreso")!="" && $F("sucursalEgreso")!=""
        && $F("autorizador")!="" && $F("hc")!="" && $F("descripcion")!="" && $F("bancoIngreso")!="" && $F("sucursalIngreso")!=""){
            if(diferenciaFechas($F("fechaVencimiento"), $F("fechaInicial"))>=0){
                var ptoBasico = parseFloat($F("ptoBasicos"));
                if(ptoBasico>0){
                    if(($F("periodicidad")!="" && $F("tasaEA")!="" && $F("tasaNominal")!="" && $F("tasaVencida")!="" && $F("tasaDiaria")!="")
                        || ($F("vlrIntereses")!="" && $F("tasaCobrada")!="" )){
                        document.form1.submit();
                    }else{
                        alert("Debe llenar todos los campos: periodicidad");
                    }
                }else{
                    alert("Los puntos basicos no pueden ser cero")
                }
            }else{
                alert("La fecha incial debe ser menor que la fecha de vencimiento");
            }
    }else{
        alert("Debe llenar todos los campos..");
    }

}

function editarPuntosBasicos(baseurl, opcion, i){
    var params = "";
    if(opcion=="EDITAR"){
        params = "&banco="+$F("banco"+i)+"&linea="+$F("linea"+i)+"&ptoBasicos="+$F("ptoBasicos"+i)
    }
    new Ajax.Request(
        baseurl+"/jsp/creditoBancario/insertarPuntosBasicos.jsp?opcion="+opcion,
        {method: 'post',
            parameters: params,
            onComplete: openwin
        }
    );
}

function guardarPuntosBasicos(controller, opcion){
    if($F("ptoBasicos")!="" && $F("banco")!="" && $F("linea")!=""){
        if(opcion=="NUEVO"){
            var opcionAction = 'INSERTAR_PUNTOS_BASICOS';
        }else{
            opcionAction = 'EDITAR_PUNTOS_BASICOS';
        }
        new Ajax.Request(
            controller+'?estado=Credito&accion=Bancario',
            {
                method: 'post',
                parameters: {opcion: opcionAction, banco: $F("banco"), linea: $F('linea'), puntosBasicos: $F("ptoBasicos")},
                onSuccess: function (resp){
                    if(resp.responseText=="ok"){
                        alert("Se ha guardado el registro");
                    }else{
                        alert(resp.responseText);
                    }
                    closewin();
                }
            }
        );
    }else{
        alert("Debe ingresar todos los datos");
    }
}

function editarCupo(baseurl, opcion, i){
    var params = "";
    if(opcion=="EDITAR"){
        params = "&banco="+$F("banco"+i)+"&linea="+$F("linea"+i)+"&cupo="+$F("cupo"+i)
    }
    new Ajax.Request(
        baseurl+"/jsp/creditoBancario/insertarCupo.jsp?opcion="+opcion,
        {method: 'post',
            parameters: params,
            onComplete: openwin
        }
    );
}

function guardarCupo(controller, opcion){
    if($F("cupo")!="" && $F("banco")!="" && $F("linea")!=""){
        if(opcion=="NUEVO"){
            var opcionAction = 'INSERTAR_CUPO';
        }else{
            opcionAction = 'EDITAR_CUPO';
        }
        new Ajax.Request(
            controller+'?estado=Credito&accion=Bancario',
            {
                method: 'post',
                parameters: {opcion: opcionAction, banco: $F("banco"), linea: $F('linea'), cupo: $F("cupo")},
                onSuccess: function (resp){
                    if(resp.responseText=="ok"){
                        alert("Se ha guardado el registro");
                    }else{
                        alert(resp.responseText);
                    }
                    closewin();
                }
            }
        );
    }else{
        alert("Debe ingresar todos los datos")
    }
}

/**
 * Funciones para el manejo de ventanas
 */

function closewin(){
    $('contenido').innerHTML="";
    $('contenido').style.visibility='hidden';
    win.close();
    //win.destroy();
    if($("reload")!=null){
        location.reload();
    }
}

function openwin(response){
    $('contenido').innerHTML=response.responseText;
    $('contenido').style.display='block';
    $('contenido').style.visibility='visible';
    win= new Window(
    {id: "nuevo",
        title: "",
        width:$('contenido').width,
        height:$('contenido').heigth,
        destroyOnClose: true,
        onClose:closewin,
        resizable: false
    });
    win.setContent('contenido', true, true);
    win.show(false);
    win.showCenter();
}

function openwin2(response){
    $('contenido').innerHTML=response.responseText;
    $('contenido').style.display='block';
    $('contenido').style.visibility='visible';
    win= new Window(
    {id: "nuevo",
        title: "",
        parent: "form1",
        width:$('contenido').width,
        height:$('contenido').heigth,
        destroyOnClose: true,
        onClose:closewin,
        resizable: false
    });
    win.setContent('contenido', true, true);
    win.show(false);
    win.showCenter();
}

function calcularInteresesCausados(fila){
   
    var tasaDiaria = parseFloat($F("tasaDiaria"+fila));

    var ref = $F("ref"+fila);


    var tasaCobrada = parseFloat($F("tasaCobrada"+fila))/100;
    var tasaEA = parseFloat($F("tasaEA"+fila));
    var fechaInicial = $F("fecha_inicial"+fila);
    var fechaFinal = $F("fecha_final"+fila);
    var capital = parseFloat($F("capital_inicial"+fila).replace(/,/ig, ""));
    var saldoInicial = parseFloat($F("saldo_inicial"+fila).replace(/,/ig, ""));
    var periodicidad = $F("periodicidad"+fila);
    var intereses = 0;
    var finicial = new Date(fechaInicial.replace(/-/ig, "/"));
    var ffinal = new Date(fechaFinal.replace(/-/ig, "/"));
    if($F("baseBanco"+fila) == "360" && periodicidad!=0){

         if(ref=="TESORERIA") //si es tesoreria
         {
            var diff = ffinal.getTime() - finicial.getTime();
            numDias =  diff / (1000 * 60 * 60 * 24);

         }
         else
         {
            var numDias = dias360(finicial, ffinal);
         }

    }
    else
    {
        var diff = ffinal.getTime() - finicial.getTime();
        numDias =  diff / (1000 * 60 * 60 * 24);
    }

    
    //---calculo intereses ----
    if(periodicidad==0){ // Si es factoring
        intereses = (saldoInicial * Math.pow(1+tasaCobrada, numDias/360)) - saldoInicial;
    }
    else// si es ordinaria o tesoreria
    {

        if($F("baseBanco"+fila) == "365")//si es base 365
        {

           intereses = capital * tasaDiaria * numDias;

        }
        else
        {    

              intereses = (saldoInicial * Math.pow(1+(tasaEA), numDias/360)) - saldoInicial;
        }

    }




    //Se coloca el valor en los campos
    $('interesesCalculados'+fila).value=formatNumber(intereses.toFixed(0));
    $('intereses'+fila).value=formatNumber(intereses.toFixed(0));
}


function validarCausacion(){
    var num = $F('numCreditos');
    for(i=0;i<num;i++){
        if($("chkCredito"+i).checked==true){
            if(formatoFecha($("fecha_final"+i))){
                if(diferenciaFechas($F("fecha_final"+i), $F("fecha_inicial"+i))>=0){
                    document.forms[0].submit();
                }else{
                    alert("La fecha incial debe ser menor que la fecha fina para el credito " + $F("documento"+i))
                    break;
                }
            }else{
                alert("Error en el formato de fecha: "+$F("fecha_final"+i))
                break;
            }
        }
    }
}

function checkAll(check){
    if(check.checked==true){
        $$("input:checkbox").each(function(input){
            input.checked = true;
        });
    }else{
        $$("input:checkbox").each(function(input){
            input.checked = false;
        });
    }
}

function cargarBancosNacionalidad(nacionales){
    var controller = document.getElementById("CONTROLLER").value;
    new Ajax.Updater({success: 'banco', failure:'divMensaje'}, controller+'?estado=Credito&accion=Bancario', {
        method: 'post',
        parameters: {opcion: 'CARGAR_BANCOS_NACIONALIDAD', nacionales: nacionales}
    });
}

function mostrarFechas(){
    $("trFechaInicial").toggle();
    $("trFechaFinal").toggle();
    $("trRango").toggle();
}

function validarFiltros(){
    if($("chkVigente").checked==false){
        if(diferenciaFechas($F("fechaFinal"), $F("fechaInicial"))>=0){
            document.forms[0].submit();
        }else{
            alert("La fecha inicial debe ser menor que la fecha final");
        }
    }else{
        document.forms[0].submit();
    }
}

function verCreditosBanco(baseurl, nit, nombreBanco, vigente){
    location.href = baseurl+"/jsp/creditoBancario/creditosBanco.jsp?nit="+nit+"&vigente="+vigente+"&nombreBanco="+nombreBanco;
}

function verLiquidacionCredito(documento){
    var baseurl = $F("BASEURL");
    var nit = $F("nit");
    location.href = baseurl+"/jsp/creditoBancario/consultaCreditoBancario.jsp?nit="+nit+"&documento="+documento;
}

function agregarFila(BASEURL,periodicidad){
    if($("fecha_final")==null){
        var periodo = periodicidad;
        var hoy = new Date();
        var curr_date = hoy.getDate();
        if(curr_date < 10){
            curr_date = "0"+curr_date;
        }
        var curr_month = hoy.getMonth() + 1; //months are zero based
        if(curr_month<10){
            curr_month = "0"+curr_month;
        }
        var curr_year = hoy.getFullYear();
        var tam = parseInt($F("numDetalles"))+1;
        var fila = '<tr class="fila" style="font-size: 10px;">'+
                        '<td>'+tam+'</td>'+
                        '<td><input type="text" id="fecha_inicial" name="fecha_inicial" value="'+$F("hidFechaFinal")+'" class="fila" style="border: 0pt none;text-align:left; width:70px;" readonly/></td>'+
                        '<td>'+
                            '<input type="text" id="fecha_final" name="fecha_final" value="'+(curr_year+"-"+curr_month+"-"+curr_date)+'" class="fila" style="border: 0pt none;text-align:left; width:80px;" readonly/>'+
                            '<img src="'+BASEURL+'/js/Calendario/cal.gif" id="imgFechaFinal" />'+
                        '</td>'+
                        '<td align="right" width="60">'+
                            '<img src="'+BASEURL+'/images/botones/iconos/restablecer.gif" alt="actualizar" style="cursor:pointer" onclick="actualizarDTF('+periodo+')"/>'+
                            '<input type="text" id="dtf" name="dtf" value="'+$F("hidDtf")+'" class="fila" style="border: 0pt none;text-align:right; width:30px;" onblur="calcularInteresesConsulta()" />'+
                        '</td>'+
                        '<td align="right"><input type="text" id="tasa" name="tasa" class="fila" style="border: 0pt none;text-align:right; width:30px;" readonly/></td>'+
                        '<td align="right"><input type="text" id="saldo_inicial" name="saldo_inicial" value="'+formatNumber(parseFloat($F("hidSaldo")))+'" class="fila" style="border: 0pt none;text-align:right; width:80px;" readonly/></td>'+
                        '<td align="right">'+
                            '<span id="mik">'+formatNumber(parseFloat($F("hidCapitalInicial")))+'</span>' +
                            '<input type="hidden" id="capital_inicial" name="capital_inicial" value="'+$F("hidCapitalInicial")+'" />'+
                        '</td>'+
                        '<td align="right"><input type="text" id="interesesCalculados" name="interesesCalculados" class="fila" style="border: 0pt none;text-align:right; width:80px;" readonly/><input type="hidden" id="intereses" name="intereses"/></td>'+
                        '<td align="right"><input type="text" id="ajuste" name="ajuste" value="0" onblur="recalcularSaldo()" class="fila" style="border: 0pt none;text-align:right; width:40px;"/></td>'+
                        '<td align="right"><input type="text" id="interesAcumulado" name="interesAcumulado" class="fila" style="border: 0pt none;text-align:right; width:80px;" readonly/></td>'+
                        '<td align="right"><input type="text" id="pagoCapital" name="pagoCapital" value="0" onblur="recalcularSaldo()" onkeyup="soloNumerosformateado(this.id);" class="fila" style="border: 0pt none;text-align:right; width:80px;" /></td>'+
                        '<td align="right"><input type="text" id="pagoIntereses" name="pagoIntereses" value="0" onblur="recalcularSaldo()" onkeyup="soloNumerosformateado(this.id);" class="fila" style="border: 0pt none;text-align:right; width:80px;" /></td>'+
                        '<td align="right"><input type="text" id="valorPago" name="valorPago" class="fila" style="border: 0pt none;text-align:right; width:80px;" readonly /></td>'+
                        '<td align="right"><input type="text" id="saldoFinal" name="saldoFinal" class="fila" style="border: 0pt none;text-align:right; width:80px;" readonly /></td>'+
                        '<td align="center"></td>'+
                        '<td align="center"></td>'+
                    '</tr>';

        $("tablaDetalles").insert(fila);

        calcularInteresesConsulta();

        Calendar.setup({
            inputField : "fecha_final",
            trigger    : "imgFechaFinal",
            onSelect   : function() {
                calcularInteresesConsulta();
                this.hide();
            }
        });
    }else{
        alert("Solo puede agregar un detalle");
    }
}

function modificarSaldoAgregarFila(id){
    //alert(""+id);
    var mivalor=parseFloat(document.getElementById(id).innerHTML.replace(/,/ig, ""));
    var capital = parseFloat($F("capital_inicial").replace(/,/ig, ""));
     
     $("capital_inicial").value=formatNumber(capital-mivalor);
     document.getElementById("mik").innerHTML=formatNumber(capital-mivalor);
    //alert(capital);
    
}



function calcularInteresesConsulta(){
    if($F("periodicidad")==0){
        $("tasa").value = (parseFloat($F("tasaCobrada"))).toFixed(2);
    }else{
        var dtf = parseFloat($F("dtf"))/100;
        var puntosBasicos = parseFloat($F("ptoBasicos"))/100;

        calcularTasaOrdinario(parseInt($F("baseBanco")), dtf, puntosBasicos, parseInt($F("periodicidad")));
        $("tasaDiaria").value = $F("tasa");
        $("tasa").value = (parseFloat($F("tasa"))*100).toFixed(5) + "%";


    }
    calcularInteresesCausados('');
    recalcularSaldo();

}

function recalcularSaldo(){
    //Calcular intereses acumulados
    var interesAcumuladoAnterior = parseFloat($F("hidInteresAcumulado")) - parseFloat($F("hidPagoIntereses"));
    var interes = parseFloat($F("interesesCalculados").replace(/,/ig, "")) + parseFloat($F("ajuste").replace(/,/ig, ""));
    $("intereses").value = interes;
    var interesAcumulado = interesAcumuladoAnterior + interes;
    $("interesAcumulado").value = formatNumber(interesAcumulado.toFixed(0));

    //Calcular Pago
    var vlrPago = parseFloat($F("pagoCapital").replace(/,/ig, "")) + parseFloat($F("pagoIntereses").replace(/,/ig, ""));
    $("valorPago").value = formatNumber(vlrPago);

    //Calcular saldo final
    $("saldoFinal").value = formatNumber(parseFloat($F("capital_inicial").replace(/,/ig, "")) + interesAcumulado - vlrPago);
}

function validarDetalle(baseurl){
    if($("fecha_final")!=null){
        if(diferenciaFechas($F("fecha_final"), $F("fecha_inicial"))>=0){
            if($F("intereses")!="" && !isNaN($F("saldoFinal").replace(/,/ig, ""))){

                //Obtener el id del credito bancario para enviarlo a la siguiente interfaz
                var params = "&nit="+$F("nit")+"&documento="+$F("documento");
                new Ajax.Request(
                    baseurl+"/jsp/creditoBancario/confirmarEgreso.jsp",
                    {method: 'post',
                        asynchronous: false,
                        parameters: params,
                        onComplete: openwin2
                    }
                );
                cargarSucursalesEgreso();
                $("sucursalEgreso").value = $F("sucursalDefault");
            }else{
                alert("Debe llenar todos los datos");
            }
        }else{
            alert("La fecha final debe ser mayor a la inicial");
        }
    }else{
        alert("No hay detalles que guardar");
    }
}

function guardarDetallesCredito(){
    if($("tipoTodo").checked==false || ($("tipoTodo").checked==true && $F("bancoEgreso")!="" && $F("sucursalEgreso")!="")){
        document.forms[0].submit();
    }else{
        alert("Debe ingresar todos los datos")
    }
}

function mostrarBancoEgreso(){
    $("trBancoEgreso").toggle();
    $("trSucursalEgreso").toggle();
}
/*
function actualizarDTF(){
    new Ajax.Request(
        $F("CONTROLLER")+'?estado=Credito&accion=Bancario',
        {
            method: 'post',
            asynchronous: false,
            parameters: {opcion: 'CARGAR_DTF'},
            onSuccess: function (resp){
                $('dtfTA').value = resp.responseText;
            }
        }
    );
    calcularDTF();
    calcularInteresesConsulta();
} */

function actualizarDTF(periodo){
    new Ajax.Request(
        $F("CONTROLLER")+'?estado=Credito&accion=Bancario',
        {
            method: 'post',
            asynchronous: false,
            parameters: {opcion: 'CARGAR_DTF_CUOTA', fechaDTF:$F("hidFechaFinal"), periodo:periodo },
            onSuccess: function (resp){
                $('dtfTA').value = resp.responseText;
            }
        }
    );
    calcularDTF();
    calcularInteresesConsulta();
}

function calcularDTF(){
    if($F("tipoDTF") == "EA"){
        var dtfTA = parseFloat($F("dtfTA"))/100;
        var dtf = Math.pow(((dtfTA/4)/(1-(dtfTA/4))+1),4) -1;
        $("dtf").value = (dtf*100).toFixed(6);
    }else{
        $("dtf").value = $F("dtfTA");
    }
}

/**
 * Calcula el numero de dias entre 2 fechas basandose en un a�o de 360 dias(12 meses de 30 d�as)
 */
function dias360(date1, date2){
    var D1 = date1.getDate();
    var M1 = date1.getMonth() + 1; //months are zero based
    var Y1 = date1.getFullYear();

    var D2 = date2.getDate();
    var M2 = date2.getMonth() + 1; //months are zero based
    var Y2 = date2.getFullYear();

    var dias = 360*(Y2-Y1) + 30*(M2-M1) + (D2-D1);
    return dias;
}

function calcularTasasCausacion(fila){
    if($F("periodicidad"+fila)!=0){
        var dtf = parseFloat($F("dtf"+fila))/100;
        var puntosBasicos = parseFloat($F("ptoBasicos"+fila))/100;
        calcularTasaOrdinario(parseInt($F("baseBanco"+fila)), dtf, puntosBasicos, parseInt($F("periodicidad"+fila)));
        $("tasaDiaria"+fila).value = $F("tasa");
        $("tasa"+fila).value = $F("tasaEA");
        //Se reinician los valores de los campos temporales
        $("tasa").value=0;
        $("tasaEA").value=0;
    }
    calcularInteresesCausados(fila);
}

function editarLineaCredito(baseurl, opcion, i){
    var params = "";
    if(opcion=="EDITAR"){
        params = "&linea="+$F("linea"+i)+"&idLinea="+$F("idLinea"+i);
    }
    new Ajax.Request(
        baseurl+"/jsp/creditoBancario/crearLineaCredito.jsp?opcion="+opcion,
        {method: 'post',
            parameters: params,
            onComplete: openwin
        }
    );
}


function guardarLineaCredito(controller, opcion){
    if($F("linea")!=""){
        if(opcion=="NUEVO"){
            var opcionAction = 'INSERTAR_LINEA_CREDITO';
        }else{
            opcionAction = 'EDITAR_LINEA_CREDITO';
        }
        new Ajax.Request(
            controller+'?estado=Credito&accion=Bancario',
            {
                method: 'post',
                parameters: {opcion: opcionAction, linea: $F('linea'), idLinea: $F('idLinea'), hc: $F('hc')},
                onSuccess: function (resp){
                    if(resp.responseText=="ok"){
                        alert("Se ha guardado el registro");
                    }else{
                        alert(resp.responseText);
                    }
                    closewin();
                }
            }
        );
    }else{
        alert("Debe ingresar todos los datos")
    }
}

function editarCupoCredito(baseurl, opcion, i) {
    var params = "";
    //alert($("#cupo" + i).val());
     $('#div_modificar').fadeOut('slow');
   // if (opcion == "EDITAR") {
        params = "&cupo=" + $("#cupo" + i).val() + "&idCupo=" + $("#idCupo" + i).val() + "&opcion=" + opcion;
       
          $.ajax({
                    type: "POST",
                    url : "/fintra/jsp/creditoBancario/crearCuposCreditoBancario.jsp",
                    async:true,
                    dataType: "html",
                    data:{
                       opcion: opcion, nomCupo: $('#cupo' + i).val(), idCupo: $('#idCupo' + i).val(),
                    },
                   success:function (data){
                        if (data!=""){
                            $('#div_modificar').html(data);
                            $('#div_modificar').fadeIn('slow');
                          
                        }
                    }
                });


    

}



function guardarCupoCredito(controller, opcion){
    if($("#cupo").val()!="" && $("#cuenta").val()!=""){
        if(opcion=="NUEVO"){
            var opcionAction = 'INSERTAR_CUPO_CREDITO';
        }else{
            opcionAction = 'EDITAR_CUPO_CREDITO';
        }
        
         $.ajax({
                    type: "POST",
                    url : controller+'?estado=Credito&accion=Bancario',
                    async:true,
                    dataType: "text",
                    data:{
                       opcion:opcionAction, cupo: $('#cupo').val(), idCupo: $('#idCupo').val(), cuenta: $('#cuenta').val(),
                    },
                   success:function (resp){
                        
                    alert("Se guardo el registro");
                    
                  },
                  error: function(error) {
                    alert("No se guardo el registro");
                }
                });

    }else{
        alert("Debe ingresar todos los datos")
    }
}

function cargarHC(){
   var tq= $F("linea")
   var dd=tq.split(";");
   $("hc").value=dd[0];
    
}

function cargarCuenta(){
   var tq= $F("cupo")
   var dd=tq.split(";");
   $("cuenta").value=dd[0];
    
}


//var fieldGlobal;
function buscarCuenta(field) {

    var parm = $("#" + field).val();
    //alert(parm);
    //  fieldGlobal=field.substr(6,6);
    $("#" + field).autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "./controller?estado=Credito&accion=Bancario",
                type: "POST",
                dataType: "json",
                data: "opcion=CONSULTAR_CUENTAS&cuenta=" + parm,
                success: function(data) {

                    response($.map(data, function(item) {
                        return {
                            label: item.cuenta + ' ' + item.nombre_corto,
                            value: item.cuenta,
                        }
                    }));
                },
                error: function(error) {
                    alert('error: ' + error);
                }
            });
        },
        minLength: 1
    });

} 
