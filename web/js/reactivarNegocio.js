/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $("#fecha_ini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    $("#fecha_fin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    $('#ui-datepicker-div').css('clip', 'auto');

    $('#buscar').click(function () {
        mostrarPresolicitud();
    });

    $('#limpiar').click(function () {
        $("#identificacion").val('');
        $("#numero_solicitud").val('');
        $("#fecha_ini").val('');
        $("#fecha_fin").val('');
    });



});

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function mostrarPresolicitud() {
    console.log('Entra aqui');
    var numero_solicitud = $('#numero_solicitud').val();
    var identificacion = $('#identificacion').val();
    var fecha_ini = $('#fecha_ini').val();
    var fecha_fin = $('#fecha_fin').val();
    console.log(fecha_ini + ' ' + fecha_fin)
    //alert(fecha_ini);
    if (numero_solicitud !== '' || identificacion !== '' || fecha_ini !== '' && fecha_fin !== '') {
        var grid_tabla = $("#tabla_reactivarNegocio");
        if ($("#gview_tabla_reactivarNegocio").length) {
            reloadGridMostrar(grid_tabla, 22, numero_solicitud, identificacion, fecha_ini, fecha_fin);
        } else {

            grid_tabla.jqGrid({
                caption: "Reactivar Negocio",
                url: "./controller?estado=Admin&accion=Fintra",
                mtype: "POST",
                datatype: "json",
                height: '100',
                width: '880',
                colNames: ['Solicitud', 'Fecha', 'Monto', 'Identificacion', 'Nombre', 'Apellido', 'Causal', 'Acciones'],
                colModel: [
                    {name: 'numero_solicitud', index: 'numero_solicitud', width: 70, sortable: true, align: 'left', hidden: false, key: true},
                    {name: 'fecha_credito', index: 'fecha_credito', width: 70, sortable: true, align: 'left', hidden: false, search: true},
                    {name: 'monto_credito', index: 'monto_credito', width: 70, sortable: true, align: 'left', hidden: false, search: true},
                    {name: 'identificacion', index: 'identificacion', width: 70, sortable: true, align: 'left', hidden: false, search: true},
                    {name: 'primer_nombre', index: 'primer_nombre', width: 70, sortable: true, align: 'left', hidden: false, search: true},
                    {name: 'primer_apellido', index: 'primer_apellido', width: 70, sortable: true, align: 'left', hidden: false, search: true},
                    {name: 'comentario', index: 'comentario', width: 300, sortable: true, align: 'left', hidden: false, search: true},
                    {name: 'acciones', index: 'acciones', width: 80, sortable: true, align: 'left', hidden: false, search: false}
                ],
                rownumbers: true,
                rownumWidth: 25,
                rowNum: 1000,
                rowTotal: 1000,
                loadonce: true,
                gridview: true,
                viewrecords: false,
                hidegrid: false,
                shrinkToFit: false,
                footerrow: false,
                pager: '#pager',
                multiselect: false,
                multiboxonly: false,
                pgtext: null,
                pgbuttons: false,
                jsonReader: {
                    root: "json",
                    repeatitems: false,
                    id: "0"
                }, ajaxGridOptions: {
                    dataType: "json",
                    type: "get",
                    data: {
                        opcion: 22
                        , numero_solicitud: numero_solicitud
                        , identificacion: identificacion
                        , fecha_ini: fecha_ini
                        , fecha_fin: fecha_fin

                    }
                },
                loadError: function (xhr, status, error) {
                    mensajesDelSistema(error, 250, 150);

                }, loadComplete: function (id, rowid) {

                },
                gridComplete: function (index) {

                    var registros = $("#tabla_reactivarNegocio").getGridParam('records');
                    var cant = jQuery("#tabla_reactivarNegocio").jqGrid('getDataIDs');
                    if (registros == 0) {
                        mensajesDelSistema("No se encontraron negocios por reactivar", '230', '150', false);
                    }
                    else
                    {
                        for (var i = 0; i < cant.length; i++) {
                            var cl = cant[i];
                            //var reactivar = $("#tabla_reactivarNegocio").getRowData(cant[i]).cambio;
                            be = '<input style="height:20px;width:68px;margin-left: 8px;margin-right: 8px;" type="button" title="Reactivar" value="Reactivar" onclick="ventanaReactivar(' + cl + ')" />';
                            jQuery("#tabla_reactivarNegocio").jqGrid('setRowData', cant[i], {acciones: be});
                        }
                    }
                }


            }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
            });
//        $("#tabla_reactivarNegocio").navButtonAdd('#pager', {
//            caption: "Todos",
//            onClickButton: function () {
//                operacion = 'Reactivar';
//                ventanaReactivar(operacion);
//            }
//        });
        }
    } else {
        mensajesDelSistema("Diligencie uno de los campos del filtro", '230', '150', false);
    }
}

function reloadGridMostrar(grid_tabla, op, numero_solicitud, identificacion, fecha_ini, fecha_fin) {

    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            async: false,
            type: "POST",
            data: {
                opcion: 22
                , numero_solicitud: numero_solicitud
                , identificacion: identificacion
                , fecha_ini: fecha_ini
                , fecha_fin: fecha_fin
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function ventanaReactivar(numero_solicitud) {
    $("#num_solicitud").val(numero_solicitud);
    //$("#identificacion").val(identificacion);
    //$("#comentario").val(comentario);
    $("#reactivar").dialog({
        width: '500',
        height: '180',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'REACTIVAR',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Reactivar": function () {
                reactivarNegocio();
            },
            "Salir": function () {
                $(this).dialog("close");
                $("#num_solicitud").val('');
                // $("#identificacion").val('');
                // $("#comentario").val('');
            }
        }
    });
    //}
}

function reactivarNegocio() {
    var numero_solicitud = document.getElementById('num_solicitud').value;
    //var numero_solicitud = $("#numero_solicitud").val();
    //var identificacion = $("#identificacion").val();
    //var comentario = $("#comentario").val();
    var identificacion = document.getElementById('identificacion').value;
    var comentario = document.getElementById('comentario').value;
    //if (convenio !== '' && tasa !== '') {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        data: {
            opcion: 23
            , numero_solicitud: numero_solicitud
            , identificacion: identificacion
            , comentario: comentario
        },
        success: function (data, textStatus, jqXHR) {
            if (data.respuesta === 'Guardado') {
                mensajesDelSistema2("Exito al reactivar", '230', '150', true);
            }
        }, error: function (result) {
            alert('No se puede reactivar, intente nuevamente');
        }
    });
    /*} else {
     mensajesDelSistema("Falta digitar informacion", '230', '150', false);
     }*/

}

function mensajesDelSistema2(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $("#reactivar").dialog('close');
                mostrarPresolicitud();
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}