/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var idclie='';
var fechai='';
var fechaf='';
var ms='';
var factura='';
var nota='';
var temp=';;;;;;;;;;';
var param='';
function init(){
    Calendar.setup({
        inputField : "fechai",
        trigger    : "calendar-triggeri",
        onSelect   : function() { this.hide() }
    });
    Calendar.setup({
        inputField : "fechaf",
        trigger    : "calendar-triggerf",
        onSelect   : function() { this.hide() }
    });
}

function enviar(url,parametro,formx){
    var p="";param=parametro;
    if(parametro=='BUSCAR_INTERESES'){
        if(($('fechai').value=='' && $('fechaf').value=='')||($('fechai').value!='' && $('fechaf').value!=''))
        {   openInfoDialog('<b> La busqueda se esta realizando. <br/>Por favor espere...</b>');
            p="parametro="+parametro+"&idclie="+$('idclie').value+"&ms="+$('ms').value+"&fechai="+$('fechai').value+"&fechaf="+$('fechaf').value+"&factura="+$('factura').value+"&nota="+$('nota').value;
            idclie=$('idclie').value;
            fechai=$('fechai').value;
            fechaf=$('fechaf').value;
            factura=$('factura').value;
            nota=$('nota').value;
            ms=$('ms').value;
            new Ajax.Request(
            url,
            {   method: 'post',
                parameters: p,
                onComplete: showTable
            });
        }
        else{
            Dialog.alert("Debe determinar bien el rango de las fechas ...", {
                width:250,
                height:100,
                windowParameters: {className: "alphacube"}
            });
        }
    }
    else{
        if(parametro=='PAGINACION')
        {   openInfoDialog('<b> La busqueda se esta realizando. <br/>Por favor espere...</b>');
            p="parametro="+parametro;
            new Ajax.Request(
            url,
            {   method: 'post',
                parameters: p,
                onComplete: showTable
            });
        }
        else{
            if(parametro=='ANULAR_INTERESES'||parametro=='IGNORAR_INTERESES'||parametro=='FACTURAR_INTERESES'){
                sw=0;
                p="parametro="+parametro+"&idclie="+idclie+"&ms="+ms+"&fechai="+fechai+"&fechaf="+fechaf+"&factura="+factura+"&nota="+nota;
                if (typeof formx != 'undefined') {
                    for (i=0;i<$(formx).length;i++){
                        if ($(formx).elements[i].type=='checkbox' && $(formx).elements[i].name=='fact' && $(formx).elements[i].checked==true){
                            p=p+"&fact="+$(formx).elements[i].value;sw=1;
                        }
                    }
                }
                if(sw==1){
                    openInfoDialog('<b> El proceso ha comenzado. <br/>Por favor espere...</b>');
                    new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onComplete: showTable
                    });
                }
                else{
                    Dialog.alert("Debe escojer almenos una factura para iniciar el proceso...", {
                        width:250,
                        height:100,
                        windowParameters: {className: "alphacube"}
                    });
                }
            }
            else
            {   if(parametro=='EXPORTAR_INTERESES'){
                    openInfoDialog('<b> El proceso se esta ejecutando. <br/>Por favor espere...</b>');
                    p="parametro="+parametro;
                    new Ajax.Request(
                    url,
                    {   method: 'post',
                        parameters: p,
                        onComplete: showTableII
                    });
                }

            }
        }
    }
}

function showTable (response){
    formx='formulario';sw=0;
    Dialog.closeInfo();
    $('tabla_generica').innerHTML=response.responseText;
    if (typeof formx != 'undefined') {
        for (i=0;i<$(formx).length && sw==0;i++){
            if ($(formx).elements[i].type=='checkbox' && $(formx).elements[i].name=='fact'){
                sw=1;
            }
        }
    }
    if(sw==0){
        Dialog.alert("La consulta no arrojo resultados...", {
            width:250,
            height:100,
            windowParameters: {className: "alphacube"}
        });
    }
    if(param=='FACTURAR_INTERESES'){
        Dialog.alert("El proceso ha terminado por favor verifique el log de procesos...", {
            width:250,
            height:100,
            windowParameters: {className: "alphacube"}
        });
    }
}

function showTableII (response){
    Dialog.closeInfo();
    Dialog.alert(response.responseText, {
        width:250,
        height:100,
        windowParameters: {className: "alphacube"}
    });
}

function openInfoDialog(mensaje) {
    Dialog.info(mensaje, {
        width:250,
        height:100,
        showProgress: true,
        windowParameters: {className: "alphacube"}
    });
}

function sendAction(url,filtro,nomselect,def){
    if(temp!=filtro){
        $('imgworking').style.visibility="visible";
        temp=filtro;
        var p = "filtro=" + filtro + "&nomselect=" + nomselect  + "&opcion=clselect" + "&idclie="+def;

        new Ajax.Request(
        url,
        {
            method: 'get',
            parameters: p,
            onSuccess: deploySelect
        });

    }
}

function deploySelect(response){
    respuesta=response.responseText.split(';;;;;;;;;;');
    if(respuesta[0]==$('nomclie').value){
        document.getElementById("cliselect").innerHTML='&nbsp;'+respuesta[1];
        $('imgworking').style.visibility="hidden";
    }
}
function color_fila(id,color){
    $(id).style.backgroundColor=color;
}
function Sell_all_col(nombre){
    theForm=$('formulario');
    //alert("nombre"+nombre+"theform"+theForm);
    for (i=0;i<theForm.length;i++){
        if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name==nombre){
            theForm.elements[i].checked=theForm.All.checked;
        }
    }
}



