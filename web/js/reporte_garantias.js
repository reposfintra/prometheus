/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
   cargarComboGenerico('unidad_negocio', 5);
   cargarComboGenerico('empresa_fianza', 30);
   maximizarventana();
   
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
});

function initCartera(){
    $('#listarNegocios').click(function () {
       if($('#unidad_negocio').val()===''){
            mensajesDelSistema('Por favor, seleccione la unidad de negocio', '250', '150');
            return;
       }else if($('#empresa_fianza').val()===''){
            mensajesDelSistema('Por favor, seleccione la empresa', '250', '150');
            return;
//       }else if($('#periodo_corte').val()===''){
//            mensajesDelSistema('Por favor, ingrese el periodo de vencimiento', '250', '150');
//            return;
       }else{
           generarReporteCartera();
       }
    });
}

function initDesembolsos(){
    $('#listarNegocios').click(function () {
       if($('#unidad_negocio').val()===''){
            mensajesDelSistema('Por favor, seleccione la unidad de negocio', '250', '150');
            return;
       }else if($('#empresa_fianza').val()===''){
            mensajesDelSistema('Por favor, seleccione la empresa', '250', '150');
            return;
       }else if($('#periodo_corte').val()===''){
            mensajesDelSistema('Por favor, ingrese el periodo de vencimiento', '250', '150');
            return;
       }else{
           generarReporteDesembolsos();
       }
    });
}

function initReclamaciones(){
    $('#listarNegocios').click(function () {
       if($('#unidad_negocio').val()===''){
            mensajesDelSistema('Por favor, seleccione la unidad de negocio', '250', '150');
            return;
       }else if($('#empresa_fianza').val()===''){
            mensajesDelSistema('Por favor, seleccione la empresa', '250', '150');
            return;
       }else if($('#periodo_corte').val()===''){
            mensajesDelSistema('Por favor, ingrese el periodo de vencimiento', '250', '150');
            return;
       }else{
           generarReporteReclamaciones();
       }
    });
}

function initPrepagos(){
    $('#listarNegocios').click(function () {
       if($('#unidad_negocio').val()===''){
            mensajesDelSistema('Por favor, seleccione la unidad de negocio', '250', '150');
            return;
       }else if($('#empresa_fianza').val()===''){
            mensajesDelSistema('Por favor, seleccione la empresa', '250', '150');
            return;
       }else if($('#periodo_corte').val()===''){
            mensajesDelSistema('Por favor, ingrese el periodo de vencimiento', '250', '150');
            return;
       }else{
           generarReportePrepagos();
       }
    });
}

function initRecuperaciones(){
    $('#listarNegocios').click(function () {
       if($('#unidad_negocio').val()===''){
            mensajesDelSistema('Por favor, seleccione la unidad de negocio', '250', '150');
            return;
       }else if($('#empresa_fianza').val()===''){
            mensajesDelSistema('Por favor, seleccione la empresa', '250', '150');
            return;
       }else if($('#periodo_corte').val()===''){
            mensajesDelSistema('Por favor, ingrese el periodo de vencimiento', '250', '150');
            return;
       }else{
           generarReporteRecuperaciones(); 
       }
    });     
}

function initResumenIndemnizacion(){
    $('#listarNegocios').click(function () {
        if($('#periodo_corte').val()===''){
            mensajesDelSistema('Por favor, ingrese el periodo a consultar', '250', '150');
            return;
       }else{
           generarResumenIndemnizacion();
       }
    });
}

function initVistoBueno(){
    $('#listarNegocios').click(function () {
       if($('#unidad_negocio').val()===''){
            mensajesDelSistema('Por favor, seleccione la unidad de negocio', '250', '150');
            return;
       }/*else if($('#empresa_fianza').val()===''){
            mensajesDelSistema('Por favor, seleccione la empresa', '250', '150');
            return;
       }*/else{
           cargarNegociosPendientesVistoBueno(); 
       }
    });     
}

function cargarComboGenerico(idCombo, option) {
    $('#'+idCombo).html('');
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Garantias&accion=Comunitarias",
        dataType: 'json',
        async:false,
        data: {
            opcion: option
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#'+idCombo).append("<option value=''>Seleccione</option>");
                    for (var key in json) {              
                       $('#'+idCombo).append('<option value=' + key + '>' + json[key] + '</option>');                
                    }
                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#'+idCombo).append("<option value=''>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function generarReporteCartera() {      
    var grid_tbl_reporte_cartera = jQuery("#tabla_reporte_cartera");
     if ($("#gview_tabla_reporte_cartera").length) {
        refrescarGridReporteCartera();
     }else {
        grid_tbl_reporte_cartera.jqGrid({
            caption: "REPORTE DE CARTERA",
            url: "./controller?estado=Garantias&accion=Comunitarias",           	 
            datatype: "json",  
            height: '465',
            width: '1150',           
            colNames: ['C.C/DNI/NIT/RUC del Cliente','N�mero de pagar�','N�mero de Cr�dito/Ref', 'Valor/Importe Saldo capital', 'Valor/Importe Intereses corrientes', 'Valor/Importe Intereses mora', 'Fecha de corte'],
            colModel: [               
                {name: 'id_cliente', index: 'id_cliente', width: 165, align: 'left'},         
                {name: 'num_pagare', index: 'num_pagare', width: 130, align: 'center'},               
                {name: 'num_credito', index: 'num_credito', width: 140, align: 'center', key:true},
                {name: 'vlr_capital', index: 'vlr_capital', sortable: true, width: 160, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes_corriente', index: 'interes_corriente', sortable: true, width: 175, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes_mora', index: 'interes_mora', sortable: true, width: 170, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'fecha_corte', index: 'fecha_corte', width: 120, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_reporte_cartera'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion:20,
                       id_unidad_negocio:$('#unidad_negocio').val(),
                       nit_empresa_fianza:$('#empresa_fianza').val(),
                       periodo_corte:$('#periodo_corte').val()
                     }
            },  
            loadComplete: function () {
                    if (grid_tbl_reporte_cartera.jqGrid('getGridParam', 'records') > 0) {
                        deshabilitarPanelBusqueda(true);
                    }else{
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_reporte_cartera", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        //boton que sirve para desbloquear el panel de buqueda
        $("#tabla_reporte_cartera").navButtonAdd('#page_tabla_reporte_cartera', {
            caption: "Desbloquear",
            title: "desbloquear",
            onClickButton: function (e) {
                deshabilitarPanelBusqueda(false);
                grid_tbl_reporte_cartera.jqGrid("clearGridData", true);
            }
        });
        
        jQuery("#tabla_reporte_cartera").jqGrid("navButtonAdd", "#page_tabla_reporte_cartera", {
            caption: "Exportar excel",
            onClickButton: function() {
                var info = jQuery('#tabla_reporte_cartera').getGridParam('records');
                if (info > 0) {
                    exportarExcel(25,'tabla_reporte_cartera');
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }
            }

        });        
    
    }  
       
}

function refrescarGridReporteCartera(){   
    jQuery("#tabla_reporte_cartera").setGridParam({
        url: "./controller?estado=Garantias&accion=Comunitarias",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: {
                opcion:20,
                id_unidad_negocio:$('#unidad_negocio').val(),
                nit_empresa_fianza:$('#empresa_fianza').val(),
                periodo_corte:$('#periodo_corte').val()
            }
        }       
    });
    
    jQuery('#tabla_reporte_cartera').trigger("reloadGrid");
}

function generarReporteDesembolsos() {      
    var grid_tbl_reporte_desembolsos = jQuery("#tabla_reporte_desembolsos");
     if ($("#gview_tabla_reporte_desembolsos").length) {
        refrescarGridReporteDesembolsos();
     }else {
        grid_tbl_reporte_desembolsos.jqGrid({
            caption: "REPORTE DESEMBOLSOS CON GARANT�A",
            url: "./controller?estado=Garantias&accion=Comunitarias",           	 
            datatype: "json",  
            height: '475',
            width: '1350',           
            colNames: ['Nro. de Cr�dito/Ref', 'Numero de Convenio', 'Linea de cr�dito', 'Nombre de la entidad', 'Nit/RUC de la Entidad', 'Sucursal', 'Mes de desembolso', 'Nombre Persona Natural/Jur�dica', 'Apellidos Persona Natural/Jur�dica', 
                       'C.C/DNI/NIT/RUC del Cliente', 'Sucursal de desembolso', 'Tel�fono del cliente', 'Direcci�n del cliente', 'Ciudad/Provincia/Municipio', 'Departamento/Regi�n', 'Celular del cliente', 'N�mero de Pagar�', 'Valor/Importe del cr�dito', 
                       'Destino/Tipo Cr�dito', 'Valor/Importe Cuota', 'Plazo(meses)', 'Fecha Desembolso', 'Fecha Vencimiento', 'Correo electr�nico Cliente', 'Periodo gracia', 'Amortizaci�n capital', 'Amortizaci�n periodo', 'Amortizaci�n modalidad', 
                       'Tasa inter�s', 'Porcentaje comisi�n', 'Modalidad comisi�n', 'Tipo comisi�n', 'Valor/Importe Comisi�n','Valor/Importe Impuesto de Comisi�n', 'Valor/Importe Total Comisi�n', 'Especificaci�n de la Garant�a', 
                       'Garantia adicional Nombre persona natural/Jur�dica', 'Garantia adicional Apellidos persona', 'Garant�a C.C/DNI/NIT/RUC', 'Garantia adicional Sucursal persona', 'Garantia adicional Tel�fono', 'Garantia adicional Direcci�n', 
                       'Garant�a Provincia/Ciudad/Municipio', 'Garantia adicional Departamento/Regi�n', 'Observaciones'],
            colModel: [                              
                {name: 'num_credito', index: 'num_credito', width: 120, align: 'center', key:true},
                {name: 'convenio', index: 'convenio', width: 120, align: 'center'},
                {name: 'linea_credito', index: 'linea_credito', width: 110, align: 'center'},
                {name: 'entidad_desembolso', index: 'entidad_desembolso', width: 140, align: 'left'},
                {name: 'nit_entidad', index: 'nit_entidad', width: 140, align: 'left'},   
                {name: 'sucursal', index: 'sucursal', width: 110, align: 'center'},                   
                {name: 'mes_desembolso', index: 'mes_desembolso', width: 115, align: 'center'},  
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 190, align: 'left'},
                {name: 'apellidos_cliente', index: 'apellidos_cliente', width: 190, align: 'left'},
                {name: 'id_cliente', index: 'id_cliente', width: 160, align: 'left'},         
                {name: 'sucursal_desembolso', index: 'sucursal_desembolso', width: 140, align: 'center'},              
                {name: 'telefono', index: 'telefono', width: 110, align: 'left'}, 
                {name: 'direccion', index: 'direccion', width: 160, align: 'left'}, 
                {name: 'ciudad', index: 'ciudad', width: 150, align: 'left'},    
                {name: 'departamento', index: 'departamento', width: 150, align: 'left'},    
                {name: 'celular', index: 'celular', width: 110, align: 'left'},    
                {name: 'num_pagare', index: 'num_pagare', width: 120, align: 'center'},
                {name: 'vlr_credito', index: 'vlr_credito', sortable: true, width: 140, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'tipo_credito', index: 'tipo_credito', width: 120, align: 'left'},
                {name: 'vlr_cuota', index: 'vlr_cuota', sortable: true, width: 120, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'plazo', index: 'plazo', width: 90, align: 'center'}, 
                {name: 'fecha_desembolso', index: 'fecha_desembolso', width: 120, align: 'center'},
                {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 120, align: 'center'},
                {name: 'email', index: 'email', width: 160, align: 'left'},
                {name: 'periodo_gracia', index: 'periodo_gracia', width: 110, align: 'center'},
                {name: 'amortizacion_capital', index: 'amortizacion_capital', width: 120, align: 'center'},
                {name: 'amortizacion_periodo', index: 'amortizacion_periodo', width: 120, align: 'center'},
                {name: 'amortizacion_modalidad', index: 'amortizacion_modalidad', width: 130, align: 'center'},
                {name: 'tasa_interes', index: 'tasa_interes', width: 80, align: 'right'},
                {name: 'porc_comision', index: 'porc_comision', width: 120, align: 'right'},
                {name: 'modalidad_comision', index: 'modalidad_comision', width: 120, align: 'center'},
                {name: 'tipo_comision', index: 'tipo_comision', width: 110, align: 'center'},
                {name: 'vlr_comision', index: 'vlr_comision', sortable: true, width: 130, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'vlr_impuesto', index: 'vlr_impuesto', width: 130, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total_comision', index: 'total_comision', sortable: true, width: 150, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'especif_garantia', index: 'especif_garantia', width: 160, align: 'left'},
                {name: 'nombre_coodeudor', index: 'nombre_coodeudor', width: 190, align: 'left'},
                {name: 'apellidos_coodeudor', index: 'apellidos_coodeudor', width: 190, align: 'left'},
                {name: 'id_coodeudor', index: 'id_coodeudor', width: 160, align: 'left'},         
                {name: 'sucursal_coodeudor', index: 'sucursal_coodeudor', width: 130, align: 'center'},              
                {name: 'telefono_coodeudor', index: 'telefono_coodeudor', width: 110, align: 'left'}, 
                {name: 'direccion_coodeudor', index: 'direccion_coodeudor', width: 160, align: 'left'}, 
                {name: 'ciudad_coodeudor', index: 'ciudad_coodeudor', width: 120, align: 'left'},    
                {name: 'departamento_coodeudor', index: 'departamento_coodeudor', width: 120, align: 'left'},  
                {name: 'observaciones', index: 'observaciones', width: 170, align: 'left'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_reporte_desembolsos'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                        opcion:21,
                        id_unidad_negocio:$('#unidad_negocio').val(),
                        nit_empresa_fianza:$('#empresa_fianza').val(),
                        periodo_corte:$('#periodo_corte').val()
                     }
            },  
            loadComplete: function () {
                    if (grid_tbl_reporte_desembolsos.jqGrid('getGridParam', 'records') > 0) {
                        deshabilitarPanelBusqueda(true);
                    }else{
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_reporte_desembolsos", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        //boton que sirve para desbloquear el panel de buqueda
        $("#tabla_reporte_desembolsos").navButtonAdd('#page_tabla_reporte_desembolsos', {
            caption: "Desbloquear",
            title: "desbloquear",
            onClickButton: function (e) {
                deshabilitarPanelBusqueda(false);
                grid_tbl_reporte_desembolsos.jqGrid("clearGridData", true);
            }
        });
        
        jQuery("#tabla_reporte_desembolsos").jqGrid("navButtonAdd", "#page_tabla_reporte_desembolsos", {
            caption: "Exportar excel",
            onClickButton: function() {
                var info = jQuery('#tabla_reporte_desembolsos').getGridParam('records');
                if (info > 0) {
                    exportarExcel(26,'tabla_reporte_desembolsos');
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }
            }

        });
       
    }  
       
}

function refrescarGridReporteDesembolsos(){   
    jQuery("#tabla_reporte_desembolsos").setGridParam({
        url: "./controller?estado=Garantias&accion=Comunitarias",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: {
                    opcion:21,
                    id_unidad_negocio:$('#unidad_negocio').val(),
                    nit_empresa_fianza:$('#empresa_fianza').val(),
                    periodo_corte:$('#periodo_corte').val()
            }
        }       
    });
    
    jQuery('#tabla_reporte_desembolsos').trigger("reloadGrid");
}

function generarReporteReclamaciones() {      
    var grid_tbl_reporte_reclamaciones = jQuery("#tabla_reporte_reclamaciones");
     if ($("#gview_tabla_reporte_reclamaciones").length) {
        refrescarGridReporteReclamaciones();
     }else {
        grid_tbl_reporte_reclamaciones.jqGrid({
            caption: "REPORTE DE RECLAMACIONES CON GARANT�A",
            url: "./controller?estado=Garantias&accion=Comunitarias",           	 
            datatype: "json",  
            height: '350',
            width: '950',           
            colNames: ['Nit Entidad', 'Id Cliente', 'No Pagar�', 'No Cr�dito', 'Tipo Reclamaci�n', 'F. Inicio Mora', 'D�as Mora', 'Valor Capital', 'Intereses Corriente', 'Intereses Mora', 'No Cuotas'],
            colModel: [              
                {name: 'nit_entidad', index: 'nit_entidad', width: 110, align: 'left'},    
                {name: 'nit_cliente', index: 'nit_cliente', width: 110, align: 'left'},    
                {name: 'num_pagare', index: 'num_pagare', width: 90, align: 'center'},               
                {name: 'num_credito', index: 'num_credito', width: 110, align: 'center', key:true},
                {name: 'tipo_reclamacion', index: 'tipo_reclamacion', width: 110, align: 'center'},
                {name: 'fecha_inicio_mora', index: 'fecha_inicio_mora', width: 120, align: 'center'},
                {name: 'dias_mora', index: 'dias_mora', width: 90, align: 'center'},
                {name: 'vlr_capital', index: 'vlr_capital', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes_corriente', index: 'interes_corriente', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes_mora', index: 'interes_mora', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'num_cuotas', index: 'num_cuotas', width: 90, align: 'center'}
                
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_reporte_reclamaciones'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                        opcion: 22,
                        id_unidad_negocio: $('#unidad_negocio option:selected').text(),
                        nit_empresa_fianza: $('#empresa_fianza').val(),
                        periodo_corte: $('#periodo_corte').val()
                     }
            }, 
            loadComplete: function () {
                    if (grid_tbl_reporte_reclamaciones.jqGrid('getGridParam', 'records') > 0) {
                        deshabilitarPanelBusqueda(true);
                    }else{
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_reporte_reclamaciones", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        //boton que sirve para desbloquear el panel de buqueda
        $("#tabla_reporte_reclamaciones").navButtonAdd('#page_tabla_reporte_reclamaciones', {
            caption: "Desbloquear",
            title: "desbloquear",
            onClickButton: function (e) {
                deshabilitarPanelBusqueda(false);
                grid_tbl_reporte_reclamaciones.jqGrid("clearGridData", true);
            }
        });
        
        jQuery("#tabla_reporte_reclamaciones").jqGrid("navButtonAdd", "#page_tabla_reporte_reclamaciones", {
            caption: "Exportar excel",
            onClickButton: function() {
                var info = jQuery('#tabla_reporte_reclamaciones').getGridParam('records');
                if (info > 0) {
                    exportarExcel(27,'tabla_reporte_reclamaciones');
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }
            }

        });
        
        $("#tabla_reporte_reclamaciones").navButtonAdd('#page_tabla_reporte_reclamaciones', {
            caption: "Generar Notificaci�n",
            title: "Generar Notificaci�n a Cliente",
            onClickButton: function (e) {
                generarNotificacion('tabla_reporte_reclamaciones');
            }
        });
    }  
       
}

function refrescarGridReporteReclamaciones(){   
    jQuery("#tabla_reporte_reclamaciones").setGridParam({
        url: "./controller?estado=Garantias&accion=Comunitarias",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: { 
                opcion:22,
                id_unidad_negocio: $('#unidad_negocio option:selected').text(),
                nit_empresa_fianza: $('#empresa_fianza').val(),
                periodo_corte: $('#periodo_corte').val()
            }
        }       
    });
    
    jQuery('#tabla_reporte_reclamaciones').trigger("reloadGrid");
}

function generarReportePrepagos() {      
    var grid_tbl_reporte_prepagos = jQuery("#tabla_reporte_prepagos");
     if ($("#gview_tabla_reporte_prepagos").length) {
        refrescarGridReportePrepagos();
     }else {
        grid_tbl_reporte_prepagos.jqGrid({
            caption: "REPORTE PREPAGOS CON GARANT�A",
            url: "./controller?estado=Garantias&accion=Comunitarias",           	 
            datatype: "json",  
            height: '350',
            width: '650',           
            colNames: ['Nit Entidad', 'Id Cliente', 'No Pagar�', 'No Cr�dito', 'Fecha Prepago'],
            colModel: [               
                {name: 'nit_entidad', index: 'nit_entidad', width: 110, align: 'left'},    
                {name: 'id_cliente', index: 'id_cliente', width: 110, align: 'left'},    
                {name: 'num_pagare', index: 'num_pagare', width: 90, align: 'center'},               
                {name: 'num_credito', index: 'num_credito', width: 110, align: 'center', key:true},                
                {name: 'fecha_prepago', index: 'fecha_prepago', width: 120, align: 'center'}  
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_reporte_prepagos'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                        opcion: 23,
                        id_unidad_negocio: $('#unidad_negocio').val(),
                        nit_empresa_fianza: $('#empresa_fianza').val(),
                        periodo_corte: $('#periodo_corte').val()
                     }
            }, 
            loadComplete: function () {
                    if (grid_tbl_reporte_prepagos.jqGrid('getGridParam', 'records') > 0) {
                        deshabilitarPanelBusqueda(true);
                    }else{
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_reporte_prepagos", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        //boton que sirve para desbloquear el panel de buqueda
        $("#tabla_reporte_prepagos").navButtonAdd('#page_tabla_reporte_prepagos', {
            caption: "Desbloquear",
            title: "desbloquear",
            onClickButton: function (e) {
                deshabilitarPanelBusqueda(false);
                grid_tbl_reporte_prepagos.jqGrid("clearGridData", true);
            }
        });
        
        jQuery("#tabla_reporte_prepagos").jqGrid("navButtonAdd", "#page_tabla_reporte_prepagos", {
            caption: "Exportar excel",
            onClickButton: function() {
                var info = jQuery('#tabla_reporte_prepagos').getGridParam('records');
                if (info > 0) {
                    exportarExcel(28,'tabla_reporte_prepagos');
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }
            }

        });
    }  
       
}

function refrescarGridReportePrepagos(){   
    jQuery("#tabla_reporte_prepagos").setGridParam({
        url: "./controller?estado=Garantias&accion=Comunitarias",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: { 
                opcion:23,
                id_unidad_negocio: $('#unidad_negocio').val(),
                nit_empresa_fianza: $('#empresa_fianza').val(),
                periodo_corte: $('#periodo_corte').val()
            }
        }       
    });
    
    jQuery('#tabla_reporte_prepagos').trigger("reloadGrid");
}

function generarReporteRecuperaciones() {      
    var grid_tbl_reporte_recuperaciones = jQuery("#tabla_reporte_recuperaciones");
     if ($("#gview_tabla_reporte_recuperaciones").length) {
        refrescarGridReporteRecuperaciones();
     }else {
        grid_tbl_reporte_recuperaciones.jqGrid({
            caption: "REPORTE RECUPERACIONES CON GARANT�A",
            url: "./controller?estado=Garantias&accion=Comunitarias",           	 
            datatype: "json",  
            height: '350',
            width: '975',           
            colNames: ['No Pagar�', 'No Cr�dito', 'Fecha Pago', 'Valor Recuperado', 'Valor Transferir GC', 'Valor GC', 'Valor Entidad', 'Valor Reserva Entidad'],
            colModel: [              
                {name: 'num_pagare', index: 'num_pagare', width: 90, align: 'center'},               
                {name: 'num_credito', index: 'num_credito', width: 110, align: 'center', key:true},                
                {name: 'fecha_pago', index: 'fecha_pago', width: 120, align: 'center'},
                {name: 'vlr_recuperado', index: 'vlr_recuperado', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'vlr_transferir_gc', index: 'vlr_transferir_gc', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'vlr_gc', index: 'vlr_gc', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'vlr_entidad', index: 'vlr_entidad', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'vlr_reserva_entidad', index: 'vlr_reserva_entidad', sortable: true, width: 130, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_reporte_recuperaciones'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion:24
                     }
            },   
            loadComplete: function () {
                    if (grid_tbl_reporte_recuperaciones.jqGrid('getGridParam', 'records') > 0) {
                        deshabilitarPanelBusqueda(true);
                    }else{
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_reporte_recuperaciones", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        //boton que sirve para desbloquear el panel de buqueda
        $("#tabla_reporte_recuperaciones").navButtonAdd('#page_tabla_reporte_recuperaciones', {
            caption: "Desbloquear",
            title: "desbloquear",
            onClickButton: function (e) {
                deshabilitarPanelBusqueda(false);
                grid_tbl_reporte_recuperaciones.jqGrid("clearGridData", true);
            }
        });
        
        jQuery("#tabla_reporte_recuperaciones").jqGrid("navButtonAdd", "#page_tabla_reporte_recuperaciones", {
            caption: "Exportar excel",
            onClickButton: function() {
                var info = jQuery('#tabla_reporte_recuperaciones').getGridParam('records');
                if (info > 0) {
                    exportarExcel(29,'tabla_reporte_recuperaciones');
                } else {
                    mensajesDelSistema("No hay informacion que exportar", '250', '150');
                }
            }

        });
    }  
       
}

function refrescarGridReporteRecuperaciones(){   
    jQuery("#tabla_reporte_recuperaciones").setGridParam({
        url: "./controller?estado=Garantias&accion=Comunitarias",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: { 
                opcion:24
            }
        }       
    });
    
    jQuery('#tabla_reporte_recuperaciones').trigger("reloadGrid");
}

function generarResumenIndemnizacion(){

    var grid_tbl_resumen_indemnizados = $("#tabla_negocios_indemnizados");
    if ($("#gview_tabla_negocios_indemnizados").length) {
         refrescarGridResumenIndemnizacion();
     }else {
         grid_tbl_resumen_indemnizados.jqGrid({        
            caption:'RESUMEN NEGOCIOS INDEMNIZADOS',
            url: "./controller?estado=Garantias&accion=Comunitarias",
            mtype: "POST",
            datatype: "json",
            height: '380',
            width: '510',
            colNames: ['Negocios Indemnizados', 'Valor Indemnizado', 'Linea'],
            colModel: [                           
                {name: 'negocios_indemnizados', index: 'negocios_indemnizados', resizable:false, sortable: true, width: 140, align: 'center'},             
                {name: 'valor_indemnizado', index: 'valor_indemnizado', sortable: true, width: 140, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'linea_negocio', index: 'linea_negocio', resizable:false, sortable: true, width: 180, align: 'left'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            restoreAfterError: true,
            pager:'#page_tabla_negocios_indemnizados',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {                
                dataType: "json",
                type: "POST",  
                async:false,
                data: {
                    opcion: 19,                    
                    periodo_corte:$('#periodo_corte').val()
                }
            },  
            gridComplete: function() {             
                var colSumNeg = jQuery("#tabla_negocios_indemnizados").jqGrid('getCol', 'negocios_indemnizados', false, 'sum');
                var colSumTotal = jQuery("#tabla_negocios_indemnizados").jqGrid('getCol', 'valor_indemnizado', false, 'sum'); 
                jQuery("#tabla_negocios_indemnizados").jqGrid('footerData', 'set', {negocios_indemnizados: colSumNeg});
                jQuery("#tabla_negocios_indemnizados").jqGrid('footerData', 'set', {valor_indemnizado: colSumTotal}); 
            },    
            loadComplete: function () {
                    if (grid_tbl_resumen_indemnizados.jqGrid('getGridParam', 'records') > 0) {
                        deshabilitarPanelBusqueda(true);
                    }else{
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_negocios_indemnizados",{search:false,refresh:false,edit:false,add:false,del:false});           
        
         //boton que sirve para desbloquear el panel de buqueda
        $("#tabla_negocios_indemnizados").navButtonAdd('#page_tabla_negocios_indemnizados', {
            caption: "Desbloquear",
            title: "desbloquear",
            onClickButton: function (e) {
                deshabilitarPanelBusqueda(false);
                grid_tbl_resumen_indemnizados.jqGrid("clearGridData", true);
            }
        });

     }
}


function refrescarGridResumenIndemnizacion(){    
    jQuery("#tabla_negocios_indemnizados").setGridParam({
        url: "./controller?estado=Garantias&accion=Comunitarias",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async:false,
            data: {
                opcion: 19,
                periodo_corte:$('#periodo_corte').val()
            }
        }
    }).trigger("reloadGrid");
}

function  exportarExcel(op, tableid) {
    var fullData = jQuery("#"+tableid).jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: './controller?estado=Garantias&accion=Comunitarias',
        data: {
            listado: myJsonString,
            opcion: op
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function deshabilitarPanelBusqueda(status) {

    var nodes = document.getElementById("div_filtro_negocios").getElementsByTagName('*');
    for (var i = 0; i < nodes.length; i++)
    {
        nodes[i].disabled = status;
    }

}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}


function generarNotificacion(tableid) {
 
        var url = './controller?estado=Garantias&accion=Comunitarias';
        var rowKey = jQuery("#"+tableid).jqGrid('getGridParam','selrow');
        if (rowKey){
            var nit_cliente = jQuery("#" + tableid).getRowData(rowKey).nit_cliente;
            var negocio = jQuery("#" + tableid).getRowData(rowKey).num_credito;
            var mora = jQuery("#" + tableid).getRowData(rowKey).dias_mora; 
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 18,
                    nit_cliente: nit_cliente,
                    negocio: negocio,
                    mora: mora
                },
                success: function (json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }

                        if (json.respuesta === "OK") {
                            window.open('.' + json.Ruta);
                        }
                    } else {
                        mensajesDelSistema("Lo sentimos ocurri� un error al generar notificaci�n!!", '250', '150');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }else{
            if (jQuery('#'+tableid).jqGrid('getGridParam', 'records')>0) {
                 mensajesDelSistema("Debe seleccionar el cliente al cual va a notificar!!", '250', '150');
            }else{
                 mensajesDelSistema("No hay clientes para notificar", '250', '150');
            }             
        }
 }
 
 function cargarNegociosPendientesVistoBueno(){

    var grid_tbl_visto_bueno = $("#tabla_visto_bueno");
    if ($("#gview_tabla_visto_bueno").length) {
         refrescarGridNegociosPendVistoBueno();
     }else {
         grid_tbl_visto_bueno.jqGrid({        
            caption:'NEGOCIOS PENDIENTES VISTO BUENO FIANZA',
            url: "./controller?estado=Garantias&accion=Comunitarias",
            mtype: "POST",
            datatype: "json",
            height: '450',
            width: '1040',
            colNames: ['Id', 'Nit Cliente', 'Nombre', 'Negocio', 'No Cuotas', 'Convenio', 'Fecha Vencimiento', 'Valor Negocio', 'Valor Desembolso', 'Valor Fianza','Valor Poliza'],
            colModel: [
                {name: 'id', index: 'id', width: 80, resizable:false, sortable: true, align: 'center', key: true, hidden:true},              
                {name: 'nit_cliente', index: 'nit_cliente', resizable:false, sortable: true, width: 90, align: 'left'},
                {name: 'nombre_cliente', index: 'nombre_cliente', resizable:false, sortable: true, width: 190, align: 'left'},             
                {name: 'negocio', index: 'negocio', resizable: false, sortable: true, width: 110, align: 'center'},
                {name: 'plazo', index: 'plazo', resizable: false, sortable: true, width: 80, align: 'center'},
                {name: 'id_convenio', index: 'id_convenio', resizable: false, sortable: true, width: 90, align: 'left', hidden:true},      
                {name: 'fecha_vencimiento', index: 'fecha_vencimiento', resizable: false, sortable: true, width: 110, align: 'left', hidden:true},                       
                {name: 'valor_negocio', index: 'valor_negocio', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_desembolsado', index: 'valor_desembolsado', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_fianza', index: 'valor_fianza', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_total_poliza', index: 'valor_total_poliza', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}  
            ],
            rowNum: 100000,
            rowTotal: 100000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: true,
            restoreAfterError: true,
            pager:'#page_tabla_visto_bueno',
            pgtext: null,
            pgbuttons: false,
            multiselect:true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {                
                dataType: "json",
                type: "POST",  
                async:false,
                data: {
                    opcion: 33,                    
                    id_unidad_negocio:$('#unidad_negocio').val()
                }
            },  
            gridComplete: function() {             
                var colSumTotal = jQuery("#tabla_visto_bueno").jqGrid('getCol', 'valor_negocio', false, 'sum');
                var colSumDesem = jQuery("#tabla_visto_bueno").jqGrid('getCol', 'valor_desembolsado', false, 'sum');  
                var colSumFianza = jQuery("#tabla_visto_bueno").jqGrid('getCol', 'valor_fianza', false, 'sum');  
                jQuery("#tabla_visto_bueno").jqGrid('footerData', 'set', {valor_negocio: colSumTotal});
                jQuery("#tabla_visto_bueno").jqGrid('footerData', 'set', {valor_desembolsado: colSumDesem});                  
                jQuery("#tabla_visto_bueno").jqGrid('footerData', 'set', {valor_fianza: colSumFianza});   
            },    
            loadComplete: function () {
                    if (grid_tbl_visto_bueno.jqGrid('getGridParam', 'records') > 0) {
                        deshabilitarPanelBusqueda(true);
                    }else{
                         mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150', true); 
                    }
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_visto_bueno",{search:false,refresh:false,edit:false,add:false,del:false});           
        
         //boton que sirve para desbloquear el panel de buqueda
        $("#tabla_visto_bueno").navButtonAdd('#page_tabla_visto_bueno', {
            caption: "Desbloquear",
            title: "desbloquear",
            onClickButton: function (e) {
                deshabilitarPanelBusqueda(false);
                grid_tbl_visto_bueno.jqGrid("clearGridData", true);
            }
        });
        
        $("#tabla_visto_bueno").navButtonAdd('#page_tabla_visto_bueno', {
            caption: "Visto Bueno Fianza",
            title: "Otorgar Visto Bueno Fianza",
            onClickButton: function (e) {
               generarVistoBuenoFIanza();
            }
        });
        
        $("#tabla_visto_bueno").navButtonAdd('#page_tabla_visto_bueno', {
            caption: "Visto Bueno Poliza",
            title: "Otorgar Visto Bueno Poliza",
            onClickButton: function (e) {
               generarVistoBuenoPoliza();
            }
        });

     }
}


function refrescarGridNegociosPendVistoBueno(){    
    jQuery("#tabla_visto_bueno").setGridParam({
        url: "./controller?estado=Garantias&accion=Comunitarias",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async:false,
            data: {
                opcion: 33,
                id_unidad_negocio:$('#unidad_negocio').val()
            }
        }
    }).trigger("reloadGrid");
}

function generarVistoBuenoFIanza(grid_negocios) {
    
    var i, selRowIds = $("#tabla_visto_bueno").jqGrid("getGridParam", "selarrrow"), rowData;
    if (selRowIds.length > 0) {
        
        loading("Espere un momento procesando negocios.", "270", "140");
        var arrayJson = new Array();
        for (i = 0; i < selRowIds.length; i++) {
            if (parseInt(selRowIds[i])){
                rowData = $("#tabla_visto_bueno").jqGrid("getLocalRow", selRowIds[i]);
                arrayJson.push(rowData);
            }else{
                mensajesDelSistema("Incluiste un negocio con Poliza para darle visto bueno de Fianza, desmarcalo.! ", '300', '150', true);
                $("#dialogLoading").dialog('close');
                return;
            }   
        }       

        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Garantias&accion=Comunitarias",
                type: 'POST',
                dataType: 'json',
                data: {
                    opcion: 34,
                    listJson: JSON.stringify(arrayJson)
                },
                success: function (json) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        $("#dialogLoading").dialog('close');
                        return;
                    }

                    if (json.respuesta === 'OK') {    
                        refrescarGridNegociosPendVistoBueno();
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema('Proceso exitoso', '300', '150', true);
                    } else {
                        mensajesDelSistema(json.respuesta, '300', '150', true);
                        $("#dialogLoading").dialog('close');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);

    }else{
        
        mensajesDelSistema("No hay datos seleccionados para procesar", '300', '150', true);
    }
}



function generarVistoBuenoPoliza(grid_negocios) {
    
    var i, selRowIds = $("#tabla_visto_bueno").jqGrid("getGridParam", "selarrrow"), rowData; 
    if (selRowIds.length > 0) {
        
        loading("Espere un momento procesando negocios.", "270", "140");
        var arrayJson = new Array();
        for (i = 0; i < selRowIds.length; i++) {
            if (parseInt(selRowIds[i])){                
                mensajesDelSistema("Incluiste un negocio con Fianza para darle visto bueno de Poliza, desmarcalo.! ", '300', '150', true);
                $("#dialogLoading").dialog('close');
                return;                
            }else {
                rowData = $("#tabla_visto_bueno").jqGrid("getLocalRow", selRowIds[i]);
                arrayJson.push(rowData);
            }
        }       

        setTimeout(function () {
            $.ajax({
                async: false,
                url: "./controller?estado=Garantias&accion=Comunitarias",
                type: 'POST',
                dataType: 'json',
                data: {
                    opcion: 45,
                    listJson: JSON.stringify(arrayJson)
                },                
                success: function (json) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        $("#dialogLoading").dialog('close');
                        return;
                    }

                    if (json.respuesta === 'OK') {    
                        refrescarGridNegociosPendVistoBueno();
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema('Proceso exitoso', '300', '150', true);
                    } else {
                        mensajesDelSistema(json.respuesta, '300', '150', true);
                        $("#dialogLoading").dialog('close');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    $("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);

    }else{
        
        mensajesDelSistema("No hay datos seleccionados para procesar", '300', '150', true);
    }
}
