/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    maximumId = 0;
    var items = cargarInfoWbs();
    var options = new primitives.orgdiagram.Config();
    options.items = items;
    options.cursorItem = 0;
    options.hasButtons = primitives.common.Enabled.False;
    options.hasSelectorCheckbox = primitives.common.Enabled.False;
    options.leavesPlacementType = primitives.orgdiagram.ChildrenPlacementType.Horizontal;
    options.pageFitMode = primitives.common.PageFitMode.FitToPage;
    options.templates = [getCursorTemplate()];
    options.defaultTemplateName = "CursorTemplate"; // We use the same template for all items, ideally every template should have its own set of buttons.
    options.onCursorRender = onCursorRender;
    options.itemTitleFirstFontColor = primitives.common.Colors.White;
    options.itemTitleSecondFontColor = primitives.common.Colors.White;
    options.onCursorChanged = function (e, data) {
        ResizePlaceholder(data.context.nivel);
    };
    options.onButtonClick = function (e, /*primitives.orgdiagram.EventArgs*/ data) {
        switch (data.name) {
            case "budget":
                //alert('Ok');
                AbrirDivMostrarCotizacion();
                break;
            case "delete":
                if (/*parentItem: primitives.orgdiagram.ItemConfig*/data.parentItem == null) {
                    mensajesDelSistema("No es posible eliminar el proyecto", '250', '150');
                }
                else {
                    eliminarItemWbs(data);
                }
                break;
            case "edit":
                (data.context.nivel === '0') ? AbrirDivMostrarProyecto(data) : (data.context.nivel === '1') ? AbrirDivEditarArea(data) : (data.context.nivel === '2') ? AbrirDivEditarDisciplina(data) : (data.context.nivel === '3') ? AbrirDivEditarCapitulo(data) : AbrirDivEditarActividad(data);
                break;
            case "properties":
                (data.context.nivel === '0') ? AbrirDivMostrarProyecto(data) : (data.context.nivel === '1') ? AbrirDivMostrarArea(data) : (data.context.nivel === '2') ? AbrirDivMostrarDisciplina(data) : (data.context.nivel === '3') ? AbrirDivMostrarCapitulo(data) : AbrirDivMostrarActividad(data);
                break;
            case "add":
                /* get items collection */
                (data.context.nivel === '0') ? AbrirDivAgregarArea(data) : (data.context.nivel === '1') ? AbrirDivAgregarDisciplina(data) : (data.context.nivel === '2') ? AbrirDivAgregarCapitulo(data) : (data.context.nivel === '3') ? AbrirDivAgregarActividades(data) : alert('Ha pulsado en el boton agregar APU');
                break;
        }
    };
    jQuery("#basicdiagram").orgDiagram(options);
    ResizePlaceholder('0');
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    });
    eventos();
    cargando_toggle();
});

function getSubItemsForParent(items, parentItem) {
    var children = {},
            itemsById = {},
            index, len, item;
    for (index = 0, len = items.length; index < len; index += 1) {
        var item = items[index];
        if (children[item.parent] == null) {
            children[item.parent] = [];
        }
        children[item.parent].push(item);
    }
    var newChildren = children[parentItem.id];
    var result = {};
    if (newChildren != null) {
        while (newChildren.length > 0) {
            var tempChildren = [];
            for (var index = 0; index < newChildren.length; index++) {
                var item = newChildren[index];
                result[item.id] = item;
                if (children[item.id] != null) {
                    tempChildren = tempChildren.concat(children[item.id]);
                }
            }
            newChildren = tempChildren;
        }
    }
    return result;
}
;
function cargarInfoWbs() {
    var respuesta = "{}";
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {opcion: 0,
            id_solicitud: $('#id_solicitud').val()},
        async: false,
        url: '/fintra/controlleropav?estado=Maestro&accion=Proyecto',
        success: function (jsonData) {
            if (!isEmptyJSON(jsonData)) {
                respuesta = jsonData.nodes;
                maximumId = jsonData.total;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return respuesta;
}


function cargarAreas() {
    var info = {};
    $.ajax({
        url: './controlleropav?estado=Maestro&accion=Proyecto',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            opcion: 1
        },
        success: function (json) {
            info = json;
            $("#area").autocomplete({
                source: info,
                minLength: 1,
                select: function (event, ui) {
                    event.preventDefault();
                    $('#idarea').val(ui.item.value);
                    $('#area').val(ui.item.label);
                },
                focus: function (event, ui) {
                    event.preventDefault();
                    $('#area').val(ui.item.label);
                },
                change: function (event, ui) {
                    if (ui.item == null) {
                        //here is null if entered value is not match in suggestion list                 
                        $(this).val((ui.item ? ui.item.id : ""));
                    }
                }
            });
        }
    });
}

function cargarActividades() {
    var info = {};
    $.ajax({
        url: './controlleropav?estado=Maestro&accion=Proyecto',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            opcion: 8
        },
        success: function (json) {
            info = json;
            $("#actividad").autocomplete({
                source: info,
                minLength: 1,
                mustMatch: true,
                select: function (event, ui) {
                    event.preventDefault();
                    $('#id_actividad').val(ui.item.value);
                    $('#actividad').val(ui.item.label);
                },
                focus: function (event, ui) {
                    event.preventDefault();
                    $('#actividad').val(ui.item.label);
                },
                change: function (event, ui) {
                    if (ui.item == null) {
                        $('#id_actividad').val('');
                        //here is null if entered value is not match in suggestion list
                        $(this).val((ui.item ? ui.item.id : ""));
                    }
                }
            });
        }
    });
}


function cargarCboGenerico(op, cboId) {
    $('#' + cboId).html('');
    $.ajax({
        type: 'GET',
        url: "/fintra/controlleropav?estado=Maestro&accion=Proyecto&opcion=" + op,
        dataType: 'json',
        async: false,
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }
                try {
                    $('#' + cboId).append("<option value=''>Seleccione</option>");
                    for (var key in json) {
                        $('#' + cboId).append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function addNewItem(data, title, descripcion, nivel, recordId, otro, logo) {
    var items = jQuery("#basicdiagram").orgDiagram("option", "items");
    /* create new item */
    var newItem = new primitives.orgdiagram.ItemConfig({
        id: ++maximumId,
        parent: data.context.id,
        title: title,
        description: descripcion,
        groupTitle: '$0',
        groupTitleColor: '#FE2E2E',
        image: "/fintra/images/botones/iconos/" + logo,
        nivel: nivel,
        recordId: recordId,
        otro: otro,
        buttonsType: (nivel === '1') ? "Area" : (nivel === '2') ? "Disciplina" : "Capitulo"
    });
    /* add it to items collection and put it back to chart, actually it is the same reference*/
    items.push(newItem);
    jQuery("#basicdiagram").orgDiagram({
        items: items,
        cursorItem: newItem.id
    });
    /* updating chart options does not fire its referesh, so it should be call explicitly */
    jQuery("#basicdiagram").orgDiagram("update", /*Refresh: use fast refresh to update chart*/ primitives.orgdiagram.UpdateMode.Refresh);
}

function setNewItemValues(data, nombre, descripcion, otro) {
    data.context.description = descripcion;
    data.context.otro = otro;
    jQuery("#basicdiagram").orgDiagram("update", /*Refresh: use fast refresh to update chart*/ primitives.orgdiagram.UpdateMode.Refresh);
}

function AbrirDivAgregarArea(data) {
    $('#mas_areas').fadeIn();
    $('#area').attr({disabled: false});
    $('#idarea').val('');
    $('#area').val('');
    //cargarCboGenerico(1,'area');   
    $('#id_area_proyecto').val('0');
    ventanaAgregarArea(data);
}


function ventanaAgregarArea(data) {
    $("#dialogArea").dialog({
        width: 450,
        height: 180,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'AGREGAR NUEVA AREA',
        closeOnEscape: false,
        buttons: {
            "Agregar": function () {
                guardarAreaProyecto(data, 'Insertar');
                //alert('Ha pulsado sobre el boton agregar areas');
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function AbrirDivAgregarDisciplina(data) {
    $('#disciplina').attr({disabled: false});
    cargarCboGenerico(2, 'disciplina');
    $('#id_disciplina_area').val('0');
    $('#id_area').val(data.context.recordId);
    $('#nomarea').val(data.context.description);
    ventanaAgregarDisciplina(data);
}


function ventanaAgregarDisciplina(data) {
    $("#dialogDisciplina").dialog({
        width: 450,
        height: 180,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'AGREGAR NUEVA DISCIPLINA',
        closeOnEscape: false,
        buttons: {
            "Agregar": function () {
                guardarDisciplinaArea(data, 'Insertar');
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function AbrirDivAgregarCapitulo(data) {
    $('#desccapitulo').attr({readonly: false});
    $('#id_capitulo_disciplina').val('0');
    $('#id_disciplina').val(data.context.recordId);
    $('#desccapitulo').val('');
    ventanaAgregarCapitulo(data);
}


function ventanaAgregarCapitulo(data) {
    $("#dialogCapitulo").dialog({
        width: 450,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'AGREGAR NUEVO CAPITULO',
        closeOnEscape: false,
        buttons: {
            "Agregar": function () {
                guardarCapitulo(data, 'Insertar');
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function AbrirDivAgregarActividades(data) {
    cargando_toggle();
    $('#actividad').attr({readonly: false});
    $('#id_actividad').val('');
    $('#id_capitulo').val(data.context.recordId);
    $('#nombre_capitulo').val(data.context.description);
    $('#actividad').val('');
    cargarGrillaActividades();
    ventanaAgregarActividadesCapitulo();
    cargando_toggle();
}


function ventanaAgregarActividadesCapitulo() {
    $("#dialogActividad").dialog({
        width: 850,
        height: 550,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTIVIDADES',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function AbrirDivEditarArea(data) {
    $('#mas_areas').fadeIn();
    $('#area').attr({disabled: false});
    //cargarCboGenerico(1,'area');  
    $('#id_area_proyecto').val(data.context.recordId);
    $('#area').val(data.context.description);
    ventanaEditarArea(data);
}


function ventanaEditarArea(data) {
    $("#dialogArea").dialog({
        width: 450,
        height: 180,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'EDITAR AREA',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarAreaProyecto(data, 'Actualizar');
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function AbrirDivEditarDisciplina(data) {
    $('#disciplina').attr({disabled: false});
    cargarCboGenerico(2, 'disciplina');
    $('#id_disciplina_area').val(data.context.recordId);
    $('#id_area').val(data.parentItem.recordId);
    $('#nomarea').val(data.parentItem.description);
    $('#disciplina').val(data.context.otro);
    ventanaEditarDisciplina(data);
}


function ventanaEditarDisciplina(data) {
    $("#dialogDisciplina").dialog({
        width: 450,
        height: 180,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'EDITAR DISCIPLINA',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarDisciplinaArea(data, 'Actualizar');
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function AbrirDivEditarCapitulo(data) {
    $('#desccapitulo').attr({readonly: false});
    $('#id_capitulo_disciplina').val(data.context.recordId);
    $('#id_disciplina').val(data.parentItem.recordId);
    $('#desccapitulo').val(data.context.description);
    ventanaEditarCapitulo(data);
}


function ventanaEditarCapitulo(data) {
    $("#dialogCapitulo").dialog({
        width: 450,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'EDITAR CAPITULO',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarCapitulo(data, 'Actualizar');
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function AbrirDivEditarActividad(data) {
    $('#actividad').attr({readonly: false});
    $('#id_actividad').val(data.context.recordId);
    $('#id_capitulo').val(data.parentItem.recordId);
    $('#actividad').val(data.context.description);
    ventanaEditarActividad(data);
}

function AbrirDivMostrarProyecto(data) {
    $('#nombre_proyecto').attr({readonly: true});
    $('#nombre_proyecto').val(data.context.description);
    ventanaMostrarProyecto();
}


function ventanaMostrarProyecto() {
    $("#dialogProyecto").dialog({
        width: 700,
        height: 180,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'PROYECTO',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function AbrirDivMostrarArea(data) {
    $('#mas_areas').fadeOut();
    $('#area').attr({disabled: true});
    $('#id_area_proyecto').val(data.context.recordId);
    $('#area').val(data.context.description);
    ventanaMostrarArea();
}


function ventanaMostrarArea() {
    $("#dialogArea").dialog({
        width: 450,
        height: 180,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'AREA',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function AbrirDivMostrarDisciplina(data) {
    $('#disciplina').attr({disabled: true});
    cargarCboGenerico(2, 'disciplina');
    $('#id_disciplina_area').val(data.context.recordId);
    $('#id_area').val(data.parentItem.recordId);
    $('#nomarea').val(data.parentItem.description);
    $('#disciplina').val(data.context.otro);
    ventanaMostrarDisciplina();
}


function ventanaMostrarDisciplina() {
    $("#dialogDisciplina").dialog({
        width: 450,
        height: 180,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'DISCIPLINA',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function AbrirDivMostrarCapitulo(data) {
    $('#desccapitulo').attr({readonly: true});
    $('#id_capitulo_disciplina').val(data.context.recordId);
    $('#id_disciplina').val(data.parentItem.recordId);
    $('#desccapitulo').val(data.context.description);
    ventanaMostrarCapitulo();
}


function ventanaMostrarCapitulo() {
    $("#dialogCapitulo").dialog({
        width: 450,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CAPITULO',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function AbrirDivMostrarActividad(data) {
    $('#actividad').attr({readonly: true});
    $('#id_actividad').val(data.context.recordId);
    $('#id_capitulo').val(data.parentItem.recordId);
    $('#actividad').val(data.context.description);
    ventanaMostrarActividad();
}


function ventanaMostrarActividad() {
    $("#dialogActividad").dialog({
        width: 450,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTIVIDAD',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}


function guardarAreaProyecto(data, op) {
    if ($('#area').val() === '') {
        mensajesDelSistema('Por favor, ingrese el area', '250', '150');
        return;
    }
    var area = $('#area').val();
    //var idarea = $('#idarea').val();
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 3,
            id_area_proyecto: $('#id_area_proyecto').val(),
            id_solicitud: $('#id_solicitud').val(),
            area: $('#area').val()
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    (op === 'Insertar') ? addNewItem(data, 'Area ' + (calculateTotalNodes('1') + 1), area, '1', json.lastRecordId, '', 'Work_area.png') : setNewItemValues(data, area, area, '');
                    $("#dialogArea").dialog('close');
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo guardar el area!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function guardarDisciplinaArea(data, op) {
    if ($('#disciplina').val() === '') {
        alert('Por favor, seleccione la disciplina');
        return;
    }
    var newIndex = "";
    if (op === 'Insertar') {
        var pIndex = data.context.title.substring(5, data.context.title.length);
        var items = jQuery("#basicdiagram").orgDiagram("option", "items");
        var index = calculateTotalHijos(items, data.context) + 1;
        newIndex = pIndex + '.' + index;
    }

    var disciplina = $('#disciplina option:selected').text();
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 4,
            id_disciplina_area: $('#id_disciplina_area').val(),
            id_area_proyecto: $('#id_area').val(),
            id_disciplina: $('#disciplina').val()
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    (op === 'Insertar') ? addNewItem(data, 'Disciplina ' + newIndex, disciplina, '2', json.lastRecordId, $('#disciplina').val(), 'System.png') : setNewItemValues(data, disciplina, disciplina, $('#disciplina').val());
                    $("#dialogDisciplina").dialog('close');
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo guardar la disciplina!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function guardarCapitulo(data, op) {
    if ($('#desccapitulo').val() !== '') {

        var newIndex = "";
        if (op === 'Insertar') {
            var pIndex = data.context.title.substring(11, data.context.title.length);
            var items = jQuery("#basicdiagram").orgDiagram("option", "items");
            var index = calculateTotalHijos(items, data.context) + 1;
            newIndex = pIndex + '.' + index;
        }

        var url = './controlleropav?estado=Maestro&accion=Proyecto';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 5,
                id_capitulo_disciplina: $('#id_capitulo_disciplina').val(),
                id_disciplina: $('#id_disciplina').val(),
                descripcion: $('#desccapitulo').val()
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        (op === 'Insertar') ? addNewItem(data, 'Capitulo ' + newIndex, $('#desccapitulo').val(), '3', json.lastRecordId, '', 'chapter.png') : setNewItemValues(data, $('#desccapitulo').val(), $('#desccapitulo').val(), $('#id_capitulo_disciplina').val());
                        $("#dialogCapitulo").dialog('close');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo guardar el capitulo!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        alert('Por favor, Debe llenar todos los campos');
    }
}

function guardarActividadCapitulo() {
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        async: false,
        data: {
            opcion: 6,
            id_capitulo: $('#id_capitulo').val(),
            id_actividad: $('#id_actividad').val()
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    $('#id_actividad').val('');
                    $('#actividad').val('');
                    refrescarGridActividades();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo agregar la actividad!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function eliminarItemWbs(data) {

    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 7,
            id: data.context.recordId,
            nivel: data.context.nivel
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    var items = jQuery("#basicdiagram").orgDiagram("option", "items");
                    var newItems = [];
                    /* collect all children of deleted items, we are going to delete them as well. */
                    var itemsToBeDeleted = getSubItemsForParent(items, /*context: primitives.orgdiagram.ItemConfig*/data.context);
                    /* add deleted item to that collection*/
                    itemsToBeDeleted[data.context.id] = true;
                    /* copy to newItems collection only remaining items */
                    for (var index = 0, len = items.length; index < len; index += 1) {
                        var item = items[index];
                        if (!itemsToBeDeleted.hasOwnProperty(item.id)) {
                            newItems.push(item);
                        }
                    }
                    /* update items list in chart */
                    jQuery("#basicdiagram").orgDiagram({
                        items: newItems,
                        cursorItem: data.parentItem.id
                    });
                    jQuery("#basicdiagram").orgDiagram("update", /*Refresh: use fast refresh to update chart*/ primitives.orgdiagram.UpdateMode.Refresh);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo eliminar el nodo!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarGrillaActividades() {

    var grid_tbl_actividades = jQuery("#tabla_actividades");
    if ($("#gview_tabla_actividades").length) {
        refrescarGridActividades();
    } else {
        grid_tbl_actividades.jqGrid({
            caption: "Listado de Actividades",
            url: "./controlleropav?estado=Maestro&accion=Proyecto",
            datatype: "json",
            height: '330',
            width: '815',
            colNames: ['Id', 'Id Actividad', 'Descripcion', 'Valor Actividad', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'id_actividad', index: 'id_actividad', width: 80, align: 'left', hidden: true},
                {name: 'descripcion', index: 'descripcion', width: 630, align: 'left'},
                {name: 'valor_act', index: 'valor_act', width: 130, align: 'center'},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 80, hidden: true}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_actividades'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            subGrid: true,
            subGridOptions: {"plusicon": "ui-icon-triangle-1-e",
                "minusicon": "ui-icon-triangle-1-s",
                "openicon": "ui-icon-arrowreturn-1-e",
                "reloadOnExpand": true,
                "selectOnExpand": true},
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 9,
                    id_capitulo: $('#id_capitulo').val()
                }
            },
            gridComplete: function () {
                var ids = jQuery("#tabla_actividades").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    el = "<img src='/fintra/images/botones/iconos/eliminar.gif' align='absbottom'  name='desasignar' id='editar' width='15' height='15' title ='Desasignar'  onclick=\"eliminarActividad('" + cl + "');\">";
                    // an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='desasignar' id='desasignar' width='15' height='15' title ='Desasignar'  onclick=\"mensajeConfirmAction('Esta seguro de desasginar la unidad seleccionada del proceso juridico?','250','150',anularEstadoCartera,'" + cl + "');\">";

                    jQuery("#tabla_actividades").jqGrid('setRowData', ids[i], {actions: el});
                }
            },
            subGridRowExpanded: function (subgrid_id, row_id) {
                var subgrid_table_id = subgrid_id + "_t";
                jQuery("#" + subgrid_id).html("<table id='" + subgrid_table_id + "'></table>");
                jQuery("#" + subgrid_table_id).jqGrid({
                    url: './controlleropav?estado=Maestro&accion=Proyecto',
                    datatype: 'json',
                    colNames: ['Id', 'Id Actividad', 'Id APU', 'Descripcion', 'Acciones'],
                    colModel: [
                        {name: 'id', index: 'id', width: 80, align: 'left', hidden: true, key: true},
                        {name: 'id_actividad', index: 'id_actividad', width: 80, align: 'left', hidden: true},
                        {name: 'id_apu', index: 'id_apu', width: 80, align: 'left', hidden: true},
                        {name: 'descripcion', index: 'descripcion', width: 530, align: 'left'},
                        {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 80, hidden: true}
                    ],
                    rowNum: 150,
                    height: '80%',
                    width: '90%',
                    jsonReader: {
                        root: 'rows',
                        cell: '',
                        repeatitems: false,
                        id: '0'
                    },
                    ajaxGridOptions: {
                        type: "POST",
                        async: false,
                        data: {
                            opcion: 10,
                            id_actividad: row_id
                        }
                    },
                    gridComplete: function () {
                        var ids = jQuery("#" + subgrid_table_id).jqGrid('getDataIDs');
                        for (var i = 0; i < ids.length; i++) {
                            var cl = ids[i];
                            el = "<img src='/fintra/images/botones/iconos/eliminar.gif' align='absbottom'  name='desasignar' id='editar' width='15' height='15' title ='Desasignar'  onclick=\"desasignarApuActividad('" + cl + "');\">";
                            jQuery("#" + subgrid_table_id).jqGrid('setRowData', ids[i], {actions: el});
                        }
                    },
                    ondblClickRow: function (rowid, iRow, iCol, e) {
                        return false;
                    },
                    loadError: function (xhr, status, error) {
                        alert(error);
                    }
                });
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                AsociarAPU(rowid);
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_actividades", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        /*jQuery("#tabla_actividades").jqGrid('filterToolbar',
         {
         autosearch: true,
         searchOnEnter: false,
         defaultSearch: "cn",
         stringResult: true,
         ignoreCase: true,
         multipleSearch: true
         });*/
    }
}

function AsociarAPU(id) {
    cargando_toggle();
    AbrirDivApus();
    $('#id').val(id);
    CargarGridApusAsoc();
    cargando_toggle();
}

function AbrirDivApus() {
    $("#div_apus").dialog({
        width: 850,
        height: 800,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'APU',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $('#tbl_apus').jqGrid('GridUnload');
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function cargarComboGrupo1(filtro) {
    var elemento = $('#grupo_apu1'), sql = 'ConsultaGruposAPUS';
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'get',
        data: {opcion: 1, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '" onclick="CargarGridApus();">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });
}



function filtro_apu(buscar) {

    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    jQuery("#tbl_apus").setGridParam({
        datatype: 'json',
        url: url,
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: 73,
                buscar: buscar,
                apu_proyecto: $('#apu_proyecto').prop('checked'),
                id_solicitud: $('#id_solicitud').val()

            }
        }
    });
    $('#tbl_apus').trigger("reloadGrid");
}




function CargarGridApus() {
    var grupo_apu = $('#grupo_apu1').val();
    var id = $('#id').val();

    if ($("#gview_tbl_apus").length) {
        refrescarGridGrupoAPU(grupo_apu, id);
    } else {
        jQuery("#tbl_apus").jqGrid({
            caption: 'Grid Apu',
            url: './controlleropav?estado=Maestro&accion=Proyecto',
            datatype: 'json',
            height: 500,
            width: 689,
            colNames: ['Id', 'Descripcion APU', 'Unidad de Medida', 'Id Grupo', 'Id Unidad', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', hidden: false, sortable: true, align: 'center', width: '50', sorttype: 'currency', key: true},
                {name: 'nombreapu', index: 'nombreapu', sortable: true, align: 'left', width: '600'},
                {name: 'nombre_unidad', index: 'nombre_unidad', search: false, sortable: true, align: 'center', width: '100'},
                {name: 'idgrupo', index: 'idgrupo', hidden: true, sortable: true, align: 'center', width: '600'},
                {name: 'idunidad', index: 'idunidad', hidden: true, sortable: true, align: 'center', width: '600'},
                {name: 'action', index: 'action', search: false, align: 'center', width: '100'}
            ],
            rowNum: 10000,
            rowTotal: 50000,
            loadonce: true,
            scroll: 1,
            rownumWidth: 40,
            gridview: true,
            multiselect: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_apus',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false,
                data: {
                    opcion: 25,
                    grupo_apu: grupo_apu,
                    id: id
                }
            },
            gridComplete: function () {
                var ids = jQuery("#tbl_apus").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    el = "<img src='/fintra/images/botones/iconos/editDoc2.png' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarAPU('" + cl + "', 'tbl_apus', 0);\">";
                    clonar = "<img src='/fintra/images/opav/Copia.png' align='absbottom'  name='Clonar' id='Clonar' width='15' height='15' title ='Clonar'  onclick=\"mensajeConfirmAction('Esta seguro que desea Clonar Apu', '270', '180', ClonarAPU, '" + cl + "');\">";
                    jQuery("#tbl_apus").jqGrid('setRowData', ids[i], {action: el + ' ' + clonar});
                }
            },
            ondblClickRow: function (id) {
                VisualizarAPU(id, 'tbl_apus');
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });

        jQuery("#tbl_apus").jqGrid('navGrid', '#page_apus', {del: false, add: false, edit: false, search: false, refresh: false});
        jQuery("#tbl_apus").navButtonAdd('#page_apus', {
            caption: "Crear APU",
            title: "Crear APU",
            buttonicon: "ui-icon-new",
            onClickButton: function () {
                crearAPU();
            }
        });

    }
}

function refrescarGridGrupoAPU(grupo_apu, id) {
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    jQuery("#tbl_apus").setGridParam({
        datatype: 'json',
        url: url,
        opcion: 25,
        grupo_apu: grupo_apu,
        id: id
    });
    jQuery('#tbl_apus').trigger("reloadGrid");
}

function CargarGridApusAsoc() {
    var id = $('#id').val();
    $('#tbl_apus_asociado').jqGrid('GridUnload');
    if ($("#gview_tbl_apus_asociado").length) {
        //refrescarGridAPUAsoc(id);
    } else {
        jQuery("#tbl_apus_asociado").jqGrid({
            caption: 'Grid Apu Asociados',
            url: './controlleropav?estado=Maestro&accion=Proyecto',
            datatype: 'json',
            height: 500,
            width: 712,
            colNames: ['Id','#' ,'Descripcion APU', 'Cantidad', 'Unidad de Medida', 'Id Grupo', 'Id Unidad', 'Acciones', 'Estado', 'Tipo APU'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, sortable: true, align: 'center', width: '100', key: true},
                {name: 'posicion', index: 'posicion', search: false, sortable: true, align: 'center', width: '100'},
                {name: 'nombreapu', index: 'nombreapu', sortable: true, align: 'left', width: '900'},
                {name: 'cantidad', index: 'cantidad', sortable: true, align: 'center', width: '100', formatter: 'integer'},
                {name: 'nombre_unidad', index: 'nombre_unidad', search: false, sortable: true, align: 'center', width: '100'},
                {name: 'idgrupo', index: 'idgrupo', hidden: true, sortable: true, align: 'center', width: '600'},
                {name: 'idunidad', index: 'idunidad', hidden: true, sortable: true, align: 'center', width: '600'},
                {name: 'action', index: 'action', search: false, align: 'center', width: '100'},
                {name: 'estado', index: 'estado', search: false, align: 'center', width: '100', hidden: true},
                {name: 'tipo_apu', index: 'tipo_apu', search: false, align: 'center', hidden: true}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            multiselect: false,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_apus_asociado',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false,
                data: {
                    opcion: 26,
                    id: id
                }
            },
            gridComplete: function () {
                var ids = jQuery("#tbl_apus_asociado").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    el = "<img src='/fintra/images/botones/iconos/editDoc2.png' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Visualizar Apu'  onclick=\"editarAPU('" + cl + "', 'tbl_apus_asociado', 1);\">";
                    jQuery("#tbl_apus_asociado").jqGrid('setRowData', ids[i], {action: el});
//                    var estado = jQuery("#tbl_apus_asociado").getRowData(cl).estado;
//                    if (estado === '0') {
//                        be = "<img src='/fintra/images/botones/iconos/chulo.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Pendiente'  onclick=\"mensajeConfirmAction('Esta seguro de cambiar el estado a terminado?','250','150',ActualizarEstadoRel,'" + cl + "');\">";
//                    } else {
//                        be = "<img src='/fintra/images/botones/iconos/obligatorio.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Terminado'  onclick=\"mensajeConfirmAction('Esta seguro de cambiar el estado a pendiente?','250','150',ActualizarEstadoRel,'" + cl + "');\">";
//                    }
//                    clonar = "<img src='/fintra/images/opav/Copia.png' align='absbottom'  name='Clonar' id='Clonar' width='15' height='15' title ='Clonar'  onclick=\"mensajeConfirmAction('Esta seguro que desea Clonar Apu', '270', '180', ClonarAPU2, '" + cl + "');\">";
//
//                    jQuery("#tbl_apus_asociado").jqGrid('setRowData', ids[i], {estado: be + clonar});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });
        jQuery("#tbl_apus_asociado").navGrid("#page_apus_asociado", {add: false, edit: false, del: false, search: false, refresh: false});
    }
}

function refrescarGridAPUAsoc(id) {

    var url = './controlleropav?estado=Maestro&accion=Proyecto&opcion=25&id=' + id;
    jQuery("#tbl_apus_asociado").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#tbl_apus_asociado').trigger("reloadGrid");
}

function AsociarAPUActividad() {
    cargando_toggle();
    var id = $('#id').val();
    var filasId = jQuery('#tbl_apus').jqGrid('getGridParam', 'selarrrow');
    var coma = '', listado = '';
    if (filasId !== '') {

        for (var i = 0; i < filasId.length; i++) {
            var ret = jQuery("#tbl_apus").jqGrid('getRowData', filasId[i]);
            listado += coma + ret.id;
            coma = ",";
        }

        var url = './controlleropav?estado=Maestro&accion=Proyecto';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: false,
            data: {
                opcion: 27,
                listado: listado,
                id: id
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        CargarGridApus(id);
                        CargarGridApusAsoc(id);
                        refrescarGridActividades();
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar los APUs a la Actividad!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#tbl_apus").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar los APUs a asociar", '250', '150');
        } else {
            mensajesDelSistema("No hay APUs por asociar", '250', '150');
        }
    }
    cargando_toggle();
}

function DesAsociarAPUActividad() {
    cargando_toggle();
    var id = $('#id').val();
    var filasId = jQuery('#tbl_apus_asociado').jqGrid('getGridParam', 'selarrrow');
    var coma = '', listado = '';
    if (filasId !== '') {

        for (var i = 0; i < filasId.length; i++) {
            var ret = jQuery("#tbl_apus_asociado").jqGrid('getRowData', filasId[i]);
            listado += coma + ret.id;
            coma = ",";
        }

        var url = './controlleropav?estado=Maestro&accion=Proyecto';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: false,
            data: {
                opcion: 28,
                listado: listado,
                id: id
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        CargarGridApus(id);
                        CargarGridApusAsoc(id);
                        refrescarGridActividades();
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo Desasociar los APUs de la Actividad!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#tbl_apus").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar los APUs a Desasociar", '250', '150');
        } else {
            mensajesDelSistema("No hay APUs por Desasociar", '250', '150');
        }
    }
    cargando_toggle();
}

function refrescarGridActividades() {
    jQuery("#tabla_actividades").setGridParam({
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 9,
                id_capitulo: $('#id_capitulo').val()
            }
        }
    });
    jQuery('#tabla_actividades').trigger("reloadGrid");
}

function agregarActividad(descripcion) {
    var grid = $("#tabla_actividades")
            , rowid = 'neo_' + grid.getRowData().length;
    grid.addRowData(rowid, {id_actividad: $('#id_actividad').val(), descripcion: descripcion});
    grid.jqGrid('editRow', rowid, true, function () {
        //  $("input, select", e.target).focus();
    }, null, null, {}, function (rowid) {
        var descripcion = jQuery("#tabla_actividades").getRowData(rowid).descripcion;
    }, null, function (id) {
        var g = $('#tabla_actividades')
                , r = g.jqGrid('getLocalRow', id);
        if (!r.id) {
            g.jqGrid('delRowData', id);
        }
    });
}


function ventanaNuevaActividad() {
    $("#dialogAddActividad").dialog({
        width: 450,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'NUEVA ACTIVIDAD',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarActividad();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function ventanaEditarActividad(data) {
    $("#dialogAddActividad").dialog({
        width: 450,
        height: 210,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'EDITAR ACTIVIDAD',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                alert('Ha pulsado sobre el boton actualizar actividad');
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function eliminarActividad(rowid) {
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 11,
            id: rowid
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridActividades();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo desasignar el apu!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function desasignarApuActividad(rowid) {
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 12,
            id: rowid
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridActividades();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo desasignar el apu!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function guardarActividad() {
    if ($('#nombre_actividad').val() === '') {
        mensajesDelSistema('Por favor, ingrese nombre de la actividad', '250', '150');
        return;
    }
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 14,
            descripcion: $('#nombre_actividad').val()
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    cargarActividades();
                    $("#dialogAddActividad").dialog('close');
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo guardar la actividad!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function calculateTotalNodes(nivel) {
    var items = jQuery("#basicdiagram").orgDiagram("option", "items");
    var cont = 0;
    $.each(items, function (key, value) {
        if (value.nivel === nivel)
            cont++;
    });
    return cont;
}

function calculateTotalHijos(items, parentItem) {
    var children = {},
            itemsById = {},
            index, len, item, cant = 0;
    for (index = 0, len = items.length; index < len; index += 1) {
        var item = items[index];
        if (children[item.parent] == null) {
            children[item.parent] = [];
        }
        children[item.parent].push(item);
    }
    var newChildren = children[parentItem.id];
    try {
        if (newChildren !== null) {
            cant = newChildren.length;
        }
    }
    catch (e) {
        cant = 0;
    }
    finally {
        return cant;
    }
}

function ResizePlaceholder(nivel) {
    //var bodyHeight = $(window).height() - ((nivel==='0')? 400:(nivel==='1')? 330: 0); 
    //var bodyHeight = $(window).height() - ((nivel==='0')? 600:(nivel==='1')? 550: 300);    
    var bodyHeight = ((nivel === '0') ? 400 : (nivel === '1') ? 500 : $(window).height() - 150);
    // var bodyHeight = $(window).height() * ((nivel==='0')? 0.4:(nivel==='1')? 0.5: 0.8);    
    jQuery("#basicdiagram").css(
            {
                "height": bodyHeight + "px"
            });
    jQuery("#basicdiagram").orgDiagram("update", /*Refresh: use fast refresh to update chart*/ primitives.orgdiagram.UpdateMode.Refresh);
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });
    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function crearAPU() {
    $('#nomapu').val('');
    $('#tbl_insumos').jqGrid('GridUnload');
    //$('#div_apu').fadeIn('slow');
    AbrirDivAPU();
    GridInsumos();
}

function cargarComboGrupo(filtro) {
    var elemento = $('#grupo_apu'), sql = 'ConsultaGruposAPUS1';
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'get',
        data: {opcion: 1, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    elemento.append('<option value=""> ...</option>');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });
}

function cargarComboUnidadM(filtro) {
    var elemento = $('#unmed'), sql = 'ConsultaUnidadM';
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'get',
        data: {opcion: 1, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    elemento.append('<option value=""> ...</option>');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
            }
        }
    });
}

function AbrirDivAPU(tipo) {
    var ancho = '1480';

    if (tipo === 1) {
        ancho = '1480';
    }

    $("#div_apu").dialog({
        width: ancho,
        height: 535,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        title: 'Armado de APU',
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
//                mensajeConfirmAction("Recuerde guardar los cambios. Esta seguro que desea salir ?", "250", "160", cerrarDialogo, "div_apu");
                $('#div_apu').dialog("destroy");
            }
        }
    });
}

function GridInsumos() {

    var grid_ingreso_insumos = jQuery("#tbl_insumos");
    var id = "0";
    /*if ($("#gview_tbl_insumos").length) {
     reloadGridIngresoCatalogo(grid_ingreso_catalogo, arrNames, arrColModel);
     } else {*/

    grid_ingreso_insumos.jqGrid({
        caption: "Lista de Insumos",
        datatype: "local",
        height: '240',
        width: '1400',
        colNames: ['ID', 'Tipo Insumo', 'Id Tipo Insumo', 'Descripcion Insumo', 'Find', 'Id Descripcion Insumo', 'Unidad de Medida', 'Id Unidad de Medida', 'Nueva UM', 'Cantidad', 'Rendimiento'],
        colModel: [
            {name: 'id', index: 'id', hidden: true, key: true},
            {name: 'tipoinsumo', index: 'tipoinsumo', align: 'center', width: 140, search: false, edittype: 'select', editrules: {required: true},
                editoptions: {
                    //value: ajaxSelect('ConsultaTiposInsumos', ''),
                    value: listarTiposMaterial(),
                    style: "width: 140px",
                    dataInit: function (elem) {
                        if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
                            // we are NOT in the searching bar
                            $(elem).find("option[value=\"\"]").remove();
                            setTimeout(function () {
                                $(elem).trigger('change');
                            }, 500);
                        }
                    },
                    dataEvents: [{type: 'change', fn: function (e) {
                                try {

                                    var rowid = e.target.id.replace("_tipoinsumo", "");
                                    id = e.target.value.replace("_", "");
                                    grid_ingreso_insumos.jqGrid('setCell', rowid, "idtipoinsumo", id);
                                    ////////////////////////////
                                    /*var jsonIns = listarInsumosXTipo(e.target.value.replace("_", ""));
                                     cargaListaDependentSelect(rowid + "_descripcionins", jsonIns);
                                     var idinsumo = grid_ingreso_insumos.getRowData(rowid).iddescripcionins;
                                     $("select#" + rowid + "_descripcionins").val(idinsumo);
                                     $("select#" + rowid + "_descripcionins").trigger('change');*/
                                    ////////////////////////////

                                } catch (exc) {
                                }
                                return;
                            }
                        }, {type: "keyup", fn: function (e) {
                                $(e.target).trigger("change");
                            }
                        }]
                }
            },
            {name: 'idtipoinsumo', index: 'idtipoinsumo', hidden: true},
            {name: 'descripcionins', index: 'descripcionins', width: 380, edittype: 'text', resizable: false, align: 'left'},
            {name: 'finddescripcionins', index: 'finddescripcionins', width: 30, edittype: 'text', resizable: false, align: 'center', hidden:true},
            {name: 'iddescripcionins', index: 'iddescripcionins', hidden: true},
            {name: 'unidadmedida', index: 'unidadmedida', align: 'center', width: 100, search: false, edittype: 'select', editrules: {required: false},
                editoptions: {
                    value: ajaxSelect('ConsultaUnidadesMed', ''),
                    style: "width: 150px",
                    dataInit: function (elem) {
                        $(elem).addClass('unidadm');
                        if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
                            // we are NOT in the searching bar
                            $(elem).find("option[value=\"\"]").remove();
                            setTimeout(function () {
                                $(elem).trigger('change');
                            }, 500);
                        }
                    },
                    dataEvents: [{type: 'change', fn: function (e) {
                                try {

                                    var rowid = e.target.id.replace("_unidadmedida", "");
                                    id = e.target.value.replace("_", "");
                                    grid_ingreso_insumos.jqGrid('setCell', rowid, "idunidadmedida", id);
                                } catch (exc) {
                                }
                                return;
                            }
                        }, {type: "keyup", fn: function (e) {
                                $(e.target).trigger("change");
                            }
                        }]
                }
            },
            {name: 'idunidadmedida', index: 'idunidadmedida', hidden: true},
            {name: 'newunidadmedida', hidden: false, index: 'newunidadmedida', width: 50, edittype: 'text', resizable: false, align: 'center', hidden:true},
            {name: 'cantidad', index: 'cantidad', align: 'center', width: 60, search: false,  formatter: 'number',
                formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 2, prefix: ""},
                editoptions: {size: 50, dataInit: function (elem) {
                        $(elem).bind("keypress", function (e) {
                            //alert(soloNumeros(e));
                            return decimales(e, $(this));
                        });
                    }
                }},
            {name: 'rendimiento', index: 'rendimiento', align: 'center', width: 60, search: false,  formatter: 'number',
                formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 2, prefix: ""},
                editoptions: {size: 15, dataInit: function (elem) {
                        $(elem).bind("keypress", function (e) {
                            return decimales(e, $(this));
                        });
                    }
                }
            }
        ],
        rowNum: 10000,
        rowTotal: 10000000,
        pager: '#page_insumos',
        loadonce: true,
        rownumWidth: 40,
        gridview: true,
        viewrecords: true,
        rownumbers: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: true,
        reloadAfterSubmit: true,
        multiselect: true,
        cellsubmit: 'clientArray',
        editurl: 'clientArray',
        ajaxGridOptions: {
            async: false
        },
        gridComplete: function () {
            var ids = jQuery("#tbl_insumos").jqGrid('getDataIDs');
            for (var i = 0; i < ids.length; i++) {
                var cl = ids[i];
                fil = "<img src='/fintra/images/botones/iconos/lupaOver.gif' align='absbottom'  name='Find' id='Find' width='15' height='15' title ='Find' onclick=\"buscarInsumoxFiltro('" + cl + "');\">";
                new1 = "<img src='/fintra/images/botones/iconos/new.gif' align='absbottom'  name='nuevo' id='nuevo' width='15' height='15' title ='Nueva UM' onclick=\"crearUnidadMedida();\">";
                jQuery("#tbl_insumos").jqGrid('setRowData', ids[i], {finddescripcionins: fil});
                jQuery("#tbl_insumos").jqGrid('setRowData', ids[i], {newunidadmedida: new1});
            }
        },
        ondblClickRow: function (rowid, iRow, iCol, e) {
        },
        restoreAfterError: true
    });
    grid_ingreso_insumos.navGrid("#page_insumos", {add: false, edit: false, del: false, search: false, refresh: false});
    $("#tbl_insumos").parents('div.ui-jqgrid-bdiv').css({"min-height": "240px", "max-height": "500px"});


}

function listarTiposMaterial() {
    var Result = {};
    $.ajax({
        type: 'GET',
        url: "/fintra/controlleropav?estado=Procesos&accion=APU&opcion=5",
        dataType: 'json',
        async: false,
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    Result = {};
                } else {
                    Result = json;
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return Result;
}

function ajaxSelect(sql, id) {

    var resultado;
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'get',
        data: {opcion: 2, informacion: JSON.stringify({query: sql, filtros: []})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '300', 'auto', false);
                    resultado = {};
                } else {
                    resultado = json;
                }
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {
            console.log('error');
        }
    });
    return resultado;
}

function buscarInsumoxFiltro(cl) {

    //$('#div_filtro_insumos').fadeIn("slow");
    limpiarCombos();
    var fila = jQuery("#tbl_insumos").getRowData(cl).idtipoinsumo;
    $('#_tipo_insumo').val(fila);
    AbrirDivFiltroInsumos();
    cargarCombo('categoria', [fila]);
    CargarInsumosxFiltro();
    $('#idregistro').val(cl);

}

function AbrirDivFiltroInsumos() {
    $("#div_filtro_insumos").dialog({
        width: 750,
        height: 631,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'FILTROS DE BUSQUEDA DE INSUMOS',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                limpiarCombos();
                $(this).dialog("destroy");
            }
        }
    });
}

function limpiarCombos() {
    $('#categoria').html('');
    $('#categoria').append('<option value="0">...</option>');
    $('#sub').html('');
    $('#sub').append('<option value="0">...</option>');
}

function cargarCombo(id, filtro) {
    var elemento = $('#' + id)
            , sql = (id === 'categoria') ? 'categorias' : 'subcategoriaxcategoria';
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'get',
        data: {opcion: 2, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {

            }
        },
        error: function () {

        }
    });
}

function CargarInsumosxFiltro() {
    var url = '/fintra/controlleropav?estado=Procesos&accion=APU&opcion=6';
    $('#tbl_filtro_insumos').jqGrid('GridUnload');

    jQuery("#tbl_filtro_insumos").jqGrid({
        caption: 'Filtro Insumos',
        url: url,
        datatype: 'json',
        height: 300,
        width: 650,
        colNames: ['Id', 'Descripcion'],
        colModel: [
            {name: 'id', index: 'id', sortable: true, align: 'center', width: '100', key: true},
            {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600'}
        ],
        rowNum: 1000,
        rowTotal: 50000,
        loadonce: true,
        rownumWidth: 40,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        pager: '#page_filtro_insumos',
        jsonReader: {
            root: 'rows',
            repeatitems: false,
            id: '0'
        },
        ajaxGridOptions: {
            async: false,
            data: {
                subcategoria: $('#sub').val()
            }
        },
        ondblClickRow: function (id) {
            var ret = jQuery("#tbl_filtro_insumos").jqGrid('getRowData', id);
            var nombre = ret.nombre;
            var idcl = $('#idregistro').val();
            jQuery("#tbl_insumos").jqGrid('setCell', idcl, "descripcionins", nombre);
            jQuery("#tbl_insumos").jqGrid('setCell', idcl, "iddescripcionins", ret.id);
            document.getElementById("sub").length = 0;
            $("#div_filtro_insumos").dialog("destroy");
        },
        loadError: function (xhr, status, error) {
            alert(error);
        }
    });

}

function guardarAPU() {
    var grid = jQuery("#tbl_insumos")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;
    var grupo_apu = $('#grupo_apu').val(), nomapu = $('#nomapu').val(), unidadm = $('#unmed').val();
    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);
        if (data.descripcionins === '') {
            error = true;
            mensajesDelSistema('Escoja Un Insumo', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else {
            if (data.unidadmedida === '') {

                error = true;
                mensajesDelSistema('Escoja una Unidad de Medida', '300', 'auto', false);
                grid.jqGrid('editRow', filas[i], true, function () {
                    $("input, select", e.target).focus();
                });
                break;
            } else {
                if (data.cantidad === '') {

                    error = true;
                    mensajesDelSistema('Digite la Cantidad', '300', 'auto', false);
                    grid.jqGrid('editRow', filas[i], true, function () {
                        $("input, select", e.target).focus();
                    });
                    break;
                } else {
                    if (data.rendimiento === '') {

                        error = true;
                        mensajesDelSistema('Digite el rendimiento', '300', 'auto', false);
                        grid.jqGrid('editRow', filas[i], true, function () {
                            $("input, select", e.target).focus();
                        });
                        break;
                    }
                }
            }

        }

        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        //mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
        toastr.error("Inserte al menos un registro", "Error");
    }

    if ($('#nomapu').val() === '') {
        error = true;
        //mensajesDelSistema('Digite el Nombre del APU', '300', 'auto', false);
        toastr.error("Digite el Nombre del APU", "Error");

    }

    if ($('#unidadm').val() === '') {
        error = true;
        //mensajesDelSistema('Escoja una Unidad de Medida', '300', 'auto', false);
        toastr.error("Escoja una Unidad de Medida", "Error");
    }
    if ($('#grupo_apu').val() === '') {
        error = true;
        //mensajesDelSistema('Escoja una Grupo de Apu', '300', 'auto', false);
        toastr.error("Escoja una Grupo de Apu", "Error");
    }
    if ($('#unmed').val() === '') {
        error = true;
        //mensajesDelSistema('Escoja una Unidad de Medida', '300', 'auto', false);
        toastr.error("Escoja una Unidad de Medida", "Error");
    }

    if (error)
        return;
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=APU",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 7, informacion: JSON.stringify({rows: filas}), grupo_apu: grupo_apu, nomapu: nomapu, unidadm: unidadm},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    CargarGridApus();
                    $('#nomapu').val('');
                    $('#tbl_insumos').jqGrid('GridUnload');
                    GridInsumos();
                }
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {

        }
    });
}

function crearGrupoApu() {
    $('#nomgrupo').val('');
    $('#descgrupo').val('');
    //$('#div_grupo_apu').fadeIn('slow');
    AbrirDivCrearGrupoApu();
}

function AbrirDivCrearGrupoApu() {
    $("#div_grupo_apu").dialog({
        width: 700,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR GRUPO APU',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarGrupoApu();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarGrupoApu() {
    var nomGrupo = $('#nomgrupo').val();
    var descGrupo = $('#descgrupo').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=APU';
    if (nomGrupo !== '' && descGrupo !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 3,
                nombre: nomGrupo,
                descripcion: descGrupo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        cargarComboGrupo([]);
                        mensajesDelSistema("Se creo el Grupo", '250', '150', true);
                        $('#nomgrupo').val('');
                        $('#descgrupo').val('');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear el grupo!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }
}

function crearUnidadMedida() {
    $('#nomunidad').val('');
    //$('#div_unidad_medida').fadeIn('slow');
    AbrirDivCrearUnidadMedida();
}

function AbrirDivCrearUnidadMedida() {
    $("#div_unidad_medida").dialog({
        width: 500,
        height: 160,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR UNIDAD DE MEDIDA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarUnidadMedida();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarUnidadMedida() {
    var nomunidad = $('#nomunidad').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=APU';
    if (nomunidad !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 12,
                nombre: nomunidad
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        mensajesDelSistema("Se creo la Unidad de Medida...", '250', '150', true);
                        $('#nomunidad').val('');
                        var u = json.idCab;
                        $('.unidadm').append('<option value="' + u + '">' + nomunidad.toUpperCase() + '</option>');
                        cargarComboUnidadM([]);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear la unidad!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }
}

function AbrirDivAreaProyecto() {
    $("#div_area_proyecto").dialog({
        width: 1080,
        height: 520,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Maestro de Areas',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $(".ui-dialog-titlebar-close").hide();
}

function listarAreas() {
    var grid_tbl_areas = jQuery("#tabla_areas");
    if ($("#gview_tabla_areas").length) {
        refrescarGridAreas();
    } else {
        grid_tbl_areas.jqGrid({
            caption: "AREAS",
            url: "./controlleropav?estado=Maestro&accion=Proyecto",
            datatype: "json",
            height: '290',
            width: '710',
            cellEdit: true,
            colNames: ['Id', 'Nombre', 'Estado', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'nombre', index: 'nombre', width: 530, align: 'left', sortable: false},
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden: true},
                {name: 'cambio', index: 'cambio', width: 90, align: 'center', sortable: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_areas'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 17
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_areas").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_areas").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoArea('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_areas").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_areas"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;
                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '250', '165', false);
                } else {
                    editarArea(id);
                }

            }
        }).navGrid("#page_tabla_areas", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_areas").jqGrid("navButtonAdd", "#page_tabla_areas", {
            caption: "Nuevo",
            onClickButton: function () {
                crearArea();
            }
        });
    }

}

function refrescarGridAreas() {
    jQuery("#tabla_areas").setGridParam({
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 17
            }
        }
    });
    jQuery('#tabla_areas').trigger("reloadGrid");
}

function crearArea() {
    $('#div_area').fadeIn('slow');
    $('#idArea').val('');
    $('#nombre').val('');
    AbrirDivCrearArea();
}

function AbrirDivCrearArea() {
    $("#div_area").dialog({
        width: 'auto',
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR AREA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarArea();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#div_area").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarArea(cl) {

    $('#div_area').fadeIn("slow");
    var fila = jQuery("#tabla_areas").getRowData(cl);
    var nombre = fila['nombre'];
    $('#idArea').val(cl);
    $('#nombre').val(nombre);
    AbrirDivEditarMaestroArea();
}

function AbrirDivEditarMaestroArea() {
    $("#div_area").dialog({
        width: 'auto',
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR AREA',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarArea();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#div_area").parent().find(".ui-dialog-titlebar-close").hide();
}

function guardarArea() {
    var nombre = $('#nombre').val();
    if (nombre !== '') {
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function () {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "./controlleropav?estado=Maestro&accion=Proyecto",
                data: {
                    opcion: ($('#idArea').val() === '') ? 18 : 19,
                    id: $('#idArea').val(),
                    nombre: $('#nombre').val()
                },
                success: function (json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '270', '165');
                            return;
                        }

                        if (json.respuesta === "OK") {
                            $("#dialogLoading").dialog('close');
                            refrescarGridAreas();
                            $("#div_area").dialog('close');
                        }

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se pudo guardar el area!!", '250', '150');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }, 500);
    } else {
        mensajesDelSistema("DEBE INGRESAR EL NOMBRE!!", '250', '150');
    }
}


function CambiarEstadoArea(rowid) {
    var grid_tabla = jQuery("#tabla_areas");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        data: {
            opcion: 20,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridAreas();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado del area!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function getCursorTemplate() {
    var result = new primitives.orgdiagram.TemplateConfig();
    result.name = "CursorTemplate";
    result.itemSize = new primitives.common.Size(130, 110);
    result.minimizedItemSize = new primitives.common.Size(3, 3);
    result.highlightPadding = new primitives.common.Thickness(2, 2, 2, 2);
    result.cursorPadding = new primitives.common.Thickness(3, 3, 38, 8);
    var cursorTemplate = jQuery("<div></div>")
            .css({
                position: "absolute",
                overflow: "hidden",
                width: (result.itemSize.width + result.cursorPadding.left + result.cursorPadding.right) + "px",
                height: (result.itemSize.height + result.cursorPadding.top + result.cursorPadding.bottom) + "px"
            });
    var cursorBorder = jQuery("<div></div>")
            .css({
                width: (result.itemSize.width + result.cursorPadding.left + 1) + "px",
                height: (result.itemSize.height + result.cursorPadding.top + 1) + "px"
            }).addClass("bp-item bp-corner-all bp-cursor-frame");
    cursorTemplate.append(cursorBorder);
    var buttonsGroup = jQuery("<div></div>")
            .css({
                position: "absolute",
                overflow: "hidden",
                top: result.cursorPadding.top + "px",
                left: (result.itemSize.width + result.cursorPadding.left + 10) + "px",
                width: "35px",
                height: (result.itemSize.height + 1) + "px"
            }).addClass("buttons-panel");
    cursorTemplate.append(buttonsGroup);
    result.cursorTemplate = cursorTemplate.wrap('<div>').parent().html();
    return result;
}

function onCursorRender(event, data) {
    switch (data.renderingMode) {
        case primitives.common.RenderingMode.Create:
            break;
        case primitives.common.RenderingMode.Update:
            /* Update widgets here */
            break;
    }

    var buttons = [];
    // It is not nessesary to use ButtonConfig class here. We can use regular noname objects as well.
    switch (data.context.nivel) {
        case "3":
            buttons.push(new primitives.orgdiagram.ButtonConfig("add", "ui-icon-gear", "Visualizar"));
            break;
    }
    // Cursor template contains several div elements so we have to select div we are going to use as buttons container
    var buttonsPanel = data.element.find(".buttons-panel");
    if (buttons.length > 0) {
        var topOffset = 2;
        var buttonsInterval = 10;
        
        // Clean up contents of buttons panel first
        buttonsPanel.empty();
        for (index = 0; index < buttons.length; index += 1) {
            buttonConfig = buttons[index];
            // We use data-buttonname attriute to pass button name to orgchart onButtonClick event
            button = jQuery('<li data-buttonname="' + buttonConfig.name + '"></li>')
                    .css({
                        position: "absolute",
                        top: topOffset + "px",
                        left: "0px",
                        width: buttonConfig.size.width + "px",
                        height: buttonConfig.size.height + "px",
                        padding: "3px"
                    })
                    .addClass("orgdiagrambutton"); // This class forces widget to consider button as its own. 
            //Otherwise you have to listen to onMouseClick event and use target in order to recognize button click.

            buttonsPanel.append(button);
            button.button({
                icons: {primary: buttonConfig.icon},
                text: buttonConfig.text,
                label: buttonConfig.label
            });
            if (!primitives.common.isNullOrEmpty(buttonConfig.tooltip)) {
                if (button.tooltip != null) {
                    button.tooltip({content: buttonConfig.tooltip});
                }
            }
            topOffset += buttonsInterval + buttonConfig.size.height;
        }
    }else{
        buttonsPanel.empty();
    }
}
;
function editarAPU(cl, grid, tipo) {
    
    var fila = jQuery("#" + grid).getRowData(cl);
    $('#grupo_apu').attr({disabled: false});
    $('#nomapu').attr({disabled: false});
    $('#unmed').attr({disabled: false});
    $('#add_grupo').attr({onclick: "crearGrupoApu();"});
    $('#add_unidadmed').attr({onclick: "crearUnidadMedida();"});
    $('#grupo_apu').val(fila['idgrupo']);
    $('#nomapu').val(fila['nombreapu']);
    $('#unmed').val(fila['idunidad']);
    $('#cantidadapu').val(fila['cantidad']);
    $('#tipo_apu').val(fila['tipo_apu']);
    $('#tbl_insumos').jqGrid('GridUnload');
    //$('#div_apu').fadeIn('slow');
    AbrirDivAPU(tipo);
    GridInsumosEdit(cl, tipo);
}

function GridInsumosEdit(ids, tipo) {

    var grid_ingreso_insumos = jQuery("#tbl_insumos"), arr = [], ColModel = [], ancho = '1400', id = $('#id').val();
    //var id = "0";
    var url = '/fintra/controlleropav?estado=Maestro&accion=Proyecto&opcion=51&ids=' + ids + '&tipo=' + tipo + '&id=' + id;

    arr.push('ID', 'Tipo Insumo', 'Id Tipo Insumo', 'Descripcion Insumo', 'Find', 'Id Descripcion Insumo', 'Unidad de Medida', 'Id Unidad de Medida', 'Nueva UM', 'Cantidad', 'Rendimiento');
    ColModel.push({name: 'id', index: 'id', hidden: true, key: true},
    {name: 'tipoinsumo', index: 'tipoinsumo', align: 'center', width: 140, search: false, edittype: 'select', editrules: {required: true} ,
        editoptions: {
            //value: ajaxSelect('ConsultaTiposInsumos', ''),
            value: listarTiposMaterial(),
            style: "width: 140px",
            dataInit: function (elem) {
                if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
                    // we are NOT in the searching bar
                    $(elem).find("option[value=\"\"]").remove();
                    setTimeout(function () {
                        $(elem).trigger('change');
                    }, 500);
                }
            },
            dataEvents: [{type: 'change', fn: function (e) {
                        try {

                            var rowid = e.target.id.replace("_tipoinsumo", "");
                            id = e.target.value.replace("_", "");
                            grid_ingreso_insumos.jqGrid('setCell', rowid, "idtipoinsumo", id);

                            console.log(grid_ingreso_insumos.jqGrid('getCell', rowid, "idtipoinsumo"));
                            console.log($('#_tipo_insumo').val());
                            if (grid_ingreso_insumos.jqGrid('getCell', rowid, "idtipoinsumo") !== $('#_tipo_insumo').val())
                            {
                                grid_ingreso_insumos.jqGrid('setCell', rowid, "descripcionins", " ");
                            }

                            ////////////////////////////
                            /*var jsonIns = listarInsumosXTipo(e.target.value.replace("_", ""));
                             cargaListaDependentSelect(rowid + "_descripcionins", jsonIns);
                             var idinsumo = grid_ingreso_insumos.getRowData(rowid).iddescripcionins;
                             $("select#" + rowid + "_descripcionins").val(idinsumo);
                             $("select#" + rowid + "_descripcionins").trigger('change');*/
                            ////////////////////////////

                        } catch (exc) {
                        }
                        return;
                    }
                }]
        }
    },
    {name: 'idtipoinsumo', index: 'idtipoinsumo', hidden: true},
    {name: 'descripcionins', index: 'descripcionins', width: 400, edittype: 'text', resizable: false, align: 'left'},
    {name: 'finddescripcionins', index: 'finddescripcionins', width: 30, edittype: 'text', resizable: false, align: 'center', hidden:true},
    {name: 'iddescripcionins', index: 'iddescripcionins', hidden: true},
    {name: 'unidadmedida', index: 'unidadmedida', align: 'center', width: 100, search: false,  edittype: 'select', hidden:true, editrules: {required: false},
        editoptions: {
            value: ajaxSelect('ConsultaUnidadesMed', ''),
            style: "width: 150px",
            dataInit: function (elem) {
                if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
                    // we are NOT in the searching bar
                    $(elem).find("option[value=\"\"]").remove();
                    setTimeout(function () {
                        $(elem).trigger('change');
                    }, 500);
                }
            },
            dataEvents: [{type: 'change', fn: function (e) {
                        try {

                            var rowid = e.target.id.replace("_unidadmedida", "");
                            id = e.target.value.replace("_", "");
                            grid_ingreso_insumos.jqGrid('setCell', rowid, "idunidadmedida", id);
                        } catch (exc) {
                        }
                        return;
                    }
                }, {type: "keyup", fn: function (e) {
                        $(e.target).trigger("change");
                    }
                }]
        }
    },
    {name: 'idunidadmedida', index: 'idunidadmedida', hidden: true},
    {name: 'newunidadmedida', hidden: false, index: 'newunidadmedida', width: 50, edittype: 'text', resizable: false, align: 'center', hidden:true},
    {name: 'cantidad', index: 'cantidad', align: 'center', width: 60, search: false,  formatter: 'number',
        formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 2, prefix: ""},
        editoptions: {size: 50, dataInit: function (elem) {
                $(elem).bind("keypress", function (e) {
                    return decimales(e, $(this));
                });
            },
            dataEvents: [{type: 'change', fn: function (e) {
                        try {
                            var rowid = e.target.id.replace("_cantidad", "");
                            var cant = e.target.value.replace("_", "");

                            var rend = $('#' + rowid + "_rendimiento").val();
                            var val = grid_ingreso_insumos.jqGrid('getCell', rowid, 'valor_insumo');

                            grid_ingreso_insumos.jqGrid('setCell', rowid, "total_insumo", cant * rend * val);

                            var total = grid_ingreso_insumos.jqGrid('getCol', 'total_insumo', false, 'sum');
                            jQuery("#tbl_insumos").jqGrid('footerData', 'set', {total_insumo: total});

                        } catch (exc) {
                        }
                        return;

                    }
                }, {type: "keyup", fn: function (e) {
                        $(e.target).trigger("change");
                    }
                }]
        }},
    {name: 'rendimiento', index: 'rendimiento', align: 'center', width: 60, search: false, formatter: 'number',
        formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 4, prefix: ""},
        editoptions: {size: 15, dataInit: function (elem) {
                $(elem).bind("keypress", function (e) {
                    return decimales(e, $(this));
                });
            },
            dataEvents: [{type: 'change', fn: function (e) {
                        try {
                            var rowid = e.target.id.replace("_rendimiento", "");
                            var cant = e.target.value.replace("_", "");

                            var rend = $('#' + rowid + "_cantidad").val();
                            var val = grid_ingreso_insumos.jqGrid('getCell', rowid, 'valor_insumo');

                            grid_ingreso_insumos.jqGrid('setCell', rowid, "total_insumo", cant * rend * val);

                            var total = grid_ingreso_insumos.jqGrid('getCol', 'total_insumo', false, 'sum');
                            jQuery("#tbl_insumos").jqGrid('footerData', 'set', {total_insumo: total});

                        } catch (exc) {
                        }
                        return;

                    }
                }, {type: "keyup", fn: function (e) {
                        $(e.target).trigger("change");
                    }
                }]
        }
    });

    if (tipo === 1) {
        arr.push('Valor', 'Total');
        ColModel.push({name: 'valor_insumo', index: 'valor_insumo', align: 'center', width: 100, search: false, formatter: 'number'},
        {name: 'total_insumo', index: 'total_insumo', align: 'center', width: 100, search: false, formatter: 'number'});
        ancho = '1400';
    }

    grid_ingreso_insumos.jqGrid({
        caption: "Lista de Insumos",
        url: url,
        datatype: 'json',
        height: 'auto',
        width: ancho,
        colNames: arr,
        colModel: ColModel,
        rowNum: 10000,
        rowTotal: 10000000,
        pager: '#page_insumos',
        loadonce: true,
        rownumWidth: 40,
        gridview: true,
        viewrecords: true,
        rownumbers: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: true,
        userDataOnFooter: true,
        reloadAfterSubmit: true,
        multiselect: false,
        cellsubmit: 'clientArray',
        editurl: 'clientArray',
        jsonReader: {
            root: 'rows',
            repeatitems: false,
            id: '0'
        },
        ajaxGridOptions: {
            async: false
        },
        gridComplete: function () {
            var ids = jQuery("#tbl_insumos").jqGrid('getDataIDs');
            for (var i = 0; i < ids.length; i++) {
                var cl = ids[i];
                fil = "<img src='/fintra/images/botones/iconos/lupaOver.gif' align='absbottom'  name='Find' id='Find' width='15' height='15' title ='Find' onclick=\"buscarInsumoxFiltro('" + cl + "');\">";
                new1 = "<img src='/fintra/images/botones/iconos/new.gif' align='absbottom'  name='nuevo' id='nuevo' width='15' height='15' title ='Nueva UM' onclick=\"crearUnidadMedida();\">";
                jQuery("#tbl_insumos").jqGrid('setRowData', ids[i], {finddescripcionins: fil});
                jQuery("#tbl_insumos").jqGrid('setRowData', ids[i], {newunidadmedida: new1});

                if (tipo === 1) {
                    var total = jQuery("#tbl_insumos").jqGrid('getCol', 'total_insumo', false, 'sum');
                    jQuery("#tbl_insumos").jqGrid('footerData', 'set', {total_insumo: total});
                }
            }
        },
        ondblClickRow: function (rowid, iRow, iCol, e) {
            grid_ingreso_insumos.jqGrid('editRow', rowid, true, function () {
                $("#_tipo_insumo").val(grid_ingreso_insumos.jqGrid('getCell', rowid, "idtipoinsumo"));
            });
            return;
        },
        restoreAfterError: true
    });
    grid_ingreso_insumos.navGrid("#page_insumos", {add: false, edit: false, del: false, search: false, refresh: false});
    $("#tbl_insumos").parents('div.ui-jqgrid-bdiv').css({"min-height": "240px", "max-height": "500px"});


}

function guardarAPUEdit(ids, tipo) {

    var grid = jQuery("#tbl_insumos")
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;
    var grupo_apu = $('#grupo_apu').val(), nomapu = $('#nomapu').val(), id_actividad = $('#id').val(), cantidad = $('#cantidadapu').val(),
            tipo_apu = $('#tipo_apu').val(), unidad_medida = $('#unmed').val();
    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);
        if ((data.descripcionins === '') || (typeof data.descripcionins === "undefined") || (data.descripcionins === " ")) {
            error = true;
            mensajesDelSistema('Escoja Un Insumo', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else {
            if ((data.unidadmedida === '') || (typeof data.unidadmedida === "undefined")) {

                error = true;
                mensajesDelSistema('Escoja una Unidad de Medida', '300', 'auto', false);
                grid.jqGrid('editRow', filas[i], true, function () {
                    $("input, select", e.target).focus();
                });
                break;
            } else {
                if ((data.cantidad === '') || (typeof data.cantidad === "undefined")) {
                    error = true;
                    mensajesDelSistema('Digite la Cantidad', '300', 'auto', false);
                    grid.jqGrid('editRow', filas[i], true, function () {
                        $("input, select", e.target).focus();
                    });
                    break;
                }
            }

        }

        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        //mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
        toastr.error("Inserte al menos un registro", "Error");
    }

    if ($('#nomapu').val() === '') {
        error = true;
        //mensajesDelSistema('Digite el Nombre del APU', '300', 'auto', false);
        toastr.error("Digite el Nombre del APU", "Error");

    }

    if ($('#unidadm').val() === '') {
        error = true;
        //mensajesDelSistema('Escoja una Unidad de Medida', '300', 'auto', false);
        toastr.error("Escoja una Unidad de Medida", "Error");
    }
    if ($('#grupo_apu').val() === '') {
        error = true;
        //mensajesDelSistema('Escoja una Grupo de Apu', '300', 'auto', false);
        toastr.error("Escoja una Grupo de Apu", "Error");
    }
    if ($('#unmed').val() === '') {
        error = true;
        //mensajesDelSistema('Escoja una Unidad de Medida', '300', 'auto', false);
        toastr.error("Escoja una Unidad de Medida", "Error");
    }

    if (error)
        return;
    $.ajax({
        url: "/fintra/controlleropav?estado=Maestro&accion=Proyecto",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 39, informacion: JSON.stringify({rows: filas}), grupo_apu: grupo_apu, nomapu: nomapu, ids: ids, id_actividad: id_actividad, tipo: tipo, cantidad: cantidad, tipo_apu: tipo_apu, unidad_medida: unidad_medida},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    toastr.success(json.mensaje, 'OK');
                    $('#nomapu').val('');
                    $('#tbl_insumos').jqGrid('GridUnload');

//                    CargarGridApus(ids);
                    CargarGridApusAsoc(ids);

                    refrescarGridActividades();
                    $("#div_apu").dialog("destroy");
                }
            } catch (exc) {
                console.error(exc);
            } finally {

            }
        },
        error: function () {

        }
    });
}

function VisualizarAPU(cl, grid) {
    //alert(cl);
    var fila = jQuery("#" + grid).getRowData(cl);
    $('#grupo_apu').attr({disabled: true});
    $('#nomapu').attr({disabled: true});
    $('#unmed').attr({disabled: true});
    $('#add_grupo').attr({onclick: ""});
    $('#add_unidadmed').attr({onclick: ""});
    $('#grupo_apu').val(fila['idgrupo']);
    $('#nomapu').val(fila['nombreapu']);
    $('#unmed').val(fila['idunidad']);
    $('#tbl_insumos').jqGrid('GridUnload');
    //$('#div_apu').fadeIn('slow');
    VisualizarDivAPU();
    GridInsumosVis(cl);
}

function VisualizarDivAPU() {
    $("#div_apu").dialog({
        width: 1080,
        height: 520,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Visualizar APU',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function GridInsumosVis(ids) {

    var grid_ingreso_insumos = jQuery("#tbl_insumos");
    //var id = "0";
    var url = '/fintra/controlleropav?estado=Procesos&accion=APU&opcion=9&ids=' + ids;
    /*if ($("#gview_tbl_insumos").length) {
     reloadGridIngresoCatalogo(grid_ingreso_catalogo, arrNames, arrColModel);
     } else {*/

    grid_ingreso_insumos.jqGrid({
        caption: "Lista de Insumos",
        url: url,
        datatype: 'json',
        height: '240',
        width: '1400',
        colNames: ['ID', 'Tipo Insumo', 'Id Tipo Insumo', 'Descripcion Insumo', 'Find', 'Id Descripcion Insumo', 'Unidad de Medida', 'Id Unidad de Medida', 'Nueva UM', 'Cantidad', 'Rendimiento'],
        colModel: [
            {name: 'id', index: 'id', hidden: true, key: true},
            {name: 'tipoinsumo', index: 'tipoinsumo', align: 'center', width: 140, search: false, editable: true, edittype: 'select', editrules: {required: true},
                editoptions: {
                    //value: ajaxSelect('ConsultaTiposInsumos', ''),
                    value: listarTiposMaterial(),
                    style: "width: 140px",
                    dataInit: function (elem) {
                        if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
                            // we are NOT in the searching bar
                            $(elem).find("option[value=\"\"]").remove();
                            setTimeout(function () {
                                $(elem).trigger('change');
                            }, 500);
                        }
                    },
                    dataEvents: [{type: 'change', fn: function (e) {
                                try {

                                    var rowid = e.target.id.replace("_tipoinsumo", "");
                                    id = e.target.value.replace("_", "");
                                    grid_ingreso_insumos.jqGrid('setCell', rowid, "idtipoinsumo", id);
                                    ////////////////////////////
                                    /*var jsonIns = listarInsumosXTipo(e.target.value.replace("_", ""));
                                     cargaListaDependentSelect(rowid + "_descripcionins", jsonIns);
                                     var idinsumo = grid_ingreso_insumos.getRowData(rowid).iddescripcionins;
                                     $("select#" + rowid + "_descripcionins").val(idinsumo);
                                     $("select#" + rowid + "_descripcionins").trigger('change');*/
                                    ////////////////////////////

                                } catch (exc) {
                                }
                                return;
                            }
                        }, {type: "keyup", fn: function (e) {
                                $(e.target).trigger("change");
                            }
                        }]
                }
            },
            {name: 'idtipoinsumo', index: 'idtipoinsumo', hidden: true},
            {name: 'descripcionins', index: 'descripcionins', width: 380, edittype: 'text', resizable: false, align: 'left'},
            {name: 'finddescripcionins', index: 'finddescripcionins', width: 30, edittype: 'text', resizable: false, align: 'center'},
            {name: 'iddescripcionins', index: 'iddescripcionins', hidden: true},
            {name: 'unidadmedida', index: 'unidadmedida', align: 'center', width: 100, search: false, editable: true, edittype: 'select', editrules: {required: false},
                editoptions: {
                    value: ajaxSelect('ConsultaUnidadesMed', ''),
                    style: "width: 150px",
                    dataInit: function (elem) {
                        if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
                            // we are NOT in the searching bar
                            $(elem).find("option[value=\"\"]").remove();
                            setTimeout(function () {
                                $(elem).trigger('change');
                            }, 500);
                        }
                    },
                    dataEvents: [{type: 'change', fn: function (e) {
                                try {

                                    var rowid = e.target.id.replace("_unidadmedida", "");
                                    id = e.target.value.replace("_", "");
                                    grid_ingreso_insumos.jqGrid('setCell', rowid, "idunidadmedida", id);
                                } catch (exc) {
                                }
                                return;
                            }
                        }, {type: "keyup", fn: function (e) {
                                $(e.target).trigger("change");
                            }
                        }]
                }
            },
            {name: 'idunidadmedida', index: 'idunidadmedida', hidden: true},
            {name: 'newunidadmedida', hidden: false, index: 'newunidadmedida', width: 50, edittype: 'text', resizable: false, align: 'center'},
            {name: 'cantidad', index: 'cantidad', align: 'center', width: 60, search: false, editable: true, formatter: 'number',
                formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 2, prefix: ""},
                editoptions: {size: 50, dataInit: function (elem) {
                        $(elem).bind("keypress", function (e) {
                            //alert(soloNumeros(e));
                            return decimales(e, $(this));
                        });
                    }
                }},
            {name: 'rendimiento', index: 'rendimiento', align: 'center', width: 60, search: false, editable: true, formatter: 'number',
                formatoptions: {decimalSeparator: ".", thousandsSeparator: "", decimalPlaces: 2, prefix: ""},
                editoptions: {size: 15, dataInit: function (elem) {
                        $(elem).bind("keypress", function (e) {
                            return decimales(e, $(this));
                        });
                    }
                }
            }
        ],
        rowNum: 10000,
        rowTotal: 10000000,
        pager: '#page_insumos',
        loadonce: true,
        rownumWidth: 40,
        gridview: true,
        viewrecords: true,
        rownumbers: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: false,
        reloadAfterSubmit: true,
        multiselect: false,
        cellsubmit: 'clientArray',
        editurl: 'clientArray',
        jsonReader: {
            root: 'rows',
            repeatitems: false,
            id: '0'
        },
        ajaxGridOptions: {
            async: false
        },
        gridComplete: function () {
            var ids = jQuery("#tbl_insumos").jqGrid('getDataIDs');
            for (var i = 0; i < ids.length; i++) {
                var cl = ids[i];
                fil = "<img src='/fintra/images/botones/iconos/lupaOver.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular' onclick=\"\">";
                new1 = "<img src='/fintra/images/botones/iconos/new.gif' align='absbottom'  name='nuevo' id='nuevo' width='15' height='15' title ='Nueva UM' onclick=\"\">";
                jQuery("#tbl_insumos").jqGrid('setRowData', ids[i], {finddescripcionins: fil});
                jQuery("#tbl_insumos").jqGrid('setRowData', ids[i], {newunidadmedida: new1});
            }
        },
        restoreAfterError: true
    });
    grid_ingreso_insumos.navGrid("#page_insumos", {add: false, edit: false, del: false, search: false, refresh: false});
}


function AbrirDivMostrarCotizacion() {
    var id_solicitud = $('#id_solicitud').val();
    //window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/&pagina=CotizacionSl.jsp?idaccion=" + id_solicitud, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');

    $.ajax({
        type: 'POST',
        url: "/fintra/controlleropav?estado=Maestro&accion=Proyecto",
        dataType: 'json',
        async: false,
        data: {
            opcion: 40,
            id_solicitud: id_solicitud
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }

                window.open("/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/opav/&pagina=CotizacionSl.jsp?id_accion=" + json.id_accion, '', 'top=0,left=200,scrollbars=yes,status=yes,resizable=yes,fullscreen=yes');
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ActualizarApuCant(id_apu, val, posicion) {
    var id = $('#id').val();
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 41,
            id_apu: id_apu,
            id_actividad: id,
            cantidad: val,
            posicion : posicion
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    mensajesDelSistema("Por favor ir a presupuesto y actualizarlo!!", '250', '150');
                    CargarGridApusAsoc(id);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo actualizar la cantidad!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensajeConfirmAction(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj1").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function ActualizarEstadoRel(id_apu) {
    var id = $('#id').val();
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 50,
            id_apu: id_apu,
            id_actividad: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    CargarGridApusAsoc(id);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo actualizar la cantidad!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cerrarDialogo(Nom_div) {
    $("#" + Nom_div).dialog("destroy");
}

function eventos() {
    $('#btn_filtro_apu').on('click', function () {
        filtro_apu($('#filtro_nom_apu').val());
    });

    $('#filtro_nom_apu').keypress(function (e) {
        if (e.which == 13) {
            filtro_apu($('#filtro_nom_apu').val());
        }
    });
    $('#btn_filtro_insumo').on('click', function () {
        filtro_insumo($('#filtro_insumo').val());
    });

    $('#filtro_insumo').keypress(function (e) {
        if (e.which == 13) {
            filtro_insumo($('#filtro_insumo').val());
        }
    });
}
;
function ClonarAPU(id_apu) {
    var id = $('#id').val();
    var url = './controlleropav?estado=Procesos&accion=APU';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 18,
            id_apu: id_apu
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    toastr.error(json.error, 'Error');
                    return;
                }

                if (json.respuesta === "OK") {
                    toastr.success('Al Clonar APU!!!!!!', 'Exito');
                    CargarGridApus();
                    CargarGridApusAsoc(id);

                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo actualizar la cantidad!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ClonarAPU2(id_apu) {
    var id_actividad_capitulo = $('#id').val();
    var url = './controlleropav?estado=Procesos&accion=APU';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 19,
            id_actividad_capitulo: id_actividad_capitulo,
            id_apu: id_apu
            
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    toastr.error(json.error, 'Error');
                    return;
                }

                if (json.respuesta === "OK") {
                    toastr.success('Al Clonar APU!!!!!!', 'Exito');
                    CargarGridApus();
                    CargarGridApusAsoc(id);

                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo actualizar la cantidad!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function  filtro_insumo(buscar) {
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    jQuery("#tbl_filtro_insumos").setGridParam({
        datatype: 'json',
        url: url,
        ajaxGridOptions: {
            async: false,
            data: {
                opcion: 74,
                buscar: buscar,
                categoria: $('#categoria').val(),
                subcategoria: $('#sub').val(),
                id_tipo_insumo: $('#_tipo_insumo').val()

            }
        }

    });
    jQuery('#tbl_filtro_insumos').trigger("reloadGrid");
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}
