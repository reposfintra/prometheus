function ConfirmAddPlanilla(){
   var frm = document.frmplamanual;
   var val = frm.confirm[0].checked;
   if (val){
      window.location = "controller?estado=CaravanaPlanViaje&accion=Search";
   }
   else{
      window.location = "controller?estado=Caravana&accion=Finalize&cmd=show";
   }
}

function validarInfoComun(){
   var frm = document.forms[0];
   var i = 0;
   var sw = false;
   for(i=0;i<frm.elements.length-1;i++){
      if (frm.elements[i].value != ""){
         sw = true;
         break;
      }
   }
   if (!sw) {
      alert("DEBE LLENAR AL MENOS UN CAMPO DE LA INFORMACION COMUN");
   }
   return sw;
}

function validarfrmpla(){
   var frm = document.forms[0];
   var pla = frm.planilla.value;
   if (pla == ""){
      alert("DIGITE UN NUMERO DE PLANILLA");
      return false;
   }
   return true;
}

function validarPlanviaje(){
      if ( document.frmplanvj.planilla.value == "" )
      {
         alert("Digite n�mero de la planilla");
         document.frmplanvj.planilla.focus();
         return false;
      }
      if (document.frmplanvj.placa.value == "" )
      {
         alert("Digite placa del veh�culo");
         document.frmplanvj.placa.focus();
         return false;
      }
      if (document.frmplanvj.producto.value == "" )
      {
         alert("Digite producto");
         document.frmplanvj.producto.focus();
         return false;
      }
      if (document.frmplanvj.fecha.value == "" )
      {
         alert("Seleccione fecha y hora de salida");
         document.frmplanvj.fecha.focus();
         return false;
      }
      if (document.frmplanvj.destinatario.value == "" )
      {
         alert("Digite destinatario");
         document.frmplanvj.destinatario.focus();
         return false;
      }
      if (document.frmplanvj.cedcon.value == "" )
      {
         alert("Digite c�dula del conductor");
         document.frmplanvj.cedcon.focus();
         return false;
      }
      if (document.frmplanvj.nomcon.value == "" )
      {
         alert("Digite nombre del conductor");
         document.frmplanvj.nomcon.focus();
         return false;
      }
      return true;
}
