/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {   
    cargarLineasNegocio();
    $('#listarSolicitudes').click(function(){            
        var fechainicio = $("#fecha_ini_contrato").val();
        var fechafin = $("#fecha_fin_contrato").val();
        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
            if (fechainicio > fechafin) {
               mensajesDelSistema("La fecha final no puede ser inferior a la inicial", '350', '150', false);
            } else {
                cargarSolicitudesPorAsignarBroker();
            }               
        } else {
            mensajesDelSistema("Por favor revise el rango de fecha ingresado", '350', '150', false);
        }       
    });   
    
    $("#clearSolicitudes").click(function () {
        DeshabilitarFiltroSearchSol(false);
        resetSearchValuesSol();
        $("#tabla_solicitudes_pendientes").jqGrid('GridUnload');      
    });
    
     $("#id_aseguradora, #id_beneficiario, #otro_si").change(function() {
        if($('#id_aseguradora').val()!=='0' && $('#id_beneficiario').val()!=='0') { 
            var num_contrato = $('#num_contrato').val();  
            cargarOtrosCostosAseguradora(num_contrato,$('#id_aseguradora').val(),$('#id_beneficiario').val(),$('#otro_si').val());
            cargarDetalleCxPAseguradora(num_contrato);         
        }else{        
            $('#total_gastos_cxp').val('');
            $('#total_iva_cxp').val('0');
            $('#total_cxp').val('0');
            $("#tabla_cxp_aseguradora").jqGrid("clearGridData", true);         
        }
    });
    
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });   
    
    $('.solo-numeric').live('keypress', function (event) {
        return numbersonly(this, event);
    });   
    
    $('.solo-numeric').live('blur', function (event) {
        this.value = numberConComas(this.value);
    });  
    
     $('.solo-numero').live('blur', function (event) {
        this.value = numberConComas(this.value);
    });  
    
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    }); 
    
    $('#total_gastos_cxp').live('blur', function (event) {    
        calculateTotalesCxPAseguradora();
    });      
          
    $("#fecha_ini_contrato").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    $("#fecha_fin_contrato").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });    
        
    var myDate = new Date();     
    $('#ui-datepicker-div').css('clip', 'auto');
    
    maximizarventana();
    
    autocompletar("txt_nom_cliente", 1);
    autocompletar("txt_nom_proyecto", 2);
});


function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarSolicitudesPorAsignarBroker() {     
   
    $('#div_solicitudes_pendientes').fadeIn();
    var grid_tbl_solicitudes_pendientes = jQuery("#tabla_solicitudes_pendientes");
     if ($("#gview_tabla_solicitudes_pendientes").length) {        
        refrescarGridSolicitudesPorAsignarBroker();
     }else { 
        grid_tbl_solicitudes_pendientes.jqGrid({
            caption: "Listado de Solicitudes Para asignar a broker",
            url: "./controlleropav?estado=Minutas&accion=Contratacion",           	 
            datatype: "json",  
            height: '450',
            width: '1450',          
            colNames: ['idSolicitud','Fecha Creacion','Nombre Cliente', 'Nombre Proyecto', 'Foms', 'Etapa', 'Valor Cotizacion',
                'Id Cliente',  'Tipo Solicitud','F Contrato',  'Responsable', 'Interventor','IdTrazabilidad', 'Id estado','Nombre estado', 'No Contrato',
                'Tipo Contrato', 'Cot. Broker',  'Cot. Aceptada', 'CxP Aseguradora', 'Cotizado', 'Accion'],
            colModel: [
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, align: 'left', key: true},
                {name: 'creation_date', index: 'creation_date', width: 130, align: 'center'},
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 230, align: 'left'},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 230, align: 'left'},
                {name: 'num_os', index: 'num_os', width: 139, align: 'center', hidden:true},
                {name: 'etapa_trazabilidad', index: 'etapa_trazabilidad', width: 143, align: 'left'},
                {name: 'valor_cotizacion', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                
                {name: 'id_cliente', index: 'id_cliente', width: 110, align: 'left', hidden:true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 110, align: 'left', hidden:true},             
                {name: 'fecha_contrato', index: 'fecha_contrato', width: 110, align: 'center', hidden:true},
                {name: 'Responsable', index: 'Responsable', width: 130, align: 'left', hidden:true},
                {name: 'Interventor', index: 'Interventor', width: 130, align: 'left', hidden:true},
                {name: 'id_trazabilidad', index: 'id_trazabilidad', width: 110, align: 'center', hidden:true},
                {name: 'id_estado', index: 'id_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'nombre_estado', index: 'nombre_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},                
                {name: 'num_contrato', index: 'num_contrato', width: 110, align: 'center', hidden: true},
                {name: 'tipo_contrato', index: 'tipo_contrato', width: 110, align: 'center', hidden: true},               
                {name: 'cotizado_broker', index: 'cotizado_broker', width: '80px', align: 'center', hidden: true},
                {name: 'cotizacion_aceptada', index: 'cotizacion_aceptada', width: '80px', align: 'center', hidden: true},
                {name: 'cxp_aseguradora', index: 'cxp_aseguradora', width: '165', align: 'center', hidden: true},
                
                
                {name: 'cotizado', index: 'cotizado', width: '80px', align: 'center', fixed: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '120px', search: false}               
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_solicitudes_pendientes'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            subGrid: true,   
            subGridOptions: { "plusicon" : "ui-icon-triangle-1-e",
                      "minusicon" :"ui-icon-triangle-1-s",
                      "openicon" : "ui-icon-arrowreturn-1-e",
                      "reloadOnExpand" : true,
                      "selectOnExpand" : true },
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,  
            multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                    opcion: 25,
                    fecha_inicial:$('#fecha_ini_contrato').val(),
                    fecha_final:$('#fecha_fin_contrato').val(),
                    num_solicitud: $('#num_solicitud').val(),
                    nombre_cliente: $('#nombre_cliente').val(),
                    lineaNegocio: $("#linea_negocio").val(),
                    responsable: $("#responsable").val(),
                    trazabilidad: '4',
                    etapaActual: $('#etapaActual').val(),
                    id_cliente: $('#id_cliente').val(),
                    nom_proyecto: $('#txt_nom_proyecto').val(),
                    foms: $('#txt_foms').val(),
                    tipo_proyecto: $('#tipo_proyecto').val()
                }
            },   
            gridComplete: function () {
                var ids = grid_tbl_solicitudes_pendientes.jqGrid('getDataIDs');
                var fila;
                var tipo_contrato,num_contrato,cotizado_broker,cotizacion_aceptada,cxp_aseguradora;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tbl_solicitudes_pendientes.jqGrid("getLocalRow", cl);                
                    tipo_contrato = fila.tipo_contrato;
                    num_contrato = fila.num_contrato;
                    cotizado_broker = fila.cotizado_broker;
                    cotizacion_aceptada = fila.cotizacion_aceptada;
                    cxp_aseguradora = fila.cxp_aseguradora;
                    if (cotizado_broker === 'N') {
                        be = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                    } else {
                        be = '<img src = "/fintra/images/flag_green.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                    }
                    grid_tbl_solicitudes_pendientes.jqGrid('setRowData', ids[i], {cotizado: be});
                                 
                    //broker = "<input style='height:26px;width:57px;' type='button' name='broker' id='generar'value='Broker'  width='19' height='19' title ='Abrir ventana de asignaci�n del broker '  onclick=\"AbrirDivAsignarBroker('" + cl + "');\">";
                    broker = "<img src='/fintra/images/opav/Broker.png' align='absbottom'  name='Broker' id='Broker' width='23' height='23' title ='Abrir ventana de asignaci�n del broker ' onclick=\"AbrirDivAsignarBroker('" + cl + "');\">";
                    //cxp = "<input style='height:26px;width:57px;' type='button' name='cxp' id='generar'value='CXP'  width='19' height='19' title ='Abrir ventana CXP Aseguradora'  onclick=\"AbrirDivGenerarCxPAseguradora('" + num_contrato + "');\">";
                    
                    if(cxp_aseguradora!=='' || (cotizado_broker==='S' && cotizacion_aceptada==='N')){
                        //grid_tbl_solicitudes_pendientes.jqGrid('setRowData', ids[i], {actions:  broker });
                        cxp = "<img src='/fintra/images/opav/CxP_grey.png' align='absbottom'  name='cxp' id='cxp' width='23' height='23' title ='Abrir ventana CXP Aseguradora');\">";
                    }else if(cotizado_broker==='S' && cotizacion_aceptada==='S'){                    
                        //grid_tbl_solicitudes_pendientes.jqGrid('setRowData', ids[i], {actions: broker +'     '+ cxp});
                        cxp = "<img src='/fintra/images/opav/CxP.png' align='absbottom'  name='cxp' id='cxp' width='23' height='23' title ='Abrir ventana CXP Aseguradora'  onclick=\"AbrirDivGenerarCxPAseguradora('" + num_contrato + "');\">";
                    }else{
                        //grid_tbl_solicitudes_pendientes.jqGrid('setRowData', ids[i], {actions: broker});
                        cxp = "<img src='/fintra/images/opav/CxP_grey.png' align='absbottom'  name='cxp' id='cxp' width='23' height='23' title ='Abrir ventana CXP Aseguradora'  );\">";
                    }                   
                    grid_tbl_solicitudes_pendientes.jqGrid('setRowData', ids[i], {actions: broker +'     '+ cxp});
                }
            },
            subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id = subgrid_id+"_t"; 
                var num_contrato = $("#tabla_solicitudes_pendientes").getRowData(row_id).num_contrato;              
                jQuery("#"+subgrid_id).html("<table id='"+subgrid_table_id+"'></table>");
                jQuery("#"+subgrid_table_id).jqGrid({
                    url: './controlleropav?estado=Minutas&accion=Contratacion',                                          
                    datatype: 'json',
                    width: '630',  
                    colNames: ['Id','Id Aseguradora','Aseguradora', 'Broker', 'Id Beneficiario','Beneficiario', 'Secuencia', 'Otro Si', 'Valor Prima', 'Otros gastos', 'Valor IVA', 'Total', 'Cotiz. Broker','Cotiz. aceptada'],
                    colModel: [
                        {name: 'id', index: 'id', sortable: true, width: 60, align: 'center', hidden:true, key:true},
                        {name: 'id_aseguradora', index: 'id_aseguradora', sortable: true, width: 60, align: 'center', hidden:true},
                        {name: 'nombre_seguro', index: 'nombre_seguro', sortable: true, align: 'left',  width: 333},
                        {name: 'broker', index: 'broker', sortable: true, align: 'left',  width: 165},
                        {name: 'id_beneficiario', index: 'id_beneficiario', sortable: true, width: 60, align: 'center', hidden:true, key:true},
                        {name: 'beneficiario', index: 'beneficiario', sortable: true, align: 'left',  width: 150},
                        {name: 'secuencia', index: 'secuencia', sortable: true, width: 60, align: 'center', hidden:true, key:true},
                        {name: 'otro_si', index: 'otro_si', sortable: true, align: 'left',  width: 110},
                        {name: 'valor_prima', index: 'valor_prima', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                        formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_otros_gastos', index: 'valor_otros_gastos', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_iva', index: 'valor_iva', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                        {name: 'valor_total', index: 'valor_total', sortable: true, width: 110, align: 'right', search: false, sorttype: 'number',
                                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},                      
                        {name: 'cotiz_broker_aceptada', index: 'cotiz_broker_aceptada', width: '80px', align: 'center', hidden: true},  
                        {name: 'cambio', index: 'cambio', width: '110px', align: 'center'}
                    ],
                    rowNum:150,
                    height:'80%',
                    width:'90%',  
                    jsonReader: {
                        root: 'rows',
                        cell:'',
                        repeatitems: false,
                        id: '0'
                    },  
                    ajaxGridOptions: {
                        type: "POST",
                        async: false,
                        data: {
                            opcion: 32,
                            num_contrato: num_contrato,
                            cotizado_broker: 'S'
                        }
                    },   
                    gridComplete: function (index) {                        
                        var cant = jQuery("#"+subgrid_table_id).jqGrid('getDataIDs');
                        for (var i = 0; i < cant.length; i++) {
                            var id_aseguradora = $("#"+subgrid_table_id).getRowData(cant[i]).id_aseguradora;
                            var id_beneficiario = $("#"+subgrid_table_id).getRowData(cant[i]).id_beneficiario;
                            var secuencia = $("#"+subgrid_table_id).getRowData(cant[i]).secuencia;
                            var cambioEstado = $("#"+subgrid_table_id).getRowData(cant[i]).cambio;
                            var cl = cant[i];
                            be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"seleccionaCotizacionBroker('" + num_contrato + "','" + id_aseguradora + "','" + id_beneficiario + "','" + secuencia + "','" + row_id + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 20px;height: 16px;'></span> </label></div>";
                            jQuery("#"+subgrid_table_id).jqGrid('setRowData', cant[i], {cambio: be});
                        }
                    },
                    ondblClickRow: function (rowid, iRow, iCol, e) {
                        var myGrid = jQuery("#tabla_solicitudes_pendientes"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                        filas = myGrid.jqGrid("getLocalRow", selRowIds);
                        var id_aseguradora = $("#"+subgrid_table_id).getRowData(rowid).id_aseguradora;                        
                        var nombre_aseguradora = $("#"+subgrid_table_id).getRowData(rowid).nombre_seguro; 
                        var secuencia = $("#"+subgrid_table_id).getRowData(rowid).secuencia; 
                        var otro_si = $("#"+subgrid_table_id).getRowData(rowid).otro_si; 
                        var id_beneficiario = $("#"+subgrid_table_id).getRowData(rowid).id_beneficiario; 
                        var beneficiario = $("#"+subgrid_table_id).getRowData(rowid).beneficiario; 
                        var valor_otros_gastos = $("#" + subgrid_table_id).getRowData(rowid).valor_otros_gastos;
                        var valor_iva = $("#" + subgrid_table_id).getRowData(rowid).valor_iva;
                        var valor_total = $("#" + subgrid_table_id).getRowData(rowid).valor_total;   
                        cargarDetalleAseguradora(num_contrato,id_aseguradora, id_beneficiario, secuencia);                      
                        $('#nombre_aseguradora').val(nombre_aseguradora);
                        $('#total_gastos').val(numberConComas(valor_otros_gastos));
                        $('#total_iva').val(numberConComas(valor_iva));
                        $('#total').val(numberConComas(valor_total)); 
                        AbrirDivDetalleAseguradora(beneficiario+' '+otro_si);

                    },
                    loadError: function(xhr, status, error) {
                        alert(error);
                    }
                });               
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_solicitudes_pendientes"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);              
                cargarGarantias(rowid);
                AbrirDivPolizasProyecto();
                

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_solicitudes_pendientes", {add: false, edit: false, del: false, search: false, refresh: false}, {});   
        jQuery("#tabla_solicitudes_pendientes").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: false,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
        });
       
    }
  }          


function refrescarGridSolicitudesPorAsignarBroker(){     
    jQuery("#tabla_solicitudes_pendientes").setGridParam({
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 25,
                fecha_inicial: $('#fecha_ini_contrato').val(),
                fecha_final: $('#fecha_fin_contrato').val(),
                num_solicitud: $('#num_solicitud').val(),
                nombre_cliente: $('#nombre_cliente').val(),
                lineaNegocio: $("#linea_negocio").val(),
                responsable: $("#responsable").val(),
                trazabilidad: '4',
                etapaActual: $('#etapaActual').val(),
                id_cliente: $('#id_cliente').val(),
                nom_proyecto: $('#txt_nom_proyecto').val(),
                foms: $('#txt_foms').val(),
                tipo_proyecto: $('#tipo_proyecto').val()
            }
        }       
    });
    
    jQuery('#tabla_solicitudes_pendientes').trigger("reloadGrid");
}

function AbrirDivPolizasProyecto() {
    $("#div_polizas_proyecto").dialog({
        width: 850,
        height: 430,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'P�lizas requeridas para el proyecto',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $(".ui-dialog-titlebar-close").hide();
}

function cargarGarantias(id_solicitud) {     
      
    var grid_tbl_garantias = jQuery("#tabla_garantias");
     if ($("#gview_tabla_garantias").length) {        
        refrescarGridGarantias();
     }else { 
        grid_tbl_garantias.jqGrid({
            caption: "Garantias",
            url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",           	 
            datatype: "json",  
            height: '150',
            width: '750', 
            colNames: ['Id', 'Id Poliza', 'Poliza','Valor Base', '% Poliza', 'Valor Poliza', 'Vigencia'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden:true},
                {name: 'id_poliza', index: 'id_poliza', width: 80, align: 'left', hidden:true},
                {name: 'poliza', index: 'poliza', width: 260, align: 'left'},
                {name: 'valor_base', index: 'valor_base', sortable: true, width: 120, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},              
                {name: 'porcentaje_poliza', index: 'porcentaje_poliza', editable:true, width: 80, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                {name: 'valor_poliza', index: 'valor_poliza', sortable: true, width: 120, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'vigencia_poliza', index: 'vigencia_poliza', width: 80, align: 'center'}              
                
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_garantias'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pgtext: null,
            pgbuttons: false, 
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                    opcion: 8,                    
                    num_solicitud: id_solicitud
                }
            },   
            gridComplete: function() {             
                var colSumBase = jQuery("#tabla_garantias").jqGrid('getCol', 'valor_base', false, 'sum');
                var colSumPol = jQuery("#tabla_garantias").jqGrid('getCol', 'valor_poliza', false, 'sum');  
                jQuery("#tabla_garantias").jqGrid('footerData', 'set', {valor_base: colSumBase});
                jQuery("#tabla_garantias").jqGrid('footerData', 'set', {valor_poliza: colSumPol});                  
            },          
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_tabla_garantias", {add: false, edit: false, del: false, search: false, refresh: false}, {});  
    }
  }          


function refrescarGridGarantias(id_solicitud){     
    jQuery("#tabla_garantias").setGridParam({
        url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            async:false,
            data: { 
                  opcion: 8,                    
                  num_solicitud: id_solicitud
            }
        }       
    });
    
    jQuery('#tabla_garantias').trigger("reloadGrid");
}

function AbrirDivAsignarBroker(id_solicitud){   
        cargarBroker();     
        cargarBeneficiariosContrato(id_solicitud);
        ventanaAsignacionBroker(id_solicitud); 
}

 
  function cargarBroker() {
    $('#broker').html('');
    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        dataType: 'json',
        async:false,
        data: {
            opcion: 26
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#broker').append("<option value='0'>Seleccione</option>");
               
                    for (var key in json) {              
                       $('#broker').append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#broker').append("<option value='0'>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ventanaAsignacionBroker(id_solicitud){
      var num_contrato = $("#tabla_solicitudes_pendientes").getRowData(id_solicitud).num_contrato;   
      $("#dialogAsignarBroker").dialog({
        width: 450,
        height: 180,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ASIGNACI�N DE BROKER',
        closeOnEscape: false,
        buttons: {  
            "Guardar": function () {
                if ($('#beneficiario').val() === '0') {
                    mensajesDelSistema('Por favor, seleccione el beneficiario!!!', '250', '150');
                    return;
                }else if ($('#broker').val() === '0') {
                    mensajesDelSistema('Por favor, seleccione el broker encargado!!!', '250', '150');
                    return;
                } else {
                   asignarBrokerMinuta(num_contrato);
                }
              
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function asignarBrokerMinuta(num_contrato){
       $.ajax({
        type: "POST",
        url: './controlleropav?estado=Minutas&accion=Contratacion',
        async: false,
        dataType: "json",
        data: {
            opcion: 27,        
            num_contrato: num_contrato,
            id_broker: $('#broker').val(),
            id_beneficiario: $('#beneficiario').val()
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '270', '165');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridSolicitudesPorAsignarBroker();
                    $("#dialogAsignarBroker").dialog('close');
                    mensajesDelSistema(".::BROKER ASIGNADO CORRECTAMENTE::.", '250', '150');                    
                } else {
                    mensajesDelSistema(".::ERROR AL ASIGNAR BROKER::.", '250', '150');
                }

            }
        }
    });
}

function cargarDetalleAseguradora(num_contrato, id_aseguradora, id_beneficiario, secuencia) {     
      
    var grid_tbl_garantias = jQuery("#tabla_detalle_aseguradora");
     if ($("#gview_tabla_detalle_aseguradora").length) {        
        refrescarGridDetalleAseguradora(num_contrato, id_aseguradora, id_beneficiario, secuencia);
     }else { 
        grid_tbl_garantias.jqGrid({
            caption: "Detalle Aseguradora",
            url: "./controlleropav?estado=Minutas&accion=Contratacion",           	 
            datatype: "json",  
            height: '150',
            width: '486', 
            colNames: ['Id', 'P�liza', '% Base', 'Valor Asegurar', '% Prima', 'Valor Prima'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden:true}, 
                {name: 'nombre_poliza', index: 'nombre_poliza', width: 250, align: 'left'},
                {name: 'porcentaje_poliza', index: 'porcentaje_poliza', sortable:true, hidden:true, width: 90, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                {name: 'valor_poliza', index: 'valor_poliza', sortable: true, hidden:true, width: 130, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'porcentaje_aseguradora', index: 'porcentaje_aseguradora', editable:true, width: 90, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 3}},
                {name: 'valor_aseguradora', index: 'valor_aseguradora', sortable: true, width: 130, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}         
                
            ],
            rowNum: 1000,
            rowTotal: 1000,           
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pgtext: null,
            pgbuttons: false, 
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                    opcion: 33,                    
                    num_contrato: num_contrato,
                    id_aseguradora: id_aseguradora,
                    id_beneficiario: id_beneficiario,
                    secuencia:secuencia
                }
            },   
            gridComplete: function() {             
                var colSumPol = jQuery("#tabla_detalle_aseguradora").jqGrid('getCol', 'valor_poliza', false, 'sum'); 
                var colSumAseg = jQuery("#tabla_detalle_aseguradora").jqGrid('getCol', 'valor_aseguradora', false, 'sum');               
                jQuery("#tabla_detalle_aseguradora").jqGrid('footerData', 'set', {valor_poliza: colSumPol});       
                jQuery("#tabla_detalle_aseguradora").jqGrid('footerData', 'set', {valor_aseguradora: colSumAseg});
            },          
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });  
    }
  }          


function refrescarGridDetalleAseguradora(num_contrato, id_aseguradora, id_beneficiario, secuencia){     
    jQuery("#tabla_detalle_aseguradora").setGridParam({
        url: "/fintra/controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            async:false,
            data:{
                    opcion: 33,                    
                    num_contrato: num_contrato,
                    id_aseguradora: id_aseguradora,
                    id_beneficiario: id_beneficiario,
                    secuencia:secuencia
            }
        }       
    });
    
    jQuery('#tabla_detalle_aseguradora').trigger("reloadGrid");
}


function AbrirDivDetalleAseguradora(tipo_beneficiario) {
     $("#div_detalle_cot_aseguradora").dialog({
        width: 570,
        height: 450,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'DETALLE COTIZACION - '+tipo_beneficiario,
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $(".ui-dialog-titlebar-close").hide();
}

function seleccionaCotizacionBroker(num_contrato,id_aseguradora,id_beneficiario,secuencia,index){    
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        data: {
            opcion: 35,
            num_contrato: num_contrato,
            id_aseguradora: id_aseguradora,
            id_beneficiario: id_beneficiario,
            secuencia: secuencia
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridSolicitudesPorAsignarBroker(); 
                    $('#tabla_solicitudes_pendientes').jqGrid('expandSubGridRow', index);
                }

            } else {
                mensajesDelSistema("Lo sentimos ocurri�n un error al seleccionar cotizaci�n!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function generarCXPAseguradora(num_contrato) {
    var jsonPolizas = [];
    var filasId =jQuery('#tabla_cxp_aseguradora').jqGrid('getGridParam', 'selarrrow');
    if (filasId != ''){
        for (var i = 0; i < filasId.length; i++) {        
            var valor_aseguradora = jQuery("#tabla_cxp_aseguradora").getRowData(filasId[i]).valor_aseguradora;         
            var polizas = {};
            polizas ["id"] = filasId[i];
            polizas ["valor_aseguradora"] = valor_aseguradora;            
            jsonPolizas.push(polizas);   
        }    
        var listPolizas = {};
        listPolizas ["polizas"] = jsonPolizas;       
      
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function () {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Minutas&accion=Contratacion",
                dataType: 'json',
                data: {
                    opcion: 38,
                    num_contrato: num_contrato,
                    listadoPolizas: JSON.stringify(listPolizas),
                    valor_gastos:numberSinComas(($('#total_gastos_cxp').val()!=='')?$('#total_gastos_cxp').val():0),
                    concepto_iva: 'IVA16',
                    valor_iva: numberSinComas($('#total_iva_cxp').val()),
                    num_factura:$('#num_factura').val()
                },
                success: function (json) {
                    if (!isEmptyJSON(json)) {
                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '250', '180');
                            return;
                        }

                        if (json.respuesta === "OK") {
                            refrescarGridDetalleCxPAseguradora();
                            refrescarGridSolicitudesPorAsignarBroker();
                            $("#dialogFactura").dialog('close');
                            $("#dialogLoading").dialog('close');                           
                            mensajesDelSistema("Exito al generar cuenta de cobro", '363', '145', true);
                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Error al generar la cuenta de cobro", '363', '140');
                        }

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos ocurri� un error al generar CXP!!", '250', '150');

                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }, 500);
        
    }else{
        if (jQuery('#tabla_cxp_aseguradora').jqGrid('getGridParam', 'records')>0) {
             mensajesDelSistema("Debe seleccionar los item con los que va a generar la cxp!!", '250', '150');
        }else{
             mensajesDelSistema("No hay items pendientes por generar cxp", '250', '150');
        }
             
    }    
}

function cargarInfoAseguradoraSeleccionada(num_contrato) {
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {opcion: 39,
               num_contrato: num_contrato},
        async: false,
        url: './controlleropav?estado=Minutas&accion=Contratacion',
        success: function (jsonData) {
            if (!isEmptyJSON(jsonData)) {                            
                $('#id_aseguradora').val(jsonData.id_aseguradora);
                $('#nombre_seguro').val(jsonData.nombre_seguro);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

  function cargarAseguradorasSeleccionadas(num_contrato) {
    $('#id_aseguradora').html('');
    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        dataType: 'json',
        async:false,
        data: {
            opcion: 39,
            num_contrato:num_contrato
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#id_aseguradora').append("<option value='0'>Seleccione</option>");
               
                    for (var key in json) {              
                       $('#id_aseguradora').append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#id_aseguradora').append("<option value='0'>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarBeneficiariosSeleccionados(num_contrato) {
    $('#id_beneficiario').html('');
    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        dataType: 'json',
        async:false,
        data: {
            opcion: 43,            
            num_contrato:num_contrato
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#id_beneficiario').append("<option value='0'>Seleccione</option>");
             
                    for (var key in json) {              
                       $('#id_beneficiario').append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#id_beneficiario').append("<option value='0'>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarOtrosSiSeleccionados(num_contrato) {
    $('#otro_si').html('');
    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        dataType: 'json',
        async:false,
        data: {
            opcion: 52,            
            num_contrato:num_contrato
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#otro_si').append("<option value='0'>...</option>");
             
                    for (var key in json) {              
                       $('#otro_si').append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#otro_si').append("<option value='0'>...</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarOtrosCostosAseguradora(num_contrato, id_aseguradora, id_beneficiario, secuencia) {
    var colSumAseg = jQuery("#tabla_cxp_aseguradora").jqGrid('getCol', 'valor_aseguradora', false, 'sum'); 
    $('#total_gastos_cxp').val('');
    $('#total_iva_cxp').val('0');
    $('#total_cxp').val('0');
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {opcion: 37,
               id_contrato: num_contrato,
               id_aseguradora:id_aseguradora,
               id_beneficiario:id_beneficiario,
               secuencia: secuencia
              },
        async: false,
        url: './controlleropav?estado=Minutas&accion=Contratacion',
        success: function (jsonData) {
            if (!isEmptyJSON(jsonData)) {               
                var total = parseFloat(colSumAseg)+parseFloat(jsonData.valor_otros_gastos)+parseFloat(jsonData.valor_iva);
                if(parseFloat(jsonData.valor_otros_gastos)>0) $('#total_gastos_cxp').val(numberConComas(jsonData.valor_otros_gastos));
                $('#total_iva_cxp').val(numberConComas(jsonData.valor_iva));
                $('#total_cxp').val(numberConComas(total)); 
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function AbrirDivGenerarCxPAseguradora(num_contrato){   
        $('#total_gastos_cxp').val('');
        $('#total_iva_cxp').val(0);
        $('#total_cxp').val(0);
        //cargarInfoAseguradoraSeleccionada(num_contrato);
        $('#num_contrato').val(num_contrato);
        cargarAseguradorasSeleccionadas(num_contrato);
        cargarBeneficiariosSeleccionados(num_contrato);
        cargarOtrosSiSeleccionados(num_contrato);
        cargarDetalleCxPAseguradora("");       
        ventanaCxPAseguradora(num_contrato); 
}

function ventanaCxPAseguradora(num_contrato) {
     $("#div_cxp_aseguradora").dialog({
        width: 790,
        height: 450,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'GENERAR CXP ASEGURADORA',
        closeOnEscape: false,
        buttons: {
            "Generar CxP": function () {
                var filasId = jQuery('#tabla_cxp_aseguradora').jqGrid('getGridParam', 'selarrrow');
                if (filasId != '') {
                    ventanaIngresoFactura(num_contrato);
                } else {
                    if (jQuery('#tabla_cxp_aseguradora').jqGrid('getGridParam', 'records') > 0) {
                        mensajesDelSistema("Debe seleccionar los item con los que va a generar la cxp!!", '250', '150');
                    } else {
                        mensajesDelSistema("No hay items pendientes por generar cxp", '250', '150');
                    }
                }                 
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $(".ui-dialog-titlebar-close").hide();
}


function cargarDetalleCxPAseguradora(num_contrato) {     
      
    var grid_tbl_garantias = jQuery("#tabla_cxp_aseguradora");
     if ($("#gview_tabla_cxp_aseguradora").length) {        
        refrescarGridDetalleCxPAseguradora(num_contrato);
     }else { 
        grid_tbl_garantias.jqGrid({
            caption: "CXP Aseguradora",
            url: "./controlleropav?estado=Minutas&accion=Contratacion",           	 
            datatype: "json",  
            height: '150',
            width: '752', 
            colNames: ['Id', 'P�liza',  '% Base', 'Valor Asegurar', '% Prima', 'Valor Prima'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden:true}, 
                {name: 'nombre_poliza', index: 'nombre_poliza', width: 260, align: 'left'},
                {name: 'porcentaje_poliza', index: 'porcentaje_poliza', editable:true, width: 90, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                {name: 'valor_poliza', index: 'valor_poliza', sortable: true, width: 130, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'porcentaje_aseguradora', index: 'porcentaje_aseguradora', editable:true, width: 90, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 3}},
                {name: 'valor_aseguradora', index: 'valor_aseguradora', sortable: true, width: 130, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}         
                
            ],
            rowNum: 1000,
            rowTotal: 1000,           
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            pgtext: null,
            pgbuttons: false, 
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                    opcion: 40,                    
                    num_contrato: num_contrato,
                    id_aseguradora: $('#id_aseguradora').val(),
                    id_beneficiario: $('#id_beneficiario').val(),
                    secuencia: $('#otro_si').val()
                }
            },   
            gridComplete: function() {             
                setSubtotalCxPAseguradora();
            },     
            onSelectRow: function (rowid) {
                setSubtotalCxPAseguradora();
            },
            onSelectAll: function(aRowids, status) {
               setSubtotalCxPAseguradora();
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });  
    }
  }
  
    function refrescarGridDetalleCxPAseguradora(num_contrato) {
        jQuery("#tabla_cxp_aseguradora").setGridParam({
            url: "./controlleropav?estado=Minutas&accion=Contratacion",
            datatype: 'json',
            ajaxGridOptions: {
                type: "POST",
                async: false,
                data: {
                    opcion: 40,
                    num_contrato: num_contrato,
                    id_aseguradora: $('#id_aseguradora').val(),
                    id_beneficiario: $('#id_beneficiario').val(),
                    secuencia: $('#otro_si').val()
                }
            }
        });

        jQuery('#tabla_cxp_aseguradora').trigger("reloadGrid");
    }
    
    function setSubtotalCxPAseguradora() {
        var subtotal = 0;
        var valor_gastos= parseFloat(numberSinComas(($('#total_gastos_cxp').val()!=='')?$('#total_gastos_cxp').val():0));
        var filasId = jQuery('#tabla_cxp_aseguradora').jqGrid('getGridParam', 'selarrrow');
        if (filasId != '') {
            for (var i = 0; i < filasId.length; i++) {
                var valor_aseguradora = jQuery("#tabla_cxp_aseguradora").getRowData(filasId[i]).valor_aseguradora;
                subtotal = subtotal + parseFloat(valor_aseguradora);
            }
        }
        jQuery("#tabla_cxp_aseguradora").jqGrid('footerData', 'set', {valor_aseguradora: subtotal});
        var valor_iva = parseFloat(((parseFloat(subtotal) + valor_gastos) * 19) / 100);
        var total = parseFloat(subtotal) + parseFloat(valor_iva) + parseFloat(valor_gastos);
        setTimeout(function () {
            $('#total_iva_cxp').val(numberConComas(valor_iva));
            $('#total_cxp').val(numberConComas(total));
        }, 500);    
}

   function calculateTotalesCxPAseguradora() {
        var subtotal = calculateSubtotalCxPAseguradora(); 
        var valor_gastos= parseFloat(numberSinComas(($('#total_gastos_cxp').val()!=='')?$('#total_gastos_cxp').val():0));
        var valor_iva = parseFloat(((parseFloat(subtotal) + valor_gastos) * 19) / 100);
        var total = parseFloat(subtotal) + parseFloat(valor_iva) + parseFloat(valor_gastos);
        $('#total_iva_cxp').val(numberConComas(valor_iva));
        $('#total_cxp').val(numberConComas(total));          
}

function calculateSubtotalCxPAseguradora(){
    var subtotal =0;
    var filasId = jQuery('#tabla_cxp_aseguradora').jqGrid('getGridParam', 'selarrrow');
    if (filasId != '') {
        for (var i = 0; i < filasId.length; i++) {
            var valor_aseguradora = jQuery("#tabla_cxp_aseguradora").getRowData(filasId[i]).valor_aseguradora;
            subtotal = subtotal + parseFloat(valor_aseguradora);
        }
    }
    return subtotal;
}

function ventanaIngresoFactura(num_contrato){     
      $('#num_factura').val('');   
      $("#dialogFactura").dialog({
        width: 450,
        height: 180,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'INGRESAR NUMERO DE FACTURA',
        closeOnEscape: false,
        buttons: {  
            "Aceptar": function () {
                if ($('#num_factura').val() === '') {
                    mensajesDelSistema('Por favor, ingrese el n�mero de factura!!!', '250', '150');
                    return;
                } else {
                    generarCXPAseguradora(num_contrato);
                }              
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function resetSearchValuesSol(){     
    $('#fecha_ini_contrato').val(''); 
    $('#fecha_fin_contrato').val(''); 
    $('#num_solicitud').val('');
    $('#nombre_cliente').val('');
    $('#linea_negocio').val('');
    $('#responsable').val('');
    $('#etapaActual').val('');
    $('#id_cliente').val('');
    $('#txt_nom_proyecto').val('');
    $('#txt_foms').val('');
    $('#tipo_proyecto').val('');
    $('#txt_nom_cliente').val('');
}


function DeshabilitarFiltroSearchSol(estado){          
    (estado) ? $('#fecha_ini_contrato').datepicker("destroy") : $('#fecha_ini_contrato').datepicker({dateFormat: 'yy-mm-dd'});
    (estado) ? $('#fecha_fin_contrato').datepicker("destroy") : $('#fecha_fin_contrato').datepicker({dateFormat: 'yy-mm-dd'});        
    $('#num_solicitud').attr({readonly: estado});
} 

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function mensajeConfirmAction(msj, width, height, okAction, id) {
    
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function cargarBeneficiariosContrato(id_solicitud) {
    $('#beneficiario').html('');
    var num_contrato = $("#tabla_solicitudes_pendientes").getRowData(id_solicitud).num_contrato;
    $.ajax({
        type: 'POST',
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        dataType: 'json',
        async:false,
        data: {
            opcion: 44,
            num_contrato:num_contrato
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    
                    $('#beneficiario').append("<option value='0'>Seleccione</option>");
                      
                    for (var key in json) {              
                       $('#beneficiario').append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#beneficiario').append("<option value='0'>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}
function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}