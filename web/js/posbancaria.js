function validarForma(){
    for (i = 0; i < forma.elements.length; i++){
            if (forma.elements[i].value == "" && forma.elements[i].name!="agenciaSelec" 
                    && forma.elements[i].name!="banco"  && forma.elements[i].name!="agencia"
                    && forma.elements[i].name!="fechatr"){
				forma.elements[i].focus();
				alert('No se puede procesar la informaci�n. Campo Vac�o.');
				return false;	
            } else if ( forma.elements[i].value == "campo1" && isNaN(forma.elements[i].value.replace(/,/g,'')) ){
				forma.elements[i].focus();
				alert('No se puede procesar la informaci�n. El campo debe ser n�merico.');
				return false;	
			} else if ( forma.elements[i].value == "campo5" && isNaN(forma.elements[i].value.replace(/,/g,'')) ){
				forma.elements[i].focus();
				alert('No se puede procesar la informaci�n. El campo debe ser n�merico.');
				return false;	
			}
    }
    return true;
}
	
function formatear(objeto){
	if(!isNaN(objeto.value.replace(/,/g,''))){
    	var n= parseFloat(objeto.value.replace(/,/g,''));
    	objeto.value=formatearNumero(n);
	} else {
		objeto.focus();
		alert('El valor debe ser num�rico!');
		objeto.value = 0;
	}
}
	
function formatearNumero(n){
    // para estar seguros que vamos a trabajar con un string
	var sw = 0;
	if( parseInt(n)<0 ){
		sw = 1;
		n = parseInt(n) * -1;
	}
		
    var valor = ""+n;
    var res = "";
    for( var i=valor.length-1,j = 0; i >= 0; i--,j++ ){
           if ( j % 3 == 0 && j != 0 ){
                  res += ",";
           }
           res += valor.charAt(i);
    }
    //ahora nos toca invertir el numero;
    var aux = "";
    for( var i=res.length-1; i >= 0; i-- ){
           aux += res.charAt(i);
    }
	
	if( sw == 1 ){
		aux = '-' + aux;
	}
	
    return aux;
}

function validarFormaUpdate(){
    for (i = 0; i < forma.elements.length; i++){
            if (forma.elements[i].value == "" ){
                    forma.elements[i].focus();
                    alert('No se puede procesar la informaci�n. Campo Vac�o.');
                    return false;	
            }
    }
    return true;
}

function maximizar(){
	window.moveTo(0,0);
	//if (document.all) {
		if( window.width != screen.availWidth || window.height != screen.availHeight)
			top.window.resizeTo(screen.availWidth,screen.availHeight);
	/*} else if (document.layers||document.getElementById) {
		if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
			top.window.outerHeight = screen.availHeight;
			top.window.outerWidth = screen.availWidth;
		}
	}*/	
}

function actualizarPendiente(obj, obj2){
	var n = parseInt(obj);
	var i = parseInt(obj2);
	if( n == 1 ){
		var saldoa = parseInt(forma.saldo_ant.value.replace(/,/g,''));
		var saldoi = parseInt(forma.campo1.value.replace(/,/g,''));
		
		if( !isNaN(saldoa) && !isNaN(saldoi) ){
			forma.campo2.value = saldoa - saldoi;
			formatear(forma.campo2);
		}
	} else {
		var saldoa = parseInt(forma.saldo_ant[i].value.replace(/,/g,''));
		var saldoi = parseInt(forma.campo1[i].value.replace(/,/g,''));
		
		if( !isNaN(saldoa) && !isNaN(saldoi) ){
			forma.campo2[i].value = saldoa - saldoi;
			formatear(forma.campo2[i]);
		}
	}	
}

function actualizarPendiente2(){
	var saldoa = parseInt(forma.saldo_ant.value.replace(/,/g,''));
	var saldoi = parseInt(forma.campo1.value.replace(/,/g,''));
	
	if( !isNaN(saldoa) && !isNaN(saldoi) ){
		forma.campo2.value = saldoa - saldoi;
		formatear(forma.campo2);
	}
}

function actualizarNSaldo(obj, obj2){
	var n = parseInt(obj);
	var i = parseInt(obj2);
	if( n == 1 ){
		var saldoi = parseInt(forma.campo1.value.replace(/,/g,''));
		var pendiente =  parseInt(forma.campo2.value.replace(/,/g,''));
		var anticipos = parseInt(forma.campo3.value.replace(/,/g,''));
		var proveed = parseInt(forma.campo4.value.replace(/,/g,''));
		
		if( !isNaN(saldoi) && !isNaN(pendiente) && !isNaN(anticipos) && !isNaN(proveed) ){
			forma.nuevo_saldo.value = saldoi - pendiente - ( anticipos + proveed );
			formatear(forma.nuevo_saldo);
		}
	} else {
		var saldoi = parseInt(forma.campo1[i].value.replace(/,/g,''));
		var pendiente = parseInt(forma.campo2[i].value.replace(/,/g,''));
		var anticipos = parseInt(forma.campo3[i].value.replace(/,/g,''));
		var proveed = parseInt(forma.campo4[i].value.replace(/,/g,''));
				
		if( !isNaN(saldoi) && !isNaN(pendiente) && !isNaN(anticipos) && !isNaN(proveed) ){
			forma.nuevo_saldo[i].value = saldoi - pendiente - ( anticipos + proveed );
			formatear(forma.nuevo_saldo[i]);
		}
	}	
}

function actualizarNSaldo2(){
	var saldoi = parseInt(forma.campo1.value.replace(/,/g,''));
	var pendiente =  parseInt(forma.campo2.value.replace(/,/g,''));
	var anticipos = parseInt(forma.campo3.value.replace(/,/g,''));
	var proveed = parseInt(forma.campo4.value.replace(/,/g,''));
	
	if( !isNaN(saldoi) && !isNaN(pendiente) && !isNaN(anticipos) && !isNaN(proveed) ){
		forma.nuevo_saldo.value = saldoi - pendiente - ( anticipos + proveed );
		formatear(forma.nuevo_saldo);
	}
}

function trasladarSaldos(controller){
	document.location.href = controller + '?estado=PosBancaria&accion=TrasladarSaldos&cmd=show&agency_id=' + forma.agencia.options(forma.agencia.selectedIndex).value + '&banco=' + forma.banco.options(forma.banco.selectedIndex).value + '&fecha=' + forma.fecha.value;
}

var isIE = document.all?true:false;
var isNS = document.layers?true:false;
function soloDigitos2(e,decReq, objeto) {
	//alert(window.event.keyCode);
	var key = (isIE) ? window.event.keyCode : e.which;
    var obj = (isIE) ? event.srcElement : e.target;
    var isNum = (key > 47 && key < 58) ? true:false;
    var dotOK =  (decReq=='decOK' && key ==46) ? true:false;
	var isMenos = ( key==45 ) ? true : false;
    window.event.keyCode = (!isNum && !dotOK && isIE && isMenos) ? 0:key;
    e.which = (!isNum && !dotOK && isNS && isMenos) ? 0:key;
	if( isMenos ){
		var vlr = "" + objeto.value
		if( vlr.indexOf("-")==-1 ){			
			objeto.value = '-' + objeto.value;
			objeto.maxLength = 10;
		} else {
			objeto.value = vlr.substring(1);
			objeto.maxLength = 9;
		}
	}
	if( objeto.value.indexOf("-")!=-1 && objeto.value.length == 9 ){
		objeto.maxLength = 10;
	}
    return (isNum || dotOK || !isMenos);
}
