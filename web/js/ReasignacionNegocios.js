function ListaAnalistas() {

   // var usuario = $("#"+id).val();
    $.ajax({
        type: 'POST',
        url: "/fintra/controller?estado=Negocios&accion=Reasignar",
        dataType: 'json',
        data: {
            opcion: 1         
                },
        success: function(Resp) {
            
        for(var i=0;i<Resp.length;i++) {
        // for(var j in Resp[i]) {
             $("#analista").append('<option value="' + Resp[i]["analista"] + '">' + Resp[i]["analista"] + '</option>');
         // }
         } 
       RetornarDatosNegAna();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al cargar los analistas.\n"+
                    "Favor actualizar la pagina", '350', '150',  true);
        }
    }); // fin $.ajax
}


function RetornarDatosNegAna() {


    var analista = $("#analista").val();
     //Limpiar lista del analita a reasignar 
       $("#analista_reasignado").empty();     
       $("#analista_reasignado").append('<option value=\'\'>Seleccionar Analista</option>');
       
        //Limpiar tabla del analita a reasignar
       $("#divTabNegAnaReasig").empty(); 
       $("#divTabNegAnaReasig").append("<table id=\"TabNegociosAnalistaReasig\"></table>");     
       
        //Limpiar tabla de los negocios
       $("#divTabNeg").empty(); 
       $("#divTabNeg").append("<table id=\"TabNegocios\"></table>");
     if(analista=='todos'){
         $('#analista_reasignado').attr('disabled', true)
     }else{
          $('#analista_reasignado').attr('disabled', false)
     }
    if(analista==''){
        //Limpiar tabla del analita 1
       $("#divTablaNegAna").empty(); 
       $("#divTablaNegAna").append("<table id=\"TabNegociosAnalista\"></table>");
    }else{
            $.ajax({
        type: 'POST',
           url: "/fintra/controller?estado=Negocios&accion=Reasignar",
        dataType: 'json',
        data: {
            opcion: 3,
            analista:analista            
                },
        success: function(Resp) {
             $("#divTablaNegAna").empty(); //Se limpia el div donde esta la tabla para poder actualizarla 
             $("#divTablaNegAna").append("<table id=\"TabNegociosAnalista\"></table>");
             $("#TabNegociosAnalista").jqGrid({
                
                data: Resp,       
                datatype: "local",
                height: 70,
		width: 190,
                viewrecords: true, // show the current page, data rang and total records on the toolbar
                caption: "Negocios asignados",
                rowNum: 1000,
               // pager: "#",
                
                colModel: 
                   [
                    //{ label: 'analista',      name: 'analista',       width: 60 },
                    { label: 'Estado del negocio',   name: 'estado_negocio',    width: 100, key:true },
                    {label: 'Cantidad', name: 'cant_cred_asig', index: 'cant_cred_asig', sortable: true, width: 50, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: ""}},

                   ]

            });
            ListaAnalistasReasignar();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al cargar la informacion de los negocios.\n"+
                    "Favor actualizar la pagina", '350', '150',  true);
        }
    }); // fin $.ajax
    }


}

function ListaAnalistasReasignar(){

     var analista = $("#analista").val();
     
       
       
  if(analista==""){
       $("#analista_reasignado").empty();     
       $("#analista_reasignado").append('<option value=\'\'>Seleccionar Analista</option>');
  }else
  {
     $.ajax({
        type: 'POST',
        url: "/fintra/controller?estado=Negocios&accion=Reasignar",
        dataType: 'json',
        data: {
            opcion: 2,
            analista :analista
                },
        success: function(Resp) {
            $("#analista_reasignado").empty();
            $("#analista_reasignado").append('<option value=\'\'>Seleccionar Analista</option>');
       
        for(var i=0;i<Resp.length;i++) {
        // for(var j in Resp[i]) {
             $("#analista_reasignado").append('<option value="' + Resp[i]["analista"] + '">' + Resp[i]["analista"] + '</option>');
         // }
         } 
        RetornarDatosNegAnalista();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al cargar los analistas.\n"+
                    "Favor actualizar la pagina", '350', '150',  true);
        }
      }); // fin $.ajax   
   }
}


function RetornarDatosNegAnalista() {


    var analistaAsignado = $("#analista").val();
    
         $.ajax({
        type: 'POST',
           url: "/fintra/controller?estado=Negocios&accion=Reasignar",
        dataType: 'json',
        data: {
            opcion: 6,
            analistaAsignado:analistaAsignado            
                },
        success: function(Resp) {
            
             $("#TabNegocios").jqGrid({
                
                data: Resp,       
                datatype: "local",
                height: 150,
		width: 1200,
                viewrecords: true, // show the current page, data rang and total records on the toolbar
                caption: "Negocios asignados",
               rowNum: 1000,
               // pager: "#",
                
                colModel: 
                   [
                    //{ label: 'analista',      name: 'analista',       width: 60 },
                    { label: 'Negocios',   name: 'negocio',    width: 50, key:true },
                    { label: 'Estado Negocio',   name: 'estado_negocio',    width: 80, key:true },
                    { label: 'Analista asignado',   name: 'analista',    width: 80 },
                    {label: 'Perfil', name:'perfil',index:'perfil', width:80},
                    {label: 'Monto minimo', name: 'monto_minimo', index: 'monto_minimo', sortable: true, width: 80, align: 'right', search: false, sorttype: 'currency',
                      formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
   		    {label: 'Monto maximo', name: 'monto_maximo', index: 'monto_maximo', sortable: true, width: 80, align: 'right', search: false, sorttype: 'currency',
                      formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    {label: 'Fecha asignacion', name:'fecha_asignacion',index:'fecha_asignacion', width:80},
                    {label: 'Id cliente', name: 'id_cliente', index: 'id_cliente', sortable: true, width: 65, align: 'right', search: false, sorttype: 'currency'},
                    { label: 'Cliente',   name: 'cliente',    width: 190, key:true },
                    {label: 'Valor', name: 'valor', index: 'valor', sortable: true, width: 80, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                    { label: 'Unidad',   name: 'unidad',    width: 80, key:true }
                   ]

            });
            
        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al cargar la informacion de los negocios.\n"+
                    "Favor actualizar la pagina", '350', '150',  true);
        }
    }); // fin $.ajax
    
}
function RetornarDatosNegAnaReasig() {


    var analista = $("#analista_reasignado").val();
    
     //Limpiar tabla de los negocios
       $("#divTabNeg").empty(); 
       $("#divTabNeg").append("<table id=\"TabNegocios\"></table>");
       
    if(analista==''){
          $("#divTabNegAnaReasig").empty(); //Se limpia el div donde esta la tabla para poder actualizarla 
          $("#divTabNegAnaReasig").append("<table id=\"TabNegociosAnalistaReasig\"></table>");
          RetornarDatosNegAnalista();
    }else{
         $.ajax({
        type: 'POST',
           url: "/fintra/controller?estado=Negocios&accion=Reasignar",
        dataType: 'json',
        data: {
            opcion: 3,
            analista:analista            
                },
        success: function(Resp) {
             $("#divTabNegAnaReasig").empty(); //Se limpia el div donde esta la tabla para poder actualizarla 
             $("#divTabNegAnaReasig").append("<table id=\"TabNegociosAnalistaReasig\"></table>");
             $("#TabNegociosAnalistaReasig").jqGrid({
                
                data: Resp,       
                datatype: "local",
                height: 70,
		width: 190,
                viewrecords: true, // show the current page, data rang and total records on the toolbar
                caption: "Negocios asignados",
               rowNum: 1000,
               // pager: "#",
                
                colModel: 
                   [
                    //{ label: 'analista',      name: 'analista',       width: 60 },
                    { label: 'Estado del negocio',   name: 'estado_negocio',    width: 100, key:true },
                    {label: 'Cantidad', name: 'cant_cred_asig', index: 'cant_cred_asig', sortable: true, width: 50, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: ""}}
                   ]

            });
            RetornarDatosNegReasig();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al cargar la informacion de los negocios.\n"+
                    "Favor actualizar la pagina", '350', '150',  true); 
        }
    }); // fin $.ajax
    }
}


function RetornarDatosNegReasig(){
       
       var analistaAsignado = $("#analista").val();
       var analistaAReasignar = $("#analista_reasignado").val();
       
      $.ajax({
        type: 'POST',
      //url: "../controller?estado=Perfiles&accion=Riesgos",
        url: "/fintra/controller?estado=Negocios&accion=Reasignar",
      
        dataType: 'json',
        data: {
            analistaAsignado:analistaAsignado,
            analistaAReasignar:analistaAReasignar,
            opcion: 4
            //id:5            
                },
        success: function(Resp) {
            if(Resp.length==0){
               mensajesDelSistema("No se le puede asignar ninguno de los negocios a "+analistaAReasignar, '350', '150',  true); 
               $("#analista_reasignado option[value=''] ").attr("selected",true); 
               RetornarDatosNegAnaReasig();
 
            }else{
  jQuery("#TabNegocios").jqGrid({
	datatype: "local",
	height: 150,
	width: 1300,
   	colModel:[
   		{label: 'Negocio', name:'negocio',index:'negocio', width:10},
                { label: 'Estado Negocio',   name: 'estado_negocio',    width: 12 },
               // { label: 'Analista asignado',   name: 'analista',    width: 10 },
                { label: 'Analista a reasignar',   name: 'analista_a_reasignar',    width: 10 },
   		{label: 'Perfil', name:'perfil',index:'perfil', width:12},
                {label: 'Monto minimo', name: 'monto_minimo', index: 'monto_minimo', sortable: true, width: 10, align: 'right', search: false, sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
   		{label: 'Monto maximo', name: 'monto_maximo', index: 'monto_maximo', sortable: true, width: 10, align: 'right', search: false, sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {label: 'Fecha asignacion', name:'fecha_asignacion',index:'fecha_asignacion', width:10},
                {label: 'Id cliente', name: 'id_cliente', index: 'id_cliente', sortable: true, width: 10, align: 'right', search: false, sorttype: 'currency',},
   		{label:'Cliente',name:'cliente',index:'cliente', width:25},
   		{label: 'Valor', name: 'valor', index: 'valor', sortable: true, width: 10, align: 'right', search: false, sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
   		{label: 'Unidad', name:'unidad',index:'unidad', width:10}
   	],
   	multiselect: true
        });
        for(var i=0;i<Resp.length;i++){
	jQuery("#TabNegocios").jqGrid('addRowData',Resp[i]['negocio']+'_'+Resp[i]['id_perfil'],Resp[i]); 
        }

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al cargar la informacion de los negocios.\n"+
                    "Favor actualizar la pagina", '350', '150',  true);
        }
    }); // fin $.ajax         
}




function ReasignarNegocios(){
    

    
       var analistaAsignado = $("#analista").val();
       var analistaAReasignar = $("#analista_reasignado").val();
    
       
    //guardar en una cadeana de texto los ids de los negocios marcados
     var perfiles_selecionados = '';
     var negocios_selecionados = '';
        $("input:checkbox[id^='jqg_']").each(function(index,e){
            var $this = $(this);
            if($this.is(":checked")){
                var idCheck = $this.attr("id");
                var arrayIdCheck = idCheck.split('_');//Se divide el id del Checkbox
                var cod_neg = arrayIdCheck[2];//Se guarda la parte que contiene el codigo del negocio
                var idperfil = arrayIdCheck[3];//Se guarda la parte que contiene el codigo id del perfil
                
                 negocios_selecionados += cod_neg +',';
                 perfiles_selecionados += idperfil +',';
            } 
        });
    
   if(negocios_selecionados==="" ||perfiles_selecionados==="" ){
      mensajesDelSistema("Debe de ingresar almenos un negocio", '300', '150',  true);  
   } else{
 var msj="Desea Confirmar la reasignacion de los negocios!";
   $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: 300,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Cancelar": function () {
                $(this).dialog("destroy");
                mensajesDelSistema("Asignacion cancelada", '300', '150',  true); 
            }, "Aceptar": function () {
                
     $.ajax({
        type: 'POST',
         url: "/fintra/controller?estado=Negocios&accion=Reasignar",
        dataType: 'json',
        data: {
            analistaAsignado: analistaAsignado,
            analistaAReasignar: analistaAReasignar,
            negocios_selecionados: negocios_selecionados,
            perfiles_selecionados:perfiles_selecionados,
            opcion: 5
                },
        success: function(Resp) {
            
            if(Resp=="ok"){
                 alertaExitosa("Reasignacion realizada correctamente", '300', '150'); 
            }else if(Resp.includes('no fue posible asignar la totalidad de negocios')){
                alertaExitosa(Resp, '300', '150'); 
            }else {
                  mensajesDelSistema(Resp, '300', '150',  true); 
            }
            
            

        },
        error: function(xhr, ajaxOptions, thrownError) {
            mensajesDelSistema("Error al reasignar el negocio.\n"+
                    "Favor actualizar la pagina", '350', '150',  true);
        }
    }); // fin $.ajax 
             $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
    
   }
    
}


function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}


function alertaExitosa(msj, width, height) {
    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {
                $(this).dialog("destroy");
                location.reload();
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}