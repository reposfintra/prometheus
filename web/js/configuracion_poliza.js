window.onload = () => {
    document.getElementById("unidades_de_negocios").addEventListener("change", () => {
        cargarSucursales();
    });
    document.getElementById("dpto").addEventListener("change", () => {
        cargarCiudades();
    });
    document.getElementById("ciudades").addEventListener("change", () => {
        cargarSucursales();
    });
    document.getElementById("btn_buscar").addEventListener("click", () => {
        cargarConfiguracionPoliza();
    });
    document.getElementById("btn_aplicar").addEventListener("click", () => {
        aplicarPolizasTodasSucursales();
    });
    cargarFiltro();
};

function cargarSucursales() {
    var unidad = document.getElementById("unidades_de_negocios").value;
    var ciudad = document.getElementById("ciudades").value;
    var dpto = document.getElementById("dpto").value;

    if (unidad !== "" && ciudad !== "" && dpto !== "") {
        var xhr;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        } else {
            xhr = new ActiveXObject();
        }
    
        xhr.onreadystatechange = function () {
            switch (xhr.status) {
                case 200:
                    if (xhr.readyState === 4) {
                        var json = JSON.parse(xhr.responseText);
                        var sucursales = document.getElementById("sucursales");
                        limpiarSelect(sucursales);

                        json.forEach(function (e) {
                            sucursales.add(new Option(e.descripcion, e.id));
                        });
                        cargarPolizas();
                    }
                    break;
                case 400:
                case 404:
                case 500:
                case 501:
                case 502:
                case 503:
                    mensajesDelSistema("Error en la comunicación con el servidor.");
                    break;
            }
        };
        xhr.open("GET", "./controller?estado=Fintra&accion=Soporte&opcion=137&unidad=" + unidad + "&ciudad=" + ciudad + "&departamento=" + dpto, true);
        xhr.send();
    }
}

function cargarPolizas() {
    var xhr;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject();
    }

    xhr.onreadystatechange = function () {
        switch (xhr.status) {
            case 200:
                if (xhr.readyState === 4) {
                    var json = JSON.parse(xhr.responseText);
                    var poliza = document.getElementById("poliza");
                    limpiarSelect(poliza);
                
                    json.forEach(function(e) {
                        if (e.estado !== "A") {
                            poliza.add(new Option(e.descripcion, e.id));
                        }
                    });
                    cargarAseguradoras();
                }
                break;
            case 400:
            case 404:
            case 500:
            case 501:
            case 502:
            case 503:
                mensajesDelSistema("Error en la comunicación con el servidor.");
                break;
        }
    };
    xhr.open("GET", "./controller?estado=Fintra&accion=Soporte&opcion=116", true);
    xhr.send();    
}

function cargarAseguradoras() {
    var xhr;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject();
    }

    xhr.onreadystatechange = function () {
        switch (xhr.status) {
            case 200:
                if (xhr.readyState === 4) {
                    var json = JSON.parse(xhr.responseText);
                    cargarTipoCobro();
                    var aseguradora = document.getElementById("aseguradora");
                    limpiarSelect(aseguradora);
        
                        json.forEach(function (e) {
                            if (e.estado !== "A") {
                                aseguradora.add(new Option(e.descripcion, e.id));
                            }
                        });
                }
                break;
            case 400:
            case 404:
            case 500:
            case 501:
            case 502:
            case 503:
                mensajesDelSistema("Error en la comunicación con el servidor.");
                break;
        }
    };
    xhr.open("GET", "./controller?estado=Fintra&accion=Soporte&opcion=120", true);
    xhr.send();    
}

function cargarTipoCobro() {
    var xhr;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject();
    }

    xhr.onreadystatechange = function () {
        switch (xhr.status) {
            case 200:
                if (xhr.readyState === 4) {
                    var json = JSON.parse(xhr.responseText);
                    var tipo_cobro = document.getElementById("tipo_cobro");
                    limpiarSelect(tipo_cobro);

                    json.forEach(function (e) {
                        if (e.estado !== "A") {
                            tipo_cobro.add(new Option(e.descripcion, e.id));
                        }
                    });
                    cargarValorPoliza();
                }
                break;
            case 400:
            case 404:
            case 500:
            case 501:
            case 502:
            case 503:
                mensajesDelSistema("Error en la comunicación con el servidor.");
                break;
        }
    };
    xhr.open("GET", "./controller?estado=Fintra&accion=Soporte&opcion=124", true);
    xhr.send();    
}

function cargarValorPoliza() {
    var xhr;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject();
    }

    xhr.onreadystatechange = function () {
        switch (xhr.status) {
            case 200:
                if (xhr.readyState === 4) {
                    var json = JSON.parse(xhr.responseText);
                    var valor_poliza = document.getElementById("valor_poliza");
                    limpiarSelect(valor_poliza);

                    json.forEach(function (e) {
                        if (e.estado !== "A") {
                            valor_poliza.add(new Option(e.descripcion, e.id));
                        }
                    });
                }
                break;
            case 400:
            case 404:
            case 500:
            case 501:
            case 502:
            case 503:
                mensajesDelSistema("Error en la comunicación con el servidor.");
                break;
        }
    };
    xhr.open("GET", "./controller?estado=Fintra&accion=Soporte&opcion=128", true);
    xhr.send();
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                unidad_negocio: $("#unidades_de_negocios").val(),
                sucursal: $("#sucursales").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function cargarConfiguracionPoliza() {
    var operacion;
    var grid_tabla_ = jQuery("#tabla_conf");
    if ($("#gview_tabla_conf").length) {
        reloadGridMostrar(grid_tabla_, 133);
    } else {
        grid_tabla_.jqGrid({
            caption: "Configuración Poliza",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: 'auto',
            colNames: ['Id', 'Sucursal', 'Id aseguradora', 'Aseguradora', 'Id Poliza', 'Nombre Poliza', 'Id tipo cobro', 'Tipo cobro', 'Financación', 'Id valor poliza', 'Tipo Valor poliza', 'Calculo sobre',
                'Valor Absoluto', 'Valor Porcentaje', 'Id Unidad Negocio', 'Unidad Negocio', 'Estado', 'Activar/Inactivar'],
            colModel: [               
                {name: 'id', index: 'id', width: 50, sortable: true, align: 'center', hidden: true, key: true},
                {name: 'nombre_sucursal', index: 'nombre_sucursal', width: 200, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'id_aseguradora', index: 'id_aseguradora', width: 100, sortable: false, align: 'center', hidden: true},
                {name: 'nombre_aseguradora', index: 'nombre_aseguradora', width: 200, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'id_poliza', index: 'id_poliza', width: 100, sortable: false, align: 'center', hidden: true},
                {name: 'nombre_poliza', index: 'nombre_poliza', width: 200, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'id_tipo_cobro', index: 'id_tipo_cobro', width: 100, sortable: false, align: 'center', hidden: true},
                {name: 'tipo_cobro', index: 'tipo_cobro', width: 200, sortable: false, align: 'center', hidden: false},
                {name: 'financiacion', index: 'financiacion', width: 100, sortable: false, align: 'center', hidden: false, formatter: legendFmatter},
                {name: 'id_valor_poliza', index: 'id_valor_poliza', width: 100, sortable: false, align: 'center', hidden: true},
                {name: 'tipo_valor', index: 'tipo_valor', width: 200, sortable: false, align: 'center', hidden: false},
                {name: 'valor_sobre', index: 'valor_sobre', width: 100, sortable: false, align: 'center', hidden: false, formatter: legendFmatter},
                {name: 'valor_absoluto', index: 'valor_absoluto', width: 100, sortable: false, align: 'center', hidden: false, formatter: 'number', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_porcentaje', index: 'valor_porcentaje', width: 100, sortable: false, align: 'center', hidden: false, formatter: 'number', formatoptions: {decimalSeparator: ",", decimalPlaces: 3, suffix: " %" }},
                {name: 'id_unidad_negocio', index: 'id_unidad_negocio', width: 100, sortable: false, align: 'center', hidden: true},
                {name: 'nombre_unidad_negocio', index: 'nombre_unidad_negocio', width: 100, sortable: true, align: 'center', hidden: true},
                {name: 'estado', index: 'estado', width: 100, sortable: false, align: 'left', hidden: true},
                {name: 'cambio', index: 'cambio', width: 100, sortable: false, align: 'left', hidden: false}
            ],
            rownumbers: true,
            rownumWidth: 25,
            loadonce: true,
            gridview: true,
            viewrecords: true,            
            pager: '#pager',
            multiselect: true,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 133,
                    unidad_negocio: $("#unidades_de_negocios").val(),
                    sucursal: $("#sucursales").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_conf").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_conf").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoPoliza('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_conf").jqGrid('setRowData', cant[i], { cambio: be });
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                var myGrid = jQuery("#tabla_conf"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.estado;
                var poliza = filas.id_poliza;
                var aseguradora = filas.id_aseguradora;
                var tipoCobro = filas.id_tipo_cobro;
                var valorPoliza = filas.id_valor_poliza;
                var unidadNegocio = filas.id_unidad_negocio;
                var sucursal = filas.id_sucursal;
                if (estado === " ") {
                    ventanaConfiguracion(operacion, id, poliza, aseguradora, tipoCobro, valorPoliza, unidadNegocio, sucursal);
                } else if (estado === "A") {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager", { add: false, edit: false, del: false, search: true, refresh: false }, {
        });
        $("#tabla_conf").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaConfiguracion(operacion);
            }
        });
    }
}

function ventanaConfiguracion(operacion, id, poliza, aseguradora, tipoCobro, valorPoliza, unidadNegocio, sucursal) {
    if (operacion === 'Nuevo') {
        $("#dialog").dialog({
            width: 'auto',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'CONFIGURACION POLIZA',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    insertarConfiguracion();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val("");
                    $("#poliza").val("");
                    $("#aseguradora").val("");
                    $("#tipo_cobro").val("");
                    $("#valor_poliza").val("");
                    $("#unidad_negocio").val("");
                    $("#sucursal").val("");
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id").val(id);
        $("#poliza").val(poliza);
        $("#aseguradora").val(aseguradora);
        $("#tipo_cobro").val(tipoCobro);
        $("#valor_poliza").val(valorPoliza);
        $("#unidad_negocio").val(unidadNegocio);
        $("#sucursal").val(sucursal);

        $("#dialog").dialog({
            width: 'auto',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'CONFIGURACION POLIZA',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarPoliza();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val("");
                    $("#poliza").val("");
                    $("#aseguradora").val("");
                    $("#tipo_cobro").val("");
                    $("#valor_poliza").val("");
                    $("#unidad_negocio").val("");
                    $("#sucursal").val("");
                }
            }
        });
    }
}

function insertarConfiguracion() {
    var poliza = $("#poliza").val();
    var aseguradora = $("#aseguradora").val();
    var tipoCobro = $("#tipo_cobro").val();
    var valorPoliza = $("#valor_poliza").val();
    var unidadNegocio = $("#unidades_de_negocios").val();
    var sucursal = $("#sucursales").val();

    if (poliza !== "" && aseguradora !== "" && tipoCobro !== "" && valorPoliza !== "" && unidadNegocio !== "" && sucursal !== "") {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 134,
                poliza: poliza,
                aseguradora: aseguradora,
                tipo_cobro: tipoCobro,
                valor_poliza: valorPoliza,
                unidad_negocio: unidadNegocio,
                sucursal: sucursal
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al guardar", '230', '150', true);
                    $("#dialog").dialog('close');
                    $("#id").val("");
                    $("#poliza").val("");
                    $("#aseguradora").val("");
                    $("#tipo_cobro").val("");
                    $("#valor_poliza").val("");
                    $("#unidad_negocio").val("");
                    $("#sucursal").val("");
                } else {
                    mensajesDelSistema(data.error, '230', '150', true);
                }
                cargarConfiguracionPoliza();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }
}

function ActualizarPoliza() {
    var id = $("#id").val();
    var poliza = $("#poliza").val();
    var aseguradora = $("#aseguradora").val();
    var tipoCobro = $("#tipo_cobro").val();
    var valorPoliza = $("#valor_poliza").val();
    var unidadNegocio = $("#unidades_de_negocios").val();
    var sucursal = $("#sucursales").val();

    if (id !== "" && poliza !== "" && aseguradora !== "" && tipoCobro !== "" && valorPoliza !== "" && unidadNegocio !== "" && sucursal !== "") {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 135,
                id: id,
                poliza: poliza,
                aseguradora: aseguradora,
                tipo_cobro: tipoCobro,
                valor_poliza: valorPoliza,
                unidad_negocio: unidadNegocio,
                sucursal: sucursal
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#dialog").dialog('close');
                    $("#id").val("");
                    $("#poliza").val("");
                    $("#aseguradora").val("");
                    $("#tipo_cobro").val("");
                    $("#valor_poliza").val("");
                    $("#unidad_negocio").val("");
                    $("#sucursal").val("");
                } else {
                    mensajesDelSistema(data.error, '230', '150', true);
                }
                cargarConfiguracionPoliza();
            }, error: function (result) {
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function CambiarEstadoPoliza(rowid) {
    var grid_tabla = jQuery("#tabla_conf");
    var id = grid_tabla.getRowData(rowid).id;
    var estado = grid_tabla.getRowData(rowid).estado;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 136,
            id: id,
            status: toggleStatus(estado)
        },
        success: function (data) {
            cargarConfiguracionPoliza();
        }, error: function (result) {
            alert('ERROR ');
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function toggleStatus(estado) {
    return estado === "" || estado === " " ? "A" : "";
}

function limpiarSelect(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
    element.add(new Option("...", ""));
}

function legendFmatter(cellvalue, options, rowObject) {
    switch (cellvalue) {
        case "S":
            return "Si";
        case "N":
        case "NLL":
            return "No";
        case "A":
            return "Anticipado";
        case "C":
            return "Cuota";
        case "K":
            return "Capital";
        case "KI":
            return "Capital e Interes";
    }
}

function cargarCiudades() {
    var xhr;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject();
    }

    xhr.onreadystatechange = function () {
        switch (xhr.status) {
            case 200:
                if (xhr.readyState === 4) {
                    var json = JSON.parse(xhr.responseText);
                    var ciudades = document.getElementById("ciudades");
                    limpiarSelect(ciudades);

                    for (var c of json) {
                        ciudades.add(new Option(c.nombre, c.id));
                    }
                }
                break;
            case 400:
            case 404:
            case 500:
            case 501:
            case 502:
            case 503:
                mensajesDelSistema("Error en la comunicación con el servidor.");
                break;
        }
    };
    xhr.open("GET", "./controller?estado=Fintra&accion=Soporte&opcion=138&dpto=" + document.getElementById("dpto").value, true);
    xhr.send();
}

function cargarFiltro() {
    var xhr;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject();
    }

    xhr.onreadystatechange = function () {
        switch (xhr.status) {
            case 200:
                if (xhr.readyState === 4) {
                    var json = JSON.parse(xhr.responseText);
                    var dptos = document.getElementById("dpto");
                    var un = document.getElementById("unidades_de_negocios");

                    limpiarSelect(un);
                    limpiarSelect(dptos);

                    for (var u in json.unidades) {
                        un.add(new Option(json.unidades[u], u));
                    }
                    for (var d of json.departamentos) {
                        dptos.add(new Option(d.nombre, d.id));
                    }
                }
                break;
            case 404:
            case 500:

                mensajesDelSistema("Error en la comunicación con el servidor.");
                break;
        }
    };
    xhr.open("GET", "./controller?estado=Fintra&accion=Soporte&opcion=139", true);
    xhr.send();
}

function aplicarPolizasTodasSucursales() {
    var grid = $("#tabla_conf");
    var rowKey = grid.getGridParam("selrow");

    if ( grid[0].childElementCount === 0)
        mensajesDelSistema("Primero realice la búsqueda de las configuraciones");
    else if (!rowKey) {
        mensajesDelSistema("Debe seleccionar al menos una configuración");
    } else {
        var selectedIDs = grid.getGridParam("selarrrow");
        
        var xhr;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        } else {
            xhr = new ActiveXObject();
        }

        xhr.onreadystatechange = function () {
            switch (xhr.status) {
                case 200:
                    if (xhr.readyState === 4) {
                        var json = JSON.parse(xhr.responseText);
                        mensajesDelSistema(json.respuesta);
                    }
                    break;
                case 400:
                case 404:
                case 500:
                case 501:
                case 502:
                case 503:
                    mensajesDelSistema("Error en la comunicación con el servidor.");
                    break;
            }
        };
        xhr.open("POST", "./controller?estado=Fintra&accion=Soporte&opcion=140", true);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify({configuraciones: selectedIDs}));
    }
}
