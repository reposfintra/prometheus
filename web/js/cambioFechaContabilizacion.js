/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#subir").click(function () {
        var examinar = $("#examinar").val();
        validarExtension(examinar);
    });

    $("#fechainicio").val('');
    $("#fechafin").val('');
    $("#fechainicio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $("#fechainicio").datepicker("setDate", myDate);
    $("#fechafin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    cargar(7);
});

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function validarExtension(examinar) {
    var ext = /.XLS$/gi;
    var nombre = $("#examinar").val().split('\\').pop();

    if (!ext.test(examinar.toUpperCase())) {
        mensajesDelSistema("la extension del archivo no es valida", '410', '150', false);
    } else {
        if (nombre !== 'cambioDocumentos.xls') {
            mensajesDelSistema("archivo no valido", '410', '150', false);
        } else {
            // subirArchivo();
            var info = jQuery("#tabla_documentos").getGridParam('records');
            if (info > 0) {
                mensajesDelSistema("Hay documentos por procesar", '250', '150', false);
            } else {
                subirArchivo();
            }

        }

    }
}

function subirArchivo() {

    var fd = new FormData(document.getElementById('formulario'));

    $.ajax({
        async: false,
        url: "./controller?estado=Fintra&accion=Soporte&opcion=4",
        type: 'POST',
        dataType: 'json',
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function (json) {

            if (json.json === 'OK')
                cargar(7);

        }, error: function (xhr, ajaxOptions, thrownError) {

            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}



function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        title: 'Mensaje',
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    //  $("#info").siblings('div.ui-dialog-titlebar').remove();
}


function cargar(opcion) {
    var filaInvalida = 0;
    var grid_tabla_ = jQuery("#tabla_documentos");
    if ($("#gview_tabla_documentos").length) {
        reloadGridMostrar(grid_tabla_, opcion);
    } else {
        grid_tabla_.jqGrid({
            caption: "Documentos",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '1420',
            colNames: ['Id', 'Tipo', 'Documento', 'Tipo_Documento', 'Nit', 'Banco', 'Sucursal', 'Cambio', 'Fecha_nueva', 'Periodo_actual', 'Periodo_Nuevo', 'Grupo Trasaccion'],
            colModel: [
                {name: 'id', index: 'id', width: 120, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'tipo', index: 'tipo', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'documento', index: 'documento', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'tipo_documento', index: 'Tipo_Documento', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'nit', index: 'nit', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'banco', index: 'banco', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'sucursal', index: 'sucursal', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'cambio', index: 'cambio', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'fecha_nueva', index: 'sucursal', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'periodo', index: 'periodo', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'periodo_nuevo', index: 'periodo_nuevo', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'grupo_transaccion', index: 'grupo_transaccion', width: 120, sortable: true, align: 'left', hidden: false}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: opcion
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {
                var ids = grid_tabla_.jqGrid('getDataIDs');
                var allRowsInGrid = grid_tabla_.jqGrid('getRowData');
                for (var i = 0; i < allRowsInGrid.length; i++) {
                    var cl = ids[i];
                    var tipo = grid_tabla_.getRowData(cl).tipo;
                    var documento = grid_tabla_.getRowData(cl).documento;
                    var tipo_documento = grid_tabla_.getRowData(cl).tipo_documento;
                    var nit = grid_tabla_.getRowData(cl).nit;
                    var banco = grid_tabla_.getRowData(cl).banco;
                    var sucursal = grid_tabla_.getRowData(cl).sucursal;
                    var cambio = grid_tabla_.getRowData(cl).cambio;
                    var fecha_nueva = grid_tabla_.getRowData(cl).fecha_nueva;
                    var periodo = grid_tabla_.getRowData(cl).periodo;
                    var periodo_nuevo = grid_tabla_.getRowData(cl).periodo_nuevo;
                    var grupo_transaccion = grid_tabla_.getRowData(cl).grupo_transaccion;
                    if (tipo === 'CXP' && cambio === 'P') {
                        var valinfo = isNaN(nit);

                        if (fecha_nueva === '0099-01-01' && periodo !== '' && periodo_nuevo !== '' && grupo_transaccion !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo_nuevo", "", {'background-color': '#90CC00', 'background-image': 'none'});
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "grupo_transaccion", "", {'background-color': '#90CC00', 'background-image': 'none'});
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#90CC00', 'background-image': 'none'});
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        } else {
                            if (fecha_nueva !== '0099-01-01') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_nueva", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                            if (periodo === '') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                            if (grupo_transaccion === '') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "grupo_transaccion", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                            if (periodo_nuevo === '') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo_nuevo", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                        }
                    } else if (tipo === 'CXP' && cambio === 'F') {
                        if (fecha_nueva === '0099-01-01') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_nueva", "", {'background-color': '#F32541', 'background-image': 'none'});
                        } else {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_nueva", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        }
                        if (periodo !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#F32541', 'background-image': 'none'});
                        }
                        if (grupo_transaccion !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "grupo_transaccion", "", {'background-color': '#F32541', 'background-image': 'none'});
                        }
                        if (periodo_nuevo !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo_nuevo", "", {'background-color': '#F32541', 'background-image': 'none'});
                        }
                    }
                    if (tipo === 'EGRESO' && cambio === 'P') {
                        if (fecha_nueva === '0099-01-01' && periodo !== '' && periodo_nuevo !== '' && grupo_transaccion !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo_nuevo", "", {'background-color': '#90CC00', 'background-image': 'none'});
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "grupo_transaccion", "", {'background-color': '#90CC00', 'background-image': 'none'});
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#90CC00', 'background-image': 'none'});
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        } else {
                            if (fecha_nueva !== '0099-01-01') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_nueva", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                            if (periodo === '') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                            if (grupo_transaccion === '') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "grupo_transaccion", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                            if (periodo_nuevo === '') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo_nuevo", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                        }
                    } else if (tipo === 'EGRESO' && cambio === 'F') {
                        if (fecha_nueva === '0099-01-01') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_nueva", "", {'background-color': '#F32541', 'background-image': 'none'});
                        } else {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_nueva", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        }
                        if (periodo !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#F32541', 'background-image': 'none'});
                        }
                        if (grupo_transaccion !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "grupo_transaccion", "", {'background-color': '#F32541', 'background-image': 'none'});
                        }
                        if (periodo_nuevo !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo_nuevo", "", {'background-color': '#F32541', 'background-image': 'none'});
                        }
                    }
                    if (tipo === 'INGRESO' && cambio === 'P') {
                        if (fecha_nueva === '0099-01-01' && periodo !== '' && periodo_nuevo !== '' && grupo_transaccion !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo_nuevo", "", {'background-color': '#90CC00', 'background-image': 'none'});
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "grupo_transaccion", "", {'background-color': '#90CC00', 'background-image': 'none'});
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#90CC00', 'background-image': 'none'});
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        } else {
                            if (fecha_nueva !== '0099-01-01') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_nueva", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                            if (periodo === '') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                            if (grupo_transaccion === '') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "grupo_transaccion", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                            if (periodo_nuevo === '') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo_nuevo", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                        }
                    } else if (tipo === 'INGRESO' && cambio === 'F') {
                        if (fecha_nueva === '0099-01-01') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_nueva", "", {'background-color': '#F32541', 'background-image': 'none'});
                        } else {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_nueva", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        }
                        if (periodo !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#F32541', 'background-image': 'none'});
                        }
                        if (grupo_transaccion !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "grupo_transaccion", "", {'background-color': '#F32541', 'background-image': 'none'});
                        }
                        if (periodo_nuevo !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo_nuevo", "", {'background-color': '#F32541', 'background-image': 'none'});
                        }
                    }
                    if (tipo === 'CXC' && cambio === 'P') {
                        if (fecha_nueva === '0099-01-01' && periodo !== '' && periodo_nuevo !== '' && grupo_transaccion !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo_nuevo", "", {'background-color': '#90CC00', 'background-image': 'none'});
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "grupo_transaccion", "", {'background-color': '#90CC00', 'background-image': 'none'});
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#90CC00', 'background-image': 'none'});
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        } else {
                            if (fecha_nueva !== '0099-01-01') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_nueva", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                            if (periodo === '') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                            if (grupo_transaccion === '') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "grupo_transaccion", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                            if (periodo_nuevo === '') {
                                grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo_nuevo", "", {'background-color': '#F32541', 'background-image': 'none'});
                            }
                        }
                    } else if (tipo === 'CXC' && cambio === 'F') {
                        if (fecha_nueva === '0099-01-01') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_nueva", "", {'background-color': '#F32541', 'background-image': 'none'});
                        } else {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "fecha_nueva", "", {'background-color': '#90CC00', 'background-image': 'none'});
                        }
                        if (periodo !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo", "", {'background-color': '#F32541', 'background-image': 'none'});
                        }
                        if (grupo_transaccion !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "grupo_transaccion", "", {'background-color': '#F32541', 'background-image': 'none'});
                        }
                        if (periodo_nuevo !== '') {
                            grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "periodo_nuevo", "", {'background-color': '#F32541', 'background-image': 'none'});
                        }
                    }
                }
            },
            gridComplete: function (index) {
                var ids = grid_tabla_.jqGrid('getDataIDs');
                var allRowsInGrid = grid_tabla_.jqGrid('getRowData');
                for (var i = 0; i < allRowsInGrid.length; i++) {
                    var cl = ids[i];
                    var tipo = grid_tabla_.getRowData(cl).tipo;
                    var documento = grid_tabla_.getRowData(cl).documento;
                    var tipo_documento = grid_tabla_.getRowData(cl).tipo_documento;
                    var nit = grid_tabla_.getRowData(cl).nit;
                    var banco = grid_tabla_.getRowData(cl).banco;
                    var sucursal = grid_tabla_.getRowData(cl).sucursal;
                    var cambio = grid_tabla_.getRowData(cl).cambio;
                    var fecha_nueva = grid_tabla_.getRowData(cl).fecha_nueva;
                    var periodo = grid_tabla_.getRowData(cl).periodo;
                    var periodo_nuevo = grid_tabla_.getRowData(cl).periodo_nuevo;
                    var grupo_transaccion = grid_tabla_.getRowData(cl).grupo_transaccion;

                    if ((tipo !== '') && (documento !== '') && (nit !== '') && (cambio !== '')) {
                        if (cambio === 'F') {
                            if (tipo === 'CXP') {
                                if (tipo_documento === '' || (fecha_nueva === '' || fecha_nueva === '0099-01-01') || periodo !== '' || periodo_nuevo !== '' || grupo_transaccion !== '' || (banco !== '' || sucursal !== '')) {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                                var valinfo = isNaN(nit);
                                if (valinfo === true) {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                            }
                            if (tipo === 'INGRESO') {
                                if ((fecha_nueva === '' || fecha_nueva === '0099-01-01') || periodo !== '' || periodo_nuevo !== '' || grupo_transaccion !== '' || tipo_documento === '') {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                                var valinfo = isNaN(nit);
                                if (valinfo === false) {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                            }
                            if (tipo === 'EGRESO') {
                                if ((banco === '' || sucursal === '') && (fecha_nueva === '' || fecha_nueva === '0099-01-01') || periodo !== '' || periodo_nuevo !== '' || grupo_transaccion !== '' || tipo_documento !== '') {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                                var valinfo = isNaN(nit);
                                if (valinfo === true) {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                            }
                            if (tipo === 'CXC') {
                                if (nit === '' || fecha_nueva === '' || fecha_nueva === '0099-01-01' || periodo !== '' || periodo_nuevo !== '' || grupo_transaccion !== '' || (banco !== '' || sucursal !== '') || tipo_documento === '') {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                                var valinfo = isNaN(nit);
                                if (valinfo === true) {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                            }
                        } else if (cambio === 'P') {
                            if (tipo === 'CXP') {
                                if (tipo_documento === '' || (fecha_nueva === '' || fecha_nueva !== '0099-01-01') || periodo === '' || periodo_nuevo === '' || grupo_transaccion === '') {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                                var valinfo = isNaN(nit);
                                if (valinfo === true) {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                            }
                            if (tipo === 'INGRESO') {
                                if ((fecha_nueva === '' || fecha_nueva !== '0099-01-01') || periodo === '' || periodo_nuevo === '' || grupo_transaccion === '' || tipo_documento === '') {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                                var valinfo = isNaN(nit);
                                if (valinfo === false) {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                            }
                            if (tipo === 'EGRESO') {
                                if ((banco === '' || sucursal === '') || (fecha_nueva === '' || fecha_nueva !== '0099-01-01') || periodo === '' || periodo_nuevo === '' || grupo_transaccion === '') {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                                var valinfo = isNaN(nit);
                                if (valinfo === true) {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                            }
                            if (tipo === 'CXC') {
                                if (nit === '' || (fecha_nueva === '' || fecha_nueva !== '0099-01-01') || tipo_documento === '') {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                                var valinfo = isNaN(nit);
                                if (valinfo === true) {
                                    filaInvalida = filaInvalida + 1;
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                    grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                                }
                            }
                        }
                    } else {
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo", "", {'background-color': '#FFB215', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "tipo_documento", "", {'background-color': '#FFB215', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "nit", "", {'background-color': '#FFB215', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "banco", "", {'background-color': '#FFB215', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "sucursal", "", {'background-color': '#FFB215', 'background-image': 'none'});
                        grid_tabla_.jqGrid('setCell', allRowsInGrid[i].id, "cambio", "", {'background-color': '#FFB215', 'background-image': 'none'});
                        mensajesDelSistema("Faltan Datos y/o hay datos no validos", '256', '150', false);
                    }

                }
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_documentos").navButtonAdd('#pager1', {
            caption: "Actualizar",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    if (filaInvalida > 0) {
                        mensajesDelSistema("No se puede procesar los datos se encuentran valores no validos, por favor limpie y vuelva a cargar el documento ", '389', '150', false);
                    } else {
                        actualizarDocumentos();
                    }
                } else {
                    mensajesDelSistema("No hay informacion para Actualizar ", '250', '150', false);
                }
            }
        });
        $("#tabla_documentos").navButtonAdd('#pager1', {
            caption: "Limpiar",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    filaInvalida = 0;
                    limpiar();
                } else {
                    mensajesDelSistema("No hay informacion para eliminar ", '250', '150', false);
                }
            }
        });
        $("#tabla_documentos").navButtonAdd('#pager1', {
            caption: "Prefijos",
            onClickButton: function () {
                ventanaPrefijos();
            }
        });
        $("#tabla_documentos").navButtonAdd('#pager1', {
            caption: "Documentos",
            onClickButton: function () {
                Documentos();
            }
        });
    }
}

function reloadGridMostrar(grid_mostrar_eds, opcion) {
    grid_mostrar_eds.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            dataType: "json",
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_mostrar_eds.trigger("reloadGrid");
}

function  actualizarDocumentos() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 5
        },
        success: function (data) {
            if (data.rows === 'SI') {
                mensajesDelSistema("Exito al actualizar  ", '230', '150', true);
                cargar(7);
            }
        }, error: function (result) {
            alert('ERROR  VERIFICAR DATOS');
        }
    });
}

function  limpiar() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 6
        },
        success: function (data) {
            if (data.rows === 'ELIMINADO') {
                jQuery("#tabla_documentos").jqGrid("clearGridData", true);
                mensajesDelSistema("Exito al eliminar datos  ", '230', '150', true);
            }

        }, error: function (result) {
            alert('ERROR  VERIFICAR DATOS');
        }
    });
}

function ventanaPrefijos() {
    cargarPrefijos();
    $("#dialogMsjPrefijos").dialog({
        width: '419',
        height: '297',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Tipo Documentos'
    });
}

function cargarPrefijos() {
    var grid_tabla_prefijos = jQuery("#tabla_prefijos");
    if ($("#gview_tabla_prefijos").length) {
        reloadGridMostrarPrefijos(grid_tabla_prefijos, 8);
    } else {
        grid_tabla_prefijos.jqGrid({
            // caption: "PREFIJOS",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '200',
            width: '400',
            colNames: ['Documento', 'Tipo Documento', 'Tipo'],
            colModel: [
                {name: 'descripcion', index: 'descripcion', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'codigo', index: 'codigo', width: 110, sortable: true, align: 'center', hidden: false, key: true},
                {name: 'tipo', index: 'tipo', width: 120, sortable: true, align: 'left', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 8
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, gridComplete: function (index) {
                var ids = grid_tabla_prefijos.jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var tipo = grid_tabla_prefijos.getRowData(cl).tipo;

                    if (tipo === 'CXC') {
                        $("#tabla_prefijos").jqGrid('setRowData', ids[i], false, {weightfont: 'bold', background: '#FFFF99'});
                    } else if (tipo === 'CXP') {
                        $("#tabla_prefijos").jqGrid('setRowData', ids[i], false, {weightfont: 'bold', background: '#CCFF99'});
                    } else if (tipo === 'INGRESO') {
                        $("#tabla_prefijos").jqGrid('setRowData', ids[i], false, {weightfont: 'bold', background: '#FF99CC'});
                    }
                }
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridMostrarPrefijos(grid_tabla_prefijos, opcion) {
    grid_tabla_prefijos.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla_prefijos.trigger("reloadGrid");
}

function Documentos() {

    $("#dialogMsjDocumentos").dialog({
        width: '448',
        height: '210',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Tipo Documentos',
        buttons: {//crear bot�n cerrar
            "Buscar": function () {
                verDocumentos();
                $(this).dialog("close");
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function verDocumentos() {
    cargarDocumentos();
    $("#dialogMsjVerDocumentos").dialog({
        width: '1021',
        height: '530',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Informacion Docuemnetos Operativos',
        buttons: {//crear bot�n cerrar
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}


function cargarDocumentos() {
    var grid_tabla_doc = jQuery("#tabla_ver_documentos");
    if ($("#gview_tabla_ver_documentos").length) {
        reloadGridMostrarDoc(grid_tabla_doc, 9);
    } else {
        grid_tabla_doc.jqGrid({
            // caption: "Informacion Docuemnetos Operativos",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '380',
            width: '995',
            colNames: ['Tipo', 'Documento', 'Tipo Documento', 'Nit', 'Banco', 'Sucursal', 'Fecha Cotabilizacion', 'Transaccion', 'Periodo'],
            colModel: [
                {name: 'tipo', index: 'tipo', width: 100, sortable: true, align: 'left',hidden: true},
                {name: 'documento', index: 'documento', width: 150, sortable: true, align: 'left', hidden: false, key: true},
                {name: 'tipo_documento', index: 'tipo_documento', width: 110, sortable: true, align: 'center', hidden: false},
                {name: 'nit', index: 'nit', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'banco', index: 'banco', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'sucursal', index: 'sucursal', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'fecha_contabilizacion', index: 'fecha_contabilizacion', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'transaccion', index: 'transaccion', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'periodo', index: 'periodo', width: 100, sortable: true, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager2',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 9,
                    fecha_inicio: $("#fechainicio").val(),
                    fecha_fin: $("#fechafin").val(),
                    user: $("#creacion_user").val(),
                    tipo_documento: $("#tipodocumento").val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager2", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_ver_documentos").navButtonAdd('#pager2', {
            caption: "Exportal excel",
            onClickButton: function () {
                var info = grid_tabla_doc.getGridParam('records');
                if (info > 0) {
                    exportarInformacionDO();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
    }
}

function reloadGridMostrarDoc(grid_tabla_doc, opcion) {
    grid_tabla_doc.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                fecha_inicio: $("#fechainicio").val(),
                fecha_fin: $("#fechafin").val(),
                user: $("#creacion_user").val(),
                tipo_documento: $("#tipodocumento").val()
            }
        }
    });
    grid_tabla_doc.trigger("reloadGrid");
}


function  exportarInformacionDO() {
    var fullData = jQuery("#tabla_ver_documentos").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            listado: myJsonString,
            opcion: 10
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}