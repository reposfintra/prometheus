// JavaScript Document
function validarActividad(){
   var d = document; 
   var campos = new Array("codactividad", "descorta",  "deslarga");
   var checs = new Array("fecini", "fecfin", "duracion", "documento", "tiempodemora", "causademora", "resdemora", "observacion", "feccierre", "cantrealizada","cantplaneada","referencia1", "referencia2", "refnumerica1", "refnumerica2");
   var sw = false;
   var i = 0;
   var campo = "";
   var chec = "";
   var c = "corta";
   var l = "larga";
   var m = "m_";

   for (i = 0; i < campos.length; i++){
      campo = campos[i];
      if (forma.elements[campo].value == ""){
          alert("EL CAMPO " + forma.elements[campo].name + " ESTA VACIO!");
          forma.elements[campo].focus();
          sw = true;
          break;
      }
   }
   if(forma.linkimg.checked){
	   	forma.linkimg.value = 'S';
   }
   else{
   		forma.linkimg.value = 'N';
   }
  for (i = 0; i < checs.length; i++){
      chec = checs[i];
      if (forma.elements[chec].checked){
          forma.elements[chec].value = "S";
          if(forma.elements[m+chec].checked){
			forma.elements[m+chec].value = "S"; 
          } 
          if (forma.elements[c+chec].value == ""){
          	  alert("EL CAMPO NOMBRE ESTA VACIO!");
              forma.elements[c+chec].focus();
              sw = true;
              break;
          }
          if (forma.elements[l+chec].value == ''){
          	  alert("EL CAMPO DESCRIPCIÓN ESTA VACIO!");
              forma.elements[l+chec].focus();
              sw = true;
              break;
          }
      }
      else if (chec == "feccierre"){
          forma.elements[chec].value = "S";
	      if (forma.elements[c+chec].value == ""){
          	  alert("EL CAMPO NOMBRE ESTA VACIO!");
              forma.elements[c+chec].focus();
              sw = true;
              break;
          }
          if (forma.elements[l+chec].value == ""){
          	  alert("EL CAMPO DESCRIPCIÓN ESTA VACIO!");
              forma.elements[l+chec].focus();
              sw = true;
              break;
          }	
      }
      else {
		forma.elements[chec].value = "N";        
      } 
   }
   
   if (!sw)
      forma.submit();  
}

function imgObligatorios(){
	imgFecinicio();
	imgFecfin();
	imgDur();
    imgDoc();
    imgTiempo();
    imgCausa();
    imgResp();
    imgReal();
    imgPlan();
    imgObs();
    imgRef1();
    imgRef2();
    imgRefnum1();
    imgRefnum2();
}
function imgFecinicio(){
	var img = document.getElementById('imgfecini');
	var img1 = document.getElementById('imgfecini1');
	if (forma.fecini.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}

function imgFecfin(){
	var img = document.getElementById('imgfecfin');
	var img1 = document.getElementById('imgfecfin1');
	if (forma.fecfin.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}

function imgDur(){
	var img = document.getElementById('imgdur');
	var img1 = document.getElementById('imgdur1');
	if (forma.duracion.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}

function imgDoc(){
	var img = document.getElementById('imgdoc');
	var img1 = document.getElementById('imgdoc1');
	if (forma.documento.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}

function imgTiempo(){
	var img = document.getElementById('imgtiempo');
	var img1 = document.getElementById('imgtiempo1');
	if (forma.tiempodemora.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}
function imgCausa(){
	var img = document.getElementById('imgcausa');
	var img1 = document.getElementById('imgcausa1');
	if (forma.causademora.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}
function imgResp(){
	var img = document.getElementById('imgres');
	var img1 = document.getElementById('imgres1');
	if (forma.resdemora.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}

function imgReal(){
	var img = document.getElementById('imgrea');
	var img1 = document.getElementById('imgrea1');
	if (forma.cantrealizada.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}
function imgPlan(){
	var img = document.getElementById('imgplan');
	var img1 = document.getElementById('imgplan1');
	if (forma.cantplaneada.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}

function imgObs(){
	var img = document.getElementById('imgobs');
	var img1 = document.getElementById('imgobs1');
	if (forma.observacion.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}

function imgRef1(){
	var img = document.getElementById('imgref1');
	var img1 = document.getElementById('imgref11');
	if (forma.referencia1.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}
function imgRef2(){
	var img = document.getElementById('imgref2');
	var img1 = document.getElementById('imgref21');
	if (forma.referencia2.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}
function imgRefnum1(){
	var img = document.getElementById('imgrefnum1');
	var img1 = document.getElementById('imgrefnum11');
	if (forma.refnumerica1.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}

function imgRefnum2(){
	var img = document.getElementById('imgrefnum2');
	var img1 = document.getElementById('imgrefnum21');
	if (forma.refnumerica2.checked ){
        img.style.visibility="visible";
		img1.style.visibility="visible";
	}
	else{
		img.style.visibility="hidden";
		img1.style.visibility="hidden";
	}
}