var id_bodega = '' //BODEGA DE PROYECTO SELECCIONADA
var id_solicitud = '' // PROYECTO DE LA BODEGA SELECCIONADA

$(document).ready(function() {
 $.fn.dataTable.ext.errMode = 'none';
    $('#table').on( 'error.dt', function ( e, settings, techNote, message ) {
    console.log( 'An error has been reported by DataTables: ', message );
    } ) ;    
    cargarSolicitudes();
    
    $('.ui.basic.modal').modal('show');
    $(".ui.dropdown").dropdown();
    
   
    $('#div_items tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
        //$($(this).find('td').eq(3)[0]).find("div").find("input").prop('disabled', false);
        
    } );

    
} );

  $( function() {
    $( "#div_items" ).draggable();
  } );


function cargarModalBodega(){
    cargarBodegasTraspaso("#select_bodega");
    $('.ui.mini.modal').modal('show');
};



function cargarSolicitudes() { 
  array_de_items=[];
  tabla = '#tabla_items2';
  columnas = [
      {
          className: 'details-control',
          orderable: false,
          data: null,
          defaultContent: '<i class="ellipsis horizontal icon"></i>'
      },
      {
        title: "Codigo de solicitud"
      }, {
        title: "Hecha por"
      }, {
        title: "Fecha esperada"        
      },{
        title: "Solicitada por"        
      },{
        title: "Acciones"        
      }];
  
    $.ajax({
      type: 'POST',
      url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
      dataType: 'json',
      async: false,
      data: {
        opcion: 43
      },
      success: function(json) {
          for (var datos in json) {
            array_de_items.push(Object.values(json[datos]));
          }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert("Error: " + xhr.status + "\n" +
          "Message: " + xhr.statusText + "\n" +
          "Response: " + xhr.responseText + "\n" + thrownError);
      }
    });
        
    tabla = $(tabla).DataTable({
    language: {
        url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
    },
    destroy: true,
    processing: false,
    pageLength: 25,
    responsive: true, 
    data: array_de_items,
    lengthChange: false,
    columns: columnas,
    columnDefs: [{
      targets: -1,
      data: null,
      render: function(data, type, full, meta){
          return "<i class='ban icon'  data-id-solicitud-ejecucion='"+data[5]+"' onclick='rechazarSolicitud(this)' title='Rechazar solicitud'></i> <i class='check icon'  data-id-solicitud-ejecucion='"+data[5]+"' title='Despachar solicitud' onclick='despacharSolicitud(this)'></i>";
      }       
    }]    
  });
  
   
    $('#tabla_items2 tbody').on('click', 'td.details-control', function () {        
        var tr = $(this).closest('tr');
        var row = tabla.row( tr );
        
        
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row) ).show();
            tr.addClass('shown');
        }
    } ); 
    
};

function format ( d ) {
    d = (JSON.parse(d.data()[6]));    
    // `d` es el data original de la fila (el detalle de la solicitud)
    a = ''
        for (i = 0; i < d.length; i++) {
        a +=         
        '<div class="item">'+
        '<i class="large cube middle aligned icon"></i>'+
        '<div class="content">'+
        '<a class="header">'+d[i].codigo_insumo+' - '+capitalizeFirstLetter(d[i].descripcion_insumo)+'</a>'+
        '<div class="description">'+d[i].cantidad_solicitada+' '+ d[i].nombre_unidad +'</div>'+
        '</div>'+
        '</div>'
    };
    text = '<div class="ui relaxed divided list">'+
            a+
        '</div>';

return text;
}

function capitalizeFirstLetter(string) {
    string = string.toLowerCase();
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function cargarItems(e) {
  $('.ui.modal').modal('hide')
  array_de_items.length = 0;
  var tipo_movimiento =  'ejecucion'
  var evento = e;
  
  id_bodega =  $("#select_bodega").val(); //BODEGA DE PROYECTO

  id_solicitud = '000000' // PROYECTO DE LA BODEGA
  
  PosicionarDivLitleLeft('div_items', e, 100);
  //en la posicion 0 de array_de_items se guarda el ID de la bodega a la que pertenecen los items. 
       $.ajax({
          type: 'POST',
          url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
          dataType: 'json',
          async: false,
          select: true,
          data: {
            opcion: 32,
            id_bodega: id_bodega,
            tipo_movimiento: tipo_movimiento,
            id_solicitud: id_solicitud
          },
          success: function(json) {
            if (json.error) {
              //  mensajesDelSistema(json.error, '250', '180');
              return;
            } 
              array_de_items[0] = id_bodega;
              for (var datos in json) {
                array_de_items.push(Object.values(json[datos]));
              }
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
              "Message: " + xhr.statusText + "\n" +
              "Response: " + xhr.responseText + "\n" + thrownError);
          }
        });

    
    if ( $.fn.DataTable.isDataTable('#show_items') ) {
      $('#show_items').DataTable().destroy();
    }

    $('#show_items tbody').empty();     
    tabla = $('#show_items').DataTable({
    destroy: true,
    processing: false,
    pageLength: 25,
    dom: '<"toolbar">frtip',
    data: array_de_items.slice(1),
    columns: [
        {title: "COD MATERIAL"},
        {title: "DESCRIPCION"},
        {title: "UNIDADES"},
        {title: "SOLICITAR"}
    ],
    columnDefs: [
        {
          targets: -1,
          data: null,
          render: function (data, type, full, meta) {
                    return "<div class='ui input'>"+
                            "<input type='text' value="+data[3]+"> "+
                            "</div>";
                }
                  
        },{
          targets: 1,
          class:'col-descripcion'          
        }
    ]
  });
  tabla_de_items = tabla;    
  $('#div_items').fadeIn('slow');
};

function PosicionarDivLitleLeft(id_objeto, e, resta) {

  obj = document.getElementById(id_objeto);
  var posx = 0;
  var posy = 0;

  if (!e)
    var e = window.event;

  if (e.pageX || e.pageY) {
    posx = e.pageX;
    posy = e.pageY;
  } else if (e.clientX || e.clientY) {
    posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
  } else
    alert('ninguna de las anteriores');

  obj.style.left = 600 - resta;
  obj.style.top = 100;
}


function cargarBodegasTraspaso(cbx_lista_bodega) {
    
    $(cbx_lista_bodega).prop("disabled", false);
    $(cbx_lista_bodega).html('');
    $(cbx_lista_bodega).append('<option value="0">Selecciona bodega...</option>');

    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Orden&accion=Compra',
        dataType: 'json',
        async: false,
        data: {
            //id_solicitud: "identificador_solicitud",
            opcion: 6
        },
        success: function (json) {                
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                for (var i in json) {
                    $(cbx_lista_bodega).append('<option value="'+json[i].id+'">' + json[i].direccion + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    $('select[id=casa] option:eq(3)').attr('selected', 'selected');    
};

function despacharSolicitud(e){
    var id_solicitud_ejecuciion = $(e).data('id-solicitud-ejecucion');     
    $.ajax({
      type: 'POST',
      url: "./controlleropav?estado=Compras&accion=Proceso",
      dataType: 'json',
      async: false,
      data: {
        opcion: 44,
        solicitud_ejecucion: id_solicitud_ejecuciion
      },
      success: function(json) {
        void(json.error && toastr.error(json.error));
        if(json.status == '200'){         
            alert("Creado con exito");       
            location.reload();          
        }            
      }
      ,
      error: function(xhr, ajaxOptions, thrownError) {
        alert("Error: " + xhr.status + "\n" +
          "Message: " + xhr.statusText + "\n" +
          "Response: " + xhr.responseText + "\n" + thrownError);
      }    
  });
};

function rechazarSolicitud(e){
    var id_solicitud_ejecuciion = $(e).data('id-solicitud-ejecucion');     
    $.ajax({
      type: 'POST',
      url: "./controlleropav?estado=Compras&accion=Proceso",
      dataType: 'json',
      async: false,
      data: {
        opcion: 45,
        solicitud_ejecucion: id_solicitud_ejecuciion
      },
      success: function(json) {
        void(json.error && toastr.error(json.error));
        if(json.status == '200'){         
            alert("Solicitud rechazada!");       
            location.reload();          
        }            
      }
      ,
      error: function(xhr, ajaxOptions, thrownError) {
        alert("Error: " + xhr.status + "\n" +
          "Message: " + xhr.statusText + "\n" +
          "Response: " + xhr.responseText + "\n" + thrownError);
      }    
  });
};