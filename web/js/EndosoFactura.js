/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    cargarEndosoFacturas();
});

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function cargarEndosoFacturas() {
    var operacion;
    var grid_tabla_endoso = jQuery("#tabla_endosoFactura");
    if ($("#gview_tabla_endosoFactura").length) {
        reloadGridEndosoFactura(grid_tabla_endoso, 48);
    } else {
        grid_tabla_endoso.jqGrid({
            caption: "Proceso Configuracion Endoso",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '250',
            width: '850',
            colNames: ['id', 'Estado', 'Nombre proceso', 'Custodiada por', 'Custodiador', 'Cuenta cabecera', 'CMC'],
            colModel: [
                {name: 'id', index: 'id', width: 70, sortable: true, align: 'center', hidden: true, key: true},
                {name: 'reg_status', index: 'reg_status', width: 70, sortable: true, align: 'left', hidden: true},
                {name: 'nombre_proceso', index: 'nombre_proceso', width: 190, sortable: true, align: 'left', hidden: false},
                {name: 'custodiada_por', index: 'custodiada_por', width: 150, sortable: true, align: 'center', hidden: false},
                {name: 'nombre_custodiador', index: 'nombre_custodiador', width: 230, sortable: true, align: 'left', hidden: false},
                {name: 'cuenta_cabecera_cdiar', index: 'cuenta_cabecera_cdiar', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'cmc_to_facturas', index: 'cmc_to_facturas', width: 120, sortable: true, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 48
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_endosoFactura").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 0;
                ventanaGuardarEditarFacturasEndoso(operacion);
            }
        });
        $("#tabla_endosoFactura").navButtonAdd('#pager', {
            caption: "Editar",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_endosoFactura"), i, selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow
                operacion = 1;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var estado = filas.reg_status;
                var nombre_proceso = filas.nombre_proceso;
                var custodiada_por = filas.custodiada_por;
                var nombre_custodiador = filas.nombre_custodiador;
                var cuenta_cabecera_cdiar = filas.cuenta_cabecera_cdiar;
                var cmc_to_facturas = filas.cmc_to_facturas;

                if (filas === false) {
                    mensajesDelSistema("SELECIONE UNA FILA", '200', '150', false);
                    return;
                } else {
                    if (estado === "") {
                        ventanaGuardarEditarFacturasEndoso(operacion, id, nombre_proceso, custodiada_por, nombre_custodiador, cuenta_cabecera_cdiar, cmc_to_facturas);
                    } else {
                        alert(estado);
                        mensajesDelSistema("PARA EDITAR LA EDS, EL ESTADO DEBE SER ACTIVO", '300', '150', false);
                    }
                }
            }
        });
    }
}

function reloadGridEndosoFactura(grid_tabla_endoso, opcion) {
    grid_tabla_endoso.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla_endoso.trigger("reloadGrid");
}

function ventanaGuardarEditarFacturasEndoso(operacion, id, nombre_proceso, custodiada_por, nombre_custodiador, cuenta_cabecera_cdiar, cmc_to_facturas) {
    if (operacion === 0) {
        cargarHC();
        CustodiadaPor();
        Custodiador();
        $("#dialogMsjFacuraendoso").dialog({
            width: '382',
            height: '300',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            //title: '',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    guardarFacturaEndoso();
                },
                "Salir": function () {
                    $(this).dialog("close");

                }
            }
        });
    } else if (operacion === 1) {

        $('#id').val(id);
        $('#nomproces').val(nombre_proceso);
        CustodiadaPor();
        $('#custodiadap').val(custodiada_por);
        Custodiador();
        $('#custodiador').val(nombre_custodiador);
        $('#cuenta').val(cuenta_cabecera_cdiar);
        cargarHC();
        $('#cmc').val(cmc_to_facturas);
        $("#dialogMsjFacuraendoso").dialog({
            width: '382',
            height: '300',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            //title: '',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarFacturaEndoso();
                    
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $('#id').val('');
                    $('#nomproces').val('');
                    $('#custodiadap').val('');
                    $('#custodiador').val('');
                    $('#cuenta').val('');
                    $('#cmc').val('');
                }
            }
        });
    }

}

function cargarHC() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Eds",
        dataType: 'json',
        async: false,
        data: {
            opcion: 33
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#cmc').html('');
                for (var datos in json) {
                    $('#cmc').append('<option value="' + datos + '">' + datos + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function CustodiadaPor() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 49
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#custodiadap').html('');
                for (var datos in json) {
                    $('#custodiadap').append('<option value="' + datos + '">' + datos + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function Custodiador() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 50,
            nit: $('#custodiadap').val()
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#custodiador').html('');
                for (var datos in json) {
                    $('#custodiador').append('<option value="' + datos + '">' + datos + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function guardarFacturaEndoso() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 51,
            nomproces: $('#nomproces').val(),
            nit: $('#custodiadap').val(),
            custodiador: $('#custodiador').val(),
            cuenta: $('#cuenta').val(),
            cmc: $('#cmc').val()
        },
        success: function (json) {
            mensajesDelSistema("Exito al guardar", '230', '150', true);
        }
    });
}

function ActualizarFacturaEndoso() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 52,
            nomproces: $('#nomproces').val(),
            nit: $('#custodiadap').val(),
            custodiador: $('#custodiador').val(),
            cuenta: $('#cuenta').val(),
            cmc: $('#cmc').val(),
            id: $('#id').val()
        },
        success: function (json) {
            cargarEndosoFacturas();
            // mensajesDelSistema("Exito al guardar", '230', '150', true);
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}