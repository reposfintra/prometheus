/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var $j = jQuery.noConflict();

var ns4 = (document.layers);
var ie4 = (document.all && !document.getElementById);
var ie5 = (document.all && document.getElementById);
var ns6 = (!document.all && document.getElementById);
var msg = new Array();
var id_bodega;
var ElementoMortal;


/**
 ---ELEMENTOS ..::ON::..
 */

$(document).on(
  "blur", ".input_80px",
  function() {
    var data = $('#tbl_solicitudes').DataTable().row($(this).parents('tr')).data()
    var valor_tabla = parseInt(this.value);
    var valor_input = parseInt(data[8]);
  }
);

$(document).on(
  "dblclick", ".input_80px",
  function(e) {
    var data = $('#tbl_solicitudes').DataTable().row($(this).parents('tr')).data()
    var coordenada_x = 1225;
    var accion = 'NUEVO';
    PosicionarDivLitleLeft('div_InsumosApu', e, coordenada_x);
    cargando_toggle();
    ElementoMortal = this;
    $.ajax({
      type: "POST",
      url: "/fintra/jsp/solicitud_insumos/nueva_solicitud_detalle.jsp",
      async: true,
      dataType: "html",
      data: {
        id_sol: $('#id_solicitudu').val(),
        cod_insumo: data[2],
        ACCIONE: accion,
        ITEM: 1
      },
      success: function(data) {
        if (data != "") {
          $("#div_InsumosApu").html(data);
          $('#div_InsumosApu').fadeIn('slow');
          cargando_toggle();
        }
      }
    });
  }
);

$(document).on(
  "blur", ".input_80InsumoAPUpx",
  function() {
    var sumValor = 0;
    $('#tbl_insumos_apus tbody tr').each(function() {
      var toton = $(this).find("td")[6].children[0].value;
      sumValor = sumValor + parseFloat(toton);
    });
    $(ElementoMortal).val(sumValor);
  }
);

$(document).on(
  "click", ".inputCalendar",
  function() {
    var Calendars = $(this).parents().get(0);
    var InputCalendar = Calendars.children[0].id;
    var TriggerCalendar = Calendars.children[0].id;
    Calendar.setup({
      inputField: InputCalendar,
      trigger: TriggerCalendar,
      align: "top",
      onSelect: function() {
        this.hide();
      }
    });
  }
);

$(document).on(
  "change", ".input_80pxEspecial",
  function() {
    var totales = 0;
    var data = $('#tbl_insumos_ocs').DataTable().row($(this).parents('tr')).data();
    var valor_tabla = parseInt(this.value);
    var valor_input = parseInt(data[9]);
    if (valor_tabla > valor_input) {
      smoke.signal("<b>SUPERAS EL VALOR PRESUPUESTADO, FAVOR VALIDAR!</b> <br> ", 700);
      this.value = '0.0000';
    }
    $('#tbl_insumos_ocs tbody tr').each(
      function() {

        var dataHolder = $(".data-holder", $(this));
        var UserResponsable = dataHolder.attr('UserResponsable');
        var CodSolicitud = dataHolder.attr('CodSolicitud');
        var CodigoMaterial = dataHolder.attr('CodigoMaterial');
        var CostoCompra = dataHolder.attr('CostoCompra');
        var CantSolicitar = dataHolder.attr('CantSolicitar');
        var TotalRow = dataHolder.attr('TotalRow');

        $(this).find("td")[12].children[0].value = $(this).find("td")[11].children[0].value * $(this).find("td")[10].children[0].value;
        totales = totales + ($(this).find("td")[11].children[0].value * $(this).find("td")[10].children[0].value);
      }
    );
    document.getElementById("subtotal_orden_ocs").value = totales;
    document.getElementById("total_orden_ocs").value = totales;
  }
);

$(document).on(
  "change", ".input_80pxValor",
  function() {

    var totales = 0;
    var data = $('#tbl_insumos_ocs').DataTable().row($(this).parents('tr')).data();
    var valor_tabla = parseInt(this.value);
    var valor_input = parseInt(data[4]);

    if (valor_tabla > valor_input) {


    }

    $('#tbl_insumos_ocs tbody tr').each(
      function() {

        //SE PUEDE CAMBIAR EL COLOR EN EL RECORRIDO

        var dataHolder = $(".data-holder", $(this));
        var UserResponsable = dataHolder.attr('UserResponsable');
        var CodSolicitud = dataHolder.attr('CodSolicitud');
        var CodigoMaterial = dataHolder.attr('CodigoMaterial');
        var CostoCompra = dataHolder.attr('CostoCompra');
        var CantSolicitar = dataHolder.attr('CantSolicitar');
        var TotalRow = dataHolder.attr('TotalRow');

        $(this).find("td")[12].children[0].value = $(this).find("td")[11].children[0].value * $(this).find("td")[10].children[0].value;
        totales = totales + ($(this).find("td")[11].children[0].value * $(this).find("td")[10].children[0].value);
      }
    );
    document.getElementById("subtotal_orden_ocs").value = totales;
    document.getElementById("total_orden_ocs").value = totales;
  }
);

$(document).on(
  "blur", ".input_80pxEspecialxxx",
  function() {

    var data = $('#tbl_insumos_ocs').DataTable().row($(this).parents('tr')).data();

    if (this.value >= data[8]) {

      alert("Superas el valor disponible, favor validar");
      this.value = '0.0000';
    }

    var dataHolder = $(".input_80pxWithOutAction", $(this));
    var data2 = $('#tbl_insumos_ocs').DataTable().row($(dataHolder)).data();
  }
);

$(document).on(
  "click", ".RetornoInsumo",
  function(e) {

    var hola = $(this).parent().parent();
    var celdas = hola.children();

    $.ajax({
      type: "POST",
      url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
      async: true,
      dataType: "json",
      data: {
        opcion: 17,
        usuario: $('#NmUser').val(),
        id_solicitud: $('#id_solicitud').val(),
        TipoInsumo: $(celdas[0]).text(),
        CodigoInsumo: $(celdas[1]).text(),
        DescripcionInsumo: $(celdas[2]).text(),
        NombreUnidadInsumo: $(celdas[3]).text(),
        Cantidad: celdas[4].children[0].value,
        CdSol: $('#CdSol').val()
      },
      success: function(json) {

        if (json.error) {
          mensajesDelSistema(json.error, '250', '180');
          return;
        }
        try {
          if (json[0].resultado_accion == 'POSITIVO') {
            smoke.signal("<b>PROCESO EXITOSO!</b> <br> ", 500);
          } else {
            smoke.signal("<b>NO FUE POSIBLE AGREGAR ESTE NUEVO ELEMENTO!</b> <br> ", 500);
          }

        } catch (exception) {

        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert("Error: " + xhr.status + "\n" +
          "Message: " + xhr.statusText + "\n" +
          "Response: " + xhr.responseText + "\n" + thrownError);
      }
    });
  }
);

$(function () {
    $('#id_solicitud__').on('keypress', function (e) {
        if (e.which === 13) {
            cargando_toggle();
            $.ajax({
                url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
                datatype: 'json',
                type: 'GET',
                data: {opcion: 47, id_solicitud: $("#id_solicitud__").val()},
                async: true,
                success: function (json) {
                    try {
                        cargando_toggle();
                        if (json.error) {
                            alert(json.error + " Error.");
                            
//                            mensajesDelSistema(json.error, '300', 'auto', false);
                        } else {
                            var res = json.respuesta;
                            smoke.signal("<b>Cargado!</b> <br> ", 500);
                            $("#id_solicitud__").val('');
                        }
                    } finally {
                    }
                }
            });
            

        }
    });
});



$('.input_80pxDespacho').on('change', function() {
  alert('C!!!');
});

/**
 ---POSICIONES
 */
function Posicionar_div(id_objeto, e) {
  obj = document.getElementById(id_objeto);
  var posx = 0;
  var posy = 0;
  if (!e)
    var e = window.event;
  if (e.pageX || e.pageY) {
    posx = e.pageX;
    posy = e.pageY;
  } else if (e.clientX || e.clientY) {
    posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
  } else
    alert('ninguna de las anteriores');
  obj.style.left = posx;
  obj.style.top = posy;
}

function PosicionarDivLitleLeft(id_objeto, e, resta) {

  obj = document.getElementById(id_objeto);
  var posx = 0;
  var posy = 0;

  if (!e)
    var e = window.event;

  if (e.pageX || e.pageY) {
    posx = e.pageX;
    posy = e.pageY;
  } else if (e.clientX || e.clientY) {
    posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
  } else
    alert('ninguna de las anteriores');

  obj.style.left = posx - resta;
  obj.style.top = posy + 100;
}

function PosicionarDivLitleRight(id_objeto, e, suma) {

  obj = document.getElementById(id_objeto);
  var posx = 0;
  var posy = 0;

  if (!e)
    var e = window.event;

  if (e.pageX || e.pageY) {
    posx = e.pageX;
    posy = e.pageY;
  } else if (e.clientX || e.clientY) {
    posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
  } else
    alert('ninguna de las anteriores');

  obj.style.left = posx + suma;
  obj.style.top = posy;
}

function cargando_toggle() {
  $('#loader-wrapper').toggle();
}

 function format(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};
/**
 ---FUNCIONES OPERATIVAS
 */

function LoadOrdenesOCOS(EstadoSolicitud) {

  var primero = true;
  var arr = [];
  cargando_toggle(); 

  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    async: false,
    data: {
      opcion: 0,
      usuario: $('#NmUser').val(),
      id_solicitud: $('#id_solicitudu').val(),
      ano: $('#ano').val(),
      mes: $('#mes').val(),
      tiposolicitud: $('#tiposolicitudu').val(),
      EstadoSolicitud: EstadoSolicitud
    },
    success: function(json) {
      if (json.error) {
          mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        for (var datos in json) {
          arr.push(Object.values(json[datos]));
        }
        var table = $('#example').DataTable({
          destroy: true,
          processing: false,
          pageLength: 25,
          dom: '<"top">rt<"bottom" ip><"clear">',
          data: arr,
          columns: [{
            title: "IdEstado"
          }, {
            title: "Estado"
          }, {
            title: "Cod Solicitud"
          }, {
            title: "Fecha Creacion"
          }, {
            title: "Responsable"
          }, {
            title: "No. Solicitud"
          }, {
            title: "Tipo Solicitud"
          }, {
            title: "Bodega Entrega"
          }, {
            title: "Fecha Entrega"
          }, {
            title: "Descripcion"
          }, {
            title: "Total Insumos"
          }, {
            title: "accion"
          }],
          columnDefs: [{
              targets: -1,
              data: null,
              defaultContent: "<IMG SRC='./images/sl_visualizar.png' WIDTH=40 HEIGHT=40 BORDER=2 ALT='Visualizar' id='visualizar' title='Visualizar' > \n\
                                             <IMG SRC='./images/sl_edit.png' WIDTH=40 HEIGHT=40 BORDER=2 ALT='Editar' id='editar' title='Editar'> \n\
                                             <IMG SRC='./images/sl_mandar.png' WIDTH=40 HEIGHT=40 BORDER=2 ALT='Mandar a Compras' id='mandar' title='Mandar a Compras'> \n\ \n\
                                             <IMG SRC='./images/EliminarSol.png' WIDTH=30 HEIGHT=30 BORDER=2 ALT='ELIMINAR' id='eliminar' title='Eliminar'> \n\ "
            },
            {
              targets: [0],
              visible: false,
              searchable: false
            }
          ]
        });

        $('#example tbody').on('click', 'img', function(e) {

          var data = table.row($(this).parents('tr')).data();

          if (this.id == 'visualizar') {
            var data = table.row($(this).parents('tr')).data();
            VisualizarSolicitud(data[2], e.originalEvent);
          }

          if (this.id == 'editar') {
            if (data[0] == 0 || data[0] == 2) {
              var data = table.row($(this).parents('tr')).data();
              CrearEditarSolicitud('EDIT', data[2], e.originalEvent);
            } else {
              smoke.signal("<b>NO SE PUEDE EDITAR!</b> <br> ", 500);
            }
          }

          if (this.id == 'mandar') {
            if (data[0] == 0 || data[0] == 2) {
              EnviarSolicitudCompras(data[2]);
            } else {
              smoke.signal("<b>YA SE ENCUENTRA EN COMPRAS!</b> <br> ", 700);
            }

          }

          if (this.id == 'eliminar') {
            if (data[0] == 0 || data[0] == 2) {
              EliminarSolicitud(data[2]);
            } else {
              smoke.signal("<b>NO PUEDES ELIMIAR UNA SOLICITUD EN COMPRAS!</b> <br> ", 500);
            }
          }
        });
        cargando_toggle();
      } catch (exception) {
        //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function CrearEditarSolicitud(accion, item, e) {

  var coordenada_x;

  if (accion == 'VISUALIZAR') {
    coordenada_x = 1225;
  } else if (accion == 'EDIT') {
    coordenada_x = 1600;
  } else if (accion == 'NUEVO') {
    coordenada_x = 1600;
  } else {
    coordenada_x = 1450;
  }

  PosicionarDivLitleLeft('div_nueva_solicitud', e, coordenada_x);
  cargando_toggle();

  $.ajax({
    type: "POST",
    url: "/fintra/jsp/solicitud_insumos/nueva_solicitud.jsp",
    async: true,
    dataType: "html",
    data: {
      id_sol: $('#id_solicitudu').val(),
      ACCIONE: accion,
      ITEM: item
    },
    success: function(data) {
      if (data != "") {        
        $("#div_nueva_solicitud").html(data);
        $('#div_nueva_solicitud').fadeIn('slow');
        $('#div_espera').fadeOut('slow');

        if (accion == 'VISUALIZAR') {
          $("#HeadTable").css("display", "none");
        }
        cargando_toggle();
      }
    }
  });

  $('#div_espera').fadeOut('slow');
}

function CrearOCS(accion, item, e, id_solicitud, codigo_solicitud) {

  var coordenada_x;
  coordenada_x = 1450;
  PosicionarDivLitleLeft('div_nueva_ocs', e, coordenada_x);

  if (document.getElementById('carri-ojo').disabled == false) {
    cargando_toggle();
    $.ajax({
      type: "POST",
      url: "/fintra/jsp/solicitud_insumos/nueva_ocs.jsp",
      async: true,
      dataType: "html",
      data: {
        usuario: $('#NmUser').val(),
        ACCIONE: accion,
        ITEM: item,
        id_sol: id_solicitud,
        codigo_solicitud: codigo_solicitud
      },
      success: function(data) {
        if (data != "") {
          $("#div_nueva_ocs").html(data);
          $('#div_nueva_ocs').fadeIn('slow');
          $('#div_espera').fadeOut('slow');
          SolsParaOCS($('#NmUser').val());
          cargando_toggle();
        } else {
          cargando_toggle();
          smoke.signal("<b>NO SE ENCONTRARON SOLICITUDES PENDIENTES</b> <br> ", 800);
        }
      }
    });
    $('#div_espera').fadeOut('slow');
  }
}

function ClearTable(NameTable, estado) {

  var Tabla = $('#' + NameTable).DataTable();
  Tabla.clear();
  Tabla.destroy();
  $('#misreq').empty();
  document.getElementById('misreq').innerHTML = "<table id='example' class='table table-striped table-bordered'></table>";
  LoadOrdenesOCOS(estado);
}


function ClearTableTodasSolicitudes(NameTable) {

  var Tabla = $('#' + NameTable).DataTable();
  Tabla.clear();
  Tabla.destroy();
  $('#misreq').empty();
  document.getElementById('misreq').innerHTML = "<table id='sol_detodos' class='table table-striped table-bordered'></table>";
  SolicituDeTodos();

}


function ClearTableInventario(NameTable) {

  var Tabla = $('#' + NameTable).DataTable();
  Tabla.clear();
  Tabla.destroy();
  $('#misreq').empty();
  document.getElementById('misreq').innerHTML = "<table id='sol_detodos' class='table table-striped table-bordered'></table>";
  inventario_todos();
}


function SolsParaOCS(NameTable) {

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 15,
      usuario: $('#NmUser').val()

    },
    success: function(json) {

      if (json.error) {
        alert("ERROR");
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        if (json[0].respta == 'NEGATIVO') {
          $("#carri-ojo").attr("src", "./images/review_item-grey.png");
          $('#carri-ojo').attr('disabled', true);
        } else if (json[0].respta == 'POSITIVO') {
          $("#carri-ojo").attr("src", "./images/review_item.png");
          $('#carri-ojo').attr('disabled', false);
        }

        $('#misreq').empty();
        document.getElementById('misreq').innerHTML = "<table id='sol_paraOrdenCS' class='table table-striped table-bordered'></table>";

        if ($('#modo_compra').val() == 1) {
          SolicitudesParaOCS('SQL_SOLICITUDtoORDEN');
        } else {
          InsumosParaOCS('SQL_INSUMOtoORDEN');
        }

      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }

  });

}


function OrdenesCS(recepcion) {

    if (recepcion=='true'){
       OrdenesCS_with_foms()
    }else{
      $('#misreq').empty();
      document.getElementById('misreq').innerHTML = "<table id='OrdenesCS' class='table table-striped table-bordered'></table>";
      LoadOrdenesCS('SQL_LISTADO_OCS');    
    }
}

function OrdenesCS_with_foms() {

  $('#misreq').empty();
  document.getElementById('misreq').innerHTML = "<table id='OrdenesCS' class='table table-striped table-bordered'></table>";
  LoadOrdenesCS('SQL_LISTADO_OCS_WITH_FOMS');

}

function cargar_insumos_proyecto() {

  $('#div_espera').fadeIn('slow');
  $.ajax({
    type: "POST",
    url: "/fintra/jsp/solicitud_insumos/listado_insumos.jsp",
    async: true,
    dataType: "html",
    data: {
      estado: $('#estado').val(),
      mes: $('#mes').val(),
      ano: $('#ano').val(),
      proceso: $('#proceso_filtro').val(),
      asignadas: "",
      selec_prioridades: $('#selec_prioridades').val(),
      tipo_tarearq: $('#tipo_tarearq').val()

    },
    success: function(data) {
      if (data != "") {
        $("#rsult_listado_insumo").html(data);
        $('#div_espera').fadeOut('slow');
      }
    }
  });
}

function EstadoSolicitudes() {

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 3,
      usuario: $('#NmUser').val(),
      id_solicitud: $('#id_solicitudu').val()
    },
    success: function(json) {

      if (json.error) {
        alert("ERROR");
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        $('#project_name').val(json[0].nombre_proyecto);
        if (json[0].estado_solicitud == 'NO-NUEVO') {
          $('#NewSolicitude').attr("disabled", true);
        } else if (json[0].estado_solicitud == 'NUEVO') {
          $('#NewSolicitude').attr("disabled", false);
        }
        LoadOrdenesOCOS(0);
      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });

}

function WhatIdo(Usar) {

  var CantidadElementos = 0;
  var SinPercanse = 0;

  if ($('#tiposolicitud').val() == '') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#cbx_lista_bodegas').val() == null) {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#descripcion').val() == '') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#fecha_actual').val() == '') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#fecha_entrega').val() == '') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#txt_direccion').val() == '' || $('#txt_direccion').val() == 'Selecciona bodega...') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else {

    $('#tbl_solicitudes tbody tr').each(function() {

      CantidadElementos = CantidadElementos + parseInt($(this).find("td")[8].children[0].value);
      var val_input = parseInt($(this).find("td")[8].children[0].value);
      $(this).children('td').each(function(indiceColumna) {

        if (indiceColumna == 7 && val_input != '0') { 
          var val_tabla = parseInt($(this).text());
        }
      });
    });

    if (SinPercanse == 0) {
      if (CantidadElementos > 0) {
          id_bodega = $('#cbx_lista_bodegas').val();
        if ($('#cbx_lista_bodegas').val() == 1) {
          tipo_bodega = 1;
        } else {
          tipo_bodega = 2;
          //BODEGA TIPO 1: BODEGA PRINCIPAL
          //BODEGA TIPO 2: BODEGA DEL PROYECTO
        }
        
        $.ajax({
          type: "POST",
          url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
          async: true,
          dataType: "json",
          data: {
            opcion: 2,
            usuario: Usar,
            id_solicitud: $('#id_solicitud').val(),
            tiposolicitud: $('#tiposolicitud').val(),
            tipo_bodega: tipo_bodega,
            id_bodega : id_bodega,
            descripcion: $('#descripcion').val(),
            fecha_actual: $('#fecha_actual').val(),
            fecha_entrega: $('#fecha_entrega').val(),
            direccion_entrega: $('#txt_direccion').val()
          },
          success: function(json) {
            if (json.error) {
              mensajesDelSistema(json.error, '250', '180');
              return;
            }
            try {
              if (json[0].resultado_operacion == 'EXISTE') {
                //BLOQUEA LOS CAMPOS DE LA PRESOLICITUD
              } else {
                $('#item').val(json[0].resultado_operacion);
              }
              GuardarElemento(Usar);
            } catch (exception) {
              //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
              "Message: " + xhr.statusText + "\n" +
              "Response: " + xhr.responseText + "\n" + thrownError);
          }
        });
      } else {
        smoke.signal("<b>NO HAY CANTIDADES SELECCIONADAS!</b> <br> ", 1000);
      }
    } else {
      smoke.signal("<b>ERROR DE CANTIDADES!</b> <br> ", 1000);
    }
  }
}

function GuardarElemento(Usar) {

  var TablaInsumos = new CapturarInputsDataTable(); //funcionFomurarios();
  var JsonFormulario = TablaInsumos.informacion('CapsuleTable');

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 1,
      usuario: Usar,
      id_solicitud: $('#id_solicitud').val(),
      informacion: JSON.stringify({
        json: JsonFormulario
      })
    },
    success: function(json) {

      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        smoke.signal("<b>ELEMENTOS INSERTADOS!</b> <br> ", 700);

      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function GuardarOCS(Usar) {

  if ($('#tiposolicitudu').val() == '') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#proveedor').val() == '0') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#descripcion').val() == '') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#fecha_entrega').val() == '') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#txt_direccion').val() == '' || $('#txt_direccion').val() == 'Selecciona bodega...') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#f_pago').val() == '') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#cbx_lista_bodegas').val() == null) {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);    
  } else {


    if ($('#total_orden_ocs').val() != '0') {

      $.ajax({
        type: "POST",
        url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
        async: true,
        dataType: "json",
        data: {
          opcion: 10,
          usuario: Usar,
          tipo_solicitud: $('#tiposolicitudu').val(),
          proveedor: $('#proveedor').val(),
          descripcion: $('#descripcion').val(),
          fecha_entrega: $('#fecha_entrega').val(),
          direccion_entrega: $('#txt_direccion').val(),
          f_pago: $('#f_pago').val(),
          id_bodega: $('#cbx_lista_bodegas').val()
        },
        success: function(json) {

          if (json.error) {
            mensajesDelSistema(json.error, '250', '180');
            return;
          }
          try {
            if (json[0].resultado_accion == 'NEGATIVO') {
              smoke.signal("<b>NO SE PUDO GUERDAR LA ORDEN!</b> <br> ", 800);
            } else {

              $('#orden_ocs').val(json[0].resultado_accion);
              GuardarDetalleOCS(Usar, json[0].resultado_accion)
            }

          } catch (exception) {
            //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
          }

        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert("Error: " + xhr.status + "\n" +
            "Message: " + xhr.statusText + "\n" +
            "Response: " + xhr.responseText + "\n" + thrownError);
        }
      });
    } else {
      smoke.signal("<b>LA ORDEN ESTA EN CERO(0)!</b> <br> ", 1000);
    }
  }
}

function GuardarDetalleOCS(UserMe, oSC) {

  var TablaInsumos = new CapturarInputsOrdenCS(); //funcionFomurarios();
  var JsonFormulario = TablaInsumos.informacion('CapsuleTable');

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 11,
      usuario: UserMe,
      oSC: oSC,
      informacion: JSON.stringify({
        json: JsonFormulario
      })
    },
    success: function(json) {

      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        $("#tipo_solicitud").prop("disabled", true);
        $("#proveedor").prop("disabled", true);
        $("#descripcion").prop("disabled", true);
        $("#orden_ocs").prop("disabled", true);
        $("#fecha_entrega").prop("disabled", true);
        $("#direccion_entrega").prop("disabled", true);
        $("#f_pago").prop("disabled", true);
        $("#dias_pago_credito").prop("disabled", true);
        $("#tbl_insumos_ocs").find("input,button,textarea,select").attr("disabled", "disabled");
        $('#img_guardar_ocs').hide();
        $('#img_eliminar_preocs').hide();
        $('#cerrar_orden').show();
        ActualizarSolicitudes(UserMe, oSC);

      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}


function EliminarSolicitud(codSolicitud) {

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 5,
      usuario: $('#NmUser').val(),
      id_solicitud: $('#id_solicitudu').val(),
      CodSol: codSolicitud
    },
    success: function(json) {

      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        if (json[0].respta == 'POSITIVO') {
          $('#NewSolicitude').attr("disabled", false);
          LoadOrdenesOCOS(0);
        } else {
          alert("El elemento no pudo ser borrado!");
        }
      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function EliminarPreOCS(Usar) {

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 19,
      usuario: Usar
    },
    success: function(json) {

      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        if (json[0].respta == 'POSITIVO') {
          SolsParaOCS(Usar);
          $('#div_nueva_ocs').fadeOut('slow');
          smoke.signal("<b>PREORDEN ELIMINADA!</b> <br> ", 800);
        } else {
          smoke.signal("<b>EL ELEMENTO NO PUDO SER BORRADO!</b> <br> ", 800);
        }
      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function VisualizarSolicitud(codSolicitud, e) {

  cargando_toggle();
  var arr = [];
  PosicionarDivLitleLeft('div_visualizar_solicitud', e, 1225);

  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    async: false,
    data: {
      opcion: 4,
      cod_solicitud: codSolicitud
    },
    success: function(json) {
      if (json.error) {
        //  mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        $('#div_visualizar_solicitud').fadeIn('slow');
        for (var datos in json) {
          arr.push(Object.values(json[datos]));
        }
        var table = $('#show_solicitud').DataTable({
          destroy: true
            //,bAutoWidth: false
            //,autoWidth: false
            ,
          processing: false,
          pageLength: 25,
          dom: '<"top">rt<"bottom" ip><"clear">'

            ,
          data: arr,
          columns: [{
            title: "TIPO INSUMO"
          }, {
            title: "CODIGO MATERIAL"
          }, {
            title: "DESCRIPCION"
          }, {
            title: "U. MEDIDA"
          }, {
            title: "PRESUPUESTO"
          }, {
            title: "SOLICITADOS"
          }, {
            title: "DISPONIBLES"
          }]
        });
        cargando_toggle();

      } catch (exception) {
        //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function VisualizarOCS(codOCS, e) {

  cargando_toggle();
  var arr = [];
  PosicionarDivLitleLeft('div_visualizar_ocs', e, 1225);

  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    async: false,
    data: {
      opcion: 14,
      codOCS: codOCS
    },
    success: function(json) {

      if (json.error) {
        //  mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {

        $('#div_visualizar_ocs').fadeIn('slow');
        for (var datos in json) {
          arr.push(Object.values(json[datos]));
        }
        $("#show_detalle_ocs").html('');
        $("#show_detalle_ocs").append('<tfoot><th colspan="7" style="text-align:right"></th><th></th></tfoot>');
        var table = $('#show_detalle_ocs').DataTable({
          destroy: true,
          processing: false,
          pageLength: 25,
          fixedHeader: {            
            footer: true
          },
          dom: '<"top">rt<"bottom" ip><"clear">'
            ,
          footerCallback: function( tfoot, data, start, end, display, json) {
              
            var formatter = new Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD',
              minimumFractionDigits: 4,
            });          
            
            var api = this.api(), data;
            console.log(api);
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $(tfoot).find('th').eq(0).html( "Total compra:");
            $(tfoot).find('th').eq(1).html(formatter.format(total));
          },            
          data: arr,
          columns: [{
            title: "CODIGO INSUMO"
          }, {
            title: "DESCRIPCION"
          }, {
            title: "ID MEDIDA"
          }, {
            title: "U. MEDIDA"
          }, {
            title: "REF. EXTERNA"
          }, {
            title: "CANTIDAD SOLICITADA"
          }, {
            title: "COSTO COMPRA"
          }, {
            title: "TOTAL COMPRA"
          }],
          columnDefs: [{
            targets: [2],
            visible: false,
            searchable: false
          }],
      
        });
        cargando_toggle();
      } catch (exception) {
        //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });

}


function CargarProveedor(e) {

  cargando_toggle();
  var arr = [];
  PosicionarDivLitleLeft('div_proveedores', e, 500);

  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    async: false,
    data: {
      opcion: 12
    },
    success: function(json) {

      if (json.error) {
        //  mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {

        $('#div_proveedores').fadeIn('slow');

        for (var datos in json) {
          arr.push(Object.values(json[datos]));
        }
        var table = $('#show_proveedores').DataTable({
          destroy: true,
          processing: false,
          pageLength: 25,
          dom: '<"toolbar">frtip',
          data: arr,
          columns: [{
              title: "COD PROVEEDOR"
            }, {
              title: "NOMBRE PROVEEDOR"
            }, {
              title: "accion"
            }]

            ,
          columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: "<IMG SRC='./images/select_ad-x.png' WIDTH=20 HEIGHT=20 BORDER=2 ALT='Visualizar' id='TomarProv'> \n\ "
          }]
        });

        $('#show_proveedores tbody').on('click', 'img', function(e) {
          var data = table.row($(this).parents('tr')).data();
          if (this.id == 'TomarProv') {
            var data = table.row($(this).parents('tr')).data();
            TomarProveedor(data[0], data[1], e.originalEvent);
          }
        });
        cargando_toggle();
      } catch (exception) {
        //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}


function TomarProveedor(cod_proveedor, nombre_proveedor, e) {   
    $('#proveedor').val(cod_proveedor);
    $('#name_proveedor').val(nombre_proveedor);    
    $('#div_proveedores').fadeOut('slow');   
}

function EnviarSolicitudCompras(codSolicitud) {

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 6,
      usuario: $('#NmUser').val(),
      id_solicitud: $('#id_solicitudu').val(),
      CodSol: codSolicitud
    },
    success: function(json) {
      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        if (json[0].respta == 'POSITIVO') {
          $('#NewSolicitude').attr("disabled", false);
          smoke.signal("<b>PROCESO EXITOSO!</b> <br> ", 500);
          LoadOrdenesOCOS(0);
        } else {
          smoke.signal("<b>LA SOLICITUD NO PUDO SER PASADA A COMPRAS!</b> <br> ", 500);
        }
      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function maximizarventana() {
  window.moveTo(0, 0);
  window.resizeTo(screen.width, screen.height);
}

function isNumberKey(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 46 || charCode > 57))
    return false;
  return true;
}

function ReloadGrillaInsumos() {

  var indice = $('#tiposolicitud option:selected').val();
  oTable = $('#tbl_solicitudes').DataTable();
  if (indice == 0) {
    oTable.columns(0).search('');
    oTable.draw();
  } else {
    oTable.columns(0).search(indice);
    oTable.draw();
  }
}

function SolicituDeTodos() {

  cargando_toggle();
  var arr = [];
  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    async: false,
    data: {
      opcion: 7,
      ano: $('#ano').val(),
      mes: $('#mes').val(),
      tiposolicitud: $('#tiposolicitudu').val()
    },
    success: function(json) {

      if (json.error) {
        //  mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {

        for (var datos in json) {
          arr.push(Object.values(json[datos]));
        }

        var table = $('#sol_detodos').DataTable({
          destroy: true,
          responsive: true,
          processing: false,
          data: arr,
          bAutoWidth: false,
          columns: [{
            title: "IdEstado"
          }, {
            title: "Estado"
          }, {
            title: "Cod Solicitud"
          }, {
            title: "Fecha Creacion"
          }, {
            title: "Responsable"
          }, {
            title: "No. Solicitud"
          }, {
            title: "Tipo Solicitud"
          }, {
            title: "Bodega Entrega"
          }, {
            title: "Fecha Entrega"
          }, {
            title: "Descripcion"
          }, {
            title: "Total Insumos"
          }, {
            title: "No. OCS"
          }, {
            title: "accion"
          }],
          columnDefs: [{
              targets: -1,
              data: null,
              defaultContent: "<IMG SRC='./images/sl_visualizar.png' WIDTH=40 HEIGHT=40 BORDER=2 title='Visualizar' id='visualizar'> \n\
                                             <IMG SRC='./images/aprove.png' WIDTH=40 HEIGHT=40 BORDER=2 title='Autorizar' id='autorizar_solicitud' class=''> "
            },
            {
              targets: [0],
              visible: false,
              searchable: false
            },
            {
              targets: [8],
              searchable: false
            }
          ]
        });

        $('#sol_detodos tbody').on('click', 'img', function(e) {
          var data = table.row($(this).parents('tr')).data();
          if (this.id == 'visualizar') {
            var data = table.row($(this).parents('tr')).data();
            VisualizarSolicitud(data[2], e.originalEvent);
          }
          if (this.id == 'autorizar_solicitud') {
            var data = table.row($(this).parents('tr')).data();
            AutorizarSolicitud(data[2], e.originalEvent);
          }

        });
        cargando_toggle();
      } catch (exception) {
        //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}


function SolicitudesParaOCS(myquery) {

  cargando_toggle();  
//  $('#sol_paraOrdenCS').DataTable().destroy();
  var arr = [];
  var query;
  query = myquery;
  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    async: false,
    data: {
      opcion: 8,
      ano: $('#ano').val(),
      mes: $('#mes').val(),
      tiposolicitud: $('#tiposolicitudu').val(),
      consultaDB: query
    },
    success: function(json) {

      if (json.error) {
        //  mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {

        for (var datos in json) {
          arr.push(Object.values(json[datos]));
        }
        $('#sol_paraOrdenCS tbody').empty();     
        var table = $('#sol_paraOrdenCS').DataTable({
          destroy: true,
          responsive: true,
          processing: false,
          pageLength: 25,
          data: arr,
          bAutoWidth: false,
          columns: [{
            title: "IdEstado"
          }, {
            title: "Estado"
          }, {
            title: "Cod Solicitud"
          }, {
            title: "Fecha Creacion"
          }, {
            title: "Responsable"
          }, {
            title: "No. Solicitud"
          }, {
            title: "Tipo Solicitud"
          }, {
            title: "Bodega Entrega"
          }, {
            title: "Fecha Entrega"
          }, {
            title: "Descripcion"
          }, {
            title: "Total Insumos"
          }, {
            title: "No. OCS"
          }, {
            title: "accion"
          }],
          columnDefs: [{
              targets: -1,
              data: null,
              defaultContent: "<IMG SRC='./images/sl_visualizar.png' WIDTH=40 HEIGHT=40 BORDER=2 title='Visualizar' id='visualizar'> \n\
                                             <IMG SRC='./images/shopcartadd.png' WIDTH=40 HEIGHT=40 BORDER=2 title='Adicionar a Carrito' id='shopping_cart_addall' class='torototo'> \n\
                                             <IMG SRC='./images/Deny.png' WIDTH=30 HEIGHT=30 BORDER=2 title='Rechazar' id='deny_solicitud' class='torototo'> "
            },
            {
              targets: [0],
              visible: false,
              searchable: false
            },
            {
              targets: [1],
              visible: false,
              searchable: false
            }

          ]

        });

        $('#sol_paraOrdenCS tbody').on('click', 'img', function(e) {
          var data = table.row($(this).parents('tr')).data();
          if (this.id == 'visualizar') {
            var data = table.row($(this).parents('tr')).data();
            VisualizarSolicitud(data[2], e.originalEvent);
          }
          if (this.id == 'shopping_cart_addall') {
            $("#asignar_id_solicitud").val(data[5]); //data[5] es el id solicitud
            $("#asignar_codigo_solicitud").val(data[2]); //data[5] es el cod_solicitud
            AddElementToOrderOCS($('#modo_compra').val(), data[2], '', this, data[5]);
          }
          if (this.id == 'deny_solicitud') {

            $.confirm({
              title: 'Confirmar Devolver Solicitud!',
              content: 'Si devuelve la solicitud No: ' + data[2] + ' no podra recuperarla!',
              buttons: {
                confirm: function() {
                  DevolverASolicitud(data[2], this);
                  $.alert('La Solicitud fue devuelta!');
                },
                cancel: function() {
                  $.alert('Accion Canceleda!');
                }
              }
            });
          }
        });
        cargando_toggle();
      } catch (exception) {
        //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}


function LoadOrdenesCS(myquery) {
 
  cargando_toggle();
  var arr = [];
  var query;
  var recepcion_materiales = false;
  query = myquery;
  var recepcion_materiales = false
  if (query == "SQL_LISTADO_OCS_WITH_FOMS"){
      recepcion_materiales = true
  }
  
  var columnas = [ 
                   {title: "Orden"},
                   {title: "Fecha Creacion"},
                   {title: "Responsable"},
                   {title: "Cod. Proveedor"},
                   {title: "Proveedor"},
                   {title: "Tipo Solicitud"},
                   {title: "Descripcion"},
                   {title: "Direccion Entrega"},
                   {title: "Fecha Entrega"},
                   {title: "Forma Pago"},
                   {title: "accion"}
                ]
    if (recepcion_materiales) {
      obj = columnas.find((o, i) => {
        if (o.title === 'Tipo Solicitud') {
          columnas[i] = {
            title: "FOMS",
            render: function(data, type, row, meta) {
              if (type === 'display') {
                nombre_del_proyecto = row[10]
                data = '<a href="#" data-toggle="tooltip" title="' + nombre_del_proyecto + '">' + data + '</a>'
              }
              return data;
            }
          };
          return true; // parar de buscar
        }
      });
    }
  
  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    async: false,
    data: {
      opcion: 13,
      ano: $('#ano').val(),
      mes: $('#mes').val(),
      tiposolicitud: $('#tiposolicitudu').val(),
      consultaDB: query
    },
    success: function(json) {
      if (json.error) {
        //  mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        for (var datos in json) {
          arr.push(Object.values(json[datos]));
        }
        
        var table = $('#OrdenesCS').DataTable({
          destroy: true,
          responsive: true,
          processing: false,
          pageLength: 25,
          data: arr,
          bAutoWidth: false,
          columns: columnas,         
          columnDefs: [{
            targets: -1,
            data: null,
            render: function(data, type, row, meta){              
                return regresarImagenes(data[0].includes('-'),recepcion_materiales, data[10]);
            }
          }]

        });

        $('#OrdenesCS tbody').on('click', 'img', function(e) {

          var data = table.row($(this).parents('tr')).data();
          if (this.id == 'visualizar_orden') {
            var data = table.row($(this).parents('tr')).data();
            VisualizarOCS(data[0], e.originalEvent);
          }

          if (this.id == 'editar_orden') {
            var data = table.row($(this).parents('tr')).data();
            EditarOCS('EDITAR', data[0], e.originalEvent);
          }

          if (this.id == 'imprimir_pdf') {
            var data = table.row($(this).parents('tr')).data();
            ImprimirOCS(data[0]);
          }
          
          if (this.id == 'comprobar_orden') {
            if (data[10]=='NO'){
                verificarOCS(data[0], table.row($(this).parents('tr')));
            }else{
                smoke.signal("<b>YA ESTA SUBORDEN FUE PASADA APOTEOSYS!</b> <br> ", 400);
            }             
           
          }
          if (this.id == 'eliminar_orden') {
            var data = table.row($(this).parents('tr')).data();
            $.confirm({
              title: 'Confirmar Eliminar Compras!',
              content: 'Si borra la orden No: ' + data[0] + ' no podra recuperarla!',
              buttons: {
                confirm: function() {
                  EliminarOCS(data[0]);
                  $.alert('La Orden fue Eliminada!');
                },
                cancel: function() {
                  $.alert('Accion Canceleda!');
                }
              }
            });

          }

          if (this.id == 'inventory') {
            var data = table.row($(this).parents('tr')).data();            
            var recepcion = $("#paginaRecepcionMateriales").val();
            if (!data[0].includes('-')){
                CrearDespacho('CREAR', data[0], e.originalEvent,recepcion);
            };
          }
          
        });
        cargando_toggle();

      } catch (exception) {
        //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function regresarImagenes(suborden, recepcion, oc_en_apoteosys){
    imagenes = "";
    var botones_recepcion = "";
    var gris = "";
    var btn_gris = "";
    var linea_boton_check ="";
    
    if (oc_en_apoteosys == "SI"){
        btn_gris = "gris";
    }
    
    if (suborden==true){
        gris = "gris";
        linea_boton_check = "<IMG SRC='./images/comprobar.png' WIDTH=33 HEIGHT=33 BORDER=2 title='Comprobar compra' id='comprobar_orden' class='torototo check_orden "+btn_gris+"'>";                                    
    }

   
    if (recepcion==true){
            imagenes = "    <IMG SRC='./images/sl_visualizar.png' WIDTH=37 HEIGHT=37 BORDER=2 title='Visualizar' id='visualizar_orden'> \n\
                            <IMG SRC='./images/Inventory.png' WIDTH=40 HEIGHT=40 BORDER=2 title='Inventario' id='inventory' class='torototo "+gris+"'> \n\
                       "           
    };
    
    if (recepcion ==false ){
        imagenes = `<IMG SRC='./images/sl_visualizar.png' WIDTH=37 HEIGHT=37 BORDER=2 title='Visualizar' id='visualizar_orden'> \n\
                             <IMG SRC='./images/sl_edit.png' WIDTH=37 HEIGHT=37 BORDER=2 title='Editar' id='editar_orden'> \n\
                             <IMG SRC='./images/pdf-128.png' WIDTH=40 HEIGHT=40 BORDER=2 title='Imprimir PDF' id='imprimir_pdf' class='torototo'> \n\
                             <IMG SRC='./images/aprove.png' WIDTH=40 HEIGHT=40 BORDER=2 title='Autorizar Compra' id='autorizar_compra' class='torototo'> \n\
                             <IMG SRC='./images/Inventory.png' WIDTH=40 HEIGHT=40 BORDER=2 title='Inventario' id='inventory' class='torototo `+gris+`'> \n\
                             <IMG SRC='./images/DeleteMachinne.png' WIDTH=33 HEIGHT=33 BORDER=2 title='Eliminar Compra' id='eliminar_orden' class='torototo'> \n\                             
                    `+linea_boton_check;
    };
   
    return imagenes;
}

function InsumosParaOCS(myquery) {

  cargando_toggle();
  var arr = [];
  var query;
  query = myquery;

  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    async: false,
    data: {
      opcion: 8,
      ano: $('#ano').val(),
      mes: $('#mes').val(),
      tiposolicitud: $('#tiposolicitudu').val(),
      consultaDB: query
    },
    success: function(json) {

      if (json.error) {
        //  mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {

        for (var datos in json) {
          arr.push(Object.values(json[datos]));
        }

        var table = $('#sol_paraOrdenCS').DataTable({
          destroy: true,
          responsive: true,
          processing: false,
          pageLength: 25,
          data: arr,
          bAutoWidth: false,
          columns: [{
                title: "Cod Solicitud"
              }, {
                title: "No. Solicitud"
              }, {
                title: "Nombre Proyecto"
              }, {
                title: "Fecha Creacion"
              }, {
                title: "Responsable"
              }
              //,{ title: "Tipo Solicitud" }
              , {
                title: "Bodega Entrega"
              }, {
                title: "Fecha Entrega"
              }, {
                title: "Codigo Insumo"
              }, {
                title: "Tipo Insumo"
              }, {
                title: "Descripcion Insumo"
              }, {
                title: "U. Medida"
              }, {
                title: "Total"
              }, {
                title: "accion"
              }
            ]

            ,
          columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: "<IMG SRC='./images/shopcartadd.png' WIDTH=40 HEIGHT=40 BORDER=2 ALT='Visualizar' id='shopping_cart_addone' class='torototo'>"
          }, {
            targets: [1],
            visible: false,
            searchable: false
          }]
        });

        $('#sol_paraOrdenCS tbody').on('click', 'img', function(e) {

          var data = table.row($(this).parents('tr')).data();
          if (this.id == 'shopping_cart_addone') {
            AddElementToOrderOCS($('#modo_compra').val(), data[0], data[7], this);
            this.id = 'TempOlvido';
          }
        });
        cargando_toggle();

      } catch (exception) {
        //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}


function AddElementToOrderOCS(ModoCompra, CodSolicitud, CodInsumo, ObjetAction, id_sol) {

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 9,
      usuario: $('#NmUser').val(),
      modo_compra: ModoCompra,
      cod_solicitud: CodSolicitud,
      cod_insumo: CodInsumo,
      id_sol: id_sol
    },
    success: function(json) {
      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
          console.log(json);
        if (json[0].respta == 'POSITIVO') {

          //marcar carrito
          $(ObjetAction).attr("src", "./images/shopcartdown.png");

          //Encender el carri-ojo
          $("#carri-ojo").attr("src", "./images/review_item.png");

          //Se habilita el objeto
          $('#carri-ojo').attr('disabled', false);

          smoke.signal("<b>SE HA ADICIONADO UN NUEVO ELEMENTO</b> <br> ", 400);
        } else {
          smoke.signal("<b>NEGADO!</b> <br> ", 400);
        }

      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function PopUpAPUs(CodigoInsumo, e) {

  var coordenada_x;
  var accion = 'NUEVO';
  coordenada_x = 1225;
  PosicionarDivLitleLeft('div_InsumosApu', e, coordenada_x);

  cargando_toggle();

  $.ajax({
    type: "POST",
    url: "/fintra/jsp/solicitud_insumos/nueva_solicitud_detalle.jsp",
    async: true,
    dataType: "html",
    data: {
      id_sol: $('#id_solicitudu').val(),
      cod_insumo: CodigoInsumo,
      ACCIONE: accion,
      ITEM: 1
    },
    success: function(data) {
      if (data != "") {

        $("#div_InsumosApu").html(data);
        $('#div_InsumosApu').fadeIn('slow');
        cargando_toggle();
      }
    }
  });
  $('#div_espera').fadeOut('slow');
}

function ImprimirOCS(orden, e) {
  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    data: {
      opcion: 16,
      orden_compra: orden
    },
    success: function(data) {
      var res = data.respuesta;
      if (res === 'OK') {
        smoke.signal("<b>SE GENERO EL PDF DE LA ORDEN No: " + orden + ". REVISAR EL LOG DE DESCARGA</b> <br> ", 1000);
        //mensajesDelSistema("Exito ", '400', '120', true);
      } else {
        smoke.signal("<b>ERROR AL GENERAR EL PDF!</b> <br> ", 500);
        // mensajesDelSistema("Error", '363', '140', false);
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function verificarOCS(orden, data ){
    var tr = data.node()
    $(tr).find(".check_orden").addClass('gris')

  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    data: {
      opcion: 39,
      orden_compra: orden
    },
    success: function(data) {
      var res = data.respuesta;
      if (res === 'ok') {
        smoke.signal("<b>SUB ORDEN " + orden + "FUE PASADA A APOTEOSYS!</b> <br> ", 1000);
        //mensajesDelSistema("Exito ", '400', '120', true);
      } else {
        smoke.signal("<b>ERROR</b> <br> ", 500);
        // mensajesDelSistema("Error", '363', '140', false);
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
        
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function CatalogoInsumos(solicitud, e) {

  var coordenada_x = 1225;
  PosicionarDivLitleLeft('div_CatalogoInsumos', e, coordenada_x);
  cargando_toggle();

  $.ajax({
    type: "POST",
    url: "/fintra/jsp/solicitud_insumos/nuevo_catalogo.jsp",
    async: true,
    dataType: "html",
    data: {
      id_sol: $('#id_solicitudu').val(),
      solicitud: solicitud
    },
    success: function(data) {
      if (data != "") {
        $("#div_CatalogoInsumos").html(data);
        $('#div_CatalogoInsumos').fadeIn('slow');
        cargando_toggle();
      }
    }
  });
}

function CargarInfoRequisicion(Rqs) {

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 18,
      RqSol: Rqs
    },
    success: function(json) {
      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        $("#tiposolicitud option[value=" + json[0].tipo_solicitud + "]").attr("selected", true);
        $('#fecha_actual').val(json[0].fecha_actual);
        $('#fecha_entrega').val(json[0].fecha_entrega);
        $('#descripcion').val(json[0].descripcion);
        $('#cbx_lista_bodegas').val(json[0].id_bodega);
        $('#txt_direccion').val(json[0].direccion_entrega);
        
        ReloadGrillaInsumos();
      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function ValidarFormaPago() {

  if ($('#f_pago').val() == '1') {
    $('#dias_pago_credito').val("");
    $('#dias_pago_credito').attr('readonly', true);
  } else {
    $('#dias_pago_credito').attr('readonly', false);
  }

}

function AutorizarSolicitud(Solicitud, e) {

  var coordenada_x = 1225;
  PosicionarDivLitleLeft('div_autorizar_solicitud', e, coordenada_x);
  cargando_toggle();
  $('#div_autorizar_solicitud').fadeIn('slow');
  $('#sol_aprobar').val(Solicitud);
  $("#opcion_accion option[value=0]").attr("selected", true);
  $('#DescripcionAprobacion').val("");
  cargando_toggle();

}

function GuardarRechazarSolicitud() {

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 20,
      usuario: $('#NmUser').val(),
      sol_aprobar: $('#sol_aprobar').val(),
      opcion_accion: $('#opcion_accion').val(),
      DescripcionAprobacion: $('#DescripcionAprobacion').val()
    },
    success: function(json) {
      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        $('#div_autorizar_solicitud').fadeOut('slow');
      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });

}

function ActualizarSolicitudes(uSer, oSC) {

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 21,
      usuario: uSer,
      oSC: oSC
    },
    success: function(json) {

      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        smoke.signal("<b>ORDEN No: " + oSC + " GENERADA EXITOSAMENTE!</b> <br> ", 1200);
        SolsParaOCS(uSer);


      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }

  });

}

function ValidarCierreCatalogo(eventy) {

  $('#div_CatalogoInsumos').fadeOut('slow');
  if ($('#item').val() != '') {
    CrearEditarSolicitud('EDIT', $('#item').val(), eventy);
  } else {
    CrearEditarSolicitud('NUEVO', '', eventy);
  }
}

function DevolverASolicitud(CodSolicitud, ObjetAction) {

  //cargando_toggle();

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 22,
      cod_solicitud: CodSolicitud
    },
    success: function(json) {

      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        if (json[0].respta == 'NEGATIVO') {
          smoke.signal("<b>ERROR AL DEVOLVER LA SOLICITUD!</b> <br> ", 3000);
        } else {
          SolsParaOCS('sol_paraOrdenCS');
          smoke.signal("<b>LA SOLICITUD SE DEVOLVIO EXITOSAMENTE!</b> <br> ", 3000);
        }


      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function EliminarOCS(CodOCS) {

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 23,
      cod_OCS: CodOCS,
      usuario: $('#NmUser').val()
    },
    success: function(json) {

      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        if (json[0].respta == 'NEGATIVO') {
          smoke.signal("<b>LA ORDEN No: " + CodOCS + " NO PUDO SER ELIMINADA</b> <br> ", 2000);
        } else {
          smoke.signal("<b>LA ORDEN No: " + CodOCS + " FUE ELIMINADA EXITOSAMENTE! Solicitud No: " + json[0].respta + "</b> <br> ", 3000);
        }
      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function RecargarRequisiciones() {

  $('#misreq').empty();
  document.getElementById('misreq').innerHTML = "<table id='example' class='table table-striped table-bordered'></table>";
  $('#div_nueva_solicitud').fadeOut('slow');
  LoadOrdenesOCOS(0);
}

function EditarOCS(accion, item, e) {

  var coordenada_x;
  coordenada_x = 1450; //1450
  PosicionarDivLitleLeft('div_editar_ocs', e, coordenada_x);
  cargando_toggle();
  $.ajax({
    type: "POST",
    url: "/fintra/jsp/solicitud_insumos/editar_ocs.jsp",
    async: true,
    dataType: "html",
    data: {
      usuario: $('#NmUser').val(),
      ACCIONE: 'accion',
      ORDENEDITAR: item
    },
    success: function(data) {
      if (data != "") {
        $("#div_editar_ocs").html(data);
        $('#div_editar_ocs').fadeIn('slow');
        $('#div_espera').fadeOut('slow');
        cargando_toggle();

      } else {
        cargando_toggle();
        smoke.signal("<b>NO SE ENCONTRARON SOLICITUDES PENDIENTES</b> <br> ", 800);
      }
    }
  });

  $('#div_espera').fadeOut('slow');
}


function inventario_todos() {

  cargando_toggle();
  var arr = [];
  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    async: false,
    data: {
      opcion: 24,
      ano: $('#ano').val(),
      mes: $('#mes').val(),
      tipomovimiento: $('#tiposolicitudu').val()
    },
    success: function(json) {
       
      if (json.error) {
        //  mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {

        for (var datos in json) {
          arr.push(Object.values(json[datos]));
        }
        console.log(arr);
        var table = $('#sol_detodos').DataTable({
          destroy: true,
          responsive: true,
          processing: false,
          data: arr,
          bAutoWidth: false,
          columns: [{
            title: "Tipo de movimiento"
          }, {
            title: "Codigo"
          }, {
            title: "Fecha de movimiento"
          }, {
            title: "Id solicitud origen"
          }, {
            title: "Id solicitud destino"
          }, {
            title: "Bodega origen"
          }, {
            title: "Bodega destino"
          }, {
            title: "accion"
          }],
          columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: "<IMG SRC='./images/sl_visualizar.png' WIDTH=40 HEIGHT=40 BORDER=2 title='Visualizar' id='visualizar'>\n\
                             <IMG SRC='./images/pdf-128.png' WIDTH=40 HEIGHT=40 BORDER=2 title='Imprimir PDF' id='imprimir_pdf'>"
          }]

        });

        $('#sol_detodos tbody').on('click', 'img', function(e) {
          var data = table.row($(this).parents('tr')).data();
          if (this.id == 'visualizar') {
            var data = table.row($(this).parents('tr')).data();
            DetalleVisualizacion(data[1], e.originalEvent);
          }

          if (this.id == 'autorizar_solicitud') {
            var data = table.row($(this).parents('tr')).data();
            AutorizarSolicitud(data[2], e.originalEvent);
          }
          if (this.id == 'imprimir_pdf') {
            var data = table.row($(this).parents('tr')).data();
            imprimirPdfMovimientoInventario(data[1], e.originalEvent);
          }          
        });
        cargando_toggle();
      } catch (exception) {
        //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function ClearTableMovInventario(NameTable) {

  var Tabla = $('#' + NameTable).DataTable();
  Tabla.clear();
  Tabla.destroy();
  $('#misreq').empty();
  document.getElementById('misreq').innerHTML = "<table id='sol_detodos' class='table table-striped table-bordered'></table>";
  inventario_todos();
}

function despachos_todos() {

  cargando_toggle();
  var arr = [];

  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    async: false,
    data: {
      opcion: 29,
      ano: $('#ano').val(),
      mes: $('#mes').val()
    },
    success: function(json) {
      if (json.error) {
        //  mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {

        for (var datos in json) {
          arr.push(Object.values(json[datos]));
        }
        var table = $('#sol_detodos').DataTable({
          destroy: true,
          responsive: true,
          processing: false,
          data: arr,
          bAutoWidth: false,
          columns: [{
            title: "Codigo Despacho"
          }, {
            title: "Fecha Despacho"
          }, {
            title: "Orden Compra"
          }, {
            title: "Fecha Entrega"
          }, {
            title: "Cod Proveedor"
          }, {
            title: "Proveedor"
          }, {
            title: "Dir. Entrega"
          }, {
            title: "Costo Despacho"
          }, {
            title: "accion"
          }],
          columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: "<IMG SRC='./images/sl_visualizar.png' WIDTH=40 HEIGHT=40 BORDER=2 title='Visualizar' id='visualizar'> "
          }]
        });

        $('#sol_detodos tbody').on('click', 'img', function(e) {
          var data = table.row($(this).parents('tr')).data();
          if (this.id == 'visualizar') {
            var data = table.row($(this).parents('tr')).data();
            DetalleDespacho(data[0], e.originalEvent);
          }

          if (this.id == 'autorizar_solicitud') {
            var data = table.row($(this).parents('tr')).data();
            AutorizarSolicitud(data[2], e.originalEvent);
          }
        });

        cargando_toggle();

      } catch (exception) {
        //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function ClearTableDespachos(NameTable) {

  var Tabla = $('#' + NameTable).DataTable();
  Tabla.clear();
  Tabla.destroy();
  $('#misreq').empty();
  document.getElementById('misreq').innerHTML = "<table id='sol_detodos' class='table table-striped table-bordered'></table>";
  despachos_todos();
}


function VisualizarMovimiento(codSolicitud, e) {

  cargando_toggle();
  var arr = [];
  PosicionarDivLitleLeft('div_visualizar_solicitud', e, 1225);
  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    async: false,
    data: {
      opcion: 25,
      cod_movimiento: codSolicitud
    },
    success: function(json) {
      if (json.error) {
        //  mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        $('#div_visualizar_solicitud').fadeIn('slow');

        for (var datos in json) {
          arr.push(Object.values(json[datos]));
        }
        var table = $('#show_solicitud').DataTable({
          destroy: true,
          processing: false,
          pageLength: 25,
          dom: '<"top">rt<"bottom" ip><"clear">',
          data: arr,
          columns: [{
            title: "TIPO INSUMO"
          }, {
            title: "CODIGO MATERIAL"
          }, {
            title: "DESCRIPCION"
          }, {
            title: "U. MEDIDA"
          }, {
            title: "PRESUPUESTO"
          }, {
            title: "SOLICITADOS"
          }, {
            title: "DISPONIBLES"
          }]
        });
        cargando_toggle();

      } catch (exception) {
        //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function DetalleVisualizacion(codMovimiento, e) {

  cargando_toggle();
  var arr = [];
  PosicionarDivLitleLeft('div_visualizar_solicitud', e, 1225);

  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    async: false,
    data: {
      opcion: 25,
      cod_movimiento: codMovimiento
    },
    success: function(json) {
      if (json.error) {
        //  mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        $('#div_visualizar_solicitud').fadeIn('slow');
        for (var datos in json) {
          arr.push(Object.values(json[datos]));
        }
        var table = $('#show_solicitud').DataTable({
          destroy: true,
          processing: false,
          pageLength: 25,
          dom: '<"top">rt<"bottom" ip><"clear">',
          data: arr,
          columns: [{
            title: "CODIGO MATERIAL"
          }, {
            title: "DESCRIPCION"
          }, {
            title: "U. MEDIDA"
          }, {
            title: "REF. EXTERNA"
          }, {
            title: "CANTIDAD"
          }]
        });
        cargando_toggle();

      } catch (exception) {
        //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}


function imprimirPdfMovimientoInventario(codMovimiento, e) {
   var test = 0;
  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    data: {
      opcion: 35,
      cod_movimiento: codMovimiento
    },
    success: function(data) {
      var res = data.respuesta;      
      if (res === 'OK') {
        alert("Se genero el PDF del movimiento '"+codMovimiento+"', revisar el log de descargas.");         
      } else {
        toastr.error("ERROR AL GENERAR EL PDF!");
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}


function CrearDespacho(accion, item, e, recepcion) {
  var coordenada_x;
  coordenada_x = 1450; //1450
  PosicionarDivLitleLeft('div_nuevo_despacho', e, coordenada_x);
  var direccion_entrega = getDireccionEntrega(item);
  $.ajax({
    type: "POST",
    url: "/fintra/jsp/solicitud_insumos/nuevo_despacho.jsp",
    async: true,
    dataType: "html",
    data: {
      usuario: $('#NmUser').val(),
      ACCIONE: accion,
      ITEM: item,
      direccion_entrega: direccion_entrega,
      recepcion : recepcion
    },
    success: function(data) {
      if (data != "") {

        $("#div_nuevo_despacho").html(data);
        $('#div_nuevo_despacho').fadeIn('slow');
        $('#div_espera').fadeOut('slow');
        cargando_toggle();
      } else {
        cargando_toggle();
        smoke.signal("<b>NO HAY DESPACHO APROBADO</b> <br> ", 800);
      }
    }
  });
}


function GuardarDespacho(Usar, recepcion) {

  if ($('#tiposolicitudu').val() == '') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#proveedor').val() == '0') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#descripcion').val() == '') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#fecha_entrega').val() == '') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else if ($('#direccion_entrega').val() == '') {
    smoke.signal("<b>FALTAN CAMPOS REQUERIDOS!</b> <br> ", 1000);
  } else {

    if ($('#total_despacho').val() != '0') {
      $.ajax({
        type: "POST",
        url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
        async: true,
        dataType: "json",
        data: {
          opcion: 26,
          usuario: Usar,
          tipo_solicitud: $('#OrdenCompDespacho').val(),
          proveedor: $('#proveedor').val(),
          descripcion: $('#descripcion').val(),
          fecha_entrega: $('#fecha_entrega').val(),
          direccion_entrega: $('#direccion_entrega').val(),
          OrdenCompDespacho: $('#OrdenCompDespacho').val()
        },
        success: function(json) {

          if (json.error) {
            mensajesDelSistema(json.error, '250', '180');
            return;
          }
          try {
            if (json[0].resultado_accion == 'NEGATIVO') {
              smoke.signal("<b>NO SE PUDO GUERDAR EL DESPACHO!</b> <br> ", 800);
            } else {
              $('#orden_ocs').val(json[0].resultado_accion);
              GuardarDetalleDespacho(Usar, json[0].resultado_accion, recepcion)
            }

          } catch (exception) {
            //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert("Error: " + xhr.status + "\n" +
            "Message: " + xhr.statusText + "\n" +
            "Response: " + xhr.responseText + "\n" + thrownError);
        }
      });

    } else {
      smoke.signal("<b>EL DESPACHO ESTA EN CERO(0)!</b> <br> ", 1000);
    }
  }
}

function GuardarDetalleDespacho(UserMe, NoDsptch, recepcion) {

  var TablaInsumos = new CapturarInputsOrdenCS(); //funcionFomurarios();
  var JsonFormulario = TablaInsumos.InfoDespacho('CapsuleDespacho');

  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 27,
      usuario: UserMe,
      NoDsptch: NoDsptch,
      InfoDespacho: JSON.stringify({
        json: JsonFormulario
      })
    },
    success: function(json) {

      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        InventarioDespacho(UserMe, NoDsptch, recepcion);
      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function InventarioDespacho(UserMe, NoDsptch, recepcion) {
  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    async: true,
    dataType: "json",
    data: {
      opcion: 28,
      NoDsptch: NoDsptch
    },
    success: function(json) {

      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        $("#tipo_solicitud").prop("disabled", true);
        $("#proveedor").prop("disabled", true);
        $("#descripcion").prop("disabled", true);
        $("#orden_ocs").prop("disabled", true);
        $("#fecha_entrega").prop("disabled", true);
        $("#direccion_entrega").prop("disabled", true);
        $("#tbl_despacho").find("input,button,textarea,select").attr("disabled", "disabled");
        $('#ExtencionCompleta').hide();
        $('#img_guardar_dsp').hide();
        OrdenesCS(recepcion);
        smoke.signal("<b>EL DESPACHO No: " + NoDsptch + " GENERADA EXITOSAMENTE!</b> <br> ", 1200);
      } catch (exception) {
        //mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function DetalleDespacho(codDespacho, e) {

  cargando_toggle();
  var arr = [];
  PosicionarDivLitleLeft('div_visualizar_solicitud', e, 1225);
  $.ajax({
    type: 'POST',
    url: '/fintra/controlleropav?estado=Compras&accion=Proceso',
    dataType: 'json',
    async: false,
    data: {
      opcion: 30,
      cod_movimiento: codDespacho
    },
    success: function(json) {
      if (json.error) {
        //  mensajesDelSistema(json.error, '250', '180');
        return;
      }
      try {
        $('#div_visualizar_solicitud').fadeIn('slow');
        for (var datos in json) {
          arr.push(Object.values(json[datos]));
        }
        var table = $('#show_solicitud').DataTable({
          destroy: true,
          processing: false,
          pageLength: 25,
          dom: '<"top">rt<"bottom" ip><"clear">',
          data: arr,
          columns: [{
            title: "CODIGO MATERIAL"
          }, {
            title: "DESCRIPCION"
          }, {
            title: "U. MEDIDA"
          }, {
            title: "REF. EXTERNA"
          }, {
            title: "CANT. RECIBIDA"
          }, {
            title: "COSTO UNITARIO RECIBIDO"
          }, {
            title: "COSTO TOTAL RECIBIDO"
          }]
        });
        cargando_toggle();

      } catch (exception) {
        //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
      }

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
}

function formato(valor) {
  var vaux;
  try {
    valor = valor.toString().replace(new RegExp(',', 'g'), '');
    var pattern = /\S+/;
    if (pattern.test(valor)) {
      pattern = /^-?(\d+\.?\d*)$|(\d*\.?\d+)$/;
      if (pattern.test(valor)) {
        vaux = parseFloat(valor);
      } else {
        vaux = 0;
      }
    } else {
      vaux = 0;
    }
  } catch (exc) {
    vaux = 0;
  }
  return {
    moneda: vaux.toString().replace(/(\d)(?=(\d{3})+(\.|$))/g, '$1,'),
    numero: vaux,
    porcentaje: vaux / 100
  }
}

function replaceAll(text, busca, reemplaza) {
  while (text.toString().indexOf(busca) != -1)
    text = text.toString().replace(busca, reemplaza);
  return text;
}

function getDireccionEntrega(item) {

  var direccion_entrega;
  $.ajax({
    type: "POST",
    url: '/fintra/controlleropav?estado=Orden&accion=Compra',
    async: false,
    dataType: "json",
    data: {
      opcion: 2,
      orden_compra: item
    },
    success: function(json) {

      if (json.error) {
        mensajesDelSistema(json.error, '250', '180');
        return;
      }

      direccion_entrega = json[0]["direccion_entrega"];

    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("Error: " + xhr.status + "\n" +
        "Message: " + xhr.statusText + "\n" +
        "Response: " + xhr.responseText + "\n" + thrownError);
    }
  });
  return direccion_entrega;
};


function CrearEntradaBodega() {
  $('#show_proveedores tbody').empty();      
  $('#div_espera').fadeIn('slow'); 
  $.ajax({
    type: "POST",
    url: "/fintra/jsp/solicitud_insumos/formulario_movimiento_inventario.jsp",
    async: true,
    dataType: "html",
    data: {
      estado: 'aaa'
    },
    success: function(data) {
        $("#div_nueva_ocs").html(data);
        $('#div_nueva_ocs').fadeIn('slow');
        $('#div_espera').fadeOut('slow');
    }
  });  
};

 