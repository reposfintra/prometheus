$(document).ready(function() {
    $("#nit").focus();
    cargarDepartamentos('cooddpto');
    cargarDepartamentos('dep_dir');
    $('#dep_dir').val('ATL');
    cargarCiudad('ATL', "ciu_dir");
    $('#ciu_dir').val('BQ');
    cargarVias('BQ', "via_princip_dir");
    cargarVias('BQ', "via_genera_dir");

    //agregar eventos onchange
    $("#cooddpto").change(function() {
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "codciu");
    });
    $("#dep_dir").change(function() {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciu_dir");
    });
    $("#ciu_dir").change(function() {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarVias(op, "via_princip_dir");
        cargarVias(op, "via_genera_dir");
    });
    $("#via_princip_dir").change(function() {
          $("#via_genera_dir").val('');
    });
    //agregar eventos onclick
    $("#actulizar").click(function() {
        guardar();
    });
    
    $("#salir").click(function(){
        close();        
    });
    
    $("#limpiar").click(function (){
        limpiar();
    });


});



function buscarCliente() {
 
    if (document.getElementById("nit").value !== '') {
        var p = document.getElementById("nit").value;
        mensajesDelSistema2("Espere un momento por favor...", "280", "150");
        $.ajax({
            url: './controller?estado=Clientefen&accion=Update&op=3&nit=' + p,
            method: 'post',
            dataType: 'text',
            success: function(resp) {
                var datos = resp;

                if (trim(resp) !== 'N') {
                    setTimeout(function() {
                    var array = resp.split(";");
                    cargarCiudad(array[8], "codciu");
                    document.getElementById("codigo").value = array[0];
                    document.getElementById("name").value = array[1];
                    document.getElementById("direccion").value = array[3];
                    document.getElementById("telefono").value = array[4];
                    document.getElementById("celular").value = array[5];
                    document.getElementById("barrio").value = array[6];
                    document.getElementById("codciu").value = array[7];
                    document.getElementById("cooddpto").value = array[8];
                    document.getElementById("observaciones").value = array[9];
                    document.getElementById("email").value = array[10];
                     $("#nit").attr("readonly","readonly");
                    buscar_negocio(p);
                     $("#dialogo2").dialog("close"); 
                    }, 1000);
                   
                  
                } else {

                    mensajesDelSistema("No se encontraron resultados.","250","150")
                }

            }
        });
    } else {

          mensajesDelSistema("No se ha digitado un documento para consultar","250","150")
    }
}


function trim(cadena) {
    var cadena1 = cadena.replace('/^\s+/', '').replace('/\s+$/', '');
    return(cadena1);
}

function limpiar() {
    document.getElementById("nit").value = "";
    document.getElementById("codigo").value = "";
    document.getElementById("name").value = "";
    document.getElementById("direccion").value = "";
    document.getElementById("telefono").value = "";
    document.getElementById("celular").value = "";
    document.getElementById("barrio").value = "";
    document.getElementById("codciu").value = "";
    document.getElementById("cooddpto").value = "";
    document.getElementById("observaciones").value = "";
    document.getElementById("email").value = "";
    $("#nit").removeAttr("readonly");
    $(".negocios").remove();
    $("#nit").focus();

}

function validar_email_structure( email ) 
{
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}

function validateMail(){
    var _mail = document.getElementById("email").value;
    var reg = /^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)+([a-zA-Z0-9]{2,3})?(com|COM|es|ES|edu.co|EDU.CO|com.co|COM.CO|gov.co|GOV.CO|mil.co|MIL.CO)$/;
    if(reg.test(_mail)){
         $("#email").removeClass("error");
    }else{
         $("#email").addClass("error");        
    }
}

function guardar() {

    if (document.getElementById("nit").value !== '') {

        var nit = document.getElementById("nit").value;
        var codigo = document.getElementById("codigo").value;
        var name = document.getElementById("name").value;
        var direccion = document.getElementById("direccion").value;
        var telefono = document.getElementById("telefono").value;
        var celular = document.getElementById("celular").value;
        var barrio = document.getElementById("barrio").value;
        var codciu = document.getElementById("codciu").value;
        var cooddpto = document.getElementById("cooddpto").value;
        var email = document.getElementById("email").value;
        var observaciones = document.getElementById("observaciones").value;

        if (nit !== '' && codigo !== '' && name !== '' && direccion !== '' && telefono !== '' &&
            barrio !=='' && codciu !=='' && cooddpto !=='' &&  email !== '' && observaciones !== '') {
        
            var tabla = [];
            var negocios = {};
            
            var cod_neg, num_sol, ext_email, ext_corr;
            $("#tabla_negocios").find("tbody >tr").each(function (index) {
               
                if (index >= 0) {                    
                    $(this).children("td").each(function (index2) {
                        switch (index2) {

                            case 0:
                                cod_neg = $(this).text();
                                break;
                            case 1:
                                num_sol = $(this).text();
                                break;
                            case 2:
                                ext_email = $("#chkemail"+num_sol).is(":checked");
                                break;
                            case 3:
                                ext_corr = $("#chkcorr"+num_sol).is(":checked");
                                break;
                        }
                    });
                    var row = {'cod_neg': cod_neg, 'num_sol': num_sol, 'ext_email': ext_email, 'ext_corr': ext_corr};
                    tabla.push(row);
                   
                    negocios ["rows"] = tabla;

                } 
            });
            
            if(validar_email_structure(email)){
                
                $.ajax({
                    type: 'POST',
                    url: "./controller?estado=Clientefen&accion=Update&op=6",
                    dataType: 'json',
                    async:false,
                    data: {
                        nit: nit,
                        codigo: codigo,
                        name: name.toUpperCase(),
                        direccion: direccion.toUpperCase(),
                        telefono: telefono.toUpperCase(),
                        celular : celular.toUpperCase(),
                        barrio : barrio.toUpperCase(),
                        codciu: codciu,
                        cooddpto:cooddpto,
                        email: email,
                        observaciones:observaciones,
                        negocios:JSON.stringify(negocios)

                    },
                    method: 'post',
                    success: function(resp) {
                        if(resp.error){
                            document.getElementById("email").focus();
                            $("#email").addClass("error");
                            respuesta = true;
                            mensajesDelSistema(resp.error,"250","150");
                        }else{
                            $("#email").removeClass("error");//Removemos la clase de error                            
                            respuesta = false;
                             limpiar();
                             $(".negocios").remove();
                            mensajesDelSistema(resp.mensaje,"250","150");
                            $("#nit").focus();
                        }

                    }
                });      
            }else{                
                mensajesDelSistema("Error! email Invalido","300","150");                
            }
        } else {

            mensajesDelSistema("Para modificar debes llenar todos los campos.","300","150");
        }



    } else {

        mensajesDelSistema("En este momento este boton no esta disponible.","300","150");

    }



}

function cargarDepartamentos(combo) {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructuracion&accion=Negocios",
        dataType: 'json',
        async:false,
        data: {
            opcion: 5
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#'+combo).append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#'+combo).append('<option value=' + key + '>' + json[key] + '</option>');

                    }

                } catch (exception) {
                    alert('error : ' + key + '>' + json[key][key]);
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!","300","150");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Reestructuracion&accion=Negocios",
            dataType: 'json',
            data: {
                opcion: 6,
                cod_ciudad: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '450', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function Posicionar_div(id_objeto,e){
  obj = document.getElementById(id_objeto);
  var posx = 0;
  var posy = 0;
  if (!e) var e = window.event;
  if (e.pageX || e.pageY) {
   posx = e.pageX;
   posy = e.pageY;
  }
  else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
      }
      else
       alert('ninguna de las anteriores');

 obj.style.left = posx+'px';
 obj.style.top = posy+'px';

}

function genDireccion(elemento,e) {
  
    var contenedor = document.getElementById("direccion_dialogo")
      , res = document.getElementById("dir_resul");
    $("#direccion_dialogo").draggable({ handle: "#drag_direcciones"});  
    Posicionar_div("direccion_dialogo",e);  
    contenedor.style.display = "block";
    
    res.name = elemento;    
    res.value = (elemento.value) ? elemento.value : '';
    
}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3: 
            var res = document.getElementById('dir_resul')
              , des = document.getElementById(res.name);
              des.value = res.value;
        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");
            
            document.getElementById("direccion_dialogo").style.display = "none";
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
              , g = document.getElementById('via_genera_dir')
              , opcion;           
            for (var i = 0; i < g.length; i++) {
                
                opcion = g[i];
                
                if (opcion.value === p) {
                    
                    opcion.style.display = 'none';
                    opcion.disabled = true;
                    
                } else {
                    
                    opcion.disabled = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if(!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                || !jQuery('#placa_dir').val() ) {
                  jQuery('#dir_resul').val("");
                  jQuery('#dir_resul').attr("class","validation-failed");
              } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                    jQuery('#via_princip_dir option:selected').text()
                   + ' '  + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                   + ' '  + jQuery('#via_genera_dir option:selected').text().trim()
                   + ' '  + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                   + ((jQuery('#via_genera_dir option:selected').text().toUpperCase()==='#' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='TRANSVERSAL') ?  '-':  ' ')
                   + jQuery('#placa_dir').val().trim().toUpperCase()
                   + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
              }
            break;
    }
}


function cargarVias(codciu, combo) {
    //alert(codciu);

        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "/fintra/controller?estado=GestionSolicitud&accion=Aval",
            dataType: 'json',
            data: {
                opcion: 'cargarvias',
                ciu: codciu
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''></option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } 
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

}

 function resetAddressValues(){
       $("#dir_resul").val('');
       $("#nom_princip_dir").val('');
       $("#nom_genera_dir").val('');
       $("#placa_dir").val('');
       $("#cmpl_dir").val('');
       $("#via_princip_dir").val('');
       $("#via_genera_dir").val(''); 
       $("#nit").removeAttr("readonly");
 }

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function mensajesDelSistema2(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();
}


function buscar_negocio(nit){
    $(".negocios").remove();
     $.ajax({
      caption: "INICIAR SESION",
            url: './controller?estado=Clientefen&accion=Update&op=5&nit=' + nit,
            mtype: "POST",
            datatype: "json",
         success: function(json) {
            json.rows.forEach(negocios => {
                
                 var e_email = ( negocios.extracto_email==="t") ? 'checked' : '';
                 var e_corr = ( negocios.extracto_correspondecia==="t") ? 'checked' : '';
                
              var fila = '<tr id="' + negocios.cod_neg + '" class="negocios">\n\
                <td id="negocio" class="td1">' + negocios.cod_neg  + '</td>\n\
                <td id="numero_solicitud" class="td1">' + negocios.numero_solicitud  + '</td>\n\
                <td id="extracto_email" class="td1"><input type="checkbox" '+e_email+' id="chkemail'+negocios.numero_solicitud +'"></input></td>\n\
                <td id="extracto_correspondencia" class="td1"><input type="checkbox" '+e_corr+' id="chkcorr'+negocios.numero_solicitud +'"></input></td>\n\
               /</tr>'; 

                $('#tabla_negocios tbody').append(fila);
            });
                        
        },
        error: function(xhr, ajaxOptions, thrownError) {
              alert(xhr, '350', '150',  true); 
        }
    }); 
    
  }
  
 