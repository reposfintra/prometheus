/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global ultilidad llama las funciones del otro js*/

$(document).ready(function () {
    $("#fecha_max").css("display", "none");
    $("#labelFechaMax").css("display", "none");
    $("#fecha_max").val('');
    $("#fecha_max").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        // maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $("#fecha_max").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

    convenios();

    $('#buscar').click(function () {
        $("#fecha_max").css("display", "block");
        $("#labelFechaMax").css("display", "block");
        cargarEstadoCuenta();
    });
});

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center", modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear botón cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarEstadoCuenta() {
    var grid_tabla_ = jQuery("#tabla_estadoCuenta");
    if ($("#gview_tabla_estadoCuenta").length) {
        reloadGridMostrar(grid_tabla_, 73);
    } else {
        grid_tabla_.jqGrid({
            caption: "Estado Cuenta",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '195',
            autowidth: false,
            colNames: ['Cedula', 'Nombre Cliente', 'Negocio', 'Fecha Negocio', 'Estado Negocio', 'Convenio', 'Id Convenio', 'Tasa', 'Afiliado', 'Tipo Negocio', 'Cuotas', 'Valor Negocio', 'Valor Aval', 'Valor Negocio Padre', 'Valor Aval In', 'Saldo Cartera', 'Fianza', 'Liquidacion Detalle', 'Liquidacion'],
            colModel: [
                {name: 'cod_cli', index: 'cod_cli', width: 80, align: 'center'},
                {name: 'cliente', index: 'cliente', width: 250, align: 'left', hidden: false},
                {name: 'negasoc', index: 'negasoc', width: 80, align: 'center', hidden: false, key: true},
                {name: 'fecha_negocio', index: 'fecha_negocio', width: 80, align: 'center', hidden: false},
                {name: 'estado_neg', index: 'estado_neg', width: 100, align: 'left', hidden: true},
                {name: 'convenio', index: 'convenio', width: 180, align: 'left', hidden: true},
                {name: 'id_convenio', index: 'id_convenio', width: 100, align: 'center', hidden: true},
                {name: 'tasa_interes', index: 'tasa_interes', width: 60, align: 'center', hidden: false},
                {name: 'afiliado', index: 'afiliado', width: 100, align: 'left', hidden: true},
                {name: 'tipo_negocio', index: 'tipo_negocio', width: 180, align: 'left', hidden: true},
                {name: 'nro_docs', index: 'nro_docs', width: 100, align: 'center', hidden: true},
                {name: 'vr_negocio', index: 'vr_negocio', width: 80, align: 'right', sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_aval', index: 'valor_aval', width: 130, align: 'right', sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', hidden: true, formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_neg', index: 'valor_neg', width: 130, align: 'right', sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', hidden: true, formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valoraval', index: 'valoraval', width: 130, align: 'right', sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', hidden: true, formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'saldo_cartera', index: 'saldo_cartera', width: 80, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'fianza', index: 'fianza', width: 50, align: 'center', hidden: false},
                {name: 'liquidacion_detalle', index: 'generar', width: 100, align: 'center', hidden: true},
                {name: 'liquidacion', index: 'visto', width: 100, align: 'center', hidden: false}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            subGrid: true,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 73,
                    cliente: $('#cedula').val(),
                    negocio: $('#negocio').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var ids = grid_tabla_.jqGrid('getDataIDs'), fila;
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla_.jqGrid("getLocalRow", cl);
                    det = "<img src='/fintra/images/detalle.png' align='absbottom'  style='height:30px;width:31px;margin-left: 8px;padding: 3px;' name='liquidacion_detalle' id='liquidacion_detalle'  width='19' height='19' title ='Liquidacion Detalle'  onclick=\"liquidacionDetalle('" + cl + "','liquidacionDetalle');\">";
                    liq = "<img src='/fintra/images/liquidacion.png' align='absbottom'  style='height:36px;width:35px;margin-left: 8px;' name='liquidacion' id='liquidacion'  width='19' height='19' title ='Liquidacion'  onclick=\"liquidacionDetalle('" + cl + "','liquidacion');\">";
                    grid_tabla_.jqGrid('setRowData', ids[i], {liquidacion_detalle: det});
                    grid_tabla_.jqGrid('setRowData', ids[i], {liquidacion: liq});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
    if (true) {
        $("#tabla_estadoCuenta").setGridParam({
            subGridRowExpanded: function (subgrid_id, row_id) {
                myrow_id = row_id;
                subgrid_table_id = subgrid_id + "_t";
                var pager_id = "p_" + subgrid_table_id;
                $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                jQuery("#" + subgrid_table_id).jqGrid({
                    url: "./controller?estado=Admin&accion=Fintra&negocio=" + myrow_id,
                    datatype: "json",
                    colNames: ['Cliente', 'Tiepo Negocio', 'Negocio', 'Id Convenio', 'Convenio', 'fecha Negocio', 'Valor Negocio'],
                    colModel: [
                        {name: "cod_cli", index: "cod_cli", width: 90, align: "center", sortable: false},
                        {name: "tipo", index: "tipo", width: 80, align: "center", sortable: false, key: true},
                        {name: "cod_neg", index: "cod_neg", width: 90, align: "center", sortable: false},
                        {name: "id_convenio", index: "id_convenio", width: 80, align: "center", sortable: false, hidden: true},
                        {name: "convenio", index: "convenio", width: 80, align: "center", sortable: false},
                        {name: "fecha_negocio", index: "fecha_negocio", width: 80, align: "center", sortable: false},
                        {name: 'vr_negocio', index: 'vr_negocio', sortable: true, width: 95, align: 'right', search: true, sorttype: 'currency',
                            formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}}
                    ],
                    rowNum: 28,
                    sortname: 'num',
                    width: '1000',
                    height: '150',
                    pgtext: null,
                    pgbuttons: false,
                    subGrid: false,
                    jsonReader: {
                        root: "rows",
                        repeatitems: false,
                        id: "0"
                    },
                    ajaxGridOptions: {
                        data: {
                            opcion: 85
                        }
                    }, gridComplete: function () {

                    }
                });
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                cliente: $('#cedula').val(),
                negocio: $('#negocio').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function liquidacionDetalle(id, vista) {
    ultilidad = funcionFomurarios();
    var ventana;
    var ancho;
    var contenido;
    var mostrar = obtenerMostrarCampo(id);
    if (vista == 'liquidacionDetalle') {
        ventana = 'dialogMsjliquidacionDetalle';
        ancho = '1625';
        contenido = {
            "Salir": function () {
                $(this).dialog("close");
                ultilidad.limpiarCampos(ventana);
            }
        };
        llenarTablaInfoNegocios(id, vista);
        cargarDatosEstadoCuenta(mostrar);

    } else if (vista == 'liquidacion') {
        ventana = 'dialogMsjliquidacion';
        ancho = '850';
        contenido = {
            "Generar Estado Cuenta": function () {
                $('.ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable').hide();
                generarEstadoCuenta();
                $('.ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable').show();
            },
//            "Generar Cupon Pago": function () {
//                $(this).dialog("close");
//            },
            "Salir": function () {
                $(this).dialog("close");
                ultilidad.limpiarCampos(ventana);
            }
        };
        llenarTablaInfoNegocios(id, vista);
        cargarLiquidacion();
    }
    $("#" + ventana).dialog({
        width: ancho,
        height: '930',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: contenido
    });
}

function llenarTablaInfoNegocios(id, vista) {
    funcionFormatos = new formatos();
    var grid_tabla = jQuery("#tabla_estadoCuenta");
    var cliente = grid_tabla.getRowData(id).cliente;
    var negasoc = grid_tabla.getRowData(id).negasoc;
    var vr_negocio = grid_tabla.getRowData(id).valor_neg;
    var tasa_interes = grid_tabla.getRowData(id).tasa_interes;
    var nro_docs = grid_tabla.getRowData(id).nro_docs;
    var valor_aval = grid_tabla.getRowData(id).valoraval;

    if (vista == 'liquidacionDetalle') {
        $('#cliente').val(cliente);
        $('#negocio_').val(negasoc);
        $('#vlr_negocio').val(funcionFormatos.numberConComas(vr_negocio));
        $('#tasa').val(tasa_interes);
        $('#ncuotas').val(nro_docs);
        $('#vlr_aval').val(funcionFormatos.numberConComas(valor_aval));
    } else if (vista == 'liquidacion') {
        $('#cliente_').val(cliente);
        $('#negocioC_').val(negasoc);
        $('#vlr_negocio_').val(funcionFormatos.numberConComas(vr_negocio));
        $('#tasa_').val(tasa_interes);
        $('#ncuotas_').val(nro_docs);
        $('#vlr_aval_').val(funcionFormatos.numberConComas(valor_aval));
    }
}

function cargarDatosEstadoCuenta(mostrar) {
    $.ajax({
        async: false,
        url: "./controller?estado=Admin&accion=Fintra",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 74,
            negocio: $('#negocio_').val(),
            fecha_max: $('#fecha_max').val()
        },
        success: function (json) {
            console.log(json);
            cargarLiquidacionDetalle(mostrar, json);
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarLiquidacionDetalle(mostrar, jsonInfomacion) {
    $('#tabla_liquidacionDetalle').jqGrid('GridUnload');
    var grid_tabla_ = jQuery("#tabla_liquidacionDetalle");
    var data = {
        "page": "1",
        "records": "3",
        "rows": jsonInfomacion
    };
    grid_tabla_.jqGrid({
        caption: "Liquidacion Detalle",
        height: '195',
        autowidth: false,
        colNames: ['Factura', 'Fecha', 'Convenio', 'Cuota', 'Dias Mora', 'Saldo Inicial', 'Valor Cuota', 'Valor Saldo Cuota', 'Capital', 'Interes', 'Seguro', 'Comisión Ley mipyme', 'Cuota Administracion', 'Interes Mora', 'Gasto de Cobranza', 'Valor Saldo Global Cuota', 'Estado'],
        colModel: [
            {name: 'factura', index: 'factura', width: 100, align: 'center'},
            {name: 'fecha', index: 'fecha', width: 100, align: 'center'},
            {name: 'convenio', index: 'convenio', width: 250, align: 'left', hidden: true},
            {name: 'item', index: 'item', width: 100, align: 'center', hidden: false},
            {name: 'dias_mora', index: 'dias_mora', width: 100, align: 'center', hidden: false},
            {name: 'saldo_inicial', index: 'saldo_inicial', width: 130, align: 'right', hidden: false, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'valor_cuota', index: 'valor_cuota', width: 130, hidden: false, align: 'right', search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'valor_saldo_cuota', index: 'valor_saldo_cuota', width: 130, align: 'right', search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'capital', index: 'capital', width: 110, align: 'right', hidden: false, search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'interes', index: 'interes', width: 110, align: 'right', hidden: false, search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'seguro', index: 'seguro', width: 110, align: 'right', search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'mipyme', index: 'mipyme', width: 110, align: 'right', search: true, hidden: mostrar, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'cuota_manejo', index: 'cuota_manejo', width: 110, align: 'right', search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'interes_mora', index: 'interes_mora', width: 130, align: 'right', hidden: false, search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'gac', index: 'gac', width: 130, align: 'right', hidden: false, search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'valor_saldo_global_cuota', index: 'valor_saldo_global_cuota', width: 130, align: 'right', search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'estado', index: 'estado', width: 90, align: 'left', hidden: false}
        ],
        rownumbers: true,
        rownumWidth: 25,
        rowNum: 1000,
        rowTotal: 1000,
        loadonce: true,
        gridview: true,
        viewrecords: true,
        hidegrid: false,
        shrinkToFit: true,
        footerrow: false,
        pager: '#pager1',
        multiselect: false,
        multiboxonly: false,
        pgtext: null,
        pgbuttons: false,
        datatype: "jsonstring",
        datastr: data,
        jsonReader: {repeatitems: false},
        cmTemplate: {sortable: false},
        ajaxGridOptions: {
        },
        loadError: function (xhr, status, error) {
            mensajesDelSistema(error, 250, 150);
        }, loadComplete: function (id, rowid, json) {
            cargarDatosEstadoCuentaGeneral('tabla_liquidacionDetalle');
        },
        gridComplete: function (index) {

        },
        ondblClickRow: function (rowid, iRow, iCol, e) {
        }
    }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
    });
    jQuery("#tabla_liquidacionDetalle").navButtonAdd('#pager1', {
        caption: "Exportar",
        title: "Exportar Excel ",
        onClickButton: function () {
            exportarExcel();
        }
    });
}


function cargarDatosEstadoCuentaGeneral(tabla) {
    var negocio;
    if (tabla == 'tabla_liquidacionDetalle') {
        negocio = $('#negocio_').val();
    } else if (tabla == 'tabla_liquidacion') {
        negocio = $('#negocioC_').val();
    }
    $.ajax({
        async: false,
        url: "./controller?estado=Admin&accion=Fintra",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 74,
            negocio: negocio,
            fecha_max: $('#fecha_max').val(),
            general: 'general'
        },
        success: function (json) {
            console.log('-----------inicio---------');
            console.log(json);
            console.log('-----------fin------------');
            if (tabla == 'tabla_liquidacionDetalle') {
                console.log(json[0].cuotasvencidas);
                $('#cuotavencidas').val(funcionFormatos.numberConComas(json[0].cuotasvencidas));
                $('#deuda').val(funcionFormatos.numberConComas(json[0].valorcuotaactual));
                $('#capitalfuturo').val(funcionFormatos.numberConComas(json[0].capitalfuturo));
                $('#interesescapitalf').val(funcionFormatos.numberConComas(json[0].interescuotafutura));
                $('#mipyme').val(funcionFormatos.numberConComas(json[0].totalmipyme));
                $('#seguro').val(funcionFormatos.numberConComas(json[0].totalseguro));
                $('#intereses').val(funcionFormatos.numberConComas(json[0].totalixm));
                $('#gac').val(funcionFormatos.numberConComas(json[0].totalgastocobranza));
                $('#cuotaadminfutura').val(funcionFormatos.numberConComas(json[0].cuotaadminfuturo));
                $('#total').val(funcionFormatos.numberConComas(json[0].totalpagar));

            } else if (tabla == 'tabla_liquidacion') {
                $('#capital_').val(funcionFormatos.numberConComas(json[0].valor_saldo_capital));
                $('#interes_').val(funcionFormatos.numberConComas(json[0].valor_saldo_mi));
                $('#miPyme_').val(funcionFormatos.numberConComas(json[0].valor_saldo_ca));
                $('#cuotaAdmin_').val(funcionFormatos.numberConComas(json[0].valor_cm));
                $('#seguro_').val(funcionFormatos.numberConComas(json[0].valor_seguro));
                $('#interesMora_').val(funcionFormatos.numberConComas(json[0].ixm));
                $('#gac_').val(funcionFormatos.numberConComas(json[0].gac));
                $('#total_').val(funcionFormatos.numberConComas(json[0].suma_saldos));
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarLiquidacion() {
    var grid_tabla_ = jQuery("#tabla_liquidacion");
    if ($("#gview_tabla_liquidacion").length) {
        reloadGridMostrarLiquidacion(grid_tabla_, 74);
    } else {
        grid_tabla_.jqGrid({
            caption: "Liquidacion",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '195',
            width: 780,
            colNames: ['Factura', 'Fecha', 'Cuota', 'Dias Mora', 'Capital', 'Interes', 'Mi Pyme', 'Intermediación Financiera','Seguro', 'Capital Aval', 'Interes AVal', 'Saldo Cuota', 'Interes Mora', 'Gasto de Cobranza', 'Total Saldo', 'Estado'],
        colModel: [
            {name: 'documento', index: 'documento', width: 80, align: 'center'},
            {name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: 80, align: 'center'},
            {name: 'cuota', index: 'cuota', width: 50, align: 'center', hidden: false},
            {name: 'dias_mora', index: 'dias_mora', width: 60, align: 'center', hidden: false},
            {name: 'valor_saldo_capital', index: 'valor_saldo_capital', width: 80, align: 'right', hidden: false, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'valor_saldo_mi', index: 'valor_saldo_mi', width: 80, hidden: false, align: 'right', search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'valor_saldo_ca', index: 'valor_saldo_ca', width: 80, align: 'right', search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'valor_cm', index: 'valor_cm', width: 80, align: 'right', hidden: false, search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'valor_seguro', index: 'valor_seguro', width: 80, align: 'right', hidden: false, search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'valor_capital_aval', index: 'valor_capital_aval', width: 80, align: 'right', hidden: true, search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'valor_interes_aval', index: 'valor_interes_aval', width: 80, align: 'right',  hidden: true, search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'valor_saldo_cuota', index: 'valor_saldo_cuota', width: 80, align: 'right', search: true, hidden: false, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'ixm', index: 'ixm', width: 80, align: 'right', search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'gac', index: 'gac', width: 80, align: 'right', hidden: false, search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'suma_saldos', index: 'suma_saldos', width: 80, align: 'center', hidden: false, search: true, sorttype: 'currency',
                formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
            {name: 'estado', index: 'estado', width: 90, align: 'left', hidden: false}
        ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            restoreAfterError: true,
            pager: '#pager2',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 74,
                    negocio: $('#negocioC_').val(),
                    fecha_max: $('#fecha_max').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {
                cargarDatosEstadoCuentaGeneral('tabla_liquidacion');
            },
            gridComplete: function () {
                var valor_saldo_capital = jQuery("#tabla_liquidacion").jqGrid('getCol', 'valor_saldo_capital', false, 'sum');
                var valor_saldo_mi = jQuery("#tabla_liquidacion").jqGrid('getCol', 'valor_saldo_mi', false, 'sum');
                var valor_saldo_ca = jQuery("#tabla_liquidacion").jqGrid('getCol', 'valor_saldo_ca', false, 'sum');
                var valor_cm = jQuery("#tabla_liquidacion").jqGrid('getCol', 'valor_cm', false, 'sum');
                var IxM = jQuery("#tabla_liquidacion").jqGrid('getCol', 'ixm', false, 'sum');
                var GaC = jQuery("#tabla_liquidacion").jqGrid('getCol', 'gac', false, 'sum');
                var valor_seguro = jQuery("#tabla_liquidacion").jqGrid('getCol', 'valor_seguro', false, 'sum');
                $("#tabla_liquidacion").jqGrid('footerData', 'set', 
                {
                 valor_saldo_capital: valor_saldo_capital,
                 valor_saldo_mi:valor_saldo_mi,               
                 valor_saldo_ca:valor_saldo_ca,               
                 valor_cm:valor_cm,  
                 valor_seguro:valor_seguro,
                 ixm:IxM,               
                 gac:GaC            
                             
                });

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            }
        }).navGrid("#pager2", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridMostrarLiquidacion(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                negocio: $('#negocioC_').val(),
                fecha_max: $('#fecha_max').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function generarEstadoCuenta() {
    ultilidad = funcionFomurarios();
    var informacionNegocio = ultilidad.informacion('infoNegocio');
    var informacionGeneral = ultilidad.informacion('infoGeneral');
    var informacionDetalle = $("#tabla_liquidacion").jqGrid('getRowData');
    $.ajax({
        async: false,
        url: "./controller?estado=Admin&accion=Fintra",
        type: 'POST',
        dataType: 'text',
        data: {
            opcion: 76,
            informacion: JSON.stringify({informacionNegocio: informacionNegocio, informacionGeneral: informacionGeneral, informacionDetalle: informacionDetalle})
        },
        success: function (json) {
            console.log(json);
            if (json.trim() === 'OK') {
                generarExtractoEstadoCuenta();
            } else {
                mensajesDelSistema('Ocurrio un error', '300', 'auto', false);
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

var convenios_;
function obtenerMostrarCampo(id) {
    var grid_tabla = jQuery("#tabla_estadoCuenta");
    var id_convenio = grid_tabla.getRowData(id).id_convenio;
    var longitud = Object.keys(convenios_).length;
    var convenioPertenece;
    var mostrar;
    for (var i = 0; i < longitud; i++) {
        var esta = (convenios_[Object.keys(convenios_)[i]]);
        if (esta.includes(id_convenio)) {
            convenioPertenece = Object.keys(convenios_)[i];
            console.log(Object.keys(convenios_)[i]);
            break;
        }
    }
    switch (convenioPertenece) {
        case 'CONSUMO':
            mostrar = true;
            $('#mipyme_').hide();
//            $('#labeldeuda_').text('Deuda Actual mas Aval');
            break;
        case 'EDUCATIVO':
            mostrar = true;
            $('#mipyme_').hide();
//            $('#labeldeuda_').text('Deuda Actual mas Aval');
            break;
        case 'LIBRANZA':
            mostrar = true;
            $('#mipyme_').hide();
//            $('#labeldeuda_').text('Deuda Actual mas Aval');
            break;
        case 'MICROCREDITO':
            mostrar = false;
            $('#mipyme_').show();
//            $('#labeldeuda_').text('Deuda Actual');
            break;
    }
    return mostrar;
}

function convenios() {
    $.ajax({
        async: false,
        url: "./controller?estado=Admin&accion=Fintra",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 91
        },
        success: function (json) {
            console.log(json);
            var llaves = [];
            var llaveDef = {};
            for (var i in json) {
                var val = json[i].convenio;
                llaves.push(val);
            }
            var jsontemp = {};
            llaves = llaves.filter(function (datos) {
                var exists = !jsontemp[datos] || false;
                jsontemp[datos] = true;
                return exists;
            });
            for (var i in llaves) {
                var contenido = [];
                for (var j in json) {
                    if (json[j].convenio == llaves[i]) {
                        //console.log(llaves[i] + ': ' + json[j].id_convenio);
                        contenido.push(json[j].id_convenio);
                    }
                }
                llaveDef[llaves[i]] = contenido;
            }
            console.log(llaveDef);
            convenios_ = llaveDef;
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function generarExtractoEstadoCuenta() {
    ultilidad = funcionFomurarios();
    var informacionNegocio = ultilidad.informacion('infoNegocio');
    $.ajax({
        async: false,
        url: "./controller?estado=Admin&accion=Fintra",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 93,
            informacion: JSON.stringify({informacionNegocio: informacionNegocio, fecha: $('#fecha_max').val()})
        },
        success: function (json) {
            if (json.respuesta === 'Guardado') {
                mensajesDelSistema('Exito al generar Pdf y el Extracto', '300', 'auto', false);
            } else {
                mensajesDelSistema('Ocurrio un error', '300', 'auto', false);
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            $("#dialogLoading").dialog('close');
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function exportarExcel() {
    var exportar = new exportarDatos();
    exportar.exportarExcelJqgrid('tabla_liquidacionDetalle', 1, 'Estado_Cuenta_Detalle');
}

