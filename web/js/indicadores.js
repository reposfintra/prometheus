var BASEURL = '';
var CONTROLLER = '';

function obtenerImagen(ruta){
	return ruta + document.formularioNuevo.Opcion.value + ".gif";
}        
function _onmousemove(item) { item.className='fila';   }
function _onmouseout (item) { item.className='fila'; }
function addOption(Comb,valor,texto){
		var Ele = document.createElement("OPTION");
		Ele.value=valor;
		Ele.text=texto;
		Comb.add(Ele);
}    
function loadCombo(datos, cmb){
	cmb.length = 0;
	for (i=0;i<datos.length;i++){
		var dat = datos[i].split(SeparadorJS);
		addOption(cmb, dat[0], dat[1]);
	}
}
function loadCombo2(datos, datosA ,cmbA, cmbNA){
	cmbA.length = 0;
	cmbNA.length = 0;
	for (i=0;i<datos.length;i++){
		var dat = datos[i].split(SeparadorJS);                
		if (datosA.indexOf(dat[0])==-1)
			addOption(cmbNA, dat[0], dat[1]);
		else
			addOption(cmbA , dat[0], dat[1]);
	}
}        
function move(cmbO, cmbD){
   for (i=cmbO.length-1; i>=0 ;i--)
	if (cmbO[i].selected){
	   addOption(cmbD, cmbO[i].value, cmbO[i].text)
	   cmbO.remove(i);
	}
  // order(cmbD);
}
function moveAll(cmbO, cmbD){
   for (i=cmbO.length-1; i>=0 ;i--){
	   addOption(cmbD, cmbO[i].value, cmbO[i].text)
	   cmbO.remove(i);
   }
}
function order(cmb){
   for (i=0; i<cmb.length;i++)
	 for (j=i+1; j<cmb.length;j++)
		if (cmb[i].text > cmb[j].text){
			var temp = document.createElement("OPTION");
			temp.value = cmb[i].value;
			temp.text  = cmb[i].text;
			cmb[i].value = cmb[j].value;
			cmb[i].text  = cmb[j].text;
			cmb[j].value = temp.value;
			cmb[j].text  = temp.text;
		}
}
function selectAll(cmb){
	for (i=0;i<cmb.length;i++)
	  cmb[i].selected = true;
}

function validarFormulario (tform){
	for (i = 0; i < tform.elements.length; i++){
            if (tform.elements[i].value == "" && (tform.elements[i].type == "text" || tform.elements[i].type == "select-one") ){
                    tform.elements[i].focus();
                    alert('No se puede procesar la informaci�n. Campo Vac�o.');
                    return false;	
            }
			
    }
	
	if( parseInt(tform.porcent.value)<0 ||  parseInt(tform.porcent.value)> 100 ){
     	tform.porcent.focus();
        alert('No se puede procesar la informaci�n. El valor del porcentaje debe estar entre 0 y 100.');
        return false;	
    }
	
	if( parseInt(tform.valor.value)<parseInt(tform.vlr_min.value) ||  parseInt(tform.valor.value)> parseInt(tform.vlr_max.value) ){
     	tform.valor.focus();
        alert('No se puede procesar la informaci�n. El valor debe estar entre Valor M�nimo y Valor M�ximo digitado.');
        return false;	
    }
	//alert('............. OPCION' + tform.Opcion.value);
	if ( tform.Opcion.value != "modificar") {
		var list = parseInt(tform.listaSize.value);//alert(tform.listaSize.value + '.... lista: ' + list)
		for( i=0; i<list; i++){
			var op = eval('op' + i);
			if( tform.descripcion.value == op.descripcion ){
				tform.descripcion.focus();
				alert('No se puede procesar la informaci�n. La descripci�n del indicador ya existe.');
				return false;	
			}
		}
	}
	
	return true;	
}        
function selAll(){
	for(i=0;i<FormularioListado.length;i++)
	   FormularioListado.elements[i].checked=FormularioListado.All.checked;
}
function actAll(){
	FormularioListado.All.checked = true;
	for(i=0;i<FormularioListado.length;i++)	
	  if (FormularioListado.elements[i].type=='checkbox' && !FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All'){
		  FormularioListado.All.checked = false;
		  break;
	  }
} 
function validarListado(form){
	for(i=0;i<FormularioListado.length;i++)	
		if (FormularioListado.elements[i].type=='checkbox' && FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All')
			return true;
	alert('Por favor seleccione un item para poder continuar');
	return false;
}   

function objetoOpcion(id, nivel, idFolder, descripcion, vlr_min, vlr_max, valor, porcentaje, metodo, formula, ejecucion, idpadre){
	this.id = id;
	this.idFolder = idFolder;
	this.nivel = nivel;
	this.descripcion = descripcion;
	this.vlr_min = vlr_min;
	this.vlr_max = vlr_max;
	this.valor = valor;
	this.porcentaje = porcentaje;
	this.metodo = metodo;
	this.formula = formula;
	this.ejecucion = ejecucion;
	this.idpadre = idpadre;
	return this;
}

function edicion (idObj, edit){
	var op = eval('op'+idObj);
	with(formularioNuevo){
	   Id.value = op.id;
	   descripcion.value = op.descripcion;
	   idFolder.value = op.idFolder;
	   valor.value = op.valor;
	   vlr_min.value = op.vlr_min;    
	   vlr_max.value = op.vlr_max;
	   porcent.value = op.porcentaje;
	   metodo.value = op.metodo;
	   formula.value = op.formula;
	   ejecucion.value = op.ejecucion;
	   nivel.value = op.nivel;
	   
	   idFolder[(op.idFolder=='N'?1:0)].checked = true;   
	   
	   if (edit){
		   movera.disabled=false;
		   moveradestino.disabled=false;
		   Opcion.value='modificar';
		   botonVariable.src = BASEURL+"/images/botones/modificar.gif";			   		   
		   if(op.idFolder=='Y'){
			  idFolder1.disabled = true;
			  idFolder2.disabled = true;
		   }
		   index.value = idObj;
	   }
	   else{
		   movera.disabled=true;
		   moveradestino.disabled=true;
		   Opcion.value='agregar';
		   botonVariable.src = BASEURL+"/images/botones/agregar.gif";	   
		   idFolder1.disabled = false;
		   idFolder2.disabled = false;
		   index.value = '';		   
	   }
	   
	}
	formNuevo.style.visibility='visible';	
}

function goYaNIN (idObj){
  try{
	var op = eval('op'+idObj);
	var url = CONTROLLER + '?estado=Indicadores&accion=Entrar&pagina=indicadoresindex.jsp&carpeta=jsp/ctrlgestion/tblindicadores&Opcion=cargarvistafolder&index=' + idObj +'&codigo='+ op.id + '&idpadre=' + op.idpadre;
	//alert('.............. URL : ' + url);
	document.location.href = url;
  }catch(e){}
}

function moveradest(form){            
	if(formularioNuevo.mover.value=='false' || formularioNuevo.mover.value==''){                
		formularioNuevo.mover.value='true';                
	}
	else{
		formularioNuevo.mover.value='false';                
	}
}
function eliminar(form){
	FormularioListado.Opcion.value='Eliminar';
	FormularioListado.submit();
}
function botonAgregar(form){	
	form.movera.disabled=true;
	form.moveradestino.disabled=true;
	formNuevo.style.visibility='visible';
	form.Opcion.value='agregar';
	
}


