/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    validarPermisos();

    $("#aprobar").click(function () {
        AprobarExtracto();

    });

    $("#imprimir").click(function () {
        if ($("#estado").val() === "S") {
            imprimirExtracto();
        } else {
            mensajesDelSistema("Bebe aprobar la reestructuracion, para imprimir", '290', '150');
        }
    });
    
     $("#rechazar").click(function () {
         
         rechazarExtracto();
 
    });


});

function validarPermisos() {
    var salida;
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructurar&accion=Fenalco",
        dataType: 'json',
        data: {
            opcion: 12
        },
        success: function (json) {

            if (json.respuesta === 'T') {//todos
                buscarNegociosCliente(json.respuesta);
                $("#permiso").val("T");
            } else if (json.respuesta === "A") {//solo aprobar
                $("#permiso").val("A");
                buscarNegociosCliente(json.respuesta);
                $("#imprimir").hide();
                $('#aprobar').css({'margin': '-30px 86px 0px 5px'});
                $('#rechazar').css({'margin': '-30px 0px 0px 5px'});
            } else if (json.respuesta === "I") {//solo imprimir
                $("#permiso").val("I");
                $("#aprobar").hide();
                $("#rechazar").hide();
                $("#estado").val("S");
                buscarNegociosCliente(json.respuesta);
            } else if (json.respuesta === "F") {//no encontrado
                mensajesDelSistema("No se encontraron permisos para su usuario", '290', '150');
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function AprobarExtracto() {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructurar&accion=Fenalco",
        dataType: 'json',
        data: {
            opcion: 13,
            idRop: $("#id_rop").val()
        },
        success: function (json) {

            if (json.respuesta === 'OK') {//todos
                mensajesDelSistema("Se ha aprobado la reestructuracion de forma exitosa", "280", "150");
                $("#estado").val("S");
                validarPermisos();
                if ($("#permiso").val() !== "T") {
                    $("#ventanaIzq").hide();
                }
            } else {
                mensajesDelSistema("Error al aprobar la reestructuracion", "280", "150");
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function imprimirExtracto() {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructurar&accion=Fenalco",
        dataType: 'json',
        data: {
            opcion: 14,
            idRop: $("#id_rop").val()
        },
        success: function (json) {

            if (json.respuesta === 'OK') {//todos
                mensajesDelSistema2("Se ha impreso su cupon de pago", '320', '170');
            } else {
                mensajesDelSistema("No se pudo rechazar la reestructuracion", '300', '180');
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}


function rechazarExtracto(){

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructurar&accion=Fenalco",
        dataType: 'json',
        data: {
            opcion: 17,
            idRop: $("#id_rop").val()
        },
        success: function (json) {

            if (json.respuesta === 'OK') {//todos
                mensajesDelSistema2("Se ha rechazado la solicitud de reestructuracion", '320', '170');
            } else {
                mensajesDelSistema("No se pudo rechazar la reestructuracion", '300', '180');
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}



function buscarNegociosCliente(aprobado) {

    var grid_listar_negocios = jQuery("#tabla_negocios");

    if ($("#gview_tabla_negocios").length) {
        reloadGridListarNegocios(grid_listar_negocios, aprobado);
    } else {

        grid_listar_negocios.jqGrid({
            caption: "Negocios",
            url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=9&apro=" + aprobado,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: '800',
            width: '930',
            colNames: ['Negocio', 'Id Rop', 'Nro Extracto', 'Cedula', 'Nombre cliente', 'Aprobado', 'Usuario', 'Acciones'],
            colModel: [
                {name: 'valor_01', index: 'valor_01', width: 70, align: 'right'},
                {name: 'valor_07', index: 'valor_02', sortable: true, width: 80, align: 'center', key: true},
                {name: 'valor_02', index: 'valor_02', sortable: true, width: 100, align: 'center'},
                {name: 'valor_03', index: 'valor_03', sortable: true, width: 80, align: 'center'},
                {name: 'valor_04', index: 'valor_04', width: 250, align: 'center', hidden: false},
                {name: 'valor_05', index: 'valor_05', sortable: false, width: 90, align: 'center'},
                {name: 'valor_06', index: 'valor_06', width: 100, align: 'center', hidden: false},
                {name: 'acciones', index: 'acciones', sortable: false, width: 60, align: 'right'}
            ],
            rowNum: 500,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            rownumbers: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            multiselect: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, loadComplete: function () {

                if (grid_listar_negocios.jqGrid('getGridParam', 'records') <= 0) {
                    mensajesDelSistema("Lo sentimos ne se encontraron resultados.<br/> Verifique los datos de entrada.", "330", "150");
                    grid_listar_negocios.jqGrid("clearGridData", true).trigger("reloadGrid");
                }

            }, gridComplete: function () {
                var ids = grid_listar_negocios.jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    be = "<input style='height:22px;width:30px;' type='button' value='Ver' onclick=\"ver_condiciones('" + cl + "');\">";
                    se = "<input style='height:22px;width:30px;' type='button' value='Liq' onclick=\"ver_liquidacion('" + cl + "');\">";
                    grid_listar_negocios.jqGrid('setRowData', ids[i], {acciones: be + se});
                }
            }, loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });
    }

}

function reloadGridListarNegocios(grid_listar_negocios, aprobado) {
    grid_listar_negocios.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=9&apro=" + aprobado
    });

    grid_listar_negocios.trigger("reloadGrid");
}



function ver_condiciones(idRows) {
    $("#ventanaIzq").show();
    $("#id_rop").val(idRows);

    var grid_listar_saldo = jQuery("#tabla_saldos");

    if ($("#gview_tabla_saldos").length) {
        reloadGridListarSaldo(grid_listar_saldo, idRows);
    } else {

        grid_listar_saldo.jqGrid({
            caption: "Resumen",
            url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=10&idRop=" + idRows,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: 'auto',
            width: 'auto',
            colNames: ['Negocio', 'Estado', 'Tipo', 'Capital', 'Interes', 'IntxMora', 'GaC', 'Sub Total', 'Dcto capital', 'Dcto interes', 'Dcto intxMora', 'Dcto gac', 'Total Dcto', 'Total Items'],
            colModel: [
                {name: 'Negocio', index: 'Negocio', width: 70, align: 'right'},
                {name: 'Estado', index: 'Estado', sortable: true, width: 80, align: 'center'},
                {name: 'Tipo_neg', index: 'Tipo_neg', sortable: true, width: 70, align: 'center'},
                {name: 'Capital', index: 'Capital', width: 90, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'Interes', index: 'Interes', width: 90, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'intxMora', index: 'intxMora', width: 90, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'GaC', index: 'GaC', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'Sub_Total', index: 'Sub_Total', width: 90, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'Dcto_capital', index: 'Dcto_capital', width: 90, align: 'right', sorttype: ''},
                {name: 'Dcto_interes', index: 'Dcto_interes', width: 90, align: 'right', sorttype: ''},
                {name: 'Dcto_intxMora', index: 'Dcto_intxMora', width: 90, align: 'right', sorttype: ''},
                {name: 'Dcto_gac', index: 'Dcto_gac', width: 90, align: 'right', sorttype: ''},
                {name: 'Total_Dcto', index: 'Total_Dcto', width: 90, align: 'right', sorttype: '',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'Total_Items', index: 'Total_Items', width: 90, align: 'right', sorttype: '',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}

            ],
            rowNum: 500,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            rownumbers: false,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            multiselect: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, loadComplete: function () {
                // buscarPagosIniciales(idRows);
                if (grid_listar_saldo.jqGrid('getGridParam', 'records') > 0) {
                    cacularTotalesListarSaldos(grid_listar_saldo);
                    buscarPagosIniciales(idRows);
                } else {
                    $("#ventanaIzq").hide();
                    mensajesDelSistema("Lo sentimos ne se encontraron resultados.<br/> Verifique los datos de entrada.", "330", "150");
                    grid_listar_saldo.jqGrid("clearGridData", true).trigger("reloadGrid");
                }

            }, loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });
    }
}

function reloadGridListarSaldo(grid_listar_saldo, idRows) {
    grid_listar_saldo.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=10&idRop=" + idRows,
        ajaxGridOptions: {async: false}
    });

    grid_listar_saldo.trigger("reloadGrid");

    reloadGridListarInicial(jQuery("#tabla_inicial"), idRows);
}

function cacularTotalesListarSaldos(grid_listar_saldo) {

    var Sub_Total = grid_listar_saldo.jqGrid('getCol', 'Sub_Total', false, 'sum');
    var Total_Items = grid_listar_saldo.jqGrid('getCol', 'Total_Items', false, 'sum');
    grid_listar_saldo.jqGrid('footerData', 'set', {
        Sub_Total: Sub_Total,
        Total_Items: Total_Items

    });

}

function buscarPagosIniciales(idRows) {


    var grid_listar_inicial = jQuery("#tabla_inicial");

    if ($("#gview_tabla_inicial").length) {
        reloadGridListarInicial(grid_listar_inicial, idRows);
    } else {

        grid_listar_inicial.jqGrid({
            caption: "Pago Inicial",
            url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=11&idRop=" + idRows,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: 'auto',
            width: 'auto',
            colNames: ['Negocio', 'Tipo', 'Capital', 'Interes', 'IntxMora', 'GaC', 'Total', 'pctPagar', 'Valor Pagar', 'Saldo Vencido', 'Saldo Corriente'],
            colModel: [
                {name: 'negocio', index: 'negocio', width: 70, align: 'right'},
                {name: 'Tipo_neg', index: 'Tipo_neg', sortable: true, width: 80, align: 'center'},
                {name: 'capital', index: 'capital', width: 90, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes', index: 'interes', width: 90, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'intXmora', index: 'intXmora', width: 90, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'GAC', index: 'GAC', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'Total', index: 'Total', width: 90, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'pctPagar', index: 'pctPagar', width: 90, align: 'right', sorttype: ''},
                {name: 'ValorPagar', index: 'ValorPagar', width: 90, align: 'right', sorttype: '',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'SaldoVencido', index: 'SaldoVencido', width: 110, align: 'right', sorttype: '',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'SaldoCorriente', index: 'SaldoCorriente', width: 110, align: 'right', sorttype: '',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}

            ],
            rowNum: 500,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            rownumbers: false,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            multiselect: false,
            ajaxGridOptions: {async: false},
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            }, loadComplete: function () {

                if (grid_listar_inicial.jqGrid('getGridParam', 'records') > 0) {
                    cacularTotalesListarInicial(grid_listar_inicial);
                } else {
                    mensajesDelSistema("Lo sentimos ne se encontraron resultados.<br/> Verifique los datos de entrada.", "330", "150");
                    grid_listar_inicial.jqGrid("clearGridData", true).trigger("reloadGrid");
                }

            }, loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });
    }

}


function reloadGridListarInicial(grid_listar_inicial, idRows) {
    grid_listar_inicial.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=11&idRop=" + idRows,
        ajaxGridOptions: {async: false}
    });

    grid_listar_inicial.trigger("reloadGrid");
}

function cacularTotalesListarInicial(grid_listar_inicial) {

    var Total = grid_listar_inicial.jqGrid('getCol', 'Total', false, 'sum');
    var ValorPagar = grid_listar_inicial.jqGrid('getCol', 'ValorPagar', false, 'sum');
    var SaldoVencido = grid_listar_inicial.jqGrid('getCol', 'SaldoVencido', false, 'sum');
    var SaldoCorriente = grid_listar_inicial.jqGrid('getCol', 'SaldoCorriente', false, 'sum');
    var totalReestructuracion = parseInt(SaldoVencido) + parseInt(SaldoCorriente);
    grid_listar_inicial.jqGrid('footerData', 'set', {
        Total: Total,
        ValorPagar: ValorPagar,
        SaldoVencido: SaldoVencido,
        SaldoCorriente: SaldoCorriente

    });
    $("#totalReestructuracion").html("$ " + numberConComas(totalReestructuracion));
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function ver_liquidacion(idRows) {
    
    //buscamos datos cabecera
    cargarCabecera(idRows);
   
//    $("#interes").html(fila['tasa']);
//    $("#fneg").html(fila['fecha_neg']);
//    $("#cuota1").html(fila['nodocs']);
//    $("#cat").html(fila['porcentaje_cat']);
//    $("#vlr_seguro").html(numberConComas(fila['valor_seguro']));
//    $("#vlr_negocio").html(numberConComas(fila['vr_negocio']));

//    cargarLiquidacion(idRows);

    $("#ver_liquidacion").dialog({
        width: 1022,
        height: 700,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n 
            "Salir": function() {
               $(this).dialog("close");
            }
        }
    });


}

function cargarCabecera(idRop){
    
     $.ajax({
        type: 'POST',
        url: "./controller?estado=Reestructurar&accion=Fenalco",
        dataType: 'json',
        data: {
            opcion: 15,
            idRop: idRop
        },
        success: function (json) {
            if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                 //cargamos los datos desde el json.
                $("#interes").html(json.tasa);
                $("#fneg").html(json.fecha_pr_cuota);
                $("#cuota1").html(json.numcuota);
                $("#remesa").html(numberConComas(json.remesa));
                $("#vlr_seguro").html(numberConComas(json.seguro));
                $("#vlr_negocio").html(numberConComas(json.capital));
                
                cargarLiquidador(idRop);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarLiquidador(idRop) {
    var grid_simulador_credito = jQuery("#tabla_liquidador_neg");

    if ($("#gview_tabla_liquidador_neg").length) {
        reloadGridLiquidadorFenalco(grid_simulador_credito,idRop);
    } else {

        grid_simulador_credito.jqGrid({
            caption: "Liquidacion Negocio",
            url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=16&idRop=" + idRop,
            //editurl: "./controller?estado=RegistrarIngreso&accion=Banco&op=8",
            mtype: "POST",
            datatype: "json",
            height: '400',
            width: '995',
            colNames: ['Fecha','Dias', 'Cuota', 'Saldo Inicial', 'Capital', 'Interes', 'Custodia', 'Seguro', 'Remesa','Valor Cuota', 'Saldo Final'],
            colModel: [
                {name: 'fecha', index: 'fecha', width: 80, align: 'center'},
                {name: 'dias', index: 'dias', width: 80, align: 'center',hidden: true},
                {name: 'item', index: 'item', sortable: true, width: 50, align: 'center', key: true},
                {name: 'saldo_inicial', index: 'saldo_inicial', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'capital', index: 'capital', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes', index: 'interes', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'custodia', index: 'custodia', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'seguro', index: 'seguro', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'remesa', index: 'remesa', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor', index: 'valor', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'saldo_final', index: 'saldo_final', sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            // pager: ('#page_detalles_cartera'),
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            userDataOnFooter: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function() {

                cacularTotalesLiquidadorFenalco(grid_simulador_credito);
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });
    }
}

function  reloadGridLiquidadorFenalco(grid_simulador_credito,idRop){
    grid_simulador_credito.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Reestructurar&accion=Fenalco&opcion=16&idRop=" + idRop
    });
    grid_simulador_credito.trigger("reloadGrid");
}

function  cacularTotalesLiquidadorFenalco(grid_simulador_credito) {

    var capital = grid_simulador_credito.jqGrid('getCol', 'capital', false, 'sum');
    var interes = grid_simulador_credito.jqGrid('getCol', 'interes', false, 'sum');
    var custodia = grid_simulador_credito.jqGrid('getCol', 'custodia', false, 'sum');
    var seguro = grid_simulador_credito.jqGrid('getCol', 'seguro', false, 'sum');
    var remesa = grid_simulador_credito.jqGrid('getCol', 'remesa', false, 'sum');
    var valor = grid_simulador_credito.jqGrid('getCol', 'valor', false, 'sum');
 
    grid_simulador_credito.jqGrid('footerData', 'set', {
        item: 'Total:',
        capital: capital,
        interes: interes,
        custodia: custodia,
        seguro: seguro,
        remesa: remesa,        
        valor: valor

    });

}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function mensajesDelSistema2(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
                location.reload();
            }
        }
    });

}