var controller;
var controlleropav;

function varcontr(contr){
    controller=contr;
}

function varcontropav(contr){
    controlleropav=contr;
}


function busqueda(id_field){
    var dato = $(id_field).value;
    if(dato==''){
        alert('Para consultar debe escribir algo');
    }else{        
        var url = controlleropav+"?estado=GestionSolicitud&accion=AiresAAE";
        var p = "opcion=0&dato="+dato+"&campo="+id_field;
        new Ajax.Request(url, {
            parameters: p,
            method: 'post',
            onComplete:  function (resp){
                var texto = resp.responseText;
                var array = texto.split(";_;");
                if(array.length==1){
                    alert(array[0]);
                }else{                    
                    if(id_field=="nic"){
                        document.getElementById('titular').value = array[1];
                        document.getElementById('dir_nat').value = array[2];
                        document.getElementById('tel_tit').value = array[3];
                        document.getElementById('estr_nat').value = array[4];
                        document.getElementById('puntaje').value = array[5];
                    }
                    if(id_field=="id"){
                        document.getElementById('pr_apellido_nat').value = array[0];
                        document.getElementById('seg_apellido_nat').value = array[1];
                        document.getElementById('pr_nombre_nat').value = array[2];
                        document.getElementById('seg_nombre_nat').value = array[3];
                        document.getElementById('razon_social').value = array[4];
                        document.getElementById('tipo_p').value = array[5]=="NIT"?"juridica":"natural";
                        document.getElementById('id').value = array[6];
                        document.getElementById('dir_nat').value = array[7];
                        document.getElementById('tel_nat').value = array[8];
                        document.getElementById('f_nac_nat').value = array[9];
                        document.getElementById('est_civil_nat').value = array[10];
                        document.getElementById('dep_nat').value = array[12];
                        cargarCiudades('dep_nat', 'ciu_nat',array[11]);
                        document.getElementById('estr_nat').value = array[13];
                        document.getElementById('tipo_viv_nat').value = array[14];
                        document.getElementById('rep_legal').value = array[15];
                        document.getElementById('emp_sal_con_nat').value = array[16];
                        document.getElementById('sal_nat').value = array[17];
                        document.getElementById('otros_nat').value = array[18];
                        document.getElementById('arr_nat').value = array[19];
                    }
                    tipopersona();
                }            
            }
        } );
    }
}

function soloNumeros(id) {
    var valor = document.getElementById(id).value;
    valor =  valor.replace(/[^0-9^.]+/gi,"");
    document.getElementById(id).value = valor;
}

function tipopersona() {
    var valor = document.getElementById("tipo_p").value;
    if(valor=="natural"){
        $('natural').style.display= "table-row";
        $('juridica').style.display= "none";
        $('natural').style.visibility= "visible";
        $('juridica').style.visibility= "hidden";
       
    }else{
        
        $('natural').style.display= "none";
        $('juridica').style.display= "table-row";

        $('natural').style.visibility= "hidden";
        $('juridica').style.visibility= "visible";
        

    }
}

function cargarCiudades(id_dept, id_ciud){
    var dept = $(id_dept).value;
    var url = controlleropav+"?estado=GestionSolicitud&accion=AiresAAE";
    var p = "opcion=4&dept="+dept+"&ciu="+id_ciud;
    new Ajax.Request(url, {
        parameters: p,
        asynchronous: false,
        method: 'post',
        onComplete:  function (resp){
            document.getElementById("d_"+id_ciud).innerHTML = resp.responseText;           
        }
    });

}

function cargarHoras(){
    var fecha=$("f_vis_plan").value;
    var contratista=$("instalador").value;
    var url = controlleropav+"?estado=GestionSolicitud&accion=AiresAAE";
    var p = "opcion=5&fecha="+fecha+"&instalador="+contratista;
    new Ajax.Request(url, {
        parameters: p,
        asynchronous: false,
        method: 'post',
        onComplete:  function (resp){
            document.getElementById("d_hora").innerHTML = resp.responseText;
        }
    });

}

function getPrecioVenta(campo, i){
    var tipo_solicitud = $("tipo_solicitud").value;
    if(tipo_solicitud!=''){
        var material="";
        var indice="";
        var url="";
        var p="";
        if (i === undefined) {
            for (j = 0; j <= fily; j++){
                 if($('cantidad'+j).value==''){
                    $('cantidad'+j).value=1;
                 }
                material = $(campo+j).value;
                indice = $(campo+j).selectedIndex;
                $('nmb_'+campo+j).value=$(campo+j).options[indice].text ;
                url = controlleropav+"?estado=GestionSolicitud&accion=AiresAAE";
                p = "opcion=3&material="+material+"&tipo_solicitud="+tipo_solicitud;
                new Ajax.Request(url, {
                    parameters: p,
                    asynchronous: false,
                    method: 'post',
                    onComplete:  function (resp){
                        $('valor_'+campo+j).value=dar_formato(parseFloat(resp.responseText)*$('cantidad'+j).value);
                        valorFinanciado(j);
                    }
                });

            }
        }else{
            if($('cantidad'+i).value==''){
                    $('cantidad'+i).value=1;
                 }
            material = $(campo+i).value;
            indice = $(campo+i).selectedIndex;
            $('nmb_'+campo+i).value=$(campo+i).options[indice].text ;
            url = controlleropav+"?estado=GestionSolicitud&accion=AiresAAE";
            p = "opcion=3&material="+material+"&tipo_solicitud="+tipo_solicitud;
            new Ajax.Request(url, {
                parameters: p,
                asynchronous: false,
                method: 'post',
                onComplete:  function (resp){
                    $('valor_'+campo+i).value=dar_formato(parseFloat(resp.responseText)*$('cantidad'+i).value);
                    valorFinanciado(i);
                }
            });
        }
    }else{
        for (j = 0; j <= fily; j++){  
            $(campo+j).value="";
            $('nmb_'+campo+j).value="";
            $("valor_"+campo+j).value="0.0";
            $('valor_solicitado').value="0.0";
            alert("Seleccione un Tipo de solicitud");
        }
    }
}

function valorFinanciado(){
    var plazo = $("plazo").value;
    var equipo = 0;
    var instalacion=0;
    for (j = 0; j <= fily; j++){
        equipo += parseFloat($('valor_equipo'+j).value.replace(/,/g, ''));
        instalacion+=parseFloat($('valor_instalacion'+j).value.replace(/,/g, ''));
    }
    var url = controlleropav+"?estado=GestionSolicitud&accion=AiresAAE";
    var p = "opcion=7&equipo="+equipo+"&instalacion="+instalacion+"&plazo="+plazo;
    new Ajax.Request(url, {
        parameters: p,
        asynchronous: false,
        method: 'post',
        onComplete:  function (resp){
            $('valor_solicitado').value=resp.responseText;
            $('cuota').value=dar_formato(parseFloat(resp.responseText.replace(/,/g, ''))/plazo);
            $('valor_sin_fin').value=dar_formato(equipo+instalacion);
        }
    });
    
}

function cargarInstalaciones(i){
    var tipo_solicitud = $("tipo_solicitud").value;
    if(tipo_solicitud!=''){
        var equipo = $("equipo"+i).value;
         for (j = 0; j <= fily; j++){
          if (i!=j){
              if($("equipo"+j).value==equipo){
                  $("equipo"+i).value="";
                  alert("el equipo seleccionado ya fue solicitado, si necesita mas, aumente la cantidad. ");
                  return false;
                  break;
              }
          }
         }
        var url = controlleropav+"?estado=GestionSolicitud&accion=AiresAAE";
        var p = "opcion=2&equipo="+equipo+"&i="+i;
        new Ajax.Request(url, {
            parameters: p,
            asynchronous: false,
            method: 'post',
            onComplete:  function (resp){
                document.getElementById("d_inst"+i).innerHTML = resp.responseText;
                $("valor_instalacion"+i).value="0.0";
            }
        });
    }
}

String.prototype.trim = function() {return this.replace(/^\s+|\s+$/g, "");};

/****************************
 * autor :Iris vargas
 * parametros:
 * proposito: validar la Solicitud de Aval
 */
function validar_solicitud_equipos(){

    var campos = "";
    var ncampos = "";
    //INFORMACIONÓN DEL CLIENTE
    campos = new Array("nic","id","f_nac_nat","tipo_viv_nat","dir_nat","dep_nat","ciu_nat","est_civil_nat",
        "estr_nat","tel_nat","sal_nat", "ejecutivo_cta");
    ncampos = new Array("Nic","Identificacion", "Fecha de Nacimiento","Tipo de Vivienda", "Direccion","Departamento ","Ciudad","Estado Civil",
        "Estrato","Telefono",   "Salario/Mesada/Ingreso mes", "Ejecutivo de Cuenta");

    for (i = 0; i < campos.length; i++){
        campo = campos[i];
        if (document.getElementById("formulario").elements[campo].value.trim() == ''){
            alert("El campo "+ncampos[i]+" en la informacion del cliente, esta vacio.!");
            document.getElementById("formulario").elements[campo].focus();
            return (false);
            break;
        }
    }


    if(document.getElementById("tipo_p").value=="natural"){
        campos = new Array("pr_apellido_nat","pr_nombre_nat");
        ncampos = new Array("Primer Apellido ","Primer Nombre");

        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en la informacion del cliente, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
    }else{
        campos = new Array("razon_social","rep_legal");
        ncampos = new Array("Razon Social ","Representante Legal");

        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en la informacion del cliente, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
    }

    if(document.getElementById("tipo_viv_nat").value=='02'&&(document.getElementById("arr_nat").value==''||parseInt(document.getElementById("arr_nat").value)<=0)){
        alert("El campo Gastos por arriendo en la informacion del cliente, esta vacio o es menor a 0.!");
        return (false);
    }
        
    // INFORMACIÓN DE LA SOLICITUD
    campos = new Array("convenio","distribucion","fecha_cons", "num_form","proveedor", "instalador",  "zona","responsable");
    ncampos = new Array("Convenio","Tipo Solicitud","Fecha de consulta ", "No. Formulario", "Proveedor","Instalador", "Distrito","Responsable");

    for (i = 0; i < campos.length; i++){
        campo = campos[i];
        if (document.getElementById("formulario").elements[campo].value == ''){
            alert("El campo "+ncampos[i]+" en la informacion de la solicitud, esta vacio.!");
            document.getElementById("formulario").elements[campo].focus();
            return (false);
            break;
        }
    }
    // EQUIPOS E INSTALACIONES
    for (j = 0; j <= fily; j++){
       
        campos = new Array("equipo"+j,"instalacion"+j,"cantidad"+j);
        ncampos = new Array("Equipo","Instalacion","Cantidad");
        for (i = 0; i < campos.length; i++){
            campo = campos[i];
            if (document.getElementById("formulario").elements[campo].value == ''){
                alert("El campo "+ncampos[i]+" en el item "+(j+1)+" en la informacion de la solicitud, esta vacio.!");
                document.getElementById("formulario").elements[campo].focus();
                return (false);
                break;
            }
        }
    }

    // INFORMACIÓN DE LA SOLICITUD
    campos = new Array( "plazo",  "f_vis_plan","hora","valor_solicitado" ,"situacion");
    ncampos = new Array( "Plazo",  "Fecha Visita Planeada", "Hora Visita Planeada",  "Valor solicitado","Situacion");

    for (i = 0; i < campos.length; i++){
        campo = campos[i];
        if (document.getElementById("formulario").elements[campo].value == ''){
            alert("El campo "+ncampos[i]+" en la informacion de la solicitud, esta vacio.!");
            document.getElementById("formulario").elements[campo].focus();
            return (false);
            break;
        }
    }


    // INFORMACIÓN DE TECNICA DEL EQUIPO
    campos = new Array("pregunta1","pregunta2","pregunta3", "pregunta4", "pregunta5");
    ncampos = new Array("1","2","3", "4", "5");

    for (i = 0; i < campos.length; i++){
        campo = campos[i];
        if (document.getElementById("formulario").elements[campo].value == ''){
            alert("La pregunta "+ncampos[i]+" se encuentra sin responder en la informacion tecnica del equipo, esta vacio.!");
            document.getElementById("formulario").elements[campo].focus();
            return (false);
            break;
        }
    }
    puedeHacerVisita();
    var accion = controlleropav+"?estado=GestionSolicitud&accion=AiresAAE";
    var valid = new Validation('formulario');
    if(valid.validate()){

        $('formulario').action = accion + '&opcion=1';
        $("filas").value=fily;
        document.getElementById("formulario").submit();
    }else{
        alert("una o mas fechas en el formulario son invalidas");
    }

}

function dar_formato(num){
    var cadena = "";
    var aux;
    var cont = 1,m,k;
    var dec="0";
    if(num<0) aux=1; else aux=0;
    dec=num.toString().split(".")[1];
    if (dec==undefined)
    {
        dec="0";
    }
    num=num.toString().split(".")[0];
    for(m=num.length-1; m>=0; m--){
        cadena = num.charAt(m) + cadena;
        if(cont%3 == 0 && m >aux)  cadena = "," + cadena; else cadena = cadena;
        if(cont== 3) cont = 1; else cont++;
    }
    return cadena;

}

function tiposol(){
     var indice = $("distribucion").selectedIndex;
        $('tipo_solicitud').value=$("distribucion").options[indice].text ;
}

function formato(obj, decimales){
    var numero = obj.value.replace( new RegExp(",","g"), "");
    var nums = ( new String (numero) ).split('.');
    var salida = new String();
    var TieneDec = numero.indexOf('.');
    var dato = new String();
    if( TieneDec !=-1  ){
        var deci = numero.split('.');
        var dec       = (deci[1].length >2)?deci[1].charAt(2):deci[1].substr(0,deci[1].length);


        if(dec > 5){
            dato =  (parseInt(deci[1].substr(0,2)) + 1);
            if(dato>100){
                nums[0] = new String (parseInt(nums[0])+1);
                obj.value = nums[0]+'';
            }else{
                for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
                obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>9)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '');

            }
        }else{

            for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
            obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '');

        }
    }else{

        for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
        obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '');

    }
}

function validarFechaPla(){
    fecha1 = new Date($("f_vis_plan").value.replace(/-/ig, "/"));
    fecha2 = new Date($("visita_pla").value.replace(/-/ig, "/"));
    if(fecha1<fecha2){
        $("f_vis_plan").value="";
        alert("La fecha digitada debe ser mayor o igual a la fecha sugerida");
    }else if(fecha1>fecha2){
       if($("hora").value==""){
            cargarHoras();
        }
    }else{
         cargarHoras();
    }
}

function puedeHacerVisita(){
    var hora=$("hora").value;
    var fecha=$("f_vis_plan").value;
    var contratista=$("instalador").value;

    if(hora!=""){

        if(fecha!=""&&contratista!=""){

            var url = controlleropav+"?estado=GestionSolicitud&accion=AiresAAE";
            var p = "opcion=6&hora="+hora+"&fecha="+fecha+"&contratista="+contratista;
            new Ajax.Request(url, {
                parameters: p,
                asynchronous: false,
                method: 'post',
                onComplete:  function (resp){
                    if(resp.responseText.replace("\r\n", "")=='false'){
                        $("hora").value="";
                        alert("El instalador no esta disponible en ese horario");
                        return (false);
                    }
                }
            });

        }else{
            $("hora").value="";
            alert("debe digitar una fecha de visita y seleccionar un instalador");
            return (false);
        }
    }

}
