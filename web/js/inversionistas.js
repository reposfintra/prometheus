/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  paso1:Ingreso
 *  paso2:salida inversionista
 *  paso3:salida inversionista v2
 *  paso4:causacion intereses
 */
$(document).ready(function () {
    $('#wast').mouseover(function () {
        var src = '/fintra/images/help_2.png';
        $(this).attr("src", src);
    }).mouseout(function () {
        var src = '/fintra/images/help_.png';
        $(this).attr("src", src);
    });

    cargarTerceroInversionista();
    $('#buscar').click(function () {
        var filtro_ = $('#filtro_').val();
        var periodo_inicio = $('#periodo_inicio').val();
        var periodo_fin = $('#periodo_fin').val();
        var paso = saberPaso();
        if ((filtro_ !== 'otro' && (periodo_inicio === '' || periodo_fin === '')) || (filtro_ === 'otro' && (periodo_inicio !== '' && periodo_fin !== ''))) {
            cargando_toggle();
            switch (paso) {
                case 'paso1':
                    cargarInversionistaPaso1('paso1');
                    break;
                case  'paso2':
                    cargarInversionistaPaso2('paso2');
                    break;
                case 'paso3':
                    cargarInversionistaPaso3('paso3');
                    break;
                case 'paso4':
                    cargarInversionistaPaso4('paso4');
                    break;
                case 'paso5':
                    cargarInversionistaPaso5('paso5');
                    break;
            }

        } else {
            mensajesDelSistema('Debe digitar el rando de periodos', '320', 'auto', false);
        }
    });

    $('#atpaso1').click(function () {
        $('#paso_busqueda').val('atpaso1');
    });
    $('#atpaso2').click(function () {
        $('#paso_busqueda').val('atpaso2');
    });
    $('#atpaso3').click(function () {
        $('#paso_busqueda').val('atpaso3');
    });
    $('#atpaso4').click(function () {
        $('#paso_busqueda').val('atpaso4');
    });
    $('#atpaso5').click(function () {
        $('#paso_busqueda').val('atpaso5');
    });

    $('#wast').click(function () {
        var paso_busqueda = $('#paso_busqueda').val();
        if (paso_busqueda.startsWith('atpaso')) {
            ventanaMostrarhelp();
        } else {
            mensajesDelSistema("Debe seleccionar una pesta�a", '250', '150', false);
        }

    });

});

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function soloNumeros(e, num) {
    var keynum = window.event ? window.event.keyCode : e.which;
    //if (document.getElementById(num.id).value.indexOf('.') !== -1 && keynum === 46)
    //  return false;
    if ((keynum === 0 || keynum === 8 || keynum === 48 || keynum === 46)) {
        return true;
    }
    if (keynum >= 58) {
        return false;
    }
    return /\d/.test(String.fromCharCode(keynum));
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function openTAb(evt, idTab) {
// Declare all variables
    var i, tabcontent, tablinks, tabla_filtro;
    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

// Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

// Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById("tablita").style.display = "block";
    document.getElementById(idTab).style.display = "block";
    evt.currentTarget.className += " active";
}

function busqueda_filtro() {
    var filtro = $('#filtro_').val();
    if (filtro === 'otro') {
        $(".f_otro").css("display", "block");
        $("#tablita").css("width", "1128px");
        $("#tablainterna").css("width", "1132px");
    } else {
        $(".f_otro").css("display", "none");
        $('#periodo_inicio').val('');
        $('#periodo_fin').val('');
        $("#tablita").css("width", "621px");
        $("#tablainterna").css("width", "566px");
    }
}

function saberPaso() {
    var paso_busqueda = $('#paso_busqueda').val();
    var paso;
    switch (paso_busqueda) {
        case 'atpaso1':
            paso = 'paso1';
            break;
        case  'atpaso2':
            paso = 'paso2';
            break;
        case 'atpaso3':
            paso = 'paso3';
            break;
        case 'atpaso4':
            paso = 'paso4';
            break;
        case 'atpaso5':
            paso = 'paso5';
            break;
    }
    return paso;
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center", modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarInversionistaPaso1(paso) {
    $('#tabla_inversionistaPaso1').jqGrid('GridUnload');
    var grid_tabla_ = jQuery("#tabla_inversionistaPaso1");
    if ($("#gview_tabla_inversionistaPaso1").length) {
        reloadGridMostrar(grid_tabla_, 37, paso);
    } else {
        grid_tabla_.jqGrid({
            caption: "Movimiento Auxiliar",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            autowidth: false,
            colNames: ['Periodo', 'Tipo Comprobante', 'Cuenta', 'Valor Debito', 'Valor Credito'],
            colModel: [
                {name: 'periodo', index: 'periodo', width: 100, align: 'center'},
                {name: 'tipo_comprobante', index: 'tipo_comprobante', width: 100, align: 'center', hidden: true},
                {name: 'cuenta', index: 'cuenta', width: 100, align: 'left', hidden: false, summaryType: function (value, name, record, newGroup) {
                        return "Total:";
                    }},
                {name: 'valor_debito', index: 'valor_debito', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_credito', index: 'valor_credito', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: true,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: true,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 37,
                    paso: paso,
                    filtro_: $('#filtro_').val(),
                    periodo_inicio: $('#periodo_inicio').val(),
                    periodo_fin: $('#periodo_fin').val(),
                    tercero: $('#tercero').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {
                var valor_debito = grid_tabla_.jqGrid('getCol', 'valor_debito', false, 'sum');
                var valor_credito = grid_tabla_.jqGrid('getCol', 'valor_credito', false, 'sum');
                grid_tabla_.jqGrid('footerData', 'set', {cuenta: 'TOTAL', valor_debito: valor_debito, valor_credito: valor_credito});
                var info = grid_tabla_.getGridParam('records');
                var contenedorGrafica = sabercontenedorGrafica('tabla_inversionistaPaso1');
                if (info > 0) {
                    mostrarGraficaInversionista('tabla_inversionistaPaso1');
                } else {
                    $('#' + contenedorGrafica).css("display", "none");
                }
                cargando_toggle();
            },
            gridComplete: function (index) {

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_inversionistaPaso1").navButtonAdd('#pager', {
            caption: "Export Excel",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    exportarExcel('tabla_inversionistaPaso1', paso);
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
        $("#tabla_inversionistaPaso1").navButtonAdd('#pager', {
            caption: "Export All Data",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    exportarExcelTotal(paso);
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion, paso) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                paso: paso,
                filtro_: $('#filtro_').val(),
                periodo_inicio: $('#periodo_inicio').val(),
                periodo_fin: $('#periodo_fin').val(),
                tercero: $('#tercero').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function cargarTerceroInversionista() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 38
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#tercero').html('');
                $('#tercero').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#tercero').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarInversionistaPaso2(paso) {
    $('#tabla_inversionistaPaso2').jqGrid('GridUnload');
    var grid_tabla_ = jQuery("#tabla_inversionistaPaso2");
    if ($("#gview_tabla_inversionistaPaso2").length) {
        reloadGridMostrar(grid_tabla_, 37, paso);
    } else {
        grid_tabla_.jqGrid({
            caption: "Movimiento Auxiliar",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            autowidth: false,
            colNames: ['Periodo', 'Tipo Comprobante', 'Cuenta', 'Valor Debito', 'Valor Credito'],
            colModel: [
                {name: 'periodo', index: 'periodo', width: 100, align: 'center'},
                {name: 'tipo_comprobante', index: 'tipo_comprobante', width: 100, align: 'center', hidden: true},
                {name: 'cuenta', index: 'cuenta', width: 100, align: 'left', hidden: false, summaryType: function (value, name, record, newGroup) {
                        return "Total:";
                    }},
                {name: 'valor_debito', index: 'valor_debito', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_credito', index: 'valor_credito', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: true,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: true,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 37,
                    paso: paso,
                    filtro_: $('#filtro_').val(),
                    periodo_inicio: $('#periodo_inicio').val(),
                    periodo_fin: $('#periodo_fin').val(),
                    tercero: $('#tercero').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {
                var valor_debito = grid_tabla_.jqGrid('getCol', 'valor_debito', false, 'sum');
                var valor_credito = grid_tabla_.jqGrid('getCol', 'valor_credito', false, 'sum');
                grid_tabla_.jqGrid('footerData', 'set', {cuenta: 'TOTAL', valor_debito: valor_debito, valor_credito: valor_credito});
                var info = grid_tabla_.getGridParam('records');
                var contenedorGrafica = sabercontenedorGrafica('tabla_inversionistaPaso2');
                if (info > 0) {
                    mostrarGraficaInversionista('tabla_inversionistaPaso2');
                } else {
                    $('#' + contenedorGrafica).css("display", "none");
                }
                cargando_toggle();
            },
            gridComplete: function (index) {

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

            }, groupingView: {
                groupField: ['periodo'],
                groupSummary: [true],
                groupColumnShow: [true],
                groupText: ['<b>{0} Total Debito:</b> ${valor_debito}  <b>Total Credito:</b> ${valor_credito}'],
                groupCollapse: true,
                groupOrder: ['asc'],
                groupSummaryPos: ["footer"]
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_inversionistaPaso2").navButtonAdd('#pager1', {
            caption: "Export Excel",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    exportarExcel('tabla_inversionistaPaso2', paso);
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
        $("#tabla_inversionistaPaso2").navButtonAdd('#pager1', {
            caption: "Export All Data",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    exportarExcelTotal(paso);
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
//        $("#tabla_inversionistaPaso2").navButtonAdd('#pager1', {
//            caption: "Graph",
//            onClickButton: function () {
//                var info = grid_tabla_.getGridParam('records');
//                if (info > 0) {
//                    mostrarGraficaAnalisisReporteProduccion('tabla_inversionistaPaso2');
//                } else {
//                    mensajesDelSistema("No hay informacion para graficar ", '250', '150', false);
//                }
//            }
//        });
    }
}

function cargarInversionistaPaso3(paso) {
    $('#tabla_inversionistaPaso3').jqGrid('GridUnload');
    var grid_tabla_ = jQuery("#tabla_inversionistaPaso3");
    if ($("#gview_tabla_inversionistaPaso3").length) {
        reloadGridMostrar(grid_tabla_, 37, paso);
    } else {
        grid_tabla_.jqGrid({
            caption: "Movimiento Auxiliar",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            autowidth: false,
            colNames: ['Periodo', 'Tipo Comprobante', 'Cuenta', 'Valor Debito', 'Valor Credito'],
            colModel: [
                {name: 'periodo', index: 'periodo', width: 100, align: 'center'},
                {name: 'tipo_comprobante', index: 'tipo_comprobante', width: 100, align: 'center', hidden: false},
                {name: 'cuenta', index: 'cuenta', width: 100, align: 'left', hidden: false, summaryType: function (value, name, record, newGroup) {
                        return "Total:";
                    }},
                {name: 'valor_debito', index: 'valor_debito', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_credito', index: 'valor_credito', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: true,
            pager: '#pager2',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: true,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 37,
                    paso: paso,
                    filtro_: $('#filtro_').val(),
                    periodo_inicio: $('#periodo_inicio').val(),
                    periodo_fin: $('#periodo_fin').val(),
                    tercero: $('#tercero').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {
                var valor_debito = grid_tabla_.jqGrid('getCol', 'valor_debito', false, 'sum');
                var valor_credito = grid_tabla_.jqGrid('getCol', 'valor_credito', false, 'sum');
                grid_tabla_.jqGrid('footerData', 'set', {cuenta: 'TOTAL', valor_debito: valor_debito, valor_credito: valor_credito});
                var info = grid_tabla_.getGridParam('records');
                var contenedorGrafica = sabercontenedorGrafica('tabla_inversionistaPaso3');
                if (info > 0) {
                    mostrarGraficaInversionista('tabla_inversionistaPaso3');
                } else {
                    $('#' + contenedorGrafica).css("display", "none");
                }
                cargando_toggle();
            },
            gridComplete: function (index) {

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

            }, groupingView: {
                groupField: ['periodo'],
                groupSummary: [true],
                groupColumnShow: [true],
                groupText: ['<b>{0} Total Debito:</b> ${valor_debito}  <b>Total Credito:</b> ${valor_credito}'],
                groupCollapse: true,
                groupOrder: ['asc'],
                groupSummaryPos: ["footer"]
            }
        }).navGrid("#pager2", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_inversionistaPaso3").navButtonAdd('#pager2', {
            caption: "Export Excel",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    exportarExcel('tabla_inversionistaPaso3', paso);
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
        $("#tabla_inversionistaPaso3").navButtonAdd('#pager2', {
            caption: "Export All Data",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    exportarExcelTotal(paso);
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
//        $("#tabla_inversionistaPaso3").navButtonAdd('#pager2', {
//            caption: "Graph",
//            onClickButton: function () {
//                var info = grid_tabla_.getGridParam('records');
//                if (info > 0) {
//                    mostrarGraficaAnalisisReporteProduccion('tabla_inversionistaPaso3');
//                } else {
//                    mensajesDelSistema("No hay informacion para graficar ", '250', '150', false);
//                }
//            }
//        });
    }
}

function cargarInversionistaPaso4(paso) {
    $('#tabla_inversionistaPaso4').jqGrid('GridUnload');
    var grid_tabla_ = jQuery("#tabla_inversionistaPaso4");
    if ($("#gview_tabla_inversionistaPaso4").length) {
        reloadGridMostrar(grid_tabla_, 37, paso);
    } else {
        grid_tabla_.jqGrid({
            caption: "Movimiento Auxiliar",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            autowidth: false,
            colNames: ['Periodo', 'Tipo Comprobante', 'Cuenta', 'Valor Debito', 'Valor Credito'],
            colModel: [
                {name: 'periodo', index: 'periodo', width: 100, align: 'center'},
                {name: 'tipo_comprobante', index: 'tipo_comprobante', width: 100, align: 'center', hidden: false},
                {name: 'cuenta', index: 'cuenta', width: 100, align: 'left', hidden: false, summaryType: function (value, name, record, newGroup) {
                        return "Total:";
                    }},
                {name: 'valor_debito', index: 'valor_debito', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_credito', index: 'valor_credito', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: true,
            pager: '#pager3',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: true,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 37,
                    paso: paso,
                    filtro_: $('#filtro_').val(),
                    periodo_inicio: $('#periodo_inicio').val(),
                    periodo_fin: $('#periodo_fin').val(),
                    tercero: $('#tercero').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {
                var valor_debito = grid_tabla_.jqGrid('getCol', 'valor_debito', false, 'sum');
                var valor_credito = grid_tabla_.jqGrid('getCol', 'valor_credito', false, 'sum');
                grid_tabla_.jqGrid('footerData', 'set', {cuenta: 'TOTAL', valor_debito: valor_debito, valor_credito: valor_credito});
                var info = grid_tabla_.getGridParam('records');
                var contenedorGrafica = sabercontenedorGrafica('tabla_inversionistaPaso4');
                if (info > 0) {
                    mostrarGraficaInversionista('tabla_inversionistaPaso4');
                } else {
                    $('#' + contenedorGrafica).css("display", "none");
                }
                cargando_toggle();
            },
            gridComplete: function (index) {

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

            }, groupingView: {
                groupField: ['periodo'],
                groupSummary: [true],
                groupColumnShow: [true],
                groupText: ['<b>{0} Total Debito:</b> ${valor_debito}  <b>Total Credito:</b> ${valor_credito}'],
                groupCollapse: true,
                groupOrder: ['asc'],
                groupSummaryPos: ["footer"]
            }
        }).navGrid("#pager3", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_inversionistaPaso4").navButtonAdd('#pager3', {
            caption: "Export Excel",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    exportarExcel('tabla_inversionistaPaso4', paso);
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
        $("#tabla_inversionistaPaso4").navButtonAdd('#pager3', {
            caption: "Export All Data",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    exportarExcelTotal(paso);
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
//        $("#tabla_inversionistaPaso4").navButtonAdd('#pager3', {
//            caption: "Graph",
//            onClickButton: function () {
//                var info = grid_tabla_.getGridParam('records');
//                if (info > 0) {
//                    mostrarGraficaAnalisisReporteProduccion('tabla_inversionistaPaso4');
//                } else {
//                    mensajesDelSistema("No hay informacion para graficar ", '250', '150', false);
//                }
//            }
//        });
    }
}

function cargarInversionistaPaso5(paso) {
    $('#tabla_inversionistaPaso5').jqGrid('GridUnload');
    var grid_tabla_ = jQuery("#tabla_inversionistaPaso5");
    if ($("#gview_tabla_inversionistaPaso5").length) {
        reloadGridMostrar(grid_tabla_, 37, paso);
    } else {
        grid_tabla_.jqGrid({
            caption: "Movimiento Auxiliar",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            autowidth: false,
            colNames: ['Periodo', 'Tipo Comprobante', 'Cuenta', 'Valor Debito', 'Valor Credito'],
            colModel: [
                {name: 'periodo', index: 'periodo', width: 100, align: 'center'},
                {name: 'tipo_comprobante', index: 'tipo_comprobante', width: 100, align: 'center', hidden: false},
                {name: 'cuenta', index: 'cuenta', width: 100, align: 'left', hidden: false, summaryType: function (value, name, record, newGroup) {
                        return "Total:";
                    }},
                {name: 'valor_debito', index: 'valor_debito', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_credito', index: 'valor_credito', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: true,
            pager: '#pager4',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            grouping: true,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 37,
                    paso: paso,
                    filtro_: $('#filtro_').val(),
                    periodo_inicio: $('#periodo_inicio').val(),
                    periodo_fin: $('#periodo_fin').val(),
                    tercero: $('#tercero').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {
                var valor_debito = grid_tabla_.jqGrid('getCol', 'valor_debito', false, 'sum');
                var valor_credito = grid_tabla_.jqGrid('getCol', 'valor_credito', false, 'sum');
                grid_tabla_.jqGrid('footerData', 'set', {cuenta: 'TOTAL', valor_debito: valor_debito, valor_credito: valor_credito});
                var info = grid_tabla_.getGridParam('records');
                var contenedorGrafica = sabercontenedorGrafica('tabla_inversionistaPaso5');
                if (info > 0) {
                    mostrarGraficaInversionista('tabla_inversionistaPaso5');
                } else {
                    $('#' + contenedorGrafica).css("display", "none");
                }
                cargando_toggle();
            },
            gridComplete: function (index) {

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

            }, groupingView: {
                groupField: ['periodo'],
                groupSummary: [true],
                groupColumnShow: [true],
                groupText: ['<b>{0} Total Debito:</b> ${valor_debito}  <b>Total Credito:</b> ${valor_credito}'],
                groupCollapse: true,
                groupOrder: ['asc'],
                groupSummaryPos: ["footer"]
            }
        }).navGrid("#pager4", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_inversionistaPaso5").navButtonAdd('#pager4', {
            caption: "Export Excel",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    exportarExcel('tabla_inversionistaPaso5', paso);
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
        $("#tabla_inversionistaPaso5").navButtonAdd('#pager4', {
            caption: "Export All Data",
            onClickButton: function () {
                var info = grid_tabla_.getGridParam('records');
                if (info > 0) {
                    exportarExcelTotal(paso);
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
    }
}


function exportarExcel(tabla, paso) {
    var fullData = $("#" + tabla).jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Admin&accion=Fintra",
        data: {
            listado: myJsonString,
            opcion: 39,
            paso: paso

        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function exportarExcelTotal(paso) {
    var opt = {
        autoOpen: false,
        modal: true,
        width: 276,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Admin&accion=Fintra",
        data: {
            opcion: 40,
            paso: paso,
            filtro_: $('#filtro_').val(),
            periodo_inicio: $('#periodo_inicio').val(),
            periodo_fin: $('#periodo_fin').val(),
            tercero: $('#tercero').val()
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function sabercontenedorGrafica(tabla) {
    var contenedor;
    switch (tabla) {
        case 'tabla_inversionistaPaso1':
            contenedor = 'containerinversionistaPaso1';
            break;
        case  'tabla_inversionistaPaso2':
            contenedor = 'containerinversionistaPaso2';
            break;
        case 'tabla_inversionistaPaso3':
            contenedor = 'containerinversionistaPaso3';
            break;
        case 'tabla_inversionistaPaso4':
            contenedor = 'containerinversionistaPaso4';
            break;
        case 'tabla_inversionistaPaso5':
            contenedor = 'containerinversionistaPaso5';
            break;
    }
    return contenedor;
}

function mostrarGraficaInversionista(tabla) {
    var grid = jQuery("#" + tabla), filas = grid.jqGrid('getDataIDs');
    var periodo_ = '';
    var periodos = [], valorDebito = [], valorCredito = [];
    var periodo_2 = grid.getRowData(1).periodo;
    var valorDebitosum = 0;
    var valorCreditosum = 0;
    var contenedorGrafica = sabercontenedorGrafica(tabla);
    for (var i = 0; i < filas.length; i++) {
        var cl = filas[i];
        if (periodo_ !== grid.getRowData(cl).periodo) {
            periodos.push((grid.getRowData(cl).periodo) * 1);
            periodo_ = grid.getRowData(cl).periodo;
        }
    }
    for (var i = 0; i < filas.length + 1; i++) {
        var cl = filas[i];
        if (periodo_2 === grid.getRowData(cl).periodo) {
            valorDebitosum = parseFloat(valorDebitosum) + ((grid.getRowData(cl).valor_debito) * 1);
            valorCreditosum = parseFloat(valorCreditosum) + ((grid.getRowData(cl).valor_credito) * 1);
        } else if (periodo_2 !== grid.getRowData(cl).periodo) {
            periodo_2 = grid.getRowData(cl).periodo;
            valorDebito.push(valorDebitosum * 1);
            valorCredito.push(valorCreditosum * 1);
            valorDebitosum = grid.getRowData(cl).valor_debito;
            valorCreditosum = grid.getRowData(cl).valor_credito;
        }
    }
    console.log(contenedorGrafica);
    console.log(periodos);
    console.log(valorDebito);
    console.log(valorCredito);
    verGrafica(periodos, valorDebito, valorCredito, contenedorGrafica);
}

function verGrafica(periodos, valorDebito, valorCredito, contenedorGrafica) {
    $('#' + contenedorGrafica).css("display", "block");
    $('#' + contenedorGrafica).highcharts({
        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'REPORTE INVERSIONISTAS'
        },
        subtitle: {
            text: ''
        },
        plotOptions: {
            column: {
                depth: 5
            }
        },
        xAxis: {
            title: {
                text: 'Periodo'
            },
            categories: periodos,
            labels: {
                formatter: function () {
                    return '<b>' + this.value + '</b>';
                }
            }
        },
        yAxis: {
            title: {
                text: 'Rango Valores'
            },
            labels: {
                formatter: function () {
                    return '<b> $' + numberConComas(this.value) + '</b>';
                }
            }
        },
        series: [
            {name: 'Valor Debito', data: valorDebito},
            {name: 'Valor Credito', data: valorCredito, color: 'rgba(249, 135, 25, 0.9)'}
        ]
    });
}

function ventanaMostrarhelp() {
    var mensaje = mostrarHelp();
    var paso = saberPaso();
    var permiso = confirmarPermisoHelp();
    console.log(mensaje);
    if (permiso.startsWith('SI')) {
        /*usuarios con permiso para editar el texto de la ayuda*/
        if (mensaje === '') {
            mensajesDelSistema('No se ha realizado la configuraci�n de este paso', '370', 'auto', false);
        } else {
            swal({
                title: "Ayuda " + paso.toLowerCase(),
                text: '<p style = "text-align : justify;"> ' + mensaje + ' </p>',
                html: true,
                showCancelButton: true,
                confirmButtonColor: "#397EAD",
                confirmButtonText: "Aceptar",
                cancelButtonText: "Editar",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (!isConfirm) {
                    ventanaEditarHelp();
                }
            });
        }

    } else if (permiso.startsWith('NO')) {
        /*usuarios sin permiso para editar el texto de la ayuda*/
        swal({
            title: "Ayuda " + paso.toLowerCase(),
            text: '<p style = "text-align : justify;"> ' + mensaje + ' </p>',
            html: true,
            confirmButtonText: "Aceptar"
        });
    }
}

function mostrarHelp() {
    var paso = saberPaso();
    var help;
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'text',
        async: false,
        data: {
            opcion: 41,
            paso: paso,
            modulo: $('#modulo').val()
        },
        success: function (json) {
            console.log(json.length);
            if (json.length > 1) {
                help = json;
            } else {
                help = '';
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return help;
}

function confirmarPermisoHelp() {
    var permiso;
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'text',
        async: false,
        data: {
            opcion: 42
        },
        success: function (json) {
            permiso = json;
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return permiso;
}

function ventanaEditarHelp() {
    mostrarHelpDatos();
    $("#dialogMsjHelp").dialog({
        width: '556',
        height: '380',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: '',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Actualizar": function () {
                guardarCambioHelp();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function mostrarHelpDatos() {
    var paso = saberPaso();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 43,
            paso: paso,
            modulo: $('#modulo').val()
        },
        success: function (json) {
            $('#id').val(json[0].id);
            $('#modulo_').val(json[0].modulo);
            $('#paso_').val(json[0].paso);
            $('#textoArea').val(json[0].descripcion_ayuda);
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function guardarCambioHelp() {
    var id = $('#id').val();
    var textoAyuda = $('#textoArea').val();
    if (textoAyuda == '') {
        mensajesDelSistema('Debe ingresar la descripcion de la ayuda', '370', 'auto', false);
    } else {
        $.ajax({
            async: false,
            url: "./controller?estado=Admin&accion=Fintra",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 44,
                id: id,
                textoAyuda: textoAyuda
            },
            success: function (json) {
                console.log(json.respuesta);
                if (json.respuesta === 'Guardado') {
                    $("#dialogMsjHelp").dialog("close");
                    mensajesDelSistema('Exito al guardar', '300', 'auto', false);
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" + "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }

}