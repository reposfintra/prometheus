/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    }); 
    
    listarTiposInsumo();
    maximizarventana();
});

function listarTiposInsumo() {      
    var grid_tbl_tipo_insumo = jQuery("#tabla_tipo_insumo");
     if ($("#gview_tabla_tipo_insumo").length) {
        refrescarGridTipoInsumo();
     }else {
        grid_tbl_tipo_insumo.jqGrid({
            caption: "Tipos de Insumos",
            url: "./controlleropav?estado=Procesos&accion=Catalogo",           	 
            datatype: "json",  
            height: '290',
            width: '710',         
            colNames: ['Id','Nombre', 'Rentab. Contratista', 'IdIvaAiu', 'Iva Con Aiu', 'Estado','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'nombre', index: 'nombre', width: 350, align: 'left'},      
                {name: 'porc_rentabilidad', index: 'porc_rentabilidad', width: 110, align: 'right', formatter: 'number', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 3}},
                {name: 'iva_con_aiu', index: 'iva_con_aiu', width: 90, align: 'center', hidden:true}, 
                {name: 'iva_aiu', index: 'iva_aiu', width: 90, align: 'center'},   
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden:true}, 
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}         
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_tipo_insumo'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext:null,
            pgbuttons:false,         
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion: 62
                     }
            },   
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_tipo_insumo").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_tipo_insumo").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    var iva_con_aiu = $("#tabla_tipo_insumo").getRowData(cant[i]).iva_con_aiu;
                    if (iva_con_aiu==='1') {
                        ba = '<img src = "/fintra/images/flag_green.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                    } else {
                        ba = '<img src = "/fintra/images/flag_red.gif"style = "margin-left: -4px; height: 19px; vertical-align: middle;"onclick = "">';
                    }
                    jQuery("#tabla_tipo_insumo").jqGrid('setRowData', cant[i], {iva_aiu: ba});
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoTipoInsumo('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_tipo_insumo").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_tipo_insumo"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;
                                 
                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                    editarTipoInsumo(id);
                }
                    
          }
        }).navGrid("#page_tabla_tipo_insumo", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_tipo_insumo").jqGrid("navButtonAdd", "#page_tabla_tipo_insumo", {
            caption: "Nuevo",
            onClickButton: function() {               
                crearTipoInsumo();
            }
        });
    }  
       
}

function refrescarGridTipoInsumo(){   
    jQuery("#tabla_tipo_insumo").setGridParam({
        url: "./controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",
            data: {
                opcion: 62
            }
        }       
    });
    
    jQuery('#tabla_tipo_insumo').trigger("reloadGrid");
}

function crearTipoInsumo(){   
    $('#div_tipo_insumo').fadeIn('slow'); 
    $('#idTipoInsumo').val('');
    $('#nombre_insumo').val('');
    $('#porc_rent').val('');
    $('#chx_iva_aiu').attr("checked",false);
    AbrirDivCrearTipoInsumo();
}

function AbrirDivCrearTipoInsumo(){
      $("#div_tipo_insumo").dialog({
        width: 'auto',
        height: 160,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR TIPO INSUMO',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {         
                guardarTipoInsumo();             
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_tipo_insumo").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarTipoInsumo(cl){
    
    $('#div_tipo_insumo').fadeIn("slow");
    var fila = jQuery("#tabla_tipo_insumo").getRowData(cl);  
    var nombre = fila['nombre'];
    var porc_rentabilidad = fila['porc_rentabilidad'];
    var iva_con_aiu = fila['iva_con_aiu'];

    $('#idTipoInsumo').val(cl);
    $('#nombre_insumo').val(nombre); 
    $('#porc_rent').val(porc_rentabilidad); 
    $('#chx_iva_aiu').attr("checked",(iva_con_aiu==='1')? true:false);
    AbrirDivEditarTipoInsumo();
}

function AbrirDivEditarTipoInsumo(){
      $("#div_tipo_insumo").dialog({
        width: 'auto',
        height: 160,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR TIPO INSUMO',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              guardarTipoInsumo();
            },
            "Salir": function () {              
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_tipo_insumo").parent().find(".ui-dialog-titlebar-close").hide();
}

function guardarTipoInsumo(){   
    var nombre = $('#nombre_insumo').val();
    var porc_rentabilidad = $('#porc_rent').val();
    
    if(nombre!=='' && porc_rentabilidad!==''){
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: "./controlleropav?estado=Procesos&accion=Catalogo",
                    data: {
                        opcion:($('#idTipoInsumo').val()==='')? 59:63,
                        id:$('#idTipoInsumo').val(),
                        nombre:nombre,
                        porc_rentabilidad:porc_rentabilidad,
                        iva_con_aiu:$('#chx_iva_aiu').attr("checked")?'1':'0'
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '270', '165');
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                refrescarGridTipoInsumo(); 
                                $("#div_tipo_insumo").dialog('close');
                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo guardar el tipo de insumo!!", '250', '150');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                                "Message: " + xhr.statusText + "\n" +
                                "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                });
             },500);
    }else{
         mensajesDelSistema("DEBE DILIGENCIAR LOS CAMPOS OBLIGATORIOS!!", '250', '150');      
    }  
}


function CambiarEstadoTipoInsumo(rowid){
    var grid_tabla = jQuery("#tabla_tipo_insumo");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Procesos&accion=Catalogo",
        data: {
            opcion: 64,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarGridTipoInsumo();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado del tipo de insumo!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}


function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

