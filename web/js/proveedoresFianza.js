/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
   cargarProvFianza();
   maximizarventana();      
});

function cargarProvFianza(){

    var grid_listar_config_libranza = $("#tabla_prov_fianza");
    if ($("#gview_tabla_prov_fianza").length) {
         refrescarGridProvFianza();
     }else {
         grid_listar_config_libranza.jqGrid({        
            caption:'MAESTRO PROVEEDORES FIANZA',
            url: "./controller?estado=Garantias&accion=Comunitarias",
            mtype: "POST",
            datatype: "json",
            height: '380',
            //width: '1100',
            colNames: ['Id', 'Nit Empresa', 'Empresa', 'Estado','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, resizable:false, sortable: true, align: 'center', key: true, hidden:true},   
                {name: 'nit', index: 'nit', resizable:false, sortable: true, width: 80, align: 'center', hidden:false},
                {name: 'nombre', index: 'nombre', resizable:false, sortable: true, width: 250, align: 'left'},                               
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden:true}, 
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            pager:'#page_tabla_prov_fianza',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {                
                dataType: "json",
                type: "POST",  
                async:false,
                data: {
                    opcion: 40
                }
            },    
            gridComplete: function() {
                var cant = jQuery("#tabla_prov_fianza").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_prov_fianza").getRowData(cant[i]).cambio;                 
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstado('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_prov_fianza").jqGrid('setRowData', cant[i], {cambio: be});

                }
            },
            /*ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_prov_fianza"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;

                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                    editarConfigFactor(id);
                }

            },*/
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_prov_fianza",{search:false,refresh:false,edit:false,add:false,del:false});      
        jQuery("#tabla_prov_fianza").jqGrid("navButtonAdd", "#page_tabla_prov_fianza", {
            caption: "Nuevo", 
            title: "Agregar nuevo factor",           
            onClickButton: function() {
                crearProvFianza();           
            }
        });           

     }
}

function refrescarGridProvFianza(){    
    jQuery("#tabla_prov_fianza").setGridParam({
        url: "./controller?estado=Garantias&accion=Comunitarias",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async:false,
            data: {
                opcion: 40
            }
        }
    }).trigger("reloadGrid");
}

function crearProvFianza(){   
    $('#div_prov_fianza').fadeIn('slow');   
    resetearFormulario();
    AbrirDivCrearConfigFactor();
}

function AbrirDivCrearConfigFactor(){
      $("#div_prov_fianza").dialog({
        width: 'auto',
        height: 330,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR CONFIGURACION FACTOR POR MILLÓN',
        closeOnEscape: false,
        buttons: {
            "Buscar": buscar,
            "Adicionar": function () {         
                guardar();             
            },
            "Salir": function () {                  
                resetearFormulario();         
                $(this).dialog("destroy");
            }
        }
    });    
}

function guardar(){  
    var nit = $('#prov').val();
    var url = './controller?estado=Garantias&accion=Comunitarias';
    if (validaCampos(nit)) {
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function () {
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 42,
                    nit: nit
                },
                success: function (json) {
                    if (!isEmptyJSON(json)) {
                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '270', '165');
                            return;
                        }
                        if (json.respuesta === "OK") {
                            $("#div_prov_fianza").dialog('close');
                            $("#dialogLoading").dialog('close');
                            resetearFormulario();
                            refrescarGridProvFianza();
                            mensajesDelSistema("Cambios guardados satisfactoriamente", '250', '150', true);
                        }
                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se pudo guardar la configuración!!", '250', '150');
                    }
                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }, 500);
    }
}

function CambiarEstado(rowid){
    var grid_tabla = jQuery("#tabla_prov_fianza");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Garantias&accion=Comunitarias",
        data: {
            opcion: 43,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }
                if (json.respuesta === "OK") {
                   refrescarGridProvFianza();
                }
            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado de la configuracion!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function validaCampos(nitOName){
    var sw = false;
    if (nitOName === '' && nitOName !== undefined) {
        mensajesDelSistema("Debe diligenciar todos los campos", '250', '150');
        return;
    }else{
         sw = true;
    }
    return sw;
}

function changeStatus(e, fieldId, value) {
    if (e.keyCode !== 9) {
        var vlr = (value !== '' && parseFloat(value)>0) ? 0 : '';
        if (fieldId === 'porcentaje_comision') {
            $('#valor_comision').val(vlr); 
        } else {           
            $('#porcentaje_comision').val(vlr);
        }
    }
}

function buscar() {
    var campo = document.getElementById("campo").value;
    var text = document.getElementById("texto").value;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Garantias&accion=Comunitarias",
        data: {
            opcion: 41,
            campo: campo,
            texto: text
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }
                var list = json.rows;
                if (list.length >= 1) {
                    var p = document.getElementById("prov");
                    p.innerHTML = '';
                    
                    list.forEach(function (i) {
                        p.innerHTML += "<option value='" + i.nit + "'>" + i.nombre + "</option>";
                    });
                } else {
                    mensajesDelSistema("No se encontraron proveedores con la información proporcionada", '250', '150');
                }
            } else {
                mensajesDelSistema("Lo sentimos no se pudo conectar con el servidor!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function resetearFormulario(){    
    document.getElementById('texto').value = '';
    document.getElementById('prov').innerHTML = '';
}

function numbersonly(myfield, e, dec)
{
    var key;
    var keychar;
    
    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

// control keys
    if ((key == null) || (key == 0) || (key == 8) ||
            (key == 9) || (key == 13) || (key == 27))
        return true;

// numbers
    else if ((("0123456789.").indexOf(keychar) > -1))
        return true;

// decimal point jump
    else if (dec && (keychar == "."))
    {
        myfield.form.elements[dec].focus();
        return false;
    }
    else
        return false;
}

function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajeConfirmAction(msj, width, height, okAction, id) {  
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear botón de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function setLength(op) {
    var i = document.getElementById("texto");
    if (op === "nit") {
        i.type = "number";
        i.maxlength = 15;
    } else {
        i.type = "text";
        i.maxLength = 30;
    }
}