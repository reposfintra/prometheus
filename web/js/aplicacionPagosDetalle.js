/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {


    $('#buscar').click(function () {
        cargarNegociosCliente();
    });
});


function cargarNegociosCliente() {
    var grid_tabla_ = jQuery("#tabla_negociosCliente");
    if ($("#gview_tabla_negociosCliente").length) {
        reloadGridMostrar(grid_tabla_, 1);
    } else {
        grid_tabla_.jqGrid({
            caption: "Estado Cuenta",
            url: "./controller?estado=Administracion&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '395',
            autowidth: false,
            colNames: ['Cedula', 'Nombre Cliente', 'Negocio', 'Fecha Negocio', 'Estado Negocio', 'Convenio', 'Id Convenio', 'Tasa', 'Afiliado', 'Tipo Negocio', 'Cuotas', 'Valor Negocio', 'Valor Aval', 'Valor Negocio Padre', 'Valor Aval In', 'Saldo Cartera', 'Detalle de Pagos'],
            colModel: [
                {name: 'cod_cli', index: 'cod_cli', width: 100, align: 'center'},
                {name: 'cliente', index: 'cliente', width: 250, align: 'left', hidden: false},
                {name: 'negasoc', index: 'negasoc', width: 100, align: 'center', hidden: false, key: true},
                {name: 'fecha_negocio', index: 'fecha_negocio', width: 100, align: 'center', hidden: false},
                {name: 'estado_neg', index: 'estado_neg', width: 100, align: 'left', hidden: true},
                {name: 'convenio', index: 'convenio', width: 180, align: 'left', hidden: true},
                {name: 'id_convenio', index: 'id_convenio', width: 100, align: 'center', hidden: true},
                {name: 'tasa_interes', index: 'tasa_interes', width: 100, align: 'center', hidden: false},
                {name: 'afiliado', index: 'afiliado', width: 100, align: 'left', hidden: true},
                {name: 'tipo_negocio', index: 'tipo_negocio', width: 180, align: 'left', hidden: true},
                {name: 'nro_docs', index: 'nro_docs', width: 100, align: 'center', hidden: true},
                {name: 'vr_negocio', index: 'vr_negocio', width: 130, align: 'right', sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_aval', index: 'valor_aval', width: 130, align: 'right', sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', hidden: true, formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valor_neg', index: 'valor_neg', width: 130, align: 'right', sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', hidden: true, formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'valoraval', index: 'valoraval', width: 130, align: 'right', sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', hidden: true, formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'saldo_cartera', index: 'saldo_cartera', width: 130, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', summaryType: 'sum', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'pagos_detalle', index: 'generar', width: 100, align: 'center', hidden: false}
                
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            subGrid: false,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 1,
                    cliente: $('#cedula').val(),
                    negocio: $('#negocio').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                
                var cant = jQuery("#tabla_negociosCliente").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var negocio = $("#tabla_negociosCliente").getRowData(cant[i]).negasoc;
                        det = "<img src='/fintra/images/detalle.png' align='center' style='height:30px;width:27px;margin-left: 8px;padding:5px' value='Detalle' title='PDF Detalle' onclick=\"pdfDetalle('" + negocio + "');\" />";
                        liq = "<img src='/fintra/images/liquidacion.png' align='absbottom'  style='height:36px;width:35px;margin-left: 8px;' name='Consolidado' id='Consolidado'  width='19' height='19' title ='PDF Consolidado'  onclick=\"pdfConsolidado('" + negocio + "');\">";
                        jQuery("#tabla_negociosCliente").jqGrid('setRowData', cant[i], {pagos_detalle: liq + det});
                }

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
    
}


function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                cliente: $('#cedula').val(),
                negocio: $('#negocio').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function pdfDetalle(negocio) {
    
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Fintra&negocio="+negocio,
        data: {
            opcion: 2
        },
        success: function (json) {
            if (json.respuesta === 'OK') {
                mensajesDelSistema("Exito en la generacion de pdf, por favor dirigirse al log de descargas", '335', '150', true);
            }
        }
    });
}


function pdfConsolidado(negocio) {
    
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Fintra&negocio="+negocio,
        data: {
            opcion: 5
        },
        success: function (json) {
            if (json.respuesta === 'OK') {
                mensajesDelSistema("Exito en la generacion de pdf, por favor dirigirse al log de descargas", '335', '150', true);
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}