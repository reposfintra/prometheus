/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    obtenerMenu("#menu-pagina");

    $("#menu-pagina").delegate(".menu-item", "click", function () {
        cargarPagina($(this).attr("ruta-pagina"));
    }); 

});

 
function Menu(nombreMenu, opciones){
    this.nombreMenu = nombreMenu;
    this.opciones = opciones;
    this.generarMenu = function(){
        var menu = this.cabecera(this.nombreMenu);
        for(i = 0; i<this.opciones.length;i++){
            menu += this.itemOpcion(opciones[i]);
        }
        menu += this.cerrarSesion();
        return menu;
    };
    this.cabecera = function(nombre){
        return  '<li class = "sidebar-brand">'+
                    '<a>'+ nombre +'</a>'+
                '</li>';
    };
    this.itemOpcion = function(opcion){
        return '<li>'+
                    '<a class="menu-item" ruta-pagina="'+opcion.ruta+'">'+opcion.descripcion+'</a>'+
               '</li>';
    };
    this.cerrarSesion = function(){
        return '<li>'+
                    '<a href="#" onclick=" window.close(); ">Salir</a>'+
               '</li>';
    };
}


function obtenerMenu(elemento) {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        data: {
            opcion: 27
        },
        success: function(json) {
                if (json.error) {                   
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                var menu = new Menu("Opciones", json);
                $(elemento).html(menu.generarMenu());

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
                
}

function cargarPagina(pagina) {
    $.ajax({
        type: 'POST',
        url: "" + pagina,
        dataType: 'html',
        success: function(html) {
            $("#container").html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: "Mensaje",
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("close");             
            }
        }
    });
}

