/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//
//$('select#tipo_de_nota').on('change',function(){
//    var valor = $(this).val();
//    alert(valor);
//    console.log(valor);
//});

$(document).ready(function () {
    cargando_toggle();
    maximizarventana();
    
    
       $("#tipo_nota option[value='-1']").attr("selected", true);

    
    $("#tipo_nota").change(function(){
          tipo_nota_seleccionada = $(this).val();
          
          
          var plantilla="";
          
          
          
          if(tipo_nota_seleccionada==1){
              
              plantilla="<span style='font-family:verdana,geneva,sans-serif;'><span style='color:#FF8C00;'><strong></strong></span></span><ul><li><span style='color:#5d5d5d;'>La presente oferta no incluye los costos de las p�lizas, en caso tal de que el cliente las solicite en la aprobaci�n de dicha Oferta Mercantil, �stas ser�n asumidas por el cliente adicional al valor inicialmente ofertado.</span></li><li><span style='color:#5d5d5d;'>En caso de realizar trabajos que no se encuentren incluidos en esta oferta ser�n cobrados como adicionales.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>Los costos de los descargos necesarios para la ejecuci�n de los trabajos est�n incluidos en la presente oferta.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>En caso de realizar trabajos que no se encuentren incluidos en esta oferta ser�n cobrados como adicionales.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>En caso que se requiera aplazar o cancelar trabajos ya adjudicados, se deber� informar por escrito con un (1) d�a de anticipaci�n y en caso que se haya desplazado personal y equipos, el cliente deber� cubrir los costos en los que se incurra, por no haberse confirmado el aplazamiento o cancelaci�n.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>La puesta a tierra es responsabilidad exclusiva del cliente, por lo tanto Electricaribe no se hace responsable por da�os causados a terceros en el evento que sea hurtado el conductor de conexi�n.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>El precio para la realizaci�n de los trabajos recogidos en la oferta variar� en funci�n de los trabajos contratados.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>Electricaribe no se hace responsable por problemas que se puedan presentar durante la apertura y cierre de los cortacircuitos y seccionadores, por fallas que sean atribuibles a falta de mantenimiento o mal estado de algunos de los elementos que componen los equipos de seccionamiento.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>Todos los permisos de rotura de v�as, pago de impuestos, licencias ambientales y cualquier otra de ser necesario que genere el proyecto, ser�n por cuenta exclusiva del cliente.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>La presente oferta est� sujeta a un estudio de cr�dito para la viabilidad de financiar los trabajos a trav�s de la factura de energ�a.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>Con car�cter general la realizaci�n de todos los trabajos se har� d� forma que garantice la seguridad, tanto de las personas que intervienen en ellos, como de los equipos y otras personas que realicen labores simult�neamente en lugares pr�ximos.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>Para la aprobaci�n de esta oferta el cliente debe entregar fotocopia de la c�dula del aprobante y los documentos diligenciados que se encuentran anexos en el inciso 8 de esta propuesta.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>Los costos asociados a apertura y cierre de cortacircuitos y seccionador general se descontar�n de la presente oferta en caso de que el cliente decida realizar la actividad con su personal de mantenimiento.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>Esta oferta no incluye ning�n trabajo relacionado con equipos de Medida.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>Una vez aceptado el trabajo por parte del cliente, MULTISERVICIOS proceder� con la compra del material necesario para ejecutar las actividades contratadas; en caso de desistimiento, los materiales que hayan sido comprados ser�n cobrados al cliente de acuerdo con las condiciones de pago pactadas en la carta de aceptaci�n firmada por �l&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>Los materiales cotizados en la presente oferta son adquiridos sobre pedido, la fecha de entrega est� sujeta a cambios, la cual puede variar en funci�n del tiempo de entrega de la materia prima, contratiempos en el momento de transportarlo, siniestros, calamidad o cualquier otra dificultad no controlable por nuestra organizaci�n.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>Esta oferta no incluye el costo del dise�o del sistema de apantallamiento ni de su construcci�n, si el an�lisis del nivel de riesgo frente a rayos determina que se debe instalar un sistema de apantallamiento, se entregar� una oferta como adicional para el dise�o del sistema de apantallamiento, del sistema de puesta a tierra y su respectiva construcci�n. Es importante aclarar que esto es un requisito indispensable para la energizaci�n del proyecto.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>En caso de que alguno de los Coopropietarios se abstenga de pagar su obligaci�n, �sta se trasladar� al �rea Com�n.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>La garant�a del mantenimiento del transformador es de doce meses contra posibles fugas de aceite a los empaques y radiadores y bajo condiciones normales de operaci�n. Para mantener las condiciones de la garant�a se requiere la instalaci�n de tres descargadores de sobretensi�n con puesta a tierra normalizada en el punto de conexi�n.&nbsp;<br></span></li><li><span style='color:#5d5d5d;'>Trabajos adicionales por cambio de tapa, tanque o paneles de radiadores, tap o cambiador de tensiones, o cualquier trabajo adicional que implique reparaci�n no contemplada en esta oferta deber� ser asumido por el cliente. Se har� revisi�n visual de las bobinas, conmutador y bajantes para verificar que no exista deterioro sobre la parte activa del transformador.</span></li></ul><span style='color:#696969;'></span>";
                      
          }else if(tipo_nota_seleccionada==2){
              
              plantilla="<p><span style='font-family:verdana,geneva,sans-serif;'><span style='color:#FF8C00;'><strong>SUMINISTRO DE LUMINARIA DE ALUMBRADO PUBLICO</strong></span><br></span></p><p><span style='color:#FF8C00;'><strong></strong></span><br><span style='color:#696969;'><strong>Se�ores</strong><br><strong>SEMSA E.S.P.<br>Atn.: Nelson Guzman<br>CR 24 No 1 A - 24 OF 906 PI 9 en Puerto Colombia.</strong></span><br></p><p><span style='color:#696969;'><strong><br>Objeto:</strong> Oferta para actividades de suministro de luminarias de alumbrado publico.<br></span></p><p><span style='color:#696969;'><br><strong>Alcance:</strong> SUMINISTRO DE LUMINARIA DE ALUMBRADO PUBLICO MARCA ROY ALPHA.<br></span></p><p><span style='color:#696969;'><br><strong>Tiempo de Ejecuci�n:</strong> Se contempla:</span></p><ul><li><p><span style='color:#696969;'>&nbsp;1000 UND en 15 d�as</span></p></li><li><p><span style='color:#696969;'>&nbsp;2000 UND en Marzo 30</span></p></li><li><span style='color:#696969;'>&nbsp;2499 UND en 60 d�as</span></li></ul>";
              
          }else if(tipo_nota_seleccionada==5){
             
             
            plantilla="<p><br></p><p>COMERCIAL: </p><p>CARGO: </p><p>TELEFONO: </p><p>CORREO: </p><p><br></p>";

              
          }
          
//        else
//          if(tipo_nota_seleccionada==2){
//              
//              plantilla="<p>- La luminarias ofertadas son de la marca ROY ALPHA (se adjunta ficha t�cnica de la misma)<br>- El periodo de garant�a comienza a regir a partir de la fecha de facturaci�n del producto.<br>- La garant�a est�ndar para la luminaria LED es de 10 a�os<br>- La luminarias se entregan en plataforma de transporte, el descargue corre por cuenta del cliente.</p><p><br><strong>- CONDICIONES DE GARANTIA:<br>La garant�a aplica si:</strong><br>-El producto ha sido debidamente conectado, instalado y operado, dentro de la configuraci�n de<br>voltaje de los elementos electr�nicos y que corresponda con el voltaje de alimentaci�n de los<br>mismos, dicha configuraci�n debe corresponder con las caracter�sticas establecidas en los<br>diagramas de conexi�n de cada uno de estos elementos electr�nicos.<br>- El producto ha sido instalado a la temperatura ambiente m�xima (Ta) indicada en el producto<br>- El producto presenta una depreciaci�n del lumen por debajo de L70F10 (70% de lumen de salida<br>original al instalar) durante el periodo de garant�a de los LED�s. (L70 B10 significa que un m�nimo del<br>70% del flujo luminoso inicial de la luminaria se mantendr� durante un periodo que corresponder�,<br>como m�nimo, al periodo de garant�a para la temperatura ambiente nocturna m�xima)<br>- Las partes mec�nicas de la luminarias han sido operadas de acuerdo a lo indicado en el<br>instructivo de la instalaci�n, con los elementos electr�nicos con los cuales est�n equipadas las<br>mismas hallan sido conectados seg�n los par�metros nominales y las variaciones de voltaje en<br>condiciones normales no excedan +/- 10%</p>"
//  
//          } else if(tipo_nota_seleccionada==4){
//          
//            plantilla="<p>- Orden de compra<br>- Existencia y representaci�n legal<br>- Fotocopia de la cedula de ciudadan�a<br>- Estados financieros<br>- Fotocopia del NIT o RUT</p>";
//          }
          
          
           var nicE = new nicEditors.findEditor('descripcion_nota');
           nicE.setContent(plantilla);
        
      
      
    });
//    bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });

    $("#fechaini").val('');
    $("#fechafin").val('');
    $("#fecha").val('');
    $("#fechaini").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fechafin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        //minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        minDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    $('#ui-datepicker-div').css('clip', 'auto');
    cargarLineasNegocio();
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $("#buscar_solicitud").click(function () {
        var fechainicio = $("#fechaini").val();
        var fechafin = $("#fechafin").val();
        if (((fechainicio === '') && (fechafin === '')) || ((fechainicio !== '') && (fechafin !== ''))) {
            cargarInfoSolicitudes();
        } else {
            mensajesDelSistema("Por favor revice el rango de fechas", '410', '150', false);
        }
    });
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    });
    ordenarCombo('linea_negocio');
    autocompletar("txt_nom_cliente", 1);
    autocompletar("txt_nom_proyecto", 2);
    autocompletar("responsable_", 3);

});


/**********************************************************************************************************************************************************
 Tabla Solicitudes
 ***********************************************************************************************************************************************************/
function cargarInfoSolicitudes() {
    var grid_tabla = jQuery("#tabla_infoSolicitud");
    if ($("#gview_tabla_infoSolicitud").length) {
        reloadGridInfoSolicitudes(grid_tabla, 39);
    } else {
        grid_tabla.jqGrid({
            caption: "SOLICITUDES",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '500',
            width: '1590',
            colNames: ['Fecha creacion', 'Nombre Cliente', 'Nombre Proyecto', 'Foms', 'Idsolicitud', 'Costo Contratista', 'Valor Cotizacion', 'Etapa', 'Estado',
                'Codigo cliente', 'Tipo solicitud', 'Estado_cartera', 'Fecha validacion cartera', 'Responsable', 'Interventor', 'Flag', 'id_trazabilidad', 'Id estado', 'presupuesto_terminado', 'Acciones'],
            colModel: [
                {name: 'creation_date', index: 'creation_date', width: 130, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nomcli', index: 'nomcli', width: 230, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 230, sortable: true, align: 'left', search: true},
                {name: 'num_os', index: 'num_os', width: 110, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'valor_cotizacion', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'total', index: 'valor_cotizacion', editable: false, align: 'left', width: 150, sorttype: 'currency', formatter: 'currency', sortable: true,
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'nombre_etapa', index: 'nombre_etapa', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_estado', index: 'nombre_estado', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'codcli', index: 'codcli', width: 90, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'tipo_solicitud', index: 'tipo_solicitud', width: 130, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'estado_cartera', index: 'estado_cartera', width: 100, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'fecha_validacion_cartera', index: 'fecha_validacion_cartera', width: 200, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'responsable', index: 'responsable', width: 180, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'interventor2', index: 'interventor2', width: 110, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'flag', index: 'flag', width: 40, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'trazabilidad', index: 'trazabilidad', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'id_estado', index: 'id_estado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'presupuesto_terminado', index: 'presupuesto_terminado', width: 150, sortable: true, align: 'left', hidden: true, search: true},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 185, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ignoreCase:true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila;
                var estado_trazabilidad;
                var id_estado, iva_aiu = '';
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);
                    //var Cargar_Archivos = "<img src='/fintra/images/opav/File_add.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Cargar Archivos'  onclick=\"divemergente_Carga_Archivos('" + cl + "');\">";
                    accion='';
                    accion2 ='';
                    if (fila.trazabilidad == '4' || fila.trazabilidad == '5' || fila.trazabilidad == '6'  ) {
                        accion = "<img src='/fintra/images/Pdf_Dereck.svg' id=      'logo_pdf' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Generar Pdf Presupuesto'  onclick=\"generar_pdf_costo('" + cl + "');\">";
                        accion2 = "<img src='/fintra/images/Pdf_Dereck.svg' id='logo_pdf' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19'  title ='Generar Pdf Cotizaci�n'  onclick=\"LoadModal('" + cl + "');\">";
                    }else{
                        console.log(fila);
                        if (fila.trazabilidad == '3') {
                            if(fila.id_estado =='330' || fila.id_estado== 350){
                                accion2 = "<img src='/fintra/images/Pdf_Dereck.svg' id='logo_pdf' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Generar Pdf Cotizaci�n'  onclick=\"generar_pdf_costo('" + cl + "');\">";
                            }                            
                        }
                        else if(fila.id_estado == '110' || fila.id_estado =='130'|| fila.id_estado =='140'|| fila.id_estado =='145'|| fila.id_estado =='150'|| fila.id_estado =='160'|| fila.id_estado =='170'|| fila.id_estado =='180'|| fila.id_estado =='190'|| fila.id_estado == '191'|| fila.id_estado == '192' || fila.id_estado =='194'){
                            accion = "<img src='/fintra/images/Pdf_Dereck.svg' id='logo_pdf' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Generar Pdf Presupuesto'  onclick=\"generar_pdf_costo('" + cl + "');\">";
                            accion2 = "<img src='/fintra/images/Pdf_Dereck.svg' id='logo_pdf' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Generar Pdf Cotizaci�n'  onclick=\"LoadModal('" + cl + "');\">";
                        }
                        
                    }  

                
                      var accion3 = "<img src='/fintra/images/opav/plus.png' id='editar' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Agregar Notas'  onclick=\"AbrirDivGuardarNotas('" + cl + "');\">";
                      
                      //var accion3 = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"AbrirDivEditarNotas('" + cl + "');\>";

                    
                      var accion4 = "<img src='/fintra/images/edit.ico' id='editar' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Ver Notas Oferta'  onclick=\"AbrirDivDUNotas('" + cl + "');\">";
              
                
                
                    grid_tabla.jqGrid('setRowData', ids[i], {actions: accion + accion2 + accion3 +accion4});
                }
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 39,
                    lineaNegocio: $('#linea_negocio').val(),
                    responsable: $('#responsable').val(),
                    solicitud: $('#solicitud').val(),
                    estadoCartera: '',
                    fechaInicio: $('#fechaini').val(),
                    fechafin: $('#fechafin').val(),
                    trazabilidad: '1',
                    etapaActual: $('#etapaActual').val(),
                    id_cliente: $('#id_cliente').val(),
                    nom_proyecto: $('#txt_nom_proyecto').val(),
                    foms: $('#txt_foms').val(),
                    tipo_proyecto: $('#tipo_proyecto').val()

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
    }
}




function cargarNotasOferta(idsolicitud) {
 var grid_tabla = jQuery("#tabla_Notas");
    if ($("#gview_tabla_Notas").length) {
        reloadGridCargarNotas(grid_tabla, 103);
    } else {
        grid_tabla.jqGrid({
            caption: "NOTAS",
            url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
            datatype: "json",
            height: '400',
            width: '950',
            colNames: ['id','id_tipo_nota', 'Tipo de nota', 'Nota', 'Acciones'],
            colModel: [
                 {name: 'id', index: 'id', width: 100, sortable: true, align: 'left', hidden: true, search: true},
                 {name: 'id_tipo_de_nota', index: 'tipo_de_nota', width: 100, sortable: true, align: 'left', hidden: true, search: false},
                 {name: 'tipo_de_nota', index: 'tipo_de_nota', width: 100, sortable: true, align: 'left', hidden: false, search: false},
                {name: 'nota_de_oferta', index: 'nota_de_oferta', width: 700, sortable: true, align: 'left', hidden: false, search: false},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: 100, search: false}

            ],
            rowNum: 1000000,
            rowTotal: 1000000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: true,
            ignoreCase:true,
            ondblClickRow: function (rowid, iRow, iCol, e) {
                //mostrarFormulario(rowid);
            }, gridComplete: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                var fila;
                var estado_trazabilidad;
                var id_estado, iva_aiu = '';
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    fila = grid_tabla.jqGrid("getLocalRow", cl);
                    var data =grid_tabla.jqGrid('getRowData',cl);

                    //var Cargar_Archivos = "<img src='/fintra/images/opav/File_add.png' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='ca' id='ca'value=''  width='19' height='19' title ='Cargar Archivos'  onclick=\"divemergente_Carga_Archivos('" + cl + "');\">";
                    accion='';
                    accion2 ='';
                   
                        accion = "<img src='/fintra/images/edit.ico' id='logo_pdf' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Modificar Nota'  onclick=\"AbrirDivModificarNotas('" + cl + "');\">";
                        accion2 = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' id=      'logo_pdf' align='absbottom'  style='height:25px;width:24px;margin-left: 8px;' name='wi' id='accion'value=''  width='19' height='19' title ='Eliminar Nota'   onclick=\"mensajeConfirmacion('�Esta seguro que desea eliminar la Nota?','250','150',eliminarNota,'" + cl + "');\">";  

                
                    grid_tabla.jqGrid('setRowData', ids[i], {actions: accion + accion2});
                }
            },
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 103,
                    id_solicitud: idsolicitud,
                    

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            loadComplete: function () {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }

        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: false}, {
        });
        grid_tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true

                });
    }

}




function mensajeConfirmacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj3").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}



function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function LoadModal(cl) {
    $("#dialog").dialog({
        modal: true,
        buttons: {
            Cancel: function () {
                $(this).dialog("close");
            },
            Aceptar: function () {
                generarCotizacion(cl)
                $(this).dialog("close");
            }
        },
        show: {
            effect: "blind",
            duration: 400
        },
        hide: {
            effect: "fade",
            duration: 400
        }
    });
}
;







function AbrirDivDUNotas(idsolicitud) {
    
    
    
   cargarNotasOferta(idsolicitud);
    
         
    
    $("#div_ver_notas").dialog({
        width: 1000,
        height: 600,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Notas de la oferta',
        closeOnEscape: false,
        buttons: {
//            "Actualizar": function () {
//                guardarNotasOfertas(idsolicitud);
//            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}




function AbrirDivModificarNotas(id) {
    
    
    
    var grid_tabla = $("#tabla_Notas");
    
    
    
    var idNota = grid_tabla.getRowData(id).id;
    var nota_de_oferta = grid_tabla.getRowData(id).nota_de_oferta;
    var tipo_nota = grid_tabla.getRowData(id).id_tipo_de_nota;



    
    $("#tipo_nota_modificar").val(tipo_nota);

    
    var nicE = new nicEditors.findEditor('descripcion_nota_modificar');
    nicE.setContent(nota_de_oferta);
    $("#div_ver_notas").dialog("destroy");
    
    $("#div_modificar_notas").dialog({
        width: 1000,
        height: 600,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Notas de la oferta',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                modificarNotasOfertas(idNota);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}



function AbrirDivGuardarNotas(idsolicitud) {
    
      $('#descripcion_nota').val('');
         
    
    $("#div_editar_notas").dialog({
        width: 1000,
        height: 600,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'Nota de la oferta',
        closeOnEscape: false,
        buttons: {
            "Guardar": function () {
                guardarNotasOfertas(idsolicitud);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_Control").parent().find(".ui-dialog-titlebar-close").hide();
}








function reloadGridInfoSolicitudes(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                lineaNegocio: $('#linea_negocio').val(),
                responsable: $('#responsable').val(),
                solicitud: $('#solicitud').val(),
                estadoCartera: '',
                fechaInicio: $('#fechaini').val(),
                fechafin: $('#fechafin').val(),
                trazabilidad: '1',
                etapaActual: $('#etapaActual').val(),
                id_cliente: $('#id_cliente').val(),
                nom_proyecto: $('#txt_nom_proyecto').val(),
                foms: $('#txt_foms').val(),
                tipo_proyecto: $('#tipo_proyecto').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}



function reloadGridCargarNotas(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
               
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}


function generar_pdf_costo(id_solicitud) {

    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        async: false,
        data: {
            opcion: 90,
            id_solicitud: id_solicitud
        },
        beforeSend: function () {
            $(".loader").css("display", "block");
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {


                if (json.respuesta === "OK") {
                    window.open('.' + json.Ruta);
                }
            }

        },
        complete: function () {
            $(".loader").css("display", "none");
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
}

function generarCotizacion(id_solicitud) {
    var detallado = $('#switch').is(":checked") === true ? 'SI' : 'NO';
    
    var apu = $('#switch2').is(":checked") === true ? 'SI' : 'NO';
    var precios = $('#precios').is(":checked");

//    var detallado = 'SI';
    console.log(detallado);
    console.log(apu);
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 85,
            id_solicitud: id_solicitud,
            detallado: detallado,
            apu:apu,
            precios: precios
        },
        beforeSend: function () {
            $(".loader").css("display", "block");
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    window.open('.' + json.Ruta);
                }
            } else {
                mensajesDelSistema("Lo sentimos ocurri� un error al generar cotizaci�n!!", '250', '150');
            }

        },
        complete: function () {
            $(".loader").css("display", "none");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function guardarNotasOfertas(idSolicitud) {
    
    
    var obj=new Object();
    
    obj.id_solicitud=idSolicitud;
    
    var nicE = new nicEditors.findEditor('descripcion_nota');
    question = nicE.getContent();
    question = question.replace(/['"]+/g, "'");
    obj.nota=encodeURIComponent(question);
    obj.tipo_nota= tipo_nota_seleccionada;

    obj=JSON.stringify(obj);
    
    
    console.log(obj);

    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        async: false,
        data: {
            opcion: 101,
            informacion: obj
        },
        beforeSend: function () {
            $(".loader").css("display", "block");
        },
        success: function (json) {
            
            
           $(".loader").css("display", "none");

            if (!isEmptyJSON(json)) {


                if (json.respuesta === "OK") {
                    window.open('.' + json.Ruta);
                }
            }
            
            
            $("#div_editar_notas").dialog("destroy");

        },
        complete: function () {
            $(".div_editar_notas").css("display", "none");
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
}





function eliminarNota(id) {
    
    var grid_tabla = $("#tabla_Notas");
    
    
    
    var idNota = grid_tabla.getRowData(id).id;

    
    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        async: false,
        data: {
            opcion: 104,
            idNota: idNota
        },
        beforeSend: function () {
            $(".loader").css("display", "block");
        },
        success: function (json) {
            
            
           $(".loader").css("display", "none");

            if (!isEmptyJSON(json)) {


                if (json.respuesta === "OK") {
                    window.open('.' + json.Ruta);
                }
            }
            
            
            $("#div_ver_notas").dialog("destroy");

        },
        complete: function () {
            $(".div_ver_notas").css("display", "none");
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
}




function modificarNotasOfertas(idNota) {
    
    
    var obj=new Object();
    
    
//    var grid_tabla = $("#tabla_Notas");
//    
//    
//    
//    var idNota = grid_tabla.getRowData(idNota).id;
    
    obj.idNota=idNota;
    
    var nicE = new nicEditors.findEditor('descripcion_nota_modificar');
    question = nicE.getContent();
    question = question.replace(/['"]+/g, "'");
    obj.nota=encodeURIComponent(question);

    obj=JSON.stringify(obj);
    
    
    console.log(obj);

    var url = '/fintra/controlleropav?estado=Procesos&accion=Cliente';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        async: false,
        data: {
            opcion: 105,
            informacion: obj
        },
        beforeSend: function () {
            $(".loader").css("display", "block");
        },
        success: function (json) {
            
            
           $(".loader").css("display", "none");

            if (!isEmptyJSON(json)) {


                if (json.respuesta === "OK") {
                    window.open('.' + json.Ruta);
                }
            }
            
            
            $("#div_modificar_notas").dialog("destroy");

        },
        complete: function () {
            $(".div_modificar_notas").css("display", "none");
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
}

/**********************************************************************************************************************************************************
 Utiles Generales
 ***********************************************************************************************************************************************************/
function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

function ordenarCombo(cboId) {
    var valor = $('#' + cboId).val();
    var options = $("#" + cboId + " option");
    options.detach().sort(function (a, b) {
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#" + cboId);
    $("#" + cboId).val(valor);
}

function autocompletar(id, opp) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controlleropav?estado=Procesos&accion=Cliente",
                dataType: "json",
                data: {
                    q: request.term,
                    opcion: 78,
                    opp: opp
                },
                success: function (data) {
                    // response( data );
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.label,
                            mivar: item.value
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //$("#"+id).val(ui.item.mivar);
            if (opp == 1) {
                $('#id_cliente').val(ui.item.mivar);
            }
            if (opp == 3) {
                $('#usuario_').val(ui.item.mivar);
            }
            console.log(ui.item ?
                    "Selected: " + ui.item.mivar :
                    "Nothing selected, input was " + ui.item.label);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

function cargarLineasNegocio() {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 37
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#linea_negocio').html('');
                $('#linea_negocio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#linea_negocio').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function agregarclases() {
    var campos = '';
    campos = ['#valcotizacion', '#valdesc', '#subtotal', '#valiva', '#val_admon', '#val_imprevisto', '#val_utilidad', '#val_anticipo', '#perc_admon', '#perc_rete'];
    for (var i = 0; i < campos.length; i++) {
        $(campos[i]).addClass("solo-numero");
    }
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function cargando_toggle() {
    $('#loader-wrapper').toggle();
}

function  mostrarVentanaAccion(id_solicitud) {
    $('#descripcion_causal').val('');
    var grid_tabla = $("#tabla_infoSolicitud");
    var trazabilidad = grid_tabla.getRowData(id_solicitud).trazabilidad;
    var id_estado = grid_tabla.getRowData(id_solicitud).id_estado;
    var nombre_estado = grid_tabla.getRowData(id_solicitud).nombre_estado;
    $('#idsolicitud').val(id_solicitud);
    $('#estadoActual').val(nombre_estado);
    $('#idestadoActual').val(id_estado);
    cargarEtapas(trazabilidad);
    cargarTrazabilidadOferta(id_solicitud);
    cargarEstadoEtapas(id_estado);
    $("#cambioEtapa").dialog({
        width: '800',
        height: '475',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Cambio de Estado',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Actualizar": function () {
                cambiarEstado();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarEtapas(trazabilidad) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 40,
            etapa: trazabilidad
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#etapa').val('');
                $('#idetapa').val('');
                for (var datos in json) {
                    $('#etapa').val(json[datos]);
                    $('#idetapa').val(datos);
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTrazabilidadOferta(id_solicitud) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 81,
            idsolicitud: id_solicitud
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#trazabilidad').val(json.respuesta);

            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarEstadoEtapas(id_estado) {
    $.ajax({
        type: 'POST',
        url: '/fintra/controlleropav?estado=Procesos&accion=Cliente',
        dataType: 'json',
        async: false,
        data: {
            opcion: 41,
            etapa: 1,
            id_estado: id_estado
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#estado').html('');
                $('#estado').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#estado').append('<option value=' + json[datos].id_estado_destino + '>' + json[datos].nombre_estado + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

/**********************************************************************************************************************************************************
 Fin Utiles Generales
 ***********************************************************************************************************************************************************/


