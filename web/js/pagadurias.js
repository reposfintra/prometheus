/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {   
   cargarPaises();
   listarPagadurias();
   
     cargarDepartamentos('CO','dep_dir');
    $('#dep_dir').val('ATL');
    cargarCiudad('ATL', "ciu_dir");
    $('#ciu_dir').val('BQ');
    cargarVias('BQ', "via_princip_dir");
    cargarVias('BQ', "via_genera_dir");
    
    cargarDepartamentos('CO','dep_dir_emp');
    $('#dep_dir_emp').val('ATL');
    cargarCiudad('ATL', "ciu_dir_emp");
    $('#ciu_dir_emp').val('BQ');
    cargarVias('BQ', "via_princip_dir_emp");
    cargarVias('BQ', "via_genera_dir_emp");
    
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    
    $("#documento").blur(function(){
        CalcularDv('documento','digito_verificacion');
    });
    
    $("#nit_empresa").blur(function(){
        CalcularDv('nit_empresa','digito_verificacion_emp');
    });
  
   $("#pais").change(function() {
        var op = $(this).find("option:selected").val();
        cargarDepartamentos(op, "departamento");
    });
    
    $("#departamento").change(function() {
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciudad");
    });
    
     $("#dep_dir").change(function() {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciu_dir");
    });
    
    $("#dep_dir_emp").change(function() {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciu_dir_emp");
    });
    
    $("#ciu_dir").change(function() {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarVias(op, "via_princip_dir");
        cargarVias(op, "via_genera_dir");
    });
    
    $("#ciu_dir_emp").change(function() {
        resetAddressValues();
        var op = $(this).find("option:selected").val();
        cargarVias(op, "via_princip_dir_emp");
        cargarVias(op, "via_genera_dir_emp");
    });
    
    $("#via_princip_dir").change(function() {
          $("#via_genera_dir").val('');
    });
    
});

function listarPagadurias() {      
    var grid_tbl_pagadurias = jQuery("#tabla_pagadurias");
     if ($("#gview_tabla_pagadurias").length) {
        refrescarGridPagadurias();
     }else {
        grid_tbl_pagadurias.jqGrid({
            caption: "PAGADURIAS",
            url: "./controller?estado=Maestro&accion=Libranza",           	 
            datatype: "json",  
            height: '290',
            width: '1050',
            cellEdit: true,
            colNames: ['Id','Razon Social','Documento', 'DV', 'Ciudad', 'Pais', 'Dpto', 'Cod Ciu', 'Direcci�n', 'Telefono', 'Correo', 'Estado','Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'razon_social', index: 'razon_social', width: 130, align: 'left'},
                {name: 'documento', index: 'documento', width: 110, align: 'center'},   
                {name: 'dv', index: 'dv', width: 70, align: 'center'}, 
                {name: 'nomciu', index: 'nomciu', width: 180, align: 'left'},                                
                {name: 'pais', index: 'pais', width: 180, align: 'left', hidden:true},     
                {name: 'dpto', index: 'dpto', width: 180, align: 'left', hidden:true},
                {name: 'municipio', index: 'municipio', width: 180, align: 'left', hidden:true},    
                {name: 'direccion', index: 'direccion', width: 120, align: 'left'}, 
                {name: 'telefono', index: 'telefono', width: 90, align: 'center'},  
                {name: 'correo', index: 'correo', width: 150, align: 'left'},  
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden:true}, 
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}         
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_pagadurias'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            subGrid: true,   
            subGridOptions: { "plusicon" : "ui-icon-triangle-1-e",
                      "minusicon" :"ui-icon-triangle-1-s",
                      "openicon" : "ui-icon-arrowreturn-1-e",
                      "reloadOnExpand" : true,
                      "selectOnExpand" : true },
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext:null,
            pgbuttons:false,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data:{
                       opcion: 5
                     }
            },   
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_pagadurias").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_pagadurias").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoPagaduria('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_pagadurias").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_pagadurias"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;
                                 
                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                     editarPagaduria(id);
                }
                    
          },
          subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id = subgrid_id+"_t"; 
                jQuery("#"+subgrid_id).html("<table id='"+subgrid_table_id+"'></table>");
                jQuery("#"+subgrid_table_id).jqGrid({
                    url: './controller?estado=Maestro&accion=Libranza',                  
                    datatype: 'json',
                    colNames: ['Id', 'IdPagaduria','Razon Social','Nit', 'DV', 'Telefono', 'Direcci�n', 'Estado','Activar/Inactivar'],
                    colModel: [
                        {name: 'id', index: 'id', sortable: true, width: 60, align: 'center', hidden:true, key:true},
                        {name: 'id_pagaduria', index: 'id_pagaduria', sortable: true, align: 'left',  width: 60, hidden:true},   
                        {name: 'razon_social', index: 'razon_social', sortable: true, align: 'left',  width: 318},                          
                        {name: 'nit_empresa', index: 'nit_empresa', width: 150, align: 'center'},  
                        {name: 'dv', index: 'dv', width: 90, align: 'center', hidden:true}, 
                        {name: 'telefono', index: 'telefono', width: 150, align: 'center'},  
                        {name: 'direccion', index: 'direccion', width: 165, align: 'left'},  
                        {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden:true}, 
                        {name: 'cambio', index: 'cambio', width: 90, align: 'center'}         
                    ],
                    rowNum:150,
                    height:'80%',
                    width:'90%',
                    cellEdit:true,  
                    cellsubmit: "clientArray",
                    jsonReader: {
                        root: 'rows',
                        cell:'',
                        repeatitems: false,
                        id: '0'
                    },     
                    ajaxGridOptions: {                        
                        dataType: "json",
                        type: "POST",                     
                        data: {
                            opcion: 69,
                            id_pagaduria:row_id
                        }
                    },     
                    gridComplete: function () {
                        var cant = jQuery("#"+subgrid_table_id).jqGrid('getDataIDs');
                        for (var i = 0; i < cant.length; i++) {
                            var cambioEstado = $("#"+subgrid_table_id).getRowData(cant[i]).cambio;
                            var cl = cant[i];
                            be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoEmpresaAsociada('" + subgrid_table_id + "','" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                            jQuery("#"+subgrid_table_id).jqGrid('setRowData', cant[i], {cambio: be});

                        }
                    },
                    ondblClickRow: function (rowid, iRow, iCol, e) {
                        var myGrid = jQuery("#"+subgrid_table_id), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                        filas = myGrid.getRowData(selRowIds);  
                        var reg_status = filas.reg_status;                       
                        if (reg_status === 'A') {
                            mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                        } else {
                            editarEmpresaAsociada(subgrid_table_id,rowid);
                        }

                    },
                    loadError: function(xhr, status, error) {
                        alert(error);
                    }
                });               
            }
        }).navGrid("#page_tabla_pagadurias", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_pagadurias").jqGrid("navButtonAdd", "#page_tabla_pagadurias", {
            caption: "Nuevo",
            onClickButton: function() {               
                crearPagaduria();
            }
        });
        jQuery("#tabla_pagadurias").jqGrid("navButtonAdd", "#page_tabla_pagadurias", {
            caption: "Empresas Asociadas",    
            title: "Agregar empresa asociada a pagaduria",   
            onClickButton: function() {               
               var myGrid = jQuery("#tabla_pagadurias"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
               filas = myGrid.jqGrid("getLocalRow", selRowIds);              
                if (selRowIds != null) {                   
                    var id_pagaduria = filas.id;                   
                    crearEmpresaAsociada(id_pagaduria);
                }else{
                    mensajesDelSistema("No hay ninguna pagadur�a seleccionada", '250', '150');
                }  
            }
        });
    }  
       
}

function refrescarGridPagadurias(){   
    jQuery("#tabla_pagadurias").setGridParam({
        url: "./controller?estado=Maestro&accion=Libranza",
        datatype: 'json',
        ajaxGridOptions: {       
            type: "POST",          
            data: {
                opcion: 5
            }
        }       
    });
    
    jQuery('#tabla_pagadurias').trigger("reloadGrid");
}

function crearPagaduria(){   
    $('#div_pagaduria').fadeIn('slow');
    DeshabilitarControles(false);
    resetearValores();
    setDireccion(0);
    AbrirDivCrearPagaduria();
}

function AbrirDivCrearPagaduria(){
      $("#div_pagaduria").dialog({
        width: 'auto',
        height: 350,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR PAGADURIA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {         
                guardarPagaduria();             
            },
            "Salir": function () {    
                DeshabilitarControles(false);
                resetearValores();
                setDireccion(0);
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_pagaduria").parent().find(".ui-dialog-titlebar-close").hide();
}

function guardarPagaduria(){  
    var documento = $('#documento').val();
    var dv = $('#digito_verificacion').val();
    var razon_social = $('#razonsocial').val();  
    var ciudad = $('#ciudad').val();
    var direccion = $('#direccion').val();
    var telefono = $('#telefono').val();
    var email = $('#correo').val();
    var url = './controller?estado=Maestro&accion=Libranza';
    if(documento!=='' && razon_social!=='' && ciudad!=='' && direccion!=='' && telefono!=='' && email!==''){
        if(!validarTelefono(telefono))
        {
            mensajesDelSistema("Por favor, ingrese un numero de telefono valido", '250', '150'); 
        }else if(!validarEmail(email)){
            mensajesDelSistema("El email ingresado es incorrecto. Por favor, Verifique", '250', '150');       
        }else{
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        opcion: 6 ,                
                        documento: documento,
                        digito_verificacion:dv,
                        razon_social: razon_social,
                        ciudad: ciudad,
                        direccion: direccion,
                        telefono: telefono,
                        email: email,
                        verificacion:$('#verificacion').val()
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '270', '165');                          
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                resetearValores();
                                refrescarGridPagadurias();  
                                mensajesDelSistema("Se cre&oacute; la pagaduria", '250', '150', true);

                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo crear la pagaduria!!", '250', '150');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                              "Message: " + xhr.statusText + "\n" +
                              "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                }); 
            },500);
        }
    }else{
       mensajesDelSistema("FALTAN CAMPOS POR LLENAR", '250', '150');
    }
}

function editarPagaduria(cl){
    
    $('#div_pagaduria').fadeIn("slow");
    var fila = jQuery("#tabla_pagadurias").getRowData(cl);  
    var razon_social = fila['razon_social'];
    var documento = fila['documento'];
    var dv = fila['dv'];
    var direccion = fila['direccion'];
    var telefono = fila['telefono'];
    var email = fila['correo'];
    var pais = fila['pais'];
    var dpto = fila['dpto'];    
    var ciudad = fila['municipio'];  
    var reg_status = fila['reg_status'];  
    if (reg_status==="A"){
        DeshabilitarControles(true);
    }else{
        DeshabilitarControles(false);
    }  
    $('#id').val(cl);
    $('#razonsocial').val(razon_social);
    $('#documento').val(documento);
    $('#digito_verificacion').val(dv);
    $('#direccion').val(direccion);
    $('#telefono').val(telefono);
    $('#correo').val(email); 
    $('#pais').val(pais);
    cargarDepartamentos(pais, "departamento");
    $('#departamento').val(dpto);
    cargarCiudad(dpto,"ciudad");
    $('#ciudad').val(ciudad);    
    AbrirDivEditarPagaduria();
}

function AbrirDivEditarPagaduria(){
      $("#div_pagaduria").dialog({
        width: 'auto',
        height: 350,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR PAGADURIA',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              actualizarPagaduria();
            },
            "Salir": function () {
                DeshabilitarControles(false);
                resetearValores();
                setDireccion(0);
                $(this).dialog("destroy");
            }
        }
    });
    
    $("#div_pagaduria").parent().find(".ui-dialog-titlebar-close").hide();
}

function actualizarPagaduria(){
    var id =  $('#id').val();
    var documento = $('#documento').val();
    var dv = $('#digito_verificacion').val();
    var razon_social = $('#razonsocial').val();  
    var ciudad = $('#ciudad').val();
    var direccion = $('#direccion').val();
    var telefono = $('#telefono').val();
    var email = $('#correo').val();  
    
    if(documento!=='' && razon_social!=='' && ciudad!=='' && direccion!=='' && telefono!=='' && email!==''){
        if(!validarTelefono(telefono))
        {
            mensajesDelSistema("Por favor, ingrese un numero de telefono valido", '250', '150'); 
        }else if(!validarEmail(email)){
            mensajesDelSistema("El email ingresado es incorrecto. Por favor, Verifique", '250', '150');       
        }else{
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: "./controller?estado=Maestro&accion=Libranza",
                    data: {
                        opcion: 7,
                        id: id,
                        documento: documento,
                        digito_verificacion:dv,
                        razon_social: razon_social,
                        ciudad: ciudad,
                        direccion: direccion,
                        telefono: telefono,
                        email: email
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '270', '165');
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                refrescarGridPagadurias(); 
                                $("#div_pagaduria").dialog('close');
                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo actualizar la pagaduria!!", '250', '150');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                                "Message: " + xhr.statusText + "\n" +
                                "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                });
             },500);
        }
    }else{
         mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');      
    }  
}


function CambiarEstadoPagaduria(rowid){
    var grid_tabla = jQuery("#tabla_pagadurias");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 8,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarGridPagadurias();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado de la pagaduria!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}



function cargarPaises() {
  
    $.ajax({
        type: 'POST',
        async:false,
        url: "./controller?estado=Archivo&accion=Asobancaria",
        dataType: 'json',
        data: {
            opcion: 12
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#pais').append("<option value=''>Seleccione</option>"); 
                    $('#paisRO').append("<option value=''></option>"); 

                    for (var key in json) {
                        $('#pais').append('<option value=' + key + '>' + json[key] + '</option>');  
                        $('#paisRO').append('<option value=' + key + '>' + json[key] + '</option>');   
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarDepartamentos(codigo, combo) {
  if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 13,
                cod_pais: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#'+combo).append("<option value=''>Seleccione</option>");                 

                        for (var key in json) {
                            $('#'+combo).append('<option value=' + key + '>' + json[key] + '</option>');                      
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
  }
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function DeshabilitarControles(estado){      
        $('#razonsocial').attr({readonly: estado});
        $('#documento').attr({readonly: estado});
//      $('#direccion').attr({readonly: estado});
        $('#telefono').attr({readonly: estado});
        $('#correo').attr({readonly: estado});
        $('#pais').attr({disabled: estado});
        $('#departamento').attr({disabled: estado});
        $('#ciudad').attr({disabled: estado});              
}


function resetearValores(){  
    $('#id').val('');
    $('#razonsocial').val('');
    $('#documento').val('');
    $('#digito_verificacion').val('');
    $('#direccion').val('');
    $('#telefono').val('');
    $('#correo').val('');  
    $('#pais').val('');
    $('#departamento').empty();
    $('#ciudad').empty(); 
}

function CalcularDv(documentId,dvId) {
    var vpri, x, y, z, i, nit1, dv1;
    nit1 = document.getElementById(documentId).value;
    if (isNaN(nit1)) {
        //document.form1.dv.value="X";
        alert('El valor digitado no es un numero valido');
    } else {
        vpri = new Array(16);
        x = 0;
        y = 0;
        z = nit1.length;
        vpri[1] = 3;
        vpri[2] = 7;
        vpri[3] = 13;
        vpri[4] = 17;
        vpri[5] = 19;
        vpri[6] = 23;
        vpri[7] = 29;
        vpri[8] = 37;
        vpri[9] = 41;
        vpri[10] = 43;
        vpri[11] = 47;
        vpri[12] = 53;
        vpri[13] = 59;
        vpri[14] = 67;
        vpri[15] = 71;
        for (i = 0; i < z; i++) {
            y = (nit1.substr(i, 1));
            //document.write(y+"x"+ vpri[z-i] +":");
            x += (y * vpri[z - i]);
            //document.write(x+"<br>");     
        }
        y = x % 11;
        //document.write(y+"<br>");
        if (y > 1) {
            dv1 = 11 - y;
        } else {
            dv1 = y;
        }
        //document.form1.dv.value=dv1;
        document.getElementById(dvId).value = dv1;
    }
}


function validarTelefono(phone) {
    var filter = /^[0-9-()+]+$/;
    return filter.test(phone);
}

function validarEmail(email) {
  // var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  var emailReg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  return emailReg.test( email );
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}


function AbrirDivDirecciones(){
      $("#direccion_dialogo").dialog({
        width: 550,
        height: 350,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'INGRESAR DIRECCION',
        closeOnEscape: false
    });
}

function Posicionar_div(id_objeto,e){
  obj = document.getElementById(id_objeto);
  var posx = 0;
  var posy = 0;
  if (!e) var e = window.event;
  if (e.pageX || e.pageY) {
   posx = e.pageX;
   posy = e.pageY;
  }
  else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
      }
      else
       alert('ninguna de las anteriores');

 obj.style.left = posx+'px';
 obj.style.top = posy+'px';
 obj.style.zIndex = 1300;

}

function genDireccion(elemento,e) {
  
    var contenedor = document.getElementById("direccion_dialogo")
      , res = document.getElementById("dir_resul");
     
   //$("#direccion_dialogo").draggable({ handle: "#drag_direcciones"});
   // Posicionar_div("direccion_dialogo",e); 
    contenedor.style.left = 515 + 'px';  
    contenedor.style.display = "block";

    $("#div_pagaduria").css({
        'width':'945px'
    });
    res.name = elemento;    
    res.value = (elemento.value) ? elemento.value : '';
    
}

function setDireccion(orden) {
    switch (orden) {
        default:
        case 3: 
            var res = document.getElementById('dir_resul')
              , des = document.getElementById(res.name);
              des.value = res.value;
        case 0:
            jQuery('#dir_resul').val("");
            jQuery('#via_princip_dir').val("");
            jQuery('#nom_princip_dir').val("");
            jQuery('#via_genera_dir').val("");
            jQuery('#nom_genera_dir').val("");
            jQuery('#placa_dir').val("");
            jQuery('#cmpl_dir').val("");
            
            document.getElementById("direccion_dialogo").style.display = "none";
            $("#div_pagaduria").css({
                'width': 'auto'
            });
            break;
        case 2:
            var p = jQuery('#via_princip_dir').val()
              , g = document.getElementById('via_genera_dir')
              , opcion;
            for (var i = 0; i < g.length; i++) {
                
                opcion = g[i];
                
                if (opcion.value === p) {
                    
                    opcion.style.display = 'none';
                    opcion.disabled = true;
                    
                } else {
                    
                    opcion.disabled = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if(!jQuery('#via_princip_dir').val() || !jQuery('#nom_princip_dir').val()
                || !jQuery('#via_genera_dir').val() || !jQuery('#nom_genera_dir').val()
                || !jQuery('#placa_dir').val() ) {
                  jQuery('#dir_resul').val("");
                  jQuery('#dir_resul').attr("class","validation-failed");
              } else {
                jQuery('#dir_resul').removeAttr("class");
                jQuery('#dir_resul').val(
                    jQuery('#via_princip_dir option:selected').text()
                   + ' '  + jQuery('#nom_princip_dir').val().trim().toUpperCase()
                   + ' '  + jQuery('#via_genera_dir option:selected').text().trim()
                   + ' '  + jQuery('#nom_genera_dir').val().trim().toUpperCase()
                   + ((jQuery('#via_genera_dir option:selected').text().toUpperCase()==='#' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CALLE' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='CARRERA' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='DIAGONAL' || jQuery('#via_genera_dir option:selected').text().toUpperCase()==='TRANSVERSAL') ?  '-':  ' ')
                   + jQuery('#placa_dir').val().trim().toUpperCase()
                   + (!jQuery('#cmpl_dir').val() ? '' : ', ' + jQuery('#cmpl_dir').val().trim().toUpperCase()));
              }
            break;
    }
}


function cargarVias(codciu, combo) {
    //alert(codciu);

        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "/fintra/controller?estado=GestionSolicitud&accion=Aval",
            dataType: 'json',
            data: {
                opcion: 'cargarvias',
                ciu: codciu
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        alert(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''></option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        alert('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } 
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });

}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajeConfirmAction(msj, width, height, okAction, id) {  
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

 function resetAddressValues(){
       $("#dir_resul").val('');
       $("#nom_princip_dir").val('');
       $("#nom_genera_dir").val('');
       $("#placa_dir").val('');
       $("#cmpl_dir").val('');
       $("#via_princip_dir").val('');
       $("#via_genera_dir").val(''); 
 }

function verificarNit(){
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion:62,
            documento: $('#documento').val()
        },
        success: function (json) {
         //alert (json.respuesta);
         $('#verificacion').val(json.respuesta);
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function crearEmpresaAsociada(id_pagaduria){   
    $('#div_empresa_asociada').fadeIn('slow');
    $('#id_pagaduria').val(id_pagaduria);   
    resetearValoresEmpresaAsociada();  
    AbrirDivCrearEmpresaAsociada();
}

function AbrirDivCrearEmpresaAsociada(){
      $("#div_empresa_asociada").dialog({
        width: 'auto',
        height: 250,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR EMPRESA ASOCIADA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
                guardarEmpresaAsociada('insert');             
            },
            "Salir": function () {                   
                resetearValoresEmpresaAsociada();  
                setDireccion2(0);
                $(this).dialog("destroy");
            }
        }
    });
    
}

function editarEmpresaAsociada(tableid, cl){
    
    $('#div_empresa_asociada').fadeIn("slow");
    var fila = jQuery("#"+tableid).getRowData(cl);  
    var id_pagaduria = fila['id_pagaduria'];
    var razon_social = fila['razon_social'];
    var documento = fila['nit_empresa'];  
    var dv = fila['dv'];  
    var telefono = fila['telefono'];
    var direccion = fila['direccion']; 
    var reg_status = fila['reg_status'];  
    $('#id_empresa').val(cl);
    $('#id_pagaduria').val(id_pagaduria);
    $('#razon_social').val(razon_social);
    $('#nit_empresa').val(documento);    
    $('#digito_verificacion_emp').val(dv);    
    $('#telefono_empresa').val(telefono);
    $('#direccion_empresa').val(direccion);   
    AbrirDivEditarEmpresaAsociada();
}

function AbrirDivEditarEmpresaAsociada(){
      $("#div_empresa_asociada").dialog({
        width: 'auto',
        height: 250,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR EMPRESA ASOCIADA',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              guardarEmpresaAsociada('update');
            },
            "Salir": function () {               
                resetearValoresEmpresaAsociada(); 
                setDireccion2(0);
                $(this).dialog("destroy");
            }
        }
    });    
}

function guardarEmpresaAsociada(action){  
    var id = $('#id_empresa').val();
    var id_pagaduria = $('#id_pagaduria').val();
    var nit_empresa = $('#nit_empresa').val(); 
    var digito_verificacion = $('#digito_verificacion_emp').val(); 
    var razon_social = $('#razon_social').val();
    var telefono = $('#telefono_empresa').val();
    var direccion = $('#direccion_empresa').val();
    var url = './controller?estado=Maestro&accion=Libranza';
    if(nit_empresa!=='' && razon_social!=='' && telefono!=='' && direccion!==''){
        if(!validarTelefono(telefono))
        {
            mensajesDelSistema("Por favor, ingrese un numero de telefono valido", '250', '150'); 
        }else{
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        opcion: (action==='insert') ? 70: 71,     
                        id:id,
                        id_pagaduria:id_pagaduria,
                        documento: nit_empresa,         
                        digito_verificacion:digito_verificacion,
                        razon_social: razon_social,                        
                        telefono: telefono,
                        direccion: direccion
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '270', '165');                          
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                resetearValoresEmpresaAsociada();
                                refrescarGridPagadurias();  
                                (action === 'update') ? $("#div_empresa_asociada").dialog('destroy'): mensajesDelSistema("Empresa asociada satisfactoriamente a la pagadur�a", '270', '165', true);
                               
                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo crear la empresa asociada!!", '270', '165');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                              "Message: " + xhr.statusText + "\n" +
                              "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                }); 
            },500);
        }
    }else{
       mensajesDelSistema("FALTAN CAMPOS POR LLENAR", '250', '150');
    }
}

function CambiarEstadoEmpresaAsociada(tableid,rowid){
    var grid_tabla = jQuery("#"+tableid);
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        async:false,
        url: "./controller?estado=Maestro&accion=Libranza",
        data: {
            opcion: 72,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarGridPagadurias();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado de la empresa asociada!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function resetearValoresEmpresaAsociada(){  
    $('#id_empresa').val('');
    $('#razon_social').val('');
    $('#nit_empresa').val('');   
    $('#digito_verificacion_emp').val('');   
    $('#telefono_empresa').val('');
    $('#direccion_empresa').val('');  
}

function genDireccion2(elemento,e) {
  
    var contenedor = document.getElementById("direccion_dialogo_emp")
      , res = document.getElementById("dir_resul_emp");
     
   //$("#direccion_dialogo").draggable({ handle: "#drag_direcciones"});
   // Posicionar_div("direccion_dialogo",e); 
    contenedor.style.left = 515 + 'px';  
    contenedor.style.display = "block";

    $("#div_empresa_asociada").css({
        'width':'950px',
        'height':'270px'
    });
    res.name = elemento;    
    res.value = (elemento.value) ? elemento.value : '';
    
}

function setDireccion2(orden) {
    switch (orden) {
        default:
        case 3: 
            var res = document.getElementById('dir_resul_emp')
              , des = document.getElementById(res.name);
              des.value = res.value;
        case 0:
            jQuery('#dir_resul_emp').val("");
            jQuery('#via_princip_dir_emp').val("");
            jQuery('#nom_princip_dir_emp').val("");
            jQuery('#via_genera_dir_emp').val("");
            jQuery('#nom_genera_dir_emp').val("");
            jQuery('#placa_dir_emp').val("");
            jQuery('#cmpl_dir_emp').val("");
            
            document.getElementById("direccion_dialogo_emp").style.display = "none";
            $("#div_empresa_asociada").css({
                'width': 'auto',
                'height':'165px'                
            });
            break;
        case 2:
            var p = jQuery('#via_princip_dir_emp').val()
              , g = document.getElementById('via_genera_dir_emp')
              , opcion;
            for (var i = 0; i < g.length; i++) {
                
                opcion = g[i];
                
                if (opcion.value === p) {
                    
                    opcion.style.display = 'none';
                    opcion.disabled = true;
                    
                } else {
                    
                    opcion.disabled = false;
                    opcion.style.display = 'block';

                }
            }
        case 1:
            if(!jQuery('#via_princip_dir_emp').val() || !jQuery('#nom_princip_dir_emp').val()
                || !jQuery('#via_genera_dir_emp').val() || !jQuery('#nom_genera_dir_emp').val()
                || !jQuery('#placa_dir_emp').val() ) {
                  jQuery('#dir_resul_emp').val("");
                  jQuery('#dir_resul_emp').attr("class","validation-failed");
              } else {
                jQuery('#dir_resul_emp').removeAttr("class");
                jQuery('#dir_resul_emp').val(
                    jQuery('#via_princip_dir_emp option:selected').text()
                   + ' '  + jQuery('#nom_princip_dir_emp').val().trim().toUpperCase()
                   + ' '  + jQuery('#via_genera_dir_emp option:selected').text().trim()
                   + ' '  + jQuery('#nom_genera_dir_emp').val().trim().toUpperCase()
                   + ((jQuery('#via_genera_dir_emp option:selected').text().toUpperCase()==='#' || jQuery('#via_genera_dir_emp option:selected').text().toUpperCase()==='CALLE' || jQuery('#via_genera_dir_emp option:selected').text().toUpperCase()==='CARRERA' || jQuery('#via_genera_dir_emp option:selected').text().toUpperCase()==='DIAGONAL' || jQuery('#via_genera_dir_emp option:selected').text().toUpperCase()==='TRANSVERSAL') ?  '-':  ' ')
                   + jQuery('#placa_dir_emp').val().trim().toUpperCase()
                   + (!jQuery('#cmpl_dir_emp').val() ? '' : ', ' + jQuery('#cmpl_dir_emp').val().trim().toUpperCase()));
              }
            break;
    }
}

