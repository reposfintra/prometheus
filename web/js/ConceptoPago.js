var BASEURL = '';
var CONTROLLER = '';

function obtenerImagen(ruta){
	return ruta + document.formularioNuevo.Opcion.value + ".gif";
}        
function _onmousemove(item) { item.className='fila';   }
function _onmouseout (item) { item.className='fila'; }
function addOption(Comb,valor,texto){
		var Ele = document.createElement("OPTION");
		Ele.value=valor;
		Ele.text=texto;
		Comb.add(Ele);
}    
function loadCombo(datos, cmb){
	cmb.length = 0;
	for (i=0;i<datos.length;i++){
		var dat = datos[i].split(SeparadorJS);
		addOption(cmb, dat[0], dat[1]);
	}
}
function loadCombo2(datos, datosA ,cmbA, cmbNA){
	cmbA.length = 0;
	cmbNA.length = 0;
	for (i=0;i<datos.length;i++){
		var dat = datos[i].split(SeparadorJS);                
		if (datosA.indexOf(dat[0])==-1)
			addOption(cmbNA, dat[0], dat[1]);
		else
			addOption(cmbA , dat[0], dat[1]);
	}
}        
function move(cmbO, cmbD){
   for (i=cmbO.length-1; i>=0 ;i--)
	if (cmbO[i].selected){
	   addOption(cmbD, cmbO[i].value, cmbO[i].text)
	   cmbO.remove(i);
	}
  // order(cmbD);
}
function moveAll(cmbO, cmbD){
   for (i=cmbO.length-1; i>=0 ;i--){
	   addOption(cmbD, cmbO[i].value, cmbO[i].text)
	   cmbO.remove(i);
   }
}
function order(cmb){
   for (i=0; i<cmb.length;i++)
	 for (j=i+1; j<cmb.length;j++)
		if (cmb[i].text > cmb[j].text){
			var temp = document.createElement("OPTION");
			temp.value = cmb[i].value;
			temp.text  = cmb[i].text;
			cmb[i].value = cmb[j].value;
			cmb[i].text  = cmb[j].text;
			cmb[j].value = temp.value;
			cmb[j].text  = temp.text;
		}
}
function selectAll(cmb){
	for (i=0;i<cmb.length;i++)
	  cmb[i].selected = true;
}

function validarFormulario (tform){
	if (tform.Nombre.value==''){
		alert('Debe indicar el nombre de la Opcion para poder continuar.');
		return false;
	}
	
	if (!tform.TipoOpcion[0].checked && tform.Parametros.value==''){
		alert('Debe indicar los Parametros, para poder continuar.');
		return false;            
	}
	selectAll (tform.UsuariosA);
	selectAll (tform.GruposA  );
	return true;
}        
function selAll(){
	for(i=0;i<FormularioListado.length;i++)
	   FormularioListado.elements[i].checked=FormularioListado.All.checked;
}
function actAll(){
	FormularioListado.All.checked = true;
	for(i=0;i<FormularioListado.length;i++)	
	  if (FormularioListado.elements[i].type=='checkbox' && !FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All'){
		  FormularioListado.All.checked = false;
		  break;
	  }
} 
function validarListado(form){
	for(i=0;i<FormularioListado.length;i++)	
		if (FormularioListado.elements[i].type=='checkbox' && FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All')
			return true;
	alert('Por favor seleccione un item para poder continuar');
	return false;
}   

function objetoOpcion(id, nombre, idFolder, refer1, refer2, nivel, refer3, refer4, combo){
	this.id         = id;
	this.nombre     = nombre;
	this.idFolder   = idFolder;
	this.refer1     = refer1;
	this.refer2     = refer2;
        this.refer3     = refer3;
	this.refer4     = refer4;
	this.nivel      = nivel;
        this.combo      = combo;
	return this;
}

function edicion (idObj, edit){	
	var op = eval('op'+idObj);
	with(formularioNuevo){
	   Id.value         = op.id;
	   Nombre.value     = op.nombre;
	   idFolder.value   = op.idFolder;
	   ref1.value       = op.refer1;
	   ref2.value       = op.refer2;
           ref3.value       = op.refer3;
           ref4.value       = op.refer4;
	   nivel.value      = op.nivel;           
	   idFolder[(op.idFolder=='N'?1:0)].checked = true;   
	   cambiarLista();
           if( idFolder1.checked ){
                nombres.value = op.nombre;                
           }else{
                nombres.value = op.combo;
           }
	   if (edit){
		   movera.disabled=false;
		   moveradestino.disabled=false;
		   Opcion.value='modificar';
		   botonVariable.src = BASEURL+"/images/botones/modificar.gif";			   		   
		   if(op.idFolder=='Y'){
			  idFolder1.disabled = true;
			  idFolder2.disabled = true;
		   }
		   index.value = idObj;
	   }
	   else{
		   movera.disabled=true;
		   moveradestino.disabled=true;
		   Opcion.value='agregar';
		   botonVariable.src = BASEURL+"/images/botones/agregar.gif";	   
		   idFolder1.disabled = false;
		   idFolder2.disabled = false;
		   index.value = '';		   
	   }
	   
	}
	formNuevo.style.visibility='visible';	
}

function goYaNIN (idObj){        
	var op = eval('op'+idObj);
	var url = CONTROLLER + '?estado=ConceptoPago&accion=Entrar&pagina=conceptosindex.jsp&carpeta=jsp/cxpagar/Concepto_Pagos&Opcion=cargarvistafolder&index=' + idObj +'&codigo='+ op.id; 
	document.location.href = url;
}

function moveradest(form){            
	if(formularioNuevo.mover.value=='false' || formularioNuevo.mover.value==''){                
		formularioNuevo.mover.value='true';                
	}
	else{
		formularioNuevo.mover.value='false';                
	}
}
function eliminar(form){
	FormularioListado.Opcion.value='Eliminar';
	FormularioListado.submit();
}
function botonAgregar(form){	
	form.movera.disabled=true;
	form.moveradestino.disabled=true;
	formNuevo.style.visibility='visible';
	form.Opcion.value='agregar';
	
}
function colocarInfo( valuet ){
    if( valuet != "" ){
        var valores = valuet.split(";");
        formularioNuevo.Nombre.value = valores[ 0 ];
        formularioNuevo.ref1.value = valores[ 1 ];
        formularioNuevo.ref2.value = valores[ 2 ];                
    }
}
function cambiarLista(){    
    if( formularioNuevo.idFolder1.checked ){        
        seleccion.innerHTML = soloNombres.innerHTML;
        formularioNuevo.ref1.readOnly = false;
        formularioNuevo.ref2.readOnly = false;
    }else{
        seleccion.innerHTML = listaNombres.innerHTML;
        formularioNuevo.ref1.readOnly = true;
        formularioNuevo.ref2.readOnly = true;
    }            
}

function validar(frm){
     
    if(frm.nombres.value == ''){
        alert('El nombre no debe estar vacio'); 
   
    }else{
      frm.submit();
    }

}


