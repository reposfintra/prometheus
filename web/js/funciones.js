function validarProveedorEscolta() {
	if (form1.nit.value=="") {
    	alert("Debe digitar el nit del escolta");
	    form1.nit.focus();       
        return false;
    } else if (form1.origen.value=="") {
    	alert("Debe seleccionar el origen");
	    form1.origen.focus();
        return false;
    } else if (form1.destino.value=="") {
    	alert("Debe seleccionar el destino");
	    form1.destino.focus();
        return false;
    } else if (form1.tarifa.value=="") {
    	alert("Debe digitar el valor de la tarifa");
	    form1.tarifa.focus();
        return false;
    } else if (form1.codigo.value=="") {
    	alert("Debe digitar el codigo contable");
	    form1.codigo.focus();
        return false;
    } else {
       form1.submit();
    }
}
function validarBuscar(){
    if (form1.nit.value=="") {
    	alert("Debe digitar el nit del escolta");
	    form1.nit.focus();       
        return false;
    } else {
        form1.submit();
    }  
}
var isIE = document.all?true:false;
var isNS = document.layers?true:false;
function soloDigitos(e,decReq) {
    var key = (isIE) ? window.event.keyCode : e.which;
    var obj = (isIE) ? event.srcElement : e.target;
    var isNum = (key > 47 && key < 58) ? true:false;
    var dotOK =  (decReq=='decOK' && key ==46 && obj.value.indexOf('.')==-1) ? true:false;
    window.event.keyCode = (!isNum && !dotOK && isIE) ? 0:key;
    e.which = (!isNum && !dotOK && isNS) ? 0:key;   
    return (isNum || dotOK );
}

function validarCamposLlenos( formulario ){
	for (i = 0; i < formulario.elements.length; i++){
		if (formulario.elements[i].value == ""){
		  alert("No se puede procesar la informaci�n. Verifique que todos lo campos esten llenos.");
		  formulario.elements[i].focus();
		  return (false);
	  	}
	}
	return (true);
}

//Jose 15.12.05
function validarCamposLlenos( formulario ){
	for (i = 0; i < formulario.elements.length; i++){
		if (formulario.elements[i].value == ""){
		  alert("No se puede procesar la informaci�n. Verifique que todos lo campos esten llenos.");
		  formulario.elements[i].focus();
		  return (false);
	  	}
	}
	return (true);
}/*fin*/


/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller , baseurl 
 * proposito: validar identidad
 */
/*function validarIdentidad(CONTROLLER,BASEURL){
	
    var campos = new Array("c_tipdoc","c_doc","c_nom1", "c_ape1", "c_ape2", "c_genero", "c_fecha", "pais","est","ciudad" );
	var ncampos = new Array("Tipo Documento","Documento","Primer Nombre", "Primer Apellido", "Segundo Apellido", "Genero", "Fecha Nacimineto", "Pais","Estado","Ciudad" );

    var c_empleado = new Array("c_cargo" );
	var nempleado = new Array("Cargo" );
	
	var c_conductor = new Array("c_dir","c_tel11","c_tel12","c_tel13","c_ref");
	var nconductor = new Array("Direccion","Telefono","Telefono","Telefono","Referencia");

    var npropietario = new Array("Id mims","Direccion","Telefono","Telefono","Telefono","Referencia");
	var c_propietario = new Array("c_idmims","c_dir","c_tel11","c_tel12","c_tel13","c_ref");

    var nproveedor = new Array("c_tipdoc","Documento","Nombre","Direccion","Telefono","Telefono","Telefono");
	var c_proveedor = new Array("Tipo Documento","c_doc","c_nom","c_dir","c_tel11","c_tel12","c_tel13");
	
	var carbon = new Array("c_tipdoc","c_doc","c_nom");
	var ncarbon = new Array("Tipo Documento","Documento","Nombre");
    var campo = "";
	
     if (!forma.empleado.checked && !forma.conductor.checked && !forma.propietario.checked && !forma.proveedor.checked && !forma.procarbon.checked){
		alert("Debe seleccionar una Clasificaci�n!");
		return (false);
	}
	
    //valido campos comunes de empleado,conductor, propietario                   
	if (forma.empleado.checked || forma.conductor.checked || forma.propietario.checked){
    	for (i = 0; i < campos.length; i++){
      		campo = campos[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+ncampos[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
        if(forma.c_nom2.value == ''){
	    	forma.c_nom2.value = ' ';
    	}
		//validar si es femenino masculino debe llenar libmilitar
	    if(forma.c_genero.value == 'M' && forma.c_libreta.value == ''){
	    	alert("El Campo Libreta Militar esta vacio.!");
            forma.c_libreta.focus();
            return (false);
      	}

	}
	if (forma.empleado.checked){
    	for (i = 0; i < c_empleado.length; i++){
      		campo = c_empleado[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+nempleado[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	if (forma.conductor.checked){
    	for (i = 0; i < c_conductor.length; i++){
      		campo = c_conductor[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+nconductor[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	if (forma.propietario.checked){
    	for (i = 0; i < c_propietario.length; i++){
      		campo = c_propietario[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+npropietario[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	if (forma.proveedor.checked){
    	for (i = 0; i < c_proveedor.length; i++){
      		campo = c_proveedor[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+nproveedor[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	if (forma.procarbon.checked){
    	for (i = 0; i < carbon.length; i++){
      		campo = carbon[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+ncarbon[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	  
	  
      
      if(forma.c_tel21.value == '' && forma.c_tel22.value == '' && forma.c_tel23.value == ''){ 
   	     forma.c_tel21.value = ' ';
 	     forma.c_tel22.value = ' '; 
   	     forma.c_tel23.value = ' ';
      }
      else{
          if(forma.c_tel21.value == ''){ 
      		alert('Digite el codigo del pais');
        	forma.c_tel21.focus();
	  	    return (false);
      	  }
          if(forma.c_tel22.value == ''){ 
      		alert('Digite el codigo de l area');
	        forma.c_tel22.focus();
	  	    return (false);
          }  
      	  if(forma.c_tel23.value == ''){ 
      		alert('Digite el numero telefonico');
            forma.c_tel23.focus();
	  	    return (false);
          }
      } 
	  forma.submit();
}
/****************************
 * autor :Diogenes Bastidas
 * parametros:  controller , baseurl 
 * proposito: validar identidad
 */

/*function validarIdentidadMod(CONTROLLER,BASEURL){
	
	var campos = new Array("c_tipdoc","c_doc","c_nom1", "c_ape1", "c_ape2", "c_genero", "c_fecha", "pais","est","ciudad" );
	var ncampos = new Array("Tipo Documento","Documento","Primer Nombre", "Primer Apellido", "Segundo Apellido", "Genero", "Fecha Nacimineto", "Pais","Estado","Ciudad" );

    var c_empleado = new Array("c_cargo" );
	var nempleado = new Array("Cargo" );
	
	var c_conductor = new Array("c_dir","c_tel11","c_tel12","c_tel13","c_ref");
	var nconductor = new Array("Direccion","Telefono","Telefono","Telefono","Referencia");

    var npropietario = new Array("Id mims","Direccion","Telefono","Telefono","Telefono","Referencia");
	var c_propietario = new Array("c_idmims","c_dir","c_tel11","c_tel12","c_tel13","c_ref");

    var nproveedor = new Array("Documento","Nombre","Direccion","Telefono","Telefono","Telefono");
	var c_proveedor = new Array("c_doc","c_nom","c_dir","c_tel11","c_tel12","c_tel13");

	var carbon = new Array("c_doc","c_nom");
	var ncarbon = new Array("Documento","Nombre");
    var campo = "";

    if (!forma.empleado.checked && !forma.conductor.checked && !forma.propietario.checked && !forma.proveedor.checked && !forma.procarbon.checked){
		alert("Debe seleccionar una Clasificaci�n!");
		return (false);
	}
	
    //valido campos comunes de empleado,conductor, propietario                   
	if (forma.empleado.checked || forma.conductor.checked || forma.propietario.checked){
    	for (i = 0; i < campos.length; i++){
      		campo = campos[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+ncampos[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
        if(forma.c_nom2.value == ''){
	    	forma.c_nom2.value = ' ';
    	}
		//validar si es femenino masculino debe llenar libmilitar
	    if(forma.c_genero.value == 'M' && forma.c_libreta.value == ''){
	    	alert("El Campo Libreta Militar esta vacio.!");
            forma.c_libreta.focus();
            return (false);
      	}

	}
	if (forma.empleado.checked){
    	for (i = 0; i < c_empleado.length; i++){
      		campo = c_empleado[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+nempleado[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	if (forma.conductor.checked){
    	for (i = 0; i < c_conductor.length; i++){
      		campo = c_conductor[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+nconductor[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	if (forma.propietario.checked){
    	for (i = 0; i < c_propietario.length; i++){
      		campo = c_propietario[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+npropietario[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	if (forma.proveedor.checked){
    	for (i = 0; i < c_proveedor.length; i++){
      		campo = c_proveedor[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+nproveedor[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	if (forma.procarbon.checked){
    	for (i = 0; i < carbon.length; i++){
      		campo = carbon[i];
      		if (forma.elements[campo].value == ''){
            	alert("El campo "+ncarbon[i]+" esta vacio.!");
                forma.elements[campo].focus();
                return (false);
                break;
            }
        }
	}
	  
	  
      
      if(forma.c_tel21.value == '' && forma.c_tel22.value == '' && forma.c_tel23.value == ''){ 
   	     forma.c_tel21.value = ' ';
 	     forma.c_tel22.value = ' ';
   	     forma.c_tel23.value = ' ';
      }
      else{
          if(forma.c_tel21.value == ''){ 
      		alert('Digite el codigo del pais');
        	forma.c_tel21.focus();
	  	    return (false);
      	  }
          if(forma.c_tel22.value == ''){ 
      		alert('Digite el codigo de l area');
	        forma.c_tel22.focus();
	  	    return (false);
          }  
      	  if(forma.c_tel23.value == ''){ 
      		alert('Digite el numero telefonico');
            forma.c_tel23.focus();
	  	    return (false);
          }
      } 
	  forma.submit();
}
*/

function autorizaciones(CONTROLLER,BASEURL, tipo){
	var em;
	var con;
	var pro;
	var prove;
	var procar;
	if(!confirm ("Se necesita una autorizacion para hacer este cambio.\n�Desea escribirla ahora?")){
		if(confirm ("Usted debe solicitar una clave de autorizacion.\n�Desea solicitarla ahora?")){
			window.location= CONTROLLER+'?estado=Solicitar&accion=AutorizacionPlacaProp&nit='+forma.c_doc.value+'&nombre='+forma.c_nom.value+'&cmd='+tipo;
			//window.close();
		}
	}else{
		if(forma.empleado.checked){
			em = 'E';
		}
		else{
			em = '0';
		}
		if(forma.conductor.checked){
			con = 'C';
		}
		else{
			con = '0';
		}
		if(forma.propietario.checked){
			pro = 'P';
		}
		else{
			pro = '0';
		}
		if(forma.proveedor.checked){
			prove = 'V';
		}
		else{
			prove = '0';
		}
		if(forma.procarbon.checked){
			procar = 'C';
		}
		else{
			procar = '0';
		}
		forma.action = BASEURL+"/jsp/hvida/identidad/identidad.jsp?clave=ok&clas="+em+con+pro+prove+procar;
		forma.submit();
	}
}


/*function autorizaciones(CONTROLLER,BASEURL, tipo){
	var em;
	var con;
	var pro;
	var prove;
	var procar;
	if(!confirm ("Se necesita una autorizacion para hacer este cambio.\n�Desea escribirla ahora?")){
		if(confirm ("Usted debe solicitar una clave de autorizacion.\n�Desea solicitarla ahora?")){
			window.location= CONTROLLER+'?estado=Solicitar&accion=AutorizacionPlacaProp&nit='+forma.c_doc.value+'&nombre='+forma.c_nom.value+'&cmd='+tipo;
			//window.close();
		}
	}else{
		if(forma.empleado.checked){
			em = 'E';
		}
		else{
			em = '0';
		}
		if(forma.conductor.checked){
			con = 'C';
		}
		else{
			con = '0';
		}
		if(forma.propietario.checked){
			pro = 'P';
		}
		else{
			pro = '0';
		}
		if(forma.proveedor.checked){
			prove = 'V';
		}
		else{
			prove = '0';
		}
		if(forma.procarbon.checked){
			procar = 'C';
		}
		else{
			procar = '0';
		}
		forma.action = BASEURL+"/jsp/hvida/identidad/identidad.jsp?clave=ok&clas="+em+con+pro+prove+procar;
		forma.submit();
	}
} */

function campos(){
	if (!forma.empleado.checked && !forma.conductor.checked && !forma.propietario.checked && (forma.proveedor.checked || forma.procarbon.checked ) ) {
		nombre.style.display="block";
		titnom.style.display="none";
		nombres.style.display="none";
                apellidos.style.display="none";
		otros.style.display="none";
		gen.style.display="none";
		senal.style.display="none";
	} else {
		nombre.style.display="none";
		titnom.style.display="block";
		nombres.style.display="block";
                apellidos.style.display="block";
		otros.style.display="block";
		gen.style.display="block";
		senal.style.display="block";
	}
}
function cargarSelects ( dir ) {
	forma.action = dir;
	forma.submit();
}

/****************************
 * autor :Diogenes Bastidas
 * parametros:   
 * proposito: validar las cantidades del movimineto discrepancia
 */
 function validarCantidad() {
	if(forma.numpla.value==''){
		alert('Digite la Planilla');
		return false;
	}
	var items =document.getElementById("numItems");
	var j= parseInt(items.value);
	for(var i=0;i<j;i++){
		if (document.getElementById("candes"+i).value == ""){
		  alert("Las Cantidades No Pueden ser vacias");
		  document.getElementById("candes"+i).focus();
		  return false;
	  	}
		var v1=parseFloat(document.getElementById("candes"+i).value);
		var v2=parseFloat(document.getElementById("cant"+i).value);
		if(v1 > v2){
			alert ("La cantidad Despachada (" + v1 + ") no debe ser mayor a la Cantidad (" + v2 + ")");
			document.getElementById("candes"+i).focus();
			return false;
		}
	}
	forma.submit();
	
}
/* validacion de formulario ubicacion (dbastidas)
 * modificado 01.02.06 sescalante */
 function validarUbicacion(tipo, estado) {
	var campos;
	var ncampos;
	var nestado = (estado=='Mod')?"c_comboestado":"c_estado";
	if (tipo=="T"){
		campos = new Array("c_codigo","c_descripcion","c_tipou", "c_cia", "c_pais", nestado, "c_ciudad", "c_direccion","c_tel1", "c_tramo", "c_tiempo", "c_distancia", "modalidad" );
		ncampos = new Array("codigo","Descripcion","Tipo Ubicacion", "Compa�ia", "Pais", "Estado", "Ciudad", "Direccion","Telefono 1", "Tramo", "Tiempo", "Distancia", "Modalidad" );
	}else {
		campos = new Array("c_codigo","c_descripcion","c_tipou", "c_cia", "c_pais", nestado, "c_ciudad", "c_direccion","c_tel1", "modalidad");
		ncampos = new Array("codigo","Descripcion","Tipo Ubicacion", "Compa�ia", "Pais", "Estado", "Ciudad", "Direccion","Telefono 1", "Modalidad");
	}
   	for (i = 0; i < campos.length; i++){
    	campo = campos[i];
      	if (forma.elements[campo].value == ''){
       		alert("El campo "+ncampos[i]+" esta vacio.!");
            forma.elements[campo].focus();
            return (false);
            break;
        }
 	}
	forma.submit();
}

	/*script que registra datos de una pagina a otra en ubicacion(sescalante)*/
	function copiarCampoUbicacion(codigo1, campo1, codigo2, campo2){
		campo1.value=codigo1;
		campo2.value=codigo2;
 		campo1.focus();
	 	window.close();
	}


/*script que controla la muestra del bloque tramo segun la modalidad (sescalante)*/
function verTramo (){
	if (forma.modalidad.options(forma.modalidad.selectedIndex).value == "T"){ 
		tblTramo.style.display="block";
		forma.m.value = "T";
	}else if (forma.modalidad.options(forma.modalidad.selectedIndex).value == "C"){ 
		tblTramo.style.display="none";
		forma.m.value = "C";
	}
}

/*script que valida la insercion de los campos del formulario contacto (sescalante)*/
function validarFormContacto(tipo){
	var campos;
	var ncampos;
	if (tipo==".: Ingresar Contacto"){
		campos = new Array("c_codigo","c_cia","c_tipo");
		ncampos = new Array("Contacto Id","Compa�ia Id","Tipo de Contacto");
	}else{
		campos = new Array("c_tipo");
		ncampos = new Array("Tipo de Contacto");
	}
   	for (i = 0; i < campos.length; i++){
    	campo = campos[i];
      	if (forma.elements[campo].value == ''){
       		alert("El campo " + ncampos[i] + " esta vacio!");
            forma.elements[campo].focus();
            return (false);
            break;
        }
 	}
	forma.submit();
}

function validarFormTramo (){
	var campos = new Array("c_paiso","c_origen","c_paisd","c_destino",  "c_unidadc", "c_distancia", "c_unidadd", "c_duracion", "c_unidaddu" );
	var ncampos = new Array("Pais Origen","Origen","Pais Destino", "Destino",  "Unidad de Combustible", "Distancia", "Unidad de Distancia", "Duracion", "Unidad de Duracion");
	
	var camposNR = new Array("c_paiso","c_origen","c_paisd","c_destino");
	var ncamposNR = new Array("Pais Origen","Origen","Pais Destino", "Destino");
	
   	for (i = 0; i < campos.length; i++){
    	campo = campos[i];
      	if (forma.elements[campo].value == ''){
       		alert("El campo " + ncampos[i] + " esta vacio!");
            forma.elements[campo].focus();
            return (false);
            break;
        }
 	}

	for (j = 0; j < camposNR.length; j++){
		campoNR = camposNR[j];
      	if (forma.elements[campoNR].value == 'NR'){
       		alert(ncamposNR[j] + " no puede ser 'NO REGISTRADO'");
            forma.elements[campoNR].focus();
            return (false);
            break;
        }
 	}
		
	forma.submit();
}

function validarFormModificarTramo (){
	var campos = new Array( "c_unidadc", "c_distancia", "c_unidadd", "c_duracion", "c_unidaddu" );
	var ncampos = new Array( "Unidad de Combustible", "Distancia", "Unidad de Distancia", "Duracion", "Unidad de Duracion");
		
   	for (i = 0; i < campos.length; i++){
    	campo = campos[i];
      	if (forma.elements[campo].value == ''){
       		alert("El campo " + ncampos[i] + " esta vacio!");
            forma.elements[campo].focus();
            return (false);
            break;
        }
 	}
		
	forma.submit();
}
/* Script que adiciona una opci�n a un campo tipo select*/
/* Ing. Tito Andr�s Maturana */
function addOption(Comb,valor,texto){
	var Ele = document.createElement("OPTION");
	Ele.value=valor;
	Ele.text=texto;
	Comb.add(Ele);
}    
/* Script que llena dos campos tipo select - A�o y Mes, con lo valores correspondientes a los meses y a�os */
/* Ing. Tito Andr�s Maturana */
function Llenar(CmbAnno, CmbMes){
	var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	var FechaAct = new Date();
	//CmbAnno.length = 0;
	//CmbMes.length  = 0;
	if( CmbAnno!=null ){
		for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
			CmbAnno.value = FechaAct.getYear();
	}
	
	if( CmbMes!=null ){
		for (i=0;i<Meses.length;i++)
			if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
			else          addOption(CmbMes,(i+1),Meses[i]);                
		CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);	
	}
} 
/* Script que adiciona una opci�n a un campo tipo select*/
/* Ing. Tito Andr�s Maturana */
function addOption(Comb,valor,texto){
	var Ele = document.createElement("OPTION");
	Ele.value=valor;
	Ele.text=texto;
	Comb.add(Ele);
}    
/* Script que llena dos campos tipo select - A�o y Mes, con lo valores correspondientes a los meses y a�os */
/* Ing. Tito Andr�s Maturana */
function Llenar(CmbAnno, CmbMes){
	var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	var FechaAct = new Date();
	//CmbAnno.length = 0;
	//CmbMes.length  = 0;        
	if( CmbAnno!=null ){
		for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
			CmbAnno.value = FechaAct.getYear();
	}
	
	if( CmbMes!=null ){
		for (i=0;i<Meses.length;i++)
			if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
			else          addOption(CmbMes,(i+1),Meses[i]);                
		CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);	
	}
} 
/* Script que formate un campo tipo input a formato moneda - miles separados por ','*/
/* Ing. Tito Andr�s Maturana */
function formatear(objeto){
	if( objeto.value!='' ){
	    var n= parseFloat(objeto.value.replace(/,/g,''));
    	objeto.value=formatearNumero(n);
	}
}
/* Script que formate un campo tipo input a formato moneda - miles separados por ','*/
/* Ing. Tito Andr�s Maturana */	
function formatearNumero(n){
    // para estar seguros que vamos a trabajar con un string
    var valor = ""+n;
    var res = "";
    for( var i=valor.length-1,j = 0; i >= 0; i--,j++ ){
           if ( j % 3 == 0 && j != 0 ){
                  res += ",";
           }
           res += valor.charAt(i);
    }
    //ahora nos toca invertir el numero;
    var aux = "";
    for( var i=res.length-1; i >= 0; i-- ){
           aux += res.charAt(i);
    }
    return aux;
}
/* Script que toma un periodo en AAAAMM y lo formatea*/
/* Ing. Tito Andr�s Maturana */	
function format(str){	
	var meses = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	var salida = '';
	salida = meses[parseInt(str.substring(4, 6)) - 1] + ' ' + str.substring(0, 4);
	document.writeln(salida.toUpperCase());   							
}
/* Script que selecciona un valor dentro de un select */
/* Ing. Tito Andr�s Maturana */
function seleccionar(cmb, key) {
   for( var i=0; i<cmb.length; i++){
		if( cmb[i].value == key ){
			cmb[i].selected = true;
			break;
		}
	}
}
/*Script jose Validacion general x form*/
function validarCamposFormulario(formularo){
	for (i = 0; i < formulario.elements.length; i++){
		if (formulario.elements[i].value == ""){
		  alert("No se puede procesar la informaci�n. Verifique que todos lo campos esten llenos.");
		  formulario.elements[i].focus();
		  return (false);
	  	}
	}
	formulario.submit();
}
function validarbusqueda(){
    var fecha1 = document.forma.c_fecha_inicio.value.replace('-','').replace('-','');
   	var fecha2 = document.forma.c_fecha_fin.value.replace('-','').replace('-','');
	if (forma.c_placa.value == ''){
		alert("Digite la Placa");
    }
	else{ 
    	if ( forma.c_fecha_inicio.value == '' && forma.c_fecha_fin.value ==''){
			if(fecha1>fecha2) {     
		   		alert('La fecha final debe ser mayor que la fecha inicial');
		    }
            else
				forma.submit();
		}
		else if ( forma.c_fecha_inicio.value != '' && forma.c_fecha_fin.value !=''){
			if(fecha1>fecha2) {     
		   		alert('La fecha final debe ser mayor que la fecha inicial');
		    }
            else
                forma.submit();
		}
		else{
			alert("Se deben Digitar las 2 Fecha ");
		}
	}
}


/*script q cuenta los puntos decimales en un  numero*/
function  contarPDecimales(val){
	var cad = val;
    var n = 0;
    for (i=0;i<cad.length;i++){
    	var car = cad.charAt(i);
        if(car == '.')
        	n++;
   	}	
   	return n;
}
/*script q da formato a  los campos numericos en tramo*/ 
function formatValoresTramo (val){	
	var v = val.value;
	
	if ( v.indexOf( '.' ) == 0){
		alert('Error...El primer digito no puede ser un . (punto)');
		val.focus();		
	}
 	else if ( ( v != "" ) && (v.indexOf( '.' ) == -1) ){ 		
		if (v.length == 5){
			v = v.substr(0,4) + '.' + v.substr(4,5) ;
		}
		else if (v.length < 5){
			v = v + '.0';			
		}
		val.value = v;
	}
	else if ( ( v != "" ) && (contarPDecimales(v) > 1) ){			
		alert('Error...El numero tiene ' + contarPDecimales(v) + ' puntos decimales');	
		val.focus();	
	}	
	return;
}
/*script que resetea los campos tipo texto de un formulario*/
function borrarTexto(formulario){
	if ( formulario == null ) {
        return;
    }
    var i;
    for ( i=0; i< formulario.elements.length; i++ ) {
        var el = formulario.elements[i];
        if ( el.type == "text" ) 
            el.value = "";        
    }
    return;
}
function validarUsuario(){
  var campos = new Array("agencia","tabla","usuario");
  for (i = 0; i < campos.length; i++){
   		campo = campos[i];
   		if (forma.elements[campo].value == ''){
       	  alert("El campo "+campos[i]+" esta vacio.!");
          forma.elements[campo].focus();
          return (false);
          break;
        }
  }
  forma.submit();
}
