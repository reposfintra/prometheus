/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 $(document).ready(function() { 
     
    $('.solo-numero').keyup(function() {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });    
    
    $('.solo-numeric').live('keypress', function(event) {
        return numbersonly(this, event);
    });
    
    $('.solo-numeric').live('blur',function (event) {
           this.value = numberConComas(this.value);            
    });   
    
    $('#btn_actualizarEtapa').click(function() {
        actualizarEtapa();
    });
    
    $('#tipo_costo').change(function() {
        if (this.value == 'P') {
            $('#money').fadeOut('fast');
            $('#percent').fadeIn('slow');
            $('#lbl_valor').fadeIn('slow');
            $('#valor').fadeIn('slow');
        } else if (this.value == 'M') {
            $('#percent').fadeOut('fast');
            $('#money').fadeIn('slow');
            $('#lbl_valor').fadeIn('slow');
            $('#valor').fadeIn('slow');
        }else {
            $('#percent').fadeOut('fast');
            $('#money').fadeOut('fast');
            $('#lbl_valor').fadeOut('fast');
            $('#valor').fadeOut('fast');

        }
    });      
    
 });
 
 function initEtapas(){
    cargarEtapas();   
 }
 
 function initAutorizacion(){
     cargarAutorizacionCostos();
 }
 
 
 function cargarEtapas(){
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=8&soloActivas=N';
    if ($("#gview_tabla_etapas").length) {
         refrescarGridEtapas();
     }else {
        jQuery("#tabla_etapas").jqGrid({
            caption: 'Etapas proceso',
            url: url,
            datatype: 'json',
            height: 250,
            width: 400,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Dur. aprox.', 'Estado', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key:true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '700px'},
                {name: 'descripcion', index: 'descripcion', hidden:true, sortable: true, align: 'center', width: '700px'},
                {name: 'estimado_dias', index: 'estimado_dias', sortable: true, align: 'center', width: '180px'},
                {name: 'reg_status', index: 'reg_status', hidden:true, sortable: true, align: 'center', width: '90px'},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '200px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            pager:'#page_tabla_etapas',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {
                    var ids = jQuery("#tabla_etapas").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        var estado = jQuery("#tabla_etapas").getRowData(cl).reg_status;
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarEtapa('" + cl + "');\">";
                        traz = "<img src='/fintra/images/botones/iconos/trazabilidad.gif' align='absbottom'  name='ver_traza' id='ver_traza' width='15' height='15' title ='Ver trazabilidad'  onclick=\"verTrazabilidadEtapa('" + cl + "');\">";
                        if (estado==='A'){
                            $("#tabla_etapas").jqGrid('setRowData',ids[i],false, {weightfont:'bold',background:'#F6CECE'});          
                            ac = "<img src='/fintra/images/botones/iconos/check-blue.png' align='absbottom'  name='activar' id='activar' width='15' height='15' title ='Activar'  onclick=\"mensajeConfirmAction('Esta seguro de activar la etapa seleccionada?','250','150',activarEtapa,'" + cl + "');\">";
                        }else{
                            ac = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular la etapa seleccionada?','250','150',anularEtapa,'" + cl + "');\">";
                        }                      
                        jQuery("#tabla_etapas").jqGrid('setRowData', ids[i], {actions: ed + '  ' + traz/*+'  '+ac*/});
                       
                    }
                },
            loadError: function(xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_etapas",{search:false,refresh:false,edit:false,add:false,del:false});
        jQuery("#tabla_etapas").jqGrid("navButtonAdd", "#page_tabla_etapas", {
            caption: "Nuevo", 
            title: "Agregar nueva etapa",           
            onClickButton: function() {
               crearEtapas(); 
            }
        });
     }
}


function refrescarGridEtapas(){    
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=8&soloActivas=N';
    jQuery("#tabla_etapas").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#tabla_etapas').trigger("reloadGrid");
}

function crearEtapas(){
    $('#nometapa').val('');
    $('#descetapa').val('');
    $('#estimado_dias').val('');
    $('#div_etapas').fadeIn('slow');
    AbrirDivCrearEtapas();
}

function AbrirDivCrearEtapas(){
      $("#div_etapas").dialog({
        width: 700,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR ETAPAS PROCESO',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
              guardarEtapa();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarEtapa(){    
    var nomEtapa = $('#nometapa').val();
    var descEtapa = $('#descetapa').val();
    var estimadoDias = $('#estimado_dias').val();   
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if(nomEtapa!==''){
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 9,
                    nombre: nomEtapa,
                    descripcion: descEtapa,
                    estimado_dias: estimadoDias
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {
                            refrescarGridEtapas();                           
                            mensajesDelSistema("Se cre� la etapa correctamente", '250', '150', true); 
                            $('#nometapa').val('');
                            $('#descetapa').val('');  
                            $('#estimado_dias').val('');
                        }
                        
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear la nueva etapa!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
    }else{
       mensajesDelSistema("Debe ingresar el nombre de la etapa", '250', '150');
    }
}

function editarEtapa(cl){
    
    $('#div_editar_etapas').fadeIn("slow");
    var fila = jQuery("#tabla_etapas").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    var estimado_dias = fila['estimado_dias'];
    var estado = fila['reg_status'];
     if (estado==="A"){
        $('#nometapaEdit').attr({readonly: true});
        $('#descetapaEdit').attr({readonly: true});
        $('#estimado_diasEdit').attr({readonly: true});
    }else{
        $('#nometapaEdit').attr({readonly: false});
        $('#descetapaEdit').attr({readonly: false});
        $('#estimado_diasEdit').attr({readonly: false});
    }
    $('#idEtapaEdit').val(cl);
    $('#nometapaEdit').val(nombre);
    $('#descetapaEdit').val(descripcion);
    $('#estimado_diasEdit').val(estimado_dias);
    AbrirDivEditarEtapa();
    cargarRespuestasEtapa();
    cargarCostosEtapa();
}

function AbrirDivEditarEtapa(){
      $("#div_editar_etapas").dialog({
        width: 800,
        height: 565,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'EDITAR ETAPAS PROCESO',
        closeOnEscape: false,
        buttons: {  
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarEtapa(){
    var nombre = $('#nometapaEdit').val();
    var descripcion = $('#descetapaEdit').val();
    var estimadoDias = $('#estimado_diasEdit').val();
    var idEtapa = $('#idEtapaEdit').val();
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if(nombre!==''){
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 10,
                nombre: nombre,
                descripcion: descripcion,
                estimado_dias: estimadoDias,
                idEtapa: idEtapa
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridEtapas(); 
                        mensajesDelSistema("Etapa actualizada correctamente", '250', '150', true);                      
                        
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar la etapa!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }else{
         mensajesDelSistema("Debe ingresar un nombre para la etapa!!", '250', '150');      
    }
    
}

function anularEtapa(cl){
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 11,            
            idEtapa: cl,
            estadoEtapa: "A"
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridEtapas();                  
                    mensajesDelSistema("Se anul� la etapa", '250', '150', true);                                   
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular la etapa!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function activarEtapa(cl){
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 11,            
            idEtapa: cl,
            estadoEtapa: ""
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridEtapas();                  
                    mensajesDelSistema("Se activ� la etapa", '250', '150', true);                               
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo activar la etapa!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarRespuestasEtapa(){
    var idEtapa = $('#idEtapaEdit').val();
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=12&idEtapa='+idEtapa;
    if ($("#gview_respuestasEtapa").length) {
         refrescarGridRespuestasEtapa(idEtapa);
     }else {
        jQuery("#respuestasEtapa").jqGrid({
            caption: 'Respuestas de la Etapa',
            url: url,
            datatype: 'json',
            height: 230,
            width: 360,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Dur. aprox', 'Orden', 'Fin Proceso', 'Permite Editar', 'Id Etapa', /*'Etapa',*/'Estado', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden:true, align: 'center', width: '110px', key:true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '450px'},
                {name: 'descripcion', index: 'descripcion', hidden:true, sortable: true, align: 'center', width: '600px'},
                {name: 'estimado_dias', index: 'estimado_dias', sortable: true, align: 'center', width: '120px'},
                {name: 'secuencia', index: 'secuencia', hidden: false, sortable: true, align: 'center',  width: '100px'},
                {name: 'finaliza_proceso', index: 'finaliza_proceso', hidden: true},
                {name: 'editar_respuesta', index: 'editar_respuesta', hidden: true},
                {name: 'id_etapa', index: 'id_etapa', hidden:true},                
               /* {name: 'etapa', index: 'etapa', sortable: true, align: 'center', width: '600px'},*/
                {name: 'reg_status', index: 'reg_status', hidden:true},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '200px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            pager:'#page_tabla_respuestas_etapa',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {
                    var ids = jQuery("#respuestasEtapa").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        var editar_respuesta = jQuery("#respuestasEtapa").getRowData(cl).editar_respuesta;
                        var estado = jQuery("#respuestasEtapa").getRowData(cl).reg_status;
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarRespuestaEtapa('" + cl + "');\">";
                        if (estado==='A'){
                            $("#respuestasEtapa").jqGrid('setRowData',ids[i],false, {weightfont:'bold',background:'#F6CECE'});          
                            ac = "<img src='/fintra/images/botones/iconos/check-blue.png' align='absbottom'  name='activar' id='activar' width='15' height='15' title ='Activar'  onclick=\"mensajeConfirmAction('Esta seguro de activar la respuesta seleccionada?','250','150',activarRespuestaEtapa,'" + cl + "');\">";
                        }else{
                            ac = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular la respuesta seleccionada?','250','150',anularRespuestaEtapa,'" + cl + "');\">";
                        }  
                        if (editar_respuesta !== "N")  jQuery("#respuestasEtapa").jqGrid('setRowData', ids[i], {actions:ed+'   '+ac});

                    }
            },   
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_respuestas_etapa",{search:false,refresh:false,edit:false,add:false,del:false});
        jQuery("#respuestasEtapa").jqGrid("navButtonAdd", "#page_tabla_respuestas_etapa", {
            caption: "Nuevo", 
            title: "Agregar nueva respuesta",           
            onClickButton: function() {
               crearRespuestasEtapa(); 
            }
        });
     }
}

function refrescarGridRespuestasEtapa(id){   
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=12&idEtapa='+id;
    jQuery("#respuestasEtapa").setGridParam({
        datatype: 'json',
        url: url
    });    
    jQuery('#respuestasEtapa').trigger("reloadGrid");
}

function crearRespuestasEtapa(){
    $('#respuesta').val('');
    $('#descRespuesta').val('');
    $('#estimado_dias_resp').val('');
    $('#secuencia').val('');
    $('input[name=finproc]').val(['N']);
    $('#div_respuestasEtapa').fadeIn('slow');
    AbrirDivCrearRespuestasEtapa();
}

function AbrirDivCrearRespuestasEtapa(){
      $("#div_respuestasEtapa").dialog({
        width: 700,
        height: 250,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR RESPUESTA ETAPA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
              guardarRespuestaEtapa();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarRespuestaEtapa(){    
    var idEtapa = $('#idEtapaEdit').val();
    var nomRespuesta = $('#respuesta').val();
    var descRespuesta = $('#descRespuesta').val();
    var estimadoDiasResp = $('#estimado_dias_resp').val(); 
    var secuencia = $('#secuencia').val();
    var finaliza_proceso = $("input[name=finproc]:checked").val(); 
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if(nomRespuesta===''){
        mensajesDelSistema("Debe ingresar el nombre de la respuesta", '250', '150');
    }else if (secuencia===''){
        mensajesDelSistema("Debe llenar el campo orden", '250', '150');
    }else{
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 13,
                    idEtapa: idEtapa,
                    nombre: nomRespuesta,
                    descripcion: descRespuesta,
                    estimado_dias: estimadoDiasResp,
                    secuencia: secuencia,
                    finaliza_proceso: finaliza_proceso
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {
                            refrescarGridRespuestasEtapa(idEtapa);                           
                            mensajesDelSistema("Se cre� la respuesta correctamente", '250', '150', true); 
                            $('#respuesta').val('');
                            $('#descRespuesta').val('');  
                            $('#estimado_dias_resp').val('');
                            $('#secuencia').val('');
                            $('input[name=finproc]').val(['N']);
                        }
                        
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear la nueva respuesta!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
    }
}

function editarRespuestaEtapa(cl){
    
    $('#div_respuestasEtapa').fadeIn("slow");
    var fila = jQuery("#respuestasEtapa").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    var estimado_dias = fila['estimado_dias'];  
    var secuencia = fila['secuencia'];  
    var finaliza_proceso = fila['finaliza_proceso'];     
     
    var estado = fila['reg_status'];
     if (estado==="A"){
        $('#respuesta').attr({readonly: true});
        $('#descRespuesta').attr({readonly: true});
        $('#estimado_dias_resp').attr({readonly: true});
        $('#secuencia').attr({readonly: true});
        $('input[name=finproc]').attr({disabled: true});  
    }else{
        $('#respuesta').attr({readonly: false});
        $('#descRespuesta').attr({readonly: false});
        $('#estimado_dias_resp').attr({readonly: false});
        $('#secuencia').attr({readonly: false});
        $('input[name=finproc]').attr({disabled: false});
    }

    $('#idRespuesta').val(cl);
    $('#respuesta').val(nombre);
    $('#descRespuesta').val(descripcion);    
    $('#estimado_dias_resp').val(estimado_dias);
    $('#secuencia').val(secuencia);
    $('input[name=finproc]').val([finaliza_proceso]);    
    AbrirDivEditarRespuesta();  
}

function AbrirDivEditarRespuesta(){
      $("#div_respuestasEtapa").dialog({
        width: 700,
        height: 250,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'EDITAR RESPUESTA ETAPA',
        closeOnEscape: false,
        buttons: {   
            "Actualizar": function(){
               actualizarRespuestaEtapa();  
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarRespuestaEtapa(){
    var id = $('#idRespuesta').val();
    var respuesta = $('#respuesta').val();
    var descRespuesta = $('#descRespuesta').val();
    var estimadoDiasResp = $('#estimado_dias_resp').val();
    var idEtapa = $('#idEtapaEdit').val();
    var secuencia = $('#secuencia').val();
    var finaliza_proceso = $("input[name=finproc]:checked").val(); 
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if(respuesta===''){
        mensajesDelSistema("Debe ingresar un nombre para la respuesta!", '250', '150');
    }else if (secuencia===''){
        mensajesDelSistema("Debe llenar el campo orden", '250', '150');
    }else{
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 14,
                id: id,
                nombre: respuesta,
                descripcion: descRespuesta,
                estimado_dias: estimadoDiasResp,
                secuencia:secuencia,
                finaliza_proceso: finaliza_proceso,
                idEtapa: idEtapa
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridRespuestasEtapa(idEtapa); 
                        mensajesDelSistema("Respuesta actualizada correctamente", '250', '150', true);                      
                        
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar la respuesta!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
    
}

function anularRespuestaEtapa(cl){
    var idEtapa = $('#idEtapaEdit').val();
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 15,            
            id: cl,
            estadoRespuesta: "A"
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridRespuestasEtapa(idEtapa);                  
                    mensajesDelSistema("Se anul� la respuesta", '250', '150', true);                                   
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular la respuesta!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function activarRespuestaEtapa(cl){
    var idEtapa = $('#idEtapaEdit').val();
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 15,            
            id: cl,
            estadoRespuesta: ""
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridRespuestasEtapa(idEtapa);                  
                    mensajesDelSistema("Respuesta activada correctamente", '250', '150', true);                               
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo activar la respuesta!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargar_tipos_costos(){
    
    $('#tipo_costo').html('');  
    $('#tipo_costo').append('<option value=M>Dinero</option>');       
    $('#tipo_costo').append('<option value=P>Porcentaje</option>');    
    $('#tipo_costo').append('<option value=D>Valor a definir en etapa</option>'); 
}

function cargarCostosEtapa(){
    var idEtapa = $('#idEtapaEdit').val();
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=16&idEtapa='+idEtapa;
    if ($("#gview_costosEtapa").length) {
         refrescarGridCostosEtapa(idEtapa);
     }else {
        jQuery("#costosEtapa").jqGrid({
            caption: 'Costos de la Etapa',
            url: url,
            datatype: 'json',
            height: 230,
            width: 360,
            colNames: ['Id', 'Concepto', 'Tipo', 'Tipo', 'Valor', 'S. autom', 'Aprob.', 'Id Etapa',/* 'Etapa',*/'Estado', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden:true, align: 'center', width: '100px', key:true},
                {name: 'concepto', index: 'concepto', sortable: true, align: 'left', width: '430px'},
                {name: 'tipo', index: 'tipo', sortable: true, hidden:true, align: 'center', width: '100px'},
                {name: 'simbolo', index: 'simbolo', sortable: true, align: 'center', width: '100px'},
                {name: 'valor', index: 'valor', sortable: true, align: 'center', width: '220px',sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2}},
                {name: 'solo_automotor', index: 'solo_automotor', sortable: true, align: 'center', width: '180px'},
                {name: 'id_estado_ap', index: 'id_estado_ap', sortable: true, align: 'center', width: '180px'},
                {name: 'id_etapa', index: 'id_etapa', hidden:true},
              /*{name: 'etapa', index: 'etapa', sortable: true, align: 'center', width: '600px'},*/
                {name: 'reg_status', index: 'reg_status', hidden:true},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '200px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            pager:'#page_tabla_costos_etapa',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {
                    var ids = jQuery("#costosEtapa").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        var estado = jQuery("#costosEtapa").getRowData(cl).reg_status;
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarCostoEtapa('" + cl + "');\">";
                        if (estado==='A'){
                            $("#costosEtapa").jqGrid('setRowData',ids[i],false, {weightfont:'bold',background:'#F6CECE'});          
                            ac = "<img src='/fintra/images/botones/iconos/check-blue.png' align='absbottom'  name='activar' id='activar' width='15' height='15' title ='Activar'  onclick=\"mensajeConfirmAction('Esta seguro de activar el costo seleccionado?','250','150',activarCostoEtapa,'" + cl + "');\">";
                        }else{
                            ac = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el costo seleccionado?','250','150',anularCostoEtapa,'" + cl + "');\">";
                        }  
                        jQuery("#costosEtapa").jqGrid('setRowData', ids[i], {actions:ed+'   '+ac});                        
                    }
                },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_costos_etapa",{search:false,refresh:false,edit:false,add:false,del:false});
        jQuery("#costosEtapa").jqGrid("navButtonAdd", "#page_tabla_costos_etapa", {
            caption: "Nuevo", 
            title: "Agregar nuevo costo",           
            onClickButton: function() {
               crearCostosEtapa(); 
            }
        });
     }
}

function refrescarGridCostosEtapa(id){   
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=16&idEtapa='+id;
    jQuery("#costosEtapa").setGridParam({
        datatype: 'json',
        url: url
    });    
    jQuery('#costosEtapa').trigger("reloadGrid");
}

function crearCostosEtapa(){
    $('#concepto').val('');
    $('#valor').val('');   
    $('input[name=sauto]').val(['N']);
    $('#div_costosEtapa').fadeIn('slow');
    $('#percent').fadeOut('fast');
    $('#money').fadeIn('slow');
    $('#lbl_valor').fadeIn('slow');
    $('#valor').fadeIn('slow');
    cargar_tipos_costos();
    AbrirDivCrearCostosEtapa();
}

function AbrirDivCrearCostosEtapa(){
      $("#div_costosEtapa").dialog({
        width: 480,
        height: 240,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR COSTO ETAPA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
              guardarCostoEtapa();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarCostoEtapa(){    
    var idEtapa = $('#idEtapaEdit').val();
    var concepto = $('#concepto').val();
    var tipo = $('#tipo_costo').val();
    var valor = (tipo!=='D') ? $('#valor').val():0;
    var solo_automotor = $("input[name=sauto]:checked").val(); 
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if (concepto == '') {
        mensajesDelSistema("Debe ingresar el concepto!!", '250', '150');
    } else if (tipo !== 'D' && valor == '') {
        mensajesDelSistema("Debe ingresar el valor!!", '250', '150');
    } else if (tipo == 'P' && (numberSinComas(valor) < 0 || numberSinComas(valor) > 100)) {
        mensajesDelSistema("El porcentaje debe estar comprendido entre 0 y 100!!", '250', '150');
    }   
    else {
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 17,
                    idEtapa: idEtapa,
                    concepto: concepto,
                    tipo: tipo,
                    valor: numberSinComas(valor),
                    solo_automotor: solo_automotor
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {
                            refrescarGridCostosEtapa(idEtapa);                           
                            mensajesDelSistema("Se cre� el costo correctamente", '250', '150', true); 
                            $('#concepto').val('');
                            $('#tipo_costo').val('M');  
                            $('#percent').fadeOut('fast');
                            $('#money').fadeIn('slow');       
                            $('#lbl_valor').fadeIn('slow');
                            $('#valor').fadeIn('slow');
                            $('#valor').val('');  
                            $('input[name=sauto]').val(['N']);
                          
                        }
                        
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear la nueva etapa!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
    }
}

function editarCostoEtapa(cl){
    
    $('#div_costosEtapa').fadeIn("slow");
    var fila = jQuery("#costosEtapa").getRowData(cl);
    var concepto = fila['concepto'];
    var tipo = fila['tipo'];
    var valor = fila['valor'];
    var solo_automotor = fila['solo_automotor'];  
    var estado = fila['reg_status'];
     if (estado==="A"){
        $('#concepto').attr({readonly: true});
        $('#tipo_costo').attr({disabled: true});
        $('#valor').attr({readonly: true});
        $('input[name=sauto]').attr({disabled: true});  

    }else{
        $('#concepto').attr({readonly: false});
        $('#tipo_costo').attr({disabled: false});
        $('#valor').attr({readonly: false});
        $('input[name=sauto]').attr({disabled: false});  
    }
    cargar_tipos_costos();
    $('#idCosto').val(cl);
    $('#concepto').val(concepto);
    $('#tipo_costo').val(tipo);
    $('#valor').val(numberConComas(valor));
    $('input[name=sauto]').val([solo_automotor]);    
    if (tipo == 'P'){
        $('#money').fadeOut('fast'); 
        $('#percent').fadeIn('slow');  
        $('#lbl_valor').fadeIn('slow');
        $('#valor').fadeIn('slow');
    }else if (tipo == 'M') {
        $('#percent').fadeOut('fast');
        $('#money').fadeIn('slow');
        $('#lbl_valor').fadeIn('slow');
        $('#valor').fadeIn('slow');
    }else{
        $('#percent').fadeOut('fast'); 
        $('#money').fadeOut('fast');
        $('#lbl_valor').fadeOut('fast');
        $('#valor').fadeOut('fast');
    }
    AbrirDivEditarCosto();  
}

function AbrirDivEditarCosto(){
      $("#div_costosEtapa").dialog({
        width: 480,
        height: 240,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'EDITAR COSTOS ETAPA',
        closeOnEscape: false,
        buttons: {   
            "Actualizar": function(){
               actualizarCostoEtapa();  
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarCostoEtapa(){
    var id = $('#idCosto').val();
    var concepto = $('#concepto').val();
    var tipo = $('#tipo_costo').val();
    var valor = (tipo!=='D') ? $('#valor').val():0;
    var solo_automotor = $("input[name=sauto]:checked").val(); 
    var idEtapa = $('#idEtapaEdit').val();
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if(concepto==''){
        mensajesDelSistema("Debe ingresar el concepto!!", '250', '150');      
    }else if (tipo !== 'D' && valor==''){
        mensajesDelSistema("Debe ingresar el valor!!", '250', '150');      
    }else if (tipo == 'P' && (numberSinComas(valor) < 0 || numberSinComas(valor) > 100)) {
        mensajesDelSistema("El porcentaje debe estar comprendido entre 0 y 100!!", '250', '150');
    }  
    else{
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 18,
                id: id,
                concepto: concepto,
                tipo: tipo,
                valor: numberSinComas(valor),
                solo_automotor: solo_automotor,
                idEtapa: idEtapa
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridCostosEtapa(idEtapa); 
                        mensajesDelSistema("El costo se ha actualizado correctamente", '250', '150', true);                      
                        
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar el costo!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
    
}

function anularCostoEtapa(cl){
    var idEtapa = $('#idEtapaEdit').val();
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 19,            
            id: cl,
            estadoCosto: "A"
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridCostosEtapa(idEtapa);                  
                    mensajesDelSistema("Se anul� el costo seleccionado", '250', '150', true);                                   
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular el costo!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function activarCostoEtapa(cl){
    var idEtapa = $('#idEtapaEdit').val();
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 19,            
            id: cl,
            estadoCosto: ""
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridCostosEtapa(idEtapa);                  
                    mensajesDelSistema("Costo activado correctamente", '250', '150', true);                               
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo activar el costo!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarAutorizacionCostos(){  
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=45';
    if ($("#gview_tabla_autoriz_costos").length) {
         refrescarGridAutorizacionCostos();
     }else {
        jQuery("#tabla_autoriz_costos").jqGrid({
            caption: 'Autorizar costos',
            url: url,
            datatype: 'json',
            height: 230,
            width: 750,
            colNames: ['Id', 'Concepto', 'Tipo', 'Simbolo', 'Valor', 'S. autom', 'Aprob.', 'Id Etapa',/* 'Etapa',*/'Estado'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden:true, align: 'center', width: '100px', key:true},
                {name: 'concepto', index: 'concepto', sortable: true, align: 'left', width: '430px'},
                {name: 'tipo', index: 'tipo', sortable: true, hidden:true, align: 'center', width: '100px'},
                {name: 'simbolo', index: 'simbolo', sortable: true, align: 'center', width: '100px'},
                {name: 'valor', index: 'valor', sortable: true, align: 'center', width: '170px',sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'solo_automotor', index: 'solo_automotor', sortable: true, align: 'center', width: '180px'},
                {name: 'estado_ap', index: 'estado_ap', sortable: true, align: 'center', width: '180px'},
                {name: 'id_etapa', index: 'id_etapa', hidden:true},
              /*{name: 'etapa', index: 'etapa', sortable: true, align: 'center', width: '600px'},*/
                {name: 'reg_status', index: 'reg_status', hidden:true}              
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            pager:'#page_tabla_autoriz_costos',
            pgtext: null,
            pgbuttons: false,
            multiselect:true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },           
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_autoriz_costos",{search:false,refresh:false,edit:false,add:false,del:false});
        jQuery("#tabla_autoriz_costos").jqGrid("navButtonAdd", "#page_tabla_autoriz_costos", {
            caption: "Asignar estado", 
            title: "Asignar estado",           
            onClickButton: function() {
                var filasId = jQuery('#tabla_autoriz_costos').jqGrid('getGridParam', 'selarrrow');
                if (filasId != '') {                  
                    cargarEstadosAprobacion();
                    $("#dialogAprobacionCostos").fadeIn();       
                    ventanaAprobacionCostos(); 
                }else {                      
                    if (jQuery("#tabla_autoriz_costos").jqGrid('getGridParam', 'records') > 0) {
                        mensajesDelSistema("Debe seleccionar el(los) concepto(s) a los que desea cambiar el estado!!", '250', '150');
                    } else {                    
                        mensajesDelSistema("No hay conceptos para asignar estado", '250', '150');
                    }

                }    
            }
        });
     }
}

function refrescarGridAutorizacionCostos(){   
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=45';
    jQuery("#tabla_autoriz_costos").setGridParam({
        datatype: 'json',
        url: url
    });    
    jQuery('#tabla_autoriz_costos').trigger("reloadGrid");
}

 
  function cargarEstadosAprobacion() {
    $('#estados_aprob').html('');
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Proceso&accion=Ejecutivo",
        dataType: 'json',
        data: {
            opcion: 46
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                                 
                    for (var key in json) {              
                       $('#estados_aprob').append('<option value=' + key + '>' + json[key] + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                $('#estados_aprob').append("<option value='0'>Seleccione</option>");

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ventanaAprobacionCostos(){
      $("#dialogAprobacionCostos").dialog({
        width: 600,
        height: 170,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'APROBACION COSTOS PROCESO EJECUTIVO',
        closeOnEscape: false,
        buttons: {  
            "Aceptar": function () {
               asignarEstadoAprob();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function asignarEstadoAprob(){
    
    var listado = "";
    var filasId =jQuery('#tabla_autoriz_costos').jqGrid('getGridParam', 'selarrrow');
    if (filasId != ''){
        for (var i = 0; i < filasId.length; i++) {        
            listado += filasId[i] + ",";      
        }    
      
        var url = './controller?estado=Proceso&accion=Ejecutivo';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 47,        
                estadoAprob: $('#estados_aprob').val(),
                listado: listado
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {    
                        refrescarGridAutorizacionCostos();  
                        $("#dialogAprobacionCostos").dialog('close');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asignar el nuevo estado a los elementos seleccionados!!", '250', '150');
                }
              
            }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
            } 
        });
        
    }
}

function verTrazabilidadEtapa(cl){
    
    $('#dialogTrazEtapa').fadeIn("slow");  
    cargarEtapaTrazabilidad(cl);
    ventanaVerTrazabilidad();
   
}

function ventanaVerTrazabilidad() { 
    $("#dialogTrazEtapa").dialog({        
        width: 900,
        height: 480,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:"Ver Trazabilidad Etapa",
        closeOnEscape: true,       
        buttons: {          
            "Salir": function () {          
		$(this).dialog("close");               
            }
        }
    });
}

function cargarEtapaTrazabilidad(idetapa){  
  
    var jsonParam = {
            opcion:55,
            id_etapa:idetapa
    };
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if ($("#gview_tabla_etapa_traz").length) {     
         refrescarGridEtapaTrazabilidad(jsonParam);
     }else {
        jQuery("#tabla_etapa_traz").jqGrid({
            caption: '',
            url: url,
            datatype: 'json',
            height: 350,
            width: 870,
            colNames: ['Id',  /*'Tipo',*/ 'Etapa', 'Usuario', 'Fecha', 'Comentarios', 'Concepto', 'Valor'],
            colModel: [
                {name: 'id_etapa', index: 'id_etapa', sortable: true, hidden: true, align: 'center', width: '100px'},
//              {name: 'tipo', index: 'tipo', sortable: true, align: 'center', width: '90px'},
                {name: 'etapa', index: 'etapa', sortable: true, align: 'left', width: '230px'},
                {name: 'usuario', index: 'usuario', sortable: true, align: 'center', width: '100px'},
                {name: 'fecha', index: 'fecha', sortable: true, align: 'center', width: '150px'},
                {name: 'comentarios', index: 'comentarios', sortable: true, align: 'left', width: '430px'},
                {name: 'concepto', index: 'concepto', sortable: true, align: 'left', width: '280px'},
                {name: 'valor', index: 'valor', sortable: true, width: '110px', align: 'right', sorttype: 'currency',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}    
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,           
            pgtext: null,
            pgbuttons: false,          
            jsonReader: {
                root: 'rows',
                cell:'',
                repeatitems: false,
                id: '0'
            },          
            ajaxGridOptions: {                
               async:false,
               type:"POST",
               data:jsonParam
            },         
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });      
     }
}

function refrescarGridEtapaTrazabilidad(params){   
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    jQuery("#tabla_etapa_traz").setGridParam({
        url: url,
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: params
        }       
    });    
    jQuery('#tabla_etapa_traz').trigger("reloadGrid");
}

function mensajeConfirmAction(msj, width, height, okAction, id) {   
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {   
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function numbersonly(myfield, e, dec)
{
    var key;
    var keychar;
    
    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

// control keys
    if ((key == null) || (key == 0) || (key == 8) ||
            (key == 9) || (key == 13) || (key == 27))
        return true;

// numbers
    else if ((("0123456789.").indexOf(keychar) > -1))
        return true;

// decimal point jump
    else if (dec && (keychar == "."))
    {
        myfield.form.elements[dec].focus();
        return false;
    }
    else
        return false;
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

