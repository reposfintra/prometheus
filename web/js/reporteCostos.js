/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

});

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}


function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
}


function reporteCostos() {
    console.log('Entra aqui');
    var grid_tabla_ = jQuery("#tabla_Costos");
    if ($("#gview_tabla_Costos").length) {
        reloadGridMostrar(grid_tabla_, 90);
    } else {
        //alert('Entra aqui');    
        grid_tabla_.jqGrid({
            caption: "REPORTE COSTOS",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '700',
            width: '1600',
            colNames: ['Multiservicio', 'Id solicitud', 'Nombre cliente', 'Nombre proyecto', 'Valor contratista antes iva', 'Utilizado', 'Rentabilidad', '% Administracion', '% Imprevisto', '% Utilidad',
                'Valor administracion', 'Valor imprevisto', 'Valor utilidad', 'Valor venta', '% Iva', 'Valor iva', '% OPAV', '% FINTRA', '% PROVINTEGRAL', '% INTERVENTORIA',
                'Total esquema', 'Sub total esquema', '% ECA', 'Sub total eca', 'Total comision', 'Total'],
            colModel: [
                {name: 'multiservicio', index: 'multiservicio', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'id_solicitud', index: 'id_solicitud', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'nombre_cliente', index: 'nombre_cliente', width: 200, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'nombre_proyecto', index: 'nombre_proyecto', width: 250, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'valor_contratista_antes_iva', index: 'valor_contratista_antes_iva', sortable: true, width: 150, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}},
                {name: 'utilizado', index: 'utilizado', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}},
                {name: 'rentabilidad', index: 'rentabilidad', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: '% administracion', index: '% administracion', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: '% imprevisto', index: '% imprevisto', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: '% utilidad', index: '% utilidad', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: 'vlr_administracion', index: 'vlr_administracion', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}},
                {name: 'vlr_imprevisto', index: 'vlr_imprevisto', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}},
                {name: 'vlr_utilidad', index: 'vlr_utilidad', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}},
                {name: 'valor_venta', index: 'valor_venta', sortable: true, width: 130, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}},
                {name: '% iva', index: '% iva', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: 'valor_iva', index: 'valor_iva', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}},
                {name: '% OPAV', index: '% OPAV', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: '% FINTRA', index: '% FINTRA', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: '% PROVINTEGRAL', index: '% PROVINTEGRAL', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: '% INTERVENTORIA', index: '% INTERVENTORIA', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: 'total_esquema', index: 'total_esquema', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: 'sub_total_esquema', index: 'sub_total_esquema', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: '% ECA', index: '% ECA', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: 'sub_total_eca', index: 'sub_total_eca', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: 'total_comision', index: 'total_comision', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, suffix: " %"}},
                {name: 'total', index: 'total', sortable: true, width: 110, align: 'right', search: false, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 90,
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function () {
                var valor_contratista_antes_iva = jQuery("#tabla_Costos").jqGrid('getCol', 'valor_contratista_antes_iva', false, 'sum');
                var utilizado = jQuery("#tabla_Costos").jqGrid('getCol', 'utilizado', false, 'sum');
                var total = jQuery("#tabla_Costos").jqGrid('getCol', 'total', false, 'sum');
                var vlr_administracion = jQuery("#tabla_Costos").jqGrid('getCol', 'vlr_administracion', false, 'sum');
                var vlr_imprevisto = jQuery("#tabla_Costos").jqGrid('getCol', 'vlr_imprevisto', false, 'sum');
                var vlr_utilidad = jQuery("#tabla_Costos").jqGrid('getCol', 'vlr_utilidad', false, 'sum');
                var valor_venta = jQuery("#tabla_Costos").jqGrid('getCol', 'valor_venta', false, 'sum');
                var valor_iva = jQuery("#tabla_Costos").jqGrid('getCol', 'valor_iva', false, 'sum');
                jQuery("#tabla_Costos").jqGrid('footerData', 'set', {total: total});
                jQuery("#tabla_Costos").jqGrid('footerData', 'set', {valor_contratista_antes_iva: valor_contratista_antes_iva});
                jQuery("#tabla_Costos").jqGrid('footerData', 'set', {utilizado: utilizado});
                jQuery("#tabla_Costos").jqGrid('footerData', 'set', {vlr_administracion: vlr_administracion});
                jQuery("#tabla_Costos").jqGrid('footerData', 'set', {vlr_imprevisto: vlr_imprevisto});
                jQuery("#tabla_Costos").jqGrid('footerData', 'set', {vlr_utilidad: vlr_utilidad});
                jQuery("#tabla_Costos").jqGrid('footerData', 'set', {valor_venta: valor_venta});
                jQuery("#tabla_Costos").jqGrid('footerData', 'set', {valor_iva: valor_iva});

            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_Costos"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var multiservicio = filas.multiservicio;
                var id_solicitud = filas.id_solicitud;
                var valor_contratista_antes_iva = "$" + filas.valor_contratista_antes_iva;
                var utilizado = "$" + filas.utilizado;
                var cliente = filas.nombre_cliente;
                var proyecto = filas.nombre_proyecto;
                var rentabilidad = filas.rentabilidad + "%";

                //alert(multiservicio);
                ventanaDetalles(multiservicio, id_solicitud, valor_contratista_antes_iva, utilizado, cliente, proyecto, rentabilidad);

            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: true, refresh: true},
        {multipleSearch: true, uniqueSearchFields: true, multipleGroup: true}
        );
        grid_tabla_.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
        $("#tabla_Costos").navButtonAdd('#pager', {
            caption: "Exportar",
            onClickButton: function () {
                exportarExcel();
            }
        });
    }
    document.getElementById("gs_multiservicio").style.width = '60px';
    document.getElementById("gs_id_solicitud").style.width = '60px';
    document.getElementById("gs_nombre_proyecto").style.width = '215px';
    document.getElementById("gs_nombre_cliente").style.width = '165px';
}


function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function  exportarExcel() {
    var fullData = $("#tabla_Costos").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 240,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            listado: myJsonString,
            opcion: 93
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function ventanaDetalles(multiservicio, id_solicitud, valor_contratista_antes_iva, utilizado, cliente, proyecto, rentabilidad) {
    $('#multiservicio').val(multiservicio);
    $('#valor_contratista_antes_iva').val(numberConComas(valor_contratista_antes_iva));
    $('#utilizado').val(numberConComas(utilizado));
    $('#nombre_cliente').val(cliente);
    $('#nombre_proyecto').val(proyecto);
    $('#rentabilidad').val(numberConComas(rentabilidad));
    detalleCostos(multiservicio, id_solicitud);
    $("#dialogDetalleCostos").dialog({
        width: '1700',
        height: '900',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'REPORTE DETALLE COSTOS',
        open: function (event, ui) {
            // $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Exportar": function () {
                exportarExcelDetalleCostos();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

    var valor_total_con_iva;
function detalleCostos(multiservicio, id_solicitud) {

    console.log('Entra aqui');
    $('#tabla_Detalle_Costos').jqGrid('GridUnload');
    var grid_tabla_ = jQuery("#tabla_Detalle_Costos");
    if ($("#gview_tabla_Detalle_Costos").length) {
        reloadGridMostrar1(grid_tabla_, 91, multiservicio, id_solicitud);
    } else {
        //alert('Entra aqui');    
        grid_tabla_.jqGrid({
            caption: "REPORTE DETALLE COSTOS",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '600',
            width: '1650',
            colNames: ['Empresa', 'Tipo referencia', 'Multiservicio', 'Nit proveedor', 'Nombre proveedor', 'Fecha documento', 'Tipo documento', 'Documento',
                'Descripcion', 'Valor antes Iva', 'Valor iva', 'Total factura con iva', 'Valor pagado', 'Total abonos', 'Fecha pago', 'Codigo orden', 'Valor orden compra', 'Codigo cuenta'],
            colModel: [
                {name: 'empresa', index: 'empresa', width: 80, sortable: true, align: 'center', hidden: true, search: true},
                {name: 'tipo_referencia_1', index: 'tipo_referencia_1', width: 80, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'referencia_1', index: 'referencia_1', width: 90, sortable: true, align: 'center', hidden: false, search: true, key: true},
                {name: 'proveedor', index: 'proveedor', width: 80, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'nombre_proveedor', index: 'nombre_proveedor', width: 150, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'fecha_documento', index: 'fecha_documento', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'tipo_documento', index: 'tipo_documento', width: 100, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'documento', index: 'documento', width: 120, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'descripcion', index: 'descripcion', width: 200, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'valor_antes_iva', index: 'valor_antes_iva', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}, summaryType: 'sum'},
                {name: 'valor_iva', index: 'valor_iva', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}, summaryType: 'sum'},
                {name: 'valor_total_con_iva', index: 'valor_total_con_iva', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}, summaryType: 'sum'},
                {name: 'valor_pagado', index: 'valor_pagado', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}, summaryType: 'sum'},
                {name: 'vlr_total_abonos', index: 'vlr_total_abonos', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}, summaryType: 'sum'},
                {name: 'fecha_pago', index: 'fecha_pago', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'cod_orden', index: 'cod_orden', width: 90, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'vlr_orden_compra', index: 'vlr_orden_compra', sortable: true, width: 110, align: 'right', search: true, sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 3, prefix: "$ "}, summaryType: 'sum'},
                {name: 'codigo_cuenta', index: 'codigo_cuenta', width: 90, sortable: true, align: 'center', hidden: false, search: true}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 5000,
            rowTotal: 5000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            sortname: 'empresa',
            grouping: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pagerDetalles',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 91,
                    multiservicio: multiservicio,
                    id_solicitud: id_solicitud

                }
            },
            gridComplete: function () {


            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {
//                var total_factura_con_iva = jQuery("#tabla_Detalle_Costos").jqGrid('getCol', 'valor_total_con_iva', false, 'sum');
//                //jQuery("#tabla_Detalle_Costos").jqGrid('footerData', 'set', {valor_total_con_iva: total_factura_con_iva});
//                var valor_total_con_iva = (numberConComas(total_factura_con_iva));

            },
            groupingView: {
                groupField: ['empresa'],
                groupSummary: [true],
                groupColumnShow: [true],
                groupText: ['<h3>{0} Total factura con iva: <font color="red">${valor_total_con_iva},00 </font></h3>', '<h3>{0} Total factura con iva: <font color="red">${valor_total_con_iva},00 </font></h3>'],
                groupCollapse: true,
                groupOrder: ['asc']
            }
        });
    }
}

function  darFormato() {
    var total_factura_con_iva = jQuery("#tabla_Detalle_Costos").jqGrid('getCol', 'valor_total_con_iva', false, 'sum');
    //jQuery("#tabla_Detalle_Costos").jqGrid('footerData', 'set', {valor_total_con_iva: total_factura_con_iva});
    var valor_total_con_iva = (numberConComas(total_factura_con_iva));
    alert(total_factura_con_iva);
    return valor_total_con_iva;
}

function reloadGridMostrar1(grid_tabla, opcion, multiservicio, id_solicitud) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                multiservicio: multiservicio,
                id_solicitud: id_solicitud
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function  exportarExcelDetalleCostos() {
    var fullData = $("#tabla_Detalle_Costos").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 240,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            listado: myJsonString,
            opcion: 92
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

