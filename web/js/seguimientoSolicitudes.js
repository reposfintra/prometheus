/**
 * Javascript para la pagina de seguimiento de solicitudes
 * Autor: darrieta-Geotech
 * Fecha: 10/05/2010
 */

/**
 * Muestra u oculta el div para seleccionar vencidos
 */
function mostrarVencido() {
    if($F("cmbEstado")=="-1" || $F("cmbEstado")=="020" || $F("cmbEstado")=="030" || $F("cmbEstado")=="040"){
        $("spanVencido").show();
    }else{
        $("spanVencido").hide();
        $("chkVencido").checked = false;
    }
}

/**
 * Realiza la validaci�n y envia los datos para la consulta
 */
function consultarSolicitudes(informe){//------- RESPONSABLE OPAV

    var sw = true;
    if($F("txtFechaInicial")!="" && $F("txtFechaFinal")!=""){
        var fechaInicial = $F("txtFechaInicial").replace(/-/gi, "/");
        var fechaFinal = $F("txtFechaFinal").replace(/-/gi, "/");

        if(fechaInicial>fechaFinal){
            sw = false;
            alert("La fecha final debe ser mayor o igual que la inicial");
        }
    }
    if(($F("txtFechaInicial")!="" && $F("txtFechaFinal")=="") || ($F("txtFechaInicial")=="" && $F("txtFechaFinal")!="")){
        sw = false;
        alert("Debe seleccionar una fecha inicial y una final");
    }

if(informe!=""){
document.getElementById("informe").value = informe;//------- RESPONSABLE OPAV



}



    if(sw){
        document.forms[0].submit();
    }
}

var estado=4;

/**
* Metodo que retorna el objecto ajax
* @autor: Jcastro
* @Solicitud: creaci�n de categorias
* @fecha: 15-07-2010
*/
function objetoAjax(){
    var xmlhttp = null;
    try{
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e){
        try{
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (ex){
            xmlhttp = null;
        }
    }

    if (!xmlhttp && typeof XMLHttpRequest != 'undefined'){
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

/**
 *
 */
function buscarResponsables(CONTROLLER,idcargado){

    var responsable = document.getElementById("responsable");


    if ((responsable != "")){
        //new Ajax.request(url,pars,onLoading:metodo1,onComplete:ejecutar(response.responseText));$('x')
        var ajax = objetoAjax();
        var parametros = "opcion=responsable";

        ajax.open("POST", CONTROLLER+"?estado=Negocios&accion=Applus", true);
        ajax.onreadystatechange = function() {

            if (ajax.readyState == 1){
                responsable.options.length = 0;
                responsable.options[0] = new Option("Cargando...", "");
                responsable.disabled = true;
            }




            else if (ajax.readyState == 4){

             if( ajax.status == 200){
                    var cat = ajax.responseXML.getElementsByTagName("responsable");
                    if (cat[0].childNodes[0].nodeValue != "null"){
                        cargarResponsable(responsable, cat, idcargado);

                }
                    else{
                        responsable.options.length = 0;
                        responsable.options[0] = new Option("Seleccione un Responsable", "");
                        responsable.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }

}



function cargarResponsable(combobox, cod, idcargado){

    var option ;
    try{
        if (cod.length > 1){
            combobox.disabled = false;
            var i = 0;

            for (i; i<cod.length; i++){

                var nomente = cod[i].childNodes[0].nodeValue;
                var codente = cod[i].getAttribute("codente");

                if(codente==idcargado){
                    option = new Option(nomente, codente, codente,"selected");
                }else{
                    if(((codente==0)&&(idcargado!=""))||((codente==0)&&idcargado == undefined) ){
                        option = new Option(nomente, codente, codente,"selected");

                    }else{
                        option = new Option(nomente, codente, codente, "");
                    }
                }
                combobox.options[i] = option;
            }
        }
        else{
            var option = new Option("No hay Interventor", "");
            combobox.options[0] = option;
        }
    }catch (e){
        alert("error___________"+e);

    }

}


///**
// * Actualiza el valor del hidden cuando se borre el contenido del campo de texto
// */
//function actualizarHidden(textfield, hidden){
//    if($F(textfield)==""){
//        $(hidden).value="";
//    }
//}
//
///**
// * Guarda el id del elemento seleccionado en el campo oculto
// */
//function guardarIdCliente(text, li) {
//    $("hidCliente").value=li.id;
//}
//
///**
// * Guarda el id del elemento seleccionado en el campo oculto
// */
//function guardarIdEjecutivo(text, li) {
//    $("hidEjecutivo").value=li.id;
//}