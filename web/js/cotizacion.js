/*
 * Created by: Ing. Jos� Castro
 * and open the template in the editor.
 */


// JavaScript Document
var estado=4;

/**
* Metodo que retorna el objecto ajax
* @autor: Jcastro
* @Solicitud: Plantilla Cotizaciones
* @fecha: 16-06-2010
*/
function objetoAjax(){
    var xmlhttp = null;
    try{
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e){
        try{
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (ex){
            xmlhttp = null;
        }
    }

    if (!xmlhttp && typeof XMLHttpRequest != 'undefined'){
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}


/**
    * Metodo que retorna el cuerpo del mensaje mientras esta cargando una accion
    * @parameter: msg, BASEURL
    * @autor: Jcastro
    * @Solicitud:  Plantilla Cotizaciones
    * @fecha: 16-06-2010
*/
function loading(msg, BASEURL){
    var html = "";
    html += "<table border=\"1\" align=\"center\">";
    html += "<tr>";
    html += "<td>";
    html += "<table width=\"400\" height=\"50\"  align=\"center\"  bordercolor=\"#F7F5F4\" bgcolor=\"#FFFFFF\">";
    html += "<tr>";
    html += "<td align=\"center\"><img src=\""+BASEURL+"/images/cargando.gif\" border=\"0\" align=\"absmiddle\"></td>";
    html += "<td class=\"mensajes\" align=\"center\">"+msg+"</td>";
    html += "</tr>";
    html += "</table>";
    html += "</td>";
    html += "</tr>";
    html += "</table>";
    return(html);
}

/**
	* Metodo que retorna el cuerpo del mensaje
	* @parameter: msg, BASEURL
	 * @autor: Jcastro
	* @Solicitud: Plantilla Cotizaciones
	* @fecha: 16-06-2010
	*/
function message(msg, BASEURL){
    var html = "";
    html += "<table border=\"2\" align=\"center\">";
    html += "<tr>";
    html += "<td>";
    html += "<table width=\"400\" height=\"73\" border=\"1\" align=\"center\"  bordercolor=\"#F7F5F4\" bgcolor=\"#FFFFFF\">";
    html += "<tr>";
    html += "<td width=\"360\" align=\"center\" class=\"mensajes\">"+msg+"</td>";
    html += "<td width=\"40\" background=\""+BASEURL+"/images/cuadronaranja.JPG\">&nbsp;</td>";
    html += "</tr>";
    html += "</table>";
    html += "</td>";
    html += "</tr>";
    html += "</table>";
    return(html);
}

function obtenerSolicitudes(CONTROLLER, BASEURL, contratista, iao){

    //alert(codcliente);
    if (estado == 4){
        var ajax = objetoAjax();
        ajax.open("POST", CONTROLLER+"?estado=Cotizacion&accion=Orden", true);
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 1){
                document.getElementById("capaCentral").innerHTML = loading("Cargando informacion...", BASEURL);
                estado = 1;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    estado = 4;
                    document.getElementById("capaCentral").innerHTML = ajax.responseText;

                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send("opcion=0&contratista="+contratista+"&id_accion_inicial="+iao);
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}




function obtenerCotizacion(CONTROLLER, BASEURL, index, id_accion){

    var imagen = document.getElementById("ima"+index);
    var celda = document.getElementById("solicitud"+index);
    var visible = ((celda.style.display == "none")?false:true);
    if (estado == 4){
        if (visible){
            imagen.src = BASEURL+"/images/botones/iconos/expand.gif";
            celda.style.display="none";
        } else {
            if(celda.innerHTML == ""){
                var ajax = objetoAjax();
                ajax.open("POST", CONTROLLER+"?estado=Cotizacion&accion=Orden", true);
                ajax.onreadystatechange = function(){
                    if (ajax.readyState == 1){
                        celda.style.display = "block";
                        celda.innerHTML = loading("Cargando informacion...", BASEURL);
                        estado = 1;
                    }
                    else if (ajax.readyState == 4){
                        if( ajax.status == 200){
                            estado = 4;
                            celda.style.display = "block";
                            imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
                            celda.innerHTML = ajax.responseText;

                        }
                    }
                }
                ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                ajax.send("opcion=1&id_accion="+id_accion+"&index="+index);
            }else{
                celda.style.display = "block";
                imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
            }
        }
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}



/*
function operacionCotizacion(CONTROLLER, BASEURL, index, id_accion, criterio){
alert("agregar");
alert("agregar"+CONTROLLER);
alert("agregar"+BASEURL);
alert("agregar"+index);
alert("agregar"+id_accion);
alert("agregar"+criterio);




    var imagen = document.getElementById("ima"+index);
    var celda = document.getElementById("solicitud"+index);
    var visible = ((celda.style.display == "none")?false:true);
    if (estado == 4){
        alert("agregar2");
        if (visible){
            imagen.src = BASEURL+"/images/botones/iconos/expand.gif";
            celda.style.display="none";
        } else {
            if(celda.innerHTML == ""){
                var ajax = objetoAjax();
                ajax.open("POST", CONTROLLER+"?estado=Cotizacion&accion=Orden", true);
                alert("agregar2");
                ajax.onreadystatechange = function(){
                    if (ajax.readyState == 1){
                        alert("agregar3");
                        celda.style.display = "block";
                        celda.innerHTML = loading("Cargando informacion...", BASEURL);
                        estado = 1;
                    }
                    else if (ajax.readyState == 4){
                        if( ajax.status == 200){
                            alert("agregarno");
                            estado = 4;
                            celda.style.display = "block";
                            imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
                            celda.innerHTML = ajax.responseText;

                        }
                    }
                }
                ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                alert("agregarsend");
                ajax.send("opcion=2&id_accion="+id_accion+"&index="+index+"&criterio="+criterio);
            }else{
                celda.style.display = "block";
                imagen.src = BASEURL+"/images/botones/iconos/collapse.gif";
            }
        }
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}*/


function operacionCotizacion(CONTROLLER, BASEURL, index, id_accion, criterio){


    if (estado == 4){
        var ajax = objetoAjax();
        ajax.open("POST", CONTROLLER+"?estado=Cotizacion&accion=Orden", true);
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 1){
                document.getElementById("capaCentral").innerHTML = loading("Cargando informacion...", BASEURL);
                estado = 1;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    estado = 4;
                   location.href = BASEURL+ajax.responseText;

                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      ajax.send("opcion=2&id_accion="+id_accion+"&index="+index+"&criterio="+criterio);
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}



function formatCantidad(valor, indice) {
    alert("123");
    var num = new Number(valor);//20100608
    //document.getElementById("cantidad"+indice).value =
    num =    formatNumber(num.toFixed(0));//20100608
return num;
}

function obtenerCotizacionCabecera(CONTROLLER, BASEURL, idaccion, operacion, criterio, id_plantilla){


    if (estado == 4){
        var ajax = objetoAjax();
        ajax.open("POST", CONTROLLER+"?estado=Cotizacion&accion=Orden", true);
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 1){
                document.getElementById("capaCentral").innerHTML = loading("Cargando informacion...", BASEURL);
                estado = 1;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    estado = 4;
                    document.getElementById("capaCentral").innerHTML = ajax.responseText;
                    obtenerItemCotizacion(CONTROLLER, BASEURL, idaccion, operacion, criterio, id_plantilla);

                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send("opcion=3&idaccion="+idaccion+"&operacion="+operacion);
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}



function obtenerItemCotizacion(CONTROLLER, BASEURL, idaccion, operacion, criterio, id_plantilla){

var celda = document.getElementById("detalles_items");
    if (estado == 4){
        var ajax = objetoAjax();
        ajax.open("POST", CONTROLLER+"?estado=Cotizacion&accion=Orden", true);
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 1){
                celda.style.display = "block";
                celda.innerHTML = loading("Cargando informacion...", BASEURL);
                estado = 1;
            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    estado = 4;
                    celda.style.display = "block";
                    celda.innerHTML = ajax.responseText;


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send("opcion=7&idaccion="+idaccion+"&operacion="+operacion+"&criterio="+criterio+"&id_accion_plantilla="+id_plantilla);
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
    }
}





				function borradorOptions(selection){
					var ind = 1;
					var tm = selection.length;
					var elim = tm-1;
					while(ind<tm){
						selection.remove(elim);
						elim = elim - 1;
						ind = ind + 1;
					}
				}

function buscarCategorias(CONTROLLER, BASEURL, tipo, indice, categoria, idcatcargado){

    try{

        var tipo = document.getElementById("tipo"+indice).value;
        var categoria = document.getElementById("categoria"+indice);



//if(tipo !="D"){
if(tipo =="M" || tipo=='MO'){


        //Dependiendo del tipo debe dejar modificar el precio
        if(tipo=="M"){
            document.getElementById("precio"+indice).readOnly = true;//JJCASTRO
        }else{
            document.getElementById("precio"+indice).readOnly = false;//JJCASTRO
        }
        //

        if ((tipo!= "")){
            var ajax = objetoAjax();
            var parametros = "opcion=4&tipo="+tipo;

            ajax.open("POST", CONTROLLER+"?estado=Cotizacion&accion=Orden", true);
            ajax.onreadystatechange = function() {
                if (ajax.readyState == 1){

                    categoria.options.length = 0;
                    categoria.options[0] = new Option("Cargando...", "");
                    categoria.disabled = true;

                }
                else if (ajax.readyState == 4){
                    if( ajax.status == 200){
                        var cat = ajax.responseXML.getElementsByTagName("categoria");

                        if ((cat[0].childNodes[0].nodeValue != "null") || (cat==[])){
                            cargarCategorias(categoria, cat, idcatcargado, indice);

                        }
                        else{
                            categoria.options.length = 0;
                            categoria.options[0] = new Option("Seleccione una Categoria", "");
                            categoria.disabled = true;
                        }


                    }
                }
            }
            ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            ajax.send(parametros);
        }


}else{
categoria.disabled = true;
buscarMateriales(CONTROLLER, BASEURL,indice);
}



    }catch (e){
        alert("Error___"+e);
    }

}


function cargarCategorias(combobox, categoria, idcatcargado, indice){

    var option ;
    try{
        if (categoria.length > 1){
            combobox.disabled = false;
            borradorOptions(document.getElementById("categoria"+indice));
            borradorOptions(document.getElementById("material"+indice));
            for (i=0; i<categoria.length; i++){

                var nomciu = categoria[i].childNodes[0].nodeValue;
                var codcat = categoria[i].getAttribute("codcat");
                if(codcat==idcatcargado){
                    option = new Option(nomciu, codcat, codcat,"selected");
                }else{
                    if(((codcat==0)&&(idcatcargado!="")&&(idcatcargado < 0)&&(idcatcargado > 0))||((codcat==0)&&idcatcargado == undefined) ){
                        option = new Option(nomciu, codcat, codcat,"selected");
                    }else{
                        option = new Option(nomciu, codcat, codcat, "");//20100915
                    }

                }
                combobox.options[i] = option;
            }
        }
        else{
            borradorOptions(document.getElementById("categoria"+indice));
            var option = new Option("No hay Categorias", "");
            combobox.options[0] = option;
        }
    }catch (e){
        alert("Error____"+e);

    }


}



////////////////


function buscarMateriales(CONTROLLER, BASEURL,indice, tipoprevio,  categoria, idmatcargado){
try{



    var material = document.getElementById("material"+indice);

    var tipo = document.getElementById("tipo"+indice).value;
    var idcat = document.getElementById("categoria"+indice).value;

    var idsubcat = 0;
    var idtiposubcat = 0;



    if ((tipo!= "")){
        var ajax = objetoAjax();
        var parametros = "opcion=5&tipo="+tipo+"&idcat="+idcat+"&idsubcat="+idsubcat+"&idtiposubcat="+idtiposubcat;

        ajax.open("POST", CONTROLLER+"?estado=Cotizacion&accion=Orden", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 1){

                material.options.length = 0;
                material.options[0] = new Option("Cargando...", "");
                material.disabled = true;

            }
            else if (ajax.readyState == 4){
                if( ajax.status == 200){
                    var cat = ajax.responseXML.getElementsByTagName("material");

                    if (cat[0].childNodes[0].nodeValue != "null"){
                        cargarMateriales(material, cat, idmatcargado, indice);

                }
                    else{
                        material.options.length = 0;
                        material.options[0] = new Option("Seleccione un Material", "");
                        material.disabled = true;
                    }


                }
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send(parametros);
    }

}catch (e){
    alert("Error___."+e);
}

}


function cargarMateriales(combobox, material, idcatcargado, indice){



    var option ;
    try{
        if (material.length > 1){
            combobox.disabled = false;
//            borradorOptions(document.getElementById("categoria"+indice));
//            borradorOptions(document.getElementById("material"+indice));
            for (i=0; i<material.length; i++){

                var nomciu = material[i].childNodes[0].nodeValue;
                var codcat = material[i].getAttribute("codcat");
                if(codcat==idcatcargado){
                    option = new Option(nomciu, codcat, codcat,"selected");
                }else{
                    if(((codcat==0)&&(idcatcargado!="")&&(idcatcargado < 0)&&(idcatcargado > 0))||((codcat==0)&&idcatcargado == undefined) ){
                        option = new Option(nomciu, codcat, codcat,"selected");
                    }else{
                        option = new Option(nomciu, codcat, codcat, "");//20100915
                    }

                }
                combobox.options[i] = option;
            }
        }
        else{
    //        borradorOptions(document.getElementById('material'+indice));
            var option = new Option("No hay Materiales", "");
            combobox.options[0] = option;
        }
    }catch (e){
        alert("Error____."+e);

    }


}



///////////

function precioMaterial(CONTROLLER, BASEURL, index){

    var idmat = document.getElementById("material"+index).value;
    var tipo = document.getElementById("tipo"+index).value;
    if (estado == 4){
        if(idmat.value != ""){
            var param = "opcion=6&idmat="+idmat+"&tipo="+tipo;
            var ajax = objetoAjax();
            ajax.open("POST", CONTROLLER+"?estado=Cotizacion&accion=Orden", true);
            ajax.onreadystatechange = function(){
                if (ajax.readyState == 1){
                    estado = 1;
                }
                else if (ajax.readyState == 4){
                    if( ajax.status == 200){
                        estado = 4;
                        var value = ajax.responseXML.getElementsByTagName("respuesta");

                        if (value[0].childNodes[0].nodeValue == "null"){
                            alert("Este material no tiene un precio asignado");
                        }
                        else{

                        var val = value[0].childNodes[0].nodeValue;
                        val = formatNumber(val);
                        document.getElementById("precio"+index).value = val;
                        }
                    }
                }
            }
            ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            ajax.send(param);
        }else{
            trailer.style.background = "#FFFFFF";
        }
    }
    else{
        alert("Un momento Por Favor Los Datos Estan Siendo consultados en el servidor...");
    }
}



/////////


//////////////////////////////////////////////
/**
 * Comment
 * jjcastro
 */
function agregar(numero) {

 document.getElementById("Maximo").value = numero;

}



var n=1;
function agregarCelda(){
n++;
var index = 0;

var tam_items = document.getElementById("tam_items").value;

document.getElementById("tam_items").value = (tam_items*1)  + 1;



var tabla = document.getElementById("tablaValores");
//Obtengo el tamano de la tabla para ubicar la ultima celda
var numrow = tabla.rows.length;
//alert(numrow);
var tbody = tabla.insertRow(numrow);
var td1 = tbody.insertCell(0);
//td1.colSpan = "5";

//<tr class="fila">
//<td colspan="5">
var x1='';
//x1 = x1 + '<table id="tablaValores"><tr><td>';
x1 = x1 + '<table width="1600" height="30" border="1" style="border-collapse:collapse; border-color:#E4E4E4">';
x1 = x1 + '<tr class = "fila">';

x1 = x1 + '<td width="63" rowspan="2" align="center"><table width="100%"  align="left"><tr>';
x1 = x1 + '<td width="17" ><div align="right" class="fila">'+n+'</div></td>';
x1 = x1 + '<td width="33"><div align="center" id="resta">';
x1 = x1 + '<img id="imgmas'+n+'" name="imgmas'+n+'" src="<%=BASEURL%>/images/botones/iconos/mas.gif" alt="Agregar fila" onClick="agregarCelda()" width="12" height="12"> ';
x1 = x1 + '<img id="imgini'+n+'" name="imgini'+n+'" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" alt="Eliminar fila" onClick="borrarDatosListaNew('+n+');"  width="12" height="12">';
x1 = x1 + '</div></td>';
x1 = x1 + '</tr></table></td>';

x1 = x1 + '</tr>';

x1 = x1 + '<tr class = "fila">';


x1 = x1 + '<td height="27" width="40"><div align="center">';
x1 = x1 + '<select name="radiobutton'+n+'" id="radiobutton'+n+'" onChange="cargarTipos(this.selectedIndex,'+n+')" style="width:40">';
x1 = x1 + '<option value="-1">Seleccione</option>';
x1 = x1 + '<option value="M">M</option>';
x1 = x1 + '<option value="D">D</option>';
x1 = x1 + '<option value="O">O</option>';
x1 = x1 + '</select></div></td>';


x1 = x1 + '<td width="49"><div align="center">';
x1 = x1 + '<select name="menutipos'+n+'" id="menutipos'+n+'" onChange="updateList(this.options[this.selectedIndex],'+n+');" style="width:50">';
x1 = x1 + '<option value="-1" selected >Seleccione...</option>';
x1 = x1 + '</select>';
x1 = x1 + '</div></td>';


//


x1 = x1 + '<td width="10"><span class="barratitulo"><input name="textfieldx'+n+'" type="text" id="textfieldx'+n+'" onKeyUp="buscar(document.forma.producto'+n+',this);instantValue('+n+');xperience(document.forma.cantidad'+n+'.value,'+n+');" size="10"></span></td>';





x1 = x1 + '<td width="830"><span class="barratitulo">';
x1 = x1 + '<select name="producto'+n+'" id="producto'+n+'" style="width:830" onChange="actualizarPrecio(this,'+n+'); xperience(document.forma.cantidad'+n+'.value,'+n+')">';
x1 = x1 + '<option value="-1" selected>Seleccione ...</option>';


x1 = x1 + '</select>';
x1 = x1 + '</span></td>';
///

x1 = x1 + '<td width="10"><div align="center">';
x1 = x1 + '<input name="cantidad'+n+'" type="text" id="cantidad'+n+'"  style="text-align:right" onFocus="instantValue   ('+n+')" onKeyUp="xperience(this.value,'+n+');colocarvalor(this.value,'+n+');soloNumeros(this.id)" value="0" size="10"  >';
x1 = x1 + '</div></td>';

x1 = x1 + '<td width="10"><div align="center">';
x1 = x1 + '<input name="precio'+n+'" type="text" id="precio'+n+'" value="0.00" size="10"  style="text-align:right"  onKeyUp="xperience(document.getElementById(\'cantidad'+n+'\').value,'+n+');soloNumeros(this.id)">';//fase3
x1 = x1 + '<input name="valor_2'+n+'" type="hidden" id="valor_2'+n+'">';
x1 = x1 + '</div></td>';

x1 = x1 + '<td width="10"><div align="center">';
x1 = x1 + '<input name="base'+n+'" type="text" id="base'+n+'" value="0.00" size="10" readonly style="text-align:right" >';
x1 = x1 + '<input name="base_2'+n+'" type="hidden" id="base_2'+n+'">';
x1 = x1 + '</div></td>';

x1 = x1 + '<td width="18"><div align="center">';
x1 = x1 + '<input name="nota'+n+'" type="text" id="nota'+n+'" readonly onclick="fila=this.alt;control(event,nota'+n+')" name="nota'+n+'" id="nota'+n+'" width="18" >';
x1 = x1 + '</div></td>';

x1 = x1 + '<td width="20">';
x1 = x1 + '<input name="compra'+n+'" type="checkbox" value="checkbox'+n+'" checked onClick="cambiarCantcompra('+n+')" id="compra'+n+'">';
x1 = x1 + '</td><td width="10">';
x1 = x1 + '<input name="cantidad_compra'+n+'" type="text" id="cantidad_compra'+n+'"  style="text-align:right" onFocus="instantValue('+n+')" onBlur="validarvalor(this.value,'+n+')" value="0" size="10"  >';
x1 = x1 + '</td>';
x1 = x1 + '</tr>';
x1 = x1 + '</table>';
x1 = x1 + '<input name="codp'+n+'" id="codp'+n+'" type="hidden" value="0">';


//




//

x1 = x1 + '<input name="indiceDelete'+n+'" id="indiceDelete'+n+'" type="hidden" value="-1">';



td1.innerHTML = x1;



}


/////////////////






function agregarCelda1(CONTROLLER, BASEURL) {

n++;
var index = 0;
var n;

var tam_items = document.getElementById("tam_items").value;

n = tam_items;


document.getElementById("tam_items").value = (tam_items*1)  + 1;
var tabla = document.getElementById("tablaValores");

//Obtengo el tamano de la tabla para ubicar la ultima celda
var numrow = tabla.rows.length;

//numrow = (tam_items*1)  + 1;
var secuencia = (tam_items*1)  + 1;


//n = numrow;
n = tam_items;

//alert("longitud......"+numrow);
var tbody;//tabla.insertRow(numrow);

 if(navigator.appName=='Microsoft Internet Explorer')
                {tbody = tabla.insertRow();
                }else{
                    tbody = tabla.insertRow(-1);
}

//alert("tam_items2______"+tam_items);
var td1 = tbody.insertCell(0);

var x1='';
x1 = x1 + '<table width="1540"  height="30" border="1"   style="border-collapse:collapse; border-color:#E4E4E4" cellpadding="0">';
x1 = x1 + '<tr>';
x1 = x1 + '<td width="107" rowspan="2" align="center">';
x1 = x1 + '<table width="100%" border="0" align="center" cellpadding="0" style="border-collapse:collapse; ">';
x1 = x1 + '<tr>';
x1 = x1 + '<td width="17" height="20">';
x1 = x1 + '<div align="right" class="fila">'+secuencia+'</div>';
x1 = x1 + '</td>';
x1 = x1 + '<td width="33">';
x1 = x1 + '<div align="center" id="resta">';
x1 = x1 + '<img id="imgmas'+n+'" name="imgmas'+n+'" src="'+BASEURL+'/images/botones/iconos/mas.gif" alt="Agregar fila" onClick="agregarCelda1(\''+CONTROLLER+'\', \''+BASEURL+'\')" width="12" height="12">';
x1 = x1 + '<img id="imgini'+n+'" name="imgini'+n+'" src="'+BASEURL+'/images/botones/iconos/menos1.gif" alt="Eliminar fila" onClick="borrarDatosListaNew1('+n+');"  width="12" height="12">';
x1 = x1 + '</div>';
x1 = x1 + '</td>';
x1 = x1 + '</tr>';
x1 = x1 + '</table>';
x1 = x1 + '</td>';
x1 = x1 + '</tr>';
x1 = x1 + '<tr>';
x1 = x1 + '<td height="27" width="45">';
x1 = x1 + '<div align="center">';
x1 = x1 + '<select name="tipo'+n+'" id="tipo'+n+'" onChange="buscarCategorias(\''+CONTROLLER+'\', \''+BASEURL+'\',this.selectedIndex,'+n+', 0)" style="width:40">';
x1 = x1 + '<option value="-1">Seleccione</option>';
x1 = x1 + '<option value="M"  >Material</option>';
x1 = x1 + '<option value="MO" >Material Oficial</option>';
x1 = x1 + '<option value="D"  >Mano de Obra</option>';
x1 = x1 + '<option value="O"  >Otros</option>';
x1 = x1 + '</select>';
x1 = x1 + '</div>';
x1 = x1 + '</td>';
x1 = x1 + '<td width="146">';
x1 = x1 + '<div align="center">';
x1 = x1 + '<select name="categoria'+n+'" id="categoria'+n+'" onChange="buscarMateriales(\''+CONTROLLER+'\', \''+BASEURL+'\','+n+');" style="width:50">';
x1 = x1 + '<option value="-1" selected >Seleccione...</option>';
x1 = x1 + '</select>';
x1 = x1 + '<img src="'+BASEURL+'/images/botones/iconos/lupa.gif" width="12" height="12" onClick="abrirPaginaDos(\''+BASEURL+'/jsp/delectricaribe/cotizacion/filtrocotizacion/listadoCategoria.jsp\', \'\', '+n+');" title="Filtrar Categoria" style="cursor:hand" ></div>';
x1 = x1 + '</td>';
x1 = x1 + '<td width="60">';

x1 = x1 + '<input name="idsubcategoria'+n+'" type="hidden" id="idsubcategoria'+n+'" value="0" ">';
x1 = x1 + '<input name="idtiposubcategoria'+n+'" type="hidden" id="idtiposubcategoria'+n+'" value="0">';

x1 = x1 + '<span class="barratitulo">';
x1 = x1 + '<input name="textfieldx'+n+'" type="text" id="textfieldx'+n+'" onKeyUp="buscarfiltromaterial(document.forma.material'+n+',this);instantValue('+n+');xperience(document.forma.cantidad'+n+'.value,'+n+');" size="10">';
x1 = x1 + '</span>';
x1 = x1 + '</td>';
x1 = x1 + '<td width="700"><div align="center"><span class="barratitulo">';
x1 = x1 + '<select name="material'+n+'" id="material'+n+'" style="width:740px" onChange="precioMaterial(\''+CONTROLLER+'\', \''+BASEURL+'\','+n+');" >';
x1 = x1 + '<option value="-1" selected >Seleccione...</option>';
x1 = x1 + '</select>';
x1 = x1 + '</span>';
x1 = x1 + '</div></td>';

x1 = x1 + '<td width="68">';
x1 = x1 + '<div align="center">';
x1 = x1 + '<input name="cantidad'+n+'" type="text" id="cantidad'+n+'" onChange="calcularpndcompra('+n+')" value="0" style="text-align:right" onFocus="instantValue('+n+')" onKeyUp="xperience(this.value,'+n+');colocarvalor(this.value,'+n+');soloNumeros(this.id)"  size="8"  >';
x1 = x1 + '</div>';
x1 = x1 + '</td>';
x1 = x1 + '<td width="68">';
x1 = x1 + '<div align="center">';
x1 = x1 + '<input name="cantcomp'+n+'" type="hidden" id="cantcomp'+n+'" value="0" >';
x1 = x1 + '<input name="pndcompra'+n+'" type="text" id="pndcompra'+n+'"  value="0" style="text-align:right"  readonly size="7">';
x1 = x1 + '</div>';
x1 = x1 + '</td>';
x1 = x1 + '<td width="68">';
x1 = x1 + '<div align="center">';
x1 = x1 + '<input name="precio'+n+'" type="text" id="precio'+n+'" value="0"  size="10"  style="text-align:right" onKeyUp="xperience(document.getElementById(\'cantidad'+n+'\').value,'+n+');soloNumeros(this.id)">';
x1 = x1 + '<input name="valor_2'+n+'" type="hidden" id="valor_2'+n+'">';
x1 = x1 + '</div>';
x1 = x1 + '</td>';

x1 = x1 + '<td width="68">';
x1 = x1 + '<div align="center">';
x1 = x1 + '<input name="base'+n+'" type="text" id="base'+n+'"value="0.00" size="10" readonly style="text-align:right" >';
x1 = x1 + '<input name="base_2'+n+'" type="hidden" id="base_2'+n+'">';
x1 = x1 + '</div></td>';
x1 = x1 + '<td width="20">';
x1 = x1 + '<div align="center">';
x1 = x1 + '<input name="nota'+n+'" type="text" id="nota'+n+'" alt='+n+'  value=""  readonly onClick="fila=this.alt;control(event,nota'+n+')" size="10">';
x1 = x1 + '</div>';
x1 = x1 + '</td>';
x1 = x1 + '<td width="23">';
x1 = x1 + '<div align="center">';
x1 = x1 + '<input name="compra'+n+'" type="checkbox" value="checkbox'+n+'"   id="compra'+n+'" checked onClick="cambiarCantcompra('+n+')" >';
x1 = x1 + '</div>';
x1 = x1 + '</td>';
x1 = x1 + '<td width="74">';
x1 = x1 + '<div align="center">';
x1 = x1 + '<input name="cantidad_compra'+n+'"  type="text" id="cantidad_compra'+n+'"  style="text-align:right" onFocus="instantValue('+n+')" onBlur="validarvalor(this.value,'+n+')" size="8"  >';
x1 = x1 + '</div>';
x1 = x1 + '</td>';
x1 = x1 + '</tr>';
x1 = x1 + '</table>';
x1 = x1 + '<input name="indiceDelete'+n+'" id="indiceDelete'+n+'" type="hidden" value="-1">';

td1.innerHTML = x1;

}



function buscarfiltromaterial(lista,campo){
        var nombreNav = navigator.appName;
	var texto = campo.value.toLowerCase();
        var v ;
        var preg = nombreNav == 'Microsoft Internet Explorer'? true:false;
	for( var i=0; i<lista.length; i++ ){

		if(preg) v = lista[i].innerText.toLowerCase();
                else  v = lista[i].textContent.toLowerCase();
		if ( v.indexOf(texto) >= 0 ){
			lista.selectedIndex = i;
			break;
		}
	}
}


function instantValue(indice){
    alert("");
    var indsel = document.getElementById("producto"+indice);
    var selind = indsel.selectedIndex;
    var idx = indsel.options[selind].id;
    document.getElementById("codp"+indice).value= idx;
    document.getElementById("valor_2"+indice).value = indsel.options[selind].value;
}



function xperience(cant,numero){
    var vlr = unformatNumber(document.getElementById("precio"+numero).value.replace (/[,]+/gi,''));//document.getElementById("precio"+numero).value; //fase3
    cant = cant.replace (/[,]+/gi,'');
    var tot = cant*vlr;

    document.getElementById("valor_2"+numero).value = vlr;
    document.getElementById("base_2"+numero).value = tot;
    document.getElementById("base"+numero).value = tot;
    document.getElementById("base"+numero).value = formatNumber(document.getElementById("base"+numero).value);

}


/*
 *Created by: Ing. Jose Castro
 *Funtion: Asigna el mismo valor de cantidad a cantidad compra
 */
function colocarvalor(cant,numero){
    document.getElementById("cantidad_compra"+numero).value =document.getElementById("cantidad"+numero).value;

}


    /**
     *JJCastro
     */
    function soloNumeros(id) {
     var precio = document.getElementById(id).value;
     precio =  precio.replace(/[^0-9^.]+/gi,"");
     document.getElementById(id).value = precio;

    }


function cambiarCantcompra(indice){

    var variable = document.getElementById("compra"+indice).checked;

    if(variable ==  true){
        document.getElementById("cantidad_compra"+indice).readOnly = false;//JJCASTRO
    }else{
        document.getElementById("cantidad_compra"+indice).readOnly = true;//JJCASTRO
        document.getElementById("cantidad_compra"+indice).value = 0;//JJCASTRO
    }

}


/*
 *Created by: Ing. Jose Castro
 *Funtion: Valida el valor de la columna cantidad compra que este en el rango de 0 y el valor en cantidad
 */
function validarvalor(cant,numero){

    var cantidad = document.getElementById("cantidad"+numero).value ;
    var valor      = document.getElementById("cantidad_compra"+numero).value ;
    cantidad = unformatNumber(cantidad);
    valor = unformatNumber(valor);


    if((valor<0)|| (valor>cantidad)){
        alert("La cantidad digitada no es valida");
        document.getElementById("cantidad_compra"+numero).value = cantidad;
    }

}



/**
 * Comment
 * jjcastro
 */
function borrarDatosListaNew1(ind) {


    var indc = document.getElementById("indiceDelete"+ind).value;
    var tabla = document.getElementById("tablaValores");
    var numrow = tabla.rows.length;
    var max = document.getElementById("tam_items");
    var i=0;
    var cont = 1;
    for (i=0;i<=max.value;i++)
    {

        if(document.getElementById("indiceDelete"+i) ==null){

        }else{
            document.getElementById("indiceDelete"+i).value = cont;

            cont ++;
        }
    }




    var borrar = document.getElementById("indiceDelete"+ind);
    t=document.getElementById("tablaValores").deleteRow(borrar.value -1);
    alert('Fila eliminada!');
    cont = 1;
    for (i=0;i<=max.value;i++)
    {


        if(document.getElementById("indiceDelete"+i) ==null){

        }else{
            document.getElementById("indiceDelete"+i).value = cont;
            cont ++;
        }
    }

}


function abrirPagina(url, nombrePagina)
{



  var wdth = screen.width - screen.width * 0.20;
  var hght = screen.height - screen.height * 0.40;
  var lf = screen.width * 0.1;
  var tp = screen.height * 0.2;
  var options = "menubar=yes,scrollbars=yes,resizable=yes,status=yes,titlebar=no," +
                "toolbar=no," + setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( document.window != null && !hWnd.opener )
    hWnd.opener = document.window;
}


function abrirPaginaDos(url, nombrePagina, ind)
{


    var tipo = document.getElementById("tipo"+ind).value;
    var idcat = document.getElementById("categoria"+ind).value;



  url = url + "?ind="+ind+"&tipo="+tipo+"&idcat="+idcat;

  var wdth = screen.width - screen.width * 0.20;
  var hght = screen.height - screen.height * 0.40;
  var lf = screen.width * 0.1;
  var tp = screen.height * 0.2;
  var options = "menubar=yes,scrollbars=yes,resizable=yes,status=yes,titlebar=no," +
                "toolbar=no," + setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( document.window != null && !hWnd.opener )
    hWnd.opener = document.window;
}


/**
* envia a la pagina buscar solicitud
* @author ivargas
* @param controller controlador
* @param id_solic id de la solicitud
* @since 2011-02-04
*/
function regresar(controller,id_solic){

                    document.getElementById("forma").action=controller+"?estado=Clientes&accion=Ver&opcion=buscarsolicitud&idsolicitud="+id_solic;
                    document.getElementById("forma").submit();
}

/**
* calcula los valores de la cantidad pendiente por comprar y valida el valor digitado en cantidad
* @author ivargas
* @param indice numero de fila donde se encuentra la cantidad modificada
* @since 2011-02-04
*/
function  calcularpndcompra(indice){
    var num1 = new Number(document.getElementById("cantidad"+indice).value);
    var num2 = num1.toFixed(1);
    if(num2<parseFloat(document.getElementById("cantcomp"+indice).value)){
        alert("la cantidad no puede ser inferior a la cantidad comprada");
        document.getElementById("cantidad"+indice).value=document.getElementById("cantcomp"+indice).value;
    }
    document.getElementById("pndcompra"+indice).value=document.getElementById("cantidad"+indice).value-document.getElementById("cantcomp"+indice).value
    document.getElementById("cantidad_compra"+indice).value =document.getElementById("pndcompra"+indice).value;
}





/**
 * Comment
 */
function guardarCotizacion(CONTROLLER, BASEURL, operacion) {

    var sendit;

    if(operacion == "guardar"){
        sendit = confirm("Confirma que quiere guardar la cotizacion?");
    }

    if(operacion == "crear"){
        sendit = confirm("Confirma que ya esta terminada la cotizacion?");
    }

    if(operacion == "modificar"){
        sendit = confirm("Confirma que desea modificar la cotizacion?");
    }

    if(sendit==true){
        var tabla = document.getElementById("tablaValores");//jjcastro
        var numrow = tabla.rows.length;
        var tam_items = document.getElementById("tam_items");




        var param = "opcion=8";



        var com = "";


        if(tam_items!=null){
            tam_items = tam_items.value;
            param += "&tam_items="+tam_items+"&operacion="+operacion;

            param += "&multiservicios="+document.getElementById("multiservicios").value;
            param += "&fecha="+document.getElementById("fecha").value;
            param += "&solic="+document.getElementById("textfield").value;

            var i = 0;
            var sw=true;
            for(i=0;i<tam_items;i++){

                if(document.getElementById("indiceDelete"+i)!=null){

                    if(parseFloat(document.getElementById("cantidad_compra"+i).value)>parseFloat(document.getElementById("pndcompra"+i).value )){
                        sw=false;
                        document.getElementById("cantidad_compra"+i).parentNode.parentNode.parentNode.parentNode.style.background='red';
                    }
                    param += "&tipo"+i+"="+document.getElementById("tipo"+i).value;
                    param += "&categoria"+i+"="+document.getElementById("categoria"+i).value;
                    param += "&material"+i+"="+document.getElementById("material"+i).value;
                    param += "&cantidad"+i+"="+document.getElementById("cantidad"+i).value;
                    param += "&precio"+i+"="+document.getElementById("precio"+i).value;
                    param += "&base"+i+"="+document.getElementById("base"+i).value;
                    param += "&nota"+i+"="+document.getElementById("nota"+i).value;
                    com = document.getElementById("compra"+i).checked?"S":"N";
                    param += "&compra"+i+"="+com;
                    param += "&cantidad_compra"+i+"="+document.getElementById("cantidad_compra"+i).value;
                    if(document.getElementById("idcotdet"+i)==null){
                        param += "&idcotdet"+i+"="+"";
                    }else{
                        param += "&idcotdet"+i+"="+document.getElementById("idcotdet"+i).value;
                    }
                }
            }

        }
        var celda = document.getElementById("detalles_items");

        if(sw){
            if (estado == 4){

                var ajax = objetoAjax();
                ajax.open("POST", CONTROLLER+"?estado=Cotizacion&accion=Orden", true);
                ajax.onreadystatechange = function(){
                    if (ajax.readyState == 1){
                        celda.style.display = "block";
                        celda.innerHTML = loading("Cargando informacion...", BASEURL);
                        estado = 1;
                    }
                    else if (ajax.readyState == 4){
                        if( ajax.status == 200){
                            var response=ajax.responseText.toString().split("-");
                            if(response[1]==0){
                                alert("No hay datos para "+operacion);
                            }else{
                                alert("Datos Guardados con Exito...");
                            }
                            if(operacion == "crear"){
                                location.href = CONTROLLER+response[0];
                            }else{
                                location.href = BASEURL+response[0];
                            }


                        }
                    }
                }
                ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                ajax.send(param);
            }
            else{
                alert("Un momento Por Favor Los Datos Estan Siendo Cargados...");
            }
        }else{
            alert("La cantidad a comprar debe ser menor a la cantidad pendiente");
        }


    }//cierre del confirm
//

}/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


