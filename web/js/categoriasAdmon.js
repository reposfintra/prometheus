/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
  
    listarCategorias();
    maximizarventana();
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $('.solo-numeric').live('keypress', function (event) {
        return numbersonly(this, event);
    });

    $('.solo-numeric').live('blur', function (event) {
        this.value = numberConComas(this.value);
    });   
    
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    });   

});

function listarCategorias(){
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    if ($("#gview_tabla_categorias").length) {
         refrescarGridCategorias();
     }else {
        jQuery("#tabla_categorias").jqGrid({
            caption: 'Categor�as',
            url: url,
            datatype: 'json',
            height: 350,
            width: 750,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Estado', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', hidden:true, sortable: true, align: 'center', width: '100px', key:true},                 
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600'},
                {name: 'descripcion', index: 'descripcion', hidden: true, sortable: true, align: 'center', width: '700px'},
                {name: 'reg_status', index: 'reg_status', sortable: true, hidden:true, align: 'center', width: '90'},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '100'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            pager:'#page_tabla_categorias',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                       opcion: 57
                     }
            },   
            gridComplete: function (index) {
                var ids = jQuery("#tabla_categorias").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var reg_status = $("#tabla_categorias").getRowData(cl).reg_status;
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarCategoria('" + cl + "');\">";
                    if(reg_status !== ''){
                        actdes = "<img src='/fintra/images/botones/iconos/check-blue.png' align='absbottom'  name='activar' id='activar' width='15' height='15' title ='Activar Categor�a'  onclick=\"mensajeConfirmAction('Esta seguro de activar la Categor�a seleccionado?','250','150',CambiarEstadoCategoria,'" + cl + "');\">";
                    }else{
                        actdes = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular Categor�a'  onclick=\"mensajeConfirmAction('Esta seguro de anular la Categor�a seleccionada?','250','150',CambiarEstadoCategoria,'" + cl + "');\">";  
                    }
                                     
                    jQuery("#tabla_categorias").jqGrid('setRowData', ids[i], {actions: ed + '   ' + '   ' + actdes});
                }
            },           
            loadError: function(xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_categorias",{search:false,refresh:false,edit:false,add:false,del:false});
        jQuery("#tabla_categorias").jqGrid("navButtonAdd", "#page_tabla_categorias", {
            caption: "Nuevo", 
            title: "Agregar nueva categor�a",           
            onClickButton: function() {
               crearCategoria(); 
            }
        });
     }
}


function refrescarGridCategorias(){    
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    jQuery("#tabla_categorias").setGridParam({
        url: url,
        datatype: 'json',       
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 57
            }
        }       
    });
    
    jQuery('#tabla_categorias').trigger("reloadGrid");
}

function crearCategoria() {
    $('#nomcategoria').val('');
    $('#desccategoria').val('');
    $('#div_categoria').fadeIn('slow');
    AbrirDivCrearCategoria();
}

function AbrirDivCrearCategoria() {
    $("#div_categoria").dialog({
        width: 700,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR CATEGORIA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarCategoria();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarCategoria() {  
    var nomCategoria = $('#nomcategoria').val();
    var descCategoria = $('#desccategoria').val();
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    if (nomCategoria !== ''  && descCategoria !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 58,
                nombre: nomCategoria,
                descripcion: descCategoria
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {                   
                        refrescarGridCategorias();
                        mensajesDelSistema("Se cre� la Categoria", '250', '150', true);
                        $('#nomcategoria').val('');
                        $('#desccategoria').val('');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear la categoria!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }
}

function editarCategoria(cl) {

    $('#div_editar_categoria').fadeIn("slow");
    var fila = jQuery("#tabla_categorias").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    $('#idCategoriaEdit').val(cl);
    $('#nomcategoriaEdit').val(nombre);
    $('#desccategoriaEdit').val(descripcion);
    AbrirDivEditarCategoria();
    cargarItemsCategoria();
}

function AbrirDivEditarCategoria() {
    $("#div_editar_categoria").dialog({
        width: 700,
        height: 675,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'EDITAR CATEGORIA',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarCategoria() {
    var nombre = $('#nomcategoriaEdit').val();
    var descripcion = $('#desccategoriaEdit').val();
    var idCategoria = $('#idCategoriaEdit').val();
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    if (nombre !== '' && descripcion !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 59,
                nombre: nombre,
                descripcion: descripcion,
                idCategoria: idCategoria
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridCategorias();                    
                        mensajesDelSistema("Se actualiz� la Categoria", '250', '150', true);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar la Categoria!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');
    }

}

function CambiarEstadoCategoria(cl) { 
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        data: {
            opcion: 60,
            id: cl,
            query:'activarInactivarCategoria'
        },
        success: function (json) {
              if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarGridCategorias();
                }
            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado de la categor�a!!", '250', '150');
            }           
        }, error: function (result) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarItemsCategoria(){
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    if ($("#gview_tabla_items_categoria").length) {
         refrescarGridItemsCategoria();
     }else {
        jQuery("#tabla_items_categoria").jqGrid({
            caption: 'Elementos Categor�a',
            url: url,
            datatype: 'json',
            height: 310,
            width: 615,
            colNames: ['Id', 'Nombre', 'Valor', 'Por Defecto', 'Estado', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', hidden:true, sortable: true, align: 'center', width: '100px', key:true},                 
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'left', width: '580'},
                {name: 'valor', index: 'valor', sortable: true, editable:true, width: 110, align: 'right', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'is_default', index: 'is_default', sortable: true, align: 'center', width: '90'},
                {name: 'reg_status', index: 'reg_status', sortable: true, hidden:true, align: 'center', width: '90'},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '100'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            pager:'#page_tabla_items_categoria',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                type: "POST",
                async:false,
                data:{
                       opcion: 61,
                       idCategoria: $('#idCategoriaEdit').val()
                     }
            },   
            gridComplete: function (index) {
                var ids = jQuery("#tabla_items_categoria").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var reg_status = $("#tabla_items_categoria").getRowData(cl).reg_status;
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarItemCategoria('" + cl + "');\">";
                    if (reg_status !== '') {
                        actdes = "<img src='/fintra/images/botones/iconos/check-blue.png' align='absbottom'  name='activar' id='activar' width='15' height='15' title ='Activar Item'  onclick=\"mensajeConfirmAction('Esta seguro de activar el item seleccionado?','250','150',CambiarEstadoItemCategoria,'" + cl + "');\">";
                    } else {
                        actdes = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular Item'  onclick=\"mensajeConfirmAction('Esta seguro de anular el item seleccionado?','250','150',CambiarEstadoItemCategoria,'" + cl + "');\">";
                    }
                     jQuery("#tabla_items_categoria").jqGrid('setRowData', ids[i], {actions: ed + '   ' + actdes});
                }
            },           
            loadError: function(xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_items_categoria",{search:false,refresh:false,edit:false,add:false,del:false});
        jQuery("#tabla_items_categoria").jqGrid("navButtonAdd", "#page_tabla_items_categoria", {
            caption: "Nuevo", 
            title: "Agregar nuevo item",           
            onClickButton: function() {
               crearItemsCategoria(); 
            }
        });
     }
}


function refrescarGridItemsCategoria(){    
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    jQuery("#tabla_items_categoria").setGridParam({
        url: url,
        datatype: 'json',       
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: 61,
                idCategoria: $('#idCategoriaEdit').val()
            }
        }       
    });
    
    jQuery('#tabla_items_categoria').trigger("reloadGrid");
}

function crearItemsCategoria(){  
    $('#div_items_categoria').fadeIn('slow');
    AbrirDivCrearItemsCategoria();
}

function AbrirDivCrearItemsCategoria(){ 
    $('#idItem').val('');
    $('#nomItem').val('');  
    $('#valor_item').val('');  
    $('input[name=default]').val(['N']);
    $('#div_items_categoria').fadeIn('slow');
    $("#div_items_categoria").dialog({
        width: 'auto',
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR ELEMENTO CATEGORIA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
              guardarItemCategoria();
            },
            "Salir": function () {  
                $(this).dialog("destroy");
            }
        }
    });
}

function editarItemCategoria(cl){
    
    $('#div_items_categoria').fadeIn("slow");
    var fila = jQuery("#tabla_items_categoria").getRowData(cl);  
    var nombre = fila['descripcion'];
    var valor = fila['valor'];
    var is_default = fila['is_default'];
        
    $('#idItem').val(cl);      
    $('#nomItem').val(nombre);   
    $('#valor_item').val(numberConComas(valor));
    $('input[name=default]').val([is_default]); 
    AbrirDivEditarItemCategoria();
}

function AbrirDivEditarItemCategoria(){
      $("#div_items_categoria").dialog({
        width: 'auto',
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'EDITAR ELEMENTO CATEGORIA',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              guardarItemCategoria();
            },
            "Salir": function () {
               $(this).dialog("destroy");
            }
        }
    });
}

function guardarItemCategoria(){    
  
    var nombre = $('#nomItem').val();  
    var valor = numberSinComas($('#valor_item').val()); 
    var is_default = $("input[name=default]:checked").val();    
    
    var url = './controlleropav?estado=Maestro&accion=Proyecto';
    if(nombre!=='' && valor !== ''){
        if(parseFloat(valor)<=0)
        {
            mensajesDelSistema("El valor del item debe ser mayor que 0", '250', '150'); 
        }else{
            loading("Espere un momento por favor...", "270", "140");
            setTimeout(function(){
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        opcion: ($('#idItem').val()==='')? 62:63,
                        idItem:$('#idItem').val(), 
                        idCategoria: $('#idCategoriaEdit').val(),
                        nombre: nombre,
                        valor: valor,
                        is_default: is_default
                    },
                    success: function(json) {
                        if (!isEmptyJSON(json)) {

                            if (json.error) {
                                $("#dialogLoading").dialog('close');
                                mensajesDelSistema(json.error, '250', '150');                          
                                return;
                            }

                            if (json.respuesta === "OK") {
                                $("#dialogLoading").dialog('close');
                                refrescarGridItemsCategoria(); 
                                mensajesDelSistema("Se guardaron los cambios satisfactoriamente", '250', '150', true);     
                                $("#div_items_categoria").dialog('close');

                            }

                        } else {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lo sentimos no se pudo guardar el item!!", '250', '150');
                        }

                    }, error: function(xhr, ajaxOptions, thrownError) {
                        alert("Error: " + xhr.status + "\n" +
                              "Message: " + xhr.statusText + "\n" +
                              "Response: " + xhr.responseText + "\n" + thrownError);
                    }
                }); 
            }, 500);
        }
    }else{
       mensajesDelSistema("Hay campos obligatorios que faltan por llenarse", '250', '150');
    }
}

function CambiarEstadoItemCategoria(cl) { 
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Maestro&accion=Proyecto",
        data: {
            opcion: 60,
            id: cl,
            query:'activarInactivarItemCategoria'
        },
        success: function (json) {
              if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                   refrescarGridItemsCategoria();
                }
            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado del item!!", '250', '150');
            }           
        }, error: function (result) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function numbersonly(myfield, e, dec)
{
    var key;
    var keychar;
    
    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

// control keys
    if ((key == null) || (key == 0) || (key == 8) ||
            (key == 9) || (key == 13) || (key == 27))
        return true;

// numbers
    else if ((("0123456789.").indexOf(keychar) > -1))
        return true;

// decimal point jump
    else if (dec && (keychar == "."))
    {
        myfield.form.elements[dec].focus();
        return false;
    }
    else
        return false;
}


function numberConComas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajeConfirmAction(msj, width, height, okAction, id) {  
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function igualarcampo(campo, campo2) {
    var valor = $('#' + campo2).val();
    $('#' + campo).val(valor);
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}


function ordenarCombo(cboId){
    var valor =$('#'+cboId).val();
    var options = $("#"+ cboId +" option");                  
    options.detach().sort(function (a, b) {              
        var at = $(a).text();
        var bt = $(b).text();
        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
    });
    options.appendTo("#"+cboId);   
    $("#"+ cboId).val(valor);
}