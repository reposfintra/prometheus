/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
//  tipoDistribuciones();
    tipoProyecto();
    $("#agregar").click(function () {
        agregarAsociacion();
    });
    $("#quitar").click(function () {
        quitarAsociacion();
    });
    $("#agregarRel").click(function () {
        agregarAsociacionSistribucion();
    });
    $("#quitarRel").click(function () {
        quitarAsociacionSistribucion();
    });
});
function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function onKeyDecimal(e, num) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if (document.getElementById(num.id).value.indexOf('.') !== -1 && keynum === 6)
        return false;
    if ((keynum === 8 || keynum === 48 || keynum === 46))
        return true;
    if (keynum <= 47 || keynum >= 58)
        return false;
    return /\d/.test(String.fromCharCode(keynum));
}

function tipoDistribuciones() {
    var grid_tabla = jQuery("#tabla_distribuciones");
    if ($("#gview_tabla_distribuciones").length) {
        reloadGridDistribuciones(grid_tabla, 16);
    } else {
        grid_tabla.jqGrid({
//            caption: "DISTRIBUCIONES",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '710',
            colNames: ['Tipo solicitud', '# Distribucion', '% Opav', '% Fintra', '% Interventoria', '% Provintegral', '% Eca', '% Iva', 'Valor agregado', 'desasociar'],
            colModel: [
                {name: 'distribucion', index: 'distribucion', width: 150, sortable: true, align: 'left', hidden: false},
                {name: 'tipo', index: 'tipo', width: 80, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_opav', index: 'porc_opav', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_fintra', index: 'porc_fintra', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_interventoria', index: 'porc_interventoria', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_provintegral', index: 'porc_provintegral', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_eca', index: 'porc_eca', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_iva', index: 'porc_iva', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'valor_agregado', index: 'valor_agregado', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'desasociar', index: 'desasociar', width: 60, sortable: true, align: 'center', hidden: false, search: true}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false
            },
            gridComplete: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    desasociar = '<img src = "/fintra/images/botones/iconos/deletelink2.png"style = "margin-left: -4px; height: 20px; vertical-align: middle;"onclick = "funcion(' + cl + ')">';
                    jQuery("#tabla_distribuciones").jqGrid('setRowData', ids[i], {desasociar: desasociar});
                }
            },
            ajaxGridOptions: {
                async: false,
                data: {
                    opcion: 16
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_distribuciones").navButtonAdd('#pager', {
            caption: "Nuevo",
            onClickButton: function () {
                nuevaDistribucion();
            }
        });
    }
}

function reloadGridDistribuciones(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function  nuevaDistribucion() {
    cargartipo_numos();
    $("#dialogMsjnuevaDistribucion").dialog({
        width: '375',
        height: '385',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'NUEVA DISTRIBUCION/SOLICITUD',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Guardar": function () {
                var opav = $("#opav").val();
                var fintra = $("#fintra").val();
                var interv = $("#interv").val();
                var provin = $("#provin").val();
                var eca = $("#eca").val();
                var iva = $("#iva").val();
                var distribucion = $("#distribucion").val();
                if (opav === '' || fintra === '' || interv === '' || provin === '' || eca === '' || iva === '' || distribucion === '') {
                    mensajesDelSistema("Falta llenar datos", '230', '150', false);
                } else {
                    if (opav > 100 || fintra > 100 || interv > 100 || provin > 100 || eca > 100 || iva > 100) {
                        mensajesDelSistema("porcentaje no valido", '230', '150', false);
                    } else {
                        guardarDistribucion();
                    }
                }
            },
            "Salir": function () {
                $(this).dialog("close");
                $("#distribucion").val("");
                $("#opav").val("");
                $("#fintra").val("");
                $("#interv").val("");
                $("#provin").val("");
                $("#eca").val("");
                $("#iva").val("");
                $("#vlr_agregado").val("");
                $("#tipo_numos").val("");
            }
        }
    });
}

function guardarDistribucion() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 19,
            distribucion: $("#distribucion").val(),
            opav: $("#opav").val(),
            fintra: $("#fintra").val(),
            interv: $("#interv").val(),
            provin: $("#provin").val(),
            eca: $("#eca").val(),
            iva: $("#iva").val(),
            vlr_agregado: $("#vlr_agregado").val(),
            tipo_numos: $("#tipo_numos").val()
        },
        success: function (resp) {
            mensajesDelSistema("Exito al guardar la distribucion/solicitud", '230', '150', true);
            tipoDistribuciones();
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function cargartipo_numos() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 20
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#tipo_numos').html('');
                for (var datos in json) {
                    $('#tipo_numos').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    //  $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function verificartiposol() {
    var distribucion = $("#distribucion").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        datatype: 'text',
        data: {
            opcion: 26,
            distribucion: distribucion
        },
        success: function (json) {
            $("#sms2").html(json);
        }
    });
}

function abrirVentanaTabs() {
    $('#cuadro_pestanas').fadeIn("slow");
    $("#tabs").tabs();
    Distribucion();
}


function  Distribucion() {
    tipoDistribuciones();
    DistribucionesLibres();
    $("#cuadro_pestanas").dialog({
        width: '784',
        height: '510',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        // title: 'DISTRIBUCION',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
    $("#cuadro_pestanas").siblings('div.ui-dialog-titlebar').remove();
}

function DistribucionesLibres() {
    var grid_tabla = jQuery("#ditribucionNoRel");
    if ($("#gview_ditribucionNoRel").length) {
        reloadGridDistribucionesNoRel(grid_tabla, 34);
    } else {
        grid_tabla.jqGrid({
//            caption: "DISTRIBUCIONES",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '710',
            colNames: ['# Distribucion', '% Opav', '% Fintra', '% Interventoria', '% Provintegral', '% Eca', '% Iva', 'Valor agregado', 'Asociar'],
            colModel: [
                {name: 'tipo', index: 'tipo', width: 80, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_opav', index: 'porc_opav', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_fintra', index: 'porc_fintra', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_interventoria', index: 'porc_interventoria', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_provintegral', index: 'porc_provintegral', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_eca', index: 'porc_eca', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'porc_iva', index: 'porc_iva', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'valor_agregado', index: 'valor_agregado', width: 60, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'asociar', index: 'asociar', width: 80, sortable: true, align: 'center', hidden: false, search: true}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager4',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false

            }, gridComplete: function () {
                var cant = jQuery("#ditribucionNoRel").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cl = cant[i];
                    asociar = '<img src = "/fintra/images/link_new.png"style = "margin-left: -4px; height: 23px; vertical-align: middle;"onclick = "ventanaAsociarSolDist(' + cl + ')">';
                    jQuery("#ditribucionNoRel").jqGrid('setRowData', cant[i], {asociar: asociar});
                }
            },
            ajaxGridOptions: {
                data: {
                    opcion: 34
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager4", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#ditribucionNoRel").navButtonAdd('#pager4', {
            caption: "Nuevo",
            onClickButton: function () {
                // nuevaDistribucion();
            }
        });
    }
}


function reloadGridDistribucionesNoRel(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function tipoProyecto() {
    var accion = '';
    var grid_tabla = jQuery("#tabla_tipoProyecto");
    if ($("#gview_tabla_tipoProyecto").length) {
        reloadGridtipoProyecto(grid_tabla, 27);
    } else {
        grid_tabla.jqGrid({
            caption: "Linea de negocio",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '440',
            colNames: ['Estado', 'Codigo Proyecto', 'Descripcion', 'Asociar', 'Anular'],
            colModel: [
                {name: 'estado', index: 'estado', width: 120, sortable: true, align: 'center', hidden: true},
                {name: 'cod_proyecto', index: 'cod_proyecto', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'proyecto', index: 'proyecto', width: 250, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'asociar', index: 'asociar', width: 80, sortable: true, align: 'center', hidden: false, search: true},
                {name: 'anular', index: 'anular', width: 80, sortable: true, align: 'center', hidden: false, search: true}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager1',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false

            }, gridComplete: function () {
                var ids = grid_tabla.jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    asociar = '<img src = "/fintra/images/link_new.png"style = "margin-left: -4px; height: 23px; vertical-align: middle;"onclick = "ventanaAsociar(' + cl + ')">';
                    anular = '<img src = "/fintra/images/botones/iconos/anular3.png"style = "margin-left: -4px; height: 18px; vertical-align: middle;"onclick = "anularProyecto(' + cl + ')">';
                    jQuery("#tabla_tipoProyecto").jqGrid('setRowData', ids[i], {asociar: asociar, anular: anular});
                }
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    var estado = grid_tabla.getRowData(cl).estado;
                    if (estado === 'Anulado') {
                        $("#tabla_tipoProyecto").jqGrid('setRowData', ids[i], false, {weightfont: 'bold', background: '#F5A9A9'});
                    }
                }
            }, ondblClickRow: function (rowid, iRow, iCol, e) {
                accion = 'editar';
                var myGrid = jQuery("#tabla_tipoProyecto"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas; ////selarrrow  
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var cod_proyecto = filas.cod_proyecto;
                var proyecto = filas.proyecto;
                var estado = filas.estado;
                if (estado === 'Anulado') {
                    mensajesDelSistema("Debe estar activo", '230', '150', false);
                } else {
                    nuevoTipoProyecto(accion, cod_proyecto, proyecto);
                }
            }, loadComplete: function (id, rowid) {
                var info = grid_tabla.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema("No se encontraron registros", '204', '140', false);
                }
            }, ajaxGridOptions: {
                async: false,
                data: {
                    opcion: 27
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager1", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_tipoProyecto").navButtonAdd('#pager1', {
            caption: "Nuevo",
            onClickButton: function () {
                accion = 'nuevo';
                nuevoTipoProyecto(accion);
            }
        });
        $("#tabla_tipoProyecto").navButtonAdd('#pager1', {
            caption: "Distribuciones",
            onClickButton: function () {
                abrirVentanaTabs();
            }
        });
    }
}

function reloadGridtipoProyecto(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function nuevoTipoProyecto(accion, cod_proyecto, proyecto) {

    if (accion === 'nuevo') {
        $("#dialogMsjnuevoTipoProyecto").dialog({
            width: '375',
            height: '175',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'NUEVA LINEA DE NEGOCIO',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    var tipo_proyecto = $("#tipo_proyecto").val();
                    if (tipo_proyecto === '') {
                        mensajesDelSistema("Faltan datos", '230', '150', false);
                    } else {
                        guardarTipoProyecto();
                        $(this).dialog("close");
                    }
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#tipo_proyecto").val("");
                }
            }
        });
    } else if (accion === 'editar') {
        $("#tipo_proyecto").val(proyecto);
        $("#codigoProyecto").val(cod_proyecto);
        $("#dialogMsjnuevoTipoProyecto").dialog({
            width: '375',
            height: '175',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'EDITAR LINEA DE NEGOCIO',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    actualizarTipoProyecto();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#tipo_proyecto").val("");
                }
            }
        });
    }
}

function guardarTipoProyecto() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 28,
            tipo_proyecto: $("#tipo_proyecto").val()
        },
        success: function (data) {
            if (data.respuesta === 'Guardado') {
                $("#tipo_proyecto").val("");
                tipoProyecto();
                var ids = jQuery("#tabla_tipoProyecto").jqGrid('getDataIDs');
                var numero = ids.length;
                ventanaAsociar(numero, 'Activo');
                //mensajesDelSistema("Exito al guardar", '230', '150', true);
            }
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function actualizarTipoProyecto() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 33,
            tipo_proyecto: $("#tipo_proyecto").val(),
            cod_proyecto: $("#codigoProyecto").val()
        },
        success: function (data) {
            if (data.respuesta === 'Guardado') {
                tipoProyecto();
                mensajesDelSistema("Exito al ACTUALIZAR", '230', '150', true);
            }
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function ventanaAsociar(id, est) {
    var grid_tabla = jQuery("#tabla_tipoProyecto");
    var cod_proyecto = grid_tabla.getRowData(id).cod_proyecto;
    var proyecto = grid_tabla.getRowData(id).proyecto;
    var estado = grid_tabla.getRowData(id).estado;
    $("#tipoPoyect").val(proyecto).attr('readOnly', true);
    $("#cod_proyecto").val(cod_proyecto).attr('readOnly', true);
    if (estado === 'Activo' || est === 'Activo') {
        tipoDstSolicitudes();
        tipoTipoProyecto(cod_proyecto);
        $("#dialogMsjventanaAsociar").dialog({
            width: '689',
            height: '550',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'Asociacion',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Salir": function () {
                    $(this).dialog("close");
                }
            }
        });
    } else {
        mensajesDelSistema("Debe estar activo", '230', '150', false);
    }
}

function tipoDstSolicitudes() {
    var grid_tabla = jQuery("#tabla_DstSol");
    if ($("#gview_tabla_DstSol").length) {
        reloadGridtipoDstSolicitudes(grid_tabla, 29);
    } else {
        grid_tabla.jqGrid({
            caption: "Tipo de Solicitud",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '248',
            // async: false,
            colNames: ['Id', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'distribucion', index: 'distribucion', width: 195, sortable: true, align: 'left', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager2',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false
            },
            ajaxGridOptions: {
                async: false,
                data: {
                    opcion: 29
                            //cod_proyecto: cod_proyecto
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager2", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function tipoTipoProyecto(cod_proyecto) {
    var grid_tabla = jQuery("#tabla_TipoPtoyect");
    if ($("#gview_tabla_TipoPtoyect").length) {
        reloadGridtipoTipoProyecto(grid_tabla, 30, cod_proyecto);
    } else {
        grid_tabla.jqGrid({
            caption: "Linea de negocio",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '248',
            //async: false,
            colNames: ['Id', 'Codigo Proyecto', 'Descripcion', 'Tipo de solicitud'],
            colModel: [
                {name: 'id', index: 'id', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'cod_proyecto', index: 'cod_proyecto', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'descripcion', index: 'descripcion', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'distribucion', index: 'distribucion', width: 195, sortable: true, align: 'left', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager3',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false
            },
            ajaxGridOptions: {
                async: false,
                data: {
                    opcion: 30,
                    cod_proyecto: cod_proyecto
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager3", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridtipoDstSolicitudes(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function reloadGridtipoTipoProyecto(grid_tabla, op, cod_proyecto) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                cod_proyecto: cod_proyecto
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function  agregarAsociacion() {
    var cod_proyecto = $("#cod_proyecto").val();
    var selRowIds = jQuery("#tabla_DstSol").jqGrid('getGridParam', 'selarrrow'), rowData;
    if (selRowIds.length > 0) {
        var arrayJson = new Array();
        for (var i = 0; i < selRowIds.length; i++) {
            rowData = $("#tabla_DstSol").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }
        console.log(JSON.stringify(arrayJson));
        $.ajax({
            async: false,
            url: "./controller?estado=Fintra&accion=Soporte",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 31,
                listJson: JSON.stringify(arrayJson),
                cod_proyecto: cod_proyecto
            },
            success: function (json) {
                if (json.respuesta === 'GUARDADO') {
                    tipoDstSolicitudes();
                    tipoTipoProyecto(cod_proyecto);
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("No hay datos seleccionados", '300', '150', true);
    }
}

function  quitarAsociacion() {
    var cod_proyecto = $("#cod_proyecto").val();
    var selRowIds = jQuery("#tabla_TipoPtoyect").jqGrid('getGridParam', 'selarrrow'), rowData;
    if (selRowIds.length > 0) {
        var arrayJson = new Array();
        for (var i = 0; i < selRowIds.length; i++) {
            rowData = $("#tabla_TipoPtoyect").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }
        console.log(JSON.stringify(arrayJson));
        $.ajax({
            async: false,
            url: "./controller?estado=Fintra&accion=Soporte",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 32,
                listJson: JSON.stringify(arrayJson),
                cod_proyecto: cod_proyecto
            },
            success: function (json) {
                if (json.respuesta === 'GUARDADO') {
                    // mensajesDelSistema("Exito al quitar asociacion ", '230', '150', true);
                    tipoDstSolicitudes();
                    tipoTipoProyecto(cod_proyecto);
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("No hay datos seleccionados", '300', '150', true);
    }
}

function ventanaAsociarSolDist(id) {
    var grid_tabla = jQuery("#ditribucionNoRel");
    var cod_distribucion = grid_tabla.getRowData(id).tipo;
    $("#cod_distribucion").val(cod_distribucion).attr('readOnly', true);
    SolicitudesLibres();
    tipoDistribucionLib(cod_distribucion);
    $("#dialogVentanaAsociarSolDst").dialog({
        width: '689',
        height: '550',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Asociacion',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
                tipoDistribuciones();
                DistribucionesLibres();
            }
        }
    });
}

function SolicitudesLibres() {
    var grid_tabla = jQuery("#tabla_tipoSol");
    if ($("#gview_tabla_tipoSol").length) {
        reloadGridSolicitudesLibres(grid_tabla, 35);
    } else {
        grid_tabla.jqGrid({
            caption: "Tipo de Solicitud",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '248',
            // async: false,
            colNames: ['Id', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'distribucion', index: 'distribucion', width: 195, sortable: true, align: 'left', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager5',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false
            },
            ajaxGridOptions: {
                async: false,
                data: {
                    opcion: 35
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager5", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridSolicitudesLibres(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function tipoDistribucionLib(tipo) {
    var grid_tabla = jQuery("#tabla_Ditrib");
    if ($("#gview_tabla_Ditrib").length) {
        reloadGridDistribucionLib(grid_tabla, 37, tipo);
    } else {
        grid_tabla.jqGrid({
            caption: "Distribucion",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '248',
            colNames: ['# Ditribucion', 'Tipo de solicitud'],
            colModel: [
                {name: 'tipo', index: 'tipo', width: 120, sortable: true, align: 'left', hidden: true},
                {name: 'distribucion', index: 'descripcion', width: 120, sortable: true, align: 'left', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 60,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            pager: '#pager3',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            onSelectCell: true,
            jsonReader: {
                root: "rows",
                repeatitems: false
            },
            ajaxGridOptions: {
                async: false,
                data: {
                    opcion: 37,
                    cod_distribucion: tipo
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager3", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
    }
}

function reloadGridDistribucionLib(grid_tabla, op, tipo) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            async: false,
            data: {
                opcion: op,
                cod_distribucion: tipo
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function anularProyecto(id) {
    var grid_tabla = jQuery("#tabla_tipoProyecto");
    var cod_proyecto = grid_tabla.getRowData(id).cod_proyecto;
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 36,
            cod_proyecto: cod_proyecto
        },
        success: function (data) {
            if (data.respuesta === 'Guardado') {
                tipoProyecto();
            }
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function  agregarAsociacionSistribucion() {
    var cod_distribucion = $("#cod_distribucion").val();
    var selRowIds = jQuery("#tabla_tipoSol").jqGrid('getGridParam', 'selarrrow'), rowData;
    if (selRowIds.length > 0) {
        var arrayJson = new Array();
        for (var i = 0; i < selRowIds.length; i++) {
            rowData = $("#tabla_tipoSol").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }
        console.log(JSON.stringify(arrayJson));
        $.ajax({
            async: false,
            url: "./controller?estado=Fintra&accion=Soporte",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 38,
                listJson: JSON.stringify(arrayJson),
                cod_distribucion: cod_distribucion
            },
            success: function (json) {
                if (json.respuesta === 'GUARDADO') {
                    SolicitudesLibres();
                    tipoDistribucionLib(cod_distribucion);
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("No hay datos seleccionados", '300', '150', true);
    }
}

function  quitarAsociacionSistribucion() {
    var cod_distribucion = $("#cod_distribucion").val();
    var selRowIds = jQuery("#tabla_Ditrib").jqGrid('getGridParam', 'selarrrow'), rowData;
    if (selRowIds.length > 0) {
        var arrayJson = new Array();
        for (var i = 0; i < selRowIds.length; i++) {
            rowData = $("#tabla_Ditrib").jqGrid("getLocalRow", selRowIds[i]);
            arrayJson.push(rowData);
        }
        console.log(JSON.stringify(arrayJson));
        $.ajax({
            async: false,
            url: "./controller?estado=Fintra&accion=Soporte",
            type: 'POST',
            dataType: 'json',
            data: {
                opcion: 39,
                listJson: JSON.stringify(arrayJson),
                cod_distribucion: cod_distribucion
            },
            success: function (json) {
                if (json.respuesta === 'GUARDADO') {
                    SolicitudesLibres();
                    tipoDistribucionLib(cod_distribucion);
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("No hay datos seleccionados", '300', '150', true);
    }
}

