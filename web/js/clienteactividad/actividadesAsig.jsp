<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado Actividades</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

<body onResize="redimensionar()" onLoad="redimensionar()"> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Actividades Asignadas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <div align="center">
  <%String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;

    Vector Vecca = model.clientactSvc.obtVecClientAct();

	if ( Vecca.size() >0 ){ 
  %>
  </div>
  <table width="800" border="2" align="center">
    <tr>
      <td><table width="100%">
        <tr>
          <td width="48%" class="subtitulo1">&nbsp;Datos </td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
                        </td>
        </tr>
      </table>
        <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
        <tr class="tblTitulo" align="center">
          <td width="218" >Tipo Viaje </td>
          <td width="197" >Cliente</td>
          <td width="85" >Nombre </td>
          <td width="252"  >Descripci&oacute;n</td>
        </tr>
        <pg:pager
    items="<%=Vecca.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
        <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, Vecca.size()); i < l; i++) {
            ClienteActividad ca = (ClienteActividad) Vecca.elementAt(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" 
        onClick="window.open('<%=CONTROLLER%>?estado=Clientact&accion=Buscar&pagina=actividad.jsp&carpeta=jsp/trafico/actividad&codact=<%=ca.getCodActividad()%>&codcli=<%=ca.getCodCliente()%>&tipo=<%=ca.getTipoViaje()%>' ,'','status=yes,scrollbars=no,width=630,height=350,resizable=yes');">
          <td height="20" class="bordereporte"><%=ca.getDestipo_viaje()%></td>
          <td height="20" class="bordereporte"><%=ca.getNomCliente()%></td>
          <td height="20" class="bordereporte"><%=ca.getDescortaAct()%></td>
          <td height="20" class="bordereporte"><%=ca.getLargaAct()%></td>
        </tr>
        </pg:item>
        <%}%>
        <tr class="fila">
          <td colspan="5" align="center" valign="middle" >
            <div align="center"><pg:index>
              <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
    </pg:index></div></td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
  </table>
  <%}
 else { %>
   <table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
  <br>
  <table width="750" border="0" align="center">
    <tr>
      <td><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Idioma&accion=Cargar&pagina=BuscarActAsig.jsp&ruta=/jsp/trafico/clienteactividad/'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
  </table>
</div>
</body>
</html>
