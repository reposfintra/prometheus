
$(document).ready(function () {
    buscar_negocio_aprobar_refi();
});

function buscar_negocio_aprobar_refi() {
    $("#div_detalle_refinanciacion").hide();
    var grid_tabla = $("#tabla_aprobar_refi");
    if ($("#gview_tabla_aprobar_refi").length) {
        reloadGridTabla(grid_tabla, 24);
    } else {
        grid_tabla.jqGrid({
            caption: "Aprobar Negocios Refinanciar",
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '1155',
            colNames: ['Negocio', 'Estado', 'Identificacion', 'Nombre', 'Capital', 'Porcentaje', 'Cuota Inicial', 'primera cuota', 'Departamento', 'Plazo',
                'pago', 'Fecha Creacion', 'Usuario Creador', 'Key Ref', 'Vencimiento acuerdo', 'estrategia', 'Int mora', 'Gac', 
                'Interes Corriente', 'Cat Vencido', 'Saldo cat', 'Vlr cuota actal','Cuota admin','Periodos Pago Inicial', 
                'Observ.Simulacion','Enviado','Seleccionar'],
            colModel: [
                {name: 'cod_neg', index: 'cod_neg', width: 80, sortable: true, align: 'center', hidden: false},
                {name: 'estado', index: 'estado', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'cod_cli', index: 'cod_cli', width: 70, sortable: true, align: 'center', hidden: false},
                {name: 'nomcli', index: 'nomcli', width: 200, sortable: true, align: 'center', hidden: false},
                {name: 'capital_a_refinanciar', index: 'capital_a_refinanciar', width: 80, sortable: true, align: 'center', hidden: false, search: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'porc_negociacion', index: 'porc_negociacion', width: 90, sortable: true, align: 'center', hidden: true,
                    formatter: 'currency', formatoptions: {decimalPlaces: 0, suffix: "%"}},
                {name: 'valor_a_pagar', index: 'valor_a_pagar', width: 65, sortable: true, align: 'center', hidden: false,
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'fecha_pago_primera_cuota', index: 'fecha_pago_primera_cuota', width: 65, sortable: true, align: 'center', hidden: false},
                {name: 'departamento', index: 'departamento', width: 90, sortable: true, align: 'center', hidden: true},
                {name: 'plazo', index: 'plazo', width: 30, sortable: true, align: 'center', hidden: false},
                {name: 'pago', index: 'pago', width: 60, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'creation_date', index: 'creation_date', width: 80, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'creation_user', index: 'creation_user', width: 80, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'key_ref', index: 'key_ref',key:true, width: 80, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'fecha_vencimiento_acuerdo', index: 'fecha_vencimiento_acuerdo', width: 70, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'estrategia', index: 'estrategia', width: 80, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'intxmora', index: 'intxmora', width: 80, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'gac', index: 'gac', width: 80, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'interes_corrientes', index: 'interes_corrientes', width: 80, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'cat_vencido', index: 'cat_vencido', width: 80, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'saldo_cat', index: 'saldo_cat', width: 80, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'valor_cuota_actual', index: 'valor_cuota_actual', width: 80, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'cuota_admin', index: 'cuota_admin', width: 80, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'periodos_pago_inicial', index: 'periodos_pago_inicial', width: 80, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'observacion_simulacion', index: 'observacion_simulacion', width: 80, sortable: true, align: 'center', hidden: false, search: false},
                {name: 'sms_enviado', index: 'sms_enviado', width: 80, sortable: true, align: 'center', hidden: true, search: false},
                {name: 'select', index: 'select', width: 60, sortable: true, align: 'center', hidden: false, search: false}
                
            ],
            rowNum: 100000,
            rowTotal: 100000,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            restoreAfterError: true,
            pager: '#page_tabla_aprobar_refi',
            pgtext: null,
            pgbuttons: false,
            multiselect: false,

            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                if (grid_tabla.jqGrid('getGridParam', 'records') <= 0) {
                    mensajesDelSistema("No se encontraron resultados para las parametros de busqueda.", '250', '150');
                }
            },
            ajaxGridOptions: {

                data: {
                    opcion: 24,
                    tipo_busqueda: $("#tipo_busqueda").val(),
                    estado_refi: $("#id_estado_refi").val(),
                    documento: $("#documento").val()
                }
            },
            gridComplete: function () {
                var cant = $("#tabla_aprobar_refi").jqGrid('getDataIDs');

                for (let i = 0; i < cant.length; i++) {
                    let key_ref = $("#tabla_aprobar_refi").getRowData(cant[i]).key_ref;
                    let valor_inicial = $("#tabla_aprobar_refi").getRowData(cant[i]).valor_a_pagar;
                    let intxmora = $("#tabla_aprobar_refi").getRowData(cant[i]).intxmora;
                    let gac = $("#tabla_aprobar_refi").getRowData(cant[i]).gac;
                    let nomcli = $("#tabla_aprobar_refi").getRowData(cant[i]).nomcli;
                    let cod_neg = $("#tabla_aprobar_refi").getRowData(cant[i]).cod_neg;
                    let estado = $("#tabla_aprobar_refi").getRowData(cant[i]).estado;
                    let interes_corrientes = $("#tabla_aprobar_refi").getRowData(cant[i]).interes_corrientes;
                    let cat_vencido = $("#tabla_aprobar_refi").getRowData(cant[i]).cat_vencido;
                    let capital_a_refinanciar = $("#tabla_aprobar_refi").getRowData(cant[i]).capital_a_refinanciar;
                    let saldo_cat = $("#tabla_aprobar_refi").getRowData(cant[i]).saldo_cat;
                    let valor_cuota_actual = $("#tabla_aprobar_refi").getRowData(cant[i]).valor_cuota_actual;
                    let cuota_admin = $("#tabla_aprobar_refi").getRowData(cant[i]).cuota_admin;
                    let periodos_pago_inicial = $("#tabla_aprobar_refi").getRowData(cant[i]).periodos_pago_inicial;
                    let sms_enviado = $("#tabla_aprobar_refi").getRowData(cant[i]).sms_enviado;
                    let btn;
                        if(sms_enviado ==="true")
                        {
                            $("#tabla_aprobar_refi").jqGrid('setRowData',cant[i],false, {  color:'#000000',weightfont:'bold',background:'#6deca0ed'});            
                        }
                    
                    btn = "<img src='/fintra/images/botones/iconos/lupa.gif' align='center' style='height:15px;width:15px;margin-left: 4px;padding:3px; cursor: pointer;' value='detalle' title='Ver detalle' onclick=\"cargarDetalleCartera('" + key_ref + "','" + cod_neg + "','" + nomcli + "','" + interes_corrientes + "','" + cat_vencido + "','" + capital_a_refinanciar + "','" + saldo_cat + "','" + valor_cuota_actual + "','" + intxmora + "','" + gac + "','" + valor_inicial + "','" + cuota_admin + "','" + estado + "','" + periodos_pago_inicial + "');\" />";
                   
                    if(estado==='Aprobado'){
                    btn += "<img src='/fintra/images/botones/iconos/logosms.png' align='center' style='height:15px;width:15px;margin-left: 4px;padding:3px; cursor: pointer;' value='genPdf' title='Enviar Sms' onclick=\"imprimirPdf('" + key_ref + "','" + intxmora + "','" + gac + "','" + valor_inicial + "');\" />";
                    }
                    
                    $("#tabla_aprobar_refi").jqGrid('setRowData', cant[i], {select: btn});
                }

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });

    }

}


function reloadGridTabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",

        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                tipo_busqueda: $("#tipo_busqueda").val(),
                estado_refi: $("#id_estado_refi").val(),
                documento: $("#documento").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");

}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {

                $(this).dialog("close");
            }
        }
    });

}


function cargarDetalleCartera(key_ref,cod_neg,nomcli,interes_corrientes,cat_vencido, capital_a_refinanciar, saldo_cat,valor_cuota_actual, intxmora ,gac,valor_inicial,cuota_admin,estado,periodos_pago_inicial,id_observaciones_simulacion) {
    $("#id_cliente").text(nomcli);
    $("#id_cod_negocio").text(cod_neg);
    $("#id_interes_tb").text(numberConComas( interes_corrientes));
    $("#id_cat_vencido_tb").text(numberConComas( cat_vencido));
    $("#id_cuota_admon_tb").text(numberConComas(0));
    $("#id_ixm_tb").text(numberConComas(intxmora));
    $("#id_gac_tb").text(numberConComas(gac));
    $("#id_vlr_cuota_actual").text(numberConComas(valor_cuota_actual));
    $("#id_capital_tb").text(numberConComas( capital_a_refinanciar));
    $("#id_cat_tb").text(numberConComas (saldo_cat));
    $("#total_pagar").text(numberConComas (valor_inicial));
    $("#id_cuota_admon_tb").text(numberConComas (cuota_admin));
    $("#peridos").text(periodos_pago_inicial);
    $("#id_observaciones_simulacion").val("");   
    $("#div_detalle_refinanciacion").dialog({
        modal: true,
        width: '850',
        height: '669',
        show: "scale",
        hide: "scale",
        resizable: true,
        title: 'Refinanciar',
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aprobar / Anular": function () {
                agregarObservacion(key_ref,estado);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    
    if (estado==="Aprobado"){
         $('.ui-dialog-buttonpane button:contains("Anular")').button().hide();
     }
    
    var gripdetalle = $("#table_detalle_refinanciacion");
    $('#div_detalle_refinanciacion').fadeIn();   
    if ($("#gview_table_detalle_refinanciacion").length) {
        reloadtabla_saldo_financiar(gripdetalle, key_ref, 25);
    } else {
        gripdetalle.jqGrid({
            caption: 'Detalle Refinanciacion',
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '275',
            width: '810',
            colNames: ['Negocio', 'Cuota', 'vencimiento', 'Saldo Inicial', 'Vlr. capital', 'Vlr. interes',
                'Vlr. CAT', 'Vlr. admon', 'Vlr Seguro','Vlr. K Aval', 'Vlr. Int Aval', 'Valor Cuota', 'Valor Aval', 'Saldo Final', 'Color'],
            colModel: [
                {name: 'cod_neg', index: 'cod_neg', width: 80, resizable: false, sortable: true, align: 'center', hidden: true},
                {name: 'item', index: 'item', width: 30, resizable: false, sortable: true, align: 'center'},
                {name: 'fecha', index: 'fecha', resizable: false, sortable: true, width: 80, align: 'center'},
                {name: 'saldo_inicial', index: 'saldo_inicial', sortable: true, width: 70, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'capital', index: 'capital', summaryType: 'sum', sortable: true, width: 75, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes', index: 'interes', summaryType: 'sum', sortable: true, width: 70, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cat', index: 'cat', summaryType: 'sum', sortable: true, width: 70, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'cuota_manejo', index: 'cuota_manejo', summaryType: 'sum', sortable: true, width: 70, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'seguro', index: 'seguro', summaryType: 'sum', sortable: true, width: 70, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'capital_aval', index: 'capital_aval', summaryType: 'sum', sortable: true, width: 70, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'interes_aval', index: 'interes_aval', summaryType: 'sum', sortable: true, width: 70, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_cuota', index: 'valor_cuota', summaryType: 'sum', sortable: true, width: 70, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_aval', index: 'valor_aval', summaryType: 'sum', sortable: true, width: 70, align: 'center', search: false, sorttype: 'number', hidden: true,
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'saldo_final', index: 'saldo_final', sortable: true, width: 70, align: 'center', search: false, sorttype: 'number',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'color', index: 'color', width: 80, resizable: false, sortable: true, align: 'center', hidden: true}

            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            restoreAfterError: true,
            pager: '#page_table_detalle_refinanciacion',
            pgtext: null,
            pgbuttons: false,            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "1"
            },
            gridComplete: function () {
              let capital = gripdetalle.jqGrid('getCol', 'capital', false, 'sum');
                let interes = gripdetalle.jqGrid('getCol', 'interes', false, 'sum');
                let cat = gripdetalle.jqGrid('getCol', 'cat', false, 'sum');
                let cuota_manejo = gripdetalle.jqGrid('getCol', 'cuota_manejo', false, 'sum');
                let seguro = gripdetalle.jqGrid('getCol', 'seguro', false, 'sum');
                let capital_aval = gripdetalle.jqGrid('getCol', 'capital_aval', false, 'sum');
                let interes_aval = gripdetalle.jqGrid('getCol', 'interes_aval', false, 'sum');
                let valor_cuota = gripdetalle.jqGrid('getCol', 'valor_cuota', false, 'sum');
                
                var rows = gripdetalle.getDataIDs();
                for (var i = 0; i < rows.length; i++)
                {
                  gripdetalle.setCell (rows[i],"valor_cuota",'',{ 'background-color':'#fff6cd'});
                }   
                gripdetalle.jqGrid('footerData', 'set', 
                {
                 capital: capital,
                 interes:interes,
                 cat:cat,               
                 cuota_manejo:cuota_manejo,  
                 seguro:seguro,
                 capital_aval:capital_aval,                
                 interes_aval:interes_aval,               
                 valor_cuota:valor_cuota                
                });   
             
            },
            ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {

                    opcion: 25,
                    negocio: key_ref

                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });

    }
     getObservacionSimulacion(key_ref);
}

function reloadtabla_saldo_financiar(grid_tabla, negocio, op) {
    $('#div_detalle_refinanciacion').fadeIn();
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",

        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                negocio: negocio
            }
        }
    });
    grid_tabla.trigger("reloadGrid");

}

function aprobar_ref(key_ref) {
    loading("Espere un momento por favor...", "270", "140");
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        data: {
            opcion: 26,
            negocio: $("#cod_negocio").val(),
            tipo_refi: "B",
            key_ref: key_ref,
            fecha_vencimiento_acuerdo: $("#fecha_vencimiento_acuerdo").val(),
            observacion:$("#id_observaciones").val()

        },
        success: function (json) {
            $("#dialogLoading").dialog('close');
            if (json.success === true) {
                mensajesDelSistema("Negocio aprobado con �xito", '250', '150');
                $('#div_detalle_refinanciacion').dialog('close');
                $('#div_obsevaciones').dialog('close');
                
                 buscar_negocio_aprobar_refi();
            } else {
                mensajesDelSistema(json.data, '250', '150');
            }

        },
        error: function (xhr) {
            $("#dialogLoading").dialog('close');
            mensajesDelSistema("Error al refinanciar " + "\n" +
                    xhr.responseText, '350', '180', true);
        }
    });

}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function imprimirPdf(keyref, intxmora, gac, valor_inicial) {

    loading("Espere un momento por favor...", "270", "140");
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        data: {
            opcion: 48,
            key_ref: keyref,
            intxmora: intxmora,
            gac: gac,
            valor_inicial: valor_inicial
        },
        success: function (json) {
            if (json.success) {
                mensajesDelSistema("Se generado el extracto # : " + json.data, '250', '200');
            } else {
                mensajesDelSistema("Lo sentimos ha fallado el envio del extracto al cliente.", '250', '200');
            }
            $("#dialogLoading").dialog('close');
        },
        error: function (xhr) {
            $("#dialogLoading").dialog('close');
            mensajesDelSistema("Error al enviar mensaje aprobaci�n " + "\n" +
                    xhr.responseText, '350', '180', true);
        }
    });

}

function anular_refinanciacion(key_ref) {
    loading("Espere un momento por favor...", "270", "140");
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        data: {
            opcion: 50,
            key_ref: key_ref,
            observacion:$("#id_observaciones").val()
        },
        success: function (json) {
            $("#dialogLoading").dialog('close');
            
            if (json.success) {
                mensajesDelSistema(json.mensaje, '250', '150');
                $('#div_detalle_refinanciacion').dialog('close');
                $('#div_obsevaciones').dialog('close');
                $("#id_observaciones").val("");
                 buscar_negocio_aprobar_refi();
            } else {
                mensajesDelSistema(json.mensaje, '250', '150');
            }
        },
        error: function (xhr) {
            $("#dialogLoading").dialog('close');
            mensajesDelSistema("Error al refinanciar " + "\n" +
                    xhr.responseText, '350', '180', true);
        }
    });

}

function numberConComas(x) {
    return "$" + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function agregarObservacion(key_ref,estado) {
    $("#id_observaciones").val("");    
    $("#div_obsevaciones").dialog({
        modal: true,
        width: '350',
        height: '250',
        show: "scale",
        hide: "scale",
        resizable: true,
        title: 'Observaciones',
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            
            "Aprobar Negociaci�n": function () {
                aprobar_ref(key_ref);
            },
            "Anular": function () {
                anular_refinanciacion(key_ref);
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
    if (estado==="Aprobado"){
         $('.ui-dialog-buttonpane button:contains("Negociaci�n")').button().hide();
         $('.ui-dialog-buttonpane button:contains("Anular")').button().hide();
     }
   
    
}

function getObservacionSimulacion(key_ref) {
     loading("Espere un momento por favor...", "270", "140");
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        data: {
            opcion: 51,
            key_ref: key_ref
        },
        success: function (json) {            
            if (json.success) {
                 $("#id_observaciones_simulacion").val(json.observacion);
                  $("#dialogLoading").dialog('close');
            } else {
                 $("#dialogLoading").dialog('close');
                mensajesDelSistema(json.observacion, '250', '150');
            }
        },
        error: function (xhr) {
             $("#dialogLoading").dialog('close');
            mensajesDelSistema("Error al cargar informacion " + "\n" +
                    xhr.responseText, '350', '180', true);
        }
    });
}