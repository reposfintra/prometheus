/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cargarConveniosxUnidadNegocio() {
    console.log('Entra aqui');
    var unidad_negocio = $('#unidad_negocio').val();
    if (unidad_negocio !== '' ) {
    
    var grid_listar_convenios = $("#tabla_convenios");
    if ($("#gview_tabla_convenios").length) {
        reloadGridMostrar(unidad_negocio);
    } else {

        grid_listar_convenios.jqGrid({
            caption: "Convenios",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '240',
            width: '1050',
            colNames: ['Id', 'Nombre', 'Tasa interes','Tasa Interes Mora','Tasa Compra Cartera','Tasa Aval', 'Tasa Fintra EA','Tasa SIC EA'],
            colModel: [
                {name: 'id_convenio', index: 'id_convenio', width: 30, sortable: true, align: 'left', hidden: true, key: true},
                {name: 'convenio', index: 'convenio', width: 430, sortable: true, align: 'center', hidden: false},
                {name: 'tasa_interes', index: 'tasa_interes', width: 70, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'tasa_usura', index: 'tasa_usura', width: 80, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'tasa_compra_cartera', index: 'tasa_compra_cartera', width: 100, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'tasa_aval', index: 'tasa_aval', width: 100, sortable: true, align: 'left', hidden: false, search: true},
                {name: 'tasa_max_fintra', index: 'tasa_max_fintra', width: 100, sortable: true, align: 'left', hidden: false, search: true, formatter: 'currency', formatoptions: {decimalPlaces: 2}},
                {name: 'tasa_sic_ea', index: 'tasa_sic_ea', width: 100, sortable: true, align: 'left', hidden: false, search: true, formatter: 'currency', formatoptions: {decimalPlaces: 2}}

            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#page_tabla_convenios',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 84
                    , unidad_negocio: unidad_negocio
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);

            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_convenios").jqGrid('getDataIDs');
                /*for (var i = 0; i < cant.length; i++) {
                 var cambioEstado = $("#tabla_convenios").getRowData(cant[i]).cambio;
                 var cl = cant[i];
                 be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoPoliza('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                 jQuery("#tabla_convenios").jqGrid('setRowData', cant[i], {cambio: be});
                 }*/
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

                operacion = 'Editar';
                var myGrid = jQuery("#tabla_convenios"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id_convenio = filas.id_convenio;
                var convenio = filas.convenio;
                var tasa_interes = filas.tasa_interes;
                var tasa_usura = filas.tasa_usura;
                var tasa_compra_cartera = filas.tasa_compra_cartera;
                var tasa_aval = filas.tasa_aval;
                var tasa_max_fintra = filas.tasa_max_fintra;
                var tasa_sic_ea = filas.tasa_sic_ea;
                ventanaConvenios(operacion, id_convenio, convenio, tasa_interes, tasa_usura,tasa_compra_cartera,tasa_aval,tasa_max_fintra,tasa_sic_ea);

            }
        }).navGrid("#page_tabla_convenios", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        $("#tabla_convenios").navButtonAdd('#page_tabla_convenios', {
            caption: "Todos",
            onClickButton: function () {
                operacion = 'Todos';
                ventanaConvenios(operacion);
            }
        });
    }
    }else {
        mensajesDelSistema("Escoga una unidad de negocio", '230', '150', false);
    }
}

function reloadGridMostrar(unidad_negocio) {
    jQuery("#tabla_convenios").setGridParam({
        url: "./controller?estado=Fintra&accion=Soporte",
        datatype: 'json',
        ajaxGridOptions: {
            async: false,
            type: "POST",
            data: {
                opcion: 84
                , unidad_negocio: unidad_negocio
            }
        }
    });
    jQuery("#tabla_convenios").trigger("reloadGrid");
}


function ventanaConvenios(operacion, id_convenio, convenio, tasa_interes, tasa_usura, tasa_compra_cartera,tasa_aval,tasa_max_fintra,tasa_sic_ea) {
    if (operacion === 'Todos') {
        $("#dialogConvenios2").dialog({
            width: '750',
            height: '150',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'CONVENIOS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    actualizarConveniosxUnidadNegocio();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id").val('');
                    $("#nombre").val('');
                    $("#descripcion").val('');
                }
            }
        });
    } else if (operacion === 'Editar') {

        $("#id_convenio").val(id_convenio);
        $("#convenio").val(convenio);
        $("#tasa_interes").val(tasa_interes);
        $("#tasa_usura").val(tasa_usura);
        $("#tasa_compra_cartera").val(tasa_compra_cartera);
        $("#tasa_aval").val(tasa_aval);
        $("#tasa_max_fintra").val(tasa_max_fintra);
        $("#tasa_sic_ea").val(tasa_sic_ea);

        $("#dialogConvenios").dialog({
            width: '800',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'CONVENIO',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    actualizarConvenio();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id_convenio").val('');
                    $("#convenio").val('');
                    $("#tasa_interes").val('');
                    $("#tasa_usura").val('');
                    $("#tasa_compra_cartera").val('');
                    $("#tasa_aval").val('');
                    $("#tasa_max_fintra").val('');
                    $("#tasa_sic_ea").val('');
                }
            }
        });
    }
}

function actualizarConvenio() {
    var id = $("#id_convenio").val();
    var convenio = $("#convenio").val();
    var tasa = $("tasa_interes").val();
    var tasa_usura = $("tasa_usura").val();
    var tasa_compra_cartera = $("tasa_compra_cartera").val();
    var tasa_aval = $("tasa_aval").val();
    var tasa_max_fintra= $("#tasa_max_fintra").val();
    var tasa_sic_ea= $("#tasa_sic_ea").val();
    //alert(id_convenio);
    if (tasa_max_fintra>tasa_sic_ea){
         mensajesDelSistema("La tasa maxima de fintra no debe ser superior a la tasa sic EA", '230', '150', false);
    }else{
    if (convenio !== '' && tasa !== '' && tasa_usura !== '' && tasa_compra_cartera!== ''&& tasa_aval!== '' && tasa_max_fintra!== '' && tasa_sic_ea!== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 85,
                id: $("#id_convenio").val(),
                convenio: $("#convenio").val(),
                tasa: $("#tasa_interes").val(),
                tasa_usura: $("#tasa_usura").val(),
                tasa_compra_cartera: $("#tasa_compra_cartera").val(),
                tasa_aval: $("#tasa_aval").val(),
                tasa_max_fintra: $("#tasa_max_fintra").val(),
                tasa_sic_ea: $("#tasa_sic_ea").val()
                
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#dialogConvenios").dialog('close');
                    $("#id_convenio").val('');
                    $("#convenio").val('');
                    $("#tasa_interes").val('');
                    $("#tasa_usura").val('');
                    $("#tasa_compra_cartera").val('');
                    $("#tasa_aval").val('');
                    $("#tasa_max_fintra").val('');
                    $("#tasa_sic_ea").val('');
                }
                cargarConveniosxUnidadNegocio();
            }, error: function (result) {
                alert('No se puede guardar, verifique los datos e intente nuevamente');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }
    }
}

function actualizarConveniosxUnidadNegocio() {
    var unidad_negocio = $('#unidad_negocio').val();
    var tasa = $("tasa_interesd").val();
    var tasam = $("tasa_usurad").val();
    var tasac = $("tasa_compra_carterac").val();
    var tasa_avalc = $("tasa_avalc").val();
    var tasa_max_fintrac = $("tasa_max_fintrac").val();
    var tasa_sic_eac = $("tasa_sic_eac").val();

    //alert(desc);
    if (tasa !== '' && tasam !== '' && tasac !== ''&& tasa_avalc !== ''&& tasa_max_fintrac !== ''&& tasa_sic_eac !== '') {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 86,
                unidad_negocio: unidad_negocio,
                tasa: $("#tasa_interesd").val(),
                tasam: $("#tasa_usurad").val(),
                tasac: $("#tasa_compra_carterac").val(),
                tasa_avalc: $("#tasa_ctasa_avalcompra_carterac").val(),
                tasa_max_fintrac: $("#tasa_max_fintrac").val(),
                tasa_sic_eac: $("#tasa_sic_eac").val()
            },
            success: function (data, textStatus, jqXHR) {
                
                if (data.respuesta === 'Guardado') {
                    mensajesDelSistema("Exito al actualizar", '230', '150', true);
                    $("#dialogConvenios2").dialog('close');
                    //$("#unidad_negocio")val('');
                    $("#tasa_interesd").val('');
                    $("#tasa_usurad").val('');
                    $("#tasa_compra_carterac").val('');
                    $("#tasa_avalc").val('');
                    $("#tasa_max_fintrac").val('');
                    $("#tasa_sic_eac").val('');
                }
                cargarConveniosxUnidadNegocio();
            }, error: function (result) {
                
                alert('ERROR ');
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}


