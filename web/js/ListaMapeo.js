/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    cargarCombo('categoria', []);
    $("#buscar").click(function () {
        CargarGrid();
    });
    maximizarventana();

});

function CargarGrid() {
    var subcategoria = $('#sub').val();
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=57&subcategoria=' + subcategoria;
    $('#tbl_lista_mapeo').jqGrid('GridUnload');
    if ($("#gview_tbl_lista_mapeo").length) {
        refrescarGridMapeo(subcategoria);
    } else {
        jQuery("#tbl_lista_mapeo").jqGrid({
            caption: 'Lista de Insumos Mapeados',
            url: url,
            datatype: 'json',
            height: '500',
            width: '1400',
            colNames: ['Id', 'Cod. Material', 'Descripcion', 'Fecha', 'Nit', 'Nomb. Proveedor','Cantidad', 'Precio Unitario'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100', key: true},
                {name: 'codigo_material', index: 'codigo_material', sortable: true, align: 'center', width: '100'},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'center', width: '400'},
                {name: 'fecha', index: 'fecha', resizable: false, align: 'center', width: '100'},
                {name: 'nit_proveedor', index: 'nit_proveedor', resizable: false, align: 'center', width: '100'},
                {name: 'payment_name', index: 'payment_name', resizable: false, align: 'center', width: '400'},
                {name: 'cantidad', index: 'cantidad', resizable: false, align: 'center', width: '100'},
                {name: 'valor_unitario', index: 'valor_unitario', resizable: false, align: 'center', width: '100', formatter: 'currency',
                    formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}],
            rowNum: 10000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_lista_mapeo',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_lista_mapeo", {edit: false, add: false, del: false, search: false, refresh: false});
        jQuery("#tbl_lista_mapeo").navButtonAdd('#page_lista_mapeo', {
            caption: "Exportar Excel",
            onClickButton: function () {
                var info = jQuery("#tbl_lista_mapeo").getGridParam('records');
                if (info > 0) {
                    exportarExcel();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }

            }
        });

    }
}

function refrescarGridMapeo(subcategoria) {
    var url = '/fintra/controlleropav?estado=Procesos&accion=Catalogo&opcion=57&subcategoria=' + subcategoria;
    jQuery("#tbl_lista_mapeo").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#tbl_lista_mapeo').trigger("reloadGrid");
}

function cargarCombo(id, filtro) {
    var elemento = $('#' + id)
            , sql = (id === 'categoria') ? 'categorias' : 'subcategoriaxcategoria';
    //$("#lui_tabla_productos,#load_tabla_productos").show();
    $.ajax({
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        datatype: 'json',
        type: 'get',
        data: {opcion: 40, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');

                    }
                }
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });

}

function  exportarExcel() {
    var fullData = jQuery("#tbl_lista_mapeo").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "/fintra/controlleropav?estado=Procesos&accion=Catalogo",
        data: {
            listado: myJsonString,
            opcion: 58
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}