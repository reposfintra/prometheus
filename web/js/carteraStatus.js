/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 $(document).ready(function() {
     
   jsonEstadosCartera = listarEstadosCartera();
    if ($('#div_config_estados_cartera').is(':visible')) {    
         listarUndsNegocio();
    }else{
        cargarEstadosCartera();
    }
  
 });
 
 
 function cargarEstadosCartera(){
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=0';
    if ($("#gview_tabla_status_cartera").length) {
         refrescarGridEstadosCartera();
     }else {
        jQuery("#tabla_status_cartera").jqGrid({
            caption: 'Estados Cartera',
            url: url,
            datatype: 'json',
            height: 250,
            width: 380,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Estado', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key:true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '700px'},
                {name: 'descripcion', index: 'descripcion', hidden:true, sortable: true, align: 'center', width: '700px'},
                {name: 'reg_status', index: 'reg_status', hidden:true, sortable: true, align: 'center', width: '90px'},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '200px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            pager:'#page_tabla_status_cartera',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {
                    var ids = jQuery("#tabla_status_cartera").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        var estado = jQuery("#tabla_status_cartera").getRowData(cl).reg_status;
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarEstadosCartera('" + cl + "');\">";
                        if (estado==='A'){
                            $("#tabla_status_cartera").jqGrid('setRowData',ids[i],false, {weightfont:'bold',background:'#F6CECE'});          
                            ac = "<img src='/fintra/images/botones/iconos/check-blue.png' align='absbottom'  name='activar' id='activar' width='15' height='15' title ='Activar'  onclick=\"mensajeConfirmAction('Esta seguro de activar el estado seleccionado?','250','150',activarEstadoCartera,'" + cl + "');\">";
                        }else{
                            ac = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el estado seleccionado?','250','150',anularEstadoCartera,'" + cl + "');\">";
                        }                      
                        jQuery("#tabla_status_cartera").jqGrid('setRowData', ids[i], {actions: ed+'  '+ac});
                       
                    }
                },
            loadError: function(xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_status_cartera",{search:false,refresh:false,edit:false,add:false,del:false});
        jQuery("#tabla_status_cartera").jqGrid("navButtonAdd", "#page_tabla_status_cartera", {
            caption: "Nuevo", 
            title: "Agregar nuevo estado",           
            onClickButton: function() {
               crearEstadosCartera(); 
            }
        });
     }
}


function refrescarGridEstadosCartera(){    
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=0';
    jQuery("#tabla_status_cartera").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#tabla_status_cartera').trigger("reloadGrid");
}

function crearEstadosCartera(){
    $('#nomestado').val('');
    $('#descestado').val('');
    $('#div_estadosCartera').fadeIn('slow');
    AbrirDivCrearEstadosCartera();
}

function AbrirDivCrearEstadosCartera(){
      $("#div_estadosCartera").dialog({
        width: 700,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR ESTADOS CARTERA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
              guardarEstadoCartera();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarEstadoCartera(){    
    var nomEstado = $('#nomestado').val();
    var descEstado = $('#descestado').val();
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if(nomEstado!==''){
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 1,
                    nombre: nomEstado,
                    descripcion: descEstado
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {
                            refrescarGridEstadosCartera();                           
                            mensajesDelSistema("Se cre� el estado de cartera exitosamente", '250', '150', true); 
                            $('#nomestado').val('');
                            $('#descestado').val('');  
                        }
                        
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear el nuevo estado!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
    }else{
       mensajesDelSistema("Debe ingresar el nombre del estado", '250', '150');
    }
}

function editarEstadosCartera(cl){
    
    $('#div_editar_estadosCartera').fadeIn("slow");
    var fila = jQuery("#tabla_status_cartera").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    var estado = fila['reg_status'];
     if (estado==="A"){
        $('#nomestadoEdit').attr({readonly: true});
        $('#descestadoEdit').attr({readonly: true});
    }else{
        $('#nomestadoEdit').attr({readonly: false});
        $('#descestadoEdit').attr({readonly: false});
    }
    $('#idEstadoEdit').val(cl);
    $('#nomestadoEdit').val(nombre);
    $('#descestadoEdit').val(descripcion);
    AbrirDivEditarEstadosCartera();
    
}

function AbrirDivEditarEstadosCartera(){
      $("#div_editar_estadosCartera").dialog({
        width: 700,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'EDITAR ESTADOS CARTERA',
        closeOnEscape: false,
        buttons: {   
            "Actualizar": function(){
               actualizarEstadoCartera();  
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarEstadoCartera(){
    var nombre = $('#nomestadoEdit').val();
    var descripcion = $('#descestadoEdit').val();
    var idEstado = $('#idEstadoEdit').val();
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if(nombre!==''){
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 2,
                nombre: nombre,
                descripcion: descripcion,
                idEstado: idEstado
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridEstadosCartera(); 
                        mensajesDelSistema("Se actualiz� el estado de cartera correctamente", '250', '150', true);                      
                        
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar el estado!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }else{
         mensajesDelSistema("Debe ingresar un nombre para el estado!!", '250', '150');      
    }
    
}

function anularEstadoCartera(cl){
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 3,            
            idEstado: cl,
            estadoCartera: "A"
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridEstadosCartera();                  
                    mensajesDelSistema("Se anul� el estado", '250', '150', true);                                   
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular el estado!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function activarEstadoCartera(cl){
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 3,            
            idEstado: cl,
            estadoCartera: ""
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridEstadosCartera();                  
                    mensajesDelSistema("Se activ� el estado", '250', '150', true);                               
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo activar el estado!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function listarEstadosCartera (){
    var  Result={};
    $.ajax({
        type: 'GET',
        url: "/fintra/controller?estado=Proceso&accion=Ejecutivo&opcion=6",        
        dataType: 'json', 
        async:false,
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                   Result={};
                }else{
                    Result=json;
                }                
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
    return Result;
}

function listarUndsNegocio() {
    
    var jsonParam = {opcion: 4,
                     ref_1: "und_proc_ejec"};
    var lastSel = -1;    
    var grid_listar_unds_negocio = $("#tabla_unidades_negocio");
  
    if ($("#gview_tabla_unidades_negocio").length) {
        reloadGridListarUndsNegocio(grid_listar_unds_negocio,jsonParam);
    } else {

        grid_listar_unds_negocio.jqGrid({        
            caption:'ESTADOS CARTERA POR UNIDAD DE NEGOCIO',
            url: "./controller?estado=Proceso&accion=Ejecutivo",
            mtype: "POST",	 
            datatype: "json",           
            height: '300',
            width: '450',          
            colNames: ['Id', 'C�digo', 'Descripci�n', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', width: 80, resizable:false, sortable: true, align: 'center', key: true},
                {name: 'codigo', index: 'codigo', width: 80, resizable:false, sortable: true, align: 'center'},
                {name: 'descripcion', index: 'descripcion', resizable:false, sortable: true, width: 150, align: 'center'},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: 80}
              
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,
            subGrid: true,   
            subGridOptions: { "plusicon" : "ui-icon-triangle-1-e",
                      "minusicon" :"ui-icon-triangle-1-s",
                      "openicon" : "ui-icon-arrowreturn-1-e",
                      "reloadOnExpand" : true,
                      "selectOnExpand" : true },
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            pager:'#page_tabla_unidades_negocio',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {                
                dataType: "json",
                type: "POST",                
                data: jsonParam
            },    
            gridComplete: function() {
                var ids = jQuery("#tabla_unidades_negocio").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];                  
                    el = "<img src='/fintra/images/botones/iconos/eliminar.gif' align='absbottom'  name='desasignar' id='editar' width='15' height='15' title ='Desasignar'  onclick=\"mensajeConfirmAction('Esta seguro de quitar la unidad de negocio seleccionada del proceso juridico?','250','175',existeDemandaRelUndNegocio,'" + cl + "');\">";
                   // an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='desasignar' id='desasignar' width='15' height='15' title ='Desasignar'  onclick=\"mensajeConfirmAction('Esta seguro de desasginar la unidad seleccionada del proceso juridico?','250','150',anularEstadoCartera,'" + cl + "');\">";
               
                    jQuery("#tabla_unidades_negocio").jqGrid('setRowData', ids[i], {actions: el});

                }
            },
            subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id = subgrid_id+"_t"; 
                jQuery("#"+subgrid_id).html("<table id='"+subgrid_table_id+"'></table>");
                jQuery("#"+subgrid_table_id).jqGrid({
                    url: './controller?estado=Proceso&accion=Ejecutivo&opcion=5'
                        + '&und_negocio='+row_id,                    
                    datatype: 'json',
                    colNames: ['Id','D�as Mora', 'Estado','IdEstado'],
                    colModel: [
                        {name: 'valor_01', index: 'valor_01', sortable: true, width: 60, align: 'center', hidden:true, key:true},
                        {name: 'valor_02', index: 'valor_02', sortable: true, align: 'center',  width: 163},
                        {name: 'valor_03', index: 'valor_03', sortable: true, editable: true, align: 'center', edittype: 'select', 
                        editoptions: {
                            value:jsonEstadosCartera,
                            defaultValue: 1,
                            dataEvents: [{type: 'change', fn: function(e) {
                                        try {
                                            var rowId = e.target.id.replace("_valor_03", "");
                                            var id_estado_cartera = e.target.value;                                          
                                            jQuery("#" + subgrid_table_id).jqGrid('setCell', rowId, 'valor_04', id_estado_cartera);
                                        } catch (exc) {
                                        }
                                        return;
                                    }}, {type: "keyup", fn: function(e) {
                                        $(e.target).trigger("change");
                                    }}
                            ]}   
                       },                            
                       {name: 'valor_04', index: 'valor_04', hidden: true, editrules: {required: true}}
                    ],
                    rowNum:150,
                    height:'80%',
                    width:'90%',
                    cellEdit:true,  
                    cellsubmit: "clientArray",
                    jsonReader: {
                        root: 'rows',
                        cell:'',
                        repeatitems: false,
                        id: '0'
                    },     
                    afterSaveCell: function(rowid, celname, value, iRow, iCol) {                      
                        if (celname === 'valor_03') {      
                            var intervalo_mora = $("#"+subgrid_table_id).getRowData(rowid).valor_01;
                            var estado_cartera = $("#"+subgrid_table_id).getRowData(rowid).valor_04;
                            actualizarConfigStatusCartera(row_id, intervalo_mora, estado_cartera);         
                        }
                    },
                    onSelectRow: function(id) {
                        if (id && id !== lastSel) {
                            jQuery("#"+subgrid_table_id).jqGrid('restoreRow', lastSel);
                            jQuery("#"+subgrid_table_id).jqGrid('editRow', id, true);
                            lastSel = id;
                        }
                    },
                    loadError: function(xhr, status, error) {
                        alert(error);
                    }
                });               
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_unidades_negocio",{search:false,refresh:false,edit:false,add:false,del:false});      
        jQuery("#tabla_unidades_negocio").jqGrid("navButtonAdd", "#page_tabla_unidades_negocio", {
            caption: "Nuevo", 
            title: "Agregar unidad de negocio",           
            onClickButton: function() {
               cargarUnidadesNegocio();
               $("#dialogAsignarUndProceso").fadeIn();     
               ventanaAgregarUndNegocio();               
            }
        });   

    }

}

function reloadGridListarUndsNegocio(grid_listar_unds_negocio, params) {
   grid_listar_unds_negocio.setGridParam({
            url: "./controller?estado=Proceso&accion=Ejecutivo",       
            datatype: 'json',
            ajaxGridOptions: {
                type: "POST",
                data: params
            }            
        }).trigger("reloadGrid");

}

function actualizarConfigStatusCartera(id_undNegocio, id_intervMora, id_estadoCartera) {
    
    $.ajax({
        async: false,
        url: "./controller?estado=Proceso&accion=Ejecutivo",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 7,
            unidad_negocio: id_undNegocio,
            intervalo_mora: id_intervMora,
            estado_cartera: id_estadoCartera

        },
        success: function(json) {

            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

//                if (json.respuesta === "OK") {                  
//                    reloadGridListarUndsNegocio($("#tabla_unidades_negocio"),{opcion: 4});                 
//                }

            } else {

                mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci�n!!", '250', '150');
            }

        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
   
}

 function cargarUnidadesNegocio() {
    $('#unidad_negocio').empty();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Proceso&accion=Ejecutivo",
        dataType: 'json',
        async:false,
        data: {
            opcion: 20,
            ref_1: "und_proc_ejec"
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#unidad_negocio').append("<option value='0'>Seleccione</option>");
               
                    for (var key in json) {              
                       $('#unidad_negocio').append('<option value=' + json[key].id + '>' + json[key].descripcion + '</option>');                
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {
                
                  $('#unidad_negocio').append("<option value='0'>Seleccione</option>");


            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function ventanaAgregarUndNegocio(){
      $("#dialogAsignarUndProceso").dialog({
        width: 500,
        height: 170,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ASIGNAR UNIDAD DE NEGOCIO AL PROCESO EJECUTIVO',
        closeOnEscape: false,
        buttons: {  
            "Agregar": function () {
               agregarUnidadProceso();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function agregarUnidadProceso(){
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Proceso&accion=Ejecutivo",
        dataType: 'json',
        data: {
            opcion: 32,
            und_negocio: $('#unidad_negocio').val(),
            ref_1: "und_proc_ejec"
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                 if (json.respuesta === "OK") {                  
                    reloadGridListarUndsNegocio($("#tabla_unidades_negocio"),{opcion: 4,
                                                                              ref_1: "und_proc_ejec"});   
                    $("#dialogAsignarUndProceso").dialog('close');
                                                                          
                }

            } else {
                
                  mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci�n!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function existeDemandaRelUndNegocio(id){
    $.ajax({
        type: 'POST',
        url: './controller?estado=Proceso&accion=Ejecutivo',
        datatype:'json',     
        data:{
            opcion: 34,
            und_negocio:id    
        },          
        success: function(json) {
            if (!isEmptyJSON(json)) {
     
                if (json.respuesta === "SI") {               
                    mensajeConfirmAnulacion('Alerta!!! Existen demandas realizadas para dicha unidad, desea continuar?','350','165',desasignarUnidadProceso, id);                    
                }else{
                    desasignarUnidadProceso(id);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo realizar el proceso!!", '250', '150');
            }                 
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }  
    });
}

function desasignarUnidadProceso(cl){
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 33,            
            und_negocio:cl,
            ref_1:""    
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    reloadGridListarUndsNegocio($("#tabla_unidades_negocio"),{opcion: 4,
                                                                              ref_1: "und_proc_ejec"}); 
                    mensajesDelSistema("Unidad de negocio desasignada satisfactoriamente", '250', '150', true);                                    
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo desasignar la unidad de negocio seleccionada!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


/*function ConfigStatusCarteraToJson() {
    var filasId = jQuery("#tabla_unidades_negocio").jqGrid('getDataIDs');
     var jsonObj = {"data": []};
    if (filasId != '') {
        for (var i = 0; i < filasId.length; i++) {
            var idUnd = filasId[i];
            var filasIdDet = jQuery("#tabla_unidades_negocio_"+idUnd+"_t").jqGrid('getDataIDs');       
            for (var j = 0; j < filasIdDet.length; j++) {
                var idRangoMora = filasIdDet[j];
                var idEstadoCartera = jQuery("#tabla_unidades_negocio_"+idUnd+"_t").getRowData(idRangoMora).valor_03;
                var item = {};
                item ["unidad_negocio"] = parseInt(idUnd);
                item ["intervalo_mora"] = parseInt(idRangoMora);
                item ["estado_cartera"] = parseInt(idEstadoCartera);               
                jsonObj.data.push(item);                 
            }      
        }
    }
    
   return JSON.stringify(jsonObj); 
}*/


function mensajeConfirmAnulacion(msj, width, height, okAction, id) {   
    $("#msj").html("<span style='background: url(/fintra/images/warning03.png); height:32px; width:32px; float: left; margin: 0 7px 20px 0'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);               
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajeConfirmAction(msj, width, height, okAction, id) {
  
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}



function mensajesDelSistema(msj, width, height, swHideDialog) {   
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}
