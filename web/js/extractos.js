/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $("#divSalida").dialog({
        autoOpen: false,
        height: "auto",
        width: "300px",
        modal: true,
        autoResize: true,
        resizable: false,
        position: "center",
        closeOnEscape: false
    });
    $("#divSalidaEx").dialog({
        autoOpen: false,
        height: "auto",
        width: "300px",
        modal: true,
        autoResize: true,
        resizable: false,
        position: "center",
        closeOnEscape: false
    });
    $("#div_bt_foto").dialog({
        autoOpen: false,
        height: "auto",
        width: "300px",
        modal: true,
        autoResize: true,
        resizable: false,
        position: "center",
        closeOnEscape: false
    });

    $("#botonAceptar").hide();

});

function cerrarDiv(div)
{
    $(div).dialog('close');
}

function cargarPrevisualizacionExtracto() {
    var uneg = $('#uNeg').val();
    var listUneg = uneg.toString().split(",");
    var periodo = $('#periodo').val();
    var ciclo = $('#ciclo').val();
    var fechaHoy = $('#fechaHoy').val();
    var vencMayor = ($('#vencMayor').val() === null) ? 'undefined' : $('#vencMayor').val();
    if (periodo === '' || ciclo === '' || vencMayor === 'undefined' || fechaHoy === '') {
        mensaje('Seleccione todos los filtros');
        $('#dialogo4').dialog("close");
    } else {
        if (listUneg[0] === '1') {
            var url = './controller?estado=Previsualizar&accion=Extractos&opcion=2&periodo=' + periodo + '&uneg=' + uneg + '&ciclo=' + ciclo + '&vencMayor=' + vencMayor + '&fechaHoy=' + fechaHoy;
            cargarPrevisualizacionExtractoMicro(url);
        } else {
            var url = './controller?estado=Previsualizar&accion=Extractos&opcion=3&periodo=' + periodo + '&uneg=' + uneg + '&ciclo=' + ciclo + '&vencMayor=' + vencMayor + '&fechaHoy=' + fechaHoy;
            cargarPrevisualizacionExtractoFenalco(url);
        }
    }
}

function cargarPrevisualizacionExtractoMicro(url) {
    $('#tb_fenalco').fadeOut();
    $('#tb_micro').fadeIn();
    if ($("#gview_PreExtractoMicro").length) {
        refrescarGridPreExtractoMicro(url);
    } else {
        jQuery("#PreExtractoMicro").jqGrid({
            caption: 'Previsualizacion Extracto',
            url: url,
            datatype: 'json',
            height: 450,
            width: 3600,
            colNames: ['', 'Fecha Vencimiento', 'Venc. Mayor', 'Negocio', 'Nit', 'Nombre Cliente', 'Direccion', 'Barrio', 'Ciudad', 'Departamento', 'Agencia', 'Linea Producto', 'Cuotas Vencidas', 'Cuotas Pendientes', 'Dias Vencidos', 'Fecha Ultimo Pago', 'Subtotal Det', 'Total Sanciones', 'Descuentos', 'Total Abonos', 'Total Det',
                'Observaciones', 'Paguese Antes De', 'Estado', 'Capital', 'Cat', 'Int. Financ', 'Interes Mora', 'GxC', 'Dscto Capital', 'Dscto Cat', 'Dscto Int. Financ', 'Dscto Int. Mora', 'Dscto GxC', 'Subtotal Cte', 'Subtotal Venc', 'Subtotal', 'Total Dscto', 'Total'],
            colModel: [
                {name: 'idUndNegocio', index: 'idUndNegocio', hidden: true},
                {name: 'vencimiento_rop', index: 'und_negocio', sortable: true, align: 'center', width: '900px'},
                {name: 'venc_mayor', index: 'venc_mayor', sortable: true, align: 'center', width: '900px'},
                {name: 'negasoc', index: 'negasoc', sortable: true, align: 'center', width: '600px', key: true},
                {name: 'nit', index: 'nit', sortable: true, align: 'center', width: '800px'},
                {name: 'nom_cli', index: 'nom_cli', sortable: true, align: 'left', width: '2000px'},
                {name: 'direccion', index: 'direccion', sortable: true, align: 'center', width: '2000px'},
                {name: 'barrio', index: 'barrio', sortable: true, width: '900px', align: 'center'},
                {name: 'ciudad', index: 'ciudad', sortable: true, align: 'center', width: '900px'},
                {name: 'departamento', index: 'departamento', sortable: true, width: '700px', align: 'center'},
                {name: 'agencia', index: 'agencia', sortable: true, width: '700px', align: 'center'},
                {name: 'linea_producto', index: 'linea_producto', sortable: true, width: '900px', align: 'center'},
                {name: 'total_cuotas_vencidas', index: 'total_cuotas_vencidas', sortable: true, width: '700px', align: 'center'},
                {name: 'cuotas_pendientes', index: 'cuotas_pendientes', sortable: true, width: '700px', align: 'center'},
                {name: 'min_dias_ven', index: 'min_dias_ven', sortable: true, width: '650px', align: 'center'},
                {name: 'fecha_ultimo_pago', index: 'fecha_ultimo_pago', sortable: true, width: '800px', align: 'center'},
                {name: 'subtotal_det', index: 'subtotal_det', width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'total_sanciones', index: 'total_sanciones', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'total_dscto_det', index: 'total_dscto_det', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'total_abonos', index: 'total_dscto_det', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'total_det', index: 'total_det', width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'observaciones', index: 'observaciones', width: '2000px', align: 'center'},
                {name: 'msg_paguese_antes', index: 'msg_paguese_antes', width: '800px', align: 'center'},
                {name: 'msg_estado', index: 'msg_estado', width: '800px', align: 'center'},
                {name: 'capital', index: 'capital', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'cat', index: 'cat', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'int_cte', index: 'int_cte', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'int_mora', index: 'int_mora', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'gxc', index: 'gxc', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'dscto_capital', index: 'dscto_capital', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'dscto_cat', index: 'dscto_cat', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'dscto_int_cte', index: 'dscto_int_cte', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'dscto_int_mora', index: 'dscto_int_mora', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'dscto_gxc', index: 'dscto_gxc', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'subtotal_corriente', index: 'subtotal_corriente', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'subtotal_vencido', index: 'subtotal_vencido', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'subtotal', index: 'subtotal', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'total_descuento', index: 'total_descuento', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'total', index: 'total', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}}
            ],
            rowNum: 10000,
            rowTotal: 500000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            pager: $('#pageMc'),
            viewrecords: true,
            hidegrid: false,
            rownumbers: true,
            multiselect: true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });

        jQuery("#PreExtractoMicro").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    }
}

function cargarPrevisualizacionExtractoFenalco(url) {
    $('#tb_fenalco').fadeIn();
    $('#tb_micro').fadeOut();
    if ($("#gview_PreExtractoFenalco").length) {
        refrescarGridPreExtractoFenalco();
    } else {
        jQuery("#PreExtractoFenalco").jqGrid({
            caption: 'Previsualizacion Extracto',
            url: url,
            datatype: 'json',
            height: 450,
            width: 3800,
            colNames: ['', 'Fecha Vencimiento', 'Venc. Mayor', 'Negocio', 'Nit', 'Nombre Cliente', 'Direccion', 'Barrio', 'Ciudad', 'Departamento', 'Agencia', 'Linea Producto', 'Cuotas Vencidas', 'Cuotas Pendientes', 'Dias Vencidos', 'Fecha Ultimo Pago', 'Subtotal Det', 'Total Sanciones', 'Descuentos', 'Total Abonos', 'Total Det',
                'Observaciones', 'Paguese Antes De', 'Estado', 'Capital', 'Seguro', 'Int. Financ', 'Interes Mora', 'GxC', 'Dscto Capital', 'Dscto Seguro', 'Dscto Int. Financ', 'Dscto Int. Mora', 'Dscto GxC', 'Subtotal Corriente', 'Subtotal Vencido', 'Subtotal', 'Total Dscto', 'Total', 'Est. Comercio'],
            colModel: [
                {name: 'idUndNegocio', index: 'idUndNegocio', hidden: true},
                {name: 'vencimiento_rop', index: 'vencimiento_rop', sortable: true, align: 'center', width: '900px'},
                {name: 'venc_mayor', index: 'venc_mayor', sortable: true, align: 'center', width: '900px'},
                {name: 'negasoc', index: 'negasoc', sortable: true, align: 'center', width: '600px', key: true},
                {name: 'nit', index: 'nit', sortable: true, align: 'center', width: '800px'},
                {name: 'nom_cli', index: 'nom_cli', sortable: true, align: 'left', width: '2000px'},
                {name: 'direccion', index: 'direccion', sortable: true, align: 'left', width: '2000px'},
                {name: 'barrio', index: 'barrio', sortable: true, width: '1000px', align: 'center'},
                {name: 'ciudad', index: 'ciudad', sortable: true, align: 'center', width: '800px'},
                {name: 'departamento', index: 'departamento', sortable: true, width: '800px', align: 'center'},
                {name: 'agencia', index: 'agencia', sortable: true, width: '800px', align: 'center'},
                {name: 'linea_producto', index: 'linea_producto', sortable: true, width: '900px', align: 'center'},
                {name: 'total_cuotas_vencidas', index: 'total_cuotas_vencidas', sortable: true, width: '700px', align: 'center'},
                {name: 'cuotas_pendientes', index: 'cuotas_pendientes', sortable: true, width: '800px', align: 'center'},
                {name: 'min_dias_ven', index: 'min_dias_ven', sortable: true, width: '650px', align: 'center'},
                {name: 'fecha_ultimo_pago', index: 'fecha_ultimo_pago', sortable: true, width: '800px', align: 'center'},
                {name: 'subtotal_det', index: 'subtotal_det', width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'total_sanciones', index: 'total_sanciones', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'total_dscto_det', index: 'total_dscto_det', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'total_abonos', index: 'total_dscto_det', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'total_det', index: 'total_det', width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'observaciones', index: 'observaciones', width: '2000px', align: 'center'},
                {name: 'msg_paguese_antes', index: 'msg_paguese_antes', width: '800px', align: 'center'},
                {name: 'msg_estado', index: 'msg_estado', width: '800px', align: 'center'},
                {name: 'capital', index: 'capital', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'seguro', index: 'seguros', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'int_cte', index: 'int_cte', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'int_mora', index: 'int_mora', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'gxc', index: 'gxc', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'dscto_capital', index: 'dscto_capital', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'dscto_seguro', index: 'dscto_seguro', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'dscto_int_cte', index: 'dscto_int_cte', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'dscto_int_mora', index: 'dscto_int_mora', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'dscto_gxc', index: 'dscto_gxc', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'subtotal_corriente', index: 'subtotal_corriente', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'subtotal_vencido', index: 'subtotal_vencido', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'subtotal', index: 'subtotal', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'total_descuento', index: 'total_descuento', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'total', index: 'total', sortable: false, width: '800px', align: 'center', formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0}},
                {name: 'est_comercio', index: 'est_comercio', width: '800px', align: 'center'}
            ],
            rowNum: 10000,
            rowTotal: 500000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            pager: $('#pageFen'),
            viewrecords: true,
            hidegrid: false,
            rownumbers: true,
            multiselect: true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });

        jQuery("#PreExtractoFenalco").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    }
}

function refrescarGridPreExtractoMicro() {
    var uneg = $('#uNeg').val();
    var periodo = $('#periodo').val();
    var ciclo = $('#ciclo').val();
    var vencMayor = ($('#vencMayor').val() === null) ? 'undefined' : $('#vencMayor').val();
    var fechaHoy= $('#fechaHoy').val();
    var url = './controller?estado=Previsualizar&accion=Extractos&opcion=2&periodo=' + periodo + '&uneg=' + uneg + '&ciclo=' + ciclo + '&vencMayor=' + vencMayor + '&fechaHoy=' + fechaHoy;
    jQuery("#PreExtractoMicro").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#PreExtractoMicro").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    jQuery('#PreExtractoMicro').trigger("reloadGrid");
}

function refrescarGridPreExtractoFenalco() {
    var uneg = $('#uNeg').val();
    var periodo = $('#periodo').val();
    var ciclo = $('#ciclo').val();
    var vencMayor = ($('#vencMayor').val() === null) ? 'undefined' : $('#vencMayor').val();
    var url = './controller?estado=Previsualizar&accion=Extractos&opcion=3&periodo=' + periodo + '&uneg=' + uneg + '&ciclo=' + ciclo + '&vencMayor=' + vencMayor;
    jQuery("#PreExtractoFenalco").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#PreExtractoFenalco").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    jQuery('#PreExtractoFenalco').trigger("reloadGrid");
}

function generarArchivoExtracto() {
    var uneg = $('#uNeg').val();
    var periodo = $('#periodo').val();
    var ciclo = $('#ciclo').val();
    var fechaHoy = $('#fechaHoy').val();
    var vencMayor = ($('#vencMayor').val() === null) ? 'undefined' : $('#vencMayor').val();
    if (periodo === '' || ciclo === '' || vencMayor === 'undefined' || fechaHoy === '') {
        mensaje('Seleccione todos los filtros');
        $('#dialogo4').dialog("close");
    } else {
        $("#divSalida").dialog("open");
        $("#imgload1").show();
        $("#msj3").show();
        $("#resp1").hide();
        $.ajax({
            type: "POST",
            url: "./controller?estado=Previsualizar&accion=Extractos&opcion=4&periodo=" + periodo + "&uneg=" + uneg + "&ciclo=" + ciclo + "&vencMayor=" + vencMayor + "&fechaHoy=" + fechaHoy,
            dataType: "html",
            success: function (resp) {
                $("#imgload1").hide();
                $("#msj3").hide();
                $("#resp1").show();
                var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalida" + "\")' /></div>"
                document.getElementById('resp1').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
            },
            error: function (resp) {
                alert("Ocurrio un error enviando los datos al servidor");
            }
        });
    }
}

function confirmarGeneracionExtracto() {
    var msj = "Desea generar el extracto?";
    $("#msj4").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo4").dialog({
        width: 300,
        height: 150,
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Si": function () {
                generarExtracto();
            }
            , "No": function () {
                $(this).dialog("close");
            }
        }
    });

}

function generarExtracto() {
    $('#dialogo4').dialog("close");
    var uneg = $('#uNeg').val();
    var periodo = $('#periodo').val();
    var ciclo = $('#ciclo').val();
    var fechaHoy = $('#fechaHoy').val();
    var vencMayor = ($('#vencMayor').val() === null) ? 'undefined' : $('#vencMayor').val();
    if (periodo === '' || ciclo === '' || vencMayor === 'undefined' || fechaHoy === '') {
        mensaje('Seleccione todos los filtros');
        $('#dialogo4').dialog("close");
    } else {
        var url = './controller?estado=Previsualizar&accion=Extractos&opcion=5&periodo=' + periodo + '&ciclo=' + ciclo + '&uneg=' + uneg + '&vencMayor=' + vencMayor + "&fechaHoy=" + fechaHoy;
        divEspera();
        $('#imgload2').show();
        $.ajax({
            type: "POST",
            url: url,
            dataType: "text",
            success: function (resp) {
                $('#dialogo2').dialog("close");

                var res = resp.trim();
                if (res === "OK") {
                    mensaje("Extracto Generado con �xito");
                } else if (res === "ERROR") {
                    mensaje("No se pudo generar el extracto");
                } else {
                    mensaje("El extracto ya ha sido generado o no hay registros para generar");
                }
            },
            error: function (error) {
                alert(error);
            }

        });
    }
}


function mensaje(msj) {
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'>X</span> " + msj);
    $("#dialogo").dialog({
        width: 300,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function divEspera() {
    $("#msj2").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'>X</span> Generando ...");
    $("#dialogo2").dialog({
        width: 300,
        height: 130,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false
    });
}

function confirmarIncluirExtracto() {
    var msj = "Desea incluir los registros seleccionados para generar?";
    $("#msj4").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo4").dialog({
        width: 300,
        height: 150,
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Si": function () {
                incluirExtracto();
            }
            , "No": function () {
                $(this).dialog("close");
            }
        }
    });
}

function incluirExtracto() {
    var ciclo = $('#ciclo').val();
    var uneg = $('#uNeg').val();
    var listUneg = uneg.toString().split(",");
    var periodo = $('#periodo').val();
    if (periodo === '' || ciclo === '') {
        mensaje('Seleccione todos los filtros');
        $('#dialogo4').dialog("close");
    } else {
        var url = './controller?estado=Previsualizar&accion=Extractos';
        var listado = "";
        var filasId = "";
        var grid = "";
        if (listUneg[0] === "1") {
            grid = jQuery("#PreExtractoMicro");
            filasId = grid.jqGrid('getGridParam', 'selarrrow');
        } else {
            grid = jQuery("#PreExtractoFenalco");
            filasId = grid.jqGrid('getGridParam', 'selarrrow');
        }

        if (filasId !== "") {
            for (var i = 0; i < filasId.length; i++) {
                var fila = grid.getRowData(filasId[i]);
                var und = fila['idUndNegocio'];
                var codNeg = fila['negasoc'];
                var nit = fila['nit'];
                listado += und + "," + codNeg + "," + nit + ";";
            }

            $.ajax({
                type: "POST",
                url: url,
                dataType: "text",
                data: {
                    opcion: 6,
                    periodo: periodo,
                    listado: listado,
                    ciclo: ciclo
                },
                success: function (data) {
                    var resp = data.trim();
                    if (resp === "OK") {
                        mensaje("Registros Incluidos con Exito");
                        $("#dialogo4").dialog("close");
                        if (listUneg[0] === "1") {
                            refrescarGridPreExtractoMicro();
                        } else {
                            refrescarGridPreExtractoFenalco();
                        }
                    }

                }

            });

        } else {
            $("#dialogo4").dialog("close");
            mensaje("Debe seleccionar los registros a guardar");
        }
    }
}

function confirmarlimpiarExtracto() {
    var msj = "Esta seguro que desea borrar los registros incluidos?";
    $("#msj4").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo4").dialog({
        width: 300,
        height: 150,
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Si": function () {
                limpiarExtracto();
            }
            , "No": function () {
                $(this).dialog("close");
            }
        }
    });
}

function limpiarExtracto() {
    var uneg = $('#uNeg').val();
    var listUneg = uneg.toString().split(",");
    var periodo = $('#periodo').val();
    var ciclo = $('#ciclo').val();
    if (periodo === '' || ciclo === '') {
        mensaje('Seleccione todos los filtros');
        $('#dialogo4').dialog("close");
    } else {
        var url = './controller?estado=Previsualizar&accion=Extractos&opcion=7&periodo=' + periodo + '&uneg=' + uneg + '&ciclo=' + ciclo;
        $.ajax({
            type: "POST",
            url: url,
            dataType: "text",
            success: function (data) {
                if (data !== "") {
                    mensaje("Registros Limpiados");
                    $("#dialogo4").dialog("close");
                    if (listUneg[0] === "1") {
                        refrescarGridPreExtractoMicro();
                    } else {
                        refrescarGridPreExtractoFenalco();
                    }
                }
            }
        });
    }
}

function exportarPrevisualizacionExtracto() {
    var uneg = $('#uNeg').val();
    var periodo = $('#periodo').val();
    var ciclo = $('#ciclo').val();
    var vencMayor = ($('#vencMayor').val() === null) ? 'undefined' : $('#vencMayor').val();
    if (periodo === '' || ciclo === '' || vencMayor === 'undefined') {
        mensaje('Seleccione todos los filtros');
        $('#dialogo4').dialog("close");
    } else {
        $("#divSalidaEx").dialog("open");
        $("#imgloadEx").show();
        $("#msjEx").show();
        $("#respEx").hide();
        $.ajax({
            type: "POST",
            url: "./controller?estado=Previsualizar&accion=Extractos&opcion=8&periodo=" + periodo + "&uneg=" + uneg + "&ciclo=" + ciclo + "&vencMayor=" + vencMayor,
            dataType: "html",
            success: function (resp) {
                $("#imgloadEx").hide();
                $("#msjEx").hide();
                $("#respEx").show();
                var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>"
                document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
            },
            error: function (resp) {
                alert("Ocurrio un error enviando los datos al servidor");
            }
        });
    }
}

function exportarExtracto() {
    var fullData = "";
    var uneg = $('#uNeg').val();
    var listUneg = uneg.toString().split(",");
    if (listUneg[0] === "1") {
        fullData = jQuery("#PreExtractoMicro").jqGrid('getRowData');
    } else {
        fullData = jQuery("#PreExtractoFenalco").jqGrid('getRowData');
    }

    var myJsonString = JSON.stringify(fullData);

    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Previsualizar&accion=Extractos&uneg=" + uneg,
        data: {
            listado: myJsonString,
            opcion: 8
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}


function GenerarMI() {
    loading("Espere un momento mientras se genera el cat y mi.", "270", "140");
    var periodo = $('#periodo').val();
    var ciclo = $('#ciclo').val();
    
    if (periodo !== '' || ciclo !== '') {
   
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "./controller?estado=Previsualizar&accion=Extractos&opcion=9&periodo="+periodo+"&ciclo="+ciclo,
            success: function (resp) {
                setTimeout(function () {
                    var re = resp.trim();
                    $("#dialogLoading").dialog('close');
                    if (re === "OK") {
                        mensaje("Generaci�n de MI y Cat exitoso");
                    } else if (re === "Error") {
                        mensaje("Error al generar MI y Cat");
                    } else {
                        mensaje(re);
                    }

                }, 5000);
            },
            error: function (resp) {
                alert("Ocurrio un error enviando los datos al servidor");
            }
        });

        
     } else {
        mensaje("Debe seleccior el periodo y el n�mero del ciclo");

    }
}

function TomarFoto() {
    var periodo = $('#periodo').val();
    var ciclo = $('#ciclo').val();
    loading("Espere un momento mientras se toma la foto.", "270", "140");
    if (periodo !== '' || ciclo !== '') {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "./controller?estado=Previsualizar&accion=Extractos&opcion=10&periodo=" + periodo + "&ciclo=" + ciclo,
            success: function (resp) {
                var re = resp.trim();
                $("#dialogLoading").dialog('close');
                if (re === "OK") {
                    mensaje("Foto tomada con �xito");
                } else if (re === "Error") {
                    mensaje("Error al tomar la foto");
                } else {
                    mensaje(re);
                }

            },
            error: function (resp) {
                alert("Ocurrio un error enviando los datos al servidor");
            }
        });
    } else {
        mensaje("Debe seleccior el periodo y el n�mero del ciclo");

    }
}

function TomarFotoMes() {

    loading("Espere un momento mientras se toma la foto,<br>este proceso puede tardar entre 40 y 60 minutos", "300", "140");

    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Previsualizar&accion=Extractos&opcion=16",
        success: function (resp) {
            var re = resp.trim();
            $("#dialogLoading").dialog('close');
            if (re === "FOTO_TOMADA") {
                mensaje("Foto de me ha sido tomada con �xito");
            } else if (re === "FOTO_YAFUEGENERADA!") {
                mensaje("La foto de este mes ya fue generada");
            } else {
                mensaje(re);
            }

        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function CopiarSancionesMes() {
    var periodo = $('#periodo').val();
    if (periodo !== '') {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "./controller?estado=Previsualizar&accion=Extractos&opcion=11&periodo=" + periodo,
            success: function (resp) {
                var re = resp.trim();
                if (re === "OK") {
                    mensaje("Sanciones generadas con �xito");
                } else if (re === "Error") {
                    mensaje("Error al generar las sanciones");
                } else {
                    mensaje(re);
                }

            },
            error: function (resp) {
                alert("Ocurrio un error enviando los datos al servidor");
            }
        });
    } else {
        mensaje("Debe seleccior el periodo");

    }
}


var lastsel;
function buscarDetalleSanciones() {
    var periodo = $('#periodo').val();
    var tipo = "";
    var listUnd = "";
    var url = "";

    if (document.getElementById("autofa").checked)
        listUnd += document.getElementById("autofa").value + ";";
    if (document.getElementById("autofb").checked)
        listUnd += document.getElementById("autofb").value + ";";
    if (document.getElementById("consufa").checked)
        listUnd += document.getElementById("consufa").value + ";";
    if (document.getElementById("consufb").checked)
        listUnd += document.getElementById("consufb").value + ";";
    if (document.getElementById("edufa").checked)
        listUnd += document.getElementById("edufa").value + ";";
    if (document.getElementById("edufb").checked)
        listUnd += document.getElementById("edufb").value + ";";
    if (document.getElementById("micro").checked)
        listUnd += document.getElementById("micro").value + ";";

    var ediurl = './controller?estado=Previsualizar&accion=Extractos&opcion=13';

    if (document.getElementById("sanc").checked) {
        tipo = "1";
        url = './controller?estado=Previsualizar&accion=Extractos&opcion=12&periodo=' + periodo + '&tipo=' + tipo + '&listUnd=' + listUnd;
        cargarDetalleSancion(url, ediurl);
    } else {
        tipo = "2";
        url = './controller?estado=Previsualizar&accion=Extractos&opcion=15&periodo=' + periodo + '&tipo=' + tipo + '&listUnd=' + listUnd;
        buscarDetalleCondonaciones(url, ediurl);
    }
}

function cargarDetalleSancion(url, ediurl) {
    $('#tb_condonacion').fadeOut();
    $('#tb_detalle').fadeIn();
    $('#bt_buscar').show();
    if ($("#gview_DetalleSanciones").length) {
        refrescarGridDetalleSanciones(url);
    } else {
        jQuery("#DetalleSanciones").jqGrid({
            caption: 'Detalle Sanciones',
            url: url,
            editurl: ediurl,
            datatype: 'json',
            height: 450,
            width: 1200,
            colNames: ['', 'Unidad Negocio', 'Id Concepto', 'Concepto Recaudo', 'Categoria', 'Descripcion Sancion', 'Rango Inicial', 'Rango Final', 'Porcentaje'],
            colModel: [
                {name: 'id_sancion', index: 'id_sancion', hidden: true, key: true},
                {name: 'und_negocio', index: 'und_negocio', sortable: false, width: '400px', align: 'center'},
                {name: 'id_concepto_recaudo', index: 'id_concepto_recaudo', sortable: false, width: '300px', align: 'center'},
                {name: 'concepto_recaudo', index: 'concepto_recaudo', sortable: false, width: '600px', align: 'center'},
                {name: 'categoria', index: 'categoria', sortable: false, width: '300px', align: 'center'},
                {name: 'descripcion_sancion', index: 'descripcion_sancion', sortable: false, width: '600px', align: 'center'},
                {name: 'rango_ini', index: 'rango_ini', sortable: false, width: '400px', align: 'center', editable: true},
                {name: 'rango_fin', index: 'rango_fin', sortable: false, width: '400px', align: 'center', editable: true},
                {name: 'porcentaje', index: 'porcentaje', sortable: false, width: '400px', align: 'center', editable: true}
            ],
            rowNum: 10000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            onSelectRow: function (rowid) {
                var fila = jQuery("#DetalleSanciones").getRowData(rowid).porcentaje;
                var cm = null;
                if (fila === "CALCULADO") {
                    cm = jQuery('#DetalleSanciones').jqGrid('getColProp', 'porcentaje');
                    cm.editable = false;
                } else {
                    cm = jQuery('#DetalleSanciones').jqGrid('getColProp', 'porcentaje');
                    cm.editable = true;
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                jQuery('#DetalleSanciones').jqGrid('restoreRow', lastsel);
                jQuery('#DetalleSanciones').jqGrid('editRow', rowid, true);
                lastsel = rowid;

            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });

        jQuery("#DetalleSanciones").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    }
}

function refrescarGridDetalleSanciones(url) {
    jQuery("#DetalleSanciones").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#DetalleSanciones").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    jQuery('#DetalleSanciones').trigger("reloadGrid");
}

function refrescarGridDetalleCondonaciones(url) {
    jQuery("#DetalleCondonaciones").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#DetalleCondonaciones").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    jQuery('#DetalleCondonaciones').trigger("reloadGrid");
}

function generarSancionesMes() {
    var fullData = null;
    var periodo = $('#periodo').val();
    var tipo = "";
    if (document.getElementById("sanc").checked) {
        tipo = "1";
        fullData = jQuery("#DetalleSanciones").jqGrid('getRowData');
    } else {
        tipo = "2";
        fullData = jQuery("#DetalleCondonaciones").jqGrid('getRowData');
    }

    var myJsonString = JSON.stringify(fullData);

    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Previsualizar&accion=Extractos",
        data: {
            listado: myJsonString,
            opcion: 14,
            periodo: periodo,
            tipo: tipo
        },
        success: function (resp) {
            var re = resp.trim();
            if (re === "OK") {
                mensaje("Sanciones generadas con �xito");
            } else if (re === "Error") {
                mensaje("Error al generar las sanciones");
            } else {
                mensaje(re);
            }
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function buscarDetalleCondonaciones(url, ediurl) {
    $('#tb_detalle').fadeOut();
    $('#tb_condonacion').fadeIn();
    $('#bt_buscar').show();
    if ($("#gview_DetalleCondonaciones").length) {
        refrescarGridDetalleCondonaciones(url);
    } else {
        jQuery("#DetalleCondonaciones").jqGrid({
            caption: 'Detalle Condonaciones',
            url: url,
            editurl: ediurl,
            datatype: 'json',
            height: 450,
            width: 1500,
            colNames: ['', 'Unidad Negocio', 'Id Concepto', 'Concepto Recaudo', '', 'Descripcion Sancion', '', 'Aplicado A', 'Rango Inicial', 'Rango Final', 'Porcentaje'],
            colModel: [
                {name: 'id_sancion', index: 'id_sancion', hidden: true, key: true},
                {name: 'und_negocio', index: 'und_negocio', sortable: false, width: '400px', align: 'center'},
                {name: 'id_concepto_recaudo', index: 'id_concepto_recaudo', sortable: false, width: '300px', align: 'center'},
                {name: 'concepto_recaudo', index: 'concepto_recaudo', sortable: false, width: '700px', align: 'left'},
                {name: 'categoria', index: 'categoria', hidden: true, sortable: false, width: '300px', align: 'center'},
                {name: 'descripcion_sancion', index: 'descripcion_sancion', sortable: false, width: '900px', align: 'left'},
                {name: 'id_aplicado', index: 'id_aplicado', sortable: false, hidden: true, width: '300px', align: 'center'},
                {name: 'aplicado_a', index: 'aplicado_a', sortable: false, width: '600px', align: 'left'},
                {name: 'rango_ini', index: 'rango_ini', sortable: false, width: '300px', align: 'center', editable: true},
                {name: 'rango_fin', index: 'rango_fin', sortable: false, width: '300px', align: 'center', editable: true},
                {name: 'porcentaje', index: 'porcentaje', sortable: false, width: '300px', align: 'center', editable: true}
            ],
            rowNum: 10000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                jQuery('#DetalleCondonaciones').jqGrid('restoreRow', lastsel);
                jQuery('#DetalleCondonaciones').jqGrid('editRow', rowid, true);
                lastsel = rowid;

            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        });

        jQuery("#DetalleCondonaciones").jqGrid('gridResize', {minWidth: 1000, minHeight: 500});
    }
}

function GenerarCuotaManejo() {
    loading("Espere un momento mientras se genera la cuota de manejo.", "270", "140");
    var periodo = $('#periodo').val();
    var ciclo = $('#ciclo').val();
    
    if (periodo !== '' || ciclo !== '') {
   
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "./controller?estado=Previsualizar&accion=Extractos&opcion=17&periodo="+periodo+"&ciclo="+ciclo,
            success: function (resp) {
                setTimeout(function () {
                    var re = resp.trim();
                    $("#dialogLoading").dialog('close');
                    if (re === "OK") {
                        mensaje("Generaci�n de Cuota de Manejo exitosa");
                    } else if (re === "Error") {
                        mensaje("Error al generar Cuota de Manejo");
                    } else {
                        mensaje(re);
                    }

                }, 5000);
            },
            error: function (resp) {
                alert("Ocurrio un error enviando los datos al servidor");
            }
        });

        
     } else {
        mensaje("Debe seleccior el periodo y el n�mero del ciclo");

    }
}


function GenerarCATyMImes() {
    loading("Espere un momento mientras se genera el cat y mi.", "270", "140");
    var periodo = $('#periodo').val();
    var ciclo = $('#ciclo').val();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Previsualizar&accion=Extractos&opcion=18&periodo=" + periodo + "&ciclo=" + ciclo,
        success: function (resp) {
            setTimeout(function () {
                var re = resp.trim();
                $("#dialogLoading").dialog('close');
                if (re === "OK") {
                    mensaje("Generaci�n de MI y Cat exitoso");
                } else if (re === "Error") {
                    mensaje("Error al generar MI y Cat");
                } else {
                    mensaje(re);
                }

            }, 5000);
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}