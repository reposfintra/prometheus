window.onload = () => {
    document.getElementById("btn_aseguradora").addEventListener("click", () => {
        cargarAseguradoras();
    });
    document.getElementById("btn_polizas").addEventListener("click", () => {
        cargarPolizas()
    });
    document.getElementById("btn_tipo_cobro").addEventListener("click", () => {
        cargarTipoCobro();
    });
    document.getElementById("btn_valor_poliza").addEventListener("click", () => {
        cargarValorPoliza();
    });
    document.getElementById("tipo_tipo_cobro").addEventListener("change", (e) => {
        console.log(e.currentTarget.value);
        if (e.currentTarget.value === "A") {
            document.getElementById("financiacion_tipo_cobro").disabled = "true";
        } else {
            document.getElementById("financiacion_tipo_cobro").disabled = "";
        }
    });
    document.getElementById("tipo_valor_poliza").addEventListener("change", (e) => {
        alternarCamposTipoValorPoliza(e.currentTarget.value);
    });

    let nit = document.getElementById("nit_aseguradora");
    let retorno = document.getElementById("retorno_aseguradora");
    let prr = document.getElementById("prr_aseguradora");
    let plazo_pago = document.getElementById("plazo_pago_aseguradora");
    let porcentaje = document.getElementById("valor_porcentaje_valor_poliza");
    let vAbsoluto = document.getElementById("valor_absoluto_valor_poliza");

    nit.addEventListener("change", (e) => {
        if (esNumero(e.target.value)) {
            let v = obtenerNumeroDeUnTexto(e.target.value);

            if (esNegativo(v)) {
                mostrarMensajeNumeroNegativo();
                nit.value = "";
            } else if (v < 100000000) {
                mensajesDelSistema("El NIT debe tener m�nimo 10 d�gitos.");
                nit.value = "";
            } else {
                nit.value += calcularDigitoVerificacion(e.target.value);
            }
        } else {
            nit.value = "";
            mostrarMensajeFormatoIncorrecto();
        }
    });

    retorno.addEventListener("change", (e) => {
        if (esNumero(e.target.value)) {
            let v = obtenerNumeroDeUnTexto(e.target.value);

            if (esNegativo(v)) {
                mostrarMensajeNumeroNegativo();
                retorno.value = "";
            } else if (v > 100) {
                mensajesDelSistema("El retorno no debe ser mayor a 100.");
                retorno.value = "";
            }
        } else {
            retorno.value = "";
            mostrarMensajeFormatoIncorrecto();
        }
    });

    plazo_pago.addEventListener("change", (e) => {
        if (esNumero(e.target.value)) {
            let v = obtenerNumeroDeUnTexto(e.target.value);

            if (esNegativo(v)) {
                mostrarMensajeNumeroNegativo();
                plazo_pago.value = "";
            } else if (v > 30) {
                mensajesDelSistema("El retorno no debe ser mayor a 30.");
                plazo_pago.value = "";
            }
        } else {
            plazo_pago.value = "";
            mostrarMensajeFormatoIncorrecto();
        }
    });

    prr.addEventListener("change", (e) => {
        if (esNumero(e.target.value)) {
            let v = obtenerNumeroDeUnTexto(e.target.value);

            if (esNegativo(v)) {
                mostrarMensajeNumeroNegativo();
                prr.value = "";
            } else if (v > 30) {
                mensajesDelSistema("El retorno no debe ser mayor a 30.");
                prr.value = "";
            }
        } else {
            prr.value = "";
            mostrarMensajeFormatoIncorrecto();
        }
    });

    vAbsoluto.addEventListener("change", (e) => {
        if (esNumero(e.target.value)) {
            let v = obtenerNumeroDeUnTexto(e.target.value);

            if (esNegativo(v) || v === null) {
                vAbsoluto.value = "";
                mostrarMensajeNumeroNegativo();
            }
        } else {
            vAbsoluto.value = "";
            mostrarMensajeFormatoIncorrecto();
        }
    });

    porcentaje.addEventListener("change", (e) => {
        if (esNumero(e.target.value)) {
            let v = obtenerNumeroDeUnTexto(e.target.value);

            if (esNegativo(v)) {
                porcentaje.value = "";
                mostrarMensajeNumeroNegativo();
            }
        } else {
            porcentaje.value = "";
            mostrarMensajeFormatoIncorrecto();
        }
    });
}

function obtenerNumeroDeUnTexto(string) {
    let matches = string.match(/^-?[0-9]+(\.\d+)?$/g);

    console.log(matches);
    return matches[0];
}

function aplicarFormatoMoneda(element) {
    element.value = element.value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
}

function obtenerNumeroDeFormatoMoneda(value) {
    return Number.parseFloat(value.replace(".", "").replace(",", "."));
}

function esNumero(string) {
    return /^-?[0-9]+(\.\d+)?$/g.test(string);
}

function esNegativo(value) {
    return value < 0;
}

function alternarCamposTipoValorPoliza(value) {
    if (value === "A") {
        document.getElementById("fila-valor-absoluto").style.display = "table-row";
        document.getElementById("fila-valores").style.display = "none";
        document.getElementById("fila-porcentaje").style.display = "none";
    } else {
        document.getElementById("fila-valor-absoluto").style.display = "none";
        document.getElementById("fila-valores").style.display = "table-row";
        document.getElementById("fila-porcentaje").style.display = "table-row";
    }
}

function alternarDisplayTablas(element) {
    document.getElementById(element).style.display = 'block';
    let wrappers = ['aseguradoras-wrapper', 'polizas-wrapper', 'tipo-cobro-wrapper', 'valor-poliza-wrapper'];
    wrappers.forEach((e) => {
        if (e !== element) {
            document.getElementById(e).style.display = 'none';
        }
    });
}

function obtenerEstado(estado) {
    return estado === "" || estado === " " ? "A" : "";
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function cargarPolizas() {
    alternarDisplayTablas("polizas-wrapper");

    let operacion;
    let grid_tabla_ = jQuery("#tabla_Polizas");
    if ($("#gview_tabla_Polizas").length) {
        reloadGridMostrar(grid_tabla_, 116);
    } else {
        grid_tabla_.jqGrid({
            caption: "Polizas",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: 'auto',
            colNames: ['Id', 'Nombre Poliza', 'Estado', 'Activar/Inactivar'],
            colModel: [
                { name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true },
                { name: 'descripcion', index: 'descripcion', width: 270, sortable: true, align: 'left', hidden: false, search: true },
                { name: 'estado', index: 'estado', width: 100, sortable: false, align: 'left', hidden: true },
                { name: 'cambio', index: 'cambio', width: 100, sortable: false, align: 'left', hidden: false }
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager_Polizas',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 116
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {

            },
            gridComplete: function (index) {
                let cant = jQuery("#tabla_Polizas").jqGrid('getDataIDs');
                for (let i = 0; i < cant.length; i++) {
                    let cambioEstado = $("#tabla_Polizas").getRowData(cant[i]).cambio;
                    let cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoPoliza('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_Polizas").jqGrid('setRowData', cant[i], { cambio: be });
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                let myGrid = jQuery("#tabla_Polizas"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                let id = filas.id;
                let estado = filas.estado;
                let descripcion = filas.descripcion;
                if (estado === " ") {
                    ventanaPolizas(operacion, id, descripcion);
                } else if (estado === "A") {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager_Polizas", { add: false, edit: false, del: false, search: false, refresh: false }, {
        });
        $("#tabla_Polizas").navButtonAdd('#pager_Polizas', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaPolizas(operacion);
            }
        });
    }
}

function ventanaPolizas(operacion, id, descripcion) {
    if (operacion === 'Nuevo') {
        $("#dialogPolizas").dialog({
            width: 'auto',
            height: '150',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'POLIZAS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    insertarPoliza();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id_poliza").val("");
                    $("#descripcion_poliza").val("");
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id_poliza").val(id);
        $("#descripcion_poliza").val(descripcion);

        $("#dialogPolizas").dialog({
            width: 'auto',
            height: '150',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'POLIZAS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarPoliza();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id_poliza").val("");
                    $("#descripcion_poliza").val("");
                }
            }
        });
    }
}

function insertarPoliza() {
    let descripcion = $("#descripcion_poliza").val();
    let aseguradora = $("#aseguradora_poliza").val();

    if (descripcion !== "" && aseguradora !== "") {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 117,
                descripcion: descripcion
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === "Guardado") {
                    mostrarMensajeGuardado();

                    $("#dialogPolizas").dialog('close');
                    $("#id_poliza").val("");
                    $("#descripcion_poliza").val("");

                    cargarPolizas();
                }
            }, error: function (result) {
                mostrarMensajeErrorServidor();
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }
}

function ActualizarPoliza() {
    let id = $("#id_poliza").val();
    let descripcion = $("#descripcion_poliza").val();

    if (descripcion !== "") {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 118,
                id: id,
                descripcion: descripcion
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === "Guardado") {
                    mostrarMensajeGuardado();

                    $("#dialogPolizas").dialog('close');
                    $("#id_poliza").val("");
                    $("#descripcion_poliza").val("");

                    cargarPolizas();
                }
            }, error: function (result) {
                mostrarMensajeErrorServidor();
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function CambiarEstadoPoliza(rowid) {
    let grid_tabla = jQuery("#tabla_Polizas");
    let id = grid_tabla.getRowData(rowid).id;
    let estado = grid_tabla.getRowData(rowid).estado;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 119,
            id: id,
            status: obtenerEstado(estado)
        },
        success: function (data) {
            cargarPolizas();
        }, error: function (result) {
            mostrarMensajeErrorServidor();
        }
    });
}


function cargarAseguradoras() {
    alternarDisplayTablas("aseguradoras-wrapper");

    let operacion;
    let grid_tabla_ = jQuery("#tabla_aseguradoras");
    if ($("#gview_tabla_aseguradoras").length) {
        reloadGridMostrar(grid_tabla_, 120);
    } else {
        grid_tabla_.jqGrid({
            caption: "Aseguradoras",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: 'auto',
            colNames: ['Id', 'Descripcion', 'NIT', 'Retorno', 'Plazo pago', 'Plazo Recuperacion Retorno', 'Estado', 'Activar/Inactivar'],
            colModel: [
                { name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true },
                { name: 'descripcion', index: 'descripcion', width: 270, sortable: true, align: 'left', hidden: false, search: true },
                { name: 'nit', index: 'nit', width: 150, sortable: true, align: 'center', hidden: false, search: true },
                { name: 'retorno', index: 'retorno', width: 100, sortable: true, align: 'center', hidden: false },
                { name: 'plazo_pago', index: 'plazo_pago', width: 100, sortable: true, align: 'center', hidden: false },
                { name: 'prr', index: 'prr', width: 150, sortable: true, align: 'center', hidden: false },
                { name: 'estado', index: 'estado', width: 100, sortable: false, align: 'left', hidden: true },
                { name: 'cambio', index: 'cambio', width: 100, sortable: false, align: 'left', hidden: false }
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager_aseguradoras',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 120
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            gridComplete: function (index) {
                let cant = jQuery("#tabla_aseguradoras").jqGrid('getDataIDs');
                for (let i = 0; i < cant.length; i++) {
                    let cambioEstado = $("#tabla_aseguradoras").getRowData(cant[i]).cambio;
                    let cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoAseguradoras('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_aseguradoras").jqGrid('setRowData', cant[i], { cambio: be });
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                let myGrid = jQuery("#tabla_aseguradoras"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                let id = filas.id;
                let estado = filas.estado;
                console.log("+" + estado + "+");
                let retorno = filas.retorno;
                let nit = filas.nit;
                let plazo_pago = filas.plazo_pago;
                let prr = filas.prr;
                let descripcion = filas.descripcion;
                if (estado === " ") {
                    ventanaAseguradoras(operacion, id, descripcion, nit, retorno, plazo_pago, prr);
                } else if (estado === "A") {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager_aseguradoras", { add: false, edit: false, del: false, search: false, refresh: false }, {
        });
        $("#tabla_aseguradoras").navButtonAdd('#pager_aseguradoras', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaAseguradoras(operacion);
            }
        });
    }
}

function ventanaAseguradoras(operacion, id, descripcion, nit, retorno, plazo_pago, prr) {
    if (operacion === 'Nuevo') {
        $("#dialogAseguradoras").dialog({
            width: '400',
            height: '250',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'ASEGURADORAS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    insertarAseguradoras();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id_aseguradora").val("");
                    $("#descripcion_aseguradora").val("");
                    $("#nit_aseguradora").val("");
                    $("#retorno_aseguradora").val("");
                    $("#plazo_pago_aseguradora").val("");
                    $("#prr_aseguradora").val("");
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id_aseguradora").val(id);
        $("#descripcion_aseguradora").val(descripcion);
        $("#nit_aseguradora").val(nit);
        document.getElementById("nit_aseguradora").disabled="true";
        $("#retorno_aseguradora").val(retorno);
        $("#plazo_pago_aseguradora").val(plazo_pago);
        $("#prr_aseguradora").val(prr);

        $("#dialogAseguradoras").dialog({
            width: '400',
            height: '250',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'ASEGURADORAS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarAseguradoras();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id_aseguradora").val("");
                    $("#descripcion_aseguradora").val("");
                    $("#nit_aseguradora").val("");
                    document.getElementById("nit_aseguradora").disabled="";
                    $("#retorno_aseguradora").val("");
                    $("#plazo_pago_aseguradora").val("");
                    $("#prr_aseguradora").val("");
                }
            }
        });
    }
}

function insertarAseguradoras() {
    let descripcion = $("#descripcion_aseguradora").val();
    let nit = $("#nit_aseguradora").val();
    let retorno = $("#retorno_aseguradora").val();
    let plazo_pago = $("#plazo_pago_aseguradora").val();
    let prr = $("#prr_aseguradora").val();

    if (nit !== "" && descripcion !== "" && retorno !== "" && plazo_pago !== "" && prr !== "") {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 121,
                descripcion: descripcion,
                nit: nit,
                retorno: retorno,
                plazo_pago: plazo_pago,
                prr: prr
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === "Guardado") {
                    mostrarMensajeGuardado();

                    $("#dialogAseguradoras").dialog('close');
                    $("#id_aseguradora").val("");
                    $("#descripcion_aseguradora").val("");
                    $("#nit_aseguradora").val("");
                    $("#retorno_aseguradora").val("");
                    $("#plazo_pago_aseguradora").val("");
                    $("#prr_aseguradora").val("");

                    cargarAseguradoras();
                }
            }, error: function (result) {
                mostrarMensajeErrorServidor();
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }

}

function ActualizarAseguradoras() {
    let id_aseguradora = $("#id_aseguradora").val();
    let descripcioN = $("#descripcion_aseguradora").val();
    let retorno = $("#retorno_aseguradora").val();
    let plazo_pago = $("#plazo_pago_aseguradora").val();
    let prr = $("#prr_aseguradora").val();

    if (descripcioN !== "" && retorno !== "" && plazo_pago !== "" && prr !== "") {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 122,
                id: id_aseguradora,
                descripcion: descripcioN,
                retorno: retorno,
                plazo_pago: plazo_pago,
                prr: prr
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === "Guardado") {
                    mostrarMensajeGuardado();

                    $("#dialogAseguradoras").dialog('close');
                    $("#id_aseguradora").val("");
                    $("#descripcion_aseguradora").val("");
                    $("#nit_aseguradora").val("");
                    $("#retorno_aseguradora").val("");
                    $("#plazo_pago_aseguradora").val("");
                    $("#prr_aseguradora").val("");

                    cargarAseguradoras();
                }
            }, error: function (result) {
                mostrarMensajeErrorServidor();
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }
}

function CambiarEstadoAseguradoras(rowid) {
    let grid_tabla = jQuery("#tabla_aseguradoras");
    let id = grid_tabla.getRowData(rowid).id;
    let estado = grid_tabla.getRowData(rowid).estado;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 123,
            id: id,
            status: obtenerEstado(estado)
        },
        success: function (data) {
            cargarAseguradoras();
        }, error: function (result) {
            mostrarMensajeErrorServidor();
        }
    });
}


function cargarTipoCobro() {
    alternarDisplayTablas("tipo-cobro-wrapper");

    let operacion;
    let grid_tabla_ = jQuery("#tabla_tipo_cobro");
    if ($("#gview_tabla_tipo_cobro").length) {
        reloadGridMostrar(grid_tabla_, 124);
    } else {
        grid_tabla_.jqGrid({
            caption: "Tipo cobro",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: 'auto',
            colNames: ['Id', 'Descripcion', 'Tipo', 'Financiacion', 'Estado', 'Activar/Inactivar'],
            colModel: [
                { name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true },
                { name: 'descripcion', index: 'descripcion', width: 270, sortable: true, align: 'left', hidden: false, search: true },
                { name: 'tipo', index: 'tipo', width: 100, sortable: true, align: 'center', hidden: false, formatter: legendFmatter },
                { name: 'financiacion', index: 'financiacion', width: 100, sortable: true, align: 'center', hidden: false, formatter: legendFmatter },
                { name: 'estado', index: 'estado', width: 100, sortable: false, align: 'left', hidden: true },
                { name: 'cambio', index: 'cambio', width: 100, sortable: false, align: 'left', hidden: false }
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager_tipo_cobro',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 124
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            gridComplete: function (index) {
                let cant = jQuery("#tabla_tipo_cobro").jqGrid('getDataIDs');
                for (let i = 0; i < cant.length; i++) {
                    let cambioEstado = $("#tabla_tipo_cobro").getRowData(cant[i]).cambio;
                    let cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoTipoCobro('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_tipo_cobro").jqGrid('setRowData', cant[i], { cambio: be });
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                let myGrid = jQuery("#tabla_tipo_cobro"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                let id = filas.id;
                let estado = filas.estado;
                let tipo = filas.tipo;
                let descripcion = filas.descripcion;
                let financiacion = filas.financiacion;
                if (estado === " ") {
                    ventanaTipoCobro(operacion, id, descripcion, tipo, financiacion);
                } else if (estado === "A") {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager_tipo_cobro", { add: false, edit: false, del: false, search: false, refresh: false }, {
        });
        $("#tabla_tipo_cobro").navButtonAdd('#pager_tipo_cobro', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaTipoCobro(operacion);
            }
        });
    }
}

function ventanaTipoCobro(operacion, id, descripcion, tipo, financiacion) {
    if (operacion === 'Nuevo') {
        $("#dialogTipoCobro").dialog({
            width: '300',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'TIPO COBRO',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    insertarTipoCobro();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id_tipo_cobro").val("");
                    $("#tipo_tipo_cobro").val("");
                    $("#financiacion_tipo_cobro").val("");
                    $("#descripcion").val("");
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id_tipo_cobro").val(id);
        $("#tipo_tipo_cobro").val(tipo);
        $("#financiacion_tipo_cobro").val(financiacion);
        $("#descripcion_tipo_cobro").val(descripcion);

        if (tipo === "C") {
            document.getElementById("financiacion_tipo_cobro").disabled = false;
        } else {
            document.getElementById("financiacion_tipo_cobro").disabled = true;
        }

        $("#dialogTipoCobro").dialog({
            width: '300',
            height: '200',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'TIPO COBRO',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarTipoCobro();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id_tipo_cobro").val("");
                    $("#tipo_tipo_cobro").val("");
                    $("#financiacion_tipo_cobro").val("");
                    $("#descripcion_tipo_cobro").val("");
                }
            }
        });
    }
}

function insertarTipoCobro() {
    let tipo = $("#tipo_tipo_cobro").val();
    let financiacion = $("#financiacion_tipo_cobro").val();
    let descripcion = $("#descripcion_tipo_cobro").val();

    if (descripcion !== "" && tipo !== "" && financiacion !== "") {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 125,
                tipo: tipo,
                descripcion: descripcion,
                financiacion: financiacion
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === "Guardado") {
                    mostrarMensajeGuardado();

                    $("#dialogTipoCobro").dialog('close');
                    $("#id_tipo_cobro").val("");
                    $("#tipo_tipo_cobro").val("");
                    $("#financiacion_tipo_cobro").val("");
                    $("#descripcion_tipo_cobro").val("");

                    cargarTipoCobro();
                }
            }, error: function (result) {
                mostrarMensajeErrorServidor();
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }
}

function ActualizarTipoCobro() {
    let id = $("#id_tipo_cobro").val();
    let tipo = $("#tipo_tipo_cobro").val();
    let financiacion = $("#financiacion_tipo_cobro").val();
    let descripcion = $("#descripcion_tipo_cobro").val();

    if (descripcion !== "" && tipo !== "" && financiacion !== "") {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 126,
                id: id,
                tipo: tipo,
                descripcion: descripcion,
                financiacion: financiacion
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === "Guardado") {
                    mostrarMensajeGuardado();

                    $("#dialogTipoCobro").dialog('close');
                    $("#id_tipo_cobro").val("");
                    $("#tipo_tipo_cobro").val("");
                    $("#financiacion_tipo_cobro").val("");
                    $("#descripcion_tipo_cobro").val("");

                    cargarTipoCobro();
                }
            }, error: function (result) {
                mostrarMensajeErrorServidor();
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }
}

function CambiarEstadoTipoCobro(rowid) {
    let grid_tabla = jQuery("#tabla_tipo_cobro");
    let id = grid_tabla.getRowData(rowid).id;
    let estado = grid_tabla.getRowData(rowid).estado;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 127,
            id: id,
            status: obtenerEstado(estado)
        },
        success: function (data) {
            cargarTipoCobro();
        }, error: function (result) {
            mostrarMensajeErrorServidor();
        }
    });
}


function cargarValorPoliza() {
    alternarDisplayTablas("valor-poliza-wrapper");

    let operacion;
    let grid_tabla_ = jQuery("#tabla_valor_poliza");
    if ($("#gview_tabla_valor_poliza").length) {
        reloadGridMostrar(grid_tabla_, 128);
    } else {
        grid_tabla_.jqGrid({
            caption: "Valor poliza",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: 'auto',
            colNames: ['Id', 'Descripcion', 'Tipo', 'Valor Sobre', 'Valor Porcentaje', 'Valor Absoluto', 'IVA', 'Estado', 'Activar/Inactivar'],
            colModel: [
                { name: 'id', index: 'id', width: 50, sortable: true, align: 'left', hidden: true, key: true },
                { name: 'descripcion', index: 'descripcion', width: 270, sortable: true, align: 'left', hidden: false, search: true },
                { name: 'tipo', index: 'estado', width: 80, sortable: true, align: 'center', hidden: false, formatter: (cellvalue) => { return cellvalue === "P" ? "Porcentaje" : "Absoluto"; } },
                { name: 'calcular_sobre', index: 'valor calcular_sobre', width: 80, sortable: true, align: 'center', hidden: false, formatter: legendFmatter },
                { name: 'valor_porcentaje', index: 'valor_porcentaje', width: 80, sortable: true, align: 'center', hidden: false, formatter: 'number', formatoptions: {decimalSeparator: ",", decimalPlaces: 4, suffix: "%"}},
                { name: 'valor_absoluto', index: 'valor_absoluto', width: 80, sortable: true, align: 'center', hidden: false, formatter: 'number', formatoptions: { decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ " } },
                { name: 'iva', index: 'iva', width: 100, sortable: false, align: 'left', hidden: true },
                { name: 'estado', index: 'estado', width: 100, sortable: false, align: 'left', hidden: true },
                { name: 'cambio', index: 'cambio', width: 100, sortable: false, align: 'left', hidden: false }
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: false,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            pager: '#pager_valor_poliza',
            multiselect: false,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 128
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            },
            gridComplete: function (index) {
                let cant = jQuery("#tabla_valor_poliza").jqGrid('getDataIDs');
                for (let i = 0; i < cant.length; i++) {
                    let cambioEstado = $("#tabla_valor_poliza").getRowData(cant[i]).cambio;
                    let cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoValorPoliza('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_valor_poliza").jqGrid('setRowData', cant[i], { cambio: be });
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                operacion = 'Editar';
                let myGrid = jQuery("#tabla_valor_poliza"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                let id = filas.id;
                let estado = filas.estado;
                let tipo = filas.tipo;
                let descripcion = filas.descripcion;
                let calcularSobre = filas.calcular_sobre;
                let valorPorcentaje = filas.valor_porcentaje;
                let valorAbsoluto = filas.valor_absoluto;
                let iva = filas.iva;
                if (estado === " ") {
                    ventanaValorPoliza(operacion, id, descripcion, tipo, calcularSobre, valorPorcentaje, valorAbsoluto, iva);
                } else if (estado === "A") {
                    mensajesDelSistema("Debe estar en estado Activo", '230', '150', false);
                }
            }
        }).navGrid("#pager_valor_poliza", { add: false, edit: false, del: false, search: false, refresh: false }, {
        });
        $("#tabla_valor_poliza").navButtonAdd('#pager_valor_poliza', {
            caption: "Nuevo",
            onClickButton: function () {
                operacion = 'Nuevo';
                ventanaValorPoliza(operacion);
            }
        });
    }
}

function ventanaValorPoliza(operacion, id, descripcion, tipo, calcularSobre, valorPorcentaje, valorAbsoluto, iva) {
    if (operacion === 'Nuevo') {
        $("#dialogValorPoliza").dialog({
            width: 'auto',
            height: '250',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'VALOR POLIZA',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Guardar": function () {
                    insertarValorPoliza();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id_valor_poliza").val("");
                    $("#tipo_valor_poliza").val("P");
                    $("#descripcion_valor_poliza").val("");
                    document.getElementById("valor_solicitado_valor_poliza").checked = false;
                    document.getElementById("valor_cartera_valor_poliza").checked = false;
                    document.getElementById("iva_valor_poliza").checked = false;
                    $("#valor_porcentaje_valor_poliza").val("");
                    $("#valor_absoluto_valor_poliza").val("");
                    alternarCamposTipoValorPoliza("P");
                }
            }
        });
    } else if (operacion === 'Editar') {
        $("#id_valor_poliza").val(id);
        $("#tipo_valor_poliza").val(tipo);
        $("#descripcion_valor_poliza").val(descripcion);
        document.getElementById("valor_solicitado_valor_poliza").checked = calcularSobre === "K" ? true : false;
        document.getElementById("valor_cartera_valor_poliza").checked = calcularSobre === "KI" ? true : false;
        document.getElementById("iva_valor_poliza").checked = iva ? true : false;
        $("#valor_porcentaje_valor_poliza").val(valorPorcentaje);
        $("#valor_absoluto_valor_poliza").val(valorAbsoluto);
        alternarCamposTipoValorPoliza(tipo);

        $("#dialogValorPoliza").dialog({
            width: 'auto',
            height: '250',
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closeOnEscape: false,
            title: 'VALOR POLIZAS',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "Actualizar": function () {
                    ActualizarValorPoliza();
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#id_valor_poliza").val("");
                    $("#tipo_valor_poliza").val("P");
                    $("#descripcion_valor_poliza").val("");
                    document.getElementById("valor_solicitado_valor_poliza").checked = false;
                    document.getElementById("valor_cartera_valor_poliza").checked = false;
                    document.getElementById("iva_valor_poliza").checked = false;
                    $("#valor_porcentaje_valor_poliza").val("");
                    $("#valor_absoluto_valor_poliza").val("");
                    alternarCamposTipoValorPoliza("P");
                }
            }
        });
    }
}

function insertarValorPoliza() {
    let id = $("#id_valor_poliza").val();
    let tipo = $("#tipo_valor_poliza").val();
    let descripcion = $("#descripcion_valor_poliza").val();
    let valorAbsoluto = $("#valor_absoluto_valor_poliza").val();
    let valorSolicitado = document.querySelector("#valor_solicitado_valor_poliza");
    let valorCartera = document.querySelector("#valor_cartera_valor_poliza");
    let iva = document.querySelector("#iva_valor_poliza").checked;
    let calcularSobre = "NLL";
    let valorPorcentaje;

    if (tipo === "A") {
        calcularSobre = "NLL";
        valorPorcentaje = 0;
    } else {
        calcularSobre = valorSolicitado.checked ? valorSolicitado.value : valorCartera.value;
        valorPorcentaje = $("#valor_porcentaje_valor_poliza").val();
        valorAbsoluto = 0;
    }

    if (descripcion !== "" && tipo !== "" && ((tipo === "P" && calcularSobre !== "" && valorPorcentaje !== undefined) || (tipo === "A" && valorAbsoluto !== ""))) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 129,
                id: id,
                tipo: tipo,
                descripcion: descripcion,
                calcular_sobre: calcularSobre,
                valor_porcentaje: valorPorcentaje,
                valor_absoluto: valorAbsoluto,
                iva: iva
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === "Guardado") {
                    mostrarMensajeGuardado();

                    $("#dialogValorPoliza").dialog('close');
                    $("#id_valor_poliza").val("");
                    $("#tipo_valor_poliza").val("P");
                    $("#descripcion_valor_poliza").val("");
                    document.getElementById("valor_solicitado_valor_poliza").checked = false;
                    document.getElementById("valor_cartera_valor_poliza").checked = false;
                    document.getElementById("iva_valor_poliza").checked = false;
                    $("#valor_absoluto_valor_poliza").val("");

                    alternarCamposTipoValorPoliza("P");
                    cargarValorPoliza();
                }
            }, error: function (result) {
                mostrarMensajeErrorServidor();
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }
}

function ActualizarValorPoliza() {
    let id = $("#id_valor_poliza").val();
    let tipo = $("#tipo_valor_poliza").val();
    let descripcion = $("#descripcion_valor_poliza").val();
    let valorAbsoluto = $("#valor_absoluto_valor_poliza").val();
    let valorSolicitado = document.querySelector("#valor_solicitado_valor_poliza");
    let valorCartera = document.querySelector("#valor_cartera_valor_poliza");
    let iva = document.querySelector("#iva_valor_poliza").checked;
    let calcularSobre = "NLL";
    let valorPorcentaje;

    if (tipo === "A") {
        calcularSobre = "NLL";
        valorPorcentaje = 0;
    } else {
        calcularSobre = valorSolicitado.checked ? valorSolicitado.value : valorCartera.value;
        valorPorcentaje = $("#valor_porcentaje_valor_poliza").val();
        valorAbsoluto = 0;
    }

    if (descripcion !== "" && tipo !== "" && ((tipo === "P" && calcularSobre !== "" && valorPorcentaje !== undefined) || (tipo === "A" && valorAbsoluto !== ""))) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "./controller?estado=Fintra&accion=Soporte",
            data: {
                opcion: 130,
                id: id,
                tipo: tipo,
                descripcion: descripcion,
                calcular_sobre: calcularSobre,
                valor_porcentaje: valorPorcentaje,
                valor_absoluto: valorAbsoluto,
                iva: iva
            },
            success: function (data, textStatus, jqXHR) {
                if (data.respuesta === "Guardado") {
                    mostrarMensajeGuardado();

                    $("#dialogValorPoliza").dialog('close');
                    $("#id_valor_poliza").val("");
                    $("#tipo_valor_poliza").val("P");
                    $("#descripcion_valor_poliza").val("");
                    document.getElementById("valor_solicitado_valor_poliza").checked = false;
                    document.getElementById("valor_cartera_valor_poliza").checked = false;
                    document.getElementById("iva_valor_poliza").checked = false;
                    $("#valor_absoluto_valor_poliza").val("");

                    alternarCamposTipoValorPoliza("P");
                    cargarValorPoliza();
                }
            }, error: function (result) {
                mostrarMensajeErrorServidor();
            }
        });
    } else {
        mensajesDelSistema("Falta digitar informacion", '230', '150', false);
    }
}

function CambiarEstadoValorPoliza(rowid) {
    let grid_tabla = jQuery("#tabla_valor_poliza");
    let id = grid_tabla.getRowData(rowid).id;
    let estado = grid_tabla.getRowData(rowid).estado;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        data: {
            opcion: 131,
            id: id,
            status: obtenerEstado(estado)
        },
        success: function (data) {
            cargarValorPoliza();
        }, error: function (result) {
            showErrorServer
            Message();
        }
    });
}

function legendFmatter(cellvalue, options, rowObject) {
    switch (cellvalue) {
        case "S":
            return "Si";
        case "N":
        case "NLL":
            return "No";
        case "A":
            return "Anticipado";
        case "C":
            return "Cuota";
        case "K":
            return "Capital";
        case "KI":
            return "Capital e Interes";
    }
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: true,
        closeOnEscape: false,
        buttons: {
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function mostrarMensajeGuardado() {
    mensajesDelSistema("Exito al actualizar", "230", "150", true);
}

function mostrarMensajeErrorServidor() {
    mensajesDelSistema("Error en la comunicaci�n con el servidor", "230", "150", true);
}

function mostrarMensajeNumeroNegativo() {
    mensajesDelSistema("Debe ser un n�mero positivo.", "230", "150", true);
}

function mostrarMensajeFormatoIncorrecto() {
    mensajesDelSistema("Formato incorrecto.", "230", "150", true);
}

// function calcularDigitoVerificacion(numero) {
//     const MODULO = 11;
//     let arr = numero.split("");
//     let factor = 2, suma = 0, resto;

//     for (let i = arr.length - 1; i >= 0; i--) {
//         suma += Number.parseInt(arr[i]) * factor;
//         if (factor != 7) {
//             factor++;
//         } else {
//             factor = 2;
//         }
//     }
//     console.log(suma);
//     resto = suma % 11;
//     console.log(resto);
//     return MODULO - resto;
// }

function calcularDigitoVerificacion(nit) {
    var vpri, x, y, z, i, dv1;

    vpri = new Array(16);
    x = 0; y = 0; z = nit.length;
    vpri[1] = 3;
    vpri[2] = 7;
    vpri[3] = 13;
    vpri[4] = 17;
    vpri[5] = 19;
    vpri[6] = 23;
    vpri[7] = 29;
    vpri[8] = 37;
    vpri[9] = 41;
    vpri[10] = 43;
    vpri[11] = 47;
    vpri[12] = 53;
    vpri[13] = 59;
    vpri[14] = 67;
    vpri[15] = 71;
    for (i = 0; i < z; i++) {
        y = (nit.substr(i, 1));
        x += (y * vpri[z - i]);
    }
    y = x % 11
    if (y > 1) {
        dv1 = 11 - y;
    } else {
        dv1 = y;
    }
    return dv1;
}