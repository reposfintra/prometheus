// JavaScript Document
function calcVals(){
	var por_a = 0.0;
	var por_i = 0.0;
	var por_u = 0.0;
	var a = 0.0;
	var i = 0.0;
	var u = 0.0;
	var mats;
	var mx;
	var ots;
	if(document.getElementById("materiales").value!=null) mats = new Number(document.getElementById("materiales").value);
	if(document.getElementById("mano").value!=null) mx = new Number(document.getElementById("mano").value);
	if(document.getElementById("otros").value!=null) ots = new Number(document.getElementById("otros").value);
	var subt = mats.valueOf() + mx.valueOf() + ots.valueOf();
	por_a = (document.getElementById("porc_a").value)/100;
	por_i = (document.getElementById("porc_i").value)/100;
	por_u = (document.getElementById("porc_u").value)/100;
	a=subt*por_a;
	i=subt*por_i;
	u=subt*por_u;
	document.getElementById("val_a2").value = a;
	document.getElementById("val_i2").value = i;
	document.getElementById("val_u2").value = u;	
	document.getElementById("val_a").value = formatNumber(a);
	document.getElementById("val_i").value = formatNumber(i);
	document.getElementById("val_u").value = formatNumber(u);
	var subtotal = subt + (a+i+u);
	var iva = u * 0.19;
	var total = subtotal + iva;
	document.getElementById("subt").value = formatNumber(subtotal);
	document.getElementById("iva").value = formatNumber(iva);
	document.getElementById("total").value = formatNumber(total);
	document.getElementById("subt2").value = subtotal;
	document.getElementById("iva2").value = iva;
	document.getElementById("total2").value = total;
}
