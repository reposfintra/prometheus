/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    funciones = funcionFomurarios();
    $("#fecha_inicio").val('');
    $("#fecha_fin").val('');
    $("#fecha_inicio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha_fin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    var myDate = new Date();
    //$("#fecha_inicio").datepicker("setDate", myDate);
    //$("#fecha_fin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

    cargarAsesores('asesor');

    $('#buscar').click(function () {
        var fecha_inicio = $("#fecha_inicio").val();
        var fecha_fin = $("#fecha_fin").val();
        var requeridos_ = funciones.requeridos();
        var campos = funciones.camposRequeridos();
        if (requeridos_ > 0) {
            mensajesDelSistema('Falta colocar la informacion del campo: ' + campos, '300', 'auto', false);
        } else {
            if (fecha_inicio > fecha_fin) {
                mensajesDelSistema('La fecha de inicio no puede ser mayor que la fecha de fin ', '370', 'auto', false);
            } else {
                cargarNegocios();
            }
        }
    });

    $('#limpiar').click(function () {
        $("#fecha_inicio").val('');
        $("#fecha_fin").val('');
    });
});

function autocompletarAsesor() {
    $("#asesor").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Admin&accion=Fintra",
                dataType: "json",
                data: {
                    asesor: request.term,
                    opcion: 77
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.value
                        };
                    }));
                }
            });
        },
        minLength: 1,
        delay: 500,
        disabled: false,
        select: function (event, ui) {
            $("#asesor").val(ui.item.value);
            console.log(ui.item ?
                    "Selected: " + ui.item.value :
                    "Nothing selected, input was " + ui.item.label);
        },
        open: function () {
            //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function () {

            // $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}


function cargarNegocios() {
    var grid_tabla_ = jQuery("#tabla_negocios");
    if ($("#gview_tabla_negocios").length) {
        reloadGridMostrar(grid_tabla_, 78);
    } else {
        grid_tabla_.jqGrid({
            caption: "Negocios",
            url: "./controller?estado=Admin&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '495',
            whidth: '800',
            //autowidth: false,
            colNames: ['Asesor', 'Negocio', 'Altura de Mora', 'Cedula', 'Nombre', 'Departamento', 'Ciudad', 'Barrio','Departamento Negocio', 'Ciudad Negocio', 'Barrio Negocio', 'Cliente', 'Fecha', 'Reasiganado a'],
            colModel: [
                {name: 'asesor', index: 'asesor', width: 120, align: 'center', sortable: true},
                {name: 'cod_neg', index: 'cod_neg', width: 120, align: 'center', hidden: false, sortable: true},
                {name: 'altura_mora', index: 'altura_mora', width: 120, align: 'left', hidden: false, sortable: true},
                {name: 'identificacion', index: 'identificacion', width: 120, align: 'center', sortable: true},
                {name: 'nombre', index: 'nombre', width: 150, align: 'center', sortable: true},
                {name: 'departamento', index: 'departamento', width: 120, align: 'center', sortable: true},
                {name: 'ciudad', index: 'ciudad', width: 120, align: 'center', sortable: true},
                {name: 'barrio', index: 'barrio', width: 120, align: 'center', sortable: true},
                {name: 'departamento_negocio', index: 'departamento_negocio', width: 120, align: 'center', sortable: true},
                {name: 'ciudad_negocio', index: 'ciudad_negocio', width: 120, align: 'center', sortable: true},
                {name: 'barrio_negocio', index: 'barrio_negocio', width: 120, align: 'center', sortable: true},
                {name: 'cod_cli', index: 'cod_cli', width: 120, align: 'center', hidden: true, sortable: true},
                {name: 'fecha', index: 'fecha', width: 120, align: 'center', hidden: false, sortable: true},
                {name: 'responsable_cuenta', index: 'responsable_cuenta', width: 150, align: 'center', hidden: false, sortable: true}
            ],
            rownumbers: true,
            rownumWidth: 25,
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: true,
            footerrow: false,
            pager: '#pager',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
            cmTemplate: {sortable: false},
            jsonReader: {
                root: "json",
                repeatitems: false,
                id: "0"
            }, ajaxGridOptions: {
                dataType: "json",
                type: "POST",
                async: false,
                data: {
                    opcion: 78,
                    asesor: $('#asesor').val(),
                    fecha_inicio: $('#fecha_inicio').val(),
                    fecha_fin: $('#fecha_fin').val(),
                    estadoCartera: $('#estado_cuenta').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }, loadComplete: function (id, rowid) {
                var info = grid_tabla_.getGridParam('records');
                if (info === 0) {
                    mensajesDelSistema('No hay resultados en la busqueda', '300', 'auto', false);
                }
            },
            gridComplete: function (index) {
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
        jQuery("#tabla_negocios").navButtonAdd('#pager', {
            caption: "Asignar",
            title: "asignar ",
            onClickButton: function () {
                var myGrid = jQuery("#tabla_negocios"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                if (filas === false) {
                    mensajesDelSistema('No ha seleccionado ningun negocio', '300', 'auto', false);
                } else {
                    ventana();
                }
            }
        });
        jQuery("#tabla_negocios").navButtonAdd('#pager', {
            caption: "Exportar Excel",
            title: "Exportar Excel ",
            onClickButton: function () {
                exportarExcelReporte();
            }
        });
    }
}

function reloadGridMostrar(grid_tabla, opcion) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Admin&accion=Fintra",
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                asesor: $('#asesor').val(),
                fecha_inicio: $('#fecha_inicio').val(),
                fecha_fin: $('#fecha_fin').val(),
                estadoCartera: $('#estado_cuenta').val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height) {
    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Mensaje de sistema',
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });
}

function ventana() {
    cargarAsesores('asesores');
    $("#dialogMsjAddAsesores").dialog({
        width: '400',
        height: '200',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'Asesores',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Aceptar": function () {
                actualizarNegocios();
            },
            "Salir": function () {
                $(this).dialog("close");
            }
        }
    });
}

function cargarAsesores(id) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Admin&accion=Fintra",
        dataType: "json",
        async: false,
        data: {
            opcion: 79,
            id_combo: id,
            producto: '4'
        },
        success: function (json) {
            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {

                if (id === 'asesor') {
                    $('#asesor').html('');
                    $('#asesor').append('<option value="" ></option>');
                    for (var datos in json) {
                        $('#asesor').append('<option value="' + datos + '">' + json[datos] + '</option>');
                    }
                } else {
                    $('#asesores').html('');
                    $('#asesores').append('<option value="" ></option>');
                    for (var datos in json) {
                        $('#asesores').append('<option value="' + datos + '">' + json[datos] + '</option>');
                    }
                }

            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function actualizarNegocios() {
    //var negocios = $("#tabla_negocios").jqGrid('getRowData');
    ultilidad = grilla();
    var negocios = ultilidad.selecciondados('tabla_negocios');
    console.log(JSON.stringify(negocios));
    var asesores = $('#asesores').val();
    if (asesores == '' && negocios == '') {
        mensajesDelSistema('Escoger el asesor', '300', 'auto', false);
    } else {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Admin&accion=Fintra",
            dataType: "json",
            async: false,
            data: {
                opcion: 80,
                informacion: JSON.stringify({negocios: negocios, asesores: asesores})
            },
            success: function (json) {
                if (json.respuesta === 'Guardado') {
                    $('#dialogMsjAddAsesores').dialog("close");
                    cargarNegocios();
                    mensajesDelSistema('Exito en la asignacion', '300', 'auto', false);
                } else {
                    mensajesDelSistema('Ocurrio un error', '300', 'auto', false);
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function  exportarExcelReporte() {
    var fullData = $("#tabla_negocios").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var nameColum = $("#tabla_negocios").jqGrid('getGridParam', 'colNames');
    console.log(nameColum);
    nameColum.splice(0, 2);
    console.log(nameColum);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url:  "./controller?estado=Admin&accion=Fintra",
        data: {
            listado: myJsonString,
            namecolum: nameColum.toString(),
            namereporte:'Asesores',
            opcion: 92
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}