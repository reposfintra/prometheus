/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

if ($('#div_search_scoring').is(':visible')) {    
      
        jsonStatusCheck =  [];
            
        $('.solo-numeric').live('keypress', function (event) {
            return numbersonly(this, event);
        });
        
        $('input[type=text]').live('keyup', function (event) {
            if ($('input[name=min_max]:checked').val() === 'minimo') {
                $("#min_value").val(calcularSumatoriaTablaScoring());
            } else {
                $("#max_value").val(calcularSumatoriaTablaScoring());
            }
        });
        
        $('.sumatoria').live('change', function (event) {
            if ($('input[name=min_max]:checked').val()==='minimo'){
               $("#min_value").val(calcularSumatoriaTablaScoring());
            }else{
                $("#max_value").val(calcularSumatoriaTablaScoring());
            }                
        });
        
        $('input[name=min_max]:checked').live('change', function (event) {          
            setearCheckboxesByType(this.value);
        });
        
        cargarComboCategorias();        
        $("#btn_show_scoring").click(function () {
            cargarTablaScoring();           
        });
        $("#btn_clear").click(function () {
            $('#idProMeta').attr({disabled: false}); 
            $('#idProMeta').val('');
            $('#tbl_scoring').html('');     
            $('#botones_footer').fadeOut("fast");
            jsonStatusCheck =  [];
        });
        
        $("#btn_guardar").click(function () {
            var infoValid = validFormData();            
            if(infoValid === ''){                
                guardarInfoScoring();
            }else{
                mensajesDelSistema(infoValid.toString(), '250', '150');
            }
        });

        $("#btn_salir").click(function () {
            window.close();
        });
 }else{
     
    cargarComboCategorias();
    cargarCategorias();
    cargarCategoriasAC();
    $("#idinsumo").val(1);

    $("#boton_guardar_proInterno").click(function () {
        actualizarProInterno();
    });
    $("#buscarnumos").click(function () {
        busqueda();
    });
    /////////////////////////////////////
    $("#buscarfitro").click(function () {
        var valor = $('#b_idespecificacion').val();
        refrescarGridvalPredFiltro(valor);
    });
    /////////////////////////////////////
    //cargarComboInsumos();
 }
});

function cargarCombo(id, filtro) {
    var elemento = $('#' + id)
            , sql = (id === 'b_idsubcategoria') ? 'subcategorias' : 'especificacionxsubcategoria';
    //$("#lui_tabla_productos,#load_tabla_productos").show();
    $.ajax({
        url: "./controller?estado=Auto_&accion=Scoring",
        datatype: 'json',
        type: 'get',
        data: {opcion: 40, informacion: JSON.stringify({query: sql, filtros: filtro})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    mensajesDelSistema(json.error, '300', 'auto', false);
                } else {
                    elemento.html('');
                    for (var e in json) {
                        elemento.append('<option value="' + e + '">' + json[e] + '</option>');
                    }
                }
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}


function busqueda() {
    buscarMultiservicios();
    $("#dialogMsjmultiservicio").dialog({
        width: '400',
        height: '270',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'BUSCAR CATEGORIA',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            "Asignar": function () {
                var categoria = $("#multiser").val();
                $('#idcategoria').val(categoria);
                $("#multiser").val('');
                buscarCategoria(categoria);
                $(this).dialog("close");
            },
            "Salir": function () {
                $("#multiser").val('');
                $(this).dialog("close");
            }
        }
    });
}

function buscarCategoria(id) {
    var url = './controller?estado=Auto_&accion=Scoring';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 36,
            idCategoria: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridProcesosMeta();
                    refrescarGridProcesosInterno(idProceso);
                    cargarComboCategorias();
                    mensajesDelSistema("Se actualizó la Categoria", '250', '150', true);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo actualizar la Categoria!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}




function buscarMultiservicios() {
    var info = {};
    $.ajax({
        url: "./controller?estado=Auto_&accion=Scoring",
        type: 'POST',
        dataType: 'json',
        data: {
            opcion: 35
                    // multiservicio:$('#multiservicio').val()
        },
        success: function (json) {
            info = json;
            $("#multiser").autocomplete({
                source: info,
                minLength: 2
            });
        }
    });
}

//Carga los nombre del autocompletar de Categoria
function cargarCategoriasAC() {
    var url = './controller?estado=Auto_&accion=Scoring';
    var insumo = 1;
    var nomCategorias = [];
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 1,
            insumo: insumo
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                for (j in json.rows) {
                    nomCategorias.push(json.rows[j].nombre);
                }
                $("#nommeta").autocomplete({
                    source: nomCategorias,
                    minLength: 2
                });

            }
        }
    });
}

function cargarCategorias() {

    //var insumo = document.getElementById("insumos").value;
    var insumo = 1;
    var url = './controller?estado=Auto_&accion=Scoring&opcion=1&insumo=' + insumo;
    if ($("#gview_Categorias").length) {
        refrescarGridCategoria(insumo);
    } else {
        jQuery("#Categorias").jqGrid({
            caption: 'Unidades de Negocio',
            url: url,
            datatype: 'json',
            height: 650,
            width: 800,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600'},
                {name: 'descripcion', index: 'descripcion', hidden: true, sortable: true, align: 'center', width: '700px'},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '100'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_categorias',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                var ids = jQuery("#Categorias").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarProcesosMeta('" + cl + "');\">";
                    //an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular la Categoria?','250','150',existeUsuarioRelMetaProceso,'" + cl + "');\">";
                    jQuery("#Categorias").jqGrid('setRowData', ids[i], {actions: ed});
                }
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_categorias", {edit: false, add: false, del: false});
        jQuery("#Categorias").jqGrid('filterToolbar', "#searchCategoria",
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true,
                    gs_nombe: false

                });
        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function crearMetaProceso() {
    $('#nommeta').val('');
    $('#descmeta').val('');
    $('#div_categoria').fadeIn('slow');
    AbrirDivCrearMetaProceso();
}

function AbrirDivCrearMetaProceso() {
    $("#div_categoria").dialog({
        width: 700,
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR CATEGORIA',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarMetaProceso();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarMetaProceso() {
    var empresa = $('#nomempresa').val();
    var idinsumo = $('#idinsumo').val();
    var nomMeta = $('#nommeta').val();
    var descMeta = $('#descmeta').val();
    var url = './controller?estado=Auto_&accion=Scoring';
    if (empresa !== '' && nomMeta !== '' && descMeta !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 2,
                nombre: nomMeta,
                descripcion: descMeta,
                insumo: idinsumo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        //cargarComboCategorias();
                        //refrescarGridCategoria(idinsumo);
                        cargarCategorias();
                        mensajesDelSistema("Se creó la Categoria", '250', '150', true);
                        $('#nommeta').val('');
                        $('#descmeta').val('');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear la categoria!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }
}

function refrescarGridUnidadMedida() {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=39';
    jQuery("#unidadmedida").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#unidadmedida').trigger("reloadGrid");
}

function refrescarGridCategoria(insumo) {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=1&insumo=' + insumo;
    jQuery("#Categorias").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#Categorias').trigger("reloadGrid");
}

function editarProcesosMeta(cl) {

    $('#div_editar_categoria').fadeIn("slow");
    var fila = jQuery("#Categorias").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    $('#idMetaEdit').val(cl);
    $('#nommetaEdit').val(nombre);
    $('#descmetaEdit').val(descripcion);
    AbrirDivEditarMetaProceso();
    cargarProcesosInternos();
}

function AbrirDivEditarMetaProceso() {
    $("#div_editar_categoria").dialog({
        width: 700,
        height: 600,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'TIPO VARIABLE MERCADO',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarMetaProceso() {
    var nombre = $('#nommetaEdit').val();
    var descripcion = $('#descmetaEdit').val();
    var idProceso = $('#idMetaEdit').val();
    var url = './controller?estado=Auto_&accion=Scoring';
    if (nombre !== '' && descripcion !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 3,
                nombre: nombre,
                descripcion: descripcion,
                idProceso: idProceso
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridProcesosMeta();
                        refrescarGridProcesosInterno(idProceso);
                        cargarComboCategorias();
                        mensajesDelSistema("Se actualizó la Categoria", '250', '150', true);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar la Categoria!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');
    }

}

function anularMetaProceso(cl) {
    var url = './controller?estado=Auto_&accion=Scoring';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 15,
            idProceso: cl
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridCategoria(document.getElementById("insumos").value);
                    cargarComboCategorias();
                    mensajesDelSistema("Se anuló la Categoria", '250', '150', true);
                    $('#div_categoria').fadeOut();
                } else if (json.respuesta === "OK1") {
                    mensajesDelSistema("Esta categoria ya tiene datos, no se puede anular...", '250', '150', true);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular la Categoria!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function existeUsuarioRelMetaProceso(id) {
    $.ajax({
        url: './controller?estado=Auto_&accion=Scoring',
        datatype: 'json',
        type: 'post',
        data: {
            opcion: 22,
            tipo: "META",
            idProceso: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.respuesta === "SI") {
                    mensajeConfirmAnulacion('Alerta!!! Existen usuarios asociados al Meta proceso, desea continuar?', '350', '165', id);
                } else {
                    anularMetaProceso(id);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo realizar el proceso!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function crearProcesoInterno() {
    var idMetaProceso = $('#idMetaEdit').val();
    $('#idProMeta').val(idMetaProceso);
    $('#nomProceso').val('');
    $('#div_Subcategoria').fadeIn("slow");
    AbrirDivCrearSubCategoria();
    cargarSubcategoriaAC();
}



function cargarSubcategoriaAC() {
    var url = './controller?estado=Auto_&accion=Scoring';
    var insumo = $('#idinsumo').val();
    var nomSubCategoria = [];
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 55,
            insumo: insumo
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                for (j in json) {
                    nomSubCategoria.push(json[j]);
                }
                $("#nomProceso").autocomplete({
                    source: nomSubCategoria,
                    minLength: 2
                });

            }
        }
    });
}


function AbrirDivCrearSubCategoria() {
    $("#div_Subcategoria").dialog({
        width: 675,
        height: 175,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarSubCategoria();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

//Carga los nombre del autocompletar de subcategoria
function cargarEspecificacionAC() {
    var url = './controller?estado=Auto_&accion=Scoring';
    var categoria = $('#idMetaEdit').val();
    var nomEspecificacion = [];
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 56,
            categoria: categoria
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                for (j in json) {
                    nomEspecificacion.push(json[j]);
                }
                $("#nomEspecificacion").autocomplete({
                    source: nomEspecificacion,
                    minLength: 2
                });

            }
        }
    });
}

function cargarProcesosInternos() {
    var idProcesoMeta = $('#idMetaEdit').val();
    var url = './controller?estado=Auto_&accion=Scoring&opcion=4&procesoMeta=' + idProcesoMeta;
    if ($("#gview_subcategoria").length) {
        refrescarGridProcesosInterno(idProcesoMeta);
    } else {
        jQuery("#subcategoria").jqGrid({
            caption: 'Tipo Variable Mercado',
            url: url,
            datatype: 'json',
            height: 330,
            width: 620,
            colNames: ['Id', 'Id Proceso Meta', 'Categoria', 'Tipo Variable Mercado', 'Descripcion', 'Acciones', 'Id Subcategoria'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key: true},
                {name: 'id_tabla_rel', index: 'id_tabla_rel', hidden: true},
                {name: 'descripcionTablaRel', index: 'descripcionTablaRel', sortable: true, align: 'center', width: '600px', hidden: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'center', width: '600px'},
                {name: 'descripcion', index: 'descripcion', hidden: true, sortable: true, align: 'center', width: '600px'},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '200px'},
                {name: 'idsubcategoria', index: 'idsubcategoria', hidden: true, sortable: true, align: 'center', width: '600px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_subcategorias',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                var ids = jQuery("#subcategoria").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/chart_link.png' align='absbottom'  name='editar' id='editar' width='16' height='16' title ='Editar'  onclick=\"editarSubCategoria('" + cl + "');\">";
                    //us = "<img src='/fintra/images/botones/iconos/users.png' align='absbottom'  name='asociar' id='asociar' width='16' height='16' title ='Asociar usuarios'  onclick=\"asociarUsuarioProceso('" + cl + "');\">";
//                      an = "<img src='/fintra/images/botones/iconos/anular.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el proceso interno seleccionado?','250','150',existeUsuarioRelProcesoInterno,'" + cl + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='16' height='16' title ='Anular'  onclick=\"mensajeConfirmAnulacion('ALERTA!!! Esta seguro de anular este registro, desea continuar?','350','165',anularProcesoInterno,'" + cl + "');\">";
                    jQuery("#subcategoria").jqGrid('setRowData', ids[i], {actions: ed + '   ' + an});
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_subcategorias", {edit: false, add: false, del: false});
        jQuery("#subcategoria").jqGrid("navButtonAdd", "#page_tabla_subcategorias", {
            caption: "Nuevo",
            title: "Agregar nuevo Tipo Variable Mercado",
            onClickButton: function () {
                crearProcesoInterno();
            }
        });
    }
}

function refrescarGridProcesosInterno(id) {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=4&procesoMeta=' + id;
    jQuery("#subcategoria").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#subcategoria').trigger("reloadGrid");
}

function guardarSubCategoria() {
    var idProcesoMetaAct = $('#idMetaEdit').val();
    var idProMeta = $('#idProMeta').val();
    var nomProceso = $('#nomProceso').val();
    var url = './controller?estado=Auto_&accion=Scoring';
    if (nomProceso !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 34,
                nombre: nomProceso,
                procesoMeta: idProcesoMetaAct
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        mensajesDelSistema("Se creó la Relacion", '250', '150', true);
                        $('#nomProceso').val('');
                        $('#descProceso').val('');
                        refrescarGridProcesosInterno(idProcesoMetaAct);
                    }
                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear la Subcategoria", '250', '150');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');
    }
}


function anularProcesoInterno(cl) {
    var idProcesoMeta = $('#idMetaEdit').val();
    $.ajax({
        url: './controller?estado=Auto_&accion=Scoring',
        datatype: 'json',
        type: 'get',
        data: {
            opcion: 16,
            idProinterno: cl,
            idProcesoMeta: idProcesoMeta

        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridProcesosInterno(idProcesoMeta);
                    mensajesDelSistema("Se anuló el registro", '250', '150', true);
                }
                else if (json.respuesta === "OK1") {
                    mensajesDelSistema("No se puede anular subcategoria, tiene datos asociados...", '250', '150', true);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function editarSubCategoria(id) {
    var nomProinterno = jQuery('#subcategoria').getCell(id, 'nombre');
    var descProinterno = jQuery('#subcategoria').getCell(id, 'descripcion');
    var idMetaProceso = jQuery('#subcategoria').getCell(id, 'id_tabla_rel');
    var idSubcategoria = jQuery('#subcategoria').getCell(id, 'idsubcategoria');
    $('#nomProinterno').val(nomProinterno);
    $('#descProinterno').val(descProinterno);
    $('#idProMetaEdit').val(idMetaProceso);
    $('#idProinterno').val(id);
    $('#idSubcategoria').val(idSubcategoria);
    $('#div_editar_especificacion').fadeIn();
    $('#div_und_prointerno').fadeIn();
    AbrirDivEditarProcesoInterno();
    listarUndNegocioPro(id, idMetaProceso);
    listarUndNegocio(idMetaProceso);
}




function AbrirDivEditarProcesoInterno() {
    $("#div_editar_especificacion").dialog({
        width: 800,
        height: 700,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'EDITAR TIPO VARIABLE MERCADO',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    }).navGrid("#page_tabla_especificacion", {edit: false, add: false, del: false, search: false});
    ;
}

function listarUndNegocioPro(id, idMetaProceso) {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=8&idProinterno=' + id + '&idProMetaEdit=' + idMetaProceso;
    if ($("#gview_UndNegocioPro").length) {
        refrescarGridUndNegocioPro(id, idMetaProceso);
    } else {
        jQuery("#UndNegocioPro").jqGrid({
            caption: 'Variables Mercado del Tipo Variable',
            url: url,
            datatype: 'json',
            height: 380,
            width: 300,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Tipo Dato', 'R', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden: true, align: 'center', width: '100px', key: true},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '600px'},
                {name: 'descripcion', index: 'descripcion', hidden: true, sortable: true, align: 'left', width: '600px'},
                /*{name: 'obligatorio', index: 'obligatorio', sortable: true, align: 'center', width: '100px'},*/
                {name: 'tipo_dato', index: 'tipo_dato', hidden: true, sortable: true, align: 'left', width: '600px'},
                {name: 'obligatorio', index: 'obligatorio', hidden: true, width: 75, align: 'center', formatter: 'checkbox',
                    edittype: 'checkbox', editoptions: {value: '1:0'}},
                {name: 'actions', index: 'actions', hidden: true, resizable: false, align: 'center', width: '100'}

            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                $('#bt_asociar_especificacion').show();
                var ids = jQuery("#UndNegocioPro").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' align: 'center' height='15' title ='Editar'  onclick=\"crearEspecificacionEdit2(" + cl + ");\">";
                    jQuery("#UndNegocioPro").jqGrid('setRowData', ids[i], {actions: ed});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#UndNegocioPro"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", rowid);
                var id = filas.id;
                consultarEspecificacion(id);
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}

function crearEspecificacionEdit2(id) {
    $('#div_especificacion').fadeIn("slow");

    var fila = jQuery("#UndNegocioPro").getRowData(id);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    var tipo_dato = fila['tipo_dato'];
    $('#idEsp').val(id);
    $('#nomEspecificacion').val(nombre);
    $('#descEspecificacion').val(descripcion);
    $('#tipo_dato').val(tipo_dato);
    $('#tipodatos').val(tipo_dato);


    //cargarEspecificacionesedit();
    //cargarProcesosInternos();
    rediseñareditesp(tipo_dato, id);
    cargarCombo('b_idsubcategoria', []);
    $("#div_especificacion").dialog("destroy");
    if (tipo_dato === '0') {
        AbrirDivEditEspecificacion("715", "790");
    } else {
        AbrirDivEditEspecificacion("715", "255");
    }



}

function refrescarGridUndNegocioPro(id, idMetaProceso) {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=8&idProinterno=' + id + '&idProMetaEdit=' + idMetaProceso;
    jQuery("#UndNegocioPro").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#UndNegocioPro').trigger("reloadGrid");
}

function listarUndNegocio(idMetaProceso) {
    var idProInterno = $('#idProinterno').val();
    var url = './controller?estado=Auto_&accion=Scoring&opcion=6&idProceso=' + idProInterno + '&idProMetaEdit=' + idMetaProceso;
    $('#div_undNegocios').fadeIn();
    $('#bt_listar_undPro').hide();
    $('#bt_asociar_especificacion').show();
    if ($("#gview_UndadesNegocios").length) {
        refrescarGridUndNegocio(idProInterno, idMetaProceso);
    } else {
        jQuery("#UndadesNegocios").jqGrid({
            caption: 'Variable Mercado',
            url: url,
            datatype: 'json',
            height: 380,
            width: 300,
            colNames: ['Id', 'Descripcion', 'Nombre', 'Tipo Dato', 'R', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden: true, align: 'center', width: '100px', key: true},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'left', width: '600px'},
                {name: 'nombre', index: 'nombre', hidden: true, sortable: true, align: 'left', width: '600px'},
                {name: 'tipo_dato', index: 'tipo_dato', hidden: true, sortable: true, align: 'left', width: '600px'},
                {name: 'obligatorio', index: 'obligatorio', hidden: true, sortable: true, width: 75, align: 'center', formatter: 'checkbox',
                    edittype: 'checkbox', formatoptions: {disabled: false}, editoptions: {value: '1:0'}},
                {name: 'actions', index: 'actions', hidden: false, resizable: false, align: 'center', width: '110px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            pager: '#page_tabla_especificacion',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            gridComplete: function () {
                $('#bt_desasociar_especificacion').show();
                var ids = jQuery("#UndadesNegocios").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' align: 'center' height='15' title ='Editar'  onclick=\"crearEspecificacionEdit(" + cl + ");\">";
                    jQuery("#UndadesNegocios").jqGrid('setRowData', ids[i], {actions: ed});
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_especificacion", {
            first_page: false,
            l_page: false,
            refresh: false,
            search: false,
            edit: false,
            add: false,
            del: false});
        jQuery("#UndadesNegocios").jqGrid("navButtonAdd", "#page_tabla_especificacion", {
            caption: "Nuevo",
            title: "Agregar nuev Especificacion",
            onClickButton: function () {
                cargarEspecificacionAC();
                crearEspecificacion();
            }
        });/*
         jQuery("#UndadesNegocios").jqGrid('filterToolbar', "#searchEspecificaciones",
         {
         autosearch: true,
         searchOnEnter: true,
         defaultSearch: "cn",
         stringResult: true,
         ignoreCase: true,
         multipleSearch: true,
         gs_nombe: false
         
         });*/
        $("#gs_obligatorio").attr('hidden', true);
    }
}

function refrescarvalorespredeterminadosespedit(id) {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=49&id=' + id;
    jQuery("#valorespredeterminados_aso").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#valorespredeterminados_aso').trigger("reloadGrid");
}

function listarvalorespredeterminadosespedit() {

    var id = $('#idEsp').val();

    var url = './controller?estado=Auto_&accion=Scoring&opcion=49&id=' + id;
    //var url = './controller?estado=Auto_&accion=Scoring';
    if ($("#gview_valorespredeterminados_aso").length) {
        refrescarvalorespredeterminadosespedit(id);
    } else {
        jQuery("#valorespredeterminados_aso").jqGrid({
            caption: 'Valores Predeterminados',
            url: url,
            datatype: 'json',
            height: 280,
            width: 300,
            colNames: ['Id', 'Nombre', 'Id Relacion', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, key: true},
                {name: 'valor_xdefecto', index: 'valor_xdefecto', sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'idrel', index: 'idrel', hidden: false, sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, editable: true, align: 'left', width: '600px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_valorespredeterminados_aso',
            multiselect: true,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {


            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}

function refrescarGridvalorespredeterminadosedit() {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=38&id=' + 0;
    jQuery("#valorespredeterminados").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#valorespredeterminados').trigger("reloadGrid");
}

function listarvalorespredeterminadosedit() {
    var id = $('#idEsp').val();
    var url = './controller?estado=Auto_&accion=Scoring&opcion=38';
    //var url = './controller?estado=Auto_&accion=Scoring';
    if ($("#gview_valorespredeterminados").length) {
        refrescarGridvalorespredeterminadosedit();
    } else {
        jQuery("#valorespredeterminados").jqGrid({
            caption: 'Valores Predeterminados',
            url: url,
            datatype: 'json',
            height: 280,
            width: 300,
            colNames: ['Id', 'Nombre', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, key: true},
                {name: 'valor_xdefecto', index: 'valor_xdefecto', sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, editable: true, align: 'left', width: '600px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_valorespredeterminados',
            multiselect: true,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {

                var ids = jQuery("#valorespredeterminados").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' align: 'center' height='15' title ='Editar'  onclick=\"pruebas('" + cl + "');\">";
                    jQuery("#valorespredeterminados").jqGrid('setRowData', ids[i], {actions: ed});
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_valorespredeterminados", {add: false, edit: false, del: false, search: false, refresh: false});
        jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
            caption: "Nuevo",
            title: "Agregar nueva fila",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {

                var grid = $("#valorespredeterminados")
                        , rowid = 'neo_' + grid.getRowData().length;
                //alert(rowid);
                grid.addRowData(rowid, {});
                grid.jqGrid('editRow', rowid, true, function () {
                    //  $("input, select", e.target).focus();
                }, null, null, {}, null, null, function (id) {
                    var g = $('#valorespredeterminados')
                            , r = g.jqGrid('getLocalRow', id);
                    if (!r.id) {
                        g.jqGrid('delRowData', id);
                    }
                });
            }
            //position: "first"
        });
        jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                guardarValp();
            }
            //position: "first"
        });
        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function cargardivseleccionedit(id) {
    //listarvalorespredeterminadosesp();
    //listarvalorespredeterminados();
    listarvalorespredeterminados();
    listarvalorespredeterminadosedit();
    //$('#rtipodato').hide();
    $('#div_valorespredeterminados').show();
    $('#div_valorespredeterminados_aso').show();
    //$("#div_seleccion").show();
//    $("#div_especificacion").dialog({
//        position: {'my': 'center', 'at': 'center'}
//    });

}

function cargardigitacionedit() {
    $('#rtipodato').show();
    $("#div_seleccion").hide();
    /*$("#div_especificacion").dialog({
     position: {'my': 'center', 'at': 'center'}
     });*/
}

function rediseñareditesp(tipo_dato, id) {

    if (tipo_dato === "0") {
        rediseñardivespecificaciones(1);
        cargardivseleccionedit(id);
        $('#idtiposeleccion').val(0);
        //$('#radio1').attr('checked', true);
        //$('#radio2').attr('checked', false);

    } else {
        rediseñardivespecificaciones(2);
        cargardigitacionedit();
        $('#idtiposeleccion').val('');
        //$('#radio1').attr('checked', false);
        //$('#radio2').attr('checked', true);

    }
    //document.getElementById('radio1').disabled = true;
    //document.getElementById('radio2').disabled = true;

}

function crearEspecificacionEdit(id) {
    $('#div_especificacion').fadeIn("slow");

    var fila = jQuery("#UndadesNegocios").getRowData(id);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    var tipo_dato = fila['tipo_dato'];
    $('#idEsp').val(id);
    $('#id_und').val($('#idProMetaEdit').val());
    $('#nomEspecificacion').val(nombre);
    $('#descEspecificacion').val(descripcion);
    $('#t_entrada').val(tipo_dato);
    //Se habilitan controles
    $('#nomEspecificacion').attr({readonly: false});
    $('#t_entrada').attr({disabled: false});
   // $('#tipodatos').val(tipo_dato);


    //cargarEspecificacionesedit();
    //cargarProcesosInternos();
   /* rediseñareditesp(tipo_dato, id);
    cargarCombo('b_idsubcategoria', []);*/
    $("#div_especificacion").dialog("destroy");
    AbrirDivEditEspecificacion("500", "500");
    if (tipo_dato === '0') {
         listarvaloresvariablemercado();
    } else {
          listarvaloresvariablemercado2();
    }



}

function AbrirDivEditEspecificacion(width, height) {
    $("#div_especificacion").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'EDITAR ESPECIFICACION',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarEspecificacionedit();
            },
            "Salir": function () {
                $(this).dialog("destroy");


            }
        },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        }
    });

}

function guardarEspecificacionedit() {
    var grid = jQuery("#valorespredeterminados")
            , nomesp = $('#nomEspecificacion').val()
            , idSubcategoria = $('#idProinterno').val()
            , idProMetaEdit = $('#id_und').val()
            , t_entrada = $('#t_entrada').val()
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false, vara = 0,
            id = $('#idEsp').val();

    if ($('#t_entrada').val() !== "0") {
        grid = jQuery("#valorespredeterminados2");
    }


    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);
        grid.saveRow(filas[i]);
    }

    filas = grid.jqGrid('getRowData');

    if (nomesp === '') {

        error = true;
        mensajesDelSistema('Ingrese el nombre de la especificacion', '300', 'auto', false);
        vara = 1;
    }

    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
        vara = 1;
    }


    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: "./controller?estado=Auto_&accion=Scoring",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 51, informacion: JSON.stringify({id: id, nomesp: nomesp, t_entrada: t_entrada, rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    //alert(json.mensaje);
                    $('#nomEspecificacion').val('');
                    jQuery('#valorespredeterminados').jqGrid('clearGridData');
                    jQuery('#valorespredeterminados2').jqGrid('clearGridData');
                    refrescarGridUndNegocio(idSubcategoria, idProMetaEdit);
                    $("#div_especificacion").dialog("destroy");
                  

                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}


//este es el que hay que modificar para la creacion de los item select y digitar
function crearEspecificacion() {
    $('#div_especificacion').fadeIn();
    $('#valorespredeterminados').fadeIn();
    $('#id_und').val($('#idProMetaEdit').val());
    $('#idEsp').val('');
    $('#nomEspecificacion').val('');
    $('#t_entrada').val('0');
    //Se habilitan controles
    $('#nomEspecificacion').attr({readonly: false});
    $('#t_entrada').attr({disabled: false});
    AbrirDivCrearEspecificacion();
    
    if ($('#t_entrada').val() === "0") {
        listarvalorespredeterminados();
    } else {
        listarvalorespredeterminados2();
    }
}

function AbrirDivCrearEspecificacion() {
    $("#div_especificacion").dialog({
        width: 500,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        title: 'CREAR VARIABLE MERCADO',
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarEspecificacion();
            },
            "Salir": function () {
                $(this).dialog("destroy");

            }
        },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        }
    });
}

function guardarEspecificacion() {
    var grid = jQuery("#valorespredeterminados")
            , nomesp = $('#nomEspecificacion').val()
            , idSubcategoria = $('#idProinterno').val()
            , idProMetaEdit = $('#id_und').val()
            , t_entrada = $('#t_entrada').val()
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;

    if ($('#t_entrada').val() !== "0") {
        grid = jQuery("#valorespredeterminados2");
    }


    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);
        grid.saveRow(filas[i]);
    }

    filas = grid.jqGrid('getRowData');

    if (nomesp === '') {

        error = true;
        mensajesDelSistema('Ingrese el nombre', '300', 'auto', false);
        vara = 1;
    }

    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }


    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();

    $.ajax({
        url: "./controller?estado=Auto_&accion=Scoring",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 33, informacion: JSON.stringify({nomesp: nomesp, t_entrada: t_entrada, rows: filas})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    //alert(json.mensaje);
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                    $('#nomEspecificacion').val('');
                    jQuery('#valorespredeterminados').jqGrid('clearGridData');
                    jQuery('#valorespredeterminados2').jqGrid('clearGridData');
                    refrescarGridUndNegocio(idSubcategoria, idProMetaEdit);
                    $("#div_especificacion").dialog("destroy");

                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}

function refrescarGridUndNegocio(id, idMetaProceso) {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=6&idProceso=' + id + '&idProMetaEdit=' + idMetaProceso;
    jQuery("#UndadesNegocios").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#UndadesNegocios').trigger("reloadGrid");
}

function actualizarProInterno() {
    var idProcesoMetaAct = $('#idMetaEdit').val();
    var idProinterno = $('#idProinterno').val();
    var nomProinterno = $('#nomProinterno').val();
    var idProMeta = $('#idProMetaEdit').val();
    var idSubcategoria = $('#idSubcategoria').val();
    if (idProMeta !== '' && nomProinterno !== '') {
        $.ajax({
            url: './controller?estado=Auto_&accion=Scoring',
            datatype: 'json',
            type: 'post',
            data: {
                opcion: 9,
                idProinterno: idProinterno,
                nomProinterno: nomProinterno,
                idProMeta: idProMeta,
                idSubcategoria: idSubcategoria
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridProcesosInterno(idProcesoMetaAct);
                        mensajesDelSistema("El proceso se actualizó correctamente!!", '250', '150', true);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo actualizar el proceso!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }

}

function desasociarUndProinterno() {
    var idSubcategoria = $('#idSubcategoria').val();
    var idProinterno = $('#idProinterno').val();
    var idProMetaEdit = $('#idProMetaEdit').val();
    var listado = "";
    var filasId = jQuery('#UndNegocioPro').jqGrid('getGridParam', 'selarrrow');
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        $.ajax({
            url: './controller?estado=Auto_&accion=Scoring',
            datatype: 'json',
            type: 'post',
            data: {
                opcion: 10,
                listado: listado,
                idProinterno: idProinterno,
                idSubcategoria: idSubcategoria
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridUndNegocioPro(idProinterno, idProMetaEdit);
                        refrescarGridUndNegocio(idProinterno, idProMetaEdit);
                    } else if (json.respuesta === "OK1") {

                        mensajesDelSistema("No se puede desasociar especificaciones, existes datos asociados...", '250', '150');

                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo desasignar la especificacion", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#UndNegocioPro").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las especificaciones", '250', '150');
        } else {
            mensajesDelSistema("No hay categorias por desasignar", '250', '150');
        }
    }
}

function asociarUndProinterno() {
    var idSubcategoria = $('#idSubcategoria').val();
    var idProinterno = $('#idProinterno').val();
    var idProMetaEdit = $('#idProMetaEdit').val();
    var listado = "";
    var filasId = jQuery('#UndadesNegocios').jqGrid('getGridParam', 'selarrrow');
    var coma = "";
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            var ret = jQuery("#UndadesNegocios").jqGrid('getRowData', filasId[i]);
            //listado += filasId[i] + ",";
            listado += coma + ret.id + "-" + ret.obligatorio;
            coma = ",";
        }

        var url = './controller?estado=Auto_&accion=Scoring';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 7,
                listado: listado,
                idProinterno: idSubcategoria,
                idProMetaEdit: idProMetaEdit
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        $('#div_Subcategoria').fadeOut();
                        refrescarGridUndNegocioPro(idProinterno, idProMetaEdit);
                        refrescarGridUndNegocio(idProinterno, idProMetaEdit);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar las Especificaciones a la Subcategoria!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#UndadesNegocios").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las especificaciones a asociar", '250', '150');
        } else {
            mensajesDelSistema("No hay especificaciones por asociar", '250', '150');
        }
    }
}



function cargarComboCategorias() {
    $('#idProMeta').html('');
    $('#idProMetaEdit').html('');
    ;
    $.ajax({
        type: 'POST',
        url: './controller?estado=Auto_&accion=Scoring',
        dataType: 'json',
        data: {
            opcion: 17
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#idProMeta').append("<option value=''>Seleccione</option>");
                    $('#idProMetaEdit').append("<option value=''>Seleccione</option>");
                    for (var key in json) {
                        $('#idProMeta').append('<option value=' + key + '>' + json[key] + '</option>');
                        $('#idProMetaEdit').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                $('#idProMeta').append("<option value=''>Seleccione</option>");
                $('#idProMetaEdit').append("<option value=''>Seleccione</option>");
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function asociarUsuarioProceso(id) {
    var nomProinterno = jQuery('#subcategoria').getCell(id, 'nombre');
    var metaProceso = jQuery('#subcategoria').getCell(id, 'descripcionTablaRel');
    var descProinterno = jQuery('#subcategoria').getCell(id, 'descripcion');
    $('#idProinternoAsoc').val(id);
    $('#nomProinternoAsoc').val(nomProinterno);
    $('#nommetaproceso').val(metaProceso);
    $('#descProinternoAsoc').val(descProinterno);
    $('#div_asoc_user_prointerno').fadeIn("slow");
    $('#div_user_prointerno').fadeIn();
    AbrirDivAsociarUsuarioProInterno();
    listarUsuariosProInterno(id);
    listarUsuariosRelProInterno();
}

function AbrirDivAsociarUsuarioProInterno() {
    $("#div_asoc_user_prointerno").dialog({
        width: 850,
        height: 675,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ASOCIAR USUARIOS A PROCESOS INTERNOS',
        closeOnEscape: false,
        buttons: {
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }/*,
         close: function(event, ui){ 
         $('#div_asoc_user_prointerno').fadeOut('slow');
         $('#div_usuarios').fadeOut();       
         $('#bt_asociar_userPro').hide();
         $('#bt_desasociar_userPro').hide();
         }*/
    });
}

function listarUsuariosProInterno(idProceso) {

    if ($("#gview_userProInterno").length) {
        reloadGridUsersProInterno(idProceso);
    } else {
        $("#userProInterno").jqGrid({
            caption: 'Usuarios relacionados con el proceso',
            url: './controller?estado=Auto_&accion=Scoring&opcion=18&idProceso=' + idProceso,
            cellsubmit: "clientArray",
            datatype: 'json',
            height: 384,
            width: 'auto',
            cellEdit: true,
            // editurl: 'clientArray',
            colNames: ['Código', 'Id usuario', 'Nombre', 'Moderador'],
            colModel: [
                {name: 'codUsuario', index: 'codUsuario', hidden: true, sortable: true, align: 'center', width: '100px', key: true},
                {name: 'idusuario', index: 'idusuario', hidden: true, sortable: true, align: 'center', width: '100px'},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '250px'},
                {name: 'moderador', index: 'moderador', sortable: true, editable: true, align: 'center', width: '80px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            afterSaveCell: function (rowid, celname, value, iRow, iCol) {
                if (celname === 'moderador') {
                    var idProinterno = $('#idProinternoAsoc').val();
                    var idusuario = $("#userProInterno").getRowData(rowid).codUsuario;
                    var moderador = $("#userProInterno").getRowData(rowid).moderador;
                    if ((value.toUpperCase() !== "S" && value.toUpperCase() !== "N")) {

                        mensajesDelSistema("Especifique S o N, segun sea el caso", 250, 150);
                        $("#userProInterno").jqGrid("restoreCell", iRow, iCol);
                        return;
                    } else {
                        actualizarModerador(idProinterno, idusuario, moderador.toUpperCase());
                    }
                }
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                $('#bt_asociar_userPro').show();
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}

function reloadGridUsersProInterno(idProceso) {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=18&idProceso=' + idProceso;
    jQuery("#userProInterno").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#userProInterno').trigger("reloadGrid");
}

function listarUsuariosRelProInterno() {
    var idProceso = $('#idProinternoAsoc').val();
    $('#div_usuarios').fadeIn();
    $('#bt_listar_userPro').hide();
    $('#bt_asociar_userPro').show();
    idsOfSelectedRows = [];
    if ($("#gview_listUsuarios").length) {
        reloadGridUsuariosRelProInterno(idProceso);
    } else {
        var cbColModel;
        jQuery("#listUsuarios").jqGrid({
            caption: 'Usuarios',
            url: './controller?estado=Auto_&accion=Scoring&opcion=19&idProceso=' + idProceso,
            datatype: 'json',
            height: 368,
            width: 'auto',
            colNames: ['Cod Usuario', 'Nombre Usuario', 'Usuario Login'],
            colModel: [
                {name: 'codUsuario', index: 'codUsuario', hidden: true, align: 'center', width: '100px', key: true},
                {name: 'nombre', index: 'nombre', align: 'left', width: '300px'},
                {name: 'idusuario', index: 'idusuario', hidden: true, align: 'center', width: '200px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            ignoreCase: true,
            multiselect: true,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            onSelectRow: function (id, isSelected) {
                var p = this.p, item = p.data[p._index[id]], i = $.inArray(id, idsOfSelectedRows);
                item.cb = isSelected;
                if (!isSelected && i >= 0) {
                    idsOfSelectedRows.splice(i, 1); // remove id from the list
                } else if (i < 0) {
                    idsOfSelectedRows.push(id);
                }
            },
            loadComplete: function () {
                var p = this.p, data = p.data, item, $this = $(this), index = p._index, rowid, i, selCount;
                for (i = 0, selCount = idsOfSelectedRows.length; i < selCount; i++) {
                    rowid = idsOfSelectedRows[i];
                    item = data[index[rowid]];
                    if ('cb' in item && item.cb) {
                        $this.jqGrid('setSelection', rowid, false);
                    }
                }
            },
            gridComplete: function () {
                $('#bt_desasociar_userPro').show();
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
        cbColModel = jQuery("#listUsuarios").jqGrid('getColProp', 'cb');
        cbColModel.sortable = true;
        cbColModel.sorttype = function (value, item) {
            return 'cb' in item && item.cb ? 1 : 0;
        };
        jQuery("#listUsuarios").jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });
    }
}

function reloadGridUsuariosRelProInterno(idProceso) {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=19&idProceso=' + idProceso;
    jQuery("#listUsuarios").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery("#listUsuarios").jqGrid('filterToolbar',
            {
                autosearch: true,
                searchOnEnter: true,
                defaultSearch: "cn",
                stringResult: true,
                ignoreCase: true,
                multipleSearch: true
            });
    jQuery('#listUsuarios').trigger("reloadGrid");
}

function asociarUsuariosProinterno() {

    var idProinterno = $('#idProinternoAsoc').val();
    var jsonObj = [];
    var filasId = jQuery('#listUsuarios').jqGrid('getGridParam', 'selarrrow');
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            var id = filasId[i];
            var j = $.inArray(id, idsOfSelectedRows);
            idsOfSelectedRows.splice(j, 1); // remove id from the list
            var idUsuario = jQuery("#listUsuarios").getRowData(id).idusuario;
            var item = {};
            item ["cod_usuario"] = id;
            item ["id_usuario"] = idUsuario;
            jsonObj.push(item);
        }
        var listUser = {};
        listUser ["usuarios"] = jsonObj;
        var url = './controller?estado=Auto_&accion=Scoring';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 20,
                listado: JSON.stringify(listUser),
                idProInterno: idProinterno
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        reloadGridUsersProInterno(idProinterno);
                        reloadGridUsuariosRelProInterno(idProinterno);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar los usuarios al proceso interno!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#listUsuarios").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar los usuarios a asociar al proceso interno!!", '250', '150');
        } else {
            mensajesDelSistema("No hay usuarios por asociar", '250', '150');
        }

    }
}



function desasociarUsuariosProinterno() {
    var idProinterno = $('#idProinternoAsoc').val();
    var listado = "";
    var filasId = jQuery('#userProInterno').jqGrid('getGridParam', 'selarrrow');
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        $.ajax({
            url: './controller?estado=Auto_&accion=Scoring',
            datatype: 'json',
            type: 'post',
            data: {
                opcion: 21,
                listado: listado,
                idProinterno: idProinterno
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        reloadGridUsersProInterno(idProinterno);
                        reloadGridUsuariosRelProInterno(idProinterno);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo desasignar los usuarios del proceso interno!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#userProInterno").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar los usuarios a desasignar", '250', '150');
        } else {
            mensajesDelSistema("No hay usuarios por desasignar", '250', '150');
        }
    }


}

function actualizarModerador(idProinterno, idusuario, moderador) {
    var url = './controller?estado=Auto_&accion=Scoring';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 27,
            idProinterno: idProinterno,
            idusuario: idusuario,
            moderador: moderador
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    reloadGridUsersProInterno(idProinterno);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo actualizar información del moderador!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function existeUsuarioRelProcesoInterno(id) {
    $.ajax({
        url: './controller?estado=Auto_&accion=Scoring',
        datatype: 'json',
        type: 'post',
        data: {
            opcion: 22,
            tipo: "PINI",
            idProceso: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.respuesta === "SI") {
                    mensajeConfirmAnulacion('ALERTA!!! Existen usuarios asociados al proceso interno, desea continuar?', '350', '165', id);
                } else {
                    anularProcesoInterno(id);
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo realizar el proceso!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function mensajeConfirmAnulacion(msj, width, height, okAction, id) {
    mostrarContenido('dialogMsgMeta');
    $("#msj").html("<span style='background: url(/fintra/images/warning03.png); height:32px; width:32px; float: left; margin: 0 7px 20px 0'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajeConfirmAction(msj, width, height, okAction, id, param) {
    mostrarContenido('dialogMsgMeta');
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id, param);
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {
    mostrarContenido('dialogMsgMeta');
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsgMeta").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear botón cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mostrarContenido(Id_Contenido) {
    document.getElementById(Id_Contenido).style.display = "block";
    document.getElementById(Id_Contenido).style.visibility = "visible";
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}


function prueba(valor) {

    if (valor === '1') {
        rediseñardivespecificaciones(1);
        cargardivseleccion();
        $('#idtiposeleccion').val(0);
    }

}



function rediseñardivespecificaciones(val) {

    if (val === 1) { //es seleccion
        $('#div_especificacion').css({
            'width': '670px',
            'height': '734px'

        });

    } else if (val === 2) {
        $('#div_especificacion').css({
            'height': '157px'
        });
    } else {
        $('#div_especificacion').css({
            'height': '600px'
        });
    }

}


function cargaritemseleccion() {

    var url = './controller?estado=Auto_&accion=Scoring&opcion=38';
    if ($("#gview_procesosMeta").length) {
        refrescarGridProcesosMeta();
    } else {
        jQuery("#tablaseleccion").jqGrid({
            caption: 'Seleccion',
            url: url,
            datatype: 'json',
            height: 207,
            width: 300,
            colNames: ['Id', 'Nombre', 'Acciones'],
            colModel: [
                {name: 'iditemseleccion', index: 'iditemseleccion', sortable: true, align: 'center', width: '25%', key: true},
                {name: 'nombre', indeidunidadmedicionx: 'nombre', sortable: true, align: 'center', width: '45%'},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '30%'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            pager: '#page_tabla_selecion',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {
                var ids = jQuery("#tablaseleccion").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarProcesosMeta('" + cl + "');\">";
                    //proc_an = "<img src='/fintra/images/botones/iconos/detail1.png' align='absbottom'  name='proc_anul' id='proc_anul' width='15' height='15' title ='Ver procesos internos anulados'  onclick=\"verProcesosAnulados('" + cl + "');\">";
                    an = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el meta proceso seleccionado?','250','150',existeUsuarioRelMetaProceso,'" + cl + "');\">";
//                      an = "<img src='/fintra/images/botones/iconos/anular.gif' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAnulacion('Alerta!!! Puede que existan usuarios asociados al Meta proceso, desea continuar?','350','165',anularMetaProceso,'" + cl + "');\">";
                    jQuery("#tablaseleccion").jqGrid('setRowData', ids[i], {actions: ed + '   ' + '   ' + an});
                }
                $('#div_editar_categoria').fadeIn("slow");
                var fila = jQuery("#Categorias").getRowData(cl);
                var nombre = fila['nombre'];
                var descripcion = fila['descripcion'];
                $('#idMetaEdit').val(cl);
                $('#nommetaEdit').val(nombre);
                $('#descmetaEdit').val(descripcion);
                AbrirDivEditarMetaProceso();
                cargarProcesosInternos();
            },
            loadError: function (xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_selecion", {edit: false, add: false, del: false});
    }
}

function cargardigitacion() {
    $('#rtipodato').show();
    $("#div_seleccion").hide();
    $("#div_especificacion").dialog({
        position: {'my': 'center', 'at': 'center'}
    });
}

function guardarEspecificacion2() {
    var idSubcategoria = $('#idProinterno').val();
    var nomEspecificacion = $('#nomEspecificacion').val();
    var descEspecificacion = $('#descEspecificacion').val();
    var url = './controller?estado=Auto_&accion=Scoring';
    if (idSubcategoria !== '' && nomEspecificacion !== '' && descEspecificacion !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 33,
                nomEspecificacion: nomEspecificacion,
                descEspecificacion: descEspecificacion
            },
            success: function (json) {

                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        mensajesDelSistema("Se creó Especificacion", '250', '150', true);
                        $('#nomEspecificacion').val('');
                        $('#descEspecificacion').val('');
                        refrescarGridUndNegocio(idSubcategoria);
                    }
                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear especificacion", '250', '150');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos!!", '250', '150');
    }
}


function asociarvalorpredeterminado() {
    var idSubcategoria = $('#idSubcategoria').val();
    var idProinterno = $('#idProinterno').val();
    var listado = "";
    var filasId = jQuery('#UndadesNegocios').jqGrid('getGridParam', 'selarrrow');
    var coma = "";
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            var ret = jQuery("#UndadesNegocios").jqGrid('getRowData', filasId[i]);
            //listado += filasId[i] + ",";
            listado += coma + ret.id + "-" + ret.obligatorio;
            coma = ",";
        }

        var url = './controller?estado=Auto_&accion=Scoring';
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 7,
                listado: listado,
                idProinterno: idSubcategoria
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        $('#div_Subcategoria').fadeOut();
                        refrescarGridUndNegocioPro(idProinterno);
                        refrescarGridUndNegocio(idProinterno);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar las Especificaciones a la Subcategoria!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#UndadesNegocios").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las especificaciones a asociar", '250', '150');
        } else {
            mensajesDelSistema("No hay especificaciones por asociar", '250', '150');
        }
    }
}

function desasociarvalorpredeterminado() {

    var idSubcategoria = $('#idSubcategoria').val();
    var idProinterno = $('#idProinterno').val();
    var listado = "";
    var filasId = jQuery('#UndNegocioPro').jqGrid('getGridParam', 'selarrrow');
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            listado += filasId[i] + ",";
        }
        $.ajax({
            url: './controller?estado=Auto_&accion=Scoring',
            datatype: 'json',
            type: 'post',
            data: {
                opcion: 10,
                listado: listado,
                idProinterno: idProinterno,
                idSubcategoria: idSubcategoria
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        refrescarGridUndNegocioPro(idProinterno);
                        refrescarGridUndNegocio(idProinterno);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo desasignar la especificacion", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#UndNegocioPro").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las especificaciones", '250', '150');
        } else {
            mensajesDelSistema("No hay categorias por desasignar", '250', '150');
        }
    }
}

function cargardivseleccion() {
    listarvalorespredeterminados();
    //$('#rtipodato').hide();
    $('#div_valorespredeterminados').show();
    //$("#div_seleccion").show();
    /*$("#div_especificacion").dialog({
     position: {'my': 'center', 'at': 'center'}
     });*/
}

function listarvalorespredeterminados() {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=38';
    //var url = './controller?estado=Auto_&accion=Scoring';
    //$('#div_valorespredeterminados2').fadeOut();
    $('#valorespredeterminados2').jqGrid('GridUnload');
    $('#div_valorespredeterminados').fadeIn();
    if ($("#gview_valorespredeterminados").length) {
        refrescarGridvalorespredeterminados("valorespredeterminados");
    } else {
        jQuery("#valorespredeterminados").jqGrid({
            caption: 'Valores',
            url: url,
            datatype: 'json',
            height: 220,
            width: 300,
            colNames: ['Id', 'Descripcion', 'Inicio', 'Fin'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, key: true},
                {name: 'valor_xdefecto', index: 'valor_xdefecto', sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'inicio', index: 'inicio', sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'fin', index: 'fin', sortable: true, editable: true, align: 'left', width: '600px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_valorespredeterminados',
            multiselect: false,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {

                var ids = jQuery("#valorespredeterminados").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' align: 'center' height='15' title ='Editar'  onclick=\"pruebas('" + cl + "');\">";
                    jQuery("#valorespredeterminados").jqGrid('setRowData', ids[i], {actions: ed});
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_valorespredeterminados", {add: false, edit: false, del: false, search: false, refresh: false});
        jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
            caption: "Nuevo",
            title: "Agregar nueva fila",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {

                var grid = $("#valorespredeterminados")
                        , rowid = 'neo_' + grid.getRowData().length;
                //alert(rowid);
                grid.addRowData(rowid, {});
                grid.jqGrid('editRow', rowid, true, function () {
                    //  $("input, select", e.target).focus();
                }, null, null, {}, null, null, function (id) {
                    var g = $('#valorespredeterminados')
                            , r = g.jqGrid('getLocalRow', id);
                    if (!r.id) {
                        g.jqGrid('delRowData', id);
                    }
                });
            }
            //position: "first"
        });
        jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                guardarValp("valorespredeterminados");
            }
            //position: "first"
        });
        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function listarvalorespredeterminados2() {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=43';
    //var url = './controller?estado=Auto_&accion=Scoring';
    //$('#div_valorespredeterminados').fadeOut();
    $('#valorespredeterminados').jqGrid('GridUnload');
    $('#div_valorespredeterminados2').fadeIn();
    if ($("#gview_valorespredeterminados2").length) {
        refrescarGridvalorespredeterminados("valorespredeterminados2");
    } else {
        jQuery("#valorespredeterminados2").jqGrid({
            caption: 'Valores',
            url: url,
            datatype: 'json',
            height: 220,
            width: 300,
            colNames: ['Id', 'Descripcion', 'Unico', 'Fin'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, key: true},
                {name: 'valor_xdefecto', index: 'valor_xdefecto', sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'inicio', index: 'inicio', sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'fin', index: 'fin', hidden: true, sortable: true, editable: true, align: 'left', width: '600px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_valorespredeterminados2',
            multiselect: false,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false
            },
            gridComplete: function () {

                var ids = jQuery("#valorespredeterminados2").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' align: 'center' height='15' title ='Editar'  onclick=\"pruebas('" + cl + "');\">";
                    jQuery("#valorespredeterminados2").jqGrid('setRowData', ids[i], {actions: ed});
                }
            },           
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_valorespredeterminados2", {add: false, edit: false, del: false, search: false, refresh: false});
        jQuery("#valorespredeterminados2").navButtonAdd('#page_tabla_valorespredeterminados2', {
            caption: "Nuevo",
            title: "Agregar nueva fila",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {

                var grid = $("#valorespredeterminados2")
                        , rowid = 'neo_' + grid.getRowData().length;
                //alert(rowid);
                grid.addRowData(rowid, {});
                grid.jqGrid('editRow', rowid, true, function () {
                    //  $("input, select", e.target).focus();
                }, null, null, {}, null, null, function (id) {
                    var g = $('#valorespredeterminados2')
                            , r = g.jqGrid('getLocalRow', id);
                    if (!r.id) {
                        g.jqGrid('delRowData', id);
                    }
                });
            }
            //position: "first"
        });
        jQuery("#valorespredeterminados2").navButtonAdd('#page_tabla_valorespredeterminados2', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                guardarValp("valorespredeterminados2");
            }
            //position: "first"
        });
        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function guardarValp(op) {
    var grid = jQuery("#" + op)
            , filas = grid.jqGrid('getDataIDs')
            , data, error = false;
    for (var i = 0; i < filas.length; i++) {

        data = grid.jqGrid("getLocalRow", filas[i]);

        if (data.valor_xdefecto === '') {
            error = true;
            mensajesDelSistema('Digite la descipcion', '300', 'auto', false);
            grid.jqGrid('editRow', filas[i], true, function () {
                $("input, select", e.target).focus();
            });
            break;
        } else {
            if (data.inicio === '') {

                error = true;
                mensajesDelSistema('Digite el valor', '300', 'auto', false);
                grid.jqGrid('editRow', filas[i], true, function () {
                    $("input, select", e.target).focus();
                });
                break;
            } else {
                if (op === "valorespredeterminados" && isNaN(data.inicio)) {
                    error = true;
                    mensajesDelSistema('Digite un valor numerico(Inicio)', '300', 'auto', false);
                    grid.jqGrid('editRow', filas[i], true, function () {
                        $("input, select", e.target).focus();
                    });
                    break;
                } else {
                    if (op === "valorespredeterminados" && (data.fin === '')) {
                        error = true;
                        mensajesDelSistema('Digite un valor', '300', 'auto', false);
                        grid.jqGrid('editRow', filas[i], true, function () {
                            $("input, select", e.target).focus();
                        });
                        break;
                    } else {
                        if (op === "valorespredeterminados" && isNaN(data.fin)) {
                            error = true;
                            mensajesDelSistema('Digite un valor numerico(Fin)', '300', 'auto', false);
                            grid.jqGrid('editRow', filas[i], true, function () {
                                $("input, select", e.target).focus();
                            });
                            break;
                        }
                    }
                }
            }

        }

        grid.saveRow(filas[i]);
    }
    filas = grid.jqGrid('getRowData');
    if (filas.length === 0) {
        error = true;
        mensajesDelSistema('Inserte al menos un registro', '300', 'auto', false);
    }
    if (error)
        return;
    //$("#lui_tabla_productos,#load_tabla_productos").show();
    
    $.ajax({
        url: "./controller?estado=Auto_&accion=Scoring",
        datatype: 'json',
        type: 'POST',
        data: {opcion: 41, informacion: JSON.stringify({rows: filas, op: op})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '333', 'auto', false);
                } else {
                    //alert(json.mensaje);
                    mensajesDelSistema(json.mensaje, '300', 'auto', true);
                   ($('#idEsp').val()!=='')?refrescarGridvaloresvariablemercado(op):refrescarGridvalorespredeterminados(op);
                 /* if (op==='valorespredeterminados'){
                        refrescarGridvalorespredeterminados(op);
                    }else{
                        refrescarGridvalorespredeterminados2(op);
                    }*/
                    //refrescarGridvalorespredeterminados();
                    //AgregarGridAso();
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
}

function refrescarGridvalorespredeterminados(op) {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=38';
    jQuery("#" + op).setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#' + op).trigger("reloadGrid");
}

function refrescarGridvalorespredeterminados2(op) {
    var url = './controller?estado=Auto_&accion=Scoring&opcion=43';
    jQuery("#" + op).setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#' + op).trigger("reloadGrid");
}

function refrescarGridvalPredFiltro(id) {

    var ids = jQuery("#valorespredeterminados_aso").jqGrid('getDataIDs');
    var url = './controller?estado=Auto_&accion=Scoring&opcion=48&id=' + id + '&ids=' + ids;
    jQuery("#valorespredeterminados").setGridParam({
        datatype: 'json',
        url: url
    });
    jQuery('#valorespredeterminados').trigger("reloadGrid");
    //var ids = jQuery("#valorespredeterminados_aso").jqGrid('getDataIDs');
    //QuitarRowGridVal(ids);

}

function listarvalorespredeterminadosesp() {
    var idProInterno = $('#idProinterno').val();
    //var url = './controller?estado=Auto_&accion=Scoring&opcion=6&idProceso=' + idProInterno;
    var url = './controller?estado=Auto_&accion=Scoring';
    $('#div_valorespredeterminados_aso').fadeIn();
    $('#bt_listar_undPro').hide();
    $('#bt_asociar_valorpredeterminado').show();
    if ($("#gview_valorespredeterminados_aso").length) {
//refrescarGridUndNegocio(idProInterno);
    } else {
        jQuery("#valorespredeterminados_aso").jqGrid({
            caption: 'Valores Predeterminados',
            url: url,
            datatype: 'json',
            height: 329,
            width: 300,
            colNames: ['Id', 'Nombre', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, hidden: true, align: 'center', width: '100px', key: true},
                {name: 'valor_xdefecto', index: 'valor_xdefecto', sortable: true, align: 'left', width: '600px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'left', width: '600px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            multiselect: true,
            pager: '#page_tabla_valorespredeterminados_aso',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            gridComplete: function () {

            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        });
    }
}

function ajaxSelect(sql) {
    var resultado;
    $.ajax({
        url: "./controller?estado=Auto_&accion=Scoring",
        datatype: 'json',
        type: 'get',
        data: {opcion: 40, informacion: JSON.stringify({query: sql, filtros: []})},
        async: false,
        success: function (json) {
            try {
                if (json.error) {
                    //alert(json.error);
                    mensajesDelSistema(json.error, '300', 'auto', false);
                    resultado = {};
                } else {
                    resultado = json;
                }
            } catch (exc) {
                console.error(exc);
            } finally {
                //$("#lui_tabla_productos,#load_tabla_productos").hide();
            }
        },
        error: function () {
            console.log('error');
            //$("#lui_tabla_productos,#load_tabla_productos").hide();
        }
    });
    return resultado;
}

function crearvalorpredeterminado() {
    $('#div_valpre').fadeIn('slow');
    AbrirDivCrearvalpre();
}

function AbrirDivCrearvalpre() {
    $("#div_valpre").dialog({
        width: 700,
        height: 150,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR VALOR PREDETERMINADO',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarValorPredeterminado();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarValorPredeterminado() {
    var empresa = $('#nomempresa').val();
    var nomMeta = $('#nomvalorpredeterminado').val();
    var url = './controller?estado=Auto_&accion=Scoring';
    if (empresa !== '' && nomMeta !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 41,
                nombre: nomMeta

            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {

                        cargarComboCategorias();
                        mensajesDelSistema("Se creó el valor predeterminado", '250', '150', true);
                        $('#nomvalorpredeterminado').val('');
                        $('#descvalorpredeterminado').val('');
                        refrescarGridvalorespredeterminados();
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear el valor predeterminado!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }
}

function asociarespecificacionvalorpre(idProceso, idSubcategoria) {
    var idProinterno = idProceso;
    var listado = "";
    //var filasId = jQuery('#valorespredeterminados_aso').jqGrid('getGridParam', 'selarrrow');
    var filasId = jQuery("#valorespredeterminados_aso").jqGrid('getDataIDs');
    if (filasId !== '') {
        for (var i = 0; i < filasId.length; i++) {
            var ret = jQuery("#valorespredeterminados_aso").jqGrid('getRowData', filasId[i]);
            //var ret = jQuery("#valorespredeterminados_aso").jqGrid('getDataIDs');
            listado += filasId[i] + ",";
        }

        var url = './controller?estado=Auto_&accion=Scoring&idProinterno=' + idProinterno;
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 42,
                listado: listado
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        $('#div_Subcategoria').fadeOut();
                        refrescarGridUndNegocioPro(idSubcategoria);
                        refrescarGridUndNegocio(idSubcategoria);
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo asociar las Especificaciones a la Subcategoria!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        if (jQuery("#UndadesNegocios").jqGrid('getGridParam', 'records') > 0) {
            mensajesDelSistema("Debe seleccionar las especificaciones a asociar", '250', '150');
        } else {
            mensajesDelSistema("No hay especificaciones por asociar", '250', '150');
        }
    }
}

function guardarvalordigitado() {
    var empresa = $('#nomempresa').val();
    var idinsumo = $('#idinsumo').val();
    var nomMeta = $('#nommeta').val();
    var descMeta = $('#descmeta').val();
    var url = './controller?estado=Auto_&accion=Scoring';
    if (empresa !== '' && nomMeta !== '' && descMeta !== '') {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {
                opcion: 2,
                nombre: nomMeta,
                descripcion: descMeta,
                insumo: idinsumo
            },
            success: function (json) {
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '150');
                        return;
                    }

                    if (json.respuesta === "OK") {
                        //refrescarGridProcesosMeta();
                        cargarComboCategorias();
                        mensajesDelSistema("Se creó la Categoria", '250', '150', true);
                        $('#nommeta').val('');
                        $('#descmeta').val('');
                    }

                } else {
                    mensajesDelSistema("Lo sentimos no se pudo crear la categoria!!", '250', '150');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    } else {
        mensajesDelSistema("Debe llenar todos los campos", '250', '150');
    }

}

function igualarcampo(campo, campo2) {
    var valor = $('#' + campo2).val();
    $('#' + campo).val(valor);
}

function AgregarGridAso() {

    var selectRows = jQuery("#valorespredeterminados").jqGrid('getGridParam', 'selarrrow');
    for (var i = 0; i < selectRows.length; i++) {

        var fila = jQuery("#valorespredeterminados").jqGrid('getRowData', selectRows[i]);
        jQuery("#valorespredeterminados_aso").jqGrid('addRowData', selectRows[i], fila);
    }
    QuitarRowGridVal(selectRows);
}


function QuitarRowGridVal(selectRows) {

    for (var k = 0; k < selectRows.length; k++) {
        jQuery("#valorespredeterminados").jqGrid('delRowData', selectRows[k]);

        selectRows = jQuery("#valorespredeterminados").jqGrid('getGridParam', 'selarrrow');


        k = -1;
    }

}


function AgregarGridNoAso() {

    var selectRows = jQuery("#valorespredeterminados_aso").jqGrid('getGridParam', 'selarrrow'), cadena = "", coma = "";
    for (var i = 0; i < selectRows.length; i++) {

        var fila = jQuery("#valorespredeterminados_aso").jqGrid('getRowData', selectRows[i]);
        //////////////////////////
        var fila1 = jQuery("#valorespredeterminados_aso").getRowData(selectRows[i]);

        if (fila1['idrel'] !== "") {
            cadena = cadena + coma + fila1['valor_xdefecto'];
            coma = ",";
        }

        //////////////////////////
        if (cadena === "") {
            jQuery("#valorespredeterminados").jqGrid('addRowData', selectRows[i], fila);
        }
    }

    if (cadena === "") {
        QuitarRowGridValAso(selectRows);
    } else {
        alert('No se pueden desasociar los valores predeterminados ' + cadena);
    }
}


function QuitarRowGridValAso(selectRows) {

    for (var k = 0; k < selectRows.length; k++) {
        jQuery("#valorespredeterminados_aso").jqGrid('delRowData', selectRows[k]);

        selectRows = jQuery("#valorespredeterminados_aso").jqGrid('getGridParam', 'selarrrow');


        k = -1;
    }

}


function cargarComboInsumos() {
    $('#insumos').html('');
    ;
    $.ajax({
        type: 'POST',
        url: './controller?estado=Auto_&accion=Scoring',
        dataType: 'json',
        data: {
            opcion: 52
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var key in json) {
                        $('#insumos').append('<option value=' + key + '>' + json[key] + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function numeros(e)
{
    var tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8)
        return true;
    var patron = /\d/;
    var te = String.fromCharCode(tecla);
    return patron.test(te);
}

function cargarTablaScoring() {  
    var id_und_negocio = $("#idProMeta").val();
    if (id_und_negocio === '') {
        mensajesDelSistema("DEBE SELECCIONAR LA UNIDAD DE NEGOCIO", '250', '165');
    }else{                 
            $.ajax({
                type: 'POST',
                url: './controller?estado=Auto_&accion=Scoring',
                dataType: 'json',
                data: {
                    opcion: 57,
                    id_unidad_negocio: id_und_negocio
                },              
                async: false,              
                success: function(json) {
                                                     
                        var i = 0, total = 0;
                        $('#tbl_scoring').html('');
                        if (!isEmptyJSON(json)) {                         
                            $('#idProMeta').attr({disabled: true});                            
                            $('#tbl_scoring').append('<tr><th>#</th> <th style="display:none;">Id Conf Pred</th> <th>TIPO</th> <th>VARIABLES MERCADO</th> <th>DESCRIPCION</th> <th>PUNTAJE '+$("#idProMeta option:selected").text()+'</th> <th></th> </tr>');
                            //Agregamos fila para la constante
                            $('#tbl_scoring').append('<tr>');  
                            $('#tbl_scoring tr:last').append('<td colspan="4" align="right"><b>Constante</b></td>');
                            $('#tbl_scoring tr:last').append('<td align="center"> <input name="puntaje" type="text" class="solo-numeric" id=' + 'score_' + i+ ' value= ' + json[0].puntaje_maximo + '> </td>'); 
                            $('#tbl_scoring tr:last').append('<td align="center"> <input name="chk_select" type="checkbox" class="sumatoria" id=' + 'chk_' + i + ' checked readonly> </td>');
                            $('#tbl_scoring').append('</tr>');
                            for (var key in json) {
                                i++;                               
                                $('#tbl_scoring').append('<tr>');                            
                                $('#tbl_scoring tr:last').append('<td align="center">' + i + '</td>');
                                $('#tbl_scoring tr:last').append('<td align="center" style="display:none;">' + json[key].id_conf_pred + '</td>');
                                $('#tbl_scoring tr:last').append('<td align="center">' + json[key].tipo + '</td>');
                                $('#tbl_scoring tr:last').append('<td align="center">' + json[key].variable_mercado + '</td>');
                                $('#tbl_scoring tr:last').append('<td align="center">' + json[key].descripcion + '</td>');
                                $('#tbl_scoring tr:last').append('<td align="center"> <input name="puntaje" type="text" class="solo-numeric" id=' + 'score_' + i + ' value= ' + json[key].puntaje + '> </td>'); 
                                $('#tbl_scoring tr:last').append('<td align="center"> <input name="chk_select" type="checkbox" class="sumatoria" id=' + 'chk_' + i + '> </td>');
                                $('#tbl_scoring').append('</tr>');
                                var item = {};
                                item ["id"] = 'chk_' + i; 
                                item ["id_score"] = 'score_' + i; 
                                item ["puntaje"] = json[key].puntaje; 
                                item ["minimo"] = json[key].minimo;  
                                item ["maximo"] = json[key].maximo;  
                                jsonStatusCheck.push(item);   
                               // total += json[key].sub_total;

                            }
                            setearCheckboxesByType("minimo");

                            $('#tbl_scoring').append('<tr>');
                            $('#tbl_scoring tr:last').append('<td align="right" colspan="4"><b>Min:</b></td>');
                            $('#tbl_scoring tr:last').append('<td align="center"><input name="min_value" type="text" id="min_value" value=' + json[key].valor_min + ' readonly></td>');
                            $('#tbl_scoring tr:last').append('<td align="center"> <input name="min_max" type="radio" value="minimo" checked> </td>');
                            $('#tbl_scoring').append('</tr>');
                            $('#tbl_scoring').append('<tr>');
                            $('#tbl_scoring tr:last').append('<td align="right" colspan="4"><b>Max:</b></td>');
                            $('#tbl_scoring tr:last').append('<td align="center"><input name="max_value" type="text" id="max_value" value=' + json[key].valor_max + ' readonly></td>');
                            $('#tbl_scoring tr:last').append('<td align="center"> <input name="min_max" type="radio" value="maximo"> </td>');
                            $('#tbl_scoring').append('</tr>');
                            $('#tbl_scoring').append('<tr>');
                            $('#tbl_scoring tr:last').append('<td align="right" colspan="4"><b>Cut-Off:</b></td>');
                            $('#tbl_scoring tr:last').append('<td align="center"><input name="cut_off" type="text" id="cut_off" value="" readonly></td>');                      
                            $('#tbl_scoring').append('</tr>');
                            $('#botones_footer').fadeIn("slow");

                        } else {
                             mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');
                        }
                                         

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });            

    } 
}

function setearCheckboxesByType(type){
    $.each(jsonStatusCheck, function (i, item) {
        var checked = (type==='minimo')? (jsonStatusCheck[i].minimo==='S') ? true : false : (jsonStatusCheck[i].maximo === 'S') ? true : false;
        $('#'+jsonStatusCheck[i].id).attr('checked', checked);
        $('#'+jsonStatusCheck[i].id_score).val(jsonStatusCheck[i].puntaje);
    });
}

function validFormData(){  
     var errorMessage = "";
     for (var i=0;i < document.getElementById('tbl_scoring').rows.length -4; i++){   
        
        if ($('#score_'+i).val()===''){
           errorMessage = "Faltan puntajes por llenar";
           break;
        }else if(!isNumber($('#score_'+i).val())){
            errorMessage = "Algunos puntajes son inválidos. Por favor, verifique";
            break;
        }      
     }        
    return errorMessage;
}



function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}


function calcularSumatoriaTablaScoring() {  
     var total = 0;
     for (var i=0;i < document.getElementById('tbl_scoring').rows.length -4; i++){   
        if (document.getElementById('chk_'+i).checked){
           total  = total +parseInt(($('#score_'+i).val()!=='') ? $('#score_'+i).val(): 0);   
        }       
     } 
    return total;
}

function guardarInfoScoring (){    
    var url = './controller?estado=Auto_&accion=Scoring';
    loading("Espere un momento por favor...", "270", "140"); 
    setTimeout(function () {
        $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion:58,                  
                    listadoScoring:guardarScoringJson
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {       
                            $("#dialogLoading").dialog('close');
                            jsonStatusCheck =  [];
                            cargarTablaScoring();
                            mensajesDelSistema("Cambios guardados satisfactoriamente", '250', '150', true);                                 
                        }
                        
                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se pudo realizar la operación!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
    }, 500);    
 }

function guardarScoringJson(){
    var jsonScoring = {"data": [{
                "id_unidad_negocio":$('#idProMeta').val(),
                "puntaje_maximo":$('#score_0').val(),
                "valor_puntaje":($('input[name=min_max]:checked').val()==='minimo') ? $('#min_value').val():$('#max_value').val(),
                "guardar_min_max":$('input[name=min_max]:checked').val(),
                "conf_puntajes": []
            }
     ]};       
     for (var i = 1; i < document.getElementById('tbl_scoring').rows.length - 4; i++) {
        var id_conf_pred = document.getElementById("tbl_scoring").rows[i+1].cells[1].innerHTML;
        var puntaje =  ($('#score_' + i).val() !== '') ? $('#score_' + i).val() : 0;
        var checkedScore = ($('#chk_' + i).is(':checked')) ? "S":"N";       
        var item = {};
        item ["id_conf_pred"] = id_conf_pred;
        item ["puntaje"] = puntaje;  
        item ["checkedScore"] = checkedScore;  
        jsonScoring.data[0].conf_puntajes.push(item);       
    } 
    return JSON.stringify(jsonScoring);  
}

function listarvaloresvariablemercado() {
    var estadoBtn = ($( "#div_especificacion" ).dialog( "option", "title" ) === 'EDITAR ESPECIFICACION')? false:true;
    var url = './controller?estado=Auto_&accion=Scoring'; 
    $('#valorespredeterminados').jqGrid('GridUnload');
    $('#valorespredeterminados2').jqGrid('GridUnload');
    $('#div_valorespredeterminados').fadeIn();
    if ($("#gview_valorespredeterminados").length) {
        refrescarGridvaloresvariablemercado("valorespredeterminados");
    } else {
        jQuery("#valorespredeterminados").jqGrid({
            caption: 'Valores',
            url: url,
            datatype: 'json',
            height: 220,
            width: 300,
            colNames: ['Id', 'Descripcion', 'Inicio', 'Fin', 'Accion'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, key: true},
                {name: 'valor_xdefecto', index: 'valor_xdefecto', sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'inicio', index: 'inicio', sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'fin', index: 'fin', sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '120px', hidden: estadoBtn}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_valorespredeterminados',
            multiselect: false,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false,
                type: "POST",
                data:{
                    opcion:63,
                    id_variable_mercado:$('#idEsp').val(),
                    tipo_entrada: $('#t_entrada').val()
                }
            },
            gridComplete: function () {

                var ids = jQuery("#valorespredeterminados").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    el = "<img src='/fintra/images/botones/iconos/eliminar.gif' align='absbottom'  name='desasignar' id='desasignar' width='15' align: 'center' height='15' title ='Desasignar'  onclick=\"mensajeConfirmAction('Esta seguro de remover el valor seleccionado?','250','175',eliminarLineaGridVP,'" + cl + "','valorespredeterminados');\">";
                    jQuery("#valorespredeterminados").jqGrid('setRowData', ids[i], {actions: el});
                }
            }, 
            ondblClickRow: function (rowid, iRow, iCol, e) {    
             if($( "#div_especificacion" ).dialog( "option", "title" ) === 'EDITAR ESPECIFICACION') jQuery('#valorespredeterminados').editRow(rowid, true);
            },  
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_valorespredeterminados", {add: false, edit: false, del: false, search: false, refresh: false});
        jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
            caption: "Nuevo",
            title: "Agregar nueva fila",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {

                var grid = $("#valorespredeterminados")
                        , rowid = 'neo_' + grid.getRowData().length;
                //alert(rowid);
                grid.addRowData(rowid, {});
                grid.jqGrid('editRow', rowid, true, function () {
                    //  $("input, select", e.target).focus();
                }, null, null, {}, null, null, function (id) {
                    var g = $('#valorespredeterminados')
                            , r = g.jqGrid('getLocalRow', id);
                    if (!r.id) {
                        g.jqGrid('delRowData', id);
                    }
                });
            }
            //position: "first"
        });
        jQuery("#valorespredeterminados").navButtonAdd('#page_tabla_valorespredeterminados', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                guardarValp("valorespredeterminados");
            }
            //position: "first"
        });
        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function listarvaloresvariablemercado2() {
    var estadoBtn = ($( "#div_especificacion" ).dialog( "option", "title" ) === 'EDITAR ESPECIFICACION')? false:true;
    var url = './controller?estado=Auto_&accion=Scoring';
    $('#valorespredeterminados').jqGrid('GridUnload');
    $('#valorespredeterminados2').jqGrid('GridUnload');
    $('#div_valorespredeterminados2').fadeIn();
    if ($("#gview_valorespredeterminados2").length) {
        refrescarGridvaloresvariablemercado("valorespredeterminados2");
    } else {
        jQuery("#valorespredeterminados2").jqGrid({
            caption: 'Valores',
            url: url,
            datatype: 'json',
            height: 220,
            width: 300,
            colNames: ['Id', 'Descripcion', 'Unico', 'Fin', 'Accion'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, key: true},
                {name: 'valor_xdefecto', index: 'valor_xdefecto', sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'inicio', index: 'inicio', sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'fin', index: 'fin', hidden: true, sortable: true, editable: true, align: 'left', width: '600px'},
                {name: 'actions', index: 'actions', resizable: false, align: 'center', width: '120px', hidden:estadoBtn}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            pager: '#page_tabla_valorespredeterminados2',
            multiselect: false,
            reloadAfterSubmit: true,
            pgtext: null,
            pgbuttons: null,
            cellsubmit: 'clientArray',
            editurl: 'clientArray',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {
                async: false,
                type: "POST",
                data: {
                    opcion: 63,
                    id_variable_mercado: $('#idEsp').val(),
                    tipo_entrada: $('#t_entrada').val()
                }
            },
            gridComplete: function () {

                var ids = jQuery("#valorespredeterminados2").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    el = "<img src='/fintra/images/botones/iconos/eliminar.gif' align='absbottom'  name='desasignar' id='desasignar' width='15' align: 'center' height='15' title ='Desasignar'  onclick=\"mensajeConfirmAction('Esta seguro de remover el valor seleccionado?','250','175',eliminarLineaGridVP,'" + cl + "','valorespredeterminados2');\">";
                    jQuery("#valorespredeterminados2").jqGrid('setRowData', ids[i], {actions: el});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {    
                if($( "#div_especificacion" ).dialog( "option", "title" ) === 'EDITAR ESPECIFICACION') jQuery('#valorespredeterminados2').editRow(rowid, true);
            },          
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_valorespredeterminados2", {add: false, edit: false, del: false, search: false, refresh: false});
        jQuery("#valorespredeterminados2").navButtonAdd('#page_tabla_valorespredeterminados2', {
            caption: "Nuevo",
            title: "Agregar nueva fila",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {

                var grid = $("#valorespredeterminados2")
                        , rowid = 'neo_' + grid.getRowData().length;
                //alert(rowid);
                grid.addRowData(rowid, {});
                grid.jqGrid('editRow', rowid, true, function () {
                    //  $("input, select", e.target).focus();
                }, null, null, {}, null, null, function (id) {
                    var g = $('#valorespredeterminados2')
                            , r = g.jqGrid('getLocalRow', id);
                    if (!r.id) {
                        g.jqGrid('delRowData', id);
                    }
                });
            }
            //position: "first"
        });
        jQuery("#valorespredeterminados2").navButtonAdd('#page_tabla_valorespredeterminados2', {
            caption: "Guardar",
            title: "Guardar cambios",
            buttonicon: "ui-icon-save",
            onClickButton: function () {
                guardarValp("valorespredeterminados2");
            }
            //position: "first"
        });
        $("#gs_id").attr('hidden', true);
        $("#gs_actions").attr('hidden', true);
    }
}

function refrescarGridvaloresvariablemercado(op) {
    var url = './controller?estado=Auto_&accion=Scoring';
    jQuery("#" + op).setGridParam({
        datatype: 'json',
        url: url,
        ajaxGridOptions: {
            async: false,
            type: "POST",
            data: {
                opcion: 63,
                id_variable_mercado: $('#idEsp').val()
            }
        }
    });
    jQuery('#' + op).trigger("reloadGrid");
}

function consultarEspecificacion(id) {
    $('#div_especificacion').fadeIn("slow");

    var fila = jQuery("#UndNegocioPro").getRowData(id);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];
    var tipo_dato = fila['tipo_dato'];
    $('#idEsp').val(id);
    $('#nomEspecificacion').val(nombre);
    $('#descEspecificacion').val(descripcion);
    $('#t_entrada').val(tipo_dato);
    //Se deshabilitan controles
    $('#nomEspecificacion').attr({readonly: true});
    $('#t_entrada').attr({disabled: true});

    $("#div_especificacion").dialog("destroy");
    ($('#t_entrada').val()=='1')? listarvaloresvariablemercado2(): listarvaloresvariablemercado();
    var pagerId =  ($('#t_entrada').val()=='1')? '#page_tabla_valorespredeterminados2': '#page_tabla_valorespredeterminados';   
    $(pagerId).find("table.navtable").hide();
//    jQuery("#valorespredeterminados").jqGrid({pgbuttons:false, recordtext: ''});
    AbrirDivConsultarEspecificacion("500", "500");

}

function AbrirDivConsultarEspecificacion(width, height) {
    $("#div_especificacion").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'VISUALIZAR ESPECIFICACION',
        closeOnEscape: false,
        buttons: {           
            "Salir": function () {
                $('#valorespredeterminados1').jqGrid('GridUnload');
                $('#valorespredeterminados2').jqGrid('GridUnload');
                $(this).dialog("destroy");


            }
        },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        }
    });

}

function eliminarLineaGridVP(rowid, tableid){
    if(rowid.match("^neo_")){
        jQuery("#"+tableid).delRowData(rowid);       
        return;
    }
    var url = './controller?estado=Auto_&accion=Scoring';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 64,
            id_variable_mercado: $('#idEsp').val(),
            id_vp: rowid
        },
        async:false,
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }else {      
                    refrescarGridvaloresvariablemercado(tableid);
                }                

            } else {
                mensajesDelSistema("Lo sentimos no se pudo crear la categoria!!", '250', '150');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
  /* jQuery("#"+tableid).delRowData(rowid);
   jQuery("#"+tableid).resetSelection();*/
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function numbersonly(myfield, e, dec)
{
    var key;
    var keychar;
    
    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

// control keys
    if ((key == null) || (key == 0) || (key == 8) ||
            (key == 9) || (key == 13) || (key == 27))
        return true;

// numbers
    else if ((("0123456789-").indexOf(keychar) > -1))
        return true;

// decimal point jump
    else if (dec && (keychar == "."))
    {
        myfield.form.elements[dec].focus();
        return false;
    }
    else
        return false;
}

