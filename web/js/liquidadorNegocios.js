
function pulsar(e){
    var mykey;
    if(e.which) mykey = e.which;// si es moz/webkit
    else mykey = e.keyCode;// internet exploiter
    if(mykey == 13){
        buscardatos();
    }
}
var contr;
function verConvs(controller, tipo_liq){
    contr=controller;
    var selected = document.getElementById("cmbAfiliado").selectedIndex;
    var filtro = document.getElementById("cmbAfiliado").options[selected].value;
 var convenio=document.getElementById("convid").value;
    var p, url = "";
    if(tipo_liq == "MASTER"){
        url = controller+"?estado=Gestion&accion=Condiciones";
        p =  'opcion=busconv&nitprov='+filtro;
    }else if(tipo_liq == "SIMULADOR") {
        url = controller+"?estado=Liquidador&accion=Negocios";
        p =  'opcion=busConvUsuario&nitprov='+filtro;
    }else{
        url = controller+"?estado=Liquidador&accion=Negocios";
        p =  'opcion=busSectUsuario&nitprov='+filtro+'&convenio='+convenio;
    }
    new Ajax.Request(
        url,
        {
            method: 'post',
            parameters: p,
            onLoading: loading,
            onComplete: openWin
        }
    );
}
function verForms(controller){
    contr=controller;
    var selected = document.getElementById("cmbAfiliado").selectedIndex;
    var filtro = document.getElementById("cmbAfiliado").options[selected].value;
    var p, url = "";
    
        url = controller+"?estado=Liquidador&accion=Negocios";
        p =  'opcion=buscarForm&nitprov='+filtro;
    
    new Ajax.Request(
        url,
        {
            method: 'post',
            parameters: p,
            onLoading: loading,
            onComplete: openWin2
        }
    );
}

function llenarDiv(response){
    document.getElementById("spangen").innerHTML = "";
    $("cmbAfiliado").update(response.responseText);
}

var win;
function openWin(response){
    $("spangen").innerHTML = "";
    $('contenido').update(response.responseText);
    $('contenido').style.display='block';
    win= new Window(
    {id: "diving",
        title: "Convenios",
        showEffectOptions: {duration:1},
        hideEffectOptions: {duration:1},
        destroyOnClose: true,
        onClose:closewin,
        resizable: true
    });
    win.show(true);
    win.showCenter();
    win.setContent('contenido', true, false);
    $('contenido').style.visibility='visible';
}

function openWin2(response){
    $("spangen").innerHTML = "";
    $('contenido2').innerHTML=response.responseText;
    $('contenido2').style.display='block';
    win= new Window(
    {id: "diving",
        title: "Formularios",
        showEffectOptions: {duration:1},
        hideEffectOptions: {duration:1},
        destroyOnClose: true,
        onClose:closewin2,
        resizable: true,
        width:400,
        height:300

    });
    win.show(true);
    win.showCenter();
    win.setContent('contenido2', true, false);
    $('contenido2').style.visibility='visible';
}

function closewin(){
    $('contenido').style.display='none';
    $('contenido').style.visibility='hidden';
    $('contenido').innerHTML="";
}
function closewin2(){
    $('contenido2').style.display='none';
    $('contenido2').style.visibility='hidden';
    $('contenido2').innerHTML="";
}

function colocar(convenio, sector, subsector, nomConvenio, nomSector, nomsubsector, facturaTercero, nitTercero, idProvConvenio){
    document.getElementById("hidIdProvConvenio").value = idProvConvenio;
    document.getElementById("convtext").value = nomConvenio;
    document.getElementById("convid").value = convenio;
    document.getElementById("secttext").value = nomSector;
    document.getElementById("sectid").value = sector;
    document.getElementById("subsecttext").value = nomsubsector;
    document.getElementById("subsectid").value = subsector;
    document.getElementById("hidFacturaTercero").value = facturaTercero;
    if(facturaTercero=="t"){
        $("trPlazo").hide();
        $("trTipoCli").show();
        $("trSolicitud").show();
        $("trCiclo").show();
        cargarCiclos(nitTercero, convenio);
    }else{
        $("trPlazo").show();
        $("trTipoCli").hide();
        $("trSolicitud").hide();
        $("trCiclo").hide();
        cargarTitulosValor(convenio);
    }
    win.close();
}

function datosform(numero_solicitud, identificacion, valor_solic, convenio, nomConvenio, facturaTercero, nitTercero,plazo, forma_pago, titulo_valor, cod_sector, cod_subsector, sector, subsector,  idProvConvenio, fianza){
    valor_solic=ReplaceAll(valor_solic,",","");
    document.getElementById("numsolc").value = numero_solicitud;
    document.getElementById("nit").value = identificacion;
    document.getElementById("txtValor").value = valor_solic;
    document.getElementById("convtext").value = nomConvenio;
    document.getElementById("convid").value = convenio;
    document.getElementById("hidFacturaTercero").value = facturaTercero;
    document.getElementById("secttext").value = sector;
    document.getElementById("sectid").value = cod_sector;
    document.getElementById("subsecttext").value = subsector;
    document.getElementById("subsectid").value = cod_subsector;
    document.getElementById("txtNumCuotas").value =plazo;
    document.getElementById("fianza").value =fianza;
    //document.getElementById("forma_pago").value = forma_pago;
    document.getElementById("hidIdProvConvenio").value = idProvConvenio;
    obtenerFechaUltimoPago(identificacion);
    if(facturaTercero=="t"){
        $("trPlazo").hide();
        $("trTipoCli").show();
        $("trSolicitud").show();
        $("trCiclo").show();
        cargarCiclos(nitTercero, convenio);
    }else{
        $("trTipoCli").hide();
        $("trSolicitud").hide();
        $("trCiclo").hide();
        cargarTitulosValor(convenio,titulo_valor );
        sleep(250);
        //validaPreaprobado(); /// JPACOSTA. Habilitar plazos sin ser preaprobado
        $("trPlazo").show();
    }
    win.close();
}


function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}


function habilitar_plazos(sw)
{

    var j = jQuery.noConflict();
    j("#trPlazo option").each(function(){
        if( j(this).val()==60||j(this).val()==90 )
        {
            if(sw)
            {
                j(this).css("display","");
            }
           else
        {
            j(this).css("display","none");

        }
    
        }

    });

}

function validaPreaprobado()
{
    var j = jQuery.noConflict();
    var controller =j("#CONTROLLER").val();
    var url = controller+'?estado=Liquidador&accion=Negocios';
    j.ajax({
        dataType: 'html',
        url:url,
        data: {
            opcion : 'valida_preaprobados',
            nit : j("#nit").val(),
            convenio : j("#convid").val()
        },
        method: 'post',
        success:  function (resp){
           if(Boolean(resp)==true)
               {
                   habilitar_plazos(true);
               }
               else
                   {
                       habilitar_plazos(false);
                   }
        },
        error: function(resp){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });
}


function errorEnvio(){
    document.getElementById("spangen").innerHTML = "";
    Dialog.alert("Error enviando los datos al servidor. Verifique que la sesion no este cerrada",{
                width:250,
                height:100,
                windowParameters: {className: "alphacube"}
    });
}

function cerrarDiv(){
    win.close();
}

function mostrarCamposRemesa(){
    if($F("cmbTituloValor").split("_")[1] == "t"){
        $("trBanco").show();
        $("trCiudadCh").show();
    } else {
        $("trBanco").hide();
        $("trCiudadCh").hide();
    }
}

function enviarFormLiquidador(op_liq){
    var enviar = true;
    var mensaje= "Error: debe llenar todos los campos";
    if($F("cmbAfiliado")!="" && $F("convid")!="" && $F("sectid")!="" && $F("subsectid")!="" && ($F("tipo_liq")=="SIMULADOR" || $F("tipo_liq")!="SIMULADOR" && $F("nit")!="")
       && $F("txtNumCuotas")!="" && $F("txtValor")!="" && $F("fechainicio")!="" && $F("cmbTituloValor")!=""){

         if($F("cmbTituloValor").split("_")[1] == "t"){
            if($F("cmbBanco")=="" || $F("txtCiudadCheque")==""){
                enviar=false;
            }
         }
         if($F("hidFacturaTercero")=="t"){
             if($F("cmbTipoCliente")=="" || $F("txtSolicitud")=="" || $F("cmbCiclo")==""){
                 enviar=false;
             }
         }else{
             if($F("forma_pago")==""){
                 enviar=false;
             }
         }
       /*if (!validarConfiguracionFianza()) {
            enviar = false;
            mensaje = "No existe valor de factor de fianza configurado para dicho plazo y linea de negocio";
        }*/
    }else{
        enviar=false;
    }

    if(enviar){
        if( $F("tipo_liq")!="SIMULADOR"){
            var solc=$F("numsolc");
            var idprovconv=$F("hidIdProvConvenio");
            var p, url = "";

            url = contr+"?estado=Liquidador&accion=Negocios";
            p =  'opcion=validartasa&solc='+solc+'&idprovconv='+idprovconv;

            new Ajax.Request(
                url,
                {
                    method: 'post',
                    parameters: p,
                    onComplete: function (resp){
                        var res= resp.responseText.replace(/^\s+/g,'').replace(/\s+$/g,'');                        
                        if(res=="E"){
                            Dialog.alert("Error: La tasa de inter�s ha cambiado, debe comunicarse con su asesor en Fintravalores",{
                                width:250,
                                height:100,
                                windowParameters: {
                                    className: "alphacube"
                                }
                            });
                        }else{
                            if(res=="N"){
                                document.getElementById("tasa").value="N";
                            }
                            if(op_liq=="PERF")
                                {
                                    if(validar_fecha())
                                        {
                                             habilitar();
                                             document.forms[0].submit();
                                        }                                       
                                }
                                else
                                    {
                                           habilitar();
                                           document.forms[0].submit();
                                    }
                         
                        }
                    }
                }
                );
        }else{
            document.forms[0].submit();
        }
    }else{
        Dialog.alert(mensaje,{
            width:250,
            height:100,
                windowParameters: {className: "alphacube"}
        });
    }
}

function validar_monto() {
    var monto_inicial = document.getElementById("monto").value;
    var monto_final = document.getElementById("txtValor").value;
   if(parseFloat(monto_final)> parseFloat(monto_inicial))
       {
           alert("El monto a liquidar debe ser menor o igual al monto aprobado");
           document.getElementById("txtValor").value=monto_inicial;

       }
}

function validar_fecha() {
    var sw=true;
    var fecha_inicial = document.getElementById("fecha").value;
         var fecha_final = document.getElementById("fechainicio").value;

	var fec_tem=fecha_inicial


	fecha_inicial=new Date(fecha_inicial);
        fecha_final=new Date(fecha_final);


	if (fecha_final<fecha_inicial)
	{

		alert("La fecha debe ser mayor a la fecha de liquidacion anterior");
                document.getElementById("fechainicio").value=fec_tem;
	}

   return sw;
}





function habilitar() {
    frm = document.forms['formulario'];
    for(i=0; ele=frm.elements[i]; i++)
        ele.disabled=false;
}

function soloNumeros(id) {
             var precio = document.getElementById(id).value;
             precio =  precio.replace(/[^0-9^.]+/gi,"");
             document.getElementById(id).value = precio;
        }

function borrarSeleccion(){
    document.getElementById("convtext").value = "";
    document.getElementById("convid").value = "";
    document.getElementById("secttext").value = "";
    document.getElementById("sectid").value = "";
    document.getElementById("subsecttext").value = "";
    document.getElementById("subsectid").value = "";
    document.getElementById("hidFacturaTercero").value = "";
    document.getElementById("numsolc").value = "";
    document.getElementById("nit").value = "";
    document.getElementById("txtValor").value = "";
    $("trPlazo").hide();
    $("trTipoCli").hide();
    $("trSolicitud").hide();
    $("trCiclo").hide();
}



function colocar2(sector, subsector,  nomSector, nomsubsector,  idProvConvenio){
    document.getElementById("hidIdProvConvenio").value = idProvConvenio;
    document.getElementById("secttext").value = nomSector;
    document.getElementById("sectid").value = sector;
    document.getElementById("subsecttext").value = nomsubsector;
    document.getElementById("subsectid").value = subsector;
    win.close();
}



function ReplaceAll(Source,stringToFind,stringToReplace){

    var temp = Source;

    var index = temp.indexOf(stringToFind);

    while(index != -1){

        temp = temp.replace(stringToFind,stringToReplace);

        index = temp.indexOf(stringToFind);

    }

    return temp;

}

function validarConfiguracionFianza(){
    var convenio=$F("convid");
    var fianza=$F("fianza");
    var plazo=$F("txtNumCuotas");
    var continuar = true;  
    if(fianza ==='S'){
        var url = contr + "?estado=Liquidador&accion=Negocios";
        var p = 'opcion=existeConfigFianza&idConvenio=' + convenio + '&plazo=' + plazo;
        new Ajax.Request(
                url,
                {
                    method: 'post',
                    asynchronous: false,
                    parameters: p,
                    onLoading: loading,
                    onSuccess: function (transport) {
                        var existeConfig = transport.responseText; 
                        if (existeConfig.startsWith("NO")) {
                            continuar = false;                            
                        }
                    },
                    onFailure: errorEnvio
                }
        );
    }
   return continuar;          
}
