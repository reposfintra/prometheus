/* JPACOSTA. Febrero 2014 */
//initAsig(); /*inicializa eventos en AsignacionCartera.jsp*/

function initAsig() {

    $('#buscar').click(function() {
        buscarNegocios(true, 'agente');
    });
    $('#aceptar').click(function() {
        asignar('agente');
    });
    cargarFiltros(true, true, false, true, true, false);
    buscarNegocios(false, 'agente');

    var p = document.getElementById("periodos");
    var fecha = new Date();
    p.value = fecha.getFullYear() + ((fecha.getMonth() < 9) ? '0' : '') + (1 + fecha.getMonth());
    p.disabled = 'true';
    $('#reasignacion').click(function() {
        document.getElementById('asesoresR').style.display = (this.checked)?'':'none';
    });
}

function initAgen() {

    $('#buscar').click(function() {
        buscarNegocios(true, 'agente_campo');
    });
    $('#aceptar').click(function() {
        asignar('agente_campo');
    });
    $('#fichas').click(function() {       
        previsualizarFichas();      
    });
    cargarFiltros(true, true, false, true, false, true);
    buscarNegocios(false, 'agente_campo');

    var p = document.getElementById("periodos");
    var fecha = new Date();
    p.value = fecha.getFullYear() + ((fecha.getMonth() < 9) ? '0' : '') + (1 + fecha.getMonth());
    p.disabled = 'true';
    $('#reasignacion').click(function() {
        document.getElementById('asesoresR').style.display = (this.checked)?'':'none';
    });
 
   // $("#tramoAnterior").html( $('#vencMayor').html());
}

function cargarFiltros(unidades, agencias, periodos, vencimientos, asesores, agCampo) {
    $.ajax({
        url: './controller?estado=Asignacion&accion=Cartera&opcion=0'
                + '&unidades=' + unidades + '&periodos=' + periodos
                + '&vencimiento=' + vencimientos + '&asesores=' + asesores 
                + '&agentes=' + agCampo + '&agencias=' + agencias,
        datatype: 'json',
        type: 'get',
        success: function(json) {
            var elemento;
            var opcion;
            if (json.mensaje) {
                alert(json.mensaje);
                return;
            }
            for (var id in json) {
                try {
                    elemento = $('#' + id);
                    for (var index in json[id]) {
                        try {
                            opcion = document.createElement('option');
                            opcion.id = index;
                            opcion.innerHTML = json[id][index];
                            elemento.append(opcion);
                        } catch (exception) {
                            alert('error :' + index + ' > ' + json[id][index]);
                            continue;
                        }
                    }
                } catch (exception) {
                    alert('error : ' + index + '>' + json[id][index]);
                    continue;
                }
            }
            if (json.asesores) {
                elemento = $('#asesoresR');
                for (var index in json.asesores) {
                    try {
                        opcion = document.createElement('option');
                        opcion.id = index;
                        opcion.innerHTML = json.asesores[index];
                        elemento.append(opcion);
                    } catch (exception) {
                        alert('error :' + index + ' > ' + json.asesores[index]);
                        continue;
                    }
                }
            } else if (json.agentes) {
                elemento = $('#asesoresR');
                for (var index in json.agentes) {
                    try {
                        opcion = document.createElement('option');
                        opcion.id = index;
                        opcion.innerHTML = json.agentes[index];
                        elemento.append(opcion);
                    } catch (exception) {
                        alert('error :' + index + ' > ' + json.agentes[index]);
                        continue;
                    }
                }
            } else if (json.agencias) {
                elemento = $('#agencias');
                for (var index in json.agencias) {
                    try {
                        opcion = document.createElement('option');
                        opcion.id = index;
                        opcion.innerHTML = json.agencias[index];
                        elemento.append(opcion);
                    } catch (exception) {
                        alert('error :' + index + ' > ' + json.agencias[index]);
                        continue;
                    }
                }
            }
        },
        error: function(result) {
            alert(result);
        }
    });
}

function buscarNegocios(cargar, tipo) {
    if (cargar) {
        var unegocio = $('#uNegocios').find('option:selected').attr('id');
        var agencia = $('#agencias').val();
        var periodo = ($('#periodos').val() === '') ? 'undefined' : $('#periodos').val();
        var vencM = $('#vencMayor').find('option:selected').attr('id');
        var reasig = (document.getElementById('reasignacion').checked) ? $('#asesoresR').find('option:selected').attr('id') : 'vacio'; 
        
        jQuery('#negocios').setGridParam({
            url: './controller?estado=Asignacion&accion=Cartera&opcion=1'
                    + '&unegocio=' + unegocio + '&periodo=' + periodo + '&rangoMayor=' + vencM
                    + '&reasigna=' + reasig + '&tipo='+tipo + '&agencia=' + agencia,
            mtype: 'get',
            datatype: 'json',
            async: 'false',
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            loadError: function(xhr, status, error) {
                alert(error);
            },
            loadComplete: function(json) {
                if (json.mensaje) {
                    alert(json.mensaje);
                    return;
                }
                cacularTotales();
                try {
                    if(json && json.asignados) {
                        var div = document.getElementById('estado');
                        div.cantidad = json.asignados.negocios;
                        div.valor = json.asignados.valor;
                        div.innerHTML='Asignado(s) '+div.cantidad+', por valor de '+(new Number(div.valor)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                    }
                } catch(exception) { alert('load no completo: '+exception.message);}
            }
        }).trigger("reloadGrid"); 
    } else {
        var tabla = jQuery("#negocios");
        tabla.jqGrid({
            caption: 'Negocios',
            datatype: 'local',
            data: {},
            width: 1500,
            height: 500,
            rowTotal: 500000,
            rowNum: 100,
            rowList: [25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 500],
            pager: $('#page'),
            viewrecords: true,
            multiselect: true,
            gridview: true,
            hidegrid: true,
            rownumbers: true,
            shrinkToFit: true,
            footerrow: true,
            loadonce: true,
            colNames: ['Negocio', 'Cedula', 'Nombre', 'Departamento', 'Cuotas vencidas', 'Saldo vencido', 'Vencimiento mayor'],
            colModel: [
                {name: 'negocio', index: 'negocio', sortable: true, key:true},
                {name: 'cedula', index: 'cedula', sortable: true, align: 'right'},
                {name: 'nombre', index: 'nombre', sortable: true},
                {name: 'departamento', index: 'departamento', sortable: true},
                {name: 'cuotas_vencidas', index: 'cuotas_vencidas', sortable: true, align: 'right'},
                {name: 'saldo_vencido', index: 'saldo_vencido', sortable: true, formatter: 'number', align: 'right'},
                {name: 'vencimiento_mayor', index: 'vencimiento_mayor', sortable: true}
            ],
            onCellSelect: function(rowid, iCol, cellcontent, e) {
                cacularTotales();
            }
        });
        tabla.jqGrid('filterToolbar',
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    multipleSearch: true
                });
        tabla.jqGrid('gridResize', {minWidth: 1500, minHeight: 500});
        try {
            $('#cb_negocios').click(function() {
                cacularTotales();
            });
            var div = document.createElement('DIV');
            div.id='estado';
            div.name='estado';
            div.style='font-weight:normal; height:19px; margin-top:3px; margin-left:4px;';
            div.cantidad = 0;
            div.valor = 0;
            $('#page_left').append(div);
        } catch(exception) {alert(exception);}
    }
}

function cacularTotales() {
    var grid = jQuery('#negocios');

    var saldoVencido = grid.jqGrid('getCol', 'saldo_vencido', false, 'sum');
    var negocios = grid.jqGrid('getGridParam', 'selarrrow');
    var seleccionado = 0;
    for (var neg = 0; neg < negocios.length; neg++) {
        seleccionado += parseInt(grid.jqGrid('getRowData', negocios[neg]).saldo_vencido);
    }
    grid.jqGrid('footerData', 'set', {
        valor_05: 'seleccionados',
        valor_06: negocios.length,
        valor_07: 'Total Seleccionado',
        valor_08: (new Number(seleccionado)).toFixed(2).replace('.',',').replace(/\d(?=(\d{3})+\,)/g, '$&.'),
        valor_09: 'Total:',
        valor_10: saldoVencido
    });

}

function asignar(tipo) {
    var periodo = $('#periodos').val();
    var asesor = (tipo==='agente')
                 ? $('#asesores').find('option:selected').attr('id')
                 : $('#agentes').find('option:selected').attr('id');
    if ((!asesor || asesor === 'vacio')) {
        if (!document.getElementById('reasignacion').checked) {
            alert('Seleccione un agente');
            return;
        } else {
            if(!confirm("�Confirma remover las asignaciones a la cartera seleccionada?")){
                return;
            }
        }
    }
    var json = "";
    var negocios = jQuery("#negocios").jqGrid('getGridParam', 'selarrrow');
    if (negocios.length === 0) {
        alert('Seleccione al menos un negocio');
        return;
    }
    for (var i = 0; i < negocios.length; i++) {
        negocios[i] = "'" + negocios[i] + "'";
    }
    json += negocios.join(",");
    json += "";
    $.ajax({
        url: './controller?estado=Asignacion&accion=Cartera&opcion=2',
        datatype: 'json',
        type: 'post',
        data: {"negocios": json, "asesor": asesor, "periodo": periodo, "tipo": tipo},
        success: function(json) {
            try {
                if (json.mensaje) {
                    alert(json.mensaje);
                    return;
                }
                var div = document.getElementById('estado');
                for (var x in json) {
                    jQuery("#negocios").delRowData(x);
                    if(!document.getElementById('reasignacion').checked) {
                        div.cantidad += 1;
                        div.valor += parseInt(json[x].valor_asignado);
                    }
                }
                div.innerHTML= 'Asignado(s) '+div.cantidad+', por valor de '+(new Number(div.valor)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                jQuery("#negocios").trigger("reloadGrid");
                cacularTotales();
            } catch (exception) {
                alert(exception.message);
            }
        },
        error: function(result) {
            alert('error : ' + result);
        }
    });
}
    
function generarFichas() {
    var periodo = $('#periodos').val();
    var unegocio = $('#uNegocios').find('option:selected').attr('id');
    var vencM = $('#vencMayor').find('option:selected').attr('id');
    var asesor = $('#agentes').find('option:selected').attr('id');
    if ((!asesor || asesor === 'vacio')) {
        alert('Seleccione un agente');
        return;
    } loading("Espere un momento por favor...", "270", "140");
    var wFichas, fha;
    $.ajax({
        url: './controller?estado=Asignacion&accion=Cartera&opcion=3',
        datatype: 'json',
        type: 'post',
        data: {"asesor": asesor, "periodo": periodo, "negocio":unegocio, "venci":vencM},
        success: function(json) {
            try {
                if (json.mensaje) {
                    modalMessage(json.mensaje, "270", "140");
                    return;
                } else {
                    unegocio = $('#uNegocios').find('option:selected').attr('value');
                    if(jQuery.isEmptyObject(json)) {
                        $("#dialogo2").dialog('close');
                        alert("Sin fichas asignadas para el agente "+asesor+" en la linea de negocio "+unegocio);
                    } else {
                        wFichas = window.open('',asesor);
                        wFichas.document.getElementsByTagName('head')[0].innerHTML = 
                                '<link href="http://'+window.location.hostname+':'+window.location.port
                                +'/fintra/css/ex/imprimible.css" rel="stylesheet" type="text/css" media="screen, print">';

                        fha = wFichas.document.createElement('div');
                        fha.className='ficha noPrint';
                        fha.innerHTML = "<button class='noPrint' onclick='window.print();'>Imprimir Fichas</button>    <b id='nfichas'></b>";
                        wFichas.document.getElementsByTagName('body')[0].appendChild(fha);
                    
                        fha = wFichas.document.createElement('br');
                        fha.className='noPrint';
                        wFichas.document.getElementsByTagName('body')[0].appendChild(fha);
                    
                        var nav = (navigator.userAgent.indexOf('Firefox') !=-1) ? 'moz':'oth';
                        var salto = false; var n = 0;
                        for(var i in json) {
                            n++;
                            fha = wFichas.document.createElement('div');
                            fha.className='ficha '+nav;
                            fha.innerHTML = ficha(json[i], n, unegocio);
                            wFichas.document.getElementsByTagName('body')[0].appendChild(fha);

                            fha = wFichas.document.createElement('div');
                            fha.className='salto '+salto;
                            salto = !salto;
                            wFichas.document.getElementsByTagName('body')[0].appendChild(fha);
                        }
                        wFichas.document.getElementById('nfichas').innerHTML = 'Numero de fichas '+n;
                        $("#dialogo2").dialog('close');
                    }
                }
            }catch(exception) { $("#dialogo2").dialog('close'); modalMessage(exception.message, "270", "140");}
        }
    });
    
    //wFichas.print();
}

function previsualizarFichas() {
    var periodo = $('#periodos').val();
    var unegocio = $('#uNegocios').find('option:selected').attr('id');
    var vencM = $('#vencMayor').find('option:selected').attr('id');
    var asesor = $('#agentes').find('option:selected').attr('id');
    var juridica=(document.getElementById('juridica').checked)?"S":"N";
    var ciclo=$('#numciclo').val();
    var agencia = $('#agencias').val();

    if ((!asesor || asesor === 'vacio')) {
        alert('Seleccione un agente');
        return;
    } loading("Espere un momento por favor...", "270", "180");
    
    $.ajax({
        url: './controller?estado=Asignacion&accion=Cartera&opcion=5',
        datatype: 'json',
        type: 'post',
        data: {
            "asesor": asesor,
            "periodo": periodo,
            "negocio": unegocio,
            "venci": vencM,
            "juridica":juridica,
            "ciclo":ciclo,
            "agencia": agencia
        },
        success: function(json) {
            var arr = [];
            var ColModel = [];
            if (json.error) {
                $("#dialogo2").dialog('close');
                modalMessage(json.error, "270", "180");
            } else {
                for (var obj in json) {
                    if (json.hasOwnProperty(obj)) {
                        for (var prop in json[obj]) {
                            if (json[obj].hasOwnProperty(prop)) {
                                console.log(prop + " :" + json[obj][prop]);
                                arr.push(prop);
                                if (prop === 'cod_neg') {//variable que sera la llave
                                    ColModel.push({name: prop, index: prop, align: 'center', width: 120, search: true, key: true});
                                } else {
                                    ColModel.push({name: prop, index: prop, align: 'center', width: 120, search: true});
                                }
                            }
                        }
                    }
                    break;
                }
                console.log(arr + " ----> " + ColModel);
                crearJqGrid(arr, ColModel, "Lista negocios", "pager_fichas", json, "tabla_fichas", '1000', '400', true);

                $("#ventamaEmergente").dialog({
                    width: 1050,
                    height: 600,
                    show: "scale",
                    hide: "scale",
                    resizable: true,
                    position: "center",
                    buttons: {//crear bot�n de cerrar
                        "Salir": function () {
                            $(this).dialog("close");
                        },
                        "Ver fichas": function () {
                            generaFichasdefinitivo();
                        }
                    }
                });

                $("#dialogo2").dialog('close');
            }
        },
        error: function(error) {
            $("#dialogo2").dialog('close');
            modalMessage(error.error, "270", "180");
        }        
    });
    
   
}

function generaFichasdefinitivo(){
   
    var asesor = $('#agentes').find('option:selected').attr('id');
    var unegocio = $('#uNegocios').find('option:selected').attr('id');
    
    var i, selRowIds = $("#tabla_fichas").jqGrid("getGridParam", "selarrrow"), rowData;
    var sortColumnName = $("#tabla_fichas").jqGrid('getGridParam','sortname');
    var sortOrder = $("#tabla_fichas").jqGrid('getGridParam','sortorder');
    var agencia = $('#agencias').val();
    
    if (selRowIds.length > 0) {

        loading("Espere un momento generando fichas...", "270", "140");
        var arrayJson = new Array();
        for (i = 0; i < selRowIds.length; i++) {
            rowData = $("#tabla_fichas").jqGrid("getRowData", selRowIds[i]);
            arrayJson.push(rowData);
        }

        console.log(JSON.stringify(arrayJson));
        var wFichas, fha;
        $.ajax({
            url: './controller?estado=Asignacion&accion=Cartera&opcion=3',
            datatype: 'json',
            type: 'post',
            data: {
                "listado": JSON.stringify(arrayJson),
                "campoOrden": sortColumnName,
                "agencia": agencia,
                "criterio": sortOrder
            },
            success: function (json) {
                try {
                    if (json.mensaje) {
                        alert(json.mensaje);
                        return;
                    } else {
                        unegocio = $('#uNegocios').find('option:selected').attr('value');
                        if (jQuery.isEmptyObject(json)) {
                            $("#dialogo2").dialog('close');
                            alert("Sin fichas asignadas para el agente " + asesor + " en la linea de negocio " + unegocio);
                        } else {
                            wFichas = window.open('', asesor);
                            wFichas.document.getElementsByTagName('head')[0].innerHTML =
                                    '<link href="http://' + window.location.hostname + ':' + window.location.port
                                    + '/fintra/css/ex/imprimible.css" rel="stylesheet" type="text/css" media="screen, print">';

                            fha = wFichas.document.createElement('div');
                            fha.className = 'ficha noPrint';
                            fha.innerHTML = "<button class='noPrint' onclick='window.print();'>Imprimir Fichas</button>    <b id='nfichas'></b>";
                            wFichas.document.getElementsByTagName('body')[0].appendChild(fha);

                            fha = wFichas.document.createElement('br');
                            fha.className = 'noPrint';
                            wFichas.document.getElementsByTagName('body')[0].appendChild(fha);

                            var nav = (navigator.userAgent.indexOf('Firefox') != -1) ? 'moz' : 'oth';
                            var salto = false;
                            var n = 0;
                            for (var i in json) {
                                n++;
                                fha = wFichas.document.createElement('div');
                                fha.className = 'ficha ' + nav;
                                fha.innerHTML = ficha(json[i], n, unegocio);
                                wFichas.document.getElementsByTagName('body')[0].appendChild(fha);

                                fha = wFichas.document.createElement('div');
                                fha.className = 'salto ' + salto;
                                salto = !salto;
                                wFichas.document.getElementsByTagName('body')[0].appendChild(fha);
                            }
                            wFichas.document.getElementById('nfichas').innerHTML = 'Numero de fichas ' + n;
                            $("#dialogo2").dialog('close');
                        }
                    }
                } catch (exception) {
                    $("#dialogo2").dialog('close');
                    alert(exception.message);
                }
            }
        });

    }
}

/*
 * Grid estandar para visualizar datos con exportar excel
 * 
 * @param {type} arrNames
 * @param {type} arrColModel
 * @param {type} caption
 * @param {type} pager
 * @param {type} json
 * @param {type} name_table
 * @param {type} width
 * @param {type} height
 * @returns {undefined}
 */

function crearJqGrid(arrNames,arrColModel,caption,pager,json,name_table,width,height,multiselect) {
    
    var grid = jQuery("#" + name_table);
    grid.jqGrid("clearGridData", true);
    if ($("#gview_" + name_table).length) {
        grid.setGridParam({
            datatype: 'local',
            data: json,
            autowidth: true
        });
        grid.trigger("reloadGrid");
    } else {
        grid.jqGrid({
            caption: caption,
            datatype: 'local',
            data: json,
            height: height,
            width: width,
            colNames: arrNames,
            colModel: arrColModel,
            rowNum: 10000,
            rowTotal: 10000000,
            pager: pager,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            multiselect: multiselect,
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#" + pager, {add: false, edit: false, del: false, search: false, refresh: false}, {});
    }
}

function ficha(datos, numero, linea) {
    var html='';
    html = '<div class="titulo">'
    +'    <table>'
    +'        <tr><th>Negocio:</th><td>'+datos.cod_neg+'</td>'
    +'            <th>Gestor:</th><td>'+datos.agente+'</td>'
    +'            <th>Linea Credito:</th><td>'+linea+'</td>'
    +'            <th>Vencimiento Mayor:</th><td>'+datos.vencimiento_mayor+'</td>'
    +'            <th>Ficha No.: '+numero+'</th>'
    +'        </tr>'
    +'    </table>'
    +'</div>';

    html += '<fieldset>'
    +'    <legend>DEUDOR PRINCIPAL</legend>'
    +'    <table>'
    +'        <tr><th>CEDULA</th><td>'+datos.deudor.cedula+'</td><th>NOMBRE</th><td>'+datos.deudor.nombre+'</td><th>DIRECCION</th><td>'+datos.deudor.direccion+'</td></tr>'
    +'        <tr><th>DEPARTAMENTO</th><td>'+datos.deudor.departamento+'</td><th>CIUDAD</th><td>'+datos.deudor.ciudad+'</td><th>BARRIO</th><td>'+datos.deudor.barrio+'</td></tr>'
    +'        <tr><th>CELULAR</th><td>'+datos.deudor.celular+'</td><th>TELEFONO</th><td>'+datos.deudor.telefono+'</td><th>TELEFONO</th><td>'+datos.deudor.telefono2+'</td></tr>'
    +'    </table>'
    +'    <table>'
    +'        <tr><th>CONYUGE</th><th>CEDULA</th><td>'+datos.deudor.idCony+'</td><th>NOMBRE</th><td>'+datos.deudor.nomCony+'</td><th>Direccion</th><td>'+datos.deudor.direccionCony+'</td><th>TELEFONO</th><td>'+datos.deudor.telefonoCony+'</td><th>CELULAR</th><td>'+datos.deudor.celularCony+'</td></tr>'
    +'    </table>';
    if(!datos.cartera) { datos.cartera = eval('({})');}
    html +='    <table>'
    +'        <tr><th>DIA PAGO</th><td>'+(datos.cartera.dia_pago||'')+'</td><th>CUOTAS VENCIDAS</th><td>'+(datos.cartera.cuotas_vencidas||'')+'<th>VALOR CUOTA</th><td>'+(datos.cartera.valor_cuota||'')+'</td>'+'</td><th>SALDO VENCIDO</th><td>'+(datos.cartera.saldo_vencido||'')+'</td><th>TOTAL SALDO</th><td>'+(datos.cartera.total||'')+'</td>'
    +'            <th>F.U.P.</th><td>'+(datos.cartera.fecha_ult_pago||'')+'</td><th>V.U.P</th><td>'+(datos.cartera.vlr_ult_pago||'')+'</td></tr>'
    +'    </table>'
    
    +'</fieldset>';
    
    html += '<fieldset>'
    +'    <legend>INFORMACION LABORAL DEUDOR PRINCIPAL</legend>'
    +'    <table>'
    +'        <tr><th>NOMBRE EMPRESA</th><td>'+datos.deudor.nom_emp+'</td><th>DEPARTAMENTO</th><td>'+datos.deudor.dep_emp+'</td><th>CIUDAD</th><td>'+datos.deudor.ciu_emp+'</td></tr>'
    +'        <tr><th>DIRECCION</th><td>'+datos.deudor.dir_emp+'</td><th>BARRIO</th><td>'+datos.deudor.bar_emp+'</td><th>TELEFONO</th><td>'+datos.deudor.tel_emp+'</td></tr>'
    +'    </table>'
    +'</fieldset>';
    if(!datos.codeudor) { datos.codeudor = eval('({})');}
    html += '<fieldset>'
    +'    <legend>DEUDOR SOLIDARIO</legend>'
    +'    <table>'
    +'        <tr><th>CEDULA</th><td>'+(datos.codeudor.cedula||'')+'</td><th>NOMBRE</th><td>'+(datos.codeudor.nombre||'')+'</td><th>DIRECCION</th><td>'+(datos.codeudor.direccion||'')+'</td></tr>'
    +'        <tr><th>DEPARTAMENTO</th><td>'+(datos.codeudor.departamento||'')+'</td><th>CIUDAD</th><td>'+(datos.codeudor.ciudad||'')+'</td><th>BARRIO</th><td>'+(datos.codeudor.barrio||'')+'</td></tr>'
    +'        <tr><th>CELULAR</th><td>'+(datos.codeudor.celular||'')+'</td><th>TELEFONO</th><td>'+(datos.codeudor.telefono||'')+'</td><th>TELEFONO</th><td>'+(datos.codeudor.telefono2||'')+'</td></tr>'
    +'    </table>'
    +'</fieldset>'
    
    +'<fieldset>'
    +'    <legend>INFORMACION LABORAL DEUDOR SOLIDARIO</legend>'
    +'    <table>'
    +'        <tr><th>NOMBRE EMPRESA</th><td>'+(datos.codeudor.nom_emp||'')+'</td><th>DEPARTAMENTO</th><td>'+(datos.codeudor.dep_emp||'')+'</td><th>CIUDAD</th><td>'+(datos.codeudor.ciu_emp||'')+'</td></tr>'
    +'        <tr><th>DIRECCION</th><td>'+(datos.codeudor.dir_emp||'')+'</td><th>BARRIO</th><td>'+(datos.codeudor.bar_emp||'')+'</td><th>TELEFONO</th><td>'+(datos.codeudor.tel_emp||'')+'</td></tr>'
    +'    </table>'
    +'</fieldset>';

    html += '<fieldset>'
    +'    <legend>REFERENCIAS FAMILIARES</legend>'
    +'    <table>'
    +'        <tr><th>PARENTESCO</th><th>NOMBRE</th><th>CELULAR</th><th>TELEFONO</th></tr>';
    var ref;
    for(var i in datos.referencias.familiar) {
        ref = datos.referencias.familiar[i];
        html += '        <tr><td>'+ref.parentesco+'</td><td>'+ref.nombre+'</td><td>'+ref.celular+'</td><td>'+ref.telefono+'</td></tr>';
    }
    html += '    </table>'
    +'</fieldset>'

    +'<fieldset>'
    +'    <legend>REFERENCIAS PERSONALES</legend>'
    +'    <table>'
    +'        <tr><th>CEDULA</th><th>NOMBRE</th><th>CELULAR</th><th>TELEFONO</th></tr>';
    for(var i in datos.referencias.personal) {
        ref = datos.referencias.personal[i];
        html += '        <tr><td>'+ref.parentesco+'</td><td>'+ref.nombre+'</td><td>'+ref.celular+'</td><td>'+ref.telefono+'</td></tr>';
    }
    html += '    </table>'
    +'</fieldset>'

    +'<fieldset>'
    +'    <legend>INFORMACION NEGOCIO</legend>'
    +'    <table>'
    +'        <tr><th>DEPARTAMENTO</th><td>'+datos.negocio.departamento+'</td><th>CIUDAD</th><td>'+datos.negocio.ciudad+'</td></tr>'
    +'        <tr><th>DIRECCION</th><td>'+datos.negocio.direccion+'</td><th>BARRIO</th><td>'+datos.negocio.barrio+'</td></tr>'
    +'    </table>'
    +'</fieldset>'

    +'<fieldset>'
    +'    <legend>ACTUALIZACION DE INFORMACION CLIENTE</legend>'
    +'    <table>'
    +'        <tr><th>DEPARTAMENTO</th><td></td><th>MUNICIPIO</th><td></td><th>DIRECCION</th><td></td></tr>'
    +'        <tr><th>BARRIO</th><td></td><th>CELULAR</th><td></td><th>TELEFONO</th><td></td></tr>'
    +'    </table>'
    +'</fieldset>';
    return html;
}

function resumen(tipo) {               
    var periodo = $('#periodos').val();
    var unegocio = $('#uNegocios').find('option:selected').attr('id');
    if (!periodo || !tipo || !unegocio) {
        alert('error parametrizacion');
        return;
    }
    $("#dialogo").dialog({
        width: 700,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center"/*,
        modal: "true",
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {$(this).dialog("close");}
        }*/
    });
    $('#dialogo').html("Cargando...");

    var respuesta = "";
    $.ajax({
        url: './controller?estado=Asignacion&accion=Cartera&opcion=4',
        datatype: 'json',
        type: 'post',
        data: {"tipo": tipo, "periodo": periodo, "unegocio": unegocio},
        success: function(json) {
            try {
                respuesta = "<tr><th class='ui-state-default'>Unidad de Negocio</th><th class='ui-state-default'>Vencimiento</th><th class='ui-state-default'>Agente</th><th class='ui-state-default'>Cantidad</th></tr>";
                for(var i in json) {
                    for (var j in json[i]) {
                        for(var k in json[i][j]) {
                            respuesta += "<tr style='outline: 1px solid;'><td>"+i+"</td><td>"+j+"</td><td>"+k+"</td><td>"+json[i][j][k]+"</td></tr>"
                        }
                    }
                }
                $('#dialogo').html("<table class='ui-jqgrid' style='width:100%; border-collapse:collapse;'> "+respuesta+"</table>");
            }catch(exception) {
                $('#dialogo').html(exception.message);
            }
        },
        error: function(error) {
            $('#dialogo').html(error.message);
        }
    });
    
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();
}

function modalMessage(msj, width, height) {    
    $("#dialogo").data("title", "Mensaje");
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Cerrar": function () {
                $(this).dialog("close");
            }
        }
    });
    $("#dialogo").html("<p>" + msj +"</p>" );    
}

function mostrarAgencia() {
    var filaAgencia = document.getElementById("fila-agencia");
    var ag = document.getElementById("agencias");
    var un = document.getElementById("uNegocios");
    if (un.value === "MICROCREDITO") {
        filaAgencia.style.display = "table-row";
    } else {
        ag.value = "undefined";
        filaAgencia.style.display = "none";
    }
}
