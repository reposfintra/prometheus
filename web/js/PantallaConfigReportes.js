var actualizarAutomaticamente = false;

function camposReportesLoad(campos)
{
  var frm = document.reportFieldsFrm;

  if( frm ){
    var fldsSelected = campos.split(",");
    var checkBoxFld, jdx = 0, kdx, field, isChecked;
    var checks = document.getElementsByName("fields");
    for( var idx = 0; idx < checks.length; idx++ ) {
        field = checks[idx];
        if ( fldsSelected.length >= 1 && fldsSelected[0] != "(ninguno)" ){
            llenarCombo(document.getElementById("orden"+field.value),fldsSelected.length);
        }
        kdx = 0; isChecked = false;
        while( kdx < fldsSelected.length && !isChecked ) {
          var str = fldsSelected[kdx];
          if ( str.charAt(0) == ' ' ){
            str = str.substring(1,str.length);
          }
          if( field.value == str ){
            isChecked = true;
            var combo = document.getElementById("orden"+field.value);
            combo[kdx].selected = true;
            combo.disabled = false;
          }
          kdx += 1;
        }
        field.checked = isChecked;
    }
  }
}

function llenarCombo(combo,n){
    var i;
    for( i=1; i<=n; i++ ){
        if ( combo[i-1] == null ){
            combo[i-1] = new Option(""+i,""+i);
        }
    }
}

function limpiarCombo(combo){
    for( var i=combo.length-1; i >= 0; i-- ){
        combo.remove(i);
    }
}

function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPaginaCampos(url, nombrePagina)
{
    
  var wdth = screen.width - screen.width * 0.15;
  var hght = screen.height - screen.height * 0.15;
  var lf = screen.height * 0.075;
  var tp = screen.height * 0.075;
  var options = "scrollbars=yes,resizable=yes,closeable=no,status=yes," +
  setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) )
    hWnd.opener = document.window;
}



function accionSeleccionarCampos(codclidest,codreporte,tipoclidest,campos,tipo, BASEURL)
{
  var params = BASEURL + "?codReporte=" + codreporte +
               "&tipoUsuario=" + tipoclidest +
               "&codCliDest=" + codclidest +
               "&campos=" + campos +
               "&tipo=" + tipo;
               
               
  abrirPaginaCampos(params, "wrptfields");
}

function selectAllClick(n)
{
  var check;
  var lstChecks = document.getElementsByName("fields");
  for( var i=0; i<lstChecks.length; i++ ){
    check = lstChecks[i];
    var combo = document.getElementById("orden" + check.value);
    var estadoAnterior = check.checked;
    check.checked = document.reportFieldsFrm.selectAll.checked;
    combo.disabled = !check.checked;
    if ( check.checked ){
        llenarCombo(combo,n);
        /*if ( !estadoAnterior && document.reportFieldsFrm.selectAll.checked ){
            combo.selectedIndex = i;
        }*/
    }
    else {
        limpiarCombo(combo);
    }    
  }
}

function validarForm()
{
  var checkBoxFld;
  var itemChecked = false, idx = 0;
  var lstChecks = document.getElementsByName("fields");
  var numeros = new Array();
  for( idx = 0; idx < lstChecks.length; idx++ ){
    checkBoxFld = lstChecks[idx];
    if( checkBoxFld.checked ){
        itemChecked = true;
        var combo = document.getElementById("orden"+checkBoxFld.value);
        if ( combo.value == null ){
            alert("Debe escojer un valor para la casilla orden del campo "+checkBoxFld.value);
            return false;
        }
        var res = agregarItem(combo.value,numeros);
        if ( res ){
            alert("No pueden haber 2 o mas campos con el mismo orden!");
            return false;
        }
    } 
  }
  if( itemChecked == false ){
    alert("No se ha seleccionado ning�n campo.");
    return false;
  }
  return true;
}

function agregarItem(n,numeros){
    for( var i = 0; i<numeros.length; i++ ){
        if ( numeros[i] == n ){
            return true;
        }
    }
    numeros[numeros.length] = n;
    return false;
}

function reportSetupFrmSubmit(obj)
{
  var submitForm = false;
  if( document.reportSetupFrm.codCliDest.value == "" ){
    alert("Escoja un " + obj+"!");
  }
  /*else if ( document.reportSetupFrm.listaCampos.value = "(ninguno)" ){
    alert("Debe seleccionar por lo menos un campo en la lista de campos!");
  }*/
  else
    submitForm = true;
  return submitForm;
}

function cerrarPopup(){
    window.opener.location.reload();
    window.close();
}

function camposReportesSubmit(){
    if ( validarForm() ){
        var frm = document.reportFieldsFrm;
        var lstChecks = document.getElementsByName("fields");
        var campos = new Array();
        for( var i=0; i<lstChecks.length; i++ ){
            checkBoxFld = lstChecks[i];
            if( checkBoxFld.checked ){
                var combo = document.getElementById("orden"+checkBoxFld.value);
                var pos = parseInt(combo.value);
                campos[pos-1] = checkBoxFld.value;
            }
        }
        var str = "";
        for( var i=0; i<campos.length; i++ ){
            str += campos[i] + ",";
        }
        str = str.substring(0,str.length-1);
        window.opener.forma.listaCampos.value = str;
        window.close();
    }
}

function verificarOrden(comboOrigen){
    if ( actualizarAutomaticamente ){
        var frm = document.reportFieldsFrm;
        var valor = parseInt(comboOrigen.value);
        for( var i = 0; i < frm.elements.length; i++ ){
            var combo = frm.elements[i];
            var v = parseInt(combo.value);
            if ( combo.name.substring(0,5) == "orden" && !combo.disabled && comboOrigen.name != combo.name && v >= valor ){
                combo.selectedIndex = v;
            }
        }
    }
}

function actualizarComboOrden(check){

    var c = document.getElementById("orden"+check.value);
    c.disabled = !check.checked;
    if ( c.disabled ){
        var frm = document.reportFieldsFrm;
        for( var i = 0; i < frm.elements.length; i++ ){
            var combo = frm.elements[i];
            if ( combo.name.substring(0,5) == "orden" ){
                combo.remove(combo.length-1);
            }
        }
    }
    else {
        var frm = document.reportFieldsFrm;
        for( var i = 0; i < frm.elements.length; i++ ){
            var combo = frm.elements[i];
            if ( combo.name.substring(0,5) == "orden" ){
                combo[combo.length] = new Option(""+(combo.length+1),""+(combo.length+1));
                //c.selectedIndex = combo.length-1;
            }
        }
    }
}

function ejecutarOpcion(radio){
    var frm = document.reportFieldsFrm;
    actualizarAutomaticamente = false;
    if ( radio.value == "1" ){
        var seleccionados = 0;
        for( var i = 0; i < frm.elements.length; i++ ){
            var combo = frm.elements[i];
            if ( combo.name.substring(0,5) == "orden" && !combo.disabled){
                combo.selectedIndex = seleccionados;
                seleccionados++;
            }
        }
    }
    else if ( radio.value == "2" ){
        var seleccionados = 0;
        for( var i = frm.elements.length-1; i >= 0  ; i-- ){
            var combo = frm.elements[i];
            if ( combo.name.substring(0,5) == "orden" && !combo.disabled){
                combo.selectedIndex = seleccionados;
                seleccionados++;
            }
        }
    }
    else if ( radio.value == "3" ){
        actualizarAutomaticamente = true;
    }

}

function validarChecksEliminar(){
    if ( document.reportSetupFrm.cmd.value == "eliminar" ){
        var checks = document.getElementsByName("chk");
        for( var i=0; i<checks.length; i++ ){
            if ( checks[i].checked ){
                if ( confirm("Confirma la eliminaci�n de las configuraciones de reporte seleccionadas?") ){
                    document.reportSetupFrm.submit();
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        alert("Para eliminar algun reporte debe seleccionarlo en la columna eliminar");
        return false;
    }
    return true;
}

function cargarDatos(controller){
   var combo = document.forma.tipoUsuario;
   var valor = combo.value;
   location.href = controller + "?estado=GestionReportes&accion=Setup&cmd=loadUsers&tipo_usuario="+valor;
}

function enviarFormAgregarConf(){
    if ( document.forma.usuario == null ) {
        alert("Debe seleccionar el tipo de usuario");
        return;
    }
    var usuario = document.forma.usuario.value;
    var campos = document.forma.listaCampos.value;
    if ( usuario == null || usuario == '' ){
        alert("Debe seleccionar un "+ document.forma.tipoUsuario.value );
        return;
    }
    else if ( campos == null || campos == '' ){
        alert("Debe seleccionar uno o m�s campos" );
        return;
    }
    document.forma.submit();
}

function actualizarCamposSubmit(){
    if ( validarForm() ){
        document.reportFieldsFrm.submit();
    }
}
