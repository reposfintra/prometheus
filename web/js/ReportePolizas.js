/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
$(document).ready(function () {
    $("#contenedor").hide();
    $("#imagen_no").attr("hidden", true);
    $("#fecha_inicio").val('');
    $("#fechafin").val('');
    $("#fecha_inicio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        // maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha_fin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });
    $("#fecha_fin").click(function () {
        CalcularDiasVig();
    });
    //  var myDate = new Date();
//    $("#fecha_inicio").datepicker("setDate", myDate);
//    $("#fecha_fin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');
    $('#buscar').click(function () {
        var polizas = $("#polizas").val();
        if (polizas === '') {
            mensajesDelSistema("Debe seleccionar polizas y seguro", '250', '150', false);
        } else {
            $("#contenedor").show();
            cargarReportePolizas();
        }

    });
    cargarComboPolizas();
    cargarComboConvenios();
});

///este al buscar el codigo fasecolda y existe se permite actualizar el pgv de lo
//contrario se le asigna no 
var PermitiractualizarPGV = 'si';

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function numberSinComas(x) {
    return x.toString().replace(/,/g, "");
}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function onKeyDecimal(e, num) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if (document.getElementById(num.id).value.indexOf('.') !== -1 && keynum === 46)
        return false;
    if ((keynum === 8 || keynum === 0 || keynum === 48 || keynum === 46))
        return true;
    if (keynum >= 58)
        return false;
    return /\d/.test(String.fromCharCode(keynum));
}

function  limpiar() {
    $("#contenedor").hide();
    $("#tabla_reportePlizas").jqGrid("clearGridData", true);
}

function habilitarFiltros() {
    limpiar();
    var polizas = $("#polizas option:selected").text();
    if (polizas === 'SEGURO DE AUTOMOVIL') {
        $("#convenio").attr("disabled", true);
    } else {
        $("#convenio").val('');
        $("#convenio").attr("disabled", false);
    }
}

var condicion = false;
function cargarReportePolizas() {
    var accion_ejecutar;
    var grid_tabla_reportePolizas = jQuery("#tabla_reportePlizas");
    if ($("#gview_tabla_reportePlizas").length) {
        reloadGridReportePolizas(grid_tabla_reportePolizas, 53);
    } else {
        grid_tabla_reportePolizas.jqGrid({
            caption: "Reporte de polizas",
            url: "./controller?estado=Fintra&accion=Soporte",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '1503',
            async: false,
            colNames: ['Negocio padre', 'Convenio padre', 'Nit', 'Nombre', 'Afiliado', 'Placa', 'Servicio', 'Vlr negocio',
                'Vlr fasecolda', 'Negocio seguro', 'id_config_poliza', 'Nombre aseguradora', 'id_aseguradora', 'Vlr seguro', 'Fecha inicio',
                'Fecha fin', 'Dias vigencia', 'Codigo fasecolda', 'Estado poliza', 'Dias vencimiento', 'Aviso', 'Proceso Juridico', 'Tipo'],
            colModel: [
                {name: 'negocio_padre', index: 'negocio_padre', width: 100, sortable: true, align: 'left', hidden: false, key: true},
                {name: 'convenio_padre', index: 'convenio_padre', width: 100, sortable: true, align: 'center', hidden: true},
                {name: 'nit', index: 'nit', width: 90, sortable: true, align: 'left', hidden: false},
                {name: 'nombre', index: 'nombre', width: 220, sortable: true, align: 'left', hidden: false},
                {name: 'afiliado', index: 'afiliado', width: 170, sortable: true, align: 'left', hidden: false},
                {name: 'placa', index: 'placa', width: 90, sortable: true, align: 'center', hidden: false},
                {name: 'servicio', index: 'servicio', width: 90, sortable: true, align: 'center', hidden: false},
                {name: 'vr_negocio', index: 'vr_negocio', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_fasecolda', index: 'valor_fasecolda', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'negocio_seguro', index: 'negocio_seguro', width: 100, sortable: true, align: 'center', hidden: false},
                {name: 'id_config_poliza', index: 'id_config_poliza', width: 100, sortable: true, align: 'center', hidden: true},
                {name: 'nombre_aseguradora', index: 'nombre_aseguradora', width: 100, sortable: true, align: 'left', hidden: false},
                {name: 'id_aseguradora', index: 'id_aseguradora', width: 100, sortable: true, align: 'center', hidden: true},
                {name: 'valor_seguro', index: 'valor_seguro', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}},
                {name: 'fecha_inicio', index: 'fecha_inicio', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'fecha_fin', index: 'fecha_fin', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'dias_vigencia', index: 'dias_vigencia', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'codigo_fasecolda', index: 'codigo_fasecolda', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'estado_poliza', index: 'estado_poliza', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'dias_vencimiento', index: 'dias_vencimiento', width: 80, sortable: true, align: 'left', hidden: false},
                {name: 'aviso', index: 'aviso', width: 120, sortable: true, align: 'left', hidden: false},
                {name: 'marca', index: 'marca', width: 120, sortable: true, align: 'center', hidden: false},
                {name: 'tipo', index: 'tipo', width: 100, sortable: true, align: 'center', hidden: false}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pager: '#pager',
            multiselect: true,
            multiboxonly: false,
            pgtext: null,
            pgbuttons: false,
//            rowList: [10, 20, 40],
            sortname: 'invdate',
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function (rowid, e, iRow, iCol) {
                $("tr.jqgrow", this).contextMenu('menu', {
                    bindings: {
                        'actualizar': function (trigger) {
                            var filas = jQuery("#tabla_reportePlizas").getRowData(trigger.id);
                            var negocio_padre = filas.negocio_padre;
                            var nit = filas.nit;
                            var nombre_seguro = filas.nombre_aseguradora;
                            var id_aseguradora = filas.id_aseguradora;
                            var tipo = filas.tipo;
                            var fecha_inicio = filas.fecha_inicio;
                            var fecha_fin = filas.fecha_fin;
                            var codigo_fasecolda = filas.codigo_fasecolda;
                            var valor_seguro = filas.valor_seguro;
                            var afiliado = filas.afiliado;
                            var nombre = filas.nombre;
                            var placa = filas.placa;
                            var servicio = filas.servicio;
                            ventanaPoliza(negocio_padre, nit, nombre, nombre_seguro, tipo, fecha_inicio, fecha_fin, codigo_fasecolda, valor_seguro, afiliado, placa, servicio,id_aseguradora);
                        },
                        'renovacion': function (trigger) {
                            var filas = jQuery("#tabla_reportePlizas").getRowData(trigger.id);
                            accion_ejecutar = 'renovacion';
                            var nombre = filas.nombre;
                            var nombre_seguro = filas.nombre_aseguradora;
                            var tipo = filas.tipo;
                            var placa = filas.placa;
                            var nit = filas.nit;
                            if (nombre_seguro !== '') {
                                if (tipo === 'DIRECTA') {
                                    exportarpdf(accion_ejecutar, nombre_seguro, nombre, placa, nit);
                                }

                            } else {
                                mensajesDelSistema("No tiene nombre de la Aseguradora", '250', '150', false);
                            }
                        },
                        'marcar': function (trigger, rowid) {
                            // alert("marcar:trigger.id:" + trigger.id);
                            var filas = jQuery("#tabla_reportePlizas").getRowData(trigger.id);
                            accion_ejecutar = 'revocacion';
                            var nombre = filas.nombre;
                            var nombre_seguro = filas.nombre_aseguradora;
                            var negocio_padre = filas.negocio_padre;
                            var fecha_fin = filas.fecha_fin;
                            var placa = filas.placa;
                            var nit = filas.nit;
                            var tipo = filas.tipo;
                            if (nombre_seguro !== '') {
                                if (tipo === 'DIRECTA') {
                                    marcarPGV(accion_ejecutar, nombre_seguro, nombre, placa, nit, negocio_padre, fecha_fin);
                                }
                                //exportarpdf(accion_ejecutar, nombre_seguro, nombre);
                            } else {
                                mensajesDelSistema("No tiene nombre de la Aseguradora", '250', '150', false);
                            }
                        }
                    },
                    onContextMenu: function (event/*, menu*/) {
                        var rowId = $(event.target).closest("tr.jqgrow").attr("id");
                        var tipo = $("#tabla_reportePlizas").getRowData(rowId).tipo;
                        if (tipo === 'DIRECTA') {
                            $('#renovacion').removeAttr("disabled").removeClass('ui-state-disabled');
                            $('#marcar').removeAttr("disabled").removeClass('ui-state-disabled');
                        } else {
                            $('#renovacion').attr("disabled", "disabled").addClass('ui-state-disabled');
                            $('#marcar').attr("disabled", "disabled").addClass('ui-state-disabled');
                        }
                        return true;
                    }
                });

            },
            gridComplete: function (rowid) {
                var ids = grid_tabla_reportePolizas.jqGrid('getDataIDs');
                var allRowsInGrid = grid_tabla_reportePolizas.jqGrid('getRowData');
                for (var i = 0; i < allRowsInGrid.length; i++) {
                    var cl = ids[i];
                    var tipo = grid_tabla_reportePolizas.getRowData(cl).tipo;
                    var aviso = grid_tabla_reportePolizas.getRowData(cl).aviso;
                    if (tipo !== 'DIRECTA' && tipo !== 'PRESENTADA') {
                        $('#tabla_reportePlizas').jqGrid("setCell", cl, "tipo", "Presentada/No asociada");
                    }
                    if (aviso === 'POLIZA POR VENCER') {
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "negocio_padre", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "dias_vigencia", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "nit", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "nombre", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "afiliado", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "placa", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "servicio", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "vr_negocio", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "valor_fasecolda", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "negocio_seguro", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "nombre_aseguradora", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "valor_seguro", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "fecha_inicio", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "fecha_fin", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "dias_vigencia", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "codigo_fasecolda", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "estado_poliza", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "dias_vencimiento", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "aviso", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "tipo", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "marca", "", {'background-color': '#F2CC44', 'background-image': 'none'});
                    } else if (aviso === 'POLIZA VENCIDA') {
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "negocio_padre", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "dias_vigencia", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "nit", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "nombre", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "afiliado", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "placa", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "servicio", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "vr_negocio", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "valor_fasecolda", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "negocio_seguro", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "nombre_aseguradora", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "valor_seguro", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "fecha_inicio", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "fecha_fin", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "dias_vigencia", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "codigo_fasecolda", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "estado_poliza", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "dias_vencimiento", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "aviso", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "tipo", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                        grid_tabla_reportePolizas.jqGrid('setCell', allRowsInGrid[i].negocio_padre, "marca", "", {'background-color': 'rgba(237, 22, 49, 0.51)', 'background-image': 'none'});
                    }
                }
            },
            onSelectAll: function (rowid, e, aRowid) {
                var ids = grid_tabla_reportePolizas.jqGrid('getDataIDs');
                var allRowsInGrid = jQuery("#tabla_reportePlizas").jqGrid('getGridParam', 'selarrrow');
                var allRowsId = jQuery("#tabla_reportePlizas").jqGrid('getRowData');
                for (var i = 0; i < allRowsInGrid.length; i++) {
                    var cl = ids[i];
                    var tipo = grid_tabla_reportePolizas.getRowData(cl).tipo;
                    if (tipo !== 'DIRECTA') {
                        //El onselectall selecciona todo por defecto, pero al ejecutar la linea le quita el check
                        jQuery("#tabla_reportePlizas").jqGrid('setSelection', cl);
                    }
                }

                if (jQuery("#cb_tabla_reportePlizas").attr('checked') || condicion === true) {
                    $("#cb_tabla_reportePlizas").attr("checked", false);
                    for (var i = 0; i < allRowsId.length; i++) {
                        var cl = ids[i];
                        var tipo = grid_tabla_reportePolizas.getRowData(cl).tipo;
                        if (tipo === 'DIRECTA') {
                            jQuery("#Negocios").jqGrid('setSelection', cl);
                        }
                    }
                    condicion = false;
                } else {
                    $("#cb_tabla_reportePlizas").attr("checked", true);
                    condicion = true;
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
            },
            ajaxGridOptions: {
                dataType: "json",
                type: "get",
                data: {
                    opcion: 53,
                    polizas: $("#polizas option:selected").text(),
                    tipo_poliza: $("#polizas").val(),
                    aseguradora: $('#aseguradora').val(),
                    convenio: $('#convenio').val(),
                    cliente: $('#cliente').val(),
                    estados: $('#estado').val()
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#pager", {add: false, edit: false, del: false, search: false, refresh: false}, {
        });
//        jQuery("#tabla_reportePlizas").jqGrid('setFrozenColumns');
        $("#tabla_reportePlizas").navButtonAdd('#pager', {
            caption: "Exportar Excel",
            onClickButton: function () {
                var info = grid_tabla_reportePolizas.getGridParam('records');
                if (info > 0) {
                    exportarReporte();
                } else {
                    mensajesDelSistema("No hay informacion para exportar ", '250', '150', false);
                }
            }
        });
        $("#tabla_reportePlizas").navButtonAdd('#pager', {
            caption: "Pdf multiple renovacion",
            onClickButton: function () {
                var info = grid_tabla_reportePolizas.getGridParam('records');
                var myGrid = jQuery("#tabla_reportePlizas"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                accion_ejecutar = 'renovacion';
                if (info > 0) {
                    if (filas !== false) {
                        exportarpdf(accion_ejecutar);
                    } else {
                        mensajesDelSistema("No hay informacion seleccionada", '250', '150', false);
                    }
                } else {
                    mensajesDelSistema("No hay informacion para generar pdf ", '250', '150', false);
                }
            }
        });
    }
}

function reloadGridReportePolizas(grid_tabla_reportePolizas, opcion) {
    grid_tabla_reportePolizas.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        async: false,
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: opcion,
                polizas: $("#polizas option:selected").text(),
                tipo_poliza: $("#polizas").val(),
                aseguradora: $('#aseguradora').val(),
                convenio: $('#convenio').val(),
                cliente: $('#cliente').val(),
                estados: $('#estado').val()
            }
        }
    });
    grid_tabla_reportePolizas.trigger("reloadGrid");
}

function mensajesDelSistema(msj, width, height, swHideDialog) {

    if (swHideDialog) {
        $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#info").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closable: false,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function () {
                $(this).dialog("destroy");
            }
        }
    });
    $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function  exportarReporte() {
    var fullData = jQuery("#tabla_reportePlizas").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    var opt = {
        autoOpen: false,
        modal: true,
        width: 200,
        height: 150,
        title: 'Descarga'
    };
    //  $("#divSalidaEx").dialog("open");
    $("#divSalidaEx").dialog(opt);
    $("#divSalidaEx").dialog("open");
    $("#imgloadEx").show();
    $("#msjEx").show();
    $("#respEx").hide();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "./controller?estado=Fintra&accion=Soporte",
        async: false,
        data: {
            listado: myJsonString,
            opcion: 54
        },
        success: function (resp) {
            $("#imgloadEx").hide();
            $("#msjEx").hide();
            $("#respEx").show();
            var boton = "<div style='text-align:center'>\n\ </div>";
            //var boton = "<div style='text-align:center'><img src='.//images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\"" + "#divSalidaEx" + "\")' /></div>";
            document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;
        },
        error: function (resp) {
            alert("Ocurrio un error enviando los datos al servidor");
        }
    });
}

function ventanaPoliza(negocio_padre, nit, nombre, nombre_seguro, tipo, fecha_inicio, fecha_fin, codigo_fasecolda, valor_seguro, afiliado, placa, servicio ,id_aseguradora) {
    if (tipo === 'DIRECTA') {
        $("#tipo").val(tipo);
        $("#aseguradora_asoc").val(nombre_seguro);
        $("#aseguradora_asoc2").attr("hidden", true);
        $("#aseguradora_asoc").attr("hidden", false);
    } else {
        $("#tipo").val('PRESENTADA');
        if (nombre_seguro === '') {
            $("#aseguradora_asoc").attr("hidden", true);
            $("#aseguradora_asoc2").attr("hidden", false);
            cargarComboTodasAseguradoras();
        } else {
            $("#aseguradora_asoc").attr("hidden", true);
            $("#aseguradora_asoc2").attr("hidden", false);
            cargarComboTodasAseguradoras();
             $("#aseguradora_asoc2").val(id_aseguradora);
//            $("#aseguradora_asoc").val(nombre_seguro);
//            $("#aseguradora_asoc2").attr("hidden", true);
//            $("#aseguradora_asoc").attr("hidden", false);
        }
    }
    if (nombre_seguro === '') {
        $("#aseguradora_asoc").attr("disabled", false);
        $("#aseguradora_asoc2").attr("disabled", false);
    } else {
        $("#aseguradora_asoc").attr("disabled", true);
        $("#aseguradora_asoc2").attr("disabled", false);
    }
    if (servicio === '') {
        $("#servicio2").attr("hidden", false);
        $("#servicio").attr("hidden", true);
        $("#servicio").val('');
    } else {
        $("#servicio").val(servicio);
        $("#servicio").attr("disabled", true);
        $("#servicio").attr("hidden", false);
        $("#servicio2").val('');
        $("#servicio2").attr("hidden", true);

    }
    if (valor_seguro === '' || valor_seguro === '0') {
        $("#Vlrpoliza").attr("disabled", false);
    } else {
        if (tipo === 'DIRECTA') {
            $("#Vlrpoliza").val(numberConComas(valor_seguro));
            $("#Vlrpoliza").attr("disabled", true);
        } else {
            $("#Vlrpoliza").val(numberConComas(valor_seguro));
            $("#Vlrpoliza").attr("disabled", false);
        }
        
    }
    if (placa === '') {
        $("#placa").attr("readonly", false);
    } else {
        $("#placa").val(placa);
        $("#placa").attr("disabled", true);
    }
    $("#nomcliente").val(nombre);
    $("#nit").val(nit);
    $("#afiliado").val(afiliado);
    $("#neg_vehiculo").val(negocio_padre);
    $("#tipo_poliza").val($("#polizas option:selected").text());
    $("#fecha_inicio").val(fecha_inicio);
    $("#fecha_fin").val(fecha_fin);
    $("#cod_fasecolda").val(codigo_fasecolda);
    verificarFasecolda();
    CalcularDiasVig();
    var tipo = $("#tipo").val();
    $("#dialogFormulario").dialog({
        width: '387',
        height: '360',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        title: 'ACTUALIZAR',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        }
    });
    if (tipo === 'PRESENTADA') {
        $("#dialogFormulario").dialog({
            buttons: {
                "Actualizar": function () {
                    if (negocio_padre !== '' || nit !== '' || nombre !== '' || nombre_seguro !== '' || tipo !== '' || fecha_inicio !== '' || fecha_fin !== '' || codigo_fasecolda !== '' || valor_seguro !== '' || afiliado !== '' || placa !== '') {
                        if (PermitiractualizarPGV !== 'no') {
                            consultar_config_polizas();
                        } else {
                            mensajesDelSistema("Falta datos por llenar", '250', '150', false);
                        }
                    }
                },
                "Salir": function () {
                    $(this).dialog("close");
                    $("#Vlrpoliza").val('');
                    $("#aseguradora_asoc").val('');
                    $("#placa").val('');
                    $("#servicio").val('');
                }
            }
        });
    } else {
        $("#dialogFormulario").dialog({
            buttons: {
                "Salir": function () {
                    $(this).dialog("close");
                    $("#Vlrpoliza").val('');
                    $("#aseguradora_asoc").val('');
                    $("#placa").val('');
                    $("#servicio").val('');
                }
            }
        });
    }

}

function cargarComboPolizas() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 70
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#polizas').html('');
                $('#polizas').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#polizas').append('<option value=' + datos + ' >' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarComboAseguradora() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 71,
            poliza: $("#polizas").val()
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#aseguradora').html('');
                $('#aseguradora').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#aseguradora').append('<option value=' + datos + '>' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarComboConvenios() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 73
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#convenio').html('');
                $('#convenio').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#convenio').append('<option value=' + datos + ' >' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarComboTodasAseguradoras() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 72,
            poliza: $("#polizas").val()
        },
        success: function (json) {
            if (json.error) {
                //  mensajesDelSistema(json.error, '250', '180');
                return;
            }
            try {
                $('#aseguradora_asoc2').html('');
                $('#aseguradora_asoc2').append('<option value="" ></option>');
                for (var datos in json) {
                    $('#aseguradora_asoc2').append('<option value=' + datos + ' >' + json[datos] + '</option>');
                }
            } catch (exception) {
                //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
            }

        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function verificarFasecolda() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'text',
        async: false,
        data: {
            opcion: 74,
            codigo_fasecolda: $("#cod_fasecolda").val(),
            negocio_padre: $("#neg_vehiculo").val()
        },
        success: function (data) {
            var respuesta = data;
            if (respuesta === 'SI\n') {
                $("#imagen_no").attr("hidden", true);
                PermitiractualizarPGV = 'si';
            } else {
                $("#imagen_no").attr("hidden", false);
                PermitiractualizarPGV = 'no';
            }
        }
    });
}

function CalcularDiasVig() {
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    if (fecha_fin === '' || fecha_inicio === '') {
        $("#dias_vigencia").val(0);
    } else {
        var diferencia = new Date(fecha_fin) - new Date(fecha_inicio);
        var dias_vigencia = Math.floor(diferencia / (1000 * 60 * 60 * 24));
        $("#dias_vigencia").val(dias_vigencia);
    }
}

function consultar_config_polizas() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 75,
            aseguradora: $("#aseguradora_asoc2").val(),
            poliza: $("#polizas").val()
        },
        success: function (data) {
            var id_config_poliza = data.id;
            ActualizarPGV(id_config_poliza);
        }
    });
}

function ActualizarPGV(id_config_poliza) {
    var neg_vehiculo = $("#neg_vehiculo").val();
    var Vlrpoliza = $("#Vlrpoliza").val();
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    var codigo_fasecolda = $("#cod_fasecolda").val();
    var nomcliente = $("#nomcliente").val();
    var nit = $("#nit").val();
    var afiliado = $("#afiliado").val();
    var placa = $("#placa").val();
    var servicio1 = $("#servicio").val();
    var servicio2 = $("#servicio2").val();
    var servicio;
    if (servicio1 === '') {
        servicio = servicio2;
    } else {
        servicio = servicio1;
    }

    if (neg_vehiculo !== '' || Vlrpoliza !== '' || fecha_inicio !== '' || fecha_fin !== '' || codigo_fasecolda !== '' || nomcliente !== '' || nit !== '' || afiliado !== '' || placa !== '' || servicio !== '') {
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Fintra&accion=Soporte",
            dataType: 'json',
            async: false,
            data: {
                opcion: 76,
                tipo: $("#tipo").val(),
                neg_vehiculo: $("#neg_vehiculo").val(),
                Vlrpoliza: numberSinComas($("#Vlrpoliza").val()),
                fecha_inicio: $("#fecha_inicio").val(),
                fecha_fin: $("#fecha_fin").val(),
                codigo_fasecolda: $("#cod_fasecolda").val(),
                id_config_poliza: id_config_poliza,
                nomcliente: $("#nomcliente").val(),
                nit: $("#nit").val(),
                afiliado: $("#afiliado").val(),
                placa: $("#placa").val(),
                servicio: servicio
            },
            success: function (data) {
                mensajesDelSistema("Los datos fueron guardados satisfactoriamiente", '235', '150', true);
                cargarReportePolizas();
            }
        });
    } else {
        mensajesDelSistema("Faltan datos por llenar", '235', '150', true);
    }

}

function marcarPGV(accion_ejecutar, nombre_seguro, nombres, placa, nit, negocio_padre, fecha_fin) {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Fintra&accion=Soporte",
        dataType: 'json',
        async: false,
        data: {
            opcion: 78,
            negocio_padre: negocio_padre,
            fecha_fin: fecha_fin
        },
        success: function (data) {
            if (data.respuesta === 'Guardado') {
                cargarReportePolizas();
                exportarpdf(accion_ejecutar, nombre_seguro, nombres, placa, nit);
            } else if (data.respuesta === 'NO MARCADO') {
                mensajesDelSistema("El negocio no esta en proceso juridico", '335', '150', true);
            }
        }
    });
}

function exportarpdf(accion_ejecutar, nombre_seguro, nombres, placa, nit) {
    var infoid = jQuery('#tabla_reportePlizas').jqGrid('getGridParam', 'selarrrow');
    var json = [];
    if (infoid.length > 0) {
        for (var i = 0; i < infoid.length; i++) {
            var llave = {};
            var id = infoid[i];
            var nombre = jQuery("#tabla_reportePlizas").getRowData(id).nombre;
            var nombre_aseguradora = jQuery("#tabla_reportePlizas").getRowData(id).nombre_aseguradora;
            var placas = jQuery("#tabla_reportePlizas").getRowData(id).placa;
            var nits = jQuery("#tabla_reportePlizas").getRowData(id).nit;
            llave['nombre'] = nombre;
            llave['nombre_aseguradora'] = nombre_aseguradora;
            llave['accion_ejecutar'] = accion_ejecutar;
            llave['placa'] = placas;
            llave['nit'] = nits;
            json.push(llave);
        }
    } else {
        var llave = {};
        llave['nombre'] = nombres;
        llave['nombre_aseguradora'] = nombre_seguro;
        llave['accion_ejecutar'] = accion_ejecutar;
        llave['placa'] = placa;
        llave['nit'] = nit;

        json.push(llave);
    }

    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controller?estado=Fintra&accion=Soporte",
        async: false,
        data: {
            opcion: 77,
            info: JSON.stringify({json: json})
        },
        success: function (json) {
            //alert (json.respuesta);
            if (json.respuesta === "GENERADO") {
                mensajesDelSistema("Exito en la generacion de pdf, por favor ver en el log", '335', '150', true);
            }
        }
    });
}

