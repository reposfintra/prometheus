/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    comboMora();
    cargarComboUnidNegocio();
});

function cargarComboUnidNegocio() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 27
        },
        success: function (json) {
            if (json.error) {
                return;
            }
            $('#id_unidad_negocio').html('');
            $('#id_unidad_negocio').append('<option value="" >...</option>');
            for (var datos in json) {
                $('#id_unidad_negocio').append('<option value="' + datos + '" >' + json[datos] + '</option>');
            }
        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });


}


function generarCartasCobro() {
    if ($("#id_unidad_negocio").val() === "") {
        mensajesDelSistema("Por favor seleccione unidad negocio", '250', '150');
    } else if ($("#id_agencia").val() === "") {
        mensajesDelSistema("Por favor seleccione Agencia", '250', '150');
    } else if ($("#id_altura_mora").val() === "") {
        mensajesDelSistema("Por favor seleccione Altura mora", '250', '150');
    } else {
        loading("Espere un momento por favor...", "270", "140");
        $.ajax({
            type: 'POST',
            url: "./controller?estado=Negocios&accion=Fintra",
            dataType: 'json',
            data: {
                opcion: 45,
                id_unidad_negocio: $("#id_unidad_negocio").val(),
                id_agencia: $("#id_agencia").val(),
                id_altura_mora: $("#id_altura_mora").val(),
                id_dia: $("#id_dia").val()
            },
            success: function (json) {

                if (json.respuesta === "OK") {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Se generaron las cartas corrrectamente, verifique el log de descargas", '250', '150');
                } else {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("No se generaron las cartas", '250', '150');
                }
            },
            error: function (xhr) {
                $("#dialogLoading").dialog('close');
                mensajesDelSistema("Error al refinanciar " + "\n" +
                        xhr.responseText, '650', '250', true);
            }
        });
    }


}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {

                $(this).dialog("close");
            }
        }
    });

}


function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}


function comboMora() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Negocios&accion=Fintra",
        dataType: 'json',
        async: false,
        data: {
            opcion: 46
        },
        success: function (json) {
            if (json.error) {
                return;
            }
                $('#id_altura_mora').html('');
                $('#id_altura_mora').append('<option value="" >...</option>');
                for (var datos in json) {
                    $('#id_altura_mora').append('<option value="' + datos + '" >' + json[datos] + '</option>');
                }
        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}