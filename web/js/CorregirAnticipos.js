/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    var tiempo = new Date();
    var hora = tiempo.getHours();
    // var minuto = tiempo.getMinutes();
    // var segundo = tiempo.getSeconds(); 

    if (hora > 9 && hora < 19) {
        mensajesDelSistema("El horario permitido para esta actividad <br> corresponde entre las 8PM - 8AM ", 300, 150);

    } else {
        listarPlanillas();
    }

    $("#actualizar").click(function () {
        if (hora > 9 && hora < 19) {
            mensajesDelSistema("El horario permitido para esta actividad <br> corresponde entre las 8PM - 8AM ", 300, 150);

        } else {
           var grid_listar_planillas = jQuery("#tabla_planillas");
           reloadGridListarPlanillas(grid_listar_planillas);
        }
    });

    $("#validar_sun_dif").click(function () {
        if (hora > 9 && hora < 19) {
            mensajesDelSistema("El horario permitido para esta actividad <br> corresponde entre las 8PM - 8AM ", 300, 150);

        } else {
            VerificarTablas();

        }
    });
    $("#salir").click(function () {
        close();
    });
});


function listarPlanillas() {
    var grid_listar_planillas = jQuery("#tabla_planillas");

    if ($("#gview_tabla_planillas").length) {
        reloadGridListarPlanillas(grid_listar_planillas);
    } else {

        grid_listar_planillas.jqGrid({
            caption: "Lista de planillas",
            url: "./controller?estado=Corregir&accion=Planilla&opcion=1",
            mtype: "POST",
            datatype: "json",
            height: '500',
            width: '955',
            colNames: ['Nit', 'Num Operacion', 'Tipo Doc', 'Clase', 'Documento', 'Valor Calculado', 'Valor Registrado', 'Diferencia', 'Placa'],
            colModel: [
                {name: 'nit', index: 'nit', width: 80, align: 'center'},
                {name: 'numero_operacion', index: 'numero_operacion', sortable: true, width: 100, align: 'center'},
                {name: 'tipo_documento', index: 'tipo_documento', sortable: true, width: 80, align: 'center'},
                {name: 'clase', index: 'clase', sortable: false, width: 100, align: 'center'},
                {name: 'documento', index: 'documento', width: 100, align: 'center',key:true},
                {name: 'valor_calculado', index: 'valor_calculado', width: 105, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_registrado', index: 'valor_registrado', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'diferencia_interna', index: 'diferencia_interna', sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'placa', index: 'placa', width: 100, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ondblClickRow: function (rowid, status, e) {
                var numero_operacion = grid_listar_planillas.getRowData(rowid).numero_operacion;
                var documento = grid_listar_planillas.getRowData(rowid).documento;
                var clase = grid_listar_planillas.getRowData(rowid).clase;
                $("#tabla_planillas").jqGrid('setRowData', rowid, rowid, {color: 'red', weightfont: 'bold', background: 'rgba(251, 246, 152, 1)'});
                buscarExtractoDetalle(numero_operacion, documento, clase);
               
            },
            loadComplete: function () {
                if (grid_listar_planillas.jqGrid('getGridParam', 'records') <= 0) {
                    mensajeVerificacion("NO SE ENCONTRARON PLANILLAS DESCUADRADAS !!! <br>VERIFIQUE LAS DIFERENCIAS ENTRE TABLAS PRESIONANDO EL BOTON CONTINUAR ", "430", "170");
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });
    }


}


function reloadGridListarPlanillas(grid_listar_planillas) {
    grid_listar_planillas.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Corregir&accion=Planilla&opcion=1"
    });
    grid_listar_planillas.trigger("reloadGrid");
}


var lastsel;
function  buscarExtractoDetalle(numero_operacion, documento, clase) {

    var editCell = false;
    //var idRows;

    var grid_listar_extracto = jQuery("#tabla_extracto_detalle");
    if ($("#gview_tabla_extracto_detalle").length) {
        reloadGridListarExtracto(grid_listar_extracto, numero_operacion, documento, clase);
    } else {

        grid_listar_extracto.jqGrid({
            caption: "Lista de planillas",
            url: "./controller?estado=Corregir&accion=Planilla&opcion=2&numero_operacion=" + numero_operacion + "&documento=" + documento + "&clase=" + clase,
            editurl: "./controller?estado=Corregir&accion=Planilla&opcion=6",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '1057',
            colNames: ['Concepto', 'Documento', 'Secuencia', 'Descripcion', 'Factura', 'Valor', 'Rete Fuente', 'Rete Ica', 'Impuestos', 'Valor Total'],
            colModel: [
                {name: 'concepto', index: 'concepto', width: 80, align: 'center', key: true},
                {name: 'documento', index: 'documento', sortable: true, width: 80, align: 'right'},
                {name: 'secuencia', index: 'secuencia', sortable: true, width: 80, align: 'right'},
                {name: 'descripcion', index: 'descripcion', sortable: true, width: 180, align: 'right'},
                {name: 'factura', index: 'factura', sortable: true, width: 80, align: 'center'},
                {name: 'valor', index: 'valor', editable: true, sortable: false, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'retefuente', index: 'retefuente', editable: true, width: 105, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'reteica', index: 'reteica', editable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'impuestos', index: 'impuestos', editable: true, sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_total', index: 'valor_total', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}
            ],
            rowNum:1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

                editCell = true;
                grid_listar_extracto.jqGrid('restoreRow', lastsel);
                var editparameters = getParametersOrden(grid_listar_extracto, rowid, clase);
                grid_listar_extracto.jqGrid('editRow', rowid, editparameters);
                lastsel = rowid;

            },
            loadComplete: function () {
                cacularTotalesExtractoDetalle(grid_listar_extracto);
                abrirExtractoDetalle();
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });
    }


}

function reloadGridListarExtracto(grid_listar_extracto, numero_operacion, documento, clase) {
    grid_listar_extracto.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Corregir&accion=Planilla&opcion=2&numero_operacion=" + numero_operacion + "&documento=" + documento + "&clase=" + clase
    });
    grid_listar_extracto.trigger("reloadGrid");
}


function  cacularTotalesExtractoDetalle(grid_listar_extracto) {


    var valor = grid_listar_extracto.jqGrid('getCol', 'valor', false, 'sum');
    var retefuente = grid_listar_extracto.jqGrid('getCol', 'retefuente', false, 'sum');
    var reteica = grid_listar_extracto.jqGrid('getCol', 'reteica', false, 'sum');
    var impuestos = grid_listar_extracto.jqGrid('getCol', 'impuestos', false, 'sum');
    var valor_total = grid_listar_extracto.jqGrid('getCol', 'valor_total', false, 'sum');


    grid_listar_extracto.jqGrid('footerData', 'set', {
        factura: 'Total:',
        valor: valor,
        retefuente: retefuente,
        reteica: reteica,
        impuestos: impuestos,
        valor_total: valor_total

    });

}

function  cacularTotalesNuevos(grid_listar_extracto, clase) {


    var valor = grid_listar_extracto.jqGrid('getCol', 'valor', false, 'sum');
    var retefuente = grid_listar_extracto.jqGrid('getCol', 'retefuente', false, 'sum');
    var reteica = grid_listar_extracto.jqGrid('getCol', 'reteica', false, 'sum');
    var impuestos = grid_listar_extracto.jqGrid('getCol', 'impuestos', false, 'sum');
    var valor_total = grid_listar_extracto.jqGrid('getCol', 'valor_total', false, 'sum');

    var suma = valor + retefuente + reteica + impuestos;

    grid_listar_extracto.jqGrid('footerData', 'set', {
        factura: 'Total:',
        valor: valor,
        retefuente: retefuente,
        reteica: reteica,
        impuestos: impuestos,
        valor_total: valor_total

    });

    if (suma === valor_total) {

        mensajeConfirmacion("FELICITACIONES!!! <br>LA PLANILLA HA SIDO AJUSTADA, SI ESTAS SEGURO DE LOS VALORES SUMINISTRADOS PRESIONA EL BOTON <b>GUARDAR<b/>", "400", "180", clase);
        $("#diferencia").html("");
    } else {
        $("#diferencia").html("DIFERENCIA EXTRACTO : " + numberConComas((valor_total - suma)));

    }

}

function getParametersOrden(grid, rowid, clase) {
    var consecutivo = grid.getRowData(rowid).concepto;
    var parameters = {
        keys: true,
        mtype: "post",
        extraparam: {
            concepto: consecutivo
        },
        successfunc: function () {
            return true;
        },
        aftersavefunc: function (rowid, json) {
            var extracto = JSON.parse(json.responseText);
            var total_ingreso = jQuery.trim(extracto.retefuente);
            cacularTotalesNuevos(grid, clase);
        },
        restoreAfterError: true
    };

    return parameters;
}

function abrirExtractoDetalle() {

    $("#extracto_detalle").dialog({
        width: 1100,
        height: 520,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Salir": function () {
                $("#tabla_extracto_detalle").jqGrid("clearGridData", true).trigger("reloadGrid");
                $(this).dialog("close");
            }

        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function mensajeConfirmacion(msj, width, height, clase) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Guardar": function () {
                modificarExtractoDetalle(clase);
            }
            , "Salir": function () {
                $(this).dialog("close");
            }
        }
    });

}

function mensajeVerificacion(msj, width, height, clase) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Continuar": function () {
                VerificarTablas();
                $(this).dialog("close");
            }
            , "Salir": function () {
                $(this).dialog("close");
            }
        }
    });

}


function divEspera(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();


}


function modificarExtractoDetalle(clase) {

    var fullData = jQuery("#tabla_extracto_detalle").jqGrid('getRowData');
    var myJsonString = JSON.stringify(fullData);
    divEspera("Espere un momento por favor...", "270", "130");
    var url = './controller?estado=Corregir&accion=Planilla';
    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: {
            opcion: 3,
            lista: myJsonString,
            clase: clase
        },
        success: function (json) {

            if (json.error) {
                mensajesDelSistema(json.error, '250', '180');
                return;
            }
            if (json.respuesta === "OK") {

               // reloadGridListarPlanillas(jQuery("#tabla_planillas"));
                setTimeout(function () {
                    $("#dialogo").dialog('close');
                    $("#extracto_detalle").dialog('close');
                    $("#dialogo2").dialog('close');
                }, 2000);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }

    });

}

var lastsel2;
function VerificarTablas() {
    divEspera("Espere un momento por favor...", "270", "130");
    var grid_verificar_tabla = jQuery("#tabla_verificar_tabla");
    var editCell = true;
    if ($("#gview_tabla_verificar_tabla").length) {
        reloadGridVerificarTabla(grid_verificar_tabla);
    } else {

        grid_verificar_tabla.jqGrid({
            caption: "Diferencia entre tablas",
            url: "./controller?estado=Corregir&accion=Planilla&opcion=4",
            editurl: "./controller?estado=Corregir&accion=Planilla&opcion=8",
            mtype: "POST",
            datatype: "json",
            height: '350',
            width: '568',
            colNames: ['SECUENCIA', 'EXTRACTO DET', 'ANTI PAGOS TER', 'EXTRACTO', 'ACCIONES'],
            colModel: [
                {name: 'secuencia', index: 'secuencia', width: 80, align: 'center'},
                {name: 'extractodet', index: 'extractodet', width: 105, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'anticipo', index: 'anticipo', width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'extracto', index: 'extracto', editable: true, sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'acciones', index: 'acciones', width: 100, align: 'center'}
            ],
            rowNum: 20,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            loadComplete: function () {
                if (grid_verificar_tabla.jqGrid('getGridParam', 'records') <= 0) {
                    mensajesDelSistema("NO SE ENCONTRARON DIFERENCIAS !!!", "300", "170");
                    $("#verificar_tablas").dialog("close");
                } else {
                    $("#dialogo2").dialog('close');
                    abrirVerificarTablas();
                }
            },
            gridComplete: function () {
                var ids = grid_verificar_tabla.jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    be = "<input style='height:22px;width:50px;' type='button' value='APT' onclick=\"ver_anticipo('" + cl + "');\">";
                    grid_verificar_tabla.jqGrid('setRowData', ids[i], {acciones: be});
                }
            }, onCellSelect: function (rowid, icol, cellcontent, e) {
                if (icol === 4 && editCell) {
                    $("#tabla_verificar_tabla").jqGrid('setRowData', rowid, true, {color: 'red', weightfont: 'bold', background: 'rgba(251, 246, 152, 1)'});
                    editCell = true;
                    grid_verificar_tabla.jqGrid('restoreRow', lastsel2);
                    var editparameters = getParametersExtrato(grid_verificar_tabla, rowid);
                    grid_verificar_tabla.jqGrid('editRow', rowid, editparameters);
                    lastsel2 = rowid;
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });
    }


}

function getParametersExtrato(grid, rowid) {
    var consecutivo = grid.getRowData(rowid).secuencia;
    var parameters = {
        keys: true,
        mtype: "post",
        extraparam: {
            secuencia: consecutivo
        },
        successfunc: function () {
            return true;
        },
        aftersavefunc: function (rowid, json) {
            var anticipos = JSON.parse(json.responseText);
            var respuesta = jQuery.trim(anticipos.respuesta);
            if (respuesta === "OK") {

                mensajesDelSistema("Se han actualizado los valores correctamente.", 240, 150);

            }

        },
        restoreAfterError: true
    };

    return parameters;
}

function reloadGridVerificarTabla(grid_verificar_tabla) {
    grid_verificar_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Corregir&accion=Planilla&opcion=4"
    });
    grid_verificar_tabla.trigger("reloadGrid");
}

function abrirVerificarTablas() {

    $("#verificar_tablas").dialog({
        width: 600,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Validar": function () {
                reloadGridVerificarTabla(jQuery("#tabla_verificar_tabla"));
            },
            "Salir": function () {
                $(this).dialog("close");
            }

        }
    });
}

var last_sel;
function ver_anticipo(idRows) {
    var etd = jQuery("#tabla_verificar_tabla").jqGrid("getRowData", idRows).extractodet;
    $("#valor_refrencia").html("Valor Referencia : " + numberConComas(etd));
    $("#tabla_verificar_tabla").jqGrid('setRowData', idRows, idRows, {color: 'red', weightfont: 'bold', background: 'rgba(251, 246, 152, 1)'});

    var editCell = false;
    var grid_tabla_anticipos = jQuery("#tabla_anticipos");

    if ($("#gview_tabla_anticipos").length) {
        reloadGridTablaAnticipos(grid_tabla_anticipos, idRows);
    } else {

        grid_tabla_anticipos.jqGrid({
            caption: "Diferencia entre tablas",
            url: "./controller?estado=Corregir&accion=Planilla&opcion=7&secuencia=" + idRows,
            editurl: "./controller?estado=Corregir&accion=Planilla&opcion=5",
            mtype: "POST",
            datatype: "json",
            height: '300',
            width: '503',
            colNames: ['ID', 'PLANILLA', 'SECUENCIA', 'VALOR', 'VALOR FOR'],
            colModel: [
                {name: 'identificador', index: 'iidentificador', width: 80, align: 'center', key: true},
                {name: 'planilla', index: 'planilla', width: 85, align: 'right'},
                {name: 'secuencia', index: 'secuencia', width: 100, align: 'center'},
                {name: 'valor', index: 'valor', editable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}},
                {name: 'valor_for', index: 'valor_for', editable: true, sortable: true, width: 100, align: 'right', sorttype: 'currency',
                    formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ",", decimalPlaces: 0, prefix: "$ "}}

            ],
            rowNum: 20,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            rownumbers: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {

                editCell = true;
                grid_tabla_anticipos.jqGrid('restoreRow', last_sel);
                var editparameters = getParametersAnticipos(grid_tabla_anticipos, rowid, idRows);
                grid_tabla_anticipos.jqGrid('editRow', rowid, editparameters);
                last_sel = rowid;


            },
            loadComplete: function () {
                if (grid_tabla_anticipos.jqGrid('getGridParam', 'records') <= 0) {
                    // mensajeVerificacion("NO SE ENCONTRARON PLANILLAS DESCUADRADAS !!! <br>VERIFIQUE LAS DIFERENCIAS ENTRE TABLAS PRESIONANDO EL BOTON VERIFICAR ", "430", "170");
                } else {
                    abrirAnticipos();
                    cacularTotalesAnticipos(grid_tabla_anticipos);
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        });
    }

}

function getParametersAnticipos(grid, rowid, idRows) {
    var consecutivo = grid.getRowData(rowid).identificador;
    var parameters = {
        keys: true,
        mtype: "post",
        extraparam: {
            identificador: consecutivo
        },
        successfunc: function () {
            return true;
        },
        aftersavefunc: function (rowid, json) {
            var anticipos = JSON.parse(json.responseText);
            var respuesta = jQuery.trim(anticipos.respuesta);
            if (respuesta === "OK") {

                mensajesDelSistema("Se han actualizado los valores correctamente.", 240, 150);

            }
            cacularTotalesAnticipos(grid);
        },
        restoreAfterError: true
    };

    return parameters;
}

function reloadGridTablaAnticipos(grid_tabla_anticipos, idRows) {
    grid_tabla_anticipos.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Corregir&accion=Planilla&opcion=7&secuencia=" + idRows
    });
    grid_tabla_anticipos.trigger("reloadGrid");
}


function  cacularTotalesAnticipos(grid_tabla_anticipos) {

    var valor = grid_tabla_anticipos.jqGrid('getCol', 'valor', false, 'sum');
    var valor_for = grid_tabla_anticipos.jqGrid('getCol', 'valor_for', false, 'sum');

    grid_tabla_anticipos.jqGrid('footerData', 'set', {
        secuencia: 'Totales:',
        valor: valor,
        valor_for: valor_for
    });

}
function abrirAnticipos() {

    $("#ver_anticipos").dialog({
        width: 650,
        height: 500,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Salir": function () {
                $("#tabla_anticipos").jqGrid("clearGridData", true).trigger("reloadGrid");
                $(this).dialog("close");
            }

        }
    });

}

function numberConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function ver_extracto(idRows) {
    alert("extracto :" + idRows);
    $("#tabla_verificar_tabla").jqGrid('setRowData', idRows, idRows, {color: 'red', weightfont: 'bold', background: 'rgba(251, 246, 152, 1)'});
}

