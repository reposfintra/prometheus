var oldQuery    = "";
var countParams = 0;
var countParamSets = 0;

function setPageBounds(left, top, width, height) {
  return ("top=" + top + ",left=" + left + ",width=" +
  width + ",height=" + height);
}

function abrirPagina(url, nombrePagina) {
  var wdth = screen.width - screen.width * 0.20;
  var hght = screen.height - screen.height * 0.40;
  var lf = screen.width * 0.1;
  var tp = screen.height * 0.2;
  var options = "menubar=yes,scrollbars=yes,resizable=yes,status=yes,titlebar=no," +
  "toolbar=no," + setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( document.window != null && !hWnd.opener )
    hWnd.opener = document.window;
}

function trim(str) {
  var charFound = false;
  var trimmedString = "";
  if( str != null && str != "" )
  {
    // Quitar los espacios de la izquierda.
    var idx = 0; 
    var jdx = 0;
    while( idx < str.length && !charFound )
    {
      charFound = (str.charAt(idx) != ' ');
      if( !charFound ) idx++;
    }
    trimmedString = str.substring(idx, str.length);
    
    // Quitar los espacios de la derecha si aun quedan caracteres.
    if( trimmedString != "" )
    {
      charFound = false;
      idx = str.length - 1;
      while( idx >= 0 && !charFound )
      {
        charFound = (str.charAt(idx) != ' ');
        if( !charFound ) idx--;
      }
      trimmedString = trimmedString.substring(0, idx + 1);
    }
  }
  return trimmedString;
}

function countSelectedOptions(selectObj) {
  var contSelected = 0;
  for( var idx = 0; idx < selectObj.options.length; idx++ ) {
    if( selectObj.options[idx].selected )
      contSelected += 1;
  }
  return contSelected;
}

function contarTokens(consulta, nombreToken){
  return (consulta.split(nombreToken).length - 1);
}

function removeOptions(selectObj, fromIdx) {
  while( selectObj.options.length > fromIdx )
    selectObj.options.remove(selectObj.options.length - 1);
}

function PantallaConsultasUpdateLoad() {
  var paramCsvList = document.filtroCrearQueryFrm.nombresParams;
  var paramNamesList = document.filtroCrearQueryFrm.paramNamesList;
  paramCsvList.value = "";
  for( var idx = 0; idx < paramNamesList.options.length; idx++ )
  {
    paramCsvList.value += (paramCsvList.value == "" ? "" : ",") +
    paramNamesList.options[idx].value;
  }
}

function queryBlur() {
  var query = trim(document.filtroCrearQueryFrm.query.value);
  var nuevoValorParamBtns  = document.getElementsByName("nuevoValorParamBtn");
  var quitarValorParamBtns = document.getElementsByName("quitarValorParamBtn");
  if( oldQuery != query && query != "" ) {
    oldQuery = query;
    countParams = contarTokens(query, "<param>");
    countParamSets = contarTokens(query, "<param-in>");
    var sumParams = countParams + countParamSets;
    var selectObj = document.filtroCrearQueryFrm.paramNamesList;
    removeOptions(selectObj, countParams);
    
    selectObj = document.filtroCrearQueryFrm.paramValuesList;
    removeOptions(selectObj, countParams);
    
    selectObj = document.filtroCrearQueryFrm.paramSetList;
    removeOptions(selectObj, countParamSets);
    
    document.filtroCrearQueryFrm.nombreparam.value = "";
    document.filtroCrearQueryFrm.nombresParams.value = "";
    document.filtroCrearQueryFrm.valorparam.value = "";
    document.filtroCrearQueryFrm.valoresParams.value = "";
    document.filtroCrearQueryFrm.valoresconjparams.value = "";
    document.filtroCrearQueryFrm.valoresParamsIn.value = "";
    if( sumParams > 0 ) {
      if( countParams > 0 ) {
        document.filtroCrearQueryFrm.valorparam.disabled = false;
        for( var idx = 0; idx < nuevoValorParamBtns.length; idx++ ) {
          if( nuevoValorParamBtns[idx].id != "paramCsvAgregar" )
            nuevoValorParamBtns[idx].disabled = false;
          if( quitarValorParamBtns[idx].id != "paramCsvBorrar" )
            quitarValorParamBtns[idx].disabled = false;
        }
      }else{
        document.filtroCrearQueryFrm.valorparam.disabled = true;
        for( var idx = 0; idx < nuevoValorParamBtns.length; idx++ ) {
          if( nuevoValorParamBtns[idx].id != "paramCsvAgregar" )
            nuevoValorParamBtns[idx].disabled = true;
          if( quitarValorParamBtns[idx].id != "paramCsvBorrar" )
            quitarValorParamBtns[idx].disabled = true;
        }
      }
      if( countParamSets > 0 ) {
        for( var idx = 0; idx < nuevoValorParamBtns.length; idx++ ) {
          if( nuevoValorParamBtns[idx].id == "paramCsvAgregar" )
            nuevoValorParamBtns[idx].disabled = false;
          if( quitarValorParamBtns[idx].id == "paramCsvBorrar" )
            quitarValorParamBtns[idx].disabled = false;
        }
        document.filtroCrearQueryFrm.valoresconjparams.disabled = false;
      }else{
        for( var idx = 0; idx < nuevoValorParamBtns.length; idx++ ) {
          if( nuevoValorParamBtns[idx].id == "paramCsvAgregar" )
            nuevoValorParamBtns[idx].disabled = true;
          if( quitarValorParamBtns[idx].id == "paramCsvBorrar" )
            quitarValorParamBtns[idx].disabled = true;
        }
        document.filtroCrearQueryFrm.valoresconjparams.disabled = true;
      }
      document.filtroCrearQueryFrm.nombreparam.disabled = false;
      for( var idx = 0; idx < nuevoValorParamBtns.length; idx++ ) {
        if( nuevoValorParamBtns[idx].id == "nombreParamAgregar" )
          nuevoValorParamBtns[idx].disabled = false;
        if( quitarValorParamBtns[idx].id == "nombreParamBorrar" )
          quitarValorParamBtns[idx].disabled = false;
      }
    }else{
      document.filtroCrearQueryFrm.nombreparam.disabled = true;
      document.filtroCrearQueryFrm.valorparam.disabled = true;
      document.filtroCrearQueryFrm.valoresconjparams.disabled = true;
      for( var idx = 0; idx < nuevoValorParamBtns.length; idx++ ) {
        nuevoValorParamBtns[idx].disabled = true;
        quitarValorParamBtns[idx].disabled = true;
      }
    }
  }else if( query == "" ){
    oldQuery = query;
    for( var idx = 0; idx < nuevoValorParamBtns.length; idx++ ) {
      nuevoValorParamBtns[idx].disabled = true;
      quitarValorParamBtns[idx].disabled = true;
    }
    document.filtroCrearQueryFrm.nombreparam.disabled = true;
    document.filtroCrearQueryFrm.valorparam.disabled = true;
    document.filtroCrearQueryFrm.valoresconjparams.disabled = true;
    document.filtroCrearQueryFrm.nombreparam.value = "";
    document.filtroCrearQueryFrm.nombresParams.value = "";
    document.filtroCrearQueryFrm.valorparam.value = "";
    document.filtroCrearQueryFrm.valoresParams.value = "";
    document.filtroCrearQueryFrm.valoresconjparams.value = "";
    document.filtroCrearQueryFrm.valoresParamsIn.value = "";
  }
}

function valoresParamKeyPress(userInputCtrl) {
  /*
  alert("'" + userInputCtrl.value + "'")
  if( event.keyCode == 37 && userInputCtrl.value == "" )
    userInputCtrl.value = "%";
   */
  return true;
}

function nuevoDatoParamBtnClick(dataContainerId, valueObjId, csvDataId) {
  var selectObj = document.getElementById(dataContainerId);
  var valueObj  = document.getElementById(valueObjId);
  var paramName = trim(valueObj.value);
  if( paramName != "" ) {
    if( selectObj.id == "paramNamesListId" && selectObj.options.length == countParams + countParamSets )
      alert(
      "�� Ya se insert� el m�x. n�mero de nombres para" +
      "\nlos par�metros permitidos por el query !!"
      );
    else if( selectObj.id == "paramValuesListId" && selectObj.options.length == countParams  )
      alert(
      "�� Ya se insert� el m�x. n�mero de valores para" +
      "\nlos par�metros especificos permitidos por el query !!"
      );
    else if( selectObj.id == "paramSetListId" && selectObj.options.length == countParamSets  )
      alert(
      "�� Ya se insert� el m�x. n�mero de nombres/valores para" +
      "\nlos conjuntos de par�metros CSV permitidos por el query !!" +
      "\n(\"CSV\": siglas en ingles de \"valores separados por comas\")"
      );
    else{
      var option = document.createElement("OPTION");
      selectObj.options.add(option);
      if( selectObj.id == "paramSetListId" ) {
        var paramNameSplitted = paramName.split("\n");
        paramName = "";
        for( var idx = 0; idx < paramNameSplitted.length; idx++ ) {
          var parameter = trim(paramNameSplitted[idx]).replace("\n", "").replace("\r", "");
          if( parameter != "" )
            paramName += (paramName == "" ? "" : ",") + parameter;
        }
      }
      option.text = paramName;
      option.value = paramName;
      var paramCsvList = document.getElementById(csvDataId);
      paramCsvList.value += ( paramCsvList.value == "" ?
        "" : (selectObj.id == "paramSetListId" ? ";" : ",")
        ) + paramName;
    }
  }else
    alert("�� Debe digitar el valor a insertar !!");
}

function quitarDatoParamBtnClick(dataContainerId, csvDataId) {
  var selectObj = document.getElementById(dataContainerId);
  if( selectObj.selectedIndex >= 0 ) {
    var contSelected = countSelectedOptions(selectObj), idx, jdx, optionRemoved;
    for( idx = 1; idx <= contSelected; idx++ ) {
      jdx = 0; optionRemoved = false;
      while( !optionRemoved && jdx < selectObj.options.length ) {
        if( selectObj.options[jdx].selected ) {
          selectObj.options.remove(jdx);
          optionRemoved = true;
        }else
          jdx += 1;
      }
    }
    var paramCsvList = document.getElementById(csvDataId);
    paramCsvList.value = "";
    for( idx = 0; idx < selectObj.options.length; idx++ ) {
      paramCsvList.value += ( paramCsvList.value == "" ?
        "" : (selectObj.id == "paramSetListId" ? ";" : ",")
        ) + selectObj.options[idx].value;
    }
  }else
    alert("�� Debe seleccionar al menos el nombre/valor de un par�metro para poder eliminarlo !!");
}


function validarQuery(query, queryPattern) {
  var queryPatternExec = queryPattern.exec(query);
  return (queryPatternExec != null && queryPatternExec[0] == query);
}

function validarQueryCompleto(queryString) {
  var idx = 0;
  var esQueryCorrecto = true;
  var query = trim(queryString).toLowerCase();
  if( query != "" ) {
    if( query.indexOf("<br>") == 0 )
      query = query.substring(4, query.length);
    if( query.lastIndexOf("<br>") == query.length - 4 )
      query = query.substring(0, query.lastIndexOf("<br>"));
    var queryPart = "";
    var splittedQuery = query.split("<br>");
    if( splittedQuery.length > 0 )
      while( idx < splittedQuery.length &&
      trim(splittedQuery[idx]).toLowerCase().indexOf("select") != 0 )
        idx++;
    else
      splittedQuery = new Array(query);
    if( splittedQuery.length == 1 && idx > 0 )
      alert("Debe digitar al menos un query de seleccion!");
    else{
      queryPart = trim(splittedQuery[idx]).toLowerCase();
      var createTablePattern = /create(\s+)(temp|temporary)(\s+)table(\s+)(\w+)(\s+)as(\s+)select(.+)/i;
      idx = 0;
      while( idx < splittedQuery.length && queryPart.length > 5 &&
      queryPart.substring(0, 6) == "create" && esQueryCorrecto ) {
        esQueryCorrecto = validarQuery(queryPart, createTablePattern);
        if( esQueryCorrecto ) {
          idx++;
          if( idx < splittedQuery.length )
            queryPart = trim(splittedQuery[idx]);
        }else
          alert("Hay un error en uno de los queries de creacion de tabla.\nRectifique.");
      }
      if( idx < splittedQuery.length && esQueryCorrecto ) {
        var selectPattern = /select(\s+)(.+)from(\s+)(.+)/i;
        esQueryCorrecto = (idx < splittedQuery.length &&
        queryPart.length > 5 &&
        queryPart.substring(0, 6) == "select" &&
        validarQuery(queryPart, selectPattern));
        /*
        alert(
        "idx < splittedQuery.length ? " + (idx < splittedQuery.length) +
        "\nqueryPart.length > 5 ? " + (queryPart.length > 5) +
        "\nqueryPart.substring(0, 6) == 'select' ? " + (queryPart.substring(0, 6) == "select") +
        "\nvalidarQuery(queryPart, selectPattern) ? " + validarQuery(queryPart, selectPattern)
        );
         */
        if( esQueryCorrecto ) {
          idx++;
          if( idx < splittedQuery.length && esQueryCorrecto ) {
            queryPart = trim(splittedQuery[idx]);
            var dropTablePattern = /drop(\s+)table(\s+)(\w+)/i;
            esQueryCorrecto = (idx < splittedQuery.length &&
            queryPart.length > 3 &&
            queryPart.substring(0, 4) == "drop");
            while( idx < splittedQuery.length && esQueryCorrecto ) {
              esQueryCorrecto = validarQuery(queryPart, dropTablePattern);
              if( esQueryCorrecto ) {
                idx++;
                if( idx < splittedQuery.length ) {
                  queryPart = trim(splittedQuery[idx]);
                  esQueryCorrecto = (idx < splittedQuery.length &&
                  queryPart.length > 5 &&
                  queryPart.substring(0, 4) == "drop");
                }
              }else
                alert("Hay un error en uno de los queries de eliminacion de tabla.\nRectifique.");
            }
          }
        }else
          alert("Hay un error en el query de seleccion.\nRectifique.");
      }
      //if( esQueryCorrecto ) alert("OK!");
    }
  }else
    alert("Debe digitar el query!");
  return esQueryCorrecto;
}

function parametrosSonCorrectos( actionName ) {
  var isValidQuery  = true;
  var creador       = trim(document.filtroCrearQueryFrm.creador.value);
  var usuario       = document.filtroCrearQueryFrm.usuario.value;
  var descripcion   = trim(document.filtroCrearQueryFrm.descripcion.value);
  var query         = trim(document.filtroCrearQueryFrm.query.value.replace(/\n/ig, " ").replace(/\r/ig, " "));
  var paramNamesListSize  = document.filtroCrearQueryFrm.paramNamesList.options.length;
  var paramValuesListSize = document.filtroCrearQueryFrm.paramValuesList.options.length;
  var paramSetListSize = document.filtroCrearQueryFrm.paramSetList.options.length;
  countParams = contarTokens(query, "<param>");
  countParamSets = contarTokens(query, "<param-in>");
  var sumParams = countParams + countParamSets;
  // VERIFICAR QUE TODOS LOS PARAMETROS FUERON DIGITADOS.
  isValidQuery = validarQueryCompleto(query);
  if( isValidQuery && descripcion == "" && actionName == 'save' ) {
    alert( "�� Debe digitar la descripcion del query para poder guardarlo !!" );
    isValidQuery = false;
  }else if( sumParams != paramNamesListSize && (actionName == 'save' || actionName == 'update')){
    alert(
    "�� Debe digitar los nombres para los " + sumParams +
    " par�metros que tiene este query para poder salvarlo o actualizarlo!!"
    );
    isValidQuery = false;
  }else if( sumParams != (paramValuesListSize + paramSetListSize) && actionName == 'execute'){
    alert(
    "�� Debe digitar los valores correspondientes a los " + sumParams +
    " par�metros que tiene este query para poder ejecutarlo !!"
    );
    isValidQuery = false;
  }
  return isValidQuery;
}

function queryActionBtnsClick( actionName, baseUrl ) {
  var list = document.filtroCrearQueryFrm.userAdd;
  var param = "";
  if( parametrosSonCorrectos(actionName) ) {
    var queryObj = document.filtroCrearQueryFrm.query;
    queryObj.value = trim(queryObj.value).replace(/<param>/gi, "?");
    var paramSetList = document.filtroCrearQueryFrm.paramSetList;
    if( actionName == 'execute' ){
      for( var idx = 0; idx < paramSetList.options.length; idx++ ) {
        var csvValues = "";
        var csvSplittedValues = paramSetList.options[idx].value.split(",");
        for( var jdx = 0; jdx < csvSplittedValues.length; jdx++ ) {
          var csvValue = trim(csvSplittedValues[jdx]).replace(/\n/ig, " ").replace(/\r/ig, " ");
          csvValues += (csvValues == "" ? "'" : ",'") + csvValue + "'";
        }
        queryObj.value = queryObj.value.replace(/<param-in>/i, csvValues);
      }
      document.filtroCrearQueryFrm.cmd.value = "sql_execute";
      document.filtroCrearQueryFrm.submit();
    }else if( actionName == 'save' ){
      if (list.length > 0){
        selectItems(list);
        document.filtroCrearQueryFrm.cmd.value = "sql_create";
        document.filtroCrearQueryFrm.submit();
      }else
        alert("DEBE AGREGAR AL MENOS UN USUARIO ASOCIADO AL QUERY PARA PODER GUARDAR LA CONSULTA");
    }else if (actionName == 'update'){
      if (list.length > 0){
        selectItems(list);
        document.filtroCrearQueryFrm.cmd.value = "sql_update";
        document.filtroCrearQueryFrm.submit();
      }else
        alert("DEBE AGREGAR AL MENOS UN USUARIO ASOCIADO AL QUERY PARA PODER GUARDAR LA CONSULTA");
    }
  }
}

function executeQuery( baseUrl, recordId ) {
  var mayExecute = true;
  var paramsName = "param" + recordId;
  var params = "?estado=Consultas&accion=Manage&cmd=sql_listexec&recordId=" + recordId +
  "&descripcion=" + document.getElementById("desc" + recordId).value +
  "&contentType=" + document.getElementById("contentType" + recordId).value +
  "&baseDeDatos=" + document.getElementById("baseDeDatos" + recordId).value;
  var conteoParams = parseInt(document.getElementById("conteoParamsId" + recordId).value);
  if(conteoParams > 0) {
    for( var idx = 0; idx < conteoParams && mayExecute; idx++ ) {
      var parameterValue = "";
      var parameter = document.getElementById(paramsName + idx);
      //alert("parameter.value = " + parameter.value);
      if( parameter.tagName.toUpperCase() == "TEXTAREA" ) {
        var parameterValueSplitted = parameter.value.split(/\n/g);
        for( var jdx = 0; jdx < parameterValueSplitted.length; jdx++ ) {
          var param = trim(parameterValueSplitted[jdx]).replace(/\n/ig, "").replace(/\r/ig, "");
          if( param != "" )
            parameterValue += (parameterValue == "" ? "" : ",") + param;
            //alert("textarea parameterValue = " + parameterValue);
        }
      }else
        parameterValue = parameter.value;
      
      if( parameterValue == "" )
        mayExecute = false;
      else
        params += "&param" + recordId + idx + "=" + parameterValue.replace(/%/g, "<percentage>");
    }
  }
  if( mayExecute ) {
    //alert(baseUrl + params);
    abrirPagina(baseUrl + params, "wexecutesavedquery");
  }else
    alert("��Debe digitar los par�metros necesarios para ejecutar el query!!");
}

function addOption(list,valor,texto){
  var opt = document.createElement("OPTION");
  opt.value=valor;
  opt.text=texto;
  list.add(opt);
}

function addElement(elem, list){
  if (elem.value != ''){
    for(i=0; i < list.length; i++)
      if (list.options[i].value == elem.value){
        alert("Este usuario ya fue agregado, por favor seleccione otro");
        return;
      }
    addOption(list, elem.value, elem.options[elem.selectedIndex].text);
  }
  else{
    alert("Seleccione un usuario para agregar");
  }
}

function removeElem(list){
  for(i=(list.length-1);i>=0;i--)
    if (list.options[i].selected) list.remove(i);
}

function selectItems(list){
  for (i = 0; i < list.length; i++){
    list.options[i].selected = true;
  }
}