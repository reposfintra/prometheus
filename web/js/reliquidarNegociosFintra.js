/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
   cargarnegociosrealiquidar();
   maximizarventana();
    
   
    
    $('.solo-numero').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });   
    
    $('.solo-numeric').live('keypress', function(event) {
        return numbersonly(this, event);
    });
    
    $('.solo-numeric').live('blur',function (event) {  
           this.value = numberConComas(this.value);            
    });
    
});

function cargarnegociosrealiquidar(){

    var gripReliquidar = $("#tabla_negocios_reliquidar");
    if ($("#gview_tabla_negocios_reliquidar").length) {
         refrescarCargarNegociosReliquidar();
     }else {
         gripReliquidar.jqGrid({        
            caption:'RELIQUIDAR NEGOCIOS FINTRA',
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '380',
            width: '1400',
            colNames: ['NEGOCIO','CLIENTE','IDENTIFICACION','AFILIADO','NIT AFILIADO','NUMERO SOLICITUD','ID CONVENIO','CUOTAS','VALOR','TIPO CARRERA','ESTADO','UNID NEGOCIO','FECHA PRIMERA CUOTA'],
            colModel: [
                {name: 'COD_NEG', index: 'COD_NEG', width: 80, resizable:false, sortable: true, align: 'center', hidden:false}, 
                {name: 'NOMBRE', index: 'NOMBRE', width: 280, resizable:false, sortable: true, align: 'center'},
                {name: 'COD_CLI', index: 'COD_CLI', resizable:false, sortable: true, width: 80, align: 'center'},
                {name: 'PAYMENT_NAME', index: 'PAYMENT_NAME', resizable:false, sortable: true, width: 265, align: 'center'},
                {name: 'AFILIADO', index: 'AFILIADO', resizable:false, sortable: true, width: 80, align: 'center'},
                {name: 'NUMERO_SOLICITUD', index: 'NUMERO_SOLICITUD', resizable:false, sortable: true, width: 100, align: 'center'},
                {name: 'ID_CONVENIO', index: 'ID_CONVENIO', resizable:false, sortable: true, width: 80, align: 'center'},
                //{name: 'NOMBRE', index: 'NOMBRE', resizable:false, sortable: true, width: 120, align: 'center'},
                {name: 'NRO_DOCS', index: 'NRO_DOCS', resizable:false, sortable: true, width:80, align: 'center'},               
                {name: 'VR_DESEMBOLSO', index: 'VR_DESEMBOLSO', sortable: true, width: 120, align: 'center', search: false, sorttype: 'number',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'TIPO_CARRERA', index: 'TIPO_CARRERA', width: 80, resizable:false, sortable: true, align: 'center', hidden:true},                
                {name: 'ESTADO_NEG', index: 'ESTADO_NEG', width: 80, resizable:false, sortable: true, align: 'center', hidden:true}, 
                {name: 'ID_UNID_NEGOCIO', index: 'ID_UNID_NEGOCIO', width: 80, resizable:false, sortable: true, align: 'center', hidden:true}, 
                {name: 'FECHA', index: 'FECHA', width: 180, resizable:false, sortable: true, align: 'center', hidden:false} 
               
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            pager:'#page_tabla_negocios_reliquidar',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {                
                dataType: "json",
                type: "POST",  
                async:false,
                data: {
                    opcion: 0
                }
            },    
            gridComplete: function() {
                var cant = jQuery("#tabla_negocios_reliquidar").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_negocios_reliquidar").getRowData(cant[i]).cambio;                 
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoConfFactor('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_negocios_reliquidar").jqGrid('setRowData', cant[i], {cambio: be});

                }
            },
            loadComplete: function() { 
                 if($("#tabla_negocios_reliquidar >tbody >tr").length<=1){
              mensajesDelSistema("NO HAY NEGOCIOS A RELIQUIDAR", '230', '150', false);
             }
             
             },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_negocios_reliquidar"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                
                if(filas.ID_UNID_NEGOCIO=="1"){//si el negocio a reliquidar es microcredito va a abrir el modulo donde se reliquidan actualmente
               
                 window.open("./controller?estado=Menu&accion=Cargar&carpeta=/jsp/fenalco/liquidadores/&pagina=liquidadorNegMicrocredito.jsp", "Reliquidar Microcredito", "width=1000, height=580");
                    
                }else{
                ver_reliquidar_negocio();
            }

            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_negocios_reliquidar",{search:false,refresh:false,edit:false,add:false,del:false});      

     }
}


function refrescarCargarNegociosReliquidar(){    
    jQuery("#tabla_negocios_reliquidar").setGridParam({
        url: "./controller?estado=Negocios&accion=Fintra",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async:false,
            data: {
                opcion: 0
            }
        }
    }).trigger("reloadGrid");
}
function refrescarcargarDocumentNegAceptados(){    
    jQuery("#tabla_documen_neg_aceptados").setGridParam({
        url: "./controller?estado=Negocios&accion=Fintra",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            async:false,
            data: {
                opcion: 1,
                negocio: $("#negocio").val(),
                valor: $("#valor").val(),
                cuota: $("#cuota").val(),
                convenio: $("#convenio").val(),
                fechacuota: $("#fechacuota").val()
            }
        }
    }).trigger("reloadGrid");
}

function ver_reliquidar_negocio(){
    $('#div_reliquidar_negocio').fadeIn("slow");
    var grip = jQuery("#tabla_negocios_reliquidar"), SelRowIds=grip.jqGrid("getGridParam","selrow"),filas;
     
    filas=grip.jqGrid("getLocalRow",SelRowIds);
    var numero_solicitud = filas.NUMERO_SOLICITUD;
    var afiliado = filas.AFILIADO;
    var id_convenio = filas.ID_CONVENIO;
    var cuotas = filas.NRO_DOCS;
    var valor = filas.VR_DESEMBOLSO;
    var tipo_carrera =filas.TIPO_CARRERA;
    var cod_neg =filas.COD_NEG;
    var estado_neg =filas.ESTADO_NEG;
    var cod_cli =filas.COD_CLI;
    
    
    $('#numero_solicitud').val(numero_solicitud);
    $('#afiliado').val(afiliado);
    $('#convenio').val(id_convenio);
    $('#valor').val(valor); 
    $('#tipo_carrera').val(tipo_carrera); 
    $('#negocio').val(cod_neg);
    $("#cuota").val(cuotas);
    $("#cod_cli").val(cod_cli);
    
   
    if (estado_neg==="F"){
  
    $('#cuota').attr('disabled','disabled');
    $('#valor').attr('disabled','disabled');
        
    }
         
    AbrirDivReliquidarnegocio();
    $('#cuota option:contains("'+cuotas+'")').each(function(){
         if ($(this).text() == cuotas) {
             $(this).attr('selected', 'selected');
             return false;
         }
         return true;
     });
     getDatosClienteLiquidacion(numero_solicitud);
   
   
}


function AbrirDivReliquidarnegocio(){
    
     calcularcuota();
      $("#div_reliquidar_negocio").dialog({
        width: 'auto',
        height: 250,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'RELIQUIDAR NEGOCIO FINTRA',
        closeOnEscape: false,
        buttons: {
            "Reliquidar": function () { 
           AbrirDocumentNegAceptados();
            $(this).dialog("destroy");
          
            
            },
            "Salir": function () {  
                $(this).dialog("destroy");
            }
        }
    });       
}



function resetearValoresConfigFactor(){    
    $('#codigo_convenio').val('');
    $('#und_negocio').val('');
    $('#nit_empresa_fianza').val('');
    $('#plazo_inicial').val('');
    $('#plazo_final').val('');   
    $('#porcentaje_comision').val('');
    $('#valor_comision').val('');   
    $('#porcentaje_iva').val('');
}


function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}

function mensajeConfirmAction(msj, width, height, okAction, id) {  
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");               
            }
        }
    });

}

function mensajesDelSistema2(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("destroy"); 
                location.reload();
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}

 function carcularFecha(inputDate) {
     
    $('#fechacuota').empty();
    
    var aux = replaceAll(inputDate, "-", "/");   
    var date = new Date(aux); 
    date.setDate(date.getDate() + 30); 
    
    var days = date.getDate();
    var fecha = "0099-01-01";
    var mes = date.getMonth() + 1;
    var anio = date.getFullYear();
            
                if (days >= 1 && days <= 2) {
                    
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-02";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-12";
                    opcion1 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-17";
                    opcion2 = new Option(fecha, fecha);
                    //fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-22";
                    //opcion3 = new Option(fecha, fecha);
          
                 }
              
                if (days > 2 && days <= 12) {

                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-12";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-17";
                    opcion1 = new Option(fecha, fecha);
                    //fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-22";
                    //opcion2 = new Option(fecha, fecha);
                    
                    if(mes === 12){
                        
                        fecha = (anio +1) + "-" +"01" + "-02";
                        opcion2 = new Option(fecha, fecha);
                        
                    }else{
                        
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
                    opcion2 = new Option(fecha, fecha);
                   
                    }
           
                }

                if (days > 12 && days <= 17) {

                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-17";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    //fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-22";
                    //opcion1 = new Option(fecha, fecha);
                    
                     if(mes === 12){
                        
                        fecha = (anio +1) + "-" +"01" + "-02";
                        opcion1 = new Option(fecha, fecha);
                        fecha = (anio +1) + "-" +"01" + "-12";
                        opcion2 = new Option(fecha, fecha);
                        
                    }else{
                    
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
                    opcion1 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
                    opcion2 = new Option(fecha, fecha);
                    
                    }

                }

                if (days > 17 && days <= 22) {

                    //fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-22";
                    //opcion0 = new Option(fecha, fecha, "defauldSelected");
                    
                    if(mes === 12){
                        
                    fecha = (anio +1) + "-" + "01" + "-02";
                    opcion0 = new Option(fecha, fecha);
                    fecha = (anio +1) + "-" +  "01" + "-12";
                    opcion1 = new Option(fecha, fecha);
                    fecha = (anio +1) + "-" +  "01" + "-17";
                    opcion2 = new Option(fecha, fecha);
                    
                    }else{
                    
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
                    opcion0 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
                    opcion1 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
                    opcion2 = new Option(fecha, fecha);
                    
                    }



                }


                if (days > 22 && days <= 31) {
                    
                    
                   if(mes === 12){
                       
                    fecha = (anio +1) + "-" + "01" + "-02";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    fecha = (anio +1) + "-" + "01"  + "-12";
                    opcion1 = new Option(fecha, fecha);
                    fecha = (anio +1) + "-" + "01"  + "-17";
                    opcion2 = new Option(fecha, fecha);
                    //fecha = (anio +1) + "-" + "01"  + "-22";
                    //opcion3 = new Option(fecha, fecha);
                           
                           
                   }else{

                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
                    opcion1 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
                    opcion2 = new Option(fecha, fecha);
                    //fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
                    //opcion3 = new Option(fecha, fecha);

                   }
       
                }

         var combo = document.getElementById("fechacuota");
         combo.options.add(opcion0);
         combo.options.add(opcion1);
         combo.options.add(opcion2);
         //combo.options.add(opcion3);
    }
     
     function calcularcuota(){ 
         var myGrid = jQuery("#tabla_negocios_reliquidar"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
         filas = myGrid.jqGrid("getLocalRow", selRowIds);
                
               
        $("#cuota").empty();   
       var tc=$('#tipo_carrera').val(); //Tipo carrera
       var afi=$('#afiliado').val(); //Afiliado
       var combo = document.getElementById("cuota");
       
       if(filas.ID_UNID_NEGOCIO=="31"){
//         if (tc=="POSGRADO"){
//             
//             for(var i = 4; i<=36; i++){
//                 combo.options.add(new Option(i,i));
//             }
//       }else{
//           
//                if ( afi=="8020110655"){ //SI ES UNI ITSA SOLO 4 CUOTAS
//
//                 combo.options.add(new Option("4","4")); //itsa solo 4
//                    
//                }else if (afi=="8909109617"){ // SI ES UNI CEIPA SOLO 5 Y 6 CUOTAS
//                    
//                   combo.options.add(new Option("5","5"));
//                   combo.options.add(new Option("6","6"));
//                 
//                }else if (afi=="8600137985"){ // SI ES  UNI LIBRE y es derecho
//                    
//                  for(var i = 4; i<=12; i++){
//                             combo.options.add(new Option(i,i)); 
//                        }
//                 
//                }else if (afi=="8020152105"){
//                    
//                    if (filas.VR_DESEMBOLSO<="1500000"){
//                        
//                         for(var i = 6; i<=15; i++){
//                             combo.options.add(new Option(i,i)); 
//                        }
//                    }else {
//                        
//                         for(var i = 6; i<=24; i++){
//                             combo.options.add(new Option(i,i)); 
//                        }
//                        
//                    }
//                    
//                    
//                }else{
//         
//                  for(var i = 6; i<=8; i++){
//                         combo.options.add(new Option(i,i)); 
//                    }
//                }
//        }

            for(var i = 4; i<=36; i++){
                combo.options.add(new Option(i,i)); 
            }
    }else{
        
          for(var i = 3; i<=36; i++){
                             combo.options.add(new Option(i,i)); 
                        }
    }
 }
 
 function cargarDocumentNegAceptados(){
var gripReliquidar = $("#tabla_documen_neg_aceptados");
refrescarcargarDocumentNegAceptados();

    if ($("#gview_tabla_documen_neg_aceptados").length) {
         refrescarcargarDocumentNegAceptados();
     }else {
         gripReliquidar.jqGrid({        
            caption:'RELIQUIDAR NEGOCIOS FINTRA',
            url: "./controller?estado=Negocios&accion=Fintra",
            mtype: "POST",
            datatype: "json",
            height: '380',
            width: '1120',
            colNames: ['NEGOCIO','FECHA','DIAS','NO. CUOTA','SALDO INICIAL','CAPITAL','INTERES','CUSTODIA','REMESA','SEGURO','CUOTA ADMINISTRACION','VALOR CUOTA','SALDO FINAL','VALOR AVAL'],
            colModel: [
                //FECHA::DATE,ITEM,SALDO_INICIAL,CAPITAL,INTERES,CUSTODIA,REMESA,SEGURO,CUOTA_MANEJO,VALOR,SALDO_FINAL
                {name: 'COD_NEG', index: 'COD_NEG', width:80, resizable:false, sortable: true, align: 'center', hidden:true},
                {name: 'FECHA', index: 'FECHA', width:80, resizable:false, sortable: true, align: 'center'},
                {name: 'DIAS', index: 'DIAS', width:80, resizable:false, sortable: true, align: 'center'},
                {name: 'ITEM', index: 'ITEM', resizable:false, sortable: true, width: 80, align: 'center'},
                {name: 'SALDO_INICIAL', index: 'SALDO_INICIAL',  sortable: true, width: 100, align: 'center', search: false, sorttype: 'number',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'CAPITAL', index: 'CAPITAL', summaryType:'sum', sortable: true, width: 100, align: 'center', search: false, sorttype: 'number',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'INTERES', index: 'INTERES', summaryType:'sum', sortable: true, width: 100, align: 'center', search: false, sorttype: 'number',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'CUSTODIA', index: 'CUSTODIA', sortable: true, width: 100, align: 'center', search: false, sorttype: 'number',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'REMESA', index: 'REMESA', sortable: true, width: 100, align: 'center', search: false, sorttype: 'number',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'SEGURO', index: 'SEGURO', sortable: true, width: 100, align: 'center', search: false, sorttype: 'number',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'CUOTA_MANEJO', index: 'CUOTA_MANEJO', summaryType:'sum', sortable: true, width: 100, align: 'center', search: false, sorttype: 'number',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'VALOR', index: 'VALOR', summaryType:'sum', sortable: true, width: 100, align: 'center', search: false, sorttype: 'number',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'SALDO_FINAL', index: 'SALDO_FINAL', sortable: true, width: 100, align: 'center', search: false, sorttype: 'number',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                {name: 'VALOR_AVAL', index: 'VALOR_AVAL', sortable: true, width: 100, align: 'center',hidden:true, search: false, sorttype: 'number',
                formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: false,
            restoreAfterError: true,
            pager:'#page_tabla_documen_neg_aceptados',
            pgtext: null,
            pgbuttons: false,
                grouping:true,
                groupingView : {
   		groupField : ['COD_NEG'],
   		groupSummary : [true],
   		groupColumnShow : [false],
   		groupText : ['<b>{0}</b>'],
   		groupCollapse : false
                },
               
            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
             
            ajaxGridOptions: {                
                dataType: "json",
                type: "POST",  
                async:false,
                data: {
                    
                    opcion: 1,
                    negocio: $("#negocio").val(),
                    valor: $("#valor").val(),
                    cuota: $("#cuota").val(),
                    convenio: $("#convenio").val(),
                    fechacuota: $("#fechacuota").val(),
                    identificacion:$("#cod_cli").val()
                    
                }
            },
            loadError: function(xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
            }
        }).navGrid("#page_tabla_documen_neg_aceptados",{search:false,refresh:false,edit:false,add:false,del:false});
        

     }
    
}


function AbrirDocumentNegAceptados(){
    cargarDocumentNegAceptados();
     $('#div_documen_neg_aceptados').fadeIn("slow");
      $("#div_documen_neg_aceptados").dialog({
        modal: true,
        width: 'auto',
        height: 'auto',
        show: "scale",
        hide: "scale",
        resizable: false,
        position: [120,28],
        title:'LIQUIDACION NEGOCIOS',
        closable: true,
        closeOnEscape: true,
        buttons: {//crear bot�n de cerrar
            "Aceptar Liquidacion": function() {
               insert_document_neg_aceptados();
               
            },
            "Salir": function() {
               
            $(this).dialog("destroy");
            
            }
        }
    });       
   
    
}

function insert_document_neg_aceptados() {
              
         $.ajax({        
        type: 'POST',
        dataType: 'json',
        url: "./controller?estado=Negocios&accion=Fintra",
        data: {
            opcion: 2,
        negocio: $("#negocio").val(),
        valor: $("#valor").val(),
        cuota: $("#cuota").val(),
        convenio: $("#convenio").val(),
        fechacuota: $("#fechacuota").val(),
         identificacion:$("#cod_cli").val()
        },
        success: function () {            //mensajesDelSistema(data.respuesta);
           //cargarDocumentNegAceptados();
           
            mensajesDelSistema2("RELIQUIDACION EXITOSA", '230', '150', false);
            
            $(this).dialog('destroy');
           
        }, error: function () {
            mensajesDelSistema("ERROR DEL SISTEMA", '230', '150', false);
        }
    }); 
                        
                    
} 

function getDatosClienteLiquidacion(numero_solicitud){
    
    $.ajax({        
          type: 'POST',
          dataType: 'json',
          url: "./controller?estado=Negocios&accion=Fintra",
          async: false,
         data: {
              opcion: 3,
              numero_solicitud: numero_solicitud
              },
         success: function (data) { 
               carcularFecha(data.respuesta); 
         },
         error: function (xhr, status, error) {
             console.log("error");
         }
    }); 
}

function replaceAll(text, busca, reemplaza) {
    while (text.toString().indexOf(busca) !== - 1)
        text = text.toString().replace(busca, reemplaza);
    return text;
}


