/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    listarTiposDocumento();
    maximizarventana();
    $('.mayuscula').change(function () {
        this.value = this.value.toUpperCase();
    });
    $('.mayuscula').css({
        'text-transform': 'uppercase'
    });
});

function listarTiposDocumento() {
    var grid_tbl_tipo_docs = jQuery("#tabla_tipo_documentos");
    if ($("#gview_tabla_tipo_documentos").length) {
        refrescarGridTipoDocs();
    } else {
        grid_tbl_tipo_docs.jqGrid({
            caption: "TIPO DE DOCUMENTOS",
            url: "./controlleropav?estado=Minutas&accion=Contratacion",
            datatype: "json",
            height: '290',
            width: '710',
            cellEdit: true,
            colNames: ['Id', 'Nombre', 'Descripcion', 'Estado', 'Activar/Inactivar'],
            colModel: [
                {name: 'id', index: 'id', width: 80, align: 'left', key: true, hidden: true},
                {name: 'nombre', index: 'razon_social', width: 230, align: 'left'},
                {name: 'descripcion', index: 'descripcion', width: 320, align: 'left'},
                {name: 'reg_status', index: 'reg_status', width: 90, align: 'center', hidden: true},
                {name: 'cambio', index: 'cambio', width: 90, align: 'center'}
            ],
            rowNum: 1000,
            rowTotal: 1000,
            pager: ('#page_tabla_tipo_documentos'),
            loadonce: true,
            rownumWidth: 30,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: false,
            rownumbers: true,
            pgtext: null,
            pgbuttons: false,
            //multiselect: true,
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
            },
            ajaxGridOptions: {
                type: "POST",
                data: {
                    opcion: 17,
                    mostrarTodos: 'S'
                }
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            },
            gridComplete: function (index) {
                var cant = jQuery("#tabla_tipo_documentos").jqGrid('getDataIDs');
                for (var i = 0; i < cant.length; i++) {
                    var cambioEstado = $("#tabla_tipo_documentos").getRowData(cant[i]).cambio;
                    var cl = cant[i];
                    be = "<div class='onoffswitch'><input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_" + i + "' style='width: 51px;' value='cambio' " + cambioEstado + " > <label onclick=\"CambiarEstadoTipoDoc('" + cl + "');\" class='onoffswitch-label' for='myonoffswitch_" + i + "'><span class='onoffswitch-inner' ></span><span class='onoffswitch-switch' style ='top: 12px;height: 16px;'></span> </label></div>";
                    jQuery("#tabla_tipo_documentos").jqGrid('setRowData', cant[i], {cambio: be});
                }
            },
            ondblClickRow: function (rowid, iRow, iCol, e) {
                var myGrid = jQuery("#tabla_tipo_documentos"), selRowIds = myGrid.jqGrid("getGridParam", "selrow"), filas;
                filas = myGrid.jqGrid("getLocalRow", selRowIds);
                var id = filas.id;
                var reg_status = filas.reg_status;

                if (reg_status === 'A') {
                    mensajesDelSistema("Para editar el registro debe encontrarse Activo", '230', '150', false);
                } else {
                    editarTipoDocumento(id);
                }

            }
        }).navGrid("#page_tabla_tipo_documentos", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        jQuery("#tabla_tipo_documentos").jqGrid("navButtonAdd", "#page_tabla_tipo_documentos", {
            caption: "Nuevo",
            onClickButton: function () {
                crearTipoDocumento();
            }
        });
    }

}

function refrescarGridTipoDocs() {
    jQuery("#tabla_tipo_documentos").setGridParam({
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        datatype: 'json',
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: 17
            }
        }
    });

    jQuery('#tabla_tipo_documentos').trigger("reloadGrid");
}

function crearTipoDocumento() {
    $('#div_tipo_documento').fadeIn('slow');
    $('#idTipoDoc').val('');
    $('#nombre').val('');
    $('#descripcion').val('');
    AbrirDivCrearTipoDocumento();
}

function AbrirDivCrearTipoDocumento() {
    $("#div_tipo_documento").dialog({
        width: 'auto',
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'CREAR TIPO DOCUMENTO',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () {
                guardarTipoDocumento();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_tipo_documento").parent().find(".ui-dialog-titlebar-close").hide();
}

function editarTipoDocumento(cl) {

    $('#div_tipo_documento').fadeIn("slow");
    var fila = jQuery("#tabla_tipo_documentos").getRowData(cl);
    var nombre = fila['nombre'];
    var descripcion = fila['descripcion'];

    $('#idTipoDoc').val(cl);
    $('#nombre').val(nombre);
    $('#descripcion').val(descripcion);
    AbrirDivEditarTipoDocumento();
}

function AbrirDivEditarTipoDocumento() {
    $("#div_tipo_documento").dialog({
        width: 'auto',
        height: 230,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title: 'ACTUALIZAR TIPO DOCUMENTO',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () {
                guardarTipoDocumento();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });

    $("#div_tipo_documento").parent().find(".ui-dialog-titlebar-close").hide();
}

function guardarTipoDocumento() {
    var nombre = $('#nombre').val();
    var descripcion = $('#descripcion').val();

    if (nombre !== '' && descripcion !== '') {
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function () {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "./controlleropav?estado=Minutas&accion=Contratacion",
                data: {
                    opcion: ($('#idTipoDoc').val() === '') ? 18 : 19,
                    id: $('#idTipoDoc').val(),
                    nombre: $('#nombre').val(),
                    descripcion: $('#descripcion').val()
                },
                success: function (json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            $("#dialogLoading").dialog('close');
                            mensajesDelSistema(json.error, '270', '165');
                            return;
                        }

                        if (json.respuesta === "OK") {
                            $("#dialogLoading").dialog('close');
                            refrescarGridTipoDocs();
                            $("#div_tipo_documento").dialog('close');
                        }

                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos no se pudo guardar el tipo de documento!!", '250', '150');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }, 500);
    } else {
        mensajesDelSistema("FALTAN CAMPOS POR LLENAR!!", '250', '150');
    }
}


function CambiarEstadoTipoDoc(rowid) {
    var grid_tabla = jQuery("#tabla_tipo_documentos");
    var id = grid_tabla.getRowData(rowid).id;
    $.ajax({
        type: "POST",
        datatype: 'json',
        url: "./controlleropav?estado=Minutas&accion=Contratacion",
        data: {
            opcion: 20,
            id: id
        },
        success: function (json) {
            if (!isEmptyJSON(json)) {

                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridTipoDocs();
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo cambiar estado del tipo de documento!!", '250', '150');
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function conMayusculas(field) {
    field.value = field.value.toUpperCase();
}

function loading(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogLoading").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
}


function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function () {
                $(this).dialog("close");
            }
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function maximizarventana() {
    window.moveTo(0, 0);
    window.resizeTo(screen.width, screen.height);
}
