/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
   
//    $("#entrar").hide();
//    $("#entrar1").hide();
    //geoLocalizacion();  
   
    $("#entrar").click(function () {   
    var usuario = $("#usuario").val();
        var pass=$('#clave').val();         

            if (usuario !== "" && pass !== "") {
                validarRenovacionClave();
            } else {

                $("#msj").html("Por favor debe llenar todos los campos.");

            }
    });

     $("#cambiar").click(function () {
        $('#wrapper').hide();
        $('#dialogo2').show();
        
    });
    
    $("#atras").click(function () {
        $('#wrapper').show();
        $('#dialogo2').hide();
      
    });

});

function validarpass(){
     var clave = $("#nclave").val();
        var clave1=$('#cnclave').val();
        
        if(clave!==clave1){
            $("#msj1").html("La contraseña es diferente");                        
            return false;
        }
        $("#msj1").html(""); 
      return true;
    
} 

function buscarCia(id) {
    var usuario = $("#"+id).val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Usuario&accion=Login",
        dataType: 'json',
        data: {
            opcion: 2,
            login:usuario            
        },
        success: function(json) {
            if(!isEmptyJSON(json)){
            $('#cia').empty();
            $('#cia1').empty();
            $("#msj").html("");
           // $('#perfil').append("<option value=''>Seleccione</option>");
            for (var i = 0; i < json.length; i++) {
                $("select[id^='cia']").append("<option value='" + json[i].id_perfil + ":"+json[i].base+"'>" + json[i].nombre + "</option>");
            } 
         //   $('#perfil').selectmenu("refresh");
            if(id === 'usuario') {
                buscarPerfil(id, 'cia','dstrct','proy');
            } else {
                buscarPerfil(id, 'cia1','dstrct1','proy1');
            }
            } else {
                $("#msj").html("El usuario no existe o no tiene una empresa asignada");
            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    }); // fin $.ajax
}

function buscarPerfil(id, cia,dstrct,proy) {
    var usuario = $("#"+id).val();
    var compania=$('#'+cia).val();
    
    if(compania !== '') {
        var aux = compania.split(':');
        var distrito = aux[0];
        var proyecto = aux[1];
        $('#'+dstrct).val(distrito);
        $('#'+proy).val(proyecto);
       
    }
    
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Usuario&accion=Login",
        dataType: 'json',
        data: {
            opcion: 1,
            login:usuario,
            distrito:distrito,
            proyecto:proyecto
        },
        success: function(json) {
            $('#perfil').empty();
            $('#perfil1').empty();
            if(!isEmptyJSON(json)){
           // $('#perfil').append("<option value=''>Seleccione</option>");
            for (var i = 0; i < json.length; i++) {
                $("select[id^='perfil']").append("<option value='" + json[i].id_perfil + "'>" + json[i].nombre + "</option>");
            } 
         //   $('#perfil').selectmenu("refresh");
            
            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    }); // fin $.ajax
}


function validarRenovacionClave() {
    var usuario = $("#usuario").val();
    var pass=$('#clave').val();
    var perfil=$('#perfil').val();
    var proyecto=$('#proy').val();
    var distrito=$('#dstrct').val();
    var cia=$("#cia").val();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=Usuario&accion=Validar",
        dataType: 'json',
        data: {
            opcion: 1,
            usuario:usuario,
            clave:pass,
            loginNuevo:"SI",
            dstrct:distrito,
            proy:proyecto,
            perfil:perfil,
            cia:cia
            
        },
        success: function(json) {
            if(!isEmptyJSON(json)){
                
                if(json.respuesta === "SI"){
                    $('#wrapper').hide();
                    $('#dialogo2').show();
                    $('#usuario1').val(usuario);
                    $('#dstrct1').val(json.dstrct);
                    $('#proy1').val(json.proy);
                    $('#cia1').val(json.cia);
                    document.getElementById('titulo1').innerHTML = $('#cia option:selected').text();
                }else if (json.respuesta === "DUO") {
                    $("#msj").html("Existe una sesion activa. Cerrar sesión para ingresar. ");
                }else{
                    
                    $( "#formulario" ).submit();                    
                }
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    }); // fin $.ajax
}


function divCambioClave( width, height) {


    $("#dialogo2").dialog({
        width: "auto",
        height: "auto",
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();


}


function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function geoLocalizacion(){
    var content = '';
    if (navigator.geolocation)
    {
        console.log("content");
        navigator.geolocation.getCurrentPosition(function (objPosition)
        {
//            $("#entrar").show();
//            $("#entrar1").show();            
            document.getElementById("lat_x").value=objPosition.coords.latitude;
            document.getElementById("lon_y").value=objPosition.coords.longitude;        
            
        }, function (objPositionError)
        {
            console.log("ssdsdsd");
            switch (objPositionError.code)
            {
                case objPositionError.PERMISSION_DENIED:
                    content = "Para poder iniciar session fintra solicita tu ubicacion";                 
                    $("#msj").html(content);                   
                    break;
                case objPositionError.POSITION_UNAVAILABLE:
                    content = "No se ha podido acceder a la información de su posición.";
                    $("#msj").html(content);                 
                    break;
                case objPositionError.TIMEOUT:
                    content = "El servicio ha tardado demasiado tiempo en responder.";
                    $("#msj").html(content);                  
                    break;
                default:
                    content = "Error desconocido.";
                     $("#msj").html(content);
                   break;
            }
        }, {
            maximumAge: 75000,
            timeout: 10000
        });
    }
    else
    {
//        $("#entrar").show();
//        $("#entrar1").show();
        content = "Su navegador no soporta la API de geolocalización.";       
    }
       
}
