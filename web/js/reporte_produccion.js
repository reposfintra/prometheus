let filterFields = {
    opcion: 3
};

$(document).ready(function () {
    $("#fecha_inicio").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    $("#fecha_fin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
        maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
    });

    var myDate = new Date();
    $("#fecha_inicio").datepicker("setDate", myDate);
    $("#fecha_fin").datepicker("setDate", myDate);
    $('#ui-datepicker-div').css('clip', 'auto');

    buscarTransportadoras();
    $("#buscar_actualizar").click(function () {
        buscarTitulosGrid();
    });
});

function buscarTransportadoras() {

    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        data: {
            opcion: 1
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {

                try {
                    $('#transportadora').empty();
//                    $('#transportadora').append("<option value=''>Seleccione</option>");

                    for (var key in json) {
                        $('#transportadora').append('<option value=' + json[key].id + '>' + json[key].razon_social + '</option>');
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                  mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}

function buscarTitulosGrid() {


    $.ajax({
        type: 'POST',
        url: "./controller?estado=Administracion&accion=Logistica",
        dataType: 'json',
        data: {
            opcion: 2
        },
        success: function (json) {

            if (!isEmptyJSON(json)) {
                var arr = [];
                var ColModel1 = [];
                
                for (var key in json) {
                    arr.push(json[key].nombre);
                    if (json[key].tipo === 'numeric') {
                        ColModel1.push({name: json[key].nombre, index: json[key].nombre, align: 'right', width: 100, search: false, formatter: 'currency', formatoptions: {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: ""}});
                    } else if(json[key].tipo === 'text') {
                        ColModel1.push({name: json[key].nombre, index: json[key].nombre, align: 'left', width: 160, search: true});
                    }else if(json[key].tipo === 'int4'){
                        ColModel1.push({name: json[key].nombre, index: json[key].nombre, align: 'center', width: 30, search: false});                        
                    }else if(json[key].tipo === 'timestamp'){
                         ColModel1.push({name: json[key].nombre, index: json[key].nombre, align: 'left', width: 122, search: false});  
                    }
                    
                }   
                $("#columnas").val(arr.length);
                crearJqGrid(arr, ColModel1);
     
            } else {
                  mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });

}

function crearJqGrid(arrNames, arrColModel) {
    getFilterFields();
    let grid_listar_anticipos = jQuery("#tbl_reporte_produccion");
    
    if ($("#gview_tbl_reporte_produccion").length) {
        reloadGridListaAnticipos(grid_listar_anticipos);
    } else {
        grid_listar_anticipos.jqGrid({
            caption: "Lista de negocios",
            url: "./controller?estado=Administracion&accion=Logistica",
            mtype: "POST",
            datatype: "json",
            height: '650',
            width: '1850',
            colNames: arrNames,
            colModel: arrColModel,
            rowNum: 10000,
            rowTotal: 10000000,
            pager: '#page_reporte_produccion',
            loadonce: true,
            rownumWidth: 40,
            rownumbers: true,
            gridview: true,
            viewrecords: true,
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            postData: filterFields,
            loadComplete: function () {
                $(this).jqGrid("footerData", "set", {
                    unit: "Total:",
                    valor_manifiesto: $(this).jqGrid("getCol", "valor_manifiesto", false, "sum"),
                    valor_anticipo: $(this).jqGrid("getCol", "valor_anticipo", false, "sum"),
                    porcentanje_dscto_anttransferencia_1: $(this).jqGrid("getCol", "porcentanje_dscto_anttransferencia_1", false, "sum"),
                    valor_dscto_1: $(this).jqGrid("getCol", "valor_dscto_1", false, "sum"),
                    porcentanje_dscto_descuentoaterceros_2: $(this).jqGrid("getCol", "porcentanje_dscto_descuentoaterceros_2", false, "sum"),
                    valor_dscto_2: $(this).jqGrid("getCol", "valor_dscto_2", false, "sum"),
                    total_dscto: $(this).jqGrid("getCol", "total_dscto", false, "sum"),
                    valor_anticipo_con_descuento: $(this).jqGrid("getCol", "valor_anticipo_con_descuento", false, "sum"),
                    valor_comision: $(this).jqGrid("getCol", "valor_comision", false, "sum"),
                    valor_consignado: $(this).jqGrid("getCol", "valor_consignado", false, "sum"),
                    valor_egreso: $(this).jqGrid("getCol", "valor_egreso", false, "sum")
                });
            },
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 200);
            }
        }).navGrid("#page_reporte_produccion", {add: false, edit: false, del: false, search: false, refresh: false}, {});
        
        grid_listar_anticipos.jqGrid("filterToolbar",
                {
                    autosearch: true,
                    searchOnEnter: true,
                    defaultSearch: "cn",
                    stringResult: true,
                    ignoreCase: true,
                    multipleSearch: true
                });

        grid_listar_anticipos.navButtonAdd('#page_reporte_produccion', {
            caption: "Exportar Excel",
            onClickButton: function (e) {
                exportData($("#columnas").val(), "xls");
            }
        });

    }
}


function reloadGridListaAnticipos(grid_listar_negocios) {
    grid_listar_negocios.setGridParam({
        datatype: 'json',
        url: "./controller?estado=Administracion&accion=Logistica",
        postData: filterFields
    }).trigger("reloadGrid");
}

function exportData(cols, file) {
    var columns = cols.toString().split(",");
    var columnNms = $("#tbl_reporte_produccion").jqGrid('getGridParam', 'colNames');
    var theads = "";
    for (var cc = 1; cc < columnNms.length; cc++) {
        var isAdd = true;
        for (var p = 0; p < columns.length; p++) {
            if (cc === columns[p]) {
                isAdd = false;
                break;
            }
        }
        if (isAdd) {
            theads = theads + "<th>" + columnNms[cc] + "</th>"
        }
    }
    theads = "<tr>" + theads + "</tr>";
    var mya = new Array();
    mya = jQuery("#tbl_reporte_produccion").getDataIDs();  // Get All IDs
    var data = jQuery("#tbl_reporte_produccion").getRowData(mya[0]);     // Get First row to get the labels
    //alert(data['id']);
    var colNames = new Array();
    var ii = 0;
    for (var i in data) {
        colNames[ii++] = i;
    }

    var pageHead = "Reporte Produccion";
    var html = "<html>\n\
                <head>\n\
                <style script='css/text'>\n\
                table.tableList_1 th{\n\
                                    text-align:center; \n\
                                    vertical-align: middle; \n\
                                    padding:1px; \n\
                                    background-color:blue;\n\
                }\n\
                \n\
                table.tableList_1 td {\n\
                                    text-align: left; \n\
                                    vertical-align: top; \n\
                                    padding:1px;\n\
                }\n\
                </style>\n\
                </head>\n\
                <body>\n\
                <div class='pageHead_1'>" + pageHead + "</div>\n\
                <table border='" + (file == 'pdf' ? '0' : '1') + "' class='tableList_1 t_space' cellspacing='3' cellpadding='0'>" + theads;
 //   alert(theads);
    for (i = 0; i < mya.length; i++)
    {
        html = html + "<tr>";
        data = jQuery("#tbl_reporte_produccion").getRowData(mya[i]); // get each row
        for (j = 0; j < colNames.length; j++) {
            var isjAdd = true;
            for (var pj = 0; pj < columns.length; pj++) {
                if (j === columns[pj]) {
                    isjAdd = false;
                    break;
                }
            }
            if (isjAdd) {
                html = html + "<td>" + data[colNames[j].replace(/(<br>)*/g, '')] + "</td>"; // output each column as tab delimited
            }
        }
        html = html + "</tr>";  // output each row with end of line

    }
    
    html = html + "</table></body></html>";  // end of line at the end
    
   // alert(html+"saas");
    document.form_expr.pdfBuffer.value = html;
    document.form_expr.fileType.value = file;
    document.form_expr.method = 'POST';
    document.form_expr.action = './controller?estado=Administracion&accion=Logistica&opcion=4';//./controller?estado=Reestructuracion&accion=Negocios&opcion=14';  // send it to server which will open this contents in excel file
    document.form_expr.submit();
}

function mensajesDelSistema(msj, width, height) {

    $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    $("#dialogo").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: true,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n de cerrar
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });

}

function getFilterFields() {
    let checkboxes = document.getElementsByClassName("selection");
    for (let c of checkboxes) {
        let parent = c.parentElement;
        let cell = parent.nextSibling.nextSibling.nextSibling.nextSibling;
        let element = cell.firstElementChild;
        let finalDateElement;

        if (c.checked) {
            filterFields[element.id] = element.value;

            if (element.id === "fecha_inicio") {
                finalDateElement = document.getElementById("fecha_fin");
                filterFields[finalDateElement.id] = finalDateElement.value;
            }
        } else if (!c.checked && filterFields.hasOwnProperty(element.id)){
            delete filterFields[element.id];
            if (element.id === "fecha_inicio") {
                finalDateElement = document.getElementById("fecha_fin");
                delete filterFields[finalDateElement.id];
            }
        }
    }
    console.log(filterFields);
}
