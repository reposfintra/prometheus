/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*$(document).ready(function() {
     alert('entro');
 });*/


function initTipoActores(){
    cargarTipoActores();
}

function initActores(){
    cargarActores();
    //Cargamos tipos de actores
    cargarCboTipoActores();
    //cargamos tipos de identificacion
    cargarTiposIdentificacion();
    //cargar paises.
    cargarPaises();
    
    $('.solo-numero').keyup(function() {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    
    $('input[name="estado"]:radio').change(
        function() {
            if($("input[name=estado]:checked").val()==="A"){
                DeshabilitarControles(false);
            }else{
                DeshabilitarControles(true);
            }
    });          

    
    $("#pais").change(function() {
        var op = $(this).find("option:selected").val();
        cargarDepartamentos(op, "departamento");
    });
    
    $("#departamento").change(function() {
        var op = $(this).find("option:selected").val();
        cargarCiudad(op, "ciudad");
    });
}

function initEquiv_VariablesConf(){
    cargarEquivalencias();
}

function cargarTipoActores(){
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=35';
    if ($("#gview_tabla_tipo_actores").length) {
         refrescarGridTipoActores();
     }else {
        jQuery("#tabla_tipo_actores").jqGrid({
            caption: 'Tipo Actores',
            url: url,
            datatype: 'json',
            height: 250,
            width: 380,
            colNames: ['Id', 'Descripcion', 'Estado', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', sortable: true, align: 'center', width: '100px', key:true},           
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'left', width: '700px'},
                {name: 'reg_status', index: 'reg_status', hidden:true, sortable: true, align: 'center', width: '90px'},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '200px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            pager:'#page_tabla_tipo_actores',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {
                    var ids = jQuery("#tabla_tipo_actores").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        var estado = jQuery("#tabla_tipo_actores").getRowData(cl).reg_status;
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarTipoActores('" + cl + "');\">";
                        if (estado==='A'){
                            $("#tabla_tipo_actores").jqGrid('setRowData',ids[i],false, {weightfont:'bold',background:'#F6CECE'});          
                            ac = "<img src='/fintra/images/botones/iconos/check-blue.png' align='absbottom'  name='activar' id='activar' width='15' height='15' title ='Activar'  onclick=\"mensajeConfirmAction('Esta seguro de activar el tipo seleccionado?','250','150',activarTipoActor,'" + cl + "');\">";
                        }else{
                            ac = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el tipo seleccionado?','250','150',anularTipoActor,'" + cl + "');\">";
                        }                      
                        jQuery("#tabla_tipo_actores").jqGrid('setRowData', ids[i], {actions: ed+'  '+ac});
                       
                    }
                },
            loadError: function(xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_tipo_actores",{search:false,refresh:false,edit:false,add:false,del:false});
        jQuery("#tabla_tipo_actores").jqGrid("navButtonAdd", "#page_tabla_tipo_actores", {
            caption: "Nuevo", 
            title: "Agregar nuevo tipo",           
            onClickButton: function() {
               AbrirDivCrearTipoActores(); 
            }
        });
     }
}


function refrescarGridTipoActores(){    
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=35';
    jQuery("#tabla_tipo_actores").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#tabla_tipo_actores').trigger("reloadGrid");
}

function AbrirDivCrearTipoActores(){
    $('#tipo').val('');
    $('#div_tipoActores').fadeIn('slow');
    $("#div_tipoActores").dialog({
        width: 450,
        height: 175,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR TIPO',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
              guardarTipoActor();
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarTipoActor(){    
    var tipo = $('#tipo').val();   
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if(tipo!==''){
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 36,
                    tipo: tipo
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {
                            refrescarGridTipoActores();                           
                            mensajesDelSistema("Se cre� el nuevo tipo exitosamente", '250', '150', true); 
                            $('#tipo').val('');                         
                        }
                        
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear el nuevo tipo!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
    }else{
       mensajesDelSistema("Debe ingresar el tipo", '250', '150');
    }
}

function editarTipoActores(cl){
    
    $('#div_tipoActores').fadeIn("slow");
    var fila = jQuery("#tabla_tipo_actores").getRowData(cl);
    var descripcion = fila['descripcion'];
    var estado = fila['reg_status'];
     if (estado==="A"){
        $('#tipo').attr({readonly: true});       
    }else{
        $('#tipo').attr({readonly: false});        
    }
    $('#idTipo').val(cl);
    $('#tipo').val(descripcion);   
    AbrirDivEditarTipoActores();
    
}

function AbrirDivEditarTipoActores(){
    $("#div_tipoActores").dialog({
        width: 450,
        height: 175,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'EDITAR TIPO',
        closeOnEscape: false,
        buttons: {   
            "Actualizar": function(){
               actualizarTipoActor();  
            },
            "Salir": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarTipoActor(){  
    var idTipo = $('#idTipo').val();
    var tipo = $('#tipo').val();   
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if(tipo!==''){
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 37,
                    idTipo:idTipo,
                    tipo: tipo
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {
                            refrescarGridTipoActores();                           
                            mensajesDelSistema("Se actualiz� el tipo exitosamente", '250', '150', true);   
                            $("#div_tipoActores").dialog('close');
                        }
                        
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo actualizar el tipo!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
    }else{
       mensajesDelSistema("Debe ingresar el tipo", '250', '150');
    }
}

function anularTipoActor(cl){
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 38,            
            idTipo: cl,
            estadoTipoActor: "A"
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridTipoActores();                  
                    mensajesDelSistema("Se anul� el tipo", '250', '150', true);                                   
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo anular el tipo!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function activarTipoActor(cl){
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {
            opcion: 38,            
            idTipo: cl,
            estadoTipoActor: ""
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '150');
                    return;
                }

                if (json.respuesta === "OK") {
                    refrescarGridTipoActores();                  
                    mensajesDelSistema("Se activ� el tipo", '250', '150', true);                               
                }

            } else {
                mensajesDelSistema("Lo sentimos no se pudo activar el tipo!!", '250', '150');
            }                
        }, error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarActores(){
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=39';
    if ($("#gview_tabla_actores").length) {
         refrescarGridActores();
     }else {
        jQuery("#tabla_actores").jqGrid({
            caption: 'Actores',
            url: url,
            datatype: 'json',
            height: 250,
            width: 650,
            colNames: ['Id', 'IdTipoActor', 'Tipo Actor', 'Tipo Documento', 'Documento', 'Nombre', 'idCiudad', 'idDpto', 'idPais', 'Direccion', 'Telefono', 'Extension', 'Celular', 'Email', 'Tarjeta Profesional', 'Lugar exp. cedula', 'Estado', 'Acciones'],
            colModel: [
                {name: 'id', index: 'id', hidden:true, sortable: true, align: 'center', width: '100px', key:true}, 
                {name: 'id_tipo_actor', index: 'id_tipo_actor', hidden:true, sortable: true, align: 'center', width: '100px'}, 
                {name: 'tipo_actor', index: 'tipo_actor',  sortable: true, align: 'left', width: '150px'}, 
                {name: 'tipo_documento', index: 'tipo_documento', sortable: true, align: 'center', width: '100px'},
                {name: 'documento', index: 'documento', sortable: true, align: 'center', width: '120px'},
                {name: 'nombre', index: 'nombre', sortable: true, align: 'left', width: '280px'},
                {name: 'codciu', index: 'codciu', hidden:true, sortable: true, align: 'center', width: '100px'},
                {name: 'coddpto', index: 'coddpto', hidden:true, sortable: true, align: 'center', width: '100px'},
                {name: 'codpais', index: 'codpais', hidden:true, sortable: true, align: 'center', width: '100px'},
                {name: 'direccion', index: 'direccion', hidden:true, sortable: true, align: 'center', width: '100px'},
                {name: 'telefono', index: 'telefono', hidden:true, sortable: true, align: 'center', width: '100px'},
                {name: 'tel_extension', index: 'tel_extension', hidden:true, sortable: true, align: 'center', width: '100px'},
                {name: 'celular', index: 'celular', hidden:true, sortable: true, align: 'center', width: '100px'}, 
                {name: 'email', index: 'email', hidden:true, sortable: true, align: 'center', width: '170px'},
                {name: 'tarjeta_profesional', index: 'tarjeta_profesional', hidden:true, sortable: true, align: 'center', width: '100px'},
                {name: 'doc_lugar_exped', index: 'doc_lugar_exped', hidden:true, sortable: true, align: 'center', width: '100px'},                  
                {name: 'reg_status', index: 'reg_status', hidden:true, sortable: true, align: 'center', width: '90px'},
                {name: 'actions', index: 'actions', resizable:false, align: 'center', width: '100px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            pager:'#page_tabla_actores',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },
            gridComplete: function() {
                    var ids = jQuery("#tabla_actores").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        var estado = jQuery("#tabla_actores").getRowData(cl).reg_status;
                        ed = "<img src='/fintra/images/edit.ico' align='absbottom'  name='editar' id='editar' width='15' height='15' title ='Editar'  onclick=\"editarActores('" + cl + "');\">";
                        if (estado==='A'){
                            $("#tabla_actores").jqGrid('setRowData',ids[i],false, {weightfont:'bold',background:'#F6CECE'});          
                            ac = "<img src='/fintra/images/botones/iconos/check-blue.png' align='absbottom'  name='activar' id='activar' width='15' height='15' title ='Activar'  onclick=\"mensajeConfirmAction('Esta seguro de activar el estado seleccionado?','250','150',activarEstadoCartera,'" + cl + "');\">";
                        }else{
                            ac = "<img src='/fintra/images/botones/iconos/trash-icon-blue.png' align='absbottom'  name='anular' id='anular' width='15' height='15' title ='Anular'  onclick=\"mensajeConfirmAction('Esta seguro de anular el estado seleccionado?','250','150',anularEstadoCartera,'" + cl + "');\">";
                        }                      
                        jQuery("#tabla_actores").jqGrid('setRowData', ids[i], {actions: ed});
                       
                    }
                },
            loadError: function(xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_actores",{search:false,refresh:false,edit:false,add:false,del:false});
        jQuery("#tabla_actores").jqGrid("navButtonAdd", "#page_tabla_actores", {
            caption: "Nuevo", 
            title: "Agregar nuevo Actor",           
            onClickButton: function() {
               crearActores(); 
            }
        });
     }
}


function refrescarGridActores(){    
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=39';
    jQuery("#tabla_actores").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#tabla_actores').trigger("reloadGrid");
}

function crearActores(){  
    $('#div_actores').fadeIn('slow');
    AbrirDivCrearActores();
}

function AbrirDivCrearActores(){
    $('#tipo').val('');
    $('#div_actores').fadeIn('slow');
    $("#div_actores").dialog({
        width: 750,
        height: 380,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'CREAR ACTOR',
        closeOnEscape: false,
        buttons: {
            "Adicionar": function () { 
              guardarActor();
            },
            "Salir": function () {
                DeshabilitarControles(false);
                resetearValores();             
                $(this).dialog("destroy");
            }
        }
    });
}

function guardarActor(){  
    var tipoActor = $('#tipo').val();
    var tipoDoc = $('#tipo_doc').val();
    var documento = $('#documento').val();
    var lugarExp = $('#doc_lugar_exp').val();
    var nombre = $('#nombre').val();
    var direccion = $('#direccion').val();
    var email = $('#email').val();
    var celular = $('#celular').val();
    var telefono = $('#telefono').val();
    var extension = $('#ext').val();
    var pais = $('#pais').val();
    var departamento = $('#departamento').val();
    var ciudad = $('#ciudad').val();
    var tarj_prof = $('#tarjeta_prof').val();
    var estado = ($("input[name=estado]:checked").val() === 'A') ? "" : "A";
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if(tipoActor!=='' && tipoDoc!=='' && documento!=='' && nombre!=='' && ciudad!==''){
        if(telefono!=='' && !validarTelefono(telefono))
        {
            mensajesDelSistema("Por favor, ingrese un numero de telefono valido", '250', '150'); 
        }else if(!validarEmail(email)){
            mensajesDelSistema("El email ingresado es incorrecto. Por favor, Verifique", '250', '150');       
        }else{
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 42,                
                    tipo_actor: tipoActor,
                    tipo_doc: tipoDoc,
                    documento: documento,
                    nombre: nombre,
                    lugar_expedicion: lugarExp,
                    direccion: direccion,                   
                    email: email,
                    celular: celular,
                    telefono: telefono,
                    ext: extension,
                    pais: pais,
                    departamento: departamento,
                    ciudad: ciudad,
                    tarjeta_prof: tarj_prof,
                    reg_status: estado
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {
                        
                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');                          
                            return;
                        }
                        
                        if (json.respuesta === "OK") {
                            refrescarGridActores();   
                            resetearValores();
                            mensajesDelSistema("Se cre� el actor", '250', '150', true);                          
                        }
                        
                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo crear el actor!!", '250', '150');
                    }
                    
                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                          "Message: " + xhr.statusText + "\n" +
                          "Response: " + xhr.responseText + "\n" + thrownError);
                }
            }); 
        }
    }else{
       mensajesDelSistema("Hay campos obligatorios que faltan por llenarse", '250', '150');
    }
}

function editarActores(cl){
    
    $('#div_actores').fadeIn("slow");
    var fila = jQuery("#tabla_actores").getRowData(cl);   
    var tipoActor = fila['id_tipo_actor'];
    var tipoDoc = fila['tipo_documento'];
    var documento = fila['documento'];
    var nombre = fila['nombre'];
    var pais = fila['codpais'];
    var dpto = fila['coddpto'];
    var ciudad = fila['codciu'];
    var direccion = fila['direccion'];
    var email = fila['email'];
    var celular = fila['celular'];
    var telefono = fila['telefono'];
    var extension = fila['tel_extension'];
    var lugarExp = fila['doc_lugar_exped'];
    var tarj_prof = fila['tarjeta_profesional']; 
    var estado = (fila['reg_status']==='A') ? "I":"A";  
    if (estado==="A"){
        DeshabilitarControles(false);
    }else{
        DeshabilitarControles(true);
    }
   
    $('#idActor').val(cl);
    $('#tipo').val(tipoActor);
    $('#tipo_doc').val(tipoDoc);
    $('#doc_lugar_exp').val(lugarExp);
    $('#documento').val(documento);
    $('#nombre').val(nombre);
    $('#direccion').val(direccion);
    $('#email').val(email);
    $('#celular').val(celular);
    $('#telefono').val(telefono);
    $('#ext').val(extension);
    $('#pais').val(pais);
    cargarDepartamentos(pais, "departamento");
    $('#departamento').val(dpto);
    cargarCiudad(dpto,"ciudad");
    $('#ciudad').val(ciudad);  
    $('#tarjeta_prof').val(tarj_prof);
    $('input[name=estado]').val([estado]); 
    AbrirDivEditarActor();
}

function AbrirDivEditarActor(){
      $("#div_actores").dialog({
        width: 750,
        height: 380,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        title:'ACTUALIZAR ACTOR',
        closeOnEscape: false,
        buttons: {
            "Actualizar": function () { 
              actualizarActor();
            },
            "Salir": function () {
                DeshabilitarControles(false);
                resetearValores();
                $(this).dialog("destroy");
            }
        }
    });
}

function actualizarActor(){
    var id = $('#idActor').val(); 
    var tipoActor = $('#tipo').val();
    var tipoDoc = $('#tipo_doc').val();
    var documento = $('#documento').val();
    var lugarExp = $('#doc_lugar_exp').val();
    var nombre = $('#nombre').val();
    var direccion = $('#direccion').val();
    var email = $('#email').val();
    var celular = $('#celular').val();
    var telefono = $('#telefono').val();
    var extension = $('#ext').val();
    var pais = $('#pais').val();
    var departamento = $('#departamento').val();
    var ciudad = $('#ciudad').val();
    var tarj_prof = $('#tarjeta_prof').val();
    var estado = ($("input[name=estado]:checked").val() === 'A') ? "" : "A";
    var url = './controller?estado=Proceso&accion=Ejecutivo';
    if(tipoActor!=='' && tipoDoc!=='' && documento!=='' && nombre!=='' && ciudad!==''){ 
        if(telefono!=='' && !validarTelefono(telefono))
        {
            mensajesDelSistema("Por favor, ingrese un numero de telefono valido", '250', '150'); 
        }else if(!validarEmail(email)){
            mensajesDelSistema("El email ingresado es incorrecto. Por favor, Verifique", '250', '150');       
        }else{
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    opcion: 43,
                    id: id,
                    tipo_actor: tipoActor,
                    tipo_doc: tipoDoc,
                    documento: documento,
                    nombre: nombre,
                    lugar_expedicion: lugarExp,
                    direccion: direccion,                   
                    email: email,
                    celular: celular,
                    telefono: telefono,
                    ext: extension,
                    pais: pais,
                    departamento: departamento,
                    ciudad: ciudad,
                    tarjeta_prof: tarj_prof,
                    reg_status: estado
                },
                success: function(json) {
                    if (!isEmptyJSON(json)) {

                        if (json.error) {
                            mensajesDelSistema(json.error, '250', '150');
                            return;
                        }

                        if (json.respuesta === "OK") {
                            refrescarGridActores();                  
                            mensajesDelSistema("Se actualiz� el actor", '250', '150', true); 
                        }

                    } else {
                        mensajesDelSistema("Lo sentimos no se pudo actualizar el actor!!", '250', '150');
                    }

                }, error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }
    }else{
         mensajesDelSistema("Hay campos obligatorios que faltan por llenarse!!", '250', '150');      
    }    
}


function cargarEquivalencias(){
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=44';
    if ($("#gview_tabla_equiv_variables").length) {
         refrescarGridEquivalencias();
     }else {
        jQuery("#tabla_equiv_variables").jqGrid({
            caption: 'Tabla de Equivalencias',
            url: url,
            datatype: 'json',
            height: 350,
            width: 550,
            colNames: ['Id', 'C�d. Parametro', 'Descripcion'],
            colModel: [
                {name: 'id', index: 'id', hidden:true, sortable: true, align: 'center', width: '100px', key:true},      
                {name: 'codparam', index: 'codparam', sortable: true, align: 'center', width: '190px'},
                {name: 'descripcion', index: 'descripcion', sortable: true, align: 'left', width: '550px'}
            ],
            rowNum: 1000,
            rowTotal: 50000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            pager:'#page_tabla_equiv_variables',
            pgtext: null,
            pgbuttons: false,
            jsonReader: {
                root: 'rows',
                repeatitems: false,
                id: '0'
            },
            ajaxGridOptions: {                
               async:false
            },           
            loadError: function(xhr, status, error) {
                alert(error);
            }
        }).navGrid("#page_tabla_equiv_variables",{search:false,refresh:false,edit:false,add:false,del:false});     
     }
}


function refrescarGridEquivalencias(){    
    var url = './controller?estado=Proceso&accion=Ejecutivo&opcion=44';
    jQuery("#tabla_equiv_variables").setGridParam({
        datatype: 'json',
        url: url
    });
    
    jQuery('#tabla_equiv_variables').trigger("reloadGrid");
}

function cargarCboTipoActores() {
  
    $.ajax({
        type: 'POST',
        async:false,
        url: "./controller?estado=Proceso&accion=Ejecutivo",
        dataType: 'json',
        data: {
            opcion: 40
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#tipo').append("<option value=''>Seleccione</option>");                 

                    for (var key in json) {
                        $('#tipo').append('<option value=' + key + '>' + json[key] + '</option>');                        
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarTiposIdentificacion() {
  
    $.ajax({
        type: 'POST',
        async:false,
        url: "./controller?estado=Proceso&accion=Ejecutivo",
        dataType: 'json',
        data: {
            opcion: 41
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#tipo_doc').append("<option value=''>Seleccione</option>");                 

                    for (var key in json) {
                        $('#tipo_doc').append('<option value=' + key + '>' + json[key] + '</option>');                        
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function cargarPaises() {
  
    $.ajax({
        type: 'POST',
        async:false,
        url: "./controller?estado=Archivo&accion=Asobancaria",
        dataType: 'json',
        data: {
            opcion: 12
        },
        success: function(json) {
            if (!isEmptyJSON(json)) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    $('#pais').append("<option value=''>Seleccione</option>");                 

                    for (var key in json) {
                        $('#pais').append('<option value=' + key + '>' + json[key] + '</option>');                        
                    }

                } catch (exception) {
                    mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                }

            } else {

                mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

            }

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}


function cargarDepartamentos(codigo, combo) {
  if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            type: 'POST',
            async:false,
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 13,
                cod_pais: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#'+combo).append("<option value=''>Seleccione</option>");                 

                        for (var key in json) {
                            $('#'+combo).append('<option value=' + key + '>' + json[key] + '</option>');                      
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
  }
}

function cargarCiudad(codigo, combo) {

    if (codigo !== '') {
        $('#' + combo).empty();
        $.ajax({
            async: false,
            type: 'POST',
            url: "./controller?estado=Archivo&accion=Asobancaria",
            dataType: 'json',
            data: {
                opcion: 14,
                cod_dpto: codigo
            },
            success: function(json) {
                if (!isEmptyJSON(json)) {
                    if (json.error) {
                        mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                        $('#' + combo).empty();
                        $('#' + combo).append("<option value=''>Seleccione</option>");

                        for (var key in json) {
                            $('#' + combo).append('<option value=' + key + '>' + json[key] + '</option>');
                        }

                    } catch (exception) {
                        mensajesDelSistema('error : ' + key + '>' + json[key][key], '250', '180');
                    }

                } else {

                    mensajesDelSistema("Lo sentimos no se encontraron resultados!!", '250', '150');

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    }
}

function validarTelefono(phone) {
    var filter = /^[0-9-()+]+$/;
    return filter.test(phone);
}

function validarEmail(email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( email );
}

function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}


function DeshabilitarControles(estado){
        $('#tipo').attr({disabled: estado});
        $('#tipo_doc').attr({disabled: estado});
        $('#documento').attr({readonly: estado});
        $('#doc_lugar_exp').attr({disabled: estado});       
        $('#nombre').attr({readonly: estado});
        $('#direccion').attr({readonly: estado});
        $('#email').attr({readonly: estado});
        $('#celular').attr({readonly: estado});
        $('#telefono').attr({readonly: estado});
        $('#ext').attr({readonly: estado});
        $('#tarjeta_prof').attr({readonly: estado});
        $('#pais').attr({disabled: estado});
        $('#departamento').attr({disabled: estado});
        $('#ciudad').attr({disabled: estado});    
}


function resetearValores(){   
    $('#tipo').val('');
    $('#tipo_doc').val('');
    $('#documento').val('');
    $('#doc_lugar_exp').val('');
    $('#nombre').val('');
    $('#direccion').val('');
    $('#email').val('');
    $('#celular').val('');
    $('#telefono').val('');
    $('#ext').val('');
    $('#tarjeta_prof').val('');
    $('#pais').val('');
    $('#departamento').empty();
    $('#ciudad').empty(); 
    $('input[name=estado]').val(['A']);
}


function mensajeConfirmAction(msj, width, height, okAction, id) {
  
    $("#msj").html("<span class='ui-icon ui-icon-help' style='float: left; margin: 0 7px 20px 0;'></span> " + msj );
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,        
        closeOnEscape: false,
        buttons: {
            "Si": function () {
                $(this).dialog("destroy");
                okAction(id);                
            },
            "No": function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function mensajesDelSistema(msj, width, height, swHideDialog) {   
    if (swHideDialog) {
        $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    } else {
        $("#msj").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
    }
    $("#dialogMsj").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false,
        buttons: {//crear bot�n cerrar
            "Aceptar": function() {
                $(this).dialog("destroy");             
            }
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}
