function validarCamposIndex( tipo ){
    if ( tipo == "login" ){                 
        if (login.usuario.value == "" || login.clave.value == ""){
            alert("No se puede procesar la información. Verifique que ingresó LOGIN y/o CLAVE.");
            login.usuario.focus();
            return (false);
        }
        if ( login.perfil.selectedIndex == -1 ){
            alert("No se puede procesar la información. Seleccione un Perfil.");
            login.perfil.focus();
            return (false);
        }
        if ( document.getElementById("enviar").value == "N" ){
            alert("Espere mientras se cargan los perfiles del usuario "+login.usuario.value);
            return (false);
        }
    }else if ( tipo == "renovarClave" ){
        var sw = 0;
        if (login.clave1.value == "" || login.nclave.value == "" || login.cnclave.value == ""){
            alert("No se puede procesar la información. Verifique que ingresó los campos requeridos.");
            login.clave1.focus();
            sw = 1;
            return (false);
        }
        if (sw == 0){
            if (login.nclave.value != login.cnclave.value ){
                alert("Confirmación de Nueva Clave erronea.");
                login.cnclave.focus();
                return (false);
            }
        }
    }
    return (true);
}
function getAjax(){
    var ajax = false;
    try {
        ajax = new ActiveXObject( "Msxml2.XMLHTTP" );
    } catch ( e ) {
        try {
            ajax = new ActiveXObject( "Microsoft.XMLHTTP" );
        } catch ( E ) {
           ajax = false;
        }
    }
    if ( !ajax && typeof XMLHttpRequest!='undefined' ) {
        ajax = new XMLHttpRequest();
    }
    return ajax;
}
function cargarPerfiles( capa, target, perfil ){    
	//alert("cargarPerfiles__"+capa+"__"+target+"__"+perfil);
    var ajax = getAjax();
    var placeContent = document.getElementById( capa );
	//alert("antes de open");
    ajax.open ( 'GET', target, true );
	//alert("despues de open");
    ajax.onreadystatechange = function() {
        if ( ajax.readyState == 1 ) {
            document.getElementById("imgworking").style.visibility = "visible";
            document.getElementById("working").innerText = "Cargando Perfiles...";
            document.getElementById("enviar").value = "N";
         }else if ( ajax.readyState == 4 ){
            document.getElementById("imgworking").style.visibility = "hidden";
            document.getElementById("working").innerText = "";
            document.getElementById("enviar").value = "S";
            if( ajax.status == 200 ){                
                placeContent.innerHTML = ajax.responseText;                
                if( perfil != "null" && perfil != "" ){
                    document.getElementById("perfil").value = perfil;
                }
                if( document.getElementById("cantPerfiles").value == "0" ){
                    document.getElementById("working").innerText = "No tiene perfiles el usuario "+document.getElementById("usuario").value;
                }
            }
         }
    }
    ajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    ajax.send( null );
}
function validarBuscar( capa, target, perfil ){
	//alert("validarBuscar");
    var usuario = document.getElementById("usuario").value;
    var existente = document.getElementById("userlogin").value;
    if( usuario != null && usuario != "" ){
        if(  usuario != existente ){
            cargarPerfiles( capa, target+"&login="+usuario, perfil );
        }
    }
}