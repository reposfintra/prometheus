<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>     
    <title>Anular  Peaje</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="js/validar.js"></script>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
	<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<%String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";%>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anular Peaje"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <form name="form3" method="post" action="<%=CONTROLLER%>?estado=Tiquete&accion=Search&num=1">
      <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
      <table width="650" border="2" align="center">
        <tr>
          <td>
			<table width="100%" border="0" align="center" class="tablaInferior">
				<tr>
   					<td  height="24"  class="subtitulo1"><p align="left">Listado Peajes</p></td>
            		<td width="404"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>						
            	</tr>
			</table>
			<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">  
            	<tr class="tblTitulo">
              		<td nowrap><strong>Codigo</strong></td>
              		<td nowrap><strong>Descripcion</strong></td>
              		<td width="121" nowrap><strong>Valor</strong></td>
              		<td width="102" nowrap><strong>Moneda</strong></td>
              		<td width="135" nowrap><strong>Distrito</strong></td>
            	</tr>
            	<%if(model.peajeService.existList(usuario.getDstrct())){
					List list = model.peajeService.getPeajes(usuario.getDstrct());
					int i=0;
					Iterator it=list.iterator();
					while (it.hasNext()){%>
            		<%	i++;
						Peajes p = (Peajes) it.next();
						String  tiket= p.getTiket_id();
						String desc=p.getDescripcion();
						float valor=p.getValor();
                   		String moneda= p.getMoneda();
                    	String distrito= p.getDstrct();					
				%>
            	<tr class="<%=(i % 2 == 0 )?"filaazul":"filagris"%>" style="cursor:hand" title="Modificar Peajes..." onClick="window.location='<%=CONTROLLER%>?estado=Peajes&accion=Search&num=2&tiket=<%=tiket%>'" onMouseOver='cambiarColorMouse(this)' >
              		<td width="78" nowrap class="bordereporte"><%=tiket%></td>
              		<td width="180" nowrap class="bordereporte"><%=desc%></td>
              		<td nowrap class="bordereporte"><%=valor%></td>
              		<td nowrap class="bordereporte"><%=moneda%></td>
              		<td nowrap class="bordereporte"><%=distrito%></td>
            	</tr>
            	<%}
	  		}%>
          	</table>
		</td>
       </tr>
     </table>
   </form>

    <form name="form2" method="post" action="<%=CONTROLLER%>?estado=Peajes&accion=Delete&cmd=show">
	  <%String tiket="";
  	String distrito="", moneda="", desc="";
	float valor=0;
	if(request.getAttribute("peajes")!=null){
     	Peajes p= (Peajes)request.getAttribute("peajes");
		tiket= p.getTiket_id();
		desc=p.getDescripcion();
		valor=p.getValor();
        moneda= p.getMoneda();
        distrito= p.getDstrct();
					
	}%>
      
	  <table width="388" border="2" align="center">
        <tr>
          <td width="376">
		  	<table width="100%" height="34" border="0" align="center" class="tablaInferior">
            	<tr>
              		<td  height="24"  class="subtitulo1"><p align="left">Analular Peaje</p></td>
            		<td width="404"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            	</tr>
            	<tr class="fila">
              		<td width="168" nowrap><strong>Codigo :</strong></td>
              		<td width="346" nowrap>&nbsp;<%=tiket%>
                  		<input name="tiket" type="hidden" id="tiket" value="<%=tiket%>" class="textreadonly">
              		</td>
            	</tr>
            	<tr class="fila">
              		<td width="168" nowrap><strong>Descripcion :</strong></td>
              		<td nowrap>&nbsp;<%=desc%></td>
            	</tr>
            	<tr class="fila">
              		<td nowrap><strong>Distrito :</strong></td>
              		<td nowrap>&nbsp;<%=distrito%></td>
            	</tr>
            	<tr class="fila">
              		<td nowrap><strong>Moneda :</strong></td>
              		<td nowrap>&nbsp;<%=moneda%>           
            	</tr>
            	<tr class="fila">
              		<td nowrap><strong>Valor :</strong></td>
              		<td nowrap>&nbsp;<%=valor%></td>
            	</tr>
          	</table>
		</td>
       </tr>
     </table>
	  <%if(!Mensaje.equals("")){%>
      <p>      
      <table border="2" align="center">
        <tr>
          <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
          </table></td>
        </tr>
      </table>
      <p></p>
      <p>
        <%}%>
      </p>
      <p>          
	  <%if(request.getAttribute("peajes")!=null){%>
      </p>
      <div align="center">
          <br>
          <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
	  <%}else{%>
			 <div align="center"><br>
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
	   <%}%>
   </form>
   </div>
</body>
</html>
