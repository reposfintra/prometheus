<!--
- Date  : 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, modifica las caracteristicas de un peaje
--%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Modificar Peaje</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="js/validar.js"></script>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
	<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<%String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";%>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Peaje"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <form name="form3" method="post" action="<%=CONTROLLER%>?estado=Tiquetes&accion=Search&num=1">
<%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>    
<table width="200" border="2" align="center">
  <tr>
    <td>
		<table width="100%" border="0" align="center" class="tablaInferior">
			<tr>
   				<td  height="24"  class="subtitulo1"><p align="left">Listado Peajes</p></td>
            	<td width="404"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>						
            </tr>
		</table>			
		<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">      		
      		<tr class="tblTitulo">
        		<td nowrap><strong>Codigo</strong></td>
        		<td nowrap><strong>Descripcion</strong></td>
        		<td width="121" nowrap><strong>Valor</strong></td>
        		<td width="102" nowrap><strong>Moneda</strong></td>
        		<td width="135" nowrap><strong>Distrito</strong></td>
      		</tr>
      		<%if(model.peajeService.existList(usuario.getDstrct())){
				List list = model.peajeService.getPeajes(usuario.getDstrct());
				int i=0;
				Iterator it=list.iterator();
					while (it.hasNext()){%>      					
					<%i++;
					  Peajes p = (Peajes) it.next();					
					  String  tiket= p.getTiket_id();
					  String desc=p.getDescripcion();
					  float valor=p.getValor();
                      String moneda= p.getMoneda();
                      String distrito= p.getDstrct();					
			%>
      		<tr class="<%=(i % 2 == 0 )?"filaazul":"filagris"%>" style="cursor:hand" title="Modificar Peajes..." onClick="window.location='<%=CONTROLLER%>?estado=Peajes&accion=Search&num=1&tiket=<%=tiket%>'" onMouseOver='cambiarColorMouse(this)'>
        		<td width="78" nowrap class="bordereporte"><%=tiket%></td>
        		<td width="180" nowrap class="bordereporte"><%=desc%></td>
        		<td nowrap class="bordereporte"><%=valor%></td>
        		<td nowrap class="bordereporte"><%=moneda%></td>
        		<td nowrap class="bordereporte"><%=distrito%></td>
      		</tr>
      	<%}
	  	}%>
    	</table>
		</td>
  	</tr>
	</table>
   </form>

    <form name="form2" id="form2" method="post" action="<%=CONTROLLER%>?estado=Peajes&accion=Update&cmd=show">
  	<%	String tiket="";
  		String distrito="", moneda="", desc="";
		float valor=0;
		if(request.getAttribute("peajes")!=null){
     		Peajes p= (Peajes)request.getAttribute("peajes");
			tiket= p.getTiket_id();
			desc=p.getDescripcion();
			valor=p.getValor();
        	moneda= p.getMoneda();
        	distrito= p.getDstrct();					
		}%>
 
    <table width="530" border="2" align="center">
      <tr>
        <td>
		<table width="100%" height="34" border="0" align="center" class="tablaInferior">
          <tr>
          	<td  height="24"  class="subtitulo1"><p align="left">Modificar Peaje</p></td>
            <td width="404"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
          <tr class="fila">
          	<td width="168" nowrap><strong>Codigo:</strong></td>
            <td width="346" nowrap> <%=tiket%>
             	<input name="tiket" type="hidden" id="tiket2" value="<%=tiket%>">
            </td>
          </tr>
          <tr class="fila">
            <td width="168" nowrap><strong>Descripcion:</strong></td>
            <td nowrap><input name="descripcion" type="text" id="descripcion2" value="<%=desc%>" class="textbox"></td>
          </tr>
          <tr class="fila">
            <td nowrap><strong>Distrito:</strong></td>
            <td nowrap>
				<select name="distrito" id="select2" class="textbox">
                	<option value="FINV" selected>FINV</option>
            	</select>
			</td>
          </tr>
          <tr class="fila">
          	<td nowrap><strong>Moneda:</strong></td>
          	<td nowrap>
		  		<select name="moneda" id="select3" class="textbox">
                	<%if(moneda.equals("PES")){%>
                		<option value="PES" selected>Pesos</option>
                	<%}
					else{
					%>
                		<option value="PES" >Pesos</option>
                	<%}%>
                	<%if(moneda.equals("DOL")){%>
                		<option value="DOL" selected>Dolar</option>
                	<%}else{%>
                		<option value="DOL">Dolar</option>
                	<%}%>
                	<%if(moneda.equals("BOL")){%>
                		<option value="BOL" selected>Bolivar</option>
                	<%}else{%>
                		<option value="BOL">Bolivar</option>
                	<%}%>
           	  </select>
       	  </tr>
          	<tr class="fila">
            	<td nowrap><strong>Valor:</strong></td>
           	  <td nowrap>
					<input name="valor" type="text" id="valor2" value="<%=valor%>" class="textbox">
				<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">				</td>
          	</tr>
       	  </table>		</td>
      </tr>
    </table>
    
	<%if(!Mensaje.equals("")){%>
    <p>
    <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
    <p></p>
    <p>
      <%}%>
    </p>
    <p></p>
    <%if(request.getAttribute("peajes")!=null){%>
    <div align="center"><br>
        <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="if( ValidarFormTiquetes(form2) ){form2.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
  <%}else{%>
  		 <div align="center"><br>
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
   <%}%>
    </form>
	</div>
</body>
</html>
