<!--
- Date  : 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para 
--              ingresar un peaje
--%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
          
<html>
<head>
<title>Ingresar Peajes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src='<%=BASEURL%>/js/validar.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<%String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";%>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Peaje"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=Peajes&accion=Insert&cmd=show">

  	<table width="439" border="2" align="center">
      <tr>
        <td><table width="99%" height="34" border="0" align="center" class="tablaInferior">
          <tr>
            <td width="157" height="24"  class="subtitulo1"><p align="left">Ingresar Peaje</p></td>
            <td width="404"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
          <tr class="fila">
            <td width="157"><strong>Codigo :</strong></td>
            <td width="404"><input name="tiket" type="text" id="tiket" maxlength="3" class="textbox"> 
			<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> 

            </td>
          </tr>
          <tr class="fila">
            <td nowrap><strong>Distrito :</strong></td>
            <td nowrap><select name="distrito" id="distrito" class="textbox">
                <option value="FINV" selected>FINV</option>
              </select>
            </td>
          </tr>
          <tr class="fila">
            <td nowrap><strong>Descripcion :</strong></td>
            <td nowrap>
              <input name="descripcion" type="text" id="descripcion" class="textbox">
            </td>
          </tr>
          <tr class="fila">
            <td nowrap><strong>Valor :</strong></td>
            <td nowrap>
              <input name="valor" type="text" id="valor" class="textbox" onKeyPress="soloDigitos(event,'decOK')">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">            </td>
          </tr>
          <tr class="fila">
            <td nowrap><strong>Moneda :</strong></td>
            <td nowrap>
              <select name="moneda" id="moneda" class="textbox">
                <option value="PES" selected>Pesos</option>
                <option value="DOL">Dolar</option>
                <option value="BOL">Bolivar</option>
              </select>
            </td>
          </tr>
        </table></td>
      </tr>
    </table>
  	<center class='comentario'>


 <%if(!Mensaje.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>
</center>

	<br>
  <div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if( ValidarFormTiquetes(form1) ){form1.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">  </div>
  
</form>
</div>
</body>
</html>
