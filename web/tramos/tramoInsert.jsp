<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Ingresar TRAMO</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<link href="css/letras.css" rel="stylesheet" type="text/css">

</head>
<body>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Tramo&accion=Configurar&buscar=show">
  <table width="60%" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="letras">
    <tr bgcolor="#FFA928">
      <td colspan="2"><div align="center"><strong>BUSCAR EL TRAMO A CONFIGURAR</strong></div></td>
    </tr>
    <tr bgcolor="#EFE3DE">
      <td width="40%" bgcolor="#EFE3DE"><strong>Ciudad de Origen
      </strong>        <div align="center"></div>        </td>
      <td width="60%" bgcolor="#EFE3DE">  <%TreeMap ciudades = model.ciudadService.getCiudades(); %>
	  <input:select name="ciudado" options="<%=ciudades%>" attributesText="style='width:100%;'" /></td>
    </tr>
    <tr bgcolor="#EFE3DE">
      <td bgcolor="#EFE3DE"><strong>Ciudad de Destino </strong></td>
      <td bgcolor="#EFE3DE">  <%TreeMap ciudades1 = model.ciudadService.getCiudades(); %>
	  <input:select name="ciudadd" options="<%=ciudades1%>" attributesText="style='width:100%;'" /></td>
    </tr>
    <tr bgcolor="#EFE3DE">
      <td colspan="2"><div align="center">
        <input type="submit" name="Submit3" value="Buscar...">
      </div></td>
    </tr>
  </table>
</form>
<% if(request.getAttribute("tramo")!=null){
	Tramo t = (Tramo)request.getAttribute("tramo");
%>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Tramo&accion=Configurar" onSubmit="">

  <table width="606" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#EFE3DE" class="Letras">
    <tr bgcolor="#FFA928">
      <td colspan="3" nowrap><div align="center" class="Estilo2"><strong>CONFIGURACION DE TRAMOS</strong></div></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td colspan="3" nowrap><strong><U>        DATOS DEL TRAMO</U></strong></td>
    </tr>
    <tr>
      <td width="184" nowrap><strong>Distrito</strong></td>
      <td width="422" colspan="2" nowrap><%=t.getDstrct()%>          </td>
    </tr>
    <tr>
      <td nowrap><strong>Ciudad  Origen </strong></td>
      <td colspan="2" nowrap><%=t.getOriginn()%>
      <input name="ciudado" type="hidden" id="ciudado" value="<%=t.getOrigin()%>"></td>
    </tr>
    <tr>
      <td nowrap><strong>Ciudad Destino </strong></td>
      <td colspan="2" nowrap><%=t.getDestinationn()%>
      <input name="ciudadd" type="hidden" id="ciudadd" value="<%=t.getDestination()%>"></td>
    </tr>
	<tr>
      <td nowrap><strong>Distancia en Kms.</strong></td>
      <td colspan="2" nowrap><%=t.getKm()%></td>
	</tr>
	<tr>
      <td nowrap><strong>Distancia en Tiempo </strong></td>
      <td colspan="2" nowrap><%=t.getTime()%> Hrs.</td>
	</tr>
    <tr bgcolor="#99CCFF">
      <td colspan="3" nowrap><strong><U>        DATOS A CONFIGURAR</U></strong></td>
    </tr>
    <tr bgcolor="#ebebeb">
      <td colspan="3" nowrap>&nbsp;</td>
    </tr>
    <tr>
      <td nowrap><strong>Galones Acpm</strong></td>
      <td colspan="2" nowrap><input name="acpm" type="text" id="acpm" value="<%=t.getAcpm()%>"></td>
    </tr>
    <tr>
      <td nowrap><strong>Tiket Id 1 </strong></td>
      <td colspan="2" nowrap><input name="tid1" type="text" id="tid1" value="<%=t.getTicket_id1()%>"></td>
    </tr>
    <tr>
      <td nowrap><strong>Cantidad Max tiquetes 1 </strong></td>
      <td colspan="2" nowrap><input name="ctmax1" type="text" id="ctmax1" value="<%=t.getCantidad_maximo_ticket1()%>"></td>
    </tr>
    <tr bgcolor="#ebebeb">
      <td colspan="3" nowrap>&nbsp;</td>
    </tr>
    <tr>
      <td nowrap><strong>Tiket Id 2 </strong></td>
      <td colspan="2" nowrap><input name="tid2" type="text" id="tid2" value="<%=t.getTicket_id2()%>"></td>
    </tr>
    <tr>
      <td nowrap><strong>Cantidad Max tiquetes 2 </strong></td>
      <td colspan="2" nowrap><input name="ctmax2" type="text" id="ctmax2" value="<%=t.getCantidad_maximo_ticket2()%>"></td>
    </tr>
	<tr bgcolor="#ebebeb">
      <td colspan="3" nowrap>&nbsp;</td>
    </tr>
    <tr>
      <td nowrap><strong>Tiket Id 3 </strong></td>
      <td colspan="2" nowrap><input name="tid3" type="text" id="tid3" value="<%=t.getTicket_id3()%>"></td>
    </tr>
    <tr>
      <td nowrap><strong>Cantidad Max tiquetes 3</strong></td>
      <td colspan="2" nowrap><input name="ctmax3" type="text" id="ctmax3" value="<%=t.getCantidad_maximo_ticket3()%>"></td>
    </tr>
	<tr bgcolor="#ebebeb">
      <td colspan="3" nowrap>&nbsp;</td>
    </tr>
    <tr>
      <td nowrap><strong>Tiket Id 4 </strong></td>
      <td colspan="2" nowrap><input name="tid4" type="text" id="tid4" value="<%=t.getTicket_id4()%>"></td>
    </tr>
    <tr>
      <td nowrap><strong>Cantidad Max tiquetes 4 </strong></td>
      <td colspan="2" nowrap><input name="ctmax4" type="text" id="ctmax4" value="<%=t.getCantidad_maximo_ticket4()%>"></td>
    </tr>
	<tr bgcolor="#ebebeb">
      <td colspan="3" nowrap>&nbsp;</td>
    </tr>
    <tr>
      <td nowrap><strong>Tiket Id 5 </strong></td>
      <td colspan="2" nowrap><input name="tid5" type="text" id="tid5" value="<%=t.getTicket_id5()%>"></td>
    </tr>
    <tr>
      <td nowrap><strong>Cantidad Max tiquetes 5</strong></td>
      <td colspan="2" nowrap><input name="ctmax5" type="text" id="ctmax5" value="<%=t.getCantidad_maximo_ticket5()%>"></td>
    </tr>
	<tr bgcolor="#ebebeb">
      <td colspan="3" nowrap>&nbsp;</td>
    </tr>
    <tr>
      <td nowrap><strong>Tiket Id 6</strong></td>
      <td colspan="2" nowrap><input name="tid6" type="text" id="tid6" value="<%=t.getTicket_id6()%>"></td>
    </tr>
    <tr>
      <td nowrap><strong>Cantidad Max tiquetes 6 </strong></td>
      <td colspan="2" nowrap><input name="ctmax6" type="text" id="ctmax6" value="<%=t.getCantidad_maximo_ticket6()%>"></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <input type="submit" name="Submit" value="Registrar">
    <input type="button" name="Submit2" value="Regresar">
  </div>

</form>
<%}%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
