<%@page contentType="text/html"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.beans.*" %>
<%@page import="com.tsp.operation.model.beans.filegeneration.FileGenerator" %>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
Usuario userLoggedIn = (Usuario) session.getAttribute("Usuario");
String cmd = request.getParameter("cmd");
String tipoOpcion = request.getParameter("tipoOpcion");
cmd = (cmd == null ? "" : cmd);
String contentType = request.getParameter("contentType");
contentType = (contentType == null ? "text/html" : contentType);
contentType = (contentType.equals("file") ? "text/html" : contentType);
response.setContentType( contentType );
%>
<html>
<head>
    <title>CONSULTAS</title>
    <link href="css/Style.css" rel='stylesheet' type="text/css">
    <script language='javascript' src='js/PantallaConsultas.js'></script>
</head>
<body>
<%
// 1. CREAR CONSULTA NUEVA
if( cmd.equals("")) {
  
  String idUsuario     = null;
  String nombreUsuario = null;
  Usuario user         = null;
  String nombreCreador = (userLoggedIn == null ? "Nestor Parejo" : userLoggedIn.getNombre());
  String creador       = (userLoggedIn == null ? "nparejo" : userLoggedIn.getLogin()); %>
  
  <form name='filtroCrearQueryFrm' method='post' action='<%= CONTROLLER %>?estado=Consultas&accion=Manage'>
    <table cellspacing='2' cellpadding='2' width='100%' align='center' border='0' class='fondotabla'>
      <tr class='titulo'><th colspan='3'>Crear Consulta</th></tr>
      <tr><td colspan='3'>&nbsp;</td></tr>
      <tr>
        <th align='left' style='width: 25%'>Creador</th>
        <td align='left' colspan='2' style='width: 75%'><%= nombreCreador %></td>
      </tr>
      <tr>
        <th align='left' style="width: 25%">
            Usuarios Asociados
        </th>
        <td>
          <select name='usuario' style='width: 75%' class='select'>
            <option value='' selected></option><%
            List usersList = model.consultaService.getUsuariosSot(null);
            Iterator usersIt = usersList.iterator();
            while( usersIt.hasNext() )
            {
              user = (Usuario) usersIt.next();
              idUsuario     = user.getLogin();
              nombreUsuario = user.getNombre(); %>
              <option value='<%= idUsuario %>'><%= nombreUsuario %></option><%
            } %>
          </select>
        </td>
        <td>
           <select name='userAdd' multiple style='width: 75%'>
           </select>
        </td>
      </tr>
      <tr>
         <td></td>
         <td><input type='button' name='agregar' class='boton' value='Adicionar Usuario' onclick='addElement(usuario, userAdd)'></td>
         <td><input type='button' name='eliminar' class='boton' value='Eliminar Usuario' onclick='removeElem(userAdd)'></td>
      </tr>
      <tr>
        <th align='left' style='width: 25%'>Descripcion</th>
        <td colspan='2' style='width: 75%'>
          <textarea style='width: 100%' name='descripcion' rows='3'></textarea>
        </td>
      </tr>
      <tr>
        <th align='left' style='width: 25%'>
            Base de Datos en la Cual se va a Ejecutar la Consulta
        </th>
        <td colspan='2'>
          <select name='baseDeDatos' style='width: 100%' class='select'>
            <option value='PSQL_SOT' selected>POSTGRESQL - SOT</option>
            <option value='ORA_MIMS'>ORACLE - MIMS</option>
          </select>
        </td>
      </tr>
      <tr><th align='left' colspan='3'>&nbsp;</th></tr>
      <tr>
        <th align='left' colspan='3'>
            Query<br>NOTA: Puede parametrizar las consultas colocando el token
            <i>&lt;param&gt;</i> en lugar del valor.
        </th>
      </tr>
      <tr>
        <td colspan='3'>
          <textarea style='width: 100%' name='query' rows='17'
                    onblur='queryBlur()'></textarea>
        </td>
      </tr>
      <tr><th align='left' colspan='3'>&nbsp;</th></tr>
      <tr>
        <th align='left' rowspan='2' style='width: 25%'>
            Nombres de los parametros (Obligatorios al guardar)
        </th>
        <td colspan='2' style='width: 75%'>
          <select style='width: 100%' size='5' name='paramNamesList'></select>
          <input type='text' name='nombreparam' style='width: 100%' disabled>
        </td>
      </tr>
      <tr>
        <td align='middle' style='width: 37%'>
          <input type='button' value='A�adir' name='nuevoNombreParamBtn' disabled
                 onclick='nuevoNombreParamBtnClick()' class='boton'>
        </td>
        <td align='middle' style='width: 38%'>
          <input type='button' value='Quitar' name='quitarNombreParamBtn' disabled
                 onclick='quitarNombreParamBtnClick()' class='boton'>
        </td>
      </tr>
      <tr>
        <th align='left' rowspan='2' style='width: 25%'>
            Valores de los parametros (Obligatorios al ejecutar)
        </th>
        <td colspan='2' style='width: 75%'>
          <select style='width: 100%' size='5' name='paramValuesList'></select>
          <input type='text' name='valorparam' style='width: 100%' disabled>
        </td>
      </tr>
      <tr>
        <td align='middle' style='width: 37%'>
          <input type='button' value='A�adir' name='nuevoValorParamBtn' disabled
                 onclick='nuevoValorParamBtnClick()' class='boton'>
        </td>
        <td align='middle' style='width: 38%'>
          <input type='button' value='Quitar' name='quitarValorParamBtn' disabled
                 onclick='quitarValorParamBtnClick()' class='boton'>
        </td>
      </tr>
      <tr>
        <td align='middle' colspan='3' style='width: 100%'>
          <input type='button' value='Ejecutar' name='ejecutarquerybtn'
                 onclick='queryActionBtnsClick("execute", "<%= CONTROLLER %>")' class='boton'>&nbsp;&nbsp;
          <input type='button' value='Guardar' name='guardarquerybtn'
                 onclick='queryActionBtnsClick("save", "<%= CONTROLLER %>")' class='boton'>&nbsp;&nbsp;
        </td>
      </tr>
    </table>
    <input type='hidden' name='cmd' value=''>
    <input type='hidden' name='contentType' value='text/html'>
    <input type='hidden' name='nombresParams' value=''>
    <input type='hidden' name='valoresParams' value=''>
    <input type='hidden' name='creador' value='<%= creador %>'>
    <input type='hidden' name='tipoOpcion' value='<%= tipoOpcion %>'>
  </form><%
}else if( cmd.equals("sql_list") ) {
  List queriesList = model.consultaService.getConsultasSQL(userLoggedIn);
  if( queriesList.size() == 0 ) { %>
    <br><br><center><b>Usted no Tiene Consultas Asociadas</b></center><%
  }else{%>
    <table cellspacing='2' cellpadding='2' width='80%' align='center' border='1' class='fondotabla'>
        <tr class='titulo'><th colspan='8'>Consultas Disponibles</th></tr>
        <tr class='subtitulo'>
          <th nowrap>Ejecucion</th>
          <th nowrap>Par&aacute;metros</th>
          <th nowrap>Generar Como</th>
          <th nowrap>Descripci�n</th>
          <th nowrap>Base de Datos</th>
          <th nowrap>Parametrizado</th>
          <th nowrap>Creador</th>
          <th nowrap>Fecha de Creaci�n</th>
        </tr>
        <%
          Iterator queriesIt = queriesList.iterator();
          while( queriesIt.hasNext() )
          {
            ConsultaSQL sql         = (ConsultaSQL) queriesIt.next();
            String recordId         = sql.getRecordId();
            String descripcion      = sql.getDescripcion();
            String [] nombresParams = sql.getNombresParametros();
            String creador          = sql.getCreador();
            String fechaCreacion    = sql.getFechaCreacion();
            String baseDeDatos      = sql.getBaseDeDatos(); %>
        <tr class='peque�a'>
          <td>
             <input type="button" name="btnejecutar" value="Ejecutar" onclick="javascript:executeQuery('<%= CONTROLLER %>', <%= recordId %>)" class="boton">
          </td>
          <td align='center' nowrap><%
          if( nombresParams != null ) { %>
            <table cellSpacing='2' cellPadding='2' width='100%'
                   align='center'><%
            for( int idx = 0; idx < nombresParams.length; idx++ ) { %>
              <tr align='left' class='peque�a'>
                <td align='left' nowrap><b><%= sql.getNombresParametros(idx) %></b></td>
                <td width='100%'>
                  <input type="text" name='param<%= recordId %>' id='param<%= recordId %><%= idx %>'
                         size='20' value='' class="normal">
                </td>
              </tr><%
            } %>
            </table><%
          }else{ %>
            &nbsp;<%
          } %>
          </td>
          <td>
            <select name='contentType' id='contentType<%= recordId %>' class="select">
              <option value='text/html'>P&aacute;gina web
              <%--option value='<%= FileGenerator.MIME_MS_EXCEL %>'>MS-Excel--%>
              <option value='file'>Archivo MS-Excel
            </select>
          </td>
          <td align='left' nowrap>
            <b><%= descripcion %></b>         
            <input type='hidden' name='desc<%= recordId %>'
                   id='desc<%= recordId %>'
                   value='<%= descripcion %>'>
          </td>
          <td align='center' nowrap>
            <%= baseDeDatos.equals("PSQL_SOT") ? "PostgreSQL - SOT" :
               (baseDeDatos.equals("ORA_MIMS") ?"Oracle - MIMS" : "") %>
            <input type='hidden' name='baseDeDatos<%= recordId %>'
                   id='baseDeDatos<%= recordId %>'
                   value='<%= baseDeDatos %>'>
          </td>
          <td align='center' nowrap><%= nombresParams != null ? "S�" : "No" %></td>
          <td align='center' nowrap><%= creador %></td>
          <td align='center' nowrap><%= fechaCreacion %></td>
        </tr><%
      } %>
    </table><%
  }
}else if( cmd.equals("sql_execute") || cmd.equals("sql_listexec") ) {
  List queryResultsList = model.consultaService.getActionResultsList();
  if( queryResultsList.size() <= 0 ) { %>
    <center><b>No Hay Registros</b></center><%
  }else{
    Iterator queryResultsIt = queryResultsList.iterator();
    ConsultaSQL sqlResult = (ConsultaSQL) queryResultsIt.next(); %>
    <table cellspacing='2' cellpadding='2' width='100%' align='center' border='1' class='fondotabla'>
      <tr class='titulo'>
        <th colspan='<%= sqlResult.getConteoColumnas() %>'>
          <%= request.getParameter("descripcion") %>
        </th>
      </tr>
      <tr class='subtitulo'><%
      String [] nombresCampos = sqlResult.getNombresCampos();
      for( int idx = 0; idx < nombresCampos.length; idx++ ) { %>
        <th nowrap>
          <%= nombresCampos[idx].equals("") ? "&nbsp" : nombresCampos[idx] %>
        </th><%
      } %>
      </tr><%
      boolean hasNext = true;
      int c = 0;
      while( hasNext )
      { %>
        <tr class='peque�a'><%
        for( int idx = 0; idx < nombresCampos.length; idx++ )
        {
          String valorCampo = sqlResult.getValorCampo(nombresCampos[idx]); %>
          <td nowrap>
            <%= valorCampo.equals("") ? "&nbsp" : valorCampo %>
          </td><%
        } %>
        </tr><%
        hasNext = queryResultsIt.hasNext();
        c++;
        if( hasNext )
          sqlResult = (ConsultaSQL) queryResultsIt.next();
      } %>
        <tr class='peque�a'><td colspan='<%=nombresCampos.length%>'><%=(c + " REGISTROS SELECCIONADOS.")%></td></tr>      
    </table><%
  }
}
//ATUALIZACION DE QUERYS
else if( cmd.equals("sql_update") ) {%>
    <%DatosConsulta qry = (DatosConsulta)request.getAttribute("consulta"); %>
    <input:form name='filtroCrearQueryFrm' method='post' action='<%=(CONTROLLER + "?estado=Consultas&accion=Manage")%>' bean="consulta">
    <table cellspacing='2' cellpadding='2' width='100%' align='center' border='0' class='fondotabla'>
      <tr class='titulo'><th colspan='3'>Modificar Consulta</th></tr>
      <tr><td colspan='3'>&nbsp;</td></tr>
      <tr>
        <th align='left' style='width: 25%'>Creador</th>
        <td align='left' colspan='2' style='width: 75%'><%=qry.getCreador()%></td>
      </tr>
      <tr>
        <th align='left' style="width: 25%">
            Usuarios Asociados
        </th>
        <td>
          <input:select name="usuario" attributesText="class='select'">
           <input:option value='' attributesText="selected"> </input:option><%
            List usersList = model.consultaService.getUsuariosSot(null);
            Iterator usersIt = usersList.iterator();
            Usuario user = null;
            while( usersIt.hasNext() )
            {
              user = (Usuario) usersIt.next();
              %>
              <input:option value='<%= user.getLogin() %>'><%= user.getNombre() %></input:option><%
            } %>
            </input:select>
        </td>
        <td>
           <input:select name="userAdd" multiple="true" attributesText="style='width: 75%' class='select'" options="<%=qry.getUserAdd()%>"/>
        </td>
      </tr>
      <tr>
         <td></td>
         <td><input type='button' name='agregar' class='boton' value='Adicionar Usuario' onclick='addElement(usuario, userAdd)'></td>
         <td><input type='button' name='eliminar' class='boton' value='Eliminar Usuario' onclick='removeElem(userAdd)'></td>
      </tr>
      <tr>
        <th align='left' style='width: 25%'>Descripcion</th>
        <td colspan='2' style='width: 75%'>
          <input:textarea name="descripcion" rows="3" attributesText="style='width: 100%'"></input:textarea>
        </td>
      </tr>
      <tr>
        <th align='left' style='width: 25%'>
            Base de Datos en la Cual se va a Ejecutar la Consulta
        </th>
        <td colspan='2'>
          <input:select name="baseDeDatos" attributesText="style='width: 100%' class='select'" default="<%=qry.getBaseDeDatos()%>">
              <input:option value="PSQL_SOT">POSTGRESQL - SOT</input:option>
              <input:option value="ORA_MIMS">ORACLE - MIMS</input:option>
          </input:select>
        </td>
      </tr>
      <tr><th align='left' colspan='3'>&nbsp;</th></tr>
      <tr>
        <th align='left' colspan='3'>
            Query<br>NOTA: Puede parametrizar las consultas colocando el token
            <i>&lt;param&gt;</i> en lugar del valor.
        </th>
      </tr>
      <tr>
        <td colspan='3'>
          <textarea name='query' rows='17' onblur='queryBlur()' style='width: 100%' onfocus='jscript:oldQuery = this.value;'><%=qry.getQuery()%></textarea>         
        </td>
      </tr>
      <tr><th align='left' colspan='3'>&nbsp;</th></tr>
      <tr>
        <th align='left' rowspan='2' style='width: 25%'>
            Nombres de los parametros (Obligatorios al guardar)
        </th>
        <td colspan='2' style='width: 75%'>
          <input:select name="paramNamesList" attributesText="style='width: 100%' size='5' class='select'" options="<%=qry.getParamNamesList()%>"/>
          <input:text name="nombreparam" attributesText="disabled style='width: 100%'"/>
        </td>
      </tr>
      <tr>
        <td align='middle' style='width: 37%'>
          <input type='button' value='A�adir' name='nuevoNombreParamBtn' disabled
                 onclick='nuevoNombreParamBtnClick()' class='boton'>
        </td>
        <td align='middle' style='width: 38%'>
          <input type='button' value='Quitar' name='quitarNombreParamBtn' disabled
                 onclick='quitarNombreParamBtnClick()' class='boton'>
        </td>
      </tr>
      <tr>
        <th align='left' rowspan='2' style='width: 25%'>
            Valores de los parametros (Obligatorios al ejecutar)
        </th>
        <td colspan='2' style='width: 75%'>
          <input:select name="paramValuesList" attributesText="style='width: 100%' size='5' class='select'"/>
          <input:text name="valorparam" attributesText="disabled style='width: 100%'"/>
        </td>
      </tr>
      <tr>
        <td align='middle' style='width: 37%'>
          <input type='button' value='A�adir' name='nuevoValorParamBtn' disabled
                 onclick='nuevoValorParamBtnClick()' class='boton'>
        </td>
        <td align='middle' style='width: 38%'>
          <input type='button' value='Quitar' name='quitarValorParamBtn' disabled
                 onclick='quitarValorParamBtnClick()' class='boton'>
        </td>
      </tr>
      <tr>
        <td align='middle' colspan='3' style='width: 100%'>
          <input type='button' value='Modificar' name='ejecutarquerybtn'
                 onclick='queryActionBtnsClick("update", "<%= CONTROLLER %>")' class='boton'>&nbsp;&nbsp;
        </td>
      </tr>
    </table>
    <input type='hidden' name='cmd' value=''>
    <input type='hidden' name='contentType' value='text/html'>
    <input type='hidden' name='nombresParams' value=''>
    <input type='hidden' name='valoresParams' value=''>
    <input type='hidden' name='creador' value=''>
    <input type='hidden' name='id' value='<%= qry.getId() %>'>
    <input type='hidden' name='tipoOpcion' value='<%= tipoOpcion %>'>
  </input:form>  
<%
}
else if( cmd.equals("file") ) {%>
<table class="fondotabla" align="center">
      <tr align='center'>
         <td class='normal'> 
             SU ARCHIVO SE ESTA GENERANDO VAYA AL SERVICIO DE DESCARGAS  
             <img src="images/file.gif"> PARA OBTENERNO...
         </td>
      </tr>
</table>
<%
}
%>
</body>
</html>
