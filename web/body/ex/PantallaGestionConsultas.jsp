<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="../WEB-INF/InitModel.jsp" %>
<%
    Usuario user = (Usuario)session.getAttribute("Usuario");
    List lista = model.consultaService.getConsultasSQL(user);
    Iterator queriesIt = lista.iterator();
%>
<html>
<head>
    <link rel='stylesheet' href='<%= BASEURL %>/css/Style.css'> 
    
    <title>Mantinimiento Menu SOT</title>
    <style>
        tr.select   { background-color: teal;  color:white; font-size:10px; font-family:Verdana}
        tr.unselect { background-color: white; color:black; font-size:10px; font-family:Verdana}
    </style>
    <script src='<%= BASEURL %>/js/Validaciones.js'></script>
    <script>
        function _onmousemove(item) { item.className='select';   }
        function _onmouseout (item) { item.className='unselect'; }
                
        function SelAll(){
            for(i=0;i<FormularioListado.length;i++)
               FormularioListado.elements[i].checked=FormularioListado.All.checked;
        }
        function ActAll(){
            FormularioListado.All.checked = true;
            for(i=0;i<FormularioListado.length;i++)	
              if (FormularioListado.elements[i].type=='checkbox' && !FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All'){
                  FormularioListado.All.checked = false;
                  break;
              }
        } 
        function validarListado(form){
            for(i=0;i<FormularioListado.length;i++)	
                if (FormularioListado.elements[i].type=='checkbox' && FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All')
                    return true;
            alert('Por favor seleccione un item para poder continuar');
            return false;
        }   
        function redireccionar(url){
           window.location.href = url;
        }
    </script>
</head>
<body>
<center>
<% String mensaje =  (String)request.getAttribute("mensaje");
   if ( (mensaje != null) && (!mensaje.equals("")) ){ %>
        <center>
        <fieldset style='width=400; background=f5f5cd; align=center;' >
               <p class='negrita'>
                  <%=mensaje%>
               </p>  
        </fieldset>
        </center>
<%}%>
<form action='<%= CONTROLLER %>?estado=Consultas&accion=Manage&cmd=sql_delete' method='post' name='FormularioListado' onsubmit='javascript: return validarListado(); '>
<table border='1' class='fondotabla' width='700' cellpadding='0' cellspacing='0'>
<tr class='titulo'><th height='30'> Gestion de Consultas </th></tr>
<tr>
<td>
<% if (lista!=null && lista.size()>0){ %>
     <table width='100%' cellpadding='1' cellspacing='1' border='0' class='fondotabla'>
     
     <tr class='subtitulo'>
       <th width='20'><input type='checkbox' name='All' onclick='jscript: SelAll();'></th>
       <th width='250' colspan='2' nowrap>Consulta</th>
       <th width='350'>Creador</th>
       <th width='350'>Usarios Asociados</th>
       <th width='100'></th>
     </tr>
<%   for (int i=0;i<lista.size();i++){
        ConsultaSQL sql = (ConsultaSQL) queriesIt.next(); %>
        <tr onmousemove='_onmousemove(this);' onmouseout='_onmouseout(this);' class='unselect'>
            <td><input type='checkbox' name='id' value='<%= sql.getRecordId() %>' onclick='jscript: ActAll();'></td> 
            <td align='center'><img src="<%= BASEURL %>/images/menu-images/menu_folder_closed.gif"></td>
            <td nowrap><%= sql.getDescripcion() %></td>
            <td><%= sql.getCreador() %></td>
            <td><%= sql.getUsuarioAsociado() %></td>
            <td align='center' nowrap>
                <input type='button' value='Editar' onclick="redireccionar('<%= CONTROLLER %>?estado=Consultas&accion=Manage&cmd=sql_search&id=<%= sql.getRecordId() %>')" >
            </td>
        </tr>
<%   } %>
    </table>    
<% } %>
</td>
</tr>
</table>

<br>
<input type='button' value='Nuevo' class='boton' style='width:120' onclick="redireccionar('<%= CONTROLLER %>?estado=Consultas&accion=Manage&cmd=create')">
<input type='submit' value='Eliminar' class='boton' style='width:120' name='Opcion'>
</form>

</body>
</html>
