<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="../WEB-INF/InitModel.jsp" %>
<%
try{
	System.out.println("en pgc.jsp2");
    Usuario user = (Usuario)session.getAttribute("Usuario");
    List lista = model.consultaService.getConsultasSQL(user);
	System.out.println("lista"+lista);
    Iterator queriesIt = lista.iterator();
	System.out.println("iterator lista"+queriesIt);
%>
<html>
<head>
    <link rel='stylesheet' href='<%= BASEURL %>/css/Style.css'> 
    
    <title>Mantenimiento Menu SOT</title>
    <style>
        tr.select   { background-color: teal;  color:white; font-size:10px; font-family:Verdana}
        tr.unselect { background-color: white; color:black; font-size:10px; font-family:Verdana}
    </style>
    <script type='text/javascript' src='<%= BASEURL %>/js/Validaciones.js'></script>
    <script type='text/javascript' src='<%= BASEURL %>/js/PantallaGestionConsultas.js'></script>    
</head>
<body>
<center>
<% String mensaje =  (String)request.getAttribute("mensaje");
   if ( (mensaje != null) && (!mensaje.equals("")) ){ %>
        <center>
        <fieldset style='width=400; background=f5f5cd; align=center;' >
               <p class='negrita'>
                  <%=mensaje%>
               </p>  
        </fieldset>
        </center>
<%}%>
<form action='<%= CONTROLLER %>?estado=Consultas&accion=Manage&cmd=sql_delete' method='post' name='FormularioListado' onsubmit='javascript: return validarListado(); '>
<table border='1' class='fondotabla' width='700' cellpadding='0' cellspacing='0'>
<tr class='titulo'><th height='30'> Gestion de Consultas </th></tr>
<tr>
<td>
<% if (lista!=null && lista.size()>0){ 
	System.out.println("in if");
%>
     <table width='100%' cellpadding='1' cellspacing='1' border='0' class='fondotabla'>
     
     <tr class='subtitulo'>
       <th width='20'><input type='checkbox' name='All' onclick='jscript: SelAll();'></th>
       <th width='250' colspan='2' nowrap>Consulta</th>
       <th width='350'>Creador</th>
       <th width='350'>Usarios Asociados</th>
       <th width='100'></th>
     </tr>
<%  System.out.println("antes de for");
	for (int i=0;i<lista.size();i++){
        ConsultaSQL sql = (ConsultaSQL) queriesIt.next(); %>
        <tr onmousemove='_onmousemove(this);' onmouseout='_onmouseout(this);' class='unselect'>
            <td><input type='checkbox' name='id' value='<%= sql.getRecordId() %>' onclick='jscript: ActAll();'></td> 
            <td align='center'><img src="<%= BASEURL %>/images/menu-images/menu_folder_closed.gif"></td>
            <td nowrap><%= sql.getDescripcion() %></td>
            <td><%= sql.getCreador() %></td>
            <td><%= sql.getUsuarioAsociado() %></td>
            <td align='center' nowrap>
                <input type='button' value='Editar' onclick="redireccionar('<%= CONTROLLER %>?estado=Consultas&accion=Manage&cmd=sql_search&id=<%= sql.getRecordId() %>')" >
            </td>
        </tr>
<%   } %>
    </table>    
<% } 

}catch(Exception e){
	System.out.println("error  en pgc.jsp"+e.toString());
}
%>
</td>
</tr>
</table>

<br>
<input type='button' value='Nuevo' class='boton' style='width:120' onclick="redireccionar('<%= CONTROLLER %>?estado=Consultas&accion=Manage&cmd=create')">
<input type='submit' value='Eliminar' class='boton' style='width:120' name='Opcion'>
</form>

</body>
</html>
