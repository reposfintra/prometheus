<%@page contentType="text/html"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.beans.*" %>
<%@page import="com.tsp.operation.model.beans.filegeneration.FileGenerator" %>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
Usuario userLoggedIn = (Usuario) session.getAttribute("Usuario");
String cmd = request.getParameter("cmd");
String tipoOpcion = request.getParameter("tipoOpcion");
cmd = (cmd == null ? "" : cmd);
String contentType = request.getParameter("contentType");
contentType = (contentType == null ? "text/html" : contentType);
contentType = (contentType.equals("file") ? "text/html" : contentType);
String nitobix = request.getParameter("nitobix");//20101117
contentType = (contentType.equals("nitobix") ? "text/html" : contentType);//20101117
response.setContentType( contentType );
try {
%>
<html xmlns:ntb="http://www.nitobi.com">
<head>

      <!--20101116-->
      <script type='text/javascript' src="<%= BASEURL%>/js/nitobi.toolkit.js"></script>
      <script type='text/javascript' src="<%= BASEURL%>/js/nitobi.grid.js"></script>
      <script type='text/javascript' src="<%= BASEURL%>/js/nitobi.combo.js"></script>
      <link href="<%= BASEURL%>/css/nitobi.grid.css" rel='stylesheet'>
      <link href="<%= BASEURL%>/css/nitobi.combo.css" rel='stylesheet'>
      <link href="<%= BASEURL%>/css/grid/nitobi/toolbar/toolbar.css" rel='stylesheet'>
      <!--20101116-->

    <title>CONSULTAS</title>
    <link href="css/Style.css" rel='stylesheet' type="text/css">
    <script type='text/javascript' src='<%= BASEURL %>/js/tools.js'></script>
    <script type='text/javascript' src='<%= BASEURL %>/js/PantallaConsultas.js'></script>
</head>
<body <%= cmd.equals("sql_update") ? "onload='PantallaConsultasUpdateLoad()'" : "" %>><%
// 1. CREAR CONSULTA NUEVA
if( cmd.equals(""))
{
  String idUsuario     = null;
  String nombreUsuario = null;
  Usuario user         = null;
  String nombreCreador = (userLoggedIn == null ? "Nestor Parejo" : userLoggedIn.getNombre());
  String creador       = (userLoggedIn == null ? "nparejo" : userLoggedIn.getLogin()); %>

  <form name='filtroCrearQueryFrm' method='post' action='<%= CONTROLLER %>?estado=Consultas&accion=Manage'>
    <table cellspacing='2' cellpadding='2' width='100%' align='center' border='0' class='fondotabla'>
      <tr class='titulo'><th colspan='3'>Crear Consulta</th></tr>
      <tr><td colspan='3'>&nbsp;</td></tr>
      <tr>
        <th align='left' style='width: 25%'>Creador</th>
        <td align='left' colspan='2' style='width: 75%'><%= nombreCreador %></td>
      </tr>
      <tr>
        <th align='left' style="width: 25%">
            Usuarios Asociados
        </th>
        <td>
          <select name='usuario' style='width: 75%' class='select'>
            <option value='' selected></option><%
            List usersList = model.consultaService.getUsuariosSot(null);
            Iterator usersIt = usersList.iterator();
            while( usersIt.hasNext() )
            {
              user = (Usuario) usersIt.next();
              idUsuario     = user.getLogin();
              nombreUsuario = user.getNombre(); %>
              <option value='<%= idUsuario %>'><%= nombreUsuario %></option><%
            } %>
          </select>
        </td>
        <td>
           <select name='userAdd' multiple style='width: 75%'>
           </select>
        </td>
      </tr>
      <tr>
         <td></td>
         <td><input type='button' name='agregar' class='boton' value='Adicionar Usuario' onclick='addElement(usuario, userAdd)'></td>
         <td><input type='button' name='eliminar' class='boton' value='Eliminar Usuario' onclick='removeElem(userAdd)'></td>
      </tr>
      <tr>
        <th align='left' style='width: 25%'>Descripcion</th>
        <td colspan='2' style='width: 75%'>
          <textarea style='width: 100%' name='descripcion' rows='3'></textarea>
        </td>
      </tr>
      <tr>
        <th align='left' style='width: 25%'>
            Base de Datos en la Cual se va a Ejecutar la Consulta
        </th>
        <td colspan='2'>
          <select name='baseDeDatos' style='width: 100%' class='select'>
            <option value='PSQL_SOT' selected>POSTGRESQL - SOT</option>
            <!--<option value='ORA_MIMS'>ORACLE - MIMS</option>-->
          </select>
        </td>
      </tr>
      <tr><th align='left' colspan='3'>&nbsp;</th></tr>
      <tr>
        <th align='left' colspan='3'>
            Query<br>NOTA: Puede parametrizar las consultas colocando los tokens
            <i>&lt;param&gt;</i> para valores especificos<br>y <i>&lt;param-in&gt;</i>
            para listas de parametros separados por comas.<br>
            ADVERTENCIA: Los queries involucrados en la consulta (los de creacion,
            seleccion y eliminacion de tablas) DEBEN ser separados con el token "&lt;br&gt;"
        </th>
      </tr>
      <tr>
        <td colspan='3'>
          <textarea style='width: 100%' name='query' rows='17'
                    onblur='queryBlur()'></textarea>
        </td>
      </tr>
      <tr><th align='left' colspan='3'>&nbsp;</th></tr>
      <tr>
        <th align='left' rowspan='2' style='width: 25%'>
            Nombres de params. (Obligatorios al guardar)
        </th>
        <td colspan='2' style='width: 75%'>
          <select id='paramNamesListId' style='width: 100%' size='5' name='paramNamesList' class='select'></select>
          <input id='nombreparamId' type='text' name='nombreparam' style='width: 100%' disabled>
        </td>
      </tr>
      <tr>
        <td align='middle' style='width: 37%'>
          <input type='button' value='A�adir' id="nombreParamAgregar" name='nuevoValorParamBtn' class='boton' disabled
                 onclick='nuevoDatoParamBtnClick("paramNamesListId", "nombreparamId", "nombresParamsId")'>
        </td>
        <td align='middle' style='width: 38%'>
          <input type='button' value='Quitar' id="nombreParamBorrar" name='quitarValorParamBtn' class='boton' disabled
                 onclick='quitarDatoParamBtnClick("paramNamesListId", "nombresParamsId")'>
        </td>
      </tr>
      <tr>
        <th align='left' rowspan='2' style='width: 25%'>
            Valores de params. (Obl. al ejecutar - token "&lt;param&gt;" -)
        </th>
        <td colspan='2' style='width: 75%'>
          <select id='paramValuesListId' style='width: 100%' size='5' name='paramValuesList' class='select'></select>
          <input id='valorparamId' type='text' name='valorparam' style='width: 100%' disabled
                 onkeypress='return valoresParamKeyPress(this)'>
        </td>
      </tr>
      <tr>
        <td align='middle' style='width: 37%'>
          <input type='button' value='A�adir' name='nuevoValorParamBtn' class='boton' disabled
                 onclick='nuevoDatoParamBtnClick("paramValuesListId", "valorparamId", "valoresParamsId")'>
        </td>
        <td align='middle' style='width: 38%'>
          <input type='button' value='Quitar' name='quitarValorParamBtn' class='boton' disabled
                 onclick='quitarDatoParamBtnClick("paramValuesListId", "valoresParamsId")'>
        </td>
      </tr>
      <tr>
        <th align='left' rowspan='2' style='width: 25%'>
            Conj. de parametros CSV (Obl. al ejecutar - token "&lt;param-in&gt;" -)
        </th>
        <td colspan='2' style='width: 75%'>
          <select id='paramSetListId' style='width: 100%' size='5' name='paramSetList' class='select'></select>
          <textarea id='valoresconjparamsId' type='text' name='valoresconjparams'
                    style='width: 100%' rows='5' disabled
                    onkeypress='return valoresParamKeyPress(this)'></textarea>
        </td>
      </tr>
      <tr>
        <td align='middle' style='width: 37%'>
          <input type='button' value='A�adir' id="paramCsvAgregar" name='nuevoValorParamBtn' class='boton' disabled
                 onclick='nuevoDatoParamBtnClick("paramSetListId", "valoresconjparamsId", "valoresParamsInId")'>
        </td>
        <td align='middle' style='width: 38%'>
          <input type='button' value='Quitar' id="paramCsvBorrar" name='quitarValorParamBtn' class='boton' disabled
                 onclick='quitarDatoParamBtnClick("paramSetListId", "valoresParamsInId")'>
        </td>
      </tr>
      <tr>
        <td align='middle' colspan='3' style='width: 100%'>
          <input type='button' value='Ejecutar' name='ejecutarquerybtn'
                 onclick='queryActionBtnsClick("execute", "<%= CONTROLLER %>")' class='boton'>&nbsp;&nbsp;
          <input type='button' value='Guardar' name='guardarquerybtn'
                 onclick='queryActionBtnsClick("save", "<%= CONTROLLER %>")' class='boton'>&nbsp;&nbsp;
        </td>
      </tr>
    </table>
    <input type='hidden' name='cmd' value=''>
    <input type='hidden' name='contentType' value='text/html'>
    <input type='hidden' id="nombresParamsId" name='nombresParams' value=''>
    <input type='hidden' id="valoresParamsId" name='valoresParams' value=''>
    <input type='hidden' id="valoresParamsInId" name='valoresParamsIn' value=''>
    <input type='hidden' name='creador' value='<%= creador %>'>
    <input type='hidden' name='tipoOpcion' value='<%= tipoOpcion %>'>
  </form><%
}else if( cmd.equals("sql_list") ){
  List queriesList = model.consultaService.getConsultasSQL(userLoggedIn);
  if( queriesList.size() == 0 ) { %>
    <br><br><center><b>Usted no Tiene Consultas Asociadas</b></center><%
  }else{%>
    <table cellspacing='2' cellpadding='2' width='80%' align='center' border='1' class='fondotabla'>
        <tr class='titulo'><th colspan='8'>Consultas Disponibles</th></tr>
        <tr class='subtitulo'>
          <th nowrap>Ejecucion</th>
          <th nowrap>Par&aacute;metros</th>
          <th nowrap>Generar Como</th>
          <th nowrap>Descripci�n</th>
          <th nowrap>Base de Datos</th>
          <th nowrap>Parametrizado</th>
          <th nowrap>Creador</th>
          <th nowrap>Fecha de Creaci�n</th>
        </tr>
        <%
          Iterator queriesIt = queriesList.iterator();
          while( queriesIt.hasNext() )
          {
            ConsultaSQL sql         = (ConsultaSQL) queriesIt.next();
            String recordId         = sql.getRecordId();
            String descripcion      = sql.getDescripcion();
            String [] nombresParams = sql.getNombresParametros();
            String creador          = sql.getCreador();
            String fechaCreacion    = sql.getFechaCreacion();
            String baseDeDatos      = sql.getBaseDeDatos();
            List paramTokensList    = sql.getTokensOrdenados();
            Iterator paramTokensIt  = paramTokensList.iterator();
            ArrayList<ComponentesCosulta> componente = model.consultaService.getValidarComponente(recordId); %>
        <tr class='peque�a'>
          <td>
            <input type="button" name="btnejecutar" value="Ejecutar" class="boton"
                   onclick="javascript:executeQuery('<%= CONTROLLER %>', <%= recordId %>)">
            <input type='hidden' id='conteoParamsId<%= recordId %>' value='<%= nombresParams == null ? 0 : nombresParams.length %>'>
          </td>
          <td align='center' nowrap><%
          if( nombresParams != null ) { %>
            <table cellSpacing='2' cellPadding='2' width='100%'
                   align='center'><%
            for( int idx = 0; idx < nombresParams.length; idx++ )
            {
              String paramToken = (String) paramTokensIt.next(); %>
              <tr align='left' class='peque�a'>
                <td align='left' nowrap><b><%= sql.getNombresParametros(idx) %></b></td>
                <td width='100%'><%
                if( paramToken.equals("<param>") ) { 
                    //inicio validacion lista.
                   if(componente.isEmpty()){
                       
                %>
                    
                  <!--aqui voy a poner un componente html-->
                      
                  <input type="text" name='param<%= recordId %>'
                         id='param<%= recordId %><%= idx %>'
                         size='20' value='' class="normal">
                             
                <%
                   }else{
                   //aqui van los componentes
                    Iterator listComp=  componente.iterator();
                    ComponentesCosulta comp=null; 
                    while(listComp.hasNext()){
                        comp = (ComponentesCosulta) listComp.next();   
                    }
                   
                    if(comp.getParametro().equals(sql.getNombresParametros(idx))){
                        //aqui vamos a validar las cuentas de tgen esto queda para otro dia--CUENTASMOV.
                       // if(sql.getDescripcion().equals("reporte_mov_auxiliar")) {    

                        String [] cuentas = model.consultaService.getObtenerCuentas(userLoggedIn).split(",");
                        if(cuentas.length-1==0 && cuentas[0].equals("")){
                          out.println(": No tienes cuentas asociadas para este parametro.");
                       }else{
                           %> 
                           <select name='param<%=recordId %>' id='param<%=recordId %><%= idx %>' class="select">
                               <option value=''>...</option>
                           <%
                            for(int j=0;j<=cuentas.length-1 ;j++){

                              %>

                                                   <option value='<%=cuentas[j]%>'><%=cuentas[j]%></option>

                                    <%                 
                            }                            
                                    %>
                         
                                                                 
                                        </select> 
                      <% } 
                        
                       // } //esta llave va con el if reporte_mov_auxiliar
                    } else {%>
                       
                         <input type="text" name='param<%= recordId %>'
                         id='param<%= recordId %><%= idx %>'
                         size='20' value='' class="normal">
                       
                     <%   } 
                   }
                       
                }else if( paramToken.equals("<param-in>") ) { %>
                  <br>IMPORTANTE: este par&aacute;metro debe separar con el caracter de salto de l&iacute;nea cada valor.<br>
                  <textarea name='param<%= recordId %>' style="width: 100%"
                            id='param<%= recordId %><%= idx %>' class="normal"
                            rows='5'></textarea><%
                } %>
                </td>
              </tr><%
            } %>
            </table><%
          }else{ %>
            &nbsp;<%
          } %>
          </td>
          <td>
            <select name='contentType' id='contentType<%= recordId %>' class="select">
              <option value='text/html'>P&aacute;gina web
              <option value='<%= FileGenerator.MIME_MS_EXCEL %>'>MS-Excel
              <option value='file'>Archivo MS-Excel
              <option value='nitobix'>nitobi1<!--20101117-->
            </select>
          </td>
          <td align='left' nowrap>
            <b><%= descripcion %></b>
            <input type='hidden' name='desc<%= recordId %>'
                   id='desc<%= recordId %>'
                   value='<%= descripcion %>'>
          </td>
          <td align='center' nowrap>
            <%= baseDeDatos.equals("PSQL_SOT") ? "PostgreSQL - SOT" :
               (baseDeDatos.equals("ORA_MIMS") ?"Oracle - MIMS" : "") %>
            <input type='hidden' name='baseDeDatos<%= recordId %>'
                   id='baseDeDatos<%= recordId %>'
                   value='<%= baseDeDatos %>'>
          </td>
          <td align='center' nowrap><%= nombresParams != null ? "S�" : "No" %></td>
          <td align='center' nowrap><%= creador %></td>
          <td align='center' nowrap><%= fechaCreacion %></td>
        </tr><%
      } %>
    </table><%
  }
}else if( cmd.equals("sql_execute") || cmd.equals("sql_listexec") ){

    List queryResultsList = model.consultaService.getActionResultsList();
    if (queryResultsList.size() <= 0) {%>
        <center><b>No Hay Registros</b></center><%  } else {
      Iterator queryResultsIt = queryResultsList.iterator();
      ConsultaSQL sqlResult = (ConsultaSQL) queryResultsIt.next();%>
        <table cellspacing='2' cellpadding='2' width='100%' align='center' border='1' class='fondotabla'>
            <tr class='titulo' align="left"><!--20101116-->
                <th colspan='<%= sqlResult.getConteoColumnas()%>'>
                    &nbsp;&nbsp;&nbsp;<%= request.getParameter("descripcion")%><!--20101116-->
                </th>
            </tr>
        </table><!--20101116-->
        <%
          if (!(nitobix != null && nitobix.equals("nitobix"))) {//20101117
%>
        <table cellspacing='2' cellpadding='2' width='100%' align='center' border='1' class='fondotabla'><!--20101116-->
            <tr class='subtitulo'><%
        String[] nombresCampos = sqlResult.getNombresCampos();
        for (int idx = 0; idx < nombresCampos.length; idx++) {%>
                <th nowrap>
                    <%= nombresCampos[idx].equals("") ? "&nbsp" : nombresCampos[idx]%>
                </th><%
        }%>
            </tr><%
              boolean hasNext = true;
              int c = 0;
              while (hasNext) {%>
            <tr class='peque�a'><%
          for (int idx = 0; idx < nombresCampos.length; idx++) {
              String valorCampo = sqlResult.getValorCampo(nombresCampos[idx]);%>
                <td nowrap>
                    <%= valorCampo.equals("") ? "&nbsp" : valorCampo%>
                </td><%
          }%>
            </tr><%
                hasNext = queryResultsIt.hasNext();
                c++;
                if (hasNext) {
                    sqlResult = (ConsultaSQL) queryResultsIt.next();
            }
        }%>
        </table><!--20101116-->
        <table cellspacing='2' cellpadding='2' width='100%' align='center' border='1' class='fondotabla'><!--20101116-->
            <tr class='peque�a'><td colspan='<%=nombresCampos.length%>'><%=(c + " REGISTROS SELECCIONADOS.")%></td></tr>
        </table><!--20101116-->
        <%
        } else {//20101117
            String cabecera = "";
            queryResultsIt = queryResultsList.iterator();
            sqlResult = (ConsultaSQL) queryResultsIt.next();
            String[] nombresCampos2 = sqlResult.getNombresCampos();
            String[] letras = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "aa", "ab", "ac", "ad","ae","af","ag","ah","ai","aj","ak","al","am","an","ao","ap","aq","ar","as","at","au","av","aw","ax","ay","az","ba","bb","bc","bd","be","bf","bg","bh","bi"};
            for (int idx = 0; idx < nombresCampos2.length; idx++) {
                if (idx < (nombresCampos2.length - 1)) {
                    cabecera = cabecera + (nombresCampos2[idx].equals("") ? "" : " "+nombresCampos2[idx]) + "|";
                } else {
                    cabecera = cabecera + (nombresCampos2[idx].equals("") ? "" : " "+nombresCampos2[idx]);
                }
            }
            //cabecera = cabecera.replaceAll(" ", "_");
        %>
    <ntb:grid id="SimpleGrid"
              mode="localnonpaging"
              width="95%"
              height="700"
              pagingmode="None"
              toolbarenabled="false"
              datasourceid="data"
              rowhighlightenabled="true" >
        <ntb:datasources>
            <ntb:datasource id="data">
                <ntb:datasourcestructure FieldNames="<%=cabecera%>"></ntb:datasourcestructure>
                <ntb:data>
                    <%
           //System.out.println("cabecer" + cabecera);
           boolean hasNext2 = true;
           int c2 = 0;
           String filax = "";
           String iniciox = "";
           while (hasNext2) {
               iniciox = "<ntb:e xi='" + (c2 + 1) + "'";
               filax ="";
               for (int idx = 0; idx < nombresCampos2.length; idx++) {
                   String valorCampo2 = sqlResult.getValorCampo(nombresCampos2[idx]);
                   valorCampo2=valorCampo2.replaceAll("<br>", "XX");
                   valorCampo2=valorCampo2.replaceAll("\"", "XX");
                   valorCampo2=valorCampo2.replaceAll(">", "XX");
                   valorCampo2=valorCampo2.replaceAll("<", "XX");
                   valorCampo2=valorCampo2.replaceAll("'", "XX");
                   filax = filax + " " + letras[idx] + "='" + (valorCampo2.equals("") ? "" : valorCampo2) + "'";
               }
               filax = iniciox+filax + "></ntb:e>";
               //System.out.println("filax" + filax);
                    %><%=filax%><%
            hasNext2 = queryResultsIt.hasNext();
            c2++;
            if (hasNext2) {
                sqlResult = (ConsultaSQL) queryResultsIt.next();
            }
        }
                    %>
                    <!--
                    <ntb:e xi="1" a="Tammara" b="blue" c="cat" ></ntb:e>
                    <ntb:e xi="2" a="Dwana Barton" b="red" c="dog"></ntb:e>-->
                </ntb:data>
            </ntb:datasource>
        </ntb:datasources>
    </ntb:grid>
    <table cellspacing='2' cellpadding='2' width='100%' align='center' border='1' class='fondotabla'><!--20101116-->
        <tr class='peque�a'><td colspan='<%=nombresCampos2.length%>'><%=(c2 + " REGISTROS SELECCIONADOS.")%></td></tr>
    </table><!--20101116-->
    <%}%>
    <!--</table>20101117-->
    <%
   }

}else if( cmd.equals("sql_update") ){
  //ACTUALIZACION DE QUERYS
  DatosConsulta qry = (DatosConsulta)request.getAttribute("consulta"); %>
    <input:form name='filtroCrearQueryFrm' method='post' action='<%= CONTROLLER + "?estado=Consultas&accion=Manage" %>' bean="consulta">
    <table cellspacing='2' cellpadding='2' width='100%' align='center' border='0' class='fondotabla'>
      <tr class='titulo'><th colspan='3'>Modificar Consulta</th></tr>
      <tr><td colspan='3'>&nbsp;</td></tr>
      <tr>
        <th align='left' style='width: 25%'>Creador</th>
        <td align='left' colspan='2' style='width: 75%'><%=qry.getCreador()%></td>
      </tr>
      <tr>
        <th align='left' style="width: 25%">
            Usuarios Asociados
        </th>
        <td>
          <input:select name="usuario" attributesText="class='select'">
           <input:option value='' attributesText="selected"> </input:option><%
            List usersList = model.consultaService.getUsuariosSot(null);
            Iterator usersIt = usersList.iterator();
            Usuario user = null;
            while( usersIt.hasNext() )
            {
              user = (Usuario) usersIt.next();
              %>
              <input:option value='<%= user.getLogin() %>'><%= user.getNombre() %></input:option><%
            } %>
            </input:select>
        </td>
        <td>
           <input:select name="userAdd" multiple="true" attributesText="style='width: 75%' class='select'" options="<%=qry.getUserAdd()%>"/>
        </td>
      </tr>
      <tr>
         <td></td>
         <td><input type='button' name='agregar' class='boton' value='Adicionar Usuario' onclick='addElement(usuario, userAdd)'></td>
         <td><input type='button' name='eliminar' class='boton' value='Eliminar Usuario' onclick='removeElem(userAdd)'></td>
      </tr>
      <tr>
        <th align='left' style='width: 25%'>Descripcion</th>
        <td colspan='2' style='width: 75%'>
          <input:textarea name="descripcion" rows="3" attributesText="style='width: 100%'"></input:textarea>
        </td>
      </tr>
      <tr>
        <th align='left' style='width: 25%'>
            Base de Datos en la Cual se va a Ejecutar la Consulta
        </th>
        <td colspan='2'>
          <input:select name="baseDeDatos" attributesText="style='width: 100%' class='select'" default="<%=qry.getBaseDeDatos()%>">
              <input:option value="PSQL_SOT">POSTGRESQL - SOT</input:option>
              <!--<input:option value="ORA_MIMS">ORACLE - MIMS</input:option>-->
          </input:select>
        </td>
      </tr>
      <tr><th align='left' colspan='3'>&nbsp;</th></tr>
      <tr>
        <th align='left' colspan='3'>
            Query<br>NOTA: Puede parametrizar las consultas colocando los tokens
            <i>&lt;param&gt;</i> para valores especificos<br>y <i>&lt;param-in&gt;</i>
            para listas de parametros separados por comas.<br>
            ADVERTENCIA: Los queries involucrados en la consulta (los de creacion,
            seleccion y eliminacion de tablas) DEBEN ser separados con el token "&lt;br&gt;"
        </th>
      </tr>
      <tr>
        <td colspan='3'>
          <textarea name='query' rows='17' onblur='queryBlur()' style='width: 100%' onfocus='jscript:oldQuery = this.value;'><%=qry.getQuery()%></textarea>
        </td>
      </tr>
      <tr><th align='left' colspan='3'>&nbsp;</th></tr>
      <tr>
        <th align='left' rowspan='2' style='width: 25%'>
            Nombres de los parametros (Obligatorios al guardar)
        </th>
        <td colspan='2' style='width: 75%'>
          <input:select name="paramNamesList" attributesText="id='paramNamesListId' style='width: 100%' size='5' class='select'" options="<%=qry.getParamNamesList()%>"/>
          <input:text name="nombreparam" attributesText="id='nombreparamId' style='width: 100%' disabled"/>
        </td>
      </tr>
      <tr>
        <td align='middle' style='width: 37%'>
          <input type='button' value='A�adir' id="nombreParamAgregar" name='nuevoValorParamBtn' class='boton'
                 onclick='nuevoDatoParamBtnClick("paramNamesListId", "nombreparamId", "nombresParamsId")' disabled>
        </td>
        <td align='middle' style='width: 38%'>
          <input type='button' value='Quitar' id="nombreParamBorrar" name='quitarValorParamBtn' class='boton'
                 onclick='quitarDatoParamBtnClick("paramNamesListId", "nombresParamsId")' disabled>
        </td>
        <!--
        <td align='middle' style='width: 37%'>
          <input type='button' value='A�adir' name='nuevoNombreParamBtn' disabled
                 onclick='nuevoNombreParamBtnClick()' class='boton'>
        </td>
        <td align='middle' style='width: 38%'>
          <input type='button' value='Quitar' name='quitarNombreParamBtn' disabled
                 onclick='quitarNombreParamBtnClick()' class='boton'>
        </td>
        -->
      </tr>
      <tr>
        <th align='left' rowspan='2' style='width: 25%'>
            Valores de los parametros (Obligatorios al ejecutar)
        </th>
        <td colspan='2' style='width: 75%'>
          <input:select name="paramValuesList" attributesText="id='paramValuesListId' style='width: 100%' size='5' class='select'"/>
          <input:text name="valorparam" attributesText="id='valorparamId' style='width: 100%' disabled"/>
        </td>
      </tr>
      <tr>
        <td align='middle' style='width: 37%'>
          <input type='button' value='A�adir' name='nuevoValorParamBtn' class='boton' disabled
                 onclick='nuevoDatoParamBtnClick("paramValuesListId", "valorparamId", "valoresParamsId")'>
        </td>
        <td align='middle' style='width: 38%'>
          <input type='button' value='Quitar' name='quitarValorParamBtn' class='boton' disabled
                 onclick='quitarDatoParamBtnClick("paramValuesListId", "valoresParamsId")'>
        </td>

        <!--
        <td align='middle' style='width: 37%'>
          <input type='button' value='A�adir' name='nuevoValorParamBtn' disabled
                 onclick='nuevoValorParamBtnClick()' class='boton'>
        </td>
        <td align='middle' style='width: 38%'>
          <input type='button' value='Quitar' name='quitarValorParamBtn' disabled
                 onclick='quitarValorParamBtnClick()' class='boton'>
        </td>
        -->
      </tr>
      <tr>
        <th align='left' rowspan='2' style='width: 25%'>
            Conj. de parametros CSV (Obl. al ejecutar - token "&lt;param-in&gt;" -)
        </th>
        <td colspan='2' style='width: 75%'>
          <select id='paramSetListId' style='width: 100%' size='5' name='paramSetList' class='select'></select>
          <textarea id='valoresconjparamsId' type='text' name='valoresconjparams'
                    style='width: 100%' rows='5' disabled
                    onkeypress='return valoresParamKeyPress(this)'></textarea>
        </td>
      </tr>
      <tr>
        <td align='middle' style='width: 37%'>
          <input type='button' value='A�adir' id="paramCsvAgregar" name='nuevoValorParamBtn' class='boton' disabled
                 onclick='nuevoDatoParamBtnClick("paramSetListId", "valoresconjparamsId", "valoresParamsInId")'>
        </td>
        <td align='middle' style='width: 38%'>
          <input type='button' value='Quitar' id="paramCsvBorrar" name='quitarValorParamBtn' class='boton' disabled
                 onclick='quitarDatoParamBtnClick("paramSetListId", "valoresParamsInId")'>
        </td>
      </tr>
      <tr>
        <td align='middle' colspan='3' style='width: 100%'>
          <input type='button' value='Ejecutar' name='ejecutarquerybtn'
                 onclick='queryActionBtnsClick("execute", "<%= CONTROLLER %>")' class='boton'>&nbsp;&nbsp;
          <input type='button' value='Modificar' name='ejecutarquerybtn'
                 onclick='queryActionBtnsClick("update", "<%= CONTROLLER %>")' class='boton'>&nbsp;&nbsp;
        </td>
      </tr>
    </table>
    <input type='hidden' name='cmd' value=''>
    <input type='hidden' name='contentType' value='text/html'>
    <input type='hidden' id="nombresParamsId" name='nombresParams' value=''>
    <input type='hidden' id="valoresParamsId" name='valoresParams' value=''>
    <input type='hidden' id="valoresParamsInId" name='valoresParamsIn' value=''>
    <input type='hidden' name='creador' value=''>
    <input type='hidden' name='id' value='<%= qry.getId() %>'>
    <input type='hidden' name='tipoOpcion' value='<%= tipoOpcion %>'>
  </input:form>
<%
}
else if( cmd.equals("file") ) {%>
<table class="fondotabla" align="center">
      <tr align='center'>
         <td class='normal'>
             SU ARCHIVO SE ESTA GENERANDO VAYA AL SERVICIO DE DESCARGAS
             <img src="images/file.gif"> PARA BUSCARLO...
         </td>
      </tr>
</table>
<%
}
%>
</body>

            <!--20101116-->
            <script type="text/javascript">
                        if (!document.getBoxObjectFor) {
                            document.getBoxObjectFor = function(el) {
                                var b = el.getBoundingClientRect();
                                return { x: b.left, y: b.top, width: b.width, height: b.height };
                            };
                        }
                        <%if (cmd.equals("sql_execute") || cmd.equals("sql_listexec")) {
                            if (nitobix != null && nitobix.equals("nitobix")) {//20101117
                        %>
                        nitobi.html.attachEvent(window,"load",function(){nitobi.loadComponent("SimpleGrid")});
                            <%}
                        }%>

            </script>
            <!--20101116-->

</html>
<%
} catch (Exception e) {
    System.out.println("error en PantallaConsultas.jsp:" + e.toString());
    e.printStackTrace();
}
%>