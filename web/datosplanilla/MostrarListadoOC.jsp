<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<html>
<head>
  <title>Listado de Remisiones</title>
  <link href="<%= BASEURL %>/css/Style.css" rel='stylesheet'>
  <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>
  <SCRIPT language=JavaScript1.2 
    src="<%=BASEURL%>/js/coolmenus3.js">
  </SCRIPT>
</head>
<body>
    <BR><BR><BR>   
    <center> 
    <a href='<%= BASEURL  %>/index.html'>Index</a> <br><br>   
    <% 
       String tipo=request.getParameter("tipo");
       String fecha1=request.getParameter("f1");
       String fecha2=request.getParameter("f2");
       String titulo=(tipo.equals("2"))?"OCs REALIZADAS ENTRE ":"OCs NO CUMPLIDAS ENTRE ";
       titulo+=fecha1+"  "+ fecha2;
   
       List lista = model.PlanillasSvc.getList();
    if(request.getParameter("comentario").equals("") && lista!=null )
    {
    %>
       <TABLE width='800' class='fondotabla' border='1'>
         <TR><TH class='titulo1' colspan='4'><br><%= titulo %><br><br></TH></TR>
         <TR class='titulo3'>
            <TH width='10%'> PLANILLA </TH>
            <TH width='15%'> FEC. DESPACHO</TH>
            <TH width='30%'> CLIENTE</TH>
            <TH width='60%'> STDJOB</TH>
        </TR>
       <%
         
         Iterator it=lista.iterator();
         if(it!=null)
            while(it.hasNext()){
              Planillas planilla = (Planillas) it.next();
              %>   
               <TR class='comentario'>
                  <TD align='center'>
                      <a href="<%=CONTROLLER%>?ACCION=/ConsultaOC/Buscar&tipo=1&numeroOC=<%=  planilla.getPlanilla()%>&distrito=<%= planilla.getDistrito() %>" > 
                        <%=  planilla.getPlanilla()      %>
                      </a>
                  </TD>
                  <TD>&nbsp<%=  planilla.getFechaDespacho() %></TD>
                  <TD>&nbsp<%=  planilla.getCliente()       %></TD>
                  <TD>&nbsp<%=  planilla.getSJDescripcion() %></TD>
               </TR>
              <%  
            }
       %>
      </TABLE>
 <%
  }
  else
  {%>
    <BR><BR>
    <TABLE align='center' width='600' class='fondotabla'>
      <TR><TH Class='titulo1'>INFORMACION DE BUSQUEDA<BR><%= titulo %></TH></TR>
      <TR><TD class='comentario2' align='center'><br><br><%= request.getParameter("comentario") %><br><br><br><br><br></TD></TR>
    </TABLE>
   <%}%>
   
</body>
</html>
